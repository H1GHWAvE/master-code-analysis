.bytecode 50.0
.class public final synchronized net/hockeyapp/android/d/h
.super net/hockeyapp/android/d/g

.field protected 'h' Z

.field private 'i' Landroid/app/Activity;

.field private 'j' Landroid/app/AlertDialog;

.method public <init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial net/hockeyapp/android/d/g/<init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
aload 0
aconst_null
putfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 0
aconst_null
putfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
aload 0
iconst_0
putfield net/hockeyapp/android/d/h/h Z
aload 1
ifnull L0
aload 0
aload 1
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/app/Activity
putfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
L0:
aload 0
iload 5
putfield net/hockeyapp/android/d/h/h Z
return
.limit locals 6
.limit stack 5
.end method

.method static synthetic a(Lnet/hockeyapp/android/d/h;)Landroid/app/Activity;
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lnet/hockeyapp/android/d/h;Lorg/json/JSONArray;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
ifnull L1
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokevirtual android/app/Activity/getFragmentManager()Landroid/app/FragmentManager;
invokevirtual android/app/FragmentManager/beginTransaction()Landroid/app/FragmentTransaction;
astore 3
aload 3
sipush 4097
invokevirtual android/app/FragmentTransaction/setTransition(I)Landroid/app/FragmentTransaction;
pop
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokevirtual android/app/Activity/getFragmentManager()Landroid/app/FragmentManager;
ldc "hockey_update_dialog"
invokevirtual android/app/FragmentManager/findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
astore 2
aload 2
ifnull L3
aload 3
aload 2
invokevirtual android/app/FragmentTransaction/remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
pop
L3:
aload 3
aconst_null
invokevirtual android/app/FragmentTransaction/addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;
pop
ldc net/hockeyapp/android/at
astore 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
ifnull L0
ldc net/hockeyapp/android/at
astore 2
L0:
aload 2
ldc "newInstance"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc org/json/JSONArray
aastore
dup
iconst_1
ldc java/lang/String
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aconst_null
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
ldc "apk"
invokevirtual net/hockeyapp/android/d/h/a(Ljava/lang/String;)Ljava/lang/String;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/app/DialogFragment
aload 3
ldc "hockey_update_dialog"
invokevirtual android/app/DialogFragment/show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
pop
L1:
return
L2:
astore 2
ldc "HockeyApp"
ldc "An exception happened while showing the update fragment:"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
ldc "HockeyApp"
ldc "Showing update activity instead."
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 1
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial net/hockeyapp/android/d/h/a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V
return
.limit locals 4
.limit stack 7
.end method

.method static synthetic a(Lnet/hockeyapp/android/d/h;Lorg/json/JSONArray;Ljava/lang/Boolean;)V
aload 0
aload 1
aload 2
invokespecial net/hockeyapp/android/d/h/a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation
aconst_null
astore 3
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
ifnull L0
ldc net/hockeyapp/android/al
astore 3
L0:
aload 3
astore 4
aload 3
ifnonnull L1
ldc net/hockeyapp/android/al
astore 4
L1:
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
ifnull L2
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 3
aload 3
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 4
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 3
ldc "json"
aload 1
invokevirtual org/json/JSONArray/toString()Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 3
ldc "url"
aload 0
ldc "apk"
invokevirtual net/hockeyapp/android/d/h/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 3
invokevirtual android/app/Activity/startActivity(Landroid/content/Intent;)V
aload 2
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L2
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokevirtual android/app/Activity/finish()V
L2:
aload 0
invokevirtual net/hockeyapp/android/d/h/b()V
return
.limit locals 5
.limit stack 4
.end method

.method private b(Lorg/json/JSONArray;)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 1
invokevirtual org/json/JSONArray/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/e/x/a(Landroid/content/Context;Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
ifnull L0
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokevirtual android/app/Activity/isFinishing()Z
ifeq L1
L0:
return
L1:
new android/app/AlertDialog$Builder
dup
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 2
aload 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 513
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 0
getfield net/hockeyapp/android/d/h/f Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L2
aload 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 514
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 515
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d/i
dup
aload 0
invokespecial net/hockeyapp/android/d/i/<init>(Lnet/hockeyapp/android/d/h;)V
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 516
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d/j
dup
aload 0
aload 1
invokespecial net/hockeyapp/android/d/j/<init>(Lnet/hockeyapp/android/d/h;Lorg/json/JSONArray;)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 2
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
putfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
aload 0
getfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
return
L2:
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 512
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
iconst_1
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
aload 0
aload 1
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial net/hockeyapp/android/d/h/a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V
return
.limit locals 3
.limit stack 6
.end method

.method private c(Lorg/json/JSONArray;)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
ifnull L1
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokevirtual android/app/Activity/getFragmentManager()Landroid/app/FragmentManager;
invokevirtual android/app/FragmentManager/beginTransaction()Landroid/app/FragmentTransaction;
astore 3
aload 3
sipush 4097
invokevirtual android/app/FragmentTransaction/setTransition(I)Landroid/app/FragmentTransaction;
pop
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokevirtual android/app/Activity/getFragmentManager()Landroid/app/FragmentManager;
ldc "hockey_update_dialog"
invokevirtual android/app/FragmentManager/findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
astore 2
aload 2
ifnull L3
aload 3
aload 2
invokevirtual android/app/FragmentTransaction/remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
pop
L3:
aload 3
aconst_null
invokevirtual android/app/FragmentTransaction/addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;
pop
ldc net/hockeyapp/android/at
astore 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
ifnull L0
ldc net/hockeyapp/android/at
astore 2
L0:
aload 2
ldc "newInstance"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc org/json/JSONArray
aastore
dup
iconst_1
ldc java/lang/String
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aconst_null
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
ldc "apk"
invokevirtual net/hockeyapp/android/d/h/a(Ljava/lang/String;)Ljava/lang/String;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/app/DialogFragment
aload 3
ldc "hockey_update_dialog"
invokevirtual android/app/DialogFragment/show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
pop
L1:
return
L2:
astore 2
ldc "HockeyApp"
ldc "An exception happened while showing the update fragment:"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
ldc "HockeyApp"
ldc "Showing update activity instead."
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 1
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial net/hockeyapp/android/d/h/a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V
return
.limit locals 4
.limit stack 7
.end method

.method public final a()V
aload 0
invokespecial net/hockeyapp/android/d/g/a()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 0
getfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
ifnull L0
aload 0
getfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/dismiss()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method protected final a(Lorg/json/JSONArray;)V
aload 0
aload 1
invokespecial net/hockeyapp/android/d/g/a(Lorg/json/JSONArray;)V
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/d/h/h Z
ifeq L0
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 1
invokevirtual org/json/JSONArray/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/e/x/a(Landroid/content/Context;Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
ifnull L0
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokevirtual android/app/Activity/isFinishing()Z
ifeq L1
L0:
return
L1:
new android/app/AlertDialog$Builder
dup
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 2
aload 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 513
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 0
getfield net/hockeyapp/android/d/h/f Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L2
aload 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 514
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 515
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d/i
dup
aload 0
invokespecial net/hockeyapp/android/d/i/<init>(Lnet/hockeyapp/android/d/h;)V
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 2
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 516
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d/j
dup
aload 0
aload 1
invokespecial net/hockeyapp/android/d/j/<init>(Lnet/hockeyapp/android/d/h;Lorg/json/JSONArray;)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 2
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
putfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
aload 0
getfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
return
L2:
aload 0
getfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 0
getfield net/hockeyapp/android/d/h/g Lnet/hockeyapp/android/az;
sipush 512
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
iconst_1
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
aload 0
aload 1
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial net/hockeyapp/android/d/h/a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V
return
.limit locals 3
.limit stack 6
.end method

.method protected final b()V
aload 0
invokespecial net/hockeyapp/android/d/g/b()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/h/i Landroid/app/Activity;
aload 0
aconst_null
putfield net/hockeyapp/android/d/h/j Landroid/app/AlertDialog;
return
.limit locals 1
.limit stack 2
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 0
aload 1
checkcast org/json/JSONArray
invokevirtual net/hockeyapp/android/d/h/a(Lorg/json/JSONArray;)V
return
.limit locals 2
.limit stack 2
.end method
