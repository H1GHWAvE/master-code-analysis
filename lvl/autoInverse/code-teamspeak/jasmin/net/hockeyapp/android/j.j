.bytecode 50.0
.class public final synchronized net/hockeyapp/android/j
.super java/lang/Object
.implements java/lang/Thread$UncaughtExceptionHandler

.field 'a' Lnet/hockeyapp/android/i;

.field private 'b' Z

.field private 'c' Ljava/lang/Thread$UncaughtExceptionHandler;

.method public <init>(Ljava/lang/Thread$UncaughtExceptionHandler;Lnet/hockeyapp/android/i;Z)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield net/hockeyapp/android/j/b Z
aload 0
aload 1
putfield net/hockeyapp/android/j/c Ljava/lang/Thread$UncaughtExceptionHandler;
aload 0
iload 3
putfield net/hockeyapp/android/j/b Z
aload 0
aload 2
putfield net/hockeyapp/android/j/a Lnet/hockeyapp/android/i;
return
.limit locals 4
.limit stack 2
.end method

.method private static a()Ljava/lang/String;
aconst_null
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L1
L0:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
pop
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L1:
astore 0
return
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/lang/Throwable;Lnet/hockeyapp/android/i;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
astore 2
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 3
aload 0
new java/io/PrintWriter
dup
aload 3
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
invokevirtual java/lang/Throwable/printStackTrace(Ljava/io/PrintWriter;)V
L0:
invokestatic java/util/UUID/randomUUID()Ljava/util/UUID;
invokevirtual java/util/UUID/toString()Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".stacktrace"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Writing unhandled exception to: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/io/BufferedWriter
dup
new java/io/FileWriter
dup
aload 4
invokespecial java/io/FileWriter/<init>(Ljava/lang/String;)V
invokespecial java/io/BufferedWriter/<init>(Ljava/io/Writer;)V
astore 4
aload 4
new java/lang/StringBuilder
dup
ldc "Package: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 4
new java/lang/StringBuilder
dup
ldc "Version Code: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 4
new java/lang/StringBuilder
dup
ldc "Version Name: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 4
new java/lang/StringBuilder
dup
ldc "Android: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 4
new java/lang/StringBuilder
dup
ldc "Manufacturer: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 4
new java/lang/StringBuilder
dup
ldc "Model: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/h Ljava/lang/String;
ifnull L1
aload 4
new java/lang/StringBuilder
dup
ldc "CrashReporter Key: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/h Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
L1:
aload 4
new java/lang/StringBuilder
dup
ldc "Date: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 4
ldc "\n"
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 4
aload 3
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 4
invokevirtual java/io/BufferedWriter/flush()V
aload 4
invokevirtual java/io/BufferedWriter/close()V
L3:
aload 1
ifnull L5
L4:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".user"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/j/a(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".contact"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/j/a(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".description"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/j/a(Ljava/lang/String;)V
L5:
return
L2:
astore 0
ldc "HockeyApp"
ldc "Error saving exception stacktrace!\n"
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 5
.limit stack 5
.end method

.method private a(Lnet/hockeyapp/android/i;)V
aload 0
aload 1
putfield net/hockeyapp/android/j/a Lnet/hockeyapp/android/i;
return
.limit locals 2
.limit stack 2
.end method

.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
getstatic net/hockeyapp/android/a/a Ljava/lang/String;
ifnonnull L6
aload 0
getfield net/hockeyapp/android/j/c Ljava/lang/Thread$UncaughtExceptionHandler;
aload 1
aload 2
invokeinterface java/lang/Thread$UncaughtExceptionHandler/uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V 2
return
L6:
aload 0
getfield net/hockeyapp/android/j/a Lnet/hockeyapp/android/i;
astore 3
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
astore 4
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 5
aload 2
new java/io/PrintWriter
dup
aload 5
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
invokevirtual java/lang/Throwable/printStackTrace(Ljava/io/PrintWriter;)V
L0:
invokestatic java/util/UUID/randomUUID()Ljava/util/UUID;
invokevirtual java/util/UUID/toString()Ljava/lang/String;
astore 6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".stacktrace"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 7
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Writing unhandled exception to: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/io/BufferedWriter
dup
new java/io/FileWriter
dup
aload 7
invokespecial java/io/FileWriter/<init>(Ljava/lang/String;)V
invokespecial java/io/BufferedWriter/<init>(Ljava/io/Writer;)V
astore 7
aload 7
new java/lang/StringBuilder
dup
ldc "Package: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 7
new java/lang/StringBuilder
dup
ldc "Version Code: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 7
new java/lang/StringBuilder
dup
ldc "Version Name: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 7
new java/lang/StringBuilder
dup
ldc "Android: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 7
new java/lang/StringBuilder
dup
ldc "Manufacturer: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 7
new java/lang/StringBuilder
dup
ldc "Model: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/h Ljava/lang/String;
ifnull L1
aload 7
new java/lang/StringBuilder
dup
ldc "CrashReporter Key: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/h Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
L1:
aload 7
new java/lang/StringBuilder
dup
ldc "Date: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 7
ldc "\n"
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 7
aload 5
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 7
invokevirtual java/io/BufferedWriter/flush()V
aload 7
invokevirtual java/io/BufferedWriter/close()V
L3:
aload 3
ifnull L5
L4:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".user"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/j/a(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".contact"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/j/a(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".description"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic net/hockeyapp/android/j/a(Ljava/lang/String;)V
L5:
aload 0
getfield net/hockeyapp/android/j/b Z
ifne L7
aload 0
getfield net/hockeyapp/android/j/c Ljava/lang/Thread$UncaughtExceptionHandler;
aload 1
aload 2
invokeinterface java/lang/Thread$UncaughtExceptionHandler/uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V 2
return
L2:
astore 3
ldc "HockeyApp"
ldc "Error saving exception stacktrace!\n"
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L5
L7:
invokestatic android/os/Process/myPid()I
invokestatic android/os/Process/killProcess(I)V
bipush 10
invokestatic java/lang/System/exit(I)V
return
.limit locals 8
.limit stack 5
.end method
