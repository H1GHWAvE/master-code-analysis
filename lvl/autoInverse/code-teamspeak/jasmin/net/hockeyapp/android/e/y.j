.bytecode 50.0
.class public final synchronized net/hockeyapp/android/e/y
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "99.0"

.field private 'b' Ljava/util/ArrayList;

.field private 'c' Lorg/json/JSONObject;

.field private 'd' Lnet/hockeyapp/android/ax;

.field private 'e' I

.method public <init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/ax;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 3
putfield net/hockeyapp/android/e/y/d Lnet/hockeyapp/android/ax;
aload 0
aload 1
aload 2
invokespecial net/hockeyapp/android/e/y/a(Landroid/content/Context;Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/y/b Ljava/util/ArrayList;
new net/hockeyapp/android/e/z
dup
aload 0
invokespecial net/hockeyapp/android/e/z/<init>(Lnet/hockeyapp/android/e/y;)V
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
return
.limit locals 4
.limit stack 4
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)I
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
iconst_m1
istore 3
aload 0
ifnull L8
aload 1
ifnonnull L0
L8:
iconst_0
istore 2
L9:
iload 2
ireturn
L0:
new java/util/Scanner
dup
aload 0
ldc "\\-.*"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokespecial java/util/Scanner/<init>(Ljava/lang/String;)V
astore 0
new java/util/Scanner
dup
aload 1
ldc "\\-.*"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokespecial java/util/Scanner/<init>(Ljava/lang/String;)V
astore 1
aload 0
ldc "\\."
invokevirtual java/util/Scanner/useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;
pop
aload 1
ldc "\\."
invokevirtual java/util/Scanner/useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;
pop
L1:
aload 0
invokevirtual java/util/Scanner/hasNextInt()Z
ifeq L4
aload 1
invokevirtual java/util/Scanner/hasNextInt()Z
ifeq L4
aload 0
invokevirtual java/util/Scanner/nextInt()I
istore 4
aload 1
invokevirtual java/util/Scanner/nextInt()I
istore 5
L3:
iload 3
istore 2
iload 4
iload 5
if_icmplt L9
iload 4
iload 5
if_icmple L1
iconst_1
ireturn
L4:
aload 0
invokevirtual java/util/Scanner/hasNextInt()Z
ifeq L6
L5:
iconst_1
ireturn
L6:
aload 1
invokevirtual java/util/Scanner/hasNextInt()Z
istore 6
L7:
iload 3
istore 2
iload 6
ifne L9
iconst_0
ireturn
L2:
astore 0
iconst_0
ireturn
.limit locals 7
.limit stack 5
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)J
.catch org/json/JSONException from L0 to L1 using L2
L0:
aload 0
aload 1
invokevirtual org/json/JSONObject/getLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
lreturn
L2:
astore 0
lconst_0
lreturn
.limit locals 4
.limit stack 2
.end method

.method private a(ILorg/json/JSONObject;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 5
aload 0
getfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
invokestatic net/hockeyapp/android/e/y/c(Lorg/json/JSONObject;)I
istore 3
aload 2
invokestatic net/hockeyapp/android/e/y/c(Lorg/json/JSONObject;)I
istore 4
aload 2
invokestatic net/hockeyapp/android/e/y/d(Lorg/json/JSONObject;)Ljava/lang/String;
astore 2
aload 5
ldc "<div style='padding: 20px 10px 10px;'><strong>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 1
ifne L0
aload 5
ldc "Newest version:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L1:
aload 5
ldc "</strong></div>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 5
new java/lang/StringBuilder
dup
ldc "Version "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 4
iload 3
if_icmpeq L1
iload 4
aload 0
getfield net/hockeyapp/android/e/y/e I
if_icmpne L1
aload 0
iconst_m1
putfield net/hockeyapp/android/e/y/e I
aload 5
ldc "[INSTALLED]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L1
.limit locals 6
.limit stack 4
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
aload 0
ifnull L0
aload 0
ldc "L"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L1
L0:
ldc "5.0"
astore 1
L2:
aload 1
areturn
L1:
aload 0
ldc "M"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L3
ldc "6.0"
areturn
L3:
aload 0
astore 1
ldc "^[a-zA-Z]+"
aload 0
invokestatic java/util/regex/Pattern/matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z
ifeq L2
ldc "99.0"
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Lorg/json/JSONObject;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 0
invokestatic net/hockeyapp/android/e/y/b(Lorg/json/JSONObject;)Ljava/lang/String;
astore 0
aload 0
invokevirtual java/lang/String/length()I
ifle L0
aload 1
new java/lang/StringBuilder
dup
ldc "<a href='restore:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'  style='background: #c8c8c8; color: #000; display: block; float: right; padding: 7px; margin: 0px 10px 10px; text-decoration: none;'>Restore</a>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L0:
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.catch org/json/JSONException from L0 to L1 using L2
L0:
aload 0
aload 1
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 0
L1:
aload 0
areturn
L2:
astore 0
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
.catch org/json/JSONException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
.catch org/json/JSONException from L4 to L5 using L2
.catch java/lang/NullPointerException from L4 to L5 using L3
.catch org/json/JSONException from L6 to L7 using L2
.catch java/lang/NullPointerException from L6 to L7 using L3
.catch org/json/JSONException from L8 to L9 using L2
.catch java/lang/NullPointerException from L8 to L9 using L3
.catch org/json/JSONException from L9 to L10 using L2
.catch java/lang/NullPointerException from L9 to L10 using L3
aload 0
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
putfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield net/hockeyapp/android/e/y/b Ljava/util/ArrayList;
aload 0
aload 0
getfield net/hockeyapp/android/e/y/d Lnet/hockeyapp/android/ax;
invokeinterface net/hockeyapp/android/ax/getCurrentVersionCode()I 0
putfield net/hockeyapp/android/e/y/e I
L0:
new org/json/JSONArray
dup
aload 2
invokespecial org/json/JSONArray/<init>(Ljava/lang/String;)V
astore 2
aload 0
getfield net/hockeyapp/android/e/y/d Lnet/hockeyapp/android/ax;
invokeinterface net/hockeyapp/android/ax/getCurrentVersionCode()I 0
istore 4
L1:
iconst_0
istore 3
L4:
iload 3
aload 2
invokevirtual org/json/JSONArray/length()I
if_icmpge L11
aload 2
iload 3
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
astore 7
aload 7
ldc "version"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
iload 4
if_icmple L12
L5:
iconst_1
istore 5
L6:
aload 7
ldc "version"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
iload 4
if_icmpne L13
aload 1
aload 7
ldc "timestamp"
invokevirtual org/json/JSONObject/getLong(Ljava/lang/String;)J
invokestatic net/hockeyapp/android/e/y/a(Landroid/content/Context;J)Z
ifeq L13
L7:
iconst_1
istore 6
goto L14
L8:
aload 0
aload 7
putfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
aload 7
ldc "version"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
istore 4
L9:
aload 0
getfield net/hockeyapp/android/e/y/b Ljava/util/ArrayList;
aload 7
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L10:
iload 3
iconst_1
iadd
istore 3
goto L4
L12:
iconst_0
istore 5
goto L6
L13:
iconst_0
istore 6
goto L14
L3:
astore 1
L11:
return
L2:
astore 1
return
L14:
iload 5
ifne L8
iload 6
ifeq L9
goto L8
.limit locals 8
.limit stack 3
.end method

.method public static a(Landroid/content/Context;J)Z
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
ifnonnull L0
L3:
iconst_0
ireturn
L0:
new java/io/File
dup
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/lastModified()J
ldc2_w 1000L
ldiv
lstore 3
L1:
lload 1
lload 3
ldc2_w 1800L
ladd
lcmp
ifle L3
iconst_1
ireturn
L2:
astore 0
aload 0
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
iconst_0
ireturn
.limit locals 5
.limit stack 6
.end method

.method private static b(Lorg/json/JSONObject;)Ljava/lang/String;
.catch org/json/JSONException from L0 to L1 using L2
L0:
aload 0
ldc "id"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 0
L1:
aload 0
areturn
L2:
astore 0
ldc ""
areturn
.limit locals 1
.limit stack 2
.end method

.method private static c(Lorg/json/JSONObject;)I
.catch org/json/JSONException from L0 to L1 using L2
L0:
aload 0
ldc "version"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
istore 1
L1:
iload 1
ireturn
L2:
astore 0
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static d(Lorg/json/JSONObject;)Ljava/lang/String;
.catch org/json/JSONException from L0 to L1 using L2
L0:
aload 0
ldc "shortversion"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 0
L1:
aload 0
areturn
L2:
astore 0
ldc ""
areturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Lorg/json/JSONObject;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 0
ldc "notes"
ldc ""
invokestatic net/hockeyapp/android/e/y/a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
aload 1
ldc "<div style='padding: 0px 10px;'>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokevirtual java/lang/String/length()I
ifne L0
aload 1
ldc "<em>No information.</em>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L1:
aload 1
ldc "</div>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 1
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L1
.limit locals 2
.limit stack 3
.end method

.method private e()V
aload 0
getfield net/hockeyapp/android/e/y/b Ljava/util/ArrayList;
new net/hockeyapp/android/e/z
dup
aload 0
invokespecial net/hockeyapp/android/e/z/<init>(Lnet/hockeyapp/android/e/y;)V
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
return
.limit locals 1
.limit stack 4
.end method

.method private static f()Ljava/lang/Object;
ldc "<hr style='border-top: 1px solid #c8c8c8; border-bottom: 0px; margin: 40px 10px 0px 10px;' />"
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
ldc "shortversion"
ldc ""
invokestatic net/hockeyapp/android/e/y/a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
ldc "version"
ldc ""
invokestatic net/hockeyapp/android/e/y/a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final b()Ljava/lang/String;
new java/util/Date
dup
aload 0
getfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
ldc "timestamp"
invokestatic net/hockeyapp/android/e/y/a(Lorg/json/JSONObject;Ljava/lang/String;)J
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
astore 1
new java/text/SimpleDateFormat
dup
ldc "dd.MM.yyyy"
invokespecial java/text/SimpleDateFormat/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/text/SimpleDateFormat/format(Ljava/util/Date;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 6
.end method

.method public final c()J
aload 0
getfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
ldc "external"
ldc "false"
invokestatic net/hockeyapp/android/e/y/a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Boolean/valueOf(Ljava/lang/String;)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
istore 1
aload 0
getfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
ldc "appsize"
invokestatic net/hockeyapp/android/e/y/a(Lorg/json/JSONObject;Ljava/lang/String;)J
lstore 4
lload 4
lstore 2
iload 1
ifeq L0
lload 4
lstore 2
lload 4
lconst_0
lcmp
ifne L0
ldc2_w -1L
lstore 2
L0:
lload 2
lreturn
.limit locals 6
.limit stack 4
.end method

.method public final d()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
aload 4
ldc "<html>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
ldc "<body style='padding: 0px 0px 20px 0px'>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield net/hockeyapp/android/e/y/b Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 5
iconst_0
istore 1
L0:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/json/JSONObject
astore 6
iload 1
ifle L2
aload 4
ldc "<hr style='border-top: 1px solid #c8c8c8; border-bottom: 0px; margin: 40px 10px 0px 10px;' />"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 7
aload 0
getfield net/hockeyapp/android/e/y/c Lorg/json/JSONObject;
invokestatic net/hockeyapp/android/e/y/c(Lorg/json/JSONObject;)I
istore 2
aload 6
invokestatic net/hockeyapp/android/e/y/c(Lorg/json/JSONObject;)I
istore 3
aload 6
invokestatic net/hockeyapp/android/e/y/d(Lorg/json/JSONObject;)Ljava/lang/String;
astore 8
aload 7
ldc "<div style='padding: 20px 10px 10px;'><strong>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 1
ifne L3
aload 7
ldc "Newest version:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L4:
aload 7
ldc "</strong></div>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
aload 7
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 7
aload 6
ldc "notes"
ldc ""
invokestatic net/hockeyapp/android/e/y/a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 6
aload 7
ldc "<div style='padding: 0px 10px;'>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 6
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokevirtual java/lang/String/length()I
ifne L5
aload 7
ldc "<em>No information.</em>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L6:
aload 7
ldc "</div>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
aload 7
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L3:
aload 7
new java/lang/StringBuilder
dup
ldc "Version "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 3
iload 2
if_icmpeq L4
iload 3
aload 0
getfield net/hockeyapp/android/e/y/e I
if_icmpne L4
aload 0
iconst_m1
putfield net/hockeyapp/android/e/y/e I
aload 7
ldc "[INSTALLED]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L4
L5:
aload 7
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L6
L1:
aload 4
ldc "</body>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
ldc "</html>"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 9
.limit stack 4
.end method
