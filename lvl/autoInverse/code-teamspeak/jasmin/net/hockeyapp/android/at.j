.bytecode 50.0
.class public synchronized net/hockeyapp/android/at
.super android/app/DialogFragment
.implements android/view/View$OnClickListener
.implements net/hockeyapp/android/ax

.field private 'downloadTask' Lnet/hockeyapp/android/d/l;

.field private 'urlString' Ljava/lang/String;

.field private 'versionHelper' Lnet/hockeyapp/android/e/y;

.field private 'versionInfo' Lorg/json/JSONArray;

.method public <init>()V
aload 0
invokespecial android/app/DialogFragment/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$000(Lnet/hockeyapp/android/at;Landroid/app/Activity;)V
aload 0
aload 1
invokespecial net/hockeyapp/android/at/startDownloadTask(Landroid/app/Activity;)V
return
.limit locals 2
.limit stack 2
.end method

.method public static newInstance(Lorg/json/JSONArray;Ljava/lang/String;)Lnet/hockeyapp/android/at;
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 2
aload 2
ldc "url"
aload 1
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 2
ldc "versionInfo"
aload 0
invokevirtual org/json/JSONArray/toString()Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
new net/hockeyapp/android/at
dup
invokespecial net/hockeyapp/android/at/<init>()V
astore 0
aload 0
aload 2
invokevirtual net/hockeyapp/android/at/setArguments(Landroid/os/Bundle;)V
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private startDownloadTask(Landroid/app/Activity;)V
aload 0
new net/hockeyapp/android/d/l
dup
aload 1
aload 0
getfield net/hockeyapp/android/at/urlString Ljava/lang/String;
new net/hockeyapp/android/aw
dup
aload 0
aload 1
invokespecial net/hockeyapp/android/aw/<init>(Lnet/hockeyapp/android/at;Landroid/app/Activity;)V
invokespecial net/hockeyapp/android/d/l/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
putfield net/hockeyapp/android/at/downloadTask Lnet/hockeyapp/android/d/l;
aload 0
getfield net/hockeyapp/android/at/downloadTask Lnet/hockeyapp/android/d/l;
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
.limit locals 2
.limit stack 9
.end method

.method public getAppName()Ljava/lang/String;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
astore 1
L0:
aload 1
invokevirtual android/app/Activity/getPackageManager()Landroid/content/pm/PackageManager;
astore 2
aload 2
aload 2
aload 1
invokevirtual android/app/Activity/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
invokevirtual android/content/pm/PackageManager/getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc ""
areturn
.limit locals 3
.limit stack 4
.end method

.method public getCurrentVersionCode()I
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
L0:
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
invokevirtual android/app/Activity/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
invokevirtual android/app/Activity/getPackageName()Ljava/lang/String;
sipush 128
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionCode I
istore 1
L1:
iload 1
ireturn
L3:
astore 2
iconst_m1
ireturn
L2:
astore 2
iconst_m1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public getLayoutView()Landroid/view/View;
new net/hockeyapp/android/f/m
dup
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
iconst_0
iconst_1
invokespecial net/hockeyapp/android/f/m/<init>(Landroid/content/Context;ZZ)V
areturn
.limit locals 1
.limit stack 5
.end method

.method public onClick(Landroid/view/View;)V
aload 0
invokevirtual net/hockeyapp/android/at/prepareDownload()V
return
.limit locals 2
.limit stack 1
.end method

.method public onCreate(Landroid/os/Bundle;)V
.catch org/json/JSONException from L0 to L1 using L2
aload 0
aload 1
invokespecial android/app/DialogFragment/onCreate(Landroid/os/Bundle;)V
L0:
aload 0
aload 0
invokevirtual net/hockeyapp/android/at/getArguments()Landroid/os/Bundle;
ldc "url"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield net/hockeyapp/android/at/urlString Ljava/lang/String;
aload 0
new org/json/JSONArray
dup
aload 0
invokevirtual net/hockeyapp/android/at/getArguments()Landroid/os/Bundle;
ldc "versionInfo"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokespecial org/json/JSONArray/<init>(Ljava/lang/String;)V
putfield net/hockeyapp/android/at/versionInfo Lorg/json/JSONArray;
L1:
aload 0
iconst_1
ldc_w 16973939
invokevirtual net/hockeyapp/android/at/setStyle(II)V
return
L2:
astore 1
aload 0
invokevirtual net/hockeyapp/android/at/dismiss()V
return
.limit locals 2
.limit stack 5
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
aload 0
invokevirtual net/hockeyapp/android/at/getLayoutView()Landroid/view/View;
astore 2
aload 0
new net/hockeyapp/android/e/y
dup
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
aload 0
getfield net/hockeyapp/android/at/versionInfo Lorg/json/JSONArray;
invokevirtual org/json/JSONArray/toString()Ljava/lang/String;
aload 0
invokespecial net/hockeyapp/android/e/y/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/ax;)V
putfield net/hockeyapp/android/at/versionHelper Lnet/hockeyapp/android/e/y;
aload 2
sipush 4098
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 0
invokevirtual net/hockeyapp/android/at/getAppName()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 2
sipush 4099
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 3
new java/lang/StringBuilder
dup
ldc "Version "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/at/versionHelper Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/a()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
aload 0
getfield net/hockeyapp/android/at/versionHelper Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/b()Ljava/lang/String;
astore 7
ldc "Unknown size"
astore 1
aload 0
getfield net/hockeyapp/android/at/versionHelper Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/c()J
lstore 4
lload 4
lconst_0
lcmp
iflt L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "%.2f"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 4
l2f
ldc_w 1048576.0F
fdiv
invokestatic java/lang/Float/valueOf(F)Ljava/lang/Float;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " MB"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L1:
aload 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 2
sipush 4100
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
sipush 4101
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/webkit/WebView
astore 1
aload 1
iconst_1
invokevirtual android/webkit/WebView/clearCache(Z)V
aload 1
invokevirtual android/webkit/WebView/destroyDrawingCache()V
aload 1
ldc "https://sdk.hockeyapp.net/"
aload 0
getfield net/hockeyapp/android/at/versionHelper Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/d()Ljava/lang/String;
ldc "text/html"
ldc "utf-8"
aconst_null
invokevirtual android/webkit/WebView/loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 2
areturn
L0:
new net/hockeyapp/android/d/o
dup
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
aload 0
getfield net/hockeyapp/android/at/urlString Ljava/lang/String;
new net/hockeyapp/android/au
dup
aload 0
aload 3
aload 6
aload 7
invokespecial net/hockeyapp/android/au/<init>(Lnet/hockeyapp/android/at;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
invokespecial net/hockeyapp/android/d/o/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
goto L1
.limit locals 8
.limit stack 10
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
aload 2
arraylength
ifeq L0
aload 3
arraylength
ifne L1
L0:
return
L1:
iload 1
iconst_1
if_icmpne L0
aload 3
iconst_0
iaload
ifne L2
aload 0
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
invokespecial net/hockeyapp/android/at/startDownloadTask(Landroid/app/Activity;)V
return
L2:
ldc "HockeyApp"
ldc "User denied write permission, can't continue with updater task."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
invokestatic net/hockeyapp/android/ay/a()Lnet/hockeyapp/android/az;
ifnonnull L0
new android/app/AlertDialog$Builder
dup
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
sipush 1792
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
sipush 1793
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
sipush 1794
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
aconst_null
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
sipush 1795
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
new net/hockeyapp/android/av
dup
aload 0
aload 0
invokespecial net/hockeyapp/android/av/<init>(Lnet/hockeyapp/android/at;Lnet/hockeyapp/android/at;)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 4
.limit stack 6
.end method

.method public prepareDownload()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L0
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
ldc "android.permission.WRITE_EXTERNAL_STORAGE"
invokevirtual android/app/Activity/checkSelfPermission(Ljava/lang/String;)I
ifeq L0
aload 0
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "android.permission.WRITE_EXTERNAL_STORAGE"
aastore
iconst_1
invokevirtual net/hockeyapp/android/at/requestPermissions([Ljava/lang/String;I)V
return
L0:
aload 0
aload 0
invokevirtual net/hockeyapp/android/at/getActivity()Landroid/app/Activity;
invokespecial net/hockeyapp/android/at/startDownloadTask(Landroid/app/Activity;)V
aload 0
invokevirtual net/hockeyapp/android/at/dismiss()V
return
.limit locals 1
.limit stack 5
.end method
