.bytecode 50.0
.class public final synchronized android/support/design/internal/e
.super android/support/v7/internal/view/menu/ad

.method public <init>(Landroid/content/Context;Landroid/support/design/internal/a;Landroid/support/v7/internal/view/menu/m;)V
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/v7/internal/view/menu/ad/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/internal/view/menu/m;)V
return
.limit locals 4
.limit stack 4
.end method

.method private l()V
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
checkcast android/support/v7/internal/view/menu/i
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public final add(I)Landroid/view/MenuItem;
aload 0
iload 1
invokespecial android/support/v7/internal/view/menu/ad/add(I)Landroid/view/MenuItem;
astore 2
aload 0
invokespecial android/support/design/internal/e/l()V
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method public final add(IIII)Landroid/view/MenuItem;
aload 0
iload 1
iload 2
iload 3
iload 4
invokespecial android/support/v7/internal/view/menu/ad/add(IIII)Landroid/view/MenuItem;
astore 5
aload 0
invokespecial android/support/design/internal/e/l()V
aload 5
areturn
.limit locals 6
.limit stack 5
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
iload 1
iload 2
iload 3
aload 4
invokespecial android/support/v7/internal/view/menu/ad/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
astore 4
aload 0
invokespecial android/support/design/internal/e/l()V
aload 4
areturn
.limit locals 5
.limit stack 5
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/ad/add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
astore 1
aload 0
invokespecial android/support/design/internal/e/l()V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method
