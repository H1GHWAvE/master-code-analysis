.bytecode 50.0
.class public synchronized android/support/design/internal/f
.super android/widget/FrameLayout

.field private 'a' Landroid/graphics/drawable/Drawable;

.field private 'b' Landroid/graphics/Rect;

.field private 'c' Landroid/graphics/Rect;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/internal/f/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/internal/f/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;C)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/internal/f/c Landroid/graphics/Rect;
aload 1
aconst_null
getstatic android/support/design/n/ScrimInsetsFrameLayout [I
iconst_0
getstatic android/support/design/m/Widget_Design_ScrimInsetsFrameLayout I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/design/n/ScrimInsetsFrameLayout_insetForeground I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
iconst_1
invokevirtual android/support/design/internal/f/setWillNotDraw(Z)V
aload 0
new android/support/design/internal/g
dup
aload 0
invokespecial android/support/design/internal/g/<init>(Landroid/support/design/internal/f;)V
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/bx;)V
return
.limit locals 3
.limit stack 5
.end method

.method static synthetic a(Landroid/support/design/internal/f;)Landroid/graphics/Rect;
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/design/internal/f;Landroid/graphics/Rect;)Landroid/graphics/Rect;
aload 0
aload 1
putfield android/support/design/internal/f/b Landroid/graphics/Rect;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/internal/f;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public draw(Landroid/graphics/Canvas;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
aload 1
invokespecial android/widget/FrameLayout/draw(Landroid/graphics/Canvas;)V
aload 0
invokevirtual android/support/design/internal/f/getWidth()I
istore 2
aload 0
invokevirtual android/support/design/internal/f/getHeight()I
istore 3
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
ifnull L0
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
ifnull L0
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 4
aload 1
aload 0
invokevirtual android/support/design/internal/f/getScrollX()I
i2f
aload 0
invokevirtual android/support/design/internal/f/getScrollY()I
i2f
invokevirtual android/graphics/Canvas/translate(FF)V
aload 0
getfield android/support/design/internal/f/c Landroid/graphics/Rect;
iconst_0
iconst_0
iload 2
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/internal/f/c Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/internal/f/c Landroid/graphics/Rect;
iconst_0
iload 3
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
isub
iload 2
iload 3
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/internal/f/c Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/internal/f/c Landroid/graphics/Rect;
iconst_0
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
iload 3
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
isub
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/internal/f/c Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/internal/f/c Landroid/graphics/Rect;
iload 2
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
isub
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iload 2
iload 3
aload 0
getfield android/support/design/internal/f/b Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
isub
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/internal/f/c Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
aload 1
iload 4
invokevirtual android/graphics/Canvas/restoreToCount(I)V
L0:
return
.limit locals 5
.limit stack 6
.end method

.method protected onAttachedToWindow()V
aload 0
invokespecial android/widget/FrameLayout/onAttachedToWindow()V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method protected onDetachedFromWindow()V
aload 0
invokespecial android/widget/FrameLayout/onDetachedFromWindow()V
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/design/internal/f/a Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L0:
return
.limit locals 1
.limit stack 2
.end method
