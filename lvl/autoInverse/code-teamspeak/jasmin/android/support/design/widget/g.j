.bytecode 50.0
.class public final synchronized android/support/design/widget/g
.super android/widget/LinearLayout$LayoutParams

.field public static final 'a' I = 1


.field public static final 'b' I = 2


.field public static final 'c' I = 4


.field public static final 'd' I = 8


.field static final 'e' I = 5


.field 'f' I

.field 'g' Landroid/view/animation/Interpolator;

.method public <init>()V
aload 0
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
aload 0
iconst_1
putfield android/support/design/widget/g/f I
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(IIF)V
aload 0
iload 1
iload 2
fload 3
invokespecial android/widget/LinearLayout$LayoutParams/<init>(IIF)V
aload 0
iconst_1
putfield android/support/design/widget/g/f I
return
.limit locals 4
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_1
putfield android/support/design/widget/g/f I
aload 1
aload 2
getstatic android/support/design/n/AppBarLayout_LayoutParams [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 2
aload 0
aload 2
getstatic android/support/design/n/AppBarLayout_LayoutParams_layout_scrollFlags I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/design/widget/g/f I
aload 2
getstatic android/support/design/n/AppBarLayout_LayoutParams_layout_scrollInterpolator I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
aload 1
aload 2
getstatic android/support/design/n/AppBarLayout_LayoutParams_layout_scrollInterpolator I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
invokestatic android/view/animation/AnimationUtils/loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;
putfield android/support/design/widget/g/g Landroid/view/animation/Interpolator;
L0:
aload 2
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 3
.limit stack 5
.end method

.method private <init>(Landroid/support/design/widget/g;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(Landroid/widget/LinearLayout$LayoutParams;)V
aload 0
iconst_1
putfield android/support/design/widget/g/f I
aload 0
aload 1
getfield android/support/design/widget/g/f I
putfield android/support/design/widget/g/f I
aload 0
aload 1
getfield android/support/design/widget/g/g Landroid/view/animation/Interpolator;
putfield android/support/design/widget/g/g Landroid/view/animation/Interpolator;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_1
putfield android/support/design/widget/g/f I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
iconst_1
putfield android/support/design/widget/g/f I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/widget/LinearLayout$LayoutParams;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(Landroid/widget/LinearLayout$LayoutParams;)V
aload 0
iconst_1
putfield android/support/design/widget/g/f I
return
.limit locals 2
.limit stack 2
.end method

.method private a()I
aload 0
getfield android/support/design/widget/g/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/design/widget/g/f I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/animation/Interpolator;)V
aload 0
aload 1
putfield android/support/design/widget/g/g Landroid/view/animation/Interpolator;
return
.limit locals 2
.limit stack 2
.end method

.method private b()Landroid/view/animation/Interpolator;
aload 0
getfield android/support/design/widget/g/g Landroid/view/animation/Interpolator;
areturn
.limit locals 1
.limit stack 1
.end method
