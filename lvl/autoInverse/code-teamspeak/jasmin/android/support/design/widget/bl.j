.bytecode 50.0
.class final synchronized android/support/design/widget/bl
.super java/lang/Object

.field final 'a' Ljava/util/ArrayList;

.field 'b' Landroid/support/design/widget/bn;

.field 'c' Landroid/view/animation/Animation;

.field 'd' Ljava/lang/ref/WeakReference;

.field private 'e' Landroid/view/animation/Animation$AnimationListener;

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/design/widget/bl/a Ljava/util/ArrayList;
aload 0
aconst_null
putfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
aload 0
aconst_null
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
aload 0
new android/support/design/widget/bm
dup
aload 0
invokespecial android/support/design/widget/bm/<init>(Landroid/support/design/widget/bl;)V
putfield android/support/design/widget/bl/e Landroid/view/animation/Animation$AnimationListener;
return
.limit locals 1
.limit stack 4
.end method

.method private static synthetic a(Landroid/support/design/widget/bl;)Landroid/view/animation/Animation;
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/support/design/widget/bn;)V
aload 0
aload 1
getfield android/support/design/widget/bn/b Landroid/view/animation/Animation;
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
aload 0
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 1
aload 1
ifnull L0
aload 1
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;)V
aload 0
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 4
aload 4
aload 1
if_acmpne L0
L1:
return
L0:
aload 4
ifnull L2
aload 0
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 4
aload 0
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 3
iconst_0
istore 2
L3:
iload 2
iload 3
if_icmpge L4
aload 0
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bn
getfield android/support/design/widget/bn/b Landroid/view/animation/Animation;
astore 5
aload 4
invokevirtual android/view/View/getAnimation()Landroid/view/animation/Animation;
aload 5
if_acmpne L5
aload 4
invokevirtual android/view/View/clearAnimation()V
L5:
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
aload 0
aconst_null
putfield android/support/design/widget/bl/d Ljava/lang/ref/WeakReference;
aload 0
aconst_null
putfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
aload 0
aconst_null
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
L2:
aload 1
ifnull L1
aload 0
new java/lang/ref/WeakReference
dup
aload 1
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
putfield android/support/design/widget/bl/d Ljava/lang/ref/WeakReference;
return
.limit locals 6
.limit stack 4
.end method

.method private a([I)V
aload 0
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bn
astore 4
aload 4
getfield android/support/design/widget/bn/a [I
aload 1
invokestatic android/util/StateSet/stateSetMatches([I[I)Z
ifeq L2
aload 4
astore 1
L3:
aload 1
aload 0
getfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
if_acmpne L4
L5:
return
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L4:
aload 0
getfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
ifnull L6
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
ifnull L6
aload 0
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 4
aload 4
ifnull L7
aload 4
invokevirtual android/view/View/getAnimation()Landroid/view/animation/Animation;
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
if_acmpne L7
aload 4
invokevirtual android/view/View/clearAnimation()V
L7:
aload 0
aconst_null
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
L6:
aload 0
aload 1
putfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
aload 1
ifnull L5
aload 0
aload 1
getfield android/support/design/widget/bn/b Landroid/view/animation/Animation;
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
aload 0
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 1
aload 1
ifnull L5
aload 1
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
return
L1:
aconst_null
astore 1
goto L3
.limit locals 5
.limit stack 2
.end method

.method private b()Landroid/view/animation/Animation;
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic b(Landroid/support/design/widget/bl;)Landroid/view/animation/Animation;
aload 0
aconst_null
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method private c()V
aload 0
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 3
aload 0
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bn
getfield android/support/design/widget/bn/b Landroid/view/animation/Animation;
astore 4
aload 3
invokevirtual android/view/View/getAnimation()Landroid/view/animation/Animation;
aload 4
if_acmpne L2
aload 3
invokevirtual android/view/View/clearAnimation()V
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
aconst_null
putfield android/support/design/widget/bl/d Ljava/lang/ref/WeakReference;
aload 0
aconst_null
putfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
aload 0
aconst_null
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
return
.limit locals 5
.limit stack 2
.end method

.method private d()V
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
ifnull L0
aload 0
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 1
aload 1
ifnull L1
aload 1
invokevirtual android/view/View/getAnimation()Landroid/view/animation/Animation;
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
if_acmpne L1
aload 1
invokevirtual android/view/View/clearAnimation()V
L1:
aload 0
aconst_null
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private e()Ljava/util/ArrayList;
aload 0
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()V
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
ifnull L0
aload 0
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 1
aload 1
ifnull L0
aload 1
invokevirtual android/view/View/getAnimation()Landroid/view/animation/Animation;
aload 0
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
if_acmpne L0
aload 1
invokevirtual android/view/View/clearAnimation()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final a()Landroid/view/View;
aload 0
getfield android/support/design/widget/bl/d Ljava/lang/ref/WeakReference;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield android/support/design/widget/bl/d Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a([ILandroid/view/animation/Animation;)V
new android/support/design/widget/bn
dup
aload 1
aload 2
iconst_0
invokespecial android/support/design/widget/bn/<init>([ILandroid/view/animation/Animation;B)V
astore 1
aload 2
aload 0
getfield android/support/design/widget/bl/e Landroid/view/animation/Animation$AnimationListener;
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 3
.limit stack 5
.end method
