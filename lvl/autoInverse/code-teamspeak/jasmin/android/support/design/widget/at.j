.bytecode 50.0
.class final synchronized android/support/design/widget/at
.super java/lang/Object
.implements android/os/Handler$Callback

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final handleMessage(Landroid/os/Message;)Z
iconst_0
istore 4
aload 1
getfield android/os/Message/what I
tableswitch 0
L0
L1
default : L2
L2:
iconst_0
ireturn
L0:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast android/support/design/widget/Snackbar
astore 1
aload 1
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getParent()Landroid/view/ViewParent;
ifnonnull L3
aload 1
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 6
aload 6
instanceof android/support/design/widget/w
ifeq L4
new android/support/design/widget/bc
dup
aload 1
invokespecial android/support/design/widget/bc/<init>(Landroid/support/design/widget/Snackbar;)V
astore 7
aload 7
ldc_w 0.1F
invokestatic android/support/design/widget/SwipeDismissBehavior/a(F)F
putfield android/support/design/widget/SwipeDismissBehavior/k F
aload 7
ldc_w 0.6F
invokestatic android/support/design/widget/SwipeDismissBehavior/a(F)F
putfield android/support/design/widget/SwipeDismissBehavior/l F
aload 7
iconst_0
putfield android/support/design/widget/SwipeDismissBehavior/j I
aload 7
new android/support/design/widget/aw
dup
aload 1
invokespecial android/support/design/widget/aw/<init>(Landroid/support/design/widget/Snackbar;)V
putfield android/support/design/widget/SwipeDismissBehavior/i Landroid/support/design/widget/bp;
aload 6
checkcast android/support/design/widget/w
aload 7
invokevirtual android/support/design/widget/w/a(Landroid/support/design/widget/t;)V
L4:
aload 1
getfield android/support/design/widget/Snackbar/d Landroid/view/ViewGroup;
aload 1
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;)V
L3:
aload 1
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L5
aload 1
invokevirtual android/support/design/widget/Snackbar/a()V
L6:
iconst_1
ireturn
L5:
aload 1
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
new android/support/design/widget/ax
dup
aload 1
invokespecial android/support/design/widget/ax/<init>(Landroid/support/design/widget/Snackbar;)V
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/setOnLayoutChangeListener(Landroid/support/design/widget/bg;)V
goto L6
L1:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast android/support/design/widget/Snackbar
astore 6
aload 1
getfield android/os/Message/arg1 I
istore 5
aload 6
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getVisibility()I
ifne L7
aload 6
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 1
iload 4
istore 3
aload 1
instanceof android/support/design/widget/w
ifeq L8
aload 1
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 1
iload 4
istore 3
aload 1
instanceof android/support/design/widget/SwipeDismissBehavior
ifeq L8
aload 1
checkcast android/support/design/widget/SwipeDismissBehavior
astore 1
aload 1
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
ifnull L9
aload 1
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
istore 2
L10:
iload 4
istore 3
iload 2
ifeq L8
iconst_1
istore 3
L8:
iload 3
ifeq L11
L7:
aload 6
invokevirtual android/support/design/widget/Snackbar/b()V
L12:
iconst_1
ireturn
L9:
iconst_0
istore 2
goto L10
L11:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L13
aload 6
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
aload 6
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getHeight()I
i2f
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
ldc2_w 250L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
new android/support/design/widget/ba
dup
aload 6
iload 5
invokespecial android/support/design/widget/ba/<init>(Landroid/support/design/widget/Snackbar;I)V
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
goto L12
L13:
aload 6
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getContext()Landroid/content/Context;
getstatic android/support/design/c/design_snackbar_out I
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 1
aload 1
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 1
ldc2_w 250L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 1
new android/support/design/widget/bb
dup
aload 6
iload 5
invokespecial android/support/design/widget/bb/<init>(Landroid/support/design/widget/Snackbar;I)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 6
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
aload 1
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/startAnimation(Landroid/view/animation/Animation;)V
goto L12
.limit locals 8
.limit stack 5
.end method
