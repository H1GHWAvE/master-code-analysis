.bytecode 50.0
.class public synchronized android/support/design/widget/SwipeDismissBehavior
.super android/support/design/widget/t

.field private static final 'a' F = 0.5F


.field public static final 'b' I = 0


.field public static final 'c' I = 1


.field public static final 'd' I = 2


.field public static final 'e' I = 0


.field public static final 'f' I = 1


.field public static final 'g' I = 2


.field private static final 'm' F = 0.0F


.field private static final 'n' F = 0.5F


.field 'h' Landroid/support/v4/widget/eg;

.field 'i' Landroid/support/design/widget/bp;

.field 'j' I

.field 'k' F

.field 'l' F

.field private 'o' Z

.field private 'p' F

.field private 'q' Z

.field private 'r' F

.field private final 's' Landroid/support/v4/widget/ej;

.method public <init>()V
aload 0
invokespecial android/support/design/widget/t/<init>()V
aload 0
fconst_0
putfield android/support/design/widget/SwipeDismissBehavior/p F
aload 0
iconst_2
putfield android/support/design/widget/SwipeDismissBehavior/j I
aload 0
ldc_w 0.5F
putfield android/support/design/widget/SwipeDismissBehavior/r F
aload 0
fconst_0
putfield android/support/design/widget/SwipeDismissBehavior/k F
aload 0
ldc_w 0.5F
putfield android/support/design/widget/SwipeDismissBehavior/l F
aload 0
new android/support/design/widget/bo
dup
aload 0
invokespecial android/support/design/widget/bo/<init>(Landroid/support/design/widget/SwipeDismissBehavior;)V
putfield android/support/design/widget/SwipeDismissBehavior/s Landroid/support/v4/widget/ej;
return
.limit locals 1
.limit stack 4
.end method

.method static a(F)F
fconst_0
fload 0
invokestatic java/lang/Math/max(FF)F
fconst_1
invokestatic java/lang/Math/min(FF)F
freturn
.limit locals 1
.limit stack 2
.end method

.method static a(FFF)F
fload 2
fload 0
fsub
fload 1
fload 0
fsub
fdiv
freturn
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(III)I
iload 0
iload 1
invokestatic java/lang/Math/max(II)I
iload 2
invokestatic java/lang/Math/min(II)I
ireturn
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/i Landroid/support/design/widget/bp;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
iconst_0
putfield android/support/design/widget/SwipeDismissBehavior/j I
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/support/design/widget/bp;)V
aload 0
aload 1
putfield android/support/design/widget/SwipeDismissBehavior/i Landroid/support/design/widget/bp;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/ViewGroup;)V
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
ifnonnull L0
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/q Z
ifeq L1
aload 1
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/p F
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/s Landroid/support/v4/widget/ej;
invokestatic android/support/v4/widget/eg/a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
astore 1
L2:
aload 0
aload 1
putfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
L0:
return
L1:
aload 1
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/s Landroid/support/v4/widget/ej;
invokestatic android/support/v4/widget/eg/a(Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
astore 1
goto L2
.limit locals 2
.limit stack 3
.end method

.method static synthetic b(F)F
fload 0
invokestatic android/support/design/widget/SwipeDismissBehavior/a(F)F
freturn
.limit locals 1
.limit stack 1
.end method

.method private static b(III)I
iload 0
iload 1
invokestatic java/lang/Math/max(II)I
iload 2
invokestatic java/lang/Math/min(II)I
ireturn
.limit locals 3
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/v4/widget/eg;
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()V
aload 0
ldc_w 0.1F
invokestatic android/support/design/widget/SwipeDismissBehavior/a(F)F
putfield android/support/design/widget/SwipeDismissBehavior/k F
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic c(Landroid/support/design/widget/SwipeDismissBehavior;)I
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/j I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()V
aload 0
ldc_w 0.6F
invokestatic android/support/design/widget/SwipeDismissBehavior/a(F)F
putfield android/support/design/widget/SwipeDismissBehavior/l F
return
.limit locals 1
.limit stack 2
.end method

.method private c(F)V
aload 0
fload 1
invokestatic android/support/design/widget/SwipeDismissBehavior/a(F)F
putfield android/support/design/widget/SwipeDismissBehavior/r F
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic d(Landroid/support/design/widget/SwipeDismissBehavior;)F
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/r F
freturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
ifnull L0
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(F)V
aload 0
fload 1
putfield android/support/design/widget/SwipeDismissBehavior/p F
aload 0
iconst_1
putfield android/support/design/widget/SwipeDismissBehavior/q Z
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic e(Landroid/support/design/widget/SwipeDismissBehavior;)F
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/k F
freturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Landroid/support/design/widget/SwipeDismissBehavior;)F
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/l F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
ifnull L0
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
aload 3
invokevirtual android/support/v4/widget/eg/b(Landroid/view/MotionEvent;)V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
aload 3
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 1
L0
L1
L0
default : L1
L1:
aload 1
aload 2
aload 3
invokevirtual android/view/MotionEvent/getX()F
f2i
aload 3
invokevirtual android/view/MotionEvent/getY()F
f2i
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;II)Z
ifne L2
iconst_1
istore 4
L3:
aload 0
iload 4
putfield android/support/design/widget/SwipeDismissBehavior/o Z
L4:
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/o Z
ifeq L5
iconst_0
ireturn
L0:
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/o Z
ifeq L4
aload 0
iconst_0
putfield android/support/design/widget/SwipeDismissBehavior/o Z
iconst_0
ireturn
L2:
iconst_0
istore 4
goto L3
L5:
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
ifnonnull L6
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/q Z
ifeq L7
aload 1
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/p F
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/s Landroid/support/v4/widget/ej;
invokestatic android/support/v4/widget/eg/a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
astore 1
L8:
aload 0
aload 1
putfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
L6:
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
aload 3
invokevirtual android/support/v4/widget/eg/a(Landroid/view/MotionEvent;)Z
ireturn
L7:
aload 1
aload 0
getfield android/support/design/widget/SwipeDismissBehavior/s Landroid/support/v4/widget/ej;
invokestatic android/support/v4/widget/eg/a(Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
astore 1
goto L8
.limit locals 5
.limit stack 4
.end method
