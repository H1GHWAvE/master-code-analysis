.bytecode 50.0
.class public final synchronized android/support/design/widget/bz
.super java/lang/Object

.field public static final 'a' I = -1


.field 'b' Landroid/graphics/drawable/Drawable;

.field 'c' Ljava/lang/CharSequence;

.field 'd' Ljava/lang/CharSequence;

.field 'e' I

.field 'f' Landroid/view/View;

.field final 'g' Landroid/support/design/widget/br;

.field private 'h' Ljava/lang/Object;

.method <init>(Landroid/support/design/widget/br;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield android/support/design/widget/bz/e I
aload 0
aload 1
putfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic a(Landroid/support/design/widget/bz;)Landroid/support/design/widget/br;
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/v;
.end annotation
aload 0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
invokevirtual android/support/design/widget/br/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
iload 1
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
putfield android/support/design/widget/bz/f Landroid/view/View;
aload 0
getfield android/support/design/widget/bz/e I
iflt L0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
getfield android/support/design/widget/bz/e I
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Landroid/graphics/drawable/Drawable;)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/design/widget/bz/b Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/bz/e I
iflt L0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
getfield android/support/design/widget/bz/e I
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/design/widget/bz/f Landroid/view/View;
aload 0
getfield android/support/design/widget/bz/e I
iflt L0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
getfield android/support/design/widget/bz/e I
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/design/widget/bz/h Ljava/lang/Object;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/design/widget/bz/d Ljava/lang/CharSequence;
aload 0
getfield android/support/design/widget/bz/e I
iflt L0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
getfield android/support/design/widget/bz/e I
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b()Ljava/lang/Object;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/bz/h Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)V
aload 0
iload 1
putfield android/support/design/widget/bz/e I
return
.limit locals 2
.limit stack 2
.end method

.method private c(I)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
invokevirtual android/support/design/widget/br/getContext()Landroid/content/Context;
iload 1
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
putfield android/support/design/widget/bz/b Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/bz/e I
iflt L0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
getfield android/support/design/widget/bz/e I
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private c()Landroid/view/View;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/bz/f Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Landroid/graphics/drawable/Drawable;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/bz/b Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(I)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
invokevirtual android/support/design/widget/br/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getText(I)Ljava/lang/CharSequence;
invokevirtual android/support/design/widget/bz/a(Ljava/lang/CharSequence;)Landroid/support/design/widget/bz;
areturn
.limit locals 2
.limit stack 3
.end method

.method private e()I
aload 0
getfield android/support/design/widget/bz/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e(I)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
invokevirtual android/support/design/widget/br/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getText(I)Ljava/lang/CharSequence;
putfield android/support/design/widget/bz/d Ljava/lang/CharSequence;
aload 0
getfield android/support/design/widget/bz/e I
iflt L0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
getfield android/support/design/widget/bz/e I
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private f()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/bz/c Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
invokevirtual android/support/design/widget/br/getSelectedTabPosition()I
aload 0
getfield android/support/design/widget/bz/e I
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private h()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/bz/d Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/design/widget/bz;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/design/widget/bz/c Ljava/lang/CharSequence;
aload 0
getfield android/support/design/widget/bz/e I
iflt L0
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
getfield android/support/design/widget/bz/e I
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a()V
aload 0
getfield android/support/design/widget/bz/g Landroid/support/design/widget/br;
aload 0
iconst_1
invokevirtual android/support/design/widget/br/a(Landroid/support/design/widget/bz;Z)V
return
.limit locals 1
.limit stack 3
.end method
