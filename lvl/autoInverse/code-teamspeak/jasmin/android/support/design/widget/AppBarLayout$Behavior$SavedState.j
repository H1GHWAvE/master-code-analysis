.bytecode 50.0
.class public synchronized android/support/design/widget/AppBarLayout$Behavior$SavedState
.super android/view/View$BaseSavedState

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field 'a' I

.field 'b' F

.field 'c' Z

.method static <clinit>()V
new android/support/design/widget/f
dup
invokespecial android/support/design/widget/f/<init>()V
putstatic android/support/design/widget/AppBarLayout$Behavior$SavedState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcel;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcel;)V
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/a I
aload 0
aload 1
invokevirtual android/os/Parcel/readFloat()F
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/b F
aload 1
invokevirtual android/os/Parcel/readByte()B
ifeq L0
iconst_1
istore 2
L1:
aload 0
iload 2
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/c Z
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcelable;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcelable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
aload 0
aload 1
iload 2
invokespecial android/view/View$BaseSavedState/writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/a I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/b F
invokevirtual android/os/Parcel/writeFloat(F)V
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/c Z
ifeq L0
iconst_1
istore 2
L1:
aload 1
iload 2
i2b
invokevirtual android/os/Parcel/writeByte(B)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 3
.end method
