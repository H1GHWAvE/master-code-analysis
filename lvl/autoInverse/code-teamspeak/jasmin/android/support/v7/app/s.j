.bytecode 50.0
.class final synchronized android/support/v7/app/s
.super java/lang/Object
.implements android/support/v7/app/l

.field final 'a' Landroid/support/v7/widget/Toolbar;

.field final 'b' Landroid/graphics/drawable/Drawable;

.field final 'c' Ljava/lang/CharSequence;

.method <init>(Landroid/support/v7/widget/Toolbar;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/app/s/a Landroid/support/v7/widget/Toolbar;
aload 0
aload 1
invokevirtual android/support/v7/widget/Toolbar/getNavigationIcon()Landroid/graphics/drawable/Drawable;
putfield android/support/v7/app/s/b Landroid/graphics/drawable/Drawable;
aload 0
aload 1
invokevirtual android/support/v7/widget/Toolbar/getNavigationContentDescription()Ljava/lang/CharSequence;
putfield android/support/v7/app/s/c Ljava/lang/CharSequence;
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/app/s/b Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)V
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
iload 1
ifne L0
aload 0
getfield android/support/v7/app/s/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/app/s/c Ljava/lang/CharSequence;
invokevirtual android/support/v7/widget/Toolbar/setNavigationContentDescription(Ljava/lang/CharSequence;)V
return
L0:
aload 0
getfield android/support/v7/app/s/a Landroid/support/v7/widget/Toolbar;
iload 1
invokevirtual android/support/v7/widget/Toolbar/setNavigationContentDescription(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/graphics/drawable/Drawable;I)V
.annotation invisibleparam 2 Landroid/support/a/ah;
.end annotation
aload 0
getfield android/support/v7/app/s/a Landroid/support/v7/widget/Toolbar;
aload 1
invokevirtual android/support/v7/widget/Toolbar/setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
aload 0
iload 2
invokevirtual android/support/v7/app/s/a(I)V
return
.limit locals 3
.limit stack 2
.end method

.method public final b()Landroid/content/Context;
aload 0
getfield android/support/v7/app/s/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
