.bytecode 50.0
.class public synchronized abstract android/support/v7/widget/as
.super java/lang/Object
.implements android/view/View$OnTouchListener

.field private final 'a' F

.field private final 'b' I

.field private final 'c' I

.field private final 'd' Landroid/view/View;

.field private 'e' Ljava/lang/Runnable;

.field private 'f' Ljava/lang/Runnable;

.field private 'g' Z

.field private 'h' Z

.field private 'i' I

.field private final 'j' [I

.method public <init>(Landroid/view/View;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_2
newarray int
putfield android/support/v7/widget/as/j [I
aload 0
aload 1
putfield android/support/v7/widget/as/d Landroid/view/View;
aload 0
aload 1
invokevirtual android/view/View/getContext()Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
i2f
putfield android/support/v7/widget/as/a F
aload 0
invokestatic android/view/ViewConfiguration/getTapTimeout()I
putfield android/support/v7/widget/as/b I
aload 0
aload 0
getfield android/support/v7/widget/as/b I
invokestatic android/view/ViewConfiguration/getLongPressTimeout()I
iadd
iconst_2
idiv
putfield android/support/v7/widget/as/c I
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v7/widget/as;)Landroid/view/View;
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/isEnabled()Z
ifne L0
L1:
iconst_0
ireturn
L0:
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 0
L2
L3
L4
L3
default : L5
L5:
iconst_0
ireturn
L2:
aload 0
aload 1
iconst_0
invokevirtual android/view/MotionEvent/getPointerId(I)I
putfield android/support/v7/widget/as/i I
aload 0
iconst_0
putfield android/support/v7/widget/as/h Z
aload 0
getfield android/support/v7/widget/as/e Ljava/lang/Runnable;
ifnonnull L6
aload 0
new android/support/v7/widget/at
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/at/<init>(Landroid/support/v7/widget/as;B)V
putfield android/support/v7/widget/as/e Ljava/lang/Runnable;
L6:
aload 6
aload 0
getfield android/support/v7/widget/as/e Ljava/lang/Runnable;
aload 0
getfield android/support/v7/widget/as/b I
i2l
invokevirtual android/view/View/postDelayed(Ljava/lang/Runnable;J)Z
pop
aload 0
getfield android/support/v7/widget/as/f Ljava/lang/Runnable;
ifnonnull L7
aload 0
new android/support/v7/widget/au
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/au/<init>(Landroid/support/v7/widget/as;B)V
putfield android/support/v7/widget/as/f Ljava/lang/Runnable;
L7:
aload 6
aload 0
getfield android/support/v7/widget/as/f Ljava/lang/Runnable;
aload 0
getfield android/support/v7/widget/as/c I
i2l
invokevirtual android/view/View/postDelayed(Ljava/lang/Runnable;J)Z
pop
iconst_0
ireturn
L4:
aload 1
aload 0
getfield android/support/v7/widget/as/i I
invokevirtual android/view/MotionEvent/findPointerIndex(I)I
istore 5
iload 5
iflt L1
aload 1
iload 5
invokevirtual android/view/MotionEvent/getX(I)F
fstore 2
aload 1
iload 5
invokevirtual android/view/MotionEvent/getY(I)F
fstore 3
aload 0
getfield android/support/v7/widget/as/a F
fstore 4
fload 2
fload 4
fneg
fcmpl
iflt L8
fload 3
fload 4
fneg
fcmpl
iflt L8
fload 2
aload 6
invokevirtual android/view/View/getRight()I
aload 6
invokevirtual android/view/View/getLeft()I
isub
i2f
fload 4
fadd
fcmpg
ifge L8
fload 3
aload 6
invokevirtual android/view/View/getBottom()I
aload 6
invokevirtual android/view/View/getTop()I
isub
i2f
fload 4
fadd
fcmpg
ifge L8
iconst_1
istore 5
L9:
iload 5
ifne L1
aload 0
invokespecial android/support/v7/widget/as/d()V
aload 6
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
iconst_1
ireturn
L8:
iconst_0
istore 5
goto L9
L3:
aload 0
invokespecial android/support/v7/widget/as/d()V
iconst_0
ireturn
.limit locals 7
.limit stack 5
.end method

.method private static a(Landroid/view/View;FFF)Z
fload 1
fload 3
fneg
fcmpl
iflt L0
fload 2
fload 3
fneg
fcmpl
iflt L0
fload 1
aload 0
invokevirtual android/view/View/getRight()I
aload 0
invokevirtual android/view/View/getLeft()I
isub
i2f
fload 3
fadd
fcmpg
ifge L0
fload 2
aload 0
invokevirtual android/view/View/getBottom()I
aload 0
invokevirtual android/view/View/getTop()I
isub
i2f
fload 3
fadd
fcmpg
ifge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v7/widget/as/j [I
astore 3
aload 1
aload 3
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 2
aload 3
iconst_0
iaload
ineg
i2f
aload 3
iconst_1
iaload
ineg
i2f
invokevirtual android/view/MotionEvent/offsetLocation(FF)V
iconst_1
ireturn
.limit locals 4
.limit stack 4
.end method

.method static synthetic b(Landroid/support/v7/widget/as;)V
aload 0
invokespecial android/support/v7/widget/as/d()V
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
astore 3
aload 3
invokevirtual android/view/View/isEnabled()Z
ifeq L0
aload 3
invokevirtual android/view/View/isLongClickable()Z
ifeq L1
L0:
return
L1:
aload 0
invokevirtual android/support/v7/widget/as/b()Z
ifeq L0
aload 3
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 1
lload 1
lload 1
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 4
aload 3
aload 4
invokevirtual android/view/View/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
aload 4
invokevirtual android/view/MotionEvent/recycle()V
aload 0
iconst_1
putfield android/support/v7/widget/as/g Z
aload 0
iconst_1
putfield android/support/v7/widget/as/h Z
return
.limit locals 5
.limit stack 8
.end method

.method private b(Landroid/view/MotionEvent;)Z
iconst_1
istore 3
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
astore 5
aload 0
invokevirtual android/support/v7/widget/as/a()Landroid/support/v7/widget/an;
astore 6
aload 6
ifnull L0
aload 6
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifne L1
L0:
iconst_0
istore 3
L2:
iload 3
ireturn
L1:
aload 6
invokestatic android/support/v7/widget/an/a(Landroid/support/v7/widget/an;)Landroid/support/v7/widget/ar;
astore 6
aload 6
ifnull L3
aload 6
invokevirtual android/support/v7/widget/ar/isShown()Z
ifne L4
L3:
iconst_0
ireturn
L4:
aload 1
invokestatic android/view/MotionEvent/obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
astore 7
aload 0
getfield android/support/v7/widget/as/j [I
astore 8
aload 5
aload 8
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 7
aload 8
iconst_0
iaload
i2f
aload 8
iconst_1
iaload
i2f
invokevirtual android/view/MotionEvent/offsetLocation(FF)V
aload 0
getfield android/support/v7/widget/as/j [I
astore 5
aload 6
aload 5
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 7
aload 5
iconst_0
iaload
ineg
i2f
aload 5
iconst_1
iaload
ineg
i2f
invokevirtual android/view/MotionEvent/offsetLocation(FF)V
aload 6
aload 7
aload 0
getfield android/support/v7/widget/as/i I
invokevirtual android/support/v7/widget/ar/a(Landroid/view/MotionEvent;I)Z
istore 4
aload 7
invokevirtual android/view/MotionEvent/recycle()V
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 2
iload 2
iconst_1
if_icmpeq L5
iload 2
iconst_3
if_icmpeq L5
iconst_1
istore 2
L6:
iload 4
ifeq L7
iload 2
ifne L2
L7:
iconst_0
ireturn
L5:
iconst_0
istore 2
goto L6
.limit locals 9
.limit stack 4
.end method

.method private b(Landroid/view/View;Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v7/widget/as/j [I
astore 3
aload 1
aload 3
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 2
aload 3
iconst_0
iaload
i2f
aload 3
iconst_1
iaload
i2f
invokevirtual android/view/MotionEvent/offsetLocation(FF)V
iconst_1
ireturn
.limit locals 4
.limit stack 4
.end method

.method private d()V
aload 0
getfield android/support/v7/widget/as/f Ljava/lang/Runnable;
ifnull L0
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
aload 0
getfield android/support/v7/widget/as/f Ljava/lang/Runnable;
invokevirtual android/view/View/removeCallbacks(Ljava/lang/Runnable;)Z
pop
L0:
aload 0
getfield android/support/v7/widget/as/e Ljava/lang/Runnable;
ifnull L1
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
aload 0
getfield android/support/v7/widget/as/e Ljava/lang/Runnable;
invokevirtual android/view/View/removeCallbacks(Ljava/lang/Runnable;)Z
pop
L1:
return
.limit locals 1
.limit stack 2
.end method

.method private e()V
aload 0
invokespecial android/support/v7/widget/as/d()V
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
astore 3
aload 3
invokevirtual android/view/View/isEnabled()Z
ifeq L0
aload 3
invokevirtual android/view/View/isLongClickable()Z
ifeq L1
L0:
return
L1:
aload 0
invokevirtual android/support/v7/widget/as/b()Z
ifeq L0
aload 3
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 1
lload 1
lload 1
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 4
aload 3
aload 4
invokevirtual android/view/View/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
aload 4
invokevirtual android/view/MotionEvent/recycle()V
aload 0
iconst_1
putfield android/support/v7/widget/as/g Z
aload 0
iconst_1
putfield android/support/v7/widget/as/h Z
return
.limit locals 5
.limit stack 8
.end method

.method public abstract a()Landroid/support/v7/widget/an;
.end method

.method public b()Z
aload 0
invokevirtual android/support/v7/widget/as/a()Landroid/support/v7/widget/an;
astore 1
aload 1
ifnull L0
aload 1
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifne L0
aload 1
invokevirtual android/support/v7/widget/an/b()V
L0:
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method

.method public c()Z
aload 0
invokevirtual android/support/v7/widget/as/a()Landroid/support/v7/widget/an;
astore 1
aload 1
ifnull L0
aload 1
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
aload 1
invokevirtual android/support/v7/widget/an/d()V
L0:
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
iconst_0
istore 8
aload 0
getfield android/support/v7/widget/as/g Z
istore 9
iload 9
ifeq L0
aload 0
getfield android/support/v7/widget/as/h Z
ifeq L1
aload 0
aload 2
invokespecial android/support/v7/widget/as/b(Landroid/view/MotionEvent;)Z
istore 7
L2:
aload 0
iload 7
putfield android/support/v7/widget/as/g Z
iload 7
ifne L3
iload 8
istore 7
iload 9
ifeq L4
L3:
iconst_1
istore 7
L4:
iload 7
ireturn
L1:
aload 0
aload 2
invokespecial android/support/v7/widget/as/b(Landroid/view/MotionEvent;)Z
ifne L5
aload 0
invokevirtual android/support/v7/widget/as/c()Z
ifne L6
L5:
iconst_1
istore 7
goto L2
L6:
iconst_0
istore 7
goto L2
L0:
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
astore 1
aload 1
invokevirtual android/view/View/isEnabled()Z
ifeq L7
aload 2
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 0
L8
L9
L10
L9
default : L7
L7:
iconst_0
istore 6
L11:
iload 6
ifeq L12
aload 0
invokevirtual android/support/v7/widget/as/b()Z
ifeq L12
iconst_1
istore 7
L13:
iload 7
ifeq L14
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 10
lload 10
lload 10
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 1
aload 0
getfield android/support/v7/widget/as/d Landroid/view/View;
aload 1
invokevirtual android/view/View/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
aload 1
invokevirtual android/view/MotionEvent/recycle()V
L14:
goto L2
L8:
aload 0
aload 2
iconst_0
invokevirtual android/view/MotionEvent/getPointerId(I)I
putfield android/support/v7/widget/as/i I
aload 0
iconst_0
putfield android/support/v7/widget/as/h Z
aload 0
getfield android/support/v7/widget/as/e Ljava/lang/Runnable;
ifnonnull L15
aload 0
new android/support/v7/widget/at
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/at/<init>(Landroid/support/v7/widget/as;B)V
putfield android/support/v7/widget/as/e Ljava/lang/Runnable;
L15:
aload 1
aload 0
getfield android/support/v7/widget/as/e Ljava/lang/Runnable;
aload 0
getfield android/support/v7/widget/as/b I
i2l
invokevirtual android/view/View/postDelayed(Ljava/lang/Runnable;J)Z
pop
aload 0
getfield android/support/v7/widget/as/f Ljava/lang/Runnable;
ifnonnull L16
aload 0
new android/support/v7/widget/au
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/au/<init>(Landroid/support/v7/widget/as;B)V
putfield android/support/v7/widget/as/f Ljava/lang/Runnable;
L16:
aload 1
aload 0
getfield android/support/v7/widget/as/f Ljava/lang/Runnable;
aload 0
getfield android/support/v7/widget/as/c I
i2l
invokevirtual android/view/View/postDelayed(Ljava/lang/Runnable;J)Z
pop
goto L7
L10:
aload 2
aload 0
getfield android/support/v7/widget/as/i I
invokevirtual android/view/MotionEvent/findPointerIndex(I)I
istore 6
iload 6
iflt L7
aload 2
iload 6
invokevirtual android/view/MotionEvent/getX(I)F
fstore 3
aload 2
iload 6
invokevirtual android/view/MotionEvent/getY(I)F
fstore 4
aload 0
getfield android/support/v7/widget/as/a F
fstore 5
fload 3
fload 5
fneg
fcmpl
iflt L17
fload 4
fload 5
fneg
fcmpl
iflt L17
fload 3
aload 1
invokevirtual android/view/View/getRight()I
aload 1
invokevirtual android/view/View/getLeft()I
isub
i2f
fload 5
fadd
fcmpg
ifge L17
fload 4
aload 1
invokevirtual android/view/View/getBottom()I
aload 1
invokevirtual android/view/View/getTop()I
isub
i2f
fload 5
fadd
fcmpg
ifge L17
iconst_1
istore 6
L18:
iload 6
ifne L7
aload 0
invokespecial android/support/v7/widget/as/d()V
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
iconst_1
istore 6
goto L11
L17:
iconst_0
istore 6
goto L18
L9:
aload 0
invokespecial android/support/v7/widget/as/d()V
goto L7
L12:
iconst_0
istore 7
goto L13
.limit locals 12
.limit stack 8
.end method
