.bytecode 50.0
.class public final synchronized android/support/v7/widget/cb
.super android/widget/CompoundButton

.field private static final 'N' [I

.field private static final 'a' I = 250


.field private static final 'b' I = 0


.field private static final 'c' I = 1


.field private static final 'd' I = 2


.field private static final 'e' Ljava/lang/String; = "android.widget.Switch"

.field private static final 'f' I = 1


.field private static final 'g' I = 2


.field private static final 'h' I = 3


.field private 'A' I

.field private 'B' I

.field private 'C' I

.field private 'D' I

.field private 'E' I

.field private 'F' Landroid/text/TextPaint;

.field private 'G' Landroid/content/res/ColorStateList;

.field private 'H' Landroid/text/Layout;

.field private 'I' Landroid/text/Layout;

.field private 'J' Landroid/text/method/TransformationMethod;

.field private 'K' Landroid/support/v7/widget/cd;

.field private final 'L' Landroid/graphics/Rect;

.field private final 'M' Landroid/support/v7/internal/widget/av;

.field private 'i' Landroid/graphics/drawable/Drawable;

.field private 'j' Landroid/graphics/drawable/Drawable;

.field private 'k' I

.field private 'l' I

.field private 'm' I

.field private 'n' Z

.field private 'o' Ljava/lang/CharSequence;

.field private 'p' Ljava/lang/CharSequence;

.field private 'q' Z

.field private 'r' I

.field private 's' I

.field private 't' F

.field private 'u' F

.field private 'v' Landroid/view/VelocityTracker;

.field private 'w' I

.field private 'x' F

.field private 'y' I

.field private 'z' I

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16842912
iastore
putstatic android/support/v7/widget/cb/N [I
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v7/widget/cb/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
getstatic android/support/v7/a/d/switchStyle I
invokespecial android/support/v7/widget/cb/<init>(Landroid/content/Context;I)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;I)V
aload 0
aload 1
aconst_null
iload 2
invokespecial android/widget/CompoundButton/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v7/widget/cb/v Landroid/view/VelocityTracker;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
aload 0
new android/text/TextPaint
dup
iconst_1
invokespecial android/text/TextPaint/<init>(I)V
putfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
aload 0
invokevirtual android/support/v7/widget/cb/getResources()Landroid/content/res/Resources;
astore 6
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
aload 6
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
putfield android/text/TextPaint/density F
aload 1
aconst_null
getstatic android/support/v7/a/n/SwitchCompat [I
iload 2
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 7
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_android_thumb I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L0:
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_track I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L1:
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_android_textOn I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
putfield android/support/v7/widget/cb/o Ljava/lang/CharSequence;
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_android_textOff I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
putfield android/support/v7/widget/cb/p Ljava/lang/CharSequence;
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_showText I
iconst_1
invokevirtual android/support/v7/internal/widget/ax/a(IZ)Z
putfield android/support/v7/widget/cb/q Z
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_thumbTextPadding I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/c(II)I
putfield android/support/v7/widget/cb/k I
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_switchMinWidth I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/c(II)I
putfield android/support/v7/widget/cb/l I
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_switchPadding I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/c(II)I
putfield android/support/v7/widget/cb/m I
aload 0
aload 7
getstatic android/support/v7/a/n/SwitchCompat_splitTrack I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(IZ)Z
putfield android/support/v7/widget/cb/n Z
aload 7
getstatic android/support/v7/a/n/SwitchCompat_switchTextAppearance I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
istore 2
iload 2
ifeq L2
aload 1
iload 2
getstatic android/support/v7/a/n/TextAppearance [I
invokevirtual android/content/Context/obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
astore 8
aload 8
getstatic android/support/v7/a/n/TextAppearance_android_textColor I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
astore 6
aload 6
ifnull L3
aload 0
aload 6
putfield android/support/v7/widget/cb/G Landroid/content/res/ColorStateList;
L4:
aload 8
getstatic android/support/v7/a/n/TextAppearance_android_textSize I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 2
iload 2
ifeq L5
iload 2
i2f
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/getTextSize()F
fcmpl
ifeq L5
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
iload 2
i2f
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
L5:
aload 8
getstatic android/support/v7/a/n/TextAppearance_android_typeface I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
istore 2
aload 8
getstatic android/support/v7/a/n/TextAppearance_android_textStyle I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
istore 4
iload 2
tableswitch 1
L6
L7
L8
default : L9
L9:
aconst_null
astore 6
L10:
iload 4
ifle L11
aload 6
ifnonnull L12
iload 4
invokestatic android/graphics/Typeface/defaultFromStyle(I)Landroid/graphics/Typeface;
astore 6
L13:
aload 0
aload 6
invokevirtual android/support/v7/widget/cb/setSwitchTypeface(Landroid/graphics/Typeface;)V
aload 6
ifnull L14
aload 6
invokevirtual android/graphics/Typeface/getStyle()I
istore 2
L15:
iload 4
iload 2
iconst_m1
ixor
iand
istore 2
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 6
iload 2
iconst_1
iand
ifeq L16
iconst_1
istore 5
L17:
aload 6
iload 5
invokevirtual android/text/TextPaint/setFakeBoldText(Z)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 6
iload 2
iconst_2
iand
ifeq L18
ldc_w -0.25F
fstore 3
L19:
aload 6
fload 3
invokevirtual android/text/TextPaint/setTextSkewX(F)V
L20:
aload 8
getstatic android/support/v7/a/n/TextAppearance_textAllCaps I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
ifeq L21
aload 0
new android/support/v7/internal/b/a
dup
aload 0
invokevirtual android/support/v7/widget/cb/getContext()Landroid/content/Context;
invokespecial android/support/v7/internal/b/a/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/cb/J Landroid/text/method/TransformationMethod;
L22:
aload 8
invokevirtual android/content/res/TypedArray/recycle()V
L2:
aload 0
aload 7
invokevirtual android/support/v7/internal/widget/ax/a()Landroid/support/v7/internal/widget/av;
putfield android/support/v7/widget/cb/M Landroid/support/v7/internal/widget/av;
aload 7
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
aload 1
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
astore 1
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/v7/widget/cb/s I
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledMinimumFlingVelocity()I
putfield android/support/v7/widget/cb/w I
aload 0
invokevirtual android/support/v7/widget/cb/refreshDrawableState()V
aload 0
aload 0
invokevirtual android/support/v7/widget/cb/isChecked()Z
invokevirtual android/support/v7/widget/cb/setChecked(Z)V
return
L3:
aload 0
aload 0
invokevirtual android/support/v7/widget/cb/getTextColors()Landroid/content/res/ColorStateList;
putfield android/support/v7/widget/cb/G Landroid/content/res/ColorStateList;
goto L4
L6:
getstatic android/graphics/Typeface/SANS_SERIF Landroid/graphics/Typeface;
astore 6
goto L10
L7:
getstatic android/graphics/Typeface/SERIF Landroid/graphics/Typeface;
astore 6
goto L10
L8:
getstatic android/graphics/Typeface/MONOSPACE Landroid/graphics/Typeface;
astore 6
goto L10
L12:
aload 6
iload 4
invokestatic android/graphics/Typeface/create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;
astore 6
goto L13
L14:
iconst_0
istore 2
goto L15
L16:
iconst_0
istore 5
goto L17
L18:
fconst_0
fstore 3
goto L19
L11:
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
iconst_0
invokevirtual android/text/TextPaint/setFakeBoldText(Z)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
fconst_0
invokevirtual android/text/TextPaint/setTextSkewX(F)V
aload 0
aload 6
invokevirtual android/support/v7/widget/cb/setSwitchTypeface(Landroid/graphics/Typeface;)V
goto L20
L21:
aload 0
aconst_null
putfield android/support/v7/widget/cb/J Landroid/text/method/TransformationMethod;
goto L22
.limit locals 9
.limit stack 4
.end method

.method private static a(F)F
fload 0
fconst_0
fcmpg
ifge L0
fconst_0
fstore 1
L1:
fload 1
freturn
L0:
fload 0
fstore 1
fload 0
fconst_1
fcmpl
ifle L1
fconst_1
freturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/text/Layout;
aload 0
getfield android/support/v7/widget/cb/J Landroid/text/method/TransformationMethod;
ifnull L0
aload 0
getfield android/support/v7/widget/cb/J Landroid/text/method/TransformationMethod;
aload 1
aload 0
invokeinterface android/text/method/TransformationMethod/getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence; 2
astore 1
L1:
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 3
aload 1
ifnull L2
aload 1
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
invokestatic android/text/Layout/getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F
f2d
invokestatic java/lang/Math/ceil(D)D
d2i
istore 2
L3:
new android/text/StaticLayout
dup
aload 1
aload 3
iload 2
getstatic android/text/Layout$Alignment/ALIGN_NORMAL Landroid/text/Layout$Alignment;
fconst_1
fconst_0
iconst_1
invokespecial android/text/StaticLayout/<init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
areturn
L0:
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 4
.limit stack 9
.end method

.method private a()V
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
ifnull L0
aload 0
invokevirtual android/support/v7/widget/cb/clearAnimation()V
aload 0
aconst_null
putfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private a(II)V
iconst_0
istore 4
aconst_null
astore 5
iload 1
tableswitch 1
L0
L1
L2
default : L3
L3:
iload 2
ifle L4
aload 5
ifnonnull L5
iload 2
invokestatic android/graphics/Typeface/defaultFromStyle(I)Landroid/graphics/Typeface;
astore 5
L6:
aload 0
aload 5
invokevirtual android/support/v7/widget/cb/setSwitchTypeface(Landroid/graphics/Typeface;)V
aload 5
ifnull L7
aload 5
invokevirtual android/graphics/Typeface/getStyle()I
istore 1
L8:
iload 1
iconst_m1
ixor
iload 2
iand
istore 1
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 5
iload 1
iconst_1
iand
ifeq L9
iconst_1
istore 4
L9:
aload 5
iload 4
invokevirtual android/text/TextPaint/setFakeBoldText(Z)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 5
iload 1
iconst_2
iand
ifeq L10
ldc_w -0.25F
fstore 3
L11:
aload 5
fload 3
invokevirtual android/text/TextPaint/setTextSkewX(F)V
return
L0:
getstatic android/graphics/Typeface/SANS_SERIF Landroid/graphics/Typeface;
astore 5
goto L3
L1:
getstatic android/graphics/Typeface/SERIF Landroid/graphics/Typeface;
astore 5
goto L3
L2:
getstatic android/graphics/Typeface/MONOSPACE Landroid/graphics/Typeface;
astore 5
goto L3
L5:
aload 5
iload 2
invokestatic android/graphics/Typeface/create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;
astore 5
goto L6
L7:
iconst_0
istore 1
goto L8
L10:
fconst_0
fstore 3
goto L11
L4:
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
iconst_0
invokevirtual android/text/TextPaint/setFakeBoldText(Z)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
fconst_0
invokevirtual android/text/TextPaint/setTextSkewX(F)V
aload 0
aload 5
invokevirtual android/support/v7/widget/cb/setSwitchTypeface(Landroid/graphics/Typeface;)V
return
.limit locals 6
.limit stack 2
.end method

.method private a(Landroid/content/Context;I)V
aload 1
iload 2
getstatic android/support/v7/a/n/TextAppearance [I
invokevirtual android/content/Context/obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
astore 6
aload 6
getstatic android/support/v7/a/n/TextAppearance_android_textColor I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
astore 1
aload 1
ifnull L0
aload 0
aload 1
putfield android/support/v7/widget/cb/G Landroid/content/res/ColorStateList;
L1:
aload 6
getstatic android/support/v7/a/n/TextAppearance_android_textSize I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 2
iload 2
ifeq L2
iload 2
i2f
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/getTextSize()F
fcmpl
ifeq L2
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
iload 2
i2f
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
L2:
aload 6
getstatic android/support/v7/a/n/TextAppearance_android_typeface I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
istore 2
aload 6
getstatic android/support/v7/a/n/TextAppearance_android_textStyle I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
istore 4
iload 2
tableswitch 1
L3
L4
L5
default : L6
L6:
aconst_null
astore 1
L7:
iload 4
ifle L8
aload 1
ifnonnull L9
iload 4
invokestatic android/graphics/Typeface/defaultFromStyle(I)Landroid/graphics/Typeface;
astore 1
L10:
aload 0
aload 1
invokevirtual android/support/v7/widget/cb/setSwitchTypeface(Landroid/graphics/Typeface;)V
aload 1
ifnull L11
aload 1
invokevirtual android/graphics/Typeface/getStyle()I
istore 2
L12:
iload 4
iload 2
iconst_m1
ixor
iand
istore 2
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 1
iload 2
iconst_1
iand
ifeq L13
iconst_1
istore 5
L14:
aload 1
iload 5
invokevirtual android/text/TextPaint/setFakeBoldText(Z)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 1
iload 2
iconst_2
iand
ifeq L15
ldc_w -0.25F
fstore 3
L16:
aload 1
fload 3
invokevirtual android/text/TextPaint/setTextSkewX(F)V
L17:
aload 6
getstatic android/support/v7/a/n/TextAppearance_textAllCaps I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
ifeq L18
aload 0
new android/support/v7/internal/b/a
dup
aload 0
invokevirtual android/support/v7/widget/cb/getContext()Landroid/content/Context;
invokespecial android/support/v7/internal/b/a/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/cb/J Landroid/text/method/TransformationMethod;
L19:
aload 6
invokevirtual android/content/res/TypedArray/recycle()V
return
L0:
aload 0
aload 0
invokevirtual android/support/v7/widget/cb/getTextColors()Landroid/content/res/ColorStateList;
putfield android/support/v7/widget/cb/G Landroid/content/res/ColorStateList;
goto L1
L3:
getstatic android/graphics/Typeface/SANS_SERIF Landroid/graphics/Typeface;
astore 1
goto L7
L4:
getstatic android/graphics/Typeface/SERIF Landroid/graphics/Typeface;
astore 1
goto L7
L5:
getstatic android/graphics/Typeface/MONOSPACE Landroid/graphics/Typeface;
astore 1
goto L7
L9:
aload 1
iload 4
invokestatic android/graphics/Typeface/create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;
astore 1
goto L10
L11:
iconst_0
istore 2
goto L12
L13:
iconst_0
istore 5
goto L14
L15:
fconst_0
fstore 3
goto L16
L8:
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
iconst_0
invokevirtual android/text/TextPaint/setFakeBoldText(Z)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
fconst_0
invokevirtual android/text/TextPaint/setTextSkewX(F)V
aload 0
aload 1
invokevirtual android/support/v7/widget/cb/setSwitchTypeface(Landroid/graphics/Typeface;)V
goto L17
L18:
aload 0
aconst_null
putfield android/support/v7/widget/cb/J Landroid/text/method/TransformationMethod;
goto L19
.limit locals 7
.limit stack 4
.end method

.method private a(Landroid/graphics/Typeface;I)V
iconst_0
istore 5
iload 2
ifle L0
aload 1
ifnonnull L1
iload 2
invokestatic android/graphics/Typeface/defaultFromStyle(I)Landroid/graphics/Typeface;
astore 1
L2:
aload 0
aload 1
invokevirtual android/support/v7/widget/cb/setSwitchTypeface(Landroid/graphics/Typeface;)V
aload 1
ifnull L3
aload 1
invokevirtual android/graphics/Typeface/getStyle()I
istore 4
L4:
iload 4
iconst_m1
ixor
iload 2
iand
istore 2
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 1
iload 2
iconst_1
iand
ifeq L5
iconst_1
istore 5
L5:
aload 1
iload 5
invokevirtual android/text/TextPaint/setFakeBoldText(Z)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
astore 1
iload 2
iconst_2
iand
ifeq L6
ldc_w -0.25F
fstore 3
L7:
aload 1
fload 3
invokevirtual android/text/TextPaint/setTextSkewX(F)V
return
L1:
aload 1
iload 2
invokestatic android/graphics/Typeface/create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;
astore 1
goto L2
L3:
iconst_0
istore 4
goto L4
L6:
fconst_0
fstore 3
goto L7
L0:
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
iconst_0
invokevirtual android/text/TextPaint/setFakeBoldText(Z)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
fconst_0
invokevirtual android/text/TextPaint/setTextSkewX(F)V
aload 0
aload 1
invokevirtual android/support/v7/widget/cb/setSwitchTypeface(Landroid/graphics/Typeface;)V
return
.limit locals 6
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/widget/cb;F)V
aload 0
fload 1
invokespecial android/support/v7/widget/cb/setThumbPosition(F)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/MotionEvent;)V
aload 1
invokestatic android/view/MotionEvent/obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
astore 1
aload 1
iconst_3
invokevirtual android/view/MotionEvent/setAction(I)V
aload 0
aload 1
invokespecial android/widget/CompoundButton/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
aload 1
invokevirtual android/view/MotionEvent/recycle()V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
getfield android/support/v7/widget/cb/x F
fstore 3
iload 1
ifeq L0
fconst_1
fstore 2
L1:
aload 0
new android/support/v7/widget/cd
dup
aload 0
fload 3
fload 2
iconst_0
invokespecial android/support/v7/widget/cd/<init>(Landroid/support/v7/widget/cb;FFB)V
putfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
ldc2_w 250L
invokevirtual android/support/v7/widget/cd/setDuration(J)V
aload 0
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
invokevirtual android/support/v7/widget/cb/startAnimation(Landroid/view/animation/Animation;)V
return
L0:
fconst_0
fstore 2
goto L1
.limit locals 4
.limit stack 7
.end method

.method private a(FF)Z
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnonnull L0
L1:
iconst_0
ireturn
L0:
aload 0
invokespecial android/support/v7/widget/cb/getThumbOffset()I
istore 5
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v7/widget/cb/C I
istore 3
aload 0
getfield android/support/v7/widget/cb/s I
istore 4
iload 5
aload 0
getfield android/support/v7/widget/cb/B I
iadd
aload 0
getfield android/support/v7/widget/cb/s I
isub
istore 5
aload 0
getfield android/support/v7/widget/cb/A I
istore 6
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
istore 7
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
istore 8
aload 0
getfield android/support/v7/widget/cb/s I
istore 9
aload 0
getfield android/support/v7/widget/cb/E I
istore 10
aload 0
getfield android/support/v7/widget/cb/s I
istore 11
fload 1
iload 5
i2f
fcmpl
ifle L1
fload 1
iload 6
iload 5
iadd
iload 7
iadd
iload 8
iadd
iload 9
iadd
i2f
fcmpg
ifge L1
fload 2
iload 3
iload 4
isub
i2f
fcmpl
ifle L1
fload 2
iload 10
iload 11
iadd
i2f
fcmpg
ifge L1
iconst_1
ireturn
.limit locals 12
.limit stack 3
.end method

.method private b(Landroid/view/MotionEvent;)V
iconst_1
istore 4
aload 0
iconst_0
putfield android/support/v7/widget/cb/r I
aload 1
invokevirtual android/view/MotionEvent/getAction()I
iconst_1
if_icmpne L0
aload 0
invokevirtual android/support/v7/widget/cb/isEnabled()Z
ifeq L0
iconst_1
istore 3
L1:
aload 0
invokevirtual android/support/v7/widget/cb/isChecked()Z
istore 5
iload 3
ifeq L2
aload 0
getfield android/support/v7/widget/cb/v Landroid/view/VelocityTracker;
sipush 1000
invokevirtual android/view/VelocityTracker/computeCurrentVelocity(I)V
aload 0
getfield android/support/v7/widget/cb/v Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/getXVelocity()F
fstore 2
fload 2
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v7/widget/cb/w I
i2f
fcmpl
ifle L3
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L4
fload 2
fconst_0
fcmpg
ifge L5
L6:
iload 4
iload 5
if_icmpeq L7
aload 0
iconst_0
invokevirtual android/support/v7/widget/cb/playSoundEffect(I)V
aload 0
iload 4
invokevirtual android/support/v7/widget/cb/setChecked(Z)V
L7:
aload 1
invokestatic android/view/MotionEvent/obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
astore 1
aload 1
iconst_3
invokevirtual android/view/MotionEvent/setAction(I)V
aload 0
aload 1
invokespecial android/widget/CompoundButton/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
aload 1
invokevirtual android/view/MotionEvent/recycle()V
return
L0:
iconst_0
istore 3
goto L1
L5:
iconst_0
istore 4
goto L6
L4:
fload 2
fconst_0
fcmpl
ifgt L6
iconst_0
istore 4
goto L6
L3:
aload 0
invokespecial android/support/v7/widget/cb/getTargetCheckedState()Z
istore 4
goto L6
L2:
iload 5
istore 4
goto L6
.limit locals 6
.limit stack 2
.end method

.method private getTargetCheckedState()Z
aload 0
getfield android/support/v7/widget/cb/x F
ldc_w 0.5F
fcmpl
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private getThumbOffset()I
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L0
fconst_1
aload 0
getfield android/support/v7/widget/cb/x F
fsub
fstore 1
L1:
fload 1
aload 0
invokespecial android/support/v7/widget/cb/getThumbScrollRange()I
i2f
fmul
ldc_w 0.5F
fadd
f2i
ireturn
L0:
aload 0
getfield android/support/v7/widget/cb/x F
fstore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method private getThumbScrollRange()I
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
astore 2
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
aload 2
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
invokestatic android/support/v7/internal/widget/ae/a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
astore 1
L2:
aload 0
getfield android/support/v7/widget/cb/y I
aload 0
getfield android/support/v7/widget/cb/A I
isub
aload 2
getfield android/graphics/Rect/left I
isub
aload 2
getfield android/graphics/Rect/right I
isub
aload 1
getfield android/graphics/Rect/left I
isub
aload 1
getfield android/graphics/Rect/right I
isub
ireturn
L1:
getstatic android/support/v7/internal/widget/ae/a Landroid/graphics/Rect;
astore 1
goto L2
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private setThumbPosition(F)V
aload 0
fload 1
putfield android/support/v7/widget/cb/x F
aload 0
invokevirtual android/support/v7/widget/cb/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public final draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
astore 12
aload 0
getfield android/support/v7/widget/cb/B I
istore 3
aload 0
getfield android/support/v7/widget/cb/C I
istore 6
aload 0
getfield android/support/v7/widget/cb/D I
istore 8
aload 0
getfield android/support/v7/widget/cb/E I
istore 7
iload 3
aload 0
invokespecial android/support/v7/widget/cb/getThumbOffset()I
iadd
istore 9
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
invokestatic android/support/v7/internal/widget/ae/a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
astore 11
L1:
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
aload 12
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 12
getfield android/graphics/Rect/left I
istore 10
aload 11
ifnull L3
aload 11
invokevirtual android/graphics/Rect/isEmpty()Z
ifne L3
iload 3
istore 2
aload 11
getfield android/graphics/Rect/left I
aload 12
getfield android/graphics/Rect/left I
if_icmple L4
iload 3
aload 11
getfield android/graphics/Rect/left I
aload 12
getfield android/graphics/Rect/left I
isub
iadd
istore 2
L4:
aload 11
getfield android/graphics/Rect/top I
aload 12
getfield android/graphics/Rect/top I
if_icmple L5
aload 11
getfield android/graphics/Rect/top I
aload 12
getfield android/graphics/Rect/top I
isub
iload 6
iadd
istore 3
L6:
iload 8
istore 5
aload 11
getfield android/graphics/Rect/right I
aload 12
getfield android/graphics/Rect/right I
if_icmple L7
iload 8
aload 11
getfield android/graphics/Rect/right I
aload 12
getfield android/graphics/Rect/right I
isub
isub
istore 5
L7:
aload 11
getfield android/graphics/Rect/bottom I
aload 12
getfield android/graphics/Rect/bottom I
if_icmple L8
iload 7
aload 11
getfield android/graphics/Rect/bottom I
aload 12
getfield android/graphics/Rect/bottom I
isub
isub
istore 4
L9:
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
iload 2
iload 3
iload 5
iload 4
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
iload 10
iload 9
iadd
istore 2
L10:
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L11
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
aload 12
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
iload 2
aload 12
getfield android/graphics/Rect/left I
isub
istore 3
iload 2
aload 0
getfield android/support/v7/widget/cb/A I
iadd
aload 12
getfield android/graphics/Rect/right I
iadd
istore 2
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
iload 3
iload 6
iload 2
iload 7
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
invokevirtual android/support/v7/widget/cb/getBackground()Landroid/graphics/drawable/Drawable;
astore 11
aload 11
ifnull L11
aload 11
iload 3
iload 6
iload 2
iload 7
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;IIII)V
L11:
aload 0
aload 1
invokespecial android/widget/CompoundButton/draw(Landroid/graphics/Canvas;)V
return
L0:
getstatic android/support/v7/internal/widget/ae/a Landroid/graphics/Rect;
astore 11
goto L1
L8:
iload 7
istore 4
goto L9
L5:
iload 6
istore 3
goto L6
L3:
iload 7
istore 4
iload 6
istore 5
iload 3
istore 2
iload 5
istore 3
iload 8
istore 5
goto L9
L2:
iload 9
istore 2
goto L10
.limit locals 13
.limit stack 5
.end method

.method public final drawableHotspotChanged(FF)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
fload 1
fload 2
invokespecial android/widget/CompoundButton/drawableHotspotChanged(FF)V
L0:
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
fload 1
fload 2
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;FF)V
L1:
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
fload 1
fload 2
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;FF)V
L2:
return
.limit locals 3
.limit stack 3
.end method

.method protected final drawableStateChanged()V
aload 0
invokespecial android/widget/CompoundButton/drawableStateChanged()V
aload 0
invokevirtual android/support/v7/widget/cb/getDrawableState()[I
astore 1
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L0:
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L1:
aload 0
invokevirtual android/support/v7/widget/cb/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public final getCompoundPaddingLeft()I
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifne L0
aload 0
invokespecial android/widget/CompoundButton/getCompoundPaddingLeft()I
istore 1
L1:
iload 1
ireturn
L0:
aload 0
invokespecial android/widget/CompoundButton/getCompoundPaddingLeft()I
aload 0
getfield android/support/v7/widget/cb/y I
iadd
istore 2
iload 2
istore 1
aload 0
invokevirtual android/support/v7/widget/cb/getText()Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
iload 2
aload 0
getfield android/support/v7/widget/cb/m I
iadd
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final getCompoundPaddingRight()I
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L0
aload 0
invokespecial android/widget/CompoundButton/getCompoundPaddingRight()I
istore 1
L1:
iload 1
ireturn
L0:
aload 0
invokespecial android/widget/CompoundButton/getCompoundPaddingRight()I
aload 0
getfield android/support/v7/widget/cb/y I
iadd
istore 2
iload 2
istore 1
aload 0
invokevirtual android/support/v7/widget/cb/getText()Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
iload 2
aload 0
getfield android/support/v7/widget/cb/m I
iadd
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final getShowText()Z
aload 0
getfield android/support/v7/widget/cb/q Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getSplitTrack()Z
aload 0
getfield android/support/v7/widget/cb/n Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getSwitchMinWidth()I
aload 0
getfield android/support/v7/widget/cb/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getSwitchPadding()I
aload 0
getfield android/support/v7/widget/cb/m I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getTextOff()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/cb/p Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTextOn()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/cb/o Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getThumbDrawable()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getThumbTextPadding()I
aload 0
getfield android/support/v7/widget/cb/k I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getTrackDrawable()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final jumpDrawablesToCurrentState()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 0
invokespecial android/widget/CompoundButton/jumpDrawablesToCurrentState()V
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/jumpToCurrentState()V
L1:
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/jumpToCurrentState()V
L2:
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
ifnull L0
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
invokevirtual android/support/v7/widget/cd/hasEnded()Z
ifne L0
aload 0
invokevirtual android/support/v7/widget/cb/clearAnimation()V
aload 0
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
getfield android/support/v7/widget/cd/b F
invokespecial android/support/v7/widget/cb/setThumbPosition(F)V
aload 0
aconst_null
putfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method protected final onCreateDrawableState(I)[I
aload 0
iload 1
iconst_1
iadd
invokespecial android/widget/CompoundButton/onCreateDrawableState(I)[I
astore 2
aload 0
invokevirtual android/support/v7/widget/cb/isChecked()Z
ifeq L0
aload 2
getstatic android/support/v7/widget/cb/N [I
invokestatic android/support/v7/widget/cb/mergeDrawableStates([I[I)[I
pop
L0:
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/widget/CompoundButton/onDraw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
astore 9
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
astore 11
aload 11
ifnull L0
aload 11
aload 9
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
L1:
aload 0
getfield android/support/v7/widget/cb/C I
istore 4
aload 0
getfield android/support/v7/widget/cb/E I
istore 5
aload 9
getfield android/graphics/Rect/top I
istore 6
aload 9
getfield android/graphics/Rect/bottom I
istore 7
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
astore 10
aload 11
ifnull L2
aload 0
getfield android/support/v7/widget/cb/n Z
ifeq L3
aload 10
ifnull L3
aload 10
invokestatic android/support/v7/internal/widget/ae/a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
astore 12
aload 10
aload 9
invokevirtual android/graphics/drawable/Drawable/copyBounds(Landroid/graphics/Rect;)V
aload 9
aload 9
getfield android/graphics/Rect/left I
aload 12
getfield android/graphics/Rect/left I
iadd
putfield android/graphics/Rect/left I
aload 9
aload 9
getfield android/graphics/Rect/right I
aload 12
getfield android/graphics/Rect/right I
isub
putfield android/graphics/Rect/right I
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 2
aload 1
aload 9
getstatic android/graphics/Region$Op/DIFFERENCE Landroid/graphics/Region$Op;
invokevirtual android/graphics/Canvas/clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z
pop
aload 11
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
aload 1
iload 2
invokevirtual android/graphics/Canvas/restoreToCount(I)V
L2:
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 3
aload 10
ifnull L4
aload 10
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L4:
aload 0
invokespecial android/support/v7/widget/cb/getTargetCheckedState()Z
ifeq L5
aload 0
getfield android/support/v7/widget/cb/H Landroid/text/Layout;
astore 9
L6:
aload 9
ifnull L7
aload 0
invokevirtual android/support/v7/widget/cb/getDrawableState()[I
astore 11
aload 0
getfield android/support/v7/widget/cb/G Landroid/content/res/ColorStateList;
ifnull L8
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
aload 0
getfield android/support/v7/widget/cb/G Landroid/content/res/ColorStateList;
aload 11
iconst_0
invokevirtual android/content/res/ColorStateList/getColorForState([II)I
invokevirtual android/text/TextPaint/setColor(I)V
L8:
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
aload 11
putfield android/text/TextPaint/drawableState [I
aload 10
ifnull L9
aload 10
invokevirtual android/graphics/drawable/Drawable/getBounds()Landroid/graphics/Rect;
astore 10
aload 10
getfield android/graphics/Rect/left I
istore 2
aload 10
getfield android/graphics/Rect/right I
iload 2
iadd
istore 2
L10:
iload 2
iconst_2
idiv
istore 2
aload 9
invokevirtual android/text/Layout/getWidth()I
iconst_2
idiv
istore 8
iload 4
iload 6
iadd
iload 5
iload 7
isub
iadd
iconst_2
idiv
istore 4
aload 9
invokevirtual android/text/Layout/getHeight()I
iconst_2
idiv
istore 5
aload 1
iload 2
iload 8
isub
i2f
iload 4
iload 5
isub
i2f
invokevirtual android/graphics/Canvas/translate(FF)V
aload 9
aload 1
invokevirtual android/text/Layout/draw(Landroid/graphics/Canvas;)V
L7:
aload 1
iload 3
invokevirtual android/graphics/Canvas/restoreToCount(I)V
return
L0:
aload 9
invokevirtual android/graphics/Rect/setEmpty()V
goto L1
L3:
aload 11
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
goto L2
L5:
aload 0
getfield android/support/v7/widget/cb/I Landroid/text/Layout;
astore 9
goto L6
L9:
aload 0
invokevirtual android/support/v7/widget/cb/getWidth()I
istore 2
goto L10
.limit locals 13
.limit stack 4
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 14
.end annotation
aload 0
aload 1
invokespecial android/widget/CompoundButton/onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
ldc "android.widget.Switch"
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
aload 0
aload 1
invokespecial android/widget/CompoundButton/onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
aload 1
ldc "android.widget.Switch"
invokevirtual android/view/accessibility/AccessibilityNodeInfo/setClassName(Ljava/lang/CharSequence;)V
aload 0
invokevirtual android/support/v7/widget/cb/isChecked()Z
ifeq L1
aload 0
getfield android/support/v7/widget/cb/o Ljava/lang/CharSequence;
astore 2
L2:
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
aload 1
invokevirtual android/view/accessibility/AccessibilityNodeInfo/getText()Ljava/lang/CharSequence;
astore 3
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L3
aload 1
aload 2
invokevirtual android/view/accessibility/AccessibilityNodeInfo/setText(Ljava/lang/CharSequence;)V
L0:
return
L1:
aload 0
getfield android/support/v7/widget/cb/p Ljava/lang/CharSequence;
astore 2
goto L2
L3:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
aload 4
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
bipush 32
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
pop
aload 1
aload 4
invokevirtual android/view/accessibility/AccessibilityNodeInfo/setText(Ljava/lang/CharSequence;)V
return
.limit locals 5
.limit stack 2
.end method

.method protected final onLayout(ZIIII)V
iconst_0
istore 6
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/CompoundButton/onLayout(ZIIII)V
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
astore 7
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
aload 7
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
L2:
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
invokestatic android/support/v7/internal/widget/ae/a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
astore 8
iconst_0
aload 8
getfield android/graphics/Rect/left I
aload 7
getfield android/graphics/Rect/left I
isub
invokestatic java/lang/Math/max(II)I
istore 3
iconst_0
aload 8
getfield android/graphics/Rect/right I
aload 7
getfield android/graphics/Rect/right I
isub
invokestatic java/lang/Math/max(II)I
istore 2
L3:
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L4
aload 0
invokevirtual android/support/v7/widget/cb/getPaddingLeft()I
iload 3
iadd
istore 4
aload 0
getfield android/support/v7/widget/cb/y I
iload 4
iadd
iload 3
isub
iload 2
isub
istore 5
L5:
aload 0
invokevirtual android/support/v7/widget/cb/getGravity()I
bipush 112
iand
lookupswitch
16 : L6
80 : L7
default : L8
L8:
aload 0
invokevirtual android/support/v7/widget/cb/getPaddingTop()I
istore 3
aload 0
getfield android/support/v7/widget/cb/z I
iload 3
iadd
istore 2
L9:
aload 0
iload 4
putfield android/support/v7/widget/cb/B I
aload 0
iload 3
putfield android/support/v7/widget/cb/C I
aload 0
iload 2
putfield android/support/v7/widget/cb/E I
aload 0
iload 5
putfield android/support/v7/widget/cb/D I
return
L1:
aload 7
invokevirtual android/graphics/Rect/setEmpty()V
goto L2
L4:
aload 0
invokevirtual android/support/v7/widget/cb/getWidth()I
aload 0
invokevirtual android/support/v7/widget/cb/getPaddingRight()I
isub
iload 2
isub
istore 5
iload 2
iload 3
iload 5
aload 0
getfield android/support/v7/widget/cb/y I
isub
iadd
iadd
istore 4
goto L5
L6:
aload 0
invokevirtual android/support/v7/widget/cb/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/cb/getHeight()I
iadd
aload 0
invokevirtual android/support/v7/widget/cb/getPaddingBottom()I
isub
iconst_2
idiv
aload 0
getfield android/support/v7/widget/cb/z I
iconst_2
idiv
isub
istore 3
aload 0
getfield android/support/v7/widget/cb/z I
iload 3
iadd
istore 2
goto L9
L7:
aload 0
invokevirtual android/support/v7/widget/cb/getHeight()I
aload 0
invokevirtual android/support/v7/widget/cb/getPaddingBottom()I
isub
istore 2
iload 2
aload 0
getfield android/support/v7/widget/cb/z I
isub
istore 3
goto L9
L0:
iconst_0
istore 3
iload 6
istore 2
goto L3
.limit locals 9
.limit stack 6
.end method

.method public final onMeasure(II)V
iconst_0
istore 6
aload 0
getfield android/support/v7/widget/cb/q Z
ifeq L0
aload 0
getfield android/support/v7/widget/cb/H Landroid/text/Layout;
ifnonnull L1
aload 0
aload 0
aload 0
getfield android/support/v7/widget/cb/o Ljava/lang/CharSequence;
invokespecial android/support/v7/widget/cb/a(Ljava/lang/CharSequence;)Landroid/text/Layout;
putfield android/support/v7/widget/cb/H Landroid/text/Layout;
L1:
aload 0
getfield android/support/v7/widget/cb/I Landroid/text/Layout;
ifnonnull L0
aload 0
aload 0
aload 0
getfield android/support/v7/widget/cb/p Ljava/lang/CharSequence;
invokespecial android/support/v7/widget/cb/a(Ljava/lang/CharSequence;)Landroid/text/Layout;
putfield android/support/v7/widget/cb/I Landroid/text/Layout;
L0:
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
astore 9
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
aload 9
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
aload 9
getfield android/graphics/Rect/left I
isub
aload 9
getfield android/graphics/Rect/right I
isub
istore 4
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
istore 3
L3:
aload 0
getfield android/support/v7/widget/cb/q Z
ifeq L4
aload 0
getfield android/support/v7/widget/cb/H Landroid/text/Layout;
invokevirtual android/text/Layout/getWidth()I
aload 0
getfield android/support/v7/widget/cb/I Landroid/text/Layout;
invokevirtual android/text/Layout/getWidth()I
invokestatic java/lang/Math/max(II)I
aload 0
getfield android/support/v7/widget/cb/k I
iconst_2
imul
iadd
istore 5
L5:
aload 0
iload 5
iload 4
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/cb/A I
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
ifnull L6
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
aload 9
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
istore 4
L7:
aload 9
getfield android/graphics/Rect/left I
istore 8
aload 9
getfield android/graphics/Rect/right I
istore 7
iload 8
istore 6
iload 7
istore 5
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L8
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
invokestatic android/support/v7/internal/widget/ae/a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
astore 9
iload 8
aload 9
getfield android/graphics/Rect/left I
invokestatic java/lang/Math/max(II)I
istore 6
iload 7
aload 9
getfield android/graphics/Rect/right I
invokestatic java/lang/Math/max(II)I
istore 5
L8:
aload 0
getfield android/support/v7/widget/cb/l I
iload 6
aload 0
getfield android/support/v7/widget/cb/A I
iconst_2
imul
iadd
iload 5
iadd
invokestatic java/lang/Math/max(II)I
istore 5
iload 4
iload 3
invokestatic java/lang/Math/max(II)I
istore 3
aload 0
iload 5
putfield android/support/v7/widget/cb/y I
aload 0
iload 3
putfield android/support/v7/widget/cb/z I
aload 0
iload 1
iload 2
invokespecial android/widget/CompoundButton/onMeasure(II)V
aload 0
invokevirtual android/support/v7/widget/cb/getMeasuredHeight()I
iload 3
if_icmpge L9
aload 0
aload 0
invokestatic android/support/v4/view/cx/i(Landroid/view/View;)I
iload 3
invokevirtual android/support/v7/widget/cb/setMeasuredDimension(II)V
L9:
return
L2:
iconst_0
istore 3
iconst_0
istore 4
goto L3
L4:
iconst_0
istore 5
goto L5
L6:
aload 9
invokevirtual android/graphics/Rect/setEmpty()V
iload 6
istore 4
goto L7
.limit locals 10
.limit stack 4
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 14
.end annotation
aload 0
aload 1
invokespecial android/widget/CompoundButton/onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
invokevirtual android/support/v7/widget/cb/isChecked()Z
ifeq L0
aload 0
getfield android/support/v7/widget/cb/o Ljava/lang/CharSequence;
astore 2
L1:
aload 2
ifnull L2
aload 1
invokevirtual android/view/accessibility/AccessibilityEvent/getText()Ljava/util/List;
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L2:
return
L0:
aload 0
getfield android/support/v7/widget/cb/p Ljava/lang/CharSequence;
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
fconst_1
fstore 4
iconst_0
istore 7
iconst_1
istore 17
aload 0
getfield android/support/v7/widget/cb/v Landroid/view/VelocityTracker;
aload 1
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 0
L0
L1
L2
L1
default : L3
L3:
aload 0
aload 1
invokespecial android/widget/CompoundButton/onTouchEvent(Landroid/view/MotionEvent;)Z
istore 17
L4:
iload 17
ireturn
L0:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
invokevirtual android/support/v7/widget/cb/isEnabled()Z
ifeq L3
iload 7
istore 6
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
ifnull L5
aload 0
invokespecial android/support/v7/widget/cb/getThumbOffset()I
istore 6
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v7/widget/cb/C I
istore 8
aload 0
getfield android/support/v7/widget/cb/s I
istore 9
iload 6
aload 0
getfield android/support/v7/widget/cb/B I
iadd
aload 0
getfield android/support/v7/widget/cb/s I
isub
istore 10
aload 0
getfield android/support/v7/widget/cb/A I
istore 11
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
istore 12
aload 0
getfield android/support/v7/widget/cb/L Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
istore 13
aload 0
getfield android/support/v7/widget/cb/s I
istore 14
aload 0
getfield android/support/v7/widget/cb/E I
istore 15
aload 0
getfield android/support/v7/widget/cb/s I
istore 16
iload 7
istore 6
fload 2
iload 10
i2f
fcmpl
ifle L5
iload 7
istore 6
fload 2
iload 11
iload 10
iadd
iload 12
iadd
iload 13
iadd
iload 14
iadd
i2f
fcmpg
ifge L5
iload 7
istore 6
fload 3
iload 8
iload 9
isub
i2f
fcmpl
ifle L5
iload 7
istore 6
fload 3
iload 15
iload 16
iadd
i2f
fcmpg
ifge L5
iconst_1
istore 6
L5:
iload 6
ifeq L3
aload 0
iconst_1
putfield android/support/v7/widget/cb/r I
aload 0
fload 2
putfield android/support/v7/widget/cb/t F
aload 0
fload 3
putfield android/support/v7/widget/cb/u F
goto L3
L2:
aload 0
getfield android/support/v7/widget/cb/r I
tableswitch 0
L3
L6
L7
default : L8
L8:
goto L3
L6:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
fload 2
aload 0
getfield android/support/v7/widget/cb/t F
fsub
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v7/widget/cb/s I
i2f
fcmpl
ifgt L9
fload 3
aload 0
getfield android/support/v7/widget/cb/u F
fsub
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v7/widget/cb/s I
i2f
fcmpl
ifle L3
L9:
aload 0
iconst_2
putfield android/support/v7/widget/cb/r I
aload 0
invokevirtual android/support/v7/widget/cb/getParent()Landroid/view/ViewParent;
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
aload 0
fload 2
putfield android/support/v7/widget/cb/t F
aload 0
fload 3
putfield android/support/v7/widget/cb/u F
iconst_1
ireturn
L7:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 5
aload 0
invokespecial android/support/v7/widget/cb/getThumbScrollRange()I
istore 6
fload 5
aload 0
getfield android/support/v7/widget/cb/t F
fsub
fstore 2
iload 6
ifeq L10
fload 2
iload 6
i2f
fdiv
fstore 2
L11:
fload 2
fstore 3
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L12
fload 2
fneg
fstore 3
L12:
fload 3
aload 0
getfield android/support/v7/widget/cb/x F
fadd
fstore 3
fload 3
fconst_0
fcmpg
ifge L13
fconst_0
fstore 2
L14:
fload 2
aload 0
getfield android/support/v7/widget/cb/x F
fcmpl
ifeq L4
aload 0
fload 5
putfield android/support/v7/widget/cb/t F
aload 0
fload 2
invokespecial android/support/v7/widget/cb/setThumbPosition(F)V
iconst_1
ireturn
L10:
fload 2
fconst_0
fcmpl
ifle L15
fconst_1
fstore 2
goto L11
L15:
ldc_w -1.0F
fstore 2
goto L11
L13:
fload 4
fstore 2
fload 3
fconst_1
fcmpl
ifgt L14
fload 3
fstore 2
goto L14
L1:
aload 0
getfield android/support/v7/widget/cb/r I
iconst_2
if_icmpne L16
aload 0
iconst_0
putfield android/support/v7/widget/cb/r I
aload 1
invokevirtual android/view/MotionEvent/getAction()I
iconst_1
if_icmpne L17
aload 0
invokevirtual android/support/v7/widget/cb/isEnabled()Z
ifeq L17
iconst_1
istore 6
L18:
aload 0
invokevirtual android/support/v7/widget/cb/isChecked()Z
istore 18
iload 6
ifeq L19
aload 0
getfield android/support/v7/widget/cb/v Landroid/view/VelocityTracker;
sipush 1000
invokevirtual android/view/VelocityTracker/computeCurrentVelocity(I)V
aload 0
getfield android/support/v7/widget/cb/v Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/getXVelocity()F
fstore 2
fload 2
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v7/widget/cb/w I
i2f
fcmpl
ifle L20
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L21
fload 2
fconst_0
fcmpg
ifge L22
iconst_1
istore 17
L23:
iload 17
iload 18
if_icmpeq L24
aload 0
iconst_0
invokevirtual android/support/v7/widget/cb/playSoundEffect(I)V
aload 0
iload 17
invokevirtual android/support/v7/widget/cb/setChecked(Z)V
L24:
aload 1
invokestatic android/view/MotionEvent/obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
astore 19
aload 19
iconst_3
invokevirtual android/view/MotionEvent/setAction(I)V
aload 0
aload 19
invokespecial android/widget/CompoundButton/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
aload 19
invokevirtual android/view/MotionEvent/recycle()V
aload 0
aload 1
invokespecial android/widget/CompoundButton/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
iconst_1
ireturn
L17:
iconst_0
istore 6
goto L18
L22:
iconst_0
istore 17
goto L23
L21:
fload 2
fconst_0
fcmpl
ifle L25
iconst_1
istore 17
goto L23
L25:
iconst_0
istore 17
goto L23
L20:
aload 0
invokespecial android/support/v7/widget/cb/getTargetCheckedState()Z
istore 17
goto L23
L19:
iload 18
istore 17
goto L23
L16:
aload 0
iconst_0
putfield android/support/v7/widget/cb/r I
aload 0
getfield android/support/v7/widget/cb/v Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/clear()V
goto L3
.limit locals 20
.limit stack 3
.end method

.method public final setChecked(Z)V
fconst_1
fstore 2
aload 0
iload 1
invokespecial android/widget/CompoundButton/setChecked(Z)V
aload 0
invokevirtual android/support/v7/widget/cb/isChecked()Z
istore 1
aload 0
invokevirtual android/support/v7/widget/cb/getWindowToken()Landroid/os/IBinder;
ifnull L0
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L0
aload 0
getfield android/support/v7/widget/cb/x F
fstore 3
iload 1
ifeq L1
L2:
aload 0
new android/support/v7/widget/cd
dup
aload 0
fload 3
fload 2
iconst_0
invokespecial android/support/v7/widget/cd/<init>(Landroid/support/v7/widget/cb;FFB)V
putfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
ldc2_w 250L
invokevirtual android/support/v7/widget/cd/setDuration(J)V
aload 0
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
invokevirtual android/support/v7/widget/cb/startAnimation(Landroid/view/animation/Animation;)V
return
L1:
fconst_0
fstore 2
goto L2
L0:
aload 0
getfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
ifnull L3
aload 0
invokevirtual android/support/v7/widget/cb/clearAnimation()V
aload 0
aconst_null
putfield android/support/v7/widget/cb/K Landroid/support/v7/widget/cd;
L3:
iload 1
ifeq L4
L5:
aload 0
fload 2
invokespecial android/support/v7/widget/cb/setThumbPosition(F)V
return
L4:
fconst_0
fstore 2
goto L5
.limit locals 4
.limit stack 7
.end method

.method public final setShowText(Z)V
aload 0
getfield android/support/v7/widget/cb/q Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/widget/cb/q Z
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setSplitTrack(Z)V
aload 0
iload 1
putfield android/support/v7/widget/cb/n Z
aload 0
invokevirtual android/support/v7/widget/cb/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setSwitchMinWidth(I)V
aload 0
iload 1
putfield android/support/v7/widget/cb/l I
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setSwitchPadding(I)V
aload 0
iload 1
putfield android/support/v7/widget/cb/m I
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setSwitchTypeface(Landroid/graphics/Typeface;)V
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/getTypeface()Landroid/graphics/Typeface;
aload 1
if_acmpeq L0
aload 0
getfield android/support/v7/widget/cb/F Landroid/text/TextPaint;
aload 1
invokevirtual android/text/TextPaint/setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
pop
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
aload 0
invokevirtual android/support/v7/widget/cb/invalidate()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setTextOff(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/widget/cb/p Ljava/lang/CharSequence;
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTextOn(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/widget/cb/o Ljava/lang/CharSequence;
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setThumbDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setThumbResource(I)V
aload 0
aload 0
getfield android/support/v7/widget/cb/M Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/cb/setThumbDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final setThumbTextPadding(I)V
aload 0
iload 1
putfield android/support/v7/widget/cb/k I
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTrackDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v7/widget/cb/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTrackResource(I)V
aload 0
aload 0
getfield android/support/v7/widget/cb/M Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/cb/setTrackDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final toggle()V
aload 0
invokevirtual android/support/v7/widget/cb/isChecked()Z
ifne L0
iconst_1
istore 1
L1:
aload 0
iload 1
invokevirtual android/support/v7/widget/cb/setChecked(Z)V
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method protected final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
aload 0
aload 1
invokespecial android/widget/CompoundButton/verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
ifne L0
aload 1
aload 0
getfield android/support/v7/widget/cb/i Landroid/graphics/drawable/Drawable;
if_acmpeq L0
aload 1
aload 0
getfield android/support/v7/widget/cb/j Landroid/graphics/drawable/Drawable;
if_acmpne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method
