.bytecode 50.0
.class public final synchronized android/support/v7/widget/SearchView
.super android/support/v7/widget/aj
.implements android/support/v7/c/c

.field static final 'a' Landroid/support/v7/widget/bq;

.field private static final 'c' Z = 0


.field private static final 'd' Ljava/lang/String; = "SearchView"

.field private static final 'e' Z

.field private static final 'f' Ljava/lang/String; = "nm"

.field private final 'A' Landroid/content/Intent;

.field private final 'B' Ljava/lang/CharSequence;

.field private 'C' Landroid/support/v7/widget/bs;

.field private 'D' Landroid/support/v7/widget/br;

.field private 'E' Landroid/view/View$OnFocusChangeListener;

.field private 'F' Landroid/support/v7/widget/bt;

.field private 'G' Landroid/view/View$OnClickListener;

.field private 'H' Z

.field private 'I' Z

.field private 'J' Landroid/support/v4/widget/r;

.field private 'K' Z

.field private 'L' Ljava/lang/CharSequence;

.field private 'M' Z

.field private 'N' Z

.field private 'O' I

.field private 'P' Z

.field private 'Q' Ljava/lang/CharSequence;

.field private 'R' Ljava/lang/CharSequence;

.field private 'S' Z

.field private 'T' I

.field private 'U' Landroid/app/SearchableInfo;

.field private 'V' Landroid/os/Bundle;

.field private final 'W' Landroid/support/v7/internal/widget/av;

.field private 'aa' Ljava/lang/Runnable;

.field private final 'ab' Ljava/lang/Runnable;

.field private 'ac' Ljava/lang/Runnable;

.field private final 'ad' Ljava/util/WeakHashMap;

.field private final 'ae' Landroid/view/View$OnClickListener;

.field private final 'af' Landroid/widget/TextView$OnEditorActionListener;

.field private final 'ag' Landroid/widget/AdapterView$OnItemClickListener;

.field private final 'ah' Landroid/widget/AdapterView$OnItemSelectedListener;

.field private 'ai' Landroid/text/TextWatcher;

.field 'b' Landroid/view/View$OnKeyListener;

.field private final 'g' Landroid/support/v7/widget/SearchView$SearchAutoComplete;

.field private final 'n' Landroid/view/View;

.field private final 'o' Landroid/view/View;

.field private final 'p' Landroid/view/View;

.field private final 'q' Landroid/widget/ImageView;

.field private final 'r' Landroid/widget/ImageView;

.field private final 's' Landroid/widget/ImageView;

.field private final 't' Landroid/widget/ImageView;

.field private final 'u' Landroid/view/View;

.field private final 'v' Landroid/widget/ImageView;

.field private final 'w' Landroid/graphics/drawable/Drawable;

.field private final 'x' I

.field private final 'y' I

.field private final 'z' Landroid/content/Intent;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L0
iconst_1
istore 0
L1:
iload 0
putstatic android/support/v7/widget/SearchView/e Z
new android/support/v7/widget/bq
dup
invokespecial android/support/v7/widget/bq/<init>()V
putstatic android/support/v7/widget/SearchView/a Landroid/support/v7/widget/bq;
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v7/widget/SearchView/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
getstatic android/support/v7/a/d/searchViewStyle I
invokespecial android/support/v7/widget/SearchView/<init>(Landroid/content/Context;I)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;I)V
aload 0
aload 1
aconst_null
iload 2
invokespecial android/support/v7/widget/aj/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/support/v7/widget/be
dup
aload 0
invokespecial android/support/v7/widget/be/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/aa Ljava/lang/Runnable;
aload 0
new android/support/v7/widget/bi
dup
aload 0
invokespecial android/support/v7/widget/bi/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/ab Ljava/lang/Runnable;
aload 0
new android/support/v7/widget/bj
dup
aload 0
invokespecial android/support/v7/widget/bj/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/ac Ljava/lang/Runnable;
aload 0
new java/util/WeakHashMap
dup
invokespecial java/util/WeakHashMap/<init>()V
putfield android/support/v7/widget/SearchView/ad Ljava/util/WeakHashMap;
aload 0
new android/support/v7/widget/bn
dup
aload 0
invokespecial android/support/v7/widget/bn/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/ae Landroid/view/View$OnClickListener;
aload 0
new android/support/v7/widget/bo
dup
aload 0
invokespecial android/support/v7/widget/bo/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/b Landroid/view/View$OnKeyListener;
aload 0
new android/support/v7/widget/bp
dup
aload 0
invokespecial android/support/v7/widget/bp/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/af Landroid/widget/TextView$OnEditorActionListener;
aload 0
new android/support/v7/widget/bf
dup
aload 0
invokespecial android/support/v7/widget/bf/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/ag Landroid/widget/AdapterView$OnItemClickListener;
aload 0
new android/support/v7/widget/bg
dup
aload 0
invokespecial android/support/v7/widget/bg/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/ah Landroid/widget/AdapterView$OnItemSelectedListener;
aload 0
new android/support/v7/widget/bh
dup
aload 0
invokespecial android/support/v7/widget/bh/<init>(Landroid/support/v7/widget/SearchView;)V
putfield android/support/v7/widget/SearchView/ai Landroid/text/TextWatcher;
aload 1
aconst_null
getstatic android/support/v7/a/n/SearchView [I
iload 2
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 3
aload 0
aload 3
invokevirtual android/support/v7/internal/widget/ax/a()Landroid/support/v7/internal/widget/av;
putfield android/support/v7/widget/SearchView/W Landroid/support/v7/internal/widget/av;
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
aload 3
getstatic android/support/v7/a/n/SearchView_layout I
getstatic android/support/v7/a/k/abc_search_view I
invokevirtual android/support/v7/internal/widget/ax/e(II)I
aload 0
iconst_1
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
pop
aload 0
aload 0
getstatic android/support/v7/a/i/search_src_text I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
checkcast android/support/v7/widget/SearchView$SearchAutoComplete
putfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setSearchView(Landroid/support/v7/widget/SearchView;)V
aload 0
aload 0
getstatic android/support/v7/a/i/search_edit_frame I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
putfield android/support/v7/widget/SearchView/n Landroid/view/View;
aload 0
aload 0
getstatic android/support/v7/a/i/search_plate I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
putfield android/support/v7/widget/SearchView/o Landroid/view/View;
aload 0
aload 0
getstatic android/support/v7/a/i/submit_area I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
putfield android/support/v7/widget/SearchView/p Landroid/view/View;
aload 0
aload 0
getstatic android/support/v7/a/i/search_button I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/widget/SearchView/q Landroid/widget/ImageView;
aload 0
aload 0
getstatic android/support/v7/a/i/search_go_btn I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/widget/SearchView/r Landroid/widget/ImageView;
aload 0
aload 0
getstatic android/support/v7/a/i/search_close_btn I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/widget/SearchView/s Landroid/widget/ImageView;
aload 0
aload 0
getstatic android/support/v7/a/i/search_voice_btn I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/widget/SearchView/t Landroid/widget/ImageView;
aload 0
aload 0
getstatic android/support/v7/a/i/search_mag_icon I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/widget/SearchView/v Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/o Landroid/view/View;
aload 3
getstatic android/support/v7/a/n/SearchView_queryBackground I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/view/View/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/SearchView/p Landroid/view/View;
aload 3
getstatic android/support/v7/a/n/SearchView_submitBackground I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/view/View/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/SearchView/q Landroid/widget/ImageView;
aload 3
getstatic android/support/v7/a/n/SearchView_searchIcon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/SearchView/r Landroid/widget/ImageView;
aload 3
getstatic android/support/v7/a/n/SearchView_goIcon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/SearchView/s Landroid/widget/ImageView;
aload 3
getstatic android/support/v7/a/n/SearchView_closeIcon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/SearchView/t Landroid/widget/ImageView;
aload 3
getstatic android/support/v7/a/n/SearchView_voiceIcon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/SearchView/v Landroid/widget/ImageView;
aload 3
getstatic android/support/v7/a/n/SearchView_searchIcon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 3
getstatic android/support/v7/a/n/SearchView_searchHintIcon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/widget/SearchView/w Landroid/graphics/drawable/Drawable;
aload 0
aload 3
getstatic android/support/v7/a/n/SearchView_suggestionRowLayout I
getstatic android/support/v7/a/k/abc_search_dropdown_item_icons_2line I
invokevirtual android/support/v7/internal/widget/ax/e(II)I
putfield android/support/v7/widget/SearchView/x I
aload 0
aload 3
getstatic android/support/v7/a/n/SearchView_commitIcon I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
putfield android/support/v7/widget/SearchView/y I
aload 0
getfield android/support/v7/widget/SearchView/q Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/ae Landroid/view/View$OnClickListener;
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield android/support/v7/widget/SearchView/s Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/ae Landroid/view/View$OnClickListener;
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield android/support/v7/widget/SearchView/r Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/ae Landroid/view/View$OnClickListener;
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield android/support/v7/widget/SearchView/t Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/ae Landroid/view/View$OnClickListener;
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/ae Landroid/view/View$OnClickListener;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/ai Landroid/text/TextWatcher;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/addTextChangedListener(Landroid/text/TextWatcher;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/af Landroid/widget/TextView$OnEditorActionListener;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/ag Landroid/widget/AdapterView$OnItemClickListener;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/ah Landroid/widget/AdapterView$OnItemSelectedListener;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/b Landroid/view/View$OnKeyListener;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setOnKeyListener(Landroid/view/View$OnKeyListener;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
new android/support/v7/widget/bk
dup
aload 0
invokespecial android/support/v7/widget/bk/<init>(Landroid/support/v7/widget/SearchView;)V
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
aload 0
aload 3
getstatic android/support/v7/a/n/SearchView_iconifiedByDefault I
iconst_1
invokevirtual android/support/v7/internal/widget/ax/a(IZ)Z
invokevirtual android/support/v7/widget/SearchView/setIconifiedByDefault(Z)V
aload 3
getstatic android/support/v7/a/n/SearchView_android_maxWidth I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/c(II)I
istore 2
iload 2
iconst_m1
if_icmpeq L0
aload 0
iload 2
invokevirtual android/support/v7/widget/SearchView/setMaxWidth(I)V
L0:
aload 0
aload 3
getstatic android/support/v7/a/n/SearchView_defaultQueryHint I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
putfield android/support/v7/widget/SearchView/B Ljava/lang/CharSequence;
aload 0
aload 3
getstatic android/support/v7/a/n/SearchView_queryHint I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
putfield android/support/v7/widget/SearchView/L Ljava/lang/CharSequence;
aload 3
getstatic android/support/v7/a/n/SearchView_android_imeOptions I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/a(II)I
istore 2
iload 2
iconst_m1
if_icmpeq L1
aload 0
iload 2
invokevirtual android/support/v7/widget/SearchView/setImeOptions(I)V
L1:
aload 3
getstatic android/support/v7/a/n/SearchView_android_inputType I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/a(II)I
istore 2
iload 2
iconst_m1
if_icmpeq L2
aload 0
iload 2
invokevirtual android/support/v7/widget/SearchView/setInputType(I)V
L2:
aload 0
aload 3
getstatic android/support/v7/a/n/SearchView_android_focusable I
iconst_1
invokevirtual android/support/v7/internal/widget/ax/a(IZ)Z
invokevirtual android/support/v7/widget/SearchView/setFocusable(Z)V
aload 3
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
new android/content/Intent
dup
ldc "android.speech.action.WEB_SEARCH"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
putfield android/support/v7/widget/SearchView/z Landroid/content/Intent;
aload 0
getfield android/support/v7/widget/SearchView/z Landroid/content/Intent;
ldc_w 268435456
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 0
getfield android/support/v7/widget/SearchView/z Landroid/content/Intent;
ldc "android.speech.extra.LANGUAGE_MODEL"
ldc "web_search"
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
new android/content/Intent
dup
ldc "android.speech.action.RECOGNIZE_SPEECH"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
putfield android/support/v7/widget/SearchView/A Landroid/content/Intent;
aload 0
getfield android/support/v7/widget/SearchView/A Landroid/content/Intent;
ldc_w 268435456
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 0
aload 0
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getDropDownAnchor()I
invokevirtual android/support/v7/widget/SearchView/findViewById(I)Landroid/view/View;
putfield android/support/v7/widget/SearchView/u Landroid/view/View;
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
ifnull L3
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L4
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
new android/support/v7/widget/bl
dup
aload 0
invokespecial android/support/v7/widget/bl/<init>(Landroid/support/v7/widget/SearchView;)V
invokevirtual android/view/View/addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
L3:
aload 0
aload 0
getfield android/support/v7/widget/SearchView/H Z
invokespecial android/support/v7/widget/SearchView/a(Z)V
aload 0
invokespecial android/support/v7/widget/SearchView/q()V
return
L4:
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v7/widget/bm
dup
aload 0
invokespecial android/support/v7/widget/bm/<init>(Landroid/support/v7/widget/SearchView;)V
invokevirtual android/view/ViewTreeObserver/addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
goto L3
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/content/Intent;Landroid/app/SearchableInfo;)Landroid/content/Intent;
.annotation invisible Landroid/annotation/TargetApi;
value I = 8
.end annotation
new android/content/Intent
dup
aload 0
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 2
aload 1
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 0
aload 0
ifnonnull L0
aconst_null
astore 0
L1:
aload 2
ldc "calling_package"
aload 0
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 2
areturn
L0:
aload 0
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 0
goto L1
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/database/Cursor;)Landroid/content/Intent;
.catch java/lang/RuntimeException from L0 to L1 using L2
.catch java/lang/RuntimeException from L3 to L4 using L2
.catch java/lang/RuntimeException from L5 to L6 using L2
.catch java/lang/RuntimeException from L7 to L8 using L2
.catch java/lang/RuntimeException from L9 to L10 using L2
.catch java/lang/RuntimeException from L11 to L12 using L2
.catch java/lang/RuntimeException from L13 to L14 using L2
.catch java/lang/RuntimeException from L15 to L16 using L2
.catch java/lang/RuntimeException from L16 to L17 using L2
.catch java/lang/RuntimeException from L18 to L19 using L20
L0:
aload 1
ldc "suggest_intent_action"
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
astore 4
L1:
aload 4
astore 3
aload 4
ifnonnull L21
aload 4
astore 3
L3:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L21
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSuggestIntentAction()Ljava/lang/String;
astore 3
L4:
goto L21
L5:
aload 1
ldc "suggest_intent_data"
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
astore 5
L6:
aload 5
astore 3
L7:
getstatic android/support/v7/widget/SearchView/e Z
ifeq L10
L8:
aload 5
astore 3
aload 5
ifnonnull L10
L9:
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSuggestIntentData()Ljava/lang/String;
astore 3
L10:
aload 3
astore 5
aload 3
ifnull L22
L11:
aload 1
ldc "suggest_intent_data_id"
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
astore 6
L12:
aload 3
astore 5
aload 6
ifnull L22
L13:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic android/net/Uri/encode(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
L14:
goto L22
L15:
aload 1
ldc "suggest_intent_query"
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 0
aload 4
aload 3
aload 1
ldc "suggest_intent_extra_data"
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
aload 5
invokespecial android/support/v7/widget/SearchView/a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
areturn
L16:
aload 5
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
astore 3
L17:
goto L15
L2:
astore 3
L18:
aload 1
invokeinterface android/database/Cursor/getPosition()I 0
istore 2
L19:
ldc "SearchView"
new java/lang/StringBuilder
dup
ldc "Search suggestions cursor at row "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " returned exception."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
L20:
astore 1
iconst_m1
istore 2
goto L19
L23:
aload 3
astore 4
goto L5
L21:
aload 3
ifnonnull L23
ldc "android.intent.action.SEARCH"
astore 4
goto L5
L22:
aload 5
ifnonnull L16
aconst_null
astore 3
goto L15
.limit locals 7
.limit stack 5
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
new android/content/Intent
dup
aload 1
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 1
aload 1
ldc_w 268435456
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 2
ifnull L0
aload 1
aload 2
invokevirtual android/content/Intent/setData(Landroid/net/Uri;)Landroid/content/Intent;
pop
L0:
aload 1
ldc "user_query"
aload 0
getfield android/support/v7/widget/SearchView/R Ljava/lang/CharSequence;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;
pop
aload 4
ifnull L1
aload 1
ldc "query"
aload 4
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
L1:
aload 3
ifnull L2
aload 1
ldc "intent_extra_data_key"
aload 3
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
L2:
aload 0
getfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
ifnull L3
aload 1
ldc "app_data"
aload 0
getfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
pop
L3:
getstatic android/support/v7/widget/SearchView/e Z
ifeq L4
aload 1
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
L4:
aload 1
areturn
.limit locals 5
.limit stack 3
.end method

.method private a(Landroid/content/Intent;)V
.catch java/lang/RuntimeException from L0 to L1 using L2
aload 1
ifnonnull L0
return
L0:
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L1:
return
L2:
astore 2
ldc "SearchView"
new java/lang/StringBuilder
dup
ldc "Failed launch activity: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/hasFocus()Z
ifeq L0
getstatic android/support/v7/widget/SearchView/FOCUSED_STATE_SET [I
astore 1
L1:
aload 0
getfield android/support/v7/widget/SearchView/o Landroid/view/View;
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L2
aload 2
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L2:
aload 0
getfield android/support/v7/widget/SearchView/p Landroid/view/View;
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L3
aload 2
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L3:
aload 0
invokevirtual android/support/v7/widget/SearchView/invalidate()V
return
L0:
getstatic android/support/v7/widget/SearchView/EMPTY_STATE_SET [I
astore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;Ljava/lang/CharSequence;)V
iconst_1
istore 3
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
astore 4
aload 0
aload 4
putfield android/support/v7/widget/SearchView/R Ljava/lang/CharSequence;
aload 4
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
iconst_1
istore 2
L1:
aload 0
iload 2
invokespecial android/support/v7/widget/SearchView/b(Z)V
iload 2
ifne L2
iload 3
istore 2
L3:
aload 0
iload 2
invokespecial android/support/v7/widget/SearchView/c(Z)V
aload 0
invokespecial android/support/v7/widget/SearchView/n()V
aload 0
invokespecial android/support/v7/widget/SearchView/m()V
aload 0
getfield android/support/v7/widget/SearchView/C Landroid/support/v7/widget/bs;
ifnull L4
aload 1
aload 0
getfield android/support/v7/widget/SearchView/Q Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
ifne L4
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
pop
L4:
aload 0
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
putfield android/support/v7/widget/SearchView/Q Ljava/lang/CharSequence;
return
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 5
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;Ljava/lang/String;)V
aload 0
aload 1
invokespecial android/support/v7/widget/SearchView/a(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;)V
aload 0
aload 1
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)V
aload 0
ldc "android.intent.action.SEARCH"
aconst_null
aconst_null
aload 1
invokespecial android/support/v7/widget/SearchView/a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
astore 1
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
return
.limit locals 2
.limit stack 5
.end method

.method private a(Z)V
iconst_1
istore 5
bipush 8
istore 3
aload 0
iload 1
putfield android/support/v7/widget/SearchView/I Z
iload 1
ifeq L0
iconst_0
istore 2
L1:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L2
iconst_1
istore 4
L3:
aload 0
getfield android/support/v7/widget/SearchView/q Landroid/widget/ImageView;
iload 2
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 0
iload 4
invokespecial android/support/v7/widget/SearchView/b(Z)V
aload 0
getfield android/support/v7/widget/SearchView/n Landroid/view/View;
astore 6
iload 1
ifeq L4
bipush 8
istore 2
L5:
aload 6
iload 2
invokevirtual android/view/View/setVisibility(I)V
aload 0
getfield android/support/v7/widget/SearchView/v Landroid/widget/ImageView;
astore 6
aload 0
getfield android/support/v7/widget/SearchView/H Z
ifeq L6
iload 3
istore 2
L7:
aload 6
iload 2
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 0
invokespecial android/support/v7/widget/SearchView/n()V
iload 4
ifne L8
iload 5
istore 1
L9:
aload 0
iload 1
invokespecial android/support/v7/widget/SearchView/c(Z)V
aload 0
invokespecial android/support/v7/widget/SearchView/m()V
return
L0:
bipush 8
istore 2
goto L1
L2:
iconst_0
istore 4
goto L3
L4:
iconst_0
istore 2
goto L5
L6:
iconst_0
istore 2
goto L7
L8:
iconst_0
istore 1
goto L9
.limit locals 7
.limit stack 2
.end method

.method private a(I)Z
.catch java/lang/RuntimeException from L0 to L1 using L2
iconst_0
istore 2
aload 0
getfield android/support/v7/widget/SearchView/F Landroid/support/v7/widget/bt;
ifnull L3
aload 0
getfield android/support/v7/widget/SearchView/F Landroid/support/v7/widget/bt;
invokeinterface android/support/v7/widget/bt/b()Z 0
ifne L4
L3:
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
astore 3
aload 3
ifnull L1
aload 3
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
ifeq L1
aload 0
aload 3
invokespecial android/support/v7/widget/SearchView/a(Landroid/database/Cursor;)Landroid/content/Intent;
astore 3
aload 3
ifnull L1
L0:
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 3
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L1:
aload 0
iconst_0
invokespecial android/support/v7/widget/SearchView/setImeVisibility(Z)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/dismissDropDown()V
iconst_1
istore 2
L4:
iload 2
ireturn
L2:
astore 4
ldc "SearchView"
new java/lang/StringBuilder
dup
ldc "Failed launch activity: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 4
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
.limit locals 5
.limit stack 4
.end method

.method private a(ILandroid/view/KeyEvent;)Z
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnonnull L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
ifnull L1
aload 2
invokevirtual android/view/KeyEvent/getAction()I
ifne L1
aload 2
invokestatic android/support/v4/view/ab/b(Landroid/view/KeyEvent;)Z
ifeq L1
iload 1
bipush 66
if_icmpeq L2
iload 1
bipush 84
if_icmpeq L2
iload 1
bipush 61
if_icmpne L3
L2:
aload 0
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getListSelection()I
invokespecial android/support/v7/widget/SearchView/a(I)Z
ireturn
L3:
iload 1
bipush 21
if_icmpeq L4
iload 1
bipush 22
if_icmpne L5
L4:
iload 1
bipush 21
if_icmpne L6
iconst_0
istore 1
L7:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 1
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setSelection(I)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iconst_0
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setListSelection(I)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/clearListSelection()V
getstatic android/support/v7/widget/SearchView/a Landroid/support/v7/widget/bq;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/bq/a(Landroid/widget/AutoCompleteTextView;)V
iconst_1
ireturn
L6:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/length()I
istore 1
goto L7
L5:
iload 1
bipush 19
if_icmpne L1
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getListSelection()I
ifne L1
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method static a(Landroid/content/Context;)Z
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
getfield android/content/res/Configuration/orientation I
iconst_2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;I)Z
aload 0
iload 1
invokespecial android/support/v7/widget/SearchView/a(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/widget/SearchView;ILandroid/view/KeyEvent;)Z
aload 0
iload 1
aload 2
invokespecial android/support/v7/widget/SearchView/a(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method private b(Landroid/content/Intent;Landroid/app/SearchableInfo;)Landroid/content/Intent;
.annotation invisible Landroid/annotation/TargetApi;
value I = 8
.end annotation
aconst_null
astore 6
aload 2
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 9
new android/content/Intent
dup
ldc "android.intent.action.SEARCH"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 4
aload 4
aload 9
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
iconst_0
aload 4
ldc_w 1073741824
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 7
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 8
aload 0
getfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
ifnull L0
aload 8
ldc "app_data"
aload 0
getfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L0:
new android/content/Intent
dup
aload 1
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 10
ldc "free_form"
astore 1
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L1
aload 0
invokevirtual android/support/v7/widget/SearchView/getResources()Landroid/content/res/Resources;
astore 5
aload 2
invokevirtual android/app/SearchableInfo/getVoiceLanguageModeId()I
ifeq L2
aload 5
aload 2
invokevirtual android/app/SearchableInfo/getVoiceLanguageModeId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 1
L2:
aload 2
invokevirtual android/app/SearchableInfo/getVoicePromptTextId()I
ifeq L3
aload 5
aload 2
invokevirtual android/app/SearchableInfo/getVoicePromptTextId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 4
L4:
aload 2
invokevirtual android/app/SearchableInfo/getVoiceLanguageId()I
ifeq L5
aload 5
aload 2
invokevirtual android/app/SearchableInfo/getVoiceLanguageId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 5
L6:
aload 2
invokevirtual android/app/SearchableInfo/getVoiceMaxResults()I
ifeq L7
aload 2
invokevirtual android/app/SearchableInfo/getVoiceMaxResults()I
istore 3
L8:
aload 10
ldc "android.speech.extra.LANGUAGE_MODEL"
aload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 10
ldc "android.speech.extra.PROMPT"
aload 4
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 10
ldc "android.speech.extra.LANGUAGE"
aload 5
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 10
ldc "android.speech.extra.MAX_RESULTS"
iload 3
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;I)Landroid/content/Intent;
pop
aload 9
ifnonnull L9
aload 6
astore 1
L10:
aload 10
ldc "calling_package"
aload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 10
ldc "android.speech.extra.RESULTS_PENDINGINTENT"
aload 7
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 10
ldc "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"
aload 8
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
pop
aload 10
areturn
L9:
aload 9
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 1
goto L10
L7:
iconst_1
istore 3
goto L8
L5:
aconst_null
astore 5
goto L6
L3:
aconst_null
astore 4
goto L4
L1:
aconst_null
astore 5
aconst_null
astore 4
ldc "free_form"
astore 1
iconst_1
istore 3
goto L8
.limit locals 11
.limit stack 4
.end method

.method static synthetic b(Landroid/support/v7/widget/SearchView;)Landroid/support/v4/widget/r;
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/SearchView/H Z
ifeq L0
aload 0
getfield android/support/v7/widget/SearchView/w Landroid/graphics/drawable/Drawable;
ifnonnull L1
L0:
aload 1
areturn
L1:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getTextSize()F
f2d
ldc2_w 1.25D
dmul
d2i
istore 2
aload 0
getfield android/support/v7/widget/SearchView/w Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
iload 2
iload 2
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
new android/text/SpannableStringBuilder
dup
ldc "   "
invokespecial android/text/SpannableStringBuilder/<init>(Ljava/lang/CharSequence;)V
astore 3
aload 3
new android/text/style/ImageSpan
dup
aload 0
getfield android/support/v7/widget/SearchView/w Landroid/graphics/drawable/Drawable;
invokespecial android/text/style/ImageSpan/<init>(Landroid/graphics/drawable/Drawable;)V
iconst_1
iconst_2
bipush 33
invokevirtual android/text/SpannableStringBuilder/setSpan(Ljava/lang/Object;III)V
aload 3
aload 1
invokevirtual android/text/SpannableStringBuilder/append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
pop
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private b(Z)V
bipush 8
istore 3
iload 3
istore 2
aload 0
getfield android/support/v7/widget/SearchView/K Z
ifeq L0
iload 3
istore 2
aload 0
invokespecial android/support/v7/widget/SearchView/l()Z
ifeq L0
iload 3
istore 2
aload 0
invokevirtual android/support/v7/widget/SearchView/hasFocus()Z
ifeq L0
iload 1
ifne L1
iload 3
istore 2
aload 0
getfield android/support/v7/widget/SearchView/P Z
ifne L0
L1:
iconst_0
istore 2
L0:
aload 0
getfield android/support/v7/widget/SearchView/r Landroid/widget/ImageView;
iload 2
invokevirtual android/widget/ImageView/setVisibility(I)V
return
.limit locals 4
.limit stack 2
.end method

.method private b(I)Z
aload 0
getfield android/support/v7/widget/SearchView/F Landroid/support/v7/widget/bt;
ifnull L0
aload 0
getfield android/support/v7/widget/SearchView/F Landroid/support/v7/widget/bt;
invokeinterface android/support/v7/widget/bt/a()Z 0
ifne L1
L0:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
astore 2
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
astore 3
aload 3
ifnull L2
aload 3
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
ifeq L3
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
aload 3
invokevirtual android/support/v4/widget/r/c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
astore 3
aload 3
ifnull L4
aload 0
aload 3
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
L2:
iconst_1
ireturn
L4:
aload 0
aload 2
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
goto L2
L3:
aload 0
aload 2
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
goto L2
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method static synthetic b(Landroid/support/v7/widget/SearchView;I)Z
aload 0
getfield android/support/v7/widget/SearchView/F Landroid/support/v7/widget/bt;
ifnull L0
aload 0
getfield android/support/v7/widget/SearchView/F Landroid/support/v7/widget/bt;
invokeinterface android/support/v7/widget/bt/a()Z 0
ifne L1
L0:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
astore 2
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
astore 3
aload 3
ifnull L2
aload 3
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
ifeq L3
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
aload 3
invokevirtual android/support/v4/widget/r/c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
astore 3
aload 3
ifnull L4
aload 0
aload 3
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
L2:
iconst_1
ireturn
L4:
aload 0
aload 2
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
goto L2
L3:
aload 0
aload 2
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
goto L2
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method static synthetic c(Landroid/support/v7/widget/SearchView;)Landroid/view/View$OnFocusChangeListener;
aload 0
getfield android/support/v7/widget/SearchView/E Landroid/view/View$OnFocusChangeListener;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(I)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
astore 2
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
astore 3
aload 3
ifnonnull L0
return
L0:
aload 3
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
ifeq L1
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
aload 3
invokevirtual android/support/v4/widget/r/c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
astore 3
aload 3
ifnull L2
aload 0
aload 3
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
return
L2:
aload 0
aload 2
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
return
L1:
aload 0
aload 2
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
return
.limit locals 4
.limit stack 2
.end method

.method private c(Ljava/lang/CharSequence;)V
iconst_1
istore 3
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
astore 4
aload 0
aload 4
putfield android/support/v7/widget/SearchView/R Ljava/lang/CharSequence;
aload 4
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
iconst_1
istore 2
L1:
aload 0
iload 2
invokespecial android/support/v7/widget/SearchView/b(Z)V
iload 2
ifne L2
iload 3
istore 2
L3:
aload 0
iload 2
invokespecial android/support/v7/widget/SearchView/c(Z)V
aload 0
invokespecial android/support/v7/widget/SearchView/n()V
aload 0
invokespecial android/support/v7/widget/SearchView/m()V
aload 0
getfield android/support/v7/widget/SearchView/C Landroid/support/v7/widget/bs;
ifnull L4
aload 1
aload 0
getfield android/support/v7/widget/SearchView/Q Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
ifne L4
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
pop
L4:
aload 0
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
putfield android/support/v7/widget/SearchView/Q Ljava/lang/CharSequence;
return
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 5
.limit stack 2
.end method

.method private c(Z)V
aload 0
getfield android/support/v7/widget/SearchView/P Z
ifeq L0
aload 0
getfield android/support/v7/widget/SearchView/I Z
ifne L0
iload 1
ifeq L0
iconst_0
istore 2
aload 0
getfield android/support/v7/widget/SearchView/r Landroid/widget/ImageView;
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
L1:
aload 0
getfield android/support/v7/widget/SearchView/t Landroid/widget/ImageView;
iload 2
invokevirtual android/widget/ImageView/setVisibility(I)V
return
L0:
bipush 8
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method static synthetic d(Landroid/support/v7/widget/SearchView;)V
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
invokevirtual android/view/View/getWidth()I
iconst_1
if_icmple L0
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 7
aload 0
getfield android/support/v7/widget/SearchView/o Landroid/view/View;
invokevirtual android/view/View/getPaddingLeft()I
istore 3
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 8
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
istore 6
aload 0
getfield android/support/v7/widget/SearchView/H Z
ifeq L1
aload 7
getstatic android/support/v7/a/g/abc_dropdownitem_icon_width I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
istore 1
aload 7
getstatic android/support/v7/a/g/abc_dropdownitem_text_padding_left I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
iload 1
iadd
istore 1
L2:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getDropDownBackground()Landroid/graphics/drawable/Drawable;
aload 8
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
iload 6
ifeq L3
aload 8
getfield android/graphics/Rect/left I
ineg
istore 2
L4:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 2
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setDropDownHorizontalOffset(I)V
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
invokevirtual android/view/View/getWidth()I
istore 2
aload 8
getfield android/graphics/Rect/left I
istore 4
aload 8
getfield android/graphics/Rect/right I
istore 5
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 1
iload 2
iload 4
iadd
iload 5
iadd
iadd
iload 3
isub
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setDropDownWidth(I)V
L0:
return
L1:
iconst_0
istore 1
goto L2
L3:
iload 3
aload 8
getfield android/graphics/Rect/left I
iload 1
iadd
isub
istore 2
goto L4
.limit locals 9
.limit stack 4
.end method

.method private d(I)Z
.catch java/lang/RuntimeException from L0 to L1 using L2
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
astore 2
aload 2
ifnull L3
aload 2
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
ifeq L3
aload 0
aload 2
invokespecial android/support/v7/widget/SearchView/a(Landroid/database/Cursor;)Landroid/content/Intent;
astore 2
aload 2
ifnull L1
L0:
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 2
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L1:
iconst_1
ireturn
L2:
astore 3
ldc "SearchView"
new java/lang/StringBuilder
dup
ldc "Failed launch activity: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L3:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method static synthetic e(Landroid/support/v7/widget/SearchView;)Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/q Landroid/widget/ImageView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()V
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
new android/support/v7/widget/bl
dup
aload 0
invokespecial android/support/v7/widget/bl/<init>(Landroid/support/v7/widget/SearchView;)V
invokevirtual android/view/View/addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
return
.limit locals 1
.limit stack 4
.end method

.method private f()V
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v7/widget/bm
dup
aload 0
invokespecial android/support/v7/widget/bm/<init>(Landroid/support/v7/widget/SearchView;)V
invokevirtual android/view/ViewTreeObserver/addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic f(Landroid/support/v7/widget/SearchView;)V
aload 0
invokespecial android/support/v7/widget/SearchView/v()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Landroid/support/v7/widget/SearchView;)Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/s Landroid/widget/ImageView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
getfield android/support/v7/widget/SearchView/H Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private getPreferredWidth()I
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/g/abc_search_view_preferred_width I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic h(Landroid/support/v7/widget/SearchView;)V
aload 0
invokespecial android/support/v7/widget/SearchView/u()V
return
.limit locals 1
.limit stack 1
.end method

.method private h()Z
aload 0
getfield android/support/v7/widget/SearchView/I Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Landroid/support/v7/widget/SearchView;)Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/r Landroid/widget/ImageView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private i()Z
aload 0
getfield android/support/v7/widget/SearchView/K Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Landroid/support/v7/widget/SearchView;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
astore 1
aload 1
ifnull L0
aload 1
invokestatic android/text/TextUtils/getTrimmedLength(Ljava/lang/CharSequence;)I
ifle L0
aload 0
getfield android/support/v7/widget/SearchView/C Landroid/support/v7/widget/bs;
ifnull L1
aload 0
getfield android/support/v7/widget/SearchView/C Landroid/support/v7/widget/bs;
astore 2
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
pop
aload 2
invokeinterface android/support/v7/widget/bs/a()Z 0
ifne L0
L1:
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnull L2
aload 0
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
invokespecial android/support/v7/widget/SearchView/a(Ljava/lang/String;)V
L2:
aload 0
iconst_0
invokespecial android/support/v7/widget/SearchView/setImeVisibility(Z)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/dismissDropDown()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method private j()Z
aload 0
getfield android/support/v7/widget/SearchView/M Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Landroid/support/v7/widget/SearchView;)Landroid/widget/ImageView;
aload 0
getfield android/support/v7/widget/SearchView/t Landroid/widget/ImageView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private k()Z
.annotation invisible Landroid/annotation/TargetApi;
value I = 8
.end annotation
iconst_0
istore 2
iload 2
istore 1
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnull L0
iload 2
istore 1
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getVoiceSearchEnabled()Z
ifeq L0
aconst_null
astore 3
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getVoiceSearchLaunchWebSearch()Z
ifeq L1
aload 0
getfield android/support/v7/widget/SearchView/z Landroid/content/Intent;
astore 3
L2:
iload 2
istore 1
aload 3
ifnull L0
iload 2
istore 1
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 3
ldc_w 65536
invokevirtual android/content/pm/PackageManager/resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
ifnull L0
iconst_1
istore 1
L0:
iload 1
ireturn
L1:
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getVoiceSearchLaunchRecognizer()Z
ifeq L2
aload 0
getfield android/support/v7/widget/SearchView/A Landroid/content/Intent;
astore 3
goto L2
.limit locals 4
.limit stack 3
.end method

.method static synthetic l(Landroid/support/v7/widget/SearchView;)V
.catch android/content/ActivityNotFoundException from L0 to L1 using L2
.catch android/content/ActivityNotFoundException from L3 to L4 using L2
.catch android/content/ActivityNotFoundException from L5 to L6 using L2
.catch android/content/ActivityNotFoundException from L7 to L8 using L2
.catch android/content/ActivityNotFoundException from L8 to L9 using L2
.catch android/content/ActivityNotFoundException from L10 to L11 using L2
.catch android/content/ActivityNotFoundException from L11 to L12 using L2
.catch android/content/ActivityNotFoundException from L12 to L13 using L2
.catch android/content/ActivityNotFoundException from L14 to L15 using L2
.catch android/content/ActivityNotFoundException from L16 to L17 using L2
.catch android/content/ActivityNotFoundException from L18 to L19 using L2
.catch android/content/ActivityNotFoundException from L20 to L21 using L2
aconst_null
astore 8
aconst_null
astore 2
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnull L22
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
astore 13
L0:
aload 13
invokevirtual android/app/SearchableInfo/getVoiceSearchLaunchWebSearch()Z
ifeq L7
new android/content/Intent
dup
aload 0
getfield android/support/v7/widget/SearchView/z Landroid/content/Intent;
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 3
aload 13
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 4
L1:
aload 4
ifnonnull L5
L3:
aload 3
ldc "calling_package"
aload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 3
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L4:
return
L5:
aload 4
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 2
L6:
goto L3
L7:
aload 13
invokevirtual android/app/SearchableInfo/getVoiceSearchLaunchRecognizer()Z
ifeq L22
aload 0
getfield android/support/v7/widget/SearchView/A Landroid/content/Intent;
astore 2
aload 13
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 12
new android/content/Intent
dup
ldc "android.intent.action.SEARCH"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 3
aload 3
aload 12
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
iconst_0
aload 3
ldc_w 1073741824
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 9
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 10
aload 0
getfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
ifnull L8
aload 10
ldc "app_data"
aload 0
getfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L8:
new android/content/Intent
dup
aload 2
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 11
L9:
iconst_1
istore 1
L10:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L23
aload 0
invokevirtual android/support/v7/widget/SearchView/getResources()Landroid/content/res/Resources;
astore 4
aload 13
invokevirtual android/app/SearchableInfo/getVoiceLanguageModeId()I
ifeq L24
aload 4
aload 13
invokevirtual android/app/SearchableInfo/getVoiceLanguageModeId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 2
L11:
aload 13
invokevirtual android/app/SearchableInfo/getVoicePromptTextId()I
ifeq L25
aload 4
aload 13
invokevirtual android/app/SearchableInfo/getVoicePromptTextId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 3
L12:
aload 13
invokevirtual android/app/SearchableInfo/getVoiceLanguageId()I
ifeq L26
aload 4
aload 13
invokevirtual android/app/SearchableInfo/getVoiceLanguageId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 4
L13:
aload 4
astore 5
aload 3
astore 6
aload 2
astore 7
L14:
aload 13
invokevirtual android/app/SearchableInfo/getVoiceMaxResults()I
ifeq L16
aload 13
invokevirtual android/app/SearchableInfo/getVoiceMaxResults()I
istore 1
L15:
aload 2
astore 7
aload 3
astore 6
aload 4
astore 5
L16:
aload 11
ldc "android.speech.extra.LANGUAGE_MODEL"
aload 7
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.PROMPT"
aload 6
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.LANGUAGE"
aload 5
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.MAX_RESULTS"
iload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;I)Landroid/content/Intent;
pop
L17:
aload 12
ifnonnull L20
aload 8
astore 2
L18:
aload 11
ldc "calling_package"
aload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.RESULTS_PENDINGINTENT"
aload 9
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"
aload 10
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
pop
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 11
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L19:
return
L2:
astore 0
ldc "SearchView"
ldc "Could not find voice search activity"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L20:
aload 12
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 2
L21:
goto L18
L26:
aconst_null
astore 4
goto L13
L25:
aconst_null
astore 3
goto L12
L24:
ldc "free_form"
astore 2
goto L11
L23:
aconst_null
astore 6
ldc "free_form"
astore 7
aconst_null
astore 5
goto L16
L22:
return
.limit locals 14
.limit stack 4
.end method

.method private l()Z
aload 0
getfield android/support/v7/widget/SearchView/K Z
ifne L0
aload 0
getfield android/support/v7/widget/SearchView/P Z
ifeq L1
L0:
aload 0
getfield android/support/v7/widget/SearchView/I Z
ifne L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic m(Landroid/support/v7/widget/SearchView;)Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
areturn
.limit locals 1
.limit stack 1
.end method

.method private m()V
bipush 8
istore 2
iload 2
istore 1
aload 0
invokespecial android/support/v7/widget/SearchView/l()Z
ifeq L0
aload 0
getfield android/support/v7/widget/SearchView/r Landroid/widget/ImageView;
invokevirtual android/widget/ImageView/getVisibility()I
ifeq L1
iload 2
istore 1
aload 0
getfield android/support/v7/widget/SearchView/t Landroid/widget/ImageView;
invokevirtual android/widget/ImageView/getVisibility()I
ifne L0
L1:
iconst_0
istore 1
L0:
aload 0
getfield android/support/v7/widget/SearchView/p Landroid/view/View;
iload 1
invokevirtual android/view/View/setVisibility(I)V
return
.limit locals 3
.limit stack 2
.end method

.method private n()V
iconst_1
istore 4
iconst_0
istore 3
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
iconst_1
istore 1
L1:
iload 4
istore 2
iload 1
ifne L2
aload 0
getfield android/support/v7/widget/SearchView/H Z
ifeq L3
aload 0
getfield android/support/v7/widget/SearchView/S Z
ifne L3
iload 4
istore 2
L2:
aload 0
getfield android/support/v7/widget/SearchView/s Landroid/widget/ImageView;
astore 5
iload 2
ifeq L4
iload 3
istore 2
L5:
aload 5
iload 2
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 0
getfield android/support/v7/widget/SearchView/s Landroid/widget/ImageView;
invokevirtual android/widget/ImageView/getDrawable()Landroid/graphics/drawable/Drawable;
astore 6
aload 6
ifnull L6
iload 1
ifeq L7
getstatic android/support/v7/widget/SearchView/ENABLED_STATE_SET [I
astore 5
L8:
aload 6
aload 5
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L6:
return
L0:
iconst_0
istore 1
goto L1
L3:
iconst_0
istore 2
goto L2
L4:
bipush 8
istore 2
goto L5
L7:
getstatic android/support/v7/widget/SearchView/EMPTY_STATE_SET [I
astore 5
goto L8
.limit locals 7
.limit stack 2
.end method

.method static synthetic n(Landroid/support/v7/widget/SearchView;)V
aload 0
invokespecial android/support/v7/widget/SearchView/y()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic o(Landroid/support/v7/widget/SearchView;)Landroid/app/SearchableInfo;
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private o()V
aload 0
aload 0
getfield android/support/v7/widget/SearchView/ab Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/SearchView/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method private p()V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/hasFocus()Z
ifeq L0
getstatic android/support/v7/widget/SearchView/FOCUSED_STATE_SET [I
astore 1
L1:
aload 0
getfield android/support/v7/widget/SearchView/o Landroid/view/View;
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L2
aload 2
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L2:
aload 0
getfield android/support/v7/widget/SearchView/p Landroid/view/View;
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L3
aload 2
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L3:
aload 0
invokevirtual android/support/v7/widget/SearchView/invalidate()V
return
L0:
getstatic android/support/v7/widget/SearchView/EMPTY_STATE_SET [I
astore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method static synthetic p(Landroid/support/v7/widget/SearchView;)V
aload 0
iconst_0
invokespecial android/support/v7/widget/SearchView/setImeVisibility(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private q()V
aload 0
invokevirtual android/support/v7/widget/SearchView/getQueryHint()Ljava/lang/CharSequence;
astore 3
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
astore 4
aload 3
astore 2
aload 3
ifnonnull L0
ldc ""
astore 2
L0:
aload 2
astore 3
aload 0
getfield android/support/v7/widget/SearchView/H Z
ifeq L1
aload 0
getfield android/support/v7/widget/SearchView/w Landroid/graphics/drawable/Drawable;
ifnonnull L2
aload 2
astore 3
L1:
aload 4
aload 3
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setHint(Ljava/lang/CharSequence;)V
return
L2:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getTextSize()F
f2d
ldc2_w 1.25D
dmul
d2i
istore 1
aload 0
getfield android/support/v7/widget/SearchView/w Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
iload 1
iload 1
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
new android/text/SpannableStringBuilder
dup
ldc "   "
invokespecial android/text/SpannableStringBuilder/<init>(Ljava/lang/CharSequence;)V
astore 3
aload 3
new android/text/style/ImageSpan
dup
aload 0
getfield android/support/v7/widget/SearchView/w Landroid/graphics/drawable/Drawable;
invokespecial android/text/style/ImageSpan/<init>(Landroid/graphics/drawable/Drawable;)V
iconst_1
iconst_2
bipush 33
invokevirtual android/text/SpannableStringBuilder/setSpan(Ljava/lang/Object;III)V
aload 3
aload 2
invokevirtual android/text/SpannableStringBuilder/append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
pop
goto L1
.limit locals 5
.limit stack 5
.end method

.method private r()V
.annotation invisible Landroid/annotation/TargetApi;
value I = 8
.end annotation
iconst_1
istore 2
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSuggestThreshold()I
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setThreshold(I)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getImeOptions()I
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setImeOptions(I)V
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getInputType()I
istore 3
iload 3
istore 1
iload 3
bipush 15
iand
iconst_1
if_icmpne L0
iload 3
ldc_w -65537
iand
istore 3
iload 3
istore 1
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSuggestAuthority()Ljava/lang/String;
ifnull L0
iload 3
ldc_w 65536
ior
ldc_w 524288
ior
istore 1
L0:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 1
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setInputType(I)V
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
ifnull L1
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
aconst_null
invokevirtual android/support/v4/widget/r/a(Landroid/database/Cursor;)V
L1:
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSuggestAuthority()Ljava/lang/String;
ifnull L2
aload 0
new android/support/v7/widget/bz
dup
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 0
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
aload 0
getfield android/support/v7/widget/SearchView/ad Ljava/util/WeakHashMap;
invokespecial android/support/v7/widget/bz/<init>(Landroid/content/Context;Landroid/support/v7/widget/SearchView;Landroid/app/SearchableInfo;Ljava/util/WeakHashMap;)V
putfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
checkcast android/support/v7/widget/bz
astore 4
iload 2
istore 1
aload 0
getfield android/support/v7/widget/SearchView/M Z
ifeq L3
iconst_2
istore 1
L3:
aload 4
iload 1
putfield android/support/v7/widget/bz/o I
L2:
return
.limit locals 5
.limit stack 7
.end method

.method private s()V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
astore 1
aload 1
ifnull L0
aload 1
invokestatic android/text/TextUtils/getTrimmedLength(Ljava/lang/CharSequence;)I
ifle L0
aload 0
getfield android/support/v7/widget/SearchView/C Landroid/support/v7/widget/bs;
ifnull L1
aload 0
getfield android/support/v7/widget/SearchView/C Landroid/support/v7/widget/bs;
astore 2
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
pop
aload 2
invokeinterface android/support/v7/widget/bs/a()Z 0
ifne L0
L1:
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnull L2
aload 0
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
invokespecial android/support/v7/widget/SearchView/a(Ljava/lang/String;)V
L2:
aload 0
iconst_0
invokespecial android/support/v7/widget/SearchView/setImeVisibility(Z)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/dismissDropDown()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method private setImeVisibility(Z)V
iload 1
ifeq L0
aload 0
aload 0
getfield android/support/v7/widget/SearchView/aa Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/SearchView/post(Ljava/lang/Runnable;)Z
pop
L1:
return
L0:
aload 0
aload 0
getfield android/support/v7/widget/SearchView/aa Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/SearchView/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
ldc "input_method"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/inputmethod/InputMethodManager
astore 2
aload 2
ifnull L1
aload 2
aload 0
invokevirtual android/support/v7/widget/SearchView/getWindowToken()Landroid/os/IBinder;
iconst_0
invokevirtual android/view/inputmethod/InputMethodManager/hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
pop
return
.limit locals 3
.limit stack 3
.end method

.method private setQuery$609c24db(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 1
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/length()I
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setSelection(I)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/R Ljava/lang/CharSequence;
return
.limit locals 2
.limit stack 2
.end method

.method private t()V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/dismissDropDown()V
return
.limit locals 1
.limit stack 1
.end method

.method private u()V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
aload 0
getfield android/support/v7/widget/SearchView/H Z
ifeq L1
aload 0
getfield android/support/v7/widget/SearchView/D Landroid/support/v7/widget/br;
ifnull L2
aload 0
getfield android/support/v7/widget/SearchView/D Landroid/support/v7/widget/br;
invokeinterface android/support/v7/widget/br/a()Z 0
ifne L1
L2:
aload 0
invokevirtual android/support/v7/widget/SearchView/clearFocus()V
aload 0
iconst_1
invokespecial android/support/v7/widget/SearchView/a(Z)V
L1:
return
L0:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
ldc ""
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/requestFocus()Z
pop
aload 0
iconst_1
invokespecial android/support/v7/widget/SearchView/setImeVisibility(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private v()V
aload 0
iconst_0
invokespecial android/support/v7/widget/SearchView/a(Z)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/requestFocus()Z
pop
aload 0
iconst_1
invokespecial android/support/v7/widget/SearchView/setImeVisibility(Z)V
aload 0
getfield android/support/v7/widget/SearchView/G Landroid/view/View$OnClickListener;
ifnull L0
aload 0
getfield android/support/v7/widget/SearchView/G Landroid/view/View$OnClickListener;
aload 0
invokeinterface android/view/View$OnClickListener/onClick(Landroid/view/View;)V 1
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private w()V
.annotation invisible Landroid/annotation/TargetApi;
value I = 8
.end annotation
.catch android/content/ActivityNotFoundException from L0 to L1 using L2
.catch android/content/ActivityNotFoundException from L3 to L4 using L2
.catch android/content/ActivityNotFoundException from L5 to L6 using L2
.catch android/content/ActivityNotFoundException from L7 to L8 using L2
.catch android/content/ActivityNotFoundException from L8 to L9 using L2
.catch android/content/ActivityNotFoundException from L10 to L11 using L2
.catch android/content/ActivityNotFoundException from L11 to L12 using L2
.catch android/content/ActivityNotFoundException from L12 to L13 using L2
.catch android/content/ActivityNotFoundException from L14 to L15 using L2
.catch android/content/ActivityNotFoundException from L16 to L17 using L2
.catch android/content/ActivityNotFoundException from L18 to L19 using L2
.catch android/content/ActivityNotFoundException from L20 to L21 using L2
aconst_null
astore 8
aconst_null
astore 2
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnonnull L22
L23:
return
L22:
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
astore 13
L0:
aload 13
invokevirtual android/app/SearchableInfo/getVoiceSearchLaunchWebSearch()Z
ifeq L7
new android/content/Intent
dup
aload 0
getfield android/support/v7/widget/SearchView/z Landroid/content/Intent;
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 3
aload 13
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 4
L1:
aload 4
ifnonnull L5
L3:
aload 3
ldc "calling_package"
aload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 3
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L4:
return
L2:
astore 2
ldc "SearchView"
ldc "Could not find voice search activity"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L5:
aload 4
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 2
L6:
goto L3
L7:
aload 13
invokevirtual android/app/SearchableInfo/getVoiceSearchLaunchRecognizer()Z
ifeq L23
aload 0
getfield android/support/v7/widget/SearchView/A Landroid/content/Intent;
astore 2
aload 13
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 12
new android/content/Intent
dup
ldc "android.intent.action.SEARCH"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 3
aload 3
aload 12
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
iconst_0
aload 3
ldc_w 1073741824
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 9
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 10
aload 0
getfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
ifnull L8
aload 10
ldc "app_data"
aload 0
getfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L8:
new android/content/Intent
dup
aload 2
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 11
L9:
iconst_1
istore 1
L10:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L24
aload 0
invokevirtual android/support/v7/widget/SearchView/getResources()Landroid/content/res/Resources;
astore 4
aload 13
invokevirtual android/app/SearchableInfo/getVoiceLanguageModeId()I
ifeq L25
aload 4
aload 13
invokevirtual android/app/SearchableInfo/getVoiceLanguageModeId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 2
L11:
aload 13
invokevirtual android/app/SearchableInfo/getVoicePromptTextId()I
ifeq L26
aload 4
aload 13
invokevirtual android/app/SearchableInfo/getVoicePromptTextId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 3
L12:
aload 13
invokevirtual android/app/SearchableInfo/getVoiceLanguageId()I
ifeq L27
aload 4
aload 13
invokevirtual android/app/SearchableInfo/getVoiceLanguageId()I
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
astore 4
L13:
aload 4
astore 5
aload 3
astore 6
aload 2
astore 7
L14:
aload 13
invokevirtual android/app/SearchableInfo/getVoiceMaxResults()I
ifeq L16
aload 13
invokevirtual android/app/SearchableInfo/getVoiceMaxResults()I
istore 1
L15:
aload 2
astore 7
aload 3
astore 6
aload 4
astore 5
L16:
aload 11
ldc "android.speech.extra.LANGUAGE_MODEL"
aload 7
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.PROMPT"
aload 6
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.LANGUAGE"
aload 5
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.MAX_RESULTS"
iload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;I)Landroid/content/Intent;
pop
L17:
aload 12
ifnonnull L20
aload 8
astore 2
L18:
aload 11
ldc "calling_package"
aload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.RESULTS_PENDINGINTENT"
aload 9
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 11
ldc "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"
aload 10
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
pop
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 11
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L19:
return
L20:
aload 12
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 2
L21:
goto L18
L27:
aconst_null
astore 4
goto L13
L26:
aconst_null
astore 3
goto L12
L25:
ldc "free_form"
astore 2
goto L11
L24:
aconst_null
astore 6
ldc "free_form"
astore 7
aconst_null
astore 5
goto L16
.limit locals 14
.limit stack 4
.end method

.method private x()V
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
invokevirtual android/view/View/getWidth()I
iconst_1
if_icmple L0
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 7
aload 0
getfield android/support/v7/widget/SearchView/o Landroid/view/View;
invokevirtual android/view/View/getPaddingLeft()I
istore 3
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 8
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
istore 6
aload 0
getfield android/support/v7/widget/SearchView/H Z
ifeq L1
aload 7
getstatic android/support/v7/a/g/abc_dropdownitem_icon_width I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
istore 1
aload 7
getstatic android/support/v7/a/g/abc_dropdownitem_text_padding_left I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
iload 1
iadd
istore 1
L2:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getDropDownBackground()Landroid/graphics/drawable/Drawable;
aload 8
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
iload 6
ifeq L3
aload 8
getfield android/graphics/Rect/left I
ineg
istore 2
L4:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 2
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setDropDownHorizontalOffset(I)V
aload 0
getfield android/support/v7/widget/SearchView/u Landroid/view/View;
invokevirtual android/view/View/getWidth()I
istore 2
aload 8
getfield android/graphics/Rect/left I
istore 4
aload 8
getfield android/graphics/Rect/right I
istore 5
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 1
iload 2
iload 4
iadd
iload 5
iadd
iadd
iload 3
isub
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setDropDownWidth(I)V
L0:
return
L1:
iconst_0
istore 1
goto L2
L3:
iload 3
aload 8
getfield android/graphics/Rect/left I
iload 1
iadd
isub
istore 2
goto L4
.limit locals 9
.limit stack 4
.end method

.method private y()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
getstatic android/support/v7/widget/SearchView/a Landroid/support/v7/widget/bq;
astore 1
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
astore 2
aload 1
getfield android/support/v7/widget/bq/a Ljava/lang/reflect/Method;
ifnull L1
L0:
aload 1
getfield android/support/v7/widget/bq/a Ljava/lang/reflect/Method;
aload 2
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
getstatic android/support/v7/widget/SearchView/a Landroid/support/v7/widget/bq;
astore 1
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
astore 2
aload 1
getfield android/support/v7/widget/bq/b Ljava/lang/reflect/Method;
ifnull L4
L3:
aload 1
getfield android/support/v7/widget/bq/b Ljava/lang/reflect/Method;
aload 2
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
return
L5:
astore 1
return
L2:
astore 1
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final a()V
aload 0
getfield android/support/v7/widget/SearchView/S Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield android/support/v7/widget/SearchView/S Z
aload 0
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getImeOptions()I
putfield android/support/v7/widget/SearchView/T I
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/T I
ldc_w 33554432
ior
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setImeOptions(I)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
ldc ""
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setText(Ljava/lang/CharSequence;)V
aload 0
iconst_0
invokevirtual android/support/v7/widget/SearchView/setIconified(Z)V
return
.limit locals 1
.limit stack 3
.end method

.method public final b()V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
ldc ""
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/length()I
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setSelection(I)V
aload 0
ldc ""
putfield android/support/v7/widget/SearchView/R Ljava/lang/CharSequence;
aload 0
invokevirtual android/support/v7/widget/SearchView/clearFocus()V
aload 0
iconst_1
invokespecial android/support/v7/widget/SearchView/a(Z)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/T I
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setImeOptions(I)V
aload 0
iconst_0
putfield android/support/v7/widget/SearchView/S Z
return
.limit locals 1
.limit stack 2
.end method

.method public final clearFocus()V
aload 0
iconst_1
putfield android/support/v7/widget/SearchView/N Z
aload 0
iconst_0
invokespecial android/support/v7/widget/SearchView/setImeVisibility(Z)V
aload 0
invokespecial android/support/v7/widget/aj/clearFocus()V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/clearFocus()V
aload 0
iconst_0
putfield android/support/v7/widget/SearchView/N Z
return
.limit locals 1
.limit stack 2
.end method

.method final d()V
aload 0
aload 0
getfield android/support/v7/widget/SearchView/I Z
invokespecial android/support/v7/widget/SearchView/a(Z)V
aload 0
invokespecial android/support/v7/widget/SearchView/o()V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/hasFocus()Z
ifeq L0
aload 0
invokespecial android/support/v7/widget/SearchView/y()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final getImeOptions()I
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getImeOptions()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getInputType()I
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getInputType()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getMaxWidth()I
aload 0
getfield android/support/v7/widget/SearchView/O I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getQuery()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/getText()Landroid/text/Editable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getQueryHint()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/SearchView/L Ljava/lang/CharSequence;
ifnull L0
aload 0
getfield android/support/v7/widget/SearchView/L Ljava/lang/CharSequence;
areturn
L0:
getstatic android/support/v7/widget/SearchView/e Z
ifeq L1
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnull L1
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getHintId()I
ifeq L1
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getHintId()I
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
areturn
L1:
aload 0
getfield android/support/v7/widget/SearchView/B Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 2
.end method

.method final getSuggestionCommitIconResId()I
aload 0
getfield android/support/v7/widget/SearchView/y I
ireturn
.limit locals 1
.limit stack 1
.end method

.method final getSuggestionRowLayout()I
aload 0
getfield android/support/v7/widget/SearchView/x I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getSuggestionsAdapter()Landroid/support/v4/widget/r;
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final onDetachedFromWindow()V
aload 0
aload 0
getfield android/support/v7/widget/SearchView/ab Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/SearchView/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
aload 0
getfield android/support/v7/widget/SearchView/ac Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/SearchView/post(Ljava/lang/Runnable;)Z
pop
aload 0
invokespecial android/support/v7/widget/aj/onDetachedFromWindow()V
return
.limit locals 1
.limit stack 2
.end method

.method protected final onMeasure(II)V
aload 0
getfield android/support/v7/widget/SearchView/I Z
ifeq L0
aload 0
iload 1
iload 2
invokespecial android/support/v7/widget/aj/onMeasure(II)V
return
L0:
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 4
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 3
iload 4
lookupswitch
-2147483648 : L1
0 : L2
1073741824 : L3
default : L4
L4:
iload 3
istore 1
L5:
aload 0
iload 1
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 2
invokespecial android/support/v7/widget/aj/onMeasure(II)V
return
L1:
aload 0
getfield android/support/v7/widget/SearchView/O I
ifle L6
aload 0
getfield android/support/v7/widget/SearchView/O I
iload 3
invokestatic java/lang/Math/min(II)I
istore 1
goto L5
L6:
aload 0
invokespecial android/support/v7/widget/SearchView/getPreferredWidth()I
iload 3
invokestatic java/lang/Math/min(II)I
istore 1
goto L5
L3:
iload 3
istore 1
aload 0
getfield android/support/v7/widget/SearchView/O I
ifle L5
aload 0
getfield android/support/v7/widget/SearchView/O I
iload 3
invokestatic java/lang/Math/min(II)I
istore 1
goto L5
L2:
aload 0
getfield android/support/v7/widget/SearchView/O I
ifle L7
aload 0
getfield android/support/v7/widget/SearchView/O I
istore 1
goto L5
L7:
aload 0
invokespecial android/support/v7/widget/SearchView/getPreferredWidth()I
istore 1
goto L5
.limit locals 5
.limit stack 3
.end method

.method public final onWindowFocusChanged(Z)V
aload 0
iload 1
invokespecial android/support/v7/widget/aj/onWindowFocusChanged(Z)V
aload 0
invokespecial android/support/v7/widget/SearchView/o()V
return
.limit locals 2
.limit stack 2
.end method

.method public final requestFocus(ILandroid/graphics/Rect;)Z
aload 0
getfield android/support/v7/widget/SearchView/N Z
ifeq L0
L1:
iconst_0
ireturn
L0:
aload 0
invokevirtual android/support/v7/widget/SearchView/isFocusable()Z
ifeq L1
aload 0
getfield android/support/v7/widget/SearchView/I Z
ifne L2
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 1
aload 2
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/requestFocus(ILandroid/graphics/Rect;)Z
istore 3
iload 3
ifeq L3
aload 0
iconst_0
invokespecial android/support/v7/widget/SearchView/a(Z)V
L3:
iload 3
ireturn
L2:
aload 0
iload 1
aload 2
invokespecial android/support/v7/widget/aj/requestFocus(ILandroid/graphics/Rect;)Z
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final setAppSearchData(Landroid/os/Bundle;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/V Landroid/os/Bundle;
return
.limit locals 2
.limit stack 2
.end method

.method public final setIconified(Z)V
iload 1
ifeq L0
aload 0
invokespecial android/support/v7/widget/SearchView/u()V
return
L0:
aload 0
invokespecial android/support/v7/widget/SearchView/v()V
return
.limit locals 2
.limit stack 1
.end method

.method public final setIconifiedByDefault(Z)V
aload 0
getfield android/support/v7/widget/SearchView/H Z
iload 1
if_icmpne L0
return
L0:
aload 0
iload 1
putfield android/support/v7/widget/SearchView/H Z
aload 0
iload 1
invokespecial android/support/v7/widget/SearchView/a(Z)V
aload 0
invokespecial android/support/v7/widget/SearchView/q()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setImeOptions(I)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 1
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setImeOptions(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setInputType(I)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 1
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setInputType(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setMaxWidth(I)V
aload 0
iload 1
putfield android/support/v7/widget/SearchView/O I
aload 0
invokevirtual android/support/v7/widget/SearchView/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setOnCloseListener(Landroid/support/v7/widget/br;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/D Landroid/support/v7/widget/br;
return
.limit locals 2
.limit stack 2
.end method

.method public final setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/E Landroid/view/View$OnFocusChangeListener;
return
.limit locals 2
.limit stack 2
.end method

.method public final setOnQueryTextListener(Landroid/support/v7/widget/bs;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/C Landroid/support/v7/widget/bs;
return
.limit locals 2
.limit stack 2
.end method

.method public final setOnSearchClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/G Landroid/view/View$OnClickListener;
return
.limit locals 2
.limit stack 2
.end method

.method public final setOnSuggestionListener(Landroid/support/v7/widget/bt;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/F Landroid/support/v7/widget/bt;
return
.limit locals 2
.limit stack 2
.end method

.method final setQuery(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 1
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
astore 3
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
iconst_0
istore 2
L1:
aload 3
iload 2
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setSelection(I)V
return
L0:
aload 1
invokeinterface java/lang/CharSequence/length()I 0
istore 2
goto L1
.limit locals 4
.limit stack 2
.end method

.method public final setQueryHint(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/L Ljava/lang/CharSequence;
aload 0
invokespecial android/support/v7/widget/SearchView/q()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setQueryRefinementEnabled(Z)V
aload 0
iload 1
putfield android/support/v7/widget/SearchView/M Z
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
instanceof android/support/v7/widget/bz
ifeq L0
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
checkcast android/support/v7/widget/bz
astore 3
iload 1
ifeq L1
iconst_2
istore 2
L2:
aload 3
iload 2
putfield android/support/v7/widget/bz/o I
L0:
return
L1:
iconst_1
istore 2
goto L2
.limit locals 4
.limit stack 2
.end method

.method public final setSearchableInfo(Landroid/app/SearchableInfo;)V
iconst_1
istore 4
aload 0
aload 1
putfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnull L0
getstatic android/support/v7/widget/SearchView/e Z
ifeq L1
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSuggestThreshold()I
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setThreshold(I)V
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getImeOptions()I
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setImeOptions(I)V
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getInputType()I
istore 3
iload 3
istore 2
iload 3
bipush 15
iand
iconst_1
if_icmpne L2
iload 3
ldc_w -65537
iand
istore 3
iload 3
istore 2
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSuggestAuthority()Ljava/lang/String;
ifnull L2
iload 3
ldc_w 65536
ior
ldc_w 524288
ior
istore 2
L2:
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
iload 2
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setInputType(I)V
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
ifnull L3
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
aconst_null
invokevirtual android/support/v4/widget/r/a(Landroid/database/Cursor;)V
L3:
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSuggestAuthority()Ljava/lang/String;
ifnull L1
aload 0
new android/support/v7/widget/bz
dup
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
aload 0
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
aload 0
getfield android/support/v7/widget/SearchView/ad Ljava/util/WeakHashMap;
invokespecial android/support/v7/widget/bz/<init>(Landroid/content/Context;Landroid/support/v7/widget/SearchView;Landroid/app/SearchableInfo;Ljava/util/WeakHashMap;)V
putfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
checkcast android/support/v7/widget/bz
astore 1
aload 0
getfield android/support/v7/widget/SearchView/M Z
ifeq L4
iconst_2
istore 2
L5:
aload 1
iload 2
putfield android/support/v7/widget/bz/o I
L1:
aload 0
invokespecial android/support/v7/widget/SearchView/q()V
L0:
getstatic android/support/v7/widget/SearchView/e Z
ifeq L6
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
ifnull L7
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getVoiceSearchEnabled()Z
ifeq L7
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getVoiceSearchLaunchWebSearch()Z
ifeq L8
aload 0
getfield android/support/v7/widget/SearchView/z Landroid/content/Intent;
astore 1
L9:
aload 1
ifnull L7
aload 0
invokevirtual android/support/v7/widget/SearchView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
ldc_w 65536
invokevirtual android/content/pm/PackageManager/resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
ifnull L10
iconst_1
istore 2
L11:
iload 2
ifeq L6
L12:
aload 0
iload 4
putfield android/support/v7/widget/SearchView/P Z
aload 0
getfield android/support/v7/widget/SearchView/P Z
ifeq L13
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
ldc "nm"
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setPrivateImeOptions(Ljava/lang/String;)V
L13:
aload 0
aload 0
getfield android/support/v7/widget/SearchView/I Z
invokespecial android/support/v7/widget/SearchView/a(Z)V
return
L4:
iconst_1
istore 2
goto L5
L8:
aload 0
getfield android/support/v7/widget/SearchView/U Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getVoiceSearchLaunchRecognizer()Z
ifeq L14
aload 0
getfield android/support/v7/widget/SearchView/A Landroid/content/Intent;
astore 1
goto L9
L10:
iconst_0
istore 2
goto L11
L7:
iconst_0
istore 2
goto L11
L6:
iconst_0
istore 4
goto L12
L14:
aconst_null
astore 1
goto L9
.limit locals 5
.limit stack 7
.end method

.method public final setSubmitButtonEnabled(Z)V
aload 0
iload 1
putfield android/support/v7/widget/SearchView/K Z
aload 0
aload 0
getfield android/support/v7/widget/SearchView/I Z
invokespecial android/support/v7/widget/SearchView/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setSuggestionsAdapter(Landroid/support/v4/widget/r;)V
aload 0
aload 1
putfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
aload 0
getfield android/support/v7/widget/SearchView/g Landroid/support/v7/widget/SearchView$SearchAutoComplete;
aload 0
getfield android/support/v7/widget/SearchView/J Landroid/support/v4/widget/r;
invokevirtual android/support/v7/widget/SearchView$SearchAutoComplete/setAdapter(Landroid/widget/ListAdapter;)V
return
.limit locals 2
.limit stack 2
.end method
