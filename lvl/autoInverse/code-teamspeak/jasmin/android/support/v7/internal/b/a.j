.bytecode 50.0
.class public final synchronized android/support/v7/internal/b/a
.super java/lang/Object
.implements android/text/method/TransformationMethod

.field private 'a' Ljava/util/Locale;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
getfield android/content/res/Configuration/locale Ljava/util/Locale;
putfield android/support/v7/internal/b/a/a Ljava/util/Locale;
return
.limit locals 2
.limit stack 2
.end method

.method public final getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
aload 1
ifnull L0
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
aload 0
getfield android/support/v7/internal/b/a/a Ljava/util/Locale;
invokevirtual java/lang/String/toUpperCase(Ljava/util/Locale;)Ljava/lang/String;
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method public final onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
return
.limit locals 6
.limit stack 0
.end method
