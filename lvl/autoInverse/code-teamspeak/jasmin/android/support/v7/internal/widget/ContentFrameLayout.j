.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/ContentFrameLayout
.super android/widget/FrameLayout

.field public final 'a' Landroid/graphics/Rect;

.field private 'b' Landroid/util/TypedValue;

.field private 'c' Landroid/util/TypedValue;

.field private 'd' Landroid/util/TypedValue;

.field private 'e' Landroid/util/TypedValue;

.field private 'f' Landroid/util/TypedValue;

.field private 'g' Landroid/util/TypedValue;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/internal/widget/ContentFrameLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/internal/widget/ContentFrameLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/internal/widget/ContentFrameLayout/a Landroid/graphics/Rect;
return
.limit locals 4
.limit stack 4
.end method

.method private a(IIII)V
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/a Landroid/graphics/Rect;
iload 1
iload 2
iload 3
iload 4
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L0
aload 0
invokevirtual android/support/v7/internal/widget/ContentFrameLayout/requestLayout()V
L0:
return
.limit locals 5
.limit stack 5
.end method

.method public final a(Landroid/graphics/Rect;)V
aload 0
aload 1
invokevirtual android/support/v7/internal/widget/ContentFrameLayout/fitSystemWindows(Landroid/graphics/Rect;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public getFixedHeightMajor()Landroid/util/TypedValue;
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/f Landroid/util/TypedValue;
ifnonnull L0
aload 0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
putfield android/support/v7/internal/widget/ContentFrameLayout/f Landroid/util/TypedValue;
L0:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/f Landroid/util/TypedValue;
areturn
.limit locals 1
.limit stack 3
.end method

.method public getFixedHeightMinor()Landroid/util/TypedValue;
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/g Landroid/util/TypedValue;
ifnonnull L0
aload 0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
putfield android/support/v7/internal/widget/ContentFrameLayout/g Landroid/util/TypedValue;
L0:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/g Landroid/util/TypedValue;
areturn
.limit locals 1
.limit stack 3
.end method

.method public getFixedWidthMajor()Landroid/util/TypedValue;
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/d Landroid/util/TypedValue;
ifnonnull L0
aload 0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
putfield android/support/v7/internal/widget/ContentFrameLayout/d Landroid/util/TypedValue;
L0:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/d Landroid/util/TypedValue;
areturn
.limit locals 1
.limit stack 3
.end method

.method public getFixedWidthMinor()Landroid/util/TypedValue;
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/e Landroid/util/TypedValue;
ifnonnull L0
aload 0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
putfield android/support/v7/internal/widget/ContentFrameLayout/e Landroid/util/TypedValue;
L0:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/e Landroid/util/TypedValue;
areturn
.limit locals 1
.limit stack 3
.end method

.method public getMinWidthMajor()Landroid/util/TypedValue;
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/b Landroid/util/TypedValue;
ifnonnull L0
aload 0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
putfield android/support/v7/internal/widget/ContentFrameLayout/b Landroid/util/TypedValue;
L0:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/b Landroid/util/TypedValue;
areturn
.limit locals 1
.limit stack 3
.end method

.method public getMinWidthMinor()Landroid/util/TypedValue;
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/c Landroid/util/TypedValue;
ifnonnull L0
aload 0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
putfield android/support/v7/internal/widget/ContentFrameLayout/c Landroid/util/TypedValue;
L0:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/c Landroid/util/TypedValue;
areturn
.limit locals 1
.limit stack 3
.end method

.method protected onMeasure(II)V
iconst_0
istore 7
aload 0
invokevirtual android/support/v7/internal/widget/ContentFrameLayout/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
astore 11
aload 11
getfield android/util/DisplayMetrics/widthPixels I
aload 11
getfield android/util/DisplayMetrics/heightPixels I
if_icmpge L0
iconst_1
istore 4
L1:
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 8
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 9
iload 8
ldc_w -2147483648
if_icmpne L2
iload 4
ifeq L3
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/e Landroid/util/TypedValue;
astore 10
L4:
aload 10
ifnull L2
aload 10
getfield android/util/TypedValue/type I
ifeq L2
aload 10
getfield android/util/TypedValue/type I
iconst_5
if_icmpne L5
aload 10
aload 11
invokevirtual android/util/TypedValue/getDimension(Landroid/util/DisplayMetrics;)F
f2i
istore 3
L6:
iload 3
ifle L2
iload 3
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/a Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/a Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iadd
isub
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
invokestatic java/lang/Math/min(II)I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
iconst_1
istore 5
L7:
iload 2
istore 3
iload 9
ldc_w -2147483648
if_icmpne L8
iload 4
ifeq L9
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/f Landroid/util/TypedValue;
astore 10
L10:
iload 2
istore 3
aload 10
ifnull L8
iload 2
istore 3
aload 10
getfield android/util/TypedValue/type I
ifeq L8
aload 10
getfield android/util/TypedValue/type I
iconst_5
if_icmpne L11
aload 10
aload 11
invokevirtual android/util/TypedValue/getDimension(Landroid/util/DisplayMetrics;)F
f2i
istore 1
L12:
iload 2
istore 3
iload 1
ifle L8
iload 1
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/a Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/a Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
iadd
isub
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
invokestatic java/lang/Math/min(II)I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 3
L8:
aload 0
iload 6
iload 3
invokespecial android/widget/FrameLayout/onMeasure(II)V
aload 0
invokevirtual android/support/v7/internal/widget/ContentFrameLayout/getMeasuredWidth()I
istore 9
iload 9
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
iload 5
ifne L13
iload 8
ldc_w -2147483648
if_icmpne L13
iload 4
ifeq L14
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/c Landroid/util/TypedValue;
astore 10
L15:
aload 10
ifnull L13
aload 10
getfield android/util/TypedValue/type I
ifeq L13
aload 10
getfield android/util/TypedValue/type I
iconst_5
if_icmpne L16
aload 10
aload 11
invokevirtual android/util/TypedValue/getDimension(Landroid/util/DisplayMetrics;)F
f2i
istore 1
L17:
iload 1
istore 2
iload 1
ifle L18
iload 1
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/a Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/a Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iadd
isub
istore 2
L18:
iload 9
iload 2
if_icmpge L13
iload 2
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 1
iconst_1
istore 2
L19:
iload 2
ifeq L20
aload 0
iload 1
iload 3
invokespecial android/widget/FrameLayout/onMeasure(II)V
L20:
return
L0:
iconst_0
istore 4
goto L1
L3:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/d Landroid/util/TypedValue;
astore 10
goto L4
L5:
aload 10
getfield android/util/TypedValue/type I
bipush 6
if_icmpne L21
aload 10
aload 11
getfield android/util/DisplayMetrics/widthPixels I
i2f
aload 11
getfield android/util/DisplayMetrics/widthPixels I
i2f
invokevirtual android/util/TypedValue/getFraction(FF)F
f2i
istore 3
goto L6
L9:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/g Landroid/util/TypedValue;
astore 10
goto L10
L11:
aload 10
getfield android/util/TypedValue/type I
bipush 6
if_icmpne L22
aload 10
aload 11
getfield android/util/DisplayMetrics/heightPixels I
i2f
aload 11
getfield android/util/DisplayMetrics/heightPixels I
i2f
invokevirtual android/util/TypedValue/getFraction(FF)F
f2i
istore 1
goto L12
L14:
aload 0
getfield android/support/v7/internal/widget/ContentFrameLayout/b Landroid/util/TypedValue;
astore 10
goto L15
L16:
aload 10
getfield android/util/TypedValue/type I
bipush 6
if_icmpne L23
aload 10
aload 11
getfield android/util/DisplayMetrics/widthPixels I
i2f
aload 11
getfield android/util/DisplayMetrics/widthPixels I
i2f
invokevirtual android/util/TypedValue/getFraction(FF)F
f2i
istore 1
goto L17
L13:
iload 6
istore 1
iload 7
istore 2
goto L19
L23:
iconst_0
istore 1
goto L17
L22:
iconst_0
istore 1
goto L12
L2:
iconst_0
istore 5
iload 1
istore 6
goto L7
L21:
iconst_0
istore 3
goto L6
.limit locals 12
.limit stack 3
.end method
