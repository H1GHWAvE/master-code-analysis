.bytecode 50.0
.class final synchronized android/support/v7/internal/widget/y
.super android/widget/BaseAdapter

.field public static final 'a' I = 2147483647


.field public static final 'b' I = 4


.field private static final 'f' I = 0


.field private static final 'g' I = 1


.field private static final 'h' I = 3


.field 'c' Landroid/support/v7/internal/widget/l;

.field 'd' Z

.field final synthetic 'e' Landroid/support/v7/internal/widget/ActivityChooserView;

.field private 'i' I

.field private 'j' Z

.field private 'k' Z

.method private <init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
iconst_4
putfield android/support/v7/internal/widget/y/i I
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Landroid/support/v7/internal/widget/ActivityChooserView;B)V
aload 0
aload 1
invokespecial android/support/v7/internal/widget/y/<init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/support/v7/internal/widget/l;)V
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
astore 2
aload 2
ifnull L0
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/isShown()Z
ifeq L0
aload 2
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/i(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/database/DataSetObserver;
invokevirtual android/support/v7/internal/widget/l/unregisterObserver(Ljava/lang/Object;)V
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
aload 1
ifnull L1
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/isShown()Z
ifeq L1
aload 1
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/i(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/database/DataSetObserver;
invokevirtual android/support/v7/internal/widget/l/registerObserver(Ljava/lang/Object;)V
L1:
aload 0
invokevirtual android/support/v7/internal/widget/y/notifyDataSetChanged()V
return
.limit locals 3
.limit stack 2
.end method

.method private b()Landroid/content/pm/ResolveInfo;
aload 0
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/b()Landroid/content/pm/ResolveInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/a()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/c()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()Landroid/support/v7/internal/widget/l;
aload 0
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Z
aload 0
getfield android/support/v7/internal/widget/y/d Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
iconst_0
istore 1
aload 0
getfield android/support/v7/internal/widget/y/i I
istore 3
aload 0
ldc_w 2147483647
putfield android/support/v7/internal/widget/y/i I
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 4
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
aload 0
invokevirtual android/support/v7/internal/widget/y/getCount()I
istore 6
aconst_null
astore 7
iconst_0
istore 2
L0:
iload 1
iload 6
if_icmpge L1
aload 0
iload 1
aload 7
aconst_null
invokevirtual android/support/v7/internal/widget/y/getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
astore 7
aload 7
iload 4
iload 5
invokevirtual android/view/View/measure(II)V
iload 2
aload 7
invokevirtual android/view/View/getMeasuredWidth()I
invokestatic java/lang/Math/max(II)I
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
iload 3
putfield android/support/v7/internal/widget/y/i I
iload 2
ireturn
.limit locals 8
.limit stack 4
.end method

.method public final a(I)V
aload 0
getfield android/support/v7/internal/widget/y/i I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/internal/widget/y/i I
aload 0
invokevirtual android/support/v7/internal/widget/y/notifyDataSetChanged()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
getfield android/support/v7/internal/widget/y/k Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/internal/widget/y/k Z
aload 0
invokevirtual android/support/v7/internal/widget/y/notifyDataSetChanged()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(ZZ)V
aload 0
getfield android/support/v7/internal/widget/y/d Z
iload 1
if_icmpne L0
aload 0
getfield android/support/v7/internal/widget/y/j Z
iload 2
if_icmpeq L1
L0:
aload 0
iload 1
putfield android/support/v7/internal/widget/y/d Z
aload 0
iload 2
putfield android/support/v7/internal/widget/y/j Z
aload 0
invokevirtual android/support/v7/internal/widget/y/notifyDataSetChanged()V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public final getCount()I
aload 0
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/a()I
istore 2
iload 2
istore 1
aload 0
getfield android/support/v7/internal/widget/y/d Z
ifne L0
iload 2
istore 1
aload 0
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/b()Landroid/content/pm/ResolveInfo;
ifnull L0
iload 2
iconst_1
isub
istore 1
L0:
iload 1
aload 0
getfield android/support/v7/internal/widget/y/i I
invokestatic java/lang/Math/min(II)I
istore 2
iload 2
istore 1
aload 0
getfield android/support/v7/internal/widget/y/k Z
ifeq L1
iload 2
iconst_1
iadd
istore 1
L1:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final getItem(I)Ljava/lang/Object;
aload 0
iload 1
invokevirtual android/support/v7/internal/widget/y/getItemViewType(I)I
tableswitch 0
L0
L1
default : L2
L2:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L1:
aconst_null
areturn
L0:
iload 1
istore 2
aload 0
getfield android/support/v7/internal/widget/y/d Z
ifne L3
iload 1
istore 2
aload 0
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/b()Landroid/content/pm/ResolveInfo;
ifnull L3
iload 1
iconst_1
iadd
istore 2
L3:
aload 0
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
iload 2
invokevirtual android/support/v7/internal/widget/l/a(I)Landroid/content/pm/ResolveInfo;
areturn
.limit locals 3
.limit stack 2
.end method

.method public final getItemId(I)J
iload 1
i2l
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemViewType(I)I
aload 0
getfield android/support/v7/internal/widget/y/k Z
ifeq L0
iload 1
aload 0
invokevirtual android/support/v7/internal/widget/y/getCount()I
iconst_1
isub
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
iload 1
invokevirtual android/support/v7/internal/widget/y/getItemViewType(I)I
tableswitch 0
L0
L1
default : L2
L2:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L1:
aload 2
ifnull L3
aload 2
astore 4
aload 2
invokevirtual android/view/View/getId()I
iconst_1
if_icmpeq L4
L3:
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_activity_chooser_view_list_item I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 4
aload 4
iconst_1
invokevirtual android/view/View/setId(I)V
aload 4
getstatic android/support/v7/a/i/title I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
getstatic android/support/v7/a/l/abc_activity_chooser_view_see_all I
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L4:
aload 4
areturn
L0:
aload 2
ifnull L5
aload 2
astore 4
aload 2
invokevirtual android/view/View/getId()I
getstatic android/support/v7/a/i/list_item I
if_icmpeq L6
L5:
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_activity_chooser_view_list_item I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 4
L6:
aload 0
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 2
aload 4
getstatic android/support/v7/a/i/icon I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
astore 3
aload 0
iload 1
invokevirtual android/support/v7/internal/widget/y/getItem(I)Ljava/lang/Object;
checkcast android/content/pm/ResolveInfo
astore 5
aload 3
aload 5
aload 2
invokevirtual android/content/pm/ResolveInfo/loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 4
getstatic android/support/v7/a/i/title I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 5
aload 2
invokevirtual android/content/pm/ResolveInfo/loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/widget/y/d Z
ifeq L7
iload 1
ifne L7
aload 0
getfield android/support/v7/internal/widget/y/j Z
ifeq L7
aload 4
iconst_1
invokestatic android/support/v4/view/cx/b(Landroid/view/View;Z)V
aload 4
areturn
L7:
aload 4
iconst_0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;Z)V
aload 4
areturn
.limit locals 6
.limit stack 4
.end method

.method public final getViewTypeCount()I
iconst_3
ireturn
.limit locals 1
.limit stack 1
.end method
