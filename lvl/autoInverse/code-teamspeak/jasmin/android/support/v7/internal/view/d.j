.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/d
.super android/view/ActionMode
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation

.field final 'a' Landroid/content/Context;

.field final 'b' Landroid/support/v7/c/a;

.method public <init>(Landroid/content/Context;Landroid/support/v7/c/a;)V
aload 0
invokespecial android/view/ActionMode/<init>()V
aload 0
aload 1
putfield android/support/v7/internal/view/d/a Landroid/content/Context;
aload 0
aload 2
putfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
return
.limit locals 3
.limit stack 2
.end method

.method public final finish()V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
invokevirtual android/support/v7/c/a/c()V
return
.limit locals 1
.limit stack 1
.end method

.method public final getCustomView()Landroid/view/View;
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
invokevirtual android/support/v7/c/a/i()Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getMenu()Landroid/view/Menu;
aload 0
getfield android/support/v7/internal/view/d/a Landroid/content/Context;
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
invokevirtual android/support/v7/c/a/b()Landroid/view/Menu;
checkcast android/support/v4/g/a/a
invokestatic android/support/v7/internal/view/menu/ab/a(Landroid/content/Context;Landroid/support/v4/g/a/a;)Landroid/view/Menu;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final getMenuInflater()Landroid/view/MenuInflater;
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
invokevirtual android/support/v7/c/a/a()Landroid/view/MenuInflater;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getSubtitle()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
invokevirtual android/support/v7/c/a/g()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTag()Ljava/lang/Object;
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
getfield android/support/v7/c/a/b Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTitle()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
invokevirtual android/support/v7/c/a/f()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTitleOptionalHint()Z
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
getfield android/support/v7/c/a/c Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final invalidate()V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
invokevirtual android/support/v7/c/a/d()V
return
.limit locals 1
.limit stack 1
.end method

.method public final isTitleOptional()Z
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
invokevirtual android/support/v7/c/a/h()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final setCustomView(Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
aload 1
invokevirtual android/support/v7/c/a/a(Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setSubtitle(I)V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
iload 1
invokevirtual android/support/v7/c/a/b(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setSubtitle(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
aload 1
invokevirtual android/support/v7/c/a/a(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTag(Ljava/lang/Object;)V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
aload 1
putfield android/support/v7/c/a/b Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method public final setTitle(I)V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
iload 1
invokevirtual android/support/v7/c/a/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
aload 1
invokevirtual android/support/v7/c/a/b(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTitleOptionalHint(Z)V
aload 0
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
iload 1
invokevirtual android/support/v7/c/a/a(Z)V
return
.limit locals 2
.limit stack 2
.end method
