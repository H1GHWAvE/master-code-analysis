.bytecode 50.0
.class public synchronized abstract android/support/v7/internal/view/menu/d
.super java/lang/Object
.implements android/support/v7/internal/view/menu/x

.field public 'a' Landroid/content/Context;

.field public 'b' Landroid/content/Context;

.field public 'c' Landroid/support/v7/internal/view/menu/i;

.field protected 'd' Landroid/view/LayoutInflater;

.field protected 'e' Landroid/view/LayoutInflater;

.field public 'f' Landroid/support/v7/internal/view/menu/y;

.field public 'g' Landroid/support/v7/internal/view/menu/z;

.field public 'h' I

.field private 'i' I

.field private 'j' I

.method public <init>(Landroid/content/Context;II)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/d/a Landroid/content/Context;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/internal/view/menu/d/d Landroid/view/LayoutInflater;
aload 0
iload 2
putfield android/support/v7/internal/view/menu/d/i I
aload 0
iload 3
putfield android/support/v7/internal/view/menu/d/j I
return
.limit locals 4
.limit stack 2
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/d/h I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;I)V
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
checkcast android/view/ViewGroup
astore 3
aload 3
ifnull L0
aload 3
aload 1
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
L0:
aload 0
getfield android/support/v7/internal/view/menu/d/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/ViewGroup
aload 1
iload 2
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;I)V
return
.limit locals 4
.limit stack 3
.end method

.method private b(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/aa;
aload 0
getfield android/support/v7/internal/view/menu/d/d Landroid/view/LayoutInflater;
aload 0
getfield android/support/v7/internal/view/menu/d/j I
aload 1
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/support/v7/internal/view/menu/aa
areturn
.limit locals 2
.limit stack 4
.end method

.method private d()Landroid/support/v7/internal/view/menu/y;
aload 0
getfield android/support/v7/internal/view/menu/d/f Landroid/support/v7/internal/view/menu/y;
areturn
.limit locals 1
.limit stack 1
.end method

.method public a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
aload 0
getfield android/support/v7/internal/view/menu/d/g Landroid/support/v7/internal/view/menu/z;
ifnonnull L0
aload 0
aload 0
getfield android/support/v7/internal/view/menu/d/d Landroid/view/LayoutInflater;
aload 0
getfield android/support/v7/internal/view/menu/d/i I
aload 1
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/support/v7/internal/view/menu/z
putfield android/support/v7/internal/view/menu/d/g Landroid/support/v7/internal/view/menu/z;
aload 0
getfield android/support/v7/internal/view/menu/d/g Landroid/support/v7/internal/view/menu/z;
aload 0
getfield android/support/v7/internal/view/menu/d/c Landroid/support/v7/internal/view/menu/i;
invokeinterface android/support/v7/internal/view/menu/z/a(Landroid/support/v7/internal/view/menu/i;)V 1
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/d/a(Z)V
L0:
aload 0
getfield android/support/v7/internal/view/menu/d/g Landroid/support/v7/internal/view/menu/z;
areturn
.limit locals 2
.limit stack 5
.end method

.method public a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 2
instanceof android/support/v7/internal/view/menu/aa
ifeq L0
aload 2
checkcast android/support/v7/internal/view/menu/aa
astore 2
L1:
aload 0
aload 1
aload 2
invokevirtual android/support/v7/internal/view/menu/d/a(Landroid/support/v7/internal/view/menu/m;Landroid/support/v7/internal/view/menu/aa;)V
aload 2
checkcast android/view/View
areturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/d/d Landroid/view/LayoutInflater;
aload 0
getfield android/support/v7/internal/view/menu/d/j I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/support/v7/internal/view/menu/aa
astore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method public a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/d/b Landroid/content/Context;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/d/b Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/internal/view/menu/d/e Landroid/view/LayoutInflater;
aload 0
aload 2
putfield android/support/v7/internal/view/menu/d/c Landroid/support/v7/internal/view/menu/i;
return
.limit locals 3
.limit stack 2
.end method

.method public a(Landroid/support/v7/internal/view/menu/i;Z)V
aload 0
getfield android/support/v7/internal/view/menu/d/f Landroid/support/v7/internal/view/menu/y;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/d/f Landroid/support/v7/internal/view/menu/y;
aload 1
iload 2
invokeinterface android/support/v7/internal/view/menu/y/a(Landroid/support/v7/internal/view/menu/i;Z)V 2
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public abstract a(Landroid/support/v7/internal/view/menu/m;Landroid/support/v7/internal/view/menu/aa;)V
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/d/f Landroid/support/v7/internal/view/menu/y;
return
.limit locals 2
.limit stack 2
.end method

.method public a(Z)V
aload 0
getfield android/support/v7/internal/view/menu/d/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/ViewGroup
astore 7
aload 7
ifnonnull L0
L1:
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/d/c Landroid/support/v7/internal/view/menu/i;
ifnull L2
aload 0
getfield android/support/v7/internal/view/menu/d/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/i()V
aload 0
getfield android/support/v7/internal/view/menu/d/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/h()Ljava/util/ArrayList;
astore 8
aload 8
invokevirtual java/util/ArrayList/size()I
istore 5
iconst_0
istore 4
iconst_0
istore 2
L3:
iload 2
istore 3
iload 4
iload 5
if_icmpge L4
aload 8
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 9
aload 0
aload 9
invokevirtual android/support/v7/internal/view/menu/d/c(Landroid/support/v7/internal/view/menu/m;)Z
ifeq L5
aload 7
iload 2
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 10
aload 10
instanceof android/support/v7/internal/view/menu/aa
ifeq L6
aload 10
checkcast android/support/v7/internal/view/menu/aa
invokeinterface android/support/v7/internal/view/menu/aa/getItemData()Landroid/support/v7/internal/view/menu/m; 0
astore 6
L7:
aload 0
aload 9
aload 10
aload 7
invokevirtual android/support/v7/internal/view/menu/d/a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
astore 11
aload 9
aload 6
if_acmpeq L8
aload 11
iconst_0
invokevirtual android/view/View/setPressed(Z)V
aload 11
invokestatic android/support/v4/view/cx/v(Landroid/view/View;)V
L8:
aload 11
aload 10
if_acmpeq L9
aload 11
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
checkcast android/view/ViewGroup
astore 6
aload 6
ifnull L10
aload 6
aload 11
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
L10:
aload 0
getfield android/support/v7/internal/view/menu/d/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/ViewGroup
aload 11
iload 2
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;I)V
L9:
iload 2
iconst_1
iadd
istore 2
L11:
iload 4
iconst_1
iadd
istore 4
goto L3
L6:
aconst_null
astore 6
goto L7
L4:
iload 3
aload 7
invokevirtual android/view/ViewGroup/getChildCount()I
if_icmpge L1
aload 0
aload 7
iload 3
invokevirtual android/support/v7/internal/view/menu/d/a(Landroid/view/ViewGroup;I)Z
ifne L4
iload 3
iconst_1
iadd
istore 3
goto L4
L5:
goto L11
L2:
iconst_0
istore 3
goto L4
.limit locals 12
.limit stack 4
.end method

.method public a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public a(Landroid/support/v7/internal/view/menu/ad;)Z
aload 0
getfield android/support/v7/internal/view/menu/d/f Landroid/support/v7/internal/view/menu/y;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/d/f Landroid/support/v7/internal/view/menu/y;
aload 1
invokeinterface android/support/v7/internal/view/menu/y/a_(Landroid/support/v7/internal/view/menu/i;)Z 1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public a(Landroid/view/ViewGroup;I)Z
aload 1
iload 2
invokevirtual android/view/ViewGroup/removeViewAt(I)V
iconst_1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final b()I
aload 0
getfield android/support/v7/internal/view/menu/d/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public c(Landroid/support/v7/internal/view/menu/m;)Z
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method
