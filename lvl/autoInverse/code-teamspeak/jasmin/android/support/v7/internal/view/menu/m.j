.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/menu/m
.super java/lang/Object
.implements android/support/v4/g/a/b

.field private static final 'F' I = 1


.field private static final 'G' I = 2


.field private static final 'H' I = 4


.field private static final 'I' I = 8


.field private static final 'J' I = 16


.field private static final 'K' I = 32


.field static final 'j' I = 0


.field static 'l' Ljava/lang/String;

.field static 'm' Ljava/lang/String;

.field static 'n' Ljava/lang/String;

.field static 'o' Ljava/lang/String;

.field private static final 'p' Ljava/lang/String; = "MenuItemImpl"

.field private static final 'q' I = 3


.field private 'A' I

.field private 'B' Landroid/support/v7/internal/view/menu/ad;

.field private 'C' Ljava/lang/Runnable;

.field private 'D' Landroid/view/MenuItem$OnMenuItemClickListener;

.field private 'E' I

.field private 'L' Landroid/view/View;

.field private 'M' Landroid/support/v4/view/bf;

.field private 'N' Z

.field final 'f' I

.field 'g' Landroid/support/v7/internal/view/menu/i;

.field 'h' I

.field public 'i' Landroid/support/v4/view/n;

.field 'k' Landroid/view/ContextMenu$ContextMenuInfo;

.field private final 'r' I

.field private final 's' I

.field private final 't' I

.field private 'u' Ljava/lang/CharSequence;

.field private 'v' Ljava/lang/CharSequence;

.field private 'w' Landroid/content/Intent;

.field private 'x' C

.field private 'y' C

.field private 'z' Landroid/graphics/drawable/Drawable;

.method <init>(Landroid/support/v7/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/m/A I
aload 0
bipush 16
putfield android/support/v7/internal/view/menu/m/E I
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/m/h I
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/m/N Z
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
aload 0
iload 3
putfield android/support/v7/internal/view/menu/m/r I
aload 0
iload 2
putfield android/support/v7/internal/view/menu/m/s I
aload 0
iload 4
putfield android/support/v7/internal/view/menu/m/t I
aload 0
iload 5
putfield android/support/v7/internal/view/menu/m/f I
aload 0
aload 6
putfield android/support/v7/internal/view/menu/m/u Ljava/lang/CharSequence;
aload 0
iload 7
putfield android/support/v7/internal/view/menu/m/h I
return
.limit locals 8
.limit stack 2
.end method

.method private a(I)Landroid/support/v4/g/a/b;
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
astore 2
aload 0
aload 2
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
iload 1
new android/widget/LinearLayout
dup
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
invokespecial android/support/v7/internal/view/menu/m/a(Landroid/view/View;)Landroid/support/v4/g/a/b;
pop
aload 0
areturn
.limit locals 3
.limit stack 6
.end method

.method private a(Landroid/view/View;)Landroid/support/v4/g/a/b;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
aload 1
ifnull L0
aload 1
invokevirtual android/view/View/getId()I
iconst_m1
if_icmpne L0
aload 0
getfield android/support/v7/internal/view/menu/m/r I
ifle L0
aload 1
aload 0
getfield android/support/v7/internal/view/menu/m/r I
invokevirtual android/view/View/setId(I)V
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/g()V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Runnable;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/C Ljava/lang/Runnable;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/ContextMenu$ContextMenuInfo;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/k Landroid/view/ContextMenu$ContextMenuInfo;
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)Landroid/support/v4/g/a/b;
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/m/setShowAsAction(I)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private j()I
aload 0
getfield android/support/v7/internal/view/menu/m/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()Ljava/lang/Runnable;
aload 0
getfield android/support/v7/internal/view/menu/m/C Ljava/lang/Runnable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private l()Ljava/lang/String;
aload 0
invokevirtual android/support/v7/internal/view/menu/m/c()C
istore 1
iload 1
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
getstatic android/support/v7/internal/view/menu/m/l Ljava/lang/String;
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 2
iload 1
lookupswitch
8 : L1
10 : L2
32 : L3
default : L4
L4:
aload 2
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L5:
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L2:
aload 2
getstatic android/support/v7/internal/view/menu/m/m Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L5
L1:
aload 2
getstatic android/support/v7/internal/view/menu/m/n Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L5
L3:
aload 2
getstatic android/support/v7/internal/view/menu/m/o Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L5
.limit locals 3
.limit stack 3
.end method

.method private m()V
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/g()V
return
.limit locals 1
.limit stack 1
.end method

.method private n()Z
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/m Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private o()Z
aload 0
getfield android/support/v7/internal/view/menu/m/h I
iconst_4
iand
iconst_4
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/support/v4/view/bf;)Landroid/support/v4/g/a/b;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/M Landroid/support/v4/view/bf;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v4/view/n;)Landroid/support/v4/g/a/b;
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
astore 2
aload 2
aconst_null
putfield android/support/v4/view/n/b Landroid/support/v4/view/p;
aload 2
aconst_null
putfield android/support/v4/view/n/a Landroid/support/v4/view/o;
L0:
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
ifnull L1
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
new android/support/v7/internal/view/menu/n
dup
aload 0
invokespecial android/support/v7/internal/view/menu/n/<init>(Landroid/support/v7/internal/view/menu/m;)V
invokevirtual android/support/v4/view/n/a(Landroid/support/v4/view/p;)V
L1:
aload 0
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a()Landroid/support/v4/view/n;
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a(Landroid/support/v7/internal/view/menu/aa;)Ljava/lang/CharSequence;
aload 1
ifnull L0
aload 1
invokeinterface android/support/v7/internal/view/menu/aa/a()Z 0
ifeq L0
aload 0
invokevirtual android/support/v7/internal/view/menu/m/getTitleCondensed()Ljava/lang/CharSequence;
areturn
L0:
aload 0
invokevirtual android/support/v7/internal/view/menu/m/getTitle()Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/B Landroid/support/v7/internal/view/menu/ad;
aload 1
aload 0
invokevirtual android/support/v7/internal/view/menu/m/getTitle()Ljava/lang/CharSequence;
invokevirtual android/support/v7/internal/view/menu/ad/setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
getfield android/support/v7/internal/view/menu/m/E I
istore 3
iload 1
ifeq L0
iconst_4
istore 2
L1:
aload 0
iload 2
iload 3
bipush -5
iand
ior
putfield android/support/v7/internal/view/menu/m/E I
return
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method final b(Z)V
aload 0
getfield android/support/v7/internal/view/menu/m/E I
istore 3
aload 0
getfield android/support/v7/internal/view/menu/m/E I
istore 4
iload 1
ifeq L0
iconst_2
istore 2
L1:
aload 0
iload 2
iload 4
bipush -3
iand
ior
putfield android/support/v7/internal/view/menu/m/E I
iload 3
aload 0
getfield android/support/v7/internal/view/menu/m/E I
if_icmpeq L2
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
L2:
return
L0:
iconst_0
istore 2
goto L1
.limit locals 5
.limit stack 4
.end method

.method public final b()Z
.catch android/content/ActivityNotFoundException from L0 to L1 using L2
aload 0
getfield android/support/v7/internal/view/menu/m/D Landroid/view/MenuItem$OnMenuItemClickListener;
ifnull L3
aload 0
getfield android/support/v7/internal/view/menu/m/D Landroid/view/MenuItem$OnMenuItemClickListener;
aload 0
invokeinterface android/view/MenuItem$OnMenuItemClickListener/onMenuItemClick(Landroid/view/MenuItem;)Z 1
ifeq L3
L4:
iconst_1
ireturn
L3:
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/k()Landroid/support/v7/internal/view/menu/i;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
ifne L4
aload 0
getfield android/support/v7/internal/view/menu/m/C Ljava/lang/Runnable;
ifnull L5
aload 0
getfield android/support/v7/internal/view/menu/m/C Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
iconst_1
ireturn
L5:
aload 0
getfield android/support/v7/internal/view/menu/m/w Landroid/content/Intent;
ifnull L6
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
aload 0
getfield android/support/v7/internal/view/menu/m/w Landroid/content/Intent;
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L1:
iconst_1
ireturn
L2:
astore 1
ldc "MenuItemImpl"
ldc "Can't find activity to handle intent; ignoring"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L6:
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
ifnull L7
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
invokevirtual android/support/v4/view/n/e()Z
ifne L4
L7:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method final c()C
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/b()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/m/y C
ireturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/x C
ireturn
.limit locals 1
.limit stack 1
.end method

.method final c(Z)Z
iconst_0
istore 5
aload 0
getfield android/support/v7/internal/view/menu/m/E I
istore 3
aload 0
getfield android/support/v7/internal/view/menu/m/E I
istore 4
iload 1
ifeq L0
iconst_0
istore 2
L1:
aload 0
iload 2
iload 4
bipush -9
iand
ior
putfield android/support/v7/internal/view/menu/m/E I
iload 5
istore 1
iload 3
aload 0
getfield android/support/v7/internal/view/menu/m/E I
if_icmpeq L2
iconst_1
istore 1
L2:
iload 1
ireturn
L0:
bipush 8
istore 2
goto L1
.limit locals 6
.limit stack 4
.end method

.method public final collapseActionView()Z
aload 0
getfield android/support/v7/internal/view/menu/m/h I
bipush 8
iand
ifne L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
ifnonnull L2
iconst_1
ireturn
L2:
aload 0
getfield android/support/v7/internal/view/menu/m/M Landroid/support/v4/view/bf;
ifnull L3
aload 0
getfield android/support/v7/internal/view/menu/m/M Landroid/support/v4/view/bf;
aload 0
invokeinterface android/support/v4/view/bf/b(Landroid/view/MenuItem;)Z 1
ifeq L1
L3:
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/b(Landroid/support/v7/internal/view/menu/m;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final d(Z)V
iload 1
ifeq L0
aload 0
aload 0
getfield android/support/v7/internal/view/menu/m/E I
bipush 32
ior
putfield android/support/v7/internal/view/menu/m/E I
return
L0:
aload 0
aload 0
getfield android/support/v7/internal/view/menu/m/E I
bipush -33
iand
putfield android/support/v7/internal/view/menu/m/E I
return
.limit locals 2
.limit stack 3
.end method

.method final d()Z
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/c()Z
ifeq L0
aload 0
invokevirtual android/support/v7/internal/view/menu/m/c()C
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final e(Z)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/m/N Z
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final e()Z
aload 0
getfield android/support/v7/internal/view/menu/m/E I
iconst_4
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final expandActionView()Z
aload 0
invokevirtual android/support/v7/internal/view/menu/m/i()Z
ifne L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/M Landroid/support/v4/view/bf;
ifnull L2
aload 0
getfield android/support/v7/internal/view/menu/m/M Landroid/support/v4/view/bf;
aload 0
invokeinterface android/support/v4/view/bf/a(Landroid/view/MenuItem;)Z 1
ifeq L1
L2:
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/m;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final f()Z
aload 0
getfield android/support/v7/internal/view/menu/m/E I
bipush 32
iand
bipush 32
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final g()Z
aload 0
getfield android/support/v7/internal/view/menu/m/h I
iconst_1
iand
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
new java/lang/UnsupportedOperationException
dup
ldc "This is not supported, use MenuItemCompat.getActionProvider()"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final getActionView()Landroid/view/View;
aload 0
getfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
areturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
ifnull L1
aload 0
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
aload 0
invokevirtual android/support/v4/view/n/a(Landroid/view/MenuItem;)Landroid/view/View;
putfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
aload 0
getfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
areturn
L1:
aconst_null
areturn
.limit locals 1
.limit stack 3
.end method

.method public final getAlphabeticShortcut()C
aload 0
getfield android/support/v7/internal/view/menu/m/y C
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getGroupId()I
aload 0
getfield android/support/v7/internal/view/menu/m/s I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/view/menu/m/z Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/m/z Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/A I
ifeq L1
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
aload 0
getfield android/support/v7/internal/view/menu/m/A I
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
astore 1
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/m/A I
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/z Landroid/graphics/drawable/Drawable;
aload 1
areturn
L1:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getIntent()Landroid/content/Intent;
aload 0
getfield android/support/v7/internal/view/menu/m/w Landroid/content/Intent;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getItemId()I
.annotation visible Landroid/view/ViewDebug$CapturedViewProperty;
.end annotation
aload 0
getfield android/support/v7/internal/view/menu/m/r I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
aload 0
getfield android/support/v7/internal/view/menu/m/k Landroid/view/ContextMenu$ContextMenuInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getNumericShortcut()C
aload 0
getfield android/support/v7/internal/view/menu/m/x C
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getOrder()I
aload 0
getfield android/support/v7/internal/view/menu/m/t I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/m/B Landroid/support/v7/internal/view/menu/ad;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTitle()Ljava/lang/CharSequence;
.annotation visible Landroid/view/ViewDebug$CapturedViewProperty;
.end annotation
aload 0
getfield android/support/v7/internal/view/menu/m/u Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/menu/m/v Ljava/lang/CharSequence;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/m/v Ljava/lang/CharSequence;
astore 1
L1:
aload 1
astore 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmpge L2
aload 1
astore 2
aload 1
ifnull L2
aload 1
astore 2
aload 1
instanceof java/lang/String
ifne L2
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
astore 2
L2:
aload 2
areturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/u Ljava/lang/CharSequence;
astore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final h()Z
aload 0
getfield android/support/v7/internal/view/menu/m/h I
iconst_2
iand
iconst_2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final hasSubMenu()Z
aload 0
getfield android/support/v7/internal/view/menu/m/B Landroid/support/v7/internal/view/menu/ad;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Z
iconst_0
istore 2
iload 2
istore 1
aload 0
getfield android/support/v7/internal/view/menu/m/h I
bipush 8
iand
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
ifnonnull L1
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
ifnull L1
aload 0
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
aload 0
invokevirtual android/support/v4/view/n/a(Landroid/view/MenuItem;)Landroid/view/View;
putfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
L1:
iload 2
istore 1
aload 0
getfield android/support/v7/internal/view/menu/m/L Landroid/view/View;
ifnull L0
iconst_1
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final isActionViewExpanded()Z
aload 0
getfield android/support/v7/internal/view/menu/m/N Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isCheckable()Z
aload 0
getfield android/support/v7/internal/view/menu/m/E I
iconst_1
iand
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final isChecked()Z
aload 0
getfield android/support/v7/internal/view/menu/m/E I
iconst_2
iand
iconst_2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final isEnabled()Z
aload 0
getfield android/support/v7/internal/view/menu/m/E I
bipush 16
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final isVisible()Z
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
invokevirtual android/support/v4/view/n/b()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/m/E I
bipush 8
iand
ifne L1
aload 0
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
invokevirtual android/support/v4/view/n/c()Z
ifeq L1
L2:
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/m/E I
bipush 8
iand
ifeq L2
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
new java/lang/UnsupportedOperationException
dup
ldc "This is not supported, use MenuItemCompat.setActionProvider()"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final synthetic setActionView(I)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
astore 2
aload 0
aload 2
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
iload 1
new android/widget/LinearLayout
dup
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
invokespecial android/support/v7/internal/view/menu/m/a(Landroid/view/View;)Landroid/support/v4/g/a/b;
pop
aload 0
areturn
.limit locals 3
.limit stack 6
.end method

.method public final synthetic setActionView(Landroid/view/View;)Landroid/view/MenuItem;
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/m/a(Landroid/view/View;)Landroid/support/v4/g/a/b;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/m/y C
iload 1
if_icmpne L0
aload 0
areturn
L0:
aload 0
iload 1
invokestatic java/lang/Character/toLowerCase(C)C
putfield android/support/v7/internal/view/menu/m/y C
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/m/E I
istore 3
aload 0
getfield android/support/v7/internal/view/menu/m/E I
istore 4
iload 1
ifeq L0
iconst_1
istore 2
L1:
aload 0
iload 2
iload 4
bipush -2
iand
ior
putfield android/support/v7/internal/view/menu/m/E I
iload 3
aload 0
getfield android/support/v7/internal/view/menu/m/E I
if_icmpeq L2
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
L2:
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 5
.limit stack 4
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/m/E I
iconst_4
iand
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
astore 5
aload 0
invokeinterface android/view/MenuItem/getGroupId()I 0
istore 3
aload 5
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
iconst_0
istore 2
L1:
iload 2
iload 4
if_icmpge L2
aload 5
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 6
aload 6
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 3
if_icmpne L3
aload 6
invokevirtual android/support/v7/internal/view/menu/m/e()Z
ifeq L3
aload 6
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
ifeq L3
aload 6
aload 0
if_acmpne L4
iconst_1
istore 1
L5:
aload 6
iload 1
invokevirtual android/support/v7/internal/view/menu/m/b(Z)V
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L4:
iconst_0
istore 1
goto L5
L0:
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/m/b(Z)V
L2:
aload 0
areturn
.limit locals 7
.limit stack 2
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
iload 1
ifeq L0
aload 0
aload 0
getfield android/support/v7/internal/view/menu/m/E I
bipush 16
ior
putfield android/support/v7/internal/view/menu/m/E I
L1:
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
areturn
L0:
aload 0
aload 0
getfield android/support/v7/internal/view/menu/m/E I
bipush -17
iand
putfield android/support/v7/internal/view/menu/m/E I
goto L1
.limit locals 2
.limit stack 3
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/m/z Landroid/graphics/drawable/Drawable;
aload 0
iload 1
putfield android/support/v7/internal/view/menu/m/A I
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/m/A I
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/z Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/w Landroid/content/Intent;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/m/x C
iload 1
if_icmpne L0
aload 0
areturn
L0:
aload 0
iload 1
putfield android/support/v7/internal/view/menu/m/x C
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
new java/lang/UnsupportedOperationException
dup
ldc "This is not supported, use MenuItemCompat.setOnActionExpandListener()"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/D Landroid/view/MenuItem$OnMenuItemClickListener;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
aload 0
iload 1
putfield android/support/v7/internal/view/menu/m/x C
aload 0
iload 2
invokestatic java/lang/Character/toLowerCase(C)C
putfield android/support/v7/internal/view/menu/m/y C
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final setShowAsAction(I)V
iload 1
iconst_3
iand
tableswitch 0
L0
L0
L0
default : L1
L1:
new java/lang/IllegalArgumentException
dup
ldc "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield android/support/v7/internal/view/menu/m/h I
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/g()V
return
.limit locals 2
.limit stack 3
.end method

.method public final synthetic setShowAsActionFlags(I)Landroid/view/MenuItem;
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/m/setShowAsAction(I)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/view/menu/m/setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/u Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
getfield android/support/v7/internal/view/menu/m/B Landroid/support/v7/internal/view/menu/ad;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/m/B Landroid/support/v7/internal/view/menu/ad;
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
pop
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/m/v Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/m/c(Z)Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/f()V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield android/support/v7/internal/view/menu/m/u Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
areturn
.limit locals 1
.limit stack 1
.end method
