.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/aa
.super android/widget/PopupWindow

.field private static final 'a' Ljava/lang/String; = "AppCompatPopupWindow"

.field private static final 'b' Z

.field private 'c' Z

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L0
iconst_1
istore 0
L1:
iload 0
putstatic android/support/v7/internal/widget/aa/b Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
aload 1
aload 2
iload 3
invokespecial android/widget/PopupWindow/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 1
aload 2
getstatic android/support/v7/a/n/PopupWindow [I
iload 3
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 1
getstatic android/support/v7/a/n/PopupWindow_overlapAnchor I
invokevirtual android/support/v7/internal/widget/ax/e(I)Z
ifeq L3
aload 1
getstatic android/support/v7/a/n/PopupWindow_overlapAnchor I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(IZ)Z
istore 4
getstatic android/support/v7/internal/widget/aa/b Z
ifeq L4
aload 0
iload 4
putfield android/support/v7/internal/widget/aa/c Z
L3:
aload 0
aload 1
getstatic android/support/v7/a/n/PopupWindow_android_popupBackground I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/aa/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmpge L1
L0:
ldc android/widget/PopupWindow
ldc "mAnchor"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 1
aload 1
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
ldc android/widget/PopupWindow
ldc "mOnScrollChangedListener"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 2
aload 2
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
aload 2
aload 0
new android/support/v7/internal/widget/ab
dup
aload 1
aload 0
aload 2
aload 0
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/ViewTreeObserver$OnScrollChangedListener
invokespecial android/support/v7/internal/widget/ab/<init>(Ljava/lang/reflect/Field;Landroid/widget/PopupWindow;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L1:
return
L4:
aload 0
iload 4
invokestatic android/support/v4/widget/bo/a(Landroid/widget/PopupWindow;Z)V
goto L3
L2:
astore 1
ldc "AppCompatPopupWindow"
ldc "Exception while installing workaround OnScrollChangedListener"
aload 1
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 5
.limit stack 8
.end method

.method private static a(Landroid/widget/PopupWindow;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
ldc android/widget/PopupWindow
ldc "mAnchor"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 1
aload 1
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
ldc android/widget/PopupWindow
ldc "mOnScrollChangedListener"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 2
aload 2
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
aload 2
aload 0
new android/support/v7/internal/widget/ab
dup
aload 1
aload 0
aload 2
aload 0
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/ViewTreeObserver$OnScrollChangedListener
invokespecial android/support/v7/internal/widget/ab/<init>(Ljava/lang/reflect/Field;Landroid/widget/PopupWindow;Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L1:
return
L2:
astore 0
ldc "AppCompatPopupWindow"
ldc "Exception while installing workaround OnScrollChangedListener"
aload 0
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 3
.limit stack 8
.end method

.method private a(Z)V
getstatic android/support/v7/internal/widget/aa/b Z
ifeq L0
aload 0
iload 1
putfield android/support/v7/internal/widget/aa/c Z
return
L0:
aload 0
iload 1
invokestatic android/support/v4/widget/bo/a(Landroid/widget/PopupWindow;Z)V
return
.limit locals 2
.limit stack 2
.end method

.method private a()Z
getstatic android/support/v7/internal/widget/aa/b Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/aa/c Z
ireturn
L0:
aload 0
invokestatic android/support/v4/widget/bo/a(Landroid/widget/PopupWindow;)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final showAsDropDown(Landroid/view/View;II)V
iload 3
istore 4
getstatic android/support/v7/internal/widget/aa/b Z
ifeq L0
iload 3
istore 4
aload 0
getfield android/support/v7/internal/widget/aa/c Z
ifeq L0
iload 3
aload 1
invokevirtual android/view/View/getHeight()I
isub
istore 4
L0:
aload 0
aload 1
iload 2
iload 4
invokespecial android/widget/PopupWindow/showAsDropDown(Landroid/view/View;II)V
return
.limit locals 5
.limit stack 4
.end method

.method public final showAsDropDown(Landroid/view/View;III)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 19
.end annotation
iload 3
istore 5
getstatic android/support/v7/internal/widget/aa/b Z
ifeq L0
iload 3
istore 5
aload 0
getfield android/support/v7/internal/widget/aa/c Z
ifeq L0
iload 3
aload 1
invokevirtual android/view/View/getHeight()I
isub
istore 5
L0:
aload 0
aload 1
iload 2
iload 5
iload 4
invokespecial android/widget/PopupWindow/showAsDropDown(Landroid/view/View;III)V
return
.limit locals 6
.limit stack 5
.end method

.method public final update(Landroid/view/View;IIII)V
getstatic android/support/v7/internal/widget/aa/b Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/aa/c Z
ifeq L0
iload 3
aload 1
invokevirtual android/view/View/getHeight()I
isub
istore 3
L1:
aload 0
aload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/PopupWindow/update(Landroid/view/View;IIII)V
return
L0:
goto L1
.limit locals 6
.limit stack 6
.end method
