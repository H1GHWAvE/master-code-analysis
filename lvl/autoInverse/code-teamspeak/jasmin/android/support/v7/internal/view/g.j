.bytecode 50.0
.class final synchronized android/support/v7/internal/view/g
.super java/lang/Object
.implements android/view/MenuItem$OnMenuItemClickListener

.field private static final 'a' [Ljava/lang/Class;

.field private 'b' Ljava/lang/Object;

.field private 'c' Ljava/lang/reflect/Method;

.method static <clinit>()V
iconst_1
anewarray java/lang/Class
dup
iconst_0
ldc android/view/MenuItem
aastore
putstatic android/support/v7/internal/view/g/a [Ljava/lang/Class;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Ljava/lang/Object;Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/internal/view/g/b Ljava/lang/Object;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
astore 3
L0:
aload 0
aload 3
aload 2
getstatic android/support/v7/internal/view/g/a [Ljava/lang/Class;
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v7/internal/view/g/c Ljava/lang/reflect/Method;
L1:
return
L2:
astore 1
new android/view/InflateException
dup
new java/lang/StringBuilder
dup
ldc "Couldn't resolve menu item onClick handler "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " in class "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/view/InflateException/<init>(Ljava/lang/String;)V
astore 2
aload 2
aload 1
invokevirtual android/view/InflateException/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
pop
aload 2
athrow
.limit locals 4
.limit stack 5
.end method

.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
L0:
aload 0
getfield android/support/v7/internal/view/g/c Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/getReturnType()Ljava/lang/Class;
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
if_acmpne L1
aload 0
getfield android/support/v7/internal/view/g/c Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/internal/view/g/b Ljava/lang/Object;
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
ireturn
L1:
aload 0
getfield android/support/v7/internal/view/g/c Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/internal/view/g/b Ljava/lang/Object;
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L3:
iconst_1
ireturn
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 6
.end method
