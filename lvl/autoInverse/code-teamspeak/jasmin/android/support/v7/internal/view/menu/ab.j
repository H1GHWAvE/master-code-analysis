.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/menu/ab
.super java/lang/Object

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/g/a/a;)Landroid/view/Menu;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
new android/support/v7/internal/view/menu/ac
dup
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/ac/<init>(Landroid/content/Context;Landroid/support/v4/g/a/a;)V
areturn
L0:
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 4
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/g/a/b;)Landroid/view/MenuItem;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v7/internal/view/menu/t
dup
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/t/<init>(Landroid/content/Context;Landroid/support/v4/g/a/b;)V
areturn
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L1
new android/support/v7/internal/view/menu/o
dup
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/o/<init>(Landroid/content/Context;Landroid/support/v4/g/a/b;)V
areturn
L1:
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/content/Context;Landroid/support/v4/g/a/c;)Landroid/view/SubMenu;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
new android/support/v7/internal/view/menu/ae
dup
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/ae/<init>(Landroid/content/Context;Landroid/support/v4/g/a/c;)V
areturn
L0:
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 4
.end method
