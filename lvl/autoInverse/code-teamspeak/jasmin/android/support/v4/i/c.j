.bytecode 50.0
.class public final synchronized android/support/v4/i/c
.super java/lang/Object

.field public 'a' Z

.field public 'b' Ljava/lang/Object;

.field public 'c' Z

.field private 'd' Landroid/support/v4/i/d;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/support/v4/i/d;)V
.catch all from L0 to L1 using L2
.catch java/lang/InterruptedException from L3 to L4 using L5
.catch all from L3 to L4 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
.catch all from L14 to L15 using L2
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/i/c/c Z
istore 2
L1:
iload 2
ifeq L6
L3:
aload 0
invokevirtual java/lang/Object/wait()V
L4:
goto L0
L5:
astore 3
goto L0
L6:
aload 0
getfield android/support/v4/i/c/d Landroid/support/v4/i/d;
aload 1
if_acmpne L8
aload 0
monitorexit
L7:
return
L8:
aload 0
aload 1
putfield android/support/v4/i/c/d Landroid/support/v4/i/d;
aload 0
getfield android/support/v4/i/c/a Z
ifeq L10
L9:
aload 1
ifnonnull L14
L10:
aload 0
monitorexit
L11:
return
L2:
astore 1
L12:
aload 0
monitorexit
L13:
aload 1
athrow
L14:
aload 0
monitorexit
L15:
return
.limit locals 4
.limit stack 2
.end method

.method private c()V
aload 0
invokevirtual android/support/v4/i/c/a()Z
ifeq L0
new android/support/v4/i/h
dup
invokespecial android/support/v4/i/h/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private d()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L7
.catch all from L8 to L9 using L10
.catch all from L11 to L12 using L10
.catch all from L13 to L14 using L2
.catch all from L15 to L16 using L17
.catch all from L18 to L19 using L17
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/i/c/a Z
ifeq L3
aload 0
monitorexit
L1:
return
L3:
aload 0
iconst_1
putfield android/support/v4/i/c/a Z
aload 0
iconst_1
putfield android/support/v4/i/c/c Z
aload 0
getfield android/support/v4/i/c/b Ljava/lang/Object;
astore 1
aload 0
monitorexit
L4:
aload 1
ifnull L6
L5:
aload 1
checkcast android/os/CancellationSignal
invokevirtual android/os/CancellationSignal/cancel()V
L6:
aload 0
monitorenter
L8:
aload 0
iconst_0
putfield android/support/v4/i/c/c Z
aload 0
invokevirtual java/lang/Object/notifyAll()V
aload 0
monitorexit
L9:
return
L10:
astore 1
L11:
aload 0
monitorexit
L12:
aload 1
athrow
L2:
astore 1
L13:
aload 0
monitorexit
L14:
aload 1
athrow
L7:
astore 1
aload 0
monitorenter
L15:
aload 0
iconst_0
putfield android/support/v4/i/c/c Z
aload 0
invokevirtual java/lang/Object/notifyAll()V
aload 0
monitorexit
L16:
aload 1
athrow
L17:
astore 1
L18:
aload 0
monitorexit
L19:
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method private e()V
.catch java/lang/InterruptedException from L0 to L1 using L2
L3:
aload 0
getfield android/support/v4/i/c/c Z
ifeq L4
L0:
aload 0
invokevirtual java/lang/Object/wait()V
L1:
goto L3
L2:
astore 1
goto L3
L4:
return
.limit locals 2
.limit stack 1
.end method

.method public final a()Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/i/c/a Z
istore 1
aload 0
monitorexit
L1:
iload 1
ireturn
L2:
astore 2
L3:
aload 0
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final b()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmpge L6
aconst_null
areturn
L6:
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/i/c/b Ljava/lang/Object;
ifnonnull L1
aload 0
new android/os/CancellationSignal
dup
invokespecial android/os/CancellationSignal/<init>()V
putfield android/support/v4/i/c/b Ljava/lang/Object;
aload 0
getfield android/support/v4/i/c/a Z
ifeq L1
aload 0
getfield android/support/v4/i/c/b Ljava/lang/Object;
checkcast android/os/CancellationSignal
invokevirtual android/os/CancellationSignal/cancel()V
L1:
aload 0
getfield android/support/v4/i/c/b Ljava/lang/Object;
astore 1
aload 0
monitorexit
L3:
aload 1
areturn
L2:
astore 1
L4:
aload 0
monitorexit
L5:
aload 1
athrow
.limit locals 2
.limit stack 3
.end method
