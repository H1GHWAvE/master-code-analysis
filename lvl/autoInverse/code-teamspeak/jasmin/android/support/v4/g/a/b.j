.bytecode 50.0
.class public abstract interface android/support/v4/g/a/b
.super java/lang/Object
.implements android/view/MenuItem

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 4


.field public static final 'e' I = 8


.method public abstract a(Landroid/support/v4/view/bf;)Landroid/support/v4/g/a/b;
.end method

.method public abstract a(Landroid/support/v4/view/n;)Landroid/support/v4/g/a/b;
.end method

.method public abstract a()Landroid/support/v4/view/n;
.end method

.method public abstract collapseActionView()Z
.end method

.method public abstract expandActionView()Z
.end method

.method public abstract getActionView()Landroid/view/View;
.end method

.method public abstract isActionViewExpanded()Z
.end method

.method public abstract setActionView(I)Landroid/view/MenuItem;
.end method

.method public abstract setActionView(Landroid/view/View;)Landroid/view/MenuItem;
.end method

.method public abstract setShowAsAction(I)V
.end method

.method public abstract setShowAsActionFlags(I)Landroid/view/MenuItem;
.end method
