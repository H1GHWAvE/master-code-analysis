.bytecode 50.0
.class final synchronized android/support/v4/b/j
.super java/lang/Object
.implements android/support/v4/b/l

.field final 'a' Landroid/animation/Animator;

.method public <init>(Landroid/animation/Animator;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/b/j/a Landroid/animation/Animator;
return
.limit locals 2
.limit stack 2
.end method

.method public final a()V
aload 0
getfield android/support/v4/b/j/a Landroid/animation/Animator;
invokevirtual android/animation/Animator/start()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(J)V
aload 0
getfield android/support/v4/b/j/a Landroid/animation/Animator;
lload 1
invokevirtual android/animation/Animator/setDuration(J)Landroid/animation/Animator;
pop
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/v4/b/b;)V
aload 0
getfield android/support/v4/b/j/a Landroid/animation/Animator;
new android/support/v4/b/i
dup
aload 1
aload 0
invokespecial android/support/v4/b/i/<init>(Landroid/support/v4/b/b;Landroid/support/v4/b/l;)V
invokevirtual android/animation/Animator/addListener(Landroid/animation/Animator$AnimatorListener;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Landroid/support/v4/b/d;)V
aload 0
getfield android/support/v4/b/j/a Landroid/animation/Animator;
instanceof android/animation/ValueAnimator
ifeq L0
aload 0
getfield android/support/v4/b/j/a Landroid/animation/Animator;
checkcast android/animation/ValueAnimator
new android/support/v4/b/k
dup
aload 0
aload 1
invokespecial android/support/v4/b/k/<init>(Landroid/support/v4/b/j;Landroid/support/v4/b/d;)V
invokevirtual android/animation/ValueAnimator/addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Landroid/view/View;)V
aload 0
getfield android/support/v4/b/j/a Landroid/animation/Animator;
aload 1
invokevirtual android/animation/Animator/setTarget(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final c()V
aload 0
getfield android/support/v4/b/j/a Landroid/animation/Animator;
invokevirtual android/animation/Animator/cancel()V
return
.limit locals 1
.limit stack 1
.end method

.method public final d()F
aload 0
getfield android/support/v4/b/j/a Landroid/animation/Animator;
checkcast android/animation/ValueAnimator
invokevirtual android/animation/ValueAnimator/getAnimatedFraction()F
freturn
.limit locals 1
.limit stack 1
.end method
