.bytecode 50.0
.class public final synchronized android/support/v4/a/a
.super java/lang/Object

.field public static final 'a' I = 1


.field public static final 'b' I = 2


.field public static final 'c' I = 4


.field public static final 'd' I = 8


.field public static final 'e' I = 32


.field public static final 'f' I = -1


.field public static final 'g' I = 1


.field public static final 'h' I = 2


.field public static final 'i' I = 4


.field public static final 'j' I = 8


.field public static final 'k' I = 16


.field public static final 'l' I = 32


.field private static final 'm' Landroid/support/v4/a/e;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L0
new android/support/v4/a/c
dup
invokespecial android/support/v4/a/c/<init>()V
putstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
return
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L1
new android/support/v4/a/b
dup
invokespecial android/support/v4/a/b/<init>()V
putstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
return
L1:
new android/support/v4/a/d
dup
invokespecial android/support/v4/a/d/<init>()V
putstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(I)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 2
aload 2
ldc "["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L0:
iload 0
ifle L1
iconst_1
iload 0
invokestatic java/lang/Integer/numberOfTrailingZeros(I)I
ishl
istore 1
iload 0
iload 1
iconst_m1
ixor
iand
istore 0
aload 2
invokevirtual java/lang/StringBuilder/length()I
iconst_1
if_icmple L2
aload 2
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L2:
iload 1
lookupswitch
1 : L3
2 : L4
4 : L5
8 : L6
16 : L7
default : L8
L8:
goto L0
L3:
aload 2
ldc "FEEDBACK_SPOKEN"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L5:
aload 2
ldc "FEEDBACK_AUDIBLE"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L4:
aload 2
ldc "FEEDBACK_HAPTIC"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L7:
aload 2
ldc "FEEDBACK_GENERIC"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L6:
aload 2
ldc "FEEDBACK_VISUAL"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L1:
aload 2
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String;
getstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
aload 0
invokeinterface android/support/v4/a/e/c(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Landroid/accessibilityservice/AccessibilityServiceInfo;)Landroid/content/pm/ResolveInfo;
getstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
aload 0
invokeinterface android/support/v4/a/e/d(Landroid/accessibilityservice/AccessibilityServiceInfo;)Landroid/content/pm/ResolveInfo; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static b(I)Ljava/lang/String;
iload 0
lookupswitch
1 : L0
2 : L1
4 : L2
8 : L3
16 : L4
32 : L5
default : L6
L6:
aconst_null
areturn
L0:
ldc "DEFAULT"
areturn
L1:
ldc "FLAG_INCLUDE_NOT_IMPORTANT_VIEWS"
areturn
L2:
ldc "FLAG_REQUEST_TOUCH_EXPLORATION_MODE"
areturn
L3:
ldc "FLAG_REQUEST_ENHANCED_WEB_ACCESSIBILITY"
areturn
L4:
ldc "FLAG_REPORT_VIEW_IDS"
areturn
L5:
ldc "FLAG_REQUEST_FILTER_KEY_EVENTS"
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(I)Ljava/lang/String;
iload 0
tableswitch 1
L0
L1
L2
L3
L2
L2
L2
L4
default : L2
L2:
ldc "UNKNOWN"
areturn
L0:
ldc "CAPABILITY_CAN_RETRIEVE_WINDOW_CONTENT"
areturn
L1:
ldc "CAPABILITY_CAN_REQUEST_TOUCH_EXPLORATION"
areturn
L3:
ldc "CAPABILITY_CAN_REQUEST_ENHANCED_WEB_ACCESSIBILITY"
areturn
L4:
ldc "CAPABILITY_CAN_FILTER_KEY_EVENTS"
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String;
getstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
aload 0
invokeinterface android/support/v4/a/e/e(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/accessibilityservice/AccessibilityServiceInfo;)Z
getstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
aload 0
invokeinterface android/support/v4/a/e/a(Landroid/accessibilityservice/AccessibilityServiceInfo;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String;
getstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
aload 0
invokeinterface android/support/v4/a/e/b(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static f(Landroid/accessibilityservice/AccessibilityServiceInfo;)I
getstatic android/support/v4/a/a/m Landroid/support/v4/a/e;
aload 0
invokeinterface android/support/v4/a/e/f(Landroid/accessibilityservice/AccessibilityServiceInfo;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method
