.bytecode 50.0
.class final synchronized android/support/v4/widget/bv
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "PopupWindowCompatApi21"

.field private static 'b' Ljava/lang/reflect/Field;

.method static <clinit>()V
.catch java/lang/NoSuchFieldException from L0 to L1 using L2
L0:
ldc android/widget/PopupWindow
ldc "mOverlapAnchor"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 0
aload 0
putstatic android/support/v4/widget/bv/b Ljava/lang/reflect/Field;
aload 0
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
return
L2:
astore 0
ldc "PopupWindowCompatApi21"
ldc "Could not fetch mOverlapAnchor field from PopupWindow"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 1
.limit stack 3
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/widget/PopupWindow;Z)V
.catch java/lang/IllegalAccessException from L0 to L1 using L2
getstatic android/support/v4/widget/bv/b Ljava/lang/reflect/Field;
ifnull L1
L0:
getstatic android/support/v4/widget/bv/b Ljava/lang/reflect/Field;
aload 0
iload 1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L1:
return
L2:
astore 0
ldc "PopupWindowCompatApi21"
ldc "Could not set overlap anchor field in PopupWindow"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 2
.limit stack 3
.end method

.method static a(Landroid/widget/PopupWindow;)Z
.catch java/lang/IllegalAccessException from L0 to L1 using L2
getstatic android/support/v4/widget/bv/b Ljava/lang/reflect/Field;
ifnull L3
L0:
getstatic android/support/v4/widget/bv/b Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
istore 1
L1:
iload 1
ireturn
L2:
astore 0
ldc "PopupWindowCompatApi21"
ldc "Could not get overlap anchor field in PopupWindow"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L3:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method
