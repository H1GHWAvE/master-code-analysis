.bytecode 50.0
.class synchronized android/support/v4/widget/ck
.super android/support/v4/widget/cp

.method <init>()V
aload 0
invokespecial android/support/v4/widget/cp/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public a(Landroid/content/Context;)Landroid/view/View;
new android/widget/SearchView
dup
aload 1
invokespecial android/widget/SearchView/<init>(Landroid/content/Context;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/view/View;)Ljava/lang/CharSequence;
aload 1
checkcast android/widget/SearchView
invokevirtual android/widget/SearchView/getQuery()Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/support/v4/widget/ci;)Ljava/lang/Object;
new android/support/v4/widget/cs
dup
new android/support/v4/widget/cm
dup
aload 0
aload 1
invokespecial android/support/v4/widget/cm/<init>(Landroid/support/v4/widget/ck;Landroid/support/v4/widget/ci;)V
invokespecial android/support/v4/widget/cs/<init>(Landroid/support/v4/widget/ct;)V
areturn
.limit locals 2
.limit stack 6
.end method

.method public final a(Landroid/support/v4/widget/cj;)Ljava/lang/Object;
new android/support/v4/widget/cr
dup
new android/support/v4/widget/cl
dup
aload 0
aload 1
invokespecial android/support/v4/widget/cl/<init>(Landroid/support/v4/widget/ck;Landroid/support/v4/widget/cj;)V
invokespecial android/support/v4/widget/cr/<init>(Landroid/support/v4/widget/cu;)V
areturn
.limit locals 2
.limit stack 6
.end method

.method public final a(Landroid/view/View;I)V
aload 1
checkcast android/widget/SearchView
iload 2
invokevirtual android/widget/SearchView/setMaxWidth(I)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/content/ComponentName;)V
aload 1
checkcast android/widget/SearchView
astore 1
aload 1
aload 1
invokevirtual android/widget/SearchView/getContext()Landroid/content/Context;
ldc "search"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/SearchManager
aload 2
invokevirtual android/app/SearchManager/getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;
invokevirtual android/widget/SearchView/setSearchableInfo(Landroid/app/SearchableInfo;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/view/View;Ljava/lang/CharSequence;)V
aload 1
checkcast android/widget/SearchView
aload 2
invokevirtual android/widget/SearchView/setQueryHint(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;Ljava/lang/CharSequence;Z)V
aload 1
checkcast android/widget/SearchView
aload 2
iload 3
invokevirtual android/widget/SearchView/setQuery(Ljava/lang/CharSequence;Z)V
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Landroid/view/View;Z)V
aload 1
checkcast android/widget/SearchView
iload 2
invokevirtual android/widget/SearchView/setIconified(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 1
checkcast android/widget/SearchView
aload 2
checkcast android/widget/SearchView$OnQueryTextListener
invokevirtual android/widget/SearchView/setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/view/View;Z)V
aload 1
checkcast android/widget/SearchView
iload 2
invokevirtual android/widget/SearchView/setSubmitButtonEnabled(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
aload 1
checkcast android/widget/SearchView
aload 2
checkcast android/widget/SearchView$OnCloseListener
invokevirtual android/widget/SearchView/setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/view/View;)Z
aload 1
checkcast android/widget/SearchView
invokevirtual android/widget/SearchView/isIconified()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final c(Landroid/view/View;Z)V
aload 1
checkcast android/widget/SearchView
iload 2
invokevirtual android/widget/SearchView/setQueryRefinementEnabled(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method public final c(Landroid/view/View;)Z
aload 1
checkcast android/widget/SearchView
invokevirtual android/widget/SearchView/isSubmitButtonEnabled()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final d(Landroid/view/View;)Z
aload 1
checkcast android/widget/SearchView
invokevirtual android/widget/SearchView/isQueryRefinementEnabled()Z
ireturn
.limit locals 2
.limit stack 1
.end method
