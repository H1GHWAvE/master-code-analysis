.bytecode 50.0
.class public synchronized android/support/v4/widget/Space
.super android/view/View

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v4/widget/Space/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v4/widget/Space/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/view/View/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
invokevirtual android/support/v4/widget/Space/getVisibility()I
ifne L0
aload 0
iconst_4
invokevirtual android/support/v4/widget/Space/setVisibility(I)V
L0:
return
.limit locals 4
.limit stack 4
.end method

.method private static a(II)I
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 2
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 1
iload 2
lookupswitch
-2147483648 : L0
0 : L1
1073741824 : L2
default : L1
L1:
iload 0
ireturn
L0:
iload 0
iload 1
invokestatic java/lang/Math/min(II)I
ireturn
L2:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public draw(Landroid/graphics/Canvas;)V
return
.limit locals 2
.limit stack 0
.end method

.method protected onMeasure(II)V
aload 0
aload 0
invokevirtual android/support/v4/widget/Space/getSuggestedMinimumWidth()I
iload 1
invokestatic android/support/v4/widget/Space/a(II)I
aload 0
invokevirtual android/support/v4/widget/Space/getSuggestedMinimumHeight()I
iload 2
invokestatic android/support/v4/widget/Space/a(II)I
invokevirtual android/support/v4/widget/Space/setMeasuredDimension(II)V
return
.limit locals 3
.limit stack 4
.end method
