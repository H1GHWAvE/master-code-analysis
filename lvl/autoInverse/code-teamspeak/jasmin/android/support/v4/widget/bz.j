.bytecode 50.0
.class public synchronized abstract android/support/v4/widget/bz
.super android/support/v4/widget/r

.field private 'l' I

.field private 'm' I

.field private 'n' Landroid/view/LayoutInflater;

.method public <init>(Landroid/content/Context;I)V
aload 0
aload 1
invokespecial android/support/v4/widget/r/<init>(Landroid/content/Context;)V
aload 0
iload 2
putfield android/support/v4/widget/bz/m I
aload 0
iload 2
putfield android/support/v4/widget/bz/l I
aload 0
aload 1
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
putfield android/support/v4/widget/bz/n Landroid/view/LayoutInflater;
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
aload 1
aload 3
invokespecial android/support/v4/widget/r/<init>(Landroid/content/Context;Landroid/database/Cursor;)V
aload 0
iload 2
putfield android/support/v4/widget/bz/m I
aload 0
iload 2
putfield android/support/v4/widget/bz/l I
aload 0
aload 1
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
putfield android/support/v4/widget/bz/n Landroid/view/LayoutInflater;
return
.limit locals 4
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
aload 0
aload 1
aload 3
iload 4
invokespecial android/support/v4/widget/r/<init>(Landroid/content/Context;Landroid/database/Cursor;I)V
aload 0
iload 2
putfield android/support/v4/widget/bz/m I
aload 0
iload 2
putfield android/support/v4/widget/bz/l I
aload 0
aload 1
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
putfield android/support/v4/widget/bz/n Landroid/view/LayoutInflater;
return
.limit locals 5
.limit stack 4
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v4/widget/bz/l I
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)V
aload 0
iload 1
putfield android/support/v4/widget/bz/m I
return
.limit locals 2
.limit stack 2
.end method

.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield android/support/v4/widget/bz/n Landroid/view/LayoutInflater;
aload 0
getfield android/support/v4/widget/bz/l I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final b(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield android/support/v4/widget/bz/n Landroid/view/LayoutInflater;
aload 0
getfield android/support/v4/widget/bz/m I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
areturn
.limit locals 4
.limit stack 4
.end method
