.bytecode 50.0
.class synchronized android/support/v4/widget/i
.super java/lang/Object
.implements android/support/v4/widget/j

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/widget/m/a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 2
.limit stack 1
.end method

.method public a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
aload 1
instanceof android/support/v4/widget/ef
ifeq L0
aload 1
checkcast android/support/v4/widget/ef
aload 2
invokeinterface android/support/v4/widget/ef/setSupportButtonTintList(Landroid/content/res/ColorStateList;)V 1
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
aload 1
instanceof android/support/v4/widget/ef
ifeq L0
aload 1
checkcast android/support/v4/widget/ef
aload 2
invokeinterface android/support/v4/widget/ef/setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V 1
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public b(Landroid/widget/CompoundButton;)Landroid/content/res/ColorStateList;
aload 1
instanceof android/support/v4/widget/ef
ifeq L0
aload 1
checkcast android/support/v4/widget/ef
invokeinterface android/support/v4/widget/ef/getSupportButtonTintList()Landroid/content/res/ColorStateList; 0
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public c(Landroid/widget/CompoundButton;)Landroid/graphics/PorterDuff$Mode;
aload 1
instanceof android/support/v4/widget/ef
ifeq L0
aload 1
checkcast android/support/v4/widget/ef
invokeinterface android/support/v4/widget/ef/getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode; 0
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method
