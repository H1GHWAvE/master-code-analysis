.bytecode 50.0
.class public synchronized android/support/v4/widget/dn
.super android/view/ViewGroup
.implements android/support/v4/view/bt
.implements android/support/v4/view/bv

.field private static final 'M' [I

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field private static final 'e' Ljava/lang/String;

.field private static final 'f' I = 255


.field private static final 'g' I = 76


.field private static final 'h' I = 40


.field private static final 'i' I = 56


.field private static final 'j' F = 2.0F


.field private static final 'k' I = -1


.field private static final 'l' F = 0.5F


.field private static final 'm' F = 0.8F


.field private static final 'n' I = 150


.field private static final 'o' I = 300


.field private static final 'p' I = 200


.field private static final 'q' I = 200


.field private static final 'r' I = -328966


.field private static final 's' I = 64


.field private final 'A' Landroid/support/v4/view/bu;

.field private final 'B' [I

.field private 'C' I

.field private 'D' I

.field private 'E' Z

.field private 'F' F

.field private 'G' F

.field private 'H' Z

.field private 'I' I

.field private 'J' Z

.field private 'K' Z

.field private final 'L' Landroid/view/animation/DecelerateInterpolator;

.field private 'N' Landroid/support/v4/widget/e;

.field private 'O' I

.field private 'P' F

.field private 'Q' Landroid/support/v4/widget/bb;

.field private 'R' Landroid/view/animation/Animation;

.field private 'S' Landroid/view/animation/Animation;

.field private 'T' Landroid/view/animation/Animation;

.field private 'U' Landroid/view/animation/Animation;

.field private 'V' Landroid/view/animation/Animation;

.field private 'W' F

.field private 'aa' Z

.field private 'ab' I

.field private 'ac' I

.field private 'ad' Z

.field private 'ae' Landroid/view/animation/Animation$AnimationListener;

.field private final 'af' Landroid/view/animation/Animation;

.field private final 'ag' Landroid/view/animation/Animation;

.field private final 'ah' Landroid/view/animation/Animation;

.field protected 'c' I

.field protected 'd' I

.field private 't' Landroid/view/View;

.field private 'u' Landroid/support/v4/widget/dx;

.field private 'v' Z

.field private 'w' I

.field private 'x' F

.field private 'y' F

.field private final 'z' Landroid/support/v4/view/bw;

.method static <clinit>()V
ldc android/support/v4/widget/dn
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
putstatic android/support/v4/widget/dn/e Ljava/lang/String;
iconst_1
newarray int
dup
iconst_0
ldc_w 16842766
iastore
putstatic android/support/v4/widget/dn/M [I
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/widget/dn/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
aconst_null
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_0
putfield android/support/v4/widget/dn/v Z
aload 0
ldc_w -1.0F
putfield android/support/v4/widget/dn/x F
aload 0
iconst_2
newarray int
putfield android/support/v4/widget/dn/B [I
aload 0
iconst_0
putfield android/support/v4/widget/dn/E Z
aload 0
iconst_m1
putfield android/support/v4/widget/dn/I I
aload 0
iconst_m1
putfield android/support/v4/widget/dn/O I
aload 0
new android/support/v4/widget/do
dup
aload 0
invokespecial android/support/v4/widget/do/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/ae Landroid/view/animation/Animation$AnimationListener;
aload 0
new android/support/v4/widget/dt
dup
aload 0
invokespecial android/support/v4/widget/dt/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
aload 0
new android/support/v4/widget/du
dup
aload 0
invokespecial android/support/v4/widget/du/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/ag Landroid/view/animation/Animation;
aload 0
new android/support/v4/widget/dv
dup
aload 0
invokespecial android/support/v4/widget/dv/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
aload 0
aload 1
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/v4/widget/dn/w I
aload 0
aload 0
invokevirtual android/support/v4/widget/dn/getResources()Landroid/content/res/Resources;
ldc_w 17694721
invokevirtual android/content/res/Resources/getInteger(I)I
putfield android/support/v4/widget/dn/C I
aload 0
iconst_0
invokevirtual android/support/v4/widget/dn/setWillNotDraw(Z)V
aload 0
new android/view/animation/DecelerateInterpolator
dup
fconst_2
invokespecial android/view/animation/DecelerateInterpolator/<init>(F)V
putfield android/support/v4/widget/dn/L Landroid/view/animation/DecelerateInterpolator;
aload 1
aconst_null
getstatic android/support/v4/widget/dn/M [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
iconst_0
iconst_1
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
invokevirtual android/support/v4/widget/dn/setEnabled(Z)V
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
invokevirtual android/support/v4/widget/dn/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
astore 1
aload 0
aload 1
getfield android/util/DisplayMetrics/density F
ldc_w 40.0F
fmul
f2i
putfield android/support/v4/widget/dn/ab I
aload 0
aload 1
getfield android/util/DisplayMetrics/density F
ldc_w 40.0F
fmul
f2i
putfield android/support/v4/widget/dn/ac I
aload 0
new android/support/v4/widget/e
dup
aload 0
invokevirtual android/support/v4/widget/dn/getContext()Landroid/content/Context;
invokespecial android/support/v4/widget/e/<init>(Landroid/content/Context;)V
putfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
new android/support/v4/widget/bb
dup
aload 0
invokevirtual android/support/v4/widget/dn/getContext()Landroid/content/Context;
aload 0
invokespecial android/support/v4/widget/bb/<init>(Landroid/content/Context;Landroid/view/View;)V
putfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
ldc_w -328966
invokevirtual android/support/v4/widget/bb/b(I)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/e/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
bipush 8
invokevirtual android/support/v4/widget/e/setVisibility(I)V
aload 0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/dn/addView(Landroid/view/View;)V
aload 0
invokestatic android/support/v4/view/cx/a(Landroid/view/ViewGroup;)V
aload 0
aload 1
getfield android/util/DisplayMetrics/density F
ldc_w 64.0F
fmul
putfield android/support/v4/widget/dn/W F
aload 0
aload 0
getfield android/support/v4/widget/dn/W F
putfield android/support/v4/widget/dn/x F
aload 0
new android/support/v4/view/bw
dup
aload 0
invokespecial android/support/v4/view/bw/<init>(Landroid/view/ViewGroup;)V
putfield android/support/v4/widget/dn/z Landroid/support/v4/view/bw;
aload 0
new android/support/v4/view/bu
dup
aload 0
invokespecial android/support/v4/view/bu/<init>(Landroid/view/View;)V
putfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
aload 0
iconst_1
invokevirtual android/support/v4/widget/dn/setNestedScrollingEnabled(Z)V
return
.limit locals 3
.limit stack 5
.end method

.method private static a(Landroid/view/MotionEvent;I)F
aload 0
iload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 1
iload 1
ifge L0
ldc_w -1.0F
freturn
L0:
aload 0
iload 1
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
freturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/widget/dn;I)I
aload 0
iload 1
putfield android/support/v4/widget/dn/D I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a(II)Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/J Z
ifeq L0
invokestatic android/support/v4/widget/dn/b()Z
ifeq L0
aconst_null
areturn
L0:
new android/support/v4/widget/dr
dup
aload 0
iload 1
iload 2
invokespecial android/support/v4/widget/dr/<init>(Landroid/support/v4/widget/dn;II)V
astore 3
aload 3
ldc2_w 300L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aconst_null
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 3
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private a()V
aload 0
new android/support/v4/widget/e
dup
aload 0
invokevirtual android/support/v4/widget/dn/getContext()Landroid/content/Context;
invokespecial android/support/v4/widget/e/<init>(Landroid/content/Context;)V
putfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
new android/support/v4/widget/bb
dup
aload 0
invokevirtual android/support/v4/widget/dn/getContext()Landroid/content/Context;
aload 0
invokespecial android/support/v4/widget/bb/<init>(Landroid/content/Context;Landroid/view/View;)V
putfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
ldc_w -328966
invokevirtual android/support/v4/widget/bb/b(I)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/e/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
bipush 8
invokevirtual android/support/v4/widget/e/setVisibility(I)V
aload 0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/dn/addView(Landroid/view/View;)V
return
.limit locals 1
.limit stack 5
.end method

.method private a(F)V
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
iconst_1
invokevirtual android/support/v4/widget/bb/a(Z)V
fconst_1
fload 1
aload 0
getfield android/support/v4/widget/dn/x F
fdiv
invokestatic java/lang/Math/abs(F)F
invokestatic java/lang/Math/min(FF)F
fstore 3
fload 3
f2d
ldc2_w 0.4D
dsub
dconst_0
invokestatic java/lang/Math/max(DD)D
d2f
ldc_w 5.0F
fmul
ldc_w 3.0F
fdiv
fstore 4
fload 1
invokestatic java/lang/Math/abs(F)F
fstore 5
aload 0
getfield android/support/v4/widget/dn/x F
fstore 6
aload 0
getfield android/support/v4/widget/dn/ad Z
ifeq L0
aload 0
getfield android/support/v4/widget/dn/W F
aload 0
getfield android/support/v4/widget/dn/d I
i2f
fsub
fstore 2
L1:
fconst_0
fload 5
fload 6
fsub
fload 2
fconst_2
fmul
invokestatic java/lang/Math/min(FF)F
fload 2
fdiv
invokestatic java/lang/Math/max(FF)F
fstore 5
fload 5
ldc_w 4.0F
fdiv
f2d
fload 5
ldc_w 4.0F
fdiv
f2d
ldc2_w 2.0D
invokestatic java/lang/Math/pow(DD)D
dsub
d2f
fconst_2
fmul
fstore 5
aload 0
getfield android/support/v4/widget/dn/d I
istore 7
fload 2
fload 3
fmul
fload 2
fload 5
fmul
fconst_2
fmul
fadd
f2i
istore 8
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getVisibility()I
ifeq L2
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
iconst_0
invokevirtual android/support/v4/widget/e/setVisibility(I)V
L2:
aload 0
getfield android/support/v4/widget/dn/J Z
ifne L3
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
fconst_1
invokestatic android/support/v4/view/cx/d(Landroid/view/View;F)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
fconst_1
invokestatic android/support/v4/view/cx/e(Landroid/view/View;F)V
L3:
fload 1
aload 0
getfield android/support/v4/widget/dn/x F
fcmpg
ifge L4
aload 0
getfield android/support/v4/widget/dn/J Z
ifeq L5
aload 0
fload 1
aload 0
getfield android/support/v4/widget/dn/x F
fdiv
invokespecial android/support/v4/widget/dn/setAnimationProgress(F)V
L5:
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
bipush 76
if_icmple L6
aload 0
getfield android/support/v4/widget/dn/T Landroid/view/animation/Animation;
invokestatic android/support/v4/widget/dn/a(Landroid/view/animation/Animation;)Z
ifne L6
aload 0
aload 0
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
bipush 76
invokespecial android/support/v4/widget/dn/a(II)Landroid/view/animation/Animation;
putfield android/support/v4/widget/dn/T Landroid/view/animation/Animation;
L6:
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
ldc_w 0.8F
fload 4
ldc_w 0.8F
fmul
invokestatic java/lang/Math/min(FF)F
invokevirtual android/support/v4/widget/bb/b(F)V
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
fconst_1
fload 4
invokestatic java/lang/Math/min(FF)F
invokevirtual android/support/v4/widget/bb/a(F)V
L7:
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
ldc_w -0.25F
fload 4
ldc_w 0.4F
fmul
fadd
fload 5
fconst_2
fmul
fadd
ldc_w 0.5F
fmul
invokevirtual android/support/v4/widget/bg/c(F)V
aload 0
iload 8
iload 7
iadd
aload 0
getfield android/support/v4/widget/dn/D I
isub
iconst_1
invokespecial android/support/v4/widget/dn/a(IZ)V
return
L0:
aload 0
getfield android/support/v4/widget/dn/W F
fstore 2
goto L1
L4:
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
sipush 255
if_icmpge L7
aload 0
getfield android/support/v4/widget/dn/U Landroid/view/animation/Animation;
invokestatic android/support/v4/widget/dn/a(Landroid/view/animation/Animation;)Z
ifne L7
aload 0
aload 0
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
sipush 255
invokespecial android/support/v4/widget/dn/a(II)Landroid/view/animation/Animation;
putfield android/support/v4/widget/dn/U Landroid/view/animation/Animation;
goto L7
.limit locals 9
.limit stack 6
.end method

.method private a(ILandroid/view/animation/Animation$AnimationListener;)V
aload 0
iload 1
putfield android/support/v4/widget/dn/c I
aload 0
getfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
invokevirtual android/view/animation/Animation/reset()V
aload 0
getfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
ldc2_w 200L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/L Landroid/view/animation/DecelerateInterpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 2
ifnull L0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 2
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L0:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(IZ)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/bringToFront()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
iload 1
invokevirtual android/support/v4/widget/e/offsetTopAndBottom(I)V
aload 0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getTop()I
putfield android/support/v4/widget/dn/D I
iload 2
ifeq L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmpge L0
aload 0
invokevirtual android/support/v4/widget/dn/invalidate()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/widget/dn;F)V
aload 0
fload 1
invokespecial android/support/v4/widget/dn/setAnimationProgress(F)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/widget/dn;IZ)V
aload 0
iload 1
iload 2
invokespecial android/support/v4/widget/dn/a(IZ)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/MotionEvent;)V
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
istore 2
aload 1
iload 2
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
aload 0
getfield android/support/v4/widget/dn/I I
if_icmpne L0
iload 2
ifne L1
iconst_1
istore 2
L2:
aload 0
aload 1
iload 2
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/widget/dn/I I
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
iconst_0
invokevirtual android/support/v4/widget/e/setVisibility(I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
sipush 255
invokevirtual android/support/v4/widget/bb/setAlpha(I)V
L0:
aload 0
new android/support/v4/widget/dp
dup
aload 0
invokespecial android/support/v4/widget/dp/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/R Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/R Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/C I
i2l
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 1
ifnull L1
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 1
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L1:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/R Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(ZI)V
aload 0
iload 2
i2f
putfield android/support/v4/widget/dn/W F
aload 0
iload 1
putfield android/support/v4/widget/dn/J Z
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/invalidate()V
return
.limit locals 3
.limit stack 2
.end method

.method private a(ZII)V
aload 0
iload 1
putfield android/support/v4/widget/dn/J Z
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
bipush 8
invokevirtual android/support/v4/widget/e/setVisibility(I)V
aload 0
iload 2
putfield android/support/v4/widget/dn/D I
aload 0
iload 2
putfield android/support/v4/widget/dn/d I
aload 0
iload 3
i2f
putfield android/support/v4/widget/dn/W F
aload 0
iconst_1
putfield android/support/v4/widget/dn/ad Z
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/invalidate()V
return
.limit locals 4
.limit stack 2
.end method

.method private a(ZZ)V
aload 0
getfield android/support/v4/widget/dn/v Z
iload 1
if_icmpeq L0
aload 0
iload 2
putfield android/support/v4/widget/dn/aa Z
aload 0
invokespecial android/support/v4/widget/dn/f()V
aload 0
iload 1
putfield android/support/v4/widget/dn/v Z
aload 0
getfield android/support/v4/widget/dn/v Z
ifeq L1
aload 0
getfield android/support/v4/widget/dn/D I
istore 3
aload 0
getfield android/support/v4/widget/dn/ae Landroid/view/animation/Animation$AnimationListener;
astore 4
aload 0
iload 3
putfield android/support/v4/widget/dn/c I
aload 0
getfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
invokevirtual android/view/animation/Animation/reset()V
aload 0
getfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
ldc2_w 200L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/L Landroid/view/animation/DecelerateInterpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 4
ifnull L2
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 4
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L2:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/af Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
L0:
return
L1:
aload 0
aload 0
getfield android/support/v4/widget/dn/ae Landroid/view/animation/Animation$AnimationListener;
invokespecial android/support/v4/widget/dn/b(Landroid/view/animation/Animation$AnimationListener;)V
return
.limit locals 5
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v4/widget/dn;)Z
aload 0
getfield android/support/v4/widget/dn/v Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/animation/Animation;)Z
aload 0
ifnull L0
aload 0
invokevirtual android/view/animation/Animation/hasStarted()Z
ifeq L0
aload 0
invokevirtual android/view/animation/Animation/hasEnded()Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/bb;
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(F)V
fload 1
aload 0
getfield android/support/v4/widget/dn/x F
fcmpl
ifle L0
aload 0
iconst_1
iconst_1
invokespecial android/support/v4/widget/dn/a(ZZ)V
return
L0:
aload 0
iconst_0
putfield android/support/v4/widget/dn/v Z
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
fconst_0
invokevirtual android/support/v4/widget/bb/b(F)V
aconst_null
astore 3
aload 0
getfield android/support/v4/widget/dn/J Z
ifne L1
new android/support/v4/widget/ds
dup
aload 0
invokespecial android/support/v4/widget/ds/<init>(Landroid/support/v4/widget/dn;)V
astore 3
L1:
aload 0
getfield android/support/v4/widget/dn/D I
istore 2
aload 0
getfield android/support/v4/widget/dn/J Z
ifeq L2
aload 0
iload 2
putfield android/support/v4/widget/dn/c I
invokestatic android/support/v4/widget/dn/b()Z
ifeq L3
aload 0
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
i2f
putfield android/support/v4/widget/dn/P F
L4:
aload 0
new android/support/v4/widget/dw
dup
aload 0
invokespecial android/support/v4/widget/dw/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
ldc2_w 150L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 3
ifnull L5
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 3
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L5:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
L6:
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
iconst_0
invokevirtual android/support/v4/widget/bb/a(Z)V
return
L3:
aload 0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokestatic android/support/v4/view/cx/q(Landroid/view/View;)F
putfield android/support/v4/widget/dn/P F
goto L4
L2:
aload 0
iload 2
putfield android/support/v4/widget/dn/c I
aload 0
getfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
invokevirtual android/view/animation/Animation/reset()V
aload 0
getfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
ldc2_w 200L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/L Landroid/view/animation/DecelerateInterpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 3
ifnull L7
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 3
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L7:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
goto L6
.limit locals 4
.limit stack 4
.end method

.method private b(ILandroid/view/animation/Animation$AnimationListener;)V
aload 0
iload 1
putfield android/support/v4/widget/dn/c I
aload 0
getfield android/support/v4/widget/dn/ag Landroid/view/animation/Animation;
invokevirtual android/view/animation/Animation/reset()V
aload 0
getfield android/support/v4/widget/dn/ag Landroid/view/animation/Animation;
ldc2_w 500L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/dn/ag Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/L Landroid/view/animation/DecelerateInterpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 2
ifnull L0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 2
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L0:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/ag Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic b(Landroid/support/v4/widget/dn;F)V
aload 0
aload 0
getfield android/support/v4/widget/dn/c I
aload 0
getfield android/support/v4/widget/dn/d I
aload 0
getfield android/support/v4/widget/dn/c I
isub
i2f
fload 1
fmul
f2i
iadd
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getTop()I
isub
iconst_0
invokespecial android/support/v4/widget/dn/a(IZ)V
return
.limit locals 2
.limit stack 4
.end method

.method private b(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
new android/support/v4/widget/dq
dup
aload 0
invokespecial android/support/v4/widget/dq/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/S Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/S Landroid/view/animation/Animation;
ldc2_w 150L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 1
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/S Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 2
.limit stack 4
.end method

.method private static b()Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmpge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 0
.limit stack 2
.end method

.method private c()V
aload 0
aload 0
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
bipush 76
invokespecial android/support/v4/widget/dn/a(II)Landroid/view/animation/Animation;
putfield android/support/v4/widget/dn/T Landroid/view/animation/Animation;
return
.limit locals 1
.limit stack 4
.end method

.method private c(F)V
aload 0
aload 0
getfield android/support/v4/widget/dn/c I
aload 0
getfield android/support/v4/widget/dn/d I
aload 0
getfield android/support/v4/widget/dn/c I
isub
i2f
fload 1
fmul
f2i
iadd
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getTop()I
isub
iconst_0
invokespecial android/support/v4/widget/dn/a(IZ)V
return
.limit locals 2
.limit stack 4
.end method

.method private c(ILandroid/view/animation/Animation$AnimationListener;)V
aload 0
getfield android/support/v4/widget/dn/J Z
ifeq L0
aload 0
iload 1
putfield android/support/v4/widget/dn/c I
invokestatic android/support/v4/widget/dn/b()Z
ifeq L1
aload 0
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
i2f
putfield android/support/v4/widget/dn/P F
L2:
aload 0
new android/support/v4/widget/dw
dup
aload 0
invokespecial android/support/v4/widget/dw/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
ldc2_w 150L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 2
ifnull L3
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 2
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L3:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
return
L1:
aload 0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokestatic android/support/v4/view/cx/q(Landroid/view/View;)F
putfield android/support/v4/widget/dn/P F
goto L2
L0:
aload 0
iload 1
putfield android/support/v4/widget/dn/c I
aload 0
getfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
invokevirtual android/view/animation/Animation/reset()V
aload 0
getfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
ldc2_w 200L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/L Landroid/view/animation/DecelerateInterpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 2
ifnull L4
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 2
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L4:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/ah Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic c(Landroid/support/v4/widget/dn;)Z
aload 0
getfield android/support/v4/widget/dn/aa Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/dx;
aload 0
getfield android/support/v4/widget/dn/u Landroid/support/v4/widget/dx;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
aload 0
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
sipush 255
invokespecial android/support/v4/widget/dn/a(II)Landroid/view/animation/Animation;
putfield android/support/v4/widget/dn/U Landroid/view/animation/Animation;
return
.limit locals 1
.limit stack 4
.end method

.method private d(ILandroid/view/animation/Animation$AnimationListener;)V
aload 0
iload 1
putfield android/support/v4/widget/dn/c I
invokestatic android/support/v4/widget/dn/b()Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/bb/getAlpha()I
i2f
putfield android/support/v4/widget/dn/P F
L1:
aload 0
new android/support/v4/widget/dw
dup
aload 0
invokespecial android/support/v4/widget/dw/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
ldc2_w 150L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 2
ifnull L2
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 2
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L2:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/V Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
return
L0:
aload 0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokestatic android/support/v4/view/cx/q(Landroid/view/View;)F
putfield android/support/v4/widget/dn/P F
goto L1
.limit locals 3
.limit stack 4
.end method

.method static synthetic e(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Z
aload 0
getfield android/support/v4/widget/dn/v Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()V
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
ifnonnull L0
iconst_0
istore 1
L1:
iload 1
aload 0
invokevirtual android/support/v4/widget/dn/getChildCount()I
if_icmpge L0
aload 0
iload 1
invokevirtual android/support/v4/widget/dn/getChildAt(I)Landroid/view/View;
astore 2
aload 2
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L2
aload 0
aload 2
putfield android/support/v4/widget/dn/t Landroid/view/View;
L0:
return
L2:
iload 1
iconst_1
iadd
istore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method static synthetic f(Landroid/support/v4/widget/dn;)V
aload 0
sipush 255
invokespecial android/support/v4/widget/dn/setColorViewAlpha(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private g()Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmpge L0
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
instanceof android/widget/AbsListView
ifeq L1
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
checkcast android/widget/AbsListView
astore 1
aload 1
invokevirtual android/widget/AbsListView/getChildCount()I
ifle L2
aload 1
invokevirtual android/widget/AbsListView/getFirstVisiblePosition()I
ifgt L3
aload 1
iconst_0
invokevirtual android/widget/AbsListView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getTop()I
aload 1
invokevirtual android/widget/AbsListView/getPaddingTop()I
if_icmpge L2
L3:
iconst_1
ireturn
L2:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
iconst_m1
invokestatic android/support/v4/view/cx/b(Landroid/view/View;I)Z
ifne L4
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
invokevirtual android/view/View/getScrollY()I
ifle L5
L4:
iconst_1
ireturn
L5:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
iconst_m1
invokestatic android/support/v4/view/cx/b(Landroid/view/View;I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic g(Landroid/support/v4/widget/dn;)Z
aload 0
getfield android/support/v4/widget/dn/J Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Landroid/support/v4/widget/dn;)I
aload 0
getfield android/support/v4/widget/dn/D I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Landroid/support/v4/widget/dn;)V
aload 0
aconst_null
invokespecial android/support/v4/widget/dn/b(Landroid/view/animation/Animation$AnimationListener;)V
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic j(Landroid/support/v4/widget/dn;)Z
aload 0
getfield android/support/v4/widget/dn/ad Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Landroid/support/v4/widget/dn;)F
aload 0
getfield android/support/v4/widget/dn/W F
freturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic l(Landroid/support/v4/widget/dn;)F
aload 0
getfield android/support/v4/widget/dn/P F
freturn
.limit locals 1
.limit stack 1
.end method

.method private setAnimationProgress(F)V
invokestatic android/support/v4/widget/dn/b()Z
ifeq L0
aload 0
ldc_w 255.0F
fload 1
fmul
f2i
invokespecial android/support/v4/widget/dn/setColorViewAlpha(I)V
return
L0:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
fload 1
invokestatic android/support/v4/view/cx/d(Landroid/view/View;F)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
fload 1
invokestatic android/support/v4/view/cx/e(Landroid/view/View;F)V
return
.limit locals 2
.limit stack 3
.end method

.method private setColorViewAlpha(I)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getBackground()Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
iload 1
invokevirtual android/support/v4/widget/bb/setAlpha(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public dispatchNestedFling(FFZ)Z
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
fload 1
fload 2
iload 3
invokevirtual android/support/v4/view/bu/a(FFZ)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public dispatchNestedPreFling(FF)Z
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
fload 1
fload 2
invokevirtual android/support/v4/view/bu/a(FF)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public dispatchNestedPreScroll(II[I[I)Z
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
iload 1
iload 2
aload 3
aload 4
invokevirtual android/support/v4/view/bu/a(II[I[I)Z
ireturn
.limit locals 5
.limit stack 5
.end method

.method public dispatchNestedScroll(IIII[I)Z
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
iload 1
iload 2
iload 3
iload 4
aload 5
invokevirtual android/support/v4/view/bu/a(IIII[I)Z
ireturn
.limit locals 6
.limit stack 6
.end method

.method protected getChildDrawingOrder(II)I
aload 0
getfield android/support/v4/widget/dn/O I
ifge L0
L1:
iload 2
ireturn
L0:
iload 2
iload 1
iconst_1
isub
if_icmpne L2
aload 0
getfield android/support/v4/widget/dn/O I
ireturn
L2:
iload 2
aload 0
getfield android/support/v4/widget/dn/O I
if_icmplt L1
iload 2
iconst_1
iadd
ireturn
.limit locals 3
.limit stack 3
.end method

.method public getNestedScrollAxes()I
aload 0
getfield android/support/v4/widget/dn/z Landroid/support/v4/view/bw;
getfield android/support/v4/view/bw/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getProgressCircleDiameter()I
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
ifnull L0
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getMeasuredHeight()I
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public hasNestedScrollingParent()Z
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
invokevirtual android/support/v4/view/bu/a()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isNestedScrollingEnabled()Z
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
getfield android/support/v4/view/bu/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
invokespecial android/support/v4/widget/dn/f()V
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 3
aload 0
getfield android/support/v4/widget/dn/K Z
ifeq L0
iload 3
ifne L0
aload 0
iconst_0
putfield android/support/v4/widget/dn/K Z
L0:
aload 0
invokevirtual android/support/v4/widget/dn/isEnabled()Z
ifeq L1
aload 0
getfield android/support/v4/widget/dn/K Z
ifne L1
aload 0
invokespecial android/support/v4/widget/dn/g()Z
ifne L1
aload 0
getfield android/support/v4/widget/dn/v Z
ifeq L2
L1:
iconst_0
ireturn
L2:
iload 3
tableswitch 0
L3
L4
L5
L4
L6
L6
L7
default : L6
L6:
aload 0
getfield android/support/v4/widget/dn/H Z
ireturn
L3:
aload 0
aload 0
getfield android/support/v4/widget/dn/d I
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getTop()I
isub
iconst_1
invokespecial android/support/v4/widget/dn/a(IZ)V
aload 0
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/widget/dn/I I
aload 0
iconst_0
putfield android/support/v4/widget/dn/H Z
aload 1
aload 0
getfield android/support/v4/widget/dn/I I
invokestatic android/support/v4/widget/dn/a(Landroid/view/MotionEvent;I)F
fstore 2
fload 2
ldc_w -1.0F
fcmpl
ifeq L1
aload 0
fload 2
putfield android/support/v4/widget/dn/G F
goto L6
L5:
aload 0
getfield android/support/v4/widget/dn/I I
iconst_m1
if_icmpne L8
getstatic android/support/v4/widget/dn/e Ljava/lang/String;
ldc "Got ACTION_MOVE event but don't have an active pointer id."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L8:
aload 1
aload 0
getfield android/support/v4/widget/dn/I I
invokestatic android/support/v4/widget/dn/a(Landroid/view/MotionEvent;I)F
fstore 2
fload 2
ldc_w -1.0F
fcmpl
ifeq L1
fload 2
aload 0
getfield android/support/v4/widget/dn/G F
fsub
aload 0
getfield android/support/v4/widget/dn/w I
i2f
fcmpl
ifle L6
aload 0
getfield android/support/v4/widget/dn/H Z
ifne L6
aload 0
aload 0
getfield android/support/v4/widget/dn/G F
aload 0
getfield android/support/v4/widget/dn/w I
i2f
fadd
putfield android/support/v4/widget/dn/F F
aload 0
iconst_1
putfield android/support/v4/widget/dn/H Z
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
bipush 76
invokevirtual android/support/v4/widget/bb/setAlpha(I)V
goto L6
L7:
aload 0
aload 1
invokespecial android/support/v4/widget/dn/a(Landroid/view/MotionEvent;)V
goto L6
L4:
aload 0
iconst_0
putfield android/support/v4/widget/dn/H Z
aload 0
iconst_m1
putfield android/support/v4/widget/dn/I I
goto L6
.limit locals 4
.limit stack 3
.end method

.method protected onLayout(ZIIII)V
aload 0
invokevirtual android/support/v4/widget/dn/getMeasuredWidth()I
istore 2
aload 0
invokevirtual android/support/v4/widget/dn/getMeasuredHeight()I
istore 3
aload 0
invokevirtual android/support/v4/widget/dn/getChildCount()I
ifne L0
L1:
return
L0:
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
ifnonnull L2
aload 0
invokespecial android/support/v4/widget/dn/f()V
L2:
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
ifnull L1
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
astore 6
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingLeft()I
istore 4
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingTop()I
istore 5
aload 6
iload 4
iload 5
iload 2
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingRight()I
isub
iload 4
iadd
iload 3
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingBottom()I
isub
iload 5
iadd
invokevirtual android/view/View/layout(IIII)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getMeasuredWidth()I
istore 3
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getMeasuredHeight()I
istore 4
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
iload 2
iconst_2
idiv
iload 3
iconst_2
idiv
isub
aload 0
getfield android/support/v4/widget/dn/D I
iload 2
iconst_2
idiv
iload 3
iconst_2
idiv
iadd
aload 0
getfield android/support/v4/widget/dn/D I
iload 4
iadd
invokevirtual android/support/v4/widget/e/layout(IIII)V
return
.limit locals 7
.limit stack 6
.end method

.method public onMeasure(II)V
aload 0
iload 1
iload 2
invokespecial android/view/ViewGroup/onMeasure(II)V
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
ifnonnull L0
aload 0
invokespecial android/support/v4/widget/dn/f()V
L0:
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
ifnonnull L1
L2:
return
L1:
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
aload 0
invokevirtual android/support/v4/widget/dn/getMeasuredWidth()I
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingRight()I
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
aload 0
invokevirtual android/support/v4/widget/dn/getMeasuredHeight()I
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/widget/dn/getPaddingBottom()I
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/ab I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
aload 0
getfield android/support/v4/widget/dn/ac I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/support/v4/widget/e/measure(II)V
aload 0
getfield android/support/v4/widget/dn/ad Z
ifne L3
aload 0
getfield android/support/v4/widget/dn/E Z
ifne L3
aload 0
iconst_1
putfield android/support/v4/widget/dn/E Z
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/getMeasuredHeight()I
ineg
istore 1
aload 0
iload 1
putfield android/support/v4/widget/dn/d I
aload 0
iload 1
putfield android/support/v4/widget/dn/D I
L3:
aload 0
iconst_m1
putfield android/support/v4/widget/dn/O I
iconst_0
istore 1
L4:
iload 1
aload 0
invokevirtual android/support/v4/widget/dn/getChildCount()I
if_icmpge L2
aload 0
iload 1
invokevirtual android/support/v4/widget/dn/getChildAt(I)Landroid/view/View;
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
if_acmpne L5
aload 0
iload 1
putfield android/support/v4/widget/dn/O I
return
L5:
iload 1
iconst_1
iadd
istore 1
goto L4
.limit locals 3
.limit stack 4
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
iconst_0
ireturn
.limit locals 5
.limit stack 1
.end method

.method public onNestedPreFling(Landroid/view/View;FF)Z
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
iload 3
ifle L0
aload 0
getfield android/support/v4/widget/dn/y F
fconst_0
fcmpl
ifle L0
iload 3
i2f
aload 0
getfield android/support/v4/widget/dn/y F
fcmpl
ifle L1
aload 4
iconst_1
iload 3
aload 0
getfield android/support/v4/widget/dn/y F
f2i
isub
iastore
aload 0
fconst_0
putfield android/support/v4/widget/dn/y F
L2:
aload 0
aload 0
getfield android/support/v4/widget/dn/y F
invokespecial android/support/v4/widget/dn/a(F)V
L0:
aload 0
getfield android/support/v4/widget/dn/B [I
astore 1
aload 0
iload 2
aload 4
iconst_0
iaload
isub
iload 3
aload 4
iconst_1
iaload
isub
aload 1
aconst_null
invokevirtual android/support/v4/widget/dn/dispatchNestedPreScroll(II[I[I)Z
ifeq L3
aload 4
iconst_0
aload 4
iconst_0
iaload
aload 1
iconst_0
iaload
iadd
iastore
aload 4
iconst_1
iaload
istore 2
aload 4
iconst_1
aload 1
iconst_1
iaload
iload 2
iadd
iastore
L3:
return
L1:
aload 0
aload 0
getfield android/support/v4/widget/dn/y F
iload 3
i2f
fsub
putfield android/support/v4/widget/dn/y F
aload 4
iconst_1
iload 3
iastore
goto L2
.limit locals 5
.limit stack 5
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
iload 5
ifge L0
iload 5
invokestatic java/lang/Math/abs(I)I
istore 5
aload 0
getfield android/support/v4/widget/dn/y F
fstore 6
aload 0
iload 5
i2f
fload 6
fadd
putfield android/support/v4/widget/dn/y F
aload 0
aload 0
getfield android/support/v4/widget/dn/y F
invokespecial android/support/v4/widget/dn/a(F)V
L0:
aload 0
iload 2
iload 3
iload 4
iload 2
aconst_null
invokevirtual android/support/v4/widget/dn/dispatchNestedScroll(IIII[I)Z
pop
return
.limit locals 7
.limit stack 6
.end method

.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
aload 0
getfield android/support/v4/widget/dn/z Landroid/support/v4/view/bw;
iload 3
putfield android/support/v4/view/bw/a I
aload 0
fconst_0
putfield android/support/v4/widget/dn/y F
return
.limit locals 4
.limit stack 2
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
aload 0
invokevirtual android/support/v4/widget/dn/isEnabled()Z
ifeq L0
iload 3
iconst_2
iand
ifeq L0
aload 0
iload 3
iconst_2
iand
invokevirtual android/support/v4/widget/dn/startNestedScroll(I)Z
pop
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method public onStopNestedScroll(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/dn/z Landroid/support/v4/view/bw;
iconst_0
putfield android/support/v4/view/bw/a I
aload 0
getfield android/support/v4/widget/dn/y F
fconst_0
fcmpl
ifle L0
aload 0
aload 0
getfield android/support/v4/widget/dn/y F
invokespecial android/support/v4/widget/dn/b(F)V
aload 0
fconst_0
putfield android/support/v4/widget/dn/y F
L0:
aload 0
invokevirtual android/support/v4/widget/dn/stopNestedScroll()V
return
.limit locals 2
.limit stack 2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 4
aload 0
getfield android/support/v4/widget/dn/K Z
ifeq L0
iload 4
ifne L0
aload 0
iconst_0
putfield android/support/v4/widget/dn/K Z
L0:
aload 0
invokevirtual android/support/v4/widget/dn/isEnabled()Z
ifeq L1
aload 0
getfield android/support/v4/widget/dn/K Z
ifne L1
aload 0
invokespecial android/support/v4/widget/dn/g()Z
ifeq L2
L1:
iconst_0
ireturn
L2:
iload 4
tableswitch 0
L3
L4
L5
L4
L6
L7
L8
default : L6
L6:
iconst_1
ireturn
L3:
aload 0
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/widget/dn/I I
aload 0
iconst_0
putfield android/support/v4/widget/dn/H Z
goto L6
L5:
aload 1
aload 0
getfield android/support/v4/widget/dn/I I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 4
iload 4
ifge L9
getstatic android/support/v4/widget/dn/e Ljava/lang/String;
ldc "Got ACTION_MOVE event but have an invalid active pointer id."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L9:
aload 1
iload 4
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
aload 0
getfield android/support/v4/widget/dn/F F
fsub
ldc_w 0.5F
fmul
fstore 2
aload 0
getfield android/support/v4/widget/dn/H Z
ifeq L6
fload 2
fconst_0
fcmpl
ifle L1
aload 0
fload 2
invokespecial android/support/v4/widget/dn/a(F)V
goto L6
L7:
aload 0
aload 1
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/widget/dn/I I
goto L6
L8:
aload 0
aload 1
invokespecial android/support/v4/widget/dn/a(Landroid/view/MotionEvent;)V
goto L6
L4:
aload 0
getfield android/support/v4/widget/dn/I I
iconst_m1
if_icmpne L10
iload 4
iconst_1
if_icmpne L1
getstatic android/support/v4/widget/dn/e Ljava/lang/String;
ldc "Got ACTION_UP event but don't have an active pointer id."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L10:
aload 1
aload 1
aload 0
getfield android/support/v4/widget/dn/I I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 2
aload 0
getfield android/support/v4/widget/dn/F F
fstore 3
aload 0
iconst_0
putfield android/support/v4/widget/dn/H Z
aload 0
fload 2
fload 3
fsub
ldc_w 0.5F
fmul
invokespecial android/support/v4/widget/dn/b(F)V
aload 0
iconst_m1
putfield android/support/v4/widget/dn/I I
iconst_0
ireturn
.limit locals 5
.limit stack 3
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L0
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
instanceof android/widget/AbsListView
ifne L1
L0:
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
ifnull L2
aload 0
getfield android/support/v4/widget/dn/t Landroid/view/View;
invokestatic android/support/v4/view/cx/z(Landroid/view/View;)Z
ifeq L1
L2:
aload 0
iload 1
invokespecial android/view/ViewGroup/requestDisallowInterceptTouchEvent(Z)V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public transient setColorScheme([I)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
aload 1
invokevirtual android/support/v4/widget/dn/setColorSchemeResources([I)V
return
.limit locals 2
.limit stack 2
.end method

.method public transient setColorSchemeColors([I)V
.annotation invisible Landroid/support/a/j;
.end annotation
aload 0
invokespecial android/support/v4/widget/dn/f()V
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
astore 2
aload 2
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
aload 1
invokevirtual android/support/v4/widget/bg/a([I)V
aload 2
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
iconst_0
invokevirtual android/support/v4/widget/bg/a(I)V
return
.limit locals 3
.limit stack 2
.end method

.method public transient setColorSchemeResources([I)V
.annotation invisibleparam 1 Landroid/support/a/k;
.end annotation
aload 0
invokevirtual android/support/v4/widget/dn/getResources()Landroid/content/res/Resources;
astore 3
aload 1
arraylength
newarray int
astore 4
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 4
iload 2
aload 3
aload 1
iload 2
iaload
invokevirtual android/content/res/Resources/getColor(I)I
iastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
aload 4
invokevirtual android/support/v4/widget/dn/setColorSchemeColors([I)V
return
.limit locals 5
.limit stack 5
.end method

.method public setDistanceToTriggerSync(I)V
aload 0
iload 1
i2f
putfield android/support/v4/widget/dn/x F
return
.limit locals 2
.limit stack 2
.end method

.method public setNestedScrollingEnabled(Z)V
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
iload 1
invokevirtual android/support/v4/view/bu/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public setOnRefreshListener(Landroid/support/v4/widget/dx;)V
aload 0
aload 1
putfield android/support/v4/widget/dn/u Landroid/support/v4/widget/dx;
return
.limit locals 2
.limit stack 2
.end method

.method public setProgressBackgroundColor(I)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
iload 1
invokevirtual android/support/v4/widget/dn/setProgressBackgroundColorSchemeResource(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setProgressBackgroundColorSchemeColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
iload 1
invokevirtual android/support/v4/widget/e/setBackgroundColor(I)V
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
iload 1
invokevirtual android/support/v4/widget/bb/b(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setProgressBackgroundColorSchemeResource(I)V
.annotation invisibleparam 1 Landroid/support/a/k;
.end annotation
aload 0
aload 0
invokevirtual android/support/v4/widget/dn/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getColor(I)I
invokevirtual android/support/v4/widget/dn/setProgressBackgroundColorSchemeColor(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public setRefreshing(Z)V
iload 1
ifeq L0
aload 0
getfield android/support/v4/widget/dn/v Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v4/widget/dn/v Z
aload 0
getfield android/support/v4/widget/dn/ad Z
ifne L1
aload 0
getfield android/support/v4/widget/dn/W F
aload 0
getfield android/support/v4/widget/dn/d I
i2f
fadd
f2i
istore 2
L2:
aload 0
iload 2
aload 0
getfield android/support/v4/widget/dn/D I
isub
iconst_1
invokespecial android/support/v4/widget/dn/a(IZ)V
aload 0
iconst_0
putfield android/support/v4/widget/dn/aa Z
aload 0
getfield android/support/v4/widget/dn/ae Landroid/view/animation/Animation$AnimationListener;
astore 3
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
iconst_0
invokevirtual android/support/v4/widget/e/setVisibility(I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L3
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
sipush 255
invokevirtual android/support/v4/widget/bb/setAlpha(I)V
L3:
aload 0
new android/support/v4/widget/dp
dup
aload 0
invokespecial android/support/v4/widget/dp/<init>(Landroid/support/v4/widget/dn;)V
putfield android/support/v4/widget/dn/R Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/R Landroid/view/animation/Animation;
aload 0
getfield android/support/v4/widget/dn/C I
i2l
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 3
ifnull L4
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 3
putfield android/support/v4/widget/e/a Landroid/view/animation/Animation$AnimationListener;
L4:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
invokevirtual android/support/v4/widget/e/clearAnimation()V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/R Landroid/view/animation/Animation;
invokevirtual android/support/v4/widget/e/startAnimation(Landroid/view/animation/Animation;)V
return
L1:
aload 0
getfield android/support/v4/widget/dn/W F
f2i
istore 2
goto L2
L0:
aload 0
iload 1
iconst_0
invokespecial android/support/v4/widget/dn/a(ZZ)V
return
.limit locals 4
.limit stack 4
.end method

.method public setSize(I)V
iload 1
ifeq L0
iload 1
iconst_1
if_icmpeq L0
return
L0:
aload 0
invokevirtual android/support/v4/widget/dn/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
astore 3
iload 1
ifne L1
aload 3
getfield android/util/DisplayMetrics/density F
ldc_w 56.0F
fmul
f2i
istore 2
aload 0
iload 2
putfield android/support/v4/widget/dn/ab I
aload 0
iload 2
putfield android/support/v4/widget/dn/ac I
L2:
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aconst_null
invokevirtual android/support/v4/widget/e/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
iload 1
invokevirtual android/support/v4/widget/bb/a(I)V
aload 0
getfield android/support/v4/widget/dn/N Landroid/support/v4/widget/e;
aload 0
getfield android/support/v4/widget/dn/Q Landroid/support/v4/widget/bb;
invokevirtual android/support/v4/widget/e/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
return
L1:
aload 3
getfield android/util/DisplayMetrics/density F
ldc_w 40.0F
fmul
f2i
istore 2
aload 0
iload 2
putfield android/support/v4/widget/dn/ab I
aload 0
iload 2
putfield android/support/v4/widget/dn/ac I
goto L2
.limit locals 4
.limit stack 2
.end method

.method public startNestedScroll(I)Z
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
iload 1
invokevirtual android/support/v4/view/bu/a(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public stopNestedScroll()V
aload 0
getfield android/support/v4/widget/dn/A Landroid/support/v4/view/bu;
invokevirtual android/support/v4/view/bu/b()V
return
.limit locals 1
.limit stack 1
.end method
