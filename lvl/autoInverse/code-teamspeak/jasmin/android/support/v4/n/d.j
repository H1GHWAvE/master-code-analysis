.bytecode 50.0
.class public final synchronized android/support/v4/n/d
.super java/lang/Object

.field private 'a' [Ljava/lang/Object;

.field private 'b' I

.field private 'c' I

.field private 'd' I

.method public <init>()V
aload 0
iconst_0
invokespecial android/support/v4/n/d/<init>(B)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(B)V
bipush 8
istore 1
aload 0
invokespecial java/lang/Object/<init>()V
bipush 8
invokestatic java/lang/Integer/bitCount(I)I
iconst_1
if_icmpeq L0
iconst_1
bipush 8
invokestatic java/lang/Integer/highestOneBit(I)I
iconst_1
iadd
ishl
istore 1
L0:
aload 0
iload 1
iconst_1
isub
putfield android/support/v4/n/d/d I
aload 0
iload 1
anewarray java/lang/Object
checkcast [Ljava/lang/Object;
putfield android/support/v4/n/d/a [Ljava/lang/Object;
return
.limit locals 2
.limit stack 3
.end method

.method private a()V
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
arraylength
istore 1
iload 1
aload 0
getfield android/support/v4/n/d/b I
isub
istore 2
iload 1
iconst_1
ishl
istore 3
iload 3
ifge L0
new java/lang/RuntimeException
dup
ldc "Max array capacity exceeded"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 3
anewarray java/lang/Object
astore 4
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aload 4
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iconst_0
aload 4
iload 2
aload 0
getfield android/support/v4/n/d/b I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 4
checkcast [Ljava/lang/Object;
putfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
iconst_0
putfield android/support/v4/n/d/b I
aload 0
iload 1
putfield android/support/v4/n/d/c I
aload 0
iload 3
iconst_1
isub
putfield android/support/v4/n/d/d I
return
.limit locals 5
.limit stack 5
.end method

.method private a(I)V
iload 1
ifgt L0
L1:
return
L0:
iload 1
aload 0
invokespecial android/support/v4/n/d/g()I
if_icmple L2
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L2:
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
arraylength
istore 3
iload 3
istore 2
iload 1
iload 3
aload 0
getfield android/support/v4/n/d/b I
isub
if_icmpge L3
aload 0
getfield android/support/v4/n/d/b I
iload 1
iadd
istore 2
L3:
aload 0
getfield android/support/v4/n/d/b I
istore 3
L4:
iload 3
iload 2
if_icmpge L5
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iload 3
aconst_null
aastore
iload 3
iconst_1
iadd
istore 3
goto L4
L5:
iload 2
aload 0
getfield android/support/v4/n/d/b I
isub
istore 3
iload 1
iload 3
isub
istore 2
aload 0
iload 3
aload 0
getfield android/support/v4/n/d/b I
iadd
aload 0
getfield android/support/v4/n/d/d I
iand
putfield android/support/v4/n/d/b I
iload 2
ifle L1
iconst_0
istore 1
L6:
iload 1
iload 2
if_icmpge L7
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iload 1
aconst_null
aastore
iload 1
iconst_1
iadd
istore 1
goto L6
L7:
aload 0
iload 2
putfield android/support/v4/n/d/b I
return
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/lang/Object;)V
aload 0
aload 0
getfield android/support/v4/n/d/b I
iconst_1
isub
aload 0
getfield android/support/v4/n/d/d I
iand
putfield android/support/v4/n/d/b I
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aload 1
aastore
aload 0
getfield android/support/v4/n/d/b I
aload 0
getfield android/support/v4/n/d/c I
if_icmpne L0
aload 0
invokespecial android/support/v4/n/d/a()V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private b()Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aload 0
getfield android/support/v4/n/d/c I
if_icmpne L0
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aaload
astore 1
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aconst_null
aastore
aload 0
aload 0
getfield android/support/v4/n/d/b I
iconst_1
iadd
aload 0
getfield android/support/v4/n/d/d I
iand
putfield android/support/v4/n/d/b I
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private b(I)V
iload 1
ifgt L0
L1:
return
L0:
iload 1
aload 0
invokespecial android/support/v4/n/d/g()I
if_icmple L2
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L2:
iconst_0
istore 2
iload 1
aload 0
getfield android/support/v4/n/d/c I
if_icmpge L3
aload 0
getfield android/support/v4/n/d/c I
iload 1
isub
istore 2
L3:
iload 2
istore 3
L4:
iload 3
aload 0
getfield android/support/v4/n/d/c I
if_icmpge L5
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iload 3
aconst_null
aastore
iload 3
iconst_1
iadd
istore 3
goto L4
L5:
aload 0
getfield android/support/v4/n/d/c I
iload 2
isub
istore 2
iload 1
iload 2
isub
istore 1
aload 0
aload 0
getfield android/support/v4/n/d/c I
iload 2
isub
putfield android/support/v4/n/d/c I
iload 1
ifle L1
aload 0
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
arraylength
putfield android/support/v4/n/d/c I
aload 0
getfield android/support/v4/n/d/c I
iload 1
isub
istore 2
iload 2
istore 1
L6:
iload 1
aload 0
getfield android/support/v4/n/d/c I
if_icmpge L7
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iload 1
aconst_null
aastore
iload 1
iconst_1
iadd
istore 1
goto L6
L7:
aload 0
iload 2
putfield android/support/v4/n/d/c I
return
.limit locals 4
.limit stack 3
.end method

.method private b(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/c I
aload 1
aastore
aload 0
aload 0
getfield android/support/v4/n/d/c I
iconst_1
iadd
aload 0
getfield android/support/v4/n/d/d I
iand
putfield android/support/v4/n/d/c I
aload 0
getfield android/support/v4/n/d/c I
aload 0
getfield android/support/v4/n/d/b I
if_icmpne L0
aload 0
invokespecial android/support/v4/n/d/a()V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private c()Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aload 0
getfield android/support/v4/n/d/c I
if_icmpne L0
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/d/c I
iconst_1
isub
aload 0
getfield android/support/v4/n/d/d I
iand
istore 1
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iload 1
aaload
astore 2
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iload 1
aconst_null
aastore
aload 0
iload 1
putfield android/support/v4/n/d/c I
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method private c(I)Ljava/lang/Object;
iload 1
iflt L0
iload 1
aload 0
invokespecial android/support/v4/n/d/g()I
if_icmplt L1
L0:
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L1:
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
iload 1
iadd
aload 0
getfield android/support/v4/n/d/d I
iand
aaload
areturn
.limit locals 2
.limit stack 3
.end method

.method private d()V
aload 0
invokespecial android/support/v4/n/d/g()I
istore 3
iload 3
ifle L0
iload 3
aload 0
invokespecial android/support/v4/n/d/g()I
if_icmple L1
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L1:
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
arraylength
istore 2
iload 2
istore 1
iload 3
iload 2
aload 0
getfield android/support/v4/n/d/b I
isub
if_icmpge L2
aload 0
getfield android/support/v4/n/d/b I
iload 3
iadd
istore 1
L2:
aload 0
getfield android/support/v4/n/d/b I
istore 2
L3:
iload 2
iload 1
if_icmpge L4
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iload 2
aconst_null
aastore
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
iload 1
aload 0
getfield android/support/v4/n/d/b I
isub
istore 1
iload 3
iload 1
isub
istore 2
aload 0
iload 1
aload 0
getfield android/support/v4/n/d/b I
iadd
aload 0
getfield android/support/v4/n/d/d I
iand
putfield android/support/v4/n/d/b I
iload 2
ifle L0
iconst_0
istore 1
L5:
iload 1
iload 2
if_icmpge L6
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
iload 1
aconst_null
aastore
iload 1
iconst_1
iadd
istore 1
goto L5
L6:
aload 0
iload 2
putfield android/support/v4/n/d/b I
L0:
return
.limit locals 4
.limit stack 3
.end method

.method private e()Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aload 0
getfield android/support/v4/n/d/c I
if_icmpne L0
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aaload
areturn
.limit locals 1
.limit stack 2
.end method

.method private f()Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/b I
aload 0
getfield android/support/v4/n/d/c I
if_icmpne L0
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/d/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/d/c I
iconst_1
isub
aload 0
getfield android/support/v4/n/d/d I
iand
aaload
areturn
.limit locals 1
.limit stack 3
.end method

.method private g()I
aload 0
getfield android/support/v4/n/d/c I
aload 0
getfield android/support/v4/n/d/b I
isub
aload 0
getfield android/support/v4/n/d/d I
iand
ireturn
.limit locals 1
.limit stack 2
.end method

.method private h()Z
aload 0
getfield android/support/v4/n/d/b I
aload 0
getfield android/support/v4/n/d/c I
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method
