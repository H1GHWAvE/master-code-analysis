.bytecode 50.0
.class public final synchronized android/support/v4/n/h
.super java/io/Writer

.field private final 'a' Ljava/lang/String;

.field private 'b' Ljava/lang/StringBuilder;

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/io/Writer/<init>()V
aload 0
new java/lang/StringBuilder
dup
sipush 128
invokespecial java/lang/StringBuilder/<init>(I)V
putfield android/support/v4/n/h/b Ljava/lang/StringBuilder;
aload 0
aload 1
putfield android/support/v4/n/h/a Ljava/lang/String;
return
.limit locals 2
.limit stack 4
.end method

.method private a()V
aload 0
getfield android/support/v4/n/h/b Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/length()I
ifle L0
aload 0
getfield android/support/v4/n/h/a Ljava/lang/String;
aload 0
getfield android/support/v4/n/h/b Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield android/support/v4/n/h/b Ljava/lang/StringBuilder;
iconst_0
aload 0
getfield android/support/v4/n/h/b Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/length()I
invokevirtual java/lang/StringBuilder/delete(II)Ljava/lang/StringBuilder;
pop
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public final close()V
aload 0
invokespecial android/support/v4/n/h/a()V
return
.limit locals 1
.limit stack 1
.end method

.method public final flush()V
aload 0
invokespecial android/support/v4/n/h/a()V
return
.limit locals 1
.limit stack 1
.end method

.method public final write([CII)V
iconst_0
istore 5
L0:
iload 5
iload 3
if_icmpge L1
aload 1
iload 2
iload 5
iadd
caload
istore 4
iload 4
bipush 10
if_icmpne L2
aload 0
invokespecial android/support/v4/n/h/a()V
L3:
iload 5
iconst_1
iadd
istore 5
goto L0
L2:
aload 0
getfield android/support/v4/n/h/b Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
goto L3
L1:
return
.limit locals 6
.limit stack 3
.end method
