.bytecode 50.0
.class public synchronized android/support/v4/n/t
.super java/lang/Object
.implements android/support/v4/n/s

.field private final 'a' [Ljava/lang/Object;

.field private 'b' I

.method public <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
iload 1
ifgt L0
new java/lang/IllegalArgumentException
dup
ldc "The max pool size must be > 0"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
anewarray java/lang/Object
putfield android/support/v4/n/t/a [Ljava/lang/Object;
return
.limit locals 2
.limit stack 3
.end method

.method private b(Ljava/lang/Object;)Z
iconst_0
istore 4
iconst_0
istore 2
L0:
iload 4
istore 3
iload 2
aload 0
getfield android/support/v4/n/t/b I
if_icmpge L1
aload 0
getfield android/support/v4/n/t/a [Ljava/lang/Object;
iload 2
aaload
aload 1
if_acmpne L2
iconst_1
istore 3
L1:
iload 3
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 5
.limit stack 2
.end method

.method public a()Ljava/lang/Object;
aload 0
getfield android/support/v4/n/t/b I
ifle L0
aload 0
getfield android/support/v4/n/t/b I
iconst_1
isub
istore 1
aload 0
getfield android/support/v4/n/t/a [Ljava/lang/Object;
iload 1
aaload
astore 2
aload 0
getfield android/support/v4/n/t/a [Ljava/lang/Object;
iload 1
aconst_null
aastore
aload 0
aload 0
getfield android/support/v4/n/t/b I
iconst_1
isub
putfield android/support/v4/n/t/b I
aload 2
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public a(Ljava/lang/Object;)Z
iconst_0
istore 3
iconst_0
istore 2
L0:
iload 2
aload 0
getfield android/support/v4/n/t/b I
if_icmpge L1
aload 0
getfield android/support/v4/n/t/a [Ljava/lang/Object;
iload 2
aaload
aload 1
if_acmpne L2
iconst_1
istore 2
L3:
iload 2
ifeq L4
new java/lang/IllegalStateException
dup
ldc "Already in the pool!"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_0
istore 2
goto L3
L4:
aload 0
getfield android/support/v4/n/t/b I
aload 0
getfield android/support/v4/n/t/a [Ljava/lang/Object;
arraylength
if_icmpge L5
aload 0
getfield android/support/v4/n/t/a [Ljava/lang/Object;
aload 0
getfield android/support/v4/n/t/b I
aload 1
aastore
aload 0
aload 0
getfield android/support/v4/n/t/b I
iconst_1
iadd
putfield android/support/v4/n/t/b I
iconst_1
istore 3
L5:
iload 3
ireturn
.limit locals 4
.limit stack 3
.end method
