.bytecode 50.0
.class final synchronized android/support/v4/view/fj
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "ViewParentCompat"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/ViewParent;Landroid/view/View;)V
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 0
aload 1
invokeinterface android/view/ViewParent/onStopNestedScroll(Landroid/view/View;)V 1
L1:
return
L2:
astore 1
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onStopNestedScroll"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/view/ViewParent;Landroid/view/View;IIII)V
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 0
aload 1
iload 2
iload 3
iload 4
iload 5
invokeinterface android/view/ViewParent/onNestedScroll(Landroid/view/View;IIII)V 5
L1:
return
L2:
astore 1
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onNestedScroll"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 6
.limit stack 6
.end method

.method private static a(Landroid/view/ViewParent;Landroid/view/View;II[I)V
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 0
aload 1
iload 2
iload 3
aload 4
invokeinterface android/view/ViewParent/onNestedPreScroll(Landroid/view/View;II[I)V 4
L1:
return
L2:
astore 1
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onNestedPreScroll"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 5
.limit stack 5
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;FF)Z
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 0
aload 1
fload 2
fload 3
invokeinterface android/view/ViewParent/onNestedPreFling(Landroid/view/View;FF)Z 3
istore 4
L1:
iload 4
ireturn
L2:
astore 1
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onNestedPreFling"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_0
ireturn
.limit locals 5
.limit stack 4
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 0
aload 1
fload 2
fload 3
iload 4
invokeinterface android/view/ViewParent/onNestedFling(Landroid/view/View;FFZ)Z 4
istore 4
L1:
iload 4
ireturn
L2:
astore 1
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onNestedFling"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_0
ireturn
.limit locals 5
.limit stack 5
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 0
aload 1
aload 2
iload 3
invokeinterface android/view/ViewParent/onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z 3
istore 4
L1:
iload 4
ireturn
L2:
astore 1
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onStartNestedScroll"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_0
ireturn
.limit locals 5
.limit stack 4
.end method

.method private static b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 0
aload 1
aload 2
iload 3
invokeinterface android/view/ViewParent/onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V 3
L1:
return
L2:
astore 1
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onNestedScrollAccepted"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 4
.limit stack 4
.end method
