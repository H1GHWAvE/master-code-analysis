.bytecode 50.0
.class public synchronized abstract android/support/v4/view/n
.super java/lang/Object

.field private static final 'c' Ljava/lang/String; = "ActionProvider(support)"

.field public 'a' Landroid/support/v4/view/o;

.field public 'b' Landroid/support/v4/view/p;

.field private final 'd' Landroid/content/Context;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/view/n/d Landroid/content/Context;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/view/o;)V
aload 0
aload 1
putfield android/support/v4/view/n/a Landroid/support/v4/view/o;
return
.limit locals 2
.limit stack 2
.end method

.method private g()Landroid/content/Context;
aload 0
getfield android/support/v4/view/n/d Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()V
aload 0
aconst_null
putfield android/support/v4/view/n/b Landroid/support/v4/view/p;
aload 0
aconst_null
putfield android/support/v4/view/n/a Landroid/support/v4/view/o;
return
.limit locals 1
.limit stack 2
.end method

.method public abstract a()Landroid/view/View;
.end method

.method public a(Landroid/view/MenuItem;)Landroid/view/View;
aload 0
invokevirtual android/support/v4/view/n/a()Landroid/view/View;
areturn
.limit locals 2
.limit stack 1
.end method

.method public a(Landroid/support/v4/view/p;)V
aload 0
getfield android/support/v4/view/n/b Landroid/support/v4/view/p;
ifnull L0
ldc "ActionProvider(support)"
new java/lang/StringBuilder
dup
ldc "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " instance while it is still in use somewhere else?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
aload 1
putfield android/support/v4/view/n/b Landroid/support/v4/view/p;
return
.limit locals 2
.limit stack 4
.end method

.method public a(Landroid/view/SubMenu;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final a(Z)V
aload 0
getfield android/support/v4/view/n/a Landroid/support/v4/view/o;
ifnull L0
aload 0
getfield android/support/v4/view/n/a Landroid/support/v4/view/o;
iload 1
invokeinterface android/support/v4/view/o/b(Z)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public b()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public c()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public d()V
aload 0
getfield android/support/v4/view/n/b Landroid/support/v4/view/p;
ifnull L0
aload 0
invokevirtual android/support/v4/view/n/b()Z
ifeq L0
aload 0
getfield android/support/v4/view/n/b Landroid/support/v4/view/p;
astore 1
aload 0
invokevirtual android/support/v4/view/n/c()Z
pop
aload 1
invokeinterface android/support/v4/view/p/a()V 0
L0:
return
.limit locals 2
.limit stack 1
.end method

.method public e()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public f()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
