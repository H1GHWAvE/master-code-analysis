.bytecode 50.0
.class public final synchronized android/support/v4/view/b/e
.super java/lang/Object

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(FF)Landroid/view/animation/Interpolator;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/view/animation/PathInterpolator
dup
fload 0
fload 1
invokespecial android/view/animation/PathInterpolator/<init>(FF)V
areturn
L0:
new android/support/v4/view/b/h
dup
fload 0
fload 1
invokespecial android/support/v4/view/b/h/<init>(FF)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(FFFF)Landroid/view/animation/Interpolator;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/view/animation/PathInterpolator
dup
fload 0
fload 1
fload 2
fload 3
invokespecial android/view/animation/PathInterpolator/<init>(FFFF)V
areturn
L0:
new android/support/v4/view/b/h
dup
fload 0
fload 1
fload 2
fload 3
invokespecial android/support/v4/view/b/h/<init>(FFFF)V
areturn
.limit locals 4
.limit stack 6
.end method

.method private static a(Landroid/graphics/Path;)Landroid/view/animation/Interpolator;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/view/animation/PathInterpolator
dup
aload 0
invokespecial android/view/animation/PathInterpolator/<init>(Landroid/graphics/Path;)V
areturn
L0:
new android/support/v4/view/b/h
dup
aload 0
invokespecial android/support/v4/view/b/h/<init>(Landroid/graphics/Path;)V
areturn
.limit locals 1
.limit stack 3
.end method
