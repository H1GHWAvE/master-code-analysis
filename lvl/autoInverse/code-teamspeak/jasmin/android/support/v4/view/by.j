.bytecode 50.0
.class public synchronized abstract android/support/v4/view/by
.super java/lang/Object

.field public static final 'a' I = -1


.field public static final 'b' I = -2


.field private 'c' Landroid/database/DataSetObservable;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/database/DataSetObservable
dup
invokespecial android/database/DataSetObservable/<init>()V
putfield android/support/v4/view/by/c Landroid/database/DataSetObservable;
return
.limit locals 1
.limit stack 3
.end method

.method private static a()V
return
.limit locals 0
.limit stack 0
.end method

.method private static f()Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
ldc "Required method instantiateItem was not overridden"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 0
.limit stack 3
.end method

.method private static g()V
new java/lang/UnsupportedOperationException
dup
ldc "Required method destroyItem was not overridden"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 0
.limit stack 3
.end method

.method private static h()V
return
.limit locals 0
.limit stack 0
.end method

.method private static i()V
return
.limit locals 0
.limit stack 0
.end method

.method private static j()I
iconst_m1
ireturn
.limit locals 0
.limit stack 1
.end method

.method private k()V
aload 0
getfield android/support/v4/view/by/c Landroid/database/DataSetObservable;
invokevirtual android/database/DataSetObservable/notifyChanged()V
return
.limit locals 1
.limit stack 1
.end method

.method private static l()Ljava/lang/CharSequence;
aconst_null
areturn
.limit locals 0
.limit stack 1
.end method

.method private static m()F
fconst_1
freturn
.limit locals 0
.limit stack 1
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
ldc "Required method instantiateItem was not overridden"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public a(ILjava/lang/Object;)V
new java/lang/UnsupportedOperationException
dup
ldc "Required method destroyItem was not overridden"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/database/DataSetObserver;)V
aload 0
getfield android/support/v4/view/by/c Landroid/database/DataSetObservable;
aload 1
invokevirtual android/database/DataSetObservable/registerObserver(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 0
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/Object;)Z
.end method

.method public b()V
return
.limit locals 1
.limit stack 0
.end method

.method public final b(Landroid/database/DataSetObserver;)V
aload 0
getfield android/support/v4/view/by/c Landroid/database/DataSetObservable;
aload 1
invokevirtual android/database/DataSetObservable/unregisterObserver(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public c()V
return
.limit locals 1
.limit stack 0
.end method

.method public d()Landroid/os/Parcelable;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public abstract e()I
.end method
