.bytecode 50.0
.class synchronized android/support/v4/view/ac
.super java/lang/Object
.implements android/support/v4/view/af

.field private static final 'a' I = 247


.field private static final 'b' I = 247


.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(IIIII)I
iconst_1
istore 6
iload 1
iload 2
iand
ifeq L0
iconst_1
istore 5
L1:
iload 3
iload 4
ior
istore 3
iload 1
iload 3
iand
ifeq L2
iload 6
istore 1
L3:
iload 5
ifeq L4
iload 1
ifeq L5
new java/lang/IllegalArgumentException
dup
ldc "bad arguments"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_0
istore 5
goto L1
L2:
iconst_0
istore 1
goto L3
L5:
iload 0
iload 3
iconst_m1
ixor
iand
istore 3
L6:
iload 3
ireturn
L4:
iload 0
istore 3
iload 1
ifeq L6
iload 0
iload 2
iconst_m1
ixor
iand
ireturn
.limit locals 7
.limit stack 3
.end method

.method public a(I)I
iload 1
sipush 192
iand
ifeq L0
iload 1
iconst_1
ior
istore 1
L1:
iload 1
istore 2
iload 1
bipush 48
iand
ifeq L2
iload 1
iconst_2
ior
istore 2
L2:
iload 2
sipush 247
iand
ireturn
L0:
goto L1
.limit locals 3
.limit stack 2
.end method

.method public a(Landroid/view/View;)Ljava/lang/Object;
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public a(Landroid/view/KeyEvent;)V
return
.limit locals 2
.limit stack 0
.end method

.method public a(II)Z
aload 0
iload 1
invokevirtual android/support/v4/view/ac/a(I)I
sipush 247
iand
iload 2
iconst_1
bipush 64
sipush 128
invokestatic android/support/v4/view/ac/a(IIIII)I
iload 2
iconst_2
bipush 16
bipush 32
invokestatic android/support/v4/view/ac/a(IIIII)I
iload 2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 5
.end method

.method public a(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z
aload 1
aload 2
invokevirtual android/view/KeyEvent/dispatch(Landroid/view/KeyEvent$Callback;)Z
ireturn
.limit locals 5
.limit stack 2
.end method

.method public b(I)Z
aload 0
iload 1
invokevirtual android/support/v4/view/ac/a(I)I
sipush 247
iand
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public b(Landroid/view/KeyEvent;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method
