.bytecode 50.0
.class final synchronized android/support/v4/view/dh
.super android/support/v4/view/dg

.method <init>()V
aload 0
invokespecial android/support/v4/view/dg/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final K(Landroid/view/View;)Ljava/lang/String;
aload 1
invokevirtual android/view/View/getTransitionName()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final M(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/requestApplyInsets()V
return
.limit locals 2
.limit stack 1
.end method

.method public final N(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getElevation()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final O(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getTranslationZ()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final U(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/isNestedScrollingEnabled()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final V(Landroid/view/View;)Landroid/content/res/ColorStateList;
aload 1
invokevirtual android/view/View/getBackgroundTintList()Landroid/content/res/ColorStateList;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final W(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
aload 1
invokevirtual android/view/View/getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final X(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/stopNestedScroll()V
return
.limit locals 2
.limit stack 1
.end method

.method public final Y(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/hasNestedScrollingParent()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
aload 2
astore 3
aload 2
instanceof android/support/v4/view/gi
ifeq L0
aload 2
checkcast android/support/v4/view/gi
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
astore 4
aload 1
aload 4
invokevirtual android/view/View/onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
astore 1
aload 2
astore 3
aload 1
aload 4
if_acmpeq L0
new android/support/v4/view/gi
dup
aload 1
invokespecial android/support/v4/view/gi/<init>(Landroid/view/WindowInsets;)V
astore 3
L0:
aload 3
areturn
.limit locals 5
.limit stack 3
.end method

.method public final a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
aload 1
aload 2
invokevirtual android/view/View/setBackgroundTintList(Landroid/content/res/ColorStateList;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
aload 1
aload 2
invokevirtual android/view/View/setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/bx;)V
aload 1
new android/support/v4/view/dt
dup
aload 2
invokespecial android/support/v4/view/dt/<init>(Landroid/support/v4/view/bx;)V
invokevirtual android/view/View/setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V
return
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;)V
aload 1
aload 2
invokevirtual android/view/View/setTransitionName(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;FF)Z
aload 1
fload 2
fload 3
invokevirtual android/view/View/dispatchNestedPreFling(FF)Z
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final a(Landroid/view/View;FFZ)Z
aload 1
fload 2
fload 3
iload 4
invokevirtual android/view/View/dispatchNestedFling(FFZ)Z
ireturn
.limit locals 5
.limit stack 4
.end method

.method public final a(Landroid/view/View;IIII[I)Z
aload 1
iload 2
iload 3
iload 4
iload 5
aload 6
invokevirtual android/view/View/dispatchNestedScroll(IIII[I)Z
ireturn
.limit locals 7
.limit stack 6
.end method

.method public final a(Landroid/view/View;II[I[I)Z
aload 1
iload 2
iload 3
aload 4
aload 5
invokevirtual android/view/View/dispatchNestedPreScroll(II[I[I)Z
ireturn
.limit locals 6
.limit stack 5
.end method

.method public final aa(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getZ()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
aload 2
astore 3
aload 2
instanceof android/support/v4/view/gi
ifeq L0
aload 2
checkcast android/support/v4/view/gi
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
astore 4
aload 1
aload 4
invokevirtual android/view/View/dispatchApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
astore 1
aload 2
astore 3
aload 1
aload 4
if_acmpeq L0
new android/support/v4/view/gi
dup
aload 1
invokespecial android/support/v4/view/gi/<init>(Landroid/view/WindowInsets;)V
astore 3
L0:
aload 3
areturn
.limit locals 5
.limit stack 3
.end method

.method public final d(Landroid/view/View;Z)V
aload 1
iload 2
invokevirtual android/view/View/setNestedScrollingEnabled(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method public final f(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/isImportantForAccessibility()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final h(Landroid/view/View;I)Z
aload 1
iload 2
invokevirtual android/view/View/startNestedScroll(I)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final m(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setElevation(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final n(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setTranslationZ(F)V
return
.limit locals 3
.limit stack 2
.end method
