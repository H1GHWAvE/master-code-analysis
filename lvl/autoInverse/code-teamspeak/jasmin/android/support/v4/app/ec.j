.bytecode 50.0
.class synchronized android/support/v4/app/ec
.super android/support/v4/app/eb

.method <init>()V
aload 0
invokespecial android/support/v4/app/eb/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification;
new android/support/v4/app/ew
dup
aload 1
getfield android/support/v4/app/dm/a Landroid/content/Context;
aload 1
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
getfield android/support/v4/app/dm/b Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/c Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/h Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/f Landroid/widget/RemoteViews;
aload 1
getfield android/support/v4/app/dm/i I
aload 1
getfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 1
getfield android/support/v4/app/dm/e Landroid/app/PendingIntent;
aload 1
getfield android/support/v4/app/dm/g Landroid/graphics/Bitmap;
aload 1
getfield android/support/v4/app/dm/o I
aload 1
getfield android/support/v4/app/dm/p I
aload 1
getfield android/support/v4/app/dm/q Z
aload 1
getfield android/support/v4/app/dm/k Z
aload 1
getfield android/support/v4/app/dm/l Z
aload 1
getfield android/support/v4/app/dm/j I
aload 1
getfield android/support/v4/app/dm/n Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/v Z
aload 1
getfield android/support/v4/app/dm/C Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/dm/x Landroid/os/Bundle;
aload 1
getfield android/support/v4/app/dm/r Ljava/lang/String;
aload 1
getfield android/support/v4/app/dm/s Z
aload 1
getfield android/support/v4/app/dm/t Ljava/lang/String;
invokespecial android/support/v4/app/ew/<init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V
astore 3
aload 3
aload 1
getfield android/support/v4/app/dm/u Ljava/util/ArrayList;
invokestatic android/support/v4/app/dd/a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V
aload 3
aload 1
getfield android/support/v4/app/dm/m Landroid/support/v4/app/ed;
invokestatic android/support/v4/app/dd/a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
aload 2
aload 1
aload 3
invokevirtual android/support/v4/app/dn/a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dc;)Landroid/app/Notification;
areturn
.limit locals 4
.limit stack 25
.end method

.method public final a(Landroid/app/Notification;)Landroid/os/Bundle;
aload 1
getfield android/app/Notification/extras Landroid/os/Bundle;
areturn
.limit locals 2
.limit stack 1
.end method

.method public a(Landroid/app/Notification;I)Landroid/support/v4/app/df;
getstatic android/support/v4/app/df/e Landroid/support/v4/app/el;
astore 4
getstatic android/support/v4/app/fn/c Landroid/support/v4/app/fx;
astore 5
aload 1
getfield android/app/Notification/actions [Landroid/app/Notification$Action;
iload 2
aaload
astore 6
aconst_null
astore 3
aload 1
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.actionExtras"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 7
aload 3
astore 1
aload 7
ifnull L0
aload 7
iload 2
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/os/Bundle
astore 1
L0:
aload 4
aload 5
aload 6
getfield android/app/Notification$Action/icon I
aload 6
getfield android/app/Notification$Action/title Ljava/lang/CharSequence;
aload 6
getfield android/app/Notification$Action/actionIntent Landroid/app/PendingIntent;
aload 1
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/el;Landroid/support/v4/app/fx;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)Landroid/support/v4/app/ek;
checkcast android/support/v4/app/df
areturn
.limit locals 8
.limit stack 6
.end method

.method public final b(Landroid/app/Notification;)I
aload 1
getfield android/app/Notification/actions [Landroid/app/Notification$Action;
ifnull L0
aload 1
getfield android/app/Notification/actions [Landroid/app/Notification$Action;
arraylength
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public d(Landroid/app/Notification;)Z
aload 1
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.localOnly"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public e(Landroid/app/Notification;)Ljava/lang/String;
aload 1
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.groupKey"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public f(Landroid/app/Notification;)Z
aload 1
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.isGroupSummary"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public g(Landroid/app/Notification;)Ljava/lang/String;
aload 1
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.sortKey"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method
