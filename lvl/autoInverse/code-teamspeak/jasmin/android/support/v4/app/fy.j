.bytecode 50.0
.class final synchronized android/support/v4/app/fy
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "android.remoteinput.results"

.field public static final 'b' Ljava/lang/String; = "android.remoteinput.resultsData"

.field private static final 'c' Ljava/lang/String; = "resultKey"

.field private static final 'd' Ljava/lang/String; = "label"

.field private static final 'e' Ljava/lang/String; = "choices"

.field private static final 'f' Ljava/lang/String; = "allowFreeFormInput"

.field private static final 'g' Ljava/lang/String; = "extras"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Intent;)Landroid/os/Bundle;
aload 0
invokevirtual android/content/Intent/getClipData()Landroid/content/ClipData;
astore 0
aload 0
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
invokevirtual android/content/ClipData/getDescription()Landroid/content/ClipDescription;
astore 1
aload 1
ldc "text/vnd.android.intent"
invokevirtual android/content/ClipDescription/hasMimeType(Ljava/lang/String;)Z
ifeq L1
aload 1
invokevirtual android/content/ClipDescription/getLabel()Ljava/lang/CharSequence;
ldc "android.remoteinput.results"
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
iconst_0
invokevirtual android/content/ClipData/getItemAt(I)Landroid/content/ClipData$Item;
invokevirtual android/content/ClipData$Item/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "android.remoteinput.resultsData"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/os/Bundle
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/support/v4/app/fw;)Landroid/os/Bundle;
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 1
aload 1
ldc "resultKey"
aload 0
invokevirtual android/support/v4/app/fw/a()Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 1
ldc "label"
aload 0
invokevirtual android/support/v4/app/fw/b()Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
aload 1
ldc "choices"
aload 0
invokevirtual android/support/v4/app/fw/c()[Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V
aload 1
ldc "allowFreeFormInput"
aload 0
invokevirtual android/support/v4/app/fw/d()Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
aload 1
ldc "extras"
aload 0
invokevirtual android/support/v4/app/fw/e()Landroid/os/Bundle;
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Landroid/os/Bundle;Landroid/support/v4/app/fx;)Landroid/support/v4/app/fw;
aload 1
aload 0
ldc "resultKey"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
aload 0
ldc "label"
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
aload 0
ldc "choices"
invokevirtual android/os/Bundle/getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;
aload 0
ldc "allowFreeFormInput"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
aload 0
ldc "extras"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
invokeinterface android/support/v4/app/fx/a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw; 5
areturn
.limit locals 2
.limit stack 7
.end method

.method static a([Landroid/support/v4/app/fw;Landroid/content/Intent;Landroid/os/Bundle;)V
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 5
aload 0
arraylength
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
aaload
astore 6
aload 2
aload 6
invokevirtual android/support/v4/app/fw/a()Ljava/lang/String;
invokevirtual android/os/Bundle/get(Ljava/lang/String;)Ljava/lang/Object;
astore 7
aload 7
instanceof java/lang/CharSequence
ifeq L2
aload 5
aload 6
invokevirtual android/support/v4/app/fw/a()Ljava/lang/String;
aload 7
checkcast java/lang/CharSequence
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 0
aload 0
ldc "android.remoteinput.resultsData"
aload 5
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
pop
aload 1
ldc "android.remoteinput.results"
aload 0
invokestatic android/content/ClipData/newIntent(Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/content/ClipData;
invokevirtual android/content/Intent/setClipData(Landroid/content/ClipData;)V
return
.limit locals 8
.limit stack 3
.end method

.method static a([Landroid/support/v4/app/fw;)[Landroid/os/Bundle;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
arraylength
anewarray android/os/Bundle
astore 2
iconst_0
istore 1
L1:
iload 1
aload 0
arraylength
if_icmpge L2
aload 0
iload 1
aaload
astore 3
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 4
aload 4
ldc "resultKey"
aload 3
invokevirtual android/support/v4/app/fw/a()Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 4
ldc "label"
aload 3
invokevirtual android/support/v4/app/fw/b()Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
aload 4
ldc "choices"
aload 3
invokevirtual android/support/v4/app/fw/c()[Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V
aload 4
ldc "allowFreeFormInput"
aload 3
invokevirtual android/support/v4/app/fw/d()Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
aload 4
ldc "extras"
aload 3
invokevirtual android/support/v4/app/fw/e()Landroid/os/Bundle;
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
aload 2
iload 1
aload 4
aastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 2
areturn
.limit locals 5
.limit stack 3
.end method

.method static a([Landroid/os/Bundle;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/fw;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 1
aload 0
arraylength
invokeinterface android/support/v4/app/fx/a(I)[Landroid/support/v4/app/fw; 1
astore 3
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
if_icmpge L2
aload 0
iload 2
aaload
astore 4
aload 3
iload 2
aload 1
aload 4
ldc "resultKey"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
aload 4
ldc "label"
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
aload 4
ldc "choices"
invokevirtual android/os/Bundle/getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;
aload 4
ldc "allowFreeFormInput"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
aload 4
ldc "extras"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
invokeinterface android/support/v4/app/fx/a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw; 5
aastore
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
areturn
.limit locals 5
.limit stack 9
.end method
