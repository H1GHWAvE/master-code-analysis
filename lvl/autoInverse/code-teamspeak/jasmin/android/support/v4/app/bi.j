.bytecode 50.0
.class public synchronized abstract android/support/v4/app/bi
.super java/lang/Object

.field public static final 'a' I = 1


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Z)V
iload 0
putstatic android/support/v4/app/bl/b Z
return
.limit locals 1
.limit stack 1
.end method

.method private j()Landroid/support/v4/app/cd;
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
areturn
.limit locals 1
.limit stack 1
.end method

.method public abstract a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;
.end method

.method public abstract a(I)Landroid/support/v4/app/Fragment;
.annotation invisibleparam 1 Landroid/support/a/p;
.end annotation
.end method

.method public abstract a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
.end method

.method public abstract a()Landroid/support/v4/app/cd;
.end method

.method public abstract a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
.end method

.method public abstract a(Landroid/support/v4/app/bk;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.end method

.method public abstract a(II)Z
.end method

.method public abstract a(Ljava/lang/String;I)Z
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Landroid/support/v4/app/bk;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()V
.end method

.method public abstract d()Z
.end method

.method public abstract e()V
.end method

.method public abstract f()I
.end method

.method public abstract g()Landroid/support/v4/app/bj;
.end method

.method public abstract h()Ljava/util/List;
.end method

.method public abstract i()Z
.end method
