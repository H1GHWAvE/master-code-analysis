.bytecode 50.0
.class final synchronized android/support/v4/app/ct
.super android/support/v4/app/cr

.field static final 'a' Ljava/lang/String; = "LoaderManager"

.field static 'b' Z

.field final 'c' Landroid/support/v4/n/w;

.field final 'd' Landroid/support/v4/n/w;

.field final 'e' Ljava/lang/String;

.field 'f' Z

.field 'g' Z

.field 'h' Z

.field 'i' Z

.field 'j' Landroid/support/v4/app/bh;

.method static <clinit>()V
iconst_0
putstatic android/support/v4/app/ct/b Z
return
.limit locals 0
.limit stack 1
.end method

.method <init>(Ljava/lang/String;Landroid/support/v4/app/bh;Z)V
aload 0
invokespecial android/support/v4/app/cr/<init>()V
aload 0
new android/support/v4/n/w
dup
invokespecial android/support/v4/n/w/<init>()V
putfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
aload 0
new android/support/v4/n/w
dup
invokespecial android/support/v4/n/w/<init>()V
putfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
aload 0
aload 1
putfield android/support/v4/app/ct/e Ljava/lang/String;
aload 0
aload 2
putfield android/support/v4/app/ct/j Landroid/support/v4/app/bh;
aload 0
iload 3
putfield android/support/v4/app/ct/f Z
return
.limit locals 4
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
aload 0
getfield android/support/v4/app/ct/j Landroid/support/v4/app/bh;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/support/v4/app/bh;)V
aload 0
aload 1
putfield android/support/v4/app/ct/j Landroid/support/v4/app/bh;
return
.limit locals 2
.limit stack 2
.end method

.method private c(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;
new android/support/v4/app/cu
dup
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v4/app/cu/<init>(Landroid/support/v4/app/ct;ILandroid/os/Bundle;Landroid/support/v4/app/cs;)V
astore 2
aload 2
aload 3
invokeinterface android/support/v4/app/cs/a()Landroid/support/v4/c/aa; 0
putfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 2
areturn
.limit locals 4
.limit stack 6
.end method

.method private d(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;
.catch all from L0 to L1 using L2
L0:
aload 0
iconst_1
putfield android/support/v4/app/ct/i Z
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v4/app/ct/c(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;
astore 2
aload 0
aload 2
invokevirtual android/support/v4/app/ct/a(Landroid/support/v4/app/cu;)V
L1:
aload 0
iconst_0
putfield android/support/v4/app/ct/i Z
aload 2
areturn
L2:
astore 2
aload 0
iconst_0
putfield android/support/v4/app/ct/i Z
aload 2
athrow
.limit locals 4
.limit stack 4
.end method

.method private h()V
aload 0
getfield android/support/v4/app/ct/g Z
ifeq L0
getstatic android/support/v4/app/ct/b Z
ifeq L1
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Finished Retaining in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
iconst_0
putfield android/support/v4/app/ct/g Z
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 1
L2:
iload 1
iflt L0
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 2
aload 2
getfield android/support/v4/app/cu/i Z
ifeq L3
getstatic android/support/v4/app/ct/b Z
ifeq L4
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Finished Retaining: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 2
iconst_0
putfield android/support/v4/app/cu/i Z
aload 2
getfield android/support/v4/app/cu/h Z
aload 2
getfield android/support/v4/app/cu/j Z
if_icmpeq L3
aload 2
getfield android/support/v4/app/cu/h Z
ifne L3
aload 2
invokevirtual android/support/v4/app/cu/b()V
L3:
aload 2
getfield android/support/v4/app/cu/h Z
ifeq L5
aload 2
getfield android/support/v4/app/cu/e Z
ifeq L5
aload 2
getfield android/support/v4/app/cu/k Z
ifne L5
aload 2
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 2
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L5:
iload 1
iconst_1
isub
istore 1
goto L2
L0:
return
.limit locals 3
.limit stack 4
.end method

.method public final a(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/c/aa;
aload 0
getfield android/support/v4/app/ct/i Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "Called while creating a loader"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/a(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 4
getstatic android/support/v4/app/ct/b Z
ifeq L1
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "initLoader in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ": args="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 4
ifnonnull L2
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v4/app/ct/d(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;
astore 3
aload 3
astore 2
getstatic android/support/v4/app/ct/b Z
ifeq L3
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Created new loader "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 3
astore 2
L3:
aload 2
getfield android/support/v4/app/cu/e Z
ifeq L4
aload 0
getfield android/support/v4/app/ct/f Z
ifeq L4
aload 2
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 2
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L4:
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
areturn
L2:
getstatic android/support/v4/app/ct/b Z
ifeq L5
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Re-using existing loader "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L5:
aload 4
aload 3
putfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
aload 4
astore 2
goto L3
.limit locals 5
.limit stack 4
.end method

.method public final a(I)V
aload 0
getfield android/support/v4/app/ct/i Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "Called while creating a loader"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
getstatic android/support/v4/app/ct/b Z
ifeq L1
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "destroyLoader in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/e(I)I
istore 2
iload 2
iflt L2
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 2
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 3
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 2
invokevirtual android/support/v4/n/w/b(I)V
aload 3
invokevirtual android/support/v4/app/cu/c()V
L2:
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/e(I)I
istore 1
iload 1
iflt L3
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 3
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/b(I)V
aload 3
invokevirtual android/support/v4/app/cu/c()V
L3:
aload 0
getfield android/support/v4/app/ct/j Landroid/support/v4/app/bh;
ifnull L4
aload 0
invokevirtual android/support/v4/app/ct/a()Z
ifne L4
aload 0
getfield android/support/v4/app/ct/j Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/j()V
L4:
return
.limit locals 4
.limit stack 4
.end method

.method final a(Landroid/support/v4/app/cu;)V
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
aload 1
getfield android/support/v4/app/cu/a I
aload 1
invokevirtual android/support/v4/n/w/a(ILjava/lang/Object;)V
aload 0
getfield android/support/v4/app/ct/f Z
ifeq L0
aload 1
invokevirtual android/support/v4/app/cu/a()V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
iconst_0
istore 6
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
ifle L0
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Active Loaders:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "    "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 7
iconst_0
istore 5
L1:
iload 5
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
if_icmpge L0
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 5
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 8
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 5
invokevirtual android/support/v4/n/w/c(I)I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 8
invokevirtual android/support/v4/app/cu/toString()Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 8
aload 7
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/cu/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
iload 5
iconst_1
iadd
istore 5
goto L1
L0:
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
ifle L2
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Inactive Loaders:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "    "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 7
iload 6
istore 5
L3:
iload 5
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
if_icmpge L2
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
iload 5
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 8
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
iload 5
invokevirtual android/support/v4/n/w/c(I)I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 8
invokevirtual android/support/v4/app/cu/toString()Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 8
aload 7
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/cu/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
iload 5
iconst_1
iadd
istore 5
goto L3
L2:
return
.limit locals 9
.limit stack 5
.end method

.method public final a()Z
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
istore 3
iconst_0
istore 1
iconst_0
istore 4
L0:
iload 1
iload 3
if_icmpge L1
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 5
aload 5
getfield android/support/v4/app/cu/h Z
ifeq L2
aload 5
getfield android/support/v4/app/cu/f Z
ifne L2
iconst_1
istore 2
L3:
iload 4
iload 2
ior
istore 4
iload 1
iconst_1
iadd
istore 1
goto L0
L2:
iconst_0
istore 2
goto L3
L1:
iload 4
ireturn
.limit locals 6
.limit stack 2
.end method

.method public final b(I)Landroid/support/v4/c/aa;
aload 0
getfield android/support/v4/app/ct/i Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "Called while creating a loader"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/a(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 2
aload 2
ifnull L1
aload 2
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
ifnull L2
aload 2
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
areturn
L2:
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
areturn
L1:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public final b(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/c/aa;
aload 0
getfield android/support/v4/app/ct/i Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "Called while creating a loader"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/a(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 4
getstatic android/support/v4/app/ct/b Z
ifeq L1
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "restartLoader in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ": args="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 4
ifnull L2
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/a(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 5
aload 5
ifnull L3
aload 4
getfield android/support/v4/app/cu/e Z
ifeq L4
getstatic android/support/v4/app/ct/b Z
ifeq L5
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Removing last inactive loader: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L5:
aload 5
iconst_0
putfield android/support/v4/app/cu/f Z
aload 5
invokevirtual android/support/v4/app/cu/c()V
L6:
aload 4
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
iconst_1
putfield android/support/v4/c/aa/u Z
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
iload 1
aload 4
invokevirtual android/support/v4/n/w/a(ILjava/lang/Object;)V
L2:
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v4/app/ct/d(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
areturn
L4:
aload 4
getfield android/support/v4/app/cu/h Z
ifne L7
getstatic android/support/v4/app/ct/b Z
ifeq L8
ldc "LoaderManager"
ldc "  Current loader is stopped; replacing"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L8:
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
aconst_null
invokevirtual android/support/v4/n/w/a(ILjava/lang/Object;)V
aload 4
invokevirtual android/support/v4/app/cu/c()V
goto L2
L7:
getstatic android/support/v4/app/ct/b Z
ifeq L9
ldc "LoaderManager"
ldc "  Current loader is running; attempting to cancel"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L9:
getstatic android/support/v4/app/ct/b Z
ifeq L10
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Canceling: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L10:
aload 4
getfield android/support/v4/app/cu/h Z
ifeq L11
aload 4
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
ifnull L11
aload 4
getfield android/support/v4/app/cu/m Z
ifeq L11
aload 4
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual android/support/v4/c/aa/j()Z
ifne L11
aload 4
invokevirtual android/support/v4/app/cu/d()V
L11:
aload 4
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
ifnull L12
getstatic android/support/v4/app/ct/b Z
ifeq L13
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Removing pending loader: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L13:
aload 4
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
invokevirtual android/support/v4/app/cu/c()V
aload 4
aconst_null
putfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
L12:
getstatic android/support/v4/app/ct/b Z
ifeq L14
ldc "LoaderManager"
ldc "  Enqueuing as new pending loader"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L14:
aload 4
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v4/app/ct/c(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;
putfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
aload 4
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
areturn
L3:
getstatic android/support/v4/app/ct/b Z
ifeq L6
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Making last loader inactive: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L6
.limit locals 6
.limit stack 5
.end method

.method final b()V
getstatic android/support/v4/app/ct/b Z
ifeq L0
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Starting in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/ct/f Z
ifeq L1
new java/lang/RuntimeException
dup
ldc "here"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/lang/RuntimeException/fillInStackTrace()Ljava/lang/Throwable;
pop
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Called doStart when already started: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L2:
return
L1:
aload 0
iconst_1
putfield android/support/v4/app/ct/f Z
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 1
L3:
iload 1
iflt L2
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
invokevirtual android/support/v4/app/cu/a()V
iload 1
iconst_1
isub
istore 1
goto L3
.limit locals 3
.limit stack 4
.end method

.method final c()V
getstatic android/support/v4/app/ct/b Z
ifeq L0
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Stopping in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/ct/f Z
ifne L1
new java/lang/RuntimeException
dup
ldc "here"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/lang/RuntimeException/fillInStackTrace()Ljava/lang/Throwable;
pop
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Called doStop when not started: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
L1:
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 1
L2:
iload 1
iflt L3
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
invokevirtual android/support/v4/app/cu/b()V
iload 1
iconst_1
isub
istore 1
goto L2
L3:
aload 0
iconst_0
putfield android/support/v4/app/ct/f Z
return
.limit locals 3
.limit stack 4
.end method

.method final d()V
getstatic android/support/v4/app/ct/b Z
ifeq L0
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Retaining in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/ct/f Z
ifne L1
new java/lang/RuntimeException
dup
ldc "here"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/lang/RuntimeException/fillInStackTrace()Ljava/lang/Throwable;
pop
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Called doRetain when not started: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L2:
return
L1:
aload 0
iconst_1
putfield android/support/v4/app/ct/g Z
aload 0
iconst_0
putfield android/support/v4/app/ct/f Z
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 1
L3:
iload 1
iflt L2
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 2
getstatic android/support/v4/app/ct/b Z
ifeq L4
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Retaining: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 2
iconst_1
putfield android/support/v4/app/cu/i Z
aload 2
aload 2
getfield android/support/v4/app/cu/h Z
putfield android/support/v4/app/cu/j Z
aload 2
iconst_0
putfield android/support/v4/app/cu/h Z
aload 2
aconst_null
putfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
iload 1
iconst_1
isub
istore 1
goto L3
.limit locals 3
.limit stack 4
.end method

.method final e()V
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
iconst_1
putfield android/support/v4/app/cu/k Z
iload 1
iconst_1
isub
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method final f()V
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 2
aload 2
getfield android/support/v4/app/cu/h Z
ifeq L2
aload 2
getfield android/support/v4/app/cu/k Z
ifeq L2
aload 2
iconst_0
putfield android/support/v4/app/cu/k Z
aload 2
getfield android/support/v4/app/cu/e Z
ifeq L2
aload 2
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 2
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L2:
iload 1
iconst_1
isub
istore 1
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method final g()V
aload 0
getfield android/support/v4/app/ct/g Z
ifne L0
getstatic android/support/v4/app/ct/b Z
ifeq L1
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Destroying Active in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 1
L2:
iload 1
iflt L3
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
invokevirtual android/support/v4/app/cu/c()V
iload 1
iconst_1
isub
istore 1
goto L2
L3:
aload 0
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/b()V
L0:
getstatic android/support/v4/app/ct/b Z
ifeq L4
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Destroying Inactive in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 1
L5:
iload 1
iflt L6
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
invokevirtual android/support/v4/app/cu/c()V
iload 1
iconst_1
isub
istore 1
goto L5
L6:
aload 0
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/b()V
return
.limit locals 2
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
sipush 128
invokespecial java/lang/StringBuilder/<init>(I)V
astore 1
aload 1
ldc "LoaderManager{"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
ldc " in "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield android/support/v4/app/ct/j Landroid/support/v4/app/bh;
aload 1
invokestatic android/support/v4/n/g/a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
aload 1
ldc "}}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method
