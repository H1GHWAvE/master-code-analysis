.bytecode 50.0
.class public final synchronized android/support/v4/app/gc
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "IntentReader"

.field private 'b' Landroid/app/Activity;

.field private 'c' Landroid/content/Intent;

.field private 'd' Ljava/lang/String;

.field private 'e' Landroid/content/ComponentName;

.field private 'f' Ljava/util/ArrayList;

.method private <init>(Landroid/app/Activity;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/app/gc/b Landroid/app/Activity;
aload 0
aload 1
invokevirtual android/app/Activity/getIntent()Landroid/content/Intent;
putfield android/support/v4/app/gc/c Landroid/content/Intent;
aload 0
aload 1
invokestatic android/support/v4/app/ga/a(Landroid/app/Activity;)Ljava/lang/String;
putfield android/support/v4/app/gc/d Ljava/lang/String;
aload 0
aload 1
invokestatic android/support/v4/app/ga/b(Landroid/app/Activity;)Landroid/content/ComponentName;
putfield android/support/v4/app/gc/e Landroid/content/ComponentName;
return
.limit locals 2
.limit stack 2
.end method

.method private a(I)Landroid/net/Uri;
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
ifnonnull L0
aload 0
invokespecial android/support/v4/app/gc/c()Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.STREAM"
invokevirtual android/content/Intent/getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;
putfield android/support/v4/app/gc/f Ljava/util/ArrayList;
L0:
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
ifnull L1
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/net/Uri
areturn
L1:
iload 1
ifne L2
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.STREAM"
invokevirtual android/content/Intent/getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/net/Uri
areturn
L2:
new java/lang/StringBuilder
dup
ldc "Stream items available: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 3
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
ifnonnull L3
aload 0
invokespecial android/support/v4/app/gc/c()Z
ifeq L3
aload 0
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.STREAM"
invokevirtual android/content/Intent/getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;
putfield android/support/v4/app/gc/f Ljava/util/ArrayList;
L3:
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
ifnull L4
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
L5:
new java/lang/IndexOutOfBoundsException
dup
aload 3
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " index requested: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.STREAM"
invokevirtual android/content/Intent/hasExtra(Ljava/lang/String;)Z
ifeq L6
iconst_1
istore 2
goto L5
L6:
iconst_0
istore 2
goto L5
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/app/Activity;)Landroid/support/v4/app/gc;
new android/support/v4/app/gc
dup
aload 0
invokespecial android/support/v4/app/gc/<init>(Landroid/app/Activity;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a()Z
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
astore 1
ldc "android.intent.action.SEND"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
ldc "android.intent.action.SEND_MULTIPLE"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b()Z
ldc "android.intent.action.SEND"
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method private c()Z
ldc "android.intent.action.SEND_MULTIPLE"
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method private d()Ljava/lang/String;
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
invokevirtual android/content/Intent/getType()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.TEXT"
invokevirtual android/content/Intent/getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 2
.end method

.method private f()Ljava/lang/String;
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.HTML_TEXT"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnonnull L0
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.TEXT"
invokevirtual android/content/Intent/getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;
astore 2
aload 2
instanceof android/text/Spanned
ifeq L1
aload 2
checkcast android/text/Spanned
invokestatic android/text/Html/toHtml(Landroid/text/Spanned;)Ljava/lang/String;
areturn
L1:
aload 2
ifnull L0
invokestatic android/support/v4/app/ga/a()Landroid/support/v4/app/gd;
aload 2
invokeinterface android/support/v4/app/gd/a(Ljava/lang/CharSequence;)Ljava/lang/String; 1
areturn
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private g()Landroid/net/Uri;
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.STREAM"
invokevirtual android/content/Intent/getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/net/Uri
areturn
.limit locals 1
.limit stack 2
.end method

.method private h()I
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
ifnonnull L0
aload 0
invokespecial android/support/v4/app/gc/c()Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.STREAM"
invokevirtual android/content/Intent/getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;
putfield android/support/v4/app/gc/f Ljava/util/ArrayList;
L0:
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
ifnull L1
aload 0
getfield android/support/v4/app/gc/f Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
L1:
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.STREAM"
invokevirtual android/content/Intent/hasExtra(Ljava/lang/String;)Z
ifeq L2
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method private i()[Ljava/lang/String;
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.EMAIL"
invokevirtual android/content/Intent/getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method private j()[Ljava/lang/String;
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.CC"
invokevirtual android/content/Intent/getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method private k()[Ljava/lang/String;
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.BCC"
invokevirtual android/content/Intent/getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method private l()Ljava/lang/String;
aload 0
getfield android/support/v4/app/gc/c Landroid/content/Intent;
ldc "android.intent.extra.SUBJECT"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method private m()Ljava/lang/String;
aload 0
getfield android/support/v4/app/gc/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private n()Landroid/content/ComponentName;
aload 0
getfield android/support/v4/app/gc/e Landroid/content/ComponentName;
areturn
.limit locals 1
.limit stack 1
.end method

.method private o()Landroid/graphics/drawable/Drawable;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
getfield android/support/v4/app/gc/e Landroid/content/ComponentName;
ifnonnull L3
aconst_null
areturn
L3:
aload 0
getfield android/support/v4/app/gc/b Landroid/app/Activity;
invokevirtual android/app/Activity/getPackageManager()Landroid/content/pm/PackageManager;
astore 1
L0:
aload 1
aload 0
getfield android/support/v4/app/gc/e Landroid/content/ComponentName;
invokevirtual android/content/pm/PackageManager/getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "IntentReader"
ldc "Could not retrieve icon for calling activity"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method private p()Landroid/graphics/drawable/Drawable;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
getfield android/support/v4/app/gc/d Ljava/lang/String;
ifnonnull L3
aconst_null
areturn
L3:
aload 0
getfield android/support/v4/app/gc/b Landroid/app/Activity;
invokevirtual android/app/Activity/getPackageManager()Landroid/content/pm/PackageManager;
astore 1
L0:
aload 1
aload 0
getfield android/support/v4/app/gc/d Ljava/lang/String;
invokevirtual android/content/pm/PackageManager/getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "IntentReader"
ldc "Could not retrieve icon for calling application"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method private q()Ljava/lang/CharSequence;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
getfield android/support/v4/app/gc/d Ljava/lang/String;
ifnonnull L3
aconst_null
areturn
L3:
aload 0
getfield android/support/v4/app/gc/b Landroid/app/Activity;
invokevirtual android/app/Activity/getPackageManager()Landroid/content/pm/PackageManager;
astore 1
L0:
aload 1
aload 1
aload 0
getfield android/support/v4/app/gc/d Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
invokevirtual android/content/pm/PackageManager/getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "IntentReader"
ldc "Could not retrieve label for calling application"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method
