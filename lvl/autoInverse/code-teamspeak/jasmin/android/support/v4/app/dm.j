.bytecode 50.0
.class public synchronized android/support/v4/app/dm
.super java/lang/Object

.field private static final 'D' I = 5120


.field 'A' Landroid/app/Notification;

.field public 'B' Landroid/app/Notification;

.field public 'C' Ljava/util/ArrayList;

.field public 'a' Landroid/content/Context;

.field public 'b' Ljava/lang/CharSequence;

.field public 'c' Ljava/lang/CharSequence;

.field public 'd' Landroid/app/PendingIntent;

.field 'e' Landroid/app/PendingIntent;

.field 'f' Landroid/widget/RemoteViews;

.field public 'g' Landroid/graphics/Bitmap;

.field public 'h' Ljava/lang/CharSequence;

.field public 'i' I

.field 'j' I

.field 'k' Z

.field public 'l' Z

.field public 'm' Landroid/support/v4/app/ed;

.field public 'n' Ljava/lang/CharSequence;

.field 'o' I

.field 'p' I

.field 'q' Z

.field 'r' Ljava/lang/String;

.field 's' Z

.field 't' Ljava/lang/String;

.field public 'u' Ljava/util/ArrayList;

.field 'v' Z

.field 'w' Ljava/lang/String;

.field 'x' Landroid/os/Bundle;

.field 'y' I

.field 'z' I

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield android/support/v4/app/dm/k Z
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/dm/u Ljava/util/ArrayList;
aload 0
iconst_0
putfield android/support/v4/app/dm/v Z
aload 0
iconst_0
putfield android/support/v4/app/dm/y I
aload 0
iconst_0
putfield android/support/v4/app/dm/z I
aload 0
new android/app/Notification
dup
invokespecial android/app/Notification/<init>()V
putfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 0
aload 1
putfield android/support/v4/app/dm/a Landroid/content/Context;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
invokestatic java/lang/System/currentTimeMillis()J
putfield android/app/Notification/when J
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iconst_m1
putfield android/app/Notification/audioStreamType I
aload 0
iconst_0
putfield android/support/v4/app/dm/j I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/dm/C Ljava/util/ArrayList;
return
.limit locals 2
.limit stack 3
.end method

.method private a(II)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iload 1
putfield android/app/Notification/icon I
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iload 2
putfield android/app/Notification/iconLevel I
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(III)Landroid/support/v4/app/dm;
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
iconst_1
istore 4
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iload 1
putfield android/app/Notification/ledARGB I
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iload 2
putfield android/app/Notification/ledOnMS I
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iload 3
putfield android/app/Notification/ledOffMS I
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
getfield android/app/Notification/ledOnMS I
ifeq L0
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
getfield android/app/Notification/ledOffMS I
ifeq L0
iconst_1
istore 1
L1:
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
astore 5
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
getfield android/app/Notification/flags I
istore 2
iload 1
ifeq L2
iload 4
istore 1
L3:
aload 5
iload 2
bipush -2
iand
iload 1
ior
putfield android/app/Notification/flags I
aload 0
areturn
L0:
iconst_0
istore 1
goto L1
L2:
iconst_0
istore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private a(IIZ)Landroid/support/v4/app/dm;
aload 0
iload 1
putfield android/support/v4/app/dm/o I
aload 0
iload 2
putfield android/support/v4/app/dm/p I
aload 0
iload 3
putfield android/support/v4/app/dm/q Z
aload 0
areturn
.limit locals 4
.limit stack 2
.end method

.method private a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/u Ljava/util/ArrayList;
new android/support/v4/app/df
dup
iload 1
aload 2
aload 3
invokespecial android/support/v4/app/df/<init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
areturn
.limit locals 4
.limit stack 6
.end method

.method private a(Landroid/app/Notification;)Landroid/support/v4/app/dm;
aload 0
aload 1
putfield android/support/v4/app/dm/A Landroid/app/Notification;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/app/PendingIntent;Z)Landroid/support/v4/app/dm;
aload 0
aload 1
putfield android/support/v4/app/dm/e Landroid/app/PendingIntent;
aload 0
sipush 128
iload 2
invokevirtual android/support/v4/app/dm/a(IZ)V
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/net/Uri;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
putfield android/app/Notification/sound Landroid/net/Uri;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iconst_m1
putfield android/app/Notification/audioStreamType I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/net/Uri;I)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
putfield android/app/Notification/sound Landroid/net/Uri;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iload 2
putfield android/app/Notification/audioStreamType I
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/app/dm;
aload 1
ifnull L0
aload 0
getfield android/support/v4/app/dm/x Landroid/os/Bundle;
ifnonnull L1
aload 0
new android/os/Bundle
dup
aload 1
invokespecial android/os/Bundle/<init>(Landroid/os/Bundle;)V
putfield android/support/v4/app/dm/x Landroid/os/Bundle;
L0:
aload 0
areturn
L1:
aload 0
getfield android/support/v4/app/dm/x Landroid/os/Bundle;
aload 1
invokevirtual android/os/Bundle/putAll(Landroid/os/Bundle;)V
aload 0
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Landroid/support/v4/app/df;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/u Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/ds;)Landroid/support/v4/app/dm;
aload 1
aload 0
invokeinterface android/support/v4/app/ds/a(Landroid/support/v4/app/dm;)Landroid/support/v4/app/dm; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/ed;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/m Landroid/support/v4/app/ed;
aload 1
if_acmpeq L0
aload 0
aload 1
putfield android/support/v4/app/dm/m Landroid/support/v4/app/ed;
aload 0
getfield android/support/v4/app/dm/m Landroid/support/v4/app/ed;
ifnull L0
aload 0
getfield android/support/v4/app/dm/m Landroid/support/v4/app/ed;
aload 0
invokevirtual android/support/v4/app/ed/a(Landroid/support/v4/app/dm;)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/widget/RemoteViews;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
putfield android/app/Notification/contentView Landroid/widget/RemoteViews;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;Landroid/widget/RemoteViews;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
invokestatic android/support/v4/app/dm/d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
putfield android/app/Notification/tickerText Ljava/lang/CharSequence;
aload 0
aload 2
putfield android/support/v4/app/dm/f Landroid/widget/RemoteViews;
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/String;)Landroid/support/v4/app/dm;
aload 0
aload 1
putfield android/support/v4/app/dm/w Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Z)Landroid/support/v4/app/dm;
aload 0
iload 1
putfield android/support/v4/app/dm/k Z
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a([J)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
putfield android/app/Notification/vibrate [J
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(I)Landroid/support/v4/app/dm;
aload 0
iload 1
putfield android/support/v4/app/dm/i I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/app/PendingIntent;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
putfield android/app/Notification/deleteIntent Landroid/app/PendingIntent;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/os/Bundle;)Landroid/support/v4/app/dm;
aload 0
aload 1
putfield android/support/v4/app/dm/x Landroid/os/Bundle;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/C Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Z)Landroid/support/v4/app/dm;
aload 0
iload 1
putfield android/support/v4/app/dm/l Z
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private c(I)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iload 1
putfield android/app/Notification/defaults I
iload 1
iconst_4
iand
ifeq L0
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
astore 2
aload 2
aload 2
getfield android/app/Notification/flags I
iconst_1
ior
putfield android/app/Notification/flags I
L0:
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private c(Ljava/lang/String;)Landroid/support/v4/app/dm;
aload 0
aload 1
putfield android/support/v4/app/dm/r Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private c(Z)Landroid/support/v4/app/dm;
aload 0
bipush 8
iload 1
invokevirtual android/support/v4/app/dm/a(IZ)V
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private d(I)Landroid/support/v4/app/dm;
aload 0
iload 1
putfield android/support/v4/app/dm/j I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private d(Ljava/lang/String;)Landroid/support/v4/app/dm;
aload 0
aload 1
putfield android/support/v4/app/dm/t Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private d(Z)Landroid/support/v4/app/dm;
aload 0
iload 1
putfield android/support/v4/app/dm/v Z
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method protected static d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
aload 0
ifnonnull L0
L1:
aload 0
areturn
L0:
aload 0
invokeinterface java/lang/CharSequence/length()I 0
sipush 5120
if_icmple L1
aload 0
iconst_0
sipush 5120
invokeinterface java/lang/CharSequence/subSequence(II)Ljava/lang/CharSequence; 2
areturn
.limit locals 1
.limit stack 3
.end method

.method private e()Landroid/support/v4/app/dm;
aload 0
iconst_2
iconst_1
invokevirtual android/support/v4/app/dm/a(IZ)V
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method private e(I)Landroid/support/v4/app/dm;
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v4/app/dm/y I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private e(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 0
aload 1
invokestatic android/support/v4/app/dm/d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
putfield android/support/v4/app/dm/n Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private e(Z)Landroid/support/v4/app/dm;
aload 0
iload 1
putfield android/support/v4/app/dm/s Z
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private f()Landroid/app/Notification;
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f(I)Landroid/support/v4/app/dm;
aload 0
iload 1
putfield android/support/v4/app/dm/z I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private f(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 0
aload 1
invokestatic android/support/v4/app/dm/d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
putfield android/support/v4/app/dm/h Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a()Landroid/support/v4/app/dm;
aload 0
bipush 16
iconst_0
invokevirtual android/support/v4/app/dm/a(IZ)V
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(I)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
iload 1
putfield android/app/Notification/icon I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(J)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
lload 1
putfield android/app/Notification/when J
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/app/PendingIntent;)Landroid/support/v4/app/dm;
aload 0
aload 1
putfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/dm;
aload 0
aload 1
putfield android/support/v4/app/dm/g Landroid/graphics/Bitmap;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 0
aload 1
invokestatic android/support/v4/app/dm/d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
putfield android/support/v4/app/dm/b Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(IZ)V
iload 2
ifeq L0
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
astore 3
aload 3
aload 3
getfield android/app/Notification/flags I
iload 1
ior
putfield android/app/Notification/flags I
return
L0:
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
astore 3
aload 3
aload 3
getfield android/app/Notification/flags I
iload 1
iconst_m1
ixor
iand
putfield android/app/Notification/flags I
return
.limit locals 4
.limit stack 4
.end method

.method public final b()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/dm/x Landroid/os/Bundle;
ifnonnull L0
aload 0
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
putfield android/support/v4/app/dm/x Landroid/os/Bundle;
L0:
aload 0
getfield android/support/v4/app/dm/x Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 0
aload 1
invokestatic android/support/v4/app/dm/d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
putfield android/support/v4/app/dm/c Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c()Landroid/app/Notification;
invokestatic android/support/v4/app/dd/a()Landroid/support/v4/app/du;
aload 0
aload 0
invokevirtual android/support/v4/app/dm/d()Landroid/support/v4/app/dn;
invokeinterface android/support/v4/app/du/a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification; 2
areturn
.limit locals 1
.limit stack 3
.end method

.method public final c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 0
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
invokestatic android/support/v4/app/dm/d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
putfield android/app/Notification/tickerText Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public d()Landroid/support/v4/app/dn;
new android/support/v4/app/dn
dup
invokespecial android/support/v4/app/dn/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method
