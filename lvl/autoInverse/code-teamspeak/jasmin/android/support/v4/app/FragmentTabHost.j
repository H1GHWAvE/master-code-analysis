.bytecode 50.0
.class public final synchronized android/support/v4/app/FragmentTabHost
.super android/widget/TabHost
.implements android/widget/TabHost$OnTabChangeListener

.field private final 'a' Ljava/util/ArrayList;

.field private 'b' Landroid/widget/FrameLayout;

.field private 'c' Landroid/content/Context;

.field private 'd' Landroid/support/v4/app/bi;

.field private 'e' I

.field private 'f' Landroid/widget/TabHost$OnTabChangeListener;

.field private 'g' Landroid/support/v4/app/cc;

.field private 'h' Z

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/widget/TabHost/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/FragmentTabHost/a Ljava/util/ArrayList;
aload 0
aload 1
aconst_null
invokespecial android/support/v4/app/FragmentTabHost/a(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/TabHost/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/FragmentTabHost/a Ljava/util/ArrayList;
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/FragmentTabHost/a(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/lang/String;Landroid/support/v4/app/cd;)Landroid/support/v4/app/cd;
aconst_null
astore 4
iconst_0
istore 3
L0:
iload 3
aload 0
getfield android/support/v4/app/FragmentTabHost/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 0
getfield android/support/v4/app/FragmentTabHost/a Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/cc
astore 5
aload 5
getfield android/support/v4/app/cc/a Ljava/lang/String;
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 5
astore 4
L3:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
aload 4
ifnonnull L4
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "No tab known for tag "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 2
astore 1
aload 0
getfield android/support/v4/app/FragmentTabHost/g Landroid/support/v4/app/cc;
aload 4
if_acmpeq L5
aload 2
astore 1
aload 2
ifnonnull L6
aload 0
getfield android/support/v4/app/FragmentTabHost/d Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 1
L6:
aload 0
getfield android/support/v4/app/FragmentTabHost/g Landroid/support/v4/app/cc;
ifnull L7
aload 0
getfield android/support/v4/app/FragmentTabHost/g Landroid/support/v4/app/cc;
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
ifnull L7
aload 1
aload 0
getfield android/support/v4/app/FragmentTabHost/g Landroid/support/v4/app/cc;
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
L7:
aload 4
ifnull L8
aload 4
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
ifnonnull L9
aload 4
aload 0
getfield android/support/v4/app/FragmentTabHost/c Landroid/content/Context;
aload 4
getfield android/support/v4/app/cc/b Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
aload 4
getfield android/support/v4/app/cc/c Landroid/os/Bundle;
invokestatic android/support/v4/app/Fragment/a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;
putfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
aload 1
aload 0
getfield android/support/v4/app/FragmentTabHost/e I
aload 4
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
aload 4
getfield android/support/v4/app/cc/a Ljava/lang/String;
invokevirtual android/support/v4/app/cd/a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
L8:
aload 0
aload 4
putfield android/support/v4/app/FragmentTabHost/g Landroid/support/v4/app/cc;
L5:
aload 1
areturn
L9:
aload 1
aload 4
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
goto L8
L2:
goto L3
.limit locals 6
.limit stack 5
.end method

.method private a()V
aload 0
getfield android/support/v4/app/FragmentTabHost/b Landroid/widget/FrameLayout;
ifnonnull L0
aload 0
aload 0
aload 0
getfield android/support/v4/app/FragmentTabHost/e I
invokevirtual android/support/v4/app/FragmentTabHost/findViewById(I)Landroid/view/View;
checkcast android/widget/FrameLayout
putfield android/support/v4/app/FragmentTabHost/b Landroid/widget/FrameLayout;
aload 0
getfield android/support/v4/app/FragmentTabHost/b Landroid/widget/FrameLayout;
ifnonnull L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "No tab content FrameLayout found for id "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/FragmentTabHost/e I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 5
.end method

.method private a(Landroid/content/Context;)V
aload 0
ldc_w 16908307
invokevirtual android/support/v4/app/FragmentTabHost/findViewById(I)Landroid/view/View;
ifnonnull L0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 2
aload 2
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
aload 2
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/support/v4/app/FragmentTabHost/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/TabWidget
dup
aload 1
invokespecial android/widget/TabWidget/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc_w 16908307
invokevirtual android/widget/TabWidget/setId(I)V
aload 3
iconst_0
invokevirtual android/widget/TabWidget/setOrientation(I)V
aload 2
aload 3
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
fconst_0
invokespecial android/widget/LinearLayout$LayoutParams/<init>(IIF)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/FrameLayout
dup
aload 1
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc_w 16908305
invokevirtual android/widget/FrameLayout/setId(I)V
aload 2
aload 3
new android/widget/LinearLayout$LayoutParams
dup
iconst_0
iconst_0
fconst_0
invokespecial android/widget/LinearLayout$LayoutParams/<init>(IIF)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/FrameLayout
dup
aload 1
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
astore 1
aload 0
aload 1
putfield android/support/v4/app/FragmentTabHost/b Landroid/widget/FrameLayout;
aload 0
getfield android/support/v4/app/FragmentTabHost/b Landroid/widget/FrameLayout;
aload 0
getfield android/support/v4/app/FragmentTabHost/e I
invokevirtual android/widget/FrameLayout/setId(I)V
aload 2
aload 1
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_0
fconst_1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(IIF)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
L0:
return
.limit locals 4
.limit stack 7
.end method

.method private a(Landroid/content/Context;Landroid/support/v4/app/bi;)V
aload 0
aload 1
invokespecial android/support/v4/app/FragmentTabHost/a(Landroid/content/Context;)V
aload 0
invokespecial android/widget/TabHost/setup()V
aload 0
aload 1
putfield android/support/v4/app/FragmentTabHost/c Landroid/content/Context;
aload 0
aload 2
putfield android/support/v4/app/FragmentTabHost/d Landroid/support/v4/app/bi;
aload 0
invokespecial android/support/v4/app/FragmentTabHost/a()V
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/content/Context;Landroid/support/v4/app/bi;I)V
aload 0
aload 1
invokespecial android/support/v4/app/FragmentTabHost/a(Landroid/content/Context;)V
aload 0
invokespecial android/widget/TabHost/setup()V
aload 0
aload 1
putfield android/support/v4/app/FragmentTabHost/c Landroid/content/Context;
aload 0
aload 2
putfield android/support/v4/app/FragmentTabHost/d Landroid/support/v4/app/bi;
aload 0
iload 3
putfield android/support/v4/app/FragmentTabHost/e I
aload 0
invokespecial android/support/v4/app/FragmentTabHost/a()V
aload 0
getfield android/support/v4/app/FragmentTabHost/b Landroid/widget/FrameLayout;
iload 3
invokevirtual android/widget/FrameLayout/setId(I)V
aload 0
invokevirtual android/support/v4/app/FragmentTabHost/getId()I
iconst_m1
if_icmpne L0
aload 0
ldc_w 16908306
invokevirtual android/support/v4/app/FragmentTabHost/setId(I)V
L0:
return
.limit locals 4
.limit stack 2
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 1
aload 2
iconst_1
newarray int
dup
iconst_0
ldc_w 16842995
iastore
iconst_0
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v4/app/FragmentTabHost/e I
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
aload 0
invokespecial android/widget/TabHost/setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V
return
.limit locals 3
.limit stack 6
.end method

.method private a(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V
aload 1
new android/support/v4/app/ca
dup
aload 0
getfield android/support/v4/app/FragmentTabHost/c Landroid/content/Context;
invokespecial android/support/v4/app/ca/<init>(Landroid/content/Context;)V
invokevirtual android/widget/TabHost$TabSpec/setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;
pop
aload 1
invokevirtual android/widget/TabHost$TabSpec/getTag()Ljava/lang/String;
astore 4
new android/support/v4/app/cc
dup
aload 4
aload 2
aload 3
invokespecial android/support/v4/app/cc/<init>(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V
astore 2
aload 0
getfield android/support/v4/app/FragmentTabHost/h Z
ifeq L0
aload 2
aload 0
getfield android/support/v4/app/FragmentTabHost/d Landroid/support/v4/app/bi;
aload 4
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
putfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
aload 2
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
ifnull L0
aload 2
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/U Z
ifne L0
aload 0
getfield android/support/v4/app/FragmentTabHost/d Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 3
aload 3
aload 2
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 3
invokevirtual android/support/v4/app/cd/i()I
pop
L0:
aload 0
getfield android/support/v4/app/FragmentTabHost/a Ljava/util/ArrayList;
aload 2
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 1
invokevirtual android/support/v4/app/FragmentTabHost/addTab(Landroid/widget/TabHost$TabSpec;)V
return
.limit locals 5
.limit stack 5
.end method

.method protected final onAttachedToWindow()V
aload 0
invokespecial android/widget/TabHost/onAttachedToWindow()V
aload 0
invokevirtual android/support/v4/app/FragmentTabHost/getCurrentTabTag()Ljava/lang/String;
astore 4
aconst_null
astore 2
iconst_0
istore 1
L0:
iload 1
aload 0
getfield android/support/v4/app/FragmentTabHost/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 0
getfield android/support/v4/app/FragmentTabHost/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/cc
astore 5
aload 5
aload 0
getfield android/support/v4/app/FragmentTabHost/d Landroid/support/v4/app/bi;
aload 5
getfield android/support/v4/app/cc/a Ljava/lang/String;
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
putfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
aload 2
astore 3
aload 5
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
ifnull L2
aload 2
astore 3
aload 5
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/U Z
ifne L2
aload 5
getfield android/support/v4/app/cc/a Ljava/lang/String;
aload 4
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 0
aload 5
putfield android/support/v4/app/FragmentTabHost/g Landroid/support/v4/app/cc;
aload 2
astore 3
L2:
iload 1
iconst_1
iadd
istore 1
aload 3
astore 2
goto L0
L3:
aload 2
astore 3
aload 2
ifnonnull L4
aload 0
getfield android/support/v4/app/FragmentTabHost/d Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 3
L4:
aload 3
aload 5
getfield android/support/v4/app/cc/d Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
goto L2
L1:
aload 0
iconst_1
putfield android/support/v4/app/FragmentTabHost/h Z
aload 0
aload 4
aload 2
invokespecial android/support/v4/app/FragmentTabHost/a(Ljava/lang/String;Landroid/support/v4/app/cd;)Landroid/support/v4/app/cd;
astore 2
aload 2
ifnull L5
aload 2
invokevirtual android/support/v4/app/cd/i()I
pop
aload 0
getfield android/support/v4/app/FragmentTabHost/d Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/b()Z
pop
L5:
return
.limit locals 6
.limit stack 3
.end method

.method protected final onDetachedFromWindow()V
aload 0
invokespecial android/widget/TabHost/onDetachedFromWindow()V
aload 0
iconst_0
putfield android/support/v4/app/FragmentTabHost/h Z
return
.limit locals 1
.limit stack 2
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/v4/app/FragmentTabHost$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/v4/app/FragmentTabHost$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/widget/TabHost/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 0
aload 1
getfield android/support/v4/app/FragmentTabHost$SavedState/a Ljava/lang/String;
invokevirtual android/support/v4/app/FragmentTabHost/setCurrentTabByTag(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
new android/support/v4/app/FragmentTabHost$SavedState
dup
aload 0
invokespecial android/widget/TabHost/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/v4/app/FragmentTabHost$SavedState/<init>(Landroid/os/Parcelable;)V
astore 1
aload 1
aload 0
invokevirtual android/support/v4/app/FragmentTabHost/getCurrentTabTag()Ljava/lang/String;
putfield android/support/v4/app/FragmentTabHost$SavedState/a Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final onTabChanged(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/FragmentTabHost/h Z
ifeq L0
aload 0
aload 1
aconst_null
invokespecial android/support/v4/app/FragmentTabHost/a(Ljava/lang/String;Landroid/support/v4/app/cd;)Landroid/support/v4/app/cd;
astore 2
aload 2
ifnull L0
aload 2
invokevirtual android/support/v4/app/cd/i()I
pop
L0:
aload 0
getfield android/support/v4/app/FragmentTabHost/f Landroid/widget/TabHost$OnTabChangeListener;
ifnull L1
aload 0
getfield android/support/v4/app/FragmentTabHost/f Landroid/widget/TabHost$OnTabChangeListener;
aload 1
invokeinterface android/widget/TabHost$OnTabChangeListener/onTabChanged(Ljava/lang/String;)V 1
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public final setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V
aload 0
aload 1
putfield android/support/v4/app/FragmentTabHost/f Landroid/widget/TabHost$OnTabChangeListener;
return
.limit locals 2
.limit stack 2
.end method

.method public final setup()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/IllegalStateException
dup
ldc "Must call setup() that takes a Context and FragmentManager"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method
