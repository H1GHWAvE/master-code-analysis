.bytecode 50.0
.class final synchronized android/support/v4/app/ad
.super java/lang/Object

.field final 'a' Landroid/app/ActivityOptions;

.method <init>(Landroid/app/ActivityOptions;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/app/ad/a Landroid/app/ActivityOptions;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/ad/a Landroid/app/ActivityOptions;
invokevirtual android/app/ActivityOptions/toBundle()Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/ad;
new android/support/v4/app/ad
dup
aload 0
aload 1
aload 2
invokestatic android/app/ActivityOptions/makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ad/<init>(Landroid/app/ActivityOptions;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public static a(Landroid/app/Activity;[Landroid/view/View;[Ljava/lang/String;)Landroid/support/v4/app/ad;
aconst_null
astore 4
aload 1
ifnull L0
aload 1
arraylength
anewarray android/util/Pair
astore 4
iconst_0
istore 3
L1:
iload 3
aload 4
arraylength
if_icmpge L0
aload 4
iload 3
aload 1
iload 3
aaload
aload 2
iload 3
aaload
invokestatic android/util/Pair/create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
aastore
iload 3
iconst_1
iadd
istore 3
goto L1
L0:
new android/support/v4/app/ad
dup
aload 0
aload 4
invokestatic android/app/ActivityOptions/makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ad/<init>(Landroid/app/ActivityOptions;)V
areturn
.limit locals 5
.limit stack 5
.end method

.method private a(Landroid/support/v4/app/ad;)V
aload 0
getfield android/support/v4/app/ad/a Landroid/app/ActivityOptions;
aload 1
getfield android/support/v4/app/ad/a Landroid/app/ActivityOptions;
invokevirtual android/app/ActivityOptions/update(Landroid/app/ActivityOptions;)V
return
.limit locals 2
.limit stack 2
.end method
