.bytecode 50.0
.class public synchronized abstract android/support/v4/app/by
.super android/support/v4/view/by

.field private static final 'c' Ljava/lang/String; = "FragmentStatePagerAdapter"

.field private static final 'd' Z = 0


.field private final 'e' Landroid/support/v4/app/bi;

.field private 'f' Landroid/support/v4/app/cd;

.field private 'g' Ljava/util/ArrayList;

.field private 'h' Ljava/util/ArrayList;

.field private 'i' Landroid/support/v4/app/Fragment;

.method private <init>(Landroid/support/v4/app/bi;)V
aload 0
invokespecial android/support/v4/view/by/<init>()V
aload 0
aconst_null
putfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/by/g Ljava/util/ArrayList;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/by/h Ljava/util/ArrayList;
aload 0
aconst_null
putfield android/support/v4/app/by/i Landroid/support/v4/app/Fragment;
aload 0
aload 1
putfield android/support/v4/app/by/e Landroid/support/v4/app/bi;
return
.limit locals 2
.limit stack 3
.end method

.method public abstract a()Landroid/support/v4/app/Fragment;
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iload 2
if_icmple L0
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 3
aload 3
ifnull L0
aload 3
areturn
L0:
aload 0
getfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
ifnonnull L1
aload 0
aload 0
getfield android/support/v4/app/by/e Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
putfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
L1:
aload 0
invokevirtual android/support/v4/app/by/a()Landroid/support/v4/app/Fragment;
astore 4
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iload 2
if_icmple L2
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment$SavedState
astore 3
aload 3
ifnull L2
aload 4
getfield android/support/v4/app/Fragment/z I
iflt L3
new java/lang/IllegalStateException
dup
ldc "Fragment already active"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 3
ifnull L4
aload 3
getfield android/support/v4/app/Fragment$SavedState/a Landroid/os/Bundle;
ifnull L4
aload 3
getfield android/support/v4/app/Fragment$SavedState/a Landroid/os/Bundle;
astore 3
L5:
aload 4
aload 3
putfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
L2:
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iload 2
if_icmpgt L6
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
aconst_null
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L2
L4:
aconst_null
astore 3
goto L5
L6:
aload 4
iconst_0
invokevirtual android/support/v4/app/Fragment/b(Z)V
aload 4
iconst_0
invokevirtual android/support/v4/app/Fragment/c(Z)V
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
iload 2
aload 4
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
aload 1
invokevirtual android/view/ViewGroup/getId()I
aload 4
invokevirtual android/support/v4/app/cd/a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 4
areturn
.limit locals 5
.limit stack 3
.end method

.method public final a(ILjava/lang/Object;)V
aload 2
checkcast android/support/v4/app/Fragment
astore 2
aload 0
getfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
ifnonnull L0
aload 0
aload 0
getfield android/support/v4/app/by/e Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
putfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
L0:
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iload 1
if_icmpgt L1
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
aconst_null
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L0
L1:
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
iload 1
aload 0
getfield android/support/v4/app/by/e Landroid/support/v4/app/bi;
aload 2
invokevirtual android/support/v4/app/bi/a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
iload 1
aconst_null
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
aload 2
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
return
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
aload 1
ifnull L0
aload 1
checkcast android/os/Bundle
astore 1
aload 1
aload 2
invokevirtual android/os/Bundle/setClassLoader(Ljava/lang/ClassLoader;)V
aload 1
ldc "states"
invokevirtual android/os/Bundle/getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;
astore 2
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 2
ifnull L1
iconst_0
istore 3
L2:
iload 3
aload 2
arraylength
if_icmpge L1
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
aload 2
iload 3
aaload
checkcast android/support/v4/app/Fragment$SavedState
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 3
iconst_1
iadd
istore 3
goto L2
L1:
aload 1
invokevirtual android/os/Bundle/keySet()Ljava/util/Set;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
L3:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 4
aload 4
ldc "f"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
aload 4
iconst_1
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 3
aload 0
getfield android/support/v4/app/by/e Landroid/support/v4/app/bi;
aload 1
aload 4
invokevirtual android/support/v4/app/bi/a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
astore 5
aload 5
ifnull L4
L5:
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iload 3
if_icmpgt L6
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
aconst_null
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L5
L6:
aload 5
iconst_0
invokevirtual android/support/v4/app/Fragment/b(Z)V
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
iload 3
aload 5
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
goto L3
L4:
ldc "FragmentStatePagerAdapter"
new java/lang/StringBuilder
dup
ldc "Bad fragment at key "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L3
L0:
return
.limit locals 6
.limit stack 4
.end method

.method public final a(Ljava/lang/Object;)V
aload 1
checkcast android/support/v4/app/Fragment
astore 1
aload 1
aload 0
getfield android/support/v4/app/by/i Landroid/support/v4/app/Fragment;
if_acmpeq L0
aload 0
getfield android/support/v4/app/by/i Landroid/support/v4/app/Fragment;
ifnull L1
aload 0
getfield android/support/v4/app/by/i Landroid/support/v4/app/Fragment;
iconst_0
invokevirtual android/support/v4/app/Fragment/b(Z)V
aload 0
getfield android/support/v4/app/by/i Landroid/support/v4/app/Fragment;
iconst_0
invokevirtual android/support/v4/app/Fragment/c(Z)V
L1:
aload 1
ifnull L2
aload 1
iconst_1
invokevirtual android/support/v4/app/Fragment/b(Z)V
aload 1
iconst_1
invokevirtual android/support/v4/app/Fragment/c(Z)V
L2:
aload 0
aload 1
putfield android/support/v4/app/by/i Landroid/support/v4/app/Fragment;
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
aload 2
checkcast android/support/v4/app/Fragment
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 1
if_acmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final b()V
return
.limit locals 1
.limit stack 0
.end method

.method public final c()V
aload 0
getfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
ifnull L0
aload 0
getfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
invokevirtual android/support/v4/app/cd/j()I
pop
aload 0
aconst_null
putfield android/support/v4/app/by/f Landroid/support/v4/app/cd;
aload 0
getfield android/support/v4/app/by/e Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/b()Z
pop
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final d()Landroid/os/Parcelable;
aconst_null
astore 2
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L0
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 2
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/support/v4/app/Fragment$SavedState
astore 3
aload 0
getfield android/support/v4/app/by/g Ljava/util/ArrayList;
aload 3
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
pop
aload 2
ldc "states"
aload 3
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
L0:
iconst_0
istore 1
L1:
iload 1
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L2
aload 0
getfield android/support/v4/app/by/h Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 4
aload 2
astore 3
aload 4
ifnull L3
aload 2
astore 3
aload 4
invokevirtual android/support/v4/app/Fragment/k()Z
ifeq L3
aload 2
astore 3
aload 2
ifnonnull L4
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
L4:
new java/lang/StringBuilder
dup
ldc "f"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
aload 0
getfield android/support/v4/app/by/e Landroid/support/v4/app/bi;
aload 3
aload 2
aload 4
invokevirtual android/support/v4/app/bi/a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
L3:
iload 1
iconst_1
iadd
istore 1
aload 3
astore 2
goto L1
L2:
aload 2
areturn
.limit locals 5
.limit stack 4
.end method
