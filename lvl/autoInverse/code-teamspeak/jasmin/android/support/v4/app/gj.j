.bytecode 50.0
.class public synchronized abstract android/support/v4/app/gj
.super java/lang/Object

.field static 'b' I = 0


.field private static final 'c' Ljava/lang/String; = "sharedElement:snapshot:bitmap"

.field private static final 'd' Ljava/lang/String; = "sharedElement:snapshot:imageScaleType"

.field private static final 'e' Ljava/lang/String; = "sharedElement:snapshot:imageMatrix"

.field 'a' Landroid/graphics/Matrix;

.method static <clinit>()V
ldc_w 1048576
putstatic android/support/v4/app/gj/b I
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
aload 0
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 2
aload 0
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
istore 3
iload 2
ifle L0
iload 3
ifgt L1
L0:
aconst_null
areturn
L1:
fconst_1
getstatic android/support/v4/app/gj/b I
i2f
iload 2
iload 3
imul
i2f
fdiv
invokestatic java/lang/Math/min(FF)F
fstore 1
aload 0
instanceof android/graphics/drawable/BitmapDrawable
ifeq L2
fload 1
fconst_1
fcmpl
ifne L2
aload 0
checkcast android/graphics/drawable/BitmapDrawable
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
areturn
L2:
iload 2
i2f
fload 1
fmul
f2i
istore 2
iload 3
i2f
fload 1
fmul
f2i
istore 3
iload 2
iload 3
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 8
new android/graphics/Canvas
dup
aload 8
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 9
aload 0
invokevirtual android/graphics/drawable/Drawable/getBounds()Landroid/graphics/Rect;
astore 10
aload 10
getfield android/graphics/Rect/left I
istore 4
aload 10
getfield android/graphics/Rect/top I
istore 5
aload 10
getfield android/graphics/Rect/right I
istore 6
aload 10
getfield android/graphics/Rect/bottom I
istore 7
aload 0
iconst_0
iconst_0
iload 2
iload 3
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
aload 9
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
aload 0
iload 4
iload 5
iload 6
iload 7
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 8
areturn
.limit locals 11
.limit stack 5
.end method

.method private a(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;
aload 1
instanceof android/widget/ImageView
ifeq L0
aload 1
checkcast android/widget/ImageView
astore 12
aload 12
invokevirtual android/widget/ImageView/getDrawable()Landroid/graphics/drawable/Drawable;
astore 13
aload 12
invokevirtual android/widget/ImageView/getBackground()Landroid/graphics/drawable/Drawable;
astore 11
aload 13
ifnull L0
aload 11
ifnonnull L0
aload 13
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 5
aload 13
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
istore 6
iload 5
ifle L1
iload 6
ifgt L2
L1:
aconst_null
astore 11
L3:
aload 11
ifnull L0
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 1
aload 1
ldc "sharedElement:snapshot:bitmap"
aload 11
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 1
ldc "sharedElement:snapshot:imageScaleType"
aload 12
invokevirtual android/widget/ImageView/getScaleType()Landroid/widget/ImageView$ScaleType;
invokevirtual android/widget/ImageView$ScaleType/toString()Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 12
invokevirtual android/widget/ImageView/getScaleType()Landroid/widget/ImageView$ScaleType;
getstatic android/widget/ImageView$ScaleType/MATRIX Landroid/widget/ImageView$ScaleType;
if_acmpne L4
aload 12
invokevirtual android/widget/ImageView/getImageMatrix()Landroid/graphics/Matrix;
astore 2
bipush 9
newarray float
astore 3
aload 2
aload 3
invokevirtual android/graphics/Matrix/getValues([F)V
aload 1
ldc "sharedElement:snapshot:imageMatrix"
aload 3
invokevirtual android/os/Bundle/putFloatArray(Ljava/lang/String;[F)V
L4:
aload 1
astore 11
L5:
aload 11
areturn
L2:
fconst_1
getstatic android/support/v4/app/gj/b I
i2f
iload 5
iload 6
imul
i2f
fdiv
invokestatic java/lang/Math/min(FF)F
fstore 4
aload 13
instanceof android/graphics/drawable/BitmapDrawable
ifeq L6
fload 4
fconst_1
fcmpl
ifne L6
aload 13
checkcast android/graphics/drawable/BitmapDrawable
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
astore 11
goto L3
L6:
iload 5
i2f
fload 4
fmul
f2i
istore 5
iload 6
i2f
fload 4
fmul
f2i
istore 6
iload 5
iload 6
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 11
new android/graphics/Canvas
dup
aload 11
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 14
aload 13
invokevirtual android/graphics/drawable/Drawable/getBounds()Landroid/graphics/Rect;
astore 15
aload 15
getfield android/graphics/Rect/left I
istore 7
aload 15
getfield android/graphics/Rect/top I
istore 8
aload 15
getfield android/graphics/Rect/right I
istore 9
aload 15
getfield android/graphics/Rect/bottom I
istore 10
aload 13
iconst_0
iconst_0
iload 5
iload 6
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 13
aload 14
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
aload 13
iload 7
iload 8
iload 9
iload 10
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
goto L3
L0:
aload 3
invokevirtual android/graphics/RectF/width()F
invokestatic java/lang/Math/round(F)I
istore 6
aload 3
invokevirtual android/graphics/RectF/height()F
invokestatic java/lang/Math/round(F)I
istore 5
aconst_null
astore 12
aload 12
astore 11
iload 6
ifle L5
aload 12
astore 11
iload 5
ifle L5
fconst_1
getstatic android/support/v4/app/gj/b I
i2f
iload 6
iload 5
imul
i2f
fdiv
invokestatic java/lang/Math/min(FF)F
fstore 4
iload 6
i2f
fload 4
fmul
f2i
istore 6
iload 5
i2f
fload 4
fmul
f2i
istore 5
aload 0
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
ifnonnull L7
aload 0
new android/graphics/Matrix
dup
invokespecial android/graphics/Matrix/<init>()V
putfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
L7:
aload 0
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
aload 2
invokevirtual android/graphics/Matrix/set(Landroid/graphics/Matrix;)V
aload 0
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
aload 3
getfield android/graphics/RectF/left F
fneg
aload 3
getfield android/graphics/RectF/top F
fneg
invokevirtual android/graphics/Matrix/postTranslate(FF)Z
pop
aload 0
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
fload 4
fload 4
invokevirtual android/graphics/Matrix/postScale(FF)Z
pop
iload 6
iload 5
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 2
new android/graphics/Canvas
dup
aload 2
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 3
aload 3
aload 0
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
invokevirtual android/graphics/Canvas/concat(Landroid/graphics/Matrix;)V
aload 1
aload 3
invokevirtual android/view/View/draw(Landroid/graphics/Canvas;)V
aload 2
areturn
.limit locals 16
.limit stack 5
.end method

.method public static a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;
aload 1
instanceof android/os/Bundle
ifeq L0
aload 1
checkcast android/os/Bundle
astore 1
aload 1
ldc "sharedElement:snapshot:bitmap"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/graphics/Bitmap
astore 2
aload 2
ifnonnull L1
aconst_null
areturn
L1:
new android/widget/ImageView
dup
aload 0
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 0
aload 0
aload 2
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 0
aload 1
ldc "sharedElement:snapshot:imageScaleType"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokestatic android/widget/ImageView$ScaleType/valueOf(Ljava/lang/String;)Landroid/widget/ImageView$ScaleType;
invokevirtual android/widget/ImageView/setScaleType(Landroid/widget/ImageView$ScaleType;)V
aload 0
invokevirtual android/widget/ImageView/getScaleType()Landroid/widget/ImageView$ScaleType;
getstatic android/widget/ImageView$ScaleType/MATRIX Landroid/widget/ImageView$ScaleType;
if_acmpne L2
aload 1
ldc "sharedElement:snapshot:imageMatrix"
invokevirtual android/os/Bundle/getFloatArray(Ljava/lang/String;)[F
astore 1
new android/graphics/Matrix
dup
invokespecial android/graphics/Matrix/<init>()V
astore 2
aload 2
aload 1
invokevirtual android/graphics/Matrix/setValues([F)V
aload 0
aload 2
invokevirtual android/widget/ImageView/setImageMatrix(Landroid/graphics/Matrix;)V
L2:
aload 0
areturn
L0:
aload 1
instanceof android/graphics/Bitmap
ifeq L3
aload 1
checkcast android/graphics/Bitmap
astore 1
new android/widget/ImageView
dup
aload 0
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 0
aload 0
aload 1
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
goto L2
L3:
aconst_null
astore 0
goto L2
.limit locals 3
.limit stack 3
.end method

.method private static a()V
return
.limit locals 0
.limit stack 0
.end method

.method private static b()V
return
.limit locals 0
.limit stack 0
.end method

.method private static c()V
return
.limit locals 0
.limit stack 0
.end method

.method private static d()V
return
.limit locals 0
.limit stack 0
.end method
