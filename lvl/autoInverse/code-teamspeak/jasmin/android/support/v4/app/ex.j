.bytecode 50.0
.class public synchronized abstract android/support/v4/app/ex
.super android/app/Service

.method public <init>()V
aload 0
invokespecial android/app/Service/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(ILjava/lang/String;)V
aload 0
invokevirtual android/support/v4/app/ex/getPackageManager()Landroid/content/pm/PackageManager;
iload 1
invokevirtual android/content/pm/PackageManager/getPackagesForUid(I)[Ljava/lang/String;
astore 5
aload 5
arraylength
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 5
iload 3
aaload
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
return
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
new java/lang/SecurityException
dup
new java/lang/StringBuilder
dup
ldc "NotificationSideChannelService: Uid "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " is not authorized for package "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/SecurityException/<init>(Ljava/lang/String;)V
athrow
.limit locals 6
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/app/ex;ILjava/lang/String;)V
aload 0
invokevirtual android/support/v4/app/ex/getPackageManager()Landroid/content/pm/PackageManager;
iload 1
invokevirtual android/content/pm/PackageManager/getPackagesForUid(I)[Ljava/lang/String;
astore 0
aload 0
arraylength
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
aaload
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L2
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
new java/lang/SecurityException
dup
new java/lang/StringBuilder
dup
ldc "NotificationSideChannelService: Uid "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " is not authorized for package "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/SecurityException/<init>(Ljava/lang/String;)V
athrow
L2:
return
.limit locals 5
.limit stack 5
.end method

.method public abstract a()V
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
aload 1
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
ldc "android.support.BIND_NOTIFICATION_SIDE_CHANNEL"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmple L1
L0:
aconst_null
areturn
L1:
new android/support/v4/app/ez
dup
aload 0
iconst_0
invokespecial android/support/v4/app/ez/<init>(Landroid/support/v4/app/ex;B)V
areturn
.limit locals 2
.limit stack 4
.end method
