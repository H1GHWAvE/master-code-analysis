.bytecode 50.0
.class public synchronized android/support/v4/app/dd
.super java/lang/Object

.field public static final 'A' Ljava/lang/String; = "android.bigText"

.field public static final 'B' Ljava/lang/String; = "android.icon"

.field public static final 'C' Ljava/lang/String; = "android.largeIcon"

.field public static final 'D' Ljava/lang/String; = "android.largeIcon.big"

.field public static final 'E' Ljava/lang/String; = "android.progress"

.field public static final 'F' Ljava/lang/String; = "android.progressMax"

.field public static final 'G' Ljava/lang/String; = "android.progressIndeterminate"

.field public static final 'H' Ljava/lang/String; = "android.showChronometer"

.field public static final 'I' Ljava/lang/String; = "android.showWhen"

.field public static final 'J' Ljava/lang/String; = "android.picture"

.field public static final 'K' Ljava/lang/String; = "android.textLines"

.field public static final 'L' Ljava/lang/String; = "android.template"

.field public static final 'M' Ljava/lang/String; = "android.people"

.field public static final 'N' Ljava/lang/String; = "android.backgroundImageUri"

.field public static final 'O' Ljava/lang/String; = "android.mediaSession"

.field public static final 'P' Ljava/lang/String; = "android.compactActions"

.field public static final 'Q' I = 0

.annotation invisible Landroid/support/a/j;
.end annotation
.end field

.field public static final 'R' I = 1


.field public static final 'S' I = 0


.field public static final 'T' I = -1


.field public static final 'U' Ljava/lang/String; = "call"

.field public static final 'V' Ljava/lang/String; = "msg"

.field public static final 'W' Ljava/lang/String; = "email"

.field public static final 'X' Ljava/lang/String; = "event"

.field public static final 'Y' Ljava/lang/String; = "promo"

.field public static final 'Z' Ljava/lang/String; = "alarm"

.field public static final 'a' I = -1


.field public static final 'aa' Ljava/lang/String; = "progress"

.field public static final 'ab' Ljava/lang/String; = "social"

.field public static final 'ac' Ljava/lang/String; = "err"

.field public static final 'ad' Ljava/lang/String; = "transport"

.field public static final 'ae' Ljava/lang/String; = "sys"

.field public static final 'af' Ljava/lang/String; = "service"

.field public static final 'ag' Ljava/lang/String; = "recommendation"

.field public static final 'ah' Ljava/lang/String; = "status"

.field private static final 'ai' Landroid/support/v4/app/du;

.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 4


.field public static final 'e' I = -1


.field public static final 'f' I = 1


.field public static final 'g' I = 2


.field public static final 'h' I = 4


.field public static final 'i' I = 8


.field public static final 'j' I = 16


.field public static final 'k' I = 32


.field public static final 'l' I = 64


.field public static final 'm' I = 128


.field public static final 'n' I = 256


.field public static final 'o' I = 512


.field public static final 'p' I = 0


.field public static final 'q' I = -1


.field public static final 'r' I = -2


.field public static final 's' I = 1


.field public static final 't' I = 2


.field public static final 'u' Ljava/lang/String; = "android.title"

.field public static final 'v' Ljava/lang/String; = "android.title.big"

.field public static final 'w' Ljava/lang/String; = "android.text"

.field public static final 'x' Ljava/lang/String; = "android.subText"

.field public static final 'y' Ljava/lang/String; = "android.infoText"

.field public static final 'z' Ljava/lang/String; = "android.summaryText"

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/support/v4/app/dw
dup
invokespecial android/support/v4/app/dw/<init>()V
putstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
return
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 20
if_icmplt L1
new android/support/v4/app/dv
dup
invokespecial android/support/v4/app/dv/<init>()V
putstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
return
L1:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L2
new android/support/v4/app/ec
dup
invokespecial android/support/v4/app/ec/<init>()V
putstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
return
L2:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L3
new android/support/v4/app/eb
dup
invokespecial android/support/v4/app/eb/<init>()V
putstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
return
L3:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L4
new android/support/v4/app/ea
dup
invokespecial android/support/v4/app/ea/<init>()V
putstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
return
L4:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L5
new android/support/v4/app/dz
dup
invokespecial android/support/v4/app/dz/<init>()V
putstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
return
L5:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmplt L6
new android/support/v4/app/dy
dup
invokespecial android/support/v4/app/dy/<init>()V
putstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
return
L6:
new android/support/v4/app/dx
dup
invokespecial android/support/v4/app/dx/<init>()V
putstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/app/Notification;)Landroid/os/Bundle;
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
aload 0
invokeinterface android/support/v4/app/du/a(Landroid/app/Notification;)Landroid/os/Bundle; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Landroid/app/Notification;I)Landroid/support/v4/app/df;
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
aload 0
iload 1
invokeinterface android/support/v4/app/du/a(Landroid/app/Notification;I)Landroid/support/v4/app/df; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method static synthetic a()Landroid/support/v4/app/du;
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/app/df
invokeinterface android/support/v4/app/db/a(Landroid/support/v4/app/ek;)V 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
aload 1
ifnull L0
aload 1
instanceof android/support/v4/app/dl
ifeq L1
aload 1
checkcast android/support/v4/app/dl
astore 1
aload 0
aload 1
getfield android/support/v4/app/dl/e Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dl/g Z
aload 1
getfield android/support/v4/app/dl/f Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dl/a Ljava/lang/CharSequence;
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
L0:
return
L1:
aload 1
instanceof android/support/v4/app/dt
ifeq L2
aload 1
checkcast android/support/v4/app/dt
astore 1
aload 0
aload 1
getfield android/support/v4/app/dt/e Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dt/g Z
aload 1
getfield android/support/v4/app/dt/f Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dt/a Ljava/util/ArrayList;
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V
return
L2:
aload 1
instanceof android/support/v4/app/dk
ifeq L0
aload 1
checkcast android/support/v4/app/dk
astore 1
aload 0
aload 1
getfield android/support/v4/app/dk/e Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dk/g Z
aload 1
getfield android/support/v4/app/dk/f Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dk/a Landroid/graphics/Bitmap;
aload 1
getfield android/support/v4/app/dk/b Landroid/graphics/Bitmap;
aload 1
getfield android/support/v4/app/dk/c Z
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V
return
.limit locals 2
.limit stack 7
.end method

.method static synthetic a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/app/Notification;
aload 0
aload 1
invokevirtual android/os/Bundle/getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;
astore 3
aload 3
instanceof [Landroid/app/Notification;
ifne L0
aload 3
ifnonnull L1
L0:
aload 3
checkcast [Landroid/app/Notification;
checkcast [Landroid/app/Notification;
areturn
L1:
aload 3
arraylength
anewarray android/app/Notification
astore 4
iconst_0
istore 2
L2:
iload 2
aload 3
arraylength
if_icmpge L3
aload 4
iload 2
aload 3
iload 2
aaload
checkcast android/app/Notification
aastore
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 0
aload 1
aload 4
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
aload 4
areturn
.limit locals 5
.limit stack 4
.end method

.method private static b(Landroid/app/Notification;)I
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
aload 0
invokeinterface android/support/v4/app/du/b(Landroid/app/Notification;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/app/df
invokeinterface android/support/v4/app/db/a(Landroid/support/v4/app/ek;)V 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
aload 1
ifnull L0
aload 1
instanceof android/support/v4/app/dl
ifeq L1
aload 1
checkcast android/support/v4/app/dl
astore 1
aload 0
aload 1
getfield android/support/v4/app/dl/e Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dl/g Z
aload 1
getfield android/support/v4/app/dl/f Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dl/a Ljava/lang/CharSequence;
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
L0:
return
L1:
aload 1
instanceof android/support/v4/app/dt
ifeq L2
aload 1
checkcast android/support/v4/app/dt
astore 1
aload 0
aload 1
getfield android/support/v4/app/dt/e Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dt/g Z
aload 1
getfield android/support/v4/app/dt/f Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dt/a Ljava/util/ArrayList;
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V
return
L2:
aload 1
instanceof android/support/v4/app/dk
ifeq L0
aload 1
checkcast android/support/v4/app/dk
astore 1
aload 0
aload 1
getfield android/support/v4/app/dk/e Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dk/g Z
aload 1
getfield android/support/v4/app/dk/f Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dk/a Landroid/graphics/Bitmap;
aload 1
getfield android/support/v4/app/dk/b Landroid/graphics/Bitmap;
aload 1
getfield android/support/v4/app/dk/c Z
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V
return
.limit locals 2
.limit stack 7
.end method

.method private static b(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/app/Notification;
aload 0
aload 1
invokevirtual android/os/Bundle/getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;
astore 3
aload 3
instanceof [Landroid/app/Notification;
ifne L0
aload 3
ifnonnull L1
L0:
aload 3
checkcast [Landroid/app/Notification;
checkcast [Landroid/app/Notification;
areturn
L1:
aload 3
arraylength
anewarray android/app/Notification
astore 4
iconst_0
istore 2
L2:
iload 2
aload 3
arraylength
if_icmpge L3
aload 4
iload 2
aload 3
iload 2
aaload
checkcast android/app/Notification
aastore
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 0
aload 1
aload 4
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
aload 4
areturn
.limit locals 5
.limit stack 4
.end method

.method private static c(Landroid/app/Notification;)Ljava/lang/String;
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
aload 0
invokeinterface android/support/v4/app/du/c(Landroid/app/Notification;)Ljava/lang/String; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/app/Notification;)Z
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
aload 0
invokeinterface android/support/v4/app/du/d(Landroid/app/Notification;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Landroid/app/Notification;)Ljava/lang/String;
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
aload 0
invokeinterface android/support/v4/app/du/e(Landroid/app/Notification;)Ljava/lang/String; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static f(Landroid/app/Notification;)Z
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
aload 0
invokeinterface android/support/v4/app/du/f(Landroid/app/Notification;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static g(Landroid/app/Notification;)Ljava/lang/String;
getstatic android/support/v4/app/dd/ai Landroid/support/v4/app/du;
aload 0
invokeinterface android/support/v4/app/du/g(Landroid/app/Notification;)Ljava/lang/String; 1
areturn
.limit locals 1
.limit stack 2
.end method
