.bytecode 50.0
.class public synchronized abstract android/support/v4/app/bh
.super android/support/v4/app/bf

.field final 'b' Landroid/app/Activity;

.field final 'c' Landroid/content/Context;

.field final 'd' Landroid/os/Handler;

.field final 'e' I

.field final 'f' Landroid/support/v4/app/bl;

.field 'g' Landroid/support/v4/n/v;

.field 'h' Landroid/support/v4/app/ct;

.field 'i' Z

.field 'j' Z

.method private <init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V
aload 0
invokespecial android/support/v4/app/bf/<init>()V
aload 0
new android/support/v4/app/bl
dup
invokespecial android/support/v4/app/bl/<init>()V
putfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 0
aload 1
putfield android/support/v4/app/bh/b Landroid/app/Activity;
aload 0
aload 2
putfield android/support/v4/app/bh/c Landroid/content/Context;
aload 0
aload 3
putfield android/support/v4/app/bh/d Landroid/os/Handler;
aload 0
iload 4
putfield android/support/v4/app/bh/e I
return
.limit locals 5
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;Landroid/os/Handler;I)V
aload 0
aconst_null
aload 1
aload 2
iload 3
invokespecial android/support/v4/app/bh/<init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V
return
.limit locals 4
.limit stack 5
.end method

.method <init>(Landroid/support/v4/app/bb;)V
aload 0
aload 1
aload 1
aload 1
getfield android/support/v4/app/bb/d Landroid/os/Handler;
iconst_0
invokespecial android/support/v4/app/bh/<init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V
return
.limit locals 2
.limit stack 5
.end method

.method private a(Landroid/support/v4/n/v;)V
aload 0
aload 1
putfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mLoadersStarted="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bh/j Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Loader Manager "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc ":"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/ct/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L0:
return
.limit locals 5
.limit stack 5
.end method

.method private a(Z)V
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnonnull L0
L1:
return
L0:
aload 0
getfield android/support/v4/app/bh/j Z
ifeq L1
aload 0
iconst_0
putfield android/support/v4/app/bh/j Z
iload 1
ifeq L2
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/d()V
return
L2:
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/c()V
return
.limit locals 2
.limit stack 2
.end method

.method private j()Landroid/app/Activity;
aload 0
getfield android/support/v4/app/bh/b Landroid/app/Activity;
areturn
.limit locals 1
.limit stack 1
.end method

.method private k()Landroid/content/Context;
aload 0
getfield android/support/v4/app/bh/c Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method private l()Landroid/os/Handler;
aload 0
getfield android/support/v4/app/bh/d Landroid/os/Handler;
areturn
.limit locals 1
.limit stack 1
.end method

.method private m()Landroid/support/v4/app/bl;
aload 0
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
areturn
.limit locals 1
.limit stack 1
.end method

.method private n()Landroid/support/v4/app/ct;
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
areturn
L0:
aload 0
iconst_1
putfield android/support/v4/app/bh/i Z
aload 0
aload 0
ldc "(root)"
aload 0
getfield android/support/v4/app/bh/j Z
iconst_1
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
areturn
.limit locals 1
.limit stack 5
.end method

.method private o()V
aload 0
getfield android/support/v4/app/bh/j Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield android/support/v4/app/bh/j Z
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L1
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/b()V
L2:
aload 0
iconst_1
putfield android/support/v4/app/bh/i Z
return
L1:
aload 0
getfield android/support/v4/app/bh/i Z
ifne L2
aload 0
aload 0
ldc "(root)"
aload 0
getfield android/support/v4/app/bh/j Z
iconst_0
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L2
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/f Z
ifne L2
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/b()V
goto L2
.limit locals 1
.limit stack 5
.end method

.method private p()V
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnonnull L0
return
L0:
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/d()V
return
.limit locals 1
.limit stack 1
.end method

.method private q()V
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnonnull L0
return
L0:
aload 0
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/g()V
return
.limit locals 1
.limit stack 1
.end method

.method private r()V
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
ifnull L0
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
invokevirtual android/support/v4/n/v/size()I
istore 3
iload 3
anewarray android/support/v4/app/ct
astore 4
iload 3
iconst_1
isub
istore 1
L1:
iload 1
iflt L2
aload 4
iload 1
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
iload 1
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
checkcast android/support/v4/app/ct
aastore
iload 1
iconst_1
isub
istore 1
goto L1
L2:
iconst_0
istore 1
L3:
iload 1
iload 3
if_icmpge L0
aload 4
iload 1
aaload
astore 5
aload 5
getfield android/support/v4/app/ct/g Z
ifeq L4
getstatic android/support/v4/app/ct/b Z
ifeq L5
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Finished Retaining in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L5:
aload 5
iconst_0
putfield android/support/v4/app/ct/g Z
aload 5
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 2
L6:
iload 2
iflt L4
aload 5
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 2
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 6
aload 6
getfield android/support/v4/app/cu/i Z
ifeq L7
getstatic android/support/v4/app/ct/b Z
ifeq L8
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Finished Retaining: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L8:
aload 6
iconst_0
putfield android/support/v4/app/cu/i Z
aload 6
getfield android/support/v4/app/cu/h Z
aload 6
getfield android/support/v4/app/cu/j Z
if_icmpeq L7
aload 6
getfield android/support/v4/app/cu/h Z
ifne L7
aload 6
invokevirtual android/support/v4/app/cu/b()V
L7:
aload 6
getfield android/support/v4/app/cu/h Z
ifeq L9
aload 6
getfield android/support/v4/app/cu/e Z
ifeq L9
aload 6
getfield android/support/v4/app/cu/k Z
ifne L9
aload 6
aload 6
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 6
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L9:
iload 2
iconst_1
isub
istore 2
goto L6
L4:
aload 5
invokevirtual android/support/v4/app/ct/f()V
iload 1
iconst_1
iadd
istore 1
goto L3
L0:
return
.limit locals 7
.limit stack 4
.end method

.method final a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
ifnonnull L0
aload 0
new android/support/v4/n/v
dup
invokespecial android/support/v4/n/v/<init>()V
putfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
L0:
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
aload 1
invokevirtual android/support/v4/n/v/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/app/ct
astore 4
aload 4
ifnonnull L1
iload 3
ifeq L2
new android/support/v4/app/ct
dup
aload 1
aload 0
iload 2
invokespecial android/support/v4/app/ct/<init>(Ljava/lang/String;Landroid/support/v4/app/bh;Z)V
astore 4
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
aload 1
aload 4
invokevirtual android/support/v4/n/v/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L2:
aload 4
areturn
L1:
aload 4
aload 0
putfield android/support/v4/app/ct/j Landroid/support/v4/app/bh;
aload 4
areturn
.limit locals 5
.limit stack 5
.end method

.method public a(I)Landroid/view/View;
.annotation invisible Landroid/support/a/z;
.end annotation
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
iload 3
iconst_m1
if_icmpeq L0
new java/lang/IllegalStateException
dup
ldc "Starting activity with a requestCode requires a FragmentActivity host"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/bh/c Landroid/content/Context;
aload 2
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
return
.limit locals 4
.limit stack 3
.end method

.method public a(Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
return
.limit locals 4
.limit stack 0
.end method

.method public a(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
return
.limit locals 4
.limit stack 0
.end method

.method public a()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public a(Ljava/lang/String;)Z
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method final b(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
ifnull L0
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
aload 1
invokevirtual android/support/v4/n/v/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/app/ct
astore 2
aload 2
ifnull L0
aload 2
getfield android/support/v4/app/ct/g Z
ifne L0
aload 2
invokevirtual android/support/v4/app/ct/g()V
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
aload 1
invokevirtual android/support/v4/n/v/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public b()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public c()Landroid/view/LayoutInflater;
aload 0
getfield android/support/v4/app/bh/c Landroid/content/Context;
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
areturn
.limit locals 1
.limit stack 2
.end method

.method public d()V
return
.limit locals 1
.limit stack 0
.end method

.method public e()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public f()I
aload 0
getfield android/support/v4/app/bh/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method g()V
return
.limit locals 1
.limit stack 0
.end method

.method public abstract h()Ljava/lang/Object;
.annotation invisible Landroid/support/a/z;
.end annotation
.end method

.method final i()Landroid/support/v4/n/v;
iconst_0
istore 2
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
ifnull L0
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
invokevirtual android/support/v4/n/v/size()I
istore 4
iload 4
anewarray android/support/v4/app/ct
astore 5
iload 4
iconst_1
isub
istore 1
L1:
iload 1
iflt L2
aload 5
iload 1
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
iload 1
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
checkcast android/support/v4/app/ct
aastore
iload 1
iconst_1
isub
istore 1
goto L1
L2:
iconst_0
istore 1
L3:
iload 1
istore 3
iload 2
iload 4
if_icmpge L4
aload 5
iload 2
aaload
astore 6
aload 6
getfield android/support/v4/app/ct/g Z
ifeq L5
iconst_1
istore 1
L6:
iload 2
iconst_1
iadd
istore 2
goto L3
L5:
aload 6
invokevirtual android/support/v4/app/ct/g()V
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
aload 6
getfield android/support/v4/app/ct/e Ljava/lang/String;
invokevirtual android/support/v4/n/v/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L6
L0:
iconst_0
istore 3
L4:
iload 3
ifeq L7
aload 0
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
areturn
L7:
aconst_null
areturn
.limit locals 7
.limit stack 4
.end method
