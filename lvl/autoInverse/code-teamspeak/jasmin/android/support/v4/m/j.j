.bytecode 50.0
.class public final synchronized android/support/v4/m/j
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "ICUCompatIcs"

.field private static 'b' Ljava/lang/reflect/Method;

.method static <clinit>()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
ldc "libcore.icu.ICU"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
ldc "addLikelySubtags"
iconst_1
anewarray java/lang/Class
dup
iconst_0
ldc java/util/Locale
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic android/support/v4/m/j/b Ljava/lang/reflect/Method;
L1:
return
L2:
astore 0
new java/lang/IllegalStateException
dup
aload 0
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 6
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/util/Locale;)Ljava/lang/String;
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L0 to L1 using L3
L0:
getstatic android/support/v4/m/j/b Ljava/lang/reflect/Method;
aconst_null
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Locale
invokevirtual java/util/Locale/getScript()Ljava/lang/String;
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "ICUCompatIcs"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L4:
aload 0
invokevirtual java/util/Locale/getScript()Ljava/lang/String;
areturn
L3:
astore 1
ldc "ICUCompatIcs"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L4
.limit locals 2
.limit stack 6
.end method
