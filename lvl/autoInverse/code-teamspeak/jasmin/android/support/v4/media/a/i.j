.bytecode 50.0
.class public final synchronized android/support/v4/media/a/i
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;I)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
iload 1
invokevirtual android/media/MediaRouter$UserRouteInfo/setPlaybackType(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/graphics/drawable/Drawable;)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
aload 1
invokevirtual android/media/MediaRouter$UserRouteInfo/setIconDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/CharSequence;)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
aload 1
invokevirtual android/media/MediaRouter$UserRouteInfo/setName(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
aload 1
checkcast android/media/MediaRouter$VolumeCallback
invokevirtual android/media/MediaRouter$UserRouteInfo/setVolumeCallback(Landroid/media/MediaRouter$VolumeCallback;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;I)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
iload 1
invokevirtual android/media/MediaRouter$UserRouteInfo/setPlaybackStream(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/CharSequence;)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
aload 1
invokevirtual android/media/MediaRouter$UserRouteInfo/setStatus(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
aload 1
checkcast android/media/RemoteControlClient
invokevirtual android/media/MediaRouter$UserRouteInfo/setRemoteControlClient(Landroid/media/RemoteControlClient;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Object;I)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
iload 1
invokevirtual android/media/MediaRouter$UserRouteInfo/setVolume(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static d(Ljava/lang/Object;I)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
iload 1
invokevirtual android/media/MediaRouter$UserRouteInfo/setVolumeMax(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static e(Ljava/lang/Object;I)V
aload 0
checkcast android/media/MediaRouter$UserRouteInfo
iload 1
invokevirtual android/media/MediaRouter$UserRouteInfo/setVolumeHandling(I)V
return
.limit locals 2
.limit stack 2
.end method
