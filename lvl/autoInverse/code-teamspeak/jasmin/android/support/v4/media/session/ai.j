.bytecode 50.0
.class final synchronized android/support/v4/media/session/ai
.super java/lang/Object
.implements android/support/v4/media/session/ag

.field private 'A' Z

.field private 'B' Z

.field private 'C' Landroid/support/v4/media/ag;

.field final 'a' Landroid/support/v4/media/session/an;

.field final 'b' Ljava/lang/String;

.field final 'c' Ljava/lang/String;

.field final 'd' Landroid/media/AudioManager;

.field final 'e' Ljava/lang/Object;

.field final 'f' Landroid/os/RemoteCallbackList;

.field 'g' Z

.field 'h' Landroid/support/v4/media/session/ad;

.field 'i' I

.field 'j' Landroid/support/v4/media/MediaMetadataCompat;

.field 'k' Landroid/support/v4/media/session/PlaybackStateCompat;

.field 'l' Landroid/app/PendingIntent;

.field 'm' Ljava/util/List;

.field 'n' Ljava/lang/CharSequence;

.field 'o' I

.field 'p' Landroid/os/Bundle;

.field 'q' I

.field 'r' I

.field 's' Landroid/support/v4/media/ae;

.field private final 't' Landroid/content/Context;

.field private final 'u' Landroid/content/ComponentName;

.field private final 'v' Landroid/app/PendingIntent;

.field private final 'w' Ljava/lang/Object;

.field private final 'x' Landroid/support/v4/media/session/am;

.field private final 'y' Landroid/support/v4/media/session/MediaSessionCompat$Token;

.field private 'z' Z

.method public <init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/ComponentName;Landroid/app/PendingIntent;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putfield android/support/v4/media/session/ai/e Ljava/lang/Object;
aload 0
new android/os/RemoteCallbackList
dup
invokespecial android/os/RemoteCallbackList/<init>()V
putfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
aload 0
iconst_0
putfield android/support/v4/media/session/ai/g Z
aload 0
iconst_0
putfield android/support/v4/media/session/ai/z Z
aload 0
iconst_0
putfield android/support/v4/media/session/ai/A Z
aload 0
iconst_0
putfield android/support/v4/media/session/ai/B Z
aload 0
new android/support/v4/media/session/aj
dup
aload 0
invokespecial android/support/v4/media/session/aj/<init>(Landroid/support/v4/media/session/ai;)V
putfield android/support/v4/media/session/ai/C Landroid/support/v4/media/ag;
aload 3
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "MediaButtonReceiver component may not be null."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v4/media/session/ai/t Landroid/content/Context;
aload 0
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
putfield android/support/v4/media/session/ai/b Ljava/lang/String;
aload 0
aload 1
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
putfield android/support/v4/media/session/ai/d Landroid/media/AudioManager;
aload 0
aload 2
putfield android/support/v4/media/session/ai/c Ljava/lang/String;
aload 0
aload 3
putfield android/support/v4/media/session/ai/u Landroid/content/ComponentName;
aload 0
aload 4
putfield android/support/v4/media/session/ai/v Landroid/app/PendingIntent;
aload 0
new android/support/v4/media/session/am
dup
aload 0
invokespecial android/support/v4/media/session/am/<init>(Landroid/support/v4/media/session/ai;)V
putfield android/support/v4/media/session/ai/x Landroid/support/v4/media/session/am;
aload 0
new android/support/v4/media/session/MediaSessionCompat$Token
dup
aload 0
getfield android/support/v4/media/session/ai/x Landroid/support/v4/media/session/am;
invokespecial android/support/v4/media/session/MediaSessionCompat$Token/<init>(Ljava/lang/Object;)V
putfield android/support/v4/media/session/ai/y Landroid/support/v4/media/session/MediaSessionCompat$Token;
aload 0
new android/support/v4/media/session/an
dup
aload 0
invokestatic android/os/Looper/myLooper()Landroid/os/Looper;
invokespecial android/support/v4/media/session/an/<init>(Landroid/support/v4/media/session/ai;Landroid/os/Looper;)V
putfield android/support/v4/media/session/ai/a Landroid/support/v4/media/session/an;
aload 0
iconst_0
putfield android/support/v4/media/session/ai/o I
aload 0
iconst_1
putfield android/support/v4/media/session/ai/q I
aload 0
iconst_3
putfield android/support/v4/media/session/ai/r I
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L1
aload 0
new android/media/RemoteControlClient
dup
aload 4
invokespecial android/media/RemoteControlClient/<init>(Landroid/app/PendingIntent;)V
putfield android/support/v4/media/session/ai/w Ljava/lang/Object;
return
L1:
aload 0
aconst_null
putfield android/support/v4/media/session/ai/w Ljava/lang/Object;
return
.limit locals 5
.limit stack 5
.end method

.method private static synthetic a(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/ae;
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(II)V
aload 0
getfield android/support/v4/media/session/ai/q I
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
ifnull L1
L1:
return
L0:
aload 0
getfield android/support/v4/media/session/ai/d Landroid/media/AudioManager;
iload 1
aload 0
getfield android/support/v4/media/session/ai/r I
iload 2
invokevirtual android/media/AudioManager/adjustStreamVolume(III)V
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/media/session/ai;II)V
aload 0
getfield android/support/v4/media/session/ai/q I
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
ifnull L1
L1:
return
L0:
aload 0
getfield android/support/v4/media/session/ai/d Landroid/media/AudioManager;
iload 1
aload 0
getfield android/support/v4/media/session/ai/r I
iload 2
invokevirtual android/media/AudioManager/adjustStreamVolume(III)V
return
.limit locals 3
.limit stack 4
.end method

.method private static synthetic a(Landroid/support/v4/media/session/ai;Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
aload 0
aload 1
invokevirtual android/support/v4/media/session/ai/a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Landroid/support/v4/media/session/ai;)I
aload 0
getfield android/support/v4/media/session/ai/q I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b(II)V
aload 0
getfield android/support/v4/media/session/ai/q I
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
ifnull L1
L1:
return
L0:
aload 0
getfield android/support/v4/media/session/ai/d Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/session/ai/r I
iload 1
iload 2
invokevirtual android/media/AudioManager/setStreamVolume(III)V
return
.limit locals 3
.limit stack 4
.end method

.method private b(Landroid/support/v4/media/MediaMetadataCompat;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 2
L3:
iload 2
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 2
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 3
L0:
aload 3
aload 1
invokeinterface android/support/v4/media/session/a/a(Landroid/support/v4/media/MediaMetadataCompat;)V 1
L1:
iload 2
iconst_1
isub
istore 2
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method private b(Landroid/support/v4/media/session/PlaybackStateCompat;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 2
L3:
iload 2
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 2
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 3
L0:
aload 3
aload 1
invokeinterface android/support/v4/media/session/a/a(Landroid/support/v4/media/session/PlaybackStateCompat;)V 1
L1:
iload 2
iconst_1
isub
istore 2
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method static synthetic b(Landroid/support/v4/media/session/ai;II)V
aload 0
getfield android/support/v4/media/session/ai/q I
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
ifnull L1
L1:
return
L0:
aload 0
getfield android/support/v4/media/session/ai/d Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/session/ai/r I
iload 1
iload 2
invokevirtual android/media/AudioManager/setStreamVolume(III)V
return
.limit locals 3
.limit stack 4
.end method

.method private b(Ljava/lang/CharSequence;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 2
L3:
iload 2
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 2
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 3
L0:
aload 3
aload 1
invokeinterface android/support/v4/media/session/a/a(Ljava/lang/CharSequence;)V 1
L1:
iload 2
iconst_1
isub
istore 2
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method private b(Ljava/lang/String;Landroid/os/Bundle;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 3
L3:
iload 3
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 3
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 4
L0:
aload 4
aload 1
aload 2
invokeinterface android/support/v4/media/session/a/a(Ljava/lang/String;Landroid/os/Bundle;)V 2
L1:
iload 3
iconst_1
isub
istore 3
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 4
goto L1
.limit locals 5
.limit stack 3
.end method

.method private b(Ljava/util/List;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 2
L3:
iload 2
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 2
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 3
L0:
aload 3
aload 1
invokeinterface android/support/v4/media/session/a/a(Ljava/util/List;)V 1
L1:
iload 2
iconst_1
isub
istore 2
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method private static synthetic c(Landroid/support/v4/media/session/ai;)I
aload 0
getfield android/support/v4/media/session/ai/r I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic d(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/session/an;
aload 0
getfield android/support/v4/media/session/ai/a Landroid/support/v4/media/session/an;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic e(Landroid/support/v4/media/session/ai;)I
aload 0
getfield android/support/v4/media/session/ai/i I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic f(Landroid/support/v4/media/session/ai;)Z
aload 0
getfield android/support/v4/media/session/ai/g Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic g(Landroid/support/v4/media/session/ai;)Landroid/os/RemoteCallbackList;
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
getfield android/support/v4/media/session/ai/z Z
ifeq L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L1
aload 0
getfield android/support/v4/media/session/ai/B Z
ifne L2
aload 0
getfield android/support/v4/media/session/ai/i I
iconst_1
iand
ifeq L2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L3
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
astore 1
aload 0
getfield android/support/v4/media/session/ai/v Landroid/app/PendingIntent;
astore 2
aload 1
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
aload 2
invokevirtual android/media/AudioManager/registerMediaButtonEventReceiver(Landroid/app/PendingIntent;)V
L4:
aload 0
iconst_1
putfield android/support/v4/media/session/ai/B Z
L1:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L5
aload 0
getfield android/support/v4/media/session/ai/A Z
ifne L6
aload 0
getfield android/support/v4/media/session/ai/i I
iconst_2
iand
ifeq L6
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
astore 1
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
astore 2
aload 1
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
aload 2
checkcast android/media/RemoteControlClient
invokevirtual android/media/AudioManager/registerRemoteControlClient(Landroid/media/RemoteControlClient;)V
aload 0
iconst_1
putfield android/support/v4/media/session/ai/A Z
iconst_1
ireturn
L3:
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
astore 1
aload 0
getfield android/support/v4/media/session/ai/u Landroid/content/ComponentName;
astore 2
aload 1
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
aload 2
invokevirtual android/media/AudioManager/registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V
goto L4
L2:
aload 0
getfield android/support/v4/media/session/ai/B Z
ifeq L1
aload 0
getfield android/support/v4/media/session/ai/i I
iconst_1
iand
ifne L1
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L7
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
aload 0
getfield android/support/v4/media/session/ai/v Landroid/app/PendingIntent;
invokestatic android/support/v4/media/session/av/a(Landroid/content/Context;Landroid/app/PendingIntent;)V
L8:
aload 0
iconst_0
putfield android/support/v4/media/session/ai/B Z
goto L1
L7:
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
aload 0
getfield android/support/v4/media/session/ai/u Landroid/content/ComponentName;
invokestatic android/support/v4/media/session/bh/a(Landroid/content/Context;Landroid/content/ComponentName;)V
goto L8
L6:
aload 0
getfield android/support/v4/media/session/ai/A Z
ifeq L5
aload 0
getfield android/support/v4/media/session/ai/i I
iconst_2
iand
ifne L5
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
iconst_0
invokestatic android/support/v4/media/session/at/a(Ljava/lang/Object;I)V
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
invokestatic android/support/v4/media/session/at/a(Landroid/content/Context;Ljava/lang/Object;)V
aload 0
iconst_0
putfield android/support/v4/media/session/ai/A Z
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/media/session/ai/B Z
ifeq L9
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L10
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
aload 0
getfield android/support/v4/media/session/ai/v Landroid/app/PendingIntent;
invokestatic android/support/v4/media/session/av/a(Landroid/content/Context;Landroid/app/PendingIntent;)V
L11:
aload 0
iconst_0
putfield android/support/v4/media/session/ai/B Z
L9:
aload 0
getfield android/support/v4/media/session/ai/A Z
ifeq L5
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
iconst_0
invokestatic android/support/v4/media/session/at/a(Ljava/lang/Object;I)V
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
invokestatic android/support/v4/media/session/at/a(Landroid/content/Context;Ljava/lang/Object;)V
aload 0
iconst_0
putfield android/support/v4/media/session/ai/A Z
L5:
iconst_0
ireturn
L10:
aload 0
getfield android/support/v4/media/session/ai/t Landroid/content/Context;
aload 0
getfield android/support/v4/media/session/ai/u Landroid/content/ComponentName;
invokestatic android/support/v4/media/session/bh/a(Landroid/content/Context;Landroid/content/ComponentName;)V
goto L11
.limit locals 3
.limit stack 2
.end method

.method private static synthetic h(Landroid/support/v4/media/session/ai;)Ljava/lang/String;
aload 0
getfield android/support/v4/media/session/ai/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 1
L3:
iload 1
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 1
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 2
L0:
aload 2
invokeinterface android/support/v4/media/session/a/a()V 0
L1:
iload 1
iconst_1
isub
istore 1
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/kill()V
return
L2:
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method private static synthetic i(Landroid/support/v4/media/session/ai;)Ljava/lang/String;
aload 0
getfield android/support/v4/media/session/ai/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic j(Landroid/support/v4/media/session/ai;)Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/ai/e Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic k(Landroid/support/v4/media/session/ai;)Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/media/session/ai/l Landroid/app/PendingIntent;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic l(Landroid/support/v4/media/session/ai;)Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/session/ai/d Landroid/media/AudioManager;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic m(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/MediaMetadataCompat;
aload 0
getfield android/support/v4/media/session/ai/j Landroid/support/v4/media/MediaMetadataCompat;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic n(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/session/PlaybackStateCompat;
aload 0
invokevirtual android/support/v4/media/session/ai/f()Landroid/support/v4/media/session/PlaybackStateCompat;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic o(Landroid/support/v4/media/session/ai;)Ljava/util/List;
aload 0
getfield android/support/v4/media/session/ai/m Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic p(Landroid/support/v4/media/session/ai;)Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/ai/n Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic q(Landroid/support/v4/media/session/ai;)Landroid/os/Bundle;
aload 0
getfield android/support/v4/media/session/ai/p Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic r(Landroid/support/v4/media/session/ai;)I
aload 0
getfield android/support/v4/media/session/ai/o I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic s(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/session/ad;
aload 0
getfield android/support/v4/media/session/ai/h Landroid/support/v4/media/session/ad;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic t(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/session/PlaybackStateCompat;
aload 0
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v4/media/session/ai/e Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
iload 1
putfield android/support/v4/media/session/ai/i I
aload 2
monitorexit
L1:
aload 0
invokespecial android/support/v4/media/session/ai/g()Z
pop
return
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method public final a(Landroid/app/PendingIntent;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v4/media/session/ai/e Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
aload 1
putfield android/support/v4/media/session/ai/l Landroid/app/PendingIntent;
aload 2
monitorexit
L1:
return
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
putfield android/support/v4/media/session/ai/p Landroid/os/Bundle;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v4/media/MediaMetadataCompat;)V
.catch all from L0 to L1 using L2
.catch android/os/RemoteException from L3 to L4 using L5
.catch all from L6 to L7 using L2
aconst_null
astore 6
aconst_null
astore 5
aload 0
getfield android/support/v4/media/session/ai/e Ljava/lang/Object;
astore 7
aload 7
monitorenter
L0:
aload 0
aload 1
putfield android/support/v4/media/session/ai/j Landroid/support/v4/media/MediaMetadataCompat;
aload 7
monitorexit
L1:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 2
L8:
iload 2
iflt L9
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 2
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 7
L3:
aload 7
aload 1
invokeinterface android/support/v4/media/session/a/a(Landroid/support/v4/media/MediaMetadataCompat;)V 1
L4:
iload 2
iconst_1
isub
istore 2
goto L8
L2:
astore 1
L6:
aload 7
monitorexit
L7:
aload 1
athrow
L9:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
aload 0
getfield android/support/v4/media/session/ai/z Z
ifne L10
L11:
return
L10:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L12
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
astore 6
aload 1
ifnonnull L13
aload 5
astore 1
L14:
aload 0
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
ifnonnull L15
lconst_0
lstore 3
L16:
aload 6
checkcast android/media/RemoteControlClient
iconst_1
invokevirtual android/media/RemoteControlClient/editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;
astore 5
aload 1
aload 5
invokestatic android/support/v4/media/session/at/a(Landroid/os/Bundle;Landroid/media/RemoteControlClient$MetadataEditor;)V
aload 1
ifnull L17
aload 1
ldc "android.media.metadata.YEAR"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L18
aload 5
bipush 8
aload 1
ldc "android.media.metadata.YEAR"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
invokevirtual android/media/RemoteControlClient$MetadataEditor/putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L18:
aload 1
ldc "android.media.metadata.RATING"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L19
aload 5
bipush 101
aload 1
ldc "android.media.metadata.RATING"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putObject(ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;
pop
L19:
aload 1
ldc "android.media.metadata.USER_RATING"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L17
aload 5
ldc_w 268435457
aload 1
ldc "android.media.metadata.USER_RATING"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putObject(ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;
pop
L17:
lload 3
ldc2_w 128L
land
lconst_0
lcmp
ifeq L20
aload 5
ldc_w 268435457
invokevirtual android/media/RemoteControlClient$MetadataEditor/addEditableKey(I)V
L20:
aload 5
invokevirtual android/media/RemoteControlClient$MetadataEditor/apply()V
return
L13:
aload 1
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
astore 1
goto L14
L15:
aload 0
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
getfield android/support/v4/media/session/PlaybackStateCompat/F J
lstore 3
goto L16
L12:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L11
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
astore 5
aload 1
ifnonnull L21
aload 6
astore 1
L22:
aload 5
checkcast android/media/RemoteControlClient
iconst_1
invokevirtual android/media/RemoteControlClient/editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;
astore 5
aload 1
aload 5
invokestatic android/support/v4/media/session/at/a(Landroid/os/Bundle;Landroid/media/RemoteControlClient$MetadataEditor;)V
aload 5
invokevirtual android/media/RemoteControlClient$MetadataEditor/apply()V
return
L21:
aload 1
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
astore 1
goto L22
L5:
astore 7
goto L4
.limit locals 8
.limit stack 4
.end method

.method public final a(Landroid/support/v4/media/ae;)V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "volumeProvider may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
ifnull L1
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
aconst_null
putfield android/support/v4/media/ae/g Landroid/support/v4/media/ag;
L1:
aload 0
iconst_2
putfield android/support/v4/media/session/ai/q I
aload 0
aload 1
putfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
aload 0
new android/support/v4/media/session/ParcelableVolumeInfo
dup
aload 0
getfield android/support/v4/media/session/ai/q I
aload 0
getfield android/support/v4/media/session/ai/r I
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
getfield android/support/v4/media/ae/d I
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
getfield android/support/v4/media/ae/e I
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
getfield android/support/v4/media/ae/f I
invokespecial android/support/v4/media/session/ParcelableVolumeInfo/<init>(IIIII)V
invokevirtual android/support/v4/media/session/ai/a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
aload 1
aload 0
getfield android/support/v4/media/session/ai/C Landroid/support/v4/media/ag;
putfield android/support/v4/media/ae/g Landroid/support/v4/media/ag;
return
.limit locals 2
.limit stack 8
.end method

.method final a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 2
L3:
iload 2
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 2
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 3
L0:
aload 3
aload 1
invokeinterface android/support/v4/media/session/a/a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V 1
L1:
iload 2
iconst_1
isub
istore 2
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
.catch all from L0 to L1 using L2
.catch android/os/RemoteException from L3 to L4 using L5
.catch all from L6 to L7 using L2
aload 0
getfield android/support/v4/media/session/ai/e Ljava/lang/Object;
astore 11
aload 11
monitorenter
L0:
aload 0
aload 1
putfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
aload 11
monitorexit
L1:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 3
L8:
iload 3
iflt L9
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 3
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 11
L3:
aload 11
aload 1
invokeinterface android/support/v4/media/session/a/a(Landroid/support/v4/media/session/PlaybackStateCompat;)V 1
L4:
iload 3
iconst_1
isub
istore 3
goto L8
L2:
astore 1
L6:
aload 11
monitorexit
L7:
aload 1
athrow
L9:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
aload 0
getfield android/support/v4/media/session/ai/z Z
ifne L10
L11:
return
L10:
aload 1
ifnonnull L12
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L11
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
iconst_0
invokestatic android/support/v4/media/session/at/a(Ljava/lang/Object;I)V
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
lconst_0
invokestatic android/support/v4/media/session/at/a(Ljava/lang/Object;J)V
return
L12:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L13
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
astore 11
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/B I
istore 3
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/C J
lstore 7
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/E F
fstore 2
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/H J
lstore 5
invokestatic android/os/SystemClock/elapsedRealtime()J
lstore 9
iload 3
iconst_3
if_icmpne L14
lload 7
lconst_0
lcmp
ifle L14
lload 5
lconst_0
lcmp
ifle L15
lload 9
lload 5
lsub
lstore 9
lload 9
lstore 5
fload 2
fconst_0
fcmpl
ifle L16
lload 9
lstore 5
fload 2
fconst_1
fcmpl
ifeq L16
lload 9
l2f
fload 2
fmul
f2l
lstore 5
L16:
lload 5
lload 7
ladd
lstore 5
L17:
iload 3
invokestatic android/support/v4/media/session/at/a(I)I
istore 3
aload 11
checkcast android/media/RemoteControlClient
iload 3
lload 5
fload 2
invokevirtual android/media/RemoteControlClient/setPlaybackState(IJF)V
L18:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L19
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
astore 11
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/F J
lstore 5
aload 11
checkcast android/media/RemoteControlClient
astore 1
lload 5
invokestatic android/support/v4/media/session/av/a(J)I
istore 4
iload 4
istore 3
lload 5
ldc2_w 128L
land
lconst_0
lcmp
ifeq L20
iload 4
sipush 512
ior
istore 3
L20:
aload 1
iload 3
invokevirtual android/media/RemoteControlClient/setTransportControlFlags(I)V
return
L13:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L18
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/B I
invokestatic android/support/v4/media/session/at/a(Ljava/lang/Object;I)V
goto L18
L19:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L21
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
astore 11
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/F J
lstore 5
aload 11
checkcast android/media/RemoteControlClient
lload 5
invokestatic android/support/v4/media/session/av/a(J)I
invokevirtual android/media/RemoteControlClient/setTransportControlFlags(I)V
return
L21:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L11
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat/F J
invokestatic android/support/v4/media/session/at/a(Ljava/lang/Object;J)V
return
L5:
astore 11
goto L4
L15:
lconst_0
lstore 5
goto L16
L14:
lload 7
lstore 5
goto L17
.limit locals 12
.limit stack 5
.end method

.method public final a(Landroid/support/v4/media/session/ad;Landroid/os/Handler;)V
aload 1
aload 0
getfield android/support/v4/media/session/ai/h Landroid/support/v4/media/session/ad;
if_acmpne L0
return
L0:
aload 1
ifnull L1
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmpge L2
L1:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L3
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
aconst_null
invokestatic android/support/v4/media/session/av/a(Ljava/lang/Object;Ljava/lang/Object;)V
L3:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L4
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
aconst_null
invokestatic android/support/v4/media/session/ax/a(Ljava/lang/Object;Ljava/lang/Object;)V
L4:
aload 0
aload 1
putfield android/support/v4/media/session/ai/h Landroid/support/v4/media/session/ad;
return
L2:
new android/support/v4/media/session/ak
dup
aload 0
aload 1
invokespecial android/support/v4/media/session/ak/<init>(Landroid/support/v4/media/session/ai;Landroid/support/v4/media/session/ad;)V
astore 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L5
new android/support/v4/media/session/aw
dup
aload 2
invokespecial android/support/v4/media/session/aw/<init>(Landroid/support/v4/media/session/au;)V
astore 3
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
aload 3
invokestatic android/support/v4/media/session/av/a(Ljava/lang/Object;Ljava/lang/Object;)V
L5:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L4
new android/support/v4/media/session/ay
dup
aload 2
invokespecial android/support/v4/media/session/ay/<init>(Landroid/support/v4/media/session/au;)V
astore 2
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
aload 2
invokestatic android/support/v4/media/session/ax/a(Ljava/lang/Object;Ljava/lang/Object;)V
goto L4
.limit locals 4
.limit stack 4
.end method

.method public final a(Ljava/lang/CharSequence;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
aload 1
putfield android/support/v4/media/session/ai/n Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 2
L3:
iload 2
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 2
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 3
L0:
aload 3
aload 1
invokeinterface android/support/v4/media/session/a/a(Ljava/lang/CharSequence;)V 1
L1:
iload 2
iconst_1
isub
istore 2
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 3
L3:
iload 3
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 3
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 4
L0:
aload 4
aload 1
aload 2
invokeinterface android/support/v4/media/session/a/a(Ljava/lang/String;Landroid/os/Bundle;)V 2
L1:
iload 3
iconst_1
isub
istore 3
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 4
goto L1
.limit locals 5
.limit stack 3
.end method

.method public final a(Ljava/util/List;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
aload 1
putfield android/support/v4/media/session/ai/m Ljava/util/List;
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 2
L3:
iload 2
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 2
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 3
L0:
aload 3
aload 1
invokeinterface android/support/v4/media/session/a/a(Ljava/util/List;)V 1
L1:
iload 2
iconst_1
isub
istore 2
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
return
L2:
astore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method public final a(Z)V
iload 1
aload 0
getfield android/support/v4/media/session/ai/z Z
if_icmpne L0
L1:
return
L0:
aload 0
iload 1
putfield android/support/v4/media/session/ai/z Z
aload 0
invokespecial android/support/v4/media/session/ai/g()Z
ifeq L1
aload 0
aload 0
getfield android/support/v4/media/session/ai/j Landroid/support/v4/media/MediaMetadataCompat;
invokevirtual android/support/v4/media/session/ai/a(Landroid/support/v4/media/MediaMetadataCompat;)V
aload 0
aload 0
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
invokevirtual android/support/v4/media/session/ai/a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Z
aload 0
getfield android/support/v4/media/session/ai/z Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
iconst_0
putfield android/support/v4/media/session/ai/z Z
aload 0
iconst_1
putfield android/support/v4/media/session/ai/g Z
aload 0
invokespecial android/support/v4/media/session/ai/g()Z
pop
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/beginBroadcast()I
iconst_1
isub
istore 1
L3:
iload 1
iflt L4
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
iload 1
invokevirtual android/os/RemoteCallbackList/getBroadcastItem(I)Landroid/os/IInterface;
checkcast android/support/v4/media/session/a
astore 2
L0:
aload 2
invokeinterface android/support/v4/media/session/a/a()V 0
L1:
iload 1
iconst_1
isub
istore 1
goto L3
L4:
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/finishBroadcast()V
aload 0
getfield android/support/v4/media/session/ai/f Landroid/os/RemoteCallbackList;
invokevirtual android/os/RemoteCallbackList/kill()V
return
L2:
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final b(I)V
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
ifnull L0
aload 0
getfield android/support/v4/media/session/ai/s Landroid/support/v4/media/ae;
aconst_null
putfield android/support/v4/media/ae/g Landroid/support/v4/media/ag;
L0:
aload 0
iconst_1
putfield android/support/v4/media/session/ai/q I
aload 0
new android/support/v4/media/session/ParcelableVolumeInfo
dup
aload 0
getfield android/support/v4/media/session/ai/q I
aload 0
getfield android/support/v4/media/session/ai/r I
iconst_2
aload 0
getfield android/support/v4/media/session/ai/d Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/session/ai/r I
invokevirtual android/media/AudioManager/getStreamMaxVolume(I)I
aload 0
getfield android/support/v4/media/session/ai/d Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/session/ai/r I
invokevirtual android/media/AudioManager/getStreamVolume(I)I
invokespecial android/support/v4/media/session/ParcelableVolumeInfo/<init>(IIIII)V
invokevirtual android/support/v4/media/session/ai/a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
return
.limit locals 2
.limit stack 9
.end method

.method public final b(Landroid/app/PendingIntent;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final c()Landroid/support/v4/media/session/MediaSessionCompat$Token;
aload 0
getfield android/support/v4/media/session/ai/y Landroid/support/v4/media/session/MediaSessionCompat$Token;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c(I)V
aload 0
iload 1
putfield android/support/v4/media/session/ai/o I
return
.limit locals 2
.limit stack 2
.end method

.method public final d()Ljava/lang/Object;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/ai/w Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method final f()Landroid/support/v4/media/session/PlaybackStateCompat;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
ldc2_w -1L
lstore 3
aload 0
getfield android/support/v4/media/session/ai/e Ljava/lang/Object;
astore 7
aload 7
monitorenter
L0:
aload 0
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
astore 9
L1:
lload 3
lstore 1
L3:
aload 0
getfield android/support/v4/media/session/ai/j Landroid/support/v4/media/MediaMetadataCompat;
ifnull L6
L4:
lload 3
lstore 1
L5:
aload 0
getfield android/support/v4/media/session/ai/j Landroid/support/v4/media/MediaMetadataCompat;
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
ldc "android.media.metadata.DURATION"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L6
aload 0
getfield android/support/v4/media/session/ai/j Landroid/support/v4/media/MediaMetadataCompat;
ldc "android.media.metadata.DURATION"
invokevirtual android/support/v4/media/MediaMetadataCompat/b(Ljava/lang/String;)J
lstore 1
L6:
aload 7
monitorexit
L7:
aconst_null
astore 8
aload 8
astore 7
aload 9
ifnull L10
aload 9
getfield android/support/v4/media/session/PlaybackStateCompat/B I
iconst_3
if_icmpeq L11
aload 9
getfield android/support/v4/media/session/PlaybackStateCompat/B I
iconst_4
if_icmpeq L11
aload 8
astore 7
aload 9
getfield android/support/v4/media/session/PlaybackStateCompat/B I
iconst_5
if_icmpne L10
L11:
aload 9
getfield android/support/v4/media/session/PlaybackStateCompat/H J
lstore 3
invokestatic android/os/SystemClock/elapsedRealtime()J
lstore 5
aload 8
astore 7
lload 3
lconst_0
lcmp
ifle L10
aload 9
getfield android/support/v4/media/session/PlaybackStateCompat/E F
lload 5
lload 3
lsub
l2f
fmul
f2l
aload 9
getfield android/support/v4/media/session/PlaybackStateCompat/C J
ladd
lstore 3
lload 1
lconst_0
lcmp
iflt L12
lload 3
lload 1
lcmp
ifle L12
L13:
new android/support/v4/media/session/bl
dup
aload 9
invokespecial android/support/v4/media/session/bl/<init>(Landroid/support/v4/media/session/PlaybackStateCompat;)V
astore 7
aload 7
aload 9
getfield android/support/v4/media/session/PlaybackStateCompat/B I
lload 1
aload 9
getfield android/support/v4/media/session/PlaybackStateCompat/E F
lload 5
invokevirtual android/support/v4/media/session/bl/a(IJFJ)Landroid/support/v4/media/session/bl;
pop
new android/support/v4/media/session/PlaybackStateCompat
dup
aload 7
getfield android/support/v4/media/session/bl/b I
aload 7
getfield android/support/v4/media/session/bl/c J
aload 7
getfield android/support/v4/media/session/bl/d J
aload 7
getfield android/support/v4/media/session/bl/e F
aload 7
getfield android/support/v4/media/session/bl/f J
aload 7
getfield android/support/v4/media/session/bl/g Ljava/lang/CharSequence;
aload 7
getfield android/support/v4/media/session/bl/h J
aload 7
getfield android/support/v4/media/session/bl/a Ljava/util/List;
aload 7
getfield android/support/v4/media/session/bl/i J
aload 7
getfield android/support/v4/media/session/bl/j Landroid/os/Bundle;
iconst_0
invokespecial android/support/v4/media/session/PlaybackStateCompat/<init>(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;B)V
astore 7
L10:
aload 7
astore 8
aload 7
ifnonnull L14
aload 9
astore 8
L14:
aload 8
areturn
L2:
astore 8
L8:
aload 7
monitorexit
L9:
aload 8
athrow
L12:
lload 3
lconst_0
lcmp
ifge L15
lconst_0
lstore 1
goto L13
L15:
lload 3
lstore 1
goto L13
.limit locals 10
.limit stack 18
.end method
