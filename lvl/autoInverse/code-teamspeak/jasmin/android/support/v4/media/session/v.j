.bytecode 50.0
.class final synchronized android/support/v4/media/session/v
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Object;)Ljava/lang/Object;
new android/media/session/MediaController
dup
aload 0
aload 1
checkcast android/media/session/MediaSession$Token
invokespecial android/media/session/MediaController/<init>(Landroid/content/Context;Landroid/media/session/MediaSession$Token;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/support/v4/media/session/w;)Ljava/lang/Object;
new android/support/v4/media/session/x
dup
aload 0
invokespecial android/support/v4/media/session/x/<init>(Landroid/support/v4/media/session/w;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getTransportControls()Landroid/media/session/MediaController$TransportControls;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;II)V
aload 0
checkcast android/media/session/MediaController
iload 1
iload 2
invokevirtual android/media/session/MediaController/setVolumeTo(II)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController
aload 1
checkcast android/media/session/MediaController$Callback
invokevirtual android/media/session/MediaController/unregisterCallback(Landroid/media/session/MediaController$Callback;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Landroid/os/Handler;)V
aload 0
checkcast android/media/session/MediaController
aload 1
checkcast android/media/session/MediaController$Callback
aload 2
invokevirtual android/media/session/MediaController/registerCallback(Landroid/media/session/MediaController$Callback;Landroid/os/Handler;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
aload 0
checkcast android/media/session/MediaController
aload 1
aload 2
aload 3
invokevirtual android/media/session/MediaController/sendCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
return
.limit locals 4
.limit stack 4
.end method

.method private static a(Ljava/lang/Object;Landroid/view/KeyEvent;)Z
aload 0
checkcast android/media/session/MediaController
aload 1
invokevirtual android/media/session/MediaController/dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getPlaybackState()Landroid/media/session/PlaybackState;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;II)V
aload 0
checkcast android/media/session/MediaController
iload 1
iload 2
invokevirtual android/media/session/MediaController/adjustVolume(II)V
return
.limit locals 3
.limit stack 3
.end method

.method private static c(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getMetadata()Landroid/media/MediaMetadata;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Ljava/lang/Object;)Ljava/util/List;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getQueue()Ljava/util/List;
astore 0
aload 0
ifnonnull L0
aconst_null
areturn
L0:
new java/util/ArrayList
dup
aload 0
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static e(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getQueueTitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Ljava/lang/Object;)Landroid/os/Bundle;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getExtras()Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static g(Ljava/lang/Object;)I
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getRatingType()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static h(Ljava/lang/Object;)J
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getFlags()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static i(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getPlaybackInfo()Landroid/media/session/MediaController$PlaybackInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static j(Ljava/lang/Object;)Landroid/app/PendingIntent;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getSessionActivity()Landroid/app/PendingIntent;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static k(Ljava/lang/Object;)Ljava/lang/String;
aload 0
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getPackageName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
