.bytecode 50.0
.class final synchronized android/support/v4/media/session/bp
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;)I
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getState()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static a(IJJFJLjava/lang/CharSequence;JLjava/util/List;J)Ljava/lang/Object;
new android/media/session/PlaybackState$Builder
dup
invokespecial android/media/session/PlaybackState$Builder/<init>()V
astore 14
aload 14
iload 0
lload 1
fload 5
lload 9
invokevirtual android/media/session/PlaybackState$Builder/setState(IJFJ)Landroid/media/session/PlaybackState$Builder;
pop
aload 14
lload 3
invokevirtual android/media/session/PlaybackState$Builder/setBufferedPosition(J)Landroid/media/session/PlaybackState$Builder;
pop
aload 14
lload 6
invokevirtual android/media/session/PlaybackState$Builder/setActions(J)Landroid/media/session/PlaybackState$Builder;
pop
aload 14
aload 8
invokevirtual android/media/session/PlaybackState$Builder/setErrorMessage(Ljava/lang/CharSequence;)Landroid/media/session/PlaybackState$Builder;
pop
aload 11
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 8
L0:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 14
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/media/session/PlaybackState$CustomAction
invokevirtual android/media/session/PlaybackState$Builder/addCustomAction(Landroid/media/session/PlaybackState$CustomAction;)Landroid/media/session/PlaybackState$Builder;
pop
goto L0
L1:
aload 14
lload 12
invokevirtual android/media/session/PlaybackState$Builder/setActiveQueueItemId(J)Landroid/media/session/PlaybackState$Builder;
pop
aload 14
invokevirtual android/media/session/PlaybackState$Builder/build()Landroid/media/session/PlaybackState;
areturn
.limit locals 15
.limit stack 7
.end method

.method private static b(Ljava/lang/Object;)J
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getPosition()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static c(Ljava/lang/Object;)J
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getBufferedPosition()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Ljava/lang/Object;)F
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getPlaybackSpeed()F
freturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)J
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getActions()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static f(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getErrorMessage()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static g(Ljava/lang/Object;)J
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getLastPositionUpdateTime()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static h(Ljava/lang/Object;)Ljava/util/List;
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getCustomActions()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static i(Ljava/lang/Object;)J
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getActiveQueueItemId()J
lreturn
.limit locals 1
.limit stack 2
.end method
