.bytecode 50.0
.class public final synchronized android/support/v4/media/a/m
.super java/lang/Object
.implements java/lang/Runnable

.field private static final 'a' I = 15000


.field private final 'b' Landroid/hardware/display/DisplayManager;

.field private final 'c' Landroid/os/Handler;

.field private 'd' Ljava/lang/reflect/Method;

.field private 'e' Z

.method private <init>(Landroid/content/Context;Landroid/os/Handler;)V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmpeq L3
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
L3:
aload 0
aload 1
ldc "display"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/hardware/display/DisplayManager
putfield android/support/v4/media/a/m/b Landroid/hardware/display/DisplayManager;
aload 0
aload 2
putfield android/support/v4/media/a/m/c Landroid/os/Handler;
L0:
aload 0
ldc android/hardware/display/DisplayManager
ldc "scanWifiDisplays"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v4/media/a/m/d Ljava/lang/reflect/Method;
L1:
return
L2:
astore 1
return
.limit locals 3
.limit stack 4
.end method

.method private a(I)V
iload 1
iconst_2
iand
ifeq L0
aload 0
getfield android/support/v4/media/a/m/e Z
ifne L1
aload 0
getfield android/support/v4/media/a/m/d Ljava/lang/reflect/Method;
ifnull L2
aload 0
iconst_1
putfield android/support/v4/media/a/m/e Z
aload 0
getfield android/support/v4/media/a/m/c Landroid/os/Handler;
aload 0
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
L1:
return
L2:
ldc "MediaRouterJellybeanMr1"
ldc "Cannot scan for wifi displays because the DisplayManager.scanWifiDisplays() method is not available on this device."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L0:
aload 0
getfield android/support/v4/media/a/m/e Z
ifeq L1
aload 0
iconst_0
putfield android/support/v4/media/a/m/e Z
aload 0
getfield android/support/v4/media/a/m/c Landroid/os/Handler;
aload 0
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final run()V
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L3
aload 0
getfield android/support/v4/media/a/m/e Z
ifeq L4
L0:
aload 0
getfield android/support/v4/media/a/m/d Ljava/lang/reflect/Method;
aload 0
getfield android/support/v4/media/a/m/b Landroid/hardware/display/DisplayManager;
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 0
getfield android/support/v4/media/a/m/c Landroid/os/Handler;
aload 0
ldc2_w 15000L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
L4:
return
L2:
astore 1
ldc "MediaRouterJellybeanMr1"
ldc "Cannot scan for wifi displays."
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L3:
astore 1
ldc "MediaRouterJellybeanMr1"
ldc "Cannot scan for wifi displays."
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
.limit locals 2
.limit stack 4
.end method
