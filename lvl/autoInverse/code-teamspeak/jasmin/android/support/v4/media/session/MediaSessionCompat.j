.bytecode 50.0
.class public final synchronized android/support/v4/media/session/MediaSessionCompat
.super java/lang/Object

.field public static final 'a' I = 1


.field public static final 'b' I = 2


.field private final 'c' Landroid/support/v4/media/session/ag;

.field private final 'd' Landroid/support/v4/media/session/g;

.field private final 'e' Ljava/util/ArrayList;

.method private <init>(Landroid/content/Context;Landroid/support/v4/media/session/ag;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/media/session/MediaSessionCompat/e Ljava/util/ArrayList;
aload 0
aload 2
putfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 0
new android/support/v4/media/session/g
dup
aload 1
aload 0
invokespecial android/support/v4/media/session/g/<init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V
putfield android/support/v4/media/session/MediaSessionCompat/d Landroid/support/v4/media/session/g;
return
.limit locals 3
.limit stack 5
.end method

.method private <init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/ComponentName;Landroid/app/PendingIntent;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/media/session/MediaSessionCompat/e Ljava/util/ArrayList;
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "context must not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L1
new java/lang/IllegalArgumentException
dup
ldc "tag must not be null or empty"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 4
astore 5
aload 3
ifnull L2
aload 4
astore 5
aload 4
ifnonnull L2
new android/content/Intent
dup
ldc "android.intent.action.MEDIA_BUTTON"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 4
aload 4
aload 3
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 1
iconst_0
aload 4
iconst_0
invokestatic android/app/PendingIntent/getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 5
L2:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L3
aload 0
new android/support/v4/media/session/ah
dup
aload 1
aload 2
invokespecial android/support/v4/media/session/ah/<init>(Landroid/content/Context;Ljava/lang/String;)V
putfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 5
invokeinterface android/support/v4/media/session/ag/b(Landroid/app/PendingIntent;)V 1
L4:
aload 0
new android/support/v4/media/session/g
dup
aload 1
aload 0
invokespecial android/support/v4/media/session/g/<init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V
putfield android/support/v4/media/session/MediaSessionCompat/d Landroid/support/v4/media/session/g;
return
L3:
aload 0
new android/support/v4/media/session/ai
dup
aload 1
aload 2
aload 3
aload 5
invokespecial android/support/v4/media/session/ai/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/ComponentName;Landroid/app/PendingIntent;)V
putfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
goto L4
.limit locals 6
.limit stack 7
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Object;)Landroid/support/v4/media/session/MediaSessionCompat;
new android/support/v4/media/session/MediaSessionCompat
dup
aload 0
new android/support/v4/media/session/ah
dup
aload 1
invokespecial android/support/v4/media/session/ah/<init>(Ljava/lang/Object;)V
invokespecial android/support/v4/media/session/MediaSessionCompat/<init>(Landroid/content/Context;Landroid/support/v4/media/session/ag;)V
areturn
.limit locals 2
.limit stack 6
.end method

.method private a(I)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
iload 1
invokeinterface android/support/v4/media/session/ag/a(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/app/PendingIntent;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
invokeinterface android/support/v4/media/session/ag/a(Landroid/app/PendingIntent;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
invokeinterface android/support/v4/media/session/ag/a(Landroid/os/Bundle;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/media/MediaMetadataCompat;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
invokeinterface android/support/v4/media/session/ag/a(Landroid/support/v4/media/MediaMetadataCompat;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/media/ae;)V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "volumeProvider may not be null!"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
invokeinterface android/support/v4/media/session/ag/a(Landroid/support/v4/media/ae;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
invokeinterface android/support/v4/media/session/ag/a(Landroid/support/v4/media/session/PlaybackStateCompat;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/media/session/ad;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
invokeinterface android/support/v4/media/session/ag/a(Landroid/support/v4/media/session/ad;Landroid/os/Handler;)V 2
return
.limit locals 2
.limit stack 4
.end method

.method private a(Landroid/support/v4/media/session/ao;)V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Listener may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/e Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
invokeinterface android/support/v4/media/session/ag/a(Ljava/lang/CharSequence;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)V
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
new java/lang/IllegalArgumentException
dup
ldc "event cannot be null or empty"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
aload 2
invokeinterface android/support/v4/media/session/ag/a(Ljava/lang/String;Landroid/os/Bundle;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/util/List;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
invokeinterface android/support/v4/media/session/ag/a(Ljava/util/List;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
iload 1
invokeinterface android/support/v4/media/session/ag/a(Z)V 1
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/e Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
pop
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method private b(I)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
iload 1
invokeinterface android/support/v4/media/session/ag/b(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/app/PendingIntent;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
invokeinterface android/support/v4/media/session/ag/b(Landroid/app/PendingIntent;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/support/v4/media/session/ad;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
aload 1
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
invokeinterface android/support/v4/media/session/ag/a(Landroid/support/v4/media/session/ad;Landroid/os/Handler;)V 2
return
.limit locals 2
.limit stack 4
.end method

.method private b(Landroid/support/v4/media/session/ao;)V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Listener may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/e Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 3
.end method

.method private b()Z
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
invokeinterface android/support/v4/media/session/ag/a()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
invokeinterface android/support/v4/media/session/ag/b()V 0
return
.limit locals 1
.limit stack 1
.end method

.method private c(I)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
iload 1
invokeinterface android/support/v4/media/session/ag/c(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private d()Landroid/support/v4/media/session/g;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/d Landroid/support/v4/media/session/g;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
invokeinterface android/support/v4/media/session/ag/d()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
invokeinterface android/support/v4/media/session/ag/e()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Landroid/support/v4/media/session/MediaSessionCompat$Token;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat/c Landroid/support/v4/media/session/ag;
invokeinterface android/support/v4/media/session/ag/c()Landroid/support/v4/media/session/MediaSessionCompat$Token; 0
areturn
.limit locals 1
.limit stack 1
.end method
