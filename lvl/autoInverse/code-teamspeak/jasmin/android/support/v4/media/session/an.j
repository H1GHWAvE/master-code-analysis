.bytecode 50.0
.class final synchronized android/support/v4/media/session/an
.super android/os/Handler

.field private static final 'b' I = 1


.field private static final 'c' I = 2


.field private static final 'd' I = 3


.field private static final 'e' I = 4


.field private static final 'f' I = 5


.field private static final 'g' I = 6


.field private static final 'h' I = 7


.field private static final 'i' I = 8


.field private static final 'j' I = 9


.field private static final 'k' I = 10


.field private static final 'l' I = 11


.field private static final 'm' I = 12


.field private static final 'n' I = 13


.field private static final 'o' I = 14


.field private static final 'p' I = 15


.field private static final 'q' I = 16


.field private static final 'r' I = 17


.field private static final 's' I = 18


.field private static final 't' I = 127


.field private static final 'u' I = 126


.field final synthetic 'a' Landroid/support/v4/media/session/ai;

.method public <init>(Landroid/support/v4/media/session/ai;Landroid/os/Looper;)V
aload 0
aload 1
putfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
aload 0
aload 2
invokespecial android/os/Handler/<init>(Landroid/os/Looper;)V
return
.limit locals 3
.limit stack 2
.end method

.method private a(I)V
aload 0
iload 1
aconst_null
invokevirtual android/support/v4/media/session/an/a(ILjava/lang/Object;)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(ILjava/lang/Object;I)V
aload 0
iload 1
iload 3
iconst_0
aload 2
invokevirtual android/support/v4/media/session/an/obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
invokevirtual android/os/Message/sendToTarget()V
return
.limit locals 4
.limit stack 5
.end method

.method private a(Landroid/view/KeyEvent;)V
aload 1
ifnull L0
aload 1
invokevirtual android/view/KeyEvent/getAction()I
ifeq L1
L0:
return
L1:
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
ifnonnull L2
lconst_0
lstore 3
L3:
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
lookupswitch
79 : L4
85 : L4
86 : L5
87 : L6
88 : L7
89 : L8
90 : L9
126 : L10
127 : L11
default : L12
L12:
return
L4:
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
ifnull L0
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
getfield android/support/v4/media/session/PlaybackStateCompat/B I
istore 2
return
L2:
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
getfield android/support/v4/media/session/PlaybackStateCompat/F J
lstore 3
goto L3
L10:
lload 3
ldc2_w 4L
land
lconst_0
lcmp
ifeq L0
return
L11:
lload 3
ldc2_w 2L
land
lconst_0
lcmp
ifeq L0
return
L6:
lload 3
ldc2_w 32L
land
lconst_0
lcmp
ifeq L0
return
L7:
lload 3
ldc2_w 16L
land
lconst_0
lcmp
ifeq L0
return
L5:
lload 3
lconst_1
land
lconst_0
lcmp
ifeq L0
return
L9:
lload 3
ldc2_w 64L
land
lconst_0
lcmp
ifeq L0
return
L8:
lload 3
ldc2_w 8L
land
lconst_0
lcmp
ifeq L0
return
.limit locals 5
.limit stack 4
.end method

.method public final a(ILjava/lang/Object;)V
aload 0
iload 1
aload 2
invokevirtual android/support/v4/media/session/an/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
invokevirtual android/os/Message/sendToTarget()V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(ILjava/lang/Object;Landroid/os/Bundle;)V
aload 0
iload 1
aload 2
invokevirtual android/support/v4/media/session/an/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
astore 2
aload 2
aload 3
invokevirtual android/os/Message/setData(Landroid/os/Bundle;)V
aload 2
invokevirtual android/os/Message/sendToTarget()V
return
.limit locals 4
.limit stack 3
.end method

.method public final handleMessage(Landroid/os/Message;)V
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/h Landroid/support/v4/media/session/ad;
ifnonnull L0
L1:
return
L0:
aload 1
getfield android/os/Message/what I
tableswitch 1
L1
L2
L3
L4
L1
L1
L1
L1
L1
L1
L5
L6
L7
L8
L9
L10
L11
L12
default : L13
L13:
return
L2:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
astore 5
aload 1
invokevirtual android/os/Message/getData()Landroid/os/Bundle;
pop
return
L3:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
astore 5
aload 1
invokevirtual android/os/Message/getData()Landroid/os/Bundle;
pop
return
L12:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
astore 5
aload 1
invokevirtual android/os/Message/getData()Landroid/os/Bundle;
pop
return
L4:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
pop2
return
L5:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
pop2
return
L6:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
astore 1
return
L7:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
astore 5
aload 1
invokevirtual android/os/Message/getData()Landroid/os/Bundle;
pop
return
L8:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast android/view/KeyEvent
astore 1
new android/content/Intent
dup
ldc "android.intent.action.MEDIA_BUTTON"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
ldc "android.intent.extra.KEY_EVENT"
aload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 1
ifnull L1
aload 1
invokevirtual android/view/KeyEvent/getAction()I
ifne L1
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
ifnonnull L14
lconst_0
lstore 3
L15:
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
lookupswitch
79 : L16
85 : L16
86 : L17
87 : L18
88 : L19
89 : L20
90 : L21
126 : L22
127 : L23
default : L24
L24:
return
L16:
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
ifnull L1
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
getfield android/support/v4/media/session/PlaybackStateCompat/B I
istore 2
return
L14:
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
getfield android/support/v4/media/session/ai/k Landroid/support/v4/media/session/PlaybackStateCompat;
getfield android/support/v4/media/session/PlaybackStateCompat/F J
lstore 3
goto L15
L22:
ldc2_w 4L
lload 3
land
lconst_0
lcmp
ifeq L1
return
L23:
ldc2_w 2L
lload 3
land
lconst_0
lcmp
ifeq L1
return
L18:
ldc2_w 32L
lload 3
land
lconst_0
lcmp
ifeq L1
return
L19:
ldc2_w 16L
lload 3
land
lconst_0
lcmp
ifeq L1
return
L17:
lconst_1
lload 3
land
lconst_0
lcmp
ifeq L1
return
L21:
ldc2_w 64L
lload 3
land
lconst_0
lcmp
ifeq L1
return
L20:
ldc2_w 8L
lload 3
land
lconst_0
lcmp
ifeq L1
return
L9:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
astore 1
return
L10:
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_0
invokestatic android/support/v4/media/session/ai/a(Landroid/support/v4/media/session/ai;II)V
return
L11:
aload 0
getfield android/support/v4/media/session/an/a Landroid/support/v4/media/session/ai;
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_0
invokestatic android/support/v4/media/session/ai/b(Landroid/support/v4/media/session/ai;II)V
return
.limit locals 6
.limit stack 4
.end method
