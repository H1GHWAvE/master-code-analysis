.bytecode 50.0
.class public final synchronized android/support/v4/media/session/y
.super java/lang/Object

.field private static final 'a' I = 4


.field private static final 'b' I = 6


.field private static final 'c' I = 7


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/media/AudioAttributes;)I
iconst_3
istore 1
aload 0
invokevirtual android/media/AudioAttributes/getFlags()I
iconst_1
iand
iconst_1
if_icmpne L0
bipush 7
istore 1
L1:
iload 1
ireturn
L0:
aload 0
invokevirtual android/media/AudioAttributes/getFlags()I
iconst_4
iand
iconst_4
if_icmpne L2
bipush 6
ireturn
L2:
aload 0
invokevirtual android/media/AudioAttributes/getUsage()I
tableswitch 1
L1
L3
L4
L5
L6
L7
L6
L6
L6
L6
L1
L1
L8
L1
default : L9
L9:
iconst_3
ireturn
L3:
iconst_0
ireturn
L8:
iconst_1
ireturn
L4:
bipush 8
ireturn
L5:
iconst_4
ireturn
L7:
iconst_2
ireturn
L6:
iconst_5
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;)I
aload 0
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getPlaybackType()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;)Landroid/media/AudioAttributes;
aload 0
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getAudioAttributes()Landroid/media/AudioAttributes;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/lang/Object;)I
iconst_3
istore 1
aload 0
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getAudioAttributes()Landroid/media/AudioAttributes;
astore 0
aload 0
invokevirtual android/media/AudioAttributes/getFlags()I
iconst_1
iand
iconst_1
if_icmpne L0
bipush 7
istore 1
L1:
iload 1
ireturn
L0:
aload 0
invokevirtual android/media/AudioAttributes/getFlags()I
iconst_4
iand
iconst_4
if_icmpne L2
bipush 6
ireturn
L2:
aload 0
invokevirtual android/media/AudioAttributes/getUsage()I
tableswitch 1
L1
L3
L4
L5
L6
L7
L6
L6
L6
L6
L1
L1
L8
L1
default : L9
L9:
iconst_3
ireturn
L3:
iconst_0
ireturn
L8:
iconst_1
ireturn
L4:
bipush 8
ireturn
L5:
iconst_4
ireturn
L7:
iconst_2
ireturn
L6:
iconst_5
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static d(Ljava/lang/Object;)I
aload 0
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getVolumeControl()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)I
aload 0
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getMaxVolume()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Ljava/lang/Object;)I
aload 0
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getCurrentVolume()I
ireturn
.limit locals 1
.limit stack 1
.end method
