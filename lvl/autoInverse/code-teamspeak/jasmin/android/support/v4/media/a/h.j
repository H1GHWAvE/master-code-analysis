.bytecode 50.0
.class public final synchronized android/support/v4/media/a/h
.super java/lang/Object

.field private 'a' Ljava/lang/reflect/Method;

.method public <init>()V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L3
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmple L0
L3:
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
L0:
aload 0
ldc android/media/MediaRouter
ldc "selectRouteInt"
iconst_2
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_1
ldc android/media/MediaRouter$RouteInfo
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v4/media/a/h/a Ljava/lang/reflect/Method;
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 7
.end method

.method private a(Ljava/lang/Object;ILjava/lang/Object;)V
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L3
aload 1
checkcast android/media/MediaRouter
astore 1
aload 3
checkcast android/media/MediaRouter$RouteInfo
astore 3
aload 3
invokevirtual android/media/MediaRouter$RouteInfo/getSupportedTypes()I
ldc_w 8388608
iand
ifne L4
aload 0
getfield android/support/v4/media/a/h/a Ljava/lang/reflect/Method;
ifnull L5
L0:
aload 0
getfield android/support/v4/media/a/h/a Ljava/lang/reflect/Method;
aload 1
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 3
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 4
ldc "MediaRouterJellybean"
ldc "Cannot programmatically select non-user route.  Media routing may not work."
aload 4
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L4:
aload 1
iload 2
aload 3
invokevirtual android/media/MediaRouter/selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V
return
L3:
astore 4
ldc "MediaRouterJellybean"
ldc "Cannot programmatically select non-user route.  Media routing may not work."
aload 4
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L4
L5:
ldc "MediaRouterJellybean"
ldc "Cannot programmatically select non-user route because the platform is missing the selectRouteInt() method.  Media routing may not work."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L4
.limit locals 5
.limit stack 6
.end method
