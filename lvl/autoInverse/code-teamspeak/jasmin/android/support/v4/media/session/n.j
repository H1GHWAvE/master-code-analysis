.bytecode 50.0
.class synchronized android/support/v4/media/session/n
.super java/lang/Object
.implements android/support/v4/media/session/m

.field protected final 'a' Ljava/lang/Object;

.method public <init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
aload 2
getfield android/support/v4/media/session/MediaSessionCompat$Token/a Ljava/lang/Object;
invokestatic android/support/v4/media/session/v/a(Landroid/content/Context;Ljava/lang/Object;)Ljava/lang/Object;
putfield android/support/v4/media/session/n/a Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
ifnonnull L0
new android/os/RemoteException
dup
invokespecial android/os/RemoteException/<init>()V
athrow
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
aload 2
invokevirtual android/support/v4/media/session/MediaSessionCompat/a()Landroid/support/v4/media/session/MediaSessionCompat$Token;
getfield android/support/v4/media/session/MediaSessionCompat$Token/a Ljava/lang/Object;
invokestatic android/support/v4/media/session/v/a(Landroid/content/Context;Ljava/lang/Object;)Ljava/lang/Object;
putfield android/support/v4/media/session/n/a Ljava/lang/Object;
return
.limit locals 3
.limit stack 3
.end method

.method public a()Landroid/support/v4/media/session/r;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getTransportControls()Landroid/media/session/MediaController$TransportControls;
astore 1
aload 1
ifnull L0
new android/support/v4/media/session/s
dup
aload 1
invokespecial android/support/v4/media/session/s/<init>(Ljava/lang/Object;)V
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(II)V
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
iload 1
iload 2
invokevirtual android/media/session/MediaController/setVolumeTo(II)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/v4/media/session/i;)V
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
astore 2
aload 1
invokestatic android/support/v4/media/session/i/c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;
astore 1
aload 2
checkcast android/media/session/MediaController
aload 1
checkcast android/media/session/MediaController$Callback
invokevirtual android/media/session/MediaController/unregisterCallback(Landroid/media/session/MediaController$Callback;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
astore 3
aload 1
invokestatic android/support/v4/media/session/i/c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;
astore 1
aload 3
checkcast android/media/session/MediaController
aload 1
checkcast android/media/session/MediaController$Callback
aload 2
invokevirtual android/media/session/MediaController/registerCallback(Landroid/media/session/MediaController$Callback;Landroid/os/Handler;)V
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
aload 1
aload 2
aload 3
invokevirtual android/media/session/MediaController/sendCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/view/KeyEvent;)Z
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
aload 1
invokevirtual android/media/session/MediaController/dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b()Landroid/support/v4/media/session/PlaybackStateCompat;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getPlaybackState()Landroid/media/session/PlaybackState;
astore 1
aload 1
ifnull L0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/a(Ljava/lang/Object;)Landroid/support/v4/media/session/PlaybackStateCompat;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public final b(II)V
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
iload 1
iload 2
invokevirtual android/media/session/MediaController/adjustVolume(II)V
return
.limit locals 3
.limit stack 3
.end method

.method public final c()Landroid/support/v4/media/MediaMetadataCompat;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getMetadata()Landroid/media/MediaMetadata;
astore 1
aload 1
ifnull L0
aload 1
invokestatic android/support/v4/media/MediaMetadataCompat/a(Ljava/lang/Object;)Landroid/support/v4/media/MediaMetadataCompat;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public final d()Ljava/util/List;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getQueue()Ljava/util/List;
astore 1
aload 1
ifnonnull L0
aconst_null
astore 1
L1:
aload 1
ifnonnull L2
aconst_null
astore 1
L3:
aload 1
areturn
L0:
new java/util/ArrayList
dup
aload 1
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
astore 1
goto L1
L2:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L4:
aload 2
astore 1
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 2
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokestatic android/support/v4/media/session/MediaSessionCompat$QueueItem/a(Ljava/lang/Object;)Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L4
.limit locals 4
.limit stack 3
.end method

.method public final e()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getQueueTitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final f()Landroid/os/Bundle;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getExtras()Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final g()I
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getRatingType()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final h()J
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getFlags()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final i()Landroid/support/v4/media/session/q;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getPlaybackInfo()Landroid/media/session/MediaController$PlaybackInfo;
astore 3
aload 3
ifnull L0
aload 3
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getPlaybackType()I
istore 2
aload 3
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getAudioAttributes()Landroid/media/AudioAttributes;
astore 4
aload 4
invokevirtual android/media/AudioAttributes/getFlags()I
iconst_1
iand
iconst_1
if_icmpne L1
bipush 7
istore 1
L2:
new android/support/v4/media/session/q
dup
iload 2
iload 1
aload 3
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getVolumeControl()I
aload 3
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getMaxVolume()I
aload 3
checkcast android/media/session/MediaController$PlaybackInfo
invokevirtual android/media/session/MediaController$PlaybackInfo/getCurrentVolume()I
invokespecial android/support/v4/media/session/q/<init>(IIIII)V
areturn
L1:
aload 4
invokevirtual android/media/AudioAttributes/getFlags()I
iconst_4
iand
iconst_4
if_icmpne L3
bipush 6
istore 1
goto L2
L3:
aload 4
invokevirtual android/media/AudioAttributes/getUsage()I
tableswitch 1
L4
L5
L6
L7
L8
L9
L8
L8
L8
L8
L4
L4
L10
L4
default : L11
L11:
iconst_3
istore 1
goto L2
L4:
iconst_3
istore 1
goto L2
L10:
iconst_1
istore 1
goto L2
L5:
iconst_0
istore 1
goto L2
L6:
bipush 8
istore 1
goto L2
L7:
iconst_4
istore 1
goto L2
L9:
iconst_2
istore 1
goto L2
L8:
iconst_5
istore 1
goto L2
L0:
aconst_null
areturn
.limit locals 5
.limit stack 7
.end method

.method public final j()Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getSessionActivity()Landroid/app/PendingIntent;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final k()Ljava/lang/String;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
checkcast android/media/session/MediaController
invokevirtual android/media/session/MediaController/getPackageName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final l()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/n/a Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method
