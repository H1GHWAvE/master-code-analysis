.bytecode 50.0
.class public synchronized android/support/v4/media/c
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/os/Parcel;)Ljava/lang/Object;
getstatic android/media/MediaDescription/CREATOR Landroid/os/Parcelable$Creator;
aload 0
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getMediaId()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Landroid/os/Parcel;I)V
aload 0
checkcast android/media/MediaDescription
aload 1
iload 2
invokevirtual android/media/MediaDescription/writeToParcel(Landroid/os/Parcel;I)V
return
.limit locals 3
.limit stack 3
.end method

.method private static b(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getTitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getSubtitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getDescription()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)Landroid/graphics/Bitmap;
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getIconBitmap()Landroid/graphics/Bitmap;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Ljava/lang/Object;)Landroid/net/Uri;
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getIconUri()Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static g(Ljava/lang/Object;)Landroid/os/Bundle;
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getExtras()Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method
