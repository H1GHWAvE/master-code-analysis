.bytecode 50.0
.class public final synchronized android/support/v4/media/session/g
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "MediaControllerCompat"

.field private final 'b' Landroid/support/v4/media/session/m;

.field private final 'c' Landroid/support/v4/media/session/MediaSessionCompat$Token;

.method private <init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 2
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "sessionToken must not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 2
putfield android/support/v4/media/session/g/c Landroid/support/v4/media/session/MediaSessionCompat$Token;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L1
aload 0
new android/support/v4/media/session/n
dup
aload 1
aload 2
invokespecial android/support/v4/media/session/n/<init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
putfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
return
L1:
aload 0
new android/support/v4/media/session/p
dup
aload 0
getfield android/support/v4/media/session/g/c Landroid/support/v4/media/session/MediaSessionCompat$Token;
invokespecial android/support/v4/media/session/p/<init>(Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
putfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
return
.limit locals 3
.limit stack 5
.end method

.method public <init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 2
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "session must not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 2
invokevirtual android/support/v4/media/session/MediaSessionCompat/a()Landroid/support/v4/media/session/MediaSessionCompat$Token;
putfield android/support/v4/media/session/g/c Landroid/support/v4/media/session/MediaSessionCompat$Token;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L1
aload 0
new android/support/v4/media/session/o
dup
aload 1
aload 2
invokespecial android/support/v4/media/session/o/<init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V
putfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
return
L1:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L2
aload 0
new android/support/v4/media/session/n
dup
aload 1
aload 2
invokespecial android/support/v4/media/session/n/<init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V
putfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
return
L2:
aload 0
new android/support/v4/media/session/p
dup
aload 0
getfield android/support/v4/media/session/g/c Landroid/support/v4/media/session/MediaSessionCompat$Token;
invokespecial android/support/v4/media/session/p/<init>(Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
putfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
return
.limit locals 3
.limit stack 5
.end method

.method private a()Landroid/support/v4/media/session/r;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/a()Landroid/support/v4/media/session/r; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(II)V
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
iload 1
iload 2
invokeinterface android/support/v4/media/session/m/a(II)V 2
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/support/v4/media/session/i;)V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "callback cannot be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
astore 2
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
aload 1
aload 2
invokeinterface android/support/v4/media/session/m/a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
new java/lang/IllegalArgumentException
dup
ldc "command cannot be null or empty"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
aload 1
aload 2
aload 3
invokeinterface android/support/v4/media/session/m/a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V 3
return
.limit locals 4
.limit stack 4
.end method

.method private a(Landroid/view/KeyEvent;)Z
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "KeyEvent may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
aload 1
invokeinterface android/support/v4/media/session/m/a(Landroid/view/KeyEvent;)Z 1
ireturn
.limit locals 2
.limit stack 3
.end method

.method private b()Landroid/support/v4/media/session/PlaybackStateCompat;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/b()Landroid/support/v4/media/session/PlaybackStateCompat; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(II)V
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
iload 1
iload 2
invokeinterface android/support/v4/media/session/m/b(II)V 2
return
.limit locals 3
.limit stack 3
.end method

.method private b(Landroid/support/v4/media/session/i;)V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "callback cannot be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
astore 2
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
aload 1
aload 2
invokeinterface android/support/v4/media/session/m/a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method private c()Landroid/support/v4/media/MediaMetadataCompat;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/c()Landroid/support/v4/media/MediaMetadataCompat; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Landroid/support/v4/media/session/i;)V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "callback cannot be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
aload 1
invokeinterface android/support/v4/media/session/m/a(Landroid/support/v4/media/session/i;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private d()Ljava/util/List;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/d()Ljava/util/List; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/e()Ljava/lang/CharSequence; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Landroid/os/Bundle;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/f()Landroid/os/Bundle; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()I
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/g()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()J
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/h()J 0
lreturn
.limit locals 1
.limit stack 2
.end method

.method private i()Landroid/support/v4/media/session/q;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/i()Landroid/support/v4/media/session/q; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private j()Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/j()Landroid/app/PendingIntent; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private k()Landroid/support/v4/media/session/MediaSessionCompat$Token;
aload 0
getfield android/support/v4/media/session/g/c Landroid/support/v4/media/session/MediaSessionCompat$Token;
areturn
.limit locals 1
.limit stack 1
.end method

.method private l()Ljava/lang/String;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/k()Ljava/lang/String; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private m()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/g/b Landroid/support/v4/media/session/m;
invokeinterface android/support/v4/media/session/m/l()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method
