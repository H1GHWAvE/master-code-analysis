.bytecode 50.0
.class public synchronized android/support/v4/media/session/z
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/play()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;J)V
aload 0
checkcast android/media/session/MediaController$TransportControls
lload 1
invokevirtual android/media/session/MediaController$TransportControls/seekTo(J)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
aload 1
checkcast android/media/Rating
invokevirtual android/media/session/MediaController$TransportControls/setRating(Landroid/media/Rating;)V
return
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
aload 1
aload 2
invokevirtual android/media/session/MediaController$TransportControls/sendCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static b(Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/pause()V
return
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;J)V
aload 0
checkcast android/media/session/MediaController$TransportControls
lload 1
invokevirtual android/media/session/MediaController$TransportControls/skipToQueueItem(J)V
return
.limit locals 3
.limit stack 3
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
aload 1
aload 2
invokevirtual android/media/session/MediaController$TransportControls/playFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static c(Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/stop()V
return
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
aload 1
aload 2
invokevirtual android/media/session/MediaController$TransportControls/playFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static d(Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/fastForward()V
return
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/rewind()V
return
.limit locals 1
.limit stack 1
.end method

.method private static f(Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/skipToNext()V
return
.limit locals 1
.limit stack 1
.end method

.method private static g(Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaController$TransportControls
invokevirtual android/media/session/MediaController$TransportControls/skipToPrevious()V
return
.limit locals 1
.limit stack 1
.end method
