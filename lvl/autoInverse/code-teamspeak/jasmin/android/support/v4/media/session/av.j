.bytecode 50.0
.class public final synchronized android/support/v4/media/session/av
.super java/lang/Object

.field private static final 'a' J = 256L


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(J)I
lload 0
invokestatic android/support/v4/media/session/at/a(J)I
istore 3
iload 3
istore 2
ldc2_w 256L
lload 0
land
lconst_0
lcmp
ifeq L0
iload 3
sipush 256
ior
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/support/v4/media/session/au;)Ljava/lang/Object;
new android/support/v4/media/session/aw
dup
aload 0
invokespecial android/support/v4/media/session/aw/<init>(Landroid/support/v4/media/session/au;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/content/Context;Landroid/app/PendingIntent;)V
aload 0
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
aload 1
invokevirtual android/media/AudioManager/unregisterMediaButtonEventReceiver(Landroid/app/PendingIntent;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;IJFJ)V
lconst_0
lstore 9
invokestatic android/os/SystemClock/elapsedRealtime()J
lstore 11
lload 2
lstore 7
iload 1
iconst_3
if_icmpne L0
lload 2
lstore 7
lload 2
lconst_0
lcmp
ifle L0
lload 9
lstore 7
lload 5
lconst_0
lcmp
ifle L1
lload 11
lload 5
lsub
lstore 5
lload 5
lstore 7
fload 4
fconst_0
fcmpl
ifle L1
lload 5
lstore 7
fload 4
fconst_1
fcmpl
ifeq L1
lload 5
l2f
fload 4
fmul
f2l
lstore 7
L1:
lload 2
lload 7
ladd
lstore 7
L0:
iload 1
invokestatic android/support/v4/media/session/at/a(I)I
istore 1
aload 0
checkcast android/media/RemoteControlClient
iload 1
lload 7
fload 4
invokevirtual android/media/RemoteControlClient/setPlaybackState(IJF)V
return
.limit locals 13
.limit stack 5
.end method

.method private static a(Ljava/lang/Object;J)V
aload 0
checkcast android/media/RemoteControlClient
lload 1
invokestatic android/support/v4/media/session/av/a(J)I
invokevirtual android/media/RemoteControlClient/setTransportControlFlags(I)V
return
.limit locals 3
.limit stack 3
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/RemoteControlClient
aload 1
checkcast android/media/RemoteControlClient$OnPlaybackPositionUpdateListener
invokevirtual android/media/RemoteControlClient/setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/content/Context;Landroid/app/PendingIntent;)V
aload 0
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
aload 1
invokevirtual android/media/AudioManager/registerMediaButtonEventReceiver(Landroid/app/PendingIntent;)V
return
.limit locals 2
.limit stack 2
.end method
