.bytecode 50.0
.class final synchronized android/support/v4/media/session/br
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;)Landroid/os/Bundle;
aload 0
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/PlaybackState/getExtras()Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;)Ljava/lang/Object;
new android/media/session/PlaybackState$Builder
dup
invokespecial android/media/session/PlaybackState$Builder/<init>()V
astore 15
aload 15
iload 0
lload 1
fload 5
lload 9
invokevirtual android/media/session/PlaybackState$Builder/setState(IJFJ)Landroid/media/session/PlaybackState$Builder;
pop
aload 15
lload 3
invokevirtual android/media/session/PlaybackState$Builder/setBufferedPosition(J)Landroid/media/session/PlaybackState$Builder;
pop
aload 15
lload 6
invokevirtual android/media/session/PlaybackState$Builder/setActions(J)Landroid/media/session/PlaybackState$Builder;
pop
aload 15
aload 8
invokevirtual android/media/session/PlaybackState$Builder/setErrorMessage(Ljava/lang/CharSequence;)Landroid/media/session/PlaybackState$Builder;
pop
aload 11
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 8
L0:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 15
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/media/session/PlaybackState$CustomAction
invokevirtual android/media/session/PlaybackState$Builder/addCustomAction(Landroid/media/session/PlaybackState$CustomAction;)Landroid/media/session/PlaybackState$Builder;
pop
goto L0
L1:
aload 15
lload 12
invokevirtual android/media/session/PlaybackState$Builder/setActiveQueueItemId(J)Landroid/media/session/PlaybackState$Builder;
pop
aload 15
aload 14
invokevirtual android/media/session/PlaybackState$Builder/setExtras(Landroid/os/Bundle;)Landroid/media/session/PlaybackState$Builder;
pop
aload 15
invokevirtual android/media/session/PlaybackState$Builder/build()Landroid/media/session/PlaybackState;
areturn
.limit locals 16
.limit stack 7
.end method
