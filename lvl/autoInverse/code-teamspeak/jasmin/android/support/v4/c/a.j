.bytecode 50.0
.class public synchronized abstract android/support/v4/c/a
.super android/support/v4/c/aa

.field static final 'a' Ljava/lang/String; = "AsyncTaskLoader"

.field static final 'b' Z = 0


.field volatile 'c' Landroid/support/v4/c/b;

.field volatile 'd' Landroid/support/v4/c/b;

.field 'e' J

.field 'f' J

.field 'g' Landroid/os/Handler;

.field private final 'h' Ljava/util/concurrent/Executor;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
getstatic android/support/v4/c/ai/d Ljava/util/concurrent/Executor;
invokespecial android/support/v4/c/a/<init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
aload 0
aload 1
invokespecial android/support/v4/c/aa/<init>(Landroid/content/Context;)V
aload 0
ldc2_w -10000L
putfield android/support/v4/c/a/f J
aload 0
aload 2
putfield android/support/v4/c/a/h Ljava/util/concurrent/Executor;
return
.limit locals 3
.limit stack 3
.end method

.method private a(J)V
aload 0
lload 1
putfield android/support/v4/c/a/e J
lload 1
lconst_0
lcmp
ifeq L0
aload 0
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
putfield android/support/v4/c/a/g Landroid/os/Handler;
L0:
return
.limit locals 3
.limit stack 4
.end method

.method private b(Landroid/support/v4/c/b;Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
aload 1
if_acmpeq L0
aload 0
aload 1
aload 2
invokevirtual android/support/v4/c/a/a(Landroid/support/v4/c/b;Ljava/lang/Object;)V
return
L0:
aload 0
getfield android/support/v4/c/aa/u Z
ifeq L1
aload 0
aload 2
invokevirtual android/support/v4/c/a/a(Ljava/lang/Object;)V
return
L1:
aload 0
iconst_0
putfield android/support/v4/c/aa/x Z
aload 0
invokestatic android/os/SystemClock/uptimeMillis()J
putfield android/support/v4/c/a/f J
aload 0
aconst_null
putfield android/support/v4/c/a/c Landroid/support/v4/c/b;
aload 0
aload 2
invokevirtual android/support/v4/c/a/b(Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method private o()Ljava/lang/Object;
aload 0
invokevirtual android/support/v4/c/a/d()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private p()Z
aload 0
getfield android/support/v4/c/a/d Landroid/support/v4/c/b;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private q()V
.catch java/lang/InterruptedException from L0 to L1 using L2
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
astore 1
aload 1
ifnull L1
L0:
aload 1
getfield android/support/v4/c/b/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/await()V
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 1
.end method

.method protected final a()V
aload 0
invokespecial android/support/v4/c/aa/a()V
aload 0
invokevirtual android/support/v4/c/a/j()Z
pop
aload 0
new android/support/v4/c/b
dup
aload 0
invokespecial android/support/v4/c/b/<init>(Landroid/support/v4/c/a;)V
putfield android/support/v4/c/a/c Landroid/support/v4/c/b;
aload 0
invokevirtual android/support/v4/c/a/c()V
return
.limit locals 1
.limit stack 4
.end method

.method final a(Landroid/support/v4/c/b;Ljava/lang/Object;)V
aload 0
aload 2
invokevirtual android/support/v4/c/a/a(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/a/d Landroid/support/v4/c/b;
aload 1
if_acmpne L0
aload 0
getfield android/support/v4/c/aa/x Z
ifeq L1
aload 0
iconst_1
putfield android/support/v4/c/aa/w Z
L1:
aload 0
invokestatic android/os/SystemClock/uptimeMillis()J
putfield android/support/v4/c/a/f J
aload 0
aconst_null
putfield android/support/v4/c/a/d Landroid/support/v4/c/b;
aload 0
getfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
ifnull L2
aload 0
getfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
invokeinterface android/support/v4/c/ac/d()V 0
L2:
aload 0
invokevirtual android/support/v4/c/a/c()V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 0
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial android/support/v4/c/aa/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
ifnull L0
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mTask="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
invokevirtual java/io/PrintWriter/print(Ljava/lang/Object;)V
aload 3
ldc " waiting="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
getfield android/support/v4/c/b/b Z
invokevirtual java/io/PrintWriter/println(Z)V
L0:
aload 0
getfield android/support/v4/c/a/d Landroid/support/v4/c/b;
ifnull L1
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mCancellingTask="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/a/d Landroid/support/v4/c/b;
invokevirtual java/io/PrintWriter/print(Ljava/lang/Object;)V
aload 3
ldc " waiting="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/a/d Landroid/support/v4/c/b;
getfield android/support/v4/c/b/b Z
invokevirtual java/io/PrintWriter/println(Z)V
L1:
aload 0
getfield android/support/v4/c/a/e J
lconst_0
lcmp
ifeq L2
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mUpdateThrottle="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 0
getfield android/support/v4/c/a/e J
aload 3
invokestatic android/support/v4/n/x/a(JLjava/io/PrintWriter;)V
aload 3
ldc " mLastLoadCompleteTime="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 0
getfield android/support/v4/c/a/f J
invokestatic android/os/SystemClock/uptimeMillis()J
aload 3
invokestatic android/support/v4/n/x/a(JJLjava/io/PrintWriter;)V
aload 3
invokevirtual java/io/PrintWriter/println()V
L2:
return
.limit locals 5
.limit stack 5
.end method

.method protected final b()Z
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
ifnull L0
aload 0
getfield android/support/v4/c/a/d Landroid/support/v4/c/b;
ifnull L1
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
getfield android/support/v4/c/b/b Z
ifeq L2
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
iconst_0
putfield android/support/v4/c/b/b Z
aload 0
getfield android/support/v4/c/a/g Landroid/os/Handler;
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
L2:
aload 0
aconst_null
putfield android/support/v4/c/a/c Landroid/support/v4/c/b;
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
getfield android/support/v4/c/b/b Z
ifeq L3
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
iconst_0
putfield android/support/v4/c/b/b Z
aload 0
getfield android/support/v4/c/a/g Landroid/os/Handler;
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
aload 0
aconst_null
putfield android/support/v4/c/a/c Landroid/support/v4/c/b;
iconst_0
ireturn
L3:
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
iconst_0
invokevirtual java/util/concurrent/FutureTask/cancel(Z)Z
istore 1
iload 1
ifeq L4
aload 0
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
putfield android/support/v4/c/a/d Landroid/support/v4/c/b;
aload 0
invokevirtual android/support/v4/c/a/e()V
L4:
aload 0
aconst_null
putfield android/support/v4/c/a/c Landroid/support/v4/c/b;
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method final c()V
aload 0
getfield android/support/v4/c/a/d Landroid/support/v4/c/b;
ifnonnull L0
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
ifnull L0
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
getfield android/support/v4/c/b/b Z
ifeq L1
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
iconst_0
putfield android/support/v4/c/b/b Z
aload 0
getfield android/support/v4/c/a/g Landroid/os/Handler;
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
L1:
aload 0
getfield android/support/v4/c/a/e J
lconst_0
lcmp
ifle L2
invokestatic android/os/SystemClock/uptimeMillis()J
aload 0
getfield android/support/v4/c/a/f J
aload 0
getfield android/support/v4/c/a/e J
ladd
lcmp
ifge L2
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
iconst_1
putfield android/support/v4/c/b/b Z
aload 0
getfield android/support/v4/c/a/g Landroid/os/Handler;
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
aload 0
getfield android/support/v4/c/a/f J
aload 0
getfield android/support/v4/c/a/e J
ladd
invokevirtual android/os/Handler/postAtTime(Ljava/lang/Runnable;J)Z
pop
L0:
return
L2:
aload 0
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
aload 0
getfield android/support/v4/c/a/h Ljava/util/concurrent/Executor;
aconst_null
invokevirtual android/support/v4/c/b/a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/support/v4/c/ai;
pop
return
.limit locals 1
.limit stack 6
.end method

.method public abstract d()Ljava/lang/Object;
.end method

.method public e()V
return
.limit locals 1
.limit stack 0
.end method
