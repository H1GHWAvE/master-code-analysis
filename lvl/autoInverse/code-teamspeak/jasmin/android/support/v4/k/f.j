.bytecode 50.0
.class final synchronized android/support/v4/k/f
.super android/support/v4/k/a

.field private 'b' Landroid/content/Context;

.field private 'c' Landroid/net/Uri;

.method <init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V
aload 0
aload 1
invokespecial android/support/v4/k/a/<init>(Landroid/support/v4/k/a;)V
aload 0
aload 2
putfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
aload 3
putfield android/support/v4/k/f/c Landroid/net/Uri;
return
.limit locals 4
.limit stack 2
.end method

.method public final a()Landroid/net/Uri;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v4/k/a;
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
ldc "vnd.android.document/directory"
aload 1
invokestatic android/support/v4/k/c/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
astore 1
aload 1
ifnull L0
new android/support/v4/k/f
dup
aload 0
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 1
invokespecial android/support/v4/k/f/<init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 5
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/k/a;
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
aload 1
aload 2
invokestatic android/support/v4/k/c/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
astore 1
aload 1
ifnull L0
new android/support/v4/k/f
dup
aload 0
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 1
invokespecial android/support/v4/k/f/<init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 5
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
ldc "_display_name"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final b(Ljava/lang/String;)Z
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
astore 2
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
astore 3
aload 2
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 3
aload 1
invokestatic android/provider/DocumentsContract/renameDocument(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
astore 1
aload 1
ifnull L0
aload 0
aload 1
putfield android/support/v4/k/f/c Landroid/net/Uri;
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final c()Ljava/lang/String;
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final d()Z
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final e()Z
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/c(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final f()J
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
ldc "last_modified"
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
lreturn
.limit locals 1
.limit stack 3
.end method

.method public final g()J
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
ldc "_size"
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
lreturn
.limit locals 1
.limit stack 3
.end method

.method public final h()Z
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/d(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final i()Z
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/e(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final j()Z
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/f(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final k()Z
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
invokestatic android/support/v4/k/b/g(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final l()[Landroid/support/v4/k/a;
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 0
getfield android/support/v4/k/f/c Landroid/net/Uri;
invokestatic android/support/v4/k/c/a(Landroid/content/Context;Landroid/net/Uri;)[Landroid/net/Uri;
astore 2
aload 2
arraylength
anewarray android/support/v4/k/a
astore 3
iconst_0
istore 1
L0:
iload 1
aload 2
arraylength
if_icmpge L1
aload 3
iload 1
new android/support/v4/k/f
dup
aload 0
aload 0
getfield android/support/v4/k/f/b Landroid/content/Context;
aload 2
iload 1
aaload
invokespecial android/support/v4/k/f/<init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V
aastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 3
areturn
.limit locals 4
.limit stack 8
.end method
