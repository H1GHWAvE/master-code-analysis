.bytecode 50.0
.class public synchronized abstract android/support/v4/e/a/v
.super android/graphics/drawable/Drawable

.field private static final 'd' I = 3


.field final 'a' Landroid/graphics/Bitmap;

.field 'b' F

.field final 'c' Landroid/graphics/Rect;

.field private 'e' I

.field private 'f' I

.field private final 'g' Landroid/graphics/Paint;

.field private final 'h' Landroid/graphics/BitmapShader;

.field private final 'i' Landroid/graphics/Matrix;

.field private final 'j' Landroid/graphics/RectF;

.field private 'k' Z

.field private 'l' Z

.field private 'm' I

.field private 'n' I

.method <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
aload 0
invokespecial android/graphics/drawable/Drawable/<init>()V
aload 0
sipush 160
putfield android/support/v4/e/a/v/e I
aload 0
bipush 119
putfield android/support/v4/e/a/v/f I
aload 0
new android/graphics/Paint
dup
iconst_3
invokespecial android/graphics/Paint/<init>(I)V
putfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
aload 0
new android/graphics/Matrix
dup
invokespecial android/graphics/Matrix/<init>()V
putfield android/support/v4/e/a/v/i Landroid/graphics/Matrix;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
aload 0
new android/graphics/RectF
dup
invokespecial android/graphics/RectF/<init>()V
putfield android/support/v4/e/a/v/j Landroid/graphics/RectF;
aload 0
iconst_1
putfield android/support/v4/e/a/v/k Z
aload 1
ifnull L0
aload 0
aload 1
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/densityDpi I
putfield android/support/v4/e/a/v/e I
L0:
aload 0
aload 2
putfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
ifnull L1
aload 0
invokespecial android/support/v4/e/a/v/e()V
aload 0
new android/graphics/BitmapShader
dup
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
invokespecial android/graphics/BitmapShader/<init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V
putfield android/support/v4/e/a/v/h Landroid/graphics/BitmapShader;
return
L1:
aload 0
iconst_m1
putfield android/support/v4/e/a/v/n I
aload 0
iconst_m1
putfield android/support/v4/e/a/v/m I
aload 0
aconst_null
putfield android/support/v4/e/a/v/h Landroid/graphics/BitmapShader;
return
.limit locals 3
.limit stack 6
.end method

.method private a(I)V
aload 0
getfield android/support/v4/e/a/v/e I
iload 1
if_icmpeq L0
iload 1
istore 2
iload 1
ifne L1
sipush 160
istore 2
L1:
aload 0
iload 2
putfield android/support/v4/e/a/v/e I
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
ifnull L2
aload 0
invokespecial android/support/v4/e/a/v/e()V
L2:
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokevirtual android/graphics/Canvas/getDensity()I
invokespecial android/support/v4/e/a/v/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/util/DisplayMetrics;)V
aload 0
aload 1
getfield android/util/DisplayMetrics/densityDpi I
invokespecial android/support/v4/e/a/v/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(F)Z
fload 0
ldc_w 0.05F
fcmpl
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private b(I)V
aload 0
getfield android/support/v4/e/a/v/f I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v4/e/a/v/f I
aload 0
iconst_1
putfield android/support/v4/e/a/v/k Z
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private b(Z)V
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method private c()Landroid/graphics/Paint;
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Z)V
aload 0
iload 1
putfield android/support/v4/e/a/v/l Z
aload 0
iconst_1
putfield android/support/v4/e/a/v/k Z
iload 1
ifeq L0
aload 0
invokespecial android/support/v4/e/a/v/h()V
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
aload 0
getfield android/support/v4/e/a/v/h Landroid/graphics/BitmapShader;
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
L1:
return
L0:
aload 0
getfield android/support/v4/e/a/v/b F
fconst_0
fcmpl
ifeq L1
aload 0
iconst_0
putfield android/support/v4/e/a/v/l Z
fconst_0
invokestatic android/support/v4/e/a/v/a(F)Z
ifeq L2
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
aload 0
getfield android/support/v4/e/a/v/h Landroid/graphics/BitmapShader;
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
L3:
aload 0
fconst_0
putfield android/support/v4/e/a/v/b F
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
return
L2:
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
aconst_null
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
goto L3
.limit locals 2
.limit stack 2
.end method

.method private d()Landroid/graphics/Bitmap;
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()V
aload 0
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
aload 0
getfield android/support/v4/e/a/v/e I
invokevirtual android/graphics/Bitmap/getScaledWidth(I)I
putfield android/support/v4/e/a/v/m I
aload 0
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
aload 0
getfield android/support/v4/e/a/v/e I
invokevirtual android/graphics/Bitmap/getScaledHeight(I)I
putfield android/support/v4/e/a/v/n I
return
.limit locals 1
.limit stack 3
.end method

.method private f()I
aload 0
getfield android/support/v4/e/a/v/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/isAntiAlias()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()V
aload 0
aload 0
getfield android/support/v4/e/a/v/n I
aload 0
getfield android/support/v4/e/a/v/m I
invokestatic java/lang/Math/min(II)I
iconst_2
idiv
i2f
putfield android/support/v4/e/a/v/b F
return
.limit locals 1
.limit stack 3
.end method

.method private i()Z
aload 0
getfield android/support/v4/e/a/v/l Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()V
aload 0
getfield android/support/v4/e/a/v/b F
fconst_0
fcmpl
ifne L0
return
L0:
aload 0
iconst_0
putfield android/support/v4/e/a/v/l Z
fconst_0
invokestatic android/support/v4/e/a/v/a(F)Z
ifeq L1
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
aload 0
getfield android/support/v4/e/a/v/h Landroid/graphics/BitmapShader;
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
L2:
aload 0
fconst_0
putfield android/support/v4/e/a/v/b F
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
return
L1:
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
aconst_null
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
goto L2
.limit locals 1
.limit stack 2
.end method

.method private k()F
aload 0
getfield android/support/v4/e/a/v/b F
freturn
.limit locals 1
.limit stack 1
.end method

.method a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 6
.limit stack 2
.end method

.method public a(Z)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public a()Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method final b()V
aload 0
getfield android/support/v4/e/a/v/k Z
ifeq L0
aload 0
getfield android/support/v4/e/a/v/l Z
ifeq L1
aload 0
getfield android/support/v4/e/a/v/m I
aload 0
getfield android/support/v4/e/a/v/n I
invokestatic java/lang/Math/min(II)I
istore 1
aload 0
aload 0
getfield android/support/v4/e/a/v/f I
iload 1
iload 1
aload 0
invokevirtual android/support/v4/e/a/v/getBounds()Landroid/graphics/Rect;
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
invokevirtual android/support/v4/e/a/v/a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/height()I
invokestatic java/lang/Math/min(II)I
istore 1
iconst_0
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
iload 1
isub
iconst_2
idiv
invokestatic java/lang/Math/max(II)I
istore 2
iconst_0
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/height()I
iload 1
isub
iconst_2
idiv
invokestatic java/lang/Math/max(II)I
istore 3
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
iload 2
iload 3
invokevirtual android/graphics/Rect/inset(II)V
aload 0
iload 1
i2f
ldc_w 0.5F
fmul
putfield android/support/v4/e/a/v/b F
L2:
aload 0
getfield android/support/v4/e/a/v/j Landroid/graphics/RectF;
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
invokevirtual android/graphics/RectF/set(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/e/a/v/h Landroid/graphics/BitmapShader;
ifnull L3
aload 0
getfield android/support/v4/e/a/v/i Landroid/graphics/Matrix;
aload 0
getfield android/support/v4/e/a/v/j Landroid/graphics/RectF;
getfield android/graphics/RectF/left F
aload 0
getfield android/support/v4/e/a/v/j Landroid/graphics/RectF;
getfield android/graphics/RectF/top F
invokevirtual android/graphics/Matrix/setTranslate(FF)V
aload 0
getfield android/support/v4/e/a/v/i Landroid/graphics/Matrix;
aload 0
getfield android/support/v4/e/a/v/j Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/width()F
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
invokevirtual android/graphics/Bitmap/getWidth()I
i2f
fdiv
aload 0
getfield android/support/v4/e/a/v/j Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/height()F
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
invokevirtual android/graphics/Bitmap/getHeight()I
i2f
fdiv
invokevirtual android/graphics/Matrix/preScale(FF)Z
pop
aload 0
getfield android/support/v4/e/a/v/h Landroid/graphics/BitmapShader;
aload 0
getfield android/support/v4/e/a/v/i Landroid/graphics/Matrix;
invokevirtual android/graphics/BitmapShader/setLocalMatrix(Landroid/graphics/Matrix;)V
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
aload 0
getfield android/support/v4/e/a/v/h Landroid/graphics/BitmapShader;
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
L3:
aload 0
iconst_0
putfield android/support/v4/e/a/v/k Z
L0:
return
L1:
aload 0
aload 0
getfield android/support/v4/e/a/v/f I
aload 0
getfield android/support/v4/e/a/v/m I
aload 0
getfield android/support/v4/e/a/v/n I
aload 0
invokevirtual android/support/v4/e/a/v/getBounds()Landroid/graphics/Rect;
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
invokevirtual android/support/v4/e/a/v/a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
goto L2
.limit locals 4
.limit stack 6
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
astore 2
aload 2
ifnonnull L0
return
L0:
aload 0
invokevirtual android/support/v4/e/a/v/b()V
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getShader()Landroid/graphics/Shader;
ifnonnull L1
aload 1
aload 2
aconst_null
aload 0
getfield android/support/v4/e/a/v/c Landroid/graphics/Rect;
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
return
L1:
aload 1
aload 0
getfield android/support/v4/e/a/v/j Landroid/graphics/RectF;
aload 0
getfield android/support/v4/e/a/v/b F
aload 0
getfield android/support/v4/e/a/v/b F
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V
return
.limit locals 3
.limit stack 5
.end method

.method public getAlpha()I
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getAlpha()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getColorFilter()Landroid/graphics/ColorFilter;
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getColorFilter()Landroid/graphics/ColorFilter;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicHeight()I
aload 0
getfield android/support/v4/e/a/v/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicWidth()I
aload 0
getfield android/support/v4/e/a/v/m I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOpacity()I
aload 0
getfield android/support/v4/e/a/v/f I
bipush 119
if_icmpne L0
aload 0
getfield android/support/v4/e/a/v/l Z
ifeq L1
L0:
bipush -3
ireturn
L1:
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
astore 1
aload 1
ifnull L0
aload 1
invokevirtual android/graphics/Bitmap/hasAlpha()Z
ifne L0
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getAlpha()I
sipush 255
if_icmplt L0
aload 0
getfield android/support/v4/e/a/v/b F
invokestatic android/support/v4/e/a/v/a(F)Z
ifne L0
iconst_m1
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
aload 0
aload 1
invokespecial android/graphics/drawable/Drawable/onBoundsChange(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/e/a/v/l Z
ifeq L0
aload 0
invokespecial android/support/v4/e/a/v/h()V
L0:
aload 0
iconst_1
putfield android/support/v4/e/a/v/k Z
return
.limit locals 2
.limit stack 2
.end method

.method public setAlpha(I)V
iload 1
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getAlpha()I
if_icmpeq L0
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setAlpha(I)V
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
aload 1
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public setDither(Z)V
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setDither(Z)V
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public setFilterBitmap(Z)V
aload 0
getfield android/support/v4/e/a/v/g Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setFilterBitmap(Z)V
aload 0
invokevirtual android/support/v4/e/a/v/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method
