.bytecode 50.0
.class public final synchronized android/support/v4/e/j
.super java/lang/Object

.field private static final 'a' I = 10


.field private static final 'b' I = 10


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(I)D
iload 0
invokestatic android/graphics/Color/red(I)I
i2d
ldc2_w 255.0D
ddiv
dstore 1
dload 1
ldc2_w 0.03928D
dcmpg
ifge L0
dload 1
ldc2_w 12.92D
ddiv
dstore 1
L1:
iload 0
invokestatic android/graphics/Color/green(I)I
i2d
ldc2_w 255.0D
ddiv
dstore 3
dload 3
ldc2_w 0.03928D
dcmpg
ifge L2
dload 3
ldc2_w 12.92D
ddiv
dstore 3
L3:
iload 0
invokestatic android/graphics/Color/blue(I)I
i2d
ldc2_w 255.0D
ddiv
dstore 5
dload 5
ldc2_w 0.03928D
dcmpg
ifge L4
dload 5
ldc2_w 12.92D
ddiv
dstore 5
L5:
dload 1
ldc2_w 0.2126D
dmul
dload 3
ldc2_w 0.7152D
dmul
dadd
ldc2_w 0.0722D
dload 5
dmul
dadd
dreturn
L0:
dload 1
ldc2_w 0.055D
dadd
ldc2_w 1.055D
ddiv
ldc2_w 2.4D
invokestatic java/lang/Math/pow(DD)D
dstore 1
goto L1
L2:
dload 3
ldc2_w 0.055D
dadd
ldc2_w 1.055D
ddiv
ldc2_w 2.4D
invokestatic java/lang/Math/pow(DD)D
dstore 3
goto L3
L4:
dload 5
ldc2_w 0.055D
dadd
ldc2_w 1.055D
ddiv
ldc2_w 2.4D
invokestatic java/lang/Math/pow(DD)D
dstore 5
goto L5
.limit locals 7
.limit stack 6
.end method

.method private static a(FF)F
fload 0
fconst_0
fcmpg
ifge L0
fconst_0
fstore 2
L1:
fload 2
freturn
L0:
fload 1
fstore 2
fload 0
fload 1
fcmpl
ifgt L1
fload 0
freturn
.limit locals 3
.limit stack 2
.end method

.method public static a(II)I
iload 1
invokestatic android/graphics/Color/alpha(I)I
istore 2
iload 0
invokestatic android/graphics/Color/alpha(I)I
istore 3
sipush 255
sipush 255
iload 2
isub
sipush 255
iload 3
isub
imul
sipush 255
idiv
isub
istore 4
iload 4
iload 0
invokestatic android/graphics/Color/red(I)I
iload 3
iload 1
invokestatic android/graphics/Color/red(I)I
iload 2
iload 4
invokestatic android/support/v4/e/j/a(IIIII)I
iload 0
invokestatic android/graphics/Color/green(I)I
iload 3
iload 1
invokestatic android/graphics/Color/green(I)I
iload 2
iload 4
invokestatic android/support/v4/e/j/a(IIIII)I
iload 0
invokestatic android/graphics/Color/blue(I)I
iload 3
iload 1
invokestatic android/graphics/Color/blue(I)I
iload 2
iload 4
invokestatic android/support/v4/e/j/a(IIIII)I
invokestatic android/graphics/Color/argb(IIII)I
ireturn
.limit locals 5
.limit stack 8
.end method

.method private static a(IIF)I
iconst_0
istore 4
sipush 255
istore 3
iload 1
invokestatic android/graphics/Color/alpha(I)I
sipush 255
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "background can not be translucent"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 0
sipush 255
invokestatic android/support/v4/e/j/b(II)I
iload 1
invokestatic android/support/v4/e/j/d(II)D
fload 2
f2d
dcmpg
ifge L1
iconst_m1
istore 6
L2:
iload 6
ireturn
L1:
iconst_0
istore 5
L3:
iload 3
istore 6
iload 5
bipush 10
if_icmpgt L2
iload 3
istore 6
iload 3
iload 4
isub
bipush 10
if_icmple L2
iload 4
iload 3
iadd
iconst_2
idiv
istore 6
iload 0
iload 6
invokestatic android/support/v4/e/j/b(II)I
iload 1
invokestatic android/support/v4/e/j/d(II)D
fload 2
f2d
dcmpg
ifge L4
iload 6
istore 4
L5:
iload 5
iconst_1
iadd
istore 5
goto L3
L4:
iload 6
istore 3
goto L5
.limit locals 7
.limit stack 4
.end method

.method private static a(IIIII)I
iload 4
ifne L0
iconst_0
ireturn
L0:
iload 0
sipush 255
imul
iload 1
imul
iload 2
iload 3
imul
sipush 255
iload 1
isub
imul
iadd
iload 4
sipush 255
imul
idiv
ireturn
.limit locals 5
.limit stack 4
.end method

.method private static a([F)I
iconst_0
istore 5
aload 0
iconst_0
faload
fstore 1
aload 0
iconst_1
faload
fstore 2
aload 0
iconst_2
faload
fstore 3
fconst_1
fconst_2
fload 3
fmul
fconst_1
fsub
invokestatic java/lang/Math/abs(F)F
fsub
fload 2
fmul
fstore 2
fload 3
ldc_w 0.5F
fload 2
fmul
fsub
fstore 3
fload 2
fconst_1
fload 1
ldc_w 60.0F
fdiv
fconst_2
frem
fconst_1
fsub
invokestatic java/lang/Math/abs(F)F
fsub
fmul
fstore 4
fload 1
f2i
bipush 60
idiv
tableswitch 0
L0
L1
L2
L3
L4
L5
L5
default : L6
L6:
iconst_0
istore 6
iconst_0
istore 7
L7:
iload 7
invokestatic android/support/v4/e/j/b(I)I
iload 6
invokestatic android/support/v4/e/j/b(I)I
iload 5
invokestatic android/support/v4/e/j/b(I)I
invokestatic android/graphics/Color/rgb(III)I
ireturn
L0:
fload 2
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 7
fload 4
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 6
ldc_w 255.0F
fload 3
fmul
invokestatic java/lang/Math/round(F)I
istore 5
goto L7
L1:
fload 4
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 7
fload 2
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 6
ldc_w 255.0F
fload 3
fmul
invokestatic java/lang/Math/round(F)I
istore 5
goto L7
L2:
ldc_w 255.0F
fload 3
fmul
invokestatic java/lang/Math/round(F)I
istore 7
fload 2
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 6
fload 4
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 5
goto L7
L3:
ldc_w 255.0F
fload 3
fmul
invokestatic java/lang/Math/round(F)I
istore 7
fload 4
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 6
fload 2
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 5
goto L7
L4:
fload 4
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 7
ldc_w 255.0F
fload 3
fmul
invokestatic java/lang/Math/round(F)I
istore 6
fload 2
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 5
goto L7
L5:
fload 2
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 7
ldc_w 255.0F
fload 3
fmul
invokestatic java/lang/Math/round(F)I
istore 6
fload 4
fload 3
fadd
ldc_w 255.0F
fmul
invokestatic java/lang/Math/round(F)I
istore 5
goto L7
.limit locals 8
.limit stack 4
.end method

.method private static a(III[F)V
iload 0
i2f
ldc_w 255.0F
fdiv
fstore 4
iload 1
i2f
ldc_w 255.0F
fdiv
fstore 6
iload 2
i2f
ldc_w 255.0F
fdiv
fstore 8
fload 4
fload 6
fload 8
invokestatic java/lang/Math/max(FF)F
invokestatic java/lang/Math/max(FF)F
fstore 9
fload 4
fload 6
fload 8
invokestatic java/lang/Math/min(FF)F
invokestatic java/lang/Math/min(FF)F
fstore 10
fload 9
fload 10
fsub
fstore 5
fload 9
fload 10
fadd
fconst_2
fdiv
fstore 7
fload 9
fload 10
fcmpl
ifne L0
fconst_0
fstore 4
fconst_0
fstore 5
L1:
fload 5
ldc_w 60.0F
fmul
ldc_w 360.0F
frem
fstore 6
fload 6
fstore 5
fload 6
fconst_0
fcmpg
ifge L2
fload 6
ldc_w 360.0F
fadd
fstore 5
L2:
aload 3
iconst_0
fload 5
ldc_w 360.0F
invokestatic android/support/v4/e/j/a(FF)F
fastore
aload 3
iconst_1
fload 4
fconst_1
invokestatic android/support/v4/e/j/a(FF)F
fastore
aload 3
iconst_2
fload 7
fconst_1
invokestatic android/support/v4/e/j/a(FF)F
fastore
return
L0:
fload 9
fload 4
fcmpl
ifne L3
fload 6
fload 8
fsub
fload 5
fdiv
ldc_w 6.0F
frem
fstore 4
L4:
fload 5
fconst_1
fconst_2
fload 7
fmul
fconst_1
fsub
invokestatic java/lang/Math/abs(F)F
fsub
fdiv
fstore 6
fload 4
fstore 5
fload 6
fstore 4
goto L1
L3:
fload 9
fload 6
fcmpl
ifne L5
fload 8
fload 4
fsub
fload 5
fdiv
fconst_2
fadd
fstore 4
goto L4
L5:
fload 4
fload 6
fsub
fload 5
fdiv
ldc_w 4.0F
fadd
fstore 4
goto L4
.limit locals 11
.limit stack 4
.end method

.method private static a(I[F)V
iload 0
invokestatic android/graphics/Color/red(I)I
istore 9
iload 0
invokestatic android/graphics/Color/green(I)I
istore 10
iload 0
invokestatic android/graphics/Color/blue(I)I
istore 0
iload 9
i2f
ldc_w 255.0F
fdiv
fstore 2
iload 10
i2f
ldc_w 255.0F
fdiv
fstore 4
iload 0
i2f
ldc_w 255.0F
fdiv
fstore 6
fload 2
fload 4
fload 6
invokestatic java/lang/Math/max(FF)F
invokestatic java/lang/Math/max(FF)F
fstore 7
fload 2
fload 4
fload 6
invokestatic java/lang/Math/min(FF)F
invokestatic java/lang/Math/min(FF)F
fstore 8
fload 7
fload 8
fsub
fstore 3
fload 7
fload 8
fadd
fconst_2
fdiv
fstore 5
fload 7
fload 8
fcmpl
ifne L0
fconst_0
fstore 2
fconst_0
fstore 3
L1:
fload 3
ldc_w 60.0F
fmul
ldc_w 360.0F
frem
fstore 4
fload 4
fstore 3
fload 4
fconst_0
fcmpg
ifge L2
fload 4
ldc_w 360.0F
fadd
fstore 3
L2:
aload 1
iconst_0
fload 3
ldc_w 360.0F
invokestatic android/support/v4/e/j/a(FF)F
fastore
aload 1
iconst_1
fload 2
fconst_1
invokestatic android/support/v4/e/j/a(FF)F
fastore
aload 1
iconst_2
fload 5
fconst_1
invokestatic android/support/v4/e/j/a(FF)F
fastore
return
L0:
fload 7
fload 2
fcmpl
ifne L3
fload 4
fload 6
fsub
fload 3
fdiv
ldc_w 6.0F
frem
fstore 2
L4:
fload 3
fconst_1
fconst_2
fload 5
fmul
fconst_1
fsub
invokestatic java/lang/Math/abs(F)F
fsub
fdiv
fstore 4
fload 2
fstore 3
fload 4
fstore 2
goto L1
L3:
fload 7
fload 4
fcmpl
ifne L5
fload 6
fload 2
fsub
fload 3
fdiv
fconst_2
fadd
fstore 2
goto L4
L5:
fload 2
fload 4
fsub
fload 3
fdiv
ldc_w 4.0F
fadd
fstore 2
goto L4
.limit locals 11
.limit stack 4
.end method

.method private static b(I)I
iload 0
ifge L0
iconst_0
istore 1
L1:
iload 1
ireturn
L0:
iload 0
istore 1
iload 0
sipush 255
if_icmple L1
sipush 255
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static b(II)I
iload 1
iflt L0
iload 1
sipush 255
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "alpha must be between 0 and 255."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
ldc_w 16777215
iload 0
iand
iload 1
bipush 24
ishl
ior
ireturn
.limit locals 2
.limit stack 3
.end method

.method private static c(II)I
sipush 255
sipush 255
iload 1
isub
sipush 255
iload 0
isub
imul
sipush 255
idiv
isub
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static d(II)D
iload 1
invokestatic android/graphics/Color/alpha(I)I
sipush 255
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "background can not be translucent"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 0
istore 6
iload 0
invokestatic android/graphics/Color/alpha(I)I
sipush 255
if_icmpge L1
iload 0
iload 1
invokestatic android/support/v4/e/j/a(II)I
istore 6
L1:
iload 6
invokestatic android/support/v4/e/j/a(I)D
ldc2_w 0.05D
dadd
dstore 2
iload 1
invokestatic android/support/v4/e/j/a(I)D
ldc2_w 0.05D
dadd
dstore 4
dload 2
dload 4
invokestatic java/lang/Math/max(DD)D
dload 2
dload 4
invokestatic java/lang/Math/min(DD)D
ddiv
dreturn
.limit locals 7
.limit stack 6
.end method
