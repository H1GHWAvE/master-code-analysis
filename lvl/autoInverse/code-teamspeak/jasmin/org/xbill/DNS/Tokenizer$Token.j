.bytecode 50.0
.class public synchronized org/xbill/DNS/Tokenizer$Token
.super java/lang/Object

.field public 'type' I

.field public 'value' Ljava/lang/String;

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield org/xbill/DNS/Tokenizer$Token/type I
aload 0
aconst_null
putfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
return
.limit locals 1
.limit stack 2
.end method

.method <init>(Lorg/xbill/DNS/Tokenizer$1;)V
aload 0
invokespecial org/xbill/DNS/Tokenizer$Token/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method static access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
aload 0
iload 1
aload 2
invokespecial org/xbill/DNS/Tokenizer$Token/set(ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
.limit locals 3
.limit stack 3
.end method

.method private set(ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
iload 1
ifge L0
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
aload 0
iload 1
putfield org/xbill/DNS/Tokenizer$Token/type I
aload 2
ifnonnull L1
aconst_null
astore 2
L2:
aload 0
aload 2
putfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
aload 0
areturn
L1:
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
astore 2
goto L2
.limit locals 3
.limit stack 2
.end method

.method public isEOL()Z
aload 0
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_1
if_icmpeq L0
aload 0
getfield org/xbill/DNS/Tokenizer$Token/type I
ifne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isString()Z
aload 0
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_3
if_icmpeq L0
aload 0
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_4
if_icmpne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Tokenizer$Token/type I
tableswitch 0
L0
L1
L2
L3
L4
L5
default : L6
L6:
ldc "<unknown>"
areturn
L0:
ldc "<eof>"
areturn
L1:
ldc "<eol>"
areturn
L2:
ldc "<whitespace>"
areturn
L3:
new java/lang/StringBuffer
dup
ldc "<identifier: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ">"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L4:
new java/lang/StringBuffer
dup
ldc "<quoted_string: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ">"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L5:
new java/lang/StringBuffer
dup
ldc "<comment: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ">"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
