.bytecode 50.0
.class public synchronized org/xbill/DNS/DNSSEC$SignatureNotYetValidException
.super org/xbill/DNS/DNSSEC$DNSSECException

.field private 'now' Ljava/util/Date;

.field private 'when' Ljava/util/Date;

.method <init>(Ljava/util/Date;Ljava/util/Date;)V
aload 0
ldc "signature is not yet valid"
invokespecial org/xbill/DNS/DNSSEC$DNSSECException/<init>(Ljava/lang/String;)V
aload 0
aload 1
putfield org/xbill/DNS/DNSSEC$SignatureNotYetValidException/when Ljava/util/Date;
aload 0
aload 2
putfield org/xbill/DNS/DNSSEC$SignatureNotYetValidException/now Ljava/util/Date;
return
.limit locals 3
.limit stack 2
.end method

.method public getExpiration()Ljava/util/Date;
aload 0
getfield org/xbill/DNS/DNSSEC$SignatureNotYetValidException/when Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getVerifyTime()Ljava/util/Date;
aload 0
getfield org/xbill/DNS/DNSSEC$SignatureNotYetValidException/now Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method
