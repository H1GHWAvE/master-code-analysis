.bytecode 50.0
.class public synchronized org/xbill/DNS/RRset
.super java/lang/Object
.implements java/io/Serializable

.field private static final 'serialVersionUID' J = -3270249290171239695L


.field private 'nsigs' S

.field private 'position' S

.field private 'rrs' Ljava/util/List;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
iconst_1
invokespecial java/util/ArrayList/<init>(I)V
putfield org/xbill/DNS/RRset/rrs Ljava/util/List;
aload 0
iconst_0
putfield org/xbill/DNS/RRset/nsigs S
aload 0
iconst_0
putfield org/xbill/DNS/RRset/position S
return
.limit locals 1
.limit stack 4
.end method

.method public <init>(Lorg/xbill/DNS/RRset;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
monitorenter
L0:
aload 0
aload 1
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
checkcast java/util/ArrayList
invokevirtual java/util/ArrayList/clone()Ljava/lang/Object;
checkcast java/util/List
putfield org/xbill/DNS/RRset/rrs Ljava/util/List;
aload 0
aload 1
getfield org/xbill/DNS/RRset/nsigs S
putfield org/xbill/DNS/RRset/nsigs S
aload 0
aload 1
getfield org/xbill/DNS/RRset/position S
putfield org/xbill/DNS/RRset/position S
aload 1
monitorexit
L1:
return
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public <init>(Lorg/xbill/DNS/Record;)V
aload 0
invokespecial org/xbill/DNS/RRset/<init>()V
aload 0
aload 1
invokespecial org/xbill/DNS/RRset/safeAddRR(Lorg/xbill/DNS/Record;)V
return
.limit locals 2
.limit stack 2
.end method

.method private iterator(ZZ)Ljava/util/Iterator;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
.catch all from L13 to L14 using L2
.catch all from L14 to L15 using L2
.catch all from L16 to L17 using L2
.catch all from L17 to L18 using L2
.catch all from L19 to L20 using L2
.catch all from L21 to L22 using L2
iconst_0
istore 3
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 5
L1:
iload 1
ifeq L7
L3:
iload 5
aload 0
getfield org/xbill/DNS/RRset/nsigs S
isub
istore 4
L4:
iload 4
ifne L23
L5:
getstatic java/util/Collections/EMPTY_LIST Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 6
L6:
aload 0
monitorexit
aload 6
areturn
L7:
aload 0
getfield org/xbill/DNS/RRset/nsigs S
istore 4
L8:
goto L4
L9:
new java/util/ArrayList
dup
iload 4
invokespecial java/util/ArrayList/<init>(I)V
astore 6
L10:
iload 1
ifeq L21
L11:
aload 6
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
iload 3
iload 4
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
invokeinterface java/util/List/addAll(Ljava/util/Collection;)Z 1
pop
L12:
iload 3
ifeq L14
L13:
aload 6
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
iconst_0
iload 3
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
invokeinterface java/util/List/addAll(Ljava/util/Collection;)Z 1
pop
L14:
aload 6
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 6
L15:
goto L6
L16:
aload 0
getfield org/xbill/DNS/RRset/position S
iload 4
if_icmplt L17
aload 0
iconst_0
putfield org/xbill/DNS/RRset/position S
L17:
aload 0
getfield org/xbill/DNS/RRset/position S
istore 3
aload 0
iload 3
iconst_1
iadd
i2s
putfield org/xbill/DNS/RRset/position S
L18:
goto L9
L2:
astore 6
aload 0
monitorexit
aload 6
athrow
L19:
iload 5
aload 0
getfield org/xbill/DNS/RRset/nsigs S
isub
istore 3
L20:
goto L9
L21:
aload 6
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
iload 3
iload 5
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
invokeinterface java/util/List/addAll(Ljava/util/Collection;)Z 1
pop
L22:
goto L14
L23:
iload 1
ifeq L19
iload 2
ifne L16
goto L9
.limit locals 7
.limit stack 4
.end method

.method private iteratorToString(Ljava/util/Iterator;)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/Record
astore 3
aload 2
ldc "["
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
aload 3
invokevirtual org/xbill/DNS/Record/rdataToString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
ldc "]"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 2
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L0
L1:
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 2
.end method

.method private safeAddRR(Lorg/xbill/DNS/Record;)V
aload 1
instanceof org/xbill/DNS/RRSIGRecord
ifne L0
aload 0
getfield org/xbill/DNS/RRset/nsigs S
ifne L1
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
L1:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
invokeinterface java/util/List/size()I 0
aload 0
getfield org/xbill/DNS/RRset/nsigs S
isub
aload 1
invokeinterface java/util/List/add(ILjava/lang/Object;)V 2
return
L0:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
aload 0
getfield org/xbill/DNS/RRset/nsigs S
iconst_1
iadd
i2s
putfield org/xbill/DNS/RRset/nsigs S
return
.limit locals 2
.limit stack 3
.end method

.method public addRR(Lorg/xbill/DNS/Record;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L2 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
invokeinterface java/util/List/size()I 0
ifne L3
aload 0
aload 1
invokespecial org/xbill/DNS/RRset/safeAddRR(Lorg/xbill/DNS/Record;)V
L1:
aload 0
monitorexit
return
L3:
aload 0
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
astore 4
aload 1
aload 4
invokevirtual org/xbill/DNS/Record/sameRRset(Lorg/xbill/DNS/Record;)Z
ifne L9
new java/lang/IllegalArgumentException
dup
ldc "record does not match rrset"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L9:
aload 1
astore 3
L4:
aload 1
invokevirtual org/xbill/DNS/Record/getTTL()J
aload 4
invokevirtual org/xbill/DNS/Record/getTTL()J
lcmp
ifeq L5
aload 1
invokevirtual org/xbill/DNS/Record/getTTL()J
aload 4
invokevirtual org/xbill/DNS/Record/getTTL()J
lcmp
ifle L10
aload 1
invokevirtual org/xbill/DNS/Record/cloneRecord()Lorg/xbill/DNS/Record;
astore 3
aload 3
aload 4
invokevirtual org/xbill/DNS/Record/getTTL()J
invokevirtual org/xbill/DNS/Record/setTTL(J)V
L5:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
aload 3
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifne L1
aload 0
aload 3
invokespecial org/xbill/DNS/RRset/safeAddRR(Lorg/xbill/DNS/Record;)V
L6:
goto L1
L11:
aload 1
astore 3
L7:
iload 2
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L5
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Record
invokevirtual org/xbill/DNS/Record/cloneRecord()Lorg/xbill/DNS/Record;
astore 3
aload 3
aload 1
invokevirtual org/xbill/DNS/Record/getTTL()J
invokevirtual org/xbill/DNS/Record/setTTL(J)V
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
iload 2
aload 3
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
pop
L8:
iload 2
iconst_1
iadd
istore 2
goto L11
L10:
iconst_0
istore 2
goto L11
.limit locals 5
.limit stack 4
.end method

.method public clear()V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
invokeinterface java/util/List/clear()V 0
aload 0
iconst_0
putfield org/xbill/DNS/RRset/position S
aload 0
iconst_0
putfield org/xbill/DNS/RRset/nsigs S
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public deleteRR(Lorg/xbill/DNS/Record;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
aload 1
invokeinterface java/util/List/remove(Ljava/lang/Object;)Z 1
ifeq L1
aload 1
instanceof org/xbill/DNS/RRSIGRecord
ifeq L1
aload 0
aload 0
getfield org/xbill/DNS/RRset/nsigs S
iconst_1
isub
i2s
putfield org/xbill/DNS/RRset/nsigs S
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public first()Lorg/xbill/DNS/Record;
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
invokeinterface java/util/List/size()I 0
ifne L2
new java/lang/IllegalStateException
dup
ldc "rrset is empty"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 0
monitorexit
aload 1
athrow
L2:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Record
astore 1
L3:
aload 0
monitorexit
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public getDClass()I
aload 0
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getDClass()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getName()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTTL()J
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getTTL()J
lstore 1
L1:
aload 0
monitorexit
lload 1
lreturn
L2:
astore 3
aload 0
monitorexit
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method public getType()I
aload 0
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getRRsetType()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public rrs()Ljava/util/Iterator;
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
iconst_1
iconst_1
invokespecial org/xbill/DNS/RRset/iterator(ZZ)Ljava/util/Iterator;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public rrs(Z)Ljava/util/Iterator;
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
iconst_1
iload 1
invokespecial org/xbill/DNS/RRset/iterator(ZZ)Ljava/util/Iterator;
astore 2
L1:
aload 0
monitorexit
aload 2
areturn
L2:
astore 2
aload 0
monitorexit
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public sigs()Ljava/util/Iterator;
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
iconst_0
iconst_0
invokespecial org/xbill/DNS/RRset/iterator(ZZ)Ljava/util/Iterator;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public size()I
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 1
aload 0
getfield org/xbill/DNS/RRset/nsigs S
istore 2
L1:
aload 0
monitorexit
iload 1
iload 2
isub
ireturn
L2:
astore 3
aload 0
monitorexit
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/RRset/rrs Ljava/util/List;
invokeinterface java/util/List/size()I 0
ifne L0
ldc "{empty}"
areturn
L0:
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
ldc "{ "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
invokevirtual org/xbill/DNS/RRset/getTTL()J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
invokevirtual org/xbill/DNS/RRset/getDClass()I
invokestatic org/xbill/DNS/DClass/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
invokevirtual org/xbill/DNS/RRset/getType()I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
aload 0
iconst_1
iconst_0
invokespecial org/xbill/DNS/RRset/iterator(ZZ)Ljava/util/Iterator;
invokespecial org/xbill/DNS/RRset/iteratorToString(Ljava/util/Iterator;)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/RRset/nsigs S
ifle L1
aload 1
ldc " sigs: "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
aload 0
iconst_0
iconst_0
invokespecial org/xbill/DNS/RRset/iterator(ZZ)Ljava/util/Iterator;
invokespecial org/xbill/DNS/RRset/iteratorToString(Ljava/util/Iterator;)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L1:
aload 1
ldc " }"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 5
.end method
