.bytecode 50.0
.class public synchronized org/xbill/DNS/TSIGRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -88820909016649306L


.field private 'alg' Lorg/xbill/DNS/Name;

.field private 'error' I

.field private 'fudge' I

.field private 'originalID' I

.field private 'other' [B

.field private 'signature' [B

.field private 'timeSigned' Ljava/util/Date;

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;Ljava/util/Date;I[BII[B)V
aload 0
aload 1
sipush 250
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "alg"
aload 5
invokestatic org/xbill/DNS/TSIGRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TSIGRecord/alg Lorg/xbill/DNS/Name;
aload 0
aload 6
putfield org/xbill/DNS/TSIGRecord/timeSigned Ljava/util/Date;
aload 0
ldc "fudge"
iload 7
invokestatic org/xbill/DNS/TSIGRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/TSIGRecord/fudge I
aload 0
aload 8
putfield org/xbill/DNS/TSIGRecord/signature [B
aload 0
ldc "originalID"
iload 9
invokestatic org/xbill/DNS/TSIGRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/TSIGRecord/originalID I
aload 0
ldc "error"
iload 10
invokestatic org/xbill/DNS/TSIGRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/TSIGRecord/error I
aload 0
aload 11
putfield org/xbill/DNS/TSIGRecord/other [B
return
.limit locals 12
.limit stack 6
.end method

.method public getAlgorithm()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/TSIGRecord/alg Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getError()I
aload 0
getfield org/xbill/DNS/TSIGRecord/error I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFudge()I
aload 0
getfield org/xbill/DNS/TSIGRecord/fudge I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/TSIGRecord
dup
invokespecial org/xbill/DNS/TSIGRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getOriginalID()I
aload 0
getfield org/xbill/DNS/TSIGRecord/originalID I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOther()[B
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSignature()[B
aload 0
getfield org/xbill/DNS/TSIGRecord/signature [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTimeSigned()Ljava/util/Date;
aload 0
getfield org/xbill/DNS/TSIGRecord/timeSigned Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
ldc "no text format defined for TSIG"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/TSIGRecord/alg Lorg/xbill/DNS/Name;
aload 0
new java/util/Date
dup
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
i2l
bipush 32
lshl
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
ladd
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
putfield org/xbill/DNS/TSIGRecord/timeSigned Ljava/util/Date;
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/TSIGRecord/fudge I
aload 0
aload 1
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/TSIGRecord/signature [B
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/TSIGRecord/originalID I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/TSIGRecord/error I
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 2
iload 2
ifle L0
aload 0
aload 1
iload 2
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/TSIGRecord/other [B
return
L0:
aload 0
aconst_null
putfield org/xbill/DNS/TSIGRecord/other [B
return
.limit locals 3
.limit stack 7
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 13
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/alg Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 13
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
aload 13
ldc "(\n\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/timeSigned Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 13
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/fudge I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 13
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/signature [B
arraylength
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L1
aload 13
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/signature [B
bipush 64
ldc "\u0009"
iconst_0
invokestatic org/xbill/DNS/utils/base64/formatString([BILjava/lang/String;Z)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
aload 13
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/error I
invokestatic org/xbill/DNS/Rcode/TSIGstring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
ifnonnull L3
aload 13
iconst_0
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
L4:
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L5
aload 13
ldc " )"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L5:
aload 13
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L1:
aload 13
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/signature [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L2
L3:
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
arraylength
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L6
aload 13
ldc "\n\n\n\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L7:
aload 0
getfield org/xbill/DNS/TSIGRecord/error I
bipush 18
if_icmpne L8
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
arraylength
bipush 6
if_icmpeq L9
aload 13
ldc "<invalid BADTIME other data>"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L4
L6:
aload 13
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L7
L9:
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
iconst_0
baload
sipush 255
iand
i2l
lstore 1
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
iconst_1
baload
sipush 255
iand
i2l
lstore 3
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
iconst_2
baload
sipush 255
iand
bipush 24
ishl
i2l
lstore 5
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
iconst_3
baload
sipush 255
iand
bipush 16
ishl
i2l
lstore 7
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
iconst_4
baload
sipush 255
iand
bipush 8
ishl
i2l
lstore 9
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
iconst_5
baload
sipush 255
iand
i2l
lstore 11
aload 13
ldc "<server time: "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
new java/util/Date
dup
lload 1
bipush 40
lshl
lload 3
bipush 32
lshl
ladd
lload 5
ladd
lload 7
ladd
lload 9
ladd
lload 11
ladd
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 13
ldc ">"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L4
L8:
aload 13
ldc "<"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 13
ldc ">"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L4
.limit locals 14
.limit stack 8
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/TSIGRecord/alg Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/TSIGRecord/timeSigned Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
lstore 4
aload 1
lload 4
bipush 32
lshr
l2i
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
lload 4
ldc2_w 4294967295L
land
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/TSIGRecord/fudge I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/TSIGRecord/signature [B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/TSIGRecord/signature [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 1
aload 0
getfield org/xbill/DNS/TSIGRecord/originalID I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/TSIGRecord/error I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/TSIGRecord/other [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
L0:
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
return
.limit locals 6
.limit stack 5
.end method
