.bytecode 50.0
.class public synchronized org/xbill/DNS/ZoneTransferIn
.super java/lang/Object

.field private static final 'AXFR' I = 6


.field private static final 'END' I = 7


.field private static final 'FIRSTDATA' I = 1


.field private static final 'INITIALSOA' I = 0


.field private static final 'IXFR_ADD' I = 5


.field private static final 'IXFR_ADDSOA' I = 4


.field private static final 'IXFR_DEL' I = 3


.field private static final 'IXFR_DELSOA' I = 2


.field private 'address' Ljava/net/SocketAddress;

.field private 'client' Lorg/xbill/DNS/TCPClient;

.field private 'current_serial' J

.field private 'dclass' I

.field private 'end_serial' J

.field private 'handler' Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;

.field private 'initialsoa' Lorg/xbill/DNS/Record;

.field private 'ixfr_serial' J

.field private 'localAddress' Ljava/net/SocketAddress;

.field private 'qtype' I

.field private 'rtype' I

.field private 'state' I

.field private 'timeout' J

.field private 'tsig' Lorg/xbill/DNS/TSIG;

.field private 'verifier' Lorg/xbill/DNS/TSIG$StreamVerifier;

.field private 'want_fallback' Z

.field private 'zname' Lorg/xbill/DNS/Name;

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w 900000L
putfield org/xbill/DNS/ZoneTransferIn/timeout J
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(Lorg/xbill/DNS/Name;IJZLjava/net/SocketAddress;Lorg/xbill/DNS/TSIG;)V
.catch org/xbill/DNS/NameTooLongException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w 900000L
putfield org/xbill/DNS/ZoneTransferIn/timeout J
aload 0
aload 6
putfield org/xbill/DNS/ZoneTransferIn/address Ljava/net/SocketAddress;
aload 0
aload 7
putfield org/xbill/DNS/ZoneTransferIn/tsig Lorg/xbill/DNS/TSIG;
aload 1
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifeq L0
aload 0
aload 1
putfield org/xbill/DNS/ZoneTransferIn/zname Lorg/xbill/DNS/Name;
L3:
aload 0
iload 2
putfield org/xbill/DNS/ZoneTransferIn/qtype I
aload 0
iconst_1
putfield org/xbill/DNS/ZoneTransferIn/dclass I
aload 0
lload 3
putfield org/xbill/DNS/ZoneTransferIn/ixfr_serial J
aload 0
iload 5
putfield org/xbill/DNS/ZoneTransferIn/want_fallback Z
aload 0
iconst_0
putfield org/xbill/DNS/ZoneTransferIn/state I
return
L0:
aload 0
aload 1
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/concatenate(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/ZoneTransferIn/zname Lorg/xbill/DNS/Name;
L1:
goto L3
L2:
astore 1
new java/lang/IllegalArgumentException
dup
ldc "ZoneTransferIn: name too long"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 8
.limit stack 3
.end method

.method static access$100(Lorg/xbill/DNS/Record;)J
aload 0
invokestatic org/xbill/DNS/ZoneTransferIn/getSOASerial(Lorg/xbill/DNS/Record;)J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private closeConnection()V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/client Lorg/xbill/DNS/TCPClient;
ifnull L1
aload 0
getfield org/xbill/DNS/ZoneTransferIn/client Lorg/xbill/DNS/TCPClient;
invokevirtual org/xbill/DNS/TCPClient/cleanup()V
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 1
.end method

.method private doxfr()V
L0:
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/sendQuery()V
L1:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/state I
bipush 7
if_icmpeq L2
aload 0
getfield org/xbill/DNS/ZoneTransferIn/client Lorg/xbill/DNS/TCPClient;
invokevirtual org/xbill/DNS/TCPClient/recv()[B
astore 3
aload 0
aload 3
invokespecial org/xbill/DNS/ZoneTransferIn/parseMessage([B)Lorg/xbill/DNS/Message;
astore 2
aload 2
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getRcode()I
ifne L3
aload 0
getfield org/xbill/DNS/ZoneTransferIn/verifier Lorg/xbill/DNS/TSIG$StreamVerifier;
ifnull L3
aload 2
invokevirtual org/xbill/DNS/Message/getTSIG()Lorg/xbill/DNS/TSIGRecord;
pop
aload 0
getfield org/xbill/DNS/ZoneTransferIn/verifier Lorg/xbill/DNS/TSIG$StreamVerifier;
aload 2
aload 3
invokevirtual org/xbill/DNS/TSIG$StreamVerifier/verify(Lorg/xbill/DNS/Message;[B)I
ifeq L3
aload 0
ldc "TSIG failure"
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
L3:
aload 2
iconst_1
invokevirtual org/xbill/DNS/Message/getSectionArray(I)[Lorg/xbill/DNS/Record;
astore 3
aload 0
getfield org/xbill/DNS/ZoneTransferIn/state I
ifne L4
aload 2
invokevirtual org/xbill/DNS/Message/getRcode()I
istore 1
iload 1
ifeq L5
aload 0
getfield org/xbill/DNS/ZoneTransferIn/qtype I
sipush 251
if_icmpne L6
iload 1
iconst_4
if_icmpne L6
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/fallback()V
goto L0
L6:
aload 0
iload 1
invokestatic org/xbill/DNS/Rcode/string(I)Ljava/lang/String;
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
L5:
aload 2
invokevirtual org/xbill/DNS/Message/getQuestion()Lorg/xbill/DNS/Record;
astore 4
aload 4
ifnull L7
aload 4
invokevirtual org/xbill/DNS/Record/getType()I
aload 0
getfield org/xbill/DNS/ZoneTransferIn/qtype I
if_icmpeq L7
aload 0
ldc "invalid question section"
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
L7:
aload 3
arraylength
ifne L4
aload 0
getfield org/xbill/DNS/ZoneTransferIn/qtype I
sipush 251
if_icmpne L4
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/fallback()V
goto L0
L4:
iconst_0
istore 1
L8:
iload 1
aload 3
arraylength
if_icmpge L9
aload 0
aload 3
iload 1
aaload
invokespecial org/xbill/DNS/ZoneTransferIn/parseRR(Lorg/xbill/DNS/Record;)V
iload 1
iconst_1
iadd
istore 1
goto L8
L9:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/state I
bipush 7
if_icmpne L1
aload 0
getfield org/xbill/DNS/ZoneTransferIn/verifier Lorg/xbill/DNS/TSIG$StreamVerifier;
ifnull L1
aload 2
invokevirtual org/xbill/DNS/Message/isVerified()Z
ifne L1
aload 0
ldc "last message must be signed"
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
goto L1
L2:
return
.limit locals 5
.limit stack 3
.end method

.method private fail(Ljava/lang/String;)V
new org/xbill/DNS/ZoneTransferException
dup
aload 1
invokespecial org/xbill/DNS/ZoneTransferException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method private fallback()V
aload 0
getfield org/xbill/DNS/ZoneTransferIn/want_fallback Z
ifne L0
aload 0
ldc "server doesn't support IXFR"
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
L0:
aload 0
ldc "falling back to AXFR"
invokespecial org/xbill/DNS/ZoneTransferIn/logxfr(Ljava/lang/String;)V
aload 0
sipush 252
putfield org/xbill/DNS/ZoneTransferIn/qtype I
aload 0
iconst_0
putfield org/xbill/DNS/ZoneTransferIn/state I
return
.limit locals 1
.limit stack 2
.end method

.method private getBasicHandler()Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
instanceof org/xbill/DNS/ZoneTransferIn$BasicHandler
ifeq L0
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
checkcast org/xbill/DNS/ZoneTransferIn$BasicHandler
areturn
L0:
new java/lang/IllegalArgumentException
dup
ldc "ZoneTransferIn used callback interface"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private static getSOASerial(Lorg/xbill/DNS/Record;)J
aload 0
checkcast org/xbill/DNS/SOARecord
invokevirtual org/xbill/DNS/SOARecord/getSerial()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private logxfr(Ljava/lang/String;)V
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/ZoneTransferIn/zname Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc ": "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public static newAXFR(Lorg/xbill/DNS/Name;Ljava/lang/String;ILorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
iload 2
istore 4
iload 2
ifne L0
bipush 53
istore 4
L0:
aload 0
new java/net/InetSocketAddress
dup
aload 1
iload 4
invokespecial java/net/InetSocketAddress/<init>(Ljava/lang/String;I)V
aload 3
invokestatic org/xbill/DNS/ZoneTransferIn/newAXFR(Lorg/xbill/DNS/Name;Ljava/net/SocketAddress;Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
areturn
.limit locals 5
.limit stack 5
.end method

.method public static newAXFR(Lorg/xbill/DNS/Name;Ljava/lang/String;Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
aload 0
aload 1
iconst_0
aload 2
invokestatic org/xbill/DNS/ZoneTransferIn/newAXFR(Lorg/xbill/DNS/Name;Ljava/lang/String;ILorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
areturn
.limit locals 3
.limit stack 4
.end method

.method public static newAXFR(Lorg/xbill/DNS/Name;Ljava/net/SocketAddress;Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
new org/xbill/DNS/ZoneTransferIn
dup
aload 0
sipush 252
lconst_0
iconst_0
aload 1
aload 2
invokespecial org/xbill/DNS/ZoneTransferIn/<init>(Lorg/xbill/DNS/Name;IJZLjava/net/SocketAddress;Lorg/xbill/DNS/TSIG;)V
areturn
.limit locals 3
.limit stack 9
.end method

.method public static newIXFR(Lorg/xbill/DNS/Name;JZLjava/lang/String;ILorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
iload 5
istore 7
iload 5
ifne L0
bipush 53
istore 7
L0:
aload 0
lload 1
iload 3
new java/net/InetSocketAddress
dup
aload 4
iload 7
invokespecial java/net/InetSocketAddress/<init>(Ljava/lang/String;I)V
aload 6
invokestatic org/xbill/DNS/ZoneTransferIn/newIXFR(Lorg/xbill/DNS/Name;JZLjava/net/SocketAddress;Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
areturn
.limit locals 8
.limit stack 8
.end method

.method public static newIXFR(Lorg/xbill/DNS/Name;JZLjava/lang/String;Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
aload 0
lload 1
iload 3
aload 4
iconst_0
aload 5
invokestatic org/xbill/DNS/ZoneTransferIn/newIXFR(Lorg/xbill/DNS/Name;JZLjava/lang/String;ILorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
areturn
.limit locals 6
.limit stack 7
.end method

.method public static newIXFR(Lorg/xbill/DNS/Name;JZLjava/net/SocketAddress;Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
new org/xbill/DNS/ZoneTransferIn
dup
aload 0
sipush 251
lload 1
iload 3
aload 4
aload 5
invokespecial org/xbill/DNS/ZoneTransferIn/<init>(Lorg/xbill/DNS/Name;IJZLjava/net/SocketAddress;Lorg/xbill/DNS/TSIG;)V
areturn
.limit locals 6
.limit stack 9
.end method

.method private openConnection()V
aload 0
new org/xbill/DNS/TCPClient
dup
invokestatic java/lang/System/currentTimeMillis()J
aload 0
getfield org/xbill/DNS/ZoneTransferIn/timeout J
ladd
invokespecial org/xbill/DNS/TCPClient/<init>(J)V
putfield org/xbill/DNS/ZoneTransferIn/client Lorg/xbill/DNS/TCPClient;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/localAddress Ljava/net/SocketAddress;
ifnull L0
aload 0
getfield org/xbill/DNS/ZoneTransferIn/client Lorg/xbill/DNS/TCPClient;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/localAddress Ljava/net/SocketAddress;
invokevirtual org/xbill/DNS/TCPClient/bind(Ljava/net/SocketAddress;)V
L0:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/client Lorg/xbill/DNS/TCPClient;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/address Ljava/net/SocketAddress;
invokevirtual org/xbill/DNS/TCPClient/connect(Ljava/net/SocketAddress;)V
return
.limit locals 1
.limit stack 7
.end method

.method private parseMessage([B)Lorg/xbill/DNS/Message;
.catch java/io/IOException from L0 to L1 using L2
L0:
new org/xbill/DNS/Message
dup
aload 1
invokespecial org/xbill/DNS/Message/<init>([B)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 1
instanceof org/xbill/DNS/WireParseException
ifeq L3
aload 1
checkcast org/xbill/DNS/WireParseException
athrow
L3:
new org/xbill/DNS/WireParseException
dup
ldc "Error parsing message"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method private parseRR(Lorg/xbill/DNS/Record;)V
L0:
aload 1
invokevirtual org/xbill/DNS/Record/getType()I
istore 2
aload 0
getfield org/xbill/DNS/ZoneTransferIn/state I
tableswitch 0
L1
L2
L3
L4
L5
L6
L7
L8
default : L9
L9:
aload 0
ldc "invalid state"
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
L10:
return
L1:
iload 2
bipush 6
if_icmpeq L11
aload 0
ldc "missing initial SOA"
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
L11:
aload 0
aload 1
putfield org/xbill/DNS/ZoneTransferIn/initialsoa Lorg/xbill/DNS/Record;
aload 0
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn/getSOASerial(Lorg/xbill/DNS/Record;)J
putfield org/xbill/DNS/ZoneTransferIn/end_serial J
aload 0
getfield org/xbill/DNS/ZoneTransferIn/qtype I
sipush 251
if_icmpne L12
aload 0
getfield org/xbill/DNS/ZoneTransferIn/end_serial J
aload 0
getfield org/xbill/DNS/ZoneTransferIn/ixfr_serial J
invokestatic org/xbill/DNS/Serial/compare(JJ)I
ifgt L12
aload 0
ldc "up to date"
invokespecial org/xbill/DNS/ZoneTransferIn/logxfr(Ljava/lang/String;)V
aload 0
bipush 7
putfield org/xbill/DNS/ZoneTransferIn/state I
return
L12:
aload 0
iconst_1
putfield org/xbill/DNS/ZoneTransferIn/state I
return
L2:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/qtype I
sipush 251
if_icmpne L13
iload 2
bipush 6
if_icmpne L13
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn/getSOASerial(Lorg/xbill/DNS/Record;)J
aload 0
getfield org/xbill/DNS/ZoneTransferIn/ixfr_serial J
lcmp
ifne L13
aload 0
sipush 251
putfield org/xbill/DNS/ZoneTransferIn/rtype I
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
invokeinterface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler/startIXFR()V 0
aload 0
ldc "got incremental response"
invokespecial org/xbill/DNS/ZoneTransferIn/logxfr(Ljava/lang/String;)V
aload 0
iconst_2
putfield org/xbill/DNS/ZoneTransferIn/state I
goto L0
L13:
aload 0
sipush 252
putfield org/xbill/DNS/ZoneTransferIn/rtype I
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
invokeinterface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler/startAXFR()V 0
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/initialsoa Lorg/xbill/DNS/Record;
invokeinterface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler/handleRecord(Lorg/xbill/DNS/Record;)V 1
aload 0
ldc "got nonincremental response"
invokespecial org/xbill/DNS/ZoneTransferIn/logxfr(Ljava/lang/String;)V
aload 0
bipush 6
putfield org/xbill/DNS/ZoneTransferIn/state I
goto L0
L3:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
aload 1
invokeinterface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler/startIXFRDeletes(Lorg/xbill/DNS/Record;)V 1
aload 0
iconst_3
putfield org/xbill/DNS/ZoneTransferIn/state I
return
L4:
iload 2
bipush 6
if_icmpne L14
aload 0
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn/getSOASerial(Lorg/xbill/DNS/Record;)J
putfield org/xbill/DNS/ZoneTransferIn/current_serial J
aload 0
iconst_4
putfield org/xbill/DNS/ZoneTransferIn/state I
goto L0
L14:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
aload 1
invokeinterface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler/handleRecord(Lorg/xbill/DNS/Record;)V 1
return
L5:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
aload 1
invokeinterface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler/startIXFRAdds(Lorg/xbill/DNS/Record;)V 1
aload 0
iconst_5
putfield org/xbill/DNS/ZoneTransferIn/state I
return
L6:
iload 2
bipush 6
if_icmpne L15
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn/getSOASerial(Lorg/xbill/DNS/Record;)J
lstore 3
lload 3
aload 0
getfield org/xbill/DNS/ZoneTransferIn/end_serial J
lcmp
ifne L16
aload 0
bipush 7
putfield org/xbill/DNS/ZoneTransferIn/state I
return
L16:
lload 3
aload 0
getfield org/xbill/DNS/ZoneTransferIn/current_serial J
lcmp
ifeq L17
aload 0
new java/lang/StringBuffer
dup
ldc "IXFR out of sync: expected serial "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/ZoneTransferIn/current_serial J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " , got "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
lload 3
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
L15:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
aload 1
invokeinterface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler/handleRecord(Lorg/xbill/DNS/Record;)V 1
return
L17:
aload 0
iconst_2
putfield org/xbill/DNS/ZoneTransferIn/state I
goto L0
L7:
iload 2
iconst_1
if_icmpne L18
aload 1
invokevirtual org/xbill/DNS/Record/getDClass()I
aload 0
getfield org/xbill/DNS/ZoneTransferIn/dclass I
if_icmpne L10
L18:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
aload 1
invokeinterface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler/handleRecord(Lorg/xbill/DNS/Record;)V 1
iload 2
bipush 6
if_icmpne L10
aload 0
bipush 7
putfield org/xbill/DNS/ZoneTransferIn/state I
return
L8:
aload 0
ldc "extra data"
invokespecial org/xbill/DNS/ZoneTransferIn/fail(Ljava/lang/String;)V
return
.limit locals 5
.limit stack 4
.end method

.method private sendQuery()V
aload 0
getfield org/xbill/DNS/ZoneTransferIn/zname Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/qtype I
aload 0
getfield org/xbill/DNS/ZoneTransferIn/dclass I
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;
astore 1
new org/xbill/DNS/Message
dup
invokespecial org/xbill/DNS/Message/<init>()V
astore 2
aload 2
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_0
invokevirtual org/xbill/DNS/Header/setOpcode(I)V
aload 2
aload 1
iconst_0
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
aload 0
getfield org/xbill/DNS/ZoneTransferIn/qtype I
sipush 251
if_icmpne L0
aload 2
new org/xbill/DNS/SOARecord
dup
aload 0
getfield org/xbill/DNS/ZoneTransferIn/zname Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/dclass I
lconst_0
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/ixfr_serial J
lconst_0
lconst_0
lconst_0
lconst_0
invokespecial org/xbill/DNS/SOARecord/<init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;JJJJJ)V
iconst_2
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
L0:
aload 0
getfield org/xbill/DNS/ZoneTransferIn/tsig Lorg/xbill/DNS/TSIG;
ifnull L1
aload 0
getfield org/xbill/DNS/ZoneTransferIn/tsig Lorg/xbill/DNS/TSIG;
aload 2
aconst_null
invokevirtual org/xbill/DNS/TSIG/apply(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/TSIGRecord;)V
aload 0
new org/xbill/DNS/TSIG$StreamVerifier
dup
aload 0
getfield org/xbill/DNS/ZoneTransferIn/tsig Lorg/xbill/DNS/TSIG;
aload 2
invokevirtual org/xbill/DNS/Message/getTSIG()Lorg/xbill/DNS/TSIGRecord;
invokespecial org/xbill/DNS/TSIG$StreamVerifier/<init>(Lorg/xbill/DNS/TSIG;Lorg/xbill/DNS/TSIGRecord;)V
putfield org/xbill/DNS/ZoneTransferIn/verifier Lorg/xbill/DNS/TSIG$StreamVerifier;
L1:
aload 2
ldc_w 65535
invokevirtual org/xbill/DNS/Message/toWire(I)[B
astore 1
aload 0
getfield org/xbill/DNS/ZoneTransferIn/client Lorg/xbill/DNS/TCPClient;
aload 1
invokevirtual org/xbill/DNS/TCPClient/send([B)V
return
.limit locals 3
.limit stack 19
.end method

.method public getAXFR()Ljava/util/List;
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/getBasicHandler()Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;
invokestatic org/xbill/DNS/ZoneTransferIn$BasicHandler/access$300(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIXFR()Ljava/util/List;
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/getBasicHandler()Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;
invokestatic org/xbill/DNS/ZoneTransferIn$BasicHandler/access$400(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getName()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/ZoneTransferIn/zname Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getType()I
aload 0
getfield org/xbill/DNS/ZoneTransferIn/qtype I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isAXFR()Z
aload 0
getfield org/xbill/DNS/ZoneTransferIn/rtype I
sipush 252
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isCurrent()Z
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/getBasicHandler()Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;
astore 1
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn$BasicHandler/access$300(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
ifnonnull L0
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn$BasicHandler/access$400(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
ifnonnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public isIXFR()Z
aload 0
getfield org/xbill/DNS/ZoneTransferIn/rtype I
sipush 251
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public run()Ljava/util/List;
new org/xbill/DNS/ZoneTransferIn$BasicHandler
dup
aconst_null
invokespecial org/xbill/DNS/ZoneTransferIn$BasicHandler/<init>(Lorg/xbill/DNS/ZoneTransferIn$1;)V
astore 1
aload 0
aload 1
invokevirtual org/xbill/DNS/ZoneTransferIn/run(Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;)V
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn$BasicHandler/access$300(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
ifnull L0
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn$BasicHandler/access$300(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
areturn
L0:
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn$BasicHandler/access$400(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
areturn
.limit locals 2
.limit stack 3
.end method

.method public run(Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;)V
.catch all from L0 to L1 using L2
aload 0
aload 1
putfield org/xbill/DNS/ZoneTransferIn/handler Lorg/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
L0:
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/openConnection()V
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/doxfr()V
L1:
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/closeConnection()V
return
L2:
astore 1
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn/closeConnection()V
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public setDClass(I)V
iload 1
invokestatic org/xbill/DNS/DClass/check(I)V
aload 0
iload 1
putfield org/xbill/DNS/ZoneTransferIn/dclass I
return
.limit locals 2
.limit stack 2
.end method

.method public setLocalAddress(Ljava/net/SocketAddress;)V
aload 0
aload 1
putfield org/xbill/DNS/ZoneTransferIn/localAddress Ljava/net/SocketAddress;
return
.limit locals 2
.limit stack 2
.end method

.method public setTimeout(I)V
iload 1
ifge L0
new java/lang/IllegalArgumentException
dup
ldc "timeout cannot be negative"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
ldc2_w 1000L
iload 1
i2l
lmul
putfield org/xbill/DNS/ZoneTransferIn/timeout J
return
.limit locals 2
.limit stack 5
.end method
