.bytecode 50.0
.class public synchronized org/xbill/DNS/InvalidTTLException
.super java/lang/IllegalArgumentException

.method public <init>(J)V
aload 0
new java/lang/StringBuffer
dup
ldc "Invalid DNS TTL: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method
