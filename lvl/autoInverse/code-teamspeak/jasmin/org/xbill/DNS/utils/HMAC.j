.bytecode 50.0
.class public synchronized org/xbill/DNS/utils/HMAC
.super java/lang/Object

.field private static final 'IPAD' B = 54


.field private static final 'OPAD' B = 92


.field private 'blockLength' I

.field private 'digest' Ljava/security/MessageDigest;

.field private 'ipad' [B

.field private 'opad' [B

.method public <init>(Ljava/lang/String;I[B)V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
L0:
aload 0
aload 1
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
putfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
L1:
aload 0
iload 2
putfield org/xbill/DNS/utils/HMAC/blockLength I
aload 0
aload 3
invokespecial org/xbill/DNS/utils/HMAC/init([B)V
return
L2:
astore 3
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "unknown digest algorithm "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method public <init>(Ljava/lang/String;[B)V
aload 0
aload 1
bipush 64
aload 2
invokespecial org/xbill/DNS/utils/HMAC/<init>(Ljava/lang/String;I[B)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Ljava/security/MessageDigest;I[B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
invokevirtual java/security/MessageDigest/reset()V
aload 0
aload 1
putfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
aload 0
iload 2
putfield org/xbill/DNS/utils/HMAC/blockLength I
aload 0
aload 3
invokespecial org/xbill/DNS/utils/HMAC/init([B)V
return
.limit locals 4
.limit stack 2
.end method

.method public <init>(Ljava/security/MessageDigest;[B)V
aload 0
aload 1
bipush 64
aload 2
invokespecial org/xbill/DNS/utils/HMAC/<init>(Ljava/security/MessageDigest;I[B)V
return
.limit locals 3
.limit stack 4
.end method

.method private init([B)V
aload 1
astore 4
aload 1
arraylength
aload 0
getfield org/xbill/DNS/utils/HMAC/blockLength I
if_icmple L0
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
aload 1
invokevirtual java/security/MessageDigest/digest([B)[B
astore 4
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/reset()V
L0:
aload 0
aload 0
getfield org/xbill/DNS/utils/HMAC/blockLength I
newarray byte
putfield org/xbill/DNS/utils/HMAC/ipad [B
aload 0
aload 0
getfield org/xbill/DNS/utils/HMAC/blockLength I
newarray byte
putfield org/xbill/DNS/utils/HMAC/opad [B
iconst_0
istore 2
L1:
iload 2
istore 3
iload 2
aload 4
arraylength
if_icmpge L2
aload 0
getfield org/xbill/DNS/utils/HMAC/ipad [B
iload 2
aload 4
iload 2
baload
bipush 54
ixor
i2b
bastore
aload 0
getfield org/xbill/DNS/utils/HMAC/opad [B
iload 2
aload 4
iload 2
baload
bipush 92
ixor
i2b
bastore
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iload 3
aload 0
getfield org/xbill/DNS/utils/HMAC/blockLength I
if_icmpge L3
aload 0
getfield org/xbill/DNS/utils/HMAC/ipad [B
iload 3
bipush 54
bastore
aload 0
getfield org/xbill/DNS/utils/HMAC/opad [B
iload 3
bipush 92
bastore
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
aload 0
getfield org/xbill/DNS/utils/HMAC/ipad [B
invokevirtual java/security/MessageDigest/update([B)V
return
.limit locals 5
.limit stack 4
.end method

.method public clear()V
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/reset()V
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
aload 0
getfield org/xbill/DNS/utils/HMAC/ipad [B
invokevirtual java/security/MessageDigest/update([B)V
return
.limit locals 1
.limit stack 2
.end method

.method public digestLength()I
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/getDigestLength()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public sign()[B
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/digest()[B
astore 1
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/reset()V
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
aload 0
getfield org/xbill/DNS/utils/HMAC/opad [B
invokevirtual java/security/MessageDigest/update([B)V
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
aload 1
invokevirtual java/security/MessageDigest/digest([B)[B
areturn
.limit locals 2
.limit stack 2
.end method

.method public update([B)V
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
aload 1
invokevirtual java/security/MessageDigest/update([B)V
return
.limit locals 2
.limit stack 2
.end method

.method public update([BII)V
aload 0
getfield org/xbill/DNS/utils/HMAC/digest Ljava/security/MessageDigest;
aload 1
iload 2
iload 3
invokevirtual java/security/MessageDigest/update([BII)V
return
.limit locals 4
.limit stack 4
.end method

.method public verify([B)Z
aload 0
aload 1
iconst_0
invokevirtual org/xbill/DNS/utils/HMAC/verify([BZ)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method public verify([BZ)Z
aload 0
invokevirtual org/xbill/DNS/utils/HMAC/sign()[B
astore 3
iload 2
ifeq L0
aload 1
arraylength
aload 3
arraylength
if_icmpge L0
aload 1
arraylength
newarray byte
astore 4
aload 3
iconst_0
aload 4
iconst_0
aload 4
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 4
astore 3
L1:
aload 1
aload 3
invokestatic java/util/Arrays/equals([B[B)Z
ireturn
L0:
goto L1
.limit locals 5
.limit stack 5
.end method
