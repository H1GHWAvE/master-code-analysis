.bytecode 50.0
.class public final synchronized org/xbill/DNS/TTL
.super java/lang/Object

.field public static final 'MAX_VALUE' J = 2147483647L


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static check(J)V
lload 0
lconst_0
lcmp
iflt L0
lload 0
ldc2_w 2147483647L
lcmp
ifle L1
L0:
new org/xbill/DNS/InvalidTTLException
dup
lload 0
invokespecial org/xbill/DNS/InvalidTTLException/<init>(J)V
athrow
L1:
return
.limit locals 2
.limit stack 4
.end method

.method public static format(J)Ljava/lang/String;
lload 0
invokestatic org/xbill/DNS/TTL/check(J)V
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 10
lload 0
ldc2_w 60L
lrem
lstore 2
lload 0
ldc2_w 60L
ldiv
lstore 4
lload 4
ldc2_w 60L
lrem
lstore 0
lload 4
ldc2_w 60L
ldiv
lstore 6
lload 6
ldc2_w 24L
lrem
lstore 4
lload 6
ldc2_w 24L
ldiv
lstore 8
lload 8
ldc2_w 7L
lrem
lstore 6
lload 8
ldc2_w 7L
ldiv
lstore 8
lload 8
lconst_0
lcmp
ifle L0
aload 10
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 8
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc "W"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
lload 6
lconst_0
lcmp
ifle L1
aload 10
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 6
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc "D"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L1:
lload 4
lconst_0
lcmp
ifle L2
aload 10
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 4
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc "H"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
lload 0
lconst_0
lcmp
ifle L3
aload 10
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 0
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc "M"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L3:
lload 2
lconst_0
lcmp
ifgt L4
lload 8
lconst_0
lcmp
ifne L5
lload 6
lconst_0
lcmp
ifne L5
lload 4
lconst_0
lcmp
ifne L5
lload 0
lconst_0
lcmp
ifne L5
L4:
aload 10
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 2
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc "S"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L5:
aload 10
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 11
.limit stack 4
.end method

.method public static parse(Ljava/lang/String;Z)J
aload 0
ifnull L0
aload 0
invokevirtual java/lang/String/length()I
ifeq L0
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/isDigit(C)Z
ifne L1
L0:
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
L1:
lconst_0
lstore 4
lconst_0
lstore 6
iconst_0
istore 3
L2:
iload 3
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L3
aload 0
iload 3
invokevirtual java/lang/String/charAt(I)C
istore 2
iload 2
invokestatic java/lang/Character/isDigit(C)Z
ifeq L4
ldc2_w 10L
lload 4
lmul
iload 2
invokestatic java/lang/Character/getNumericValue(C)I
i2l
ladd
lstore 8
lload 8
lload 4
lcmp
ifge L5
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
L4:
lload 4
lstore 8
lload 4
lstore 10
lload 4
lstore 12
lload 4
lstore 14
iload 2
invokestatic java/lang/Character/toUpperCase(C)C
lookupswitch
68 : L6
72 : L7
77 : L8
83 : L9
87 : L10
default : L11
L11:
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
L10:
lload 4
ldc2_w 7L
lmul
lstore 8
L6:
lload 8
ldc2_w 24L
lmul
lstore 10
L7:
lload 10
ldc2_w 60L
lmul
lstore 12
L8:
lload 12
ldc2_w 60L
lmul
lstore 14
L9:
lload 6
lload 14
ladd
lstore 8
lconst_0
lstore 4
lload 8
lstore 6
lload 8
ldc2_w 4294967295L
lcmp
ifle L12
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
L5:
lload 8
lstore 4
L12:
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
lload 6
lconst_0
lcmp
ifne L13
L14:
lload 4
ldc2_w 4294967295L
lcmp
ifle L15
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
L15:
lload 4
lstore 6
lload 4
ldc2_w 2147483647L
lcmp
ifle L16
lload 4
lstore 6
iload 1
ifeq L16
ldc2_w 2147483647L
lstore 6
L16:
lload 6
lreturn
L13:
lload 6
lstore 4
goto L14
.limit locals 16
.limit stack 4
.end method

.method public static parseTTL(Ljava/lang/String;)J
aload 0
iconst_1
invokestatic org/xbill/DNS/TTL/parse(Ljava/lang/String;Z)J
lreturn
.limit locals 1
.limit stack 2
.end method
