.bytecode 50.0
.class synchronized org/xbill/DNS/Cache$CacheMap
.super java/util/LinkedHashMap

.field private 'maxsize' I

.method <init>(I)V
aload 0
bipush 16
ldc_w 0.75F
iconst_1
invokespecial java/util/LinkedHashMap/<init>(IFZ)V
aload 0
iconst_m1
putfield org/xbill/DNS/Cache$CacheMap/maxsize I
aload 0
iload 1
putfield org/xbill/DNS/Cache$CacheMap/maxsize I
return
.limit locals 2
.limit stack 4
.end method

.method getMaxSize()I
aload 0
getfield org/xbill/DNS/Cache$CacheMap/maxsize I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
aload 0
getfield org/xbill/DNS/Cache$CacheMap/maxsize I
iflt L0
aload 0
invokevirtual org/xbill/DNS/Cache$CacheMap/size()I
aload 0
getfield org/xbill/DNS/Cache$CacheMap/maxsize I
if_icmple L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method setMaxSize(I)V
aload 0
iload 1
putfield org/xbill/DNS/Cache$CacheMap/maxsize I
return
.limit locals 2
.limit stack 2
.end method
