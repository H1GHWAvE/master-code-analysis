.bytecode 50.0
.class public synchronized org/xbill/DNS/DLVRecord
.super org/xbill/DNS/Record

.field public static final 'SHA1_DIGEST_ID' I = 1


.field public static final 'SHA256_DIGEST_ID' I = 1


.field private static final 'serialVersionUID' J = 1960742375677534148L


.field private 'alg' I

.field private 'digest' [B

.field private 'digestid' I

.field private 'footprint' I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIII[B)V
aload 0
aload 1
ldc_w 32769
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "footprint"
iload 5
invokestatic org/xbill/DNS/DLVRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/DLVRecord/footprint I
aload 0
ldc "alg"
iload 6
invokestatic org/xbill/DNS/DLVRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/DLVRecord/alg I
aload 0
ldc "digestid"
iload 7
invokestatic org/xbill/DNS/DLVRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/DLVRecord/digestid I
aload 0
aload 8
putfield org/xbill/DNS/DLVRecord/digest [B
return
.limit locals 9
.limit stack 6
.end method

.method public getAlgorithm()I
aload 0
getfield org/xbill/DNS/DLVRecord/alg I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDigest()[B
aload 0
getfield org/xbill/DNS/DLVRecord/digest [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDigestID()I
aload 0
getfield org/xbill/DNS/DLVRecord/digestid I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFootprint()I
aload 0
getfield org/xbill/DNS/DLVRecord/footprint I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/DLVRecord
dup
invokespecial org/xbill/DNS/DLVRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/DLVRecord/footprint I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/DLVRecord/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/DLVRecord/digestid I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getHex()[B
putfield org/xbill/DNS/DLVRecord/digest [B
return
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/DLVRecord/footprint I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/DLVRecord/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/DLVRecord/digestid I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/DLVRecord/digest [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/DLVRecord/footprint I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/DLVRecord/alg I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/DLVRecord/digestid I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/DLVRecord/digest [B
ifnull L0
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/DLVRecord/digest [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/DLVRecord/footprint I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/DLVRecord/alg I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/DLVRecord/digestid I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
getfield org/xbill/DNS/DLVRecord/digest [B
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/DLVRecord/digest [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L0:
return
.limit locals 4
.limit stack 2
.end method
