.bytecode 50.0
.class synchronized org/xbill/DNS/Type$TypeMnemonic
.super org/xbill/DNS/Mnemonic

.field private 'objects' Ljava/util/HashMap;

.method public <init>()V
aload 0
ldc "Type"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
aload 0
ldc "TYPE"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/setPrefix(Ljava/lang/String;)V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield org/xbill/DNS/Type$TypeMnemonic/objects Ljava/util/HashMap;
return
.limit locals 1
.limit stack 3
.end method

.method public add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
aload 0
iload 1
aload 2
invokespecial org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
aload 0
getfield org/xbill/DNS/Type$TypeMnemonic/objects Ljava/util/HashMap;
iload 1
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
aload 3
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 4
.limit stack 3
.end method

.method public check(I)V
iload 1
invokestatic org/xbill/DNS/Type/check(I)V
return
.limit locals 2
.limit stack 1
.end method

.method public getProto(I)Lorg/xbill/DNS/Record;
aload 0
iload 1
invokevirtual org/xbill/DNS/Type$TypeMnemonic/check(I)V
aload 0
getfield org/xbill/DNS/Type$TypeMnemonic/objects Ljava/util/HashMap;
iload 1
invokestatic org/xbill/DNS/Type$TypeMnemonic/toInteger(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast org/xbill/DNS/Record
areturn
.limit locals 2
.limit stack 2
.end method
