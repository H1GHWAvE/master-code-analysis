.bytecode 50.0
.class public synchronized org/xbill/DNS/ResolverConfig
.super java/lang/Object

.field static 'class$java$lang$String' Ljava/lang/Class;

.field static 'class$org$xbill$DNS$ResolverConfig' Ljava/lang/Class;

.field private static 'currentConfig' Lorg/xbill/DNS/ResolverConfig;

.field private 'ndots' I

.field private 'searchlist' [Lorg/xbill/DNS/Name;

.field private 'servers' [Ljava/lang/String;

.method static <clinit>()V
invokestatic org/xbill/DNS/ResolverConfig/refresh()V
return
.limit locals 0
.limit stack 0
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
aload 0
aconst_null
putfield org/xbill/DNS/ResolverConfig/searchlist [Lorg/xbill/DNS/Name;
aload 0
iconst_m1
putfield org/xbill/DNS/ResolverConfig/ndots I
aload 0
invokespecial org/xbill/DNS/ResolverConfig/findProperty()Z
ifeq L0
L1:
return
L0:
aload 0
invokespecial org/xbill/DNS/ResolverConfig/findSunJVM()Z
ifne L1
aload 0
getfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
ifnull L2
aload 0
getfield org/xbill/DNS/ResolverConfig/searchlist [Lorg/xbill/DNS/Name;
ifnonnull L1
L2:
ldc "os.name"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 1
ldc "java.vendor"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 1
ldc "Windows"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L3
aload 1
ldc "95"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpne L4
aload 1
ldc "98"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpne L4
aload 1
ldc "ME"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L5
L4:
aload 0
invokespecial org/xbill/DNS/ResolverConfig/find95()V
return
L5:
aload 0
invokespecial org/xbill/DNS/ResolverConfig/findNT()V
return
L3:
aload 1
ldc "NetWare"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L6
aload 0
invokespecial org/xbill/DNS/ResolverConfig/findNetware()V
return
L6:
aload 2
ldc "Android"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L7
aload 0
invokespecial org/xbill/DNS/ResolverConfig/findAndroid()V
return
L7:
aload 0
invokespecial org/xbill/DNS/ResolverConfig/findUnix()V
return
.limit locals 3
.limit stack 2
.end method

.method private addSearch(Ljava/lang/String;Ljava/util/List;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "adding search "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
aload 1
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 1
L1:
aload 2
aload 1
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifeq L3
return
L3:
aload 2
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
L2:
astore 1
return
.limit locals 3
.limit stack 4
.end method

.method private addServer(Ljava/lang/String;Ljava/util/List;)V
aload 2
aload 1
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifeq L0
return
L0:
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "adding server "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L1:
aload 2
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 3
.limit stack 4
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
L0:
aload 0
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/NoClassDefFoundError
dup
invokespecial java/lang/NoClassDefFoundError/<init>()V
aload 0
invokevirtual java/lang/NoClassDefFoundError/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
athrow
.limit locals 1
.limit stack 2
.end method

.method private configureFromLists(Ljava/util/List;Ljava/util/List;)V
aload 0
getfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
ifnonnull L0
aload 1
invokeinterface java/util/List/size()I 0
ifle L0
aload 0
aload 1
iconst_0
anewarray java/lang/String
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Ljava/lang/String;
checkcast [Ljava/lang/String;
putfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
L0:
aload 0
getfield org/xbill/DNS/ResolverConfig/searchlist [Lorg/xbill/DNS/Name;
ifnonnull L1
aload 2
invokeinterface java/util/List/size()I 0
ifle L1
aload 0
aload 2
iconst_0
anewarray org/xbill/DNS/Name
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/Name;
checkcast [Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/ResolverConfig/searchlist [Lorg/xbill/DNS/Name;
L1:
return
.limit locals 3
.limit stack 3
.end method

.method private configureNdots(I)V
aload 0
getfield org/xbill/DNS/ResolverConfig/ndots I
ifge L0
iload 1
ifle L0
aload 0
iload 1
putfield org/xbill/DNS/ResolverConfig/ndots I
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private find95()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuffer
dup
ldc "winipcfg /all /batch "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
ldc "winipcfg.out"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
invokevirtual java/lang/Process/waitFor()I
pop
aload 0
new java/io/FileInputStream
dup
new java/io/File
dup
ldc "winipcfg.out"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
invokespecial org/xbill/DNS/ResolverConfig/findWin(Ljava/io/InputStream;)V
new java/io/File
dup
ldc "winipcfg.out"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 6
.end method

.method private findAndroid()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
L0:
ldc "android.os.SystemProperties"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 5
getstatic org/xbill/DNS/ResolverConfig/class$java$lang$String Ljava/lang/Class;
ifnonnull L9
ldc "java.lang.String"
invokestatic org/xbill/DNS/ResolverConfig/class$(Ljava/lang/String;)Ljava/lang/Class;
astore 2
aload 2
putstatic org/xbill/DNS/ResolverConfig/class$java$lang$String Ljava/lang/Class;
L1:
aload 5
ldc "get"
iconst_1
anewarray java/lang/Class
dup
iconst_0
aload 2
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 2
L3:
iconst_0
istore 1
L11:
iload 1
iconst_4
if_icmpge L12
L4:
aload 2
aconst_null
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "net.dns1"
aastore
dup
iconst_1
ldc "net.dns2"
aastore
dup
iconst_2
ldc "net.dns3"
aastore
dup
iconst_3
ldc "net.dns4"
aastore
iload 1
aaload
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 5
L5:
aload 5
ifnull L13
L6:
aload 5
ldc "^\\d+(\\.\\d+){3}$"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifne L7
aload 5
ldc "^[0-9a-f]+(:[0-9a-f]*)+:[0-9a-f]+$"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L13
L7:
aload 3
aload 5
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L13
aload 3
aload 5
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L8:
goto L13
L9:
getstatic org/xbill/DNS/ResolverConfig/class$java$lang$String Ljava/lang/Class;
astore 2
L10:
goto L1
L2:
astore 2
L12:
aload 0
aload 3
aload 4
invokespecial org/xbill/DNS/ResolverConfig/configureFromLists(Ljava/util/List;Ljava/util/List;)V
return
L13:
iload 1
iconst_1
iadd
istore 1
goto L11
.limit locals 6
.limit stack 9
.end method

.method private findNT()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
ldc "ipconfig /all"
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 0
aload 1
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial org/xbill/DNS/ResolverConfig/findWin(Ljava/io/InputStream;)V
aload 1
invokevirtual java/lang/Process/destroy()V
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 2
.end method

.method private findNetware()V
aload 0
ldc "sys:/etc/resolv.cfg"
invokespecial org/xbill/DNS/ResolverConfig/findResolvConf(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method private findProperty()Z
iconst_0
istore 2
new java/util/ArrayList
dup
iconst_0
invokespecial java/util/ArrayList/<init>(I)V
astore 3
new java/util/ArrayList
dup
iconst_0
invokespecial java/util/ArrayList/<init>(I)V
astore 4
ldc "dns.server"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 5
ifnull L0
new java/util/StringTokenizer
dup
aload 5
ldc ","
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 5
L1:
aload 5
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L0
aload 0
aload 5
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
aload 3
invokespecial org/xbill/DNS/ResolverConfig/addServer(Ljava/lang/String;Ljava/util/List;)V
goto L1
L0:
ldc "dns.search"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 5
ifnull L2
new java/util/StringTokenizer
dup
aload 5
ldc ","
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 5
L3:
aload 5
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L2
aload 0
aload 5
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
aload 4
invokespecial org/xbill/DNS/ResolverConfig/addSearch(Ljava/lang/String;Ljava/util/List;)V
goto L3
L2:
aload 0
aload 3
aload 4
invokespecial org/xbill/DNS/ResolverConfig/configureFromLists(Ljava/util/List;Ljava/util/List;)V
iload 2
istore 1
aload 0
getfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
ifnull L4
iload 2
istore 1
aload 0
getfield org/xbill/DNS/ResolverConfig/searchlist [Lorg/xbill/DNS/Name;
ifnull L4
iconst_1
istore 1
L4:
iload 1
ireturn
.limit locals 6
.limit stack 4
.end method

.method private findResolvConf(Ljava/lang/String;)V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L5
.catch java/io/IOException from L6 to L7 using L5
.catch java/io/IOException from L8 to L9 using L5
.catch java/io/IOException from L10 to L11 using L5
.catch java/io/IOException from L12 to L13 using L5
.catch java/io/IOException from L14 to L15 using L5
.catch java/io/IOException from L16 to L17 using L5
.catch java/io/IOException from L18 to L19 using L5
.catch java/io/IOException from L20 to L21 using L5
.catch java/io/IOException from L22 to L23 using L5
.catch java/io/IOException from L24 to L25 using L5
.catch java/io/IOException from L26 to L27 using L5
.catch java/io/IOException from L28 to L29 using L5
.catch java/io/IOException from L30 to L31 using L5
.catch java/io/IOException from L32 to L33 using L5
.catch java/io/IOException from L34 to L35 using L5
.catch java/io/IOException from L36 to L37 using L5
.catch java/io/IOException from L38 to L39 using L5
.catch java/io/IOException from L40 to L41 using L5
.catch java/io/IOException from L42 to L43 using L5
.catch java/io/IOException from L44 to L45 using L5
.catch java/io/IOException from L46 to L47 using L5
.catch java/io/IOException from L48 to L49 using L5
.catch java/io/IOException from L50 to L51 using L5
.catch java/io/IOException from L52 to L53 using L5
.catch java/io/IOException from L54 to L55 using L5
L0:
new java/io/FileInputStream
dup
aload 1
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 1
L1:
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 1
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 6
new java/util/ArrayList
dup
iconst_0
invokespecial java/util/ArrayList/<init>(I)V
astore 1
new java/util/ArrayList
dup
iconst_0
invokespecial java/util/ArrayList/<init>(I)V
astore 5
iconst_m1
istore 2
L56:
iload 2
istore 3
L3:
aload 6
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 7
L4:
aload 7
ifnull L57
iload 2
istore 3
L6:
aload 7
ldc "nameserver"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L58
L7:
iload 2
istore 3
L8:
new java/util/StringTokenizer
dup
aload 7
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;)V
astore 7
L9:
iload 2
istore 3
L10:
aload 7
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
pop
L11:
iload 2
istore 3
L12:
aload 0
aload 7
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
aload 1
invokespecial org/xbill/DNS/ResolverConfig/addServer(Ljava/lang/String;Ljava/util/List;)V
L13:
goto L56
L5:
astore 6
iload 3
istore 2
L59:
aload 0
aload 1
aload 5
invokespecial org/xbill/DNS/ResolverConfig/configureFromLists(Ljava/util/List;Ljava/util/List;)V
aload 0
iload 2
invokespecial org/xbill/DNS/ResolverConfig/configureNdots(I)V
return
L58:
iload 2
istore 3
L14:
aload 7
ldc "domain"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L60
L15:
iload 2
istore 3
L16:
new java/util/StringTokenizer
dup
aload 7
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;)V
astore 7
L17:
iload 2
istore 3
L18:
aload 7
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
pop
L19:
iload 2
istore 3
L20:
aload 7
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L56
L21:
iload 2
istore 3
L22:
aload 5
invokeinterface java/util/List/isEmpty()Z 0
ifeq L56
L23:
iload 2
istore 3
L24:
aload 0
aload 7
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
aload 5
invokespecial org/xbill/DNS/ResolverConfig/addSearch(Ljava/lang/String;Ljava/util/List;)V
L25:
goto L56
L60:
iload 2
istore 3
L26:
aload 7
ldc "search"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L61
L27:
iload 2
istore 3
L28:
aload 5
invokeinterface java/util/List/isEmpty()Z 0
ifne L31
L29:
iload 2
istore 3
L30:
aload 5
invokeinterface java/util/List/clear()V 0
L31:
iload 2
istore 3
L32:
new java/util/StringTokenizer
dup
aload 7
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;)V
astore 7
L33:
iload 2
istore 3
L34:
aload 7
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
pop
L35:
iload 2
istore 3
L36:
aload 7
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L56
L37:
iload 2
istore 3
L38:
aload 0
aload 7
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
aload 5
invokespecial org/xbill/DNS/ResolverConfig/addSearch(Ljava/lang/String;Ljava/util/List;)V
L39:
goto L35
L61:
iload 2
istore 3
L40:
aload 7
ldc "options"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L56
L41:
iload 2
istore 3
L42:
new java/util/StringTokenizer
dup
aload 7
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;)V
astore 7
L43:
iload 2
istore 3
L44:
aload 7
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
pop
L45:
iload 2
istore 4
L62:
iload 4
istore 2
iload 4
istore 3
L46:
aload 7
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L56
L47:
iload 4
istore 3
L48:
aload 7
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
astore 8
L49:
iload 4
istore 3
L50:
aload 8
ldc "ndots:"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L62
L51:
iload 4
istore 3
L52:
aload 0
aload 8
invokespecial org/xbill/DNS/ResolverConfig/parseNdots(Ljava/lang/String;)I
istore 4
L53:
goto L62
L57:
iload 2
istore 3
L54:
aload 6
invokevirtual java/io/BufferedReader/close()V
L55:
goto L59
L2:
astore 1
return
.limit locals 9
.limit stack 5
.end method

.method private findSunJVM()Z
.catch java/lang/Exception from L0 to L1 using L2
new java/util/ArrayList
dup
iconst_0
invokespecial java/util/ArrayList/<init>(I)V
astore 1
new java/util/ArrayList
dup
iconst_0
invokespecial java/util/ArrayList/<init>(I)V
astore 2
L0:
iconst_0
anewarray java/lang/Class
astore 4
iconst_0
anewarray java/lang/Object
astore 5
ldc "sun.net.dns.ResolverConfiguration"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 6
aload 6
ldc "open"
aload 4
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aconst_null
aload 5
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
astore 7
aload 6
ldc "nameservers"
aload 4
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aload 7
aload 5
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/List
astore 3
aload 6
ldc "searchlist"
aload 4
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aload 7
aload 5
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/List
astore 4
L1:
aload 3
invokeinterface java/util/List/size()I 0
ifne L3
iconst_0
ireturn
L2:
astore 1
iconst_0
ireturn
L3:
aload 3
invokeinterface java/util/List/size()I 0
ifle L4
aload 3
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L5:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
aload 1
invokespecial org/xbill/DNS/ResolverConfig/addServer(Ljava/lang/String;Ljava/util/List;)V
goto L5
L4:
aload 4
invokeinterface java/util/List/size()I 0
ifle L6
aload 4
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L7:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
aload 2
invokespecial org/xbill/DNS/ResolverConfig/addSearch(Ljava/lang/String;Ljava/util/List;)V
goto L7
L6:
aload 0
aload 1
aload 2
invokespecial org/xbill/DNS/ResolverConfig/configureFromLists(Ljava/util/List;Ljava/util/List;)V
iconst_1
ireturn
.limit locals 8
.limit stack 3
.end method

.method private findUnix()V
aload 0
ldc "/etc/resolv.conf"
invokespecial org/xbill/DNS/ResolverConfig/findResolvConf(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method private findWin(Ljava/io/InputStream;)V
.catch java/io/IOException from L0 to L1 using L2
ldc "org.xbill.DNS.windows.parse.buffer"
sipush 8192
invokestatic java/lang/Integer/getInteger(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
istore 2
new java/io/BufferedInputStream
dup
aload 1
iload 2
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;I)V
astore 1
aload 1
iload 2
invokevirtual java/io/BufferedInputStream/mark(I)V
aload 0
aload 1
aconst_null
invokespecial org/xbill/DNS/ResolverConfig/findWin(Ljava/io/InputStream;Ljava/util/Locale;)V
aload 0
getfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
ifnonnull L3
L0:
aload 1
invokevirtual java/io/BufferedInputStream/reset()V
L1:
aload 0
aload 1
new java/util/Locale
dup
ldc ""
ldc ""
invokespecial java/util/Locale/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokespecial org/xbill/DNS/ResolverConfig/findWin(Ljava/io/InputStream;Ljava/util/Locale;)V
L3:
return
L2:
astore 1
return
.limit locals 3
.limit stack 6
.end method

.method private findWin(Ljava/io/InputStream;Ljava/util/Locale;)V
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
.catch java/io/IOException from L9 to L10 using L2
.catch java/io/IOException from L10 to L11 using L2
.catch org/xbill/DNS/TextParseException from L12 to L13 using L14
.catch java/io/IOException from L12 to L13 using L2
.catch java/io/IOException from L13 to L15 using L2
.catch java/io/IOException from L16 to L17 using L2
.catch java/io/IOException from L17 to L18 using L2
.catch java/io/IOException from L19 to L20 using L2
.catch java/io/IOException from L21 to L22 using L2
.catch java/io/IOException from L23 to L24 using L2
.catch java/io/IOException from L25 to L26 using L2
.catch java/io/IOException from L27 to L28 using L2
.catch java/io/IOException from L29 to L30 using L2
.catch java/io/IOException from L31 to L32 using L2
.catch java/io/IOException from L33 to L34 using L2
getstatic org/xbill/DNS/ResolverConfig/class$org$xbill$DNS$ResolverConfig Ljava/lang/Class;
ifnonnull L35
ldc "org.xbill.DNS.ResolverConfig"
invokestatic org/xbill/DNS/ResolverConfig/class$(Ljava/lang/String;)Ljava/lang/Class;
astore 7
aload 7
putstatic org/xbill/DNS/ResolverConfig/class$org$xbill$DNS$ResolverConfig Ljava/lang/Class;
L36:
aload 7
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
astore 7
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 7
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ".windows.DNSServer"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
astore 7
aload 2
ifnull L37
aload 7
aload 2
invokestatic java/util/ResourceBundle/getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;
astore 2
L38:
aload 2
ldc "host_name"
invokevirtual java/util/ResourceBundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 7
aload 2
ldc "primary_dns_suffix"
invokevirtual java/util/ResourceBundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 8
aload 2
ldc "dns_suffix"
invokevirtual java/util/ResourceBundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 9
aload 2
ldc "dns_servers"
invokevirtual java/util/ResourceBundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 10
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 1
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 11
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 12
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 13
L1:
iconst_0
istore 4
iconst_0
istore 3
L3:
aload 11
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 15
L4:
aload 15
ifnull L33
L5:
new java/util/StringTokenizer
dup
aload 15
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;)V
astore 14
aload 14
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
istore 6
L6:
iload 6
ifne L7
iconst_0
istore 4
iconst_0
istore 3
goto L3
L35:
getstatic org/xbill/DNS/ResolverConfig/class$org$xbill$DNS$ResolverConfig Ljava/lang/Class;
astore 7
goto L36
L37:
aload 7
invokestatic java/util/ResourceBundle/getBundle(Ljava/lang/String;)Ljava/util/ResourceBundle;
astore 2
goto L38
L7:
aload 14
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
astore 1
aload 15
ldc ":"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L39
L8:
iconst_0
istore 3
iconst_0
istore 4
L9:
aload 15
aload 7
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L16
L10:
aload 14
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L12
aload 14
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
astore 1
L11:
goto L10
L12:
aload 1
aconst_null
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 2
L13:
aload 2
invokevirtual org/xbill/DNS/Name/labels()I
iconst_1
if_icmpeq L40
aload 0
aload 1
aload 13
invokespecial org/xbill/DNS/ResolverConfig/addSearch(Ljava/lang/String;Ljava/util/List;)V
L15:
iload 3
istore 5
iload 4
istore 3
iload 5
istore 4
goto L3
L16:
aload 15
aload 8
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L41
L17:
aload 14
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L19
aload 14
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
astore 1
L18:
goto L17
L19:
aload 1
ldc ":"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L40
aload 0
aload 1
aload 13
invokespecial org/xbill/DNS/ResolverConfig/addSearch(Ljava/lang/String;Ljava/util/List;)V
L20:
iload 3
istore 4
iconst_1
istore 3
goto L3
L41:
aload 1
astore 2
iload 4
ifne L23
L21:
aload 15
aload 9
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L42
L22:
aload 1
astore 2
L23:
aload 14
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L25
aload 14
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
astore 2
L24:
goto L23
L25:
aload 2
ldc ":"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L40
aload 0
aload 2
aload 13
invokespecial org/xbill/DNS/ResolverConfig/addSearch(Ljava/lang/String;Ljava/util/List;)V
L26:
iload 3
istore 4
iconst_1
istore 3
goto L3
L42:
aload 1
astore 2
iload 3
ifne L29
iload 3
istore 5
L27:
aload 15
aload 10
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
iconst_m1
if_icmpeq L43
L28:
aload 1
astore 2
L29:
aload 14
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L31
aload 14
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
astore 2
L30:
goto L29
L31:
aload 2
ldc ":"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L40
aload 0
aload 2
aload 12
invokespecial org/xbill/DNS/ResolverConfig/addServer(Ljava/lang/String;Ljava/util/List;)V
L32:
iconst_1
istore 5
goto L43
L33:
aload 0
aload 12
aload 13
invokespecial org/xbill/DNS/ResolverConfig/configureFromLists(Ljava/util/List;Ljava/util/List;)V
L34:
return
L2:
astore 1
return
L40:
iload 3
istore 5
iload 4
istore 3
iload 5
istore 4
goto L3
L39:
iload 3
istore 5
iload 4
istore 3
iload 5
istore 4
goto L9
L14:
astore 1
iload 3
istore 5
iload 4
istore 3
iload 5
istore 4
goto L3
L43:
iload 4
istore 3
iload 5
istore 4
goto L3
.limit locals 16
.limit stack 5
.end method

.method public static getCurrentConfig()Lorg/xbill/DNS/ResolverConfig;
.catch all from L0 to L1 using L2
ldc org/xbill/DNS/ResolverConfig
monitorenter
L0:
getstatic org/xbill/DNS/ResolverConfig/currentConfig Lorg/xbill/DNS/ResolverConfig;
astore 0
L1:
ldc org/xbill/DNS/ResolverConfig
monitorexit
aload 0
areturn
L2:
astore 0
ldc org/xbill/DNS/ResolverConfig
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 1
.end method

.method private parseNdots(Ljava/lang/String;)I
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L4 using L2
aload 1
bipush 6
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 1
L0:
aload 1
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 2
L1:
iload 2
iflt L5
L3:
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L4
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "setting ndots "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L4:
iload 2
ireturn
L2:
astore 1
L5:
iconst_m1
ireturn
.limit locals 3
.limit stack 4
.end method

.method public static refresh()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
new org/xbill/DNS/ResolverConfig
dup
invokespecial org/xbill/DNS/ResolverConfig/<init>()V
astore 1
getstatic org/xbill/DNS/ResolverConfig/class$org$xbill$DNS$ResolverConfig Ljava/lang/Class;
ifnonnull L5
ldc "org.xbill.DNS.ResolverConfig"
invokestatic org/xbill/DNS/ResolverConfig/class$(Ljava/lang/String;)Ljava/lang/Class;
astore 0
aload 0
putstatic org/xbill/DNS/ResolverConfig/class$org$xbill$DNS$ResolverConfig Ljava/lang/Class;
L6:
aload 0
monitorenter
L0:
aload 1
putstatic org/xbill/DNS/ResolverConfig/currentConfig Lorg/xbill/DNS/ResolverConfig;
aload 0
monitorexit
L1:
return
L5:
getstatic org/xbill/DNS/ResolverConfig/class$org$xbill$DNS$ResolverConfig Ljava/lang/Class;
astore 0
goto L6
L2:
astore 1
L3:
aload 0
monitorexit
L4:
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public ndots()I
aload 0
getfield org/xbill/DNS/ResolverConfig/ndots I
ifge L0
iconst_1
ireturn
L0:
aload 0
getfield org/xbill/DNS/ResolverConfig/ndots I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public searchPath()[Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/ResolverConfig/searchlist [Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public server()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
iconst_0
aaload
areturn
.limit locals 1
.limit stack 2
.end method

.method public servers()[Ljava/lang/String;
aload 0
getfield org/xbill/DNS/ResolverConfig/servers [Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
