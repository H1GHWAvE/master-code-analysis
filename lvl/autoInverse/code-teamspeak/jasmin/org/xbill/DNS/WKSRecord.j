.bytecode 50.0
.class public synchronized org/xbill/DNS/WKSRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -9104259763909119805L


.field private 'address' [B

.field private 'protocol' I

.field private 'services' [I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/net/InetAddress;I[I)V
aload 0
aload 1
bipush 11
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 5
invokestatic org/xbill/DNS/Address/familyOf(Ljava/net/InetAddress;)I
iconst_1
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "invalid IPv4 address"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 5
invokevirtual java/net/InetAddress/getAddress()[B
putfield org/xbill/DNS/WKSRecord/address [B
aload 0
ldc "protocol"
iload 6
invokestatic org/xbill/DNS/WKSRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/WKSRecord/protocol I
iconst_0
istore 2
L1:
iload 2
aload 7
arraylength
if_icmpge L2
ldc "service"
aload 7
iload 2
iaload
invokestatic org/xbill/DNS/WKSRecord/checkU16(Ljava/lang/String;I)I
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 0
aload 7
arraylength
newarray int
putfield org/xbill/DNS/WKSRecord/services [I
aload 7
iconst_0
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
iconst_0
aload 7
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
invokestatic java/util/Arrays/sort([I)V
return
.limit locals 8
.limit stack 6
.end method

.method public getAddress()Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
L0:
aload 0
getfield org/xbill/DNS/WKSRecord/address [B
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/WKSRecord
dup
invokespecial org/xbill/DNS/WKSRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getProtocol()I
aload 0
getfield org/xbill/DNS/WKSRecord/protocol I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getServices()[I
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
areturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
iconst_1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
putfield org/xbill/DNS/WKSRecord/address [B
aload 0
getfield org/xbill/DNS/WKSRecord/address [B
ifnonnull L0
aload 1
ldc "invalid address"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/WKSRecord$Protocol/value(Ljava/lang/String;)I
putfield org/xbill/DNS/WKSRecord/protocol I
aload 0
getfield org/xbill/DNS/WKSRecord/protocol I
ifge L1
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid IP protocol: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
L2:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 4
aload 4
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifeq L3
aload 4
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokestatic org/xbill/DNS/WKSRecord$Service/value(Ljava/lang/String;)I
istore 3
iload 3
ifge L4
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid TCP/UDP service: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 4
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L4:
aload 2
new java/lang/Integer
dup
iload 3
invokespecial java/lang/Integer/<init>(I)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L2
L3:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
aload 0
aload 2
invokeinterface java/util/List/size()I 0
newarray int
putfield org/xbill/DNS/WKSRecord/services [I
iconst_0
istore 3
L5:
iload 3
aload 2
invokeinterface java/util/List/size()I 0
if_icmpge L6
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
iload 3
aload 2
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iastore
iload 3
iconst_1
iadd
istore 3
goto L5
L6:
return
.limit locals 5
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
iconst_0
istore 4
aload 0
aload 1
iconst_4
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/WKSRecord/address [B
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/WKSRecord/protocol I
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
astore 1
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 5
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
iconst_0
istore 3
L2:
iload 3
bipush 8
if_icmpge L3
aload 1
iload 2
baload
sipush 255
iand
iconst_1
bipush 7
iload 3
isub
ishl
iand
ifeq L4
aload 5
new java/lang/Integer
dup
iload 2
bipush 8
imul
iload 3
iadd
invokespecial java/lang/Integer/<init>(I)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L4:
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
aload 5
invokeinterface java/util/List/size()I 0
newarray int
putfield org/xbill/DNS/WKSRecord/services [I
iload 4
istore 2
L5:
iload 2
aload 5
invokeinterface java/util/List/size()I 0
if_icmpge L6
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
iload 2
aload 5
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iastore
iload 2
iconst_1
iadd
istore 2
goto L5
L6:
return
.limit locals 6
.limit stack 5
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
aload 2
aload 0
getfield org/xbill/DNS/WKSRecord/address [B
invokestatic org/xbill/DNS/Address/toDottedQuad([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
aload 0
getfield org/xbill/DNS/WKSRecord/protocol I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
arraylength
if_icmpge L1
aload 2
new java/lang/StringBuffer
dup
ldc " "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
iload 1
iaload
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/WKSRecord/address [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 1
aload 0
getfield org/xbill/DNS/WKSRecord/protocol I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
arraylength
iconst_1
isub
iaload
bipush 8
idiv
iconst_1
iadd
newarray byte
astore 2
iconst_0
istore 4
L0:
iload 4
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
arraylength
if_icmpge L1
aload 0
getfield org/xbill/DNS/WKSRecord/services [I
iload 4
iaload
istore 5
iload 5
bipush 8
idiv
istore 6
aload 2
iload 6
iconst_1
bipush 7
iload 5
bipush 8
irem
isub
ishl
aload 2
iload 6
baload
ior
i2b
bastore
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
aload 1
aload 2
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 7
.limit stack 6
.end method
