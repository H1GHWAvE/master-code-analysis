.bytecode 50.0
.class public final synchronized org/xbill/DNS/Address
.super java/lang/Object

.field public static final 'IPv4' I = 1


.field public static final 'IPv6' I = 2


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static addrFromRecord(Ljava/lang/String;Lorg/xbill/DNS/Record;)Ljava/net/InetAddress;
aload 1
instanceof org/xbill/DNS/ARecord
ifeq L0
aload 1
checkcast org/xbill/DNS/ARecord
invokevirtual org/xbill/DNS/ARecord/getAddress()Ljava/net/InetAddress;
astore 1
L1:
aload 0
aload 1
invokevirtual java/net/InetAddress/getAddress()[B
invokestatic java/net/InetAddress/getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;
areturn
L0:
aload 1
checkcast org/xbill/DNS/AAAARecord
invokevirtual org/xbill/DNS/AAAARecord/getAddress()Ljava/net/InetAddress;
astore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method public static addressLength(I)I
iload 0
iconst_1
if_icmpne L0
iconst_4
ireturn
L0:
iload 0
iconst_2
if_icmpne L1
bipush 16
ireturn
L1:
new java/lang/IllegalArgumentException
dup
ldc "unknown address family"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public static familyOf(Ljava/net/InetAddress;)I
aload 0
instanceof java/net/Inet4Address
ifeq L0
iconst_1
ireturn
L0:
aload 0
instanceof java/net/Inet6Address
ifeq L1
iconst_2
ireturn
L1:
new java/lang/IllegalArgumentException
dup
ldc "unknown address family"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public static getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
iconst_0
istore 1
L0:
aload 0
invokestatic org/xbill/DNS/Address/getByAddress(Ljava/lang/String;)Ljava/net/InetAddress;
astore 2
L1:
iconst_1
anewarray java/net/InetAddress
dup
iconst_0
aload 2
aastore
areturn
L2:
astore 2
aload 0
iconst_1
invokestatic org/xbill/DNS/Address/lookupHostName(Ljava/lang/String;Z)[Lorg/xbill/DNS/Record;
astore 2
aload 2
arraylength
anewarray java/net/InetAddress
astore 3
L3:
iload 1
aload 2
arraylength
if_icmpge L4
aload 3
iload 1
aload 0
aload 2
iload 1
aaload
invokestatic org/xbill/DNS/Address/addrFromRecord(Ljava/lang/String;Lorg/xbill/DNS/Record;)Ljava/net/InetAddress;
aastore
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method public static getByAddress(Ljava/lang/String;)Ljava/net/InetAddress;
aload 0
iconst_1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 1
aload 1
ifnull L0
aload 0
aload 1
invokestatic java/net/InetAddress/getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;
areturn
L0:
aload 0
iconst_2
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 1
aload 1
ifnull L1
aload 0
aload 1
invokestatic java/net/InetAddress/getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;
areturn
L1:
new java/net/UnknownHostException
dup
new java/lang/StringBuffer
dup
ldc "Invalid address: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method public static getByAddress(Ljava/lang/String;I)Ljava/net/InetAddress;
iload 1
iconst_1
if_icmpeq L0
iload 1
iconst_2
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "unknown address family"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 2
aload 2
ifnull L1
aload 0
aload 2
invokestatic java/net/InetAddress/getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;
areturn
L1:
new java/net/UnknownHostException
dup
new java/lang/StringBuffer
dup
ldc "Invalid address: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 5
.end method

.method public static getByName(Ljava/lang/String;)Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
L0:
aload 0
invokestatic org/xbill/DNS/Address/getByAddress(Ljava/lang/String;)Ljava/net/InetAddress;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 0
aload 0
iconst_0
invokestatic org/xbill/DNS/Address/lookupHostName(Ljava/lang/String;Z)[Lorg/xbill/DNS/Record;
iconst_0
aaload
invokestatic org/xbill/DNS/Address/addrFromRecord(Ljava/lang/String;Lorg/xbill/DNS/Record;)Ljava/net/InetAddress;
areturn
.limit locals 2
.limit stack 3
.end method

.method public static getHostName(Ljava/net/InetAddress;)Ljava/lang/String;
new org/xbill/DNS/Lookup
dup
aload 0
invokestatic org/xbill/DNS/ReverseMap/fromAddress(Ljava/net/InetAddress;)Lorg/xbill/DNS/Name;
bipush 12
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;I)V
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
astore 0
aload 0
ifnonnull L0
new java/net/UnknownHostException
dup
ldc "unknown address"
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iconst_0
aaload
checkcast org/xbill/DNS/PTRRecord
invokevirtual org/xbill/DNS/PTRRecord/getTarget()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 4
.end method

.method public static isDottedQuad(Ljava/lang/String;)Z
aload 0
iconst_1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static lookupHostName(Ljava/lang/String;Z)[Lorg/xbill/DNS/Record;
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
.catch org/xbill/DNS/TextParseException from L3 to L4 using L2
.catch org/xbill/DNS/TextParseException from L5 to L2 using L2
.catch org/xbill/DNS/TextParseException from L6 to L7 using L2
.catch org/xbill/DNS/TextParseException from L8 to L9 using L2
L0:
new org/xbill/DNS/Lookup
dup
aload 0
iconst_1
invokespecial org/xbill/DNS/Lookup/<init>(Ljava/lang/String;I)V
astore 2
aload 2
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
astore 3
L1:
aload 3
ifnonnull L10
L3:
aload 2
invokevirtual org/xbill/DNS/Lookup/getResult()I
iconst_4
if_icmpne L5
new org/xbill/DNS/Lookup
dup
aload 0
bipush 28
invokespecial org/xbill/DNS/Lookup/<init>(Ljava/lang/String;I)V
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
astore 0
L4:
aload 0
ifnull L5
aload 0
areturn
L5:
new java/net/UnknownHostException
dup
ldc "unknown host"
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 0
new java/net/UnknownHostException
dup
ldc "invalid name"
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
L10:
aload 3
astore 2
iload 1
ifeq L9
L6:
new org/xbill/DNS/Lookup
dup
aload 0
bipush 28
invokespecial org/xbill/DNS/Lookup/<init>(Ljava/lang/String;I)V
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
astore 0
L7:
aload 3
astore 2
aload 0
ifnull L9
L8:
aload 3
arraylength
aload 0
arraylength
iadd
anewarray org/xbill/DNS/Record
astore 2
aload 3
iconst_0
aload 2
iconst_0
aload 3
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
iconst_0
aload 2
aload 3
arraylength
aload 0
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L9:
aload 2
areturn
.limit locals 4
.limit stack 5
.end method

.method private static parseV4(Ljava/lang/String;)[B
iconst_4
newarray byte
astore 8
aload 0
invokevirtual java/lang/String/length()I
istore 7
iconst_0
istore 3
iconst_0
istore 1
iconst_0
istore 4
iconst_0
istore 2
L0:
iload 3
iload 7
if_icmpge L1
aload 0
iload 3
invokevirtual java/lang/String/charAt(I)C
istore 6
iload 6
bipush 48
if_icmplt L2
iload 6
bipush 57
if_icmpgt L2
iload 2
iconst_3
if_icmpne L3
aconst_null
areturn
L3:
iload 2
ifle L4
iload 1
ifne L4
aconst_null
areturn
L4:
iload 2
iconst_1
iadd
istore 5
iload 6
bipush 48
isub
iload 1
bipush 10
imul
iadd
istore 6
iload 6
istore 2
iload 5
istore 1
iload 6
sipush 255
if_icmple L5
aconst_null
areturn
L2:
iload 6
bipush 46
if_icmpne L6
iload 4
iconst_3
if_icmpne L7
aconst_null
areturn
L7:
iload 2
ifne L8
aconst_null
areturn
L8:
aload 8
iload 4
iload 1
i2b
bastore
iload 4
iconst_1
iadd
istore 4
iconst_0
istore 1
iconst_0
istore 2
L5:
iload 3
iconst_1
iadd
istore 3
iload 2
istore 5
iload 1
istore 2
iload 5
istore 1
goto L0
L6:
aconst_null
areturn
L1:
iload 4
iconst_3
if_icmpeq L9
aconst_null
areturn
L9:
iload 2
ifne L10
aconst_null
areturn
L10:
aload 8
iload 4
iload 1
i2b
bastore
aload 8
areturn
.limit locals 9
.limit stack 3
.end method

.method private static parseV6(Ljava/lang/String;)[B
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L4 using L2
iconst_m1
istore 2
bipush 16
newarray byte
astore 7
aload 0
ldc ":"
iconst_m1
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 0
aload 0
arraylength
iconst_1
isub
istore 3
aload 0
iconst_0
aaload
invokevirtual java/lang/String/length()I
ifne L5
iload 3
iconst_0
iadd
ifle L6
aload 0
iconst_1
aaload
invokevirtual java/lang/String/length()I
ifne L6
iconst_1
istore 1
L7:
aload 0
iload 3
aaload
invokevirtual java/lang/String/length()I
ifne L8
iload 3
iload 1
isub
ifle L9
aload 0
iload 3
iconst_1
isub
aaload
invokevirtual java/lang/String/length()I
ifne L9
iload 3
iconst_1
isub
istore 3
L10:
iload 3
iload 1
isub
iconst_1
iadd
bipush 8
if_icmple L11
aconst_null
areturn
L6:
aconst_null
areturn
L9:
aconst_null
areturn
L11:
iconst_0
istore 5
iload 1
istore 4
iload 5
istore 1
L12:
iload 1
istore 5
iload 4
iload 3
if_icmpgt L13
aload 0
iload 4
aaload
invokevirtual java/lang/String/length()I
ifne L14
iload 2
iflt L15
aconst_null
areturn
L15:
iload 1
istore 2
L16:
iload 4
iconst_1
iadd
istore 4
goto L12
L14:
aload 0
iload 4
aaload
bipush 46
invokevirtual java/lang/String/indexOf(I)I
iflt L17
iload 4
iload 3
if_icmpge L18
aconst_null
areturn
L18:
iload 4
bipush 6
if_icmple L19
aconst_null
areturn
L19:
aload 0
iload 4
aaload
iconst_1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 0
aload 0
ifnonnull L20
aconst_null
areturn
L20:
iconst_0
istore 3
L21:
iload 1
istore 5
iload 3
iconst_4
if_icmpge L13
aload 7
iload 1
aload 0
iload 3
baload
bastore
iload 3
iconst_1
iadd
istore 3
iload 1
iconst_1
iadd
istore 1
goto L21
L17:
iconst_0
istore 5
L0:
iload 5
aload 0
iload 4
aaload
invokevirtual java/lang/String/length()I
if_icmpge L3
aload 0
iload 4
aaload
iload 5
invokevirtual java/lang/String/charAt(I)C
bipush 16
invokestatic java/lang/Character/digit(CI)I
ifge L22
L1:
aconst_null
areturn
L3:
aload 0
iload 4
aaload
bipush 16
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;I)I
istore 5
L4:
iload 5
ldc_w 65535
if_icmpgt L23
iload 5
ifge L24
goto L23
L24:
iload 1
iconst_1
iadd
istore 6
aload 7
iload 1
iload 5
bipush 8
iushr
i2b
bastore
iload 6
iconst_1
iadd
istore 1
aload 7
iload 6
iload 5
sipush 255
iand
i2b
bastore
goto L16
L2:
astore 0
aconst_null
areturn
L13:
iload 5
bipush 16
if_icmpge L25
iload 2
ifge L25
aconst_null
areturn
L25:
iload 2
iflt L26
bipush 16
iload 5
isub
istore 3
aload 7
iload 2
aload 7
iload 2
iload 3
iadd
iload 5
iload 2
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
istore 1
L27:
iload 1
iload 2
iload 3
iadd
if_icmpge L26
aload 7
iload 1
iconst_0
bastore
iload 1
iconst_1
iadd
istore 1
goto L27
L26:
aload 7
areturn
L8:
goto L10
L5:
iconst_0
istore 1
goto L7
L22:
iload 5
iconst_1
iadd
istore 5
goto L0
L23:
aconst_null
areturn
.limit locals 8
.limit stack 6
.end method

.method public static toArray(Ljava/lang/String;)[I
aload 0
iconst_1
invokestatic org/xbill/DNS/Address/toArray(Ljava/lang/String;I)[I
areturn
.limit locals 1
.limit stack 2
.end method

.method public static toArray(Ljava/lang/String;I)[I
aload 0
iload 1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 0
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
arraylength
newarray int
astore 2
iconst_0
istore 1
L1:
iload 1
aload 0
arraylength
if_icmpge L2
aload 2
iload 1
aload 0
iload 1
baload
sipush 255
iand
iastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method public static toByteArray(Ljava/lang/String;I)[B
iload 1
iconst_1
if_icmpne L0
aload 0
invokestatic org/xbill/DNS/Address/parseV4(Ljava/lang/String;)[B
areturn
L0:
iload 1
iconst_2
if_icmpne L1
aload 0
invokestatic org/xbill/DNS/Address/parseV6(Ljava/lang/String;)[B
areturn
L1:
new java/lang/IllegalArgumentException
dup
ldc "unknown address family"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public static toDottedQuad([B)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
iconst_0
baload
sipush 255
iand
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
iconst_1
baload
sipush 255
iand
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
iconst_2
baload
sipush 255
iand
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
iconst_3
baload
sipush 255
iand
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static toDottedQuad([I)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
iconst_0
iaload
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
iconst_1
iaload
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
iconst_2
iaload
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
iconst_3
iaload
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static truncate(Ljava/net/InetAddress;I)Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
iconst_0
istore 3
aload 0
invokestatic org/xbill/DNS/Address/familyOf(Ljava/net/InetAddress;)I
invokestatic org/xbill/DNS/Address/addressLength(I)I
bipush 8
imul
istore 2
iload 1
iflt L3
iload 1
iload 2
if_icmple L4
L3:
new java/lang/IllegalArgumentException
dup
ldc "invalid mask length"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
iload 1
iload 2
if_icmpne L5
aload 0
areturn
L5:
aload 0
invokevirtual java/net/InetAddress/getAddress()[B
astore 0
iload 1
bipush 8
idiv
iconst_1
iadd
istore 2
L6:
iload 2
aload 0
arraylength
if_icmpge L7
aload 0
iload 2
iconst_0
bastore
iload 2
iconst_1
iadd
istore 2
goto L6
L7:
iconst_0
istore 4
iload 3
istore 2
iload 4
istore 3
L8:
iload 3
iload 1
bipush 8
irem
if_icmpge L9
iload 2
iconst_1
bipush 7
iload 3
isub
ishl
ior
istore 2
iload 3
iconst_1
iadd
istore 3
goto L8
L9:
iload 1
bipush 8
idiv
istore 1
aload 0
iload 1
iload 2
aload 0
iload 1
baload
iand
i2b
bastore
L0:
aload 0
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/IllegalArgumentException
dup
ldc "invalid address"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 5
.end method
