.bytecode 50.0
.class public synchronized org/xbill/DNS/SSHFPRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -8104701402654687025L


.field private 'alg' I

.field private 'digestType' I

.field private 'fingerprint' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJII[B)V
aload 0
aload 1
bipush 44
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "alg"
iload 5
invokestatic org/xbill/DNS/SSHFPRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/SSHFPRecord/alg I
aload 0
ldc "digestType"
iload 6
invokestatic org/xbill/DNS/SSHFPRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/SSHFPRecord/digestType I
aload 0
aload 7
putfield org/xbill/DNS/SSHFPRecord/fingerprint [B
return
.limit locals 8
.limit stack 6
.end method

.method public getAlgorithm()I
aload 0
getfield org/xbill/DNS/SSHFPRecord/alg I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDigestType()I
aload 0
getfield org/xbill/DNS/SSHFPRecord/digestType I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFingerPrint()[B
aload 0
getfield org/xbill/DNS/SSHFPRecord/fingerprint [B
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/SSHFPRecord
dup
invokespecial org/xbill/DNS/SSHFPRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/SSHFPRecord/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/SSHFPRecord/digestType I
aload 0
aload 1
iconst_1
invokevirtual org/xbill/DNS/Tokenizer/getHex(Z)[B
putfield org/xbill/DNS/SSHFPRecord/fingerprint [B
return
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/SSHFPRecord/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/SSHFPRecord/digestType I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/SSHFPRecord/fingerprint [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/SSHFPRecord/alg I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SSHFPRecord/digestType I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SSHFPRecord/fingerprint [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/SSHFPRecord/alg I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/SSHFPRecord/digestType I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/SSHFPRecord/fingerprint [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 4
.limit stack 2
.end method
