.bytecode 50.0
.class synchronized abstract org/xbill/DNS/SIGBase
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -3738444391533812369L


.field protected 'alg' I

.field protected 'covered' I

.field protected 'expire' Ljava/util/Date;

.field protected 'footprint' I

.field protected 'labels' I

.field protected 'origttl' J

.field protected 'signature' [B

.field protected 'signer' Lorg/xbill/DNS/Name;

.field protected 'timeSigned' Ljava/util/Date;

.method protected <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IIJIIJLjava/util/Date;Ljava/util/Date;ILorg/xbill/DNS/Name;[B)V
aload 0
aload 1
iload 2
iload 3
lload 4
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
iload 6
invokestatic org/xbill/DNS/Type/check(I)V
lload 8
invokestatic org/xbill/DNS/TTL/check(J)V
aload 0
iload 6
putfield org/xbill/DNS/SIGBase/covered I
aload 0
ldc "alg"
iload 7
invokestatic org/xbill/DNS/SIGBase/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/SIGBase/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
iconst_1
isub
putfield org/xbill/DNS/SIGBase/labels I
aload 1
invokevirtual org/xbill/DNS/Name/isWild()Z
ifeq L0
aload 0
aload 0
getfield org/xbill/DNS/SIGBase/labels I
iconst_1
isub
putfield org/xbill/DNS/SIGBase/labels I
L0:
aload 0
lload 8
putfield org/xbill/DNS/SIGBase/origttl J
aload 0
aload 10
putfield org/xbill/DNS/SIGBase/expire Ljava/util/Date;
aload 0
aload 11
putfield org/xbill/DNS/SIGBase/timeSigned Ljava/util/Date;
aload 0
ldc "footprint"
iload 12
invokestatic org/xbill/DNS/SIGBase/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/SIGBase/footprint I
aload 0
ldc "signer"
aload 13
invokestatic org/xbill/DNS/SIGBase/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SIGBase/signer Lorg/xbill/DNS/Name;
aload 0
aload 14
putfield org/xbill/DNS/SIGBase/signature [B
return
.limit locals 15
.limit stack 6
.end method

.method public getAlgorithm()I
aload 0
getfield org/xbill/DNS/SIGBase/alg I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getExpire()Ljava/util/Date;
aload 0
getfield org/xbill/DNS/SIGBase/expire Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFootprint()I
aload 0
getfield org/xbill/DNS/SIGBase/footprint I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getLabels()I
aload 0
getfield org/xbill/DNS/SIGBase/labels I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOrigTTL()J
aload 0
getfield org/xbill/DNS/SIGBase/origttl J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSignature()[B
aload 0
getfield org/xbill/DNS/SIGBase/signature [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSigner()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/SIGBase/signer Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTimeSigned()Ljava/util/Date;
aload 0
getfield org/xbill/DNS/SIGBase/timeSigned Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTypeCovered()I
aload 0
getfield org/xbill/DNS/SIGBase/covered I
ireturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 3
aload 0
aload 3
invokestatic org/xbill/DNS/Type/value(Ljava/lang/String;)I
putfield org/xbill/DNS/SIGBase/covered I
aload 0
getfield org/xbill/DNS/SIGBase/covered I
ifge L0
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid type: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 3
aload 0
aload 3
invokestatic org/xbill/DNS/DNSSEC$Algorithm/value(Ljava/lang/String;)I
putfield org/xbill/DNS/SIGBase/alg I
aload 0
getfield org/xbill/DNS/SIGBase/alg I
ifge L1
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid algorithm: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/SIGBase/labels I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getTTL()J
putfield org/xbill/DNS/SIGBase/origttl J
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/FormattedTime/parse(Ljava/lang/String;)Ljava/util/Date;
putfield org/xbill/DNS/SIGBase/expire Ljava/util/Date;
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/FormattedTime/parse(Ljava/lang/String;)Ljava/util/Date;
putfield org/xbill/DNS/SIGBase/timeSigned Ljava/util/Date;
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/SIGBase/footprint I
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SIGBase/signer Lorg/xbill/DNS/Name;
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getBase64()[B
putfield org/xbill/DNS/SIGBase/signature [B
return
.limit locals 4
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/SIGBase/covered I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/SIGBase/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/SIGBase/labels I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/SIGBase/origttl J
aload 0
new java/util/Date
dup
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
putfield org/xbill/DNS/SIGBase/expire Ljava/util/Date;
aload 0
new java/util/Date
dup
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
putfield org/xbill/DNS/SIGBase/timeSigned Ljava/util/Date;
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/SIGBase/footprint I
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/SIGBase/signer Lorg/xbill/DNS/Name;
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/SIGBase/signature [B
return
.limit locals 2
.limit stack 7
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/covered I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/alg I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/labels I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/origttl J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
aload 1
ldc "(\n\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/expire Ljava/util/Date;
invokestatic org/xbill/DNS/FormattedTime/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/timeSigned Ljava/util/Date;
invokestatic org/xbill/DNS/FormattedTime/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/footprint I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/signer Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L1
aload 1
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/signature [B
bipush 64
ldc "\u0009"
iconst_1
invokestatic org/xbill/DNS/utils/base64/formatString([BILjava/lang/String;Z)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L1:
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/signature [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L2
.limit locals 2
.limit stack 5
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/covered I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/alg I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/labels I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/origttl J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/expire Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/timeSigned Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/footprint I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/SIGBase/signer Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/SIGBase/signature [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 4
.limit stack 5
.end method

.method setSignature([B)V
aload 0
aload 1
putfield org/xbill/DNS/SIGBase/signature [B
return
.limit locals 2
.limit stack 2
.end method
