.bytecode 50.0
.class public synchronized org/xbill/DNS/Compression
.super java/lang/Object

.field private static final 'MAX_POINTER' I = 16383


.field private static final 'TABLE_SIZE' I = 17


.field private 'table' [Lorg/xbill/DNS/Compression$Entry;

.field private 'verbose' Z

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc "verbosecompression"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
putfield org/xbill/DNS/Compression/verbose Z
aload 0
bipush 17
anewarray org/xbill/DNS/Compression$Entry
putfield org/xbill/DNS/Compression/table [Lorg/xbill/DNS/Compression$Entry;
return
.limit locals 1
.limit stack 2
.end method

.method public add(ILorg/xbill/DNS/Name;)V
iload 1
sipush 16383
if_icmple L0
L1:
return
L0:
aload 2
invokevirtual org/xbill/DNS/Name/hashCode()I
ldc_w 2147483647
iand
bipush 17
irem
istore 3
new org/xbill/DNS/Compression$Entry
dup
aconst_null
invokespecial org/xbill/DNS/Compression$Entry/<init>(Lorg/xbill/DNS/Compression$1;)V
astore 4
aload 4
aload 2
putfield org/xbill/DNS/Compression$Entry/name Lorg/xbill/DNS/Name;
aload 4
iload 1
putfield org/xbill/DNS/Compression$Entry/pos I
aload 4
aload 0
getfield org/xbill/DNS/Compression/table [Lorg/xbill/DNS/Compression$Entry;
iload 3
aaload
putfield org/xbill/DNS/Compression$Entry/next Lorg/xbill/DNS/Compression$Entry;
aload 0
getfield org/xbill/DNS/Compression/table [Lorg/xbill/DNS/Compression$Entry;
iload 3
aload 4
aastore
aload 0
getfield org/xbill/DNS/Compression/verbose Z
ifeq L1
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "Adding "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc " at "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 5
.limit stack 4
.end method

.method public get(Lorg/xbill/DNS/Name;)I
aload 1
invokevirtual org/xbill/DNS/Name/hashCode()I
istore 3
iconst_m1
istore 2
aload 0
getfield org/xbill/DNS/Compression/table [Lorg/xbill/DNS/Compression$Entry;
iload 3
ldc_w 2147483647
iand
bipush 17
irem
aaload
astore 4
L0:
aload 4
ifnull L1
aload 4
getfield org/xbill/DNS/Compression$Entry/name Lorg/xbill/DNS/Name;
aload 1
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L2
aload 4
getfield org/xbill/DNS/Compression$Entry/pos I
istore 2
L2:
aload 4
getfield org/xbill/DNS/Compression$Entry/next Lorg/xbill/DNS/Compression$Entry;
astore 4
goto L0
L1:
aload 0
getfield org/xbill/DNS/Compression/verbose Z
ifeq L3
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "Looking for "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc ", found "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 2
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L3:
iload 2
ireturn
.limit locals 5
.limit stack 4
.end method
