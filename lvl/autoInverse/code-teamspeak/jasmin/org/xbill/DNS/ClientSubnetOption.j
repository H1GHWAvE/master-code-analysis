.bytecode 50.0
.class public synchronized org/xbill/DNS/ClientSubnetOption
.super org/xbill/DNS/EDNSOption

.field private static final 'serialVersionUID' J = -3868158449890266347L


.field private 'address' Ljava/net/InetAddress;

.field private 'family' I

.field private 'scopeNetmask' I

.field private 'sourceNetmask' I

.method <init>()V
aload 0
bipush 8
invokespecial org/xbill/DNS/EDNSOption/<init>(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(IILjava/net/InetAddress;)V
aload 0
bipush 8
invokespecial org/xbill/DNS/EDNSOption/<init>(I)V
aload 0
aload 3
invokestatic org/xbill/DNS/Address/familyOf(Ljava/net/InetAddress;)I
putfield org/xbill/DNS/ClientSubnetOption/family I
aload 0
ldc "source netmask"
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
iload 1
invokestatic org/xbill/DNS/ClientSubnetOption/checkMaskLength(Ljava/lang/String;II)I
putfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
aload 0
ldc "scope netmask"
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
iload 2
invokestatic org/xbill/DNS/ClientSubnetOption/checkMaskLength(Ljava/lang/String;II)I
putfield org/xbill/DNS/ClientSubnetOption/scopeNetmask I
aload 0
aload 3
iload 1
invokestatic org/xbill/DNS/Address/truncate(Ljava/net/InetAddress;I)Ljava/net/InetAddress;
putfield org/xbill/DNS/ClientSubnetOption/address Ljava/net/InetAddress;
aload 3
aload 0
getfield org/xbill/DNS/ClientSubnetOption/address Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/equals(Ljava/lang/Object;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
ldc "source netmask is not valid for address"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 4
.limit stack 4
.end method

.method public <init>(ILjava/net/InetAddress;)V
aload 0
iload 1
iconst_0
aload 2
invokespecial org/xbill/DNS/ClientSubnetOption/<init>(IILjava/net/InetAddress;)V
return
.limit locals 3
.limit stack 4
.end method

.method private static checkMaskLength(Ljava/lang/String;II)I
iload 1
invokestatic org/xbill/DNS/Address/addressLength(I)I
bipush 8
imul
istore 1
iload 2
iflt L0
iload 2
iload 1
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "\""
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "\" "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 2
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " must be in the range [0.."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "]"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 2
ireturn
.limit locals 3
.limit stack 5
.end method

.method public getAddress()Ljava/net/InetAddress;
aload 0
getfield org/xbill/DNS/ClientSubnetOption/address Ljava/net/InetAddress;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFamily()I
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getScopeNetmask()I
aload 0
getfield org/xbill/DNS/ClientSubnetOption/scopeNetmask I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSourceNetmask()I
aload 0
getfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
ireturn
.limit locals 1
.limit stack 1
.end method

.method optionFromWire(Lorg/xbill/DNS/DNSInput;)V
.catch java/net/UnknownHostException from L0 to L1 using L2
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/ClientSubnetOption/family I
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
iconst_1
if_icmpeq L3
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
iconst_2
if_icmpeq L3
new org/xbill/DNS/WireParseException
dup
ldc "unknown address family"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
aload 0
getfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
invokestatic org/xbill/DNS/Address/addressLength(I)I
bipush 8
imul
if_icmple L4
new org/xbill/DNS/WireParseException
dup
ldc "invalid source netmask"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/ClientSubnetOption/scopeNetmask I
aload 0
getfield org/xbill/DNS/ClientSubnetOption/scopeNetmask I
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
invokestatic org/xbill/DNS/Address/addressLength(I)I
bipush 8
imul
if_icmple L5
new org/xbill/DNS/WireParseException
dup
ldc "invalid scope netmask"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
astore 1
aload 1
arraylength
aload 0
getfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
bipush 7
iadd
bipush 8
idiv
if_icmpeq L6
new org/xbill/DNS/WireParseException
dup
ldc "invalid address"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
invokestatic org/xbill/DNS/Address/addressLength(I)I
newarray byte
astore 2
aload 1
iconst_0
aload 2
iconst_0
aload 1
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L0:
aload 0
aload 2
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
putfield org/xbill/DNS/ClientSubnetOption/address Ljava/net/InetAddress;
L1:
aload 0
getfield org/xbill/DNS/ClientSubnetOption/address Ljava/net/InetAddress;
aload 0
getfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
invokestatic org/xbill/DNS/Address/truncate(Ljava/net/InetAddress;I)Ljava/net/InetAddress;
aload 0
getfield org/xbill/DNS/ClientSubnetOption/address Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/equals(Ljava/lang/Object;)Z
ifne L7
new org/xbill/DNS/WireParseException
dup
ldc "invalid padding"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new org/xbill/DNS/WireParseException
dup
ldc "invalid address"
aload 1
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L7:
return
.limit locals 3
.limit stack 5
.end method

.method optionToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/ClientSubnetOption/address Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc "/"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc ", scope netmask "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/ClientSubnetOption/scopeNetmask I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method optionToWire(Lorg/xbill/DNS/DNSOutput;)V
aload 1
aload 0
getfield org/xbill/DNS/ClientSubnetOption/family I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/ClientSubnetOption/scopeNetmask I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/ClientSubnetOption/address Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/getAddress()[B
iconst_0
aload 0
getfield org/xbill/DNS/ClientSubnetOption/sourceNetmask I
bipush 7
iadd
bipush 8
idiv
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([BII)V
return
.limit locals 2
.limit stack 5
.end method
