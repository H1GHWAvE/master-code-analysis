.bytecode 50.0
.class public synchronized org/xbill/DNS/TSIG$StreamVerifier
.super java/lang/Object

.field private 'key' Lorg/xbill/DNS/TSIG;

.field private 'lastTSIG' Lorg/xbill/DNS/TSIGRecord;

.field private 'lastsigned' I

.field private 'nresponses' I

.field private 'verifier' Lorg/xbill/DNS/utils/HMAC;

.method public <init>(Lorg/xbill/DNS/TSIG;Lorg/xbill/DNS/TSIGRecord;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield org/xbill/DNS/TSIG$StreamVerifier/key Lorg/xbill/DNS/TSIG;
aload 0
new org/xbill/DNS/utils/HMAC
dup
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/key Lorg/xbill/DNS/TSIG;
invokestatic org/xbill/DNS/TSIG/access$000(Lorg/xbill/DNS/TSIG;)Ljava/lang/String;
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/key Lorg/xbill/DNS/TSIG;
invokestatic org/xbill/DNS/TSIG/access$100(Lorg/xbill/DNS/TSIG;)I
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/key Lorg/xbill/DNS/TSIG;
invokestatic org/xbill/DNS/TSIG/access$200(Lorg/xbill/DNS/TSIG;)[B
invokespecial org/xbill/DNS/utils/HMAC/<init>(Ljava/lang/String;I[B)V
putfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 0
iconst_0
putfield org/xbill/DNS/TSIG$StreamVerifier/nresponses I
aload 0
aload 2
putfield org/xbill/DNS/TSIG$StreamVerifier/lastTSIG Lorg/xbill/DNS/TSIGRecord;
return
.limit locals 3
.limit stack 6
.end method

.method public verify(Lorg/xbill/DNS/Message;[B)I
aload 1
invokevirtual org/xbill/DNS/Message/getTSIG()Lorg/xbill/DNS/TSIGRecord;
astore 6
aload 0
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/nresponses I
iconst_1
iadd
putfield org/xbill/DNS/TSIG$StreamVerifier/nresponses I
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/nresponses I
iconst_1
if_icmpne L0
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/key Lorg/xbill/DNS/TSIG;
aload 1
aload 2
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/lastTSIG Lorg/xbill/DNS/TSIGRecord;
invokevirtual org/xbill/DNS/TSIG/verify(Lorg/xbill/DNS/Message;[BLorg/xbill/DNS/TSIGRecord;)I
istore 3
iload 3
ifne L1
aload 6
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
astore 1
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 2
aload 1
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 1
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
L1:
aload 0
aload 6
putfield org/xbill/DNS/TSIG$StreamVerifier/lastTSIG Lorg/xbill/DNS/TSIGRecord;
iload 3
ireturn
L0:
aload 6
ifnull L2
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_3
invokevirtual org/xbill/DNS/Header/decCount(I)V
L2:
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/toWire()[B
astore 7
aload 6
ifnull L3
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_3
invokevirtual org/xbill/DNS/Header/incCount(I)V
L3:
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 7
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 6
ifnonnull L4
aload 2
arraylength
aload 7
arraylength
isub
istore 3
L5:
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 2
aload 7
arraylength
iload 3
invokevirtual org/xbill/DNS/utils/HMAC/update([BII)V
aload 6
ifnull L6
aload 0
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/nresponses I
putfield org/xbill/DNS/TSIG$StreamVerifier/lastsigned I
aload 0
aload 6
putfield org/xbill/DNS/TSIG$StreamVerifier/lastTSIG Lorg/xbill/DNS/TSIGRecord;
aload 6
invokevirtual org/xbill/DNS/TSIGRecord/getName()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/key Lorg/xbill/DNS/TSIG;
invokestatic org/xbill/DNS/TSIG/access$300(Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L7
aload 6
invokevirtual org/xbill/DNS/TSIGRecord/getAlgorithm()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/key Lorg/xbill/DNS/TSIG;
invokestatic org/xbill/DNS/TSIG/access$400(Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifne L8
L7:
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L9
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "BADKEY failure"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L9:
aload 1
iconst_4
putfield org/xbill/DNS/Message/tsigState I
bipush 17
ireturn
L4:
aload 1
getfield org/xbill/DNS/Message/tsigstart I
aload 7
arraylength
isub
istore 3
goto L5
L6:
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/nresponses I
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/lastsigned I
isub
bipush 100
if_icmplt L10
iconst_1
istore 3
L11:
iload 3
ifeq L12
aload 1
iconst_4
putfield org/xbill/DNS/Message/tsigState I
iconst_1
ireturn
L10:
iconst_0
istore 3
goto L11
L12:
aload 1
iconst_2
putfield org/xbill/DNS/Message/tsigState I
iconst_0
ireturn
L8:
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 6
invokevirtual org/xbill/DNS/TSIGRecord/getTimeSigned()Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
lstore 4
aload 2
lload 4
bipush 32
lshr
l2i
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
lload 4
ldc2_w 4294967295L
land
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 2
aload 6
invokevirtual org/xbill/DNS/TSIGRecord/getFudge()I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 6
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
invokevirtual org/xbill/DNS/utils/HMAC/verify([B)Z
ifne L13
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L14
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "BADSIG failure"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L14:
aload 1
iconst_4
putfield org/xbill/DNS/Message/tsigState I
bipush 16
ireturn
L13:
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
invokevirtual org/xbill/DNS/utils/HMAC/clear()V
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 2
aload 6
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 0
getfield org/xbill/DNS/TSIG$StreamVerifier/verifier Lorg/xbill/DNS/utils/HMAC;
aload 6
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 1
iconst_1
putfield org/xbill/DNS/Message/tsigState I
iconst_0
ireturn
.limit locals 8
.limit stack 5
.end method
