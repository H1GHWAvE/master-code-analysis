.bytecode 50.0
.class final synchronized com/a/c/b/a/aa
.super com/a/c/an

.method <init>()V
aload 0
invokespecial com/a/c/an/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/a/c/d/e;Ljava/lang/Class;)V
aload 1
ifnonnull L0
aload 0
invokevirtual com/a/c/d/e/f()Lcom/a/c/d/e;
pop
return
L0:
new java/lang/UnsupportedOperationException
dup
new java/lang/StringBuilder
dup
ldc "Attempted to serialize java.lang.Class: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ". Forgot to register a type adapter?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/lang/Class;
aload 0
invokevirtual com/a/c/d/a/f()Lcom/a/c/d/d;
getstatic com/a/c/d/d/i Lcom/a/c/d/d;
if_acmpne L0
aload 0
invokevirtual com/a/c/d/a/k()V
aconst_null
areturn
L0:
new java/lang/UnsupportedOperationException
dup
ldc "Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
aload 1
invokevirtual com/a/c/d/a/f()Lcom/a/c/d/d;
getstatic com/a/c/d/d/i Lcom/a/c/d/d;
if_acmpne L0
aload 1
invokevirtual com/a/c/d/a/k()V
aconst_null
areturn
L0:
new java/lang/UnsupportedOperationException
dup
ldc "Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
aload 2
checkcast java/lang/Class
astore 2
aload 2
ifnonnull L0
aload 1
invokevirtual com/a/c/d/e/f()Lcom/a/c/d/e;
pop
return
L0:
new java/lang/UnsupportedOperationException
dup
new java/lang/StringBuilder
dup
ldc "Attempted to serialize java.lang.Class: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ". Forgot to register a type adapter?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 5
.end method
