.bytecode 50.0
.class final synchronized com/a/c/b/an
.super java/lang/Object
.implements java/util/Map$Entry

.field 'a' Lcom/a/c/b/an;

.field 'b' Lcom/a/c/b/an;

.field 'c' Lcom/a/c/b/an;

.field 'd' Lcom/a/c/b/an;

.field 'e' Lcom/a/c/b/an;

.field final 'f' Ljava/lang/Object;

.field 'g' Ljava/lang/Object;

.field 'h' I

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/a/c/b/an/f Ljava/lang/Object;
aload 0
aload 0
putfield com/a/c/b/an/e Lcom/a/c/b/an;
aload 0
aload 0
putfield com/a/c/b/an/d Lcom/a/c/b/an;
return
.limit locals 1
.limit stack 2
.end method

.method <init>(Lcom/a/c/b/an;Ljava/lang/Object;Lcom/a/c/b/an;Lcom/a/c/b/an;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/c/b/an/a Lcom/a/c/b/an;
aload 0
aload 2
putfield com/a/c/b/an/f Ljava/lang/Object;
aload 0
iconst_1
putfield com/a/c/b/an/h I
aload 0
aload 3
putfield com/a/c/b/an/d Lcom/a/c/b/an;
aload 0
aload 4
putfield com/a/c/b/an/e Lcom/a/c/b/an;
aload 4
aload 0
putfield com/a/c/b/an/d Lcom/a/c/b/an;
aload 3
aload 0
putfield com/a/c/b/an/e Lcom/a/c/b/an;
return
.limit locals 5
.limit stack 2
.end method

.method private a()Lcom/a/c/b/an;
aload 0
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 1
aload 0
astore 2
L0:
aload 1
ifnull L1
aload 1
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 3
aload 1
astore 2
aload 3
astore 1
goto L0
L1:
aload 2
areturn
.limit locals 4
.limit stack 1
.end method

.method private b()Lcom/a/c/b/an;
aload 0
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 1
aload 0
astore 2
L0:
aload 1
ifnull L1
aload 1
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 3
aload 1
astore 2
aload 3
astore 1
goto L0
L1:
aload 2
areturn
.limit locals 4
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof java/util/Map$Entry
ifeq L0
aload 1
checkcast java/util/Map$Entry
astore 1
aload 0
getfield com/a/c/b/an/f Ljava/lang/Object;
ifnonnull L1
iload 3
istore 2
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
ifnonnull L0
L2:
aload 0
getfield com/a/c/b/an/g Ljava/lang/Object;
ifnonnull L3
iload 3
istore 2
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
ifnonnull L0
L4:
iconst_1
istore 2
L0:
iload 2
ireturn
L1:
iload 3
istore 2
aload 0
getfield com/a/c/b/an/f Ljava/lang/Object;
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
goto L2
L3:
iload 3
istore 2
aload 0
getfield com/a/c/b/an/g Ljava/lang/Object;
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
goto L4
.limit locals 4
.limit stack 2
.end method

.method public final getKey()Ljava/lang/Object;
aload 0
getfield com/a/c/b/an/f Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getValue()Ljava/lang/Object;
aload 0
getfield com/a/c/b/an/g Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
iconst_0
istore 2
aload 0
getfield com/a/c/b/an/f Ljava/lang/Object;
ifnonnull L0
iconst_0
istore 1
L1:
aload 0
getfield com/a/c/b/an/g Ljava/lang/Object;
ifnonnull L2
L3:
iload 1
iload 2
ixor
ireturn
L0:
aload 0
getfield com/a/c/b/an/f Ljava/lang/Object;
invokevirtual java/lang/Object/hashCode()I
istore 1
goto L1
L2:
aload 0
getfield com/a/c/b/an/g Ljava/lang/Object;
invokevirtual java/lang/Object/hashCode()I
istore 2
goto L3
.limit locals 3
.limit stack 2
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/c/b/an/g Ljava/lang/Object;
astore 2
aload 0
aload 1
putfield com/a/c/b/an/g Ljava/lang/Object;
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/a/c/b/an/f Ljava/lang/Object;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/a/c/b/an/g Ljava/lang/Object;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
