.bytecode 50.0
.class final synchronized com/a/c/b/a/as
.super com/a/c/an

.method <init>()V
aload 0
invokespecial com/a/c/an/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Lcom/a/c/d/e;Lcom/a/c/w;)V
aload 2
ifnull L0
aload 2
instanceof com/a/c/y
ifeq L1
L0:
aload 1
invokevirtual com/a/c/d/e/f()Lcom/a/c/d/e;
pop
return
L1:
aload 2
instanceof com/a/c/ac
ifeq L2
aload 2
invokevirtual com/a/c/w/n()Lcom/a/c/ac;
astore 2
aload 2
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L3
aload 1
aload 2
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual com/a/c/d/e/a(Ljava/lang/Number;)Lcom/a/c/d/e;
pop
return
L3:
aload 2
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Boolean
ifeq L4
aload 1
aload 2
invokevirtual com/a/c/ac/l()Z
invokevirtual com/a/c/d/e/a(Z)Lcom/a/c/d/e;
pop
return
L4:
aload 1
aload 2
invokevirtual com/a/c/ac/b()Ljava/lang/String;
invokevirtual com/a/c/d/e/b(Ljava/lang/String;)Lcom/a/c/d/e;
pop
return
L2:
aload 2
instanceof com/a/c/t
ifeq L5
aload 1
invokevirtual com/a/c/d/e/b()Lcom/a/c/d/e;
pop
aload 2
instanceof com/a/c/t
ifeq L6
aload 2
checkcast com/a/c/t
invokevirtual com/a/c/t/iterator()Ljava/util/Iterator;
astore 2
L7:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
aload 0
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/c/w
invokespecial com/a/c/b/a/as/a(Lcom/a/c/d/e;Lcom/a/c/w;)V
goto L7
L6:
new java/lang/IllegalStateException
dup
ldc "This is not a JSON Array."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L8:
aload 1
invokevirtual com/a/c/d/e/c()Lcom/a/c/d/e;
pop
return
L5:
aload 2
instanceof com/a/c/z
ifeq L9
aload 1
invokevirtual com/a/c/d/e/d()Lcom/a/c/d/e;
pop
aload 2
instanceof com/a/c/z
ifeq L10
aload 2
checkcast com/a/c/z
getfield com/a/c/z/a Lcom/a/c/b/ag;
invokevirtual com/a/c/b/ag/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L11:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L12
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual com/a/c/d/e/a(Ljava/lang/String;)Lcom/a/c/d/e;
pop
aload 0
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/c/w
invokespecial com/a/c/b/a/as/a(Lcom/a/c/d/e;Lcom/a/c/w;)V
goto L11
L10:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Not a JSON Object: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L12:
aload 1
invokevirtual com/a/c/d/e/e()Lcom/a/c/d/e;
pop
return
L9:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Couldn't write "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method private b(Lcom/a/c/d/a;)Lcom/a/c/w;
getstatic com/a/c/b/a/ba/a [I
aload 1
invokevirtual com/a/c/d/a/f()Lcom/a/c/d/d;
invokevirtual com/a/c/d/d/ordinal()I
iaload
tableswitch 1
L0
L1
L2
L3
L4
L5
default : L6
L6:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L2:
new com/a/c/ac
dup
aload 1
invokevirtual com/a/c/d/a/i()Ljava/lang/String;
invokespecial com/a/c/ac/<init>(Ljava/lang/String;)V
areturn
L0:
new com/a/c/ac
dup
new com/a/c/b/v
dup
aload 1
invokevirtual com/a/c/d/a/i()Ljava/lang/String;
invokespecial com/a/c/b/v/<init>(Ljava/lang/String;)V
invokespecial com/a/c/ac/<init>(Ljava/lang/Number;)V
areturn
L1:
new com/a/c/ac
dup
aload 1
invokevirtual com/a/c/d/a/j()Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/a/c/ac/<init>(Ljava/lang/Boolean;)V
areturn
L3:
aload 1
invokevirtual com/a/c/d/a/k()V
getstatic com/a/c/y/a Lcom/a/c/y;
areturn
L4:
new com/a/c/t
dup
invokespecial com/a/c/t/<init>()V
astore 2
aload 1
invokevirtual com/a/c/d/a/a()V
L7:
aload 1
invokevirtual com/a/c/d/a/e()Z
ifeq L8
aload 2
aload 0
aload 1
invokespecial com/a/c/b/a/as/b(Lcom/a/c/d/a;)Lcom/a/c/w;
invokevirtual com/a/c/t/a(Lcom/a/c/w;)V
goto L7
L8:
aload 1
invokevirtual com/a/c/d/a/b()V
aload 2
areturn
L5:
new com/a/c/z
dup
invokespecial com/a/c/z/<init>()V
astore 2
aload 1
invokevirtual com/a/c/d/a/c()V
L9:
aload 1
invokevirtual com/a/c/d/a/e()Z
ifeq L10
aload 2
aload 1
invokevirtual com/a/c/d/a/h()Ljava/lang/String;
aload 0
aload 1
invokespecial com/a/c/b/a/as/b(Lcom/a/c/d/a;)Lcom/a/c/w;
invokevirtual com/a/c/z/a(Ljava/lang/String;Lcom/a/c/w;)V
goto L9
L10:
aload 1
invokevirtual com/a/c/d/a/d()V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
aload 0
aload 1
invokespecial com/a/c/b/a/as/b(Lcom/a/c/d/a;)Lcom/a/c/w;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
aload 0
aload 1
aload 2
checkcast com/a/c/w
invokespecial com/a/c/b/a/as/a(Lcom/a/c/d/e;Lcom/a/c/w;)V
return
.limit locals 3
.limit stack 3
.end method
