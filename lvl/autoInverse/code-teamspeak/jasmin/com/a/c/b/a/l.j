.bytecode 50.0
.class public final synchronized com/a/c/b/a/l
.super java/lang/Object
.implements com/a/c/ap

.field private final 'a' Lcom/a/c/b/f;

.field private final 'b' Z

.method public <init>(Lcom/a/c/b/f;Z)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/c/b/a/l/a Lcom/a/c/b/f;
aload 0
iload 2
putfield com/a/c/b/a/l/b Z
return
.limit locals 3
.limit stack 2
.end method

.method private static a(Lcom/a/c/k;Ljava/lang/reflect/Type;)Lcom/a/c/an;
aload 1
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
if_acmpeq L0
aload 1
ldc java/lang/Boolean
if_acmpne L1
L0:
getstatic com/a/c/b/a/z/f Lcom/a/c/an;
areturn
L1:
aload 0
aload 1
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
invokevirtual com/a/c/k/a(Lcom/a/c/c/a;)Lcom/a/c/an;
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/c/b/a/l;)Z
aload 0
getfield com/a/c/b/a/l/b Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
aload 2
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
astore 3
ldc java/util/Map
aload 2
getfield com/a/c/c/a/a Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L0
aconst_null
areturn
L0:
aload 3
aload 3
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;
astore 4
aload 4
iconst_0
aaload
astore 3
aload 3
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
if_acmpeq L1
aload 3
ldc java/lang/Boolean
if_acmpne L2
L1:
getstatic com/a/c/b/a/z/f Lcom/a/c/an;
astore 3
L3:
aload 1
aload 4
iconst_1
aaload
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
invokevirtual com/a/c/k/a(Lcom/a/c/c/a;)Lcom/a/c/an;
astore 5
aload 0
getfield com/a/c/b/a/l/a Lcom/a/c/b/f;
aload 2
invokevirtual com/a/c/b/f/a(Lcom/a/c/c/a;)Lcom/a/c/b/ao;
astore 2
new com/a/c/b/a/m
dup
aload 0
aload 1
aload 4
iconst_0
aaload
aload 3
aload 4
iconst_1
aaload
aload 5
aload 2
invokespecial com/a/c/b/a/m/<init>(Lcom/a/c/b/a/l;Lcom/a/c/k;Ljava/lang/reflect/Type;Lcom/a/c/an;Ljava/lang/reflect/Type;Lcom/a/c/an;Lcom/a/c/b/ao;)V
areturn
L2:
aload 1
aload 3
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
invokevirtual com/a/c/k/a(Lcom/a/c/c/a;)Lcom/a/c/an;
astore 3
goto L3
.limit locals 6
.limit stack 9
.end method
