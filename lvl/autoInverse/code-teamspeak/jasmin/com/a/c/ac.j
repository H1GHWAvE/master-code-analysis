.bytecode 50.0
.class public final synchronized com/a/c/ac
.super com/a/c/w

.field private static final 'b' [Ljava/lang/Class;

.field public 'a' Ljava/lang/Object;

.method static <clinit>()V
bipush 16
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_1
getstatic java/lang/Long/TYPE Ljava/lang/Class;
aastore
dup
iconst_2
getstatic java/lang/Short/TYPE Ljava/lang/Class;
aastore
dup
iconst_3
getstatic java/lang/Float/TYPE Ljava/lang/Class;
aastore
dup
iconst_4
getstatic java/lang/Double/TYPE Ljava/lang/Class;
aastore
dup
iconst_5
getstatic java/lang/Byte/TYPE Ljava/lang/Class;
aastore
dup
bipush 6
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
aastore
dup
bipush 7
getstatic java/lang/Character/TYPE Ljava/lang/Class;
aastore
dup
bipush 8
ldc java/lang/Integer
aastore
dup
bipush 9
ldc java/lang/Long
aastore
dup
bipush 10
ldc java/lang/Short
aastore
dup
bipush 11
ldc java/lang/Float
aastore
dup
bipush 12
ldc java/lang/Double
aastore
dup
bipush 13
ldc java/lang/Byte
aastore
dup
bipush 14
ldc java/lang/Boolean
aastore
dup
bipush 15
ldc java/lang/Character
aastore
putstatic com/a/c/ac/b [Ljava/lang/Class;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Ljava/lang/Boolean;)V
aload 0
invokespecial com/a/c/w/<init>()V
aload 0
aload 1
invokespecial com/a/c/ac/a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method private <init>(Ljava/lang/Character;)V
aload 0
invokespecial com/a/c/w/<init>()V
aload 0
aload 1
invokespecial com/a/c/ac/a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Ljava/lang/Number;)V
aload 0
invokespecial com/a/c/w/<init>()V
aload 0
aload 1
invokespecial com/a/c/ac/a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method <init>(Ljava/lang/Object;)V
aload 0
invokespecial com/a/c/w/<init>()V
aload 0
aload 1
invokespecial com/a/c/ac/a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial com/a/c/w/<init>()V
aload 0
aload 1
invokespecial com/a/c/ac/a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)V
iconst_0
istore 4
aload 1
instanceof java/lang/Character
ifeq L0
aload 0
aload 1
checkcast java/lang/Character
invokevirtual java/lang/Character/charValue()C
invokestatic java/lang/String/valueOf(C)Ljava/lang/String;
putfield com/a/c/ac/a Ljava/lang/Object;
return
L0:
aload 1
instanceof java/lang/Number
ifne L1
aload 1
instanceof java/lang/String
ifeq L2
iconst_1
istore 2
L3:
iload 2
ifeq L4
L1:
iconst_1
istore 4
L4:
iload 4
invokestatic com/a/c/b/a/a(Z)V
aload 0
aload 1
putfield com/a/c/ac/a Ljava/lang/Object;
return
L2:
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
astore 5
getstatic com/a/c/ac/b [Ljava/lang/Class;
astore 6
aload 6
arraylength
istore 3
iconst_0
istore 2
L5:
iload 2
iload 3
if_icmpge L6
aload 6
iload 2
aaload
aload 5
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L7
iconst_1
istore 2
goto L3
L7:
iload 2
iconst_1
iadd
istore 2
goto L5
L6:
iconst_0
istore 2
goto L3
.limit locals 7
.limit stack 2
.end method

.method private static a(Lcom/a/c/ac;)Z
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L0
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/lang/Number
astore 0
aload 0
instanceof java/math/BigInteger
ifne L1
aload 0
instanceof java/lang/Long
ifne L1
aload 0
instanceof java/lang/Integer
ifne L1
aload 0
instanceof java/lang/Short
ifne L1
aload 0
instanceof java/lang/Byte
ifeq L2
L1:
iconst_1
ireturn
L2:
iconst_0
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;)Z
aload 0
instanceof java/lang/String
ifeq L0
L1:
iconst_1
ireturn
L0:
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
astore 0
getstatic com/a/c/ac/b [Ljava/lang/Class;
astore 3
aload 3
arraylength
istore 2
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L3
aload 3
iload 1
aaload
aload 0
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L1
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method private p()Lcom/a/c/ac;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private q()Z
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Boolean
ireturn
.limit locals 1
.limit stack 1
.end method

.method private r()Z
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ireturn
.limit locals 1
.limit stack 1
.end method

.method private s()Z
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/String
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Ljava/lang/Number;
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/String
ifeq L0
new com/a/c/b/v
dup
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/lang/String
invokespecial com/a/c/b/v/<init>(Ljava/lang/String;)V
areturn
L0:
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/lang/Number
areturn
.limit locals 1
.limit stack 3
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L0
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
L0:
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Boolean
ifeq L1
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/toString()Ljava/lang/String;
areturn
L1:
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/lang/String
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()D
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L0
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/doubleValue()D
dreturn
L0:
aload 0
invokevirtual com/a/c/ac/b()Ljava/lang/String;
invokestatic java/lang/Double/parseDouble(Ljava/lang/String;)D
dreturn
.limit locals 1
.limit stack 2
.end method

.method public final d()Ljava/math/BigDecimal;
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/math/BigDecimal
ifeq L0
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/math/BigDecimal
areturn
L0:
new java/math/BigDecimal
dup
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokespecial java/math/BigDecimal/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final e()Ljava/math/BigInteger;
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/math/BigInteger
ifeq L0
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/math/BigInteger
areturn
L0:
new java/math/BigInteger
dup
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 0
aload 1
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
ifnull L2
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
if_acmpeq L3
L2:
iconst_0
ireturn
L3:
aload 1
checkcast com/a/c/ac
astore 1
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
ifnonnull L4
aload 1
getfield com/a/c/ac/a Ljava/lang/Object;
ifnull L1
iconst_0
ireturn
L4:
aload 0
invokestatic com/a/c/ac/a(Lcom/a/c/ac;)Z
ifeq L5
aload 1
invokestatic com/a/c/ac/a(Lcom/a/c/ac;)Z
ifeq L5
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/longValue()J
aload 1
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/longValue()J
lcmp
ifeq L1
iconst_0
ireturn
L5:
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L6
aload 1
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L6
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/doubleValue()D
dstore 2
aload 1
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/doubleValue()D
dstore 4
dload 2
dload 4
dcmpl
ifeq L1
dload 2
invokestatic java/lang/Double/isNaN(D)Z
ifeq L7
dload 4
invokestatic java/lang/Double/isNaN(D)Z
ifne L1
L7:
iconst_0
ireturn
L6:
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
aload 1
getfield com/a/c/ac/a Ljava/lang/Object;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 6
.limit stack 4
.end method

.method public final f()F
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L0
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/floatValue()F
freturn
L0:
aload 0
invokevirtual com/a/c/ac/b()Ljava/lang/String;
invokestatic java/lang/Float/parseFloat(Ljava/lang/String;)F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final g()J
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L0
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/longValue()J
lreturn
L0:
aload 0
invokevirtual com/a/c/ac/b()Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final h()I
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L0
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/intValue()I
ireturn
L0:
aload 0
invokevirtual com/a/c/ac/b()Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
ifnonnull L0
bipush 31
ireturn
L0:
aload 0
invokestatic com/a/c/ac/a(Lcom/a/c/ac;)Z
ifeq L1
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/longValue()J
lstore 1
lload 1
lload 1
bipush 32
lushr
lxor
l2i
ireturn
L1:
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L2
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/doubleValue()D
invokestatic java/lang/Double/doubleToLongBits(D)J
lstore 1
lload 1
lload 1
bipush 32
lushr
lxor
l2i
ireturn
L2:
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 3
.limit stack 5
.end method

.method public final i()B
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L0
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/byteValue()B
ireturn
L0:
aload 0
invokevirtual com/a/c/ac/b()Ljava/lang/String;
invokestatic java/lang/Byte/parseByte(Ljava/lang/String;)B
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final j()C
aload 0
invokevirtual com/a/c/ac/b()Ljava/lang/String;
iconst_0
invokevirtual java/lang/String/charAt(I)C
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final k()S
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L0
aload 0
invokevirtual com/a/c/ac/a()Ljava/lang/Number;
invokevirtual java/lang/Number/shortValue()S
ireturn
L0:
aload 0
invokevirtual com/a/c/ac/b()Ljava/lang/String;
invokestatic java/lang/Short/parseShort(Ljava/lang/String;)S
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final l()Z
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Boolean
ifeq L0
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
ireturn
L0:
aload 0
invokevirtual com/a/c/ac/b()Ljava/lang/String;
invokestatic java/lang/Boolean/parseBoolean(Ljava/lang/String;)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic m()Lcom/a/c/w;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method final o()Ljava/lang/Boolean;
aload 0
getfield com/a/c/ac/a Ljava/lang/Object;
checkcast java/lang/Boolean
areturn
.limit locals 1
.limit stack 1
.end method
