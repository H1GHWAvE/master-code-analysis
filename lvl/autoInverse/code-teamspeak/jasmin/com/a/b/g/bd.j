.bytecode 50.0
.class public final synchronized com/a/b/g/bd
.super java/io/FilterInputStream
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Lcom/a/b/g/al;

.method private <init>(Lcom/a/b/g/ak;Ljava/io/InputStream;)V
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/InputStream
invokespecial java/io/FilterInputStream/<init>(Ljava/io/InputStream;)V
aload 0
aload 1
invokeinterface com/a/b/g/ak/a()Lcom/a/b/g/al; 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/g/al
putfield com/a/b/g/bd/a Lcom/a/b/g/al;
return
.limit locals 3
.limit stack 2
.end method

.method private a()Lcom/a/b/g/ag;
aload 0
getfield com/a/b/g/bd/a Lcom/a/b/g/al;
invokeinterface com/a/b/g/al/a()Lcom/a/b/g/ag; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final mark(I)V
return
.limit locals 2
.limit stack 0
.end method

.method public final markSupported()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final read()I
aload 0
getfield com/a/b/g/bd/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
iconst_m1
if_icmpeq L0
aload 0
getfield com/a/b/g/bd/a Lcom/a/b/g/al;
iload 1
i2b
invokeinterface com/a/b/g/al/b(B)Lcom/a/b/g/al; 1
pop
L0:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final read([BII)I
aload 0
getfield com/a/b/g/bd/in Ljava/io/InputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/InputStream/read([BII)I
istore 3
iload 3
iconst_m1
if_icmpeq L0
aload 0
getfield com/a/b/g/bd/a Lcom/a/b/g/al;
aload 1
iload 2
iload 3
invokeinterface com/a/b/g/al/b([BII)Lcom/a/b/g/al; 3
pop
L0:
iload 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final reset()V
new java/io/IOException
dup
ldc "reset not supported"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method
