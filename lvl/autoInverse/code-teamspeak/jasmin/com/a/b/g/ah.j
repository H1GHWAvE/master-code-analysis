.bytecode 50.0
.class final synchronized com/a/b/g/ah
.super com/a/b/g/ag
.implements java/io/Serializable

.field private static final 'b' J = 0L


.field final 'a' [B

.method <init>([B)V
aload 0
invokespecial com/a/b/g/ag/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
putfield com/a/b/g/ah/a [B
return
.limit locals 2
.limit stack 2
.end method

.method public final a()I
aload 0
getfield com/a/b/g/ah/a [B
arraylength
bipush 8
imul
ireturn
.limit locals 1
.limit stack 2
.end method

.method final a([BII)V
aload 0
getfield com/a/b/g/ah/a [B
iconst_0
aload 1
iload 2
iload 3
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
return
.limit locals 4
.limit stack 5
.end method

.method final a(Lcom/a/b/g/ag;)Z
aload 0
getfield com/a/b/g/ah/a [B
aload 1
invokevirtual com/a/b/g/ag/f()[B
invokestatic java/security/MessageDigest/isEqual([B[B)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b()I
aload 0
getfield com/a/b/g/ah/a [B
arraylength
iconst_4
if_icmplt L0
iconst_1
istore 1
L1:
iload 1
ldc "HashCode#asInt() requires >= 4 bytes (it only has %s bytes)."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/g/ah/a [B
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/g/ah/a [B
iconst_0
baload
sipush 255
iand
aload 0
getfield com/a/b/g/ah/a [B
iconst_1
baload
sipush 255
iand
bipush 8
ishl
ior
aload 0
getfield com/a/b/g/ah/a [B
iconst_2
baload
sipush 255
iand
bipush 16
ishl
ior
aload 0
getfield com/a/b/g/ah/a [B
iconst_3
baload
sipush 255
iand
bipush 24
ishl
ior
ireturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method public final c()J
aload 0
getfield com/a/b/g/ah/a [B
arraylength
bipush 8
if_icmplt L0
iconst_1
istore 1
L1:
iload 1
ldc "HashCode#asLong() requires >= 8 bytes (it only has %s bytes)."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/g/ah/a [B
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/g/ah/d()J
lreturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method public final d()J
aload 0
getfield com/a/b/g/ah/a [B
iconst_0
baload
sipush 255
iand
i2l
lstore 2
iconst_1
istore 1
L0:
iload 1
aload 0
getfield com/a/b/g/ah/a [B
arraylength
bipush 8
invokestatic java/lang/Math/min(II)I
if_icmpge L1
lload 2
aload 0
getfield com/a/b/g/ah/a [B
iload 1
baload
i2l
ldc2_w 255L
land
iload 1
bipush 8
imul
lshl
lor
lstore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
lload 2
lreturn
.limit locals 4
.limit stack 6
.end method

.method public final e()[B
aload 0
getfield com/a/b/g/ah/a [B
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
areturn
.limit locals 1
.limit stack 1
.end method

.method final f()[B
aload 0
getfield com/a/b/g/ah/a [B
areturn
.limit locals 1
.limit stack 1
.end method
