.bytecode 50.0
.class final synchronized com/a/b/g/ai
.super com/a/b/g/ag
.implements java/io/Serializable

.field private static final 'b' J = 0L


.field final 'a' I

.method <init>(I)V
aload 0
invokespecial com/a/b/g/ag/<init>()V
aload 0
iload 1
putfield com/a/b/g/ai/a I
return
.limit locals 2
.limit stack 2
.end method

.method public final a()I
bipush 32
ireturn
.limit locals 1
.limit stack 1
.end method

.method final a([BII)V
iconst_0
istore 4
L0:
iload 4
iload 3
if_icmpge L1
aload 1
iload 2
iload 4
iadd
aload 0
getfield com/a/b/g/ai/a I
iload 4
bipush 8
imul
ishr
i2b
bastore
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
return
.limit locals 5
.limit stack 5
.end method

.method final a(Lcom/a/b/g/ag;)Z
aload 0
getfield com/a/b/g/ai/a I
aload 1
invokevirtual com/a/b/g/ag/b()I
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b()I
aload 0
getfield com/a/b/g/ai/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()J
new java/lang/IllegalStateException
dup
ldc "this HashCode only has 32 bits; cannot create a long"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final d()J
aload 0
getfield com/a/b/g/ai/a I
i2l
ldc2_w 4294967295L
land
lreturn
.limit locals 1
.limit stack 4
.end method

.method public final e()[B
iconst_4
newarray byte
dup
iconst_0
aload 0
getfield com/a/b/g/ai/a I
i2b
bastore
dup
iconst_1
aload 0
getfield com/a/b/g/ai/a I
bipush 8
ishr
i2b
bastore
dup
iconst_2
aload 0
getfield com/a/b/g/ai/a I
bipush 16
ishr
i2b
bastore
dup
iconst_3
aload 0
getfield com/a/b/g/ai/a I
bipush 24
ishr
i2b
bastore
areturn
.limit locals 1
.limit stack 5
.end method
