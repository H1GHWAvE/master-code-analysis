.bytecode 50.0
.class public final synchronized com/a/b/l/ao
.super java/lang/Number
.implements java/io/Serializable
.implements java/lang/Comparable
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation

.field public static final 'a' Lcom/a/b/l/ao;

.field public static final 'b' Lcom/a/b/l/ao;

.field public static final 'c' Lcom/a/b/l/ao;

.field private static final 'd' J = 9223372036854775807L


.field private final 'e' J

.method static <clinit>()V
new com/a/b/l/ao
dup
lconst_0
invokespecial com/a/b/l/ao/<init>(J)V
putstatic com/a/b/l/ao/a Lcom/a/b/l/ao;
new com/a/b/l/ao
dup
lconst_1
invokespecial com/a/b/l/ao/<init>(J)V
putstatic com/a/b/l/ao/b Lcom/a/b/l/ao;
new com/a/b/l/ao
dup
ldc2_w -1L
invokespecial com/a/b/l/ao/<init>(J)V
putstatic com/a/b/l/ao/c Lcom/a/b/l/ao;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(J)V
aload 0
invokespecial java/lang/Number/<init>()V
aload 0
lload 1
putfield com/a/b/l/ao/e J
return
.limit locals 3
.limit stack 3
.end method

.method private static a(J)Lcom/a/b/l/ao;
new com/a/b/l/ao
dup
lload 0
invokespecial com/a/b/l/ao/<init>(J)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
aload 0
getfield com/a/b/l/ao/e J
lstore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/ao
getfield com/a/b/l/ao/e J
lload 2
ladd
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/l/ao;
aload 0
bipush 10
invokestatic com/a/b/l/ap/a(Ljava/lang/String;I)J
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/math/BigInteger;)Lcom/a/b/l/ao;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual java/math/BigInteger/signum()I
iflt L0
aload 0
invokevirtual java/math/BigInteger/bitLength()I
bipush 64
if_icmpgt L0
iconst_1
istore 1
L1:
iload 1
ldc "value (%s) is outside the range for an unsigned long value"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/math/BigInteger/longValue()J
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method private a(I)Ljava/lang/String;
aload 0
getfield com/a/b/l/ao/e J
iload 1
invokestatic com/a/b/l/ap/a(JI)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a()Ljava/math/BigInteger;
aload 0
getfield com/a/b/l/ao/e J
ldc2_w 9223372036854775807L
land
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
astore 2
aload 2
astore 1
aload 0
getfield com/a/b/l/ao/e J
lconst_0
lcmp
ifge L0
aload 2
bipush 63
invokevirtual java/math/BigInteger/setBit(I)Ljava/math/BigInteger;
astore 1
L0:
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method private static b(J)Lcom/a/b/l/ao;
lload 0
lconst_0
lcmp
iflt L0
iconst_1
istore 2
L1:
iload 2
ldc "value (%s) is outside the range for an unsigned long value"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 0
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 7
.end method

.method private b(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
aload 0
getfield com/a/b/l/ao/e J
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/ao
getfield com/a/b/l/ao/e J
lsub
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
.limit locals 2
.limit stack 4
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/l/ao;
aload 0
bipush 10
invokestatic com/a/b/l/ap/a(Ljava/lang/String;I)J
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
.limit locals 1
.limit stack 2
.end method

.method private c(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/l/ao/e J
lstore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/ao
getfield com/a/b/l/ao/e J
lload 2
lmul
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
.limit locals 4
.limit stack 4
.end method

.method private d(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/l/ao/e J
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/ao
getfield com/a/b/l/ao/e J
invokestatic com/a/b/l/ap/b(JJ)J
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
.limit locals 2
.limit stack 4
.end method

.method private e(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/l/ao/e J
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/l/ao
getfield com/a/b/l/ao/e J
invokestatic com/a/b/l/ap/c(JJ)J
invokestatic com/a/b/l/ao/a(J)Lcom/a/b/l/ao;
areturn
.limit locals 2
.limit stack 4
.end method

.method private f(Lcom/a/b/l/ao;)I
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/l/ao/e J
aload 1
getfield com/a/b/l/ao/e J
invokestatic com/a/b/l/ap/a(JJ)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
aload 1
checkcast com/a/b/l/ao
astore 1
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/l/ao/e J
aload 1
getfield com/a/b/l/ao/e J
invokestatic com/a/b/l/ap/a(JJ)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final doubleValue()D
aload 0
getfield com/a/b/l/ao/e J
ldc2_w 9223372036854775807L
land
l2d
dstore 3
dload 3
dstore 1
aload 0
getfield com/a/b/l/ao/e J
lconst_0
lcmp
ifge L0
dload 3
ldc2_w 9.223372036854776E18D
dadd
dstore 1
L0:
dload 1
dreturn
.limit locals 5
.limit stack 4
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/l/ao
ifeq L0
aload 1
checkcast com/a/b/l/ao
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/l/ao/e J
aload 1
getfield com/a/b/l/ao/e J
lcmp
ifne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final floatValue()F
aload 0
getfield com/a/b/l/ao/e J
ldc2_w 9223372036854775807L
land
l2f
fstore 2
fload 2
fstore 1
aload 0
getfield com/a/b/l/ao/e J
lconst_0
lcmp
ifge L0
fload 2
ldc_w 9.223372E18F
fadd
fstore 1
L0:
fload 1
freturn
.limit locals 3
.limit stack 4
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/l/ao/e J
lstore 1
lload 1
lload 1
bipush 32
lushr
lxor
l2i
ireturn
.limit locals 3
.limit stack 5
.end method

.method public final intValue()I
aload 0
getfield com/a/b/l/ao/e J
l2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final longValue()J
aload 0
getfield com/a/b/l/ao/e J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/l/ao/e J
invokestatic com/a/b/l/ap/a(J)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
