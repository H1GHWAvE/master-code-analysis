.bytecode 50.0
.class public final synchronized com/a/b/l/u
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field public static final 'a' I = 8


.field public static final 'b' J = 4611686018427387904L


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(J)I
lload 0
bipush 32
lushr
lload 0
lxor
l2i
ireturn
.limit locals 2
.limit stack 4
.end method

.method public static a(JJ)I
lload 0
lload 2
lcmp
ifge L0
iconst_m1
ireturn
L0:
lload 0
lload 2
lcmp
ifle L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method static a([JJII)I
iload 4
iconst_1
isub
istore 4
L0:
iload 4
iload 3
if_icmplt L1
aload 0
iload 4
laload
lload 1
lcmp
ifne L2
iload 4
ireturn
L2:
iload 4
iconst_1
isub
istore 4
goto L0
L1:
iconst_m1
ireturn
.limit locals 5
.limit stack 4
.end method

.method private static a([J[J)I
aload 0
ldc "array"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "target"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
iconst_0
ireturn
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
aload 1
arraylength
isub
iconst_1
iadd
if_icmpge L2
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 0
iload 2
iload 3
iadd
laload
aload 1
iload 3
laload
lcmp
ifne L5
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
iload 2
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 4
.limit stack 4
.end method

.method public static a(BBBBBBBB)J
iload 0
i2l
ldc2_w 255L
land
bipush 56
lshl
iload 1
i2l
ldc2_w 255L
land
bipush 48
lshl
lor
iload 2
i2l
ldc2_w 255L
land
bipush 40
lshl
lor
iload 3
i2l
ldc2_w 255L
land
bipush 32
lshl
lor
iload 4
i2l
ldc2_w 255L
land
bipush 24
lshl
lor
iload 5
i2l
ldc2_w 255L
land
bipush 16
lshl
lor
iload 6
i2l
ldc2_w 255L
land
bipush 8
lshl
lor
iload 7
i2l
ldc2_w 255L
land
lor
lreturn
.limit locals 8
.limit stack 6
.end method

.method private static a([B)J
aload 0
arraylength
bipush 8
if_icmplt L0
iconst_1
istore 1
L1:
iload 1
ldc "array too small: %s < %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
bipush 8
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iconst_0
baload
aload 0
iconst_1
baload
aload 0
iconst_2
baload
aload 0
iconst_3
baload
aload 0
iconst_4
baload
aload 0
iconst_5
baload
aload 0
bipush 6
baload
aload 0
bipush 7
baload
invokestatic com/a/b/l/u/a(BBBBBBBB)J
lreturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 9
.end method

.method private static transient a([J)J
iconst_1
istore 1
aload 0
arraylength
ifle L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
laload
lstore 3
L2:
iload 1
aload 0
arraylength
if_icmpge L3
lload 3
lstore 5
aload 0
iload 1
laload
lload 3
lcmp
ifge L4
aload 0
iload 1
laload
lstore 5
L4:
iload 1
iconst_1
iadd
istore 1
lload 5
lstore 3
goto L2
L0:
iconst_0
istore 2
goto L1
L3:
lload 3
lreturn
.limit locals 7
.limit stack 4
.end method

.method private static a()Lcom/a/b/b/ak;
.annotation invisible Lcom/a/b/a/a;
.end annotation
getstatic com/a/b/l/x/a Lcom/a/b/l/x;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Long;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/String/isEmpty()Z
ifeq L0
aconst_null
areturn
L0:
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
bipush 45
if_icmpne L1
iconst_1
istore 1
L2:
iload 1
ifeq L3
iconst_1
istore 2
L4:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpne L5
aconst_null
areturn
L1:
iconst_0
istore 1
goto L2
L3:
iconst_0
istore 2
goto L4
L5:
iload 2
iconst_1
iadd
istore 3
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
bipush 48
isub
istore 2
iload 2
iflt L6
iload 2
bipush 9
if_icmple L7
L6:
aconst_null
areturn
L7:
iload 2
ineg
i2l
lstore 4
iload 3
istore 2
L8:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L9
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
bipush 48
isub
istore 3
iload 3
iflt L10
iload 3
bipush 9
if_icmpgt L10
lload 4
ldc2_w -922337203685477580L
lcmp
ifge L11
L10:
aconst_null
areturn
L11:
lload 4
ldc2_w 10L
lmul
lstore 4
lload 4
iload 3
i2l
ldc2_w -9223372036854775808L
lsub
lcmp
ifge L12
aconst_null
areturn
L12:
lload 4
iload 3
i2l
lsub
lstore 4
iload 2
iconst_1
iadd
istore 2
goto L8
L9:
iload 1
ifeq L13
lload 4
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
L13:
lload 4
ldc2_w -9223372036854775808L
lcmp
ifne L14
aconst_null
areturn
L14:
lload 4
lneg
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 6
.limit stack 6
.end method

.method private static transient a(Ljava/lang/String;[J)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
bipush 10
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
aload 1
iconst_0
laload
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
laload
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a([JJ)Z
iconst_0
istore 6
aload 0
arraylength
istore 4
iconst_0
istore 3
L0:
iload 6
istore 5
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
laload
lload 1
lcmp
ifne L2
iconst_1
istore 5
L1:
iload 5
ireturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
.limit locals 7
.limit stack 4
.end method

.method private static a(Ljava/util/Collection;)[J
iconst_0
istore 1
aload 0
instanceof com/a/b/l/w
ifeq L0
aload 0
checkcast com/a/b/l/w
astore 0
aload 0
invokevirtual com/a/b/l/w/size()I
istore 1
iload 1
newarray long
astore 3
aload 0
getfield com/a/b/l/w/a [J
aload 0
getfield com/a/b/l/w/b I
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
L0:
aload 0
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 0
aload 0
arraylength
istore 2
iload 2
newarray long
astore 3
L1:
iload 1
iload 2
if_icmpge L2
aload 3
iload 1
aload 0
iload 1
aaload
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Number
invokevirtual java/lang/Number/longValue()J
lastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([JI)[J
iload 1
newarray long
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a([JII)[J
iload 1
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "Invalid minLength: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iflt L2
iconst_1
istore 3
L3:
iload 3
ldc "Invalid padding: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
astore 4
aload 0
arraylength
iload 1
if_icmpge L4
iload 1
iload 2
iadd
istore 1
iload 1
newarray long
astore 4
aload 0
iconst_0
aload 4
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 4
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 6
.end method

.method private static transient a([[J)[J
aload 0
arraylength
istore 3
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 3
if_icmpge L1
iload 2
aload 0
iload 1
aaload
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
newarray long
astore 4
aload 0
arraylength
istore 3
iconst_0
istore 2
iconst_0
istore 1
L2:
iload 1
iload 3
if_icmpge L3
aload 0
iload 1
aaload
astore 5
aload 5
iconst_0
aload 4
iload 2
aload 5
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
aload 5
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method private static b([JJ)I
aload 0
lload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/u/c([JJII)I
ireturn
.limit locals 3
.limit stack 5
.end method

.method static synthetic b([JJII)I
aload 0
lload 1
iload 3
iload 4
invokestatic com/a/b/l/u/c([JJII)I
ireturn
.limit locals 5
.limit stack 5
.end method

.method private static transient b([J)J
iconst_1
istore 1
aload 0
arraylength
ifle L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
laload
lstore 3
L2:
iload 1
aload 0
arraylength
if_icmpge L3
lload 3
lstore 5
aload 0
iload 1
laload
lload 3
lcmp
ifle L4
aload 0
iload 1
laload
lstore 5
L4:
iload 1
iconst_1
iadd
istore 1
lload 5
lstore 3
goto L2
L0:
iconst_0
istore 2
goto L1
L3:
lload 3
lreturn
.limit locals 7
.limit stack 4
.end method

.method private static b()Ljava/util/Comparator;
getstatic com/a/b/l/v/a Lcom/a/b/l/v;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(J)[B
bipush 8
newarray byte
astore 3
bipush 7
istore 2
L0:
iload 2
iflt L1
aload 3
iload 2
ldc2_w 255L
lload 0
land
l2i
i2b
bastore
lload 0
bipush 8
lshr
lstore 0
iload 2
iconst_1
isub
istore 2
goto L0
L1:
aload 3
areturn
.limit locals 4
.limit stack 6
.end method

.method private static c([JJ)I
aload 0
lload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/u/a([JJII)I
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static c([JJII)I
L0:
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
laload
lload 1
lcmp
ifne L2
iload 3
ireturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
iconst_m1
ireturn
.limit locals 5
.limit stack 4
.end method

.method private static transient c([J)Ljava/util/List;
aload 0
arraylength
ifne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/w
dup
aload 0
invokespecial com/a/b/l/w/<init>([J)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static synthetic d([JJII)I
aload 0
lload 1
iload 3
iload 4
invokestatic com/a/b/l/u/a([JJII)I
ireturn
.limit locals 5
.limit stack 5
.end method
