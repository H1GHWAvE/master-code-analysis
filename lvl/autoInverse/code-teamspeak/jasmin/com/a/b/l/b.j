.bytecode 50.0
.class final synchronized com/a/b/l/b
.super java/util/AbstractList
.implements java/io/Serializable
.implements java/util/RandomAccess
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private static final 'd' J = 0L


.field final 'a' [Z

.field final 'b' I

.field final 'c' I

.method <init>([Z)V
aload 0
aload 1
iconst_0
aload 1
arraylength
invokespecial com/a/b/l/b/<init>([ZII)V
return
.limit locals 2
.limit stack 4
.end method

.method private <init>([ZII)V
aload 0
invokespecial java/util/AbstractList/<init>()V
aload 0
aload 1
putfield com/a/b/l/b/a [Z
aload 0
iload 2
putfield com/a/b/l/b/b I
aload 0
iload 3
putfield com/a/b/l/b/c I
return
.limit locals 4
.limit stack 2
.end method

.method private a(I)Ljava/lang/Boolean;
iload 1
aload 0
invokevirtual com/a/b/l/b/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
iload 1
iadd
baload
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(ILjava/lang/Boolean;)Ljava/lang/Boolean;
iload 1
aload 0
invokevirtual com/a/b/l/b/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
iload 1
iadd
baload
istore 3
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
iload 1
iadd
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
bastore
iload 3
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 4
.limit stack 3
.end method

.method private a()[Z
aload 0
invokevirtual com/a/b/l/b/size()I
istore 1
iload 1
newarray boolean
astore 2
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 1
instanceof java/lang/Boolean
ifeq L0
aload 0
getfield com/a/b/l/b/a [Z
aload 1
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
aload 0
getfield com/a/b/l/b/b I
aload 0
getfield com/a/b/l/b/c I
invokestatic com/a/b/l/a/a([ZZII)I
iconst_m1
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
aload 0
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/l/b
ifeq L2
aload 1
checkcast com/a/b/l/b
astore 1
aload 0
invokevirtual com/a/b/l/b/size()I
istore 3
aload 1
invokevirtual com/a/b/l/b/size()I
iload 3
if_icmpeq L3
iconst_0
ireturn
L3:
iconst_0
istore 2
L4:
iload 2
iload 3
if_icmpge L1
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
iload 2
iadd
baload
aload 1
getfield com/a/b/l/b/a [Z
aload 1
getfield com/a/b/l/b/b I
iload 2
iadd
baload
if_icmpeq L5
iconst_0
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L4
L2:
aload 0
aload 1
invokespecial java/util/AbstractList/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final synthetic get(I)Ljava/lang/Object;
iload 1
aload 0
invokevirtual com/a/b/l/b/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
iload 1
iadd
baload
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final hashCode()I
iconst_1
istore 2
aload 0
getfield com/a/b/l/b/b I
istore 1
L0:
iload 1
aload 0
getfield com/a/b/l/b/c I
if_icmpge L1
aload 0
getfield com/a/b/l/b/a [Z
iload 1
baload
ifeq L2
sipush 1231
istore 3
L3:
iload 3
iload 2
bipush 31
imul
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L2:
sipush 1237
istore 3
goto L3
L1:
iload 2
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final indexOf(Ljava/lang/Object;)I
aload 1
instanceof java/lang/Boolean
ifeq L0
aload 0
getfield com/a/b/l/b/a [Z
aload 1
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
aload 0
getfield com/a/b/l/b/b I
aload 0
getfield com/a/b/l/b/c I
invokestatic com/a/b/l/a/a([ZZII)I
istore 2
iload 2
iflt L0
iload 2
aload 0
getfield com/a/b/l/b/b I
isub
ireturn
L0:
iconst_m1
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final isEmpty()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
aload 1
instanceof java/lang/Boolean
ifeq L0
aload 0
getfield com/a/b/l/b/a [Z
aload 1
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
aload 0
getfield com/a/b/l/b/b I
aload 0
getfield com/a/b/l/b/c I
invokestatic com/a/b/l/a/b([ZZII)I
istore 2
iload 2
iflt L0
iload 2
aload 0
getfield com/a/b/l/b/b I
isub
ireturn
L0:
iconst_m1
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
aload 2
checkcast java/lang/Boolean
astore 2
iload 1
aload 0
invokevirtual com/a/b/l/b/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
iload 1
iadd
baload
istore 3
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
iload 1
iadd
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
bastore
iload 3
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 4
.limit stack 3
.end method

.method public final size()I
aload 0
getfield com/a/b/l/b/c I
aload 0
getfield com/a/b/l/b/b I
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final subList(II)Ljava/util/List;
iload 1
iload 2
aload 0
invokevirtual com/a/b/l/b/size()I
invokestatic com/a/b/b/cn/a(III)V
iload 1
iload 2
if_icmpne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/b
dup
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
iload 1
iadd
aload 0
getfield com/a/b/l/b/b I
iload 2
iadd
invokespecial com/a/b/l/b/<init>([ZII)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
aload 0
invokevirtual com/a/b/l/b/size()I
bipush 7
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
baload
ifeq L0
ldc "[true"
astore 2
L1:
aload 3
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield com/a/b/l/b/b I
iconst_1
iadd
istore 1
L2:
iload 1
aload 0
getfield com/a/b/l/b/c I
if_icmpge L3
aload 0
getfield com/a/b/l/b/a [Z
iload 1
baload
ifeq L4
ldc ", true"
astore 2
L5:
aload 3
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L2
L0:
ldc "[false"
astore 2
goto L1
L4:
ldc ", false"
astore 2
goto L5
L3:
aload 3
bipush 93
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method
