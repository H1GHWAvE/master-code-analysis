.bytecode 50.0
.class final synchronized com/a/b/n/a/dn
.super com/a/b/n/a/cb
.implements com/a/b/n/a/dp

.field private static final 'a' Ljava/util/concurrent/ThreadFactory;

.field private static final 'b' Ljava/util/concurrent/Executor;

.field private final 'c' Ljava/util/concurrent/Executor;

.field private final 'd' Lcom/a/b/n/a/bu;

.field private final 'e' Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final 'f' Ljava/util/concurrent/Future;

.method static <clinit>()V
new com/a/b/n/a/gl
dup
invokespecial com/a/b/n/a/gl/<init>()V
invokevirtual com/a/b/n/a/gl/a()Lcom/a/b/n/a/gl;
astore 0
ldc "ListenableFutureAdapter-thread-%d"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
pop
aload 0
ldc "ListenableFutureAdapter-thread-%d"
putfield com/a/b/n/a/gl/a Ljava/lang/String;
aload 0
invokevirtual com/a/b/n/a/gl/b()Ljava/util/concurrent/ThreadFactory;
astore 0
aload 0
putstatic com/a/b/n/a/dn/a Ljava/util/concurrent/ThreadFactory;
aload 0
invokestatic java/util/concurrent/Executors/newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;
putstatic com/a/b/n/a/dn/b Ljava/util/concurrent/Executor;
return
.limit locals 1
.limit stack 5
.end method

.method <init>(Ljava/util/concurrent/Future;)V
aload 0
aload 1
getstatic com/a/b/n/a/dn/b Ljava/util/concurrent/Executor;
invokespecial com/a/b/n/a/dn/<init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Executor;)V
return
.limit locals 2
.limit stack 3
.end method

.method <init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Executor;)V
aload 0
invokespecial com/a/b/n/a/cb/<init>()V
aload 0
new com/a/b/n/a/bu
dup
invokespecial com/a/b/n/a/bu/<init>()V
putfield com/a/b/n/a/dn/d Lcom/a/b/n/a/bu;
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/a/b/n/a/dn/e Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/Future
putfield com/a/b/n/a/dn/f Ljava/util/concurrent/Future;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/Executor
putfield com/a/b/n/a/dn/c Ljava/util/concurrent/Executor;
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/n/a/dn;)Ljava/util/concurrent/Future;
aload 0
getfield com/a/b/n/a/dn/f Ljava/util/concurrent/Future;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/a/b/n/a/dn;)Lcom/a/b/n/a/bu;
aload 0
getfield com/a/b/n/a/dn/d Lcom/a/b/n/a/bu;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
aload 0
getfield com/a/b/n/a/dn/d Lcom/a/b/n/a/bu;
aload 1
aload 2
invokevirtual com/a/b/n/a/bu/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
aload 0
getfield com/a/b/n/a/dn/e Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifeq L0
aload 0
getfield com/a/b/n/a/dn/f Ljava/util/concurrent/Future;
invokeinterface java/util/concurrent/Future/isDone()Z 0
ifeq L1
aload 0
getfield com/a/b/n/a/dn/d Lcom/a/b/n/a/bu;
invokevirtual com/a/b/n/a/bu/a()V
L0:
return
L1:
aload 0
getfield com/a/b/n/a/dn/c Ljava/util/concurrent/Executor;
new com/a/b/n/a/do
dup
aload 0
invokespecial com/a/b/n/a/do/<init>(Lcom/a/b/n/a/dn;)V
invokeinterface java/util/concurrent/Executor/execute(Ljava/lang/Runnable;)V 1
return
.limit locals 3
.limit stack 4
.end method

.method protected final b()Ljava/util/concurrent/Future;
aload 0
getfield com/a/b/n/a/dn/f Ljava/util/concurrent/Future;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/n/a/dn/f Ljava/util/concurrent/Future;
areturn
.limit locals 1
.limit stack 1
.end method
