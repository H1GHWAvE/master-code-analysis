.bytecode 50.0
.class synchronized abstract com/a/b/n/a/gt
.super java/lang/Object
.implements java/util/concurrent/ExecutorService

.field private final 'a' Ljava/util/concurrent/ExecutorService;

.method protected <init>(Ljava/util/concurrent/ExecutorService;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/ExecutorService
putfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
return
.limit locals 2
.limit stack 2
.end method

.method private final a(Ljava/util/Collection;)Lcom/a/b/d/jl;
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 2
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/concurrent/Callable
invokevirtual com/a/b/n/a/gt/a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Callable;
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
goto L0
L1:
aload 2
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 3
.limit stack 3
.end method

.method protected a(Ljava/lang/Runnable;)Ljava/lang/Runnable;
new com/a/b/n/a/gu
dup
aload 0
aload 0
aload 1
aconst_null
invokestatic java/util/concurrent/Executors/callable(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Callable;
invokevirtual com/a/b/n/a/gt/a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Callable;
invokespecial com/a/b/n/a/gu/<init>(Lcom/a/b/n/a/gt;Ljava/util/concurrent/Callable;)V
areturn
.limit locals 2
.limit stack 6
.end method

.method protected abstract a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Callable;
.end method

.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
lload 1
aload 3
invokeinterface java/util/concurrent/ExecutorService/awaitTermination(JLjava/util/concurrent/TimeUnit;)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final execute(Ljava/lang/Runnable;)V
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
invokevirtual com/a/b/n/a/gt/a(Ljava/lang/Runnable;)Ljava/lang/Runnable;
invokeinterface java/util/concurrent/ExecutorService/execute(Ljava/lang/Runnable;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method public final invokeAll(Ljava/util/Collection;)Ljava/util/List;
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
invokespecial com/a/b/n/a/gt/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
invokeinterface java/util/concurrent/ExecutorService/invokeAll(Ljava/util/Collection;)Ljava/util/List; 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
invokespecial com/a/b/n/a/gt/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
lload 2
aload 4
invokeinterface java/util/concurrent/ExecutorService/invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List; 4
areturn
.limit locals 5
.limit stack 5
.end method

.method public final invokeAny(Ljava/util/Collection;)Ljava/lang/Object;
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
invokespecial com/a/b/n/a/gt/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
invokeinterface java/util/concurrent/ExecutorService/invokeAny(Ljava/util/Collection;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
invokespecial com/a/b/n/a/gt/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
lload 2
aload 4
invokeinterface java/util/concurrent/ExecutorService/invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 4
areturn
.limit locals 5
.limit stack 5
.end method

.method public final isShutdown()Z
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/isShutdown()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isTerminated()Z
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/isTerminated()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final shutdown()V
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/shutdown()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final shutdownNow()Ljava/util/List;
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/shutdownNow()Ljava/util/List; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
invokevirtual com/a/b/n/a/gt/a(Ljava/lang/Runnable;)Ljava/lang/Runnable;
invokeinterface java/util/concurrent/ExecutorService/submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future; 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
invokevirtual com/a/b/n/a/gt/a(Ljava/lang/Runnable;)Ljava/lang/Runnable;
aload 2
invokeinterface java/util/concurrent/ExecutorService/submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public final submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
aload 0
getfield com/a/b/n/a/gt/a Ljava/util/concurrent/ExecutorService;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/Callable
invokevirtual com/a/b/n/a/gt/a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Callable;
invokeinterface java/util/concurrent/ExecutorService/submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future; 1
areturn
.limit locals 2
.limit stack 3
.end method
