.bytecode 50.0
.class public final synchronized com/a/b/n/a/fr
.super java/lang/Object
.implements com/a/b/n/a/gn
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Ljava/util/concurrent/ExecutorService;

.method public <init>()V
aload 0
invokestatic java/util/concurrent/Executors/newCachedThreadPool()Ljava/util/concurrent/ExecutorService;
invokespecial com/a/b/n/a/fr/<init>(Ljava/util/concurrent/ExecutorService;)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(Ljava/util/concurrent/ExecutorService;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/ExecutorService
putfield com/a/b/n/a/fr/a Ljava/util/concurrent/ExecutorService;
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic a(Ljava/lang/Exception;)Ljava/lang/Exception;
aload 0
iconst_0
invokestatic com/a/b/n/a/fr/a(Ljava/lang/Exception;Z)Ljava/lang/Exception;
areturn
.limit locals 1
.limit stack 2
.end method

.method static a(Ljava/lang/Exception;Z)Ljava/lang/Exception;
aload 0
invokevirtual java/lang/Exception/getCause()Ljava/lang/Throwable;
astore 2
aload 2
ifnonnull L0
aload 0
athrow
L0:
iload 1
ifeq L1
aload 2
aload 2
invokevirtual java/lang/Throwable/getStackTrace()[Ljava/lang/StackTraceElement;
aload 0
invokevirtual java/lang/Exception/getStackTrace()[Ljava/lang/StackTraceElement;
ldc java/lang/StackTraceElement
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;
checkcast [Ljava/lang/StackTraceElement;
invokevirtual java/lang/Throwable/setStackTrace([Ljava/lang/StackTraceElement;)V
L1:
aload 2
instanceof java/lang/Exception
ifeq L2
aload 2
checkcast java/lang/Exception
athrow
L2:
aload 2
instanceof java/lang/Error
ifeq L3
aload 2
checkcast java/lang/Error
athrow
L3:
aload 0
athrow
.limit locals 3
.limit stack 4
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;
aload 0
aload 0
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
iconst_1
anewarray java/lang/Class
dup
iconst_0
aload 0
aastore
aload 1
invokestatic java/lang/reflect/Proxy/newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 6
.end method

.method private static a(Ljava/lang/Class;)Ljava/util/Set;
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 5
aload 0
invokevirtual java/lang/Class/getMethods()[Ljava/lang/reflect/Method;
astore 0
aload 0
arraylength
istore 3
iconst_0
istore 1
L0:
iload 1
iload 3
if_icmpge L1
aload 0
iload 1
aaload
astore 6
aload 6
invokevirtual java/lang/reflect/Method/getExceptionTypes()[Ljava/lang/Class;
astore 7
aload 7
arraylength
istore 4
iconst_0
istore 2
L2:
iload 2
iload 4
if_icmpge L3
aload 7
iload 2
aaload
ldc java/lang/InterruptedException
if_acmpne L4
iconst_1
istore 2
L5:
iload 2
ifeq L6
aload 5
aload 6
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
L6:
iload 1
iconst_1
iadd
istore 1
goto L0
L4:
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
iconst_0
istore 2
goto L5
L1:
aload 5
areturn
.limit locals 8
.limit stack 2
.end method

.method private static a(Ljava/lang/reflect/Method;)Z
iconst_0
istore 4
aload 0
invokevirtual java/lang/reflect/Method/getExceptionTypes()[Ljava/lang/Class;
astore 0
aload 0
arraylength
istore 2
iconst_0
istore 1
L0:
iload 4
istore 3
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
aaload
ldc java/lang/InterruptedException
if_acmpne L2
iconst_1
istore 3
L1:
iload 3
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 5
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Class;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 5
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
lload 3
lconst_0
lcmp
ifle L0
iconst_1
istore 10
L1:
iload 10
ldc "bad timeout: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 2
invokevirtual java/lang/Class/isInterface()Z
ldc "interfaceType must be an interface type"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 11
aload 2
invokevirtual java/lang/Class/getMethods()[Ljava/lang/reflect/Method;
astore 12
aload 12
arraylength
istore 8
iconst_0
istore 6
L2:
iload 6
iload 8
if_icmpge L3
aload 12
iload 6
aaload
astore 13
aload 13
invokevirtual java/lang/reflect/Method/getExceptionTypes()[Ljava/lang/Class;
astore 14
aload 14
arraylength
istore 9
iconst_0
istore 7
L4:
iload 7
iload 9
if_icmpge L5
aload 14
iload 7
aaload
ldc java/lang/InterruptedException
if_acmpne L6
iconst_1
istore 7
L7:
iload 7
ifeq L8
aload 11
aload 13
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
L8:
iload 6
iconst_1
iadd
istore 6
goto L2
L0:
iconst_0
istore 10
goto L1
L6:
iload 7
iconst_1
iadd
istore 7
goto L4
L5:
iconst_0
istore 7
goto L7
L3:
new com/a/b/n/a/fs
dup
aload 0
aload 1
lload 3
aload 5
aload 11
invokespecial com/a/b/n/a/fs/<init>(Lcom/a/b/n/a/fr;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;Ljava/util/Set;)V
astore 1
aload 2
aload 2
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
iconst_1
anewarray java/lang/Class
dup
iconst_0
aload 2
aastore
aload 1
invokestatic java/lang/reflect/Proxy/newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 15
.limit stack 8
.end method

.method public final a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;Z)Ljava/lang/Object;
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch java/util/concurrent/ExecutionException from L0 to L1 using L3
.catch java/util/concurrent/TimeoutException from L0 to L1 using L4
.catch java/util/concurrent/ExecutionException from L5 to L3 using L3
.catch java/util/concurrent/TimeoutException from L5 to L3 using L4
.catch java/util/concurrent/ExecutionException from L6 to L7 using L3
.catch java/util/concurrent/TimeoutException from L6 to L7 using L4
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
lload 2
lconst_0
lcmp
ifle L8
iconst_1
istore 6
L9:
iload 6
ldc "timeout must be positive: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/n/a/fr/a Ljava/util/concurrent/ExecutorService;
aload 1
invokeinterface java/util/concurrent/ExecutorService/submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future; 1
astore 1
iload 5
ifeq L6
L0:
aload 1
lload 2
aload 4
invokeinterface java/util/concurrent/Future/get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
astore 4
L1:
aload 4
areturn
L8:
iconst_0
istore 6
goto L9
L2:
astore 4
L5:
aload 1
iconst_1
invokeinterface java/util/concurrent/Future/cancel(Z)Z 1
pop
aload 4
athrow
L3:
astore 1
aload 1
iconst_1
invokestatic com/a/b/n/a/fr/a(Ljava/lang/Exception;Z)Ljava/lang/Exception;
athrow
L6:
aload 1
lload 2
aload 4
invokestatic com/a/b/n/a/gs/a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
astore 4
L7:
aload 4
areturn
L4:
astore 4
aload 1
iconst_1
invokeinterface java/util/concurrent/Future/cancel(Z)Z 1
pop
new com/a/b/n/a/gr
dup
aload 4
invokespecial com/a/b/n/a/gr/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 7
.limit stack 7
.end method
