.bytecode 50.0
.class public final synchronized com/a/b/n/a/gl
.super java/lang/Object

.field 'a' Ljava/lang/String;

.field 'b' Ljava/util/concurrent/ThreadFactory;

.field private 'c' Ljava/lang/Boolean;

.field private 'd' Ljava/lang/Integer;

.field private 'e' Ljava/lang/Thread$UncaughtExceptionHandler;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/a/b/n/a/gl/a Ljava/lang/String;
aload 0
aconst_null
putfield com/a/b/n/a/gl/c Ljava/lang/Boolean;
aload 0
aconst_null
putfield com/a/b/n/a/gl/d Ljava/lang/Integer;
aload 0
aconst_null
putfield com/a/b/n/a/gl/e Ljava/lang/Thread$UncaughtExceptionHandler;
aload 0
aconst_null
putfield com/a/b/n/a/gl/b Ljava/util/concurrent/ThreadFactory;
return
.limit locals 1
.limit stack 2
.end method

.method private a(I)Lcom/a/b/n/a/gl;
iload 1
ifle L0
iconst_1
istore 2
L1:
iload 2
ldc "Thread priority (%s) must be >= %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
bipush 10
if_icmpgt L2
iconst_1
istore 2
L3:
iload 2
ldc "Thread priority (%s) must be <= %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
bipush 10
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
putfield com/a/b/n/a/gl/d Ljava/lang/Integer;
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 3
.limit stack 6
.end method

.method private a(Ljava/lang/String;)Lcom/a/b/n/a/gl;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
pop
aload 0
aload 1
putfield com/a/b/n/a/gl/a Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 5
.end method

.method private a(Ljava/lang/Thread$UncaughtExceptionHandler;)Lcom/a/b/n/a/gl;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Thread$UncaughtExceptionHandler
putfield com/a/b/n/a/gl/e Ljava/lang/Thread$UncaughtExceptionHandler;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/concurrent/ThreadFactory;)Lcom/a/b/n/a/gl;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/ThreadFactory
putfield com/a/b/n/a/gl/b Ljava/util/concurrent/ThreadFactory;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Lcom/a/b/n/a/gl;)Ljava/util/concurrent/ThreadFactory;
aload 0
getfield com/a/b/n/a/gl/a Ljava/lang/String;
astore 2
aload 0
getfield com/a/b/n/a/gl/c Ljava/lang/Boolean;
astore 3
aload 0
getfield com/a/b/n/a/gl/d Ljava/lang/Integer;
astore 4
aload 0
getfield com/a/b/n/a/gl/e Ljava/lang/Thread$UncaughtExceptionHandler;
astore 5
aload 0
getfield com/a/b/n/a/gl/b Ljava/util/concurrent/ThreadFactory;
ifnull L0
aload 0
getfield com/a/b/n/a/gl/b Ljava/util/concurrent/ThreadFactory;
astore 0
L1:
aload 2
ifnull L2
new java/util/concurrent/atomic/AtomicLong
dup
lconst_0
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
astore 1
L3:
new com/a/b/n/a/gm
dup
aload 0
aload 2
aload 1
aload 3
aload 4
aload 5
invokespecial com/a/b/n/a/gm/<init>(Ljava/util/concurrent/ThreadFactory;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicLong;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Thread$UncaughtExceptionHandler;)V
areturn
L0:
invokestatic java/util/concurrent/Executors/defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;
astore 0
goto L1
L2:
aconst_null
astore 1
goto L3
.limit locals 6
.limit stack 8
.end method

.method public final a()Lcom/a/b/n/a/gl;
aload 0
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putfield com/a/b/n/a/gl/c Ljava/lang/Boolean;
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method public final b()Ljava/util/concurrent/ThreadFactory;
aload 0
getfield com/a/b/n/a/gl/a Ljava/lang/String;
astore 3
aload 0
getfield com/a/b/n/a/gl/c Ljava/lang/Boolean;
astore 4
aload 0
getfield com/a/b/n/a/gl/d Ljava/lang/Integer;
astore 5
aload 0
getfield com/a/b/n/a/gl/e Ljava/lang/Thread$UncaughtExceptionHandler;
astore 6
aload 0
getfield com/a/b/n/a/gl/b Ljava/util/concurrent/ThreadFactory;
ifnull L0
aload 0
getfield com/a/b/n/a/gl/b Ljava/util/concurrent/ThreadFactory;
astore 1
L1:
aload 3
ifnull L2
new java/util/concurrent/atomic/AtomicLong
dup
lconst_0
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
astore 2
L3:
new com/a/b/n/a/gm
dup
aload 1
aload 3
aload 2
aload 4
aload 5
aload 6
invokespecial com/a/b/n/a/gm/<init>(Ljava/util/concurrent/ThreadFactory;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicLong;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Thread$UncaughtExceptionHandler;)V
areturn
L0:
invokestatic java/util/concurrent/Executors/defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;
astore 1
goto L1
L2:
aconst_null
astore 2
goto L3
.limit locals 7
.limit stack 8
.end method
