.bytecode 50.0
.class public synchronized abstract com/a/b/n/a/bx
.super com/a/b/d/hh
.implements java/util/concurrent/BlockingQueue

.method protected <init>()V
aload 0
invokespecial com/a/b/d/hh/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic a()Ljava/util/Queue;
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected abstract c()Ljava/util/concurrent/BlockingQueue;
.end method

.method public drainTo(Ljava/util/Collection;)I
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
aload 1
invokeinterface java/util/concurrent/BlockingQueue/drainTo(Ljava/util/Collection;)I 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public drainTo(Ljava/util/Collection;I)I
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
aload 1
iload 2
invokeinterface java/util/concurrent/BlockingQueue/drainTo(Ljava/util/Collection;I)I 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected final synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
areturn
.limit locals 1
.limit stack 1
.end method

.method public offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
aload 1
lload 2
aload 4
invokeinterface java/util/concurrent/BlockingQueue/offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z 4
ireturn
.limit locals 5
.limit stack 5
.end method

.method public poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
lload 1
aload 3
invokeinterface java/util/concurrent/BlockingQueue/poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public put(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
aload 1
invokeinterface java/util/concurrent/BlockingQueue/put(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public remainingCapacity()I
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
invokeinterface java/util/concurrent/BlockingQueue/remainingCapacity()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public take()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/bx/c()Ljava/util/concurrent/BlockingQueue;
invokeinterface java/util/concurrent/BlockingQueue/take()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method
