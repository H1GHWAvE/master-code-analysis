.bytecode 50.0
.class public final synchronized com/a/b/n/a/dy
.super java/lang/Object

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/a/b/n/a/du;Ljava/util/concurrent/Callable;Ljava/util/concurrent/BlockingQueue;)Lcom/a/b/n/a/dp;
aload 0
aload 1
invokeinterface com/a/b/n/a/du/a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp; 1
astore 0
aload 0
new com/a/b/n/a/dz
dup
aload 2
aload 0
invokespecial com/a/b/n/a/dz/<init>(Ljava/util/concurrent/BlockingQueue;Lcom/a/b/n/a/dp;)V
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(Ljava/util/concurrent/ExecutorService;)Lcom/a/b/n/a/du;
aload 0
instanceof com/a/b/n/a/du
ifeq L0
aload 0
checkcast com/a/b/n/a/du
areturn
L0:
aload 0
instanceof java/util/concurrent/ScheduledExecutorService
ifeq L1
new com/a/b/n/a/ei
dup
aload 0
checkcast java/util/concurrent/ScheduledExecutorService
invokespecial com/a/b/n/a/ei/<init>(Ljava/util/concurrent/ScheduledExecutorService;)V
areturn
L1:
new com/a/b/n/a/eh
dup
aload 0
invokespecial com/a/b/n/a/eh/<init>(Ljava/util/concurrent/ExecutorService;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/concurrent/ScheduledExecutorService;)Lcom/a/b/n/a/dv;
aload 0
instanceof com/a/b/n/a/dv
ifeq L0
aload 0
checkcast com/a/b/n/a/dv
areturn
L0:
new com/a/b/n/a/ei
dup
aload 0
invokespecial com/a/b/n/a/ei/<init>(Ljava/util/concurrent/ScheduledExecutorService;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/n/a/du;Ljava/util/Collection;ZJ)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch java/util/concurrent/ExecutionException from L8 to L9 using L10
.catch java/lang/RuntimeException from L8 to L9 using L11
.catch all from L8 to L9 using L2
.catch all from L12 to L13 using L2
.catch all from L14 to L2 using L2
.catch all from L15 to L16 using L2
.catch all from L17 to L18 using L2
.catch all from L19 to L20 using L2
.catch all from L21 to L22 using L2
.catch all from L22 to L23 using L2
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokeinterface java/util/Collection/size()I 0
istore 5
iload 5
ifle L24
iconst_1
istore 8
L25:
iload 8
invokestatic com/a/b/b/cn/a(Z)V
iload 5
invokestatic com/a/b/d/ov/a(I)Ljava/util/ArrayList;
astore 14
new java/util/concurrent/LinkedBlockingQueue
dup
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>()V
astore 15
aconst_null
astore 13
iload 2
ifeq L26
L0:
invokestatic java/lang/System/nanoTime()J
lstore 9
L1:
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 16
aload 14
aload 0
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/concurrent/Callable
aload 15
invokestatic com/a/b/n/a/dy/a(Lcom/a/b/n/a/du;Ljava/util/concurrent/Callable;Ljava/util/concurrent/BlockingQueue;)Lcom/a/b/n/a/dp;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L3:
iload 5
iconst_1
isub
istore 6
iconst_1
istore 5
aload 13
astore 1
L4:
aload 15
invokeinterface java/util/concurrent/BlockingQueue/poll()Ljava/lang/Object; 0
checkcast java/util/concurrent/Future
astore 13
L5:
aload 13
ifnonnull L27
iload 6
ifle L28
L6:
aload 14
aload 0
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/concurrent/Callable
aload 15
invokestatic com/a/b/n/a/dy/a(Lcom/a/b/n/a/du;Ljava/util/concurrent/Callable;Ljava/util/concurrent/BlockingQueue;)Lcom/a/b/n/a/dp;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L7:
iload 6
iconst_1
isub
istore 6
iload 5
iconst_1
iadd
istore 7
iload 6
istore 5
iload 7
istore 6
L29:
aload 13
ifnull L30
iload 6
iconst_1
isub
istore 6
L8:
aload 13
invokeinterface java/util/concurrent/Future/get()Ljava/lang/Object; 0
astore 1
L9:
aload 14
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 0
L31:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L32
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/concurrent/Future
iconst_1
invokeinterface java/util/concurrent/Future/cancel(Z)Z 1
pop
goto L31
L24:
iconst_0
istore 8
goto L25
L26:
lconst_0
lstore 9
goto L1
L28:
iload 5
ifeq L33
iload 2
ifeq L17
L12:
aload 15
lload 3
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokeinterface java/util/concurrent/BlockingQueue/poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
checkcast java/util/concurrent/Future
astore 13
L13:
aload 13
ifnonnull L15
L14:
new java/util/concurrent/TimeoutException
dup
invokespecial java/util/concurrent/TimeoutException/<init>()V
athrow
L2:
astore 0
aload 14
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L34:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L23
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/concurrent/Future
iconst_1
invokeinterface java/util/concurrent/Future/cancel(Z)Z 1
pop
goto L34
L15:
invokestatic java/lang/System/nanoTime()J
lstore 11
L16:
iload 6
istore 7
lload 3
lload 11
lload 9
lsub
lsub
lstore 3
lload 11
lstore 9
iload 5
istore 6
iload 7
istore 5
goto L29
L17:
aload 15
invokeinterface java/util/concurrent/BlockingQueue/take()Ljava/lang/Object; 0
checkcast java/util/concurrent/Future
astore 13
L18:
iload 6
istore 7
iload 5
istore 6
iload 7
istore 5
goto L29
L11:
astore 1
L19:
new java/util/concurrent/ExecutionException
dup
aload 1
invokespecial java/util/concurrent/ExecutionException/<init>(Ljava/lang/Throwable;)V
astore 1
L20:
goto L35
L33:
aload 1
astore 0
aload 1
ifnonnull L22
L21:
new java/util/concurrent/ExecutionException
dup
aconst_null
invokespecial java/util/concurrent/ExecutionException/<init>(Ljava/lang/Throwable;)V
astore 0
L22:
aload 0
athrow
L23:
aload 0
athrow
L32:
aload 1
areturn
L30:
goto L35
L27:
iload 6
istore 7
iload 5
istore 6
iload 7
istore 5
goto L29
L10:
astore 1
iload 6
istore 7
iload 5
istore 6
iload 7
istore 5
goto L4
L35:
iload 6
istore 7
iload 5
istore 6
iload 7
istore 5
goto L4
.limit locals 17
.limit stack 6
.end method

.method static a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;
.catch java/lang/SecurityException from L0 to L1 using L2
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/n/a/dy/e()Ljava/util/concurrent/ThreadFactory;
aload 1
invokeinterface java/util/concurrent/ThreadFactory/newThread(Ljava/lang/Runnable;)Ljava/lang/Thread; 1
astore 1
L0:
aload 1
aload 0
invokevirtual java/lang/Thread/setName(Ljava/lang/String;)V
L1:
aload 1
areturn
L2:
astore 0
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static a(Ljava/util/concurrent/Executor;Lcom/a/b/b/dz;)Ljava/util/concurrent/Executor;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/n/a/dy/a()Z
ifeq L0
aload 0
areturn
L0:
new com/a/b/n/a/ea
dup
aload 0
aload 1
invokespecial com/a/b/n/a/ea/<init>(Ljava/util/concurrent/Executor;Lcom/a/b/b/dz;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/concurrent/ExecutorService;Lcom/a/b/b/dz;)Ljava/util/concurrent/ExecutorService;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/n/a/dy/a()Z
ifeq L0
aload 0
areturn
L0:
new com/a/b/n/a/eb
dup
aload 0
aload 1
invokespecial com/a/b/n/a/eb/<init>(Ljava/util/concurrent/ExecutorService;Lcom/a/b/b/dz;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/concurrent/ThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ExecutorService;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/n/a/ed
dup
invokespecial com/a/b/n/a/ed/<init>()V
aload 0
lload 1
aload 3
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ExecutorService;
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a(Ljava/util/concurrent/ScheduledExecutorService;Lcom/a/b/b/dz;)Ljava/util/concurrent/ScheduledExecutorService;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/n/a/dy/a()Z
ifeq L0
aload 0
areturn
L0:
new com/a/b/n/a/ec
dup
aload 0
aload 1
invokespecial com/a/b/n/a/ec/<init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/a/b/b/dz;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;)Ljava/util/concurrent/ScheduledExecutorService;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/n/a/ed
dup
invokespecial com/a/b/n/a/ed/<init>()V
aload 0
ldc2_w 120L
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledExecutorService;
areturn
.limit locals 1
.limit stack 5
.end method

.method private static a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledExecutorService;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/n/a/ed
dup
invokespecial com/a/b/n/a/ed/<init>()V
aload 0
lload 1
aload 3
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledExecutorService;
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)V
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/n/a/ed
dup
invokespecial com/a/b/n/a/ed/<init>()V
aload 0
lload 1
aload 3
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)V
return
.limit locals 4
.limit stack 5
.end method

.method static synthetic a(Ljava/util/concurrent/ThreadPoolExecutor;)V
new com/a/b/n/a/gl
dup
invokespecial com/a/b/n/a/gl/<init>()V
invokevirtual com/a/b/n/a/gl/a()Lcom/a/b/n/a/gl;
astore 1
aload 1
aload 0
invokevirtual java/util/concurrent/ThreadPoolExecutor/getThreadFactory()Ljava/util/concurrent/ThreadFactory;
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/ThreadFactory
putfield com/a/b/n/a/gl/b Ljava/util/concurrent/ThreadFactory;
aload 0
aload 1
invokevirtual com/a/b/n/a/gl/b()Ljava/util/concurrent/ThreadFactory;
invokevirtual java/util/concurrent/ThreadPoolExecutor/setThreadFactory(Ljava/util/concurrent/ThreadFactory;)V
return
.limit locals 2
.limit stack 2
.end method

.method static a()Z
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L3
.catch java/lang/IllegalAccessException from L0 to L1 using L4
.catch java/lang/NoSuchMethodException from L0 to L1 using L5
ldc "com.google.appengine.runtime.environment"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
ifnonnull L0
L6:
iconst_0
ireturn
L0:
ldc "com.google.apphosting.api.ApiProxy"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
ldc "getCurrentEnvironment"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aconst_null
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
astore 0
L1:
aload 0
ifnull L6
iconst_1
ireturn
L5:
astore 0
iconst_0
ireturn
L4:
astore 0
iconst_0
ireturn
L3:
astore 0
iconst_0
ireturn
L2:
astore 0
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method private static b()Lcom/a/b/n/a/du;
.annotation visible Ljava/lang/Deprecated;
.end annotation
new com/a/b/n/a/eg
dup
iconst_0
invokespecial com/a/b/n/a/eg/<init>(B)V
areturn
.limit locals 0
.limit stack 3
.end method

.method private static b(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ExecutorService;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/n/a/ed
dup
invokespecial com/a/b/n/a/ed/<init>()V
aload 0
ldc2_w 120L
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/n/a/ed/a(Ljava/util/concurrent/ThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ExecutorService;
areturn
.limit locals 1
.limit stack 5
.end method

.method private static b(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)Z
.annotation invisible Lcom/a/b/a/a;
.end annotation
.catch java/lang/InterruptedException from L0 to L1 using L2
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokeinterface java/util/concurrent/ExecutorService/shutdown()V 0
L0:
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
lload 1
aload 3
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
ldc2_w 2L
ldiv
lstore 1
aload 0
lload 1
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokeinterface java/util/concurrent/ExecutorService/awaitTermination(JLjava/util/concurrent/TimeUnit;)Z 3
ifne L1
aload 0
invokeinterface java/util/concurrent/ExecutorService/shutdownNow()Ljava/util/List; 0
pop
aload 0
lload 1
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokeinterface java/util/concurrent/ExecutorService/awaitTermination(JLjava/util/concurrent/TimeUnit;)Z 3
pop
L1:
aload 0
invokeinterface java/util/concurrent/ExecutorService/isTerminated()Z 0
ireturn
L2:
astore 3
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
aload 0
invokeinterface java/util/concurrent/ExecutorService/shutdownNow()Ljava/util/List; 0
pop
goto L1
.limit locals 4
.limit stack 4
.end method

.method private static c()Lcom/a/b/n/a/du;
new com/a/b/n/a/eg
dup
iconst_0
invokespecial com/a/b/n/a/eg/<init>(B)V
areturn
.limit locals 0
.limit stack 3
.end method

.method private static c(Ljava/util/concurrent/ThreadPoolExecutor;)V
new com/a/b/n/a/gl
dup
invokespecial com/a/b/n/a/gl/<init>()V
invokevirtual com/a/b/n/a/gl/a()Lcom/a/b/n/a/gl;
astore 1
aload 1
aload 0
invokevirtual java/util/concurrent/ThreadPoolExecutor/getThreadFactory()Ljava/util/concurrent/ThreadFactory;
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/ThreadFactory
putfield com/a/b/n/a/gl/b Ljava/util/concurrent/ThreadFactory;
aload 0
aload 1
invokevirtual com/a/b/n/a/gl/b()Ljava/util/concurrent/ThreadFactory;
invokevirtual java/util/concurrent/ThreadPoolExecutor/setThreadFactory(Ljava/util/concurrent/ThreadFactory;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static d()Ljava/util/concurrent/Executor;
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static e()Ljava/util/concurrent/ThreadFactory;
.annotation invisible Lcom/a/b/a/a;
.end annotation
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/ClassNotFoundException from L0 to L1 using L3
.catch java/lang/NoSuchMethodException from L0 to L1 using L4
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L5
invokestatic com/a/b/n/a/dy/a()Z
ifne L0
invokestatic java/util/concurrent/Executors/defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;
areturn
L0:
ldc "com.google.appengine.api.ThreadManager"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
ldc "currentRequestThreadFactory"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aconst_null
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/ThreadFactory
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/RuntimeException
dup
ldc "Couldn't invoke ThreadManager.currentRequestThreadFactory"
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L3:
astore 0
new java/lang/RuntimeException
dup
ldc "Couldn't invoke ThreadManager.currentRequestThreadFactory"
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L4:
astore 0
new java/lang/RuntimeException
dup
ldc "Couldn't invoke ThreadManager.currentRequestThreadFactory"
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L5:
astore 0
aload 0
invokevirtual java/lang/reflect/InvocationTargetException/getCause()Ljava/lang/Throwable;
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
.limit locals 1
.limit stack 4
.end method
