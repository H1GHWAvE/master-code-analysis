.bytecode 50.0
.class final synchronized com/a/b/n/a/h
.super java/util/concurrent/locks/AbstractQueuedSynchronizer

.field static final 'a' I = 0


.field static final 'b' I = 1


.field static final 'c' I = 2


.field static final 'd' I = 4


.field static final 'e' I = 8


.field private static final 'f' J = 0L


.field private 'g' Ljava/lang/Object;

.field private 'h' Ljava/lang/Throwable;

.method <init>()V
aload 0
invokespecial java/util/concurrent/locks/AbstractQueuedSynchronizer/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(J)Ljava/lang/Object;
aload 0
iconst_m1
lload 1
invokevirtual com/a/b/n/a/h/tryAcquireSharedNanos(IJ)Z
ifne L0
new java/util/concurrent/TimeoutException
dup
ldc "Timeout waiting for task."
invokespecial java/util/concurrent/TimeoutException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual com/a/b/n/a/h/a()Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aconst_null
iconst_2
invokevirtual com/a/b/n/a/h/a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
ireturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/lang/Throwable;)Z
aload 0
aconst_null
aload 1
iconst_2
invokevirtual com/a/b/n/a/h/a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
ireturn
.limit locals 2
.limit stack 4
.end method

.method private a(Z)Z
iload 1
ifeq L0
bipush 8
istore 2
L1:
aload 0
aconst_null
aconst_null
iload 2
invokevirtual com/a/b/n/a/h/a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
ireturn
L0:
iconst_4
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method private e()Ljava/lang/Object;
aload 0
iconst_m1
invokevirtual com/a/b/n/a/h/acquireSharedInterruptibly(I)V
aload 0
invokevirtual com/a/b/n/a/h/a()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method final a()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/h/getState()I
istore 1
iload 1
lookupswitch
2 : L0
4 : L1
8 : L1
default : L2
L2:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
bipush 49
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Error, synchronizer in invalid state: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield com/a/b/n/a/h/h Ljava/lang/Throwable;
ifnull L3
new java/util/concurrent/ExecutionException
dup
aload 0
getfield com/a/b/n/a/h/h Ljava/lang/Throwable;
invokespecial java/util/concurrent/ExecutionException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
aload 0
getfield com/a/b/n/a/h/g Ljava/lang/Object;
areturn
L1:
ldc "Task was cancelled."
aload 0
getfield com/a/b/n/a/h/h Ljava/lang/Throwable;
invokestatic com/a/b/n/a/g/a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;
athrow
.limit locals 2
.limit stack 5
.end method

.method final a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
iconst_0
iconst_1
invokevirtual com/a/b/n/a/h/compareAndSetState(II)Z
istore 4
iload 4
ifeq L0
aload 0
aload 1
putfield com/a/b/n/a/h/g Ljava/lang/Object;
iload 3
bipush 12
iand
ifeq L1
new java/util/concurrent/CancellationException
dup
ldc "Future.cancel() was called."
invokespecial java/util/concurrent/CancellationException/<init>(Ljava/lang/String;)V
astore 2
L1:
aload 0
aload 2
putfield com/a/b/n/a/h/h Ljava/lang/Throwable;
aload 0
iload 3
invokevirtual com/a/b/n/a/h/releaseShared(I)Z
pop
L2:
iload 4
ireturn
L0:
aload 0
invokevirtual com/a/b/n/a/h/getState()I
iconst_1
if_icmpne L2
aload 0
iconst_m1
invokevirtual com/a/b/n/a/h/acquireShared(I)V
iload 4
ireturn
.limit locals 5
.limit stack 3
.end method

.method final b()Z
aload 0
invokevirtual com/a/b/n/a/h/getState()I
bipush 14
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method final c()Z
aload 0
invokevirtual com/a/b/n/a/h/getState()I
bipush 12
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method final d()Z
aload 0
invokevirtual com/a/b/n/a/h/getState()I
bipush 8
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method protected final tryAcquireShared(I)I
aload 0
invokevirtual com/a/b/n/a/h/b()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_m1
ireturn
.limit locals 2
.limit stack 1
.end method

.method protected final tryReleaseShared(I)Z
aload 0
iload 1
invokevirtual com/a/b/n/a/h/setState(I)V
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method
