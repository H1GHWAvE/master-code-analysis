.bytecode 50.0
.class synchronized com/a/b/n/a/cu
.super com/a/b/n/a/g

.field private static final 'i' Ljava/util/logging/Logger;

.field 'b' Lcom/a/b/d/iz;

.field final 'c' Z

.field final 'd' Ljava/util/concurrent/atomic/AtomicInteger;

.field 'e' Lcom/a/b/n/a/db;

.field 'f' Ljava/util/List;

.field final 'g' Ljava/lang/Object;

.field 'h' Ljava/util/Set;

.method static <clinit>()V
ldc com/a/b/n/a/cu
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/n/a/cu/i Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 1
.end method

.method <init>(Lcom/a/b/d/iz;ZLjava/util/concurrent/Executor;Lcom/a/b/n/a/db;)V
aload 0
invokespecial com/a/b/n/a/g/<init>()V
aload 0
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putfield com/a/b/n/a/cu/g Ljava/lang/Object;
aload 0
aload 1
putfield com/a/b/n/a/cu/b Lcom/a/b/d/iz;
aload 0
iload 2
putfield com/a/b/n/a/cu/c Z
aload 0
new java/util/concurrent/atomic/AtomicInteger
dup
aload 1
invokevirtual com/a/b/d/iz/size()I
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>(I)V
putfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
aload 0
aload 4
putfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
aload 0
aload 1
invokevirtual com/a/b/d/iz/size()I
invokestatic com/a/b/d/ov/a(I)Ljava/util/ArrayList;
putfield com/a/b/n/a/cu/f Ljava/util/List;
aload 0
aload 3
invokespecial com/a/b/n/a/cu/a(Ljava/util/concurrent/Executor;)V
return
.limit locals 5
.limit stack 4
.end method

.method private a(ILjava/util/concurrent/Future;)V
.catch java/util/concurrent/CancellationException from L0 to L1 using L2
.catch java/util/concurrent/ExecutionException from L0 to L1 using L3
.catch java/lang/Throwable from L0 to L1 using L4
.catch all from L0 to L1 using L5
.catch java/util/concurrent/CancellationException from L6 to L7 using L2
.catch java/util/concurrent/ExecutionException from L6 to L7 using L3
.catch java/lang/Throwable from L6 to L7 using L4
.catch all from L6 to L7 using L5
.catch all from L8 to L9 using L5
.catch all from L10 to L11 using L5
.catch all from L12 to L13 using L5
iconst_1
istore 5
iconst_1
istore 6
iconst_1
istore 7
iconst_1
istore 8
iconst_1
istore 4
aload 0
getfield com/a/b/n/a/cu/f Ljava/util/List;
astore 9
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
ifne L14
aload 9
ifnonnull L0
L14:
aload 0
getfield com/a/b/n/a/cu/c Z
ifne L15
aload 0
invokevirtual com/a/b/n/a/cu/isCancelled()Z
ifeq L16
L15:
iconst_1
istore 3
L17:
iload 3
ldc "Future was done before all dependencies completed"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
L0:
aload 2
invokeinterface java/util/concurrent/Future/isDone()Z 0
ldc "Tried to set value from future which is not done"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 2
invokestatic com/a/b/n/a/gs/a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
astore 2
L1:
aload 9
ifnull L7
L6:
aload 9
iload 1
aload 2
invokestatic com/a/b/b/ci/c(Ljava/lang/Object;)Lcom/a/b/b/ci;
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
pop
L7:
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L18
iload 4
istore 3
L19:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L20
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 2
aload 2
ifnull L21
aload 9
ifnull L21
aload 0
aload 2
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
L20:
return
L16:
iconst_0
istore 3
goto L17
L18:
iconst_0
istore 3
goto L19
L21:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
return
L2:
astore 2
L8:
aload 0
getfield com/a/b/n/a/cu/c Z
ifeq L9
aload 0
iconst_0
invokevirtual com/a/b/n/a/cu/cancel(Z)Z
pop
L9:
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L22
iload 5
istore 3
L23:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L20
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 2
aload 2
ifnull L24
aload 9
ifnull L24
aload 0
aload 2
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
return
L22:
iconst_0
istore 3
goto L23
L24:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
return
L3:
astore 2
L10:
aload 0
aload 2
invokevirtual java/util/concurrent/ExecutionException/getCause()Ljava/lang/Throwable;
invokespecial com/a/b/n/a/cu/b(Ljava/lang/Throwable;)V
L11:
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L25
iload 6
istore 3
L26:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L20
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 2
aload 2
ifnull L27
aload 9
ifnull L27
aload 0
aload 2
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
return
L25:
iconst_0
istore 3
goto L26
L27:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
return
L4:
astore 2
L12:
aload 0
aload 2
invokespecial com/a/b/n/a/cu/b(Ljava/lang/Throwable;)V
L13:
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L28
iload 7
istore 3
L29:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L20
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 2
aload 2
ifnull L30
aload 9
ifnull L30
aload 0
aload 2
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
return
L28:
iconst_0
istore 3
goto L29
L30:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
return
L5:
astore 2
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L31
iload 8
istore 3
L32:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L33
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 10
aload 10
ifnull L34
aload 9
ifnull L34
aload 0
aload 10
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
L33:
aload 2
athrow
L31:
iconst_0
istore 3
goto L32
L34:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
goto L33
.limit locals 11
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/n/a/cu;ILjava/util/concurrent/Future;)V
.catch java/util/concurrent/CancellationException from L0 to L1 using L2
.catch java/util/concurrent/ExecutionException from L0 to L1 using L3
.catch java/lang/Throwable from L0 to L1 using L4
.catch all from L0 to L1 using L5
.catch java/util/concurrent/CancellationException from L6 to L7 using L2
.catch java/util/concurrent/ExecutionException from L6 to L7 using L3
.catch java/lang/Throwable from L6 to L7 using L4
.catch all from L6 to L7 using L5
.catch all from L8 to L9 using L5
.catch all from L10 to L11 using L5
.catch all from L12 to L13 using L5
iconst_1
istore 5
iconst_1
istore 6
iconst_1
istore 7
iconst_1
istore 8
iconst_1
istore 4
aload 0
getfield com/a/b/n/a/cu/f Ljava/util/List;
astore 9
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
ifne L14
aload 9
ifnonnull L0
L14:
aload 0
getfield com/a/b/n/a/cu/c Z
ifne L15
aload 0
invokevirtual com/a/b/n/a/cu/isCancelled()Z
ifeq L16
L15:
iconst_1
istore 3
L17:
iload 3
ldc "Future was done before all dependencies completed"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
L0:
aload 2
invokeinterface java/util/concurrent/Future/isDone()Z 0
ldc "Tried to set value from future which is not done"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 2
invokestatic com/a/b/n/a/gs/a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
astore 2
L1:
aload 9
ifnull L7
L6:
aload 9
iload 1
aload 2
invokestatic com/a/b/b/ci/c(Ljava/lang/Object;)Lcom/a/b/b/ci;
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
pop
L7:
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L18
iload 4
istore 3
L19:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L20
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 2
aload 2
ifnull L21
aload 9
ifnull L21
aload 0
aload 2
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
L20:
return
L16:
iconst_0
istore 3
goto L17
L18:
iconst_0
istore 3
goto L19
L21:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
return
L2:
astore 2
L8:
aload 0
getfield com/a/b/n/a/cu/c Z
ifeq L9
aload 0
iconst_0
invokevirtual com/a/b/n/a/cu/cancel(Z)Z
pop
L9:
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L22
iload 5
istore 3
L23:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L20
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 2
aload 2
ifnull L24
aload 9
ifnull L24
aload 0
aload 2
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
return
L22:
iconst_0
istore 3
goto L23
L24:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
return
L3:
astore 2
L10:
aload 0
aload 2
invokevirtual java/util/concurrent/ExecutionException/getCause()Ljava/lang/Throwable;
invokespecial com/a/b/n/a/cu/b(Ljava/lang/Throwable;)V
L11:
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L25
iload 6
istore 3
L26:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L20
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 2
aload 2
ifnull L27
aload 9
ifnull L27
aload 0
aload 2
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
return
L25:
iconst_0
istore 3
goto L26
L27:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
return
L4:
astore 2
L12:
aload 0
aload 2
invokespecial com/a/b/n/a/cu/b(Ljava/lang/Throwable;)V
L13:
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L28
iload 7
istore 3
L29:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L20
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 2
aload 2
ifnull L30
aload 9
ifnull L30
aload 0
aload 2
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
return
L28:
iconst_0
istore 3
goto L29
L30:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
return
L5:
astore 2
aload 0
getfield com/a/b/n/a/cu/d Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/decrementAndGet()I
istore 1
iload 1
iflt L31
iload 8
istore 3
L32:
iload 3
ldc "Less than 0 remaining futures"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
iload 1
ifne L33
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
astore 10
aload 10
ifnull L34
aload 9
ifnull L34
aload 0
aload 10
aload 9
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
L33:
aload 2
athrow
L31:
iconst_0
istore 3
goto L32
L34:
aload 0
invokevirtual com/a/b/n/a/cu/isDone()Z
invokestatic com/a/b/b/cn/b(Z)V
goto L33
.limit locals 11
.limit stack 3
.end method

.method private a(Ljava/util/concurrent/Executor;)V
iconst_0
istore 3
aload 0
new com/a/b/n/a/cv
dup
aload 0
invokespecial com/a/b/n/a/cv/<init>(Lcom/a/b/n/a/cu;)V
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
aload 0
getfield com/a/b/n/a/cu/b Lcom/a/b/d/iz;
invokevirtual com/a/b/d/iz/isEmpty()Z
ifeq L0
aload 0
aload 0
getfield com/a/b/n/a/cu/e Lcom/a/b/n/a/db;
invokestatic com/a/b/d/jl/d()Lcom/a/b/d/jl;
invokeinterface com/a/b/n/a/db/a(Ljava/util/List;)Ljava/lang/Object; 1
invokevirtual com/a/b/n/a/cu/a(Ljava/lang/Object;)Z
pop
L1:
return
L0:
iconst_0
istore 2
L2:
iload 2
aload 0
getfield com/a/b/n/a/cu/b Lcom/a/b/d/iz;
invokevirtual com/a/b/d/iz/size()I
if_icmpge L3
aload 0
getfield com/a/b/n/a/cu/f Ljava/util/List;
aconst_null
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 0
getfield com/a/b/n/a/cu/b Lcom/a/b/d/iz;
invokevirtual com/a/b/d/iz/iterator()Ljava/util/Iterator;
astore 4
iload 3
istore 2
L4:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/n/a/dp
astore 5
aload 5
new com/a/b/n/a/cw
dup
aload 0
iload 2
aload 5
invokespecial com/a/b/n/a/cw/<init>(Lcom/a/b/n/a/cu;ILcom/a/b/n/a/dp;)V
aload 1
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
iload 2
iconst_1
iadd
istore 2
goto L4
.limit locals 6
.limit stack 6
.end method

.method private b(Ljava/lang/Throwable;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
iconst_0
istore 3
iconst_1
istore 2
aload 0
getfield com/a/b/n/a/cu/c Z
ifeq L3
aload 0
aload 1
invokespecial com/a/b/n/a/g/a(Ljava/lang/Throwable;)Z
istore 3
aload 0
getfield com/a/b/n/a/cu/g Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
getfield com/a/b/n/a/cu/h Ljava/util/Set;
ifnonnull L1
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield com/a/b/n/a/cu/h Ljava/util/Set;
L1:
aload 0
getfield com/a/b/n/a/cu/h Ljava/util/Set;
aload 1
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
istore 2
aload 4
monitorexit
L3:
aload 1
instanceof java/lang/Error
ifne L6
aload 0
getfield com/a/b/n/a/cu/c Z
ifeq L7
iload 3
ifne L7
iload 2
ifeq L7
L6:
getstatic com/a/b/n/a/cu/i Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "input future failed."
aload 1
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
L7:
return
L2:
astore 1
L4:
aload 4
monitorexit
L5:
aload 1
athrow
.limit locals 5
.limit stack 4
.end method
