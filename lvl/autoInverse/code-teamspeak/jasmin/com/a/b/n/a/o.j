.bytecode 50.0
.class public synchronized abstract com/a/b/n/a/o
.super java/util/concurrent/AbstractExecutorService
.implements com/a/b/n/a/du
.annotation invisible Lcom/a/b/a/a;
.end annotation

.method public <init>()V
aload 0
invokespecial java/util/concurrent/AbstractExecutorService/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;
aload 0
aload 1
invokestatic com/a/b/n/a/dq/a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;
aload 0
invokestatic com/a/b/n/a/dq/a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Runnable;)Lcom/a/b/n/a/dp;
aload 0
aload 1
invokespecial java/util/concurrent/AbstractExecutorService/submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
checkcast com/a/b/n/a/dp
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dp;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial java/util/concurrent/AbstractExecutorService/submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
checkcast com/a/b/n/a/dp
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp;
aload 0
aload 1
invokespecial java/util/concurrent/AbstractExecutorService/submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
checkcast com/a/b/n/a/dp
areturn
.limit locals 2
.limit stack 2
.end method

.method protected synthetic newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
aload 1
aload 2
invokestatic com/a/b/n/a/dq/a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;
areturn
.limit locals 3
.limit stack 2
.end method

.method protected synthetic newTaskFor(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/RunnableFuture;
aload 1
invokestatic com/a/b/n/a/dq/a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;
areturn
.limit locals 2
.limit stack 1
.end method

.method public synthetic submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
aload 0
aload 1
invokevirtual com/a/b/n/a/o/a(Ljava/lang/Runnable;)Lcom/a/b/n/a/dp;
areturn
.limit locals 2
.limit stack 2
.end method

.method public synthetic submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
aload 0
aload 1
aload 2
invokevirtual com/a/b/n/a/o/a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dp;
areturn
.limit locals 3
.limit stack 3
.end method

.method public synthetic submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
aload 0
aload 1
invokevirtual com/a/b/n/a/o/a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp;
areturn
.limit locals 2
.limit stack 2
.end method
