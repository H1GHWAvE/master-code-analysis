.bytecode 50.0
.class public final synchronized com/a/b/n/a/fd
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' Ljava/util/logging/Logger;

.field private static final 'b' Lcom/a/b/n/a/dt;

.field private static final 'c' Lcom/a/b/n/a/dt;

.field private final 'd' Lcom/a/b/n/a/fk;

.field private final 'e' Lcom/a/b/d/jl;

.method static <clinit>()V
ldc com/a/b/n/a/fd
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/n/a/fd/a Ljava/util/logging/Logger;
new com/a/b/n/a/fe
dup
ldc "healthy()"
invokespecial com/a/b/n/a/fe/<init>(Ljava/lang/String;)V
putstatic com/a/b/n/a/fd/b Lcom/a/b/n/a/dt;
new com/a/b/n/a/ff
dup
ldc "stopped()"
invokespecial com/a/b/n/a/ff/<init>(Ljava/lang/String;)V
putstatic com/a/b/n/a/fd/c Lcom/a/b/n/a/dt;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>(Ljava/lang/Iterable;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
invokestatic com/a/b/d/jl/a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
astore 3
aload 3
astore 1
aload 3
invokevirtual com/a/b/d/jl/isEmpty()Z
ifeq L0
getstatic com/a/b/n/a/fd/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "ServiceManager configured with no services.  Is your application configured properly?"
new com/a/b/n/a/fg
dup
iconst_0
invokespecial com/a/b/n/a/fg/<init>(B)V
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
new com/a/b/n/a/fi
dup
iconst_0
invokespecial com/a/b/n/a/fi/<init>(B)V
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;)Lcom/a/b/d/jl;
astore 1
L0:
aload 0
new com/a/b/n/a/fk
dup
aload 1
invokespecial com/a/b/n/a/fk/<init>(Lcom/a/b/d/iz;)V
putfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
aload 0
aload 1
putfield com/a/b/n/a/fd/e Lcom/a/b/d/jl;
new java/lang/ref/WeakReference
dup
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
astore 3
aload 1
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/n/a/et
astore 4
aload 4
new com/a/b/n/a/fj
dup
aload 4
aload 3
invokespecial com/a/b/n/a/fj/<init>(Lcom/a/b/n/a/et;Ljava/lang/ref/WeakReference;)V
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokeinterface com/a/b/n/a/et/a(Lcom/a/b/n/a/ev;Ljava/util/concurrent/Executor;)V 2
aload 4
invokeinterface com/a/b/n/a/et/f()Lcom/a/b/n/a/ew; 0
getstatic com/a/b/n/a/ew/a Lcom/a/b/n/a/ew;
if_acmpne L3
iconst_1
istore 2
L4:
iload 2
ldc "Can only manage NEW services, %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
goto L1
L3:
iconst_0
istore 2
goto L4
L2:
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
invokevirtual com/a/b/n/a/fk/a()V
return
.limit locals 5
.limit stack 6
.end method

.method static synthetic a()Ljava/util/logging/Logger;
getstatic com/a/b/n/a/fd/a Ljava/util/logging/Logger;
areturn
.limit locals 0
.limit stack 1
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)V
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
astore 4
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 4
getfield com/a/b/n/a/fk/h Lcom/a/b/n/a/dx;
lload 1
aload 3
invokevirtual com/a/b/n/a/dw/b(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
ifne L2
ldc "Timeout waiting for the services to become healthy. The following services have not started: "
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 4
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
getstatic com/a/b/n/a/ew/a Lcom/a/b/n/a/ew;
getstatic com/a/b/n/a/ew/b Lcom/a/b/n/a/ew;
invokestatic com/a/b/d/lo/b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/d/we/a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 5
new java/util/concurrent/TimeoutException
dup
new java/lang/StringBuilder
dup
aload 3
invokevirtual java/lang/String/length()I
iconst_0
iadd
aload 5
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/util/concurrent/TimeoutException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 3
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 3
athrow
L2:
aload 4
invokevirtual com/a/b/n/a/fk/d()V
L3:
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
.limit locals 6
.limit stack 6
.end method

.method private a(Lcom/a/b/n/a/fh;)V
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
aload 1
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokevirtual com/a/b/n/a/fk/a(Lcom/a/b/n/a/fh;Ljava/util/concurrent/Executor;)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/a/b/n/a/fh;Ljava/util/concurrent/Executor;)V
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
aload 1
aload 2
invokevirtual com/a/b/n/a/fk/a(Lcom/a/b/n/a/fh;Ljava/util/concurrent/Executor;)V
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic b()Lcom/a/b/n/a/dt;
getstatic com/a/b/n/a/fd/c Lcom/a/b/n/a/dt;
areturn
.limit locals 0
.limit stack 1
.end method

.method private b(JLjava/util/concurrent/TimeUnit;)V
.catch all from L0 to L1 using L1
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
astore 4
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 4
getfield com/a/b/n/a/fk/i Lcom/a/b/n/a/dx;
lload 1
aload 3
invokevirtual com/a/b/n/a/dw/b(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
ifne L2
ldc "Timeout waiting for the services to stop. The following services have not stopped: "
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 4
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
getstatic com/a/b/n/a/ew/e Lcom/a/b/n/a/ew;
getstatic com/a/b/n/a/ew/f Lcom/a/b/n/a/ew;
invokestatic com/a/b/d/lo/b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/we/a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 5
new java/util/concurrent/TimeoutException
dup
new java/lang/StringBuilder
dup
aload 3
invokevirtual java/lang/String/length()I
iconst_0
iadd
aload 5
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/util/concurrent/TimeoutException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 3
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 3
athrow
L2:
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
.limit locals 6
.limit stack 6
.end method

.method static synthetic c()Lcom/a/b/n/a/dt;
getstatic com/a/b/n/a/fd/b Lcom/a/b/n/a/dt;
areturn
.limit locals 0
.limit stack 1
.end method

.method private d()Lcom/a/b/n/a/fd;
.catch java/lang/IllegalStateException from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch java/lang/IllegalStateException from L3 to L5 using L2
.catch java/lang/IllegalStateException from L6 to L7 using L2
aload 0
getfield com/a/b/n/a/fd/e Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 2
L8:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L9
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/n/a/et
astore 3
aload 3
invokeinterface com/a/b/n/a/et/f()Lcom/a/b/n/a/ew; 0
astore 4
aload 4
getstatic com/a/b/n/a/ew/a Lcom/a/b/n/a/ew;
if_acmpne L10
iconst_1
istore 1
L11:
iload 1
ldc "Service %s is %s, cannot start it."
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 3
aastore
dup
iconst_1
aload 4
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
goto L8
L10:
iconst_0
istore 1
goto L11
L9:
aload 0
getfield com/a/b/n/a/fd/e Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 2
L12:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L7
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/n/a/et
astore 3
L0:
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
astore 4
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L1:
aload 4
getfield com/a/b/n/a/fk/d Ljava/util/Map;
aload 3
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/b/dw
ifnonnull L3
aload 4
getfield com/a/b/n/a/fk/d Ljava/util/Map;
aload 3
invokestatic com/a/b/b/dw/a()Lcom/a/b/b/dw;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L3:
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 3
invokeinterface com/a/b/n/a/et/h()Lcom/a/b/n/a/et; 0
pop
L5:
goto L12
L2:
astore 4
getstatic com/a/b/n/a/fd/a Ljava/util/logging/Logger;
astore 5
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
astore 6
aload 3
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 5
aload 6
new java/lang/StringBuilder
dup
aload 3
invokevirtual java/lang/String/length()I
bipush 24
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Unable to start Service "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 4
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
goto L12
L4:
astore 5
L6:
aload 4
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 5
athrow
L7:
aload 0
areturn
.limit locals 7
.limit stack 6
.end method

.method private e()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
astore 1
aload 1
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 1
getfield com/a/b/n/a/fk/h Lcom/a/b/n/a/dx;
invokevirtual com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;)V
L0:
aload 1
invokevirtual com/a/b/n/a/fk/d()V
L1:
aload 1
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
L2:
astore 2
aload 1
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method private f()Lcom/a/b/n/a/fd;
aload 0
getfield com/a/b/n/a/fd/e Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/n/a/et
invokeinterface com/a/b/n/a/et/i()Lcom/a/b/n/a/et; 0
pop
goto L0
L1:
aload 0
areturn
.limit locals 2
.limit stack 1
.end method

.method private g()V
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
astore 1
aload 1
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 1
getfield com/a/b/n/a/fk/i Lcom/a/b/n/a/dx;
invokevirtual com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;)V
aload 1
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
.limit locals 2
.limit stack 2
.end method

.method private h()Z
aload 0
getfield com/a/b/n/a/fd/e Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/n/a/et
invokeinterface com/a/b/n/a/et/e()Z 0
ifne L0
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method

.method private i()Lcom/a/b/d/kk;
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
invokevirtual com/a/b/n/a/fk/b()Lcom/a/b/d/kk;
areturn
.limit locals 1
.limit stack 1
.end method

.method private j()Lcom/a/b/d/jt;
aload 0
getfield com/a/b/n/a/fd/d Lcom/a/b/n/a/fk;
invokevirtual com/a/b/n/a/fk/c()Lcom/a/b/d/jt;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new com/a/b/b/cc
dup
ldc com/a/b/n/a/fd
invokestatic com/a/b/b/ca/a(Ljava/lang/Class;)Ljava/lang/String;
iconst_0
invokespecial com/a/b/b/cc/<init>(Ljava/lang/String;B)V
ldc "services"
aload 0
getfield com/a/b/n/a/fd/e Lcom/a/b/d/jl;
ldc com/a/b/n/a/fi
invokestatic com/a/b/b/cp/a(Ljava/lang/Class;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;
invokevirtual com/a/b/b/cc/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 4
.end method
