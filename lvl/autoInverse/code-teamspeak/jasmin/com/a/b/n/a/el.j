.bytecode 50.0
.class public synchronized abstract com/a/b/n/a/el
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.field private final 'a' Lcom/a/b/n/a/em;

.field private volatile 'b' Ljava/lang/Object;

.method <init>(Lcom/a/b/n/a/em;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/n/a/em
putfield com/a/b/n/a/el/a Lcom/a/b/n/a/em;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(D)Lcom/a/b/n/a/el;
new com/a/b/n/a/fw
dup
new com/a/b/n/a/en
dup
invokespecial com/a/b/n/a/en/<init>()V
invokespecial com/a/b/n/a/fw/<init>(Lcom/a/b/n/a/em;)V
astore 2
aload 2
dload 0
invokespecial com/a/b/n/a/el/b(D)V
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method private static a(DJLjava/util/concurrent/TimeUnit;)Lcom/a/b/n/a/el;
lload 2
lconst_0
lcmp
iflt L0
iconst_1
istore 5
L1:
iload 5
ldc "warmupPeriod must not be negative: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
new com/a/b/n/a/fx
dup
new com/a/b/n/a/en
dup
invokespecial com/a/b/n/a/en/<init>()V
lload 2
aload 4
invokespecial com/a/b/n/a/fx/<init>(Lcom/a/b/n/a/em;JLjava/util/concurrent/TimeUnit;)V
astore 4
aload 4
dload 0
invokespecial com/a/b/n/a/el/b(D)V
aload 4
areturn
L0:
iconst_0
istore 5
goto L1
.limit locals 6
.limit stack 7
.end method

.method private static a(Lcom/a/b/n/a/em;D)Lcom/a/b/n/a/el;
.annotation invisible Lcom/a/b/a/d;
.end annotation
new com/a/b/n/a/fw
dup
aload 0
invokespecial com/a/b/n/a/fw/<init>(Lcom/a/b/n/a/em;)V
astore 0
aload 0
dload 1
invokespecial com/a/b/n/a/el/b(D)V
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Lcom/a/b/n/a/em;DJLjava/util/concurrent/TimeUnit;)Lcom/a/b/n/a/el;
.annotation invisible Lcom/a/b/a/d;
.end annotation
new com/a/b/n/a/fx
dup
aload 0
lload 3
aload 5
invokespecial com/a/b/n/a/fx/<init>(Lcom/a/b/n/a/em;JLjava/util/concurrent/TimeUnit;)V
astore 0
aload 0
dload 1
invokespecial com/a/b/n/a/el/b(D)V
aload 0
areturn
.limit locals 6
.limit stack 6
.end method

.method private a(I)Z
aload 0
iload 1
lconst_0
getstatic java/util/concurrent/TimeUnit/MICROSECONDS Ljava/util/concurrent/TimeUnit;
invokespecial com/a/b/n/a/el/a(IJLjava/util/concurrent/TimeUnit;)Z
ireturn
.limit locals 2
.limit stack 5
.end method

.method private a(IJLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 4
lload 2
invokevirtual java/util/concurrent/TimeUnit/toMicros(J)J
lconst_0
invokestatic java/lang/Math/max(JJ)J
lstore 2
iload 1
invokestatic com/a/b/n/a/el/b(I)I
pop
aload 0
invokespecial com/a/b/n/a/el/c()Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
getfield com/a/b/n/a/el/a Lcom/a/b/n/a/em;
invokevirtual com/a/b/n/a/em/a()J
lstore 6
aload 0
invokevirtual com/a/b/n/a/el/b()J
lload 2
lsub
lload 6
lcmp
ifgt L9
L1:
iconst_1
istore 5
L10:
iload 5
ifne L5
L3:
aload 4
monitorexit
L4:
iconst_0
ireturn
L5:
aload 0
iload 1
lload 6
invokespecial com/a/b/n/a/el/b(IJ)J
lstore 2
aload 4
monitorexit
L6:
aload 0
getfield com/a/b/n/a/el/a Lcom/a/b/n/a/em;
lload 2
invokevirtual com/a/b/n/a/em/a(J)V
iconst_1
ireturn
L2:
astore 8
L7:
aload 4
monitorexit
L8:
aload 8
athrow
L9:
iconst_0
istore 5
goto L10
.limit locals 9
.limit stack 4
.end method

.method private a(JJ)Z
aload 0
invokevirtual com/a/b/n/a/el/b()J
lload 3
lsub
lload 1
lcmp
ifgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 4
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)Z
aload 0
iconst_1
lload 1
aload 3
invokespecial com/a/b/n/a/el/a(IJLjava/util/concurrent/TimeUnit;)Z
ireturn
.limit locals 4
.limit stack 5
.end method

.method private static b(I)I
iload 0
ifle L0
iconst_1
istore 1
L1:
iload 1
ldc "Requested permits (%s) must be positive"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 0
ireturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method private b(IJ)J
aload 0
iload 1
lload 2
invokevirtual com/a/b/n/a/el/a(IJ)J
lload 2
lsub
lconst_0
invokestatic java/lang/Math/max(JJ)J
lreturn
.limit locals 4
.limit stack 4
.end method

.method private b(D)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
dload 1
dconst_0
dcmpl
ifle L5
dload 1
invokestatic java/lang/Double/isNaN(D)Z
ifne L5
iconst_1
istore 3
L6:
iload 3
ldc "rate must be positive"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
invokespecial com/a/b/n/a/el/c()Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
dload 1
aload 0
getfield com/a/b/n/a/el/a Lcom/a/b/n/a/em;
invokevirtual com/a/b/n/a/em/a()J
invokevirtual com/a/b/n/a/el/a(DJ)V
aload 4
monitorexit
L1:
return
L2:
astore 5
L3:
aload 4
monitorexit
L4:
aload 5
athrow
L5:
iconst_0
istore 3
goto L6
.limit locals 6
.limit stack 5
.end method

.method private c()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
aload 0
getfield com/a/b/n/a/el/b Ljava/lang/Object;
astore 1
aload 1
ifnonnull L8
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/n/a/el/b Ljava/lang/Object;
astore 2
L1:
aload 2
astore 1
aload 2
ifnonnull L4
L3:
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
astore 1
aload 0
aload 1
putfield com/a/b/n/a/el/b Ljava/lang/Object;
L4:
aload 0
monitorexit
L5:
aload 1
areturn
L2:
astore 1
L6:
aload 0
monitorexit
L7:
aload 1
athrow
L8:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private d()D
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
invokespecial com/a/b/n/a/el/c()Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/n/a/el/a()D
dstore 1
aload 3
monitorexit
L1:
dload 1
dreturn
L2:
astore 4
L3:
aload 3
monitorexit
L4:
aload 4
athrow
.limit locals 5
.limit stack 2
.end method

.method private e()D
aload 0
invokespecial com/a/b/n/a/el/g()J
lstore 1
aload 0
getfield com/a/b/n/a/el/a Lcom/a/b/n/a/em;
lload 1
invokevirtual com/a/b/n/a/em/a(J)V
lload 1
l2d
dconst_1
dmul
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
lconst_1
invokevirtual java/util/concurrent/TimeUnit/toMicros(J)J
l2d
ddiv
dreturn
.limit locals 3
.limit stack 5
.end method

.method private f()D
aload 0
invokespecial com/a/b/n/a/el/g()J
lstore 1
aload 0
getfield com/a/b/n/a/el/a Lcom/a/b/n/a/em;
lload 1
invokevirtual com/a/b/n/a/em/a(J)V
lload 1
l2d
dconst_1
dmul
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
lconst_1
invokevirtual java/util/concurrent/TimeUnit/toMicros(J)J
l2d
ddiv
dreturn
.limit locals 3
.limit stack 5
.end method

.method private g()J
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
iconst_1
invokestatic com/a/b/n/a/el/b(I)I
pop
aload 0
invokespecial com/a/b/n/a/el/c()Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
iconst_1
aload 0
getfield com/a/b/n/a/el/a Lcom/a/b/n/a/em;
invokevirtual com/a/b/n/a/em/a()J
invokespecial com/a/b/n/a/el/b(IJ)J
lstore 1
aload 3
monitorexit
L1:
lload 1
lreturn
L2:
astore 4
L3:
aload 3
monitorexit
L4:
aload 4
athrow
.limit locals 5
.limit stack 4
.end method

.method private h()Z
aload 0
iconst_1
lconst_0
getstatic java/util/concurrent/TimeUnit/MICROSECONDS Ljava/util/concurrent/TimeUnit;
invokespecial com/a/b/n/a/el/a(IJLjava/util/concurrent/TimeUnit;)Z
ireturn
.limit locals 1
.limit stack 5
.end method

.method abstract a()D
.end method

.method abstract a(IJ)J
.end method

.method abstract a(DJ)V
.end method

.method abstract b()J
.end method

.method public toString()Ljava/lang/String;
ldc "RateLimiter[stableRate=%3.1fqps]"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokespecial com/a/b/n/a/el/d()D
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 6
.end method
