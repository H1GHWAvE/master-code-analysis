.bytecode 50.0
.class synchronized com/a/b/n/a/bl
.super java/lang/Object

.field final 'a' Ljava/util/Map;

.field final 'b' Ljava/util/Map;

.field final 'c' Ljava/lang/String;

.method <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/a/b/d/ql
dup
invokespecial com/a/b/d/ql/<init>()V
getstatic com/a/b/d/sh/c Lcom/a/b/d/sh;
invokevirtual com/a/b/d/ql/a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;
invokevirtual com/a/b/d/ql/e()Ljava/util/concurrent/ConcurrentMap;
putfield com/a/b/n/a/bl/a Ljava/util/Map;
aload 0
new com/a/b/d/ql
dup
invokespecial com/a/b/d/ql/<init>()V
getstatic com/a/b/d/sh/c Lcom/a/b/d/sh;
invokevirtual com/a/b/d/ql/a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;
invokevirtual com/a/b/d/ql/e()Ljava/util/concurrent/ConcurrentMap;
putfield com/a/b/n/a/bl/b Ljava/util/Map;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/a/b/n/a/bl/c Ljava/lang/String;
return
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/a/b/n/a/bl;Ljava/util/Set;)Lcom/a/b/n/a/bk;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 2
aload 0
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
ifne L0
aconst_null
astore 3
L1:
aload 3
areturn
L0:
aload 0
getfield com/a/b/n/a/bl/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/n/a/bk
astore 4
aload 4
astore 3
aload 4
ifnonnull L1
aload 0
getfield com/a/b/n/a/bl/a Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 5
L2:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast com/a/b/n/a/bl
astore 6
aload 6
aload 1
aload 2
invokespecial com/a/b/n/a/bl/a(Lcom/a/b/n/a/bl;Ljava/util/Set;)Lcom/a/b/n/a/bk;
astore 4
aload 4
ifnull L2
new com/a/b/n/a/bk
dup
aload 6
aload 0
invokespecial com/a/b/n/a/bk/<init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V
astore 1
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/b/n/a/bk
invokevirtual com/a/b/n/a/bk/getStackTrace()[Ljava/lang/StackTraceElement;
invokevirtual com/a/b/n/a/bk/setStackTrace([Ljava/lang/StackTraceElement;)V
aload 1
aload 4
invokevirtual com/a/b/n/a/bk/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
pop
aload 1
areturn
L3:
aconst_null
areturn
.limit locals 7
.limit stack 4
.end method

.method private a()Ljava/lang/String;
aload 0
getfield com/a/b/n/a/bl/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Lcom/a/b/n/a/bq;Lcom/a/b/n/a/bl;)V
aload 0
aload 2
if_acmpeq L0
iconst_1
istore 3
L1:
aload 2
getfield com/a/b/n/a/bl/c Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 4
aload 4
invokevirtual java/lang/String/length()I
ifeq L2
ldc "Attempted to acquire multiple locks with the same rank "
aload 4
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 4
L3:
iload 3
aload 4
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/n/a/bl/a Ljava/util/Map;
aload 2
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L4
return
L0:
iconst_0
istore 3
goto L1
L2:
new java/lang/String
dup
ldc "Attempted to acquire multiple locks with the same rank "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 4
goto L3
L4:
aload 0
getfield com/a/b/n/a/bl/b Ljava/util/Map;
aload 2
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/n/a/br
astore 4
aload 4
ifnull L5
aload 1
new com/a/b/n/a/br
dup
aload 2
aload 0
aload 4
getfield com/a/b/n/a/br/c Lcom/a/b/n/a/bk;
iconst_0
invokespecial com/a/b/n/a/br/<init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V
invokeinterface com/a/b/n/a/bq/a(Lcom/a/b/n/a/br;)V 1
return
L5:
aload 2
aload 0
invokestatic com/a/b/d/sz/f()Ljava/util/IdentityHashMap;
invokestatic java/util/Collections/newSetFromMap(Ljava/util/Map;)Ljava/util/Set;
invokespecial com/a/b/n/a/bl/a(Lcom/a/b/n/a/bl;Ljava/util/Set;)Lcom/a/b/n/a/bk;
astore 4
aload 4
ifnonnull L6
aload 0
getfield com/a/b/n/a/bl/a Ljava/util/Map;
aload 2
new com/a/b/n/a/bk
dup
aload 2
aload 0
invokespecial com/a/b/n/a/bk/<init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
L6:
new com/a/b/n/a/br
dup
aload 2
aload 0
aload 4
iconst_0
invokespecial com/a/b/n/a/br/<init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V
astore 4
aload 0
getfield com/a/b/n/a/bl/b Ljava/util/Map;
aload 2
aload 4
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
aload 4
invokeinterface com/a/b/n/a/bq/a(Lcom/a/b/n/a/br;)V 1
return
.limit locals 5
.limit stack 7
.end method

.method final a(Lcom/a/b/n/a/bq;Ljava/util/List;)V
aload 2
invokeinterface java/util/List/size()I 0
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 2
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/a/b/n/a/bl
astore 7
aload 0
aload 7
if_acmpeq L2
iconst_1
istore 5
L3:
aload 7
getfield com/a/b/n/a/bl/c Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 6
aload 6
invokevirtual java/lang/String/length()I
ifeq L4
ldc "Attempted to acquire multiple locks with the same rank "
aload 6
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 6
L5:
iload 5
aload 6
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/n/a/bl/a Ljava/util/Map;
aload 7
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L6
aload 0
getfield com/a/b/n/a/bl/b Ljava/util/Map;
aload 7
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/n/a/br
astore 6
aload 6
ifnull L7
aload 1
new com/a/b/n/a/br
dup
aload 7
aload 0
aload 6
getfield com/a/b/n/a/br/c Lcom/a/b/n/a/bk;
iconst_0
invokespecial com/a/b/n/a/br/<init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V
invokeinterface com/a/b/n/a/bq/a(Lcom/a/b/n/a/br;)V 1
L6:
iload 3
iconst_1
iadd
istore 3
goto L0
L2:
iconst_0
istore 5
goto L3
L4:
new java/lang/String
dup
ldc "Attempted to acquire multiple locks with the same rank "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 6
goto L5
L7:
aload 7
aload 0
invokestatic com/a/b/d/sz/f()Ljava/util/IdentityHashMap;
invokestatic java/util/Collections/newSetFromMap(Ljava/util/Map;)Ljava/util/Set;
invokespecial com/a/b/n/a/bl/a(Lcom/a/b/n/a/bl;Ljava/util/Set;)Lcom/a/b/n/a/bk;
astore 6
aload 6
ifnonnull L8
aload 0
getfield com/a/b/n/a/bl/a Ljava/util/Map;
aload 7
new com/a/b/n/a/bk
dup
aload 7
aload 0
invokespecial com/a/b/n/a/bk/<init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L6
L8:
new com/a/b/n/a/br
dup
aload 7
aload 0
aload 6
iconst_0
invokespecial com/a/b/n/a/br/<init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V
astore 6
aload 0
getfield com/a/b/n/a/bl/b Ljava/util/Map;
aload 7
aload 6
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
aload 6
invokeinterface com/a/b/n/a/bq/a(Lcom/a/b/n/a/br;)V 1
goto L6
L1:
return
.limit locals 8
.limit stack 7
.end method
