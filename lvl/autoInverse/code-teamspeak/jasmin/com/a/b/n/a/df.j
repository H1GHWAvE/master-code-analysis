.bytecode 50.0
.class synchronized abstract com/a/b/n/a/df
.super java/lang/Object
.implements com/a/b/n/a/dp

.field private static final 'a' Ljava/util/logging/Logger;

.method static <clinit>()V
ldc com/a/b/n/a/df
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/n/a/df/a Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method synthetic <init>(B)V
aload 0
invokespecial com/a/b/n/a/df/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
.catch java/lang/RuntimeException from L0 to L1 using L2
aload 1
ldc "Runnable was null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
ldc "Executor was null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
aload 2
aload 1
invokeinterface java/util/concurrent/Executor/execute(Ljava/lang/Runnable;)V 1
L1:
return
L2:
astore 3
getstatic com/a/b/n/a/df/a Ljava/util/logging/Logger;
astore 4
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
astore 5
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 2
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 4
aload 5
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 57
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "RuntimeException while executing runnable "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " with executor "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 6
.limit stack 6
.end method

.method public cancel(Z)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public abstract get()Ljava/lang/Object;
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual com/a/b/n/a/df/get()Ljava/lang/Object;
areturn
.limit locals 4
.limit stack 1
.end method

.method public isCancelled()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isDone()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
