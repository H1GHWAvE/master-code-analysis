.bytecode 50.0
.class public synchronized com/a/b/n/a/at
.super java/lang/Number
.implements java/io/Serializable

.field private static final 'a' J = 0L


.field private static final 'c' Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

.field private volatile transient 'b' J

.method static <clinit>()V
ldc com/a/b/n/a/at
ldc "b"
invokestatic java/util/concurrent/atomic/AtomicLongFieldUpdater/newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
putstatic com/a/b/n/a/at/c Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Number/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(D)V
aload 0
invokespecial java/lang/Number/<init>()V
aload 0
dload 1
invokestatic java/lang/Double/doubleToRawLongBits(D)J
putfield com/a/b/n/a/at/b J
return
.limit locals 3
.limit stack 3
.end method

.method private a()D
aload 0
getfield com/a/b/n/a/at/b J
invokestatic java/lang/Double/longBitsToDouble(J)D
dreturn
.limit locals 1
.limit stack 2
.end method

.method private a(D)V
aload 0
dload 1
invokestatic java/lang/Double/doubleToRawLongBits(D)J
putfield com/a/b/n/a/at/b J
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/io/ObjectInputStream;)V
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readDouble()D
invokespecial com/a/b/n/a/at/a(D)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/n/a/at/b J
invokestatic java/lang/Double/longBitsToDouble(J)D
invokevirtual java/io/ObjectOutputStream/writeDouble(D)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(DD)Z
getstatic com/a/b/n/a/at/c Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
aload 0
dload 1
invokestatic java/lang/Double/doubleToRawLongBits(D)J
dload 3
invokestatic java/lang/Double/doubleToRawLongBits(D)J
invokevirtual java/util/concurrent/atomic/AtomicLongFieldUpdater/compareAndSet(Ljava/lang/Object;JJ)Z
ireturn
.limit locals 5
.limit stack 6
.end method

.method private b(D)V
aload 0
dload 1
invokespecial com/a/b/n/a/at/a(D)V
return
.limit locals 3
.limit stack 3
.end method

.method private b(DD)Z
getstatic com/a/b/n/a/at/c Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
aload 0
dload 1
invokestatic java/lang/Double/doubleToRawLongBits(D)J
dload 3
invokestatic java/lang/Double/doubleToRawLongBits(D)J
invokevirtual java/util/concurrent/atomic/AtomicLongFieldUpdater/weakCompareAndSet(Ljava/lang/Object;JJ)Z
ireturn
.limit locals 5
.limit stack 6
.end method

.method private c(D)D
dload 1
invokestatic java/lang/Double/doubleToRawLongBits(D)J
lstore 3
getstatic com/a/b/n/a/at/c Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
aload 0
lload 3
invokevirtual java/util/concurrent/atomic/AtomicLongFieldUpdater/getAndSet(Ljava/lang/Object;J)J
invokestatic java/lang/Double/longBitsToDouble(J)D
dreturn
.limit locals 5
.limit stack 4
.end method

.method private d(D)D
L0:
aload 0
getfield com/a/b/n/a/at/b J
lstore 5
lload 5
invokestatic java/lang/Double/longBitsToDouble(J)D
dstore 3
dload 3
dload 1
dadd
invokestatic java/lang/Double/doubleToRawLongBits(D)J
lstore 7
getstatic com/a/b/n/a/at/c Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
aload 0
lload 5
lload 7
invokevirtual java/util/concurrent/atomic/AtomicLongFieldUpdater/compareAndSet(Ljava/lang/Object;JJ)Z
ifeq L0
dload 3
dreturn
.limit locals 9
.limit stack 6
.end method

.method private e(D)D
L0:
aload 0
getfield com/a/b/n/a/at/b J
lstore 5
lload 5
invokestatic java/lang/Double/longBitsToDouble(J)D
dload 1
dadd
dstore 3
dload 3
invokestatic java/lang/Double/doubleToRawLongBits(D)J
lstore 7
getstatic com/a/b/n/a/at/c Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
aload 0
lload 5
lload 7
invokevirtual java/util/concurrent/atomic/AtomicLongFieldUpdater/compareAndSet(Ljava/lang/Object;JJ)Z
ifeq L0
dload 3
dreturn
.limit locals 9
.limit stack 6
.end method

.method public doubleValue()D
aload 0
getfield com/a/b/n/a/at/b J
invokestatic java/lang/Double/longBitsToDouble(J)D
dreturn
.limit locals 1
.limit stack 2
.end method

.method public floatValue()F
aload 0
getfield com/a/b/n/a/at/b J
invokestatic java/lang/Double/longBitsToDouble(J)D
d2f
freturn
.limit locals 1
.limit stack 2
.end method

.method public intValue()I
aload 0
getfield com/a/b/n/a/at/b J
invokestatic java/lang/Double/longBitsToDouble(J)D
d2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method public longValue()J
aload 0
getfield com/a/b/n/a/at/b J
invokestatic java/lang/Double/longBitsToDouble(J)D
d2l
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/n/a/at/b J
invokestatic java/lang/Double/longBitsToDouble(J)D
invokestatic java/lang/Double/toString(D)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
