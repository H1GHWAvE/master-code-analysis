.bytecode 50.0
.class public final synchronized com/a/b/k/e
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field final 'a' Ljava/net/Inet4Address;

.field private final 'b' Ljava/net/Inet4Address;

.field private final 'c' I

.field private final 'd' I

.method public <init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
iload 3
iflt L0
iload 3
ldc_w 65535
if_icmpgt L0
iconst_1
istore 5
L1:
iload 5
ldc "port '%s' is out of range (0 <= port <= 0xffff)"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 4
iflt L2
iload 4
ldc_w 65535
if_icmpgt L2
iconst_1
istore 5
L3:
iload 5
ldc "flags '%s' is out of range (0 <= flags <= 0xffff)"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/k/d/a()Ljava/net/Inet4Address;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/net/Inet4Address
putfield com/a/b/k/e/b Ljava/net/Inet4Address;
aload 0
aload 2
invokestatic com/a/b/k/d/a()Ljava/net/Inet4Address;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/net/Inet4Address
putfield com/a/b/k/e/a Ljava/net/Inet4Address;
aload 0
iload 3
putfield com/a/b/k/e/c I
aload 0
iload 4
putfield com/a/b/k/e/d I
return
L0:
iconst_0
istore 5
goto L1
L2:
iconst_0
istore 5
goto L3
.limit locals 6
.limit stack 6
.end method

.method private a()Ljava/net/Inet4Address;
aload 0
getfield com/a/b/k/e/b Ljava/net/Inet4Address;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()Ljava/net/Inet4Address;
aload 0
getfield com/a/b/k/e/a Ljava/net/Inet4Address;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
getfield com/a/b/k/e/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield com/a/b/k/e/d I
ireturn
.limit locals 1
.limit stack 1
.end method
