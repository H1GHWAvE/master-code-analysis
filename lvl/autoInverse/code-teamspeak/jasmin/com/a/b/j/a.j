.bytecode 50.0
.class public final synchronized com/a/b/j/a
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field static final 'a' I = 256

.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field static final 'b' Ljava/math/BigInteger;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field private static final 'c' D

.field private static final 'd' D

.method static <clinit>()V
new java/math/BigInteger
dup
ldc "16a09e667f3bcc908b2fb1366ea957d3e3adec17512775099da2f590b0667322a"
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
putstatic com/a/b/j/a/b Ljava/math/BigInteger;
ldc2_w 10.0D
invokestatic java/lang/Math/log(D)D
putstatic com/a/b/j/a/c D
ldc2_w 2.0D
invokestatic java/lang/Math/log(D)D
putstatic com/a/b/j/a/d D
return
.limit locals 0
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/math/BigInteger;Ljava/math/RoundingMode;)I
ldc "x"
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/math/BigInteger
invokestatic com/a/b/j/k/a(Ljava/lang/String;Ljava/math/BigInteger;)Ljava/math/BigInteger;
pop
aload 0
invokevirtual java/math/BigInteger/bitLength()I
iconst_1
isub
istore 2
getstatic com/a/b/j/b/a [I
aload 1
invokevirtual java/math/RoundingMode/ordinal()I
iaload
tableswitch 1
L0
L1
L1
L2
L2
L3
L3
L3
default : L4
L4:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
invokestatic com/a/b/j/a/a(Ljava/math/BigInteger;)Z
invokestatic com/a/b/j/k/a(Z)V
L1:
iload 2
ireturn
L2:
aload 0
invokestatic com/a/b/j/a/a(Ljava/math/BigInteger;)Z
ifne L1
iload 2
iconst_1
iadd
ireturn
L3:
iload 2
sipush 256
if_icmpge L5
aload 0
getstatic com/a/b/j/a/b Ljava/math/BigInteger;
sipush 256
iload 2
isub
invokevirtual java/math/BigInteger/shiftRight(I)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/compareTo(Ljava/math/BigInteger;)I
ifle L1
iload 2
iconst_1
iadd
ireturn
L5:
aload 0
iconst_2
invokevirtual java/math/BigInteger/pow(I)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/bitLength()I
iconst_1
isub
iload 2
iconst_2
imul
iconst_1
iadd
if_icmplt L1
iload 2
iconst_1
iadd
ireturn
.limit locals 3
.limit stack 4
.end method

.method private static a(I)Ljava/math/BigInteger;
ldc "n"
iload 0
invokestatic com/a/b/j/k/b(Ljava/lang/String;I)I
pop
iload 0
getstatic com/a/b/j/i/f [J
arraylength
if_icmpge L0
getstatic com/a/b/j/i/f [J
iload 0
laload
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
areturn
L0:
new java/util/ArrayList
dup
iload 0
getstatic java/math/RoundingMode/CEILING Ljava/math/RoundingMode;
invokestatic com/a/b/j/g/a(ILjava/math/RoundingMode;)I
iload 0
imul
bipush 64
getstatic java/math/RoundingMode/CEILING Ljava/math/RoundingMode;
invokestatic com/a/b/j/g/a(IILjava/math/RoundingMode;)I
invokespecial java/util/ArrayList/<init>(I)V
astore 13
getstatic com/a/b/j/i/f [J
arraylength
istore 4
getstatic com/a/b/j/i/f [J
iload 4
iconst_1
isub
laload
lstore 7
lload 7
invokestatic java/lang/Long/numberOfTrailingZeros(J)I
istore 2
lload 7
iload 2
lshr
lstore 7
lload 7
getstatic java/math/RoundingMode/FLOOR Ljava/math/RoundingMode;
invokestatic com/a/b/j/i/a(JLjava/math/RoundingMode;)I
istore 1
iload 4
i2l
getstatic java/math/RoundingMode/FLOOR Ljava/math/RoundingMode;
invokestatic com/a/b/j/i/a(JLjava/math/RoundingMode;)I
iconst_1
iadd
istore 3
iload 4
i2l
lstore 9
iconst_1
iload 3
iconst_1
isub
ishl
istore 6
iload 1
iconst_1
iadd
istore 1
L1:
lload 9
iload 0
i2l
lcmp
ifgt L2
iload 6
istore 5
iload 3
istore 4
iload 6
i2l
lload 9
land
lconst_0
lcmp
ifeq L3
iload 6
iconst_1
ishl
istore 5
iload 3
iconst_1
iadd
istore 4
L3:
lload 9
invokestatic java/lang/Long/numberOfTrailingZeros(J)I
istore 3
iload 2
iload 3
iadd
istore 2
lload 7
lstore 11
iload 1
iload 4
iload 3
isub
iadd
bipush 64
if_icmplt L4
aload 13
lload 7
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
lconst_1
lstore 11
L4:
lload 11
lload 9
iload 3
lshr
lmul
lstore 7
lload 7
getstatic java/math/RoundingMode/FLOOR Ljava/math/RoundingMode;
invokestatic com/a/b/j/i/a(JLjava/math/RoundingMode;)I
iconst_1
iadd
istore 1
lconst_1
lload 9
ladd
lstore 9
iload 5
istore 6
iload 4
istore 3
goto L1
L2:
lload 7
lconst_1
lcmp
ifle L5
aload 13
lload 7
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L5:
aload 13
iconst_0
aload 13
invokeinterface java/util/List/size()I 0
invokestatic com/a/b/j/a/a(Ljava/util/List;II)Ljava/math/BigInteger;
iload 2
invokevirtual java/math/BigInteger/shiftLeft(I)Ljava/math/BigInteger;
areturn
.limit locals 14
.limit stack 5
.end method

.method private static a(II)Ljava/math/BigInteger;
ldc "n"
iload 0
invokestatic com/a/b/j/k/b(Ljava/lang/String;I)I
pop
ldc "k"
iload 1
invokestatic com/a/b/j/k/b(Ljava/lang/String;I)I
pop
iload 1
iload 0
if_icmpgt L0
iconst_1
istore 7
L1:
iload 7
ldc "k (%s) > n (%s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
istore 2
iload 1
iload 0
iconst_1
ishr
if_icmple L2
iload 0
iload 1
isub
istore 2
L2:
iload 2
getstatic com/a/b/j/i/g [I
arraylength
if_icmpge L3
iload 0
getstatic com/a/b/j/i/g [I
iload 2
iaload
if_icmpgt L3
iload 0
iload 2
invokestatic com/a/b/j/i/a(II)J
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
areturn
L0:
iconst_0
istore 7
goto L1
L3:
getstatic java/math/BigInteger/ONE Ljava/math/BigInteger;
astore 12
iload 0
i2l
lstore 8
lconst_1
lstore 10
lload 8
getstatic java/math/RoundingMode/CEILING Ljava/math/RoundingMode;
invokestatic com/a/b/j/i/a(JLjava/math/RoundingMode;)I
istore 3
iconst_1
istore 4
iload 3
istore 1
L4:
iload 4
iload 2
if_icmpge L5
iload 0
iload 4
isub
istore 5
iload 4
iconst_1
iadd
istore 6
iload 1
iload 3
iadd
bipush 63
if_icmplt L6
aload 12
lload 8
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;
lload 10
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;
astore 12
iload 5
i2l
lstore 8
iload 6
i2l
lstore 10
iload 3
istore 1
L7:
iload 4
iconst_1
iadd
istore 4
goto L4
L6:
lload 8
iload 5
i2l
lmul
lstore 8
lload 10
iload 6
i2l
lmul
lstore 10
iload 1
iload 3
iadd
istore 1
goto L7
L5:
aload 12
lload 8
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;
lload 10
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
.limit locals 13
.limit stack 6
.end method

.method private static a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/RoundingMode;)Ljava/math/BigInteger;
.annotation invisible Lcom/a/b/a/c;
a s = "TODO"
.end annotation
new java/math/BigDecimal
dup
aload 0
invokespecial java/math/BigDecimal/<init>(Ljava/math/BigInteger;)V
new java/math/BigDecimal
dup
aload 1
invokespecial java/math/BigDecimal/<init>(Ljava/math/BigInteger;)V
iconst_0
aload 2
invokevirtual java/math/BigDecimal/divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;
invokevirtual java/math/BigDecimal/toBigIntegerExact()Ljava/math/BigInteger;
areturn
.limit locals 3
.limit stack 4
.end method

.method private static a(Ljava/util/List;)Ljava/math/BigInteger;
aload 0
iconst_0
aload 0
invokeinterface java/util/List/size()I 0
invokestatic com/a/b/j/a/a(Ljava/util/List;II)Ljava/math/BigInteger;
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/List;II)Ljava/math/BigInteger;
iload 2
iload 1
isub
tableswitch 0
L0
L1
L2
L3
default : L4
L4:
iload 2
iload 1
iadd
iconst_1
iushr
istore 3
aload 0
iload 1
iload 3
invokestatic com/a/b/j/a/a(Ljava/util/List;II)Ljava/math/BigInteger;
aload 0
iload 3
iload 2
invokestatic com/a/b/j/a/a(Ljava/util/List;II)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
L0:
getstatic java/math/BigInteger/ONE Ljava/math/BigInteger;
areturn
L1:
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/math/BigInteger
areturn
L2:
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/math/BigInteger
aload 0
iload 1
iconst_1
iadd
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/math/BigInteger
invokevirtual java/math/BigInteger/multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
L3:
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/math/BigInteger
aload 0
iload 1
iconst_1
iadd
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/math/BigInteger
invokevirtual java/math/BigInteger/multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;
aload 0
iload 1
iconst_2
iadd
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/math/BigInteger
invokevirtual java/math/BigInteger/multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Ljava/math/BigInteger;)Z
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual java/math/BigInteger/signum()I
ifle L0
aload 0
invokevirtual java/math/BigInteger/getLowestSetBit()I
aload 0
invokevirtual java/math/BigInteger/bitLength()I
iconst_1
isub
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/math/BigInteger;Ljava/math/RoundingMode;)I
.annotation invisible Lcom/a/b/a/c;
a s = "TODO"
.end annotation
ldc "x"
aload 0
invokestatic com/a/b/j/k/a(Ljava/lang/String;Ljava/math/BigInteger;)Ljava/math/BigInteger;
pop
aload 0
invokestatic com/a/b/j/a/d(Ljava/math/BigInteger;)Z
ifeq L0
aload 0
invokevirtual java/math/BigInteger/longValue()J
aload 1
invokestatic com/a/b/j/i/b(JLjava/math/RoundingMode;)I
istore 4
L1:
iload 4
ireturn
L0:
aload 0
getstatic java/math/RoundingMode/FLOOR Ljava/math/RoundingMode;
invokestatic com/a/b/j/a/a(Ljava/math/BigInteger;Ljava/math/RoundingMode;)I
i2d
getstatic com/a/b/j/a/d D
dmul
getstatic com/a/b/j/a/c D
ddiv
d2i
istore 3
getstatic java/math/BigInteger/TEN Ljava/math/BigInteger;
iload 3
invokevirtual java/math/BigInteger/pow(I)Ljava/math/BigInteger;
astore 6
aload 6
aload 0
invokevirtual java/math/BigInteger/compareTo(Ljava/math/BigInteger;)I
istore 2
iload 2
ifle L2
L3:
iload 3
iconst_1
isub
istore 2
aload 6
getstatic java/math/BigInteger/TEN Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;
astore 7
aload 7
aload 0
invokevirtual java/math/BigInteger/compareTo(Ljava/math/BigInteger;)I
istore 4
aload 7
astore 6
iload 2
istore 3
iload 4
ifgt L3
iload 4
istore 3
aload 7
astore 6
L4:
iload 2
istore 4
getstatic com/a/b/j/b/a [I
aload 1
invokevirtual java/math/RoundingMode/ordinal()I
iaload
tableswitch 1
L5
L1
L1
L6
L6
L7
L7
L7
default : L8
L8:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L9:
getstatic java/math/BigInteger/TEN Ljava/math/BigInteger;
aload 6
invokevirtual java/math/BigInteger/multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;
astore 7
aload 7
aload 0
invokevirtual java/math/BigInteger/compareTo(Ljava/math/BigInteger;)I
istore 4
iload 4
ifgt L10
aload 7
astore 6
iload 3
iconst_1
iadd
istore 3
iload 4
istore 2
goto L9
L5:
iload 3
ifne L11
iconst_1
istore 5
L12:
iload 5
invokestatic com/a/b/j/k/a(Z)V
iload 2
ireturn
L11:
iconst_0
istore 5
goto L12
L6:
iload 2
istore 4
aload 6
aload 0
invokevirtual java/math/BigInteger/equals(Ljava/lang/Object;)Z
ifne L1
iload 2
iconst_1
iadd
ireturn
L7:
iload 2
istore 4
aload 0
iconst_2
invokevirtual java/math/BigInteger/pow(I)Ljava/math/BigInteger;
aload 6
iconst_2
invokevirtual java/math/BigInteger/pow(I)Ljava/math/BigInteger;
getstatic java/math/BigInteger/TEN Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/compareTo(Ljava/math/BigInteger;)I
ifle L1
iload 2
iconst_1
iadd
ireturn
L10:
iload 3
istore 4
iload 2
istore 3
iload 4
istore 2
goto L4
L2:
goto L9
.limit locals 8
.limit stack 4
.end method

.method private static b(Ljava/math/BigInteger;)Ljava/math/BigInteger;
.annotation invisible Lcom/a/b/a/c;
a s = "TODO"
.end annotation
aload 0
getstatic java/math/RoundingMode/FLOOR Ljava/math/RoundingMode;
invokestatic com/a/b/j/a/a(Ljava/math/BigInteger;Ljava/math/RoundingMode;)I
istore 1
iload 1
sipush 1023
if_icmpge L0
aload 0
invokestatic com/a/b/j/a/c(Ljava/math/BigInteger;)Ljava/math/BigInteger;
astore 3
L1:
aload 3
aload 0
aload 3
invokevirtual java/math/BigInteger/divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/add(Ljava/math/BigInteger;)Ljava/math/BigInteger;
iconst_1
invokevirtual java/math/BigInteger/shiftRight(I)Ljava/math/BigInteger;
astore 4
aload 4
astore 2
aload 3
aload 4
invokevirtual java/math/BigInteger/equals(Ljava/lang/Object;)Z
ifeq L2
aload 3
areturn
L0:
iload 1
bipush 52
isub
bipush -2
iand
istore 1
aload 0
iload 1
invokevirtual java/math/BigInteger/shiftRight(I)Ljava/math/BigInteger;
invokestatic com/a/b/j/a/c(Ljava/math/BigInteger;)Ljava/math/BigInteger;
iload 1
iconst_1
ishr
invokevirtual java/math/BigInteger/shiftLeft(I)Ljava/math/BigInteger;
astore 3
goto L1
L2:
aload 2
astore 3
aload 3
aload 0
aload 3
invokevirtual java/math/BigInteger/divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/add(Ljava/math/BigInteger;)Ljava/math/BigInteger;
iconst_1
invokevirtual java/math/BigInteger/shiftRight(I)Ljava/math/BigInteger;
astore 4
aload 4
astore 2
aload 4
aload 3
invokevirtual java/math/BigInteger/compareTo(Ljava/math/BigInteger;)I
iflt L2
aload 3
areturn
.limit locals 5
.limit stack 3
.end method

.method private static c(Ljava/math/BigInteger;)Ljava/math/BigInteger;
.annotation invisible Lcom/a/b/a/c;
a s = "TODO"
.end annotation
aload 0
invokestatic com/a/b/j/f/a(Ljava/math/BigInteger;)D
invokestatic java/lang/Math/sqrt(D)D
getstatic java/math/RoundingMode/HALF_EVEN Ljava/math/RoundingMode;
invokestatic com/a/b/j/c/a(DLjava/math/RoundingMode;)Ljava/math/BigInteger;
areturn
.limit locals 1
.limit stack 3
.end method

.method private static c(Ljava/math/BigInteger;Ljava/math/RoundingMode;)Ljava/math/BigInteger;
.annotation invisible Lcom/a/b/a/c;
a s = "TODO"
.end annotation
aload 0
invokevirtual java/math/BigInteger/signum()I
ifge L0
ldc "x"
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 16
iadd
aload 0
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ") must be >= 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokestatic com/a/b/j/a/d(Ljava/math/BigInteger;)Z
ifeq L1
aload 0
invokevirtual java/math/BigInteger/longValue()J
aload 1
invokestatic com/a/b/j/i/c(JLjava/math/RoundingMode;)J
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
astore 3
L2:
aload 3
areturn
L1:
aload 0
invokestatic com/a/b/j/a/b(Ljava/math/BigInteger;)Ljava/math/BigInteger;
astore 4
aload 4
astore 3
getstatic com/a/b/j/b/a [I
aload 1
invokevirtual java/math/RoundingMode/ordinal()I
iaload
tableswitch 1
L3
L2
L2
L4
L4
L5
L5
L5
default : L6
L6:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
aload 4
iconst_2
invokevirtual java/math/BigInteger/pow(I)Ljava/math/BigInteger;
aload 0
invokevirtual java/math/BigInteger/equals(Ljava/lang/Object;)Z
invokestatic com/a/b/j/k/a(Z)V
aload 4
areturn
L4:
aload 4
invokevirtual java/math/BigInteger/intValue()I
istore 2
iload 2
iload 2
imul
aload 0
invokevirtual java/math/BigInteger/intValue()I
if_icmpne L7
aload 4
iconst_2
invokevirtual java/math/BigInteger/pow(I)Ljava/math/BigInteger;
aload 0
invokevirtual java/math/BigInteger/equals(Ljava/lang/Object;)Z
ifeq L7
iconst_1
istore 2
L8:
aload 4
astore 3
iload 2
ifne L2
aload 4
getstatic java/math/BigInteger/ONE Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/add(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
L7:
iconst_0
istore 2
goto L8
L5:
aload 4
astore 3
aload 4
iconst_2
invokevirtual java/math/BigInteger/pow(I)Ljava/math/BigInteger;
aload 4
invokevirtual java/math/BigInteger/add(Ljava/math/BigInteger;)Ljava/math/BigInteger;
aload 0
invokevirtual java/math/BigInteger/compareTo(Ljava/math/BigInteger;)I
ifge L2
aload 4
getstatic java/math/BigInteger/ONE Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/add(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
.limit locals 5
.limit stack 6
.end method

.method private static d(Ljava/math/BigInteger;)Z
.annotation invisible Lcom/a/b/a/c;
a s = "TODO"
.end annotation
aload 0
invokevirtual java/math/BigInteger/bitLength()I
bipush 63
if_icmpgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method
