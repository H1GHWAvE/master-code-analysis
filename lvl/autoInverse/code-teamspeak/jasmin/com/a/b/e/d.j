.bytecode 50.0
.class public synchronized abstract com/a/b/e/d
.super com/a/b/e/g
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private static final 'a' I = 2


.method protected <init>()V
aload 0
invokespecial com/a/b/e/g/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a([CII)[C
iload 2
newarray char
astore 3
iload 1
ifle L0
aload 0
iconst_0
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L0:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokevirtual java/lang/String/length()I
istore 3
iconst_0
istore 2
L0:
aload 1
astore 4
iload 2
iload 3
if_icmpge L1
aload 0
aload 1
iload 2
invokevirtual java/lang/String/charAt(I)C
invokevirtual com/a/b/e/d/a(C)[C
ifnull L2
aload 0
aload 1
iload 2
invokevirtual com/a/b/e/d/a(Ljava/lang/String;I)Ljava/lang/String;
astore 4
L1:
aload 4
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 5
.limit stack 3
.end method

.method protected final a(Ljava/lang/String;I)Ljava/lang/String;
aload 1
invokevirtual java/lang/String/length()I
istore 9
invokestatic com/a/b/e/n/a()[C
astore 11
aload 11
arraylength
istore 3
iconst_0
istore 6
iconst_0
istore 4
iload 2
istore 5
iload 4
istore 2
L0:
iload 5
iload 9
if_icmpge L1
aload 0
aload 1
iload 5
invokevirtual java/lang/String/charAt(I)C
invokevirtual com/a/b/e/d/a(C)[C
astore 13
iload 6
istore 8
iload 2
istore 7
iload 3
istore 4
aload 11
astore 12
aload 13
ifnull L2
aload 13
arraylength
istore 7
iload 5
iload 6
isub
istore 8
iload 2
iload 8
iadd
iload 7
iadd
istore 10
iload 3
istore 4
aload 11
astore 12
iload 3
iload 10
if_icmpge L3
iload 9
iload 5
isub
iconst_2
imul
iload 10
iadd
istore 4
aload 11
iload 2
iload 4
invokestatic com/a/b/e/d/a([CII)[C
astore 12
L3:
iload 8
ifle L4
aload 1
iload 6
iload 5
aload 12
iload 2
invokevirtual java/lang/String/getChars(II[CI)V
iload 2
iload 8
iadd
istore 2
L5:
iload 2
istore 3
iload 7
ifle L6
aload 13
iconst_0
aload 12
iload 2
iload 7
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
iload 7
iadd
istore 3
L6:
iload 5
iconst_1
iadd
istore 8
iload 3
istore 7
L2:
iload 5
iconst_1
iadd
istore 5
iload 8
istore 6
iload 7
istore 2
iload 4
istore 3
aload 12
astore 11
goto L0
L1:
iload 9
iload 6
isub
istore 5
iload 2
istore 4
aload 11
astore 12
iload 5
ifle L7
iload 5
iload 2
iadd
istore 4
aload 11
astore 12
iload 3
iload 4
if_icmpge L8
aload 11
iload 2
iload 4
invokestatic com/a/b/e/d/a([CII)[C
astore 12
L8:
aload 1
iload 6
iload 9
aload 12
iload 2
invokevirtual java/lang/String/getChars(II[CI)V
L7:
new java/lang/String
dup
aload 12
iconst_0
iload 4
invokespecial java/lang/String/<init>([CII)V
areturn
L4:
goto L5
.limit locals 14
.limit stack 5
.end method

.method protected abstract a(C)[C
.end method
