.bytecode 50.0
.class public synchronized abstract com/a/b/e/a
.super com/a/b/e/d
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private final 'a' [[C

.field private final 'b' I

.field private final 'c' C

.field private final 'd' C

.method private <init>(Lcom/a/b/e/b;CC)V
aload 0
invokespecial com/a/b/e/d/<init>()V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
getfield com/a/b/e/b/a [[C
putfield com/a/b/e/a/a [[C
aload 0
aload 0
getfield com/a/b/e/a/a [[C
arraylength
putfield com/a/b/e/a/b I
iload 2
istore 5
iload 3
istore 4
iload 3
iload 2
if_icmpge L0
iconst_0
istore 4
ldc_w 65535
istore 5
L0:
aload 0
iload 5
putfield com/a/b/e/a/c C
aload 0
iload 4
putfield com/a/b/e/a/d C
return
.limit locals 6
.limit stack 2
.end method

.method protected <init>(Ljava/util/Map;CC)V
aload 0
aload 1
invokestatic com/a/b/e/b/a(Ljava/util/Map;)Lcom/a/b/e/b;
iload 2
iload 3
invokespecial com/a/b/e/a/<init>(Lcom/a/b/e/b;CC)V
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iconst_0
istore 2
L0:
aload 1
astore 4
iload 2
aload 1
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 1
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 3
iload 3
aload 0
getfield com/a/b/e/a/b I
if_icmpge L2
aload 0
getfield com/a/b/e/a/a [[C
iload 3
aaload
ifnonnull L3
L2:
iload 3
aload 0
getfield com/a/b/e/a/d C
if_icmpgt L3
iload 3
aload 0
getfield com/a/b/e/a/c C
if_icmpge L4
L3:
aload 0
aload 1
iload 2
invokevirtual com/a/b/e/a/a(Ljava/lang/String;I)Ljava/lang/String;
astore 4
L1:
aload 4
areturn
L4:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 5
.limit stack 3
.end method

.method protected abstract a()[C
.end method

.method protected final a(C)[C
iload 1
aload 0
getfield com/a/b/e/a/b I
if_icmpge L0
aload 0
getfield com/a/b/e/a/a [[C
iload 1
aaload
astore 2
aload 2
ifnull L0
aload 2
areturn
L0:
iload 1
aload 0
getfield com/a/b/e/a/c C
if_icmplt L1
iload 1
aload 0
getfield com/a/b/e/a/d C
if_icmpgt L1
aconst_null
areturn
L1:
aload 0
invokevirtual com/a/b/e/a/a()[C
areturn
.limit locals 3
.limit stack 2
.end method
