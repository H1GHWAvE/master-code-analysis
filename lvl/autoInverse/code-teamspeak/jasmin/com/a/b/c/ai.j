.bytecode 50.0
.class public final synchronized com/a/b/c/ai
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field final 'a' J

.field final 'b' J

.field final 'c' J

.field final 'd' J

.field final 'e' J

.field final 'f' J

.method public <init>(JJJJJJ)V
aload 0
invokespecial java/lang/Object/<init>()V
lload 1
lconst_0
lcmp
iflt L0
iconst_1
istore 13
L1:
iload 13
invokestatic com/a/b/b/cn/a(Z)V
lload 3
lconst_0
lcmp
iflt L2
iconst_1
istore 13
L3:
iload 13
invokestatic com/a/b/b/cn/a(Z)V
lload 5
lconst_0
lcmp
iflt L4
iconst_1
istore 13
L5:
iload 13
invokestatic com/a/b/b/cn/a(Z)V
lload 7
lconst_0
lcmp
iflt L6
iconst_1
istore 13
L7:
iload 13
invokestatic com/a/b/b/cn/a(Z)V
lload 9
lconst_0
lcmp
iflt L8
iconst_1
istore 13
L9:
iload 13
invokestatic com/a/b/b/cn/a(Z)V
lload 11
lconst_0
lcmp
iflt L10
iconst_1
istore 13
L11:
iload 13
invokestatic com/a/b/b/cn/a(Z)V
aload 0
lload 1
putfield com/a/b/c/ai/a J
aload 0
lload 3
putfield com/a/b/c/ai/b J
aload 0
lload 5
putfield com/a/b/c/ai/c J
aload 0
lload 7
putfield com/a/b/c/ai/d J
aload 0
lload 9
putfield com/a/b/c/ai/e J
aload 0
lload 11
putfield com/a/b/c/ai/f J
return
L0:
iconst_0
istore 13
goto L1
L2:
iconst_0
istore 13
goto L3
L4:
iconst_0
istore 13
goto L5
L6:
iconst_0
istore 13
goto L7
L8:
iconst_0
istore 13
goto L9
L10:
iconst_0
istore 13
goto L11
.limit locals 14
.limit stack 4
.end method

.method private a()J
aload 0
getfield com/a/b/c/ai/a J
aload 0
getfield com/a/b/c/ai/b J
ladd
lreturn
.limit locals 1
.limit stack 4
.end method

.method private a(Lcom/a/b/c/ai;)Lcom/a/b/c/ai;
new com/a/b/c/ai
dup
lconst_0
aload 0
getfield com/a/b/c/ai/a J
aload 1
getfield com/a/b/c/ai/a J
lsub
invokestatic java/lang/Math/max(JJ)J
lconst_0
aload 0
getfield com/a/b/c/ai/b J
aload 1
getfield com/a/b/c/ai/b J
lsub
invokestatic java/lang/Math/max(JJ)J
lconst_0
aload 0
getfield com/a/b/c/ai/c J
aload 1
getfield com/a/b/c/ai/c J
lsub
invokestatic java/lang/Math/max(JJ)J
lconst_0
aload 0
getfield com/a/b/c/ai/d J
aload 1
getfield com/a/b/c/ai/d J
lsub
invokestatic java/lang/Math/max(JJ)J
lconst_0
aload 0
getfield com/a/b/c/ai/e J
aload 1
getfield com/a/b/c/ai/e J
lsub
invokestatic java/lang/Math/max(JJ)J
lconst_0
aload 0
getfield com/a/b/c/ai/f J
aload 1
getfield com/a/b/c/ai/f J
lsub
invokestatic java/lang/Math/max(JJ)J
invokespecial com/a/b/c/ai/<init>(JJJJJJ)V
areturn
.limit locals 2
.limit stack 18
.end method

.method private b()J
aload 0
getfield com/a/b/c/ai/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private b(Lcom/a/b/c/ai;)Lcom/a/b/c/ai;
new com/a/b/c/ai
dup
aload 0
getfield com/a/b/c/ai/a J
aload 1
getfield com/a/b/c/ai/a J
ladd
aload 0
getfield com/a/b/c/ai/b J
aload 1
getfield com/a/b/c/ai/b J
ladd
aload 0
getfield com/a/b/c/ai/c J
aload 1
getfield com/a/b/c/ai/c J
ladd
aload 0
getfield com/a/b/c/ai/d J
aload 1
getfield com/a/b/c/ai/d J
ladd
aload 0
getfield com/a/b/c/ai/e J
aload 1
getfield com/a/b/c/ai/e J
ladd
aload 0
getfield com/a/b/c/ai/f J
aload 1
getfield com/a/b/c/ai/f J
ladd
invokespecial com/a/b/c/ai/<init>(JJJJJJ)V
areturn
.limit locals 2
.limit stack 16
.end method

.method private c()D
aload 0
invokespecial com/a/b/c/ai/a()J
lstore 1
lload 1
lconst_0
lcmp
ifne L0
dconst_1
dreturn
L0:
aload 0
getfield com/a/b/c/ai/a J
l2d
lload 1
l2d
ddiv
dreturn
.limit locals 3
.limit stack 4
.end method

.method private d()J
aload 0
getfield com/a/b/c/ai/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private e()D
aload 0
invokespecial com/a/b/c/ai/a()J
lstore 1
lload 1
lconst_0
lcmp
ifne L0
dconst_0
dreturn
L0:
aload 0
getfield com/a/b/c/ai/b J
l2d
lload 1
l2d
ddiv
dreturn
.limit locals 3
.limit stack 4
.end method

.method private f()J
aload 0
getfield com/a/b/c/ai/c J
aload 0
getfield com/a/b/c/ai/d J
ladd
lreturn
.limit locals 1
.limit stack 4
.end method

.method private g()J
aload 0
getfield com/a/b/c/ai/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private h()J
aload 0
getfield com/a/b/c/ai/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private i()D
aload 0
getfield com/a/b/c/ai/c J
aload 0
getfield com/a/b/c/ai/d J
ladd
lstore 1
lload 1
lconst_0
lcmp
ifne L0
dconst_0
dreturn
L0:
aload 0
getfield com/a/b/c/ai/d J
l2d
lload 1
l2d
ddiv
dreturn
.limit locals 3
.limit stack 4
.end method

.method private j()J
aload 0
getfield com/a/b/c/ai/e J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private k()D
aload 0
getfield com/a/b/c/ai/c J
aload 0
getfield com/a/b/c/ai/d J
ladd
lstore 1
lload 1
lconst_0
lcmp
ifne L0
dconst_0
dreturn
L0:
aload 0
getfield com/a/b/c/ai/e J
l2d
lload 1
l2d
ddiv
dreturn
.limit locals 3
.limit stack 4
.end method

.method private l()J
aload 0
getfield com/a/b/c/ai/f J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/c/ai
ifeq L0
aload 1
checkcast com/a/b/c/ai
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/c/ai/a J
aload 1
getfield com/a/b/c/ai/a J
lcmp
ifne L0
iload 3
istore 2
aload 0
getfield com/a/b/c/ai/b J
aload 1
getfield com/a/b/c/ai/b J
lcmp
ifne L0
iload 3
istore 2
aload 0
getfield com/a/b/c/ai/c J
aload 1
getfield com/a/b/c/ai/c J
lcmp
ifne L0
iload 3
istore 2
aload 0
getfield com/a/b/c/ai/d J
aload 1
getfield com/a/b/c/ai/d J
lcmp
ifne L0
iload 3
istore 2
aload 0
getfield com/a/b/c/ai/e J
aload 1
getfield com/a/b/c/ai/e J
lcmp
ifne L0
iload 3
istore 2
aload 0
getfield com/a/b/c/ai/f J
aload 1
getfield com/a/b/c/ai/f J
lcmp
ifne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final hashCode()I
bipush 6
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/ai/a J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_1
aload 0
getfield com/a/b/c/ai/b J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_2
aload 0
getfield com/a/b/c/ai/c J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_3
aload 0
getfield com/a/b/c/ai/d J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_4
aload 0
getfield com/a/b/c/ai/e J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_5
aload 0
getfield com/a/b/c/ai/f J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic java/util/Arrays/hashCode([Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 5
.end method

.method public final toString()Ljava/lang/String;
aload 0
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;)Lcom/a/b/b/cc;
ldc "hitCount"
aload 0
getfield com/a/b/c/ai/a J
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;J)Lcom/a/b/b/cc;
ldc "missCount"
aload 0
getfield com/a/b/c/ai/b J
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;J)Lcom/a/b/b/cc;
ldc "loadSuccessCount"
aload 0
getfield com/a/b/c/ai/c J
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;J)Lcom/a/b/b/cc;
ldc "loadExceptionCount"
aload 0
getfield com/a/b/c/ai/d J
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;J)Lcom/a/b/b/cc;
ldc "totalLoadTime"
aload 0
getfield com/a/b/c/ai/e J
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;J)Lcom/a/b/b/cc;
ldc "evictionCount"
aload 0
getfield com/a/b/c/ai/f J
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;J)Lcom/a/b/b/cc;
invokevirtual com/a/b/b/cc/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 4
.end method
