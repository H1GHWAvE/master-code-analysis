.bytecode 50.0
.class final synchronized com/a/b/i/ae
.super java/io/FilterInputStream

.field private 'a' J

.field private 'b' J

.method <init>(Ljava/io/InputStream;J)V
aload 0
aload 1
invokespecial java/io/FilterInputStream/<init>(Ljava/io/InputStream;)V
aload 0
ldc2_w -1L
putfield com/a/b/i/ae/b J
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
lload 2
lconst_0
lcmp
iflt L0
iconst_1
istore 4
L1:
iload 4
ldc "limit must be non-negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
lload 2
putfield com/a/b/i/ae/a J
return
L0:
iconst_0
istore 4
goto L1
.limit locals 5
.limit stack 4
.end method

.method public final available()I
aload 0
getfield com/a/b/i/ae/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/available()I
i2l
aload 0
getfield com/a/b/i/ae/a J
invokestatic java/lang/Math/min(JJ)J
l2i
ireturn
.limit locals 1
.limit stack 4
.end method

.method public final mark(I)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/i/ae/in Ljava/io/InputStream;
iload 1
invokevirtual java/io/InputStream/mark(I)V
aload 0
aload 0
getfield com/a/b/i/ae/a J
putfield com/a/b/i/ae/b J
L1:
aload 0
monitorexit
return
L2:
astore 2
aload 0
monitorexit
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public final read()I
aload 0
getfield com/a/b/i/ae/a J
lconst_0
lcmp
ifne L0
iconst_m1
ireturn
L0:
aload 0
getfield com/a/b/i/ae/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
iconst_m1
if_icmpeq L1
aload 0
aload 0
getfield com/a/b/i/ae/a J
lconst_1
lsub
putfield com/a/b/i/ae/a J
L1:
iload 1
ireturn
.limit locals 2
.limit stack 5
.end method

.method public final read([BII)I
aload 0
getfield com/a/b/i/ae/a J
lconst_0
lcmp
ifne L0
iconst_m1
ireturn
L0:
iload 3
i2l
aload 0
getfield com/a/b/i/ae/a J
invokestatic java/lang/Math/min(JJ)J
l2i
istore 3
aload 0
getfield com/a/b/i/ae/in Ljava/io/InputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/InputStream/read([BII)I
istore 2
iload 2
iconst_m1
if_icmpeq L1
aload 0
aload 0
getfield com/a/b/i/ae/a J
iload 2
i2l
lsub
putfield com/a/b/i/ae/a J
L1:
iload 2
ireturn
.limit locals 4
.limit stack 5
.end method

.method public final reset()V
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
.catch all from L3 to L4 using L1
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/i/ae/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/markSupported()Z
ifne L2
new java/io/IOException
dup
ldc "Mark not supported"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 0
monitorexit
aload 1
athrow
L2:
aload 0
getfield com/a/b/i/ae/b J
ldc2_w -1L
lcmp
ifne L3
new java/io/IOException
dup
ldc "Mark not set"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield com/a/b/i/ae/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/reset()V
aload 0
aload 0
getfield com/a/b/i/ae/b J
putfield com/a/b/i/ae/a J
L4:
aload 0
monitorexit
return
.limit locals 2
.limit stack 4
.end method

.method public final skip(J)J
lload 1
aload 0
getfield com/a/b/i/ae/a J
invokestatic java/lang/Math/min(JJ)J
lstore 1
aload 0
getfield com/a/b/i/ae/in Ljava/io/InputStream;
lload 1
invokevirtual java/io/InputStream/skip(J)J
lstore 1
aload 0
aload 0
getfield com/a/b/i/ae/a J
lload 1
lsub
putfield com/a/b/i/ae/a J
lload 1
lreturn
.limit locals 3
.limit stack 5
.end method
