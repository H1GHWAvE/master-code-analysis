.bytecode 50.0
.class final synchronized com/a/b/i/ac
.super java/lang/Object
.implements com/a/b/i/n

.field final 'a' Ljava/io/DataOutput;

.field final 'b' Ljava/io/ByteArrayOutputStream;

.method <init>(Ljava/io/ByteArrayOutputStream;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/i/ac/b Ljava/io/ByteArrayOutputStream;
aload 0
new java/io/DataOutputStream
dup
aload 1
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
putfield com/a/b/i/ac/a Ljava/io/DataOutput;
return
.limit locals 2
.limit stack 4
.end method

.method public final a()[B
aload 0
getfield com/a/b/i/ac/b Ljava/io/ByteArrayOutputStream;
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 1
.limit stack 1
.end method

.method public final write(I)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
iload 1
invokeinterface java/io/DataOutput/write(I)V 1
L1:
return
L2:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final write([B)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
aload 1
invokeinterface java/io/DataOutput/write([B)V 1
L1:
return
L2:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final write([BII)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
aload 1
iload 2
iload 3
invokeinterface java/io/DataOutput/write([BII)V 3
L1:
return
L2:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 4
.limit stack 4
.end method

.method public final writeBoolean(Z)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
iload 1
invokeinterface java/io/DataOutput/writeBoolean(Z)V 1
L1:
return
L2:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final writeByte(I)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
iload 1
invokeinterface java/io/DataOutput/writeByte(I)V 1
L1:
return
L2:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final writeBytes(Ljava/lang/String;)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
aload 1
invokeinterface java/io/DataOutput/writeBytes(Ljava/lang/String;)V 1
L1:
return
L2:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final writeChar(I)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
iload 1
invokeinterface java/io/DataOutput/writeChar(I)V 1
L1:
return
L2:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final writeChars(Ljava/lang/String;)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
aload 1
invokeinterface java/io/DataOutput/writeChars(Ljava/lang/String;)V 1
L1:
return
L2:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final writeDouble(D)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
dload 1
invokeinterface java/io/DataOutput/writeDouble(D)V 2
L1:
return
L2:
astore 3
new java/lang/AssertionError
dup
aload 3
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 4
.limit stack 3
.end method

.method public final writeFloat(F)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
fload 1
invokeinterface java/io/DataOutput/writeFloat(F)V 1
L1:
return
L2:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final writeInt(I)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
iload 1
invokeinterface java/io/DataOutput/writeInt(I)V 1
L1:
return
L2:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final writeLong(J)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
lload 1
invokeinterface java/io/DataOutput/writeLong(J)V 2
L1:
return
L2:
astore 3
new java/lang/AssertionError
dup
aload 3
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 4
.limit stack 3
.end method

.method public final writeShort(I)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
iload 1
invokeinterface java/io/DataOutput/writeShort(I)V 1
L1:
return
L2:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final writeUTF(Ljava/lang/String;)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ac/a Ljava/io/DataOutput;
aload 1
invokeinterface java/io/DataOutput/writeUTF(Ljava/lang/String;)V 1
L1:
return
L2:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 2
.limit stack 3
.end method
