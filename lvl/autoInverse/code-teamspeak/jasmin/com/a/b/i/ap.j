.bytecode 50.0
.class final synchronized com/a/b/i/ap
.super java/io/Writer

.field private static final 'a' Lcom/a/b/i/ap;

.method static <clinit>()V
new com/a/b/i/ap
dup
invokespecial com/a/b/i/ap/<init>()V
putstatic com/a/b/i/ap/a Lcom/a/b/i/ap;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/io/Writer/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a()Lcom/a/b/i/ap;
getstatic com/a/b/i/ap/a Lcom/a/b/i/ap;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final append(C)Ljava/io/Writer;
aload 0
areturn
.limit locals 2
.limit stack 1
.end method

.method public final append(Ljava/lang/CharSequence;)Ljava/io/Writer;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
areturn
.limit locals 2
.limit stack 1
.end method

.method public final append(Ljava/lang/CharSequence;II)Ljava/io/Writer;
iload 2
iload 3
aload 1
invokeinterface java/lang/CharSequence/length()I 0
invokestatic com/a/b/b/cn/a(III)V
aload 0
areturn
.limit locals 4
.limit stack 3
.end method

.method public final volatile synthetic append(C)Ljava/lang/Appendable;
aload 0
iload 1
invokevirtual com/a/b/i/ap/append(C)Ljava/io/Writer;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
aload 0
aload 1
invokevirtual com/a/b/i/ap/append(Ljava/lang/CharSequence;)Ljava/io/Writer;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
aload 0
aload 1
iload 2
iload 3
invokevirtual com/a/b/i/ap/append(Ljava/lang/CharSequence;II)Ljava/io/Writer;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final close()V
return
.limit locals 1
.limit stack 0
.end method

.method public final flush()V
return
.limit locals 1
.limit stack 0
.end method

.method public final toString()Ljava/lang/String;
ldc "CharStreams.nullWriter()"
areturn
.limit locals 1
.limit stack 1
.end method

.method public final write(I)V
return
.limit locals 2
.limit stack 0
.end method

.method public final write(Ljava/lang/String;)V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 2
.limit stack 1
.end method

.method public final write(Ljava/lang/String;II)V
iload 2
iload 2
iload 3
iadd
aload 1
invokevirtual java/lang/String/length()I
invokestatic com/a/b/b/cn/a(III)V
return
.limit locals 4
.limit stack 3
.end method

.method public final write([C)V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 2
.limit stack 1
.end method

.method public final write([CII)V
iload 2
iload 2
iload 3
iadd
aload 1
arraylength
invokestatic com/a/b/b/cn/a(III)V
return
.limit locals 4
.limit stack 3
.end method
