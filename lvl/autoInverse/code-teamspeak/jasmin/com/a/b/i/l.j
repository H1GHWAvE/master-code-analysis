.bytecode 50.0
.class final synchronized com/a/b/i/l
.super java/lang/Object
.implements com/a/b/i/bs

.field 'a' I

.field 'b' I

.field 'c' I

.field 'd' Z

.field final 'e' Lcom/a/b/b/m;

.field final synthetic 'f' Lcom/a/b/i/bu;

.field final synthetic 'g' Lcom/a/b/i/j;

.method <init>(Lcom/a/b/i/j;Lcom/a/b/i/bu;)V
aload 0
aload 1
putfield com/a/b/i/l/g Lcom/a/b/i/j;
aload 0
aload 2
putfield com/a/b/i/l/f Lcom/a/b/i/bu;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/a/b/i/l/a I
aload 0
iconst_0
putfield com/a/b/i/l/b I
aload 0
iconst_0
putfield com/a/b/i/l/c I
aload 0
iconst_0
putfield com/a/b/i/l/d Z
aload 0
aload 0
getfield com/a/b/i/l/g Lcom/a/b/i/j;
invokevirtual com/a/b/i/j/a()Lcom/a/b/b/m;
putfield com/a/b/i/l/e Lcom/a/b/b/m;
return
.limit locals 3
.limit stack 2
.end method

.method public final a()I
iconst_m1
istore 3
L0:
aload 0
getfield com/a/b/i/l/f Lcom/a/b/i/bu;
invokeinterface com/a/b/i/bu/a()I 0
istore 2
iload 2
iconst_m1
if_icmpne L1
iload 3
istore 2
aload 0
getfield com/a/b/i/l/d Z
ifne L2
iload 3
istore 2
aload 0
getfield com/a/b/i/l/g Lcom/a/b/i/j;
invokestatic com/a/b/i/j/a(Lcom/a/b/i/j;)Lcom/a/b/i/g;
aload 0
getfield com/a/b/i/l/c I
invokevirtual com/a/b/i/g/a(I)Z
ifne L2
aload 0
getfield com/a/b/i/l/c I
istore 2
new com/a/b/i/h
dup
new java/lang/StringBuilder
dup
bipush 32
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Invalid input length "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/b/i/h/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 0
getfield com/a/b/i/l/c I
iconst_1
iadd
putfield com/a/b/i/l/c I
iload 2
i2c
istore 1
aload 0
getfield com/a/b/i/l/e Lcom/a/b/b/m;
iload 1
invokevirtual com/a/b/b/m/c(C)Z
ifeq L3
aload 0
getfield com/a/b/i/l/d Z
ifne L4
aload 0
getfield com/a/b/i/l/c I
iconst_1
if_icmpeq L5
aload 0
getfield com/a/b/i/l/g Lcom/a/b/i/j;
invokestatic com/a/b/i/j/a(Lcom/a/b/i/j;)Lcom/a/b/i/g;
aload 0
getfield com/a/b/i/l/c I
iconst_1
isub
invokevirtual com/a/b/i/g/a(I)Z
ifne L4
L5:
aload 0
getfield com/a/b/i/l/c I
istore 2
new com/a/b/i/h
dup
new java/lang/StringBuilder
dup
bipush 41
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Padding cannot start at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/b/i/h/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
iconst_1
putfield com/a/b/i/l/d Z
goto L0
L3:
aload 0
getfield com/a/b/i/l/d Z
ifeq L6
aload 0
getfield com/a/b/i/l/c I
istore 2
new com/a/b/i/h
dup
new java/lang/StringBuilder
dup
bipush 61
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Expected padding character but found '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
ldc "' at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/b/i/h/<init>(Ljava/lang/String;)V
athrow
L6:
aload 0
aload 0
getfield com/a/b/i/l/a I
aload 0
getfield com/a/b/i/l/g Lcom/a/b/i/j;
invokestatic com/a/b/i/j/a(Lcom/a/b/i/j;)Lcom/a/b/i/g;
getfield com/a/b/i/g/v I
ishl
putfield com/a/b/i/l/a I
aload 0
getfield com/a/b/i/l/a I
istore 2
aload 0
getfield com/a/b/i/l/g Lcom/a/b/i/j;
invokestatic com/a/b/i/j/a(Lcom/a/b/i/j;)Lcom/a/b/i/g;
astore 4
iload 1
bipush 127
if_icmpgt L7
aload 4
getfield com/a/b/i/g/y [B
iload 1
baload
iconst_m1
if_icmpne L8
L7:
new com/a/b/i/h
dup
new java/lang/StringBuilder
dup
bipush 25
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Unrecognized character: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/b/i/h/<init>(Ljava/lang/String;)V
athrow
L8:
aload 0
aload 4
getfield com/a/b/i/g/y [B
iload 1
baload
iload 2
ior
putfield com/a/b/i/l/a I
aload 0
aload 0
getfield com/a/b/i/l/b I
aload 0
getfield com/a/b/i/l/g Lcom/a/b/i/j;
invokestatic com/a/b/i/j/a(Lcom/a/b/i/j;)Lcom/a/b/i/g;
getfield com/a/b/i/g/v I
iadd
putfield com/a/b/i/l/b I
aload 0
getfield com/a/b/i/l/b I
bipush 8
if_icmplt L0
aload 0
aload 0
getfield com/a/b/i/l/b I
bipush 8
isub
putfield com/a/b/i/l/b I
aload 0
getfield com/a/b/i/l/a I
aload 0
getfield com/a/b/i/l/b I
ishr
sipush 255
iand
istore 2
L2:
iload 2
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final b()V
aload 0
getfield com/a/b/i/l/f Lcom/a/b/i/bu;
invokeinterface com/a/b/i/bu/b()V 0
return
.limit locals 1
.limit stack 1
.end method
