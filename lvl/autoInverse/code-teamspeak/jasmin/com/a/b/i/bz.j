.bytecode 50.0
.class public final synchronized com/a/b/i/bz
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field final 'a' Ljava/util/Queue;

.field private final 'b' Ljava/lang/Readable;

.field private final 'c' Ljava/io/Reader;

.field private final 'd' [C

.field private final 'e' Ljava/nio/CharBuffer;

.field private final 'f' Lcom/a/b/i/bx;

.method public <init>(Ljava/lang/Readable;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
sipush 4096
newarray char
putfield com/a/b/i/bz/d [C
aload 0
aload 0
getfield com/a/b/i/bz/d [C
invokestatic java/nio/CharBuffer/wrap([C)Ljava/nio/CharBuffer;
putfield com/a/b/i/bz/e Ljava/nio/CharBuffer;
aload 0
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
putfield com/a/b/i/bz/a Ljava/util/Queue;
aload 0
new com/a/b/i/ca
dup
aload 0
invokespecial com/a/b/i/ca/<init>(Lcom/a/b/i/bz;)V
putfield com/a/b/i/bz/f Lcom/a/b/i/bx;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Readable
putfield com/a/b/i/bz/b Ljava/lang/Readable;
aload 1
instanceof java/io/Reader
ifeq L0
aload 1
checkcast java/io/Reader
astore 1
L1:
aload 0
aload 1
putfield com/a/b/i/bz/c Ljava/io/Reader;
return
L0:
aconst_null
astore 1
goto L1
.limit locals 2
.limit stack 4
.end method

.method private static synthetic a(Lcom/a/b/i/bz;)Ljava/util/Queue;
aload 0
getfield com/a/b/i/bz/a Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Ljava/lang/String;
L0:
aload 0
getfield com/a/b/i/bz/a Ljava/util/Queue;
invokeinterface java/util/Queue/peek()Ljava/lang/Object; 0
ifnonnull L1
aload 0
getfield com/a/b/i/bz/e Ljava/nio/CharBuffer;
invokevirtual java/nio/CharBuffer/clear()Ljava/nio/Buffer;
pop
aload 0
getfield com/a/b/i/bz/c Ljava/io/Reader;
ifnull L2
aload 0
getfield com/a/b/i/bz/c Ljava/io/Reader;
aload 0
getfield com/a/b/i/bz/d [C
iconst_0
aload 0
getfield com/a/b/i/bz/d [C
arraylength
invokevirtual java/io/Reader/read([CII)I
istore 1
L3:
iload 1
iconst_m1
if_icmpne L4
aload 0
getfield com/a/b/i/bz/f Lcom/a/b/i/bx;
invokevirtual com/a/b/i/bx/a()V
L1:
aload 0
getfield com/a/b/i/bz/a Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
checkcast java/lang/String
areturn
L2:
aload 0
getfield com/a/b/i/bz/b Ljava/lang/Readable;
aload 0
getfield com/a/b/i/bz/e Ljava/nio/CharBuffer;
invokeinterface java/lang/Readable/read(Ljava/nio/CharBuffer;)I 1
istore 1
goto L3
L4:
aload 0
getfield com/a/b/i/bz/f Lcom/a/b/i/bx;
aload 0
getfield com/a/b/i/bz/d [C
iload 1
invokevirtual com/a/b/i/bx/a([CI)V
goto L0
.limit locals 2
.limit stack 4
.end method
