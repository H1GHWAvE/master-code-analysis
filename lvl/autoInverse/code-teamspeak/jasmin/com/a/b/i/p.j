.bytecode 50.0
.class public synchronized abstract com/a/b/i/p
.super java/lang/Object

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/io/InputStream;)J
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 4
L0:
aload 4
aload 0
invokevirtual com/a/b/i/p/a()Ljava/io/OutputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/OutputStream
astore 5
aload 1
aload 5
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
lstore 2
aload 5
invokevirtual java/io/OutputStream/flush()V
L1:
aload 4
invokevirtual com/a/b/i/ar/close()V
lload 2
lreturn
L2:
astore 1
L4:
aload 4
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 4
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 6
.limit stack 2
.end method

.method private a(Ljava/nio/charset/Charset;)Lcom/a/b/i/ag;
new com/a/b/i/r
dup
aload 0
aload 1
iconst_0
invokespecial com/a/b/i/r/<init>(Lcom/a/b/i/p;Ljava/nio/charset/Charset;B)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private a([B)V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 2
L0:
aload 2
aload 0
invokevirtual com/a/b/i/p/a()Ljava/io/OutputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/OutputStream
astore 3
aload 3
aload 1
invokevirtual java/io/OutputStream/write([B)V
aload 3
invokevirtual java/io/OutputStream/flush()V
L1:
aload 2
invokevirtual com/a/b/i/ar/close()V
return
L2:
astore 1
L4:
aload 2
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 2
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method private b()Ljava/io/OutputStream;
aload 0
invokevirtual com/a/b/i/p/a()Ljava/io/OutputStream;
astore 1
aload 1
instanceof java/io/BufferedOutputStream
ifeq L0
aload 1
checkcast java/io/BufferedOutputStream
areturn
L0:
new java/io/BufferedOutputStream
dup
aload 1
invokespecial java/io/BufferedOutputStream/<init>(Ljava/io/OutputStream;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public abstract a()Ljava/io/OutputStream;
.end method
