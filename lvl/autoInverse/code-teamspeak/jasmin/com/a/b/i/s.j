.bytecode 50.0
.class public synchronized abstract com/a/b/i/s
.super java/lang/Object

.field private static final 'a' I = 4096


.field private static final 'b' [B

.method static <clinit>()V
sipush 4096
newarray byte
putstatic com/a/b/i/s/b [B
return
.limit locals 0
.limit stack 1
.end method

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/io/InputStream;)J
lconst_0
lstore 1
L0:
aload 0
aload 0
invokevirtual java/io/InputStream/available()I
ldc_w 2147483647
invokestatic java/lang/Math/min(II)I
i2l
invokevirtual java/io/InputStream/skip(J)J
lstore 3
lload 3
lconst_0
lcmp
ifgt L1
aload 0
invokevirtual java/io/InputStream/read()I
iconst_m1
if_icmpne L2
lload 1
lreturn
L2:
lload 1
lconst_0
lcmp
ifne L3
aload 0
invokevirtual java/io/InputStream/available()I
ifne L3
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L3:
lload 1
lconst_1
ladd
lstore 1
goto L0
L1:
lload 1
lload 3
ladd
lstore 1
goto L0
.limit locals 5
.limit stack 4
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/i/s;
new com/a/b/i/w
dup
aload 0
invokespecial com/a/b/i/w/<init>(Ljava/lang/Iterable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/i/s;
aload 0
invokestatic com/a/b/d/jl/a(Ljava/util/Iterator;)Lcom/a/b/d/jl;
invokestatic com/a/b/i/s/a(Ljava/lang/Iterable;)Lcom/a/b/i/s;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a([B)Lcom/a/b/i/s;
new com/a/b/i/v
dup
aload 0
invokespecial com/a/b/i/v/<init>([B)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static transient a([Lcom/a/b/i/s;)Lcom/a/b/i/s;
aload 0
invokestatic com/a/b/d/jl/a([Ljava/lang/Object;)Lcom/a/b/d/jl;
invokestatic com/a/b/i/s/a(Ljava/lang/Iterable;)Lcom/a/b/i/s;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/io/InputStream;)J
lconst_0
lstore 1
L0:
aload 0
getstatic com/a/b/i/s/b [B
invokevirtual java/io/InputStream/read([B)I
i2l
lstore 3
lload 3
ldc2_w -1L
lcmp
ifeq L1
lload 1
lload 3
ladd
lstore 1
goto L0
L1:
lload 1
lreturn
.limit locals 5
.limit stack 4
.end method

.method private static f()Lcom/a/b/i/s;
invokestatic com/a/b/i/x/f()Lcom/a/b/i/x;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a(Lcom/a/b/i/p;)J
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 4
L0:
aload 4
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
aload 4
aload 1
invokevirtual com/a/b/i/p/a()Ljava/io/OutputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/OutputStream
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
lstore 2
L1:
aload 4
invokevirtual com/a/b/i/ar/close()V
lload 2
lreturn
L2:
astore 1
L4:
aload 4
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 4
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 5
.limit stack 3
.end method

.method public a(Ljava/io/OutputStream;)J
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 4
L0:
aload 4
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
aload 1
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
lstore 2
L1:
aload 4
invokevirtual com/a/b/i/ar/close()V
lload 2
lreturn
L2:
astore 1
L4:
aload 4
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 4
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 5
.limit stack 2
.end method

.method public a(Lcom/a/b/g/ak;)Lcom/a/b/g/ag;
aload 1
invokeinterface com/a/b/g/ak/a()Lcom/a/b/g/al; 0
astore 1
aload 0
new com/a/b/g/ac
dup
aload 1
invokespecial com/a/b/g/ac/<init>(Lcom/a/b/g/bn;)V
invokevirtual com/a/b/i/s/a(Ljava/io/OutputStream;)J
pop2
aload 1
invokeinterface com/a/b/g/al/a()Lcom/a/b/g/ag; 0
areturn
.limit locals 2
.limit stack 4
.end method

.method public a(Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;
new com/a/b/i/u
dup
aload 0
aload 1
iconst_0
invokespecial com/a/b/i/u/<init>(Lcom/a/b/i/s;Ljava/nio/charset/Charset;B)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public a(JJ)Lcom/a/b/i/s;
new com/a/b/i/y
dup
aload 0
lload 1
lload 3
iconst_0
invokespecial com/a/b/i/y/<init>(Lcom/a/b/i/s;JJB)V
areturn
.limit locals 5
.limit stack 8
.end method

.method public abstract a()Ljava/io/InputStream;
.end method

.method public a(Lcom/a/b/i/o;)Ljava/lang/Object;
.annotation invisible Lcom/a/b/a/a;
.end annotation
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 2
L0:
aload 2
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
aload 1
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;Lcom/a/b/i/o;)Ljava/lang/Object;
astore 1
L1:
aload 2
invokevirtual com/a/b/i/ar/close()V
aload 1
areturn
L2:
astore 1
L4:
aload 2
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 2
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final a(Lcom/a/b/i/s;)Z
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Throwable from L1 to L4 using L2
.catch all from L1 to L4 using L3
.catch all from L5 to L3 using L3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
sipush 4096
newarray byte
astore 5
sipush 4096
newarray byte
astore 6
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 4
L0:
aload 4
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
astore 7
aload 4
aload 1
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
astore 1
L1:
aload 7
aload 5
iconst_0
sipush 4096
invokestatic com/a/b/i/z/b(Ljava/io/InputStream;[BII)I
istore 2
iload 2
aload 1
aload 6
iconst_0
sipush 4096
invokestatic com/a/b/i/z/b(Ljava/io/InputStream;[BII)I
if_icmpne L6
aload 5
aload 6
invokestatic java/util/Arrays/equals([B[B)Z
istore 3
L4:
iload 3
ifne L7
L6:
aload 4
invokevirtual com/a/b/i/ar/close()V
iconst_0
ireturn
L7:
iload 2
sipush 4096
if_icmpeq L1
aload 4
invokevirtual com/a/b/i/ar/close()V
iconst_1
ireturn
L2:
astore 1
L5:
aload 4
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 1
aload 4
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 8
.limit stack 5
.end method

.method public b()Ljava/io/InputStream;
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
astore 1
aload 1
instanceof java/io/BufferedInputStream
ifeq L0
aload 1
checkcast java/io/BufferedInputStream
areturn
L0:
new java/io/BufferedInputStream
dup
aload 1
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public c()Z
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 3
L0:
aload 3
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
invokevirtual java/io/InputStream/read()I
istore 1
L1:
iload 1
iconst_m1
if_icmpne L5
iconst_1
istore 2
L6:
aload 3
invokevirtual com/a/b/i/ar/close()V
iload 2
ireturn
L5:
iconst_0
istore 2
goto L6
L2:
astore 4
L4:
aload 3
aload 4
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 4
aload 3
invokevirtual com/a/b/i/ar/close()V
aload 4
athrow
.limit locals 5
.limit stack 2
.end method

.method public d()J
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/io/IOException from L8 to L2 using L2
.catch all from L8 to L2 using L3
.catch java/lang/Throwable from L9 to L10 using L11
.catch all from L9 to L10 using L12
.catch all from L13 to L12 using L12
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 6
L0:
aload 6
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
astore 7
L1:
lconst_0
lstore 2
L4:
aload 7
aload 7
invokevirtual java/io/InputStream/available()I
ldc_w 2147483647
invokestatic java/lang/Math/min(II)I
i2l
invokevirtual java/io/InputStream/skip(J)J
lstore 4
L5:
lload 4
lconst_0
lcmp
ifgt L14
L6:
aload 7
invokevirtual java/io/InputStream/read()I
istore 1
L7:
iload 1
iconst_m1
if_icmpne L15
aload 6
invokevirtual com/a/b/i/ar/close()V
lload 2
lreturn
L15:
lload 2
lconst_0
lcmp
ifne L16
L8:
aload 7
invokevirtual java/io/InputStream/available()I
ifne L16
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L2:
astore 7
aload 6
invokevirtual com/a/b/i/ar/close()V
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 6
L9:
aload 6
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
invokestatic com/a/b/i/s/b(Ljava/io/InputStream;)J
lstore 2
L10:
aload 6
invokevirtual com/a/b/i/ar/close()V
lload 2
lreturn
L16:
lload 2
lconst_1
ladd
lstore 2
goto L4
L14:
lload 2
lload 4
ladd
lstore 2
goto L4
L3:
astore 7
aload 6
invokevirtual com/a/b/i/ar/close()V
aload 7
athrow
L11:
astore 7
L13:
aload 6
aload 7
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L12:
astore 7
aload 6
invokevirtual com/a/b/i/ar/close()V
aload 7
athrow
.limit locals 8
.limit stack 4
.end method

.method public e()[B
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 1
L0:
aload 1
aload 0
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/InputStream
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;)[B
astore 2
L1:
aload 1
invokevirtual com/a/b/i/ar/close()V
aload 2
areturn
L2:
astore 2
L4:
aload 1
aload 2
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 2
aload 1
invokevirtual com/a/b/i/ar/close()V
aload 2
athrow
.limit locals 3
.limit stack 2
.end method
