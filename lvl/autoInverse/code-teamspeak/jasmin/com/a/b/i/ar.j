.bytecode 50.0
.class public final synchronized com/a/b/i/ar
.super java/lang/Object
.implements java/io/Closeable
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'b' Lcom/a/b/i/au;

.field final 'a' Lcom/a/b/i/au;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field private final 'c' Ljava/util/Deque;

.field private 'd' Ljava/lang/Throwable;

.method static <clinit>()V
invokestatic com/a/b/i/at/a()Z
ifeq L0
getstatic com/a/b/i/at/a Lcom/a/b/i/at;
astore 0
L1:
aload 0
putstatic com/a/b/i/ar/b Lcom/a/b/i/au;
return
L0:
getstatic com/a/b/i/as/a Lcom/a/b/i/as;
astore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method private <init>(Lcom/a/b/i/au;)V
.annotation invisible Lcom/a/b/a/d;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayDeque
dup
iconst_4
invokespecial java/util/ArrayDeque/<init>(I)V
putfield com/a/b/i/ar/c Ljava/util/Deque;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/i/au
putfield com/a/b/i/ar/a Lcom/a/b/i/au;
return
.limit locals 2
.limit stack 4
.end method

.method public static a()Lcom/a/b/i/ar;
new com/a/b/i/ar
dup
getstatic com/a/b/i/ar/b Lcom/a/b/i/au;
invokespecial com/a/b/i/ar/<init>(Lcom/a/b/i/au;)V
areturn
.limit locals 0
.limit stack 3
.end method

.method private a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/RuntimeException;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
putfield com/a/b/i/ar/d Ljava/lang/Throwable;
aload 1
ldc java/io/IOException
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;Ljava/lang/Class;)V
aload 1
aload 2
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;Ljava/lang/Class;)V
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/lang/Throwable;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/RuntimeException;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
putfield com/a/b/i/ar/d Ljava/lang/Throwable;
aload 1
ldc java/io/IOException
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;Ljava/lang/Class;)V
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
aload 2
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;Ljava/lang/Class;)V
aload 1
aload 3
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;Ljava/lang/Class;)V
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/io/Closeable;)Ljava/io/Closeable;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
ifnull L0
aload 0
getfield com/a/b/i/ar/c Ljava/util/Deque;
aload 1
invokeinterface java/util/Deque/addFirst(Ljava/lang/Object;)V 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
putfield com/a/b/i/ar/d Ljava/lang/Throwable;
aload 1
ldc java/io/IOException
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;Ljava/lang/Class;)V
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final close()V
.catch java/lang/Throwable from L0 to L1 using L2
aload 0
getfield com/a/b/i/ar/d Ljava/lang/Throwable;
astore 1
L3:
aload 0
getfield com/a/b/i/ar/c Ljava/util/Deque;
invokeinterface java/util/Deque/isEmpty()Z 0
ifne L4
aload 0
getfield com/a/b/i/ar/c Ljava/util/Deque;
invokeinterface java/util/Deque/removeFirst()Ljava/lang/Object; 0
checkcast java/io/Closeable
astore 3
L0:
aload 3
invokeinterface java/io/Closeable/close()V 0
L1:
goto L3
L2:
astore 2
aload 1
ifnonnull L5
aload 2
astore 1
goto L3
L5:
aload 0
getfield com/a/b/i/ar/a Lcom/a/b/i/au;
aload 3
aload 1
aload 2
invokeinterface com/a/b/i/au/a(Ljava/io/Closeable;Ljava/lang/Throwable;Ljava/lang/Throwable;)V 3
goto L3
L4:
aload 0
getfield com/a/b/i/ar/d Ljava/lang/Throwable;
ifnonnull L6
aload 1
ifnull L6
aload 1
ldc java/io/IOException
invokestatic com/a/b/b/ei/b(Ljava/lang/Throwable;Ljava/lang/Class;)V
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L6:
return
.limit locals 4
.limit stack 4
.end method
