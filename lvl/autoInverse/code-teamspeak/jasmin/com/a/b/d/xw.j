.bytecode 50.0
.class synchronized com/a/b/d/xw
.super com/a/b/d/gy
.implements java/io/Serializable

.field private static final 'd' J = 0L


.field final 'a' Lcom/a/b/d/xc;

.field transient 'b' Ljava/util/Set;

.field transient 'c' Ljava/util/Set;

.method <init>(Lcom/a/b/d/xc;)V
aload 0
invokespecial com/a/b/d/gy/<init>()V
aload 0
aload 1
putfield com/a/b/d/xw/a Lcom/a/b/d/xc;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;I)I
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final a()Ljava/util/Set;
aload 0
getfield com/a/b/d/xw/c Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/xw/a Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/a()Ljava/util/Set; 0
invokestatic java/util/Collections/unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
astore 1
aload 0
aload 1
putfield com/a/b/d/xw/c Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;II)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 4
.limit stack 2
.end method

.method public add(Ljava/lang/Object;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public addAll(Ljava/util/Collection;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final b(Ljava/lang/Object;I)I
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method protected synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/xw/f()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c(Ljava/lang/Object;I)I
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method c()Ljava/util/Set;
aload 0
getfield com/a/b/d/xw/a Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/n_()Ljava/util/Set; 0
invokestatic java/util/Collections/unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public clear()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method protected f()Lcom/a/b/d/xc;
aload 0
getfield com/a/b/d/xw/a Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method public iterator()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/xw/a Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;)Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/xw/f()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method public n_()Ljava/util/Set;
aload 0
getfield com/a/b/d/xw/b Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/xw/c()Ljava/util/Set;
astore 1
aload 0
aload 1
putfield com/a/b/d/xw/b Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public remove(Ljava/lang/Object;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public removeAll(Ljava/util/Collection;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public retainAll(Ljava/util/Collection;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method
