.bytecode 50.0
.class public final synchronized com/a/b/d/dg
.super com/a/b/d/as
.implements java/io/Serializable

.field private static final 'b' J = 1L


.field private final transient 'a' Ljava/util/concurrent/ConcurrentMap;

.method private <init>(Ljava/util/concurrent/ConcurrentMap;)V
.annotation invisible Lcom/a/b/a/d;
.end annotation
aload 0
invokespecial com/a/b/d/as/<init>()V
aload 1
invokeinterface java/util/concurrent/ConcurrentMap/isEmpty()Z 0
invokestatic com/a/b/b/cn/a(Z)V
aload 0
aload 1
putfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Lcom/a/b/d/ql;)Lcom/a/b/d/dg;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/d/dg
dup
aload 0
invokevirtual com/a/b/d/ql/e()Ljava/util/concurrent/ConcurrentMap;
invokespecial com/a/b/d/dg/<init>(Ljava/util/concurrent/ConcurrentMap;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/dg;
new com/a/b/d/dg
dup
new java/util/concurrent/ConcurrentHashMap
dup
invokespecial java/util/concurrent/ConcurrentHashMap/<init>()V
invokespecial com/a/b/d/dg/<init>(Ljava/util/concurrent/ConcurrentMap;)V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/d/dg;)Ljava/util/concurrent/ConcurrentMap;
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/io/ObjectInputStream;)V
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/util/concurrent/ConcurrentMap
astore 1
getstatic com/a/b/d/dl/a Lcom/a/b/d/aab;
aload 0
aload 1
invokevirtual com/a/b/d/aab/a(Ljava/lang/Object;Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method private d(Ljava/lang/Object;I)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iload 2
ifne L0
L1:
iconst_1
ireturn
L0:
iload 2
ifle L2
iconst_1
istore 5
L3:
iload 5
ldc "Invalid occurrences: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicInteger
astore 6
aload 6
ifnonnull L4
iconst_0
ireturn
L2:
iconst_0
istore 5
goto L3
L4:
aload 6
invokevirtual java/util/concurrent/atomic/AtomicInteger/get()I
istore 3
iload 3
iload 2
if_icmpge L5
iconst_0
ireturn
L5:
iload 3
iload 2
isub
istore 4
aload 6
iload 3
iload 4
invokevirtual java/util/concurrent/atomic/AtomicInteger/compareAndSet(II)Z
ifeq L4
iload 4
ifne L1
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 6
invokeinterface java/util/concurrent/ConcurrentMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z 2
pop
iconst_1
ireturn
.limit locals 7
.limit stack 6
.end method

.method private static g()Lcom/a/b/d/dg;
new com/a/b/d/dg
dup
new java/util/concurrent/ConcurrentHashMap
dup
invokespecial java/util/concurrent/ConcurrentHashMap/<init>()V
invokespecial com/a/b/d/dg/<init>(Ljava/util/concurrent/ConcurrentMap;)V
areturn
.limit locals 0
.limit stack 4
.end method

.method private h()Ljava/util/List;
aload 0
invokevirtual com/a/b/d/dg/size()I
invokestatic com/a/b/d/ov/b(I)Ljava/util/ArrayList;
astore 2
aload 0
invokevirtual com/a/b/d/dg/a()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
astore 4
aload 4
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
astore 5
aload 4
invokeinterface com/a/b/d/xd/b()I 0
istore 1
L2:
iload 1
ifle L0
aload 2
aload 5
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 1
iconst_1
isub
istore 1
goto L2
L1:
aload 2
areturn
.limit locals 6
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicInteger
astore 1
aload 1
ifnonnull L0
iconst_0
ireturn
L0:
aload 1
invokevirtual java/util/concurrent/atomic/AtomicInteger/get()I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;I)I
.catch java/lang/ArithmeticException from L0 to L1 using L2
iconst_0
istore 4
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
ifne L3
aload 0
aload 1
invokevirtual com/a/b/d/dg/a(Ljava/lang/Object;)I
istore 3
L4:
iload 3
ireturn
L3:
iload 2
ifle L5
iconst_1
istore 5
L6:
iload 5
ldc "Invalid occurrences: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L7:
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicInteger
astore 7
aload 7
astore 6
aload 7
ifnonnull L8
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
new java/util/concurrent/atomic/AtomicInteger
dup
iload 2
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>(I)V
invokeinterface java/util/concurrent/ConcurrentMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
checkcast java/util/concurrent/atomic/AtomicInteger
astore 6
iload 4
istore 3
aload 6
ifnull L4
L8:
aload 6
invokevirtual java/util/concurrent/atomic/AtomicInteger/get()I
istore 3
iload 3
ifeq L9
L0:
aload 6
iload 3
iload 3
iload 2
invokestatic com/a/b/j/g/a(II)I
invokevirtual java/util/concurrent/atomic/AtomicInteger/compareAndSet(II)Z
istore 5
L1:
iload 5
ifeq L8
iload 3
ireturn
L5:
iconst_0
istore 5
goto L6
L2:
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 65
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Overflow adding "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " occurrences to a count of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L9:
new java/util/concurrent/atomic/AtomicInteger
dup
iload 2
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>(I)V
astore 7
iload 4
istore 3
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 7
invokeinterface java/util/concurrent/ConcurrentMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
ifnull L4
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 6
aload 7
invokeinterface java/util/concurrent/ConcurrentMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z 3
ifeq L7
iconst_0
ireturn
.limit locals 8
.limit stack 6
.end method

.method public final volatile synthetic a()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/as/a()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Object;II)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
ldc "oldCount"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
iload 3
ldc "newCount"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicInteger
astore 5
aload 5
ifnonnull L0
iload 2
ifeq L1
iconst_0
ireturn
L1:
iload 3
ifne L2
iconst_1
ireturn
L2:
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
new java/util/concurrent/atomic/AtomicInteger
dup
iload 3
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>(I)V
invokeinterface java/util/concurrent/ConcurrentMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
ifnonnull L3
iconst_1
ireturn
L3:
iconst_0
ireturn
L0:
aload 5
invokevirtual java/util/concurrent/atomic/AtomicInteger/get()I
istore 4
iload 4
iload 2
if_icmpne L4
iload 4
ifne L5
iload 3
ifne L6
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 5
invokeinterface java/util/concurrent/ConcurrentMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z 2
pop
iconst_1
ireturn
L6:
new java/util/concurrent/atomic/AtomicInteger
dup
iload 3
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>(I)V
astore 6
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 6
invokeinterface java/util/concurrent/ConcurrentMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
ifnull L7
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 5
aload 6
invokeinterface java/util/concurrent/ConcurrentMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z 3
ifeq L8
L7:
iconst_1
ireturn
L8:
iconst_0
ireturn
L5:
aload 5
iload 4
iload 3
invokevirtual java/util/concurrent/atomic/AtomicInteger/compareAndSet(II)Z
ifeq L4
iload 3
ifne L9
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 5
invokeinterface java/util/concurrent/ConcurrentMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z 2
pop
L9:
iconst_1
ireturn
L4:
iconst_0
ireturn
.limit locals 7
.limit stack 5
.end method

.method public final volatile synthetic add(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/as/add(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic addAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/as/addAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 4
iload 2
ifne L0
aload 0
aload 1
invokevirtual com/a/b/d/dg/a(Ljava/lang/Object;)I
istore 3
L1:
iload 3
ireturn
L0:
iload 2
ifle L2
iconst_1
istore 6
L3:
iload 6
ldc "Invalid occurrences: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicInteger
astore 7
iload 4
istore 3
aload 7
ifnull L1
L4:
aload 7
invokevirtual java/util/concurrent/atomic/AtomicInteger/get()I
istore 5
iload 4
istore 3
iload 5
ifeq L1
iconst_0
iload 5
iload 2
isub
invokestatic java/lang/Math/max(II)I
istore 3
aload 7
iload 5
iload 3
invokevirtual java/util/concurrent/atomic/AtomicInteger/compareAndSet(II)Z
ifeq L4
iload 3
ifne L5
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 7
invokeinterface java/util/concurrent/ConcurrentMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z 2
pop
L5:
iload 5
ireturn
L2:
iconst_0
istore 6
goto L3
.limit locals 8
.limit stack 6
.end method

.method final b()Ljava/util/Iterator;
new com/a/b/d/dj
dup
aload 0
new com/a/b/d/di
dup
aload 0
invokespecial com/a/b/d/di/<init>(Lcom/a/b/d/dg;)V
invokespecial com/a/b/d/dj/<init>(Lcom/a/b/d/dg;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 6
.end method

.method final c()I
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
invokeinterface java/util/concurrent/ConcurrentMap/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c(Ljava/lang/Object;I)I
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
ldc "count"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
L0:
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicInteger
astore 5
aload 5
astore 4
aload 5
ifnonnull L1
iload 2
ifne L2
iconst_0
ireturn
L2:
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
new java/util/concurrent/atomic/AtomicInteger
dup
iload 2
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>(I)V
invokeinterface java/util/concurrent/ConcurrentMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
checkcast java/util/concurrent/atomic/AtomicInteger
astore 5
aload 5
astore 4
aload 5
ifnonnull L1
iconst_0
ireturn
L1:
aload 4
invokevirtual java/util/concurrent/atomic/AtomicInteger/get()I
istore 3
iload 3
ifne L3
iload 2
ifne L4
iconst_0
ireturn
L4:
new java/util/concurrent/atomic/AtomicInteger
dup
iload 2
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>(I)V
astore 5
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 5
invokeinterface java/util/concurrent/ConcurrentMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
ifnull L5
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 4
aload 5
invokeinterface java/util/concurrent/ConcurrentMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z 3
ifeq L0
L5:
iconst_0
ireturn
L3:
aload 4
iload 3
iload 2
invokevirtual java/util/concurrent/atomic/AtomicInteger/compareAndSet(II)Z
ifeq L1
iload 2
ifne L6
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 4
invokeinterface java/util/concurrent/ConcurrentMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z 2
pop
L6:
iload 3
ireturn
.limit locals 6
.limit stack 5
.end method

.method public final clear()V
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
invokeinterface java/util/concurrent/ConcurrentMap/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic contains(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/as/contains(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method final e()Ljava/util/Set;
new com/a/b/d/dh
dup
aload 0
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
invokeinterface java/util/concurrent/ConcurrentMap/keySet()Ljava/util/Set; 0
invokespecial com/a/b/d/dh/<init>(Lcom/a/b/d/dg;Ljava/util/Set;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/as/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final f()Ljava/util/Set;
new com/a/b/d/dk
dup
aload 0
iconst_0
invokespecial com/a/b/d/dk/<init>(Lcom/a/b/d/dg;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/as/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isEmpty()Z
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
invokeinterface java/util/concurrent/ConcurrentMap/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic iterator()Ljava/util/Iterator;
aload 0
invokespecial com/a/b/d/as/iterator()Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic n_()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/as/n_()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic remove(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/as/remove(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic removeAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/as/removeAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic retainAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/as/retainAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final size()I
aload 0
getfield com/a/b/d/dg/a Ljava/util/concurrent/ConcurrentMap;
invokeinterface java/util/concurrent/ConcurrentMap/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 3
lconst_0
lstore 1
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/concurrent/atomic/AtomicInteger
invokevirtual java/util/concurrent/atomic/AtomicInteger/get()I
i2l
lload 1
ladd
lstore 1
goto L0
L1:
lload 1
invokestatic com/a/b/l/q/b(J)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final toArray()[Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/dg/h()Ljava/util/List;
invokeinterface java/util/List/toArray()[Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/dg/h()Ljava/util/List;
aload 1
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/as/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
