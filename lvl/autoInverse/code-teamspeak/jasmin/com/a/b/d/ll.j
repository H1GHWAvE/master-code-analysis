.bytecode 50.0
.class public final synchronized com/a/b/d/ll
.super java/lang/Object

.field private final 'a' Lcom/a/b/d/yr;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic com/a/b/d/afm/c()Lcom/a/b/d/afm;
putfield com/a/b/d/ll/a Lcom/a/b/d/yr;
return
.limit locals 1
.limit stack 2
.end method

.method private a()Lcom/a/b/d/lf;
aload 0
getfield com/a/b/d/ll/a Lcom/a/b/d/yr;
invokestatic com/a/b/d/lf/d(Lcom/a/b/d/yr;)Lcom/a/b/d/lf;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Lcom/a/b/d/yl;)Lcom/a/b/d/ll;
aload 1
invokevirtual com/a/b/d/yl/f()Z
ifeq L0
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 33
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "range must not be empty, but was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield com/a/b/d/ll/a Lcom/a/b/d/yr;
invokeinterface com/a/b/d/yr/f()Lcom/a/b/d/yr; 0
aload 1
invokeinterface com/a/b/d/yr/c(Lcom/a/b/d/yl;)Z 1
ifne L1
aload 0
getfield com/a/b/d/ll/a Lcom/a/b/d/yr;
invokeinterface com/a/b/d/yr/g()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L2:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
astore 4
aload 4
aload 1
invokevirtual com/a/b/d/yl/b(Lcom/a/b/d/yl;)Z
ifeq L4
aload 4
aload 1
invokevirtual com/a/b/d/yl/c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/f()Z
ifeq L5
L4:
iconst_1
istore 2
L6:
iload 2
ldc "Ranges may not overlap, but received %s and %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
dup
iconst_1
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
goto L2
L5:
iconst_0
istore 2
goto L6
L3:
new java/lang/AssertionError
dup
ldc "should have thrown an IAE above"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L1:
aload 0
getfield com/a/b/d/ll/a Lcom/a/b/d/yr;
aload 1
invokeinterface com/a/b/d/yr/a(Lcom/a/b/d/yl;)V 1
aload 0
areturn
.limit locals 5
.limit stack 6
.end method

.method private a(Lcom/a/b/d/yr;)Lcom/a/b/d/ll;
aload 1
invokeinterface com/a/b/d/yr/g()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
astore 1
aload 1
invokevirtual com/a/b/d/yl/f()Z
ifeq L2
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 33
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "range must not be empty, but was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
getfield com/a/b/d/ll/a Lcom/a/b/d/yr;
invokeinterface com/a/b/d/yr/f()Lcom/a/b/d/yr; 0
aload 1
invokeinterface com/a/b/d/yr/c(Lcom/a/b/d/yl;)Z 1
ifne L3
aload 0
getfield com/a/b/d/ll/a Lcom/a/b/d/yr;
invokeinterface com/a/b/d/yr/g()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L4:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
astore 4
aload 4
aload 1
invokevirtual com/a/b/d/yl/b(Lcom/a/b/d/yl;)Z
ifeq L6
aload 4
aload 1
invokevirtual com/a/b/d/yl/c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/f()Z
ifeq L7
L6:
iconst_1
istore 2
L8:
iload 2
ldc "Ranges may not overlap, but received %s and %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
dup
iconst_1
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
goto L4
L7:
iconst_0
istore 2
goto L8
L5:
new java/lang/AssertionError
dup
ldc "should have thrown an IAE above"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L3:
aload 0
getfield com/a/b/d/ll/a Lcom/a/b/d/yr;
aload 1
invokeinterface com/a/b/d/yr/a(Lcom/a/b/d/yl;)V 1
goto L0
L1:
aload 0
areturn
.limit locals 5
.limit stack 6
.end method
