.bytecode 50.0
.class synchronized com/a/b/d/ads
.super com/a/b/d/adi
.implements java/util/SortedMap

.field private static final 'a' J = 0L


.method <init>(Ljava/util/SortedMap;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/adi/<init>(Ljava/util/Map;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method synthetic a()Ljava/util/Map;
aload 0
invokevirtual com/a/b/d/ads/b()Ljava/util/SortedMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method b()Ljava/util/SortedMap;
aload 0
invokespecial com/a/b/d/adi/a()Ljava/util/Map;
checkcast java/util/SortedMap
areturn
.limit locals 1
.limit stack 1
.end method

.method public comparator()Ljava/util/Comparator;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ads/b()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/comparator()Ljava/util/Comparator; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method synthetic d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ads/b()Ljava/util/SortedMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method public firstKey()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ads/b()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/firstKey()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ads/b()Ljava/util/SortedMap;
aload 1
invokeinterface java/util/SortedMap/headMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/SortedMap;Ljava/lang/Object;)Ljava/util/SortedMap;
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public lastKey()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ads/b()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/lastKey()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ads/b()Ljava/util/SortedMap;
aload 1
aload 2
invokeinterface java/util/SortedMap/subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap; 2
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/SortedMap;Ljava/lang/Object;)Ljava/util/SortedMap;
astore 1
aload 3
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ads/b()Ljava/util/SortedMap;
aload 1
invokeinterface java/util/SortedMap/tailMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
aload 0
getfield com/a/b/d/ads/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/SortedMap;Ljava/lang/Object;)Ljava/util/SortedMap;
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method
