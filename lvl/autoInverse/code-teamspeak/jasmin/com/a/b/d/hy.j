.bytecode 50.0
.class public final synchronized com/a/b/d/hy
.super java/util/AbstractMap
.implements com/a/b/d/bw
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'b' D = 1.0D


.field private static final 'h' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "Not needed in emulated source"
.end annotation
.end field

.field transient 'a' I

.field private transient 'c' [Lcom/a/b/d/ia;

.field private transient 'd' [Lcom/a/b/d/ia;

.field private transient 'e' I

.field private transient 'f' I

.field private transient 'g' Lcom/a/b/d/bw;

.method private <init>(I)V
aload 0
invokespecial java/util/AbstractMap/<init>()V
aload 0
iload 1
invokespecial com/a/b/d/hy/b(I)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/hy;)I
aload 0
getfield com/a/b/d/hy/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static a(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
iconst_0
istore 1
L1:
iload 1
invokestatic com/a/b/d/iq/a(I)I
ireturn
L0:
aload 0
invokevirtual java/lang/Object/hashCode()I
istore 1
goto L1
.limit locals 2
.limit stack 1
.end method

.method public static a()Lcom/a/b/d/hy;
bipush 16
invokestatic com/a/b/d/hy/a(I)Lcom/a/b/d/hy;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(I)Lcom/a/b/d/hy;
new com/a/b/d/hy
dup
iload 0
invokespecial com/a/b/d/hy/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/d/hy;
aload 0
invokeinterface java/util/Map/size()I 0
invokestatic com/a/b/d/hy/a(I)Lcom/a/b/d/hy;
astore 1
aload 1
aload 0
invokevirtual com/a/b/d/hy/putAll(Ljava/util/Map;)V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/hy;Ljava/lang/Object;I)Lcom/a/b/d/ia;
aload 0
aload 1
iload 2
invokespecial com/a/b/d/hy/b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
areturn
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/d/hy;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
aload 1
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
istore 4
aload 2
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
istore 5
aload 0
aload 1
iload 4
invokevirtual com/a/b/d/hy/a(Ljava/lang/Object;I)Lcom/a/b/d/ia;
astore 6
aload 6
ifnull L0
iload 5
aload 6
getfield com/a/b/d/ia/a I
if_icmpne L0
aload 2
aload 6
getfield com/a/b/d/ia/e Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 2
areturn
L0:
aload 0
aload 2
iload 5
invokespecial com/a/b/d/hy/b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
astore 7
aload 7
ifnull L1
iload 3
ifeq L2
aload 0
aload 7
invokevirtual com/a/b/d/hy/a(Lcom/a/b/d/ia;)V
L1:
aload 6
ifnull L3
aload 0
aload 6
invokevirtual com/a/b/d/hy/a(Lcom/a/b/d/ia;)V
L3:
aload 0
new com/a/b/d/ia
dup
aload 2
iload 5
aload 1
iload 4
invokespecial com/a/b/d/ia/<init>(Ljava/lang/Object;ILjava/lang/Object;I)V
invokespecial com/a/b/d/hy/b(Lcom/a/b/d/ia;)V
aload 0
invokespecial com/a/b/d/hy/d()V
aload 6
ifnonnull L4
aconst_null
areturn
L2:
aload 2
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 23
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "value already present: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 6
getfield com/a/b/d/ia/e Ljava/lang/Object;
areturn
.limit locals 8
.limit stack 7
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 1
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
istore 4
aload 2
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
istore 5
aload 0
aload 1
iload 4
invokespecial com/a/b/d/hy/b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
astore 6
aload 6
ifnull L0
iload 5
aload 6
getfield com/a/b/d/ia/b I
if_icmpne L0
aload 2
aload 6
getfield com/a/b/d/ia/f Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 2
areturn
L0:
aload 0
aload 2
iload 5
invokevirtual com/a/b/d/hy/a(Ljava/lang/Object;I)Lcom/a/b/d/ia;
astore 7
aload 7
ifnull L1
iload 3
ifeq L2
aload 0
aload 7
invokevirtual com/a/b/d/hy/a(Lcom/a/b/d/ia;)V
L1:
aload 6
ifnull L3
aload 0
aload 6
invokevirtual com/a/b/d/hy/a(Lcom/a/b/d/ia;)V
L3:
aload 0
new com/a/b/d/ia
dup
aload 1
iload 4
aload 2
iload 5
invokespecial com/a/b/d/ia/<init>(Ljava/lang/Object;ILjava/lang/Object;I)V
invokespecial com/a/b/d/hy/b(Lcom/a/b/d/ia;)V
aload 0
invokespecial com/a/b/d/hy/d()V
aload 6
ifnonnull L4
aconst_null
areturn
L2:
aload 2
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 23
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "value already present: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 6
getfield com/a/b/d/ia/f Ljava/lang/Object;
areturn
.limit locals 8
.limit stack 7
.end method

.method static synthetic a(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V
aload 0
aload 1
invokevirtual com/a/b/d/hy/a(Lcom/a/b/d/ia;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 2
aload 0
iload 2
invokespecial com/a/b/d/hy/b(I)V
aload 0
aload 1
iload 2
invokestatic com/a/b/d/zz/a(Ljava/util/Map;Ljava/io/ObjectInputStream;I)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Ljava/util/Map;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Ljava/lang/Object;)I
aload 0
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/a/b/d/hy;Ljava/lang/Object;I)Lcom/a/b/d/ia;
aload 0
aload 1
iload 2
invokevirtual com/a/b/d/hy/a(Ljava/lang/Object;I)Lcom/a/b/d/ia;
areturn
.limit locals 3
.limit stack 3
.end method

.method private b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
aload 0
getfield com/a/b/d/hy/e I
iload 2
iand
aaload
astore 3
L0:
aload 3
ifnull L1
iload 2
aload 3
getfield com/a/b/d/ia/a I
if_icmpne L2
aload 1
aload 3
getfield com/a/b/d/ia/e Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L2
aload 3
areturn
L2:
aload 3
getfield com/a/b/d/ia/c Lcom/a/b/d/ia;
astore 3
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 3
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 1
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
istore 4
aload 2
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
istore 5
aload 0
aload 1
iload 4
invokevirtual com/a/b/d/hy/a(Ljava/lang/Object;I)Lcom/a/b/d/ia;
astore 6
aload 6
ifnull L0
iload 5
aload 6
getfield com/a/b/d/ia/a I
if_icmpne L0
aload 2
aload 6
getfield com/a/b/d/ia/e Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 2
areturn
L0:
aload 0
aload 2
iload 5
invokespecial com/a/b/d/hy/b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
astore 7
aload 7
ifnull L1
iload 3
ifeq L2
aload 0
aload 7
invokevirtual com/a/b/d/hy/a(Lcom/a/b/d/ia;)V
L1:
aload 6
ifnull L3
aload 0
aload 6
invokevirtual com/a/b/d/hy/a(Lcom/a/b/d/ia;)V
L3:
aload 0
new com/a/b/d/ia
dup
aload 2
iload 5
aload 1
iload 4
invokespecial com/a/b/d/ia/<init>(Ljava/lang/Object;ILjava/lang/Object;I)V
invokespecial com/a/b/d/hy/b(Lcom/a/b/d/ia;)V
aload 0
invokespecial com/a/b/d/hy/d()V
aload 6
ifnonnull L4
aconst_null
areturn
L2:
aload 2
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 23
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "value already present: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 6
getfield com/a/b/d/ia/e Ljava/lang/Object;
areturn
.limit locals 8
.limit stack 7
.end method

.method private b(I)V
iload 1
ldc "expectedSize"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
iload 1
dconst_1
invokestatic com/a/b/d/iq/a(ID)I
istore 1
aload 0
iload 1
anewarray com/a/b/d/ia
putfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
aload 0
iload 1
anewarray com/a/b/d/ia
putfield com/a/b/d/hy/d [Lcom/a/b/d/ia;
aload 0
iload 1
iconst_1
isub
putfield com/a/b/d/hy/e I
aload 0
iconst_0
putfield com/a/b/d/hy/f I
aload 0
iconst_0
putfield com/a/b/d/hy/a I
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic b(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V
aload 0
aload 1
invokespecial com/a/b/d/hy/b(Lcom/a/b/d/ia;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Lcom/a/b/d/ia;)V
aload 1
getfield com/a/b/d/ia/a I
aload 0
getfield com/a/b/d/hy/e I
iand
istore 2
aload 1
aload 0
getfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
iload 2
aaload
putfield com/a/b/d/ia/c Lcom/a/b/d/ia;
aload 0
getfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
iload 2
aload 1
aastore
aload 1
getfield com/a/b/d/ia/b I
aload 0
getfield com/a/b/d/hy/e I
iand
istore 2
aload 1
aload 0
getfield com/a/b/d/hy/d [Lcom/a/b/d/ia;
iload 2
aaload
putfield com/a/b/d/ia/d Lcom/a/b/d/ia;
aload 0
getfield com/a/b/d/hy/d [Lcom/a/b/d/ia;
iload 2
aload 1
aastore
aload 0
aload 0
getfield com/a/b/d/hy/a I
iconst_1
iadd
putfield com/a/b/d/hy/a I
aload 0
aload 0
getfield com/a/b/d/hy/f I
iconst_1
iadd
putfield com/a/b/d/hy/f I
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic b(Lcom/a/b/d/hy;)[Lcom/a/b/d/ia;
aload 0
getfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic c(Lcom/a/b/d/hy;)I
aload 0
getfield com/a/b/d/hy/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(I)[Lcom/a/b/d/ia;
iload 0
anewarray com/a/b/d/ia
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
iconst_0
istore 1
aload 0
getfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
astore 5
aload 0
getfield com/a/b/d/hy/a I
aload 5
arraylength
invokestatic com/a/b/d/iq/a(II)Z
ifeq L0
aload 5
arraylength
iconst_2
imul
istore 2
aload 0
iload 2
anewarray com/a/b/d/ia
putfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
aload 0
iload 2
anewarray com/a/b/d/ia
putfield com/a/b/d/hy/d [Lcom/a/b/d/ia;
aload 0
iload 2
iconst_1
isub
putfield com/a/b/d/hy/e I
aload 0
iconst_0
putfield com/a/b/d/hy/a I
L1:
iload 1
aload 5
arraylength
if_icmpge L2
aload 5
iload 1
aaload
astore 3
L3:
aload 3
ifnull L4
aload 3
getfield com/a/b/d/ia/c Lcom/a/b/d/ia;
astore 4
aload 0
aload 3
invokespecial com/a/b/d/hy/b(Lcom/a/b/d/ia;)V
aload 4
astore 3
goto L3
L4:
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 0
aload 0
getfield com/a/b/d/hy/f I
iconst_1
iadd
putfield com/a/b/d/hy/f I
L0:
return
.limit locals 6
.limit stack 3
.end method

.method final a(Ljava/lang/Object;I)Lcom/a/b/d/ia;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/hy/d [Lcom/a/b/d/ia;
aload 0
getfield com/a/b/d/hy/e I
iload 2
iand
aaload
astore 3
L0:
aload 3
ifnull L1
iload 2
aload 3
getfield com/a/b/d/ia/b I
if_icmpne L2
aload 1
aload 3
getfield com/a/b/d/ia/f Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L2
aload 3
areturn
L2:
aload 3
getfield com/a/b/d/ia/d Lcom/a/b/d/ia;
astore 3
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
iconst_1
invokespecial com/a/b/d/hy/a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 4
.end method

.method final a(Lcom/a/b/d/ia;)V
aconst_null
astore 5
aload 1
getfield com/a/b/d/ia/a I
aload 0
getfield com/a/b/d/hy/e I
iand
istore 2
aload 0
getfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
iload 2
aaload
astore 3
aconst_null
astore 4
L0:
aload 3
aload 1
if_acmpne L1
aload 4
ifnonnull L2
aload 0
getfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
iload 2
aload 1
getfield com/a/b/d/ia/c Lcom/a/b/d/ia;
aastore
L3:
aload 1
getfield com/a/b/d/ia/b I
istore 2
aload 0
getfield com/a/b/d/hy/e I
iload 2
iand
istore 2
aload 0
getfield com/a/b/d/hy/d [Lcom/a/b/d/ia;
iload 2
aaload
astore 3
aload 5
astore 4
L4:
aload 3
aload 1
if_acmpne L5
aload 4
ifnonnull L6
aload 0
getfield com/a/b/d/hy/d [Lcom/a/b/d/ia;
iload 2
aload 1
getfield com/a/b/d/ia/d Lcom/a/b/d/ia;
aastore
L7:
aload 0
aload 0
getfield com/a/b/d/hy/a I
iconst_1
isub
putfield com/a/b/d/hy/a I
aload 0
aload 0
getfield com/a/b/d/hy/f I
iconst_1
iadd
putfield com/a/b/d/hy/f I
return
L2:
aload 4
aload 1
getfield com/a/b/d/ia/c Lcom/a/b/d/ia;
putfield com/a/b/d/ia/c Lcom/a/b/d/ia;
goto L3
L1:
aload 3
getfield com/a/b/d/ia/c Lcom/a/b/d/ia;
astore 6
aload 3
astore 4
aload 6
astore 3
goto L0
L6:
aload 4
aload 1
getfield com/a/b/d/ia/d Lcom/a/b/d/ia;
putfield com/a/b/d/ia/d Lcom/a/b/d/ia;
goto L7
L5:
aload 3
getfield com/a/b/d/ia/d Lcom/a/b/d/ia;
astore 5
aload 3
astore 4
aload 5
astore 3
goto L4
.limit locals 7
.limit stack 3
.end method

.method public final b()Lcom/a/b/d/bw;
aload 0
getfield com/a/b/d/hy/g Lcom/a/b/d/bw;
ifnonnull L0
new com/a/b/d/ie
dup
aload 0
iconst_0
invokespecial com/a/b/d/ie/<init>(Lcom/a/b/d/hy;B)V
astore 1
aload 0
aload 1
putfield com/a/b/d/hy/g Lcom/a/b/d/bw;
aload 1
areturn
L0:
aload 0
getfield com/a/b/d/hy/g Lcom/a/b/d/bw;
areturn
.limit locals 2
.limit stack 4
.end method

.method public final clear()V
aload 0
iconst_0
putfield com/a/b/d/hy/a I
aload 0
getfield com/a/b/d/hy/c [Lcom/a/b/d/ia;
aconst_null
invokestatic java/util/Arrays/fill([Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/hy/d [Lcom/a/b/d/ia;
aconst_null
invokestatic java/util/Arrays/fill([Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
aload 0
getfield com/a/b/d/hy/f I
iconst_1
iadd
putfield com/a/b/d/hy/f I
return
.limit locals 1
.limit stack 3
.end method

.method public final containsKey(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 1
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
invokespecial com/a/b/d/hy/b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final containsValue(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 1
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
invokevirtual com/a/b/d/hy/a(Ljava/lang/Object;I)Lcom/a/b/d/ia;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final entrySet()Ljava/util/Set;
new com/a/b/d/ib
dup
aload 0
iconst_0
invokespecial com/a/b/d/ib/<init>(Lcom/a/b/d/hy;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 1
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
invokespecial com/a/b/d/hy/b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 1
getfield com/a/b/d/ia/f Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final j_()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/hy/b()Lcom/a/b/d/bw;
invokeinterface com/a/b/d/bw/keySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final keySet()Ljava/util/Set;
new com/a/b/d/im
dup
aload 0
invokespecial com/a/b/d/im/<init>(Lcom/a/b/d/hy;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
iconst_0
invokespecial com/a/b/d/hy/a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 4
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 1
invokestatic com/a/b/d/hy/a(Ljava/lang/Object;)I
invokespecial com/a/b/d/hy/b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/hy/a(Lcom/a/b/d/ia;)V
aload 1
getfield com/a/b/d/ia/f Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final size()I
aload 0
getfield com/a/b/d/hy/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic values()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/hy/j_()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method
