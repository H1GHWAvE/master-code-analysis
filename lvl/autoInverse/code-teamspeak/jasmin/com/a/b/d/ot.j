.bytecode 50.0
.class final synchronized com/a/b/d/ot
.super java/lang/Object
.implements java/util/ListIterator

.field final 'a' Ljava/lang/Object;

.field 'b' I

.field 'c' Lcom/a/b/d/or;

.field 'd' Lcom/a/b/d/or;

.field 'e' Lcom/a/b/d/or;

.field final synthetic 'f' Lcom/a/b/d/oj;

.method <init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
putfield com/a/b/d/ot/f Lcom/a/b/d/oj;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 2
putfield com/a/b/d/ot/a Ljava/lang/Object;
aload 1
invokestatic com/a/b/d/oj/d(Lcom/a/b/d/oj;)Ljava/util/Map;
aload 2
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
astore 1
aload 1
ifnonnull L0
aconst_null
astore 1
L1:
aload 0
aload 1
putfield com/a/b/d/ot/c Lcom/a/b/d/or;
return
L0:
aload 1
getfield com/a/b/d/oq/a Lcom/a/b/d/or;
astore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method public <init>(Lcom/a/b/d/oj;Ljava/lang/Object;I)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
putfield com/a/b/d/ot/f Lcom/a/b/d/oj;
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
invokestatic com/a/b/d/oj/d(Lcom/a/b/d/oj;)Ljava/util/Map;
aload 2
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
astore 1
aload 1
ifnonnull L0
iconst_0
istore 4
L1:
iload 3
iload 4
invokestatic com/a/b/b/cn/b(II)I
pop
iload 3
iload 4
iconst_2
idiv
if_icmplt L2
aload 1
ifnonnull L3
aconst_null
astore 1
L4:
aload 0
aload 1
putfield com/a/b/d/ot/e Lcom/a/b/d/or;
aload 0
iload 4
putfield com/a/b/d/ot/b I
L5:
iload 3
iload 4
if_icmpge L6
aload 0
invokevirtual com/a/b/d/ot/previous()Ljava/lang/Object;
pop
iload 3
iconst_1
iadd
istore 3
goto L5
L0:
aload 1
getfield com/a/b/d/oq/c I
istore 4
goto L1
L3:
aload 1
getfield com/a/b/d/oq/b Lcom/a/b/d/or;
astore 1
goto L4
L2:
aload 1
ifnonnull L7
aconst_null
astore 1
L8:
aload 0
aload 1
putfield com/a/b/d/ot/c Lcom/a/b/d/or;
L9:
iload 3
ifle L6
aload 0
invokevirtual com/a/b/d/ot/next()Ljava/lang/Object;
pop
iload 3
iconst_1
isub
istore 3
goto L9
L7:
aload 1
getfield com/a/b/d/oq/a Lcom/a/b/d/or;
astore 1
goto L8
L6:
aload 0
aload 2
putfield com/a/b/d/ot/a Ljava/lang/Object;
aload 0
aconst_null
putfield com/a/b/d/ot/d Lcom/a/b/d/or;
return
.limit locals 5
.limit stack 3
.end method

.method public final add(Ljava/lang/Object;)V
aload 0
aload 0
getfield com/a/b/d/ot/f Lcom/a/b/d/oj;
aload 0
getfield com/a/b/d/ot/a Ljava/lang/Object;
aload 1
aload 0
getfield com/a/b/d/ot/c Lcom/a/b/d/or;
invokestatic com/a/b/d/oj/a(Lcom/a/b/d/oj;Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;
putfield com/a/b/d/ot/e Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/ot/b I
iconst_1
iadd
putfield com/a/b/d/ot/b I
aload 0
aconst_null
putfield com/a/b/d/ot/d Lcom/a/b/d/or;
return
.limit locals 2
.limit stack 5
.end method

.method public final hasNext()Z
aload 0
getfield com/a/b/d/ot/c Lcom/a/b/d/or;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final hasPrevious()Z
aload 0
getfield com/a/b/d/ot/e Lcom/a/b/d/or;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final next()Ljava/lang/Object;
aload 0
getfield com/a/b/d/ot/c Lcom/a/b/d/or;
invokestatic com/a/b/d/oj/e(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/ot/c Lcom/a/b/d/or;
astore 1
aload 0
aload 1
putfield com/a/b/d/ot/d Lcom/a/b/d/or;
aload 0
aload 1
putfield com/a/b/d/ot/e Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/ot/c Lcom/a/b/d/or;
getfield com/a/b/d/or/e Lcom/a/b/d/or;
putfield com/a/b/d/ot/c Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/ot/b I
iconst_1
iadd
putfield com/a/b/d/ot/b I
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
getfield com/a/b/d/or/b Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final nextIndex()I
aload 0
getfield com/a/b/d/ot/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final previous()Ljava/lang/Object;
aload 0
getfield com/a/b/d/ot/e Lcom/a/b/d/or;
invokestatic com/a/b/d/oj/e(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/ot/e Lcom/a/b/d/or;
astore 1
aload 0
aload 1
putfield com/a/b/d/ot/d Lcom/a/b/d/or;
aload 0
aload 1
putfield com/a/b/d/ot/c Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/ot/e Lcom/a/b/d/or;
getfield com/a/b/d/or/f Lcom/a/b/d/or;
putfield com/a/b/d/ot/e Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/ot/b I
iconst_1
isub
putfield com/a/b/d/ot/b I
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
getfield com/a/b/d/or/b Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final previousIndex()I
aload 0
getfield com/a/b/d/ot/b I
iconst_1
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final remove()V
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
ifnull L0
iconst_1
istore 1
L1:
iload 1
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
aload 0
getfield com/a/b/d/ot/c Lcom/a/b/d/or;
if_acmpeq L2
aload 0
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
getfield com/a/b/d/or/f Lcom/a/b/d/or;
putfield com/a/b/d/ot/e Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/ot/b I
iconst_1
isub
putfield com/a/b/d/ot/b I
L3:
aload 0
getfield com/a/b/d/ot/f Lcom/a/b/d/oj;
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
invokestatic com/a/b/d/oj/a(Lcom/a/b/d/oj;Lcom/a/b/d/or;)V
aload 0
aconst_null
putfield com/a/b/d/ot/d Lcom/a/b/d/or;
return
L0:
iconst_0
istore 1
goto L1
L2:
aload 0
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
getfield com/a/b/d/or/e Lcom/a/b/d/or;
putfield com/a/b/d/ot/c Lcom/a/b/d/or;
goto L3
.limit locals 2
.limit stack 3
.end method

.method public final set(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
ifnull L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/b(Z)V
aload 0
getfield com/a/b/d/ot/d Lcom/a/b/d/or;
aload 1
putfield com/a/b/d/or/b Ljava/lang/Object;
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method
