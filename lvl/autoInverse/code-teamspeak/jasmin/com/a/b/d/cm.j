.bytecode 50.0
.class public final synchronized com/a/b/d/cm
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field static final 'a' Lcom/a/b/b/bv;

.method static <clinit>()V
ldc ", "
invokestatic com/a/b/b/bv/a(Ljava/lang/String;)Lcom/a/b/b/bv;
ldc "null"
invokevirtual com/a/b/b/bv/b(Ljava/lang/String;)Lcom/a/b/b/bv;
putstatic com/a/b/d/cm/a Lcom/a/b/b/bv;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Ljava/util/Collection;)Ljava/lang/String;
aload 0
invokeinterface java/util/Collection/size()I 0
invokestatic com/a/b/d/cm/a(I)Ljava/lang/StringBuilder;
bipush 91
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
astore 1
getstatic com/a/b/d/cm/a Lcom/a/b/b/bv;
aload 1
aload 0
new com/a/b/d/cn
dup
aload 0
invokespecial com/a/b/d/cn/<init>(Ljava/util/Collection;)V
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;
invokevirtual com/a/b/b/bv/a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;
pop
aload 1
bipush 93
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 6
.end method

.method static a(I)Ljava/lang/StringBuilder;
iload 0
ldc "size"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
new java/lang/StringBuilder
dup
iload 0
i2l
ldc2_w 8L
lmul
ldc2_w 1073741824L
invokestatic java/lang/Math/min(JJ)J
l2i
invokespecial java/lang/StringBuilder/<init>(I)V
areturn
.limit locals 1
.limit stack 6
.end method

.method static a(Ljava/lang/Iterable;)Ljava/util/Collection;
aload 0
checkcast java/util/Collection
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/Collection;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/d/cp
dup
aload 0
aload 1
invokespecial com/a/b/d/cp/<init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;
new com/a/b/d/ct
dup
aload 0
aload 1
invokespecial com/a/b/d/ct/<init>(Ljava/util/Collection;Lcom/a/b/b/bj;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
aload 0
instanceof com/a/b/d/co
ifeq L0
aload 0
checkcast com/a/b/d/co
astore 0
new com/a/b/d/co
dup
aload 0
getfield com/a/b/d/co/a Ljava/util/Collection;
aload 0
getfield com/a/b/d/co/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokespecial com/a/b/d/co/<init>(Ljava/util/Collection;Lcom/a/b/b/co;)V
areturn
L0:
new com/a/b/d/co
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
invokespecial com/a/b/d/co/<init>(Ljava/util/Collection;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method static synthetic a(J)Z
lload 0
lconst_0
lcmp
iflt L0
lload 0
ldc2_w 2147483647L
lcmp
ifgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method static a(Ljava/util/Collection;Ljava/lang/Object;)Z
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
aload 0
aload 1
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
istore 2
L1:
iload 2
ireturn
L2:
astore 0
iconst_0
ireturn
L3:
astore 0
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method static a(Ljava/util/Collection;Ljava/util/Collection;)Z
aload 1
aload 0
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/e(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Ljava/util/List;Ljava/util/List;)Z
aload 0
invokeinterface java/util/List/size()I 0
aload 1
invokeinterface java/util/List/size()I 0
if_icmpeq L0
iconst_0
ireturn
L0:
aload 0
invokestatic com/a/b/d/ip/a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;
aload 1
invokestatic com/a/b/d/ip/a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;
invokeinterface com/a/b/d/xc/equals(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/Collection;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/d/cp
dup
aload 0
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokespecial com/a/b/d/cp/<init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static b(Ljava/util/Collection;)Ljava/util/Collection;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/d/cr
dup
aload 0
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
invokespecial com/a/b/d/cr/<init>(Lcom/a/b/d/jl;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(J)Z
lload 0
lconst_0
lcmp
iflt L0
lload 0
ldc2_w 2147483647L
lcmp
ifgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method static b(Ljava/util/Collection;Ljava/lang/Object;)Z
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
aload 0
aload 1
invokeinterface java/util/Collection/remove(Ljava/lang/Object;)Z 1
istore 2
L1:
iload 2
ireturn
L2:
astore 0
iconst_0
ireturn
L3:
astore 0
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private static b(Ljava/util/List;Ljava/util/List;)Z
aload 0
invokeinterface java/util/List/size()I 0
aload 1
invokeinterface java/util/List/size()I 0
if_icmpeq L0
iconst_0
ireturn
L0:
aload 0
invokestatic com/a/b/d/ip/a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;
aload 1
invokestatic com/a/b/d/ip/a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;
invokeinterface com/a/b/d/xc/equals(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method
