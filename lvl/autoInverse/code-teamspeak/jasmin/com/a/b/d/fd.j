.bytecode 50.0
.class public final synchronized com/a/b/d/fd
.super com/a/b/d/a
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'd' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "not needed in emulated source."
.end annotation
.end field

.field private transient 'b' Ljava/lang/Class;

.field private transient 'c' Ljava/lang/Class;

.method private <init>(Ljava/lang/Class;Ljava/lang/Class;)V
aload 0
new java/util/EnumMap
dup
aload 1
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
invokestatic com/a/b/d/agm/a(Ljava/util/Map;)Lcom/a/b/d/agm;
new java/util/EnumMap
dup
aload 2
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
invokestatic com/a/b/d/agm/a(Ljava/util/Map;)Lcom/a/b/d/agm;
invokespecial com/a/b/d/a/<init>(Ljava/util/Map;Ljava/util/Map;)V
aload 0
aload 1
putfield com/a/b/d/fd/b Ljava/lang/Class;
aload 0
aload 2
putfield com/a/b/d/fd/c Ljava/lang/Class;
return
.limit locals 3
.limit stack 5
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/a/b/d/fd;
new com/a/b/d/fd
dup
aload 0
aload 1
invokespecial com/a/b/d/fd/<init>(Ljava/lang/Class;Ljava/lang/Class;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static a(Ljava/util/Map;)Ljava/lang/Class;
aload 0
instanceof com/a/b/d/fd
ifeq L0
aload 0
checkcast com/a/b/d/fd
getfield com/a/b/d/fd/b Ljava/lang/Class;
areturn
L0:
aload 0
instanceof com/a/b/d/fe
ifeq L1
aload 0
checkcast com/a/b/d/fe
getfield com/a/b/d/fe/b Ljava/lang/Class;
areturn
L1:
aload 0
invokeinterface java/util/Map/isEmpty()Z 0
ifne L2
iconst_1
istore 1
L3:
iload 1
invokestatic com/a/b/b/cn/a(Z)V
aload 0
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Enum
invokevirtual java/lang/Enum/getDeclaringClass()Ljava/lang/Class;
areturn
L2:
iconst_0
istore 1
goto L3
.limit locals 2
.limit stack 1
.end method

.method private static a(Ljava/lang/Enum;)Ljava/lang/Enum;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Enum
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/lang/Class
putfield com/a/b/d/fd/b Ljava/lang/Class;
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/lang/Class
putfield com/a/b/d/fd/c Ljava/lang/Class;
aload 0
new java/util/EnumMap
dup
aload 0
getfield com/a/b/d/fd/b Ljava/lang/Class;
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
invokestatic com/a/b/d/agm/a(Ljava/util/Map;)Lcom/a/b/d/agm;
new java/util/EnumMap
dup
aload 0
getfield com/a/b/d/fd/c Ljava/lang/Class;
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
invokestatic com/a/b/d/agm/a(Ljava/util/Map;)Lcom/a/b/d/agm;
invokevirtual com/a/b/d/fd/a(Ljava/util/Map;Ljava/util/Map;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Ljava/util/Map;Ljava/io/ObjectInputStream;)V
return
.limit locals 2
.limit stack 5
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/d/fd/b Ljava/lang/Class;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 1
aload 0
getfield com/a/b/d/fd/c Ljava/lang/Class;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Ljava/util/Map;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/util/Map;)Lcom/a/b/d/fd;
aload 0
invokestatic com/a/b/d/fd/a(Ljava/util/Map;)Ljava/lang/Class;
astore 3
aload 0
instanceof com/a/b/d/fd
ifeq L0
aload 0
checkcast com/a/b/d/fd
getfield com/a/b/d/fd/c Ljava/lang/Class;
astore 2
L1:
new com/a/b/d/fd
dup
aload 3
aload 2
invokespecial com/a/b/d/fd/<init>(Ljava/lang/Class;Ljava/lang/Class;)V
astore 2
aload 2
aload 0
invokevirtual com/a/b/d/fd/putAll(Ljava/util/Map;)V
aload 2
areturn
L0:
aload 0
invokeinterface java/util/Map/isEmpty()Z 0
ifne L2
iconst_1
istore 1
L3:
iload 1
invokestatic com/a/b/b/cn/a(Z)V
aload 0
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Enum
invokevirtual java/lang/Enum/getDeclaringClass()Ljava/lang/Class;
astore 2
goto L1
L2:
iconst_0
istore 1
goto L3
.limit locals 4
.limit stack 4
.end method

.method private static b(Ljava/lang/Enum;)Ljava/lang/Enum;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Enum
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/util/Map;)Ljava/lang/Class;
aload 0
instanceof com/a/b/d/fd
ifeq L0
aload 0
checkcast com/a/b/d/fd
getfield com/a/b/d/fd/c Ljava/lang/Class;
areturn
L0:
aload 0
invokeinterface java/util/Map/isEmpty()Z 0
ifne L1
iconst_1
istore 1
L2:
iload 1
invokestatic com/a/b/b/cn/a(Z)V
aload 0
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Enum
invokevirtual java/lang/Enum/getDeclaringClass()Ljava/lang/Class;
areturn
L1:
iconst_0
istore 1
goto L2
.limit locals 2
.limit stack 1
.end method

.method private d()Ljava/lang/Class;
aload 0
getfield com/a/b/d/fd/b Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/Class;
aload 0
getfield com/a/b/d/fd/c Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/Enum
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Enum
areturn
.limit locals 2
.limit stack 1
.end method

.method public final volatile synthetic b()Lcom/a/b/d/bw;
aload 0
invokespecial com/a/b/d/a/b()Lcom/a/b/d/bw;
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/Enum
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Enum
areturn
.limit locals 2
.limit stack 1
.end method

.method public final volatile synthetic clear()V
aload 0
invokespecial com/a/b/d/a/clear()V
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic containsValue(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/a/containsValue(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic entrySet()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/a/entrySet()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic j_()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/a/j_()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic keySet()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/a/keySet()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic putAll(Ljava/util/Map;)V
aload 0
aload 1
invokespecial com/a/b/d/a/putAll(Ljava/util/Map;)V
return
.limit locals 2
.limit stack 2
.end method
