.bytecode 50.0
.class synchronized abstract com/a/b/d/a
.super com/a/b/d/gs
.implements com/a/b/d/bw
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'f' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "Not needed in emulated source."
.end annotation
.end field

.field transient 'a' Lcom/a/b/d/a;

.field private transient 'b' Ljava/util/Map;

.field private transient 'c' Ljava/util/Set;

.field private transient 'd' Ljava/util/Set;

.field private transient 'e' Ljava/util/Set;

.method private <init>(Ljava/util/Map;Lcom/a/b/d/a;)V
aload 0
invokespecial com/a/b/d/gs/<init>()V
aload 0
aload 1
putfield com/a/b/d/a/b Ljava/util/Map;
aload 0
aload 2
putfield com/a/b/d/a/a Lcom/a/b/d/a;
return
.limit locals 3
.limit stack 2
.end method

.method synthetic <init>(Ljava/util/Map;Lcom/a/b/d/a;B)V
aload 0
aload 1
aload 2
invokespecial com/a/b/d/a/<init>(Ljava/util/Map;Lcom/a/b/d/a;)V
return
.limit locals 4
.limit stack 3
.end method

.method <init>(Ljava/util/Map;Ljava/util/Map;)V
aload 0
invokespecial com/a/b/d/gs/<init>()V
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/a/a(Ljava/util/Map;Ljava/util/Map;)V
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/d/a;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokespecial com/a/b/d/a/d(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/d/a/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 2
invokevirtual com/a/b/d/a/b(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
invokevirtual com/a/b/d/a/containsKey(Ljava/lang/Object;)Z
istore 4
iload 4
ifeq L0
aload 2
aload 0
aload 1
invokevirtual com/a/b/d/a/get(Ljava/lang/Object;)Ljava/lang/Object;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 2
areturn
L0:
iload 3
ifeq L1
aload 0
invokevirtual com/a/b/d/a/b()Lcom/a/b/d/bw;
aload 2
invokeinterface com/a/b/d/bw/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
L2:
aload 0
getfield com/a/b/d/a/b Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
astore 5
aload 0
aload 1
iload 4
aload 5
aload 2
invokespecial com/a/b/d/a/a(Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V
aload 5
areturn
L1:
aload 0
aload 2
invokevirtual com/a/b/d/a/containsValue(Ljava/lang/Object;)Z
ifne L3
iconst_1
istore 3
L4:
iload 3
ldc "value already present: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
goto L2
L3:
iconst_0
istore 3
goto L4
.limit locals 6
.limit stack 6
.end method

.method static synthetic a(Lcom/a/b/d/a;)Ljava/util/Map;
aload 0
getfield com/a/b/d/a/b Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/a/b/d/a;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
aload 1
iconst_1
aload 2
aload 3
invokespecial com/a/b/d/a/a(Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V
return
.limit locals 4
.limit stack 5
.end method

.method private a(Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V
iload 2
ifeq L0
aload 0
aload 3
invokespecial com/a/b/d/a/e(Ljava/lang/Object;)V
L0:
aload 0
getfield com/a/b/d/a/a Lcom/a/b/d/a;
getfield com/a/b/d/a/b Ljava/util/Map;
aload 4
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 5
.limit stack 3
.end method

.method private b(Lcom/a/b/d/a;)V
aload 0
aload 1
putfield com/a/b/d/a/a Lcom/a/b/d/a;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/a/b/d/a;Ljava/lang/Object;)V
aload 0
aload 1
invokespecial com/a/b/d/a/e(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method private d(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/a/b Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
aload 0
aload 1
invokespecial com/a/b/d/a/e(Ljava/lang/Object;)V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private e(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/a/a Lcom/a/b/d/a;
getfield com/a/b/d/a/b Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method a(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
iconst_1
invokespecial com/a/b/d/a/a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 4
.end method

.method protected final a()Ljava/util/Map;
aload 0
getfield com/a/b/d/a/b Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a(Ljava/util/Map;Ljava/util/Map;)V
iconst_1
istore 4
aload 0
getfield com/a/b/d/a/b Ljava/util/Map;
ifnonnull L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/b(Z)V
aload 0
getfield com/a/b/d/a/a Lcom/a/b/d/a;
ifnonnull L2
iconst_1
istore 3
L3:
iload 3
invokestatic com/a/b/b/cn/b(Z)V
aload 1
invokeinterface java/util/Map/isEmpty()Z 0
invokestatic com/a/b/b/cn/a(Z)V
aload 2
invokeinterface java/util/Map/isEmpty()Z 0
invokestatic com/a/b/b/cn/a(Z)V
aload 1
aload 2
if_acmpeq L4
iload 4
istore 3
L5:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
aload 0
aload 1
putfield com/a/b/d/a/b Ljava/util/Map;
aload 0
new com/a/b/d/f
dup
aload 2
aload 0
iconst_0
invokespecial com/a/b/d/f/<init>(Ljava/util/Map;Lcom/a/b/d/a;B)V
putfield com/a/b/d/a/a Lcom/a/b/d/a;
return
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
L4:
iconst_0
istore 3
goto L5
.limit locals 5
.limit stack 6
.end method

.method public b()Lcom/a/b/d/bw;
aload 0
getfield com/a/b/d/a/a Lcom/a/b/d/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method b(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method public clear()V
aload 0
getfield com/a/b/d/a/b Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
aload 0
getfield com/a/b/d/a/a Lcom/a/b/d/a;
getfield com/a/b/d/a/b Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public containsValue(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/a/a Lcom/a/b/d/a;
aload 1
invokevirtual com/a/b/d/a/containsKey(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public entrySet()Ljava/util/Set;
aload 0
getfield com/a/b/d/a/e Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/c
dup
aload 0
iconst_0
invokespecial com/a/b/d/c/<init>(Lcom/a/b/d/a;B)V
astore 1
aload 0
aload 1
putfield com/a/b/d/a/e Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public j_()Ljava/util/Set;
aload 0
getfield com/a/b/d/a/d Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/h
dup
aload 0
iconst_0
invokespecial com/a/b/d/h/<init>(Lcom/a/b/d/a;B)V
astore 1
aload 0
aload 1
putfield com/a/b/d/a/d Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method protected volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/a/b Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public keySet()Ljava/util/Set;
aload 0
getfield com/a/b/d/a/c Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/g
dup
aload 0
iconst_0
invokespecial com/a/b/d/g/<init>(Lcom/a/b/d/a;B)V
astore 1
aload 0
aload 1
putfield com/a/b/d/a/c Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
iconst_0
invokespecial com/a/b/d/a/a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 4
.end method

.method public putAll(Ljava/util/Map;)V
aload 1
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 2
aload 0
aload 2
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 2
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual com/a/b/d/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/d/a/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
aload 1
invokespecial com/a/b/d/a/d(Ljava/lang/Object;)Ljava/lang/Object;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public synthetic values()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/a/j_()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method
