.bytecode 50.0
.class synchronized abstract com/a/b/d/ai
.super com/a/b/d/as
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'c' J = -2250766705698539974L

.annotation invisible Lcom/a/b/a/c;
a s = "not needed in emulated source."
.end annotation
.end field

.field transient 'a' Ljava/util/Map;

.field private transient 'b' J

.method protected <init>(Ljava/util/Map;)V
aload 0
invokespecial com/a/b/d/as/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
putfield com/a/b/d/ai/a Ljava/util/Map;
aload 0
aload 0
invokespecial com/a/b/d/as/size()I
i2l
putfield com/a/b/d/ai/b J
return
.limit locals 2
.limit stack 3
.end method

.method private static a(Lcom/a/b/d/dv;I)I
aload 0
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
iload 1
invokevirtual com/a/b/d/dv/b(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/ai;J)J
aload 0
getfield com/a/b/d/ai/b J
lload 1
lsub
lstore 1
aload 0
lload 1
putfield com/a/b/d/ai/b J
lload 1
lreturn
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/d/ai;)Ljava/util/Map;
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/util/Map;)V
aload 0
aload 1
putfield com/a/b/d/ai/a Ljava/util/Map;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/a/b/d/ai;)J
aload 0
getfield com/a/b/d/ai/b J
lstore 1
aload 0
lload 1
lconst_1
lsub
putfield com/a/b/d/ai/b J
lload 1
lreturn
.limit locals 3
.limit stack 5
.end method

.method private static g()V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectStreamException"
.end annotation
new java/io/InvalidObjectException
dup
ldc "Stream data required"
invokespecial java/io/InvalidObjectException/<init>(Ljava/lang/String;)V
athrow
.limit locals 0
.limit stack 3
.end method

.method public a(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/dv
astore 1
aload 1
ifnonnull L0
iconst_0
ireturn
L0:
aload 1
getfield com/a/b/d/dv/a I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public a(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 2
ifne L0
aload 0
aload 1
invokevirtual com/a/b/d/ai/a(Ljava/lang/Object;)I
ireturn
L0:
iload 2
ifle L1
iconst_1
istore 4
L2:
iload 4
ldc "occurrences cannot be negative: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/dv
astore 7
aload 7
ifnonnull L3
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
aload 1
new com/a/b/d/dv
dup
iload 2
invokespecial com/a/b/d/dv/<init>(I)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L4:
aload 0
aload 0
getfield com/a/b/d/ai/b J
iload 2
i2l
ladd
putfield com/a/b/d/ai/b J
iload 3
ireturn
L1:
iconst_0
istore 4
goto L2
L3:
aload 7
getfield com/a/b/d/dv/a I
istore 3
iload 3
i2l
iload 2
i2l
ladd
lstore 5
lload 5
ldc2_w 2147483647L
lcmp
ifgt L5
iconst_1
istore 4
L6:
iload 4
ldc "too many occurrences: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 5
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 7
aload 7
getfield com/a/b/d/dv/a I
iload 2
iadd
putfield com/a/b/d/dv/a I
goto L4
L5:
iconst_0
istore 4
goto L6
.limit locals 8
.limit stack 7
.end method

.method public a()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/as/a()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public b(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 2
ifne L0
aload 0
aload 1
invokevirtual com/a/b/d/ai/a(Ljava/lang/Object;)I
istore 3
L1:
iload 3
ireturn
L0:
iload 2
ifle L2
iconst_1
istore 5
L3:
iload 5
ldc "occurrences cannot be negative: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/dv
astore 6
aload 6
ifnull L1
aload 6
getfield com/a/b/d/dv/a I
istore 4
iload 2
istore 3
iload 4
iload 2
if_icmpgt L4
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
iload 4
istore 3
L4:
aload 6
iload 3
ineg
invokevirtual com/a/b/d/dv/a(I)I
pop
aload 0
aload 0
getfield com/a/b/d/ai/b J
iload 3
i2l
lsub
putfield com/a/b/d/ai/b J
iload 4
ireturn
L2:
iconst_0
istore 5
goto L3
.limit locals 7
.limit stack 6
.end method

.method final b()Ljava/util/Iterator;
new com/a/b/d/aj
dup
aload 0
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/aj/<init>(Lcom/a/b/d/ai;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method final c()I
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public c(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iload 2
ldc "count"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
iload 2
ifne L0
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/dv
iload 2
invokestatic com/a/b/d/ai/a(Lcom/a/b/d/dv;I)I
istore 3
L1:
aload 0
aload 0
getfield com/a/b/d/ai/b J
iload 2
iload 3
isub
i2l
ladd
putfield com/a/b/d/ai/b J
iload 3
ireturn
L0:
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/dv
astore 4
aload 4
iload 2
invokestatic com/a/b/d/ai/a(Lcom/a/b/d/dv;I)I
istore 3
aload 4
ifnonnull L2
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
aload 1
new com/a/b/d/dv
dup
iload 2
invokespecial com/a/b/d/dv/<init>(I)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L2:
goto L1
.limit locals 5
.limit stack 5
.end method

.method public clear()V
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/dv
iconst_0
putfield com/a/b/d/dv/a I
goto L0
L1:
aload 0
getfield com/a/b/d/ai/a Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
aload 0
lconst_0
putfield com/a/b/d/ai/b J
return
.limit locals 2
.limit stack 3
.end method

.method public iterator()Ljava/util/Iterator;
new com/a/b/d/al
dup
aload 0
invokespecial com/a/b/d/al/<init>(Lcom/a/b/d/ai;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public size()I
aload 0
getfield com/a/b/d/ai/b J
invokestatic com/a/b/l/q/b(J)I
ireturn
.limit locals 1
.limit stack 2
.end method
