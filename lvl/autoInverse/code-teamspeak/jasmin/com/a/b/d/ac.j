.bytecode 50.0
.class synchronized com/a/b/d/ac
.super java/lang/Object
.implements java/util/Iterator

.field final 'a' Ljava/util/Iterator;

.field final 'b' Ljava/util/Collection;

.field final synthetic 'c' Lcom/a/b/d/ab;

.method <init>(Lcom/a/b/d/ab;)V
aload 0
aload 1
putfield com/a/b/d/ac/c Lcom/a/b/d/ab;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 0
getfield com/a/b/d/ac/c Lcom/a/b/d/ab;
getfield com/a/b/d/ab/c Ljava/util/Collection;
putfield com/a/b/d/ac/b Ljava/util/Collection;
aload 0
aload 1
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokestatic com/a/b/d/n/b(Ljava/util/Collection;)Ljava/util/Iterator;
putfield com/a/b/d/ac/a Ljava/util/Iterator;
return
.limit locals 2
.limit stack 2
.end method

.method <init>(Lcom/a/b/d/ab;Ljava/util/Iterator;)V
aload 0
aload 1
putfield com/a/b/d/ac/c Lcom/a/b/d/ab;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 0
getfield com/a/b/d/ac/c Lcom/a/b/d/ab;
getfield com/a/b/d/ab/c Ljava/util/Collection;
putfield com/a/b/d/ac/b Ljava/util/Collection;
aload 0
aload 2
putfield com/a/b/d/ac/a Ljava/util/Iterator;
return
.limit locals 3
.limit stack 2
.end method

.method private b()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/ac/a()V
aload 0
getfield com/a/b/d/ac/a Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a()V
aload 0
getfield com/a/b/d/ac/c Lcom/a/b/d/ab;
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ac/c Lcom/a/b/d/ab;
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 0
getfield com/a/b/d/ac/b Ljava/util/Collection;
if_acmpeq L0
new java/util/ConcurrentModificationException
dup
invokespecial java/util/ConcurrentModificationException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public hasNext()Z
aload 0
invokevirtual com/a/b/d/ac/a()V
aload 0
getfield com/a/b/d/ac/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/hasNext()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public next()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ac/a()V
aload 0
getfield com/a/b/d/ac/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public remove()V
aload 0
getfield com/a/b/d/ac/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/remove()V 0
aload 0
getfield com/a/b/d/ac/c Lcom/a/b/d/ab;
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
invokestatic com/a/b/d/n/b(Lcom/a/b/d/n;)I
pop
aload 0
getfield com/a/b/d/ac/c Lcom/a/b/d/ab;
invokevirtual com/a/b/d/ab/b()V
return
.limit locals 1
.limit stack 1
.end method
