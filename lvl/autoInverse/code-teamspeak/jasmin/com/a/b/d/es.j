.bytecode 50.0
.class final synchronized com/a/b/d/es
.super com/a/b/d/ep
.implements java/io/Serializable

.field private static final 'a' Lcom/a/b/d/es;

.field private static final 'b' J = 0L


.method static <clinit>()V
new com/a/b/d/es
dup
invokespecial com/a/b/d/es/<init>()V
putstatic com/a/b/d/es/a Lcom/a/b/d/es;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial com/a/b/d/ep/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Long;Ljava/lang/Long;)J
aload 1
invokevirtual java/lang/Long/longValue()J
aload 0
invokevirtual java/lang/Long/longValue()J
lsub
lstore 4
aload 1
invokevirtual java/lang/Long/longValue()J
aload 0
invokevirtual java/lang/Long/longValue()J
lcmp
ifle L0
lload 4
lconst_0
lcmp
ifge L0
ldc2_w 9223372036854775807L
lstore 2
L1:
lload 2
lreturn
L0:
lload 4
lstore 2
aload 1
invokevirtual java/lang/Long/longValue()J
aload 0
invokevirtual java/lang/Long/longValue()J
lcmp
ifge L1
lload 4
lstore 2
lload 4
lconst_0
lcmp
ifle L1
ldc2_w -9223372036854775808L
lreturn
.limit locals 6
.limit stack 4
.end method

.method private static a(Ljava/lang/Long;)Ljava/lang/Long;
aload 0
invokevirtual java/lang/Long/longValue()J
lstore 1
lload 1
ldc2_w 9223372036854775807L
lcmp
ifne L0
aconst_null
areturn
L0:
lload 1
lconst_1
ladd
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 3
.limit stack 4
.end method

.method private static b(Ljava/lang/Long;)Ljava/lang/Long;
aload 0
invokevirtual java/lang/Long/longValue()J
lstore 1
lload 1
ldc2_w -9223372036854775808L
lcmp
ifne L0
aconst_null
areturn
L0:
lload 1
lconst_1
lsub
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 3
.limit stack 4
.end method

.method static synthetic c()Lcom/a/b/d/es;
getstatic com/a/b/d/es/a Lcom/a/b/d/es;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static d()Ljava/lang/Long;
ldc2_w -9223372036854775808L
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 0
.limit stack 2
.end method

.method private static e()Ljava/lang/Long;
ldc2_w 9223372036854775807L
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 0
.limit stack 2
.end method

.method private static f()Ljava/lang/Object;
getstatic com/a/b/d/es/a Lcom/a/b/d/es;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final synthetic a(Ljava/lang/Comparable;Ljava/lang/Comparable;)J
aload 1
checkcast java/lang/Long
astore 1
aload 2
checkcast java/lang/Long
astore 2
aload 2
invokevirtual java/lang/Long/longValue()J
aload 1
invokevirtual java/lang/Long/longValue()J
lsub
lstore 5
aload 2
invokevirtual java/lang/Long/longValue()J
aload 1
invokevirtual java/lang/Long/longValue()J
lcmp
ifle L0
lload 5
lconst_0
lcmp
ifge L0
ldc2_w 9223372036854775807L
lstore 3
L1:
lload 3
lreturn
L0:
lload 5
lstore 3
aload 2
invokevirtual java/lang/Long/longValue()J
aload 1
invokevirtual java/lang/Long/longValue()J
lcmp
ifge L1
lload 5
lstore 3
lload 5
lconst_0
lcmp
ifle L1
ldc2_w -9223372036854775808L
lreturn
.limit locals 7
.limit stack 4
.end method

.method public final synthetic a()Ljava/lang/Comparable;
ldc2_w -9223372036854775808L
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final synthetic a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
aload 1
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
lstore 2
lload 2
ldc2_w 9223372036854775807L
lcmp
ifne L0
aconst_null
areturn
L0:
lload 2
lconst_1
ladd
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final synthetic b()Ljava/lang/Comparable;
ldc2_w 9223372036854775807L
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final synthetic b(Ljava/lang/Comparable;)Ljava/lang/Comparable;
aload 1
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
lstore 2
lload 2
ldc2_w -9223372036854775808L
lcmp
ifne L0
aconst_null
areturn
L0:
lload 2
lconst_1
lsub
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
ldc "DiscreteDomain.longs()"
areturn
.limit locals 1
.limit stack 1
.end method
