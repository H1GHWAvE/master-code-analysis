.bytecode 50.0
.class final synchronized com/a/b/d/dn
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
aload 0
instanceof java/util/SortedSet
ifeq L0
aload 0
checkcast java/util/SortedSet
aload 1
invokestatic com/a/b/d/dn/a(Ljava/util/SortedSet;Lcom/a/b/d/dm;)Ljava/util/SortedSet;
areturn
L0:
aload 0
instanceof java/util/Set
ifeq L1
new com/a/b/d/ds
dup
aload 0
checkcast java/util/Set
aload 1
invokespecial com/a/b/d/ds/<init>(Ljava/util/Set;Lcom/a/b/d/dm;)V
areturn
L1:
aload 0
instanceof java/util/List
ifeq L2
aload 0
checkcast java/util/List
aload 1
invokestatic com/a/b/d/dn/a(Ljava/util/List;Lcom/a/b/d/dm;)Ljava/util/List;
areturn
L2:
new com/a/b/d/do
dup
aload 0
aload 1
invokespecial com/a/b/d/do/<init>(Ljava/util/Collection;Lcom/a/b/d/dm;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/util/List;Lcom/a/b/d/dm;)Ljava/util/List;
aload 0
instanceof java/util/RandomAccess
ifeq L0
new com/a/b/d/dr
dup
aload 0
aload 1
invokespecial com/a/b/d/dr/<init>(Ljava/util/List;Lcom/a/b/d/dm;)V
areturn
L0:
new com/a/b/d/dp
dup
aload 0
aload 1
invokespecial com/a/b/d/dp/<init>(Ljava/util/List;Lcom/a/b/d/dm;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Ljava/util/ListIterator;Lcom/a/b/d/dm;)Ljava/util/ListIterator;
new com/a/b/d/dq
dup
aload 0
aload 1
invokespecial com/a/b/d/dq/<init>(Ljava/util/ListIterator;Lcom/a/b/d/dm;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/Set;Lcom/a/b/d/dm;)Ljava/util/Set;
new com/a/b/d/ds
dup
aload 0
aload 1
invokespecial com/a/b/d/ds/<init>(Ljava/util/Set;Lcom/a/b/d/dm;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/util/SortedSet;Lcom/a/b/d/dm;)Ljava/util/SortedSet;
new com/a/b/d/dt
dup
aload 0
aload 1
invokespecial com/a/b/d/dt/<init>(Ljava/util/SortedSet;Lcom/a/b/d/dm;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic b(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
aload 0
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
astore 0
aload 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface com/a/b/d/dm/a(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
goto L0
L1:
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method private static b(Ljava/util/ListIterator;Lcom/a/b/d/dm;)Ljava/util/ListIterator;
new com/a/b/d/dq
dup
aload 0
aload 1
invokespecial com/a/b/d/dq/<init>(Ljava/util/ListIterator;Lcom/a/b/d/dm;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static c(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
new com/a/b/d/do
dup
aload 0
aload 1
invokespecial com/a/b/d/do/<init>(Ljava/util/Collection;Lcom/a/b/d/dm;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static d(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
aload 0
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
astore 0
aload 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface com/a/b/d/dm/a(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
goto L0
L1:
aload 0
areturn
.limit locals 3
.limit stack 2
.end method
