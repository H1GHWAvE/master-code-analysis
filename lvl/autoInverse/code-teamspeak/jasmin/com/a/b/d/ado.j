.bytecode 50.0
.class synchronized com/a/b/d/ado
.super com/a/b/d/add
.implements java/util/Queue

.field private static final 'a' J = 0L


.method <init>(Ljava/util/Queue;)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial com/a/b/d/add/<init>(Ljava/util/Collection;Ljava/lang/Object;B)V
return
.limit locals 2
.limit stack 4
.end method

.method a()Ljava/util/Queue;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/Queue
areturn
.limit locals 1
.limit stack 1
.end method

.method synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/ado/a()Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method synthetic d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ado/a()Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method public element()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ado/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ado/a()Ljava/util/Queue;
invokeinterface java/util/Queue/element()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public offer(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ado/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ado/a()Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/offer(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public peek()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ado/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ado/a()Ljava/util/Queue;
invokeinterface java/util/Queue/peek()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public poll()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ado/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ado/a()Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public remove()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ado/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/ado/a()Ljava/util/Queue;
invokeinterface java/util/Queue/remove()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method
