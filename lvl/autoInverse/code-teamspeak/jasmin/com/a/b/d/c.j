.bytecode 50.0
.class final synchronized com/a/b/d/c
.super com/a/b/d/hi

.field final 'a' Ljava/util/Set;

.field final synthetic 'b' Lcom/a/b/d/a;

.method private <init>(Lcom/a/b/d/a;)V
aload 0
aload 1
putfield com/a/b/d/c/b Lcom/a/b/d/a;
aload 0
invokespecial com/a/b/d/hi/<init>()V
aload 0
aload 0
getfield com/a/b/d/c/b Lcom/a/b/d/a;
invokestatic com/a/b/d/a/a(Lcom/a/b/d/a;)Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
putfield com/a/b/d/c/a Ljava/util/Set;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/d/a;B)V
aload 0
aload 1
invokespecial com/a/b/d/c/<init>(Lcom/a/b/d/a;)V
return
.limit locals 3
.limit stack 2
.end method

.method protected final a()Ljava/util/Set;
aload 0
getfield com/a/b/d/c/a Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final volatile synthetic b()Ljava/util/Collection;
aload 0
getfield com/a/b/d/c/a Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final clear()V
aload 0
getfield com/a/b/d/c/b Lcom/a/b/d/a;
invokevirtual com/a/b/d/a/clear()V
return
.limit locals 1
.limit stack 1
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/c/a Ljava/util/Set;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Collection;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final containsAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final iterator()Ljava/util/Iterator;
new com/a/b/d/d
dup
aload 0
aload 0
getfield com/a/b/d/c/a Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/d/<init>(Lcom/a/b/d/c;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/c/a Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final remove(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/c/a Ljava/util/Set;
aload 1
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L0
iconst_0
ireturn
L0:
aload 1
checkcast java/util/Map$Entry
astore 1
aload 0
getfield com/a/b/d/c/b Lcom/a/b/d/a;
getfield com/a/b/d/a/a Lcom/a/b/d/a;
invokestatic com/a/b/d/a/a(Lcom/a/b/d/a;)Ljava/util/Map;
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
aload 0
getfield com/a/b/d/c/a Ljava/util/Set;
aload 1
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
pop
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final removeAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokevirtual com/a/b/d/c/b(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final retainAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokevirtual com/a/b/d/c/c(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final toArray()[Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/c/p()[Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
aload 1
invokestatic com/a/b/d/yc/a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method
