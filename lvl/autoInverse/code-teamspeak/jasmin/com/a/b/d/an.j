.bytecode 50.0
.class synchronized abstract com/a/b/d/an
.super java/lang/Object
.implements com/a/b/d/vi
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private transient 'a' Ljava/util/Collection;

.field private transient 'b' Ljava/util/Set;

.field private transient 'c' Lcom/a/b/d/xc;

.field private transient 'd' Ljava/util/Collection;

.field private transient 'e' Ljava/util/Map;

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public a(Lcom/a/b/d/vi;)Z
aload 1
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
iconst_0
istore 2
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 0
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual com/a/b/d/an/a(Ljava/lang/Object;Ljava/lang/Object;)Z
iload 2
ior
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 4
.limit stack 3
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/d/an/c(Ljava/lang/Object;)Ljava/util/Collection;
aload 2
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
invokevirtual com/a/b/d/an/d(Ljava/lang/Object;)Ljava/util/Collection;
astore 3
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/an/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
pop
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method public b()Ljava/util/Map;
aload 0
getfield com/a/b/d/an/e Ljava/util/Map;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/an/m()Ljava/util/Map;
astore 1
aload 0
aload 1
putfield com/a/b/d/an/e Ljava/util/Map;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/an/b()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 1
aload 1
ifnull L0
aload 1
aload 2
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
instanceof java/util/Collection
ifeq L0
aload 2
checkcast java/util/Collection
astore 2
aload 2
invokeinterface java/util/Collection/isEmpty()Z 0
ifne L1
aload 0
aload 1
invokevirtual com/a/b/d/an/c(Ljava/lang/Object;)Ljava/util/Collection;
aload 2
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
ifeq L1
L2:
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 0
aload 1
invokevirtual com/a/b/d/an/c(Ljava/lang/Object;)Ljava/util/Collection;
aload 2
invokestatic com/a/b/d/nj/a(Ljava/util/Collection;Ljava/util/Iterator;)Z
ifne L2
L3:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/an/b()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 1
aload 1
ifnull L0
aload 1
aload 2
invokeinterface java/util/Collection/remove(Ljava/lang/Object;)Z 1
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpne L0
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/d/vi
ifeq L1
aload 1
checkcast com/a/b/d/vi
astore 1
aload 0
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
aload 1
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokeinterface java/util/Map/equals(Ljava/lang/Object;)Z 1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public g(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/an/b()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Collection
aload 1
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
ifeq L0
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method h()Ljava/util/Set;
new com/a/b/d/uk
dup
aload 0
invokevirtual com/a/b/d/an/b()Ljava/util/Map;
invokespecial com/a/b/d/uk/<init>(Ljava/util/Map;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/an/b()Ljava/util/Map;
invokeinterface java/util/Map/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public i()Ljava/util/Collection;
aload 0
getfield com/a/b/d/an/d Ljava/util/Collection;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/an/s()Ljava/util/Collection;
astore 1
aload 0
aload 1
putfield com/a/b/d/an/d Ljava/util/Collection;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method j()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/an/k()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/sz/b(Ljava/util/Iterator;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public k()Ljava/util/Collection;
aload 0
getfield com/a/b/d/an/a Ljava/util/Collection;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/an/o()Ljava/util/Collection;
astore 1
aload 0
aload 1
putfield com/a/b/d/an/a Ljava/util/Collection;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method abstract l()Ljava/util/Iterator;
.end method

.method abstract m()Ljava/util/Map;
.end method

.method public n()Z
aload 0
invokevirtual com/a/b/d/an/f()I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method o()Ljava/util/Collection;
aload 0
instanceof com/a/b/d/aac
ifeq L0
new com/a/b/d/aq
dup
aload 0
iconst_0
invokespecial com/a/b/d/aq/<init>(Lcom/a/b/d/an;B)V
areturn
L0:
new com/a/b/d/ap
dup
aload 0
iconst_0
invokespecial com/a/b/d/ap/<init>(Lcom/a/b/d/an;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public p()Ljava/util/Set;
aload 0
getfield com/a/b/d/an/b Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/an/h()Ljava/util/Set;
astore 1
aload 0
aload 1
putfield com/a/b/d/an/b Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public q()Lcom/a/b/d/xc;
aload 0
getfield com/a/b/d/an/c Lcom/a/b/d/xc;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/an/r()Lcom/a/b/d/xc;
astore 1
aload 0
aload 1
putfield com/a/b/d/an/c Lcom/a/b/d/xc;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method r()Lcom/a/b/d/xc;
new com/a/b/d/wn
dup
aload 0
invokespecial com/a/b/d/wn/<init>(Lcom/a/b/d/vi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method s()Ljava/util/Collection;
new com/a/b/d/ar
dup
aload 0
invokespecial com/a/b/d/ar/<init>(Lcom/a/b/d/an;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public toString()Ljava/lang/String;
aload 0
invokevirtual com/a/b/d/an/b()Ljava/util/Map;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
