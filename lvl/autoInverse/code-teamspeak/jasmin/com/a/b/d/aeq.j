.bytecode 50.0
.class public final synchronized com/a/b/d/aeq
.super com/a/b/d/bb
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private static final 'c' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "not needed in emulated source"
.end annotation
.end field

.field private transient 'a' Ljava/util/Comparator;

.field private transient 'b' Ljava/util/Comparator;

.method private <init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
aload 0
new java/util/TreeMap
dup
aload 1
invokespecial java/util/TreeMap/<init>(Ljava/util/Comparator;)V
invokespecial com/a/b/d/bb/<init>(Ljava/util/SortedMap;)V
aload 0
aload 1
putfield com/a/b/d/aeq/a Ljava/util/Comparator;
aload 0
aload 2
putfield com/a/b/d/aeq/b Ljava/util/Comparator;
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Ljava/util/Comparator;Ljava/util/Comparator;Lcom/a/b/d/vi;)V
aload 0
aload 1
aload 2
invokespecial com/a/b/d/aeq/<init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
aload 0
aload 3
invokevirtual com/a/b/d/aeq/a(Lcom/a/b/d/vi;)Z
pop
return
.limit locals 4
.limit stack 3
.end method

.method private A()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/aeq/a Ljava/util/Comparator;
areturn
.limit locals 1
.limit stack 1
.end method

.method private B()Ljava/util/NavigableMap;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableMap"
.end annotation
aload 0
invokespecial com/a/b/d/bb/w()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method private C()Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
aload 0
invokespecial com/a/b/d/bb/x()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method private D()Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
new com/a/b/d/x
dup
aload 0
aload 0
invokespecial com/a/b/d/bb/w()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokespecial com/a/b/d/x/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private E()Ljava/util/NavigableMap;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableMap"
.end annotation
aload 0
invokespecial com/a/b/d/bb/v()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method private F()Ljava/util/NavigableMap;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableMap"
.end annotation
new com/a/b/d/w
dup
aload 0
aload 0
invokespecial com/a/b/d/bb/w()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokespecial com/a/b/d/w/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/aeq;
new com/a/b/d/aeq
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
invokespecial com/a/b/d/aeq/<init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/util/Comparator
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
putfield com/a/b/d/aeq/a Ljava/util/Comparator;
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/util/Comparator
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
putfield com/a/b/d/aeq/b Ljava/util/Comparator;
aload 0
new java/util/TreeMap
dup
aload 0
getfield com/a/b/d/aeq/a Ljava/util/Comparator;
invokespecial java/util/TreeMap/<init>(Ljava/util/Comparator;)V
invokevirtual com/a/b/d/aeq/a(Ljava/util/Map;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/vi;Ljava/io/ObjectInputStream;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/d/aeq/a Ljava/util/Comparator;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 1
aload 0
invokevirtual com/a/b/d/aeq/d_()Ljava/util/Comparator;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/aeq;
new com/a/b/d/aeq
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
aload 0
invokespecial com/a/b/d/aeq/<init>(Ljava/util/Comparator;Ljava/util/Comparator;Lcom/a/b/d/vi;)V
areturn
.limit locals 1
.limit stack 5
.end method

.method private j(Ljava/lang/Object;)Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokespecial com/a/b/d/bb/h(Ljava/lang/Object;)Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 2
.limit stack 2
.end method

.method private static z()Lcom/a/b/d/aeq;
new com/a/b/d/aeq
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokespecial com/a/b/d/aeq/<init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
areturn
.limit locals 0
.limit stack 4
.end method

.method final a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
new com/a/b/d/af
dup
aload 0
aload 1
aload 2
checkcast java/util/NavigableSet
aconst_null
invokespecial com/a/b/d/af/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/NavigableSet;Lcom/a/b/d/ab;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method final a(Ljava/util/Collection;)Ljava/util/Collection;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
aload 1
checkcast java/util/NavigableSet
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 1
.end method

.method final synthetic a()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/aeq/y()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/util/Set;
aload 0
aload 1
invokespecial com/a/b/d/aeq/j(Ljava/lang/Object;)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Lcom/a/b/d/vi;)Z
aload 0
aload 1
invokespecial com/a/b/d/bb/a(Lcom/a/b/d/vi;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/bb/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic b()Ljava/util/Map;
aload 0
invokespecial com/a/b/d/bb/v()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/bb/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method final synthetic c()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/aeq/y()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokespecial com/a/b/d/aeq/j(Ljava/lang/Object;)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/bb/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/bb/c(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;
aload 0
aload 1
aload 2
invokespecial com/a/b/d/bb/d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final d_()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/aeq/b Ljava/util/Comparator;
areturn
.limit locals 1
.limit stack 1
.end method

.method final e(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
ifnonnull L0
aload 0
getfield com/a/b/d/aeq/a Ljava/util/Comparator;
aload 1
aload 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
pop
L0:
aload 0
aload 1
invokespecial com/a/b/d/bb/e(Ljava/lang/Object;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 3
.end method

.method final synthetic e()Ljava/util/Map;
aload 0
invokespecial com/a/b/d/bb/w()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/bb/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic f()I
aload 0
invokespecial com/a/b/d/bb/f()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic f(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/bb/f(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic g()V
aload 0
invokespecial com/a/b/d/bb/g()V
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic g(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/bb/g(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method final synthetic h()Ljava/util/Set;
new com/a/b/d/x
dup
aload 0
aload 0
invokespecial com/a/b/d/bb/w()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokespecial com/a/b/d/x/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final synthetic h(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
invokespecial com/a/b/d/aeq/j(Ljava/lang/Object;)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/bb/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic i()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/bb/i()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic i(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
invokespecial com/a/b/d/bb/i(Ljava/lang/Object;)Ljava/util/SortedSet;
areturn
.limit locals 2
.limit stack 2
.end method

.method final synthetic m()Ljava/util/Map;
new com/a/b/d/w
dup
aload 0
aload 0
invokespecial com/a/b/d/bb/w()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokespecial com/a/b/d/w/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final volatile synthetic n()Z
aload 0
invokespecial com/a/b/d/bb/n()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic p()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/bb/x()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic q()Lcom/a/b/d/xc;
aload 0
invokespecial com/a/b/d/bb/q()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/bb/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic u()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/bb/u()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic v()Ljava/util/SortedMap;
aload 0
invokespecial com/a/b/d/bb/v()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic w()Ljava/util/SortedMap;
aload 0
invokespecial com/a/b/d/bb/w()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic x()Ljava/util/SortedSet;
aload 0
invokespecial com/a/b/d/bb/x()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method final y()Ljava/util/SortedSet;
new java/util/TreeSet
dup
aload 0
getfield com/a/b/d/aeq/b Ljava/util/Comparator;
invokespecial java/util/TreeSet/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 1
.limit stack 3
.end method
