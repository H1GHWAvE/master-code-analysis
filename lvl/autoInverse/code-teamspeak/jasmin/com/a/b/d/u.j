.bytecode 50.0
.class synchronized com/a/b/d/u
.super com/a/b/d/uk

.field final synthetic 'a' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/util/Map;)V
aload 0
aload 1
putfield com/a/b/d/u/a Lcom/a/b/d/n;
aload 0
aload 2
invokespecial com/a/b/d/uk/<init>(Ljava/util/Map;)V
return
.limit locals 3
.limit stack 2
.end method

.method public clear()V
aload 0
invokevirtual com/a/b/d/u/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/i(Ljava/util/Iterator;)V
return
.limit locals 1
.limit stack 1
.end method

.method public containsAll(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/u/b()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
aload 1
invokeinterface java/util/Set/containsAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
if_acmpeq L0
aload 0
invokevirtual com/a/b/d/u/b()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
aload 1
invokeinterface java/util/Set/equals(Ljava/lang/Object;)Z 1
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/u/b()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public iterator()Ljava/util/Iterator;
new com/a/b/d/v
dup
aload 0
aload 0
invokevirtual com/a/b/d/u/b()Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/v/<init>(Lcom/a/b/d/u;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public remove(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/u/b()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 1
aload 1
ifnull L0
aload 1
invokeinterface java/util/Collection/size()I 0
istore 2
aload 1
invokeinterface java/util/Collection/clear()V 0
aload 0
getfield com/a/b/d/u/a Lcom/a/b/d/n;
iload 2
invokestatic com/a/b/d/n/b(Lcom/a/b/d/n;I)I
pop
L1:
iload 2
ifle L2
iconst_1
ireturn
L2:
iconst_0
ireturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method
