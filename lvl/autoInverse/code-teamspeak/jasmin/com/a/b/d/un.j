.bytecode 50.0
.class synchronized com/a/b/d/un
.super com/a/b/d/up
.implements java/util/NavigableSet
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableMap"
.end annotation

.method <init>(Ljava/util/NavigableMap;)V
aload 0
aload 1
invokespecial com/a/b/d/up/<init>(Ljava/util/SortedMap;)V
return
.limit locals 2
.limit stack 2
.end method

.method private c()Ljava/util/NavigableMap;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic a()Ljava/util/SortedMap;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic b()Ljava/util/Map;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method public ceiling(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/ceilingKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public descendingIterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/un/descendingSet()Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public descendingSet()Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
invokeinterface java/util/NavigableMap/descendingKeySet()Ljava/util/NavigableSet; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public floor(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/floorKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
aload 1
iload 2
invokeinterface java/util/NavigableMap/headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap; 2
invokeinterface java/util/NavigableMap/navigableKeySet()Ljava/util/NavigableSet; 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/un/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method

.method public higher(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/higherKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public lower(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/lowerKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public pollFirst()Ljava/lang/Object;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
invokeinterface java/util/NavigableMap/pollFirstEntry()Ljava/util/Map$Entry; 0
invokestatic com/a/b/d/sz/b(Ljava/util/Map$Entry;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public pollLast()Ljava/lang/Object;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
invokeinterface java/util/NavigableMap/pollLastEntry()Ljava/util/Map$Entry; 0
invokestatic com/a/b/d/sz/b(Ljava/util/Map$Entry;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
aload 1
iload 2
aload 3
iload 4
invokeinterface java/util/NavigableMap/subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap; 4
invokeinterface java/util/NavigableMap/navigableKeySet()Ljava/util/NavigableSet; 0
areturn
.limit locals 5
.limit stack 5
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_1
aload 2
iconst_0
invokevirtual com/a/b/d/un/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 5
.end method

.method public tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/un/d Ljava/util/Map;
checkcast java/util/NavigableMap
aload 1
iload 2
invokeinterface java/util/NavigableMap/tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap; 2
invokeinterface java/util/NavigableMap/navigableKeySet()Ljava/util/NavigableSet; 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/un/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method
