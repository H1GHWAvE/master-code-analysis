.bytecode 50.0
.class synchronized abstract com/a/b/d/bo
.super com/a/b/d/uj

.field final 'a' Lcom/a/b/d/jt;

.method private <init>(Lcom/a/b/d/jt;)V
aload 0
invokespecial com/a/b/d/uj/<init>()V
aload 0
aload 1
putfield com/a/b/d/bo/a Lcom/a/b/d/jt;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/d/jt;B)V
aload 0
aload 1
invokespecial com/a/b/d/bo/<init>(Lcom/a/b/d/jt;)V
return
.limit locals 3
.limit stack 2
.end method

.method private b(I)Ljava/lang/Object;
aload 0
getfield com/a/b/d/bo/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/g()Lcom/a/b/d/lo;
invokevirtual com/a/b/d/lo/f()Lcom/a/b/d/jl;
iload 1
invokevirtual com/a/b/d/jl/get(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method abstract a(I)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end method

.method abstract a(ILjava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end method

.method protected final a()Ljava/util/Set;
new com/a/b/d/bp
dup
aload 0
invokespecial com/a/b/d/bp/<init>(Lcom/a/b/d/bo;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method abstract b()Ljava/lang/String;
.end method

.method public clear()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public containsKey(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/bo/a Lcom/a/b/d/jt;
aload 1
invokevirtual com/a/b/d/jt/containsKey(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/bo/a Lcom/a/b/d/jt;
aload 1
invokevirtual com/a/b/d/jt/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual java/lang/Integer/intValue()I
invokevirtual com/a/b/d/bo/a(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public isEmpty()Z
aload 0
getfield com/a/b/d/bo/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/isEmpty()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public keySet()Ljava/util/Set;
aload 0
getfield com/a/b/d/bo/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/g()Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/bo/a Lcom/a/b/d/jt;
aload 1
invokevirtual com/a/b/d/jt/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
astore 3
aload 3
ifnonnull L0
aload 0
invokevirtual com/a/b/d/bo/b()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
getfield com/a/b/d/bo/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/g()Lcom/a/b/d/lo;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 9
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
aload 3
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " not in "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 3
invokevirtual java/lang/Integer/intValue()I
aload 2
invokevirtual com/a/b/d/bo/a(ILjava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 4
.limit stack 6
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public size()I
aload 0
getfield com/a/b/d/bo/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/size()I
ireturn
.limit locals 1
.limit stack 1
.end method
