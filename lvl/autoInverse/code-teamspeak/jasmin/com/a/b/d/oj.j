.bytecode 50.0
.class public final synchronized com/a/b/d/oj
.super com/a/b/d/an
.implements com/a/b/d/ou
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private static final 'f' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "java serialization not supported"
.end annotation
.end field

.field private transient 'a' Lcom/a/b/d/or;

.field private transient 'b' Lcom/a/b/d/or;

.field private transient 'c' Ljava/util/Map;

.field private transient 'd' I

.field private transient 'e' I

.method <init>()V
aload 0
invokespecial com/a/b/d/an/<init>()V
aload 0
invokestatic com/a/b/d/sz/c()Ljava/util/HashMap;
putfield com/a/b/d/oj/c Ljava/util/Map;
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(I)V
aload 0
invokespecial com/a/b/d/an/<init>()V
aload 0
new java/util/HashMap
dup
iload 1
invokespecial java/util/HashMap/<init>(I)V
putfield com/a/b/d/oj/c Ljava/util/Map;
return
.limit locals 2
.limit stack 4
.end method

.method private <init>(Lcom/a/b/d/vi;)V
aload 0
aload 1
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
invokeinterface java/util/Set/size()I 0
invokespecial com/a/b/d/oj/<init>(I)V
aload 0
aload 1
invokevirtual com/a/b/d/oj/a(Lcom/a/b/d/vi;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/oj;)I
aload 0
getfield com/a/b/d/oj/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a()Lcom/a/b/d/oj;
new com/a/b/d/oj
dup
invokespecial com/a/b/d/oj/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(I)Lcom/a/b/d/oj;
new com/a/b/d/oj
dup
iload 0
invokespecial com/a/b/d/oj/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/d/oj;Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;
aload 0
aload 1
aload 2
aload 3
invokespecial com/a/b/d/oj/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/or
dup
aload 1
aload 2
invokespecial com/a/b/d/or/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
astore 2
aload 0
getfield com/a/b/d/oj/a Lcom/a/b/d/or;
ifnonnull L0
aload 0
aload 2
putfield com/a/b/d/oj/b Lcom/a/b/d/or;
aload 0
aload 2
putfield com/a/b/d/oj/a Lcom/a/b/d/or;
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
new com/a/b/d/oq
dup
aload 2
invokespecial com/a/b/d/oq/<init>(Lcom/a/b/d/or;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
aload 0
getfield com/a/b/d/oj/e I
iconst_1
iadd
putfield com/a/b/d/oj/e I
L1:
aload 0
aload 0
getfield com/a/b/d/oj/d I
iconst_1
iadd
putfield com/a/b/d/oj/d I
aload 2
areturn
L0:
aload 3
ifnonnull L2
aload 0
getfield com/a/b/d/oj/b Lcom/a/b/d/or;
aload 2
putfield com/a/b/d/or/c Lcom/a/b/d/or;
aload 2
aload 0
getfield com/a/b/d/oj/b Lcom/a/b/d/or;
putfield com/a/b/d/or/d Lcom/a/b/d/or;
aload 0
aload 2
putfield com/a/b/d/oj/b Lcom/a/b/d/or;
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
astore 3
aload 3
ifnonnull L3
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
new com/a/b/d/oq
dup
aload 2
invokespecial com/a/b/d/oq/<init>(Lcom/a/b/d/or;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
aload 0
getfield com/a/b/d/oj/e I
iconst_1
iadd
putfield com/a/b/d/oj/e I
goto L1
L3:
aload 3
aload 3
getfield com/a/b/d/oq/c I
iconst_1
iadd
putfield com/a/b/d/oq/c I
aload 3
getfield com/a/b/d/oq/b Lcom/a/b/d/or;
astore 1
aload 1
aload 2
putfield com/a/b/d/or/e Lcom/a/b/d/or;
aload 2
aload 1
putfield com/a/b/d/or/f Lcom/a/b/d/or;
aload 3
aload 2
putfield com/a/b/d/oq/b Lcom/a/b/d/or;
goto L1
L2:
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
astore 4
aload 4
aload 4
getfield com/a/b/d/oq/c I
iconst_1
iadd
putfield com/a/b/d/oq/c I
aload 2
aload 3
getfield com/a/b/d/or/d Lcom/a/b/d/or;
putfield com/a/b/d/or/d Lcom/a/b/d/or;
aload 2
aload 3
getfield com/a/b/d/or/f Lcom/a/b/d/or;
putfield com/a/b/d/or/f Lcom/a/b/d/or;
aload 2
aload 3
putfield com/a/b/d/or/c Lcom/a/b/d/or;
aload 2
aload 3
putfield com/a/b/d/or/e Lcom/a/b/d/or;
aload 3
getfield com/a/b/d/or/f Lcom/a/b/d/or;
ifnonnull L4
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
aload 2
putfield com/a/b/d/oq/a Lcom/a/b/d/or;
L5:
aload 3
getfield com/a/b/d/or/d Lcom/a/b/d/or;
ifnonnull L6
aload 0
aload 2
putfield com/a/b/d/oj/a Lcom/a/b/d/or;
L7:
aload 3
aload 2
putfield com/a/b/d/or/d Lcom/a/b/d/or;
aload 3
aload 2
putfield com/a/b/d/or/f Lcom/a/b/d/or;
goto L1
L4:
aload 3
getfield com/a/b/d/or/f Lcom/a/b/d/or;
aload 2
putfield com/a/b/d/or/e Lcom/a/b/d/or;
goto L5
L6:
aload 3
getfield com/a/b/d/or/d Lcom/a/b/d/or;
aload 2
putfield com/a/b/d/or/c Lcom/a/b/d/or;
goto L7
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Lcom/a/b/d/oj;Lcom/a/b/d/or;)V
aload 1
getfield com/a/b/d/or/d Lcom/a/b/d/or;
ifnull L0
aload 1
getfield com/a/b/d/or/d Lcom/a/b/d/or;
aload 1
getfield com/a/b/d/or/c Lcom/a/b/d/or;
putfield com/a/b/d/or/c Lcom/a/b/d/or;
L1:
aload 1
getfield com/a/b/d/or/c Lcom/a/b/d/or;
ifnull L2
aload 1
getfield com/a/b/d/or/c Lcom/a/b/d/or;
aload 1
getfield com/a/b/d/or/d Lcom/a/b/d/or;
putfield com/a/b/d/or/d Lcom/a/b/d/or;
L3:
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
ifnonnull L4
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
ifnonnull L4
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
getfield com/a/b/d/or/a Ljava/lang/Object;
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
iconst_0
putfield com/a/b/d/oq/c I
aload 0
aload 0
getfield com/a/b/d/oj/e I
iconst_1
iadd
putfield com/a/b/d/oj/e I
L5:
aload 0
aload 0
getfield com/a/b/d/oj/d I
iconst_1
isub
putfield com/a/b/d/oj/d I
return
L0:
aload 0
aload 1
getfield com/a/b/d/or/c Lcom/a/b/d/or;
putfield com/a/b/d/oj/a Lcom/a/b/d/or;
goto L1
L2:
aload 0
aload 1
getfield com/a/b/d/or/d Lcom/a/b/d/or;
putfield com/a/b/d/oj/b Lcom/a/b/d/or;
goto L3
L4:
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
getfield com/a/b/d/or/a Ljava/lang/Object;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
astore 2
aload 2
aload 2
getfield com/a/b/d/oq/c I
iconst_1
isub
putfield com/a/b/d/oq/c I
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
ifnonnull L6
aload 2
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
putfield com/a/b/d/oq/a Lcom/a/b/d/or;
L7:
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
ifnonnull L8
aload 2
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
putfield com/a/b/d/oq/b Lcom/a/b/d/or;
goto L5
L6:
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
putfield com/a/b/d/or/e Lcom/a/b/d/or;
goto L7
L8:
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
putfield com/a/b/d/or/f Lcom/a/b/d/or;
goto L5
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/d/oj;Ljava/lang/Object;)V
aload 0
aload 1
invokespecial com/a/b/d/oj/h(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/a/b/d/or;)V
aload 1
getfield com/a/b/d/or/d Lcom/a/b/d/or;
ifnull L0
aload 1
getfield com/a/b/d/or/d Lcom/a/b/d/or;
aload 1
getfield com/a/b/d/or/c Lcom/a/b/d/or;
putfield com/a/b/d/or/c Lcom/a/b/d/or;
L1:
aload 1
getfield com/a/b/d/or/c Lcom/a/b/d/or;
ifnull L2
aload 1
getfield com/a/b/d/or/c Lcom/a/b/d/or;
aload 1
getfield com/a/b/d/or/d Lcom/a/b/d/or;
putfield com/a/b/d/or/d Lcom/a/b/d/or;
L3:
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
ifnonnull L4
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
ifnonnull L4
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
getfield com/a/b/d/or/a Ljava/lang/Object;
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
iconst_0
putfield com/a/b/d/oq/c I
aload 0
aload 0
getfield com/a/b/d/oj/e I
iconst_1
iadd
putfield com/a/b/d/oj/e I
L5:
aload 0
aload 0
getfield com/a/b/d/oj/d I
iconst_1
isub
putfield com/a/b/d/oj/d I
return
L0:
aload 0
aload 1
getfield com/a/b/d/or/c Lcom/a/b/d/or;
putfield com/a/b/d/oj/a Lcom/a/b/d/or;
goto L1
L2:
aload 0
aload 1
getfield com/a/b/d/or/d Lcom/a/b/d/or;
putfield com/a/b/d/oj/b Lcom/a/b/d/or;
goto L3
L4:
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
getfield com/a/b/d/or/a Ljava/lang/Object;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/oq
astore 2
aload 2
aload 2
getfield com/a/b/d/oq/c I
iconst_1
isub
putfield com/a/b/d/oq/c I
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
ifnonnull L6
aload 2
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
putfield com/a/b/d/oq/a Lcom/a/b/d/or;
L7:
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
ifnonnull L8
aload 2
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
putfield com/a/b/d/oq/b Lcom/a/b/d/or;
goto L5
L6:
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
putfield com/a/b/d/or/e Lcom/a/b/d/or;
goto L7
L8:
aload 1
getfield com/a/b/d/or/e Lcom/a/b/d/or;
aload 1
getfield com/a/b/d/or/f Lcom/a/b/d/or;
putfield com/a/b/d/or/f Lcom/a/b/d/or;
goto L5
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
invokestatic com/a/b/d/sz/d()Ljava/util/LinkedHashMap;
putfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
invokevirtual com/a/b/d/oj/a(Ljava/lang/Object;Ljava/lang/Object;)Z
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
invokevirtual com/a/b/d/oj/f()I
invokevirtual java/io/ObjectOutputStream/writeInt(I)V
aload 0
invokespecial com/a/b/d/an/k()Ljava/util/Collection;
checkcast java/util/List
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
goto L0
L1:
return
.limit locals 4
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/oj;
new com/a/b/d/oj
dup
aload 0
invokespecial com/a/b/d/oj/<init>(Lcom/a/b/d/vi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic b(Lcom/a/b/d/oj;)Lcom/a/b/d/or;
aload 0
getfield com/a/b/d/oj/b Lcom/a/b/d/or;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/a/b/d/oj;)Lcom/a/b/d/or;
aload 0
getfield com/a/b/d/oj/a Lcom/a/b/d/or;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/util/List;
aload 0
invokespecial com/a/b/d/an/i()Ljava/util/Collection;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/util/List;
new com/a/b/d/om
dup
aload 0
invokespecial com/a/b/d/om/<init>(Lcom/a/b/d/oj;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic d(Lcom/a/b/d/oj;)Ljava/util/Map;
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/a/b/d/oj;)I
aload 0
getfield com/a/b/d/oj/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/util/List;
aload 0
invokespecial com/a/b/d/an/k()Ljava/util/Collection;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Ljava/lang/Object;)V
aload 0
ifnonnull L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private h(Ljava/lang/Object;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/ot
dup
aload 0
aload 1
invokespecial com/a/b/d/ot/<init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V
invokestatic com/a/b/d/nj/i(Ljava/util/Iterator;)V
return
.limit locals 2
.limit stack 4
.end method

.method private static i(Ljava/lang/Object;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private j(Ljava/lang/Object;)Ljava/util/List;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/ot
dup
aload 0
aload 1
invokespecial com/a/b/d/ot/<init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V
invokestatic com/a/b/d/ov/a(Ljava/util/Iterator;)Ljava/util/ArrayList;
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
.limit locals 2
.limit stack 4
.end method

.method private t()Ljava/util/List;
new com/a/b/d/oo
dup
aload 0
invokespecial com/a/b/d/oo/<init>(Lcom/a/b/d/oj;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/List;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/ok
dup
aload 0
aload 1
invokespecial com/a/b/d/ok/<init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokespecial com/a/b/d/oj/j(Ljava/lang/Object;)Ljava/util/List;
astore 3
new com/a/b/d/ot
dup
aload 0
aload 1
invokespecial com/a/b/d/ot/<init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V
astore 1
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 1
invokeinterface java/util/ListIterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/ListIterator/next()Ljava/lang/Object; 0
pop
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/ListIterator/set(Ljava/lang/Object;)V 1
goto L0
L1:
aload 1
invokeinterface java/util/ListIterator/hasNext()Z 0
ifeq L2
aload 1
invokeinterface java/util/ListIterator/next()Ljava/lang/Object; 0
pop
aload 1
invokeinterface java/util/ListIterator/remove()V 0
goto L1
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/ListIterator/add(Ljava/lang/Object;)V 1
goto L2
L3:
aload 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public final volatile synthetic a(Lcom/a/b/d/vi;)Z
aload 0
aload 1
invokespecial com/a/b/d/an/a(Lcom/a/b/d/vi;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
aconst_null
invokespecial com/a/b/d/oj/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;
pop
iconst_1
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/oj/a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/List;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokespecial com/a/b/d/oj/j(Ljava/lang/Object;)Ljava/util/List;
astore 2
aload 0
aload 1
invokespecial com/a/b/d/oj/h(Ljava/lang/Object;)V
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method public final volatile synthetic b()Ljava/util/Map;
aload 0
invokespecial com/a/b/d/an/b()Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/an/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/oj/a(Ljava/lang/Object;)Ljava/util/List;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/an/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/an/c(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/oj/b(Ljava/lang/Object;)Ljava/util/List;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/an/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final f()I
aload 0
getfield com/a/b/d/oj/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final g()V
aload 0
aconst_null
putfield com/a/b/d/oj/a Lcom/a/b/d/or;
aload 0
aconst_null
putfield com/a/b/d/oj/b Lcom/a/b/d/or;
aload 0
getfield com/a/b/d/oj/c Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
aload 0
iconst_0
putfield com/a/b/d/oj/d I
aload 0
aload 0
getfield com/a/b/d/oj/e I
iconst_1
iadd
putfield com/a/b/d/oj/e I
return
.limit locals 1
.limit stack 3
.end method

.method public final g(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial com/a/b/d/an/i()Ljava/util/Collection;
checkcast java/util/List
aload 1
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method final h()Ljava/util/Set;
new com/a/b/d/ol
dup
aload 0
invokespecial com/a/b/d/ol/<init>(Lcom/a/b/d/oj;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/an/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic i()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/an/i()Ljava/util/Collection;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic k()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/an/k()Ljava/util/Collection;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method final l()Ljava/util/Iterator;
new java/lang/AssertionError
dup
ldc "should never be called"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method final m()Ljava/util/Map;
new com/a/b/d/wf
dup
aload 0
invokespecial com/a/b/d/wf/<init>(Lcom/a/b/d/vi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final n()Z
aload 0
getfield com/a/b/d/oj/a Lcom/a/b/d/or;
ifnonnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic o()Ljava/util/Collection;
new com/a/b/d/oo
dup
aload 0
invokespecial com/a/b/d/oo/<init>(Lcom/a/b/d/oj;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final volatile synthetic p()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/an/p()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic q()Lcom/a/b/d/xc;
aload 0
invokespecial com/a/b/d/an/q()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic s()Ljava/util/Collection;
new com/a/b/d/om
dup
aload 0
invokespecial com/a/b/d/om/<init>(Lcom/a/b/d/oj;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/an/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
