.bytecode 50.0
.class final synchronized com/a/b/d/uf
.super com/a/b/d/ty
.implements java/util/SortedMap

.method <init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ty/<init>(Ljava/util/Map;Lcom/a/b/b/co;)V
return
.limit locals 3
.limit stack 3
.end method

.method private c()Ljava/util/SortedMap;
aload 0
getfield com/a/b/d/uf/a Ljava/util/Map;
checkcast java/util/SortedMap
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/util/SortedSet;
aload 0
invokespecial com/a/b/d/ty/keySet()Ljava/util/Set;
checkcast java/util/SortedSet
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Ljava/util/SortedSet;
new com/a/b/d/ug
dup
aload 0
invokespecial com/a/b/d/ug/<init>(Lcom/a/b/d/uf;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final comparator()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/uf/a Ljava/util/Map;
checkcast java/util/SortedMap
invokeinterface java/util/SortedMap/comparator()Ljava/util/Comparator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic e()Ljava/util/Set;
new com/a/b/d/ug
dup
aload 0
invokespecial com/a/b/d/ug/<init>(Lcom/a/b/d/uf;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final firstKey()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/ty/keySet()Ljava/util/Set;
checkcast java/util/SortedSet
invokeinterface java/util/SortedSet/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
new com/a/b/d/uf
dup
aload 0
getfield com/a/b/d/uf/a Ljava/util/Map;
checkcast java/util/SortedMap
aload 1
invokeinterface java/util/SortedMap/headMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
aload 0
getfield com/a/b/d/uf/b Lcom/a/b/b/co;
invokespecial com/a/b/d/uf/<init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final volatile synthetic keySet()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ty/keySet()Ljava/util/Set;
checkcast java/util/SortedSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final lastKey()Ljava/lang/Object;
aload 0
getfield com/a/b/d/uf/a Ljava/util/Map;
checkcast java/util/SortedMap
astore 1
L0:
aload 1
invokeinterface java/util/SortedMap/lastKey()Ljava/lang/Object; 0
astore 1
aload 0
aload 1
aload 0
getfield com/a/b/d/uf/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
invokevirtual com/a/b/d/uf/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L1
aload 1
areturn
L1:
aload 0
getfield com/a/b/d/uf/a Ljava/util/Map;
checkcast java/util/SortedMap
aload 1
invokeinterface java/util/SortedMap/headMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
astore 1
goto L0
.limit locals 2
.limit stack 4
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
new com/a/b/d/uf
dup
aload 0
getfield com/a/b/d/uf/a Ljava/util/Map;
checkcast java/util/SortedMap
aload 1
aload 2
invokeinterface java/util/SortedMap/subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap; 2
aload 0
getfield com/a/b/d/uf/b Lcom/a/b/b/co;
invokespecial com/a/b/d/uf/<init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
new com/a/b/d/uf
dup
aload 0
getfield com/a/b/d/uf/a Ljava/util/Map;
checkcast java/util/SortedMap
aload 1
invokeinterface java/util/SortedMap/tailMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
aload 0
getfield com/a/b/d/uf/b Lcom/a/b/b/co;
invokespecial com/a/b/d/uf/<init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method
