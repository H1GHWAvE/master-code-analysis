.bytecode 50.0
.class public synchronized abstract com/a/b/d/gh
.super com/a/b/d/hg
.implements java/util/Collection
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method protected <init>()V
aload 0
invokespecial com/a/b/d/hg/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a([Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
aload 1
invokestatic com/a/b/d/yc/a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private c()Z
aload 0
invokevirtual com/a/b/d/gh/iterator()Ljava/util/Iterator;
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected a(Ljava/util/Collection;)Z
aload 0
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/a(Ljava/util/Collection;Ljava/util/Iterator;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public add(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public addAll(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public abstract b()Ljava/util/Collection;
.end method

.method protected b(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/gh/iterator()Ljava/util/Iterator;
aload 1
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected b(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/gh/iterator()Ljava/util/Iterator;
aload 1
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected c(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/gh/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
aload 1
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 2
invokeinterface java/util/Iterator/remove()V 0
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method protected c(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/gh/iterator()Ljava/util/Iterator;
aload 1
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public clear()V
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
invokeinterface java/util/Collection/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public contains(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public containsAll(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/containsAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public isEmpty()Z
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
invokeinterface java/util/Collection/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected l()V
aload 0
invokevirtual com/a/b/d/gh/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/i(Ljava/util/Iterator;)V
return
.limit locals 1
.limit stack 1
.end method

.method protected o()Ljava/lang/String;
aload 0
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final p()[Ljava/lang/Object;
aload 0
aload 0
invokevirtual com/a/b/d/gh/size()I
anewarray java/lang/Object
invokevirtual com/a/b/d/gh/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public remove(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/remove(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public removeAll(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/removeAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public retainAll(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/retainAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public size()I
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
invokeinterface java/util/Collection/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toArray()[Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gh/b()Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method
