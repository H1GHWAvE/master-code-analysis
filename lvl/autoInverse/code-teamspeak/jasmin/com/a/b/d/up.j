.bytecode 50.0
.class synchronized com/a/b/d/up
.super com/a/b/d/uk
.implements java/util/SortedSet

.method <init>(Ljava/util/SortedMap;)V
aload 0
aload 1
invokespecial com/a/b/d/uk/<init>(Ljava/util/Map;)V
return
.limit locals 2
.limit stack 2
.end method

.method a()Ljava/util/SortedMap;
aload 0
invokespecial com/a/b/d/uk/b()Ljava/util/Map;
checkcast java/util/SortedMap
areturn
.limit locals 1
.limit stack 1
.end method

.method synthetic b()Ljava/util/Map;
aload 0
invokevirtual com/a/b/d/up/a()Ljava/util/SortedMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method public comparator()Ljava/util/Comparator;
aload 0
invokevirtual com/a/b/d/up/a()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/comparator()Ljava/util/Comparator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public first()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/up/a()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/firstKey()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
new com/a/b/d/up
dup
aload 0
invokevirtual com/a/b/d/up/a()Ljava/util/SortedMap;
aload 1
invokeinterface java/util/SortedMap/headMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
invokespecial com/a/b/d/up/<init>(Ljava/util/SortedMap;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public last()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/up/a()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/lastKey()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
new com/a/b/d/up
dup
aload 0
invokevirtual com/a/b/d/up/a()Ljava/util/SortedMap;
aload 1
aload 2
invokeinterface java/util/SortedMap/subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap; 2
invokespecial com/a/b/d/up/<init>(Ljava/util/SortedMap;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
new com/a/b/d/up
dup
aload 0
invokevirtual com/a/b/d/up/a()Ljava/util/SortedMap;
aload 1
invokeinterface java/util/SortedMap/tailMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
invokespecial com/a/b/d/up/<init>(Ljava/util/SortedMap;)V
areturn
.limit locals 2
.limit stack 4
.end method
