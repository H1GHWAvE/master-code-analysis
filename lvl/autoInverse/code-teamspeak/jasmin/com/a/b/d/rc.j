.bytecode 50.0
.class synchronized abstract com/a/b/d/rc
.super com/a/b/d/gi
.implements java/io/Serializable

.field private static final 'a' J = 3L


.field final 'b' Lcom/a/b/d/sh;

.field final 'c' Lcom/a/b/d/sh;

.field final 'd' Lcom/a/b/b/au;

.field final 'e' Lcom/a/b/b/au;

.field final 'f' J

.field final 'g' J

.field final 'h' I

.field final 'i' I

.field final 'j' Lcom/a/b/d/qw;

.field transient 'k' Ljava/util/concurrent/ConcurrentMap;

.method <init>(Lcom/a/b/d/sh;Lcom/a/b/d/sh;Lcom/a/b/b/au;Lcom/a/b/b/au;JJIILcom/a/b/d/qw;Ljava/util/concurrent/ConcurrentMap;)V
aload 0
invokespecial com/a/b/d/gi/<init>()V
aload 0
aload 1
putfield com/a/b/d/rc/b Lcom/a/b/d/sh;
aload 0
aload 2
putfield com/a/b/d/rc/c Lcom/a/b/d/sh;
aload 0
aload 3
putfield com/a/b/d/rc/d Lcom/a/b/b/au;
aload 0
aload 4
putfield com/a/b/d/rc/e Lcom/a/b/b/au;
aload 0
lload 5
putfield com/a/b/d/rc/f J
aload 0
lload 7
putfield com/a/b/d/rc/g J
aload 0
iload 9
putfield com/a/b/d/rc/h I
aload 0
iload 10
putfield com/a/b/d/rc/i I
aload 0
aload 11
putfield com/a/b/d/rc/j Lcom/a/b/d/qw;
aload 0
aload 12
putfield com/a/b/d/rc/k Ljava/util/concurrent/ConcurrentMap;
return
.limit locals 13
.limit stack 3
.end method

.method final a(Ljava/io/ObjectInputStream;)Lcom/a/b/d/ql;
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 2
new com/a/b/d/ql
dup
invokespecial com/a/b/d/ql/<init>()V
iload 2
invokevirtual com/a/b/d/ql/d(I)Lcom/a/b/d/ql;
aload 0
getfield com/a/b/d/rc/b Lcom/a/b/d/sh;
invokevirtual com/a/b/d/ql/a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;
aload 0
getfield com/a/b/d/rc/c Lcom/a/b/d/sh;
invokevirtual com/a/b/d/ql/b(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;
aload 0
getfield com/a/b/d/rc/d Lcom/a/b/b/au;
invokevirtual com/a/b/d/ql/b(Lcom/a/b/b/au;)Lcom/a/b/d/ql;
aload 0
getfield com/a/b/d/rc/i I
invokevirtual com/a/b/d/ql/f(I)Lcom/a/b/d/ql;
astore 1
aload 0
getfield com/a/b/d/rc/j Lcom/a/b/d/qw;
astore 4
aload 1
getfield com/a/b/d/ql/a Lcom/a/b/d/qw;
ifnonnull L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/b(Z)V
aload 1
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/qw
putfield com/a/b/d/ht/a Lcom/a/b/d/qw;
aload 1
iconst_1
putfield com/a/b/d/ql/c Z
aload 0
getfield com/a/b/d/rc/f J
lconst_0
lcmp
ifle L2
aload 1
aload 0
getfield com/a/b/d/rc/f J
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/d/ql/c(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ql;
pop
L2:
aload 0
getfield com/a/b/d/rc/g J
lconst_0
lcmp
ifle L3
aload 1
aload 0
getfield com/a/b/d/rc/g J
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/d/ql/d(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ql;
pop
L3:
aload 0
getfield com/a/b/d/rc/h I
iconst_m1
if_icmpeq L4
aload 1
aload 0
getfield com/a/b/d/rc/h I
invokevirtual com/a/b/d/ql/e(I)Lcom/a/b/d/ql;
pop
L4:
aload 1
areturn
L0:
iconst_0
istore 3
goto L1
.limit locals 5
.limit stack 4
.end method

.method protected final volatile synthetic a()Ljava/util/Map;
aload 0
getfield com/a/b/d/rc/k Ljava/util/concurrent/ConcurrentMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a(Ljava/io/ObjectOutputStream;)V
aload 1
aload 0
getfield com/a/b/d/rc/k Ljava/util/concurrent/ConcurrentMap;
invokeinterface java/util/concurrent/ConcurrentMap/size()I 0
invokevirtual java/io/ObjectOutputStream/writeInt(I)V
aload 0
getfield com/a/b/d/rc/k Ljava/util/concurrent/ConcurrentMap;
invokeinterface java/util/concurrent/ConcurrentMap/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 1
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
goto L0
L1:
aload 1
aconst_null
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
return
.limit locals 4
.limit stack 2
.end method

.method protected final b()Ljava/util/concurrent/ConcurrentMap;
aload 0
getfield com/a/b/d/rc/k Ljava/util/concurrent/ConcurrentMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method final b(Ljava/io/ObjectInputStream;)V
L0:
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
astore 2
aload 2
ifnull L1
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
astore 3
aload 0
getfield com/a/b/d/rc/k Ljava/util/concurrent/ConcurrentMap;
aload 2
aload 3
invokeinterface java/util/concurrent/ConcurrentMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L0
L1:
return
.limit locals 4
.limit stack 3
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/rc/k Ljava/util/concurrent/ConcurrentMap;
areturn
.limit locals 1
.limit stack 1
.end method
