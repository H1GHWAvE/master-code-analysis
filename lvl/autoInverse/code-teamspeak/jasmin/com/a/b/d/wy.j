.bytecode 50.0
.class synchronized com/a/b/d/wy
.super com/a/b/d/gx
.implements java/io/Serializable

.field private static final 'g' J = 0L


.field final 'a' Lcom/a/b/d/vi;

.field transient 'b' Ljava/util/Collection;

.field transient 'c' Lcom/a/b/d/xc;

.field transient 'd' Ljava/util/Set;

.field transient 'e' Ljava/util/Collection;

.field transient 'f' Ljava/util/Map;

.method <init>(Lcom/a/b/d/vi;)V
aload 0
invokespecial com/a/b/d/gx/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/vi
putfield com/a/b/d/wy/a Lcom/a/b/d/vi;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/a/b/d/vi;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final b()Ljava/util/Map;
aload 0
getfield com/a/b/d/wy/f Ljava/util/Map;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/wy/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
new com/a/b/d/wz
dup
aload 0
invokespecial com/a/b/d/wz/<init>(Lcom/a/b/d/wy;)V
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
astore 1
aload 0
aload 1
putfield com/a/b/d/wy/f Ljava/util/Map;
L0:
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method protected c()Lcom/a/b/d/vi;
aload 0
getfield com/a/b/d/wy/a Lcom/a/b/d/vi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
getfield com/a/b/d/wy/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;)Ljava/util/Collection; 1
invokestatic com/a/b/d/we/a(Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final g()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final i()Ljava/util/Collection;
aload 0
getfield com/a/b/d/wy/e Ljava/util/Collection;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/wy/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/i()Ljava/util/Collection; 0
invokestatic java/util/Collections/unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;
astore 1
aload 0
aload 1
putfield com/a/b/d/wy/e Ljava/util/Collection;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public k()Ljava/util/Collection;
aload 0
getfield com/a/b/d/wy/b Ljava/util/Collection;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/wy/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
astore 1
aload 1
instanceof java/util/Set
ifeq L1
aload 1
checkcast java/util/Set
invokestatic com/a/b/d/sz/a(Ljava/util/Set;)Ljava/util/Set;
astore 1
L2:
aload 0
aload 1
putfield com/a/b/d/wy/b Ljava/util/Collection;
L0:
aload 1
areturn
L1:
new com/a/b/d/uw
dup
aload 1
invokestatic java/util/Collections/unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;
invokespecial com/a/b/d/uw/<init>(Ljava/util/Collection;)V
astore 1
goto L2
.limit locals 3
.limit stack 3
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/wy/c()Lcom/a/b/d/vi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final p()Ljava/util/Set;
aload 0
getfield com/a/b/d/wy/d Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/wy/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
invokestatic java/util/Collections/unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
astore 1
aload 0
aload 1
putfield com/a/b/d/wy/d Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public final q()Lcom/a/b/d/xc;
aload 0
getfield com/a/b/d/wy/c Lcom/a/b/d/xc;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/wy/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/q()Lcom/a/b/d/xc; 0
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;)Lcom/a/b/d/xc;
astore 1
aload 0
aload 1
putfield com/a/b/d/wy/c Lcom/a/b/d/xc;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method
