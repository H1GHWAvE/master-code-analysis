.bytecode 50.0
.class final synchronized com/a/b/m/ao
.super com/a/b/m/an

.method <init>()V
aload 0
iconst_0
invokespecial com/a/b/m/an/<init>(B)V
return
.limit locals 1
.limit stack 2
.end method

.method private static a(Lcom/a/b/m/ae;)Ljava/lang/Class;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Lcom/a/b/m/ae;)Ljava/lang/Iterable;
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
areturn
L0:
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
areturn
L1:
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 3
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericInterfaces()[Ljava/lang/reflect/Type;
astore 4
aload 4
arraylength
istore 2
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L3
aload 3
aload 0
aload 4
iload 1
aaload
invokevirtual com/a/b/m/ae/c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 3
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 5
.limit stack 4
.end method

.method private static c(Lcom/a/b/m/ae;)Lcom/a/b/m/ae;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/b/m/ae/d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L0:
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/b/m/ae/d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L1:
aload 0
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
astore 1
aload 1
ifnonnull L2
aconst_null
areturn
L2:
aload 0
aload 1
invokevirtual com/a/b/m/ae/c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 2
.limit stack 2
.end method

.method final synthetic b(Ljava/lang/Object;)Ljava/lang/Class;
aload 1
checkcast com/a/b/m/ae
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
areturn
.limit locals 2
.limit stack 1
.end method

.method final synthetic c(Ljava/lang/Object;)Ljava/lang/Iterable;
aload 1
checkcast com/a/b/m/ae
astore 1
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
areturn
L0:
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ae/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
areturn
L1:
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 4
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericInterfaces()[Ljava/lang/reflect/Type;
astore 5
aload 5
arraylength
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L3
aload 4
aload 1
aload 5
iload 2
aaload
invokevirtual com/a/b/m/ae/c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 4
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 6
.limit stack 4
.end method

.method final synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast com/a/b/m/ae
astore 1
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/b/m/ae/d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L0:
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/b/m/ae/d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
L1:
aload 1
getfield com/a/b/m/ae/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/e(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
astore 2
aload 2
ifnonnull L2
aconst_null
areturn
L2:
aload 1
aload 2
invokevirtual com/a/b/m/ae/c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 3
.limit stack 2
.end method
