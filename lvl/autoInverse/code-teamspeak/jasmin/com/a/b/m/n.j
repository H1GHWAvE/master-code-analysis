.bytecode 50.0
.class public final synchronized com/a/b/m/n
.super com/a/b/d/gs
.implements com/a/b/m/ad
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Ljava/util/Map;

.method public <init>()V
aload 0
invokespecial com/a/b/d/gs/<init>()V
aload 0
invokestatic com/a/b/d/sz/c()Ljava/util/HashMap;
putfield com/a/b/m/n/a Ljava/util/Map;
return
.limit locals 1
.limit stack 2
.end method

.method private static b()Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
ldc "Please use putInstance() instead."
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 0
.limit stack 3
.end method

.method private b(Lcom/a/b/m/ae;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/m/n/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/m/n/a Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/a/b/m/ae;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/m/ae/c()Lcom/a/b/m/ae;
invokespecial com/a/b/m/n/b(Lcom/a/b/m/ae;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/m/ae/c()Lcom/a/b/m/ae;
aload 2
invokespecial com/a/b/m/n/b(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
invokespecial com/a/b/m/n/b(Lcom/a/b/m/ae;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
aload 2
invokespecial com/a/b/m/n/b(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method protected final a()Ljava/util/Map;
aload 0
getfield com/a/b/m/n/a Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final entrySet()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/gs/entrySet()Ljava/util/Set;
invokestatic com/a/b/m/p/a(Ljava/util/Set;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/m/n/a Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
ldc "Please use putInstance() instead."
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final putAll(Ljava/util/Map;)V
new java/lang/UnsupportedOperationException
dup
ldc "Please use putInstance() instead."
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method
