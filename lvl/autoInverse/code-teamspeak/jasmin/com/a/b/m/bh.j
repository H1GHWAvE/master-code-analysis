.bytecode 50.0
.class synchronized abstract enum com/a/b/m/bh
.super java/lang/Enum

.field public static final enum 'a' Lcom/a/b/m/bh;

.field public static final enum 'b' Lcom/a/b/m/bh;

.field public static final enum 'c' Lcom/a/b/m/bh;

.field static final 'd' Lcom/a/b/m/bh;

.field private static final synthetic 'e' [Lcom/a/b/m/bh;

.method static <clinit>()V
new com/a/b/m/bi
dup
ldc "JAVA6"
invokespecial com/a/b/m/bi/<init>(Ljava/lang/String;)V
putstatic com/a/b/m/bh/a Lcom/a/b/m/bh;
new com/a/b/m/bj
dup
ldc "JAVA7"
invokespecial com/a/b/m/bj/<init>(Ljava/lang/String;)V
putstatic com/a/b/m/bh/b Lcom/a/b/m/bh;
new com/a/b/m/bk
dup
ldc "JAVA8"
invokespecial com/a/b/m/bk/<init>(Ljava/lang/String;)V
putstatic com/a/b/m/bh/c Lcom/a/b/m/bh;
iconst_3
anewarray com/a/b/m/bh
dup
iconst_0
getstatic com/a/b/m/bh/a Lcom/a/b/m/bh;
aastore
dup
iconst_1
getstatic com/a/b/m/bh/b Lcom/a/b/m/bh;
aastore
dup
iconst_2
getstatic com/a/b/m/bh/c Lcom/a/b/m/bh;
aastore
putstatic com/a/b/m/bh/e [Lcom/a/b/m/bh;
ldc java/lang/reflect/AnnotatedElement
ldc java/lang/reflect/TypeVariable
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L0
getstatic com/a/b/m/bh/c Lcom/a/b/m/bh;
putstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
return
L0:
new com/a/b/m/bl
dup
invokespecial com/a/b/m/bl/<init>()V
invokevirtual com/a/b/m/bl/a()Ljava/lang/reflect/Type;
instanceof java/lang/Class
ifeq L1
getstatic com/a/b/m/bh/b Lcom/a/b/m/bh;
putstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
return
L1:
getstatic com/a/b/m/bh/a Lcom/a/b/m/bh;
putstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method synthetic <init>(Ljava/lang/String;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/b/m/bh/<init>(Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/m/bh;
ldc com/a/b/m/bh
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/b/m/bh
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/b/m/bh;
getstatic com/a/b/m/bh/e [Lcom/a/b/m/bh;
invokevirtual [Lcom/a/b/m/bh;/clone()Ljava/lang/Object;
checkcast [Lcom/a/b/m/bh;
areturn
.limit locals 0
.limit stack 1
.end method

.method final a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 4
aload 1
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 4
aload 0
aload 1
iload 2
aaload
invokevirtual com/a/b/m/bh/b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 4
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 5
.limit stack 4
.end method

.method abstract a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method

.method abstract b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method

.method c(Ljava/lang/reflect/Type;)Ljava/lang/String;
aload 1
invokestatic com/a/b/m/ay/b(Ljava/lang/reflect/Type;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method
