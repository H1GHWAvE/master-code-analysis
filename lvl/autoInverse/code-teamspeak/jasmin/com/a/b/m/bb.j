.bytecode 50.0
.class synchronized abstract enum com/a/b/m/bb
.super java/lang/Enum

.field public static final enum 'a' Lcom/a/b/m/bb;

.field public static final enum 'b' Lcom/a/b/m/bb;

.field static final 'c' Lcom/a/b/m/bb;

.field private static final synthetic 'd' [Lcom/a/b/m/bb;

.method static <clinit>()V
iconst_0
istore 0
new com/a/b/m/bc
dup
ldc "OWNED_BY_ENCLOSING_CLASS"
invokespecial com/a/b/m/bc/<init>(Ljava/lang/String;)V
putstatic com/a/b/m/bb/a Lcom/a/b/m/bb;
new com/a/b/m/be
dup
ldc "LOCAL_CLASS_HAS_NO_OWNER"
invokespecial com/a/b/m/be/<init>(Ljava/lang/String;)V
putstatic com/a/b/m/bb/b Lcom/a/b/m/bb;
iconst_2
anewarray com/a/b/m/bb
dup
iconst_0
getstatic com/a/b/m/bb/a Lcom/a/b/m/bb;
aastore
dup
iconst_1
getstatic com/a/b/m/bb/b Lcom/a/b/m/bb;
aastore
putstatic com/a/b/m/bb/d [Lcom/a/b/m/bb;
new com/a/b/m/bf
dup
invokespecial com/a/b/m/bf/<init>()V
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
checkcast java/lang/reflect/ParameterizedType
astore 2
invokestatic com/a/b/m/bb/values()[Lcom/a/b/m/bb;
astore 3
aload 3
arraylength
istore 1
L0:
iload 0
iload 1
if_icmpge L1
aload 3
iload 0
aaload
astore 4
aload 4
ldc com/a/b/m/bd
invokevirtual com/a/b/m/bb/a(Ljava/lang/Class;)Ljava/lang/Class;
aload 2
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
if_acmpne L2
aload 4
putstatic com/a/b/m/bb/c Lcom/a/b/m/bb;
return
L2:
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 5
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method synthetic <init>(Ljava/lang/String;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/b/m/bb/<init>(Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 3
.end method

.method private static a()Lcom/a/b/m/bb;
new com/a/b/m/bf
dup
invokespecial com/a/b/m/bf/<init>()V
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
checkcast java/lang/reflect/ParameterizedType
astore 2
invokestatic com/a/b/m/bb/values()[Lcom/a/b/m/bb;
astore 3
aload 3
arraylength
istore 1
iconst_0
istore 0
L0:
iload 0
iload 1
if_icmpge L1
aload 3
iload 0
aaload
astore 4
aload 4
ldc com/a/b/m/bd
invokevirtual com/a/b/m/bb/a(Ljava/lang/Class;)Ljava/lang/Class;
aload 2
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
if_acmpne L2
aload 4
areturn
L2:
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 5
.limit stack 2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/m/bb;
ldc com/a/b/m/bb
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/b/m/bb
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/b/m/bb;
getstatic com/a/b/m/bb/d [Lcom/a/b/m/bb;
invokevirtual [Lcom/a/b/m/bb;/clone()Ljava/lang/Object;
checkcast [Lcom/a/b/m/bb;
areturn
.limit locals 0
.limit stack 1
.end method

.method abstract a(Ljava/lang/Class;)Ljava/lang/Class;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end method
