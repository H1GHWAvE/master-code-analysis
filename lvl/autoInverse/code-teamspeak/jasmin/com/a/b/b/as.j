.bytecode 50.0
.class public final synchronized com/a/b/b/as
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'a' Ljava/util/Map;
.annotation invisible Lcom/a/b/a/c;
a s = "java.lang.ref.WeakReference"
.end annotation
.end field

.method static <clinit>()V
new java/util/WeakHashMap
dup
invokespecial java/util/WeakHashMap/<init>()V
putstatic com/a/b/b/as/a Ljava/util/Map;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/b/ci;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokestatic com/a/b/b/as/a(Ljava/lang/Class;)Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/ref/WeakReference
astore 1
aload 1
ifnonnull L0
invokestatic com/a/b/b/ci/f()Lcom/a/b/b/ci;
areturn
L0:
aload 0
aload 1
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
invokestatic com/a/b/b/ci/b(Ljava/lang/Object;)Lcom/a/b/b/ci;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Enum;)Ljava/lang/reflect/Field;
.annotation invisible Lcom/a/b/a/c;
a s = "reflection"
.end annotation
.catch java/lang/NoSuchFieldException from L0 to L1 using L2
aload 0
invokevirtual java/lang/Enum/getDeclaringClass()Ljava/lang/Class;
astore 1
L0:
aload 1
aload 0
invokevirtual java/lang/Enum/name()Ljava/lang/String;
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method static a(Ljava/lang/Class;)Ljava/util/Map;
.annotation invisible Lcom/a/b/a/c;
a s = "java.lang.ref.WeakReference"
.end annotation
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L9 to L10 using L2
getstatic com/a/b/b/as/a Ljava/util/Map;
astore 3
aload 3
monitorenter
L0:
getstatic com/a/b/b/as/a Ljava/util/Map;
aload 0
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Map
astore 2
L1:
aload 2
astore 1
aload 2
ifnonnull L9
L3:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 1
aload 0
invokestatic java/util/EnumSet/allOf(Ljava/lang/Class;)Ljava/util/EnumSet;
invokevirtual java/util/EnumSet/iterator()Ljava/util/Iterator;
astore 2
L4:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Enum
astore 4
aload 1
aload 4
invokevirtual java/lang/Enum/name()Ljava/lang/String;
new java/lang/ref/WeakReference
dup
aload 4
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L5:
goto L4
L2:
astore 0
L6:
aload 3
monitorexit
L7:
aload 0
athrow
L8:
getstatic com/a/b/b/as/a Ljava/util/Map;
aload 0
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L9:
aload 3
monitorexit
L10:
aload 1
areturn
.limit locals 5
.limit stack 5
.end method

.method private static b(Ljava/lang/Class;)Ljava/util/Map;
.annotation invisible Lcom/a/b/a/c;
a s = "java.lang.ref.WeakReference"
.end annotation
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 1
aload 0
invokestatic java/util/EnumSet/allOf(Ljava/lang/Class;)Ljava/util/EnumSet;
invokevirtual java/util/EnumSet/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Enum
astore 3
aload 1
aload 3
invokevirtual java/lang/Enum/name()Ljava/lang/String;
new java/lang/ref/WeakReference
dup
aload 3
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L0
L1:
getstatic com/a/b/b/as/a Ljava/util/Map;
aload 0
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
areturn
.limit locals 4
.limit stack 5
.end method

.method private static c(Ljava/lang/Class;)Lcom/a/b/b/ak;
new com/a/b/b/at
dup
aload 0
invokespecial com/a/b/b/at/<init>(Ljava/lang/Class;)V
areturn
.limit locals 1
.limit stack 3
.end method
