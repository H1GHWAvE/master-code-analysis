.bytecode 50.0
.class public synchronized com/a/b/b/a/a
.super java/lang/Object
.implements java/lang/Runnable

.field private static final 'a' Ljava/util/logging/Logger;

.field private static final 'b' Ljava/lang/String; = "com.google.common.base.FinalizableReference"

.field private static final 'f' Ljava/lang/reflect/Field;

.field private final 'c' Ljava/lang/ref/WeakReference;

.field private final 'd' Ljava/lang/ref/PhantomReference;

.field private final 'e' Ljava/lang/ref/ReferenceQueue;

.method static <clinit>()V
ldc com/a/b/b/a/a
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/b/a/a/a Ljava/util/logging/Logger;
invokestatic com/a/b/b/a/a/b()Ljava/lang/reflect/Field;
putstatic com/a/b/b/a/a/f Ljava/lang/reflect/Field;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>(Ljava/lang/Class;Ljava/lang/ref/ReferenceQueue;Ljava/lang/ref/PhantomReference;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 2
putfield com/a/b/b/a/a/e Ljava/lang/ref/ReferenceQueue;
aload 0
new java/lang/ref/WeakReference
dup
aload 1
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
putfield com/a/b/b/a/a/c Ljava/lang/ref/WeakReference;
aload 0
aload 3
putfield com/a/b/b/a/a/d Ljava/lang/ref/PhantomReference;
return
.limit locals 4
.limit stack 4
.end method

.method private a()Ljava/lang/reflect/Method;
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
aload 0
getfield com/a/b/b/a/a/c Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast java/lang/Class
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 1
ldc "finalizeReferent"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/ref/ReferenceQueue;Ljava/lang/ref/PhantomReference;)V
.catch java/lang/Throwable from L0 to L1 using L2
aload 0
invokevirtual java/lang/Class/getName()Ljava/lang/String;
ldc "com.google.common.base.FinalizableReference"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L3
new java/lang/IllegalArgumentException
dup
ldc "Expected com.google.common.base.FinalizableReference."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
new java/lang/Thread
dup
new com/a/b/b/a/a
dup
aload 0
aload 1
aload 2
invokespecial com/a/b/b/a/a/<init>(Ljava/lang/Class;Ljava/lang/ref/ReferenceQueue;Ljava/lang/ref/PhantomReference;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
astore 0
aload 0
ldc com/a/b/b/a/a
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/Thread/setName(Ljava/lang/String;)V
aload 0
iconst_1
invokevirtual java/lang/Thread/setDaemon(Z)V
L0:
getstatic com/a/b/b/a/a/f Ljava/lang/reflect/Field;
ifnull L1
getstatic com/a/b/b/a/a/f Ljava/lang/reflect/Field;
aload 0
aconst_null
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L1:
aload 0
invokevirtual java/lang/Thread/start()V
return
L2:
astore 1
getstatic com/a/b/b/a/a/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Failed to clear thread local values inherited by reference finalizer thread."
aload 1
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
goto L1
.limit locals 3
.limit stack 7
.end method

.method private a(Ljava/lang/ref/Reference;)Z
.catch java/lang/Throwable from L0 to L1 using L2
aload 0
invokespecial com/a/b/b/a/a/a()Ljava/lang/reflect/Method;
astore 3
aload 3
ifnonnull L3
L4:
iconst_0
ireturn
L3:
aload 1
invokevirtual java/lang/ref/Reference/clear()V
aload 1
aload 0
getfield com/a/b/b/a/a/d Ljava/lang/ref/PhantomReference;
if_acmpeq L4
L0:
aload 3
aload 1
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 0
getfield com/a/b/b/a/a/e Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
astore 2
aload 2
astore 1
aload 2
ifnonnull L3
iconst_1
ireturn
L2:
astore 1
getstatic com/a/b/b/a/a/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "Error cleaning up after reference."
aload 1
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
goto L1
.limit locals 4
.limit stack 4
.end method

.method private static b()Ljava/lang/reflect/Field;
.catch java/lang/Throwable from L0 to L1 using L2
L0:
ldc java/lang/Thread
ldc "inheritableThreadLocals"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 0
aload 0
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
aload 0
areturn
L2:
astore 0
getstatic com/a/b/b/a/a/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Couldn't access Thread.inheritableThreadLocals. Reference finalizer threads will inherit thread local values."
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aconst_null
areturn
.limit locals 1
.limit stack 3
.end method

.method public run()V
.catch java/lang/InterruptedException from L0 to L1 using L2
L0:
aload 0
aload 0
getfield com/a/b/b/a/a/e Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/remove()Ljava/lang/ref/Reference;
invokespecial com/a/b/b/a/a/a(Ljava/lang/ref/Reference;)Z
istore 1
L1:
iload 1
ifne L0
return
L2:
astore 2
goto L0
.limit locals 3
.limit stack 2
.end method
