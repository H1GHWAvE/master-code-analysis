.bytecode 50.0
.class synchronized abstract com/a/b/b/dt
.super com/a/b/b/b

.field final 'c' Ljava/lang/CharSequence;

.field final 'd' Lcom/a/b/b/m;

.field final 'e' Z

.field 'f' I

.field 'g' I

.method protected <init>(Lcom/a/b/b/di;Ljava/lang/CharSequence;)V
aload 0
invokespecial com/a/b/b/b/<init>()V
aload 0
iconst_0
putfield com/a/b/b/dt/f I
aload 0
aload 1
getfield com/a/b/b/di/a Lcom/a/b/b/m;
putfield com/a/b/b/dt/d Lcom/a/b/b/m;
aload 0
aload 1
getfield com/a/b/b/di/b Z
putfield com/a/b/b/dt/e Z
aload 0
aload 1
getfield com/a/b/b/di/c I
putfield com/a/b/b/dt/g I
aload 0
aload 2
putfield com/a/b/b/dt/c Ljava/lang/CharSequence;
return
.limit locals 3
.limit stack 2
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/a/b/b/dt/f I
istore 2
L0:
aload 0
getfield com/a/b/b/dt/f I
iconst_m1
if_icmpeq L1
aload 0
aload 0
getfield com/a/b/b/dt/f I
invokevirtual com/a/b/b/dt/a(I)I
istore 1
iload 1
iconst_m1
if_icmpne L2
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
istore 1
aload 0
iconst_m1
putfield com/a/b/b/dt/f I
L3:
aload 0
getfield com/a/b/b/dt/f I
iload 2
if_icmpne L4
aload 0
aload 0
getfield com/a/b/b/dt/f I
iconst_1
iadd
putfield com/a/b/b/dt/f I
aload 0
getfield com/a/b/b/dt/f I
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
if_icmplt L0
aload 0
iconst_m1
putfield com/a/b/b/dt/f I
goto L0
L2:
aload 0
aload 0
iload 1
invokevirtual com/a/b/b/dt/b(I)I
putfield com/a/b/b/dt/f I
goto L3
L5:
iload 2
iload 1
if_icmpge L6
aload 0
getfield com/a/b/b/dt/d Lcom/a/b/b/m;
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokevirtual com/a/b/b/m/c(C)Z
ifeq L6
iload 2
iconst_1
iadd
istore 2
goto L5
L7:
iload 1
iload 2
if_icmple L8
aload 0
getfield com/a/b/b/dt/d Lcom/a/b/b/m;
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
iload 1
iconst_1
isub
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokevirtual com/a/b/b/m/c(C)Z
ifeq L8
iload 1
iconst_1
isub
istore 1
goto L7
L8:
aload 0
getfield com/a/b/b/dt/e Z
ifeq L9
iload 2
iload 1
if_icmpne L9
aload 0
getfield com/a/b/b/dt/f I
istore 2
goto L0
L9:
aload 0
getfield com/a/b/b/dt/g I
iconst_1
if_icmpne L10
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
istore 1
aload 0
iconst_m1
putfield com/a/b/b/dt/f I
L11:
iload 1
istore 3
iload 1
iload 2
if_icmple L12
iload 1
istore 3
aload 0
getfield com/a/b/b/dt/d Lcom/a/b/b/m;
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
iload 1
iconst_1
isub
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokevirtual com/a/b/b/m/c(C)Z
ifeq L12
iload 1
iconst_1
isub
istore 1
goto L11
L10:
aload 0
aload 0
getfield com/a/b/b/dt/g I
iconst_1
isub
putfield com/a/b/b/dt/g I
iload 1
istore 3
L12:
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
iload 2
iload 3
invokeinterface java/lang/CharSequence/subSequence(II)Ljava/lang/CharSequence; 2
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
areturn
L1:
aload 0
invokevirtual com/a/b/b/dt/b()Ljava/lang/Object;
pop
aconst_null
areturn
L6:
goto L7
L4:
goto L5
.limit locals 4
.limit stack 4
.end method

.method abstract a(I)I
.end method

.method protected final synthetic a()Ljava/lang/Object;
aload 0
getfield com/a/b/b/dt/f I
istore 2
L0:
aload 0
getfield com/a/b/b/dt/f I
iconst_m1
if_icmpeq L1
aload 0
aload 0
getfield com/a/b/b/dt/f I
invokevirtual com/a/b/b/dt/a(I)I
istore 1
iload 1
iconst_m1
if_icmpne L2
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
istore 1
aload 0
iconst_m1
putfield com/a/b/b/dt/f I
L3:
aload 0
getfield com/a/b/b/dt/f I
iload 2
if_icmpne L4
aload 0
aload 0
getfield com/a/b/b/dt/f I
iconst_1
iadd
putfield com/a/b/b/dt/f I
aload 0
getfield com/a/b/b/dt/f I
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
if_icmplt L0
aload 0
iconst_m1
putfield com/a/b/b/dt/f I
goto L0
L2:
aload 0
aload 0
iload 1
invokevirtual com/a/b/b/dt/b(I)I
putfield com/a/b/b/dt/f I
goto L3
L5:
iload 2
iload 1
if_icmpge L6
aload 0
getfield com/a/b/b/dt/d Lcom/a/b/b/m;
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokevirtual com/a/b/b/m/c(C)Z
ifeq L6
iload 2
iconst_1
iadd
istore 2
goto L5
L7:
iload 1
iload 2
if_icmple L8
aload 0
getfield com/a/b/b/dt/d Lcom/a/b/b/m;
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
iload 1
iconst_1
isub
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokevirtual com/a/b/b/m/c(C)Z
ifeq L8
iload 1
iconst_1
isub
istore 1
goto L7
L8:
aload 0
getfield com/a/b/b/dt/e Z
ifeq L9
iload 2
iload 1
if_icmpne L9
aload 0
getfield com/a/b/b/dt/f I
istore 2
goto L0
L9:
aload 0
getfield com/a/b/b/dt/g I
iconst_1
if_icmpne L10
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
istore 1
aload 0
iconst_m1
putfield com/a/b/b/dt/f I
L11:
iload 1
istore 3
iload 1
iload 2
if_icmple L12
iload 1
istore 3
aload 0
getfield com/a/b/b/dt/d Lcom/a/b/b/m;
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
iload 1
iconst_1
isub
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokevirtual com/a/b/b/m/c(C)Z
ifeq L12
iload 1
iconst_1
isub
istore 1
goto L11
L10:
aload 0
aload 0
getfield com/a/b/b/dt/g I
iconst_1
isub
putfield com/a/b/b/dt/g I
iload 1
istore 3
L12:
aload 0
getfield com/a/b/b/dt/c Ljava/lang/CharSequence;
iload 2
iload 3
invokeinterface java/lang/CharSequence/subSequence(II)Ljava/lang/CharSequence; 2
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
areturn
L1:
aload 0
invokevirtual com/a/b/b/dt/b()Ljava/lang/Object;
pop
aconst_null
areturn
L6:
goto L7
L4:
goto L5
.limit locals 4
.limit stack 4
.end method

.method abstract b(I)I
.end method
