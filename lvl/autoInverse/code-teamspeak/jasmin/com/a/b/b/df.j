.bytecode 50.0
.class final synchronized com/a/b/b/df
.super java/lang/Object
.implements com/a/b/b/co
.implements java/io/Serializable

.field private static final 'b' J = 0L


.field private final 'a' Ljava/util/List;

.method private <init>(Ljava/util/List;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/b/df/a Ljava/util/List;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Ljava/util/List;B)V
aload 0
aload 1
invokespecial com/a/b/b/df/<init>(Ljava/util/List;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 4
iconst_0
istore 2
L0:
iload 4
istore 3
iload 2
aload 0
getfield com/a/b/b/df/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield com/a/b/b/df/a Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/a/b/b/co
aload 1
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L2
iconst_1
istore 3
L1:
iload 3
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 5
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/b/df
ifeq L0
aload 1
checkcast com/a/b/b/df
astore 1
aload 0
getfield com/a/b/b/df/a Ljava/util/List;
aload 1
getfield com/a/b/b/df/a Ljava/util/List;
invokeinterface java/util/List/equals(Ljava/lang/Object;)Z 1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/b/df/a Ljava/util/List;
invokeinterface java/util/List/hashCode()I 0
ldc_w 87855567
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
invokestatic com/a/b/b/cp/b()Lcom/a/b/b/bv;
aload 0
getfield com/a/b/b/df/a Ljava/util/List;
invokevirtual com/a/b/b/bv/a(Ljava/lang/Iterable;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 15
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Predicates.or("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
