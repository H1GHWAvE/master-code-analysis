.bytecode 50.0
.class final synchronized com/a/b/b/cw
.super java/lang/Object
.implements com/a/b/b/co
.implements java/io/Serializable

.field private static final 'b' J = 0L


.field private final 'a' Ljava/util/Collection;

.method private <init>(Ljava/util/Collection;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
putfield com/a/b/b/cw/a Ljava/util/Collection;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Ljava/util/Collection;B)V
aload 0
aload 1
invokespecial com/a/b/b/cw/<init>(Ljava/util/Collection;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/NullPointerException from L0 to L1 using L2
.catch java/lang/ClassCastException from L0 to L1 using L3
L0:
aload 0
getfield com/a/b/b/cw/a Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
istore 2
L1:
iload 2
ireturn
L3:
astore 1
iconst_0
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/b/cw
ifeq L0
aload 1
checkcast com/a/b/b/cw
astore 1
aload 0
getfield com/a/b/b/cw/a Ljava/util/Collection;
aload 1
getfield com/a/b/b/cw/a Ljava/util/Collection;
invokeinterface java/util/Collection/equals(Ljava/lang/Object;)Z 1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/b/cw/a Ljava/util/Collection;
invokeinterface java/util/Collection/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/b/cw/a Ljava/util/Collection;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 15
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Predicates.in("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
