.bytecode 50.0
.class final synchronized com/a/b/b/bd
.super java/lang/Object
.implements com/a/b/b/bf

.field private static final 'a' Ljava/lang/String; = "Could not load Finalizer in its own class loader.Loading Finalizer in the current class loader instead. As a result, you will not be ableto garbage collect this class loader. To support reclaiming this class loader, eitherresolve the underlying issue, or move Google Collections to your system class path."

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/net/URL;)Ljava/net/URLClassLoader;
new java/net/URLClassLoader
dup
iconst_1
anewarray java/net/URL
dup
iconst_0
aload 0
aastore
aconst_null
invokespecial java/net/URLClassLoader/<init>([Ljava/net/URL;Ljava/lang/ClassLoader;)V
areturn
.limit locals 1
.limit stack 6
.end method

.method private b()Ljava/net/URL;
ldc "com.google.common.base.internal.Finalizer"
bipush 46
bipush 47
invokevirtual java/lang/String/replace(CC)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc ".class"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
aload 1
invokevirtual java/lang/ClassLoader/getResource(Ljava/lang/String;)Ljava/net/URL;
astore 2
aload 2
ifnonnull L0
new java/io/FileNotFoundException
dup
aload 1
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokevirtual java/net/URL/toString()Ljava/lang/String;
astore 3
aload 3
aload 1
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L1
aload 3
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 1
invokevirtual java/lang/String/length()I
ifeq L2
ldc "Unsupported path style: "
aload 1
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 1
L3:
new java/io/IOException
dup
aload 1
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L2:
new java/lang/String
dup
ldc "Unsupported path style: "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 1
goto L3
L1:
new java/net/URL
dup
aload 2
aload 3
iconst_0
aload 3
invokevirtual java/lang/String/length()I
aload 1
invokevirtual java/lang/String/length()I
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokespecial java/net/URL/<init>(Ljava/net/URL;Ljava/lang/String;)V
areturn
.limit locals 4
.limit stack 7
.end method

.method public final a()Ljava/lang/Class;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L2 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L8 to L9 using L2
L0:
ldc "com.google.common.base.internal.Finalizer"
bipush 46
bipush 47
invokevirtual java/lang/String/replace(CC)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc ".class"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
aload 1
invokevirtual java/lang/ClassLoader/getResource(Ljava/lang/String;)Ljava/net/URL;
astore 2
L1:
aload 2
ifnonnull L4
L3:
new java/io/FileNotFoundException
dup
aload 1
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
invokestatic com/a/b/b/bc/b()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Could not load Finalizer in its own class loader.Loading Finalizer in the current class loader instead. As a result, you will not be ableto garbage collect this class loader. To support reclaiming this class loader, eitherresolve the underlying issue, or move Google Collections to your system class path."
aload 1
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
aconst_null
areturn
L4:
aload 2
invokevirtual java/net/URL/toString()Ljava/lang/String;
astore 3
aload 3
aload 1
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L8
aload 3
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 1
invokevirtual java/lang/String/length()I
ifeq L6
ldc "Unsupported path style: "
aload 1
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 1
L5:
new java/io/IOException
dup
aload 1
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L6:
new java/lang/String
dup
ldc "Unsupported path style: "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 1
L7:
goto L5
L8:
new java/net/URLClassLoader
dup
iconst_1
anewarray java/net/URL
dup
iconst_0
new java/net/URL
dup
aload 2
aload 3
iconst_0
aload 3
invokevirtual java/lang/String/length()I
aload 1
invokevirtual java/lang/String/length()I
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokespecial java/net/URL/<init>(Ljava/net/URL;Ljava/lang/String;)V
aastore
aconst_null
invokespecial java/net/URLClassLoader/<init>([Ljava/net/URL;Ljava/lang/ClassLoader;)V
ldc "com.google.common.base.internal.Finalizer"
invokevirtual java/lang/ClassLoader/loadClass(Ljava/lang/String;)Ljava/lang/Class;
astore 1
L9:
aload 1
areturn
.limit locals 4
.limit stack 12
.end method
