.bytecode 50.0
.class synchronized abstract com/a/b/b/b
.super java/lang/Object
.implements java/util/Iterator
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private 'a' I

.field private 'b' Ljava/lang/Object;

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
getstatic com/a/b/b/d/b I
putfield com/a/b/b/b/a I
return
.limit locals 1
.limit stack 2
.end method

.method private c()Z
aload 0
getstatic com/a/b/b/d/d I
putfield com/a/b/b/b/a I
aload 0
aload 0
invokevirtual com/a/b/b/b/a()Ljava/lang/Object;
putfield com/a/b/b/b/b Ljava/lang/Object;
aload 0
getfield com/a/b/b/b/a I
getstatic com/a/b/b/d/c I
if_icmpeq L0
aload 0
getstatic com/a/b/b/d/a I
putfield com/a/b/b/b/a I
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method protected abstract a()Ljava/lang/Object;
.end method

.method protected final b()Ljava/lang/Object;
aload 0
getstatic com/a/b/b/d/c I
putfield com/a/b/b/b/a I
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method public final hasNext()Z
iconst_0
istore 2
aload 0
getfield com/a/b/b/b/a I
getstatic com/a/b/b/d/d I
if_icmpeq L0
iconst_1
istore 1
L1:
iload 1
invokestatic com/a/b/b/cn/b(Z)V
iload 2
istore 1
getstatic com/a/b/b/c/a [I
aload 0
getfield com/a/b/b/b/a I
iconst_1
isub
iaload
tableswitch 1
L2
L3
default : L4
L4:
aload 0
getstatic com/a/b/b/d/d I
putfield com/a/b/b/b/a I
aload 0
aload 0
invokevirtual com/a/b/b/b/a()Ljava/lang/Object;
putfield com/a/b/b/b/b Ljava/lang/Object;
iload 2
istore 1
aload 0
getfield com/a/b/b/b/a I
getstatic com/a/b/b/d/c I
if_icmpeq L2
aload 0
getstatic com/a/b/b/d/a I
putfield com/a/b/b/b/a I
iconst_1
istore 1
L2:
iload 1
ireturn
L0:
iconst_0
istore 1
goto L1
L3:
iconst_1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final next()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/b/b/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getstatic com/a/b/b/d/b I
putfield com/a/b/b/b/a I
aload 0
getfield com/a/b/b/b/b Ljava/lang/Object;
astore 1
aload 0
aconst_null
putfield com/a/b/b/b/b Ljava/lang/Object;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final remove()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method
