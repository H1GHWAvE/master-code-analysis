.bytecode 50.0
.class final synchronized com/a/b/b/at
.super com/a/b/b/ak
.implements java/io/Serializable

.field private static final 'b' J = 0L


.field private final 'a' Ljava/lang/Class;

.method <init>(Ljava/lang/Class;)V
aload 0
invokespecial com/a/b/b/ak/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Class
putfield com/a/b/b/at/a Ljava/lang/Class;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)Ljava/lang/Enum;
aload 0
getfield com/a/b/b/at/a Ljava/lang/Class;
aload 1
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Enum;)Ljava/lang/String;
aload 0
invokevirtual java/lang/Enum/name()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/Enum
invokevirtual java/lang/Enum/name()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/String
astore 1
aload 0
getfield com/a/b/b/at/a Ljava/lang/Class;
aload 1
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/b/at
ifeq L0
aload 1
checkcast com/a/b/b/at
astore 1
aload 0
getfield com/a/b/b/at/a Ljava/lang/Class;
aload 1
getfield com/a/b/b/at/a Ljava/lang/Class;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/b/at/a Ljava/lang/Class;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/b/at/a Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 29
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Enums.stringConverter("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".class)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
