.bytecode 50.0
.class synchronized com/a/b/f/n
.super java/lang/Object

.field final 'a' Ljava/lang/Object;

.field final 'b' Ljava/lang/reflect/Method;

.method <init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
ldc "EventSubscriber target cannot be null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
ldc "EventSubscriber method cannot be null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
putfield com/a/b/f/n/a Ljava/lang/Object;
aload 0
aload 2
putfield com/a/b/f/n/b Ljava/lang/reflect/Method;
aload 2
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method private a()Ljava/lang/Object;
aload 0
getfield com/a/b/f/n/a Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()Ljava/lang/reflect/Method;
aload 0
getfield com/a/b/f/n/b Ljava/lang/reflect/Method;
areturn
.limit locals 1
.limit stack 1
.end method

.method public a(Ljava/lang/Object;)V
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L0 to L1 using L3
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L4
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
aload 0
getfield com/a/b/f/n/b Ljava/lang/reflect/Method;
aload 0
getfield com/a/b/f/n/a Ljava/lang/Object;
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 2
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/Error
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 33
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Method rejected target/argument: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokespecial java/lang/Error/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L3:
astore 2
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/Error
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 28
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Method became inaccessible: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokespecial java/lang/Error/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L4:
astore 1
aload 1
invokevirtual java/lang/reflect/InvocationTargetException/getCause()Ljava/lang/Throwable;
instanceof java/lang/Error
ifeq L5
aload 1
invokevirtual java/lang/reflect/InvocationTargetException/getCause()Ljava/lang/Throwable;
checkcast java/lang/Error
athrow
L5:
aload 1
athrow
.limit locals 3
.limit stack 6
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/f/n
ifeq L0
aload 1
checkcast com/a/b/f/n
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/f/n/a Ljava/lang/Object;
aload 1
getfield com/a/b/f/n/a Ljava/lang/Object;
if_acmpne L0
iload 3
istore 2
aload 0
getfield com/a/b/f/n/b Ljava/lang/reflect/Method;
aload 1
getfield com/a/b/f/n/b Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public hashCode()I
aload 0
getfield com/a/b/f/n/b Ljava/lang/reflect/Method;
invokevirtual java/lang/reflect/Method/hashCode()I
bipush 31
iadd
bipush 31
imul
aload 0
getfield com/a/b/f/n/a Ljava/lang/Object;
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/f/n/b Ljava/lang/reflect/Method;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 10
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "[wrapper "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
