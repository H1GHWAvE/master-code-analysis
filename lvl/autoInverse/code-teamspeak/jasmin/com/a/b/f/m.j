.bytecode 50.0
.class final synchronized com/a/b/f/m
.super java/lang/Object
.implements com/a/b/f/q

.field private final 'a' Ljava/util/logging/Logger;

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
ldc com/a/b/f/h
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
iconst_1
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putfield com/a/b/f/m/a Ljava/util/logging/Logger;
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Ljava/lang/Throwable;Lcom/a/b/f/p;)V
aload 0
getfield com/a/b/f/m/a Ljava/util/logging/Logger;
astore 3
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
astore 4
aload 2
getfield com/a/b/f/p/a Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 5
aload 2
getfield com/a/b/f/p/b Ljava/lang/reflect/Method;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 3
aload 4
new java/lang/StringBuilder
dup
aload 5
invokevirtual java/lang/String/length()I
bipush 30
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Could not dispatch event: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual java/lang/Throwable/getCause()Ljava/lang/Throwable;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 6
.limit stack 6
.end method
