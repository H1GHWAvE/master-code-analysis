.bytecode 50.0
.class public synchronized abstract com/a/a/a/a/f
.super android/os/Binder
.implements com/a/a/a/a/e

.field static final 'a' I = 1


.field private static final 'b' Ljava/lang/String; = "com.android.vending.licensing.ILicenseResultListener"

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.android.vending.licensing.ILicenseResultListener"
invokevirtual com/a/a/a/a/f/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/os/IBinder;)Lcom/a/a/a/a/e;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.android.vending.licensing.ILicenseResultListener"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/a/a/a/a/e
ifeq L1
aload 1
checkcast com/a/a/a/a/e
areturn
L1:
new com/a/a/a/a/g
dup
aload 0
invokespecial com/a/a/a/a/g/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
iload 1
lookupswitch
1 : L0
1598968902 : L1
default : L2
L2:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L1:
aload 3
ldc "com.android.vending.licensing.ILicenseResultListener"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.android.vending.licensing.ILicenseResultListener"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/a/a/a/a/f/a(ILjava/lang/String;Ljava/lang/String;)V
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method
