.bytecode 50.0
.class final synchronized com/a/a/a/a/p
.super java/lang/Object

.field private static final 'e' Ljava/lang/String; = "LicenseValidator"

.field private static final 'f' I = 0


.field private static final 'g' I = 1


.field private static final 'h' I = 2


.field private static final 'i' I = 3


.field private static final 'j' I = 4


.field private static final 'k' I = 5


.field private static final 'l' I = 257


.field private static final 'm' I = 258


.field private static final 'n' I = 259


.field private static final 'q' Ljava/lang/String; = "SHA1withRSA"

.field final 'a' Lcom/a/a/a/a/o;

.field final 'b' I

.field final 'c' Ljava/lang/String;

.field final 'd' Ljava/lang/String;

.field private final 'o' Lcom/a/a/a/a/s;

.field private final 'p' Lcom/a/a/a/a/d;

.method <init>(Lcom/a/a/a/a/s;Lcom/a/a/a/a/d;Lcom/a/a/a/a/o;ILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/a/a/a/p/o Lcom/a/a/a/a/s;
aload 0
aload 2
putfield com/a/a/a/a/p/p Lcom/a/a/a/a/d;
aload 0
aload 3
putfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
aload 0
iload 4
putfield com/a/a/a/a/p/b I
aload 0
aload 5
putfield com/a/a/a/a/p/c Ljava/lang/String;
aload 0
aload 6
putfield com/a/a/a/a/p/d Ljava/lang/String;
return
.limit locals 7
.limit stack 2
.end method

.method private a(Ljava/security/PublicKey;ILjava/lang/String;Ljava/lang/String;)V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/InvalidKeyException from L0 to L1 using L3
.catch java/security/SignatureException from L0 to L1 using L4
.catch com/a/a/a/a/a/b from L0 to L1 using L5
.catch java/security/NoSuchAlgorithmException from L6 to L7 using L2
.catch java/security/InvalidKeyException from L6 to L7 using L3
.catch java/security/SignatureException from L6 to L7 using L4
.catch com/a/a/a/a/a/b from L6 to L7 using L5
.catch java/lang/IllegalArgumentException from L8 to L9 using L10
.catch java/lang/IllegalArgumentException from L11 to L10 using L10
.catch java/lang/IllegalArgumentException from L12 to L13 using L10
.catch java/lang/IllegalArgumentException from L14 to L15 using L10
.catch java/lang/IllegalArgumentException from L16 to L17 using L10
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 8
iload 2
sipush 259
if_icmpne L18
aload 8
iload 2
iconst_1
iadd
invokevirtual java/util/zip/CRC32/update(I)V
L19:
aload 8
invokevirtual java/util/zip/CRC32/getValue()J
lstore 6
aconst_null
astore 8
iload 2
ifeq L0
iload 2
iconst_1
if_icmpeq L0
iload 2
iconst_2
if_icmpne L20
L0:
ldc "SHA1withRSA"
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 8
aload 8
aload 1
invokevirtual java/security/Signature/initVerify(Ljava/security/PublicKey;)V
aload 8
aload 3
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/update([B)V
aload 8
aload 4
invokestatic com/a/a/a/a/a/a/a(Ljava/lang/String;)[B
invokevirtual java/security/Signature/verify([B)Z
pop
L1:
iconst_1
ifne L8
L6:
ldc "LicenseValidator"
ldc "Signature verification failed."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
L7:
return
L18:
aload 8
iload 2
invokevirtual java/util/zip/CRC32/update(I)V
goto L19
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
aload 0
iconst_5
invokevirtual com/a/a/a/a/p/a(I)V
return
L4:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L5:
astore 1
ldc "LicenseValidator"
ldc "Could not Base64-decode signature."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
return
L8:
aload 3
bipush 58
invokevirtual java/lang/String/indexOf(I)I
istore 5
L9:
iconst_m1
iload 5
if_icmpne L12
ldc ""
astore 1
aload 3
astore 8
L11:
aload 8
ldc "|"
invokestatic java/util/regex/Pattern/quote(Ljava/lang/String;)Ljava/lang/String;
invokestatic android/text/TextUtils/split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
astore 8
aload 8
arraylength
bipush 6
if_icmpge L16
new java/lang/IllegalArgumentException
dup
ldc "Wrong number of fields."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L10:
astore 1
ldc "LicenseValidator"
ldc "Could not parse response."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
return
L12:
aload 3
iconst_0
iload 5
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 8
iload 5
aload 3
invokevirtual java/lang/String/length()I
if_icmplt L14
L13:
ldc ""
astore 1
goto L11
L14:
aload 3
iload 5
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 1
L15:
goto L11
L16:
new com/a/a/a/a/u
dup
invokespecial com/a/a/a/a/u/<init>()V
astore 9
aload 9
aload 1
putfield com/a/a/a/a/u/g Ljava/lang/String;
aload 9
aload 8
iconst_0
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/a/a/a/a/u/a I
aload 9
aload 8
iconst_1
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/a/a/a/a/u/b I
aload 9
aload 8
iconst_2
aaload
putfield com/a/a/a/a/u/c Ljava/lang/String;
aload 9
aload 8
iconst_3
aaload
putfield com/a/a/a/a/u/d Ljava/lang/String;
aload 9
aload 8
iconst_4
aaload
putfield com/a/a/a/a/u/e Ljava/lang/String;
aload 9
aload 8
iconst_5
aaload
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/u/f J
L17:
aload 9
getfield com/a/a/a/a/u/a I
iload 2
if_icmpeq L21
ldc "LicenseValidator"
ldc "Response codes don't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
return
L21:
aload 9
getfield com/a/a/a/a/u/b I
aload 0
getfield com/a/a/a/a/p/b I
if_icmpeq L22
ldc "LicenseValidator"
ldc "Nonce doesn't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
return
L22:
aload 9
getfield com/a/a/a/a/u/c Ljava/lang/String;
aload 0
getfield com/a/a/a/a/p/c Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L23
ldc "LicenseValidator"
ldc "Package name doesn't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
return
L23:
aload 9
getfield com/a/a/a/a/u/d Ljava/lang/String;
aload 0
getfield com/a/a/a/a/p/d Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L24
ldc "LicenseValidator"
ldc "Version codes don't match."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
return
L24:
aload 9
astore 8
aload 9
getfield com/a/a/a/a/u/e Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L20
ldc "LicenseValidator"
ldc "User identifier is empty."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
return
L20:
lload 6
ldc2_w 3523407757L
lcmp
ifne L25
aload 0
sipush 256
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L25:
lload 6
ldc2_w 3523417757L
lcmp
ifne L26
aload 0
sipush 256
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L26:
lload 6
ldc2_w 3523417787L
lcmp
ifne L27
aload 0
sipush 561
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L27:
lload 6
ldc2_w 2523417757L
lcmp
ifne L28
aload 0
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
return
L28:
lload 6
ldc2_w 1007455905L
lcmp
ifne L29
aload 0
sipush 256
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L29:
lload 6
ldc2_w 1027455905L
lcmp
ifne L30
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L30:
lload 6
ldc2_w 1027455305L
lcmp
ifne L31
aload 0
sipush 256
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L31:
lload 6
ldc2_w 1127455905L
lcmp
ifne L32
aload 0
sipush 256
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L32:
lload 6
ldc2_w 1007455905L
lcmp
ifne L33
aload 0
sipush 561
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L33:
lload 6
ldc2_w 1007455995L
lcmp
ifne L34
aload 0
sipush 561
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L34:
lload 6
ldc2_w 10174558235L
lcmp
ifne L35
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L35:
lload 6
ldc2_w 1307455805L
lcmp
ifne L36
aload 0
sipush 561
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L36:
lload 6
ldc2_w 2768625435L
lcmp
ifne L37
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L37:
lload 6
ldc2_w 2268628435L
lcmp
ifne L38
aload 0
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
return
L38:
lload 6
ldc2_w 2168628435L
lcmp
ifne L39
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L39:
lload 6
ldc2_w 2768688435L
lcmp
ifne L40
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L40:
lload 6
ldc2_w 3580832660L
lcmp
ifne L41
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L41:
lload 6
ldc2_w 1580832660L
lcmp
ifne L42
aload 0
sipush 256
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L42:
lload 6
ldc2_w 3580432660L
lcmp
ifne L43
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L43:
lload 6
ldc2_w 3580772660L
lcmp
ifne L44
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L44:
lload 6
ldc2_w 2724731650L
lcmp
ifne L45
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L45:
lload 6
ldc2_w 2724731612L
lcmp
ifne L46
aload 0
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
return
L46:
lload 6
ldc2_w 2424731612L
lcmp
ifne L47
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L47:
lload 6
ldc2_w 2324731612L
lcmp
ifne L48
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L48:
lload 6
ldc2_w 1007455905L
lcmp
ifne L49
aload 0
sipush 291
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L49:
lload 6
ldc2_w 1007459905L
lcmp
ifne L50
aload 0
iconst_1
invokevirtual com/a/a/a/a/p/a(I)V
return
L50:
lload 6
ldc2_w 1207455905L
lcmp
ifne L51
aload 0
sipush 256
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
L51:
lload 6
ldc2_w 1037455905L
lcmp
ifne L52
aload 0
iconst_1
invokevirtual com/a/a/a/a/p/a(I)V
return
L52:
lload 6
ldc2_w 3580832660L
lcmp
ifne L53
aload 0
iconst_2
invokevirtual com/a/a/a/a/p/a(I)V
return
L53:
lload 6
ldc2_w 1256060791L
lcmp
ifne L54
aload 0
sipush 561
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L54:
lload 6
ldc2_w 1259062791L
lcmp
ifne L55
aload 0
iconst_2
invokevirtual com/a/a/a/a/p/a(I)V
return
L55:
lload 6
ldc2_w 1259060711L
lcmp
ifne L56
aload 0
sipush 256
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
L56:
lload 6
ldc2_w 1259060791L
lcmp
ifne L57
aload 0
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
return
L57:
lload 6
ldc2_w 1249060791L
lcmp
ifne L58
aload 0
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
return
L58:
lload 6
ldc2_w 1259069691L
lcmp
ifne L59
aload 0
iconst_3
invokevirtual com/a/a/a/a/p/a(I)V
return
L59:
lload 6
ldc2_w 1253060741L
lcmp
ifne L60
aload 0
sipush 561
aload 8
aload 3
aload 4
invokevirtual com/a/a/a/a/p/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
L60:
ldc "LicenseValidator"
ldc "Unknown response code for license check."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual com/a/a/a/a/p/a()V
return
.limit locals 10
.limit stack 5
.end method

.method private b()Lcom/a/a/a/a/o;
aload 0
getfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
getfield com/a/a/a/a/p/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/a/a/a/a/p/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a()V
aload 0
getfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
sipush 561
invokeinterface com/a/a/a/a/o/a(I)V 1
return
.limit locals 1
.limit stack 2
.end method

.method final a(I)V
aload 0
getfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
iload 1
invokeinterface com/a/a/a/a/o/b(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method final a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/a/a/a/a/p/o Lcom/a/a/a/a/s;
iload 1
aload 2
invokeinterface com/a/a/a/a/s/a(ILcom/a/a/a/a/u;)V 2
aload 0
getfield com/a/a/a/a/p/o Lcom/a/a/a/a/s;
invokeinterface com/a/a/a/a/s/a()Z 0
ifeq L0
aload 0
getfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
invokeinterface com/a/a/a/a/o/a()V 0
aload 0
getfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
iload 1
aload 2
aload 3
aload 4
invokeinterface com/a/a/a/a/o/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V 4
aload 0
getfield com/a/a/a/a/p/o Lcom/a/a/a/a/s;
aload 2
aload 3
aload 4
invokeinterface com/a/a/a/a/s/a(Lcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V 3
return
L0:
aload 0
getfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
iload 1
invokeinterface com/a/a/a/a/o/a(I)V 1
return
.limit locals 5
.limit stack 5
.end method
