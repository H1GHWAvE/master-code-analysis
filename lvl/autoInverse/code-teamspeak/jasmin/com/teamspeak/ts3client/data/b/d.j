.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/data/b/d
.super android/database/sqlite/SQLiteOpenHelper

.field final synthetic 'a' Lcom/teamspeak/ts3client/data/b/c;

.method public <init>(Lcom/teamspeak/ts3client/data/b/c;Landroid/content/Context;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/b/d/a Lcom/teamspeak/ts3client/data/b/c;
aload 0
aload 2
ldc "Teamspeak-Contacts"
aconst_null
iconst_2
invokespecial android/database/sqlite/SQLiteOpenHelper/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
return
.limit locals 3
.limit stack 5
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
aload 1
ldc "create table contacts (contact_id integer primary key autoincrement, u_identifier text not null, customname text not null , display integer not null, status integer not null, mute integer not null, ignorepublicchat integer not null, ignoreprivatechat integer not null, ignorepokes integer not null, hideaway integer not null,hideavatar integer not null, whisperallow integer not null, volumemodifier float not null);"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
aload 1
ldc "Create Index u_identifier_idx ON contacts(u_identifier);"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
iload 2
iload 3
if_icmpge L0
L1:
iload 2
iconst_1
isub
iload 3
if_icmpge L0
iload 2
tableswitch 2
L2
default : L3
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 1
ldc "ALTER TABLE contacts ADD volumemodifier float not null DEFAULT('0')"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
goto L3
L0:
return
.limit locals 4
.limit stack 2
.end method
