.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/l
.super android/os/AsyncTask

.field private static final 'b' J = 1048576L


.field final synthetic 'a' Lcom/teamspeak/ts3client/data/d/j;

.field private 'c' I

.field private 'd' Ljava/lang/String;

.field private 'e' I

.field private 'f' Ljava/lang/String;

.field private 'g' I

.method public <init>(Lcom/teamspeak/ts3client/data/d/j;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
aload 0
invokespecial android/os/AsyncTask/<init>()V
aload 0
ldc "ContentUPDATER"
putfield com/teamspeak/ts3client/data/d/l/d Ljava/lang/String;
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/d/l/e I
aload 0
bipush 100
putfield com/teamspeak/ts3client/data/d/l/g I
return
.limit locals 2
.limit stack 2
.end method

.method private transient a()Ljava/lang/Boolean;
.catch java/net/MalformedURLException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/net/MalformedURLException from L1 to L4 using L2
.catch java/io/IOException from L1 to L4 using L3
.catch java/net/MalformedURLException from L4 to L5 using L2
.catch java/io/IOException from L4 to L5 using L3
.catch java/net/MalformedURLException from L6 to L7 using L2
.catch java/io/IOException from L6 to L7 using L3
.catch java/net/MalformedURLException from L8 to L9 using L2
.catch java/io/IOException from L8 to L9 using L3
.catch java/net/MalformedURLException from L10 to L11 using L2
.catch java/io/IOException from L10 to L11 using L3
.catch java/io/IOException from L12 to L13 using L14
.catch java/io/IOException from L13 to L15 using L14
.catch java/io/IOException from L16 to L17 using L14
.catch java/io/IOException from L18 to L19 using L14
.catch java/io/IOException from L19 to L20 using L14
.catch java/io/IOException from L21 to L22 using L14
.catch java/io/IOException from L23 to L24 using L14
.catch java/io/IOException from L25 to L26 using L14
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/update/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 5
aload 5
invokevirtual java/io/File/exists()Z
ifne L1
aload 5
invokevirtual java/io/File/mkdirs()Z
pop
L1:
new java/net/URL
dup
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/b Ljava/lang/String;
invokespecial java/net/URL/<init>(Ljava/lang/String;)V
astore 5
aload 5
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
astore 6
aload 6
invokevirtual java/net/URLConnection/connect()V
aload 0
aload 6
invokevirtual java/net/URLConnection/getContentLength()I
putfield com/teamspeak/ts3client/data/d/l/c I
aload 0
getfield com/teamspeak/ts3client/data/d/l/c I
ifgt L4
aload 0
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/h I
putfield com/teamspeak/ts3client/data/d/l/c I
L4:
new java/io/BufferedInputStream
dup
aload 5
invokevirtual java/net/URL/openStream()Ljava/io/InputStream;
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
astore 5
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/update/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ".zip"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 6
sipush 1024
newarray byte
astore 7
L5:
lconst_0
lstore 3
L6:
aload 5
aload 7
invokevirtual java/io/InputStream/read([B)I
istore 1
L7:
iload 1
iconst_m1
if_icmpeq L10
lload 3
iload 1
i2l
ladd
lstore 3
L8:
aload 0
iconst_1
anewarray java/lang/Integer
dup
iconst_0
lload 3
l2i
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual com/teamspeak/ts3client/data/d/l/publishProgress([Ljava/lang/Object;)V
aload 6
aload 7
iconst_0
iload 1
invokevirtual java/io/OutputStream/write([BII)V
L9:
goto L6
L2:
astore 5
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 5
invokevirtual java/net/MalformedURLException/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L10:
aload 6
invokevirtual java/io/OutputStream/flush()V
aload 6
invokevirtual java/io/OutputStream/close()V
aload 5
invokevirtual java/io/InputStream/close()V
L11:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/update/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ".zip"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 5
L12:
aload 0
iconst_2
putfield com/teamspeak/ts3client/data/d/l/e I
new java/util/zip/ZipFile
dup
aload 5
invokespecial java/util/zip/ZipFile/<init>(Ljava/io/File;)V
astore 6
aload 0
aload 6
invokevirtual java/util/zip/ZipFile/size()I
putfield com/teamspeak/ts3client/data/d/l/g I
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/a Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifne L13
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/a Ljava/io/File;
invokevirtual java/io/File/mkdirs()Z
pop
L13:
aload 6
invokevirtual java/util/zip/ZipFile/entries()Ljava/util/Enumeration;
astore 7
L15:
iconst_0
istore 1
L16:
aload 7
invokeinterface java/util/Enumeration/hasMoreElements()Z 0
ifeq L25
aload 7
invokeinterface java/util/Enumeration/nextElement()Ljava/lang/Object; 0
checkcast java/util/zip/ZipEntry
astore 9
new java/io/File
dup
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/a Ljava/io/File;
aload 9
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 8
aload 8
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokevirtual java/io/File/mkdirs()Z
pop
L17:
iload 1
iconst_1
iadd
istore 2
iload 2
istore 1
L18:
aload 9
invokevirtual java/util/zip/ZipEntry/isDirectory()Z
ifne L16
aload 0
aload 8
invokevirtual java/io/File/getName()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/l/f Ljava/lang/String;
aload 0
iconst_1
anewarray java/lang/Integer
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual com/teamspeak/ts3client/data/d/l/publishProgress([Ljava/lang/Object;)V
new java/io/BufferedInputStream
dup
aload 6
aload 9
invokevirtual java/util/zip/ZipFile/getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
astore 9
sipush 1024
newarray byte
astore 10
new java/io/BufferedOutputStream
dup
new java/io/FileOutputStream
dup
aload 8
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
sipush 1024
invokespecial java/io/BufferedOutputStream/<init>(Ljava/io/OutputStream;I)V
astore 8
L19:
aload 9
aload 10
iconst_0
sipush 1024
invokevirtual java/io/BufferedInputStream/read([BII)I
istore 1
L20:
iload 1
iconst_m1
if_icmpeq L23
L21:
aload 8
aload 10
iconst_0
iload 1
invokevirtual java/io/BufferedOutputStream/write([BII)V
L22:
goto L19
L14:
astore 6
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 6
invokevirtual java/io/IOException/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 5
invokevirtual java/io/File/delete()Z
pop
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L3:
astore 5
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 5
invokevirtual java/io/IOException/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L23:
aload 8
invokevirtual java/io/BufferedOutputStream/flush()V
aload 8
invokevirtual java/io/BufferedOutputStream/close()V
aload 9
invokevirtual java/io/BufferedInputStream/close()V
L24:
iload 2
istore 1
goto L16
L25:
aload 6
invokevirtual java/util/zip/ZipFile/close()V
L26:
aload 5
invokevirtual java/io/File/delete()Z
pop
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 11
.limit stack 6
.end method

.method private a(Ljava/lang/Boolean;)V
aload 0
aload 1
invokespecial android/os/AsyncTask/onPostExecute(Ljava/lang/Object;)V
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
ldc "contentdownloader.error"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
iconst_1
invokevirtual android/app/ProgressDialog/setCancelable(Z)V
return
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/dismiss()V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/d Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/c I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 2
.limit stack 3
.end method

.method private transient a([Ljava/lang/Integer;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
aload 0
getfield com/teamspeak/ts3client/data/d/l/g I
invokevirtual android/app/ProgressDialog/setMax(I)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
aload 1
iconst_0
aaload
invokevirtual java/lang/Integer/intValue()I
invokevirtual android/app/ProgressDialog/setProgress(I)V
aload 1
iconst_0
aaload
invokevirtual java/lang/Integer/intValue()I
iconst_m1
if_icmpne L0
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
ldc "Download Failed\nPlease try again."
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
iconst_1
invokevirtual android/app/ProgressDialog/setCancelable(Z)V
L1:
aload 0
getfield com/teamspeak/ts3client/data/d/l/e I
iconst_2
if_icmpne L2
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
ldc "contentdownloader.extracting"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/d/l/f Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
L2:
return
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
new java/lang/StringBuilder
dup
ldc "Downloaded "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc "%.2f %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
iconst_0
aaload
invokevirtual java/lang/Integer/intValue()I
i2d
ldc2_w 1000000.0D
ddiv
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
aastore
dup
iconst_1
ldc "MB"
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "%.2f %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/d/l/c I
i2d
ldc2_w 1000000.0D
ddiv
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
aastore
dup
iconst_1
ldc "MB"
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
goto L1
.limit locals 2
.limit stack 10
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/teamspeak/ts3client/data/d/l/a()Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 1
checkcast java/lang/Boolean
astore 1
aload 0
aload 1
invokespecial android/os/AsyncTask/onPostExecute(Ljava/lang/Object;)V
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
ldc "contentdownloader.error"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
iconst_1
invokevirtual android/app/ProgressDialog/setCancelable(Z)V
return
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/dismiss()V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/d Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/c I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 2
.limit stack 3
.end method

.method protected final onPreExecute()V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
new java/lang/StringBuilder
dup
ldc "Update "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/app/ProgressDialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
ldc_w 2130837622
invokevirtual android/app/ProgressDialog/setIcon(I)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
bipush 100
invokevirtual android/app/ProgressDialog/setMax(I)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
iconst_0
invokevirtual android/app/ProgressDialog/setProgressStyle(I)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
ldc "contentdownloader.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
iconst_0
invokevirtual android/app/ProgressDialog/setCancelable(Z)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
new com/teamspeak/ts3client/data/d/m
dup
aload 0
invokespecial com/teamspeak/ts3client/data/d/m/<init>(Lcom/teamspeak/ts3client/data/d/l;)V
invokevirtual android/app/ProgressDialog/setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/show()V
return
.limit locals 1
.limit stack 4
.end method

.method protected final synthetic onProgressUpdate([Ljava/lang/Object;)V
aload 1
checkcast [Ljava/lang/Integer;
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
aload 0
getfield com/teamspeak/ts3client/data/d/l/g I
invokevirtual android/app/ProgressDialog/setMax(I)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
aload 1
iconst_0
aaload
invokevirtual java/lang/Integer/intValue()I
invokevirtual android/app/ProgressDialog/setProgress(I)V
aload 1
iconst_0
aaload
invokevirtual java/lang/Integer/intValue()I
iconst_m1
if_icmpne L0
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
ldc "Download Failed\nPlease try again."
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
iconst_1
invokevirtual android/app/ProgressDialog/setCancelable(Z)V
L1:
aload 0
getfield com/teamspeak/ts3client/data/d/l/e I
iconst_2
if_icmpne L2
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
ldc "contentdownloader.extracting"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/d/l/f Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
L2:
return
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/l/a Lcom/teamspeak/ts3client/data/d/j;
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
new java/lang/StringBuilder
dup
ldc "Downloaded "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc "%.2f %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
iconst_0
aaload
invokevirtual java/lang/Integer/intValue()I
i2d
ldc2_w 1000000.0D
ddiv
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
aastore
dup
iconst_1
ldc "MB"
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "%.2f %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/d/l/c I
i2d
ldc2_w 1000000.0D
ddiv
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
aastore
dup
iconst_1
ldc "MB"
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
goto L1
.limit locals 2
.limit stack 10
.end method
