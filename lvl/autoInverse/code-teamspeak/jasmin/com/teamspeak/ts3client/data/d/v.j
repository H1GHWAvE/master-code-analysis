.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/v
.super java/lang/Object
.implements com/teamspeak/ts3client/data/w
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "UseSparseArrays" 
.end annotation

.field 'a' Ljava/util/HashMap;

.field 'b' Ljava/util/HashMap;

.field public 'c' Z

.field private 'd' Ljava/lang/String;

.field private 'e' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'f' Ljava/lang/String;

.field private 'g' Ljava/lang/String;

.method public <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "UseSparseArrays" 
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/v/d Ljava/lang/String;
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/d/v/c Z
aload 0
aload 1
putfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokespecial com/teamspeak/ts3client/data/d/v/a()Z
ifne L0
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "request ServerPermissionList"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestPermissionList(JLjava/lang/String;)I
pop
L0:
return
.limit locals 2
.limit stack 4
.end method

.method private a(I)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
areturn
L0:
ldc ""
areturn
.limit locals 2
.limit stack 2
.end method

.method private a()Z
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/StreamCorruptedException from L0 to L1 using L3
.catch java/io/IOException from L0 to L1 using L4
.catch java/lang/ClassNotFoundException from L0 to L1 using L5
.catch java/io/FileNotFoundException from L6 to L7 using L2
.catch java/io/StreamCorruptedException from L6 to L7 using L3
.catch java/io/IOException from L6 to L7 using L4
.catch java/lang/ClassNotFoundException from L6 to L7 using L5
aload 0
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/v/f Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/e Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/f Ljava/lang/String;
invokevirtual java/lang/String/isEmpty()Z
ifne L8
aload 1
invokevirtual java/lang/String/isEmpty()Z
ifeq L9
L8:
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Don't have a unique server id or version, aborting load permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iconst_0
ireturn
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/data/d/v/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
ldc "/"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
aload 3
invokevirtual java/io/File/exists()Z
ifne L10
aload 3
invokevirtual java/io/File/mkdirs()Z
pop
L10:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "perm.dat"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/io/File/exists()Z
ifne L0
iconst_0
ireturn
L0:
new java/io/FileInputStream
dup
aload 2
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
new java/io/ObjectInputStream
dup
aload 2
invokespecial java/io/ObjectInputStream/<init>(Ljava/io/InputStream;)V
astore 3
aload 0
aload 3
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/d/v/g Ljava/lang/String;
aload 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/g Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L6
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Found new ServerVersion, aborting load permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 3
invokevirtual java/io/ObjectInputStream/close()V
aload 2
invokevirtual java/io/FileInputStream/close()V
L1:
iconst_0
ireturn
L6:
aload 0
aload 3
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/util/HashMap
putfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
aload 0
aload 3
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/util/HashMap
putfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
aload 3
invokevirtual java/io/ObjectInputStream/close()V
aload 2
invokevirtual java/io/FileInputStream/close()V
L7:
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Loaded Permissions from perm.dat"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/d/v/c Z
iconst_1
ireturn
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Failed to read new perm.dat, aborting load permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iconst_0
ireturn
L3:
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Failed to read new perm.dat, aborting load permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iconst_0
ireturn
L4:
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Failed to read new perm.dat, aborting load permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iconst_0
ireturn
L5:
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Failed to read new perm.dat, aborting load permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method private b()V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
aload 0
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/v/f Ljava/lang/String;
aload 0
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/e Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/v/g Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
invokevirtual java/util/HashMap/isEmpty()Z
ifne L4
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
invokevirtual java/util/HashMap/isEmpty()Z
ifeq L5
L4:
return
L5:
aload 0
getfield com/teamspeak/ts3client/data/d/v/f Ljava/lang/String;
invokevirtual java/lang/String/isEmpty()Z
ifne L6
aload 0
getfield com/teamspeak/ts3client/data/d/v/g Ljava/lang/String;
invokevirtual java/lang/String/isEmpty()Z
ifeq L7
L6:
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Don't have a unique server id or version, aborting load permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
L7:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/data/d/v/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
ldc "/"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/io/File/exists()Z
ifne L8
aload 2
invokevirtual java/io/File/mkdirs()Z
pop
L8:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "perm.dat"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
L0:
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
new java/io/ObjectOutputStream
dup
aload 1
invokespecial java/io/ObjectOutputStream/<init>(Ljava/io/OutputStream;)V
astore 2
aload 2
aload 0
getfield com/teamspeak/ts3client/data/d/v/g Ljava/lang/String;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 2
aload 0
getfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 2
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 2
invokevirtual java/io/ObjectOutputStream/close()V
aload 1
invokevirtual java/io/FileOutputStream/close()V
L1:
return
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Failed to write new perm.dat, aborting save permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
L3:
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Failed to write new perm.dat, aborting save permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 5
.end method

.method private c()Z
aload 0
getfield com/teamspeak/ts3client/data/d/v/c Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/g;)I
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
aload 1
getfield com/teamspeak/ts3client/jni/g/iy Ljava/lang/String;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
aload 1
getfield com/teamspeak/ts3client/jni/g/iy Ljava/lang/String;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
L0:
iconst_m1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/String;)I
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
L0:
iconst_m1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/PermissionList
ifeq L4
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/PermissionList
astore 2
aload 0
getfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
aload 2
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/a I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 2
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/b Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
aload 2
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/b Ljava/lang/String;
aload 2
getfield com/teamspeak/ts3client/jni/events/rare/PermissionList/a I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/PermissionListFinished
ifeq L5
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
aload 0
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/v/f Ljava/lang/String;
aload 0
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/e Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/v/g Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
invokevirtual java/util/HashMap/isEmpty()Z
ifne L6
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
invokevirtual java/util/HashMap/isEmpty()Z
ifeq L7
L6:
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/d/v/c Z
L5:
return
L7:
aload 0
getfield com/teamspeak/ts3client/data/d/v/f Ljava/lang/String;
invokevirtual java/lang/String/isEmpty()Z
ifne L8
aload 0
getfield com/teamspeak/ts3client/data/d/v/g Ljava/lang/String;
invokevirtual java/lang/String/isEmpty()Z
ifeq L9
L8:
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Don't have a unique server id or version, aborting load permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L6
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/data/d/v/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
ldc "/"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/io/File/exists()Z
ifne L10
aload 2
invokevirtual java/io/File/mkdirs()Z
pop
L10:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "perm.dat"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
L0:
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
new java/io/ObjectOutputStream
dup
aload 1
invokespecial java/io/ObjectOutputStream/<init>(Ljava/io/OutputStream;)V
astore 2
aload 2
aload 0
getfield com/teamspeak/ts3client/data/d/v/g Ljava/lang/String;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 2
aload 0
getfield com/teamspeak/ts3client/data/d/v/a Ljava/util/HashMap;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 2
aload 0
getfield com/teamspeak/ts3client/data/d/v/b Ljava/util/HashMap;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 2
invokevirtual java/io/ObjectOutputStream/close()V
aload 1
invokevirtual java/io/FileOutputStream/close()V
L1:
goto L6
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Failed to write new perm.dat, aborting save permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L6
L3:
astore 1
aload 0
getfield com/teamspeak/ts3client/data/d/v/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "Failed to write new perm.dat, aborting save permissions"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L6
.limit locals 3
.limit stack 5
.end method
