.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/b/c
.super java/lang/Object

.field public static 'a' Lcom/teamspeak/ts3client/data/b/c;

.field private static final 'd' Ljava/lang/String; = "create table contacts (contact_id integer primary key autoincrement, u_identifier text not null, customname text not null , display integer not null, status integer not null, mute integer not null, ignorepublicchat integer not null, ignoreprivatechat integer not null, ignorepokes integer not null, hideaway integer not null,hideavatar integer not null, whisperallow integer not null, volumemodifier float not null);"

.field private static final 'e' Ljava/lang/String; = "Create Index u_identifier_idx ON contacts(u_identifier);"

.field private static final 'f' Ljava/lang/String; = "contacts"

.field private static final 'g' Ljava/lang/String; = "Teamspeak-Contacts"

.field private static final 'h' I = 2


.field public 'b' Ljava/util/Vector;

.field public 'c' Lcom/teamspeak/ts3client/c/b;

.field private 'i' Landroid/database/sqlite/SQLiteDatabase;

.field private 'j' Ljava/util/HashMap;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/data/b/c/b Ljava/util/Vector;
aload 0
aconst_null
putfield com/teamspeak/ts3client/data/b/c/j Ljava/util/HashMap;
aload 0
new com/teamspeak/ts3client/data/b/d
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/data/b/d/<init>(Lcom/teamspeak/ts3client/data/b/c;Landroid/content/Context;)V
invokevirtual com/teamspeak/ts3client/data/b/d/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
putfield com/teamspeak/ts3client/data/b/c/i Landroid/database/sqlite/SQLiteDatabase;
aload 0
putstatic com/teamspeak/ts3client/data/b/c/a Lcom/teamspeak/ts3client/data/b/c;
aload 0
invokespecial com/teamspeak/ts3client/data/b/c/f()V
return
.limit locals 2
.limit stack 5
.end method

.method private a(I)Lcom/teamspeak/ts3client/c/a;
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L3 to L4 using L2
.catch android/database/SQLException from L5 to L6 using L2
.catch android/database/SQLException from L7 to L8 using L2
.catch android/database/SQLException from L9 to L10 using L2
.catch android/database/SQLException from L11 to L12 using L2
.catch android/database/SQLException from L13 to L14 using L2
.catch android/database/SQLException from L15 to L16 using L2
.catch android/database/SQLException from L16 to L17 using L2
new com/teamspeak/ts3client/c/a
dup
invokespecial com/teamspeak/ts3client/c/a/<init>()V
astore 3
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/c/i Landroid/database/sqlite/SQLiteDatabase;
astore 4
new java/lang/StringBuilder
dup
ldc "contact_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
aload 4
ldc "contacts"
bipush 13
anewarray java/lang/String
dup
iconst_0
ldc "contact_id"
aastore
dup
iconst_1
ldc "u_identifier"
aastore
dup
iconst_2
ldc "customname"
aastore
dup
iconst_3
ldc "display"
aastore
dup
iconst_4
ldc "status"
aastore
dup
iconst_5
ldc "mute"
aastore
dup
bipush 6
ldc "ignorepublicchat"
aastore
dup
bipush 7
ldc "ignoreprivatechat"
aastore
dup
bipush 8
ldc "ignorepokes"
aastore
dup
bipush 9
ldc "hideaway"
aastore
dup
bipush 10
ldc "hideavatar"
aastore
dup
bipush 11
ldc "whisperallow"
aastore
dup
bipush 12
ldc "volumemodifier"
aastore
aload 5
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 4
aload 4
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L16
aload 3
aload 4
aload 4
ldc "contact_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/c/a/b I
aload 3
aload 4
aload 4
ldc "u_identifier"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokevirtual com/teamspeak/ts3client/c/a/a(Ljava/lang/String;)V
aload 3
aload 4
aload 4
ldc "customname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
aload 3
aload 4
aload 4
ldc "display"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/c/a/d I
aload 3
aload 4
aload 4
ldc "status"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/c/a/e I
aload 4
aload 4
ldc "mute"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L18
L1:
iconst_0
istore 2
L3:
aload 3
iload 2
putfield com/teamspeak/ts3client/c/a/f Z
aload 4
aload 4
ldc "ignorepublicchat"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L19
L4:
iconst_0
istore 2
L5:
aload 3
iload 2
putfield com/teamspeak/ts3client/c/a/g Z
aload 4
aload 4
ldc "ignoreprivatechat"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L20
L6:
iconst_0
istore 2
L7:
aload 3
iload 2
putfield com/teamspeak/ts3client/c/a/h Z
aload 4
aload 4
ldc "ignorepokes"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L21
L8:
iconst_0
istore 2
L9:
aload 3
iload 2
putfield com/teamspeak/ts3client/c/a/i Z
aload 4
aload 4
ldc "hideaway"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L22
L10:
iconst_0
istore 2
L11:
aload 3
iload 2
putfield com/teamspeak/ts3client/c/a/j Z
aload 4
aload 4
ldc "hideavatar"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L23
L12:
iconst_0
istore 2
L13:
aload 3
iload 2
putfield com/teamspeak/ts3client/c/a/k Z
aload 4
aload 4
ldc "whisperallow"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L24
L14:
iconst_0
istore 2
L15:
aload 3
iload 2
putfield com/teamspeak/ts3client/c/a/l Z
aload 3
aload 4
aload 4
ldc "volumemodifier"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getFloat(I)F 1
putfield com/teamspeak/ts3client/c/a/m F
L16:
aload 4
invokeinterface android/database/Cursor/close()V 0
L17:
aload 3
areturn
L18:
iconst_1
istore 2
goto L3
L19:
iconst_1
istore 2
goto L5
L20:
iconst_1
istore 2
goto L7
L21:
iconst_1
istore 2
goto L9
L22:
iconst_1
istore 2
goto L11
L23:
iconst_1
istore 2
goto L13
L24:
iconst_1
istore 2
goto L15
L2:
astore 3
aconst_null
areturn
.limit locals 6
.limit stack 9
.end method

.method public static a()Lcom/teamspeak/ts3client/data/b/c;
getstatic com/teamspeak/ts3client/data/b/c/a Lcom/teamspeak/ts3client/data/b/c;
areturn
.limit locals 0
.limit stack 1
.end method

.method private a(Lcom/teamspeak/ts3client/c/b;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/b/c/c Lcom/teamspeak/ts3client/c/b;
return
.limit locals 2
.limit stack 2
.end method

.method private d()V
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/data/b/c/b Ljava/util/Vector;
return
.limit locals 1
.limit stack 3
.end method

.method private e()V
aload 0
getfield com/teamspeak/ts3client/data/b/c/i Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/close()V
return
.limit locals 1
.limit stack 1
.end method

.method private f()V
aload 0
invokevirtual com/teamspeak/ts3client/data/b/c/b()Ljava/util/ArrayList;
astore 1
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/data/b/c/j Ljava/util/HashMap;
aload 1
ifnonnull L0
L1:
return
L0:
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L2:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/c/a
astore 2
aload 0
getfield com/teamspeak/ts3client/data/b/c/j Ljava/util/HashMap;
aload 2
getfield com/teamspeak/ts3client/c/a/c Ljava/lang/String;
aload 2
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L2
.limit locals 3
.limit stack 3
.end method

.method private g()V
aload 0
aconst_null
putfield com/teamspeak/ts3client/data/b/c/c Lcom/teamspeak/ts3client/c/b;
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/c/a;)J
.catch java/lang/Exception from L0 to L1 using L2
iconst_1
istore 3
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 6
aload 6
ldc "customname"
aload 1
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 6
ldc "u_identifier"
aload 1
getfield com/teamspeak/ts3client/c/a/c Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 6
ldc "display"
aload 1
getfield com/teamspeak/ts3client/c/a/d I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 6
ldc "status"
aload 1
getfield com/teamspeak/ts3client/c/a/e I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 1
getfield com/teamspeak/ts3client/c/a/f Z
ifeq L3
iconst_1
istore 2
L4:
aload 6
ldc "mute"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 1
getfield com/teamspeak/ts3client/c/a/g Z
ifeq L5
iconst_1
istore 2
L6:
aload 6
ldc "ignorepublicchat"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 1
getfield com/teamspeak/ts3client/c/a/h Z
ifeq L7
iconst_1
istore 2
L8:
aload 6
ldc "ignoreprivatechat"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 1
getfield com/teamspeak/ts3client/c/a/i Z
ifeq L9
iconst_1
istore 2
L10:
aload 6
ldc "ignorepokes"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 1
getfield com/teamspeak/ts3client/c/a/j Z
ifeq L11
iconst_1
istore 2
L12:
aload 6
ldc "hideaway"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 1
getfield com/teamspeak/ts3client/c/a/k Z
ifeq L13
iconst_1
istore 2
L14:
aload 6
ldc "hideavatar"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 1
getfield com/teamspeak/ts3client/c/a/l Z
ifeq L15
iload 3
istore 2
L16:
aload 6
ldc "whisperallow"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 6
ldc "volumemodifier"
aload 1
getfield com/teamspeak/ts3client/c/a/m F
invokestatic java/lang/Float/valueOf(F)Ljava/lang/Float;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Float;)V
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/c/i Landroid/database/sqlite/SQLiteDatabase;
ldc "contacts"
aconst_null
aload 6
invokevirtual android/database/sqlite/SQLiteDatabase/insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
lstore 4
aload 0
invokespecial com/teamspeak/ts3client/data/b/c/f()V
L1:
lload 4
lreturn
L3:
iconst_0
istore 2
goto L4
L5:
iconst_0
istore 2
goto L6
L7:
iconst_0
istore 2
goto L8
L9:
iconst_0
istore 2
goto L10
L11:
iconst_0
istore 2
goto L12
L13:
iconst_0
istore 2
goto L14
L15:
iconst_0
istore 2
goto L16
L2:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
ldc2_w -1L
lreturn
.limit locals 7
.limit stack 4
.end method

.method public final a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;
aload 1
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 1
invokestatic android/database/DatabaseUtils/sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 0
getfield com/teamspeak/ts3client/data/b/c/j Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/data/b/c/j Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/c/a
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(J)Z
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/c/i Landroid/database/sqlite/SQLiteDatabase;
ldc "contacts"
new java/lang/StringBuilder
dup
ldc "contact_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
pop
aload 0
invokespecial com/teamspeak/ts3client/data/b/c/f()V
aload 0
invokevirtual com/teamspeak/ts3client/data/b/c/c()V
L1:
iconst_1
ireturn
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 4
.limit stack 5
.end method

.method public final a(JLcom/teamspeak/ts3client/c/a;)Z
.catch java/lang/Exception from L0 to L1 using L2
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 5
aload 5
ldc "customname"
aload 3
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "u_identifier"
aload 3
getfield com/teamspeak/ts3client/c/a/c Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "display"
aload 3
getfield com/teamspeak/ts3client/c/a/d I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 5
ldc "status"
aload 3
getfield com/teamspeak/ts3client/c/a/e I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
getfield com/teamspeak/ts3client/c/a/f Z
ifeq L3
iconst_1
istore 4
L4:
aload 5
ldc "mute"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
getfield com/teamspeak/ts3client/c/a/g Z
ifeq L5
iconst_1
istore 4
L6:
aload 5
ldc "ignorepublicchat"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
getfield com/teamspeak/ts3client/c/a/h Z
ifeq L7
iconst_1
istore 4
L8:
aload 5
ldc "ignoreprivatechat"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
getfield com/teamspeak/ts3client/c/a/i Z
ifeq L9
iconst_1
istore 4
L10:
aload 5
ldc "ignorepokes"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
getfield com/teamspeak/ts3client/c/a/j Z
ifeq L11
iconst_1
istore 4
L12:
aload 5
ldc "hideaway"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
getfield com/teamspeak/ts3client/c/a/k Z
ifeq L13
iconst_1
istore 4
L14:
aload 5
ldc "hideavatar"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
getfield com/teamspeak/ts3client/c/a/l Z
ifeq L15
iconst_1
istore 4
L16:
aload 5
ldc "whisperallow"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 5
ldc "volumemodifier"
aload 3
getfield com/teamspeak/ts3client/c/a/m F
invokestatic java/lang/Float/valueOf(F)Ljava/lang/Float;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Float;)V
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/c/i Landroid/database/sqlite/SQLiteDatabase;
ldc "contacts"
aload 5
new java/lang/StringBuilder
dup
ldc "contact_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
pop
aload 0
invokespecial com/teamspeak/ts3client/data/b/c/f()V
aload 0
invokevirtual com/teamspeak/ts3client/data/b/c/c()V
L1:
iconst_1
ireturn
L3:
iconst_0
istore 4
goto L4
L5:
iconst_0
istore 4
goto L6
L7:
iconst_0
istore 4
goto L8
L9:
iconst_0
istore 4
goto L10
L11:
iconst_0
istore 4
goto L12
L13:
iconst_0
istore 4
goto L14
L15:
iconst_0
istore 4
goto L16
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 6
.limit stack 6
.end method

.method public final b()Ljava/util/ArrayList;
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L3 to L4 using L2
.catch android/database/SQLException from L4 to L5 using L2
.catch android/database/SQLException from L6 to L7 using L2
.catch android/database/SQLException from L8 to L9 using L2
.catch android/database/SQLException from L10 to L11 using L2
.catch android/database/SQLException from L12 to L13 using L2
.catch android/database/SQLException from L14 to L15 using L2
.catch android/database/SQLException from L16 to L17 using L2
.catch android/database/SQLException from L18 to L19 using L2
.catch android/database/SQLException from L19 to L20 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/c/i Landroid/database/sqlite/SQLiteDatabase;
ldc "contacts"
bipush 13
anewarray java/lang/String
dup
iconst_0
ldc "contact_id"
aastore
dup
iconst_1
ldc "u_identifier"
aastore
dup
iconst_2
ldc "customname"
aastore
dup
iconst_3
ldc "display"
aastore
dup
iconst_4
ldc "status"
aastore
dup
iconst_5
ldc "mute"
aastore
dup
bipush 6
ldc "ignorepublicchat"
aastore
dup
bipush 7
ldc "ignoreprivatechat"
aastore
dup
bipush 8
ldc "ignorepokes"
aastore
dup
bipush 9
ldc "hideaway"
aastore
dup
bipush 10
ldc "hideavatar"
aastore
dup
bipush 11
ldc "whisperallow"
aastore
dup
bipush 12
ldc "volumemodifier"
aastore
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 3
aload 3
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifne L3
aload 3
invokeinterface android/database/Cursor/close()V 0
L1:
aconst_null
areturn
L3:
aload 3
invokeinterface android/database/Cursor/isAfterLast()Z 0
ifne L19
L4:
new com/teamspeak/ts3client/c/a
dup
invokespecial com/teamspeak/ts3client/c/a/<init>()V
astore 4
aload 4
aload 3
aload 3
ldc "contact_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/c/a/b I
aload 4
aload 3
aload 3
ldc "u_identifier"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokevirtual com/teamspeak/ts3client/c/a/a(Ljava/lang/String;)V
aload 4
aload 3
aload 3
ldc "customname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
aload 4
aload 3
aload 3
ldc "display"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/c/a/d I
aload 4
aload 3
aload 3
ldc "status"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/c/a/e I
aload 3
aload 3
ldc "mute"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L21
L5:
iconst_0
istore 1
L6:
aload 4
iload 1
putfield com/teamspeak/ts3client/c/a/f Z
aload 3
aload 3
ldc "ignorepublicchat"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L22
L7:
iconst_0
istore 1
L8:
aload 4
iload 1
putfield com/teamspeak/ts3client/c/a/g Z
aload 3
aload 3
ldc "ignoreprivatechat"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L23
L9:
iconst_0
istore 1
L10:
aload 4
iload 1
putfield com/teamspeak/ts3client/c/a/h Z
aload 3
aload 3
ldc "ignorepokes"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L24
L11:
iconst_0
istore 1
L12:
aload 4
iload 1
putfield com/teamspeak/ts3client/c/a/i Z
aload 3
aload 3
ldc "hideaway"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L25
L13:
iconst_0
istore 1
L14:
aload 4
iload 1
putfield com/teamspeak/ts3client/c/a/j Z
aload 3
aload 3
ldc "hideavatar"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L26
L15:
iconst_0
istore 1
L16:
aload 4
iload 1
putfield com/teamspeak/ts3client/c/a/k Z
aload 3
aload 3
ldc "whisperallow"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L27
L17:
iconst_0
istore 1
L18:
aload 4
iload 1
putfield com/teamspeak/ts3client/c/a/l Z
aload 4
aload 3
aload 3
ldc "volumemodifier"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getFloat(I)F 1
putfield com/teamspeak/ts3client/c/a/m F
aload 2
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 3
invokeinterface android/database/Cursor/moveToNext()Z 0
ifne L4
L19:
aload 3
invokeinterface android/database/Cursor/close()V 0
L20:
aload 2
areturn
L21:
iconst_1
istore 1
goto L6
L22:
iconst_1
istore 1
goto L8
L23:
iconst_1
istore 1
goto L10
L24:
iconst_1
istore 1
goto L12
L25:
iconst_1
istore 1
goto L14
L26:
iconst_1
istore 1
goto L16
L27:
iconst_1
istore 1
goto L18
L2:
astore 2
aload 2
invokevirtual android/database/SQLException/printStackTrace()V
aconst_null
areturn
.limit locals 5
.limit stack 8
.end method

.method public final c()V
aload 0
getfield com/teamspeak/ts3client/data/b/c/c Lcom/teamspeak/ts3client/c/b;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/data/b/c/c Lcom/teamspeak/ts3client/c/b;
invokevirtual com/teamspeak/ts3client/c/b/a()V
L0:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
invokevirtual com/teamspeak/ts3client/data/d/a()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L2:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/c
invokevirtual com/teamspeak/ts3client/data/c/a()V
goto L2
L1:
return
.limit locals 2
.limit stack 1
.end method
