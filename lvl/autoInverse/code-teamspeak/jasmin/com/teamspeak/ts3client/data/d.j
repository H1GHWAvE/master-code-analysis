.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d
.super java/lang/Object

.field private 'a' Ljava/util/SortedMap;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
putfield com/teamspeak/ts3client/data/d/a Ljava/util/SortedMap;
return
.limit locals 1
.limit stack 3
.end method

.method private b()I
aload 0
getfield com/teamspeak/ts3client/data/d/a Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Ljava/util/SortedMap;
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/a Ljava/util/SortedMap;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method public final a(Lcom/teamspeak/ts3client/data/c;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/a Ljava/util/SortedMap;
aload 1
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 1
invokeinterface java/util/SortedMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public final a(I)Z
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
istore 2
L1:
aload 0
monitorexit
iload 2
ireturn
L2:
astore 3
aload 0
monitorexit
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method public final b(I)Lcom/teamspeak/ts3client/data/c;
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/c
astore 2
L1:
aload 0
monitorexit
aload 2
areturn
L2:
astore 2
aload 0
monitorexit
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c(I)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/teamspeak/ts3client/data/d/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
new java/lang/StringBuilder
dup
ldc "cid_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/d/q/a(Ljava/lang/String;)V
L1:
aload 0
monitorexit
return
L2:
astore 2
aload 0
monitorexit
aload 2
athrow
.limit locals 3
.limit stack 3
.end method
