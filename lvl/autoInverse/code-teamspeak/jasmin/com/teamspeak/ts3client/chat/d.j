.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/chat/d
.super android/widget/BaseAdapter

.field private static 'e' Lcom/teamspeak/ts3client/chat/d;

.field private static 'f' Landroid/support/v4/app/bi;

.field public 'a' Ljava/util/ArrayList;

.field public 'b' Ljava/util/HashMap;

.field public 'c' Lcom/teamspeak/ts3client/chat/a;

.field public 'd' Lcom/teamspeak/ts3client/chat/a;

.field private 'g' Landroid/view/LayoutInflater;

.field private 'h' Landroid/content/Context;

.field private 'i' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'j' Ljava/text/DateFormat;

.method private <init>(Landroid/content/Context;)V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "UseSparseArrays" "UseSparseArrays" 
.end annotation
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield com/teamspeak/ts3client/chat/d/g Landroid/view/LayoutInflater;
aload 0
aload 1
putfield com/teamspeak/ts3client/chat/d/h Landroid/content/Context;
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/chat/d/i Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokestatic java/text/DateFormat/getDateTimeInstance()Ljava/text/DateFormat;
putfield com/teamspeak/ts3client/chat/d/j Ljava/text/DateFormat;
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/chat/d;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/chat/d/i Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a()Lcom/teamspeak/ts3client/chat/d;
getstatic com/teamspeak/ts3client/chat/d/e Lcom/teamspeak/ts3client/chat/d;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
getstatic com/teamspeak/ts3client/chat/d/e Lcom/teamspeak/ts3client/chat/d;
ifnonnull L0
new com/teamspeak/ts3client/chat/d
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/d/<init>(Landroid/content/Context;)V
putstatic com/teamspeak/ts3client/chat/d/e Lcom/teamspeak/ts3client/chat/d;
L0:
getstatic com/teamspeak/ts3client/chat/d/e Lcom/teamspeak/ts3client/chat/d;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/support/v4/app/bi;)V
aload 0
putstatic com/teamspeak/ts3client/chat/d/f Landroid/support/v4/app/bi;
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/String;)V
getstatic com/teamspeak/ts3client/chat/d/e Lcom/teamspeak/ts3client/chat/d;
ldc "CHANNEL"
invokevirtual com/teamspeak/ts3client/chat/d/d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;
new com/teamspeak/ts3client/chat/y
dup
aconst_null
ldc ""
aload 0
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/chat/y/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
return
.limit locals 1
.limit stack 8
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/chat/d;)Ljava/util/ArrayList;
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Lcom/teamspeak/ts3client/chat/a;)V
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
aload 0
getfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
aload 1
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/b Ljava/lang/String;
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual com/teamspeak/ts3client/chat/d/d()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public static b(Ljava/lang/String;)V
getstatic com/teamspeak/ts3client/chat/d/e Lcom/teamspeak/ts3client/chat/d;
ldc "SERVER"
invokevirtual com/teamspeak/ts3client/chat/d/d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;
new com/teamspeak/ts3client/chat/y
dup
aconst_null
ldc ""
aload 0
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/chat/y/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
invokevirtual com/teamspeak/ts3client/chat/a/a(Lcom/teamspeak/ts3client/chat/y;)V
return
.limit locals 1
.limit stack 8
.end method

.method static synthetic e()Landroid/support/v4/app/bi;
getstatic com/teamspeak/ts3client/chat/d/f Landroid/support/v4/app/bi;
areturn
.limit locals 0
.limit stack 1
.end method

.method private f()V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "UseSparseArrays" 
.end annotation
aload 0
aconst_null
putfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
aload 0
aconst_null
putfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
invokevirtual java/util/HashMap/clear()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
return
.limit locals 1
.limit stack 3
.end method

.method private g()V
aload 0
new com/teamspeak/ts3client/chat/a
dup
ldc "Server Tab"
ldc "SERVER"
aconst_null
invokespecial com/teamspeak/ts3client/chat/a/<init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V
putfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
iconst_1
putfield com/teamspeak/ts3client/chat/a/b Z
aload 0
new com/teamspeak/ts3client/chat/a
dup
ldc "Channel Tab"
ldc "CHANNEL"
aconst_null
invokespecial com/teamspeak/ts3client/chat/a/<init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V
putfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
iconst_1
putfield com/teamspeak/ts3client/chat/a/b Z
aload 0
aload 0
getfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
invokevirtual com/teamspeak/ts3client/chat/d/a(Lcom/teamspeak/ts3client/chat/a;)V
aload 0
aload 0
getfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
invokevirtual com/teamspeak/ts3client/chat/d/a(Lcom/teamspeak/ts3client/chat/a;)V
return
.limit locals 1
.limit stack 6
.end method

.method public final a(Lcom/teamspeak/ts3client/chat/a;)V
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
aload 1
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
aload 1
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final b()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/Boolean;
aload 0
getfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L0:
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;
aload 0
getfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/a
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public final d()V
aload 0
getfield com/teamspeak/ts3client/chat/d/i Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/chat/g
dup
aload 0
invokespecial com/teamspeak/ts3client/chat/g/<init>(Lcom/teamspeak/ts3client/chat/d;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method public final getCount()I
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getItem(I)Ljava/lang/Object;
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
lconst_0
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/chat/d/g Landroid/view/LayoutInflater;
ldc_w 2130903078
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
astore 2
aload 2
ldc_w 2131493101
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 3
aload 2
ldc_w 2131493100
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
astore 4
aload 2
ldc_w 2131493102
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 5
aload 2
ldc_w 2131493103
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 6
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/a
getfield com/teamspeak/ts3client/chat/a/b Z
ifeq L0
aload 4
iconst_4
invokevirtual android/widget/ImageView/setVisibility(I)V
L0:
aload 3
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/a
getfield com/teamspeak/ts3client/chat/a/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 5
aload 0
getfield com/teamspeak/ts3client/chat/d/j Ljava/text/DateFormat;
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/a
getfield com/teamspeak/ts3client/chat/a/h Ljava/util/Date;
invokevirtual java/text/DateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/a
getfield com/teamspeak/ts3client/chat/a/c Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
aload 0
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/chat/a
getfield com/teamspeak/ts3client/chat/a/d I
isub
ifle L1
aload 6
ldc "chat.newmessages"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L1:
aload 4
new com/teamspeak/ts3client/chat/e
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/chat/e/<init>(Lcom/teamspeak/ts3client/chat/d;I)V
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
ldc_w 2131493099
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/RelativeLayout
new com/teamspeak/ts3client/chat/f
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/chat/f/<init>(Lcom/teamspeak/ts3client/chat/d;I)V
invokevirtual android/widget/RelativeLayout/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
areturn
.limit locals 7
.limit stack 5
.end method
