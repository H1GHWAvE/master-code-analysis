.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/c/b
.super android/support/v4/app/Fragment

.field private 'a' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'b' Landroid/widget/ListView;

.field private 'c' Lcom/teamspeak/ts3client/c/c;

.field private 'd' Lcom/teamspeak/ts3client/data/b/c;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/c/b/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/c/b/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokevirtual com/teamspeak/ts3client/c/b/n()V
aload 0
getfield com/teamspeak/ts3client/c/b/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "menu.contact"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/c/b/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 1
ldc_w 2130903090
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 1
ldc_w 2131493236
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ListView
putfield com/teamspeak/ts3client/c/b/b Landroid/widget/ListView;
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a()V
aload 0
new com/teamspeak/ts3client/c/c
dup
aload 0
invokevirtual com/teamspeak/ts3client/c/b/i()Landroid/support/v4/app/bb;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/c/c/<init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V
putfield com/teamspeak/ts3client/c/b/c Lcom/teamspeak/ts3client/c/c;
aload 0
getfield com/teamspeak/ts3client/c/b/d Lcom/teamspeak/ts3client/data/b/c;
invokevirtual com/teamspeak/ts3client/data/b/c/b()Ljava/util/ArrayList;
astore 2
aload 2
ifnull L0
iconst_0
istore 1
L1:
iload 1
aload 2
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield com/teamspeak/ts3client/c/b/c Lcom/teamspeak/ts3client/c/c;
astore 3
aload 2
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/c/a
astore 4
aload 3
getfield com/teamspeak/ts3client/c/c/a Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L2
aload 3
getfield com/teamspeak/ts3client/c/c/a Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L2:
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
aload 0
getfield com/teamspeak/ts3client/c/b/b Landroid/widget/ListView;
aload 0
getfield com/teamspeak/ts3client/c/b/c Lcom/teamspeak/ts3client/c/c;
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
return
.limit locals 5
.limit stack 5
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/c/b/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/c/b/a Lcom/teamspeak/ts3client/Ts3Application;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 0
getstatic com/teamspeak/ts3client/data/b/c/a Lcom/teamspeak/ts3client/data/b/c;
putfield com/teamspeak/ts3client/c/b/d Lcom/teamspeak/ts3client/data/b/c;
aload 0
getfield com/teamspeak/ts3client/c/b/d Lcom/teamspeak/ts3client/data/b/c;
aload 0
putfield com/teamspeak/ts3client/data/b/c/c Lcom/teamspeak/ts3client/c/b;
aload 0
invokevirtual com/teamspeak/ts3client/c/b/a()V
return
.limit locals 2
.limit stack 2
.end method

.method public final g()V
aload 0
invokespecial android/support/v4/app/Fragment/g()V
aload 0
getfield com/teamspeak/ts3client/c/b/d Lcom/teamspeak/ts3client/data/b/c;
aconst_null
putfield com/teamspeak/ts3client/data/b/c/c Lcom/teamspeak/ts3client/c/b;
return
.limit locals 1
.limit stack 2
.end method
