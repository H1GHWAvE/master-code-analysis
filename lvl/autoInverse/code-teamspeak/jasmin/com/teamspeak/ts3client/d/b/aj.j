.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/b/aj
.super android/support/v4/app/ax

.field private 'aA' I

.field private 'at' Lcom/teamspeak/ts3client/data/c;

.field private 'au' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'av' Landroid/widget/TableLayout;

.field private 'aw' Landroid/widget/TableLayout;

.field private 'ax' Landroid/widget/Button;

.field private 'ay' J

.field private 'az' I

.method public <init>(Lcom/teamspeak/ts3client/data/c;)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/b/aj/at Lcom/teamspeak/ts3client/data/c;
return
.limit locals 2
.limit stack 2
.end method

.method private a(F)I
aload 0
getfield com/teamspeak/ts3client/d/b/aj/au Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fload 1
fmul
ldc_w 0.5F
fadd
f2i
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/d/b/aj/at Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/widget/TableLayout;Ljava/lang/String;JZZLcom/teamspeak/ts3client/d/b/am;Z)V
aload 0
invokevirtual com/teamspeak/ts3client/d/b/aj/i()Landroid/support/v4/app/bb;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
ldc_w 2130903083
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
checkcast android/widget/TableRow
astore 9
aload 9
ldc_w 2131493171
invokevirtual android/widget/TableRow/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
astore 10
aload 10
aload 2
invokevirtual android/widget/CheckBox/setText(Ljava/lang/CharSequence;)V
aload 10
iload 5
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 10
iload 6
invokevirtual android/widget/CheckBox/setEnabled(Z)V
aload 10
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual android/widget/CheckBox/setTag(Ljava/lang/Object;)V
aload 7
getfield com/teamspeak/ts3client/d/b/am/a Ljava/util/ArrayList;
aload 10
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 10
aload 7
invokevirtual android/widget/CheckBox/setOnClickListener(Landroid/view/View$OnClickListener;)V
iload 8
ifeq L0
aload 7
aload 10
putfield com/teamspeak/ts3client/d/b/am/b Landroid/widget/CheckBox;
L0:
aload 1
aload 9
invokevirtual android/widget/TableLayout/addView(Landroid/view/View;)V
return
.limit locals 11
.limit stack 3
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/d/b/aj/au Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/b/aj;)J
aload 0
getfield com/teamspeak/ts3client/d/b/aj/ay J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/b/aj;)I
aload 0
getfield com/teamspeak/ts3client/d/b/aj/aA I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
astore 11
aload 1
ldc_w 2130903082
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/FrameLayout
astore 2
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/d/b/aj/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/cg Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/b(I)I
putfield com/teamspeak/ts3client/d/b/aj/az I
aload 0
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/ci Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/b(I)I
putfield com/teamspeak/ts3client/d/b/aj/aA I
ldc "groupdialog.servertext"
aload 2
ldc_w 2131493165
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "groupdialog.channeltext"
aload 2
ldc_w 2131493167
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
aload 2
ldc_w 2131493166
invokevirtual android/widget/FrameLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TableLayout
putfield com/teamspeak/ts3client/d/b/aj/av Landroid/widget/TableLayout;
aload 0
aload 2
ldc_w 2131493168
invokevirtual android/widget/FrameLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TableLayout
putfield com/teamspeak/ts3client/d/b/aj/aw Landroid/widget/TableLayout;
aload 0
aload 2
ldc_w 2131493169
invokevirtual android/widget/FrameLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/aj/ax Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/d/b/aj/ax Landroid/widget/Button;
ldc "button.close"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/b/aj/ax Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/ak
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/ak/<init>(Lcom/teamspeak/ts3client/d/b/aj;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
new com/teamspeak/ts3client/d/b/am
dup
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/d/b/am/<init>(Lcom/teamspeak/ts3client/d/b/aj;Z)V
astore 1
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/z Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JLcom/teamspeak/ts3client/jni/i;)J
lstore 5
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 12
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/d/b/aj/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/I Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
astore 13
aload 13
ifnonnull L0
new android/widget/LinearLayout
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/aj/i()Landroid/support/v4/app/bb;
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 1
aload 1
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
new android/widget/TextView
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/aj/i()Landroid/support/v4/app/bb;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "The Client already left the Server"
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/aj/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "Close"
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/al
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/al/<init>(Lcom/teamspeak/ts3client/d/b/aj;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L1:
aload 1
areturn
L0:
aload 13
ldc ","
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 13
aload 13
arraylength
istore 4
iconst_0
istore 3
L2:
iload 3
iload 4
if_icmpge L3
aload 12
aload 13
iload 3
aaload
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/o Lcom/teamspeak/ts3client/data/g/b;
astore 13
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 14
aload 14
aload 13
getfield com/teamspeak/ts3client/data/g/b/a Ljava/util/HashMap;
invokevirtual java/util/HashMap/values()Ljava/util/Collection;
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
aload 14
invokestatic java/util/Collections/sort(Ljava/util/List;)V
aload 14
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 13
L4:
aload 13
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 13
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/g/a
astore 14
aload 14
getfield com/teamspeak/ts3client/data/g/a/c I
iconst_1
if_icmpne L4
iconst_0
istore 10
iconst_0
istore 8
iconst_0
istore 9
iload 10
istore 7
aload 14
getfield com/teamspeak/ts3client/data/g/a/e I
aload 0
getfield com/teamspeak/ts3client/d/b/aj/az I
if_icmpgt L6
iload 10
istore 7
aload 14
getfield com/teamspeak/ts3client/data/g/a/a J
lload 5
lcmp
ifeq L6
iconst_1
istore 7
L6:
aload 12
aload 14
getfield com/teamspeak/ts3client/data/g/a/a J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L7
iconst_1
istore 8
L7:
iload 8
ifeq L8
aload 14
getfield com/teamspeak/ts3client/data/g/a/f I
aload 0
getfield com/teamspeak/ts3client/d/b/aj/aA I
if_icmple L8
iconst_0
istore 7
L9:
aload 14
getfield com/teamspeak/ts3client/data/g/a/a J
lload 5
lcmp
ifne L10
iconst_1
istore 9
L10:
aload 0
aload 0
getfield com/teamspeak/ts3client/d/b/aj/av Landroid/widget/TableLayout;
aload 14
getfield com/teamspeak/ts3client/data/g/a/b Ljava/lang/String;
aload 14
getfield com/teamspeak/ts3client/data/g/a/a J
iload 8
iload 7
aload 1
iload 9
invokespecial com/teamspeak/ts3client/d/b/aj/a(Landroid/widget/TableLayout;Ljava/lang/String;JZZLcom/teamspeak/ts3client/d/b/am;Z)V
goto L4
L5:
new com/teamspeak/ts3client/d/b/am
dup
aload 0
iconst_1
invokespecial com/teamspeak/ts3client/d/b/am/<init>(Lcom/teamspeak/ts3client/d/b/aj;Z)V
astore 12
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/A Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JLcom/teamspeak/ts3client/jni/i;)J
lstore 5
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 13
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/d/b/aj/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/H Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
ldc ","
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 1
aload 1
arraylength
istore 4
iconst_0
istore 3
L11:
iload 3
iload 4
if_icmpge L12
aload 13
aload 1
iload 3
aaload
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 3
iconst_1
iadd
istore 3
goto L11
L12:
aload 11
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/p Lcom/teamspeak/ts3client/data/a/b;
astore 1
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 11
aload 11
aload 1
getfield com/teamspeak/ts3client/data/a/b/a Ljava/util/HashMap;
invokevirtual java/util/HashMap/values()Ljava/util/Collection;
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
aload 11
invokestatic java/util/Collections/sort(Ljava/util/List;)V
aload 11
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 11
L13:
aload 2
astore 1
aload 11
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 11
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/a/a
astore 1
aload 1
getfield com/teamspeak/ts3client/data/a/a/c I
iconst_1
if_icmpne L13
iconst_0
istore 7
iconst_0
istore 8
iconst_0
istore 9
aload 1
getfield com/teamspeak/ts3client/data/a/a/e I
aload 0
getfield com/teamspeak/ts3client/d/b/aj/az I
if_icmpgt L14
iconst_1
istore 7
L14:
aload 13
aload 1
getfield com/teamspeak/ts3client/data/a/a/a J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L15
iconst_1
istore 8
L15:
iload 8
ifeq L16
aload 1
getfield com/teamspeak/ts3client/data/a/a/f I
aload 0
getfield com/teamspeak/ts3client/d/b/aj/aA I
if_icmple L16
iconst_0
istore 7
L17:
aload 1
getfield com/teamspeak/ts3client/data/a/a/a J
lload 5
lcmp
ifne L18
iconst_1
istore 9
L18:
aload 0
aload 0
getfield com/teamspeak/ts3client/d/b/aj/aw Landroid/widget/TableLayout;
aload 1
getfield com/teamspeak/ts3client/data/a/a/b Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/data/a/a/a J
iload 8
iload 7
aload 12
iload 9
invokespecial com/teamspeak/ts3client/d/b/aj/a(Landroid/widget/TableLayout;Ljava/lang/String;JZZLcom/teamspeak/ts3client/d/b/am;Z)V
goto L13
L16:
goto L17
L8:
goto L9
.limit locals 15
.limit stack 9
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/ax/a(Landroid/os/Bundle;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/d/b/aj/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/d/b/aj/au Lcom/teamspeak/ts3client/Ts3Application;
aload 0
aload 0
getfield com/teamspeak/ts3client/d/b/aj/au Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/b/aj/au Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/d/b/aj/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/G Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/c(JILcom/teamspeak/ts3client/jni/d;)J
putfield com/teamspeak/ts3client/d/b/aj/ay J
return
.limit locals 2
.limit stack 6
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method
