.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/b/af
.super android/support/v4/app/ax

.field private 'at' Lcom/teamspeak/ts3client/data/c;

.field private 'au' Z

.field private 'av' Landroid/widget/EditText;

.field private 'aw' Landroid/widget/TextView;

.method public <init>(Lcom/teamspeak/ts3client/data/c;Z)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/b/af/at Lcom/teamspeak/ts3client/data/c;
aload 0
iload 2
putfield com/teamspeak/ts3client/d/b/af/au Z
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/b/af;)Z
aload 0
getfield com/teamspeak/ts3client/d/b/af/au Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/b/af;)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/d/b/af/at Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/b/af;)Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/b/af/av Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
astore 2
new android/widget/RelativeLayout
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
astore 3
aload 3
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/RelativeLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
new android/widget/TextView
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/d/b/af/aw Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/b/af/aw Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setId(I)V
aload 0
getfield com/teamspeak/ts3client/d/b/af/au Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/d/b/af/aw Landroid/widget/TextView;
ldc "clientdialog.kick.text.server"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/d/b/af/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L1:
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
bipush 10
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(I)V
aload 3
aload 0
getfield com/teamspeak/ts3client/d/b/af/aw Landroid/widget/TextView;
aload 4
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
new android/widget/EditText
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/d/b/af/av Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/b/af/av Landroid/widget/EditText;
iconst_2
invokevirtual android/widget/EditText/setId(I)V
new android/text/InputFilter$LengthFilter
dup
bipush 80
invokespecial android/text/InputFilter$LengthFilter/<init>(I)V
astore 4
aload 0
getfield com/teamspeak/ts3client/d/b/af/av Landroid/widget/EditText;
iconst_1
anewarray android/text/InputFilter
dup
iconst_0
aload 4
aastore
invokevirtual android/widget/EditText/setFilters([Landroid/text/InputFilter;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
iconst_3
aload 0
getfield com/teamspeak/ts3client/d/b/af/aw Landroid/widget/TextView;
invokevirtual android/widget/TextView/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 3
aload 0
getfield com/teamspeak/ts3client/d/b/af/av Landroid/widget/EditText;
aload 4
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
iconst_3
aload 0
getfield com/teamspeak/ts3client/d/b/af/av Landroid/widget/EditText;
invokevirtual android/widget/EditText/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
new android/widget/Button
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
ldc "clientdialog.kick.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
new com/teamspeak/ts3client/d/b/ag
dup
aload 0
aload 2
aload 3
invokespecial com/teamspeak/ts3client/d/b/ag/<init>(Lcom/teamspeak/ts3client/d/b/af;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 3
aload 1
aload 4
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/d/b/af/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 3
areturn
L0:
aload 0
getfield com/teamspeak/ts3client/d/b/af/aw Landroid/widget/TextView;
ldc "clientdialog.kick.text.channel"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/d/b/af/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L1
.limit locals 5
.limit stack 6
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method
