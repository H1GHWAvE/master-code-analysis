.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/d/y
.super android/os/AsyncTask

.field final synthetic 'a' Lcom/teamspeak/ts3client/d/t;

.method private <init>(Lcom/teamspeak/ts3client/d/t;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Lcom/teamspeak/ts3client/d/t;B)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/y/<init>(Lcom/teamspeak/ts3client/d/t;)V
return
.limit locals 3
.limit stack 2
.end method

.method private static transient a([Ljava/lang/String;)Ljava/lang/String;
.catch java/net/MalformedURLException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/net/MalformedURLException from L4 to L5 using L2
.catch java/io/IOException from L4 to L5 using L3
.catch java/net/MalformedURLException from L5 to L6 using L2
.catch java/io/IOException from L5 to L6 using L3
.catch java/net/MalformedURLException from L6 to L7 using L2
.catch java/io/IOException from L6 to L7 using L3
.catch java/net/MalformedURLException from L8 to L9 using L2
.catch java/io/IOException from L8 to L9 using L3
.catch java/net/MalformedURLException from L10 to L11 using L2
.catch java/io/IOException from L10 to L11 using L3
L0:
new java/net/URL
dup
aload 0
iconst_0
aaload
invokespecial java/net/URL/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
checkcast java/net/HttpURLConnection
astore 0
aload 0
invokevirtual java/net/HttpURLConnection/getHeaderFields()Ljava/util/Map;
astore 2
aload 1
invokevirtual java/net/URL/getHost()Ljava/lang/String;
aload 0
invokevirtual java/net/HttpURLConnection/getURL()Ljava/net/URL;
invokevirtual java/net/URL/getHost()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
L1:
aconst_null
areturn
L4:
aload 2
ldc "X-TS3-SIGNATURE"
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L12
aload 0
invokevirtual java/net/HttpURLConnection/getResponseCode()I
sipush 200
if_icmpeq L5
aload 0
invokevirtual java/net/HttpURLConnection/getResponseCode()I
sipush 304
if_icmpne L12
L5:
aload 0
sipush 1000
invokevirtual java/net/HttpURLConnection/setConnectTimeout(I)V
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 0
invokevirtual java/net/HttpURLConnection/getInputStream()Ljava/io/InputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 2
L6:
aload 1
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 3
L7:
aload 3
ifnull L10
L8:
aload 2
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L9:
goto L6
L10:
aload 0
invokevirtual java/net/HttpURLConnection/disconnect()V
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L11:
goto L13
L3:
astore 0
aconst_null
astore 0
goto L13
L12:
aconst_null
areturn
L2:
astore 0
aconst_null
astore 0
L13:
aload 0
areturn
.limit locals 4
.limit stack 5
.end method

.method private a(Ljava/lang/String;)V
aload 1
ifnull L0
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
aload 1
invokestatic com/teamspeak/ts3client/d/t/a(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "la_url"
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/d/t/b(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
L0:
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/d/t/e(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;
ldc "en"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/d/t/f(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;
iconst_0
anewarray java/lang/Object
invokeinterface com/teamspeak/ts3client/d/k/a([Ljava/lang/Object;)V 1
return
L1:
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
ldc "en"
invokestatic com/teamspeak/ts3client/d/t/b(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/d/t/g(Lcom/teamspeak/ts3client/d/t;)V
return
.limit locals 2
.limit stack 3
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast [Ljava/lang/String;
invokestatic com/teamspeak/ts3client/d/y/a([Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 1
checkcast java/lang/String
astore 1
aload 1
ifnull L0
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
aload 1
invokestatic com/teamspeak/ts3client/d/t/a(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "la_url"
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/d/t/b(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
L0:
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/d/t/e(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;
ldc "en"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/d/t/f(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;
iconst_0
anewarray java/lang/Object
invokeinterface com/teamspeak/ts3client/d/k/a([Ljava/lang/Object;)V 1
return
L1:
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
ldc "en"
invokestatic com/teamspeak/ts3client/d/t/b(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
getfield com/teamspeak/ts3client/d/y/a Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/d/t/g(Lcom/teamspeak/ts3client/d/t;)V
return
.limit locals 2
.limit stack 3
.end method
