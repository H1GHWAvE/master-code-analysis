.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/r
.super android/support/v4/app/ax

.field private 'at' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'au' Landroid/widget/Button;

.field private 'av' Lcom/teamspeak/ts3client/data/a;

.field private 'aw' Landroid/widget/EditText;

.field private 'ax' I

.field private 'ay' Landroid/widget/TextView;

.method public <init>(Landroid/content/Context;Lcom/teamspeak/ts3client/data/a;I)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/d/r/au Landroid/widget/Button;
aload 0
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/d/r/aw Landroid/widget/EditText;
aload 0
aload 2
putfield com/teamspeak/ts3client/d/r/av Lcom/teamspeak/ts3client/data/a;
aload 0
iload 3
putfield com/teamspeak/ts3client/d/r/ax I
return
.limit locals 4
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/r;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/d/r/av Lcom/teamspeak/ts3client/data/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/r;)Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/r/aw Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/r;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/d/r/at Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/r;)I
aload 0
getfield com/teamspeak/ts3client/d/r/ax I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/r;)V
aload 0
invokevirtual com/teamspeak/ts3client/d/r/b()V
return
.limit locals 1
.limit stack 1
.end method

.method private y()V
aload 0
invokevirtual com/teamspeak/ts3client/d/r/b()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
iconst_1
invokevirtual android/app/Dialog/requestWindowFeature(I)Z
pop
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/d/r/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/d/r/at Lcom/teamspeak/ts3client/Ts3Application;
new android/widget/RelativeLayout
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
astore 1
aload 1
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/RelativeLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
new android/widget/TextView
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/r/i()Landroid/support/v4/app/bb;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
bipush 10
invokevirtual android/widget/TextView/setId(I)V
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
aload 0
invokevirtual com/teamspeak/ts3client/d/r/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getResources()Landroid/content/res/Resources;
ldc_w 2130837577
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/TextView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
aload 0
invokevirtual com/teamspeak/ts3client/d/r/i()Landroid/support/v4/app/bb;
ldc_w 2131165363
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
ldc "passworddialog.text"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/d/r/av Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
bipush 15
bipush 15
bipush 15
bipush 15
invokevirtual android/widget/TextView/setPadding(IIII)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 2
aload 2
bipush 10
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(I)V
aload 1
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
aload 2
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 1
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/RelativeLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield com/teamspeak/ts3client/d/r/aw Landroid/widget/EditText;
iconst_2
invokevirtual android/widget/EditText/setId(I)V
aload 0
getfield com/teamspeak/ts3client/d/r/aw Landroid/widget/EditText;
ldc_w 524289
invokevirtual android/widget/EditText/setInputType(I)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 2
aload 2
iconst_3
aload 0
getfield com/teamspeak/ts3client/d/r/ay Landroid/widget/TextView;
invokevirtual android/widget/TextView/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 1
aload 0
getfield com/teamspeak/ts3client/d/r/aw Landroid/widget/EditText;
aload 2
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 2
aload 2
iconst_3
aload 0
getfield com/teamspeak/ts3client/d/r/aw Landroid/widget/EditText;
invokevirtual android/widget/EditText/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 0
getfield com/teamspeak/ts3client/d/r/au Landroid/widget/Button;
ldc "dialog.channel.join.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/r/au Landroid/widget/Button;
iconst_3
invokevirtual android/widget/Button/setId(I)V
aload 0
getfield com/teamspeak/ts3client/d/r/au Landroid/widget/Button;
new com/teamspeak/ts3client/d/s
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/s/<init>(Lcom/teamspeak/ts3client/d/r;Landroid/widget/RelativeLayout;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
aload 0
getfield com/teamspeak/ts3client/d/r/au Landroid/widget/Button;
aload 2
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 1
areturn
.limit locals 3
.limit stack 6
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method
