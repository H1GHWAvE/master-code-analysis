.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/c/a
.super com/teamspeak/ts3client/d/c/i

.method public <init>()V
aload 0
invokespecial com/teamspeak/ts3client/d/c/i/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a()V
return
.limit locals 1
.limit stack 0
.end method

.method public final a(Landroid/view/Menu;Lcom/teamspeak/ts3client/Ts3Application;)V
aload 1
ldc_w 2130837679
ldc "menu.settings"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
iconst_1
invokestatic com/teamspeak/ts3client/d/c/a/a(Landroid/view/Menu;ILjava/lang/String;II)V
aload 1
ldc_w 2130837631
ldc "menu.identity"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_2
iconst_1
invokestatic com/teamspeak/ts3client/d/c/a/a(Landroid/view/Menu;ILjava/lang/String;II)V
aload 1
ldc_w 2130837600
ldc "menu.contact"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_3
iconst_1
invokestatic com/teamspeak/ts3client/d/c/a/a(Landroid/view/Menu;ILjava/lang/String;II)V
aload 1
ldc_w 2130837604
ldc "menu.copyright"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_4
iconst_2
invokestatic com/teamspeak/ts3client/d/c/a/a(Landroid/view/Menu;ILjava/lang/String;II)V
aload 1
ldc_w 2130837601
ldc "menu.contactbug"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_5
iconst_2
invokestatic com/teamspeak/ts3client/d/c/a/a(Landroid/view/Menu;ILjava/lang/String;II)V
aload 1
ldc_w 2130837632
ldc "menu.changelog"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
bipush 6
iconst_1
invokestatic com/teamspeak/ts3client/d/c/a/a(Landroid/view/Menu;ILjava/lang/String;II)V
aload 1
ldc_w 2130837632
ldc "menu.licenseagreement"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
bipush 7
iconst_3
invokestatic com/teamspeak/ts3client/d/c/a/a(Landroid/view/Menu;ILjava/lang/String;II)V
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/view/MenuItem;Lcom/teamspeak/ts3client/Ts3Application;)Z
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 3
aload 1
invokeinterface android/view/MenuItem/getItemId()I 0
tableswitch 1
L0
L1
L2
L3
L4
L5
L6
default : L7
L7:
iconst_0
ireturn
L0:
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "Settings"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
ifnonnull L8
aload 3
new com/teamspeak/ts3client/f/as
dup
invokespecial com/teamspeak/ts3client/f/as/<init>()V
ldc "Settings"
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
L9:
aload 3
invokevirtual android/support/v4/app/cd/f()Landroid/support/v4/app/cd;
pop
aload 3
sipush 4097
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 3
invokevirtual android/support/v4/app/cd/i()I
pop
iconst_1
ireturn
L8:
aload 3
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "Settings"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
goto L9
L1:
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "IdentManager"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
ifnonnull L10
aload 3
new com/teamspeak/ts3client/e/b
dup
invokespecial com/teamspeak/ts3client/e/b/<init>()V
ldc "IdentManager"
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
L11:
aload 3
invokevirtual android/support/v4/app/cd/f()Landroid/support/v4/app/cd;
pop
aload 3
sipush 4097
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 3
invokevirtual android/support/v4/app/cd/i()I
pop
iconst_1
ireturn
L10:
aload 3
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "IdentManager"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
goto L11
L2:
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "Contact"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
ifnonnull L12
aload 3
new com/teamspeak/ts3client/c/b
dup
invokespecial com/teamspeak/ts3client/c/b/<init>()V
ldc "Contact"
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
L13:
aload 3
invokevirtual android/support/v4/app/cd/f()Landroid/support/v4/app/cd;
pop
aload 3
sipush 4097
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 3
invokevirtual android/support/v4/app/cd/i()I
pop
iconst_1
ireturn
L12:
aload 3
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "Contact"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
goto L13
L3:
new android/app/Dialog
dup
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
ldc_w 2131165312
invokespecial android/app/Dialog/<init>(Landroid/content/Context;I)V
astore 1
aload 1
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
new android/widget/LinearLayout
dup
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 3
aload 3
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 1
aload 3
invokevirtual android/app/Dialog/setContentView(Landroid/view/View;)V
new android/webkit/WebView
dup
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokespecial android/webkit/WebView/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "file:///android_asset/copyright/copyright.html"
invokevirtual android/webkit/WebView/loadUrl(Ljava/lang/String;)V
aload 3
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 1
ldc "menu.copyright.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
invokevirtual android/app/Dialog/show()V
iconst_1
ireturn
L4:
new android/app/AlertDialog$Builder
dup
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 1
ldc "contactbug.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
ldc "contactbug.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 1
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/d/c/b
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/c/b/<init>(Lcom/teamspeak/ts3client/d/c/a;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
iconst_m1
ldc "button.open"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/d/c/c
dup
aload 0
aload 2
aload 1
invokespecial com/teamspeak/ts3client/d/c/c/<init>(Lcom/teamspeak/ts3client/d/c/a;Lcom/teamspeak/ts3client/Ts3Application;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 1
invokevirtual android/app/AlertDialog/show()V
iconst_1
ireturn
L5:
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
getfield com/teamspeak/ts3client/StartGUIFragment/q Lcom/teamspeak/ts3client/b;
invokevirtual com/teamspeak/ts3client/b/show()V
iconst_1
ireturn
L6:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "la_url"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 1
invokevirtual java/lang/String/isEmpty()Z
ifeq L14
aload 2
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "LicenseAgreement"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
iconst_1
ireturn
L14:
new android/content/Intent
dup
ldc "android.intent.action.VIEW"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 3
aload 3
aload 1
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
invokevirtual android/content/Intent/setData(Landroid/net/Uri;)Landroid/content/Intent;
pop
aload 2
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
aload 3
invokevirtual com/teamspeak/ts3client/StartGUIFragment/startActivity(Landroid/content/Intent;)V
iconst_1
ireturn
.limit locals 4
.limit stack 8
.end method
