.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/customs/FloatingButton
.super android/view/View

.field 'a' Landroid/os/Handler;

.field 'b' I

.field public 'c' Z

.field private final 'd' Landroid/graphics/Paint;

.field private final 'e' Landroid/graphics/Paint;

.field private 'f' Landroid/graphics/Bitmap;

.field private 'g' Landroid/graphics/Paint;

.field private 'h' Ljava/lang/Runnable;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/view/View/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
new android/graphics/Paint
dup
iconst_1
invokespecial android/graphics/Paint/<init>(I)V
putfield com/teamspeak/ts3client/customs/FloatingButton/d Landroid/graphics/Paint;
aload 0
new android/graphics/Paint
dup
iconst_1
invokespecial android/graphics/Paint/<init>(I)V
putfield com/teamspeak/ts3client/customs/FloatingButton/e Landroid/graphics/Paint;
aload 0
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
putfield com/teamspeak/ts3client/customs/FloatingButton/a Landroid/os/Handler;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/FloatingButton/b I
aload 0
new android/graphics/Paint
dup
iconst_1
invokespecial android/graphics/Paint/<init>(I)V
putfield com/teamspeak/ts3client/customs/FloatingButton/g Landroid/graphics/Paint;
aload 0
new com/teamspeak/ts3client/customs/i
dup
aload 0
invokespecial com/teamspeak/ts3client/customs/i/<init>(Lcom/teamspeak/ts3client/customs/FloatingButton;)V
putfield com/teamspeak/ts3client/customs/FloatingButton/h Ljava/lang/Runnable;
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial com/teamspeak/ts3client/customs/FloatingButton/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation
aload 0
aload 1
aload 2
iload 3
invokespecial android/view/View/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/graphics/Paint
dup
iconst_1
invokespecial android/graphics/Paint/<init>(I)V
putfield com/teamspeak/ts3client/customs/FloatingButton/d Landroid/graphics/Paint;
aload 0
new android/graphics/Paint
dup
iconst_1
invokespecial android/graphics/Paint/<init>(I)V
putfield com/teamspeak/ts3client/customs/FloatingButton/e Landroid/graphics/Paint;
aload 0
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
putfield com/teamspeak/ts3client/customs/FloatingButton/a Landroid/os/Handler;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/FloatingButton/b I
aload 0
new android/graphics/Paint
dup
iconst_1
invokespecial android/graphics/Paint/<init>(I)V
putfield com/teamspeak/ts3client/customs/FloatingButton/g Landroid/graphics/Paint;
aload 0
new com/teamspeak/ts3client/customs/i
dup
aload 0
invokespecial com/teamspeak/ts3client/customs/i/<init>(Lcom/teamspeak/ts3client/customs/FloatingButton;)V
putfield com/teamspeak/ts3client/customs/FloatingButton/h Ljava/lang/Runnable;
aload 0
new android/view/ViewGroup$LayoutParams
dup
bipush 15
bipush 15
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_1
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setClickable(Z)V
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/d Landroid/graphics/Paint;
getstatic android/graphics/Paint$Style/FILL Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/d Landroid/graphics/Paint;
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getResources()Landroid/content/res/Resources;
ldc_w 2131427532
invokevirtual android/content/res/Resources/getColor(I)I
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/d Landroid/graphics/Paint;
ldc_w 10.0F
fconst_0
ldc_w 3.5F
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getResources()Landroid/content/res/Resources;
ldc_w 2131427482
invokevirtual android/content/res/Resources/getColor(I)I
invokevirtual android/graphics/Paint/setShadowLayer(FFFI)V
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/a(Z)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 0
iconst_1
aconst_null
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setLayerType(ILandroid/graphics/Paint;)V
L0:
return
.limit locals 4
.limit stack 6
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/customs/FloatingButton;)Landroid/graphics/Paint;
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/g Landroid/graphics/Paint;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/c Z
ifeq L0
return
L0:
aload 0
iconst_1
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/a(Z)V
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/invalidate()V
return
.limit locals 1
.limit stack 2
.end method

.method private b()V
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/a(Z)V
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/invalidate()V
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/customs/FloatingButton/c Z
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/g Landroid/graphics/Paint;
getstatic android/graphics/Paint$Style/FILL Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/g Landroid/graphics/Paint;
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getResources()Landroid/content/res/Resources;
ldc_w 2131427532
invokevirtual android/content/res/Resources/getColor(I)I
invokevirtual android/graphics/Paint/setColor(I)V
iload 1
ifne L0
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/g Landroid/graphics/Paint;
ldc_w 5.0F
fconst_0
ldc_w -0.1F
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getResources()Landroid/content/res/Resources;
ldc_w 2131427534
invokevirtual android/content/res/Resources/getColor(I)I
invokevirtual android/graphics/Paint/setShadowLayer(FFFI)V
return
L0:
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/g Landroid/graphics/Paint;
ldc_w 5.0F
fconst_0
ldc_w -0.1F
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getResources()Landroid/content/res/Resources;
ldc_w 2131427535
invokevirtual android/content/res/Resources/getColor(I)I
invokevirtual android/graphics/Paint/setShadowLayer(FFFI)V
return
.limit locals 2
.limit stack 6
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
aload 1
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getWidth()I
iconst_2
idiv
i2f
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getHeight()I
iconst_2
idiv
i2f
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getWidth()I
i2d
ldc2_w 2.6D
ddiv
d2f
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawCircle(FFFLandroid/graphics/Paint;)V
aload 1
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getWidth()I
iconst_2
idiv
i2f
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getHeight()I
iconst_2
idiv
i2f
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getWidth()I
i2d
ldc2_w 2.6D
ddiv
d2f
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/d Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawCircle(FFFLandroid/graphics/Paint;)V
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/f Landroid/graphics/Bitmap;
ifnull L0
aload 1
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/f Landroid/graphics/Bitmap;
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getWidth()I
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/f Landroid/graphics/Bitmap;
invokevirtual android/graphics/Bitmap/getWidth()I
isub
iconst_2
idiv
i2f
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getHeight()I
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/f Landroid/graphics/Bitmap;
invokevirtual android/graphics/Bitmap/getHeight()I
isub
iconst_2
idiv
i2f
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/e Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
L0:
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/c Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/a Landroid/os/Handler;
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/h Ljava/lang/Runnable;
ldc2_w 40L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
L1:
return
.limit locals 2
.limit stack 7
.end method

.method protected onMeasure(II)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getSuggestedMinimumWidth()I
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
invokestatic java/lang/Math/max(II)I
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getSuggestedMinimumHeight()I
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
invokestatic java/lang/Math/max(II)I
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setMeasuredDimension(II)V
return
.limit locals 3
.limit stack 4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokevirtual android/view/MotionEvent/getAction()I
iconst_1
if_icmpne L0
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getResources()Landroid/content/res/Resources;
ldc_w 2131427532
invokevirtual android/content/res/Resources/getColor(I)I
istore 2
L1:
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/d Landroid/graphics/Paint;
iload 2
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/invalidate()V
aload 0
aload 1
invokespecial android/view/View/onTouchEvent(Landroid/view/MotionEvent;)Z
ireturn
L0:
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/getResources()Landroid/content/res/Resources;
ldc_w 2131427533
invokevirtual android/content/res/Resources/getColor(I)I
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/customs/FloatingButton/f Landroid/graphics/Bitmap;
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
checkcast android/graphics/drawable/BitmapDrawable
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
putfield com/teamspeak/ts3client/customs/FloatingButton/f Landroid/graphics/Bitmap;
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/invalidate()V
return
.limit locals 2
.limit stack 2
.end method
