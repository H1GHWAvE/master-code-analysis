.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/customs/a
.super android/widget/ArrayAdapter
.implements android/widget/SpinnerAdapter

.field private 'a' Ljava/util/Vector;

.field private 'b' Landroid/content/Context;

.field private 'c' Landroid/view/LayoutInflater;

.field private 'd' I

.field private 'e' I

.method private <init>(Landroid/content/Context;I)V
aload 0
aload 1
iload 2
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;I)V
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/customs/a/a Ljava/util/Vector;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/a/d I
aload 0
aload 1
putfield com/teamspeak/ts3client/customs/a/b Landroid/content/Context;
aload 0
aload 1
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
putfield com/teamspeak/ts3client/customs/a/c Landroid/view/LayoutInflater;
aload 0
iload 2
putfield com/teamspeak/ts3client/customs/a/e I
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;ILjava/util/List;)V
aload 0
aload 1
iload 2
aload 3
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;ILjava/util/List;)V
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/customs/a/a Ljava/util/Vector;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/a/d I
aload 0
aload 1
putfield com/teamspeak/ts3client/customs/a/b Landroid/content/Context;
aload 0
aload 1
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
putfield com/teamspeak/ts3client/customs/a/c Landroid/view/LayoutInflater;
aload 0
iload 2
putfield com/teamspeak/ts3client/customs/a/e I
return
.limit locals 4
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;[Ljava/lang/String;)V
aload 0
aload 1
ldc_w 17367049
aload 2
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;I[Ljava/lang/Object;)V
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/customs/a/a Ljava/util/Vector;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/a/d I
aload 0
aload 1
putfield com/teamspeak/ts3client/customs/a/b Landroid/content/Context;
aload 0
aload 1
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
putfield com/teamspeak/ts3client/customs/a/c Landroid/view/LayoutInflater;
aload 0
ldc_w 17367049
putfield com/teamspeak/ts3client/customs/a/e I
return
.limit locals 3
.limit stack 4
.end method

.method private a(ILandroid/view/ViewGroup;I)Landroid/view/View;
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/lang/ClassCastException from L3 to L4 using L2
aload 0
getfield com/teamspeak/ts3client/customs/a/c Landroid/view/LayoutInflater;
iload 3
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 4
L0:
aload 0
getfield com/teamspeak/ts3client/customs/a/d I
ifne L3
aload 4
checkcast android/widget/TextView
astore 2
L1:
aload 0
iload 1
invokevirtual com/teamspeak/ts3client/customs/a/getItem(I)Ljava/lang/Object;
checkcast java/lang/String
astore 5
aload 5
instanceof java/lang/CharSequence
ifeq L5
aload 2
aload 5
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L6:
aload 0
iload 1
invokevirtual com/teamspeak/ts3client/customs/a/isEnabled(I)Z
ifne L7
aload 2
iconst_0
invokevirtual android/widget/TextView/setEnabled(Z)V
aload 2
ldc_w -12303292
invokevirtual android/widget/TextView/setBackgroundColor(I)V
L7:
aload 4
areturn
L3:
aload 4
aload 0
getfield com/teamspeak/ts3client/customs/a/d I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 2
L4:
goto L1
L2:
astore 2
ldc "ArrayAdapter"
ldc "You must supply a resource ID for a TextView"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/IllegalStateException
dup
ldc "ArrayAdapter requires the resource ID to be a TextView"
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L5:
aload 2
aload 5
invokevirtual java/lang/String/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L6
.limit locals 6
.limit stack 4
.end method

.method private b(I)V
aload 0
getfield com/teamspeak/ts3client/customs/a/a Ljava/util/Vector;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/Vector/removeElement(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/a/a Ljava/util/Vector;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/Vector/addElement(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final areAllItemsEnabled()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
iload 1
aload 3
aload 0
getfield com/teamspeak/ts3client/customs/a/e I
invokespecial com/teamspeak/ts3client/customs/a/a(ILandroid/view/ViewGroup;I)Landroid/view/View;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final isEnabled(I)Z
aload 0
getfield com/teamspeak/ts3client/customs/a/a Ljava/util/Vector;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/Vector/contains(Ljava/lang/Object;)Z
ifeq L0
iconst_0
ireturn
L0:
aload 0
iload 1
invokespecial android/widget/ArrayAdapter/isEnabled(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method
