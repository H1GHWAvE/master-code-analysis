.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/ci
.super java/lang/Object
.implements android/view/animation/Animation$AnimationListener

.field final synthetic 'a' Landroid/view/animation/Animation;

.field final synthetic 'b' Lcom/teamspeak/ts3client/StartGUIFragment;

.method <init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/animation/Animation;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/ci/b Lcom/teamspeak/ts3client/StartGUIFragment;
aload 0
aload 2
putfield com/teamspeak/ts3client/ci/a Landroid/view/animation/Animation;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 3
.limit stack 2
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
aload 0
getfield com/teamspeak/ts3client/ci/b Lcom/teamspeak/ts3client/StartGUIFragment;
new com/teamspeak/ts3client/cj
dup
aload 0
invokespecial com/teamspeak/ts3client/cj/<init>(Lcom/teamspeak/ts3client/ci;)V
invokevirtual com/teamspeak/ts3client/StartGUIFragment/runOnUiThread(Ljava/lang/Runnable;)V
aload 0
getfield com/teamspeak/ts3client/ci/b Lcom/teamspeak/ts3client/StartGUIFragment;
invokestatic com/teamspeak/ts3client/StartGUIFragment/h(Lcom/teamspeak/ts3client/StartGUIFragment;)V
aload 0
getfield com/teamspeak/ts3client/ci/b Lcom/teamspeak/ts3client/StartGUIFragment;
astore 1
aload 1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getIntent()Landroid/content/Intent;
ldc "UrlStart"
invokevirtual android/content/Intent/hasExtra(Ljava/lang/String;)Z
ifeq L0
aload 1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "uri"
invokevirtual android/os/Bundle/get(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/Uri
astore 2
new android/app/AlertDialog$Builder
dup
aload 1
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 3
aload 3
ldc "gui.extern.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 3
ldc "gui.extern.text"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "address"
invokevirtual android/os/Bundle/get(Ljava/lang/String;)Ljava/lang/Object;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 3
iconst_m1
ldc "gui.extern.button1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bv
dup
aload 1
aload 2
aload 3
invokespecial com/teamspeak/ts3client/bv/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
bipush -3
ldc "gui.extern.button2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bw
dup
aload 1
aload 2
aload 3
invokespecial com/teamspeak/ts3client/bw/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bx
dup
aload 1
aload 3
invokespecial com/teamspeak/ts3client/bx/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 3
invokevirtual android/app/AlertDialog/show()V
L0:
return
.limit locals 4
.limit stack 8
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
return
.limit locals 2
.limit stack 0
.end method
