.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' I

.field public 'b' Ljava/lang/String;

.field private 'c' J

.field private 'd' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/c J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/a I
aload 0
aload 4
putfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b Ljava/lang/String;
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/d Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 6
.limit stack 3
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final b()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ClientUIDfromClientID [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", clientID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", uniqueClientIdentifier="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", clientNickName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
