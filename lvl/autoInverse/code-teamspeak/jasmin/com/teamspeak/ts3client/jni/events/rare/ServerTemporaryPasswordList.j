.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' J

.field public 'b' Ljava/lang/String;

.field public 'c' Ljava/lang/String;

.field public 'd' Ljava/lang/String;

.field public 'e' Ljava/lang/String;

.field public 'f' J

.field public 'g' J

.field public 'h' J

.field public 'i' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/a J
aload 0
aload 3
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/b Ljava/lang/String;
aload 0
aload 4
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/c Ljava/lang/String;
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/d Ljava/lang/String;
aload 0
aload 6
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/e Ljava/lang/String;
aload 0
lload 7
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/f J
aload 0
lload 9
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/g J
aload 0
lload 11
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/h J
aload 0
aload 13
putfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/i Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 14
.limit stack 3
.end method

.method private a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private e()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/h J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private f()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/i Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/g J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private h()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/f J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private i()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ServerTemporaryPasswordList [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", clientNickname="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", uniqueClientIdentifier="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", description="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", password="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", timestampStart="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/f J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", timestampEnd="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/g J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", targetChannelID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/h J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", targetChannelPW="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/i Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
