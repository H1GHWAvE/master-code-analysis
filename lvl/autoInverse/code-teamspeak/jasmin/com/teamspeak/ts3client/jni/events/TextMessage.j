.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/TextMessage
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' I

.field public 'b' I

.field public 'c' I

.field public 'd' Ljava/lang/String;

.field public 'e' Ljava/lang/String;

.field public 'f' Ljava/lang/String;

.field private 'g' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/TextMessage/g J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/TextMessage/a I
aload 0
iload 4
putfield com/teamspeak/ts3client/jni/events/TextMessage/b I
aload 0
iload 5
putfield com/teamspeak/ts3client/jni/events/TextMessage/c I
aload 0
aload 6
putfield com/teamspeak/ts3client/jni/events/TextMessage/d Ljava/lang/String;
aload 0
aload 7
putfield com/teamspeak/ts3client/jni/events/TextMessage/e Ljava/lang/String;
aload 0
aload 8
putfield com/teamspeak/ts3client/jni/events/TextMessage/f Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 9
.limit stack 3
.end method

.method private f()J
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/g J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private g()I
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()I
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "TextMessage [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/g J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", targetMode="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", toID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", fromID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", fromName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", fromUniqueIdentifier="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", message="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TextMessage/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
