.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' Ljava/lang/String;

.field private 'c' J

.field private 'd' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JLjava/lang/String;JJ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/a J
aload 0
aload 3
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/b Ljava/lang/String;
aload 0
lload 4
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/c J
aload 0
lload 6
putfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/d J
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 8
.limit stack 3
.end method

.method private a()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ServerGroupByClientID [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", name="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", serverGroupID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", clientDatabaseID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
