.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/ServerStop
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JLjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/ServerStop/a J
aload 0
aload 3
putfield com/teamspeak/ts3client/jni/events/ServerStop/b Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 4
.limit stack 3
.end method

.method private b()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerStop/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerStop/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ServerStop [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerStop/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", shutdownMessage="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerStop/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
