.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/ChannelMove
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' J

.field private 'c' J

.field private 'd' I

.field private 'e' Ljava/lang/String;

.field private 'f' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JJJILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/ChannelMove/a J
aload 0
lload 3
putfield com/teamspeak/ts3client/jni/events/ChannelMove/b J
aload 0
lload 5
putfield com/teamspeak/ts3client/jni/events/ChannelMove/c J
aload 0
iload 7
putfield com/teamspeak/ts3client/jni/events/ChannelMove/d I
aload 0
aload 8
putfield com/teamspeak/ts3client/jni/events/ChannelMove/e Ljava/lang/String;
aload 0
aload 9
putfield com/teamspeak/ts3client/jni/events/ChannelMove/f Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 10
.limit stack 3
.end method

.method private e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final b()I
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ChannelMove [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", channelID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", newChannelParentID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", invokerID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/d I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", invokerName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", invokerUniqueIdentifier="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ChannelMove/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
