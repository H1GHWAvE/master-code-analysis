.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/f/c
.super android/widget/RelativeLayout
.implements android/view/View$OnClickListener

.field private 'a' Landroid/widget/CheckBox;

.field private 'b' Ljava/lang/String;

.field private 'c' Ljava/lang/String;

.field private 'd' Z

.field private 'e' Landroid/view/View;

.method public <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 1
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
aload 0
ldc ""
putfield com/teamspeak/ts3client/f/c/c Ljava/lang/String;
aload 0
iconst_0
putfield com/teamspeak/ts3client/f/c/d Z
aload 0
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
invokevirtual com/teamspeak/ts3client/f/c/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
bipush 18
iconst_0
bipush 18
invokevirtual com/teamspeak/ts3client/f/c/setPadding(IIII)V
aload 0
ldc_w 2130837686
invokevirtual com/teamspeak/ts3client/f/c/setBackgroundResource(I)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 7
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 8
aload 0
aload 1
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
ldc_w 2130903109
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
putfield com/teamspeak/ts3client/f/c/e Landroid/view/View;
aload 0
aload 0
getfield com/teamspeak/ts3client/f/c/e Landroid/view/View;
ldc_w 2131493263
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/teamspeak/ts3client/f/c/a Landroid/widget/CheckBox;
aload 0
aload 2
putfield com/teamspeak/ts3client/f/c/b Ljava/lang/String;
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
astore 1
aload 1
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 2
aload 3
invokevirtual java/lang/Boolean/booleanValue()Z
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 6
aload 7
aload 4
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 7
ldc_w 20.0F
invokevirtual android/widget/TextView/setTextSize(F)V
aload 7
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 7
iconst_1
invokevirtual android/widget/TextView/setId(I)V
aload 8
aload 5
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 8
ldc_w 10.0F
invokevirtual android/widget/TextView/setTextSize(F)V
aload 8
aconst_null
iconst_2
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 8
iconst_2
invokevirtual android/widget/TextView/setId(I)V
aload 0
getfield com/teamspeak/ts3client/f/c/a Landroid/widget/CheckBox;
iload 6
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/f/c/a Landroid/widget/CheckBox;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " Checkbox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/CheckBox/setContentDescription(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/f/c/a Landroid/widget/CheckBox;
new com/teamspeak/ts3client/f/d
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/f/d/<init>(Lcom/teamspeak/ts3client/f/c;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/CheckBox/setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 1
aload 1
bipush 10
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(I)V
aload 0
aload 7
aload 1
invokevirtual com/teamspeak/ts3client/f/c/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 1
aload 1
iconst_3
aload 7
invokevirtual android/widget/TextView/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 0
aload 8
aload 1
invokevirtual com/teamspeak/ts3client/f/c/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 1
aload 1
bipush 11
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(I)V
aload 0
aload 0
getfield com/teamspeak/ts3client/f/c/e Landroid/view/View;
aload 1
invokevirtual com/teamspeak/ts3client/f/c/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_1
invokevirtual com/teamspeak/ts3client/f/c/setClickable(Z)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/f/c/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 9
.limit stack 5
.end method

.method public <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 1
aload 2
aload 3
aload 4
aload 5
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 6
putfield com/teamspeak/ts3client/f/c/c Ljava/lang/String;
aload 0
iconst_1
putfield com/teamspeak/ts3client/f/c/d Z
return
.limit locals 7
.limit stack 6
.end method

.method public <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
aload 0
aload 1
aload 2
aload 3
aload 4
aload 5
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 6
putfield com/teamspeak/ts3client/f/c/c Ljava/lang/String;
return
.limit locals 8
.limit stack 6
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/f/c/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/f/c/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/f/c;)Z
aload 0
getfield com/teamspeak/ts3client/f/c/d Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final onClick(Landroid/view/View;)V
aload 0
getfield com/teamspeak/ts3client/f/c/a Landroid/widget/CheckBox;
astore 1
aload 0
getfield com/teamspeak/ts3client/f/c/a Landroid/widget/CheckBox;
invokevirtual android/widget/CheckBox/isChecked()Z
ifne L0
iconst_1
istore 2
L1:
aload 1
iload 2
invokevirtual android/widget/CheckBox/setChecked(Z)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method
