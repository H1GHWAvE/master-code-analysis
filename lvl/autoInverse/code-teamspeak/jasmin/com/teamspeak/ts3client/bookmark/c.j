.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/bookmark/c
.super android/widget/BaseAdapter

.field 'a' Ljava/util/ArrayList;

.field private 'b' Landroid/content/Context;

.field private 'c' Landroid/view/LayoutInflater;

.field private 'd' Landroid/widget/ImageView;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/bookmark/c/b Landroid/content/Context;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield com/teamspeak/ts3client/bookmark/c/c Landroid/view/LayoutInflater;
return
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/teamspeak/ts3client/data/ab;)V
aload 0
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private b(Lcom/teamspeak/ts3client/data/ab;)V
aload 0
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final getCount()I
aload 0
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getItem(I)Ljava/lang/Object;
aload 0
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
lconst_0
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 2
ifnonnull L0
aload 0
getfield com/teamspeak/ts3client/bookmark/c/c Landroid/view/LayoutInflater;
ldc_w 2130903071
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
astore 4
aload 0
aload 4
ldc_w 2131493023
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield com/teamspeak/ts3client/bookmark/c/d Landroid/widget/ImageView;
aload 0
getfield com/teamspeak/ts3client/bookmark/c/d Landroid/widget/ImageView;
ldc_w 2130837579
ldc_w 32.0F
ldc_w 32.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 4
ldc_w 2131493024
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 2
aload 4
aload 2
invokevirtual android/view/View/setTag(Ljava/lang/Object;)V
aload 2
astore 3
L1:
aload 3
aload 0
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/ab
getfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 4
areturn
L0:
aload 2
invokevirtual android/view/View/getTag()Ljava/lang/Object;
checkcast android/widget/TextView
astore 3
aload 2
astore 4
goto L1
.limit locals 5
.limit stack 4
.end method
