.bytecode 50.0
.class public abstract interface annotation javax/annotation/CheckReturnValue
.super java/lang/Object
.implements java/lang/annotation/Annotation
.annotation visible Ljava/lang/annotation/Documented;
.end annotation
.annotation visible Ljava/lang/annotation/Retention;
value e Ljava/lang/annotation/RetentionPolicy; = "RUNTIME"
.end annotation
.annotation visible Ljava/lang/annotation/Target;
value [e Ljava/lang/annotation/ElementType; = "METHOD" "CONSTRUCTOR" "TYPE" "PACKAGE" 
.end annotation

.method public abstract when()Ljavax/annotation/meta/When;
.annotation default
e Ljavax/annotation/meta/When; = "ALWAYS"
.end annotation
.end method
