.bytecode 50.0
.class public abstract interface annotation javax/annotation/CheckForNull
.super java/lang/Object
.implements java/lang/annotation/Annotation
.annotation visible Ljava/lang/annotation/Documented;
.end annotation
.annotation visible Ljava/lang/annotation/Retention;
value e Ljava/lang/annotation/RetentionPolicy; = "RUNTIME"
.end annotation
.annotation visible Ljavax/annotation/Nonnull;
when e Ljavax/annotation/meta/When; = "MAYBE"
.end annotation
.annotation invisible Ljavax/annotation/meta/TypeQualifierNickname;
.end annotation
