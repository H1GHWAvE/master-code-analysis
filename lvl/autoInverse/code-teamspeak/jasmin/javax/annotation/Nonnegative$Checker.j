.bytecode 50.0
.class public final synchronized javax/annotation/Nonnegative$Checker
.super java/lang/Object
.implements javax/annotation/meta/TypeQualifierValidator

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static forConstantValue$4ec69d5d(Ljava/lang/Object;)Ljavax/annotation/meta/When;
iconst_1
istore 1
aload 0
instanceof java/lang/Number
ifne L0
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
areturn
L0:
aload 0
checkcast java/lang/Number
astore 0
aload 0
instanceof java/lang/Long
ifeq L1
aload 0
invokevirtual java/lang/Number/longValue()J
lconst_0
lcmp
ifge L2
L3:
iload 1
ifeq L4
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
areturn
L2:
iconst_0
istore 1
goto L3
L1:
aload 0
instanceof java/lang/Double
ifeq L5
aload 0
invokevirtual java/lang/Number/doubleValue()D
dconst_0
dcmpg
iflt L3
iconst_0
istore 1
goto L3
L5:
aload 0
instanceof java/lang/Float
ifeq L6
aload 0
invokevirtual java/lang/Number/floatValue()F
fconst_0
fcmpg
iflt L3
iconst_0
istore 1
goto L3
L6:
aload 0
invokevirtual java/lang/Number/intValue()I
iflt L3
iconst_0
istore 1
goto L3
L4:
getstatic javax/annotation/meta/When/ALWAYS Ljavax/annotation/meta/When;
areturn
.limit locals 2
.limit stack 4
.end method

.method public final synthetic forConstantValue(Ljava/lang/annotation/Annotation;Ljava/lang/Object;)Ljavax/annotation/meta/When;
iconst_1
istore 3
aload 2
instanceof java/lang/Number
ifne L0
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
areturn
L0:
aload 2
checkcast java/lang/Number
astore 1
aload 1
instanceof java/lang/Long
ifeq L1
aload 1
invokevirtual java/lang/Number/longValue()J
lconst_0
lcmp
ifge L2
L3:
iload 3
ifeq L4
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
areturn
L2:
iconst_0
istore 3
goto L3
L1:
aload 1
instanceof java/lang/Double
ifeq L5
aload 1
invokevirtual java/lang/Number/doubleValue()D
dconst_0
dcmpg
iflt L3
iconst_0
istore 3
goto L3
L5:
aload 1
instanceof java/lang/Float
ifeq L6
aload 1
invokevirtual java/lang/Number/floatValue()F
fconst_0
fcmpg
iflt L3
iconst_0
istore 3
goto L3
L6:
aload 1
invokevirtual java/lang/Number/intValue()I
iflt L3
iconst_0
istore 3
goto L3
L4:
getstatic javax/annotation/meta/When/ALWAYS Ljavax/annotation/meta/When;
areturn
.limit locals 4
.limit stack 4
.end method
