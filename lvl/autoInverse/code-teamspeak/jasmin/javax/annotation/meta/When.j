.bytecode 50.0
.class public final synchronized enum javax/annotation/meta/When
.super java/lang/Enum

.field private static final synthetic '$VALUES' [Ljavax/annotation/meta/When;

.field public static final enum 'ALWAYS' Ljavax/annotation/meta/When;

.field public static final enum 'MAYBE' Ljavax/annotation/meta/When;

.field public static final enum 'NEVER' Ljavax/annotation/meta/When;

.field public static final enum 'UNKNOWN' Ljavax/annotation/meta/When;

.method static <clinit>()V
new javax/annotation/meta/When
dup
ldc "ALWAYS"
iconst_0
invokespecial javax/annotation/meta/When/<init>(Ljava/lang/String;I)V
putstatic javax/annotation/meta/When/ALWAYS Ljavax/annotation/meta/When;
new javax/annotation/meta/When
dup
ldc "UNKNOWN"
iconst_1
invokespecial javax/annotation/meta/When/<init>(Ljava/lang/String;I)V
putstatic javax/annotation/meta/When/UNKNOWN Ljavax/annotation/meta/When;
new javax/annotation/meta/When
dup
ldc "MAYBE"
iconst_2
invokespecial javax/annotation/meta/When/<init>(Ljava/lang/String;I)V
putstatic javax/annotation/meta/When/MAYBE Ljavax/annotation/meta/When;
new javax/annotation/meta/When
dup
ldc "NEVER"
iconst_3
invokespecial javax/annotation/meta/When/<init>(Ljava/lang/String;I)V
putstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
iconst_4
anewarray javax/annotation/meta/When
dup
iconst_0
getstatic javax/annotation/meta/When/ALWAYS Ljavax/annotation/meta/When;
aastore
dup
iconst_1
getstatic javax/annotation/meta/When/UNKNOWN Ljavax/annotation/meta/When;
aastore
dup
iconst_2
getstatic javax/annotation/meta/When/MAYBE Ljavax/annotation/meta/When;
aastore
dup
iconst_3
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
aastore
putstatic javax/annotation/meta/When/$VALUES [Ljavax/annotation/meta/When;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Ljavax/annotation/meta/When;
ldc javax/annotation/meta/When
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast javax/annotation/meta/When
areturn
.limit locals 1
.limit stack 2
.end method

.method public static final values()[Ljavax/annotation/meta/When;
getstatic javax/annotation/meta/When/$VALUES [Ljavax/annotation/meta/When;
invokevirtual [Ljavax/annotation/meta/When;/clone()Ljava/lang/Object;
checkcast [Ljavax/annotation/meta/When;
areturn
.limit locals 0
.limit stack 1
.end method
