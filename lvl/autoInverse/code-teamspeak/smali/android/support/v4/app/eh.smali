.class final Landroid/support/v4/app/eh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "call"

.field public static final b:Ljava/lang/String; = "msg"

.field public static final c:Ljava/lang/String; = "email"

.field public static final d:Ljava/lang/String; = "event"

.field public static final e:Ljava/lang/String; = "promo"

.field public static final f:Ljava/lang/String; = "alarm"

.field public static final g:Ljava/lang/String; = "progress"

.field public static final h:Ljava/lang/String; = "social"

.field public static final i:Ljava/lang/String; = "err"

.field public static final j:Ljava/lang/String; = "transport"

.field public static final k:Ljava/lang/String; = "sys"

.field public static final l:Ljava/lang/String; = "service"

.field public static final m:Ljava/lang/String; = "recommendation"

.field public static final n:Ljava/lang/String; = "status"

.field private static final o:Ljava/lang/String; = "author"

.field private static final p:Ljava/lang/String; = "text"

.field private static final q:Ljava/lang/String; = "messages"

.field private static final r:Ljava/lang/String; = "remote_input"

.field private static final s:Ljava/lang/String; = "on_reply"

.field private static final t:Ljava/lang/String; = "on_read"

.field private static final u:Ljava/lang/String; = "participants"

.field private static final v:Ljava/lang/String; = "timestamp"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method

.method static a(Landroid/support/v4/app/fw;)Landroid/app/RemoteInput;
    .registers 3

    .prologue
    .line 206
    new-instance v0, Landroid/app/RemoteInput$Builder;

    invoke-virtual {p0}, Landroid/support/v4/app/fw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/RemoteInput$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/fw;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/RemoteInput$Builder;->setLabel(Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/fw;->c()[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/RemoteInput$Builder;->setChoices([Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/fw;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/RemoteInput$Builder;->setAllowFreeFormInput(Z)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/fw;->e()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/RemoteInput$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/RemoteInput$Builder;->build()Landroid/app/RemoteInput;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/support/v4/app/em;)Landroid/os/Bundle;
    .registers 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 129
    if-nez p0, :cond_5

    .line 153
    :goto_4
    return-object v0

    .line 132
    :cond_5
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 134
    invoke-virtual {p0}, Landroid/support/v4/app/em;->d()[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1e

    invoke-virtual {p0}, Landroid/support/v4/app/em;->d()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1e

    .line 135
    invoke-virtual {p0}, Landroid/support/v4/app/em;->d()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v1

    .line 137
    :cond_1e
    invoke-virtual {p0}, Landroid/support/v4/app/em;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [Landroid/os/Parcelable;

    .line 138
    :goto_25
    array-length v4, v3

    if-ge v1, v4, :cond_42

    .line 139
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 140
    const-string v5, "text"

    invoke-virtual {p0}, Landroid/support/v4/app/em;->a()[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v5, "author"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    aput-object v4, v3, v1

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_25

    .line 144
    :cond_42
    const-string v0, "messages"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 145
    invoke-virtual {p0}, Landroid/support/v4/app/em;->g()Landroid/support/v4/app/fw;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_7f

    .line 147
    const-string v1, "remote_input"

    .line 1206
    new-instance v3, Landroid/app/RemoteInput$Builder;

    invoke-virtual {v0}, Landroid/support/v4/app/fw;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/RemoteInput$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/fw;->b()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/RemoteInput$Builder;->setLabel(Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/support/v4/app/fw;->c()[Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/RemoteInput$Builder;->setChoices([Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/support/v4/app/fw;->d()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/RemoteInput$Builder;->setAllowFreeFormInput(Z)Landroid/app/RemoteInput$Builder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/support/v4/app/fw;->e()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/RemoteInput$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/RemoteInput$Builder;->build()Landroid/app/RemoteInput;

    move-result-object v0

    .line 147
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 149
    :cond_7f
    const-string v0, "on_reply"

    invoke-virtual {p0}, Landroid/support/v4/app/em;->b()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 150
    const-string v0, "on_read"

    invoke-virtual {p0}, Landroid/support/v4/app/em;->c()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 151
    const-string v0, "participants"

    invoke-virtual {p0}, Landroid/support/v4/app/em;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 152
    const-string v0, "timestamp"

    invoke-virtual {p0}, Landroid/support/v4/app/em;->f()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    move-object v0, v2

    .line 153
    goto/16 :goto_4
.end method

.method private static a(Landroid/os/Bundle;Landroid/support/v4/app/en;Landroid/support/v4/app/fx;)Landroid/support/v4/app/em;
    .registers 15

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 159
    if-nez p0, :cond_6

    .line 196
    :cond_5
    :goto_5
    return-object v4

    .line 162
    :cond_6
    const-string v0, "messages"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    .line 164
    if-eqz v6, :cond_86

    .line 165
    array-length v0, v6

    new-array v3, v0, [Ljava/lang/String;

    move v1, v2

    .line 167
    :goto_12
    array-length v0, v3

    if-ge v1, v0, :cond_84

    .line 168
    aget-object v0, v6, v1

    instance-of v0, v0, Landroid/os/Bundle;

    if-nez v0, :cond_6e

    .line 178
    :cond_1b
    :goto_1b
    if-eqz v2, :cond_5

    move-object v10, v3

    .line 185
    :goto_1e
    const-string v0, "on_read"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/app/PendingIntent;

    .line 186
    const-string v0, "on_reply"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/app/PendingIntent;

    .line 188
    const-string v0, "remote_input"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/RemoteInput;

    .line 190
    const-string v1, "participants"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 191
    if-eqz v11, :cond_5

    array-length v1, v11

    if-ne v1, v5, :cond_5

    .line 196
    if-eqz v0, :cond_82

    .line 1217
    invoke-virtual {v0}, Landroid/app/RemoteInput;->getResultKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getChoices()[Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v4

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    move-object v0, p2

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/app/fx;->a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw;

    move-result-object v2

    .line 196
    :goto_5e
    const-string v0, "timestamp"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object v0, p1

    move-object v1, v10

    move-object v3, v9

    move-object v4, v8

    move-object v5, v11

    invoke-interface/range {v0 .. v7}, Landroid/support/v4/app/en;->a([Ljava/lang/String;Landroid/support/v4/app/fw;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)Landroid/support/v4/app/em;

    move-result-object v4

    goto :goto_5

    .line 172
    :cond_6e
    aget-object v0, v6, v1

    check-cast v0, Landroid/os/Bundle;

    const-string v7, "text"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    .line 173
    aget-object v0, v3, v1

    if-eqz v0, :cond_1b

    .line 167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    :cond_82
    move-object v2, v4

    .line 196
    goto :goto_5e

    :cond_84
    move v2, v5

    goto :goto_1b

    :cond_86
    move-object v10, v4

    goto :goto_1e
.end method

.method private static a(Landroid/app/RemoteInput;Landroid/support/v4/app/fx;)Landroid/support/v4/app/fw;
    .registers 8

    .prologue
    .line 217
    invoke-virtual {p0}, Landroid/app/RemoteInput;->getResultKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/RemoteInput;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Landroid/app/RemoteInput;->getChoices()[Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p0}, Landroid/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v4

    invoke-virtual {p0}, Landroid/app/RemoteInput;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/app/fx;->a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/Notification;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Landroid/app/Notification;->category:Ljava/lang/String;

    return-object v0
.end method
