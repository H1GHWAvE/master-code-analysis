.class final Landroid/support/v4/app/cu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/c/ac;
.implements Landroid/support/v4/c/ad;


# instance fields
.field final a:I

.field final b:Landroid/os/Bundle;

.field c:Landroid/support/v4/app/cs;

.field d:Landroid/support/v4/c/aa;

.field e:Z

.field f:Z

.field g:Ljava/lang/Object;

.field h:Z

.field i:Z

.field j:Z

.field k:Z

.field l:Z

.field m:Z

.field n:Landroid/support/v4/app/cu;

.field final synthetic o:Landroid/support/v4/app/ct;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/ct;ILandroid/os/Bundle;Landroid/support/v4/app/cs;)V
    .registers 5

    .prologue
    .line 235
    iput-object p1, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    iput p2, p0, Landroid/support/v4/app/cu;->a:I

    .line 237
    iput-object p3, p0, Landroid/support/v4/app/cu;->b:Landroid/os/Bundle;

    .line 238
    iput-object p4, p0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    .line 239
    return-void
.end method

.method private e()V
    .registers 4

    .prologue
    .line 278
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_18

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Retaining: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_18
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/cu;->i:Z

    .line 280
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    iput-boolean v0, p0, Landroid/support/v4/app/cu;->j:Z

    .line 281
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    .line 283
    return-void
.end method

.method private f()V
    .registers 4

    .prologue
    .line 286
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->i:Z

    if-eqz v0, :cond_2c

    .line 287
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_1c

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Finished Retaining: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :cond_1c
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/cu;->i:Z

    .line 289
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    iget-boolean v1, p0, Landroid/support/v4/app/cu;->j:Z

    if-eq v0, v1, :cond_2c

    .line 290
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    if-nez v0, :cond_2c

    .line 294
    invoke-virtual {p0}, Landroid/support/v4/app/cu;->b()V

    .line 299
    :cond_2c
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v0, :cond_3f

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v0, :cond_3f

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->k:Z

    if-nez v0, :cond_3f

    .line 306
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget-object v1, p0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 308
    :cond_3f
    return-void
.end method

.method private g()V
    .registers 3

    .prologue
    .line 311
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v0, :cond_16

    .line 312
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->k:Z

    if-eqz v0, :cond_16

    .line 313
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/cu;->k:Z

    .line 314
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v0, :cond_16

    .line 315
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget-object v1, p0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 319
    :cond_16
    return-void
.end method

.method private h()V
    .registers 4

    .prologue
    .line 336
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_18

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Canceling: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :cond_18
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v0, :cond_2f

    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    if-eqz v0, :cond_2f

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->m:Z

    if-eqz v0, :cond_2f

    .line 338
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0}, Landroid/support/v4/c/aa;->j()Z

    move-result v0

    if-nez v0, :cond_2f

    .line 339
    invoke-virtual {p0}, Landroid/support/v4/app/cu;->d()V

    .line 342
    :cond_2f
    return-void
.end method


# virtual methods
.method final a()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 242
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->i:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->j:Z

    if-eqz v0, :cond_c

    .line 246
    iput-boolean v3, p0, Landroid/support/v4/app/cu;->h:Z

    .line 275
    :cond_b
    :goto_b
    return-void

    .line 250
    :cond_c
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    if-nez v0, :cond_b

    .line 255
    iput-boolean v3, p0, Landroid/support/v4/app/cu;->h:Z

    .line 257
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_2a

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Starting: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_2a
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    if-nez v0, :cond_3a

    iget-object v0, p0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    if-eqz v0, :cond_3a

    .line 259
    iget-object v0, p0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    invoke-interface {v0}, Landroid/support/v4/app/cs;->a()Landroid/support/v4/c/aa;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    .line 261
    :cond_3a
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    if-eqz v0, :cond_b

    .line 262
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v0

    if-eqz v0, :cond_71

    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_71

    .line 264
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Object returned from onCreateLoader must not be a non-static inner member class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_71
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->m:Z

    if-nez v0, :cond_9b

    .line 269
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget v1, p0, Landroid/support/v4/app/cu;->a:I

    .line 1164
    iget-object v2, v0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    if-eqz v2, :cond_85

    .line 1165
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a listener registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1167
    :cond_85
    iput-object p0, v0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    .line 1168
    iput v1, v0, Landroid/support/v4/c/aa;->p:I

    .line 270
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    .line 1196
    iget-object v1, v0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    if-eqz v1, :cond_97

    .line 1197
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a listener registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1199
    :cond_97
    iput-object p0, v0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    .line 271
    iput-boolean v3, p0, Landroid/support/v4/app/cu;->m:Z

    .line 273
    :cond_9b
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0}, Landroid/support/v4/c/aa;->i()V

    goto/16 :goto_b
.end method

.method public final a(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
    .registers 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 411
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_1a

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadComplete: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_1a
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->l:Z

    if-eqz v0, :cond_2a

    .line 414
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_29

    const-string v0, "LoaderManager"

    const-string v1, "  Ignoring load complete -- destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :cond_29
    :goto_29
    return-void

    .line 418
    :cond_2a
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    iget-object v0, v0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    iget v1, p0, Landroid/support/v4/app/cu;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_42

    .line 421
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_29

    const-string v0, "LoaderManager"

    const-string v1, "  Ignoring load complete -- not active"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_29

    .line 425
    :cond_42
    iget-object v0, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    .line 426
    if-eqz v0, :cond_72

    .line 430
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_5e

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Switching to pending loader: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_5e
    iput-object v5, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    .line 432
    iget-object v1, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    iget-object v1, v1, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    iget v2, p0, Landroid/support/v4/app/cu;->a:I

    invoke-virtual {v1, v2, v5}, Landroid/support/v4/n/w;->a(ILjava/lang/Object;)V

    .line 433
    invoke-virtual {p0}, Landroid/support/v4/app/cu;->c()V

    .line 434
    iget-object v1, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/cu;)V

    goto :goto_29

    .line 440
    :cond_72
    iget-object v0, p0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    if-ne v0, p2, :cond_7a

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->e:Z

    if-nez v0, :cond_85

    .line 441
    :cond_7a
    iput-object p2, p0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    .line 442
    iput-boolean v4, p0, Landroid/support/v4/app/cu;->e:Z

    .line 443
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v0, :cond_85

    .line 444
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 454
    :cond_85
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    iget-object v0, v0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    iget v1, p0, Landroid/support/v4/app/cu;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 455
    if-eqz v0, :cond_bb

    if-eq v0, p0, :cond_bb

    .line 456
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/app/cu;->f:Z

    .line 457
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->c()V

    .line 458
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    iget-object v0, v0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    iget v1, p0, Landroid/support/v4/app/cu;->a:I

    .line 3098
    iget-object v2, v0, Landroid/support/v4/n/w;->c:[I

    iget v3, v0, Landroid/support/v4/n/w;->e:I

    invoke-static {v2, v3, v1}, Landroid/support/v4/n/f;->a([III)I

    move-result v1

    .line 3100
    if-ltz v1, :cond_bb

    .line 3101
    iget-object v2, v0, Landroid/support/v4/n/w;->d:[Ljava/lang/Object;

    aget-object v2, v2, v1

    sget-object v3, Landroid/support/v4/n/w;->a:Ljava/lang/Object;

    if-eq v2, v3, :cond_bb

    .line 3102
    iget-object v2, v0, Landroid/support/v4/n/w;->d:[Ljava/lang/Object;

    sget-object v3, Landroid/support/v4/n/w;->a:Ljava/lang/Object;

    aput-object v3, v2, v1

    .line 3103
    iput-boolean v4, v0, Landroid/support/v4/n/w;->b:Z

    .line 461
    :cond_bb
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->a()Z

    move-result v0

    if-nez v0, :cond_29

    .line 462
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->j()V

    goto/16 :goto_29
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 500
    :goto_0
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/cu;->a:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 501
    const-string v0, " mArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/cu;->b:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 502
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCallbacks="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 503
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLoader="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 504
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    if-eqz v0, :cond_4d

    .line 505
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/c/aa;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 507
    :cond_4d
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->e:Z

    if-nez v0, :cond_55

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->f:Z

    if-eqz v0, :cond_79

    .line 508
    :cond_55
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHaveData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->e:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 509
    const-string v0, "  mDeliveredData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->f:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 510
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 512
    :cond_79
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->h:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 513
    const-string v0, " mReportNextStart="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->k:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 514
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->l:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 515
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->i:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 516
    const-string v0, " mRetainingStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->j:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 517
    const-string v0, " mListenerRegistered="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->m:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 518
    iget-object v0, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    if-eqz v0, :cond_e8

    .line 519
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Loader "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 520
    iget-object v0, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 521
    iget-object p0, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 523
    :cond_e8
    return-void
.end method

.method final b()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 322
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_19

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Stopping: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_19
    iput-boolean v3, p0, Landroid/support/v4/app/cu;->h:Z

    .line 324
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->i:Z

    if-nez v0, :cond_38

    .line 325
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    if-eqz v0, :cond_38

    iget-boolean v0, p0, Landroid/support/v4/app/cu;->m:Z

    if-eqz v0, :cond_38

    .line 327
    iput-boolean v3, p0, Landroid/support/v4/app/cu;->m:Z

    .line 328
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0, p0}, Landroid/support/v4/c/aa;->a(Landroid/support/v4/c/ad;)V

    .line 329
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0, p0}, Landroid/support/v4/c/aa;->a(Landroid/support/v4/c/ac;)V

    .line 330
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0}, Landroid/support/v4/c/aa;->l()V

    .line 333
    :cond_38
    return-void
.end method

.method final b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
    .registers 8

    .prologue
    .line 467
    iget-object v0, p0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    if-eqz v0, :cond_6e

    .line 468
    const/4 v0, 0x0

    .line 469
    iget-object v1, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v1}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v1

    if-eqz v1, :cond_83

    .line 470
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    .line 471
    iget-object v1, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v1}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    const-string v2, "onLoadFinished"

    iput-object v2, v1, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    move-object v1, v0

    .line 474
    :goto_24
    :try_start_24
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_59

    const-string v0, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  onLoadFinished in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3497
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x40

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 3498
    invoke-static {p2, v3}, Landroid/support/v4/n/g;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 3499
    const-string v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3500
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 474
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_59
    .catchall {:try_start_24 .. :try_end_59} :catchall_6f

    .line 478
    :cond_59
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 479
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iput-object v1, v0, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    .line 482
    :cond_6b
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/cu;->f:Z

    .line 484
    :cond_6e
    return-void

    .line 478
    :catchall_6f
    move-exception v0

    iget-object v2, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v2}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v2

    if-eqz v2, :cond_82

    .line 479
    iget-object v2, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v2}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v2

    iget-object v2, v2, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iput-object v1, v2, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    :cond_82
    throw v0

    :cond_83
    move-object v1, v0

    goto :goto_24
.end method

.method final c()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 345
    :goto_2
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_1a

    const-string v0, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Destroying: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_1a
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/cu;->l:Z

    .line 347
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->f:Z

    .line 348
    iput-boolean v4, p0, Landroid/support/v4/app/cu;->f:Z

    .line 349
    iget-object v2, p0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    if-eqz v2, :cond_77

    iget-object v2, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    if-eqz v2, :cond_77

    iget-boolean v2, p0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v2, :cond_77

    if-eqz v0, :cond_77

    .line 350
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_47

    const-string v0, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Reseting: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :cond_47
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v0

    if-eqz v0, :cond_9f

    .line 353
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    .line 354
    iget-object v2, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v2}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v2

    iget-object v2, v2, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    const-string v3, "onLoaderReset"

    iput-object v3, v2, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    .line 359
    :goto_65
    iget-object v2, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v2}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v2

    if-eqz v2, :cond_77

    .line 360
    iget-object v2, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-static {v2}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;

    move-result-object v2

    iget-object v2, v2, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iput-object v0, v2, Landroid/support/v4/app/bl;->B:Ljava/lang/String;

    .line 364
    :cond_77
    iput-object v1, p0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    .line 365
    iput-object v1, p0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    .line 366
    iput-boolean v4, p0, Landroid/support/v4/app/cu;->e:Z

    .line 367
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    if-eqz v0, :cond_96

    .line 368
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->m:Z

    if-eqz v0, :cond_91

    .line 369
    iput-boolean v4, p0, Landroid/support/v4/app/cu;->m:Z

    .line 370
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0, p0}, Landroid/support/v4/c/aa;->a(Landroid/support/v4/c/ad;)V

    .line 371
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0, p0}, Landroid/support/v4/c/aa;->a(Landroid/support/v4/c/ac;)V

    .line 373
    :cond_91
    iget-object v0, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v0}, Landroid/support/v4/c/aa;->m()V

    .line 375
    :cond_96
    iget-object v0, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    if-eqz v0, :cond_9e

    .line 376
    iget-object p0, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    goto/16 :goto_2

    .line 378
    :cond_9e
    return-void

    :cond_9f
    move-object v0, v1

    goto :goto_65
.end method

.method public final d()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 382
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_19

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadCanceled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_19
    iget-boolean v0, p0, Landroid/support/v4/app/cu;->l:Z

    if-eqz v0, :cond_29

    .line 385
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_28

    const-string v0, "LoaderManager"

    const-string v1, "  Ignoring load canceled -- destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    :cond_28
    :goto_28
    return-void

    .line 389
    :cond_29
    iget-object v0, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    iget-object v0, v0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    iget v1, p0, Landroid/support/v4/app/cu;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_41

    .line 392
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_28

    const-string v0, "LoaderManager"

    const-string v1, "  Ignoring load canceled -- not active"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_28

    .line 396
    :cond_41
    iget-object v0, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    .line 397
    if-eqz v0, :cond_28

    .line 401
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_5d

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Switching to pending loader: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    :cond_5d
    iput-object v4, p0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    .line 403
    iget-object v1, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    iget-object v1, v1, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    iget v2, p0, Landroid/support/v4/app/cu;->a:I

    invoke-virtual {v1, v2, v4}, Landroid/support/v4/n/w;->a(ILjava/lang/Object;)V

    .line 404
    invoke-virtual {p0}, Landroid/support/v4/app/cu;->c()V

    .line 405
    iget-object v1, p0, Landroid/support/v4/app/cu;->o:Landroid/support/v4/app/ct;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/cu;)V

    goto :goto_28
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 488
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 489
    const-string v1, "LoaderInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    iget v1, p0, Landroid/support/v4/app/cu;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 493
    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    iget-object v1, p0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-static {v1, v0}, Landroid/support/v4/n/g;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 495
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
