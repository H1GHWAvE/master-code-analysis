.class public abstract Landroid/support/v4/app/bw;
.super Landroid/support/v4/view/by;
.source "SourceFile"


# static fields
.field private static final c:Ljava/lang/String; = "FragmentPagerAdapter"

.field private static final d:Z


# instance fields
.field private final e:Landroid/support/v4/app/bi;

.field private f:Landroid/support/v4/app/cd;

.field private g:Landroid/support/v4/app/Fragment;


# direct methods
.method private constructor <init>(Landroid/support/v4/app/bi;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Landroid/support/v4/view/by;-><init>()V

    .line 66
    iput-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    .line 67
    iput-object v0, p0, Landroid/support/v4/app/bw;->g:Landroid/support/v4/app/Fragment;

    .line 70
    iput-object p1, p0, Landroid/support/v4/app/bw;->e:Landroid/support/v4/app/bi;

    .line 71
    return-void
.end method

.method private static a(I)J
    .registers 3

    .prologue
    .line 169
    int-to-long v0, p0

    return-wide v0
.end method

.method private static a(IJ)Ljava/lang/String;
    .registers 6

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:switcher:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a()Landroid/support/v4/app/Fragment;
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .registers 10

    .prologue
    const/4 v6, 0x0

    .line 84
    iget-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    if-nez v0, :cond_d

    .line 85
    iget-object v0, p0, Landroid/support/v4/app/bw;->e:Landroid/support/v4/app/bi;

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    .line 2169
    :cond_d
    int-to-long v2, p2

    .line 91
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    invoke-static {v0, v2, v3}, Landroid/support/v4/app/bw;->a(IJ)Ljava/lang/String;

    move-result-object v0

    .line 92
    iget-object v1, p0, Landroid/support/v4/app/bw;->e:Landroid/support/v4/app/bi;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_2e

    .line 95
    iget-object v1, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/cd;->f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 102
    :goto_23
    iget-object v1, p0, Landroid/support/v4/app/bw;->g:Landroid/support/v4/app/Fragment;

    if-eq v0, v1, :cond_2d

    .line 103
    invoke-virtual {v0, v6}, Landroid/support/v4/app/Fragment;->b(Z)V

    .line 104
    invoke-virtual {v0, v6}, Landroid/support/v4/app/Fragment;->c(Z)V

    .line 107
    :cond_2d
    return-object v0

    .line 97
    :cond_2e
    invoke-virtual {p0}, Landroid/support/v4/app/bw;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 99
    iget-object v1, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v5

    invoke-static {v5, v2, v3}, Landroid/support/v4/app/bw;->a(IJ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v0, v2}, Landroid/support/v4/app/cd;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    goto :goto_23
.end method

.method public final a(ILjava/lang/Object;)V
    .registers 4

    .prologue
    .line 112
    iget-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    if-nez v0, :cond_c

    .line 113
    iget-object v0, p0, Landroid/support/v4/app/bw;->e:Landroid/support/v4/app/bi;

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    .line 117
    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    check-cast p2, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/cd;->e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 118
    return-void
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .registers 3

    .prologue
    .line 157
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 122
    check-cast p1, Landroid/support/v4/app/Fragment;

    .line 123
    iget-object v0, p0, Landroid/support/v4/app/bw;->g:Landroid/support/v4/app/Fragment;

    if-eq p1, v0, :cond_20

    .line 124
    iget-object v0, p0, Landroid/support/v4/app/bw;->g:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_16

    .line 125
    iget-object v0, p0, Landroid/support/v4/app/bw;->g:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->b(Z)V

    .line 126
    iget-object v0, p0, Landroid/support/v4/app/bw;->g:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->c(Z)V

    .line 128
    :cond_16
    if-eqz p1, :cond_1e

    .line 129
    invoke-virtual {p1, v2}, Landroid/support/v4/app/Fragment;->b(Z)V

    .line 130
    invoke-virtual {p1, v2}, Landroid/support/v4/app/Fragment;->c(Z)V

    .line 132
    :cond_1e
    iput-object p1, p0, Landroid/support/v4/app/bw;->g:Landroid/support/v4/app/Fragment;

    .line 134
    :cond_20
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 147
    check-cast p2, Landroid/support/v4/app/Fragment;

    .line 2237
    iget-object v0, p2, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 147
    if-ne v0, p1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 80
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 138
    iget-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    if-eqz v0, :cond_11

    .line 139
    iget-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    invoke-virtual {v0}, Landroid/support/v4/app/cd;->j()I

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/bw;->f:Landroid/support/v4/app/cd;

    .line 141
    iget-object v0, p0, Landroid/support/v4/app/bw;->e:Landroid/support/v4/app/bi;

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->b()Z

    .line 143
    :cond_11
    return-void
.end method

.method public final d()Landroid/os/Parcelable;
    .registers 2

    .prologue
    .line 152
    const/4 v0, 0x0

    return-object v0
.end method
