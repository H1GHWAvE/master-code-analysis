.class final Landroid/support/v4/app/q;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    return-void
.end method

.method static a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;
    .registers 2

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 74
    if-eqz p0, :cond_8

    .line 75
    new-instance v0, Landroid/support/v4/app/s;

    invoke-direct {v0, p0}, Landroid/support/v4/app/s;-><init>(Landroid/support/v4/app/r;)V

    .line 77
    :cond_8
    return-object v0
.end method

.method private static a(Landroid/app/Activity;)V
    .registers 1

    .prologue
    .line 35
    invoke-virtual {p0}, Landroid/app/Activity;->finishAfterTransition()V

    .line 36
    return-void
.end method

.method private static a(Landroid/app/Activity;Landroid/support/v4/app/r;)V
    .registers 3

    .prologue
    .line 40
    invoke-static {p1}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 41
    return-void
.end method

.method private static b(Landroid/app/Activity;)V
    .registers 1

    .prologue
    .line 49
    invoke-virtual {p0}, Landroid/app/Activity;->postponeEnterTransition()V

    .line 50
    return-void
.end method

.method private static b(Landroid/app/Activity;Landroid/support/v4/app/r;)V
    .registers 3

    .prologue
    .line 45
    invoke-static {p1}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setExitSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 46
    return-void
.end method

.method private static c(Landroid/app/Activity;)V
    .registers 1

    .prologue
    .line 53
    invoke-virtual {p0}, Landroid/app/Activity;->startPostponedEnterTransition()V

    .line 54
    return-void
.end method
