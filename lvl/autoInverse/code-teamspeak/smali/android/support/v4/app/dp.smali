.class public final Landroid/support/v4/app/dp;
.super Landroid/support/v4/app/em;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/app/en;


# instance fields
.field private final b:[Ljava/lang/String;

.field private final c:Landroid/support/v4/app/fn;

.field private final d:Landroid/app/PendingIntent;

.field private final e:Landroid/app/PendingIntent;

.field private final f:[Ljava/lang/String;

.field private final g:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 3157
    new-instance v0, Landroid/support/v4/app/dq;

    invoke-direct {v0}, Landroid/support/v4/app/dq;-><init>()V

    sput-object v0, Landroid/support/v4/app/dp;->a:Landroid/support/v4/app/en;

    return-void
.end method

.method constructor <init>([Ljava/lang/String;Landroid/support/v4/app/fn;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)V
    .registers 8

    .prologue
    .line 3088
    invoke-direct {p0}, Landroid/support/v4/app/em;-><init>()V

    .line 3089
    iput-object p1, p0, Landroid/support/v4/app/dp;->b:[Ljava/lang/String;

    .line 3090
    iput-object p2, p0, Landroid/support/v4/app/dp;->c:Landroid/support/v4/app/fn;

    .line 3091
    iput-object p4, p0, Landroid/support/v4/app/dp;->e:Landroid/app/PendingIntent;

    .line 3092
    iput-object p3, p0, Landroid/support/v4/app/dp;->d:Landroid/app/PendingIntent;

    .line 3093
    iput-object p5, p0, Landroid/support/v4/app/dp;->f:[Ljava/lang/String;

    .line 3094
    iput-wide p6, p0, Landroid/support/v4/app/dp;->g:J

    .line 3095
    return-void
.end method

.method private h()Landroid/support/v4/app/fn;
    .registers 2

    .prologue
    .line 3111
    iget-object v0, p0, Landroid/support/v4/app/dp;->c:Landroid/support/v4/app/fn;

    return-object v0
.end method


# virtual methods
.method public final a()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 3102
    iget-object v0, p0, Landroid/support/v4/app/dp;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final b()Landroid/app/PendingIntent;
    .registers 2

    .prologue
    .line 3120
    iget-object v0, p0, Landroid/support/v4/app/dp;->d:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final c()Landroid/app/PendingIntent;
    .registers 2

    .prologue
    .line 3129
    iget-object v0, p0, Landroid/support/v4/app/dp;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final d()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 3137
    iget-object v0, p0, Landroid/support/v4/app/dp;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3145
    iget-object v0, p0, Landroid/support/v4/app/dp;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_b

    iget-object v0, p0, Landroid/support/v4/app/dp;->f:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final f()J
    .registers 3

    .prologue
    .line 3153
    iget-wide v0, p0, Landroid/support/v4/app/dp;->g:J

    return-wide v0
.end method

.method public final bridge synthetic g()Landroid/support/v4/app/fw;
    .registers 2

    .prologue
    .line 3078
    .line 4111
    iget-object v0, p0, Landroid/support/v4/app/dp;->c:Landroid/support/v4/app/fn;

    .line 3078
    return-object v0
.end method
