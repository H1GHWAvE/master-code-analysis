.class final Landroid/support/v4/app/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Ljava/util/ArrayList;

.field final synthetic d:Landroid/support/v4/app/ap;

.field final synthetic e:Z

.field final synthetic f:Landroid/support/v4/app/Fragment;

.field final synthetic g:Landroid/support/v4/app/Fragment;

.field final synthetic h:Landroid/support/v4/app/ak;


# direct methods
.method constructor <init>(Landroid/support/v4/app/ak;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V
    .registers 9

    .prologue
    .line 1234
    iput-object p1, p0, Landroid/support/v4/app/am;->h:Landroid/support/v4/app/ak;

    iput-object p2, p0, Landroid/support/v4/app/am;->a:Landroid/view/View;

    iput-object p3, p0, Landroid/support/v4/app/am;->b:Ljava/lang/Object;

    iput-object p4, p0, Landroid/support/v4/app/am;->c:Ljava/util/ArrayList;

    iput-object p5, p0, Landroid/support/v4/app/am;->d:Landroid/support/v4/app/ap;

    iput-boolean p6, p0, Landroid/support/v4/app/am;->e:Z

    iput-object p7, p0, Landroid/support/v4/app/am;->f:Landroid/support/v4/app/Fragment;

    iput-object p8, p0, Landroid/support/v4/app/am;->g:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .registers 5

    .prologue
    .line 1237
    iget-object v0, p0, Landroid/support/v4/app/am;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1239
    iget-object v0, p0, Landroid/support/v4/app/am;->b:Ljava/lang/Object;

    if-eqz v0, :cond_40

    .line 1240
    iget-object v0, p0, Landroid/support/v4/app/am;->b:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v4/app/am;->c:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 1242
    iget-object v0, p0, Landroid/support/v4/app/am;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1244
    iget-object v0, p0, Landroid/support/v4/app/am;->h:Landroid/support/v4/app/ak;

    iget-object v1, p0, Landroid/support/v4/app/am;->d:Landroid/support/v4/app/ap;

    iget-boolean v2, p0, Landroid/support/v4/app/am;->e:Z

    iget-object v3, p0, Landroid/support/v4/app/am;->f:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ak;Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;)Landroid/support/v4/n/a;

    move-result-object v0

    .line 1246
    iget-object v1, p0, Landroid/support/v4/app/am;->b:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/am;->d:Landroid/support/v4/app/ap;

    iget-object v2, v2, Landroid/support/v4/app/ap;->d:Landroid/view/View;

    iget-object v3, p0, Landroid/support/v4/app/am;->c:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0, v3}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V

    .line 1249
    iget-object v1, p0, Landroid/support/v4/app/am;->h:Landroid/support/v4/app/ak;

    iget-object v2, p0, Landroid/support/v4/app/am;->d:Landroid/support/v4/app/ap;

    invoke-static {v1, v0, v2}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ak;Landroid/support/v4/n/a;Landroid/support/v4/app/ap;)V

    .line 1251
    iget-object v1, p0, Landroid/support/v4/app/am;->f:Landroid/support/v4/app/Fragment;

    iget-object v2, p0, Landroid/support/v4/app/am;->g:Landroid/support/v4/app/Fragment;

    iget-boolean v3, p0, Landroid/support/v4/app/am;->e:Z

    invoke-static {v1, v2, v3, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLandroid/support/v4/n/a;)V

    .line 1255
    :cond_40
    const/4 v0, 0x1

    return v0
.end method
