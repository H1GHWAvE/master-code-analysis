.class public final Landroid/support/v4/app/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/ac;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Landroid/support/v4/app/c;

.field private static final b:F = 0.33333334f

.field private static final c:I = 0x102002c


# instance fields
.field private final d:Landroid/app/Activity;

.field private final e:Landroid/support/v4/app/g;

.field private final f:Landroid/support/v4/widget/DrawerLayout;

.field private g:Z

.field private h:Z

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/support/v4/app/i;

.field private final l:I

.field private final m:I

.field private final n:I

.field private o:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 176
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 177
    const/16 v1, 0x12

    if-lt v0, v1, :cond_f

    .line 178
    new-instance v0, Landroid/support/v4/app/f;

    invoke-direct {v0, v2}, Landroid/support/v4/app/f;-><init>(B)V

    sput-object v0, Landroid/support/v4/app/a;->a:Landroid/support/v4/app/c;

    .line 184
    :goto_e
    return-void

    .line 179
    :cond_f
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1b

    .line 180
    new-instance v0, Landroid/support/v4/app/e;

    invoke-direct {v0, v2}, Landroid/support/v4/app/e;-><init>(B)V

    sput-object v0, Landroid/support/v4/app/a;->a:Landroid/support/v4/app/c;

    goto :goto_e

    .line 182
    :cond_1b
    new-instance v0, Landroid/support/v4/app/d;

    invoke-direct {v0, v2}, Landroid/support/v4/app/d;-><init>(B)V

    sput-object v0, Landroid/support/v4/app/a;->a:Landroid/support/v4/app/c;

    goto :goto_e
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
    .registers 13
    .param p3    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x15

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 229
    .line 1234
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    if-lt v1, v2, :cond_1d

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_1d

    move v1, v3

    .line 229
    :goto_11
    if-nez v1, :cond_1f

    :goto_13
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/app/a;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;ZIII)V

    .line 231
    return-void

    :cond_1d
    move v1, v0

    .line 1234
    goto :goto_11

    :cond_1f
    move v3, v0

    .line 229
    goto :goto_13
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;ZIII)V
    .registers 10
    .param p4    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    .line 262
    iput-object p1, p0, Landroid/support/v4/app/a;->d:Landroid/app/Activity;

    .line 265
    instance-of v0, p1, Landroid/support/v4/app/h;

    if-eqz v0, :cond_40

    move-object v0, p1

    .line 266
    check-cast v0, Landroid/support/v4/app/h;

    invoke-interface {v0}, Landroid/support/v4/app/h;->a()Landroid/support/v4/app/g;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->e:Landroid/support/v4/app/g;

    .line 271
    :goto_15
    iput-object p2, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    .line 272
    iput p4, p0, Landroid/support/v4/app/a;->l:I

    .line 273
    iput p5, p0, Landroid/support/v4/app/a;->m:I

    .line 274
    iput p6, p0, Landroid/support/v4/app/a;->n:I

    .line 276
    invoke-direct {p0}, Landroid/support/v4/app/a;->g()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    .line 277
    invoke-static {p1, p4}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->j:Landroid/graphics/drawable/Drawable;

    .line 278
    new-instance v0, Landroid/support/v4/app/i;

    iget-object v1, p0, Landroid/support/v4/app/a;->j:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Landroid/support/v4/app/i;-><init>(Landroid/support/v4/app/a;Landroid/graphics/drawable/Drawable;B)V

    iput-object v0, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    .line 279
    iget-object v1, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    if-eqz p3, :cond_44

    const v0, 0x3eaaaaab

    .line 1534
    :goto_3a
    iput v0, v1, Landroid/support/v4/app/i;->b:F

    .line 1535
    invoke-virtual {v1}, Landroid/support/v4/app/i;->invalidateSelf()V

    .line 280
    return-void

    .line 268
    :cond_40
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/a;->e:Landroid/support/v4/app/g;

    goto :goto_15

    .line 279
    :cond_44
    const/4 v0, 0x0

    goto :goto_3a
.end method

.method static synthetic a(Landroid/support/v4/app/a;)Landroid/app/Activity;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v4/app/a;->d:Landroid/app/Activity;

    return-object v0
.end method

.method private a(I)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 341
    const/4 v0, 0x0

    .line 342
    if-eqz p1, :cond_a

    .line 343
    iget-object v0, p0, Landroid/support/v4/app/a;->d:Landroid/app/Activity;

    invoke-static {v0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2316
    :cond_a
    if-nez v0, :cond_1e

    .line 2317
    invoke-direct {p0}, Landroid/support/v4/app/a;->g()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    .line 2318
    iput-boolean v1, p0, Landroid/support/v4/app/a;->h:Z

    .line 2324
    :goto_14
    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    if-nez v0, :cond_1d

    .line 2325
    iget-object v0, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v1}, Landroid/support/v4/app/a;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 347
    :cond_1d
    return-void

    .line 2320
    :cond_1e
    iput-object v0, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    .line 2321
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/a;->h:Z

    goto :goto_14
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 316
    if-nez p1, :cond_15

    .line 317
    invoke-direct {p0}, Landroid/support/v4/app/a;->g()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    .line 318
    iput-boolean v1, p0, Landroid/support/v4/app/a;->h:Z

    .line 324
    :goto_b
    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    if-nez v0, :cond_14

    .line 325
    iget-object v0, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v1}, Landroid/support/v4/app/a;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 327
    :cond_14
    return-void

    .line 320
    :cond_15
    iput-object p1, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    .line 321
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/a;->h:Z

    goto :goto_b
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
    .registers 6

    .prologue
    .line 484
    iget-object v0, p0, Landroid/support/v4/app/a;->e:Landroid/support/v4/app/g;

    if-eqz v0, :cond_5

    .line 490
    :goto_4
    return-void

    .line 488
    :cond_5
    sget-object v0, Landroid/support/v4/app/a;->a:Landroid/support/v4/app/c;

    iget-object v1, p0, Landroid/support/v4/app/a;->o:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/a;->d:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, p1, p2}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->o:Ljava/lang/Object;

    goto :goto_4
.end method

.method private a(Z)V
    .registers 4

    .prologue
    .line 360
    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    if-eq p1, v0, :cond_17

    .line 361
    if-eqz p1, :cond_1b

    .line 362
    iget-object v1, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    iget-object v0, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_18

    iget v0, p0, Landroid/support/v4/app/a;->n:I

    :goto_12
    invoke-direct {p0, v1, v0}, Landroid/support/v4/app/a;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 367
    :goto_15
    iput-boolean p1, p0, Landroid/support/v4/app/a;->g:Z

    .line 369
    :cond_17
    return-void

    .line 362
    :cond_18
    iget v0, p0, Landroid/support/v4/app/a;->m:I

    goto :goto_12

    .line 365
    :cond_1b
    iget-object v0, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v4/app/a;->a(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_15
.end method

.method private static a(Landroid/content/Context;)Z
    .registers 3

    .prologue
    const/16 v1, 0x15

    .line 234
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    if-lt v0, v1, :cond_10

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private a(Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 405
    if-eqz p1, :cond_24

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_24

    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    if-eqz v0, :cond_24

    .line 406
    iget-object v0, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 407
    iget-object v0, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->b()V

    .line 411
    :goto_1c
    const/4 v0, 0x1

    .line 413
    :goto_1d
    return v0

    .line 409
    :cond_1e
    iget-object v0, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->a()V

    goto :goto_1c

    .line 413
    :cond_24
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private b(I)V
    .registers 5

    .prologue
    .line 493
    iget-object v0, p0, Landroid/support/v4/app/a;->e:Landroid/support/v4/app/g;

    if-eqz v0, :cond_5

    .line 499
    :goto_4
    return-void

    .line 497
    :cond_5
    sget-object v0, Landroid/support/v4/app/a;->a:Landroid/support/v4/app/c;

    iget-object v1, p0, Landroid/support/v4/app/a;->o:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/a;->d:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, p1}, Landroid/support/v4/app/c;->a(Ljava/lang/Object;Landroid/app/Activity;I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->o:Ljava/lang/Object;

    goto :goto_4
.end method

.method private d()V
    .registers 3

    .prologue
    .line 292
    iget-object v0, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 293
    iget-object v0, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/i;->a(F)V

    .line 298
    :goto_f
    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    if-eqz v0, :cond_22

    .line 299
    iget-object v1, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    iget-object v0, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_2a

    iget v0, p0, Landroid/support/v4/app/a;->n:I

    :goto_1f
    invoke-direct {p0, v1, v0}, Landroid/support/v4/app/a;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 302
    :cond_22
    return-void

    .line 295
    :cond_23
    iget-object v0, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/i;->a(F)V

    goto :goto_f

    .line 299
    :cond_2a
    iget v0, p0, Landroid/support/v4/app/a;->m:I

    goto :goto_1f
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 376
    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    return v0
.end method

.method private f()V
    .registers 3

    .prologue
    .line 388
    iget-boolean v0, p0, Landroid/support/v4/app/a;->h:Z

    if-nez v0, :cond_a

    .line 389
    invoke-direct {p0}, Landroid/support/v4/app/a;->g()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->i:Landroid/graphics/drawable/Drawable;

    .line 391
    :cond_a
    iget-object v0, p0, Landroid/support/v4/app/a;->d:Landroid/app/Activity;

    iget v1, p0, Landroid/support/v4/app/a;->l:I

    invoke-static {v0, v1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/a;->j:Landroid/graphics/drawable/Drawable;

    .line 3292
    iget-object v0, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 3293
    iget-object v0, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/i;->a(F)V

    .line 3298
    :goto_23
    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    if-eqz v0, :cond_36

    .line 3299
    iget-object v1, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    iget-object v0, p0, Landroid/support/v4/app/a;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_3e

    iget v0, p0, Landroid/support/v4/app/a;->n:I

    :goto_33
    invoke-direct {p0, v1, v0}, Landroid/support/v4/app/a;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 393
    :cond_36
    return-void

    .line 3295
    :cond_37
    iget-object v0, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/i;->a(F)V

    goto :goto_23

    .line 3299
    :cond_3e
    iget v0, p0, Landroid/support/v4/app/a;->m:I

    goto :goto_33
.end method

.method private g()Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 477
    iget-object v0, p0, Landroid/support/v4/app/a;->e:Landroid/support/v4/app/g;

    if-eqz v0, :cond_b

    .line 478
    iget-object v0, p0, Landroid/support/v4/app/a;->e:Landroid/support/v4/app/g;

    invoke-interface {v0}, Landroid/support/v4/app/g;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 480
    :goto_a
    return-object v0

    :cond_b
    sget-object v0, Landroid/support/v4/app/a;->a:Landroid/support/v4/app/c;

    iget-object v1, p0, Landroid/support/v4/app/a;->d:Landroid/app/Activity;

    invoke-interface {v0, v1}, Landroid/support/v4/app/c;->a(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_a
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 444
    iget-object v0, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/i;->a(F)V

    .line 445
    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    if-eqz v0, :cond_10

    .line 446
    iget v0, p0, Landroid/support/v4/app/a;->n:I

    invoke-direct {p0, v0}, Landroid/support/v4/app/a;->b(I)V

    .line 448
    :cond_10
    return-void
.end method

.method public final a(F)V
    .registers 6

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 426
    iget-object v0, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    .line 3523
    iget v0, v0, Landroid/support/v4/app/i;->a:F

    .line 427
    cmpl-float v1, p1, v2

    if-lez v1, :cond_1e

    .line 428
    const/4 v1, 0x0

    sub-float v2, p1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 432
    :goto_18
    iget-object v1, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/i;->a(F)V

    .line 433
    return-void

    .line 430
    :cond_1e
    mul-float v1, p1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_18
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 459
    iget-object v0, p0, Landroid/support/v4/app/a;->k:Landroid/support/v4/app/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/i;->a(F)V

    .line 460
    iget-boolean v0, p0, Landroid/support/v4/app/a;->g:Z

    if-eqz v0, :cond_f

    .line 461
    iget v0, p0, Landroid/support/v4/app/a;->m:I

    invoke-direct {p0, v0}, Landroid/support/v4/app/a;->b(I)V

    .line 463
    :cond_f
    return-void
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 474
    return-void
.end method
