.class public final Landroid/support/v4/app/dh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/os/Bundle;

.field private final b:I

.field private final c:Ljava/lang/CharSequence;

.field private final d:Landroid/app/PendingIntent;

.field private e:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .registers 5

    .prologue
    .line 1888
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/dh;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V

    .line 1889
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V
    .registers 6

    .prologue
    .line 1900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1901
    iput p1, p0, Landroid/support/v4/app/dh;->b:I

    .line 1902
    invoke-static {p2}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/dh;->c:Ljava/lang/CharSequence;

    .line 1903
    iput-object p3, p0, Landroid/support/v4/app/dh;->d:Landroid/app/PendingIntent;

    .line 1904
    iput-object p4, p0, Landroid/support/v4/app/dh;->a:Landroid/os/Bundle;

    .line 1905
    return-void
.end method

.method private constructor <init>(Landroid/support/v4/app/df;)V
    .registers 7

    .prologue
    .line 1897
    iget v0, p1, Landroid/support/v4/app/df;->b:I

    iget-object v1, p1, Landroid/support/v4/app/df;->c:Ljava/lang/CharSequence;

    iget-object v2, p1, Landroid/support/v4/app/df;->d:Landroid/app/PendingIntent;

    new-instance v3, Landroid/os/Bundle;

    invoke-static {p1}, Landroid/support/v4/app/df;->a(Landroid/support/v4/app/df;)Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-direct {p0, v0, v1, v2, v3}, Landroid/support/v4/app/dh;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V

    .line 1898
    return-void
.end method

.method private a()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 1927
    iget-object v0, p0, Landroid/support/v4/app/dh;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/app/dh;
    .registers 3

    .prologue
    .line 1915
    if-eqz p1, :cond_7

    .line 1916
    iget-object v0, p0, Landroid/support/v4/app/dh;->a:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1918
    :cond_7
    return-object p0
.end method

.method private a(Landroid/support/v4/app/di;)Landroid/support/v4/app/dh;
    .registers 2

    .prologue
    .line 1950
    invoke-interface {p1, p0}, Landroid/support/v4/app/di;->a(Landroid/support/v4/app/dh;)Landroid/support/v4/app/dh;

    .line 1951
    return-object p0
.end method

.method private a(Landroid/support/v4/app/fn;)Landroid/support/v4/app/dh;
    .registers 3

    .prologue
    .line 1938
    iget-object v0, p0, Landroid/support/v4/app/dh;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    .line 1939
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/dh;->e:Ljava/util/ArrayList;

    .line 1941
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/dh;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1942
    return-object p0
.end method

.method private b()Landroid/support/v4/app/df;
    .registers 8

    .prologue
    .line 1960
    iget-object v0, p0, Landroid/support/v4/app/dh;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_24

    iget-object v0, p0, Landroid/support/v4/app/dh;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/dh;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/support/v4/app/fn;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/v4/app/fn;

    move-object v5, v0

    .line 1962
    :goto_15
    new-instance v0, Landroid/support/v4/app/df;

    iget v1, p0, Landroid/support/v4/app/dh;->b:I

    iget-object v2, p0, Landroid/support/v4/app/dh;->c:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/v4/app/dh;->d:Landroid/app/PendingIntent;

    iget-object v4, p0, Landroid/support/v4/app/dh;->a:Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/app/df;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;B)V

    return-object v0

    .line 1960
    :cond_24
    const/4 v5, 0x0

    goto :goto_15
.end method
