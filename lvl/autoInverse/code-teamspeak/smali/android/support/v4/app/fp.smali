.class public final Landroid/support/v4/app/fp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/CharSequence;

.field private c:[Ljava/lang/CharSequence;

.field private d:Z

.field private e:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/fp;->d:Z

    .line 99
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/fp;->e:Landroid/os/Bundle;

    .line 106
    if-nez p1, :cond_17

    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Result key can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_17
    iput-object p1, p0, Landroid/support/v4/app/fp;->a:Ljava/lang/String;

    .line 110
    return-void
.end method

.method private a()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, Landroid/support/v4/app/fp;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/app/fp;
    .registers 3

    .prologue
    .line 156
    if-eqz p1, :cond_7

    .line 157
    iget-object v0, p0, Landroid/support/v4/app/fp;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 159
    :cond_7
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/app/fp;
    .registers 2

    .prologue
    .line 118
    iput-object p1, p0, Landroid/support/v4/app/fp;->b:Ljava/lang/CharSequence;

    .line 119
    return-object p0
.end method

.method private a(Z)Landroid/support/v4/app/fp;
    .registers 2

    .prologue
    .line 144
    iput-boolean p1, p0, Landroid/support/v4/app/fp;->d:Z

    .line 145
    return-object p0
.end method

.method private a([Ljava/lang/CharSequence;)Landroid/support/v4/app/fp;
    .registers 2

    .prologue
    .line 130
    iput-object p1, p0, Landroid/support/v4/app/fp;->c:[Ljava/lang/CharSequence;

    .line 131
    return-object p0
.end method

.method private b()Landroid/support/v4/app/fn;
    .registers 7

    .prologue
    .line 176
    new-instance v0, Landroid/support/v4/app/fn;

    iget-object v1, p0, Landroid/support/v4/app/fp;->a:Ljava/lang/String;

    iget-object v2, p0, Landroid/support/v4/app/fp;->b:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/v4/app/fp;->c:[Ljava/lang/CharSequence;

    iget-boolean v4, p0, Landroid/support/v4/app/fp;->d:Z

    iget-object v5, p0, Landroid/support/v4/app/fp;->e:Landroid/os/Bundle;

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/fn;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)V

    return-object v0
.end method
