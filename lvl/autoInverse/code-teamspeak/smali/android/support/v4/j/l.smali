.class final Landroid/support/v4/j/l;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/os/CancellationSignal;

.field final synthetic b:Landroid/print/PrintAttributes;

.field final synthetic c:Landroid/print/PrintAttributes;

.field final synthetic d:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

.field final synthetic e:Landroid/support/v4/j/k;


# direct methods
.method constructor <init>(Landroid/support/v4/j/k;Landroid/os/CancellationSignal;Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;)V
    .registers 6

    .prologue
    .line 345
    iput-object p1, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iput-object p2, p0, Landroid/support/v4/j/l;->a:Landroid/os/CancellationSignal;

    iput-object p3, p0, Landroid/support/v4/j/l;->b:Landroid/print/PrintAttributes;

    iput-object p4, p0, Landroid/support/v4/j/l;->c:Landroid/print/PrintAttributes;

    iput-object p5, p0, Landroid/support/v4/j/l;->d:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .registers 3

    .prologue
    .line 363
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iget-object v0, v0, Landroid/support/v4/j/k;->g:Landroid/support/v4/j/i;

    iget-object v1, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iget-object v1, v1, Landroid/support/v4/j/k;->d:Landroid/net/Uri;

    .line 1051
    invoke-virtual {v0, v1}, Landroid/support/v4/j/i;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_b} :catch_d

    move-result-object v0

    .line 367
    :goto_c
    return-object v0

    :catch_d
    move-exception v0

    const/4 v0, 0x0

    goto :goto_c
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 372
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 373
    iget-object v1, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iput-object p1, v1, Landroid/support/v4/j/k;->b:Landroid/graphics/Bitmap;

    .line 374
    if-eqz p1, :cond_36

    .line 375
    new-instance v1, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v2, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iget-object v2, v2, Landroid/support/v4/j/k;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v1

    .line 379
    iget-object v2, p0, Landroid/support/v4/j/l;->b:Landroid/print/PrintAttributes;

    iget-object v3, p0, Landroid/support/v4/j/l;->c:Landroid/print/PrintAttributes;

    invoke-virtual {v2, v3}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_34

    .line 381
    :goto_2a
    iget-object v2, p0, Landroid/support/v4/j/l;->d:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v2, v1, v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 386
    :goto_2f
    iget-object v0, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iput-object v4, v0, Landroid/support/v4/j/k;->a:Landroid/os/AsyncTask;

    .line 387
    return-void

    .line 379
    :cond_34
    const/4 v0, 0x0

    goto :goto_2a

    .line 384
    :cond_36
    iget-object v0, p0, Landroid/support/v4/j/l;->d:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v0, v4}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    goto :goto_2f
.end method

.method private b()V
    .registers 3

    .prologue
    .line 392
    iget-object v0, p0, Landroid/support/v4/j/l;->d:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    .line 393
    iget-object v0, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v4/j/k;->a:Landroid/os/AsyncTask;

    .line 394
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 345
    invoke-direct {p0}, Landroid/support/v4/j/l;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onCancelled(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 345
    .line 1392
    iget-object v0, p0, Landroid/support/v4/j/l;->d:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutCancelled()V

    .line 1393
    iget-object v0, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v4/j/k;->a:Landroid/os/AsyncTask;

    .line 345
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 345
    check-cast p1, Landroid/graphics/Bitmap;

    .line 2372
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2373
    iget-object v1, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iput-object p1, v1, Landroid/support/v4/j/k;->b:Landroid/graphics/Bitmap;

    .line 2374
    if-eqz p1, :cond_38

    .line 2375
    new-instance v1, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v2, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iget-object v2, v2, Landroid/support/v4/j/k;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v1

    .line 2379
    iget-object v2, p0, Landroid/support/v4/j/l;->b:Landroid/print/PrintAttributes;

    iget-object v3, p0, Landroid/support/v4/j/l;->c:Landroid/print/PrintAttributes;

    invoke-virtual {v2, v3}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_36

    .line 2381
    :goto_2c
    iget-object v2, p0, Landroid/support/v4/j/l;->d:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v2, v1, v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 2386
    :goto_31
    iget-object v0, p0, Landroid/support/v4/j/l;->e:Landroid/support/v4/j/k;

    iput-object v4, v0, Landroid/support/v4/j/k;->a:Landroid/os/AsyncTask;

    .line 345
    return-void

    .line 2379
    :cond_36
    const/4 v0, 0x0

    goto :goto_2c

    .line 2384
    :cond_38
    iget-object v0, p0, Landroid/support/v4/j/l;->d:Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

    invoke-virtual {v0, v4}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    goto :goto_31
.end method

.method protected final onPreExecute()V
    .registers 3

    .prologue
    .line 350
    iget-object v0, p0, Landroid/support/v4/j/l;->a:Landroid/os/CancellationSignal;

    new-instance v1, Landroid/support/v4/j/m;

    invoke-direct {v1, p0}, Landroid/support/v4/j/m;-><init>(Landroid/support/v4/j/l;)V

    invoke-virtual {v0, v1}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 358
    return-void
.end method
