.class public final Landroid/support/v4/view/a/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2


# instance fields
.field final d:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467
    iput-object p1, p0, Landroid/support/v4/view/a/ae;->d:Ljava/lang/Object;

    .line 468
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;B)V
    .registers 3

    .prologue
    .line 456
    invoke-direct {p0, p1}, Landroid/support/v4/view/a/ae;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private a()F
    .registers 2

    .prologue
    .line 471
    iget-object v0, p0, Landroid/support/v4/view/a/ae;->d:Ljava/lang/Object;

    .line 1153
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;->getCurrent()F

    move-result v0

    .line 471
    return v0
.end method

.method private static synthetic a(Landroid/support/v4/view/a/ae;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 456
    iget-object v0, p0, Landroid/support/v4/view/a/ae;->d:Ljava/lang/Object;

    return-object v0
.end method

.method private b()F
    .registers 2

    .prologue
    .line 475
    iget-object v0, p0, Landroid/support/v4/view/a/ae;->d:Ljava/lang/Object;

    .line 1157
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;->getMax()F

    move-result v0

    .line 475
    return v0
.end method

.method private c()F
    .registers 2

    .prologue
    .line 479
    iget-object v0, p0, Landroid/support/v4/view/a/ae;->d:Ljava/lang/Object;

    .line 1161
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;->getMin()F

    move-result v0

    .line 479
    return v0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 483
    iget-object v0, p0, Landroid/support/v4/view/a/ae;->d:Ljava/lang/Object;

    .line 1165
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo$RangeInfo;->getType()I

    move-result v0

    .line 483
    return v0
.end method
