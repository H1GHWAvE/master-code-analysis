.class Landroid/support/v4/view/a/y;
.super Landroid/support/v4/view/a/x;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1524
    invoke-direct {p0}, Landroid/support/v4/view/a/x;-><init>()V

    return-void
.end method


# virtual methods
.method public final V(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1538
    .line 2033
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getLabelFor()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1538
    return-object v0
.end method

.method public final W(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1553
    .line 2045
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getLabeledBy()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1553
    return-object v0
.end method

.method public final g(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1528
    .line 2025
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLabelFor(Landroid/view/View;)V

    .line 1529
    return-void
.end method

.method public final g(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 1533
    .line 2029
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLabelFor(Landroid/view/View;I)V

    .line 1534
    return-void
.end method

.method public final h(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1543
    .line 2037
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLabeledBy(Landroid/view/View;)V

    .line 1544
    return-void
.end method

.method public final h(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 1548
    .line 2041
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLabeledBy(Landroid/view/View;I)V

    .line 1549
    return-void
.end method
