.class final Landroid/support/v4/view/a/u;
.super Landroid/support/v4/view/a/t;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1844
    invoke-direct {p0}, Landroid/support/v4/view/a/t;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1852
    .line 2032
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setTraversalBefore(Landroid/view/View;)V

    .line 1853
    return-void
.end method

.method public final b(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 1857
    .line 2036
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setTraversalBefore(Landroid/view/View;I)V

    .line 1858
    return-void
.end method

.method public final c(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1867
    .line 2044
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setTraversalAfter(Landroid/view/View;)V

    .line 1868
    return-void
.end method

.method public final c(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 1872
    .line 2048
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setTraversalAfter(Landroid/view/View;I)V

    .line 1873
    return-void
.end method

.method public final h(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1847
    .line 2028
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getTraversalBefore()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1847
    return-object v0
.end method

.method public final i(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1862
    .line 2040
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getTraversalAfter()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1862
    return-object v0
.end method
