.class public final Landroid/support/v4/view/fb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/view/fd;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 203
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 204
    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    .line 205
    new-instance v0, Landroid/support/v4/view/ff;

    invoke-direct {v0}, Landroid/support/v4/view/ff;-><init>()V

    sput-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    .line 213
    :goto_d
    return-void

    .line 206
    :cond_e
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1a

    .line 207
    new-instance v0, Landroid/support/v4/view/fe;

    invoke-direct {v0}, Landroid/support/v4/view/fe;-><init>()V

    sput-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    goto :goto_d

    .line 208
    :cond_1a
    const/16 v1, 0xe

    if-lt v0, v1, :cond_26

    .line 209
    new-instance v0, Landroid/support/v4/view/fc;

    invoke-direct {v0}, Landroid/support/v4/view/fc;-><init>()V

    sput-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    goto :goto_d

    .line 211
    :cond_26
    new-instance v0, Landroid/support/v4/view/fg;

    invoke-direct {v0}, Landroid/support/v4/view/fg;-><init>()V

    sput-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 304
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/fd;->a(Landroid/view/ViewParent;Landroid/view/View;)V

    .line 305
    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;IIII)V
    .registers 13

    .prologue
    .line 330
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/view/fd;->a(Landroid/view/ViewParent;Landroid/view/View;IIII)V

    .line 331
    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;II[I)V
    .registers 11

    .prologue
    .line 355
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/view/fd;->a(Landroid/view/ViewParent;Landroid/view/View;II[I)V

    .line 356
    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;FF)Z
    .registers 5

    .prologue
    .line 404
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/view/fd;->a(Landroid/view/ViewParent;Landroid/view/View;FF)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
    .registers 11

    .prologue
    .line 379
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/view/fd;->a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
    .registers 5

    .prologue
    .line 268
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/view/fd;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 4

    .prologue
    .line 241
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/fd;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 289
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/view/fd;->b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V

    .line 290
    return-void
.end method

.method private static c(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 424
    sget-object v0, Landroid/support/v4/view/fb;->a:Landroid/support/v4/view/fd;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/view/fd;->c(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V

    .line 425
    return-void
.end method
