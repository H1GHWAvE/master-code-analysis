.class final Landroid/support/v4/view/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/r;


# static fields
.field private static final e:I

.field private static final f:I

.field private static final g:I

.field private static final h:I = 0x1

.field private static final i:I = 0x2

.field private static final j:I = 0x3


# instance fields
.field private A:Landroid/view/VelocityTracker;

.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private final k:Landroid/os/Handler;

.field private final l:Landroid/view/GestureDetector$OnGestureListener;

.field private m:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Landroid/view/MotionEvent;

.field private t:Landroid/view/MotionEvent;

.field private u:Z

.field private v:F

.field private w:F

.field private x:F

.field private y:F

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 62
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Landroid/support/v4/view/s;->e:I

    .line 63
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Landroid/support/v4/view/s;->f:I

    .line 64
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Landroid/support/v4/view/s;->g:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
    .registers 7

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    new-instance v0, Landroid/support/v4/view/t;

    invoke-direct {v0, p0}, Landroid/support/v4/view/t;-><init>(Landroid/support/v4/view/s;)V

    iput-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    .line 158
    iput-object p2, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    .line 159
    instance-of v0, p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_14

    .line 160
    check-cast p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 1192
    iput-object p2, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 2166
    :cond_14
    if-nez p1, :cond_1e

    .line 2167
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2169
    :cond_1e
    iget-object v0, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    if-nez v0, :cond_2a

    .line 2170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "OnGestureListener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2172
    :cond_2a
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/s;->z:Z

    .line 2174
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 2175
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    .line 2176
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v2

    .line 2177
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/s;->c:I

    .line 2178
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/s;->d:I

    .line 2180
    mul-int v0, v1, v1

    iput v0, p0, Landroid/support/v4/view/s;->a:I

    .line 2181
    mul-int v0, v2, v2

    iput v0, p0, Landroid/support/v4/view/s;->b:I

    .line 163
    return-void
.end method

.method static synthetic a(Landroid/support/v4/view/s;)Landroid/view/MotionEvent;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .registers 6

    .prologue
    .line 166
    if-nez p1, :cond_a

    .line 167
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_a
    iget-object v0, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    if-nez v0, :cond_16

    .line 170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "OnGestureListener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_16
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/s;->z:Z

    .line 174
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    .line 176
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v2

    .line 177
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Landroid/support/v4/view/s;->c:I

    .line 178
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/s;->d:I

    .line 180
    mul-int v0, v1, v1

    iput v0, p0, Landroid/support/v4/view/s;->a:I

    .line 181
    mul-int v0, v2, v2

    iput v0, p0, Landroid/support/v4/view/s;->b:I

    .line 182
    return-void
.end method

.method private a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .registers 10

    .prologue
    const/4 v0, 0x0

    .line 439
    iget-boolean v1, p0, Landroid/support/v4/view/s;->r:Z

    if-nez v1, :cond_6

    .line 449
    :cond_5
    :goto_5
    return v0

    .line 443
    :cond_6
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget v1, Landroid/support/v4/view/s;->g:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gtz v1, :cond_5

    .line 447
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 448
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 449
    mul-int/2addr v1, v1

    mul-int/2addr v2, v2

    add-int/2addr v1, v2

    iget v2, p0, Landroid/support/v4/view/s;->b:I

    if-ge v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method static synthetic b(Landroid/support/v4/view/s;)Landroid/view/GestureDetector$OnGestureListener;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 409
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 410
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 411
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 412
    iget-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    .line 414
    iput-boolean v2, p0, Landroid/support/v4/view/s;->u:Z

    .line 415
    iput-boolean v2, p0, Landroid/support/v4/view/s;->n:Z

    .line 416
    iput-boolean v2, p0, Landroid/support/v4/view/s;->q:Z

    .line 417
    iput-boolean v2, p0, Landroid/support/v4/view/s;->r:Z

    .line 418
    iput-boolean v2, p0, Landroid/support/v4/view/s;->o:Z

    .line 419
    iget-boolean v0, p0, Landroid/support/v4/view/s;->p:Z

    if-eqz v0, :cond_2b

    .line 420
    iput-boolean v2, p0, Landroid/support/v4/view/s;->p:Z

    .line 422
    :cond_2b
    return-void
.end method

.method private c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 425
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 426
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 427
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 428
    iput-boolean v2, p0, Landroid/support/v4/view/s;->u:Z

    .line 429
    iput-boolean v2, p0, Landroid/support/v4/view/s;->q:Z

    .line 430
    iput-boolean v2, p0, Landroid/support/v4/view/s;->r:Z

    .line 431
    iput-boolean v2, p0, Landroid/support/v4/view/s;->o:Z

    .line 432
    iget-boolean v0, p0, Landroid/support/v4/view/s;->p:Z

    if-eqz v0, :cond_21

    .line 433
    iput-boolean v2, p0, Landroid/support/v4/view/s;->p:Z

    .line 435
    :cond_21
    return-void
.end method

.method static synthetic c(Landroid/support/v4/view/s;)V
    .registers 3

    .prologue
    .line 56
    .line 3453
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3454
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/s;->o:Z

    .line 3455
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/s;->p:Z

    .line 3456
    iget-object v0, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v1, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 56
    return-void
.end method

.method static synthetic d(Landroid/support/v4/view/s;)Landroid/view/GestureDetector$OnDoubleTapListener;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    return-object v0
.end method

.method private d()V
    .registers 3

    .prologue
    .line 453
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 454
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/view/s;->o:Z

    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/s;->p:Z

    .line 456
    iget-object v0, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v1, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 457
    return-void
.end method

.method static synthetic e(Landroid/support/v4/view/s;)Z
    .registers 2

    .prologue
    .line 56
    iget-boolean v0, p0, Landroid/support/v4/view/s;->n:Z

    return v0
.end method

.method static synthetic f(Landroid/support/v4/view/s;)Z
    .registers 2

    .prologue
    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/s;->o:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/GestureDetector$OnDoubleTapListener;)V
    .registers 2

    .prologue
    .line 192
    iput-object p1, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 193
    return-void
.end method

.method public final a(Z)V
    .registers 2

    .prologue
    .line 205
    iput-boolean p1, p0, Landroid/support/v4/view/s;->z:Z

    .line 206
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 212
    iget-boolean v0, p0, Landroid/support/v4/view/s;->z:Z

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .registers 15

    .prologue
    const/4 v7, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 224
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    .line 226
    iget-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    if-nez v0, :cond_13

    .line 227
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    .line 229
    :cond_13
    iget-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 231
    and-int/lit16 v0, v9, 0xff

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3c

    move v8, v4

    .line 233
    :goto_1e
    if-eqz v8, :cond_3e

    invoke-static {p1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 237
    :goto_24
    invoke-static {p1}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;)I

    move-result v5

    move v6, v3

    move v1, v7

    move v2, v7

    .line 238
    :goto_2b
    if-ge v6, v5, :cond_40

    .line 239
    if-eq v0, v6, :cond_39

    .line 240
    invoke-static {p1, v6}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v10

    add-float/2addr v2, v10

    .line 241
    invoke-static {p1, v6}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v10

    add-float/2addr v1, v10

    .line 238
    :cond_39
    add-int/lit8 v6, v6, 0x1

    goto :goto_2b

    :cond_3c
    move v8, v3

    .line 231
    goto :goto_1e

    .line 233
    :cond_3e
    const/4 v0, -0x1

    goto :goto_24

    .line 243
    :cond_40
    if-eqz v8, :cond_4e

    add-int/lit8 v0, v5, -0x1

    .line 244
    :goto_44
    int-to-float v6, v0

    div-float/2addr v2, v6

    .line 245
    int-to-float v0, v0

    div-float/2addr v1, v0

    .line 249
    and-int/lit16 v0, v9, 0xff

    packed-switch v0, :pswitch_data_2c2

    .line 405
    :cond_4d
    :goto_4d
    :pswitch_4d
    return v3

    :cond_4e
    move v0, v5

    .line 243
    goto :goto_44

    .line 251
    :pswitch_50
    iput v2, p0, Landroid/support/v4/view/s;->v:F

    iput v2, p0, Landroid/support/v4/view/s;->x:F

    .line 252
    iput v1, p0, Landroid/support/v4/view/s;->w:F

    iput v1, p0, Landroid/support/v4/view/s;->y:F

    .line 2425
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2426
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v0, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 2427
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 2428
    iput-boolean v3, p0, Landroid/support/v4/view/s;->u:Z

    .line 2429
    iput-boolean v3, p0, Landroid/support/v4/view/s;->q:Z

    .line 2430
    iput-boolean v3, p0, Landroid/support/v4/view/s;->r:Z

    .line 2431
    iput-boolean v3, p0, Landroid/support/v4/view/s;->o:Z

    .line 2432
    iget-boolean v0, p0, Landroid/support/v4/view/s;->p:Z

    if-eqz v0, :cond_4d

    .line 2433
    iput-boolean v3, p0, Landroid/support/v4/view/s;->p:Z

    goto :goto_4d

    .line 258
    :pswitch_76
    iput v2, p0, Landroid/support/v4/view/s;->v:F

    iput v2, p0, Landroid/support/v4/view/s;->x:F

    .line 259
    iput v1, p0, Landroid/support/v4/view/s;->w:F

    iput v1, p0, Landroid/support/v4/view/s;->y:F

    .line 263
    iget-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Landroid/support/v4/view/s;->d:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 264
    invoke-static {p1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;)I

    move-result v1

    .line 265
    invoke-static {p1, v1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 266
    iget-object v2, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-static {v2, v0}, Landroid/support/v4/view/cs;->a(Landroid/view/VelocityTracker;I)F

    move-result v2

    .line 267
    iget-object v4, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-static {v4, v0}, Landroid/support/v4/view/cs;->b(Landroid/view/VelocityTracker;I)F

    move-result v4

    move v0, v3

    .line 268
    :goto_9d
    if-ge v0, v5, :cond_4d

    .line 269
    if-eq v0, v1, :cond_be

    .line 271
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v6

    .line 272
    iget-object v8, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-static {v8, v6}, Landroid/support/v4/view/cs;->a(Landroid/view/VelocityTracker;I)F

    move-result v8

    mul-float/2addr v8, v2

    .line 273
    iget-object v9, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-static {v9, v6}, Landroid/support/v4/view/cs;->b(Landroid/view/VelocityTracker;I)F

    move-result v6

    mul-float/2addr v6, v4

    .line 275
    add-float/2addr v6, v8

    .line 276
    cmpg-float v6, v6, v7

    if-gez v6, :cond_be

    .line 277
    iget-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_4d

    .line 268
    :cond_be
    add-int/lit8 v0, v0, 0x1

    goto :goto_9d

    .line 284
    :pswitch_c1
    iget-object v0, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_187

    .line 285
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    .line 286
    if-eqz v0, :cond_d2

    iget-object v5, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v5, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 287
    :cond_d2
    iget-object v5, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    if-eqz v5, :cond_17f

    iget-object v5, p0, Landroid/support/v4/view/s;->t:Landroid/view/MotionEvent;

    if-eqz v5, :cond_17f

    if-eqz v0, :cond_17f

    iget-object v0, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    iget-object v5, p0, Landroid/support/v4/view/s;->t:Landroid/view/MotionEvent;

    .line 2439
    iget-boolean v6, p0, Landroid/support/v4/view/s;->r:Z

    if-eqz v6, :cond_17d

    .line 2443
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sget v5, Landroid/support/v4/view/s;->g:I

    int-to-long v8, v5

    cmp-long v5, v6, v8

    if-gtz v5, :cond_17d

    .line 2447
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    .line 2448
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v0, v6

    .line 2449
    mul-int/2addr v5, v5

    mul-int/2addr v0, v0

    add-int/2addr v0, v5

    iget v5, p0, Landroid/support/v4/view/s;->b:I

    if-ge v0, v5, :cond_17d

    move v0, v4

    .line 287
    :goto_112
    if-eqz v0, :cond_17f

    .line 290
    iput-boolean v4, p0, Landroid/support/v4/view/s;->u:Z

    .line 292
    iget-object v0, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    iget-object v5, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-interface {v0, v5}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 294
    iget-object v5, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v5, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    or-int/2addr v0, v5

    .line 301
    :goto_127
    iput v2, p0, Landroid/support/v4/view/s;->v:F

    iput v2, p0, Landroid/support/v4/view/s;->x:F

    .line 302
    iput v1, p0, Landroid/support/v4/view/s;->w:F

    iput v1, p0, Landroid/support/v4/view/s;->y:F

    .line 303
    iget-object v1, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    if-eqz v1, :cond_138

    .line 304
    iget-object v1, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 306
    :cond_138
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    .line 307
    iput-boolean v4, p0, Landroid/support/v4/view/s;->q:Z

    .line 308
    iput-boolean v4, p0, Landroid/support/v4/view/s;->r:Z

    .line 309
    iput-boolean v4, p0, Landroid/support/v4/view/s;->n:Z

    .line 310
    iput-boolean v3, p0, Landroid/support/v4/view/s;->p:Z

    .line 311
    iput-boolean v3, p0, Landroid/support/v4/view/s;->o:Z

    .line 313
    iget-boolean v1, p0, Landroid/support/v4/view/s;->z:Z

    if-eqz v1, :cond_164

    .line 314
    iget-object v1, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 315
    iget-object v1, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v5, Landroid/support/v4/view/s;->f:I

    int-to-long v6, v5

    add-long/2addr v2, v6

    sget v5, Landroid/support/v4/view/s;->e:I

    int-to-long v6, v5

    add-long/2addr v2, v6

    invoke-virtual {v1, v12, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 318
    :cond_164
    iget-object v1, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v5, Landroid/support/v4/view/s;->f:I

    int-to-long v6, v5

    add-long/2addr v2, v6

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 319
    iget-object v1, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v1, p1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int v3, v0, v1

    .line 320
    goto/16 :goto_4d

    :cond_17d
    move v0, v3

    .line 2449
    goto :goto_112

    .line 297
    :cond_17f
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    sget v5, Landroid/support/v4/view/s;->g:I

    int-to-long v6, v5

    invoke-virtual {v0, v11, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_187
    move v0, v3

    goto :goto_127

    .line 323
    :pswitch_189
    iget-boolean v0, p0, Landroid/support/v4/view/s;->p:Z

    if-nez v0, :cond_4d

    .line 326
    iget v0, p0, Landroid/support/v4/view/s;->v:F

    sub-float/2addr v0, v2

    .line 327
    iget v5, p0, Landroid/support/v4/view/s;->w:F

    sub-float/2addr v5, v1

    .line 328
    iget-boolean v6, p0, Landroid/support/v4/view/s;->u:Z

    if-eqz v6, :cond_1a1

    .line 330
    iget-object v0, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v3, v0, 0x0

    goto/16 :goto_4d

    .line 331
    :cond_1a1
    iget-boolean v6, p0, Landroid/support/v4/view/s;->q:Z

    if-eqz v6, :cond_1dc

    .line 332
    iget v6, p0, Landroid/support/v4/view/s;->x:F

    sub-float v6, v2, v6

    float-to-int v6, v6

    .line 333
    iget v7, p0, Landroid/support/v4/view/s;->y:F

    sub-float v7, v1, v7

    float-to-int v7, v7

    .line 334
    mul-int/2addr v6, v6

    mul-int/2addr v7, v7

    add-int/2addr v6, v7

    .line 335
    iget v7, p0, Landroid/support/v4/view/s;->a:I

    if-le v6, v7, :cond_2bf

    .line 336
    iget-object v7, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v8, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-interface {v7, v8, p1, v0, v5}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    .line 337
    iput v2, p0, Landroid/support/v4/view/s;->v:F

    .line 338
    iput v1, p0, Landroid/support/v4/view/s;->w:F

    .line 339
    iput-boolean v3, p0, Landroid/support/v4/view/s;->q:Z

    .line 340
    iget-object v1, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 341
    iget-object v1, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 342
    iget-object v1, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 344
    :goto_1d3
    iget v1, p0, Landroid/support/v4/view/s;->a:I

    if-le v6, v1, :cond_1d9

    .line 345
    iput-boolean v3, p0, Landroid/support/v4/view/s;->r:Z

    :cond_1d9
    move v3, v0

    .line 347
    goto/16 :goto_4d

    :cond_1dc
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v6

    if-gez v4, :cond_1f0

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v6

    if-ltz v4, :cond_4d

    .line 348
    :cond_1f0
    iget-object v3, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v4, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-interface {v3, v4, p1, v0, v5}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    .line 349
    iput v2, p0, Landroid/support/v4/view/s;->v:F

    .line 350
    iput v1, p0, Landroid/support/v4/view/s;->w:F

    goto/16 :goto_4d

    .line 355
    :pswitch_1fe
    iput-boolean v3, p0, Landroid/support/v4/view/s;->n:Z

    .line 356
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 357
    iget-boolean v0, p0, Landroid/support/v4/view/s;->u:Z

    if-eqz v0, :cond_238

    .line 359
    iget-object v0, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 383
    :cond_210
    :goto_210
    iget-object v2, p0, Landroid/support/v4/view/s;->t:Landroid/view/MotionEvent;

    if-eqz v2, :cond_219

    .line 384
    iget-object v2, p0, Landroid/support/v4/view/s;->t:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 387
    :cond_219
    iput-object v1, p0, Landroid/support/v4/view/s;->t:Landroid/view/MotionEvent;

    .line 388
    iget-object v1, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_227

    .line 391
    iget-object v1, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 392
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    .line 394
    :cond_227
    iput-boolean v3, p0, Landroid/support/v4/view/s;->u:Z

    .line 395
    iput-boolean v3, p0, Landroid/support/v4/view/s;->o:Z

    .line 396
    iget-object v1, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 397
    iget-object v1, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    move v3, v0

    .line 398
    goto/16 :goto_4d

    .line 360
    :cond_238
    iget-boolean v0, p0, Landroid/support/v4/view/s;->p:Z

    if-eqz v0, :cond_245

    .line 361
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 362
    iput-boolean v3, p0, Landroid/support/v4/view/s;->p:Z

    move v0, v3

    goto :goto_210

    .line 363
    :cond_245
    iget-boolean v0, p0, Landroid/support/v4/view/s;->q:Z

    if-eqz v0, :cond_25d

    .line 364
    iget-object v0, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 365
    iget-boolean v2, p0, Landroid/support/v4/view/s;->o:Z

    if-eqz v2, :cond_210

    iget-object v2, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v2, :cond_210

    .line 366
    iget-object v2, p0, Landroid/support/v4/view/s;->m:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v2, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_210

    .line 370
    :cond_25d
    iget-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    .line 371
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 372
    const/16 v5, 0x3e8

    iget v6, p0, Landroid/support/v4/view/s;->d:I

    int-to-float v6, v6

    invoke-virtual {v0, v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 373
    invoke-static {v0, v2}, Landroid/support/v4/view/cs;->b(Landroid/view/VelocityTracker;I)F

    move-result v5

    .line 375
    invoke-static {v0, v2}, Landroid/support/v4/view/cs;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    .line 378
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v6, p0, Landroid/support/v4/view/s;->c:I

    int-to-float v6, v6

    cmpl-float v2, v2, v6

    if-gtz v2, :cond_289

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v6, p0, Landroid/support/v4/view/s;->c:I

    int-to-float v6, v6

    cmpl-float v2, v2, v6

    if-lez v2, :cond_2bc

    .line 380
    :cond_289
    iget-object v2, p0, Landroid/support/v4/view/s;->l:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v6, p0, Landroid/support/v4/view/s;->s:Landroid/view/MotionEvent;

    invoke-interface {v2, v6, p1, v0, v5}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto/16 :goto_210

    .line 3409
    :pswitch_293
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 3410
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v0, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 3411
    iget-object v0, p0, Landroid/support/v4/view/s;->k:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 3412
    iget-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 3413
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/s;->A:Landroid/view/VelocityTracker;

    .line 3414
    iput-boolean v3, p0, Landroid/support/v4/view/s;->u:Z

    .line 3415
    iput-boolean v3, p0, Landroid/support/v4/view/s;->n:Z

    .line 3416
    iput-boolean v3, p0, Landroid/support/v4/view/s;->q:Z

    .line 3417
    iput-boolean v3, p0, Landroid/support/v4/view/s;->r:Z

    .line 3418
    iput-boolean v3, p0, Landroid/support/v4/view/s;->o:Z

    .line 3419
    iget-boolean v0, p0, Landroid/support/v4/view/s;->p:Z

    if-eqz v0, :cond_4d

    .line 3420
    iput-boolean v3, p0, Landroid/support/v4/view/s;->p:Z

    goto/16 :goto_4d

    :cond_2bc
    move v0, v3

    goto/16 :goto_210

    :cond_2bf
    move v0, v3

    goto/16 :goto_1d3

    .line 249
    :pswitch_data_2c2
    .packed-switch 0x0
        :pswitch_c1
        :pswitch_1fe
        :pswitch_189
        :pswitch_293
        :pswitch_4d
        :pswitch_50
        :pswitch_76
    .end packed-switch
.end method
