.class public final Landroid/support/v4/view/a/bd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Landroid/support/v4/view/a/bg;


# instance fields
.field public final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 506
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_e

    .line 507
    new-instance v0, Landroid/support/v4/view/a/bh;

    invoke-direct {v0}, Landroid/support/v4/view/a/bh;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    .line 515
    :goto_d
    return-void

    .line 508
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_1c

    .line 509
    new-instance v0, Landroid/support/v4/view/a/bf;

    invoke-direct {v0}, Landroid/support/v4/view/a/bf;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    goto :goto_d

    .line 510
    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2a

    .line 511
    new-instance v0, Landroid/support/v4/view/a/be;

    invoke-direct {v0}, Landroid/support/v4/view/a/be;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    goto :goto_d

    .line 513
    :cond_2a
    new-instance v0, Landroid/support/v4/view/a/bi;

    invoke-direct {v0}, Landroid/support/v4/view/a/bi;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    goto :goto_d
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529
    iput-object p1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    .line 530
    return-void
.end method

.method public static a()Landroid/support/v4/view/a/bd;
    .registers 2

    .prologue
    .line 560
    new-instance v0, Landroid/support/v4/view/a/bd;

    sget-object v1, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    invoke-interface {v1}, Landroid/support/v4/view/a/bg;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/a/bd;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Landroid/support/v4/view/a/bd;)Landroid/support/v4/view/a/bd;
    .registers 4

    .prologue
    .line 550
    new-instance v0, Landroid/support/v4/view/a/bd;

    sget-object v1, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v2, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v1, v2}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/a/bd;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 734
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->d(Ljava/lang/Object;I)V

    .line 735
    return-void
.end method

.method private a(Landroid/os/Parcelable;)V
    .registers 4

    .prologue
    .line 1001
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;Landroid/os/Parcelable;)V

    .line 1002
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 571
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 572
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 589
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;Landroid/view/View;I)V

    .line 590
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 931
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->b(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 932
    return-void
.end method

.method private b()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 539
    iget-object v0, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    return-object v0
.end method

.method private b(I)V
    .registers 4

    .prologue
    .line 754
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->b(Ljava/lang/Object;I)V

    .line 755
    return-void
.end method

.method private b(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 961
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 962
    return-void
.end method

.method private b(Z)V
    .registers 4

    .prologue
    .line 634
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;Z)V

    .line 635
    return-void
.end method

.method private c()Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 605
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->m(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private c(I)V
    .registers 4

    .prologue
    .line 780
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->c(Ljava/lang/Object;I)V

    .line 781
    return-void
.end method

.method private c(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 981
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->c(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 982
    return-void
.end method

.method private c(Z)V
    .registers 4

    .prologue
    .line 654
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->b(Ljava/lang/Object;Z)V

    .line 655
    return-void
.end method

.method private d()I
    .registers 3

    .prologue
    .line 614
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->p(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private d(I)V
    .registers 4

    .prologue
    .line 800
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->h(Ljava/lang/Object;I)V

    .line 801
    return-void
.end method

.method private d(Z)V
    .registers 4

    .prologue
    .line 674
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->d(Ljava/lang/Object;Z)V

    .line 675
    return-void
.end method

.method private e(I)V
    .registers 4

    .prologue
    .line 818
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->f(Ljava/lang/Object;I)V

    .line 819
    return-void
.end method

.method private e(Z)V
    .registers 4

    .prologue
    .line 694
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->c(Ljava/lang/Object;Z)V

    .line 695
    return-void
.end method

.method private e()Z
    .registers 3

    .prologue
    .line 623
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->q(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private f(I)V
    .registers 4

    .prologue
    .line 836
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->g(Ljava/lang/Object;I)V

    .line 837
    return-void
.end method

.method private f()Z
    .registers 3

    .prologue
    .line 643
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->r(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private g(I)V
    .registers 4

    .prologue
    .line 853
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->i(Ljava/lang/Object;I)V

    .line 854
    return-void
.end method

.method private g()Z
    .registers 3

    .prologue
    .line 663
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->t(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private h(I)V
    .registers 4

    .prologue
    .line 871
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->j(Ljava/lang/Object;I)V

    .line 872
    return-void
.end method

.method private h()Z
    .registers 3

    .prologue
    .line 683
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->s(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private i(I)V
    .registers 4

    .prologue
    .line 891
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;I)V

    .line 892
    return-void
.end method

.method private i()Z
    .registers 3

    .prologue
    .line 703
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->u(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private j()I
    .registers 3

    .prologue
    .line 723
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->h(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private j(I)V
    .registers 4

    .prologue
    .line 911
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->e(Ljava/lang/Object;I)V

    .line 912
    return-void
.end method

.method private k()I
    .registers 3

    .prologue
    .line 743
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->f(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private l()I
    .registers 3

    .prologue
    .line 766
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->g(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private m()I
    .registers 3

    .prologue
    .line 790
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->o(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private n()I
    .registers 3

    .prologue
    .line 809
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->k(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private o()I
    .registers 3

    .prologue
    .line 827
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->l(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private p()I
    .registers 3

    .prologue
    .line 845
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->w(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private q()I
    .registers 3

    .prologue
    .line 862
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->x(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private r()I
    .registers 3

    .prologue
    .line 880
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private s()I
    .registers 3

    .prologue
    .line 900
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->j(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private t()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 920
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->d(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private u()Ljava/util/List;
    .registers 3

    .prologue
    .line 941
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->n(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private v()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 950
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->c(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private w()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 970
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->e(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private x()Landroid/os/Parcelable;
    .registers 3

    .prologue
    .line 990
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->i(Ljava/lang/Object;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method private y()V
    .registers 3

    .prologue
    .line 1014
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bg;->v(Ljava/lang/Object;)V

    .line 1015
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .registers 4

    .prologue
    .line 714
    sget-object v0, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bg;->e(Ljava/lang/Object;Z)V

    .line 715
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1025
    if-ne p0, p1, :cond_5

    .line 1042
    :cond_4
    :goto_4
    return v0

    .line 1028
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 1029
    goto :goto_4

    .line 1031
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 1032
    goto :goto_4

    .line 1034
    :cond_15
    check-cast p1, Landroid/support/v4/view/a/bd;

    .line 1035
    iget-object v2, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    if-nez v2, :cond_21

    .line 1036
    iget-object v2, p1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1037
    goto :goto_4

    .line 1039
    :cond_21
    iget-object v2, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    iget-object v3, p1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1040
    goto :goto_4
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 1019
    iget-object v0, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_5
.end method
