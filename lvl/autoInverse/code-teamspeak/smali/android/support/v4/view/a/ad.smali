.class public final Landroid/support/v4/view/a/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 428
    iput-object p1, p0, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    .line 429
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;B)V
    .registers 3

    .prologue
    .line 412
    invoke-direct {p0, p1}, Landroid/support/v4/view/a/ad;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private a()I
    .registers 3

    .prologue
    .line 432
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->O(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static a(IIIIZZ)Landroid/support/v4/view/a/ad;
    .registers 14

    .prologue
    .line 423
    new-instance v7, Landroid/support/v4/view/a/ad;

    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/view/a/w;->a(IIIIZZ)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/support/v4/view/a/ad;-><init>(Ljava/lang/Object;)V

    return-object v7
.end method

.method private static synthetic a(Landroid/support/v4/view/a/ad;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 412
    iget-object v0, p0, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private b()I
    .registers 3

    .prologue
    .line 436
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->P(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private c()I
    .registers 3

    .prologue
    .line 440
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->Q(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private d()I
    .registers 3

    .prologue
    .line 444
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->R(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private e()Z
    .registers 3

    .prologue
    .line 448
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->S(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private f()Z
    .registers 3

    .prologue
    .line 452
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
