.class public Landroid/support/v4/view/a/aq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Landroid/support/v4/view/a/ar;


# instance fields
.field public final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 139
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_e

    .line 140
    new-instance v0, Landroid/support/v4/view/a/au;

    invoke-direct {v0}, Landroid/support/v4/view/a/au;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/aq;->b:Landroid/support/v4/view/a/ar;

    .line 146
    :goto_d
    return-void

    .line 141
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1c

    .line 142
    new-instance v0, Landroid/support/v4/view/a/as;

    invoke-direct {v0}, Landroid/support/v4/view/a/as;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/aq;->b:Landroid/support/v4/view/a/ar;

    goto :goto_d

    .line 144
    :cond_1c
    new-instance v0, Landroid/support/v4/view/a/aw;

    invoke-direct {v0}, Landroid/support/v4/view/a/aw;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/aq;->b:Landroid/support/v4/view/a/ar;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    sget-object v0, Landroid/support/v4/view/a/aq;->b:Landroid/support/v4/view/a/ar;

    invoke-interface {v0, p0}, Landroid/support/v4/view/a/ar;->a(Landroid/support/v4/view/a/aq;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/a/aq;->a:Ljava/lang/Object;

    .line 153
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    iput-object p1, p0, Landroid/support/v4/view/a/aq;->a:Ljava/lang/Object;

    .line 163
    return-void
.end method

.method public static a()Ljava/util/List;
    .registers 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return-object v0
.end method

.method public static b()Landroid/support/v4/view/a/q;
    .registers 1

    .prologue
    .line 246
    const/4 v0, 0x0

    return-object v0
.end method

.method private c()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 169
    iget-object v0, p0, Landroid/support/v4/view/a/aq;->a:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 195
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(IILandroid/os/Bundle;)Z
    .registers 5

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method
