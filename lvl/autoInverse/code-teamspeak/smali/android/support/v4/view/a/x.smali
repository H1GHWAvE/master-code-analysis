.class Landroid/support/v4/view/a/x;
.super Landroid/support/v4/view/a/v;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1457
    invoke-direct {p0}, Landroid/support/v4/view/a/v;-><init>()V

    return-void
.end method


# virtual methods
.method public final D(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1515
    .line 4053
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getMovementGranularities()I

    move-result v0

    .line 1515
    return v0
.end method

.method public final E(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1485
    .line 3037
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isVisibleToUser()Z

    move-result v0

    .line 1485
    return v0
.end method

.method public final F(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1495
    .line 3073
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isAccessibilityFocused()Z

    move-result v0

    .line 1495
    return v0
.end method

.method public final a(Landroid/view/View;I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1460
    .line 2057
    invoke-static {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1460
    return-object v0
.end method

.method public final a(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .registers 5

    .prologue
    .line 1505
    .line 4045
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->performAction(ILandroid/os/Bundle;)Z

    move-result v0

    .line 1505
    return v0
.end method

.method public final d(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 1480
    .line 3033
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;I)V

    .line 1481
    return-void
.end method

.method public final e(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1465
    .line 2061
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->findFocus(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1465
    return-object v0
.end method

.method public final e(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 1475
    .line 3029
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 1476
    return-void
.end method

.method public final f(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1470
    .line 2065
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->focusSearch(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1470
    return-object v0
.end method

.method public final f(Ljava/lang/Object;Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 1520
    .line 4069
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;I)V

    .line 1521
    return-void
.end method

.method public final g(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 1510
    .line 4049
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setMovementGranularities(I)V

    .line 1511
    return-void
.end method

.method public final k(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1490
    .line 3041
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setVisibleToUser(Z)V

    .line 1491
    return-void
.end method

.method public final l(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1500
    .line 3077
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setAccessibilityFocused(Z)V

    .line 1501
    return-void
.end method
