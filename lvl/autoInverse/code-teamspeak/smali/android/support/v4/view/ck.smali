.class public final Landroid/support/v4/view/ck;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/view/co;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 59
    const/16 v1, 0x13

    if-lt v0, v1, :cond_f

    .line 60
    new-instance v0, Landroid/support/v4/view/cn;

    invoke-direct {v0, v2}, Landroid/support/v4/view/cn;-><init>(B)V

    sput-object v0, Landroid/support/v4/view/ck;->a:Landroid/support/v4/view/co;

    .line 64
    :goto_e
    return-void

    .line 62
    :cond_f
    new-instance v0, Landroid/support/v4/view/cm;

    invoke-direct {v0, v2}, Landroid/support/v4/view/cm;-><init>(B)V

    sput-object v0, Landroid/support/v4/view/ck;->a:Landroid/support/v4/view/co;

    goto :goto_e
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 75
    sget-object v0, Landroid/support/v4/view/ck;->a:Landroid/support/v4/view/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/co;->a(Ljava/lang/Object;Z)V

    .line 76
    return-void
.end method

.method private static a(Ljava/lang/Object;)Z
    .registers 2

    .prologue
    .line 83
    sget-object v0, Landroid/support/v4/view/ck;->a:Landroid/support/v4/view/co;

    invoke-interface {v0, p0}, Landroid/support/v4/view/co;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
