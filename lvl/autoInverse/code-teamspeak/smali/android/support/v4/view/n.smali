.class public abstract Landroid/support/v4/view/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/lang/String; = "ActionProvider(support)"


# instance fields
.field public a:Landroid/support/v4/view/o;

.field public b:Landroid/support/v4/view/p;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Landroid/support/v4/view/n;->d:Landroid/content/Context;

    .line 80
    return-void
.end method

.method private a(Landroid/support/v4/view/o;)V
    .registers 2

    .prologue
    .line 226
    iput-object p1, p0, Landroid/support/v4/view/n;->a:Landroid/support/v4/view/o;

    .line 227
    return-void
.end method

.method private g()Landroid/content/Context;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v4/view/n;->d:Landroid/content/Context;

    return-object v0
.end method

.method private h()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 248
    iput-object v0, p0, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/p;

    .line 249
    iput-object v0, p0, Landroid/support/v4/view/n;->a:Landroid/support/v4/view/o;

    .line 250
    return-void
.end method


# virtual methods
.method public abstract a()Landroid/view/View;
.end method

.method public a(Landroid/view/MenuItem;)Landroid/view/View;
    .registers 3

    .prologue
    .line 109
    invoke-virtual {p0}, Landroid/support/v4/view/n;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/support/v4/view/p;)V
    .registers 5

    .prologue
    .line 236
    iget-object v0, p0, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/p;

    if-eqz v0, :cond_26

    .line 237
    const-string v0, "ActionProvider(support)"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " instance while it is still in use somewhere else?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_26
    iput-object p1, p0, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/p;

    .line 242
    return-void
.end method

.method public a(Landroid/view/SubMenu;)V
    .registers 2

    .prologue
    .line 208
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 217
    iget-object v0, p0, Landroid/support/v4/view/n;->a:Landroid/support/v4/view/o;

    if-eqz v0, :cond_9

    .line 218
    iget-object v0, p0, Landroid/support/v4/view/n;->a:Landroid/support/v4/view/o;

    invoke-interface {v0, p1}, Landroid/support/v4/view/o;->b(Z)V

    .line 220
    :cond_9
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 135
    const/4 v0, 0x1

    return v0
.end method

.method public d()V
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/p;

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Landroid/support/v4/view/n;->b()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 146
    iget-object v0, p0, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/p;

    invoke-virtual {p0}, Landroid/support/v4/view/n;->c()Z

    invoke-interface {v0}, Landroid/support/v4/view/p;->a()V

    .line 148
    :cond_12
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method
