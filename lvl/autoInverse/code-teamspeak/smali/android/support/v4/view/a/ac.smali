.class public final Landroid/support/v4/view/a/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2


# instance fields
.field final d:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    iput-object p1, p0, Landroid/support/v4/view/a/ac;->d:Ljava/lang/Object;

    .line 397
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;B)V
    .registers 3

    .prologue
    .line 372
    invoke-direct {p0, p1}, Landroid/support/v4/view/a/ac;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private a()I
    .registers 3

    .prologue
    .line 400
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ac;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->L(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static a(IIZI)Landroid/support/v4/view/a/ac;
    .registers 6

    .prologue
    .line 391
    new-instance v0, Landroid/support/v4/view/a/ac;

    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v1

    invoke-interface {v1, p0, p1, p2, p3}, Landroid/support/v4/view/a/w;->a(IIZI)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/a/ac;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private b()I
    .registers 3

    .prologue
    .line 404
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ac;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->M(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private c()Z
    .registers 3

    .prologue
    .line 408
    invoke-static {}, Landroid/support/v4/view/a/q;->p()Landroid/support/v4/view/a/w;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/view/a/ac;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->N(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
