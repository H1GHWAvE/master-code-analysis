.class final Landroid/support/v4/k/f;
.super Landroid/support/v4/k/a;
.source "SourceFile"


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/net/Uri;


# direct methods
.method constructor <init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V
    .registers 4

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/support/v4/k/a;-><init>(Landroid/support/v4/k/a;)V

    .line 28
    iput-object p2, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    .line 29
    iput-object p3, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v4/k/a;
    .registers 5

    .prologue
    .line 40
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    .line 1038
    const-string v2, "vnd.android.document/directory"

    invoke-static {v0, v1, v2, p1}, Landroid/support/v4/k/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 41
    if-eqz v1, :cond_14

    new-instance v0, Landroid/support/v4/k/f;

    iget-object v2, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    invoke-direct {v0, p0, v2, v1}, Landroid/support/v4/k/f;-><init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V

    :goto_13
    return-object v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/k/a;
    .registers 6

    .prologue
    .line 34
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1, p1, p2}, Landroid/support/v4/k/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_12

    new-instance v0, Landroid/support/v4/k/f;

    iget-object v2, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    invoke-direct {v0, p0, v2, v1}, Landroid/support/v4/k/f;-><init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V

    :goto_11
    return-object v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final b()Ljava/lang/String;
    .registers 4

    .prologue
    .line 51
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    .line 2037
    const-string v2, "_display_name"

    invoke-static {v0, v1, v2}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method public final b(Ljava/lang/String;)Z
    .registers 4

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    .line 2072
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1, p1}, Landroid/provider/DocumentsContract;->renameDocument(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_12

    .line 113
    iput-object v0, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    .line 114
    const/4 v0, 0x1

    .line 116
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .registers 3

    .prologue
    .line 61
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .registers 3

    .prologue
    .line 66
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->c(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final f()J
    .registers 4

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    .line 2067
    const-string v2, "last_modified"

    invoke-static {v0, v1, v2}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    .line 71
    return-wide v0
.end method

.method public final g()J
    .registers 4

    .prologue
    .line 76
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    .line 2071
    const-string v2, "_size"

    invoke-static {v0, v1, v2}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    .line 76
    return-wide v0
.end method

.method public final h()Z
    .registers 3

    .prologue
    .line 81
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->d(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .registers 3

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->e(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .registers 3

    .prologue
    .line 91
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->f(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .registers 3

    .prologue
    .line 96
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->g(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final l()[Landroid/support/v4/k/a;
    .registers 7

    .prologue
    .line 101
    iget-object v0, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/f;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/c;->a(Landroid/content/Context;Landroid/net/Uri;)[Landroid/net/Uri;

    move-result-object v1

    .line 102
    array-length v0, v1

    new-array v2, v0, [Landroid/support/v4/k/a;

    .line 103
    const/4 v0, 0x0

    :goto_c
    array-length v3, v1

    if-ge v0, v3, :cond_1d

    .line 104
    new-instance v3, Landroid/support/v4/k/f;

    iget-object v4, p0, Landroid/support/v4/k/f;->b:Landroid/content/Context;

    aget-object v5, v1, v0

    invoke-direct {v3, p0, v4, v5}, Landroid/support/v4/k/f;-><init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V

    aput-object v3, v2, v0

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 106
    :cond_1d
    return-object v2
.end method
