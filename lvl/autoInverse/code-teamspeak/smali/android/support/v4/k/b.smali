.class final Landroid/support/v4/k/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "DocumentFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 45
    .line 1041
    const-string v0, "mime_type"

    invoke-static {p0, p1, v0}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    const-string v1, "vnd.android.document/directory"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 47
    const/4 v0, 0x0

    .line 49
    :cond_f
    return-object v0
.end method

.method static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .registers 10

    .prologue
    const/4 v6, 0x0

    .line 144
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 148
    const/4 v1, 0x1

    :try_start_6
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_12} :catch_2e
    .catchall {:try_start_6 .. :try_end_12} :catchall_49

    move-result-object v1

    .line 149
    :try_start_13
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_29

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_29

    .line 150
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_24} :catch_51
    .catchall {:try_start_13 .. :try_end_24} :catchall_4f

    move-result-object v0

    .line 158
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    :goto_28
    return-object v0

    :cond_29
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    move-object v0, v6

    goto :goto_28

    .line 154
    :catch_2e
    move-exception v0

    move-object v1, v6

    .line 155
    :goto_30
    :try_start_30
    const-string v2, "DocumentFile"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed query: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_44
    .catchall {:try_start_30 .. :try_end_44} :catchall_4f

    .line 158
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    move-object v0, v6

    goto :goto_28

    :catchall_49
    move-exception v0

    move-object v1, v6

    :goto_4b
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    throw v0

    :catchall_4f
    move-exception v0

    goto :goto_4b

    .line 154
    :catch_51
    move-exception v0

    goto :goto_30
.end method

.method private static a(Ljava/lang/AutoCloseable;)V
    .registers 2

    .prologue
    .line 188
    if-eqz p0, :cond_5

    .line 190
    :try_start_2
    invoke-interface {p0}, Ljava/lang/AutoCloseable;->close()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_5} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_8

    .line 196
    :cond_5
    :goto_5
    return-void

    .line 192
    :catch_6
    move-exception v0

    throw v0

    :catch_8
    move-exception v0

    goto :goto_5
.end method

.method static b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
    .registers 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    .line 169
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 173
    const/4 v1, 0x1

    :try_start_8
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_14} :catch_30
    .catchall {:try_start_8 .. :try_end_14} :catchall_4b

    move-result-object v2

    .line 174
    :try_start_15
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2b

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 175
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_26} :catch_56
    .catchall {:try_start_15 .. :try_end_26} :catchall_51

    move-result-wide v0

    .line 183
    invoke-static {v2}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    :goto_2a
    return-wide v0

    :cond_2b
    invoke-static {v2}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    move-wide v0, v6

    goto :goto_2a

    .line 179
    :catch_30
    move-exception v0

    move-object v1, v8

    .line 180
    :goto_32
    :try_start_32
    const-string v2, "DocumentFile"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed query: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_46
    .catchall {:try_start_32 .. :try_end_46} :catchall_54

    .line 183
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    move-wide v0, v6

    goto :goto_2a

    :catchall_4b
    move-exception v0

    move-object v1, v8

    :goto_4d
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    throw v0

    :catchall_51
    move-exception v0

    move-object v1, v2

    goto :goto_4d

    :catchall_54
    move-exception v0

    goto :goto_4d

    .line 179
    :catch_56
    move-exception v0

    move-object v1, v2

    goto :goto_32
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 4

    .prologue
    .line 54
    const-string v0, "vnd.android.document/directory"

    .line 2041
    const-string v1, "mime_type"

    invoke-static {p0, p1, v1}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I
    .registers 5

    .prologue
    .line 164
    invoke-static {p0, p1, p2}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static c(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 4

    .prologue
    .line 58
    .line 3041
    const-string v0, "mime_type"

    invoke-static {p0, p1, v0}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    const-string v1, "vnd.android.document/directory"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 60
    :cond_14
    const/4 v0, 0x0

    .line 62
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x1

    goto :goto_15
.end method

.method public static d(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 76
    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->checkCallingOrSelfUriPermission(Landroid/net/Uri;I)I

    move-result v2

    if-eqz v2, :cond_9

    .line 86
    :cond_8
    :goto_8
    return v0

    .line 4041
    :cond_9
    const-string v2, "mime_type"

    invoke-static {p0, p1, v2}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 86
    goto :goto_8
.end method

.method public static e(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 91
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Landroid/content/Context;->checkCallingOrSelfUriPermission(Landroid/net/Uri;I)I

    move-result v2

    if-eqz v2, :cond_a

    .line 119
    :cond_9
    :goto_9
    return v0

    .line 5041
    :cond_a
    const-string v2, "mime_type"

    invoke-static {p0, p1, v2}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    const-string v3, "flags"

    .line 5164
    invoke-static {p0, p1, v3}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v4

    long-to-int v3, v4

    .line 100
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 105
    and-int/lit8 v4, v3, 0x4

    if-eqz v4, :cond_23

    move v0, v1

    .line 106
    goto :goto_9

    .line 109
    :cond_23
    const-string v4, "vnd.android.document/directory"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_31

    and-int/lit8 v4, v3, 0x8

    if-eqz v4, :cond_31

    move v0, v1

    .line 112
    goto :goto_9

    .line 113
    :cond_31
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    and-int/lit8 v2, v3, 0x2

    if-eqz v2, :cond_9

    move v0, v1

    .line 116
    goto :goto_9
.end method

.method public static f(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 3

    .prologue
    .line 123
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/provider/DocumentsContract;->deleteDocument(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public static g(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 11

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 127
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 131
    const/4 v1, 0x1

    :try_start_8
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "document_id"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_16} :catch_24
    .catchall {:try_start_8 .. :try_end_16} :catchall_3f

    move-result-object v1

    .line 133
    :try_start_17
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_1a} :catch_47
    .catchall {:try_start_17 .. :try_end_1a} :catchall_45

    move-result v0

    if-lez v0, :cond_22

    move v0, v6

    .line 138
    :goto_1e
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    :goto_21
    return v0

    :cond_22
    move v0, v7

    .line 133
    goto :goto_1e

    .line 134
    :catch_24
    move-exception v0

    move-object v1, v8

    .line 135
    :goto_26
    :try_start_26
    const-string v2, "DocumentFile"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed query: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3a
    .catchall {:try_start_26 .. :try_end_3a} :catchall_45

    .line 138
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    move v0, v7

    goto :goto_21

    :catchall_3f
    move-exception v0

    move-object v1, v8

    :goto_41
    invoke-static {v1}, Landroid/support/v4/k/b;->a(Ljava/lang/AutoCloseable;)V

    throw v0

    :catchall_45
    move-exception v0

    goto :goto_41

    .line 134
    :catch_47
    move-exception v0

    goto :goto_26
.end method

.method private static h(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 3

    .prologue
    .line 33
    invoke-static {p0, p1}, Landroid/provider/DocumentsContract;->isDocumentUri(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method private static i(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 37
    const-string v0, "_display_name"

    invoke-static {p0, p1, v0}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static j(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 41
    const-string v0, "mime_type"

    invoke-static {p0, p1, v0}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static k(Landroid/content/Context;Landroid/net/Uri;)J
    .registers 4

    .prologue
    .line 67
    const-string v0, "last_modified"

    invoke-static {p0, p1, v0}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static l(Landroid/content/Context;Landroid/net/Uri;)J
    .registers 4

    .prologue
    .line 71
    const-string v0, "_size"

    invoke-static {p0, p1, v0}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method
