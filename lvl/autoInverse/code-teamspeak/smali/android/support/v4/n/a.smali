.class public Landroid/support/v4/n/a;
.super Landroid/support/v4/n/v;
.source "SourceFile"

# interfaces
.implements Ljava/util/Map;


# instance fields
.field a:Landroid/support/v4/n/k;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/support/v4/n/v;-><init>()V

    .line 55
    return-void
.end method

.method private constructor <init>(I)V
    .registers 2

    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/support/v4/n/v;-><init>(I)V

    .line 62
    return-void
.end method

.method private constructor <init>(Landroid/support/v4/n/v;)V
    .registers 2

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/support/v4/n/v;-><init>(Landroid/support/v4/n/v;)V

    .line 69
    return-void
.end method

.method private a()Landroid/support/v4/n/k;
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Landroid/support/v4/n/a;->a:Landroid/support/v4/n/k;

    if-nez v0, :cond_b

    .line 73
    new-instance v0, Landroid/support/v4/n/b;

    invoke-direct {v0, p0}, Landroid/support/v4/n/b;-><init>(Landroid/support/v4/n/a;)V

    iput-object v0, p0, Landroid/support/v4/n/a;->a:Landroid/support/v4/n/k;

    .line 120
    :cond_b
    iget-object v0, p0, Landroid/support/v4/n/a;->a:Landroid/support/v4/n/k;

    return-object v0
.end method

.method private a(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 130
    invoke-static {p0, p1}, Landroid/support/v4/n/k;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 151
    invoke-static {p0, p1}, Landroid/support/v4/n/k;->b(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method private c(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 161
    invoke-static {p0, p1}, Landroid/support/v4/n/k;->c(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public entrySet()Ljava/util/Set;
    .registers 3

    .prologue
    .line 179
    invoke-direct {p0}, Landroid/support/v4/n/a;->a()Landroid/support/v4/n/k;

    move-result-object v0

    .line 1529
    iget-object v1, v0, Landroid/support/v4/n/k;->b:Landroid/support/v4/n/m;

    if-nez v1, :cond_f

    .line 1530
    new-instance v1, Landroid/support/v4/n/m;

    invoke-direct {v1, v0}, Landroid/support/v4/n/m;-><init>(Landroid/support/v4/n/k;)V

    iput-object v1, v0, Landroid/support/v4/n/k;->b:Landroid/support/v4/n/m;

    .line 1532
    :cond_f
    iget-object v0, v0, Landroid/support/v4/n/k;->b:Landroid/support/v4/n/m;

    .line 179
    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .registers 3

    .prologue
    .line 191
    invoke-direct {p0}, Landroid/support/v4/n/a;->a()Landroid/support/v4/n/k;

    move-result-object v0

    .line 1536
    iget-object v1, v0, Landroid/support/v4/n/k;->c:Landroid/support/v4/n/n;

    if-nez v1, :cond_f

    .line 1537
    new-instance v1, Landroid/support/v4/n/n;

    invoke-direct {v1, v0}, Landroid/support/v4/n/n;-><init>(Landroid/support/v4/n/k;)V

    iput-object v1, v0, Landroid/support/v4/n/k;->c:Landroid/support/v4/n/n;

    .line 1539
    :cond_f
    iget-object v0, v0, Landroid/support/v4/n/k;->c:Landroid/support/v4/n/n;

    .line 191
    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .registers 5

    .prologue
    .line 139
    iget v0, p0, Landroid/support/v4/n/a;->h:I

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/n/a;->a(I)V

    .line 140
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 141
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_12

    .line 143
    :cond_2a
    return-void
.end method

.method public values()Ljava/util/Collection;
    .registers 3

    .prologue
    .line 203
    invoke-direct {p0}, Landroid/support/v4/n/a;->a()Landroid/support/v4/n/k;

    move-result-object v0

    .line 1543
    iget-object v1, v0, Landroid/support/v4/n/k;->d:Landroid/support/v4/n/p;

    if-nez v1, :cond_f

    .line 1544
    new-instance v1, Landroid/support/v4/n/p;

    invoke-direct {v1, v0}, Landroid/support/v4/n/p;-><init>(Landroid/support/v4/n/k;)V

    iput-object v1, v0, Landroid/support/v4/n/k;->d:Landroid/support/v4/n/p;

    .line 1546
    :cond_f
    iget-object v0, v0, Landroid/support/v4/n/k;->d:Landroid/support/v4/n/p;

    .line 203
    return-object v0
.end method
