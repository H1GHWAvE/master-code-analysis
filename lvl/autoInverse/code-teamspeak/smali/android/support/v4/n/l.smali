.class final Landroid/support/v4/n/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:I

.field b:I

.field c:I

.field d:Z

.field final synthetic e:Landroid/support/v4/n/k;


# direct methods
.method constructor <init>(Landroid/support/v4/n/k;I)V
    .registers 4

    .prologue
    .line 41
    iput-object p1, p0, Landroid/support/v4/n/l;->e:Landroid/support/v4/n/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/n/l;->d:Z

    .line 42
    iput p2, p0, Landroid/support/v4/n/l;->a:I

    .line 43
    invoke-virtual {p1}, Landroid/support/v4/n/k;->a()I

    move-result v0

    iput v0, p0, Landroid/support/v4/n/l;->b:I

    .line 44
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 3

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v4/n/l;->c:I

    iget v1, p0, Landroid/support/v4/n/l;->b:I

    if-ge v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v4/n/l;->e:Landroid/support/v4/n/k;

    iget v1, p0, Landroid/support/v4/n/l;->c:I

    iget v2, p0, Landroid/support/v4/n/l;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/k;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 54
    iget v1, p0, Landroid/support/v4/n/l;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroid/support/v4/n/l;->c:I

    .line 55
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/n/l;->d:Z

    .line 56
    return-object v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 61
    iget-boolean v0, p0, Landroid/support/v4/n/l;->d:Z

    if-nez v0, :cond_a

    .line 62
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 64
    :cond_a
    iget v0, p0, Landroid/support/v4/n/l;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/n/l;->c:I

    .line 65
    iget v0, p0, Landroid/support/v4/n/l;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/n/l;->b:I

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/n/l;->d:Z

    .line 67
    iget-object v0, p0, Landroid/support/v4/n/l;->e:Landroid/support/v4/n/k;

    iget v1, p0, Landroid/support/v4/n/l;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v4/n/k;->a(I)V

    .line 68
    return-void
.end method
