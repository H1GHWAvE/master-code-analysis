.class public final Landroid/support/v4/n/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Landroid/support/v4/n/q;->a:Ljava/lang/Object;

    .line 36
    iput-object p2, p0, Landroid/support/v4/n/q;->b:Ljava/lang/Object;

    .line 37
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 57
    if-eq p0, p1, :cond_a

    if-eqz p0, :cond_c

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/n/q;
    .registers 3

    .prologue
    .line 77
    new-instance v0, Landroid/support/v4/n/q;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/n/q;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 49
    instance-of v1, p1, Landroid/support/v4/n/q;

    if-nez v1, :cond_6

    .line 53
    :cond_5
    :goto_5
    return v0

    .line 52
    :cond_6
    check-cast p1, Landroid/support/v4/n/q;

    .line 53
    iget-object v1, p1, Landroid/support/v4/n/q;->a:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/n/q;->a:Ljava/lang/Object;

    invoke-static {v1, v2}, Landroid/support/v4/n/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p1, Landroid/support/v4/n/q;->b:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/n/q;->b:Ljava/lang/Object;

    invoke-static {v1, v2}, Landroid/support/v4/n/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 67
    iget-object v0, p0, Landroid/support/v4/n/q;->a:Ljava/lang/Object;

    if-nez v0, :cond_c

    move v0, v1

    :goto_6
    iget-object v2, p0, Landroid/support/v4/n/q;->b:Ljava/lang/Object;

    if-nez v2, :cond_13

    :goto_a
    xor-int/2addr v0, v1

    return v0

    :cond_c
    iget-object v0, p0, Landroid/support/v4/n/q;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_13
    iget-object v1, p0, Landroid/support/v4/n/q;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_a
.end method
