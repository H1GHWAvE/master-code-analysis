.class final Landroid/support/v4/n/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# instance fields
.field final synthetic a:Landroid/support/v4/n/k;


# direct methods
.method constructor <init>(Landroid/support/v4/n/k;)V
    .registers 2

    .prologue
    .line 265
    iput-object p1, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 269
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 274
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 279
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->c()V

    .line 280
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 284
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/k;->a(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 289
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/n/k;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 339
    invoke-static {p0, p1}, Landroid/support/v4/n/k;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 344
    .line 345
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v3, v1

    :goto_b
    if-ltz v2, :cond_20

    .line 346
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/n/k;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 347
    if-nez v0, :cond_1b

    move v0, v1

    :goto_16
    add-int/2addr v3, v0

    .line 345
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_b

    .line 347
    :cond_1b
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_16

    .line 349
    :cond_20
    return v3
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 294
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->a()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 299
    new-instance v0, Landroid/support/v4/n/l;

    iget-object v1, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v4/n/l;-><init>(Landroid/support/v4/n/k;I)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 304
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/k;->a(Ljava/lang/Object;)I

    move-result v0

    .line 305
    if-ltz v0, :cond_f

    .line 306
    iget-object v1, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v1, v0}, Landroid/support/v4/n/k;->a(I)V

    .line 307
    const/4 v0, 0x1

    .line 309
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 314
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/n/k;->b(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 319
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->b()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/n/k;->c(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 324
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->a()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 329
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/n/k;->b(I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 4

    .prologue
    .line 334
    iget-object v0, p0, Landroid/support/v4/n/n;->a:Landroid/support/v4/n/k;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/n/k;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
