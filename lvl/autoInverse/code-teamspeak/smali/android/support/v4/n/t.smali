.class public Landroid/support/v4/n/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/n/s;


# instance fields
.field private final a:[Ljava/lang/Object;

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .registers 4

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    if-gtz p1, :cond_d

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The max pool size must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_d
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/n/t;->a:[Ljava/lang/Object;

    .line 95
    return-void
.end method

.method private b(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 124
    move v0, v1

    :goto_2
    iget v2, p0, Landroid/support/v4/n/t;->b:I

    if-ge v0, v2, :cond_d

    .line 125
    iget-object v2, p0, Landroid/support/v4/n/t;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_e

    .line 126
    const/4 v1, 0x1

    .line 129
    :cond_d
    return v1

    .line 124
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 100
    iget v0, p0, Landroid/support/v4/n/t;->b:I

    if-lez v0, :cond_18

    .line 101
    iget v0, p0, Landroid/support/v4/n/t;->b:I

    add-int/lit8 v2, v0, -0x1

    .line 102
    iget-object v0, p0, Landroid/support/v4/n/t;->a:[Ljava/lang/Object;

    aget-object v0, v0, v2

    .line 103
    iget-object v3, p0, Landroid/support/v4/n/t;->a:[Ljava/lang/Object;

    aput-object v1, v3, v2

    .line 104
    iget v1, p0, Landroid/support/v4/n/t;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Landroid/support/v4/n/t;->b:I

    .line 107
    :goto_17
    return-object v0

    :cond_18
    move-object v0, v1

    goto :goto_17
.end method

.method public a(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 112
    move v0, v1

    .line 1124
    :goto_3
    iget v3, p0, Landroid/support/v4/n/t;->b:I

    if-ge v0, v3, :cond_1b

    .line 1125
    iget-object v3, p0, Landroid/support/v4/n/t;->a:[Ljava/lang/Object;

    aget-object v3, v3, v0

    if-ne v3, p1, :cond_18

    move v0, v2

    .line 112
    :goto_e
    if-eqz v0, :cond_1d

    .line 113
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already in the pool!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1124
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_1b
    move v0, v1

    .line 1129
    goto :goto_e

    .line 115
    :cond_1d
    iget v0, p0, Landroid/support/v4/n/t;->b:I

    iget-object v3, p0, Landroid/support/v4/n/t;->a:[Ljava/lang/Object;

    array-length v3, v3

    if-ge v0, v3, :cond_31

    .line 116
    iget-object v0, p0, Landroid/support/v4/n/t;->a:[Ljava/lang/Object;

    iget v1, p0, Landroid/support/v4/n/t;->b:I

    aput-object p1, v0, v1

    .line 117
    iget v0, p0, Landroid/support/v4/n/t;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/n/t;->b:I

    move v1, v2

    .line 120
    :cond_31
    return v1
.end method
