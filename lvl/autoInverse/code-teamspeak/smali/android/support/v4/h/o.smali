.class final Landroid/support/v4/h/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/h/p;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 1

    .prologue
    .line 1029
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 88
    return-void
.end method

.method public final a(I)V
    .registers 2

    .prologue
    .line 97
    .line 1037
    invoke-static {p1}, Landroid/net/TrafficStats;->incrementOperationCount(I)V

    .line 98
    return-void
.end method

.method public final a(II)V
    .registers 3

    .prologue
    .line 102
    .line 1041
    invoke-static {p1, p2}, Landroid/net/TrafficStats;->incrementOperationCount(II)V

    .line 103
    return-void
.end method

.method public final a(Ljava/net/Socket;)V
    .registers 2

    .prologue
    .line 112
    .line 1049
    invoke-static {p1}, Landroid/net/TrafficStats;->tagSocket(Ljava/net/Socket;)V

    .line 113
    return-void
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 1033
    invoke-static {}, Landroid/net/TrafficStats;->getThreadStatsTag()I

    move-result v0

    .line 92
    return v0
.end method

.method public final b(I)V
    .registers 2

    .prologue
    .line 107
    .line 1045
    invoke-static {p1}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 108
    return-void
.end method

.method public final b(Ljava/net/Socket;)V
    .registers 2

    .prologue
    .line 117
    .line 1053
    invoke-static {p1}, Landroid/net/TrafficStats;->untagSocket(Ljava/net/Socket;)V

    .line 118
    return-void
.end method
