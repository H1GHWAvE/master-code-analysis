.class public final Landroid/support/v4/m/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/support/v4/m/f;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 54
    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    .line 55
    new-instance v0, Landroid/support/v4/m/i;

    invoke-direct {v0}, Landroid/support/v4/m/i;-><init>()V

    sput-object v0, Landroid/support/v4/m/e;->a:Landroid/support/v4/m/f;

    .line 61
    :goto_d
    return-void

    .line 56
    :cond_e
    const/16 v1, 0xe

    if-lt v0, v1, :cond_1a

    .line 57
    new-instance v0, Landroid/support/v4/m/h;

    invoke-direct {v0}, Landroid/support/v4/m/h;-><init>()V

    sput-object v0, Landroid/support/v4/m/e;->a:Landroid/support/v4/m/f;

    goto :goto_d

    .line 59
    :cond_1a
    new-instance v0, Landroid/support/v4/m/g;

    invoke-direct {v0}, Landroid/support/v4/m/g;-><init>()V

    sput-object v0, Landroid/support/v4/m/e;->a:Landroid/support/v4/m/f;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method public static a(Ljava/util/Locale;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 87
    sget-object v0, Landroid/support/v4/m/e;->a:Landroid/support/v4/m/f;

    invoke-interface {v0, p0}, Landroid/support/v4/m/f;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
