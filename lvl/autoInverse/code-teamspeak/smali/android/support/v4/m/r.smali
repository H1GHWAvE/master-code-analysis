.class abstract Landroid/support/v4/m/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/m/l;


# instance fields
.field private final a:Landroid/support/v4/m/q;


# direct methods
.method public constructor <init>(Landroid/support/v4/m/q;)V
    .registers 2

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object p1, p0, Landroid/support/v4/m/r;->a:Landroid/support/v4/m/q;

    .line 116
    return-void
.end method

.method private b(Ljava/lang/CharSequence;II)Z
    .registers 5

    .prologue
    .line 140
    iget-object v0, p0, Landroid/support/v4/m/r;->a:Landroid/support/v4/m/q;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/m/q;->a(Ljava/lang/CharSequence;II)I

    move-result v0

    packed-switch v0, :pswitch_data_12

    .line 146
    invoke-virtual {p0}, Landroid/support/v4/m/r;->a()Z

    move-result v0

    :goto_d
    return v0

    .line 142
    :pswitch_e
    const/4 v0, 0x1

    goto :goto_d

    .line 144
    :pswitch_10
    const/4 v0, 0x0

    goto :goto_d

    .line 140
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_e
        :pswitch_10
    .end packed-switch
.end method


# virtual methods
.method protected abstract a()Z
.end method

.method public final a(Ljava/lang/CharSequence;II)Z
    .registers 5

    .prologue
    .line 130
    if-eqz p1, :cond_d

    if-ltz p2, :cond_d

    if-ltz p3, :cond_d

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    sub-int/2addr v0, p3

    if-ge v0, p2, :cond_13

    .line 131
    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 133
    :cond_13
    iget-object v0, p0, Landroid/support/v4/m/r;->a:Landroid/support/v4/m/q;

    if-nez v0, :cond_1c

    .line 134
    invoke-virtual {p0}, Landroid/support/v4/m/r;->a()Z

    move-result v0

    .line 1144
    :goto_1b
    return v0

    .line 1140
    :cond_1c
    iget-object v0, p0, Landroid/support/v4/m/r;->a:Landroid/support/v4/m/q;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/m/q;->a(Ljava/lang/CharSequence;II)I

    move-result v0

    packed-switch v0, :pswitch_data_2e

    .line 1146
    invoke-virtual {p0}, Landroid/support/v4/m/r;->a()Z

    move-result v0

    goto :goto_1b

    .line 1142
    :pswitch_2a
    const/4 v0, 0x1

    goto :goto_1b

    .line 1144
    :pswitch_2c
    const/4 v0, 0x0

    goto :goto_1b

    .line 1140
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_2c
    .end packed-switch
.end method

.method public final a([CII)Z
    .registers 5

    .prologue
    .line 125
    invoke-static {p1}, Ljava/nio/CharBuffer;->wrap([C)Ljava/nio/CharBuffer;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Landroid/support/v4/m/r;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    return v0
.end method
