.class final Landroid/support/v4/widget/x;
.super Landroid/support/v4/view/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v4/widget/DrawerLayout;

.field private final c:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/DrawerLayout;)V
    .registers 3

    .prologue
    .line 2005
    iput-object p1, p0, Landroid/support/v4/widget/x;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-direct {p0}, Landroid/support/v4/view/a;-><init>()V

    .line 2006
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/x;->c:Landroid/graphics/Rect;

    return-void
.end method

.method private a(Landroid/support/v4/view/a/q;Landroid/support/v4/view/a/q;)V
    .registers 4

    .prologue
    .line 2098
    iget-object v0, p0, Landroid/support/v4/widget/x;->c:Landroid/graphics/Rect;

    .line 2100
    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->a(Landroid/graphics/Rect;)V

    .line 2101
    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->b(Landroid/graphics/Rect;)V

    .line 2103
    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->c(Landroid/graphics/Rect;)V

    .line 2104
    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->d(Landroid/graphics/Rect;)V

    .line 2106
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->e()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->c(Z)V

    .line 2107
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->k()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/CharSequence;)V

    .line 2108
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->l()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 2109
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->n()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->c(Ljava/lang/CharSequence;)V

    .line 2111
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->j()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->h(Z)V

    .line 2112
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->h()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->f(Z)V

    .line 2113
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->c()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->a(Z)V

    .line 2114
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->d()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->b(Z)V

    .line 2115
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->f()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 2116
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->g()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->e(Z)V

    .line 2117
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->i()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->g(Z)V

    .line 2119
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->a(I)V

    .line 2120
    return-void
.end method

.method private static a(Landroid/support/v4/view/a/q;Landroid/view/ViewGroup;)V
    .registers 6

    .prologue
    .line 2082
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 2083
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_17

    .line 2084
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2085
    invoke-static {v2}, Landroid/support/v4/widget/DrawerLayout;->f(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 2086
    invoke-virtual {p0, v2}, Landroid/support/v4/view/a/q;->c(Landroid/view/View;)V

    .line 2083
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2089
    :cond_17
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 2010
    invoke-static {}, Landroid/support/v4/widget/DrawerLayout;->f()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 2011
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 2030
    :cond_a
    const-class v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 2035
    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->a(Z)V

    .line 2036
    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->b(Z)V

    .line 2037
    sget-object v0, Landroid/support/v4/view/a/s;->a:Landroid/support/v4/view/a/s;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->a(Landroid/support/v4/view/a/s;)Z

    .line 2038
    sget-object v0, Landroid/support/v4/view/a/s;->b:Landroid/support/v4/view/a/s;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->a(Landroid/support/v4/view/a/s;)Z

    .line 2039
    return-void

    .line 2015
    :cond_24
    invoke-static {p2}, Landroid/support/v4/view/a/q;->a(Landroid/support/v4/view/a/q;)Landroid/support/v4/view/a/q;

    move-result-object v2

    .line 2017
    invoke-super {p0, p1, v2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 2019
    invoke-virtual {p2, p1}, Landroid/support/v4/view/a/q;->b(Landroid/view/View;)V

    .line 2020
    invoke-static {p1}, Landroid/support/v4/view/cx;->g(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    .line 2021
    instance-of v3, v0, Landroid/view/View;

    if-eqz v3, :cond_3b

    .line 2022
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->d(Landroid/view/View;)V

    .line 3098
    :cond_3b
    iget-object v0, p0, Landroid/support/v4/widget/x;->c:Landroid/graphics/Rect;

    .line 3100
    invoke-virtual {v2, v0}, Landroid/support/v4/view/a/q;->a(Landroid/graphics/Rect;)V

    .line 3101
    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->b(Landroid/graphics/Rect;)V

    .line 3103
    invoke-virtual {v2, v0}, Landroid/support/v4/view/a/q;->c(Landroid/graphics/Rect;)V

    .line 3104
    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->d(Landroid/graphics/Rect;)V

    .line 3106
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->e()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->c(Z)V

    .line 3107
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->k()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/CharSequence;)V

    .line 3108
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->l()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 3109
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->n()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->c(Ljava/lang/CharSequence;)V

    .line 3111
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->j()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->h(Z)V

    .line 3112
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->h()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->f(Z)V

    .line 3113
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->c()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->a(Z)V

    .line 3114
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->d()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->b(Z)V

    .line 3115
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->f()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 3116
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->g()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->e(Z)V

    .line 3117
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->i()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->g(Z)V

    .line 3119
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->b()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->a(I)V

    .line 2025
    invoke-virtual {v2}, Landroid/support/v4/view/a/q;->o()V

    .line 2027
    check-cast p1, Landroid/view/ViewGroup;

    .line 4082
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v0, v1

    .line 4083
    :goto_a7
    if-ge v0, v2, :cond_a

    .line 4084
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 4085
    invoke-static {v3}, Landroid/support/v4/widget/DrawerLayout;->f(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_b6

    .line 4086
    invoke-virtual {p2, v3}, Landroid/support/v4/view/a/q;->c(Landroid/view/View;)V

    .line 4083
    :cond_b6
    add-int/lit8 v0, v0, 0x1

    goto :goto_a7
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4

    .prologue
    .line 2043
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2045
    const-class v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 2046
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 5

    .prologue
    .line 2075
    invoke-static {}, Landroid/support/v4/widget/DrawerLayout;->f()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {p2}, Landroid/support/v4/widget/DrawerLayout;->f(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2076
    :cond_c
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/a;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 2078
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 7

    .prologue
    .line 2055
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_38

    .line 2056
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    .line 2057
    iget-object v0, p0, Landroid/support/v4/widget/x;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-static {v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/support/v4/widget/DrawerLayout;)Landroid/view/View;

    move-result-object v0

    .line 2058
    if-eqz v0, :cond_2e

    .line 2059
    iget-object v2, p0, Landroid/support/v4/widget/x;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/DrawerLayout;->c(Landroid/view/View;)I

    move-result v0

    .line 2060
    iget-object v2, p0, Landroid/support/v4/widget/x;->a:Landroid/support/v4/widget/DrawerLayout;

    .line 4668
    invoke-static {v2}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v3

    invoke-static {v0, v3}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    .line 4670
    const/4 v3, 0x3

    if-ne v0, v3, :cond_30

    .line 4671
    iget-object v0, v2, Landroid/support/v4/widget/DrawerLayout;->l:Ljava/lang/CharSequence;

    .line 2061
    :goto_29
    if-eqz v0, :cond_2e

    .line 2062
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066
    :cond_2e
    const/4 v0, 0x1

    .line 2069
    :goto_2f
    return v0

    .line 4672
    :cond_30
    const/4 v3, 0x5

    if-ne v0, v3, :cond_36

    .line 4673
    iget-object v0, v2, Landroid/support/v4/widget/DrawerLayout;->m:Ljava/lang/CharSequence;

    goto :goto_29

    .line 4675
    :cond_36
    const/4 v0, 0x0

    goto :goto_29

    .line 2069
    :cond_38
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_2f
.end method
