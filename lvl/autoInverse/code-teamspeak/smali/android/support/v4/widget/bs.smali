.class Landroid/support/v4/widget/bs;
.super Landroid/support/v4/widget/br;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/support/v4/widget/br;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Landroid/widget/PopupWindow;)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    .line 1034
    sget-boolean v0, Landroid/support/v4/widget/bx;->b:Z

    if-nez v0, :cond_1d

    .line 1036
    :try_start_5
    const-class v0, Landroid/widget/PopupWindow;

    const-string v1, "setWindowLayoutType"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 1038
    sput-object v0, Landroid/support/v4/widget/bx;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_1b} :catch_34

    .line 1042
    :goto_1b
    sput-boolean v5, Landroid/support/v4/widget/bx;->b:Z

    .line 1045
    :cond_1d
    sget-object v0, Landroid/support/v4/widget/bx;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_31

    .line 1047
    :try_start_21
    sget-object v0, Landroid/support/v4/widget/bx;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_31} :catch_32

    .line 1050
    :cond_31
    :goto_31
    return-void

    :catch_32
    move-exception v0

    goto :goto_31

    :catch_34
    move-exception v0

    goto :goto_1b
.end method

.method public c(Landroid/widget/PopupWindow;)I
    .registers 3

    .prologue
    .line 81
    invoke-static {p1}, Landroid/support/v4/widget/bx;->a(Landroid/widget/PopupWindow;)I

    move-result v0

    return v0
.end method
