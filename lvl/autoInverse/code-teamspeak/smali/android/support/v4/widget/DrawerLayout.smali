.class public Landroid/support/v4/widget/DrawerLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/ak;


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x0

.field public static final e:I = 0x1

.field public static final f:I = 0x2

.field static final n:Landroid/support/v4/widget/z;

.field private static final o:Ljava/lang/String; = "DrawerLayout"

.field private static final p:I = 0x40

.field private static final q:I = 0xa

.field private static final r:I = -0x67000000

.field private static final s:I = 0xa0

.field private static final t:I = 0x190

.field private static final u:Z = false

.field private static final v:Z = true

.field private static final w:F = 1.0f

.field private static final x:[I

.field private static final y:Z

.field private static final z:Z


# instance fields
.field private final A:Landroid/support/v4/widget/y;

.field private B:F

.field private C:I

.field private D:I

.field private E:F

.field private F:Landroid/graphics/Paint;

.field private final G:Landroid/support/v4/widget/ag;

.field private final H:Landroid/support/v4/widget/ag;

.field private I:Z

.field private J:Z

.field private K:I

.field private L:I

.field private M:Z

.field private N:F

.field private O:F

.field private P:Landroid/graphics/drawable/Drawable;

.field private Q:Landroid/graphics/drawable/Drawable;

.field private R:Landroid/graphics/drawable/Drawable;

.field private S:Ljava/lang/Object;

.field private T:Z

.field private U:Landroid/graphics/drawable/Drawable;

.field private V:Landroid/graphics/drawable/Drawable;

.field private W:Landroid/graphics/drawable/Drawable;

.field private aa:Landroid/graphics/drawable/Drawable;

.field private final ab:Ljava/util/ArrayList;

.field final g:Landroid/support/v4/widget/eg;

.field final h:Landroid/support/v4/widget/eg;

.field i:I

.field j:Z

.field k:Landroid/support/v4/widget/ac;

.field l:Ljava/lang/CharSequence;

.field m:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/16 v4, 0x15

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 164
    new-array v0, v1, [I

    const v3, 0x10100b3

    aput v3, v0, v2

    sput-object v0, Landroid/support/v4/widget/DrawerLayout;->x:[I

    .line 169
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_28

    move v0, v1

    :goto_14
    sput-boolean v0, Landroid/support/v4/widget/DrawerLayout;->y:Z

    .line 172
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_2a

    :goto_1a
    sput-boolean v1, Landroid/support/v4/widget/DrawerLayout;->z:Z

    .line 332
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 333
    if-lt v0, v4, :cond_2c

    .line 334
    new-instance v0, Landroid/support/v4/widget/aa;

    invoke-direct {v0}, Landroid/support/v4/widget/aa;-><init>()V

    sput-object v0, Landroid/support/v4/widget/DrawerLayout;->n:Landroid/support/v4/widget/z;

    .line 338
    :goto_27
    return-void

    :cond_28
    move v0, v2

    .line 169
    goto :goto_14

    :cond_2a
    move v1, v2

    .line 172
    goto :goto_1a

    .line 336
    :cond_2c
    new-instance v0, Landroid/support/v4/widget/ab;

    invoke-direct {v0}, Landroid/support/v4/widget/ab;-><init>()V

    sput-object v0, Landroid/support/v4/widget/DrawerLayout;->n:Landroid/support/v4/widget/z;

    goto :goto_27
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 343
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;B)V

    .line 344
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 347
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;C)V

    .line 348
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 9

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 351
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 175
    new-instance v0, Landroid/support/v4/widget/y;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/y;-><init>(Landroid/support/v4/widget/DrawerLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->A:Landroid/support/v4/widget/y;

    .line 181
    const/high16 v0, -0x67000000

    iput v0, p0, Landroid/support/v4/widget/DrawerLayout;->D:I

    .line 183
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->F:Landroid/graphics/Paint;

    .line 191
    iput-boolean v4, p0, Landroid/support/v4/widget/DrawerLayout;->J:Z

    .line 213
    iput-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    .line 214
    iput-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    .line 215
    iput-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->W:Landroid/graphics/drawable/Drawable;

    .line 216
    iput-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->aa:Landroid/graphics/drawable/Drawable;

    .line 352
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->setDescendantFocusability(I)V

    .line 353
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 354
    const/high16 v1, 0x42800000    # 64.0f

    mul-float/2addr v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/widget/DrawerLayout;->C:I

    .line 355
    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v1, v0

    .line 357
    new-instance v2, Landroid/support/v4/widget/ag;

    const/4 v3, 0x3

    invoke-direct {v2, p0, v3}, Landroid/support/v4/widget/ag;-><init>(Landroid/support/v4/widget/DrawerLayout;I)V

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->G:Landroid/support/v4/widget/ag;

    .line 358
    new-instance v2, Landroid/support/v4/widget/ag;

    const/4 v3, 0x5

    invoke-direct {v2, p0, v3}, Landroid/support/v4/widget/ag;-><init>(Landroid/support/v4/widget/DrawerLayout;I)V

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->H:Landroid/support/v4/widget/ag;

    .line 360
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->G:Landroid/support/v4/widget/ag;

    invoke-static {p0, v5, v2}, Landroid/support/v4/widget/eg;->a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 361
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 2437
    iput v4, v2, Landroid/support/v4/widget/eg;->u:I

    .line 362
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 3401
    iput v1, v2, Landroid/support/v4/widget/eg;->s:F

    .line 363
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->G:Landroid/support/v4/widget/ag;

    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 3816
    iput-object v3, v2, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    .line 365
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->H:Landroid/support/v4/widget/ag;

    invoke-static {p0, v5, v2}, Landroid/support/v4/widget/eg;->a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    .line 366
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    .line 4437
    const/4 v3, 0x2

    iput v3, v2, Landroid/support/v4/widget/eg;->u:I

    .line 367
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    .line 5401
    iput v1, v2, Landroid/support/v4/widget/eg;->s:F

    .line 368
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->H:Landroid/support/v4/widget/ag;

    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    .line 5816
    iput-object v2, v1, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    .line 371
    invoke-virtual {p0, v4}, Landroid/support/v4/widget/DrawerLayout;->setFocusableInTouchMode(Z)V

    .line 373
    invoke-static {p0, v4}, Landroid/support/v4/view/cx;->c(Landroid/view/View;I)V

    .line 376
    new-instance v1, Landroid/support/v4/widget/x;

    invoke-direct {v1, p0}, Landroid/support/v4/widget/x;-><init>(Landroid/support/v4/widget/DrawerLayout;)V

    invoke-static {p0, v1}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 377
    invoke-static {p0}, Landroid/support/v4/view/ec;->a(Landroid/view/ViewGroup;)V

    .line 378
    invoke-static {p0}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_a0

    .line 379
    sget-object v1, Landroid/support/v4/widget/DrawerLayout;->n:Landroid/support/v4/widget/z;

    invoke-interface {v1, p0}, Landroid/support/v4/widget/z;->a(Landroid/view/View;)V

    .line 380
    sget-object v1, Landroid/support/v4/widget/DrawerLayout;->n:Landroid/support/v4/widget/z;

    invoke-interface {v1, p1}, Landroid/support/v4/widget/z;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->P:Landroid/graphics/drawable/Drawable;

    .line 383
    :cond_a0
    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/DrawerLayout;->B:F

    .line 385
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->ab:Ljava/util/ArrayList;

    .line 386
    return-void
.end method

.method static synthetic a(Landroid/support/v4/widget/DrawerLayout;)Landroid/view/View;
    .registers 2

    .prologue
    .line 90
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->n()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(F)V
    .registers 3

    .prologue
    .line 772
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    if-eqz v0, :cond_9

    .line 773
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/ac;->a(F)V

    .line 775
    :cond_9
    return-void
.end method

.method private a(II)V
    .registers 7
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    const v3, 0x800005

    const v2, 0x800003

    .line 484
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 6450
    sget-boolean v1, Landroid/support/v4/widget/DrawerLayout;->z:Z

    if-nez v1, :cond_1e

    .line 6454
    and-int v1, p2, v2

    if-ne v1, v2, :cond_1f

    .line 6455
    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    .line 6465
    :goto_18
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->h()V

    .line 6466
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 6463
    :cond_1e
    return-void

    .line 6456
    :cond_1f
    and-int v1, p2, v3

    if-ne v1, v3, :cond_26

    .line 6457
    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    goto :goto_18

    .line 6458
    :cond_26
    and-int/lit8 v1, p2, 0x3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2e

    .line 6459
    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->W:Landroid/graphics/drawable/Drawable;

    goto :goto_18

    .line 6460
    :cond_2e
    and-int/lit8 v1, p2, 0x5

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1e

    .line 6461
    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->aa:Landroid/graphics/drawable/Drawable;

    goto :goto_18
.end method

.method private a(ILandroid/view/View;)V
    .registers 6

    .prologue
    .line 593
    invoke-static {p2}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 594
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer with appropriate layout_gravity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 597
    :cond_21
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget v0, v0, Landroid/support/v4/widget/ad;->a:I

    .line 598
    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(II)V

    .line 599
    return-void
.end method

.method private a(ILjava/lang/CharSequence;)V
    .registers 5

    .prologue
    .line 649
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    .line 651
    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    .line 652
    iput-object p2, p0, Landroid/support/v4/widget/DrawerLayout;->l:Ljava/lang/CharSequence;

    .line 656
    :cond_d
    :goto_d
    return-void

    .line 653
    :cond_e
    const/4 v1, 0x5

    if-ne v0, v1, :cond_d

    .line 654
    iput-object p2, p0, Landroid/support/v4/widget/DrawerLayout;->m:Ljava/lang/CharSequence;

    goto :goto_d
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
    .registers 6

    .prologue
    const v2, 0x800005

    const v1, 0x800003

    .line 450
    sget-boolean v0, Landroid/support/v4/widget/DrawerLayout;->z:Z

    if-eqz v0, :cond_b

    .line 467
    :cond_a
    :goto_a
    return-void

    .line 454
    :cond_b
    and-int v0, p2, v1

    if-ne v0, v1, :cond_18

    .line 455
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    .line 465
    :goto_11
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->h()V

    .line 466
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    goto :goto_a

    .line 456
    :cond_18
    and-int v0, p2, v2

    if-ne v0, v2, :cond_1f

    .line 457
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    goto :goto_11

    .line 458
    :cond_1f
    and-int/lit8 v0, p2, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_27

    .line 459
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayout;->W:Landroid/graphics/drawable/Drawable;

    goto :goto_11

    .line 460
    :cond_27
    and-int/lit8 v0, p2, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_a

    .line 461
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayout;->aa:Landroid/graphics/drawable/Drawable;

    goto :goto_11
.end method

.method private a(Z)V
    .registers 11

    .prologue
    const/4 v3, 0x0

    .line 1397
    .line 1398
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v4

    move v2, v3

    move v1, v3

    .line 1399
    :goto_7
    if-ge v2, v4, :cond_4c

    .line 1400
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1401
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 1403
    invoke-static {v5}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_38

    if-eqz p1, :cond_1f

    iget-boolean v6, v0, Landroid/support/v4/widget/ad;->c:Z

    if-eqz v6, :cond_38

    .line 1407
    :cond_1f
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 1409
    const/4 v7, 0x3

    invoke-virtual {p0, v5, v7}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v7

    if-eqz v7, :cond_3c

    .line 1410
    iget-object v7, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    neg-int v6, v6

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v7, v5, v6, v8}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;II)Z

    move-result v5

    or-int/2addr v1, v5

    .line 1417
    :goto_36
    iput-boolean v3, v0, Landroid/support/v4/widget/ad;->c:Z

    .line 1399
    :cond_38
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 1413
    :cond_3c
    iget-object v6, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v6, v5, v7, v8}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;II)Z

    move-result v5

    or-int/2addr v1, v5

    goto :goto_36

    .line 1420
    :cond_4c
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->G:Landroid/support/v4/widget/ag;

    invoke-virtual {v0}, Landroid/support/v4/widget/ag;->a()V

    .line 1421
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->H:Landroid/support/v4/widget/ag;

    invoke-virtual {v0}, Landroid/support/v4/widget/ag;->a()V

    .line 1423
    if-eqz v1, :cond_5b

    .line 1424
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1426
    :cond_5b
    return-void
.end method

.method static b(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 788
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget v0, v0, Landroid/support/v4/widget/ad;->b:F

    return v0
.end method

.method private b(I)I
    .registers 4

    .prologue
    .line 610
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    .line 612
    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    .line 613
    iget v0, p0, Landroid/support/v4/widget/DrawerLayout;->K:I

    .line 617
    :goto_d
    return v0

    .line 614
    :cond_e
    const/4 v1, 0x5

    if-ne v0, v1, :cond_14

    .line 615
    iget v0, p0, Landroid/support/v4/widget/DrawerLayout;->L:I

    goto :goto_d

    .line 617
    :cond_14
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private b(II)V
    .registers 6

    .prologue
    const/4 v2, 0x3

    .line 545
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    invoke-static {p2, v0}, Landroid/support/v4/view/v;->a(II)I

    move-result v1

    .line 547
    if-ne v1, v2, :cond_1a

    .line 548
    iput p1, p0, Landroid/support/v4/widget/DrawerLayout;->K:I

    .line 552
    :cond_d
    :goto_d
    if-eqz p1, :cond_16

    .line 554
    if-ne v1, v2, :cond_20

    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 555
    :goto_13
    invoke-virtual {v0}, Landroid/support/v4/widget/eg;->a()V

    .line 557
    :cond_16
    packed-switch p1, :pswitch_data_38

    .line 572
    :cond_19
    :goto_19
    return-void

    .line 549
    :cond_1a
    const/4 v0, 0x5

    if-ne v1, v0, :cond_d

    .line 550
    iput p1, p0, Landroid/support/v4/widget/DrawerLayout;->L:I

    goto :goto_d

    .line 554
    :cond_20
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    goto :goto_13

    .line 559
    :pswitch_23
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 560
    if-eqz v0, :cond_19

    .line 561
    invoke-direct {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->k(Landroid/view/View;)V

    goto :goto_19

    .line 565
    :pswitch_2d
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 566
    if-eqz v0, :cond_19

    .line 567
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->e(Landroid/view/View;)V

    goto :goto_19

    .line 557
    nop

    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_2d
        :pswitch_23
    .end packed-switch
.end method

.method private b(ILandroid/view/View;)V
    .registers 10

    .prologue
    const/16 v6, 0x20

    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 683
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 7421
    iget v1, v1, Landroid/support/v4/widget/eg;->m:I

    .line 684
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    .line 8421
    iget v4, v4, Landroid/support/v4/widget/eg;->m:I

    .line 687
    if-eq v1, v3, :cond_11

    if-ne v4, v3, :cond_51

    :cond_11
    move v1, v3

    .line 695
    :goto_12
    if-eqz p2, :cond_4a

    if-nez p1, :cond_4a

    .line 696
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 697
    iget v4, v0, Landroid/support/v4/widget/ad;->b:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_59

    .line 8714
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 8715
    iget-boolean v3, v0, Landroid/support/v4/widget/ad;->d:Z

    if-eqz v3, :cond_4a

    .line 8716
    iput-boolean v2, v0, Landroid/support/v4/widget/ad;->d:Z

    .line 8717
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    if-eqz v0, :cond_38

    .line 8718
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    invoke-interface {v0}, Landroid/support/v4/widget/ac;->b()V

    .line 8721
    :cond_38
    invoke-virtual {p0, p2, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;Z)V

    .line 8726
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 8727
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 8728
    if-eqz v0, :cond_4a

    .line 8729
    invoke-virtual {v0, v6}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 704
    :cond_4a
    :goto_4a
    iget v0, p0, Landroid/support/v4/widget/DrawerLayout;->i:I

    if-eq v1, v0, :cond_50

    .line 705
    iput v1, p0, Landroid/support/v4/widget/DrawerLayout;->i:I

    .line 711
    :cond_50
    return-void

    .line 689
    :cond_51
    if-eq v1, v0, :cond_55

    if-ne v4, v0, :cond_57

    :cond_55
    move v1, v0

    .line 690
    goto :goto_12

    :cond_57
    move v1, v2

    .line 692
    goto :goto_12

    .line 699
    :cond_59
    iget v0, v0, Landroid/support/v4/widget/ad;->b:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_4a

    .line 8736
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 8737
    iget-boolean v2, v0, Landroid/support/v4/widget/ad;->d:Z

    if-nez v2, :cond_4a

    .line 8738
    iput-boolean v3, v0, Landroid/support/v4/widget/ad;->d:Z

    .line 8739
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    if-eqz v0, :cond_76

    .line 8740
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    invoke-interface {v0}, Landroid/support/v4/widget/ac;->a()V

    .line 8743
    :cond_76
    invoke-virtual {p0, p2, v3}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;Z)V

    .line 8746
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_82

    .line 8747
    invoke-virtual {p0, v6}, Landroid/support/v4/widget/DrawerLayout;->sendAccessibilityEvent(I)V

    .line 8750
    :cond_82
    invoke-virtual {p2}, Landroid/view/View;->requestFocus()Z

    goto :goto_4a
.end method

.method private b(Landroid/view/View;F)V
    .registers 6

    .prologue
    .line 817
    invoke-static {p1}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/view/View;)F

    move-result v0

    .line 818
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 819
    int-to-float v2, v1

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 820
    int-to-float v1, v1

    mul-float/2addr v1, p2

    float-to-int v1, v1

    .line 821
    sub-int v0, v1, v0

    .line 823
    const/4 v1, 0x3

    invoke-virtual {p0, p1, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_1e

    :goto_17
    invoke-virtual {p1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 825
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;F)V

    .line 826
    return-void

    .line 823
    :cond_1e
    neg-int v0, v0

    goto :goto_17
.end method

.method private static b(Landroid/graphics/drawable/Drawable;I)Z
    .registers 3

    .prologue
    .line 1020
    if-eqz p0, :cond_8

    invoke-static {p0}, Landroid/support/v4/e/a/a;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1021
    :cond_8
    const/4 v0, 0x0

    .line 1025
    :goto_9
    return v0

    .line 1024
    :cond_a
    invoke-static {p0, p1}, Landroid/support/v4/e/a/a;->b(Landroid/graphics/drawable/Drawable;I)V

    .line 1025
    const/4 v0, 0x1

    goto :goto_9
.end method

.method private c(I)Ljava/lang/CharSequence;
    .registers 4
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 668
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    .line 670
    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    .line 671
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->l:Ljava/lang/CharSequence;

    .line 675
    :goto_d
    return-object v0

    .line 672
    :cond_e
    const/4 v1, 0x5

    if-ne v0, v1, :cond_14

    .line 673
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->m:Ljava/lang/CharSequence;

    goto :goto_d

    .line 675
    :cond_14
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private static d(I)Ljava/lang/String;
    .registers 3

    .prologue
    .line 855
    and-int/lit8 v0, p0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_8

    .line 856
    const-string v0, "LEFT"

    .line 861
    :goto_7
    return-object v0

    .line 858
    :cond_8
    and-int/lit8 v0, p0, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_10

    .line 859
    const-string v0, "RIGHT"

    goto :goto_7

    .line 861
    :cond_10
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method static d(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1271
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget v0, v0, Landroid/support/v4/widget/ad;->a:I

    .line 1272
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    .line 1274
    and-int/lit8 v0, v0, 0x7

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method static synthetic e()[I
    .registers 1

    .prologue
    .line 90
    sget-object v0, Landroid/support/v4/widget/DrawerLayout;->x:[I

    return-object v0
.end method

.method static synthetic f()Z
    .registers 1

    .prologue
    .line 90
    sget-boolean v0, Landroid/support/v4/widget/DrawerLayout;->y:Z

    return v0
.end method

.method static synthetic f(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 90
    .line 13758
    invoke-static {p0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_10

    invoke-static {p0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    .line 90
    goto :goto_f
.end method

.method private g()Landroid/view/View;
    .registers 5

    .prologue
    .line 806
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v3

    .line 807
    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v3, :cond_1c

    .line 808
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 809
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget-boolean v0, v0, Landroid/support/v4/widget/ad;->d:Z

    if-eqz v0, :cond_18

    move-object v0, v1

    .line 813
    :goto_17
    return-object v0

    .line 807
    :cond_18
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 813
    :cond_1c
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private g(Landroid/view/View;)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 714
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 715
    iget-boolean v1, v0, Landroid/support/v4/widget/ad;->d:Z

    if-eqz v1, :cond_2a

    .line 716
    iput-boolean v2, v0, Landroid/support/v4/widget/ad;->d:Z

    .line 717
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    if-eqz v0, :cond_16

    .line 718
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    invoke-interface {v0}, Landroid/support/v4/widget/ac;->b()V

    .line 721
    :cond_16
    invoke-virtual {p0, p1, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;Z)V

    .line 726
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 727
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 728
    if-eqz v0, :cond_2a

    .line 729
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 733
    :cond_2a
    return-void
.end method

.method private h()V
    .registers 3

    .prologue
    .line 970
    sget-boolean v0, Landroid/support/v4/widget/DrawerLayout;->z:Z

    if-eqz v0, :cond_5

    .line 975
    :goto_4
    return-void

    .line 8978
    :cond_5
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    .line 8980
    if-nez v0, :cond_2c

    .line 8981
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_38

    .line 8983
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/graphics/drawable/Drawable;I)Z

    .line 8984
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    .line 973
    :goto_16
    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->Q:Landroid/graphics/drawable/Drawable;

    .line 8997
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    .line 8998
    if-nez v0, :cond_3b

    .line 8999
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_47

    .line 9001
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/graphics/drawable/Drawable;I)Z

    .line 9002
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    .line 974
    :goto_29
    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->R:Landroid/graphics/drawable/Drawable;

    goto :goto_4

    .line 8987
    :cond_2c
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_38

    .line 8989
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/graphics/drawable/Drawable;I)Z

    .line 8990
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    goto :goto_16

    .line 8993
    :cond_38
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->W:Landroid/graphics/drawable/Drawable;

    goto :goto_16

    .line 9005
    :cond_3b
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_47

    .line 9007
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/graphics/drawable/Drawable;I)Z

    .line 9008
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    goto :goto_29

    .line 9011
    :cond_47
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->aa:Landroid/graphics/drawable/Drawable;

    goto :goto_29
.end method

.method private h(Landroid/view/View;)V
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 736
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 737
    iget-boolean v1, v0, Landroid/support/v4/widget/ad;->d:Z

    if-nez v1, :cond_27

    .line 738
    iput-boolean v2, v0, Landroid/support/v4/widget/ad;->d:Z

    .line 739
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    if-eqz v0, :cond_16

    .line 740
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    invoke-interface {v0}, Landroid/support/v4/widget/ac;->a()V

    .line 743
    :cond_16
    invoke-virtual {p0, p1, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;Z)V

    .line 746
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 747
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->sendAccessibilityEvent(I)V

    .line 750
    :cond_24
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 752
    :cond_27
    return-void
.end method

.method private i()Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 978
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    .line 980
    if-nez v0, :cond_12

    .line 981
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1e

    .line 983
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/graphics/drawable/Drawable;I)Z

    .line 984
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    .line 993
    :goto_11
    return-object v0

    .line 987
    :cond_12
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1e

    .line 989
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/graphics/drawable/Drawable;I)Z

    .line 990
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    goto :goto_11

    .line 993
    :cond_1e
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->W:Landroid/graphics/drawable/Drawable;

    goto :goto_11
.end method

.method private static i(Landroid/view/View;)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 1136
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1137
    if-eqz v1, :cond_f

    .line 1138
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_f

    const/4 v0, 0x1

    .line 1140
    :cond_f
    return v0
.end method

.method private j()Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 997
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    .line 998
    if-nez v0, :cond_12

    .line 999
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1e

    .line 1001
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/graphics/drawable/Drawable;I)Z

    .line 1002
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->V:Landroid/graphics/drawable/Drawable;

    .line 1011
    :goto_11
    return-object v0

    .line 1005
    :cond_12
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1e

    .line 1007
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/graphics/drawable/Drawable;I)Z

    .line 1008
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->U:Landroid/graphics/drawable/Drawable;

    goto :goto_11

    .line 1011
    :cond_1e
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->aa:Landroid/graphics/drawable/Drawable;

    goto :goto_11
.end method

.method private static j(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 1267
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget v0, v0, Landroid/support/v4/widget/ad;->a:I

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private k()V
    .registers 2

    .prologue
    .line 1393
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Z)V

    .line 1394
    return-void
.end method

.method private k(Landroid/view/View;)V
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 1434
    invoke-static {p1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 1435
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a sliding drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1438
    :cond_22
    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->J:Z

    if-eqz v0, :cond_39

    .line 1439
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 1440
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/support/v4/widget/ad;->b:F

    .line 1441
    iput-boolean v2, v0, Landroid/support/v4/widget/ad;->d:Z

    .line 1443
    invoke-virtual {p0, p1, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;Z)V

    .line 1452
    :goto_35
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1453
    return-void

    .line 1445
    :cond_39
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 1446
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;II)Z

    goto :goto_35

    .line 1448
    :cond_4b
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;II)Z

    goto :goto_35
.end method

.method private l()Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 1576
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v3

    move v2, v1

    .line 1577
    :goto_6
    if-ge v2, v3, :cond_1c

    .line 1578
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 1579
    iget-boolean v0, v0, Landroid/support/v4/widget/ad;->c:Z

    if-eqz v0, :cond_18

    .line 1580
    const/4 v0, 0x1

    .line 1583
    :goto_17
    return v0

    .line 1577
    :cond_18
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_1c
    move v0, v1

    .line 1583
    goto :goto_17
.end method

.method private static l(Landroid/view/View;)Z
    .registers 4

    .prologue
    .line 1521
    invoke-static {p0}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1522
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1524
    :cond_21
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget-boolean v0, v0, Landroid/support/v4/widget/ad;->d:Z

    return v0
.end method

.method private m()Z
    .registers 2

    .prologue
    .line 1646
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->n()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static m(Landroid/view/View;)Z
    .registers 4

    .prologue
    .line 1553
    invoke-static {p0}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1554
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1556
    :cond_21
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget v0, v0, Landroid/support/v4/widget/ad;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_30

    const/4 v0, 0x1

    :goto_2f
    return v0

    :cond_30
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method private n()Landroid/view/View;
    .registers 5

    .prologue
    .line 1650
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v2

    .line 1651
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_1d

    .line 1652
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1653
    invoke-static {v0}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_19

    invoke-static {v0}, Landroid/support/v4/widget/DrawerLayout;->m(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 1657
    :goto_18
    return-object v0

    .line 1651
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1657
    :cond_1d
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private static n(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1758
    invoke-static {p0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_10

    invoke-static {p0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private o()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1662
    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->j:Z

    if-nez v0, :cond_27

    .line 1663
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1664
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1666
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v1

    .line 1667
    :goto_15
    if-ge v7, v1, :cond_21

    .line 1668
    invoke-virtual {p0, v7}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1667
    add-int/lit8 v7, v7, 0x1

    goto :goto_15

    .line 1670
    :cond_21
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1671
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->j:Z

    .line 1673
    :cond_27
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)I
    .registers 4

    .prologue
    .line 629
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->c(Landroid/view/View;)I

    move-result v0

    .line 630
    const/4 v1, 0x3

    if-ne v0, v1, :cond_a

    .line 631
    iget v0, p0, Landroid/support/v4/widget/DrawerLayout;->K:I

    .line 635
    :goto_9
    return v0

    .line 632
    :cond_a
    const/4 v1, 0x5

    if-ne v0, v1, :cond_10

    .line 633
    iget v0, p0, Landroid/support/v4/widget/DrawerLayout;->L:I

    goto :goto_9

    .line 635
    :cond_10
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final a(I)Landroid/view/View;
    .registers 7

    .prologue
    .line 835
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    and-int/lit8 v2, v0, 0x7

    .line 837
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v3

    .line 838
    const/4 v0, 0x0

    move v1, v0

    :goto_10
    if-ge v1, v3, :cond_23

    .line 839
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 840
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->c(Landroid/view/View;)I

    move-result v4

    .line 841
    and-int/lit8 v4, v4, 0x7

    if-ne v4, v2, :cond_1f

    .line 845
    :goto_1e
    return-object v0

    .line 838
    :cond_1f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 845
    :cond_23
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public final a()V
    .registers 5

    .prologue
    const v3, 0x800003

    .line 1462
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1463
    if-nez v0, :cond_22

    .line 1464
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No drawer view found with gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/support/v4/widget/DrawerLayout;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1467
    :cond_22
    invoke-direct {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->k(Landroid/view/View;)V

    .line 1468
    return-void
.end method

.method final a(Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 778
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 779
    iget v1, v0, Landroid/support/v4/widget/ad;->b:F

    cmpl-float v1, p2, v1

    if-nez v1, :cond_d

    .line 785
    :cond_c
    :goto_c
    return-void

    .line 783
    :cond_d
    iput p2, v0, Landroid/support/v4/widget/ad;->b:F

    .line 8772
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    if-eqz v0, :cond_c

    .line 8773
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    invoke-interface {v0, p2}, Landroid/support/v4/widget/ac;->a(F)V

    goto :goto_c
.end method

.method final a(Landroid/view/View;Z)V
    .registers 7

    .prologue
    .line 755
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v1

    .line 756
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_23

    .line 757
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 758
    if-nez p2, :cond_13

    invoke-static {v2}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_17

    :cond_13
    if-eqz p2, :cond_1e

    if-ne v2, p1, :cond_1e

    .line 762
    :cond_17
    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/support/v4/view/cx;->c(Landroid/view/View;I)V

    .line 756
    :goto_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 765
    :cond_1e
    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/support/v4/view/cx;->c(Landroid/view/View;I)V

    goto :goto_1b

    .line 769
    :cond_23
    return-void
.end method

.method public final a(Ljava/lang/Object;Z)V
    .registers 4

    .prologue
    .line 424
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayout;->S:Ljava/lang/Object;

    .line 425
    iput-boolean p2, p0, Landroid/support/v4/widget/DrawerLayout;->T:Z

    .line 426
    if-nez p2, :cond_14

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_d
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->setWillNotDraw(Z)V

    .line 427
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->requestLayout()V

    .line 428
    return-void

    .line 426
    :cond_14
    const/4 v0, 0x0

    goto :goto_d
.end method

.method final a(Landroid/view/View;I)Z
    .registers 4

    .prologue
    .line 801
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->c(Landroid/view/View;)I

    move-result v0

    .line 802
    and-int/2addr v0, p2

    if-ne v0, p2, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 1612
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getDescendantFocusability()I

    move-result v0

    const/high16 v2, 0x60000

    if-ne v0, v2, :cond_a

    .line 1643
    :goto_9
    return-void

    .line 1618
    :cond_a
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v3

    move v2, v1

    move v0, v1

    .line 1620
    :goto_10
    if-ge v2, v3, :cond_2f

    .line 1621
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1622
    invoke-static {v4}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_29

    .line 1623
    invoke-static {v4}, Landroid/support/v4/widget/DrawerLayout;->l(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 1624
    const/4 v0, 0x1

    .line 1625
    invoke-virtual {v4, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1620
    :cond_26
    :goto_26
    add-int/lit8 v2, v2, 0x1

    goto :goto_10

    .line 1628
    :cond_29
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayout;->ab:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_26

    .line 1632
    :cond_2f
    if-nez v0, :cond_4e

    .line 1633
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->ab:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1634
    :goto_37
    if-ge v1, v2, :cond_4e

    .line 1635
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->ab:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1636
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_4a

    .line 1637
    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1634
    :cond_4a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_37

    .line 1642
    :cond_4e
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->ab:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_9
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 5

    .prologue
    .line 1730
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1732
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->g()Landroid/view/View;

    move-result-object v0

    .line 1733
    if-nez v0, :cond_f

    invoke-static {p1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1736
    :cond_f
    const/4 v0, 0x4

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;I)V

    .line 1747
    :goto_13
    sget-boolean v0, Landroid/support/v4/widget/DrawerLayout;->y:Z

    if-nez v0, :cond_1c

    .line 1748
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->A:Landroid/support/v4/widget/y;

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 1750
    :cond_1c
    return-void

    .line 1741
    :cond_1d
    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;I)V

    goto :goto_13
.end method

.method public final b()V
    .registers 5

    .prologue
    const v3, 0x800003

    .line 1502
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1503
    if-nez v0, :cond_22

    .line 1504
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No drawer view found with gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/support/v4/widget/DrawerLayout;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1507
    :cond_22
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->e(Landroid/view/View;)V

    .line 1508
    return-void
.end method

.method final c(Landroid/view/View;)I
    .registers 4

    .prologue
    .line 796
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget v0, v0, Landroid/support/v4/widget/ad;->a:I

    .line 797
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/v;->a(II)I

    move-result v0

    return v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 1537
    const v0, 0x800003

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1538
    if-eqz v0, :cond_e

    .line 1539
    invoke-static {v0}, Landroid/support/v4/widget/DrawerLayout;->l(Landroid/view/View;)Z

    move-result v0

    .line 1541
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3

    .prologue
    .line 1602
    instance-of v0, p1, Landroid/support/v4/widget/ad;

    if-eqz v0, :cond_c

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public computeScroll()V
    .registers 5

    .prologue
    .line 1121
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v3

    .line 1122
    const/4 v1, 0x0

    .line 1123
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_8
    if-ge v1, v3, :cond_1e

    .line 1124
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget v0, v0, Landroid/support/v4/widget/ad;->b:F

    .line 1125
    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1123
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 1127
    :cond_1e
    iput v2, p0, Landroid/support/v4/widget/DrawerLayout;->E:F

    .line 1130
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    invoke-virtual {v0}, Landroid/support/v4/widget/eg;->c()Z

    move-result v0

    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    invoke-virtual {v1}, Landroid/support/v4/widget/eg;->c()Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_32

    .line 1131
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1133
    :cond_32
    return-void
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 1568
    const v0, 0x800003

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1569
    if-eqz v0, :cond_e

    .line 1570
    invoke-static {v0}, Landroid/support/v4/widget/DrawerLayout;->m(Landroid/view/View;)Z

    move-result v0

    .line 1572
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .registers 16

    .prologue
    .line 1204
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getHeight()I

    move-result v4

    .line 1205
    invoke-static {p2}, Landroid/support/v4/widget/DrawerLayout;->j(Landroid/view/View;)Z

    move-result v5

    .line 1206
    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v2

    .line 1208
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    .line 1209
    if-eqz v5, :cond_6b

    .line 1210
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v7

    .line 1211
    const/4 v0, 0x0

    move v3, v0

    :goto_19
    if-ge v3, v7, :cond_63

    .line 1212
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1213
    if-eq v8, p2, :cond_61

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_61

    .line 9136
    invoke-virtual {v8}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 9137
    if-eqz v0, :cond_59

    .line 9138
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    const/4 v9, -0x1

    if-ne v0, v9, :cond_57

    const/4 v0, 0x1

    .line 1213
    :goto_35
    if-eqz v0, :cond_61

    invoke-static {v8}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_61

    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lt v0, v4, :cond_61

    .line 1219
    const/4 v0, 0x3

    invoke-virtual {p0, v8, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 1220
    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v0

    .line 1221
    if-le v0, v1, :cond_135

    :goto_50
    move v1, v0

    move v0, v2

    .line 1211
    :cond_52
    :goto_52
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_19

    .line 9138
    :cond_57
    const/4 v0, 0x0

    goto :goto_35

    .line 9140
    :cond_59
    const/4 v0, 0x0

    goto :goto_35

    .line 1223
    :cond_5b
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1224
    if-lt v0, v2, :cond_52

    :cond_61
    move v0, v2

    goto :goto_52

    .line 1227
    :cond_63
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getHeight()I

    move-result v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    :cond_6b
    move v0, v2

    .line 1229
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v7

    .line 1230
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1232
    iget v2, p0, Landroid/support/v4/widget/DrawerLayout;->E:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_a5

    if-eqz v5, :cond_a5

    .line 1233
    iget v2, p0, Landroid/support/v4/widget/DrawerLayout;->D:I

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    ushr-int/lit8 v2, v2, 0x18

    .line 1234
    int-to-float v2, v2

    iget v3, p0, Landroid/support/v4/widget/DrawerLayout;->E:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1235
    shl-int/lit8 v2, v2, 0x18

    iget v3, p0, Landroid/support/v4/widget/DrawerLayout;->D:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    .line 1236
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayout;->F:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1238
    int-to-float v1, v1

    const/4 v2, 0x0

    int-to-float v3, v0

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayout;->F:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1263
    :cond_a4
    :goto_a4
    return v7

    .line 1239
    :cond_a5
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->Q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_ea

    const/4 v0, 0x3

    invoke-virtual {p0, p2, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_ea

    .line 1241
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1242
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v1

    .line 1243
    iget-object v2, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 9448
    iget v2, v2, Landroid/support/v4/widget/eg;->t:I

    .line 1244
    const/4 v3, 0x0

    int-to-float v4, v1

    int-to-float v2, v2

    div-float v2, v4, v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1246
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayout;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    add-int/2addr v0, v1

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v3, v1, v4, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1248
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->Q:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1249
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_a4

    .line 1250
    :cond_ea
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->R:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a4

    const/4 v0, 0x5

    invoke-virtual {p0, p2, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 1252
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1253
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1254
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v2

    sub-int/2addr v2, v1

    .line 1255
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    .line 10448
    iget v3, v3, Landroid/support/v4/widget/eg;->t:I

    .line 1256
    const/4 v4, 0x0

    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1258
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayout;->R:Landroid/graphics/drawable/Drawable;

    sub-int v0, v1, v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v3, v0, v4, v1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1260
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->R:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1261
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_a4

    :cond_135
    move v0, v1

    goto/16 :goto_50
.end method

.method public final e(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 1476
    invoke-static {p1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1477
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a sliding drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1480
    :cond_21
    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->J:Z

    if-eqz v0, :cond_35

    .line 1481
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 1482
    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v4/widget/ad;->b:F

    .line 1483
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/widget/ad;->d:Z

    .line 1492
    :goto_31
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1493
    return-void

    .line 1485
    :cond_35
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 1486
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;II)Z

    goto :goto_31

    .line 1489
    :cond_4b
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;II)Z

    goto :goto_31
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 1588
    new-instance v0, Landroid/support/v4/widget/ad;

    invoke-direct {v0, v1, v1}, Landroid/support/v4/widget/ad;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 4

    .prologue
    .line 1607
    new-instance v0, Landroid/support/v4/widget/ad;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/widget/ad;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 1593
    instance-of v0, p1, Landroid/support/v4/widget/ad;

    if-eqz v0, :cond_c

    new-instance v0, Landroid/support/v4/widget/ad;

    check-cast p1, Landroid/support/v4/widget/ad;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/ad;-><init>(Landroid/support/v4/widget/ad;)V

    :goto_b
    return-object v0

    :cond_c
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_18

    new-instance v0, Landroid/support/v4/widget/ad;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/ad;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_b

    :cond_18
    new-instance v0, Landroid/support/v4/widget/ad;

    invoke-direct {v0, p1}, Landroid/support/v4/widget/ad;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b
.end method

.method public getDrawerElevation()F
    .registers 2

    .prologue
    .line 412
    sget-boolean v0, Landroid/support/v4/widget/DrawerLayout;->z:Z

    if-eqz v0, :cond_7

    .line 413
    iget v0, p0, Landroid/support/v4/widget/DrawerLayout;->B:F

    .line 415
    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public getStatusBarBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 1160
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->P:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 872
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 873
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->J:Z

    .line 874
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 866
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 867
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->J:Z

    .line 868
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 1192
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 1193
    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->T:Z

    if-eqz v0, :cond_24

    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->P:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_24

    .line 1194
    sget-object v0, Landroid/support/v4/widget/DrawerLayout;->n:Landroid/support/v4/widget/z;

    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->S:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/z;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1195
    if-lez v0, :cond_24

    .line 1196
    iget-object v1, p0, Landroid/support/v4/widget/DrawerLayout;->P:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v2

    invoke-virtual {v1, v3, v3, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1197
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->P:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1200
    :cond_24
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1279
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1282
    iget-object v3, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/eg;->a(Landroid/view/MotionEvent;)Z

    move-result v3

    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    invoke-virtual {v4, p1}, Landroid/support/v4/widget/eg;->a(Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v4, v3

    .line 1287
    packed-switch v0, :pswitch_data_be

    :cond_16
    :goto_16
    move v0, v2

    .line 1321
    :goto_17
    if-nez v4, :cond_37

    if-nez v0, :cond_37

    .line 11576
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v4

    move v3, v2

    .line 11577
    :goto_20
    if-ge v3, v4, :cond_b9

    .line 11578
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 11579
    iget-boolean v0, v0, Landroid/support/v4/widget/ad;->c:Z

    if-eqz v0, :cond_b4

    move v0, v1

    .line 1321
    :goto_31
    if-nez v0, :cond_37

    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->j:Z

    if-eqz v0, :cond_38

    :cond_37
    move v2, v1

    :cond_38
    return v2

    .line 1289
    :pswitch_39
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1290
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1291
    iput v0, p0, Landroid/support/v4/widget/DrawerLayout;->N:F

    .line 1292
    iput v3, p0, Landroid/support/v4/widget/DrawerLayout;->O:F

    .line 1293
    iget v5, p0, Landroid/support/v4/widget/DrawerLayout;->E:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_bc

    .line 1294
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    float-to-int v0, v0

    float-to-int v3, v3

    invoke-virtual {v5, v0, v3}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v0

    .line 1295
    if-eqz v0, :cond_bc

    invoke-static {v0}, Landroid/support/v4/widget/DrawerLayout;->j(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_bc

    move v0, v1

    .line 1299
    :goto_5d
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->M:Z

    .line 1300
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->j:Z

    goto :goto_17

    .line 1306
    :pswitch_62
    iget-object v5, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 11309
    iget-object v0, v5, Landroid/support/v4/widget/eg;->o:[F

    array-length v6, v0

    move v0, v2

    .line 11310
    :goto_68
    if-ge v0, v6, :cond_a9

    .line 11334
    invoke-virtual {v5, v0}, Landroid/support/v4/widget/eg;->a(I)Z

    move-result v3

    if-eqz v3, :cond_a4

    .line 11341
    iget-object v3, v5, Landroid/support/v4/widget/eg;->q:[F

    aget v3, v3, v0

    iget-object v7, v5, Landroid/support/v4/widget/eg;->o:[F

    aget v7, v7, v0

    sub-float/2addr v3, v7

    .line 11342
    iget-object v7, v5, Landroid/support/v4/widget/eg;->r:[F

    aget v7, v7, v0

    iget-object v8, v5, Landroid/support/v4/widget/eg;->p:[F

    aget v8, v8, v0

    sub-float/2addr v7, v8

    .line 11345
    mul-float/2addr v3, v3

    mul-float/2addr v7, v7

    add-float/2addr v3, v7

    iget v7, v5, Landroid/support/v4/widget/eg;->n:I

    iget v8, v5, Landroid/support/v4/widget/eg;->n:I

    mul-int/2addr v7, v8

    int-to-float v7, v7

    cmpl-float v3, v3, v7

    if-lez v3, :cond_a2

    move v3, v1

    .line 11311
    :goto_90
    if-eqz v3, :cond_a6

    move v0, v1

    .line 1306
    :goto_93
    if-eqz v0, :cond_16

    .line 1307
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->G:Landroid/support/v4/widget/ag;

    invoke-virtual {v0}, Landroid/support/v4/widget/ag;->a()V

    .line 1308
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->H:Landroid/support/v4/widget/ag;

    invoke-virtual {v0}, Landroid/support/v4/widget/ag;->a()V

    move v0, v2

    goto/16 :goto_17

    :cond_a2
    move v3, v2

    .line 11345
    goto :goto_90

    :cond_a4
    move v3, v2

    .line 11351
    goto :goto_90

    .line 11310
    :cond_a6
    add-int/lit8 v0, v0, 0x1

    goto :goto_68

    :cond_a9
    move v0, v2

    .line 11315
    goto :goto_93

    .line 1315
    :pswitch_ab
    invoke-direct {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Z)V

    .line 1316
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->M:Z

    .line 1317
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->j:Z

    goto/16 :goto_16

    .line 11577
    :cond_b4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_20

    :cond_b9
    move v0, v2

    .line 11583
    goto/16 :goto_31

    :cond_bc
    move v0, v2

    goto :goto_5d

    .line 1287
    :pswitch_data_be
    .packed-switch 0x0
        :pswitch_39
        :pswitch_ab
        :pswitch_62
        :pswitch_ab
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 1677
    const/4 v1, 0x4

    if-ne p1, v1, :cond_13

    .line 12646
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->n()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_11

    move v1, v0

    .line 1677
    :goto_b
    if-eqz v1, :cond_13

    .line 1678
    invoke-static {p2}, Landroid/support/v4/view/ab;->c(Landroid/view/KeyEvent;)V

    .line 1681
    :goto_10
    return v0

    .line 12646
    :cond_11
    const/4 v1, 0x0

    goto :goto_b

    .line 1681
    :cond_13
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_10
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 1686
    const/4 v1, 0x4

    if-ne p1, v1, :cond_17

    .line 1687
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->n()Landroid/view/View;

    move-result-object v1

    .line 1688
    if-eqz v1, :cond_13

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;)I

    move-result v2

    if-nez v2, :cond_13

    .line 13393
    invoke-direct {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Z)V

    .line 1691
    :cond_13
    if-eqz v1, :cond_16

    const/4 v0, 0x1

    .line 1693
    :cond_16
    :goto_16
    return v0

    :cond_17
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_16
.end method

.method protected onLayout(ZIIII)V
    .registers 20

    .prologue
    .line 1030
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->I:Z

    .line 1031
    sub-int v6, p4, p2

    .line 1032
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v7

    .line 1033
    const/4 v0, 0x0

    move v5, v0

    :goto_b
    if-ge v5, v7, :cond_cb

    .line 1034
    invoke-virtual {p0, v5}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1036
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3a

    .line 1040
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 1042
    invoke-static {v8}, Landroid/support/v4/widget/DrawerLayout;->j(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 1043
    iget v1, v0, Landroid/support/v4/widget/ad;->leftMargin:I

    iget v2, v0, Landroid/support/v4/widget/ad;->topMargin:I

    iget v3, v0, Landroid/support/v4/widget/ad;->leftMargin:I

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v0, v0, Landroid/support/v4/widget/ad;->topMargin:I

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v8, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 1033
    :cond_3a
    :goto_3a
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_b

    .line 1047
    :cond_3e
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 1048
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 1052
    const/4 v1, 0x3

    invoke-virtual {p0, v8, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_87

    .line 1053
    neg-int v1, v9

    int-to-float v2, v9

    iget v3, v0, Landroid/support/v4/widget/ad;->b:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v2, v1

    .line 1054
    add-int v1, v9, v2

    int-to-float v1, v1

    int-to-float v3, v9

    div-float/2addr v1, v3

    .line 1060
    :goto_59
    iget v3, v0, Landroid/support/v4/widget/ad;->b:F

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_94

    const/4 v3, 0x1

    .line 1062
    :goto_60
    iget v4, v0, Landroid/support/v4/widget/ad;->a:I

    and-int/lit8 v4, v4, 0x70

    .line 1064
    sparse-switch v4, :sswitch_data_d2

    .line 1067
    iget v4, v0, Landroid/support/v4/widget/ad;->topMargin:I

    add-int/2addr v9, v2

    iget v11, v0, Landroid/support/v4/widget/ad;->topMargin:I

    add-int/2addr v10, v11

    invoke-virtual {v8, v2, v4, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 1098
    :goto_70
    if-eqz v3, :cond_75

    .line 1099
    invoke-virtual {p0, v8, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;F)V

    .line 1102
    :cond_75
    iget v0, v0, Landroid/support/v4/widget/ad;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_c9

    const/4 v0, 0x0

    .line 1103
    :goto_7d
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_3a

    .line 1104
    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3a

    .line 1056
    :cond_87
    int-to-float v1, v9

    iget v2, v0, Landroid/support/v4/widget/ad;->b:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int v2, v6, v1

    .line 1057
    sub-int v1, v6, v2

    int-to-float v1, v1

    int-to-float v3, v9

    div-float/2addr v1, v3

    goto :goto_59

    .line 1060
    :cond_94
    const/4 v3, 0x0

    goto :goto_60

    .line 1073
    :sswitch_96
    sub-int v4, p5, p3

    .line 1074
    iget v10, v0, Landroid/support/v4/widget/ad;->bottomMargin:I

    sub-int v10, v4, v10

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    sub-int/2addr v10, v11

    add-int/2addr v9, v2

    iget v11, v0, Landroid/support/v4/widget/ad;->bottomMargin:I

    sub-int/2addr v4, v11

    invoke-virtual {v8, v2, v10, v9, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_70

    .line 1082
    :sswitch_a9
    sub-int v11, p5, p3

    .line 1083
    sub-int v4, v11, v10

    div-int/lit8 v4, v4, 0x2

    .line 1087
    iget v12, v0, Landroid/support/v4/widget/ad;->topMargin:I

    if-ge v4, v12, :cond_bb

    .line 1088
    iget v4, v0, Landroid/support/v4/widget/ad;->topMargin:I

    .line 1092
    :cond_b5
    :goto_b5
    add-int/2addr v9, v2

    add-int/2addr v10, v4

    invoke-virtual {v8, v2, v4, v9, v10}, Landroid/view/View;->layout(IIII)V

    goto :goto_70

    .line 1089
    :cond_bb
    add-int v12, v4, v10

    iget v13, v0, Landroid/support/v4/widget/ad;->bottomMargin:I

    sub-int v13, v11, v13

    if-le v12, v13, :cond_b5

    .line 1090
    iget v4, v0, Landroid/support/v4/widget/ad;->bottomMargin:I

    sub-int v4, v11, v4

    sub-int/2addr v4, v10

    goto :goto_b5

    .line 1102
    :cond_c9
    const/4 v0, 0x4

    goto :goto_7d

    .line 1108
    :cond_cb
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->I:Z

    .line 1109
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->J:Z

    .line 1110
    return-void

    .line 1064
    :sswitch_data_d2
    .sparse-switch
        0x10 -> :sswitch_a9
        0x50 -> :sswitch_96
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .registers 15

    .prologue
    const/16 v1, 0x12c

    const/4 v4, 0x0

    const/high16 v7, -0x80000000

    const/high16 v11, 0x40000000    # 2.0f

    .line 878
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 879
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 880
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 881
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 883
    if-ne v3, v11, :cond_1b

    if-eq v5, v11, :cond_124

    .line 884
    :cond_1b
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->isInEditMode()Z

    move-result v6

    if-eqz v6, :cond_8d

    .line 889
    if-eq v3, v7, :cond_26

    .line 891
    if-nez v3, :cond_26

    move v2, v1

    .line 895
    :cond_26
    if-eq v5, v7, :cond_124

    .line 898
    if-nez v5, :cond_124

    .line 908
    :goto_2a
    invoke-virtual {p0, v2, v1}, Landroid/support/v4/widget/DrawerLayout;->setMeasuredDimension(II)V

    .line 910
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->S:Ljava/lang/Object;

    if-eqz v0, :cond_95

    invoke-static {p0}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_95

    const/4 v0, 0x1

    move v3, v0

    .line 911
    :goto_39
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v5

    .line 915
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v6

    .line 916
    :goto_41
    if-ge v4, v6, :cond_123

    .line 917
    invoke-virtual {p0, v4}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 919
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v8, 0x8

    if-eq v0, v8, :cond_8a

    .line 923
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 925
    if-eqz v3, :cond_6a

    .line 926
    iget v8, v0, Landroid/support/v4/widget/ad;->a:I

    invoke-static {v8, v5}, Landroid/support/v4/view/v;->a(II)I

    move-result v8

    .line 927
    invoke-static {v7}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_97

    .line 928
    sget-object v9, Landroid/support/v4/widget/DrawerLayout;->n:Landroid/support/v4/widget/z;

    iget-object v10, p0, Landroid/support/v4/widget/DrawerLayout;->S:Ljava/lang/Object;

    invoke-interface {v9, v7, v10, v8}, Landroid/support/v4/widget/z;->a(Landroid/view/View;Ljava/lang/Object;I)V

    .line 934
    :cond_6a
    :goto_6a
    invoke-static {v7}, Landroid/support/v4/widget/DrawerLayout;->j(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_9f

    .line 936
    iget v8, v0, Landroid/support/v4/widget/ad;->leftMargin:I

    sub-int v8, v2, v8

    iget v9, v0, Landroid/support/v4/widget/ad;->rightMargin:I

    sub-int/2addr v8, v9

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 938
    iget v9, v0, Landroid/support/v4/widget/ad;->topMargin:I

    sub-int v9, v1, v9

    iget v0, v0, Landroid/support/v4/widget/ad;->bottomMargin:I

    sub-int v0, v9, v0

    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 940
    invoke-virtual {v7, v8, v0}, Landroid/view/View;->measure(II)V

    .line 916
    :cond_8a
    :goto_8a
    add-int/lit8 v4, v4, 0x1

    goto :goto_41

    .line 903
    :cond_8d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DrawerLayout must be measured with MeasureSpec.EXACTLY."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_95
    move v3, v4

    .line 910
    goto :goto_39

    .line 930
    :cond_97
    sget-object v9, Landroid/support/v4/widget/DrawerLayout;->n:Landroid/support/v4/widget/z;

    iget-object v10, p0, Landroid/support/v4/widget/DrawerLayout;->S:Ljava/lang/Object;

    invoke-interface {v9, v0, v10, v8}, Landroid/support/v4/widget/z;->a(Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/Object;I)V

    goto :goto_6a

    .line 941
    :cond_9f
    invoke-static {v7}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_fe

    .line 942
    sget-boolean v8, Landroid/support/v4/widget/DrawerLayout;->z:Z

    if-eqz v8, :cond_b8

    .line 943
    invoke-static {v7}, Landroid/support/v4/view/cx;->r(Landroid/view/View;)F

    move-result v8

    iget v9, p0, Landroid/support/v4/widget/DrawerLayout;->B:F

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_b8

    .line 944
    iget v8, p0, Landroid/support/v4/widget/DrawerLayout;->B:F

    invoke-static {v7, v8}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 947
    :cond_b8
    invoke-virtual {p0, v7}, Landroid/support/v4/widget/DrawerLayout;->c(Landroid/view/View;)I

    move-result v8

    and-int/lit8 v8, v8, 0x7

    .line 949
    and-int/lit8 v9, v8, 0x0

    if-eqz v9, :cond_e1

    .line 950
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Child drawer has absolute gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Landroid/support/v4/widget/DrawerLayout;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but this DrawerLayout already has a drawer view along that edge"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 954
    :cond_e1
    iget v8, p0, Landroid/support/v4/widget/DrawerLayout;->C:I

    iget v9, v0, Landroid/support/v4/widget/ad;->leftMargin:I

    add-int/2addr v8, v9

    iget v9, v0, Landroid/support/v4/widget/ad;->rightMargin:I

    add-int/2addr v8, v9

    iget v9, v0, Landroid/support/v4/widget/ad;->width:I

    invoke-static {p1, v8, v9}, Landroid/support/v4/widget/DrawerLayout;->getChildMeasureSpec(III)I

    move-result v8

    .line 957
    iget v9, v0, Landroid/support/v4/widget/ad;->topMargin:I

    iget v10, v0, Landroid/support/v4/widget/ad;->bottomMargin:I

    add-int/2addr v9, v10

    iget v0, v0, Landroid/support/v4/widget/ad;->height:I

    invoke-static {p2, v9, v0}, Landroid/support/v4/widget/DrawerLayout;->getChildMeasureSpec(III)I

    move-result v0

    .line 960
    invoke-virtual {v7, v8, v0}, Landroid/view/View;->measure(II)V

    goto :goto_8a

    .line 962
    :cond_fe
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Child "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 967
    :cond_123
    return-void

    :cond_124
    move v1, v0

    goto/16 :goto_2a
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4

    .prologue
    .line 1698
    check-cast p1, Landroid/support/v4/widget/DrawerLayout$SavedState;

    .line 1699
    invoke-virtual {p1}, Landroid/support/v4/widget/DrawerLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1701
    iget v0, p1, Landroid/support/v4/widget/DrawerLayout$SavedState;->a:I

    if-eqz v0, :cond_18

    .line 1702
    iget v0, p1, Landroid/support/v4/widget/DrawerLayout$SavedState;->a:I

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1703
    if-eqz v0, :cond_18

    .line 1704
    invoke-direct {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->k(Landroid/view/View;)V

    .line 1708
    :cond_18
    iget v0, p1, Landroid/support/v4/widget/DrawerLayout$SavedState;->b:I

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/DrawerLayout;->b(II)V

    .line 1709
    iget v0, p1, Landroid/support/v4/widget/DrawerLayout$SavedState;->c:I

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/DrawerLayout;->b(II)V

    .line 1710
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .registers 2

    .prologue
    .line 1187
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->h()V

    .line 1188
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 3

    .prologue
    .line 1714
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1715
    new-instance v1, Landroid/support/v4/widget/DrawerLayout$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/DrawerLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1717
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->g()Landroid/view/View;

    move-result-object v0

    .line 1718
    if-eqz v0, :cond_19

    .line 1719
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    iget v0, v0, Landroid/support/v4/widget/ad;->a:I

    iput v0, v1, Landroid/support/v4/widget/DrawerLayout$SavedState;->a:I

    .line 1722
    :cond_19
    iget v0, p0, Landroid/support/v4/widget/DrawerLayout;->K:I

    iput v0, v1, Landroid/support/v4/widget/DrawerLayout$SavedState;->b:I

    .line 1723
    iget v0, p0, Landroid/support/v4/widget/DrawerLayout;->L:I

    iput v0, v1, Landroid/support/v4/widget/DrawerLayout$SavedState;->c:I

    .line 1725
    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1326
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/eg;->b(Landroid/view/MotionEvent;)V

    .line 1327
    iget-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/eg;->b(Landroid/view/MotionEvent;)V

    .line 1329
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1332
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_74

    .line 1373
    :goto_15
    :pswitch_15
    return v1

    .line 1334
    :pswitch_16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1335
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1336
    iput v0, p0, Landroid/support/v4/widget/DrawerLayout;->N:F

    .line 1337
    iput v3, p0, Landroid/support/v4/widget/DrawerLayout;->O:F

    .line 1338
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->M:Z

    .line 1339
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->j:Z

    goto :goto_15

    .line 1344
    :pswitch_27
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1345
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1347
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    float-to-int v5, v0

    float-to-int v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v4

    .line 1348
    if-eqz v4, :cond_71

    invoke-static {v4}, Landroid/support/v4/widget/DrawerLayout;->j(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_71

    .line 1349
    iget v4, p0, Landroid/support/v4/widget/DrawerLayout;->N:F

    sub-float/2addr v0, v4

    .line 1350
    iget v4, p0, Landroid/support/v4/widget/DrawerLayout;->O:F

    sub-float/2addr v3, v4

    .line 1351
    iget-object v4, p0, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 12490
    iget v4, v4, Landroid/support/v4/widget/eg;->n:I

    .line 1352
    mul-float/2addr v0, v0

    mul-float/2addr v3, v3

    add-float/2addr v0, v3

    mul-int v3, v4, v4

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_71

    .line 1354
    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayout;->g()Landroid/view/View;

    move-result-object v0

    .line 1355
    if-eqz v0, :cond_71

    .line 1356
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_67

    move v0, v1

    .line 1360
    :goto_61
    invoke-direct {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Z)V

    .line 1361
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->M:Z

    goto :goto_15

    :cond_67
    move v0, v2

    .line 1356
    goto :goto_61

    .line 1366
    :pswitch_69
    invoke-direct {p0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Z)V

    .line 1367
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->M:Z

    .line 1368
    iput-boolean v2, p0, Landroid/support/v4/widget/DrawerLayout;->j:Z

    goto :goto_15

    :cond_71
    move v0, v1

    goto :goto_61

    .line 1332
    nop

    :pswitch_data_74
    .packed-switch 0x0
        :pswitch_16
        :pswitch_27
        :pswitch_15
        :pswitch_69
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 3

    .prologue
    .line 1381
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 1383
    iput-boolean p1, p0, Landroid/support/v4/widget/DrawerLayout;->M:Z

    .line 1384
    if-eqz p1, :cond_b

    .line 1385
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Z)V

    .line 1387
    :cond_b
    return-void
.end method

.method public requestLayout()V
    .registers 2

    .prologue
    .line 1114
    iget-boolean v0, p0, Landroid/support/v4/widget/DrawerLayout;->I:Z

    if-nez v0, :cond_7

    .line 1115
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1117
    :cond_7
    return-void
.end method

.method public setDrawerElevation(F)V
    .registers 5

    .prologue
    .line 395
    iput p1, p0, Landroid/support/v4/widget/DrawerLayout;->B:F

    .line 396
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1b

    .line 397
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 398
    invoke-static {v1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 399
    iget v2, p0, Landroid/support/v4/widget/DrawerLayout;->B:F

    invoke-static {v1, v2}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 396
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 402
    :cond_1b
    return-void
.end method

.method public setDrawerListener(Landroid/support/v4/widget/ac;)V
    .registers 2

    .prologue
    .line 504
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    .line 505
    return-void
.end method

.method public setDrawerLockMode(I)V
    .registers 3

    .prologue
    .line 521
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(II)V

    .line 522
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/DrawerLayout;->b(II)V

    .line 523
    return-void
.end method

.method public setScrimColor(I)V
    .registers 2
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 493
    iput p1, p0, Landroid/support/v4/widget/DrawerLayout;->D:I

    .line 494
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 495
    return-void
.end method

.method public setStatusBarBackground(I)V
    .registers 3

    .prologue
    .line 1170
    if-eqz p1, :cond_10

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_a
    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->P:Landroid/graphics/drawable/Drawable;

    .line 1171
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1172
    return-void

    .line 1170
    :cond_10
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 1150
    iput-object p1, p0, Landroid/support/v4/widget/DrawerLayout;->P:Landroid/graphics/drawable/Drawable;

    .line 1151
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1152
    return-void
.end method

.method public setStatusBarBackgroundColor(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 1182
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->P:Landroid/graphics/drawable/Drawable;

    .line 1183
    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1184
    return-void
.end method
