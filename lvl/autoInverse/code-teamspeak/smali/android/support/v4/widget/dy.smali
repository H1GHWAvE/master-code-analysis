.class public final Landroid/support/v4/widget/dy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/widget/ec;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 129
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 130
    const/16 v1, 0x12

    if-lt v0, v1, :cond_e

    .line 131
    new-instance v0, Landroid/support/v4/widget/eb;

    invoke-direct {v0}, Landroid/support/v4/widget/eb;-><init>()V

    sput-object v0, Landroid/support/v4/widget/dy;->a:Landroid/support/v4/widget/ec;

    .line 137
    :goto_d
    return-void

    .line 132
    :cond_e
    const/16 v1, 0x11

    if-lt v0, v1, :cond_1a

    .line 133
    new-instance v0, Landroid/support/v4/widget/ea;

    invoke-direct {v0}, Landroid/support/v4/widget/ea;-><init>()V

    sput-object v0, Landroid/support/v4/widget/dy;->a:Landroid/support/v4/widget/ec;

    goto :goto_d

    .line 135
    :cond_1a
    new-instance v0, Landroid/support/v4/widget/dz;

    invoke-direct {v0}, Landroid/support/v4/widget/dz;-><init>()V

    sput-object v0, Landroid/support/v4/widget/dy;->a:Landroid/support/v4/widget/ec;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method private static a(Landroid/widget/TextView;IIII)V
    .registers 11
    .param p0    # Landroid/widget/TextView;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 200
    sget-object v0, Landroid/support/v4/widget/dy;->a:Landroid/support/v4/widget/ec;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/widget/ec;->a(Landroid/widget/TextView;IIII)V

    .line 201
    return-void
.end method

.method public static a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p0    # Landroid/widget/TextView;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 157
    sget-object v0, Landroid/support/v4/widget/dy;->a:Landroid/support/v4/widget/ec;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/ec;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V

    .line 158
    return-void
.end method

.method private static a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .registers 11
    .param p0    # Landroid/widget/TextView;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 177
    sget-object v0, Landroid/support/v4/widget/dy;->a:Landroid/support/v4/widget/ec;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/widget/ec;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 178
    return-void
.end method
