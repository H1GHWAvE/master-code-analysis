.class final Landroid/support/v4/e/a/y;
.super Landroid/support/v4/e/a/v;
.source "SourceFile"


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    .registers 3

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/support/v4/e/a/v;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 39
    return-void
.end method


# virtual methods
.method final a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 12

    .prologue
    .line 57
    const/4 v5, 0x0

    move v0, p1

    move v1, p2

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/v;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 59
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 43
    iget-object v0, p0, Landroid/support/v4/e/a/y;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_c

    .line 44
    iget-object v0, p0, Landroid/support/v4/e/a/y;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, p1}, Landroid/support/v4/e/a;->a(Landroid/graphics/Bitmap;Z)V

    .line 45
    invoke-virtual {p0}, Landroid/support/v4/e/a/y;->invalidateSelf()V

    .line 47
    :cond_c
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Landroid/support/v4/e/a/y;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v4/e/a/y;->a:Landroid/graphics/Bitmap;

    invoke-static {v0}, Landroid/support/v4/e/a;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method
