.class Landroid/support/v4/e/a/e;
.super Landroid/support/v4/e/a/d;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 117
    invoke-direct {p0}, Landroid/support/v4/e/a/d;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Landroid/graphics/drawable/Drawable;I)V
    .registers 9

    .prologue
    const/4 v5, 0x1

    .line 1041
    sget-boolean v0, Landroid/support/v4/e/a/n;->b:Z

    if-nez v0, :cond_1d

    .line 1043
    :try_start_5
    const-class v0, Landroid/graphics/drawable/Drawable;

    const-string v1, "setLayoutDirection"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 1045
    sput-object v0, Landroid/support/v4/e/a/n;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_1b
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_1b} :catch_31

    .line 1049
    :goto_1b
    sput-boolean v5, Landroid/support/v4/e/a/n;->b:Z

    .line 1052
    :cond_1d
    sget-object v0, Landroid/support/v4/e/a/n;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_30

    .line 1054
    :try_start_21
    sget-object v0, Landroid/support/v4/e/a/n;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_30} :catch_3a

    .line 1058
    :cond_30
    :goto_30
    return-void

    .line 1046
    :catch_31
    move-exception v0

    .line 1047
    const-string v1, "DrawableCompatJellybeanMr1"

    const-string v2, "Failed to retrieve setLayoutDirection(int) method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1b

    .line 1055
    :catch_3a
    move-exception v0

    .line 1056
    const-string v1, "DrawableCompatJellybeanMr1"

    const-string v2, "Failed to invoke setLayoutDirection(int) via reflection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1057
    const/4 v0, 0x0

    sput-object v0, Landroid/support/v4/e/a/n;->a:Ljava/lang/reflect/Method;

    goto :goto_30
.end method

.method public d(Landroid/graphics/drawable/Drawable;)I
    .registers 3

    .prologue
    .line 125
    invoke-static {p1}, Landroid/support/v4/e/a/n;->a(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    .line 126
    if-gez v0, :cond_7

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method
