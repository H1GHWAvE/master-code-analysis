.class final Landroid/support/v4/e/a/o;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/graphics/drawable/Drawable;Z)V
    .registers 2

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 29
    return-void
.end method

.method private static a(Landroid/graphics/drawable/Drawable;)Z
    .registers 2

    .prologue
    .line 32
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->isAutoMirrored()Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 36
    instance-of v0, p0, Landroid/support/v4/e/a/t;

    if-nez v0, :cond_a

    .line 37
    new-instance v0, Landroid/support/v4/e/a/t;

    invoke-direct {v0, p0}, Landroid/support/v4/e/a/t;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object p0, v0

    .line 39
    :cond_a
    return-object p0
.end method
