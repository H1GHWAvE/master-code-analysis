.class final Landroid/support/v4/c/b;
.super Landroid/support/v4/c/ai;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Ljava/util/concurrent/CountDownLatch;

.field b:Z

.field final synthetic c:Landroid/support/v4/c/a;


# direct methods
.method constructor <init>(Landroid/support/v4/c/a;)V
    .registers 4

    .prologue
    .line 42
    iput-object p1, p0, Landroid/support/v4/c/b;->c:Landroid/support/v4/c/a;

    invoke-direct {p0}, Landroid/support/v4/c/ai;-><init>()V

    .line 43
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/c/b;->a:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private varargs c()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 54
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/c/b;->c:Landroid/support/v4/c/a;

    .line 1296
    invoke-virtual {v0}, Landroid/support/v4/c/a;->d()Ljava/lang/Object;
    :try_end_5
    .catch Landroid/support/v4/i/h; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 68
    :goto_6
    return-object v0

    .line 57
    :catch_7
    move-exception v0

    .line 2278
    iget-object v1, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v1

    .line 58
    if-nez v1, :cond_11

    .line 65
    throw v0

    .line 68
    :cond_11
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private d()V
    .registers 2

    .prologue
    .line 105
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/c/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_5} :catch_6

    .line 109
    :goto_5
    return-void

    :catch_6
    move-exception v0

    goto :goto_5
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/support/v4/c/b;->c()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 77
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/c/b;->c:Landroid/support/v4/c/a;

    .line 3237
    iget-object v1, v0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    if-eq v1, p0, :cond_f

    .line 3239
    invoke-virtual {v0, p0, p1}, Landroid/support/v4/c/a;->a(Landroid/support/v4/c/b;Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_0 .. :try_end_9} :catchall_17

    .line 79
    :goto_9
    iget-object v0, p0, Landroid/support/v4/c/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 80
    return-void

    .line 4235
    :cond_f
    :try_start_f
    iget-boolean v1, v0, Landroid/support/v4/c/aa;->u:Z

    .line 3241
    if-eqz v1, :cond_1e

    .line 3243
    invoke-virtual {v0, p1}, Landroid/support/v4/c/a;->a(Ljava/lang/Object;)V
    :try_end_16
    .catchall {:try_start_f .. :try_end_16} :catchall_17

    goto :goto_9

    .line 79
    :catchall_17
    move-exception v0

    iget-object v1, p0, Landroid/support/v4/c/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0

    .line 4457
    :cond_1e
    const/4 v1, 0x0

    :try_start_1f
    iput-boolean v1, v0, Landroid/support/v4/c/aa;->x:Z

    .line 3246
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/support/v4/c/a;->f:J

    .line 3247
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    .line 3249
    invoke-virtual {v0, p1}, Landroid/support/v4/c/a;->b(Ljava/lang/Object;)V
    :try_end_2d
    .catchall {:try_start_1f .. :try_end_2d} :catchall_17

    goto :goto_9
.end method

.method protected final b(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 88
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/c/b;->c:Landroid/support/v4/c/a;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/c/a;->a(Landroid/support/v4/c/b;Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_b

    .line 90
    iget-object v0, p0, Landroid/support/v4/c/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 91
    return-void

    .line 90
    :catchall_b
    move-exception v0

    iget-object v1, p0, Landroid/support/v4/c/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method

.method public final run()V
    .registers 2

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/c/b;->b:Z

    .line 99
    iget-object v0, p0, Landroid/support/v4/c/b;->c:Landroid/support/v4/c/a;

    invoke-virtual {v0}, Landroid/support/v4/c/a;->c()V

    .line 100
    return-void
.end method
