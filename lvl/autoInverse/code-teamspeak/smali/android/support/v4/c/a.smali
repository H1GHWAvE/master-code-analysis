.class public abstract Landroid/support/v4/c/a;
.super Landroid/support/v4/c/aa;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String; = "AsyncTaskLoader"

.field static final b:Z


# instance fields
.field volatile c:Landroid/support/v4/c/b;

.field volatile d:Landroid/support/v4/c/b;

.field e:J

.field f:J

.field g:Landroid/os/Handler;

.field private final h:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 122
    sget-object v0, Landroid/support/v4/c/ai;->d:Ljava/util/concurrent/Executor;

    invoke-direct {p0, p1, v0}, Landroid/support/v4/c/a;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V

    .line 123
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
    .registers 5

    .prologue
    .line 126
    invoke-direct {p0, p1}, Landroid/support/v4/c/aa;-><init>(Landroid/content/Context;)V

    .line 118
    const-wide/16 v0, -0x2710

    iput-wide v0, p0, Landroid/support/v4/c/a;->f:J

    .line 127
    iput-object p2, p0, Landroid/support/v4/c/a;->h:Ljava/util/concurrent/Executor;

    .line 128
    return-void
.end method

.method private a(J)V
    .registers 6

    .prologue
    .line 138
    iput-wide p1, p0, Landroid/support/v4/c/a;->e:J

    .line 139
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_f

    .line 140
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v4/c/a;->g:Landroid/os/Handler;

    .line 142
    :cond_f
    return-void
.end method

.method private b(Landroid/support/v4/c/b;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 237
    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    if-eq v0, p1, :cond_8

    .line 239
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/c/a;->a(Landroid/support/v4/c/b;Ljava/lang/Object;)V

    .line 252
    :goto_7
    return-void

    .line 2235
    :cond_8
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->u:Z

    .line 241
    if-eqz v0, :cond_10

    .line 243
    invoke-virtual {p0, p2}, Landroid/support/v4/c/a;->a(Ljava/lang/Object;)V

    goto :goto_7

    .line 2457
    :cond_10
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->x:Z

    .line 246
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/c/a;->f:J

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    .line 249
    invoke-virtual {p0, p2}, Landroid/support/v4/c/a;->b(Ljava/lang/Object;)V

    goto :goto_7
.end method

.method private o()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 296
    invoke-virtual {p0}, Landroid/support/v4/c/a;->d()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private p()Z
    .registers 2

    .prologue
    .line 321
    iget-object v0, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private q()V
    .registers 2

    .prologue
    .line 335
    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    .line 336
    if-eqz v0, :cond_9

    .line 3105
    :try_start_4
    iget-object v0, v0, Landroid/support/v4/c/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_9} :catch_a

    .line 3108
    :cond_9
    :goto_9
    return-void

    :catch_a
    move-exception v0

    goto :goto_9
.end method


# virtual methods
.method protected final a()V
    .registers 2

    .prologue
    .line 146
    invoke-super {p0}, Landroid/support/v4/c/aa;->a()V

    .line 147
    invoke-virtual {p0}, Landroid/support/v4/c/a;->j()Z

    .line 148
    new-instance v0, Landroid/support/v4/c/b;

    invoke-direct {v0, p0}, Landroid/support/v4/c/b;-><init>(Landroid/support/v4/c/a;)V

    iput-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    .line 150
    invoke-virtual {p0}, Landroid/support/v4/c/a;->c()V

    .line 151
    return-void
.end method

.method final a(Landroid/support/v4/c/b;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 224
    invoke-virtual {p0, p2}, Landroid/support/v4/c/a;->a(Ljava/lang/Object;)V

    .line 225
    iget-object v0, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    if-ne v0, p1, :cond_23

    .line 1468
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->x:Z

    if-eqz v0, :cond_e

    .line 1469
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->w:Z

    .line 228
    :cond_e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/c/a;->f:J

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    .line 2137
    iget-object v0, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    if-eqz v0, :cond_20

    .line 2138
    iget-object v0, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    invoke-interface {v0}, Landroid/support/v4/c/ac;->d()V

    .line 232
    :cond_20
    invoke-virtual {p0}, Landroid/support/v4/c/a;->c()V

    .line 234
    :cond_23
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 198
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9

    .prologue
    .line 343
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/c/aa;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    if-eqz v0, :cond_20

    .line 345
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 346
    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iget-boolean v0, v0, Landroid/support/v4/c/b;->b:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 348
    :cond_20
    iget-object v0, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    if-eqz v0, :cond_3d

    .line 349
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCancellingTask="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 350
    const-string v0, " waiting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    iget-boolean v0, v0, Landroid/support/v4/c/b;->b:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 352
    :cond_3d
    iget-wide v0, p0, Landroid/support/v4/c/a;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_63

    .line 353
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUpdateThrottle="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 354
    iget-wide v0, p0, Landroid/support/v4/c/a;->e:J

    invoke-static {v0, v1, p3}, Landroid/support/v4/n/x;->a(JLjava/io/PrintWriter;)V

    .line 355
    const-string v0, " mLastLoadCompleteTime="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 356
    iget-wide v0, p0, Landroid/support/v4/c/a;->f:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p3}, Landroid/support/v4/n/x;->a(JJLjava/io/PrintWriter;)V

    .line 358
    invoke-virtual {p3}, Ljava/io/PrintWriter;->println()V

    .line 360
    :cond_63
    return-void
.end method

.method protected final b()Z
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 156
    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    if-eqz v1, :cond_1d

    .line 157
    iget-object v1, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    if-eqz v1, :cond_1e

    .line 162
    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iget-boolean v1, v1, Landroid/support/v4/c/b;->b:Z

    if-eqz v1, :cond_1b

    .line 163
    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iput-boolean v0, v1, Landroid/support/v4/c/b;->b:Z

    .line 164
    iget-object v1, p0, Landroid/support/v4/c/a;->g:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 166
    :cond_1b
    iput-object v3, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    .line 187
    :cond_1d
    :goto_1d
    return v0

    .line 168
    :cond_1e
    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iget-boolean v1, v1, Landroid/support/v4/c/b;->b:Z

    if-eqz v1, :cond_32

    .line 172
    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iput-boolean v0, v1, Landroid/support/v4/c/b;->b:Z

    .line 173
    iget-object v1, p0, Landroid/support/v4/c/a;->g:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 174
    iput-object v3, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    goto :goto_1d

    .line 177
    :cond_32
    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    .line 1311
    iget-object v1, v1, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    .line 179
    if-eqz v0, :cond_43

    .line 180
    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iput-object v1, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    .line 181
    invoke-virtual {p0}, Landroid/support/v4/c/a;->e()V

    .line 183
    :cond_43
    iput-object v3, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    goto :goto_1d
.end method

.method final c()V
    .registers 7

    .prologue
    .line 201
    iget-object v0, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    if-nez v0, :cond_40

    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    if-eqz v0, :cond_40

    .line 202
    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iget-boolean v0, v0, Landroid/support/v4/c/b;->b:Z

    if-eqz v0, :cond_1a

    .line 203
    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/c/b;->b:Z

    .line 204
    iget-object v0, p0, Landroid/support/v4/c/a;->g:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 206
    :cond_1a
    iget-wide v0, p0, Landroid/support/v4/c/a;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_41

    .line 207
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 208
    iget-wide v2, p0, Landroid/support/v4/c/a;->f:J

    iget-wide v4, p0, Landroid/support/v4/c/a;->e:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_41

    .line 213
    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/c/b;->b:Z

    .line 214
    iget-object v0, p0, Landroid/support/v4/c/a;->g:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iget-wide v2, p0, Landroid/support/v4/c/a;->f:J

    iget-wide v4, p0, Landroid/support/v4/c/a;->e:J

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 221
    :cond_40
    :goto_40
    return-void

    .line 219
    :cond_41
    iget-object v0, p0, Landroid/support/v4/c/a;->c:Landroid/support/v4/c/b;

    iget-object v1, p0, Landroid/support/v4/c/a;->h:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/c/b;->a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/support/v4/c/ai;

    goto :goto_40
.end method

.method public abstract d()Ljava/lang/Object;
.end method

.method public e()V
    .registers 1

    .prologue
    .line 311
    return-void
.end method
