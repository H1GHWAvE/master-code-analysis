.class abstract Landroid/support/v4/c/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "AsyncTask"

.field private static final b:I = 0x5

.field private static final c:I = 0x80

.field public static final d:Ljava/util/concurrent/Executor;

.field private static final f:I = 0x1

.field private static final g:Ljava/util/concurrent/ThreadFactory;

.field private static final h:Ljava/util/concurrent/BlockingQueue;

.field private static final i:I = 0x1

.field private static final j:I = 0x2

.field private static k:Landroid/support/v4/c/ao;

.field private static volatile l:Ljava/util/concurrent/Executor;


# instance fields
.field final e:Ljava/util/concurrent/FutureTask;

.field private final m:Landroid/support/v4/c/aq;

.field private volatile n:I

.field private final o:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    .line 55
    new-instance v0, Landroid/support/v4/c/aj;

    invoke-direct {v0}, Landroid/support/v4/c/aj;-><init>()V

    sput-object v0, Landroid/support/v4/c/ai;->g:Ljava/util/concurrent/ThreadFactory;

    .line 63
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, Landroid/support/v4/c/ai;->h:Ljava/util/concurrent/BlockingQueue;

    .line 69
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x5

    const/16 v3, 0x80

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Landroid/support/v4/c/ai;->h:Ljava/util/concurrent/BlockingQueue;

    sget-object v8, Landroid/support/v4/c/ai;->g:Ljava/util/concurrent/ThreadFactory;

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 78
    sput-object v1, Landroid/support/v4/c/ai;->d:Ljava/util/concurrent/Executor;

    sput-object v1, Landroid/support/v4/c/ai;->l:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    sget v0, Landroid/support/v4/c/ap;->a:I

    iput v0, p0, Landroid/support/v4/c/ai;->n:I

    .line 84
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Landroid/support/v4/c/ai;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 123
    new-instance v0, Landroid/support/v4/c/ak;

    invoke-direct {v0, p0}, Landroid/support/v4/c/ak;-><init>(Landroid/support/v4/c/ai;)V

    iput-object v0, p0, Landroid/support/v4/c/ai;->m:Landroid/support/v4/c/aq;

    .line 132
    new-instance v0, Landroid/support/v4/c/al;

    iget-object v1, p0, Landroid/support/v4/c/ai;->m:Landroid/support/v4/c/aq;

    invoke-direct {v0, p0, v1}, Landroid/support/v4/c/al;-><init>(Landroid/support/v4/c/ai;Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    .line 152
    return-void
.end method

.method private varargs a([Ljava/lang/Object;)Landroid/support/v4/c/ai;
    .registers 3

    .prologue
    .line 376
    sget-object v0, Landroid/support/v4/c/ai;->l:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Landroid/support/v4/c/ai;->a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/support/v4/c/ai;

    move-result-object v0

    return-object v0
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 346
    iget-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/support/v4/c/ai;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/support/v4/c/ai;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/support/v4/c/ai;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .registers 2

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v4/c/ai;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private static a(Ljava/lang/Runnable;)V
    .registers 2

    .prologue
    .line 438
    sget-object v0, Landroid/support/v4/c/ai;->l:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 439
    return-void
.end method

.method private static a(Ljava/util/concurrent/Executor;)V
    .registers 1

    .prologue
    .line 116
    sput-object p0, Landroid/support/v4/c/ai;->l:Ljava/util/concurrent/Executor;

    .line 117
    return-void
.end method

.method protected static varargs b()V
    .registers 0

    .prologue
    .line 231
    return-void
.end method

.method static synthetic b(Landroid/support/v4/c/ai;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 48
    .line 3155
    iget-object v0, p0, Landroid/support/v4/c/ai;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    .line 3156
    if-nez v0, :cond_b

    .line 3157
    invoke-direct {p0, p1}, Landroid/support/v4/c/ai;->d(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    :cond_b
    return-void
.end method

.method private varargs b([Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 456
    .line 1278
    iget-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v0

    .line 456
    if-nez v0, :cond_19

    .line 457
    invoke-static {}, Landroid/support/v4/c/ai;->c()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    new-instance v2, Landroid/support/v4/c/an;

    invoke-direct {v2, p0, p1}, Landroid/support/v4/c/an;-><init>(Landroid/support/v4/c/ai;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 460
    :cond_19
    return-void
.end method

.method private static c()Landroid/os/Handler;
    .registers 2

    .prologue
    .line 106
    const-class v1, Landroid/support/v4/c/ai;

    monitor-enter v1

    .line 107
    :try_start_3
    sget-object v0, Landroid/support/v4/c/ai;->k:Landroid/support/v4/c/ao;

    if-nez v0, :cond_e

    .line 108
    new-instance v0, Landroid/support/v4/c/ao;

    invoke-direct {v0}, Landroid/support/v4/c/ao;-><init>()V

    sput-object v0, Landroid/support/v4/c/ai;->k:Landroid/support/v4/c/ao;

    .line 110
    :cond_e
    sget-object v0, Landroid/support/v4/c/ai;->k:Landroid/support/v4/c/ao;

    monitor-exit v1

    return-object v0

    .line 111
    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method static synthetic c(Landroid/support/v4/c/ai;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 48
    .line 4278
    iget-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v0

    .line 3463
    if-eqz v0, :cond_10

    .line 3464
    invoke-virtual {p0, p1}, Landroid/support/v4/c/ai;->b(Ljava/lang/Object;)V

    .line 3468
    :goto_b
    sget v0, Landroid/support/v4/c/ap;->c:I

    iput v0, p0, Landroid/support/v4/c/ai;->n:I

    .line 48
    return-void

    .line 3466
    :cond_10
    invoke-virtual {p0, p1}, Landroid/support/v4/c/ai;->a(Ljava/lang/Object;)V

    goto :goto_b
.end method

.method private c(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 155
    iget-object v0, p0, Landroid/support/v4/c/ai;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    .line 156
    if-nez v0, :cond_b

    .line 157
    invoke-direct {p0, p1}, Landroid/support/v4/c/ai;->d(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    :cond_b
    return-void
.end method

.method private d()I
    .registers 2

    .prologue
    .line 174
    iget v0, p0, Landroid/support/v4/c/ai;->n:I

    return v0
.end method

.method private d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    const/4 v4, 0x1

    .line 162
    invoke-static {}, Landroid/support/v4/c/ai;->c()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Landroid/support/v4/c/an;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v1, p0, v2}, Landroid/support/v4/c/an;-><init>(Landroid/support/v4/c/ai;[Ljava/lang/Object;)V

    invoke-virtual {v0, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 165
    return-object p1
.end method

.method private static e()V
    .registers 0

    .prologue
    .line 202
    return-void
.end method

.method private e(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 463
    .line 2278
    iget-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v0

    .line 463
    if-eqz v0, :cond_10

    .line 464
    invoke-virtual {p0, p1}, Landroid/support/v4/c/ai;->b(Ljava/lang/Object;)V

    .line 468
    :goto_b
    sget v0, Landroid/support/v4/c/ap;->c:I

    iput v0, p0, Landroid/support/v4/c/ai;->n:I

    .line 469
    return-void

    .line 466
    :cond_10
    invoke-virtual {p0, p1}, Landroid/support/v4/c/ai;->a(Ljava/lang/Object;)V

    goto :goto_b
.end method

.method private static f()V
    .registers 0

    .prologue
    .line 265
    return-void
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 278
    iget-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v0

    return v0
.end method

.method private h()Z
    .registers 3

    .prologue
    .line 311
    iget-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method private i()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 326
    iget-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final varargs a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/support/v4/c/ai;
    .registers 5

    .prologue
    .line 411
    iget v0, p0, Landroid/support/v4/c/ai;->n:I

    sget v1, Landroid/support/v4/c/ap;->a:I

    if-eq v0, v1, :cond_11

    .line 412
    sget-object v0, Landroid/support/v4/c/am;->a:[I

    iget v1, p0, Landroid/support/v4/c/ai;->n:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_30

    .line 423
    :cond_11
    sget v0, Landroid/support/v4/c/ap;->b:I

    iput v0, p0, Landroid/support/v4/c/ai;->n:I

    .line 427
    iget-object v0, p0, Landroid/support/v4/c/ai;->m:Landroid/support/v4/c/aq;

    iput-object p2, v0, Landroid/support/v4/c/aq;->b:[Ljava/lang/Object;

    .line 428
    iget-object v0, p0, Landroid/support/v4/c/ai;->e:Ljava/util/concurrent/FutureTask;

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 430
    return-object p0

    .line 414
    :pswitch_1f
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 417
    :pswitch_27
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 412
    nop

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_27
    .end packed-switch
.end method

.method protected varargs abstract a()Ljava/lang/Object;
.end method

.method protected a(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 218
    return-void
.end method

.method protected b(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 250
    return-void
.end method
