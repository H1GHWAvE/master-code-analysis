.class public Landroid/support/v4/c/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public p:I

.field public q:Landroid/support/v4/c/ad;

.field public r:Landroid/support/v4/c/ac;

.field s:Landroid/content/Context;

.field t:Z

.field public u:Z

.field v:Z

.field w:Z

.field x:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->t:Z

    .line 40
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->u:Z

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->v:Z

    .line 42
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->w:Z

    .line 43
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->x:Z

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/c/aa;->s:Landroid/content/Context;

    .line 115
    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 497
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 498
    invoke-static {p0, v0}, Landroid/support/v4/n/g;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 499
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 500
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILandroid/support/v4/c/ad;)V
    .registers 5

    .prologue
    .line 164
    iget-object v0, p0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    if-eqz v0, :cond_c

    .line 165
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a listener registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_c
    iput-object p2, p0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    .line 168
    iput p1, p0, Landroid/support/v4/c/aa;->p:I

    .line 169
    return-void
.end method

.method private b(Landroid/support/v4/c/ac;)V
    .registers 4

    .prologue
    .line 196
    iget-object v0, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    if-eqz v0, :cond_c

    .line 197
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a listener registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_c
    iput-object p1, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    .line 200
    return-void
.end method

.method private c()V
    .registers 2

    .prologue
    .line 137
    iget-object v0, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    if-eqz v0, :cond_9

    .line 138
    iget-object v0, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    invoke-interface {v0}, Landroid/support/v4/c/ac;->d()V

    .line 140
    :cond_9
    return-void
.end method

.method private d()Landroid/content/Context;
    .registers 2

    .prologue
    .line 146
    iget-object v0, p0, Landroid/support/v4/c/aa;->s:Landroid/content/Context;

    return-object v0
.end method

.method private e()I
    .registers 2

    .prologue
    .line 153
    iget v0, p0, Landroid/support/v4/c/aa;->p:I

    return v0
.end method

.method private o()Z
    .registers 2

    .prologue
    .line 226
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->t:Z

    return v0
.end method

.method private p()Z
    .registers 2

    .prologue
    .line 235
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->u:Z

    return v0
.end method

.method private q()Z
    .registers 2

    .prologue
    .line 244
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->v:Z

    return v0
.end method

.method private r()V
    .registers 2

    .prologue
    .line 385
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->u:Z

    .line 387
    return-void
.end method

.method private static s()V
    .registers 0

    .prologue
    .line 399
    return-void
.end method

.method private t()Z
    .registers 3

    .prologue
    .line 443
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->w:Z

    .line 444
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/c/aa;->w:Z

    .line 445
    iget-boolean v1, p0, Landroid/support/v4/c/aa;->x:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Landroid/support/v4/c/aa;->x:Z

    .line 446
    return v0
.end method

.method private u()V
    .registers 2

    .prologue
    .line 457
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->x:Z

    .line 458
    return-void
.end method

.method private v()V
    .registers 2

    .prologue
    .line 468
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->x:Z

    if-eqz v0, :cond_7

    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->w:Z

    .line 471
    :cond_7
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 1

    .prologue
    .line 337
    return-void
.end method

.method public final a(Landroid/support/v4/c/ac;)V
    .registers 4

    .prologue
    .line 211
    iget-object v0, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    if-nez v0, :cond_c

    .line 212
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No listener register"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_c
    iget-object v0, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    if-eq v0, p1, :cond_18

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to unregister the wrong listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_18
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/c/aa;->r:Landroid/support/v4/c/ac;

    .line 218
    return-void
.end method

.method public final a(Landroid/support/v4/c/ad;)V
    .registers 4

    .prologue
    .line 177
    iget-object v0, p0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    if-nez v0, :cond_c

    .line 178
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No listener register"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_c
    iget-object v0, p0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    if-eq v0, p1, :cond_18

    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to unregister the wrong listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_18
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    .line 184
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 522
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/c/aa;->p:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 523
    const-string v0, " mListener="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 524
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->t:Z

    if-nez v0, :cond_23

    iget-boolean v0, p0, Landroid/support/v4/c/aa;->w:Z

    if-nez v0, :cond_23

    iget-boolean v0, p0, Landroid/support/v4/c/aa;->x:Z

    if-eqz v0, :cond_44

    .line 525
    :cond_23
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/c/aa;->t:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 526
    const-string v0, " mContentChanged="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/c/aa;->w:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 527
    const-string v0, " mProcessingChange="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/c/aa;->x:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 529
    :cond_44
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->u:Z

    if-nez v0, :cond_4c

    iget-boolean v0, p0, Landroid/support/v4/c/aa;->v:Z

    if-eqz v0, :cond_63

    .line 530
    :cond_4c
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAbandoned="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/c/aa;->u:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 531
    const-string v0, " mReset="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/c/aa;->v:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 533
    :cond_63
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 125
    iget-object v0, p0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    if-eqz v0, :cond_9

    .line 126
    iget-object v0, p0, Landroid/support/v4/c/aa;->q:Landroid/support/v4/c/ad;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/c/ad;->a(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 128
    :cond_9
    return-void
.end method

.method protected b()Z
    .registers 2

    .prologue
    .line 317
    const/4 v0, 0x0

    return v0
.end method

.method protected f()V
    .registers 1

    .prologue
    .line 281
    return-void
.end method

.method protected g()V
    .registers 1

    .prologue
    .line 371
    return-void
.end method

.method protected h()V
    .registers 1

    .prologue
    .line 435
    return-void
.end method

.method public final i()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->t:Z

    .line 270
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->v:Z

    .line 271
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->u:Z

    .line 272
    invoke-virtual {p0}, Landroid/support/v4/c/aa;->f()V

    .line 273
    return-void
.end method

.method public final j()Z
    .registers 2

    .prologue
    .line 302
    invoke-virtual {p0}, Landroid/support/v4/c/aa;->b()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .registers 1

    .prologue
    .line 329
    invoke-virtual {p0}, Landroid/support/v4/c/aa;->a()V

    .line 330
    return-void
.end method

.method public final l()V
    .registers 2

    .prologue
    .line 360
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->t:Z

    .line 361
    invoke-virtual {p0}, Landroid/support/v4/c/aa;->g()V

    .line 362
    return-void
.end method

.method public final m()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 420
    invoke-virtual {p0}, Landroid/support/v4/c/aa;->h()V

    .line 421
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->v:Z

    .line 422
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->t:Z

    .line 423
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->u:Z

    .line 424
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->w:Z

    .line 425
    iput-boolean v1, p0, Landroid/support/v4/c/aa;->x:Z

    .line 426
    return-void
.end method

.method public final n()V
    .registers 2

    .prologue
    .line 482
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->t:Z

    if-eqz v0, :cond_8

    .line 1329
    invoke-virtual {p0}, Landroid/support/v4/c/aa;->a()V

    .line 490
    :goto_7
    return-void

    .line 488
    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/c/aa;->w:Z

    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 505
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 506
    invoke-static {p0, v0}, Landroid/support/v4/n/g;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 507
    const-string v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    iget v1, p0, Landroid/support/v4/c/aa;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 509
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
