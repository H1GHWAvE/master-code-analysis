.class public final Landroid/support/v4/c/b/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/res/TypedArray;III)I
    .registers 5
    .param p1    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-virtual {p0, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 47
    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/res/TypedArray;II)Landroid/graphics/drawable/Drawable;
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 38
    if-nez v0, :cond_a

    .line 39
    invoke-virtual {p0, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 41
    :cond_a
    return-object v0
.end method

.method private static a(Landroid/content/res/TypedArray;IIZ)Z
    .registers 5
    .param p1    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-virtual {p0, p2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 32
    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/res/TypedArray;III)I
    .registers 5
    .param p1    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/a/c;
        .end annotation
    .end param
    .annotation build Landroid/support/a/c;
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 53
    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/res/TypedArray;II)Ljava/lang/String;
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 59
    if-nez v0, :cond_a

    .line 60
    invoke-virtual {p0, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    :cond_a
    return-object v0
.end method

.method private static c(Landroid/content/res/TypedArray;II)[Ljava/lang/CharSequence;
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/a/aj;
        .end annotation
    .end param

    .prologue
    .line 67
    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 68
    if-nez v0, :cond_a

    .line 69
    invoke-virtual {p0, p2}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 71
    :cond_a
    return-object v0
.end method
