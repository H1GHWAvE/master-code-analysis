.class final Landroid/support/v4/media/session/an;
.super Landroid/os/Handler;
.source "SourceFile"


# static fields
.field private static final b:I = 0x1

.field private static final c:I = 0x2

.field private static final d:I = 0x3

.field private static final e:I = 0x4

.field private static final f:I = 0x5

.field private static final g:I = 0x6

.field private static final h:I = 0x7

.field private static final i:I = 0x8

.field private static final j:I = 0x9

.field private static final k:I = 0xa

.field private static final l:I = 0xb

.field private static final m:I = 0xc

.field private static final n:I = 0xd

.field private static final o:I = 0xe

.field private static final p:I = 0xf

.field private static final q:I = 0x10

.field private static final r:I = 0x11

.field private static final s:I = 0x12

.field private static final t:I = 0x7f

.field private static final u:I = 0x7e


# instance fields
.field final synthetic a:Landroid/support/v4/media/session/ai;


# direct methods
.method public constructor <init>(Landroid/support/v4/media/session/ai;Landroid/os/Looper;)V
    .registers 3

    .prologue
    .line 1759
    iput-object p1, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 1760
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1761
    return-void
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 1774
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1775
    return-void
.end method

.method private a(ILjava/lang/Object;I)V
    .registers 5

    .prologue
    .line 1778
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p3, v0, p2}, Landroid/support/v4/media/session/an;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1779
    return-void
.end method

.method private a(Landroid/view/KeyEvent;)V
    .registers 8

    .prologue
    const-wide/16 v2, 0x0

    .line 1852
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_b

    .line 1909
    :cond_a
    :goto_a
    return-void

    .line 1855
    :cond_b
    iget-object v0, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 6963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 1855
    if-nez v0, :cond_27

    move-wide v0, v2

    .line 1856
    :goto_12
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_66

    goto :goto_a

    .line 1896
    :sswitch_1a
    iget-object v0, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 8963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 1896
    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 9963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 10353
    iget v0, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    goto :goto_a

    .line 1855
    :cond_27
    iget-object v0, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 7963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 8405
    iget-wide v0, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->F:J

    goto :goto_12

    .line 1859
    :sswitch_2e
    const-wide/16 v4, 0x4

    and-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    goto :goto_a

    .line 1865
    :sswitch_36
    const-wide/16 v4, 0x2

    and-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    goto :goto_a

    .line 1870
    :sswitch_3e
    const-wide/16 v4, 0x20

    and-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    goto :goto_a

    .line 1875
    :sswitch_46
    const-wide/16 v4, 0x10

    and-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    goto :goto_a

    .line 1880
    :sswitch_4e
    const-wide/16 v4, 0x1

    and-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    goto :goto_a

    .line 1885
    :sswitch_56
    const-wide/16 v4, 0x40

    and-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    goto :goto_a

    .line 1890
    :sswitch_5e
    const-wide/16 v4, 0x8

    and-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    goto :goto_a

    .line 1856
    :sswitch_data_66
    .sparse-switch
        0x4f -> :sswitch_1a
        0x55 -> :sswitch_1a
        0x56 -> :sswitch_4e
        0x57 -> :sswitch_3e
        0x58 -> :sswitch_46
        0x59 -> :sswitch_5e
        0x5a -> :sswitch_56
        0x7e -> :sswitch_2e
        0x7f -> :sswitch_36
    .end sparse-switch
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .registers 4

    .prologue
    .line 1770
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/media/session/an;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1771
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 1764
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/media/session/an;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1765
    invoke-virtual {v0, p3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1766
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1767
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    .line 1783
    iget-object v0, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 1963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->h:Landroid/support/v4/media/session/ad;

    .line 1783
    if-nez v0, :cond_a

    .line 2891
    :cond_9
    :goto_9
    :pswitch_9
    return-void

    .line 1786
    :cond_a
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_d6

    goto :goto_9

    .line 1791
    :pswitch_10
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    goto :goto_9

    .line 1794
    :pswitch_16
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    goto :goto_9

    .line 1797
    :pswitch_1c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    goto :goto_9

    .line 1800
    :pswitch_22
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    goto :goto_9

    .line 1821
    :pswitch_2a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    goto :goto_9

    .line 1824
    :pswitch_32
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_9

    .line 1827
    :pswitch_35
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    goto :goto_9

    .line 1830
    :pswitch_3b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/KeyEvent;

    .line 1831
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1832
    const-string v2, "android.intent.extra.KEY_EVENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2852
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_9

    .line 2855
    iget-object v1, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 2963
    iget-object v1, v1, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 2855
    if-nez v1, :cond_6f

    move-wide v2, v4

    .line 2856
    :goto_5a
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_fe

    goto :goto_9

    .line 2896
    :sswitch_62
    iget-object v0, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 4963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 2896
    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 5963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 6353
    iget v0, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    goto :goto_9

    .line 2855
    :cond_6f
    iget-object v1, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    .line 3963
    iget-object v1, v1, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 4405
    iget-wide v2, v1, Landroid/support/v4/media/session/PlaybackStateCompat;->F:J

    goto :goto_5a

    .line 2859
    :sswitch_76
    const-wide/16 v0, 0x4

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    goto :goto_9

    .line 2865
    :sswitch_7e
    const-wide/16 v0, 0x2

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    goto :goto_9

    .line 2870
    :sswitch_86
    const-wide/16 v0, 0x20

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    goto/16 :goto_9

    .line 2875
    :sswitch_8f
    const-wide/16 v0, 0x10

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    goto/16 :goto_9

    .line 2880
    :sswitch_98
    const-wide/16 v0, 0x1

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    goto/16 :goto_9

    .line 2885
    :sswitch_a1
    const-wide/16 v0, 0x40

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    goto/16 :goto_9

    .line 2890
    :sswitch_aa
    const-wide/16 v0, 0x8

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    goto/16 :goto_9

    .line 1839
    :pswitch_b3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_9

    .line 1843
    :pswitch_b7
    iget-object v1, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0, v2}, Landroid/support/v4/media/session/ai;->a(Landroid/support/v4/media/session/ai;II)V

    goto/16 :goto_9

    .line 1846
    :pswitch_c6
    iget-object v1, p0, Landroid/support/v4/media/session/an;->a:Landroid/support/v4/media/session/ai;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0, v2}, Landroid/support/v4/media/session/ai;->b(Landroid/support/v4/media/session/ai;II)V

    goto/16 :goto_9

    .line 1786
    nop

    :pswitch_data_d6
    .packed-switch 0x1
        :pswitch_9
        :pswitch_10
        :pswitch_16
        :pswitch_22
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_2a
        :pswitch_32
        :pswitch_35
        :pswitch_3b
        :pswitch_b3
        :pswitch_b7
        :pswitch_c6
        :pswitch_1c
    .end packed-switch

    .line 2856
    :sswitch_data_fe
    .sparse-switch
        0x4f -> :sswitch_62
        0x55 -> :sswitch_62
        0x56 -> :sswitch_98
        0x57 -> :sswitch_86
        0x58 -> :sswitch_8f
        0x59 -> :sswitch_aa
        0x5a -> :sswitch_a1
        0x7e -> :sswitch_76
        0x7f -> :sswitch_7e
    .end sparse-switch
.end method
