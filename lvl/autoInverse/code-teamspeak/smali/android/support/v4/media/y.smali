.class final Landroid/support/v4/media/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnWindowAttachListener;


# instance fields
.field final synthetic a:Landroid/support/v4/media/x;


# direct methods
.method constructor <init>(Landroid/support/v4/media/x;)V
    .registers 2

    .prologue
    .line 42
    iput-object p1, p0, Landroid/support/v4/media/y;->a:Landroid/support/v4/media/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onWindowAttached()V
    .registers 6

    .prologue
    .line 45
    iget-object v0, p0, Landroid/support/v4/media/y;->a:Landroid/support/v4/media/x;

    .line 1111
    iget-object v1, v0, Landroid/support/v4/media/x;->a:Landroid/content/Context;

    iget-object v2, v0, Landroid/support/v4/media/x;->j:Landroid/content/BroadcastReceiver;

    iget-object v3, v0, Landroid/support/v4/media/x;->f:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1112
    iget-object v1, v0, Landroid/support/v4/media/x;->a:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, v0, Landroid/support/v4/media/x;->g:Landroid/content/Intent;

    const/high16 v4, 0x10000000

    invoke-static {v1, v2, v3, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    .line 1114
    new-instance v1, Landroid/media/RemoteControlClient;

    iget-object v2, v0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    invoke-direct {v1, v2}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v1, v0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    .line 1115
    iget-object v1, v0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v0}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 1116
    iget-object v1, v0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v0}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 46
    return-void
.end method

.method public final onWindowDetached()V
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Landroid/support/v4/media/y;->a:Landroid/support/v4/media/x;

    invoke-virtual {v0}, Landroid/support/v4/media/x;->d()V

    .line 50
    return-void
.end method
