.class final Landroid/support/v4/media/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;


# instance fields
.field final synthetic a:Landroid/support/v4/media/x;


# direct methods
.method constructor <init>(Landroid/support/v4/media/x;)V
    .registers 2

    .prologue
    .line 53
    iput-object p1, p0, Landroid/support/v4/media/z;->a:Landroid/support/v4/media/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onWindowFocusChanged(Z)V
    .registers 5

    .prologue
    .line 56
    if-eqz p1, :cond_22

    iget-object v0, p0, Landroid/support/v4/media/z;->a:Landroid/support/v4/media/x;

    .line 1120
    iget-boolean v1, v0, Landroid/support/v4/media/x;->n:Z

    if-nez v1, :cond_21

    .line 1121
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/media/x;->n:Z

    .line 1122
    iget-object v1, v0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    iget-object v2, v0, Landroid/support/v4/media/x;->l:Landroid/app/PendingIntent;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/app/PendingIntent;)V

    .line 1123
    iget-object v1, v0, Landroid/support/v4/media/x;->b:Landroid/media/AudioManager;

    iget-object v2, v0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 1124
    iget v1, v0, Landroid/support/v4/media/x;->o:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_21

    .line 1125
    invoke-virtual {v0}, Landroid/support/v4/media/x;->a()V

    .line 58
    :cond_21
    :goto_21
    return-void

    .line 57
    :cond_22
    iget-object v0, p0, Landroid/support/v4/media/z;->a:Landroid/support/v4/media/x;

    invoke-virtual {v0}, Landroid/support/v4/media/x;->c()V

    goto :goto_21
.end method
