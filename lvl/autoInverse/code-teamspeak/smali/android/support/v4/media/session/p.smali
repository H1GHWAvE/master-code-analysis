.class final Landroid/support/v4/media/session/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/media/session/m;


# instance fields
.field private a:Landroid/support/v4/media/session/MediaSessionCompat$Token;

.field private b:Landroid/support/v4/media/session/d;

.field private c:Landroid/support/v4/media/session/r;


# direct methods
.method public constructor <init>(Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
    .registers 3

    .prologue
    .line 806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807
    iput-object p1, p0, Landroid/support/v4/media/session/p;->a:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 1745
    iget-object v0, p1, Landroid/support/v4/media/session/MediaSessionCompat$Token;->a:Ljava/lang/Object;

    .line 808
    check-cast v0, Landroid/os/IBinder;

    invoke-static {v0}, Landroid/support/v4/media/session/e;->a(Landroid/os/IBinder;)Landroid/support/v4/media/session/d;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    .line 809
    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v4/media/session/r;
    .registers 3

    .prologue
    .line 857
    iget-object v0, p0, Landroid/support/v4/media/session/p;->c:Landroid/support/v4/media/session/r;

    if-nez v0, :cond_d

    .line 858
    new-instance v0, Landroid/support/v4/media/session/u;

    iget-object v1, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-direct {v0, v1}, Landroid/support/v4/media/session/u;-><init>(Landroid/support/v4/media/session/d;)V

    iput-object v0, p0, Landroid/support/v4/media/session/p;->c:Landroid/support/v4/media/session/r;

    .line 861
    :cond_d
    iget-object v0, p0, Landroid/support/v4/media/session/p;->c:Landroid/support/v4/media/session/r;

    return-object v0
.end method

.method public final a(II)V
    .registers 7

    .prologue
    .line 960
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Landroid/support/v4/media/session/d;->b(IILjava/lang/String;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_6} :catch_7

    .line 964
    :goto_6
    return-void

    .line 961
    :catch_7
    move-exception v0

    .line 962
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in setVolumeTo. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method

.method public final a(Landroid/support/v4/media/session/i;)V
    .registers 6

    .prologue
    .line 829
    if-nez p1, :cond_a

    .line 830
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 833
    :cond_a
    :try_start_a
    iget-object v1, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-static {p1}, Landroid/support/v4/media/session/i;->c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    invoke-interface {v1, v0}, Landroid/support/v4/media/session/d;->b(Landroid/support/v4/media/session/a;)V

    .line 835
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 836
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/media/session/i;->a(Landroid/support/v4/media/session/i;Z)Z
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_23} :catch_24

    .line 840
    :goto_23
    return-void

    .line 837
    :catch_24
    move-exception v0

    .line 838
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in unregisterCallback. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_23
.end method

.method public final a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V
    .registers 7

    .prologue
    .line 813
    if-nez p1, :cond_a

    .line 814
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 817
    :cond_a
    :try_start_a
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 818
    iget-object v1, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-static {p1}, Landroid/support/v4/media/session/i;->c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    invoke-interface {v1, v0}, Landroid/support/v4/media/session/d;->a(Landroid/support/v4/media/session/a;)V

    .line 819
    invoke-static {p1, p2}, Landroid/support/v4/media/session/i;->a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V

    .line 820
    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/support/v4/media/session/i;->a(Landroid/support/v4/media/session/i;Z)Z
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_26} :catch_27

    .line 825
    :goto_26
    return-void

    .line 821
    :catch_27
    move-exception v0

    .line 822
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in registerCallback. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_26
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
    .registers 8

    .prologue
    .line 978
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    new-instance v1, Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;

    invoke-direct {v1, p3}, Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;-><init>(Landroid/os/ResultReceiver;)V

    invoke-interface {v0, p1, p2, v1}, Landroid/support/v4/media/session/d;->a(Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_a} :catch_b

    .line 983
    :goto_a
    return-void

    .line 980
    :catch_b
    move-exception v0

    .line 981
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in sendCommand. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    .line 844
    if-nez p1, :cond_a

    .line 845
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "event may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 848
    :cond_a
    :try_start_a
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/d;->a(Landroid/view/KeyEvent;)Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    .line 852
    :goto_f
    const/4 v0, 0x0

    return v0

    .line 849
    :catch_11
    move-exception v0

    .line 850
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in dispatchMediaButtonEvent. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f
.end method

.method public final b()Landroid/support/v4/media/session/PlaybackStateCompat;
    .registers 5

    .prologue
    .line 867
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->o()Landroid/support/v4/media/session/PlaybackStateCompat;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 871
    :goto_6
    return-object v0

    .line 868
    :catch_7
    move-exception v0

    .line 869
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getPlaybackState. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final b(II)V
    .registers 7

    .prologue
    .line 969
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Landroid/support/v4/media/session/d;->a(IILjava/lang/String;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_6} :catch_7

    .line 973
    :goto_6
    return-void

    .line 970
    :catch_7
    move-exception v0

    .line 971
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in adjustVolume. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method

.method public final c()Landroid/support/v4/media/MediaMetadataCompat;
    .registers 5

    .prologue
    .line 877
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->n()Landroid/support/v4/media/MediaMetadataCompat;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 881
    :goto_6
    return-object v0

    .line 878
    :catch_7
    move-exception v0

    .line 879
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getMetadata. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final d()Ljava/util/List;
    .registers 5

    .prologue
    .line 887
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->p()Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 891
    :goto_6
    return-object v0

    .line 888
    :catch_7
    move-exception v0

    .line 889
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getQueue. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final e()Ljava/lang/CharSequence;
    .registers 5

    .prologue
    .line 897
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->q()Ljava/lang/CharSequence;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 901
    :goto_6
    return-object v0

    .line 898
    :catch_7
    move-exception v0

    .line 899
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getQueueTitle. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final f()Landroid/os/Bundle;
    .registers 5

    .prologue
    .line 907
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->r()Landroid/os/Bundle;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 911
    :goto_6
    return-object v0

    .line 908
    :catch_7
    move-exception v0

    .line 909
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getExtras. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final g()I
    .registers 5

    .prologue
    .line 917
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->s()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result v0

    .line 921
    :goto_6
    return v0

    .line 918
    :catch_7
    move-exception v0

    .line 919
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getRatingType. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final h()J
    .registers 5

    .prologue
    .line 927
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->e()J
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-wide v0

    .line 931
    :goto_6
    return-wide v0

    .line 928
    :catch_7
    move-exception v0

    .line 929
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getFlags. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    const-wide/16 v0, 0x0

    goto :goto_6
.end method

.method public final i()Landroid/support/v4/media/session/q;
    .registers 7

    .prologue
    .line 937
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->f()Landroid/support/v4/media/session/ParcelableVolumeInfo;

    move-result-object v5

    .line 938
    new-instance v0, Landroid/support/v4/media/session/q;

    iget v1, v5, Landroid/support/v4/media/session/ParcelableVolumeInfo;->a:I

    iget v2, v5, Landroid/support/v4/media/session/ParcelableVolumeInfo;->b:I

    iget v3, v5, Landroid/support/v4/media/session/ParcelableVolumeInfo;->c:I

    iget v4, v5, Landroid/support/v4/media/session/ParcelableVolumeInfo;->d:I

    iget v5, v5, Landroid/support/v4/media/session/ParcelableVolumeInfo;->e:I

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/media/session/q;-><init>(IIIII)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_15} :catch_16

    .line 944
    :goto_15
    return-object v0

    .line 941
    :catch_16
    move-exception v0

    .line 942
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getPlaybackInfo. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final j()Landroid/app/PendingIntent;
    .registers 5

    .prologue
    .line 950
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->d()Landroid/app/PendingIntent;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 954
    :goto_6
    return-object v0

    .line 951
    :catch_7
    move-exception v0

    .line 952
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getSessionActivity. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final k()Ljava/lang/String;
    .registers 5

    .prologue
    .line 988
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/p;->b:Landroid/support/v4/media/session/d;

    invoke-interface {v0}, Landroid/support/v4/media/session/d;->b()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    .line 992
    :goto_6
    return-object v0

    .line 989
    :catch_7
    move-exception v0

    .line 990
    const-string v1, "MediaControllerCompat"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dead object in getPackageName. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 992
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final l()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 997
    const/4 v0, 0x0

    return-object v0
.end method
