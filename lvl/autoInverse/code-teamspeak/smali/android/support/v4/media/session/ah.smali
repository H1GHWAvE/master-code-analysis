.class final Landroid/support/v4/media/session/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/media/session/ag;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

.field private c:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 1919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2039
    new-instance v0, Landroid/media/session/MediaSession;

    invoke-direct {v0, p1, p2}, Landroid/media/session/MediaSession;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1920
    iput-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 1921
    new-instance v1, Landroid/support/v4/media/session/MediaSessionCompat$Token;

    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 2096
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->getSessionToken()Landroid/media/session/MediaSession$Token;

    move-result-object v0

    .line 1921
    invoke-direct {v1, v0}, Landroid/support/v4/media/session/MediaSessionCompat$Token;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Landroid/support/v4/media/session/ah;->b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 1922
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 1924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3043
    instance-of v0, p1, Landroid/media/session/MediaSession;

    if-eqz v0, :cond_19

    .line 1925
    iput-object p1, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 1926
    new-instance v1, Landroid/support/v4/media/session/MediaSessionCompat$Token;

    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 3096
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->getSessionToken()Landroid/media/session/MediaSession$Token;

    move-result-object v0

    .line 1926
    invoke-direct {v1, v0}, Landroid/support/v4/media/session/MediaSessionCompat$Token;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Landroid/support/v4/media/session/ah;->b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 1927
    return-void

    .line 3046
    :cond_19
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mediaSession is not a valid MediaSession object"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .registers 3

    .prologue
    .line 1936
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 4065
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0, p1}, Landroid/media/session/MediaSession;->setFlags(I)V

    .line 1937
    return-void
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .registers 3

    .prologue
    .line 1987
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 8108
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0, p1}, Landroid/media/session/MediaSession;->setSessionActivity(Landroid/app/PendingIntent;)V

    .line 1988
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 2024
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 11132
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0, p1}, Landroid/media/session/MediaSession;->setExtras(Landroid/os/Bundle;)V

    .line 2025
    return-void
.end method

.method public final a(Landroid/support/v4/media/MediaMetadataCompat;)V
    .registers 10

    .prologue
    .line 1982
    iget-object v4, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 6552
    iget-object v0, p1, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    if-nez v0, :cond_c

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_17

    .line 6553
    :cond_c
    iget-object v1, p1, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    :goto_e
    move-object v0, v4

    .line 8104
    check-cast v0, Landroid/media/session/MediaSession;

    check-cast v1, Landroid/media/MediaMetadata;

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setMetadata(Landroid/media/MediaMetadata;)V

    .line 1983
    return-void

    .line 7048
    :cond_17
    new-instance v2, Landroid/media/MediaMetadata$Builder;

    invoke-direct {v2}, Landroid/media/MediaMetadata$Builder;-><init>()V

    .line 7481
    iget-object v0, p1, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 6557
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_26
    :goto_26
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_76

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6558
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 6559
    if-eqz v1, :cond_26

    .line 6560
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_82

    goto :goto_26

    .line 6566
    :pswitch_44
    invoke-virtual {p1, v0}, Landroid/support/v4/media/MediaMetadataCompat;->b(Ljava/lang/String;)J

    move-result-wide v6

    move-object v1, v2

    .line 8056
    check-cast v1, Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v1, v0, v6, v7}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    goto :goto_26

    .line 6562
    :pswitch_4f
    invoke-virtual {p1, v0}, Landroid/support/v4/media/MediaMetadataCompat;->d(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v1, v2

    .line 8052
    check-cast v1, Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v1, v0, v3}, Landroid/media/MediaMetadata$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;

    goto :goto_26

    .line 6570
    :pswitch_5a
    invoke-virtual {p1, v0}, Landroid/support/v4/media/MediaMetadataCompat;->c(Ljava/lang/String;)Landroid/support/v4/media/RatingCompat;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/media/RatingCompat;->a()Ljava/lang/Object;

    move-result-object v3

    move-object v1, v2

    .line 8060
    check-cast v1, Landroid/media/MediaMetadata$Builder;

    check-cast v3, Landroid/media/Rating;

    invoke-virtual {v1, v0, v3}, Landroid/media/MediaMetadata$Builder;->putRating(Ljava/lang/String;Landroid/media/Rating;)Landroid/media/MediaMetadata$Builder;

    goto :goto_26

    .line 6574
    :pswitch_6b
    invoke-virtual {p1, v0}, Landroid/support/v4/media/MediaMetadataCompat;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v1, v2

    .line 8064
    check-cast v1, Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v1, v0, v3}, Landroid/media/MediaMetadata$Builder;->putText(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/media/MediaMetadata$Builder;

    goto :goto_26

    .line 8072
    :cond_76
    check-cast v2, Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v2}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v0

    .line 6580
    iput-object v0, p1, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    .line 6581
    iget-object v1, p1, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    goto :goto_e

    .line 6560
    nop

    :pswitch_data_82
    .packed-switch 0x0
        :pswitch_44
        :pswitch_6b
        :pswitch_4f
        :pswitch_5a
    .end packed-switch
.end method

.method public final a(Landroid/support/v4/media/ae;)V
    .registers 4

    .prologue
    .line 1946
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/support/v4/media/ae;->a()Ljava/lang/Object;

    move-result-object v1

    .line 4076
    check-cast v0, Landroid/media/session/MediaSession;

    check-cast v1, Landroid/media/VolumeProvider;

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackToRemote(Landroid/media/VolumeProvider;)V

    .line 1948
    return-void
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
    .registers 21

    .prologue
    .line 1977
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    move-object/from16 v18, v0

    .line 4503
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->L:Ljava/lang/Object;

    if-nez v2, :cond_12

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_20

    .line 4504
    :cond_12
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->L:Ljava/lang/Object;

    :goto_16
    move-object/from16 v2, v18

    .line 6100
    check-cast v2, Landroid/media/session/MediaSession;

    check-cast v3, Landroid/media/session/PlaybackState;

    invoke-virtual {v2, v3}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 1978
    return-void

    .line 4507
    :cond_20
    const/4 v14, 0x0

    .line 4508
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->I:Ljava/util/List;

    if-eqz v2, :cond_71

    .line 4509
    new-instance v14, Ljava/util/ArrayList;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->I:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v14, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 4510
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->I:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_71

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;

    .line 4618
    iget-object v4, v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->e:Ljava/lang/Object;

    if-nez v4, :cond_52

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-ge v4, v5, :cond_58

    .line 4619
    :cond_52
    iget-object v2, v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->e:Ljava/lang/Object;

    .line 4511
    :goto_54
    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3c

    .line 4622
    :cond_58
    iget-object v4, v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->a:Ljava/lang/String;

    iget-object v5, v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->b:Ljava/lang/CharSequence;

    iget v6, v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->c:I

    iget-object v7, v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->d:Landroid/os/Bundle;

    .line 5097
    new-instance v8, Landroid/media/session/PlaybackState$CustomAction$Builder;

    invoke-direct {v8, v4, v5, v6}, Landroid/media/session/PlaybackState$CustomAction$Builder;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 5099
    invoke-virtual {v8, v7}, Landroid/media/session/PlaybackState$CustomAction$Builder;->setExtras(Landroid/os/Bundle;)Landroid/media/session/PlaybackState$CustomAction$Builder;

    .line 5100
    invoke-virtual {v8}, Landroid/media/session/PlaybackState$CustomAction$Builder;->build()Landroid/media/session/PlaybackState$CustomAction;

    move-result-object v4

    .line 4622
    iput-object v4, v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->e:Ljava/lang/Object;

    .line 4624
    iget-object v2, v2, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->e:Ljava/lang/Object;

    goto :goto_54

    .line 4514
    :cond_71
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x16

    if-lt v2, v3, :cond_ab

    .line 4515
    move-object/from16 v0, p1

    iget v3, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    move-object/from16 v0, p1

    iget-wide v4, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->C:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->D:J

    move-object/from16 v0, p1

    iget v8, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->E:F

    move-object/from16 v0, p1

    iget-wide v9, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->F:J

    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->G:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-wide v12, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->H:J

    move-object/from16 v0, p1

    iget-wide v15, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->J:J

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->K:Landroid/os/Bundle;

    move-object/from16 v17, v0

    invoke-static/range {v3 .. v17}, Landroid/support/v4/media/session/br;->a(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p1

    iput-object v2, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->L:Ljava/lang/Object;

    .line 4523
    :goto_a5
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->L:Ljava/lang/Object;

    goto/16 :goto_16

    .line 4519
    :cond_ab
    move-object/from16 v0, p1

    iget v3, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    move-object/from16 v0, p1

    iget-wide v4, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->C:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->D:J

    move-object/from16 v0, p1

    iget v8, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->E:F

    move-object/from16 v0, p1

    iget-wide v9, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->F:J

    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->G:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-wide v12, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->H:J

    move-object/from16 v0, p1

    iget-wide v15, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->J:J

    invoke-static/range {v3 .. v16}, Landroid/support/v4/media/session/bp;->a(IJJFJLjava/lang/CharSequence;JLjava/util/List;J)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p1

    iput-object v2, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->L:Ljava/lang/Object;

    goto :goto_a5
.end method

.method public final a(Landroid/support/v4/media/session/ad;Landroid/os/Handler;)V
    .registers 5

    .prologue
    .line 1931
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v4/media/session/ad;->a:Ljava/lang/Object;

    .line 4061
    check-cast v0, Landroid/media/session/MediaSession;

    check-cast v1, Landroid/media/session/MediaSession$Callback;

    invoke-virtual {v0, v1, p2}, Landroid/media/session/MediaSession;->setCallback(Landroid/media/session/MediaSession$Callback;Landroid/os/Handler;)V

    .line 1932
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 2010
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 10128
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0, p1}, Landroid/media/session/MediaSession;->setQueueTitle(Ljava/lang/CharSequence;)V

    .line 2011
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 1962
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 4088
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0, p1, p2}, Landroid/media/session/MediaSession;->sendSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1963
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .registers 9

    .prologue
    .line 1998
    const/4 v0, 0x0

    .line 1999
    if-eqz p1, :cond_3d

    .line 2000
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2001
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;

    .line 8847
    iget-object v1, v0, Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;->d:Ljava/lang/Object;

    if-nez v1, :cond_22

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-ge v1, v4, :cond_28

    .line 8848
    :cond_22
    iget-object v0, v0, Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;->d:Ljava/lang/Object;

    .line 2002
    :goto_24
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 8850
    :cond_28
    iget-object v1, v0, Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;->b:Landroid/support/v4/media/MediaDescriptionCompat;

    invoke-virtual {v1}, Landroid/support/v4/media/MediaDescriptionCompat;->a()Ljava/lang/Object;

    move-result-object v1

    iget-wide v4, v0, Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;->c:J

    .line 9240
    new-instance v6, Landroid/media/session/MediaSession$QueueItem;

    check-cast v1, Landroid/media/MediaDescription;

    invoke-direct {v6, v1, v4, v5}, Landroid/media/session/MediaSession$QueueItem;-><init>(Landroid/media/MediaDescription;J)V

    .line 8850
    iput-object v6, v0, Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;->d:Ljava/lang/Object;

    .line 8852
    iget-object v0, v0, Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;->d:Ljava/lang/Object;

    goto :goto_24

    :cond_3c
    move-object v0, v2

    .line 2005
    :cond_3d
    iget-object v1, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    invoke-static {v1, v0}, Landroid/support/v4/media/session/az;->a(Ljava/lang/Object;Ljava/util/List;)V

    .line 2006
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 1952
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 4080
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0, p1}, Landroid/media/session/MediaSession;->setActive(Z)V

    .line 1953
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 1957
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 4084
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->isActive()Z

    move-result v0

    .line 1957
    return v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 1967
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 4092
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->release()V

    .line 1968
    return-void
.end method

.method public final b(I)V
    .registers 4

    .prologue
    .line 1941
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 4070
    new-instance v1, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v1}, Landroid/media/AudioAttributes$Builder;-><init>()V

    .line 4071
    invoke-virtual {v1, p1}, Landroid/media/AudioAttributes$Builder;->setLegacyStreamType(I)Landroid/media/AudioAttributes$Builder;

    .line 4072
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v1}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackToLocal(Landroid/media/AudioAttributes;)V

    .line 1942
    return-void
.end method

.method public final b(Landroid/app/PendingIntent;)V
    .registers 3

    .prologue
    .line 1992
    iput-object p1, p0, Landroid/support/v4/media/session/ah;->c:Landroid/app/PendingIntent;

    .line 1993
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 8112
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0, p1}, Landroid/media/session/MediaSession;->setMediaButtonReceiver(Landroid/app/PendingIntent;)V

    .line 1994
    return-void
.end method

.method public final c()Landroid/support/v4/media/session/MediaSessionCompat$Token;
    .registers 2

    .prologue
    .line 1972
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    return-object v0
.end method

.method public final c(I)V
    .registers 4

    .prologue
    .line 2015
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_d

    .line 2018
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    .line 11023
    check-cast v0, Landroid/media/session/MediaSession;

    invoke-virtual {v0, p1}, Landroid/media/session/MediaSession;->setRatingType(I)V

    .line 2020
    :cond_d
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2029
    iget-object v0, p0, Landroid/support/v4/media/session/ah;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2034
    const/4 v0, 0x0

    return-object v0
.end method
