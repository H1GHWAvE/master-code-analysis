.class Landroid/support/v4/media/session/s;
.super Landroid/support/v4/media/session/r;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 1267
    invoke-direct {p0}, Landroid/support/v4/media/session/r;-><init>()V

    .line 1268
    iput-object p1, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 1269
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 1273
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2123
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0}, Landroid/media/session/MediaController$TransportControls;->play()V

    .line 1274
    return-void
.end method

.method public final a(J)V
    .registers 4

    .prologue
    .line 1335
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2167
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0, p1, p2}, Landroid/media/session/MediaController$TransportControls;->skipToQueueItem(J)V

    .line 1336
    return-void
.end method

.method public a(Landroid/net/Uri;Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 1331
    return-void
.end method

.method public final a(Landroid/support/v4/media/RatingCompat;)V
    .registers 4

    .prologue
    .line 1313
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    if-eqz p1, :cond_10

    invoke-virtual {p1}, Landroid/support/v4/media/RatingCompat;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2155
    :goto_8
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    check-cast v1, Landroid/media/Rating;

    invoke-virtual {v0, v1}, Landroid/media/session/MediaController$TransportControls;->setRating(Landroid/media/Rating;)V

    .line 1315
    return-void

    .line 1313
    :cond_10
    const/4 v1, 0x0

    goto :goto_8
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 1340
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2647
    iget-object v1, p1, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;->a:Ljava/lang/String;

    .line 1340
    invoke-static {v0, v1, p2}, Landroid/support/v4/media/session/z;->a(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1342
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 1319
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2159
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0, p1, p2}, Landroid/media/session/MediaController$TransportControls;->playFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1321
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 1278
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2127
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0}, Landroid/media/session/MediaController$TransportControls;->pause()V

    .line 1279
    return-void
.end method

.method public final b(J)V
    .registers 4

    .prologue
    .line 1288
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2135
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0, p1, p2}, Landroid/media/session/MediaController$TransportControls;->seekTo(J)V

    .line 1289
    return-void
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 1325
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2163
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0, p1, p2}, Landroid/media/session/MediaController$TransportControls;->playFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1327
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 1283
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2131
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0}, Landroid/media/session/MediaController$TransportControls;->stop()V

    .line 1284
    return-void
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 1346
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    invoke-static {v0, p1, p2}, Landroid/support/v4/media/session/z;->a(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1348
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 1293
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2139
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0}, Landroid/media/session/MediaController$TransportControls;->fastForward()V

    .line 1294
    return-void
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 1303
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2147
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0}, Landroid/media/session/MediaController$TransportControls;->skipToNext()V

    .line 1304
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 1298
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2143
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0}, Landroid/media/session/MediaController$TransportControls;->rewind()V

    .line 1299
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 1308
    iget-object v0, p0, Landroid/support/v4/media/session/s;->a:Ljava/lang/Object;

    .line 2151
    check-cast v0, Landroid/media/session/MediaController$TransportControls;

    invoke-virtual {v0}, Landroid/media/session/MediaController$TransportControls;->skipToPrevious()V

    .line 1309
    return-void
.end method
