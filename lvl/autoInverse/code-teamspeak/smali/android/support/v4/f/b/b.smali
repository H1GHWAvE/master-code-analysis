.class final Landroid/support/v4/f/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/f/b/g;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    return-void
.end method

.method private static a(Landroid/support/v4/f/b/m;)Landroid/support/v4/f/b/f;
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 272
    if-nez p0, :cond_4

    .line 281
    :cond_3
    :goto_3
    return-object v0

    .line 17132
    :cond_4
    iget-object v1, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 274
    if-eqz v1, :cond_10

    .line 275
    new-instance v0, Landroid/support/v4/f/b/f;

    .line 18132
    iget-object v1, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 275
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/f;-><init>(Ljavax/crypto/Cipher;)V

    goto :goto_3

    .line 19131
    :cond_10
    iget-object v1, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 276
    if-eqz v1, :cond_1c

    .line 277
    new-instance v0, Landroid/support/v4/f/b/f;

    .line 20131
    iget-object v1, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 277
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/f;-><init>(Ljava/security/Signature;)V

    goto :goto_3

    .line 20133
    :cond_1c
    iget-object v1, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 278
    if-eqz v1, :cond_3

    .line 279
    new-instance v0, Landroid/support/v4/f/b/f;

    .line 21133
    iget-object v1, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 279
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/f;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_3
.end method

.method private static a(Landroid/support/v4/f/b/d;)Landroid/support/v4/f/b/k;
    .registers 2

    .prologue
    .line 287
    new-instance v0, Landroid/support/v4/f/b/c;

    invoke-direct {v0, p0}, Landroid/support/v4/f/b/c;-><init>(Landroid/support/v4/f/b/d;)V

    return-object v0
.end method

.method private static a(Landroid/support/v4/f/b/f;)Landroid/support/v4/f/b/m;
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 257
    if-nez p0, :cond_4

    .line 266
    :cond_3
    :goto_3
    return-object v0

    .line 12137
    :cond_4
    iget-object v1, p0, Landroid/support/v4/f/b/f;->b:Ljavax/crypto/Cipher;

    .line 259
    if-eqz v1, :cond_10

    .line 260
    new-instance v0, Landroid/support/v4/f/b/m;

    .line 13137
    iget-object v1, p0, Landroid/support/v4/f/b/f;->b:Ljavax/crypto/Cipher;

    .line 260
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Cipher;)V

    goto :goto_3

    .line 14131
    :cond_10
    iget-object v1, p0, Landroid/support/v4/f/b/f;->a:Ljava/security/Signature;

    .line 261
    if-eqz v1, :cond_1c

    .line 262
    new-instance v0, Landroid/support/v4/f/b/m;

    .line 15131
    iget-object v1, p0, Landroid/support/v4/f/b/f;->a:Ljava/security/Signature;

    .line 262
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljava/security/Signature;)V

    goto :goto_3

    .line 15143
    :cond_1c
    iget-object v1, p0, Landroid/support/v4/f/b/f;->c:Ljavax/crypto/Mac;

    .line 263
    if-eqz v1, :cond_3

    .line 264
    new-instance v0, Landroid/support/v4/f/b/m;

    .line 16143
    iget-object v1, p0, Landroid/support/v4/f/b/f;->c:Ljavax/crypto/Mac;

    .line 264
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_3
.end method

.method private static synthetic b(Landroid/support/v4/f/b/m;)Landroid/support/v4/f/b/f;
    .registers 3

    .prologue
    .line 232
    .line 21272
    if-eqz p0, :cond_26

    .line 22132
    iget-object v0, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 21274
    if-eqz v0, :cond_e

    .line 21275
    new-instance v0, Landroid/support/v4/f/b/f;

    .line 23132
    iget-object v1, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 21275
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/f;-><init>(Ljavax/crypto/Cipher;)V

    .line 21279
    :goto_d
    return-object v0

    .line 24131
    :cond_e
    iget-object v0, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 21276
    if-eqz v0, :cond_1a

    .line 21277
    new-instance v0, Landroid/support/v4/f/b/f;

    .line 25131
    iget-object v1, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 21277
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/f;-><init>(Ljava/security/Signature;)V

    goto :goto_d

    .line 25133
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 21278
    if-eqz v0, :cond_26

    .line 21279
    new-instance v0, Landroid/support/v4/f/b/f;

    .line 26133
    iget-object v1, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 21279
    invoke-direct {v0, v1}, Landroid/support/v4/f/b/f;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_d

    .line 21281
    :cond_26
    const/4 v0, 0x0

    .line 232
    goto :goto_d
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/support/v4/f/b/f;ILandroid/support/v4/i/c;Landroid/support/v4/f/b/d;Landroid/os/Handler;)V
    .registers 13

    .prologue
    const/4 v1, 0x0

    .line 250
    .line 1257
    if-eqz p2, :cond_52

    .line 2137
    iget-object v0, p2, Landroid/support/v4/f/b/f;->b:Ljavax/crypto/Cipher;

    .line 1259
    if-eqz v0, :cond_38

    .line 1260
    new-instance v0, Landroid/support/v4/f/b/m;

    .line 3137
    iget-object v2, p2, Landroid/support/v4/f/b/f;->b:Ljavax/crypto/Cipher;

    .line 1260
    invoke-direct {v0, v2}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Cipher;)V

    move-object v3, v0

    .line 250
    :goto_f
    if-eqz p4, :cond_54

    invoke-virtual {p4}, Landroid/support/v4/i/c;->b()Ljava/lang/Object;

    move-result-object v2

    .line 6287
    :goto_15
    new-instance v5, Landroid/support/v4/f/b/c;

    invoke-direct {v5, p5}, Landroid/support/v4/f/b/c;-><init>(Landroid/support/v4/f/b/d;)V

    .line 7048
    invoke-static {p1}, Landroid/support/v4/f/b/i;->a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    .line 7054
    if-eqz v3, :cond_2b

    .line 7132
    iget-object v4, v3, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 7056
    if-eqz v4, :cond_56

    .line 7057
    new-instance v1, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 8132
    iget-object v3, v3, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 7057
    invoke-direct {v1, v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljavax/crypto/Cipher;)V

    .line 7048
    :cond_2b
    :goto_2b
    check-cast v2, Landroid/os/CancellationSignal;

    .line 12083
    new-instance v4, Landroid/support/v4/f/b/j;

    invoke-direct {v4, v5}, Landroid/support/v4/f/b/j;-><init>(Landroid/support/v4/f/b/k;)V

    move v3, p3

    move-object v5, p6

    .line 7048
    invoke-virtual/range {v0 .. v5}, Landroid/hardware/fingerprint/FingerprintManager;->authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;)V

    .line 253
    return-void

    .line 4131
    :cond_38
    iget-object v0, p2, Landroid/support/v4/f/b/f;->a:Ljava/security/Signature;

    .line 1261
    if-eqz v0, :cond_45

    .line 1262
    new-instance v0, Landroid/support/v4/f/b/m;

    .line 5131
    iget-object v2, p2, Landroid/support/v4/f/b/f;->a:Ljava/security/Signature;

    .line 1262
    invoke-direct {v0, v2}, Landroid/support/v4/f/b/m;-><init>(Ljava/security/Signature;)V

    move-object v3, v0

    goto :goto_f

    .line 5143
    :cond_45
    iget-object v0, p2, Landroid/support/v4/f/b/f;->c:Ljavax/crypto/Mac;

    .line 1263
    if-eqz v0, :cond_52

    .line 1264
    new-instance v0, Landroid/support/v4/f/b/m;

    .line 6143
    iget-object v2, p2, Landroid/support/v4/f/b/f;->c:Ljavax/crypto/Mac;

    .line 1264
    invoke-direct {v0, v2}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Mac;)V

    move-object v3, v0

    goto :goto_f

    :cond_52
    move-object v3, v1

    .line 1266
    goto :goto_f

    :cond_54
    move-object v2, v1

    .line 250
    goto :goto_15

    .line 9131
    :cond_56
    iget-object v4, v3, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 7058
    if-eqz v4, :cond_62

    .line 7059
    new-instance v1, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 10131
    iget-object v3, v3, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 7059
    invoke-direct {v1, v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljava/security/Signature;)V

    goto :goto_2b

    .line 10133
    :cond_62
    iget-object v4, v3, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 7060
    if-eqz v4, :cond_2b

    .line 7061
    new-instance v1, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 11133
    iget-object v3, v3, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 7061
    invoke-direct {v1, v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_2b
.end method

.method public final a(Landroid/content/Context;)Z
    .registers 3

    .prologue
    .line 239
    .line 1039
    invoke-static {p1}, Landroid/support/v4/f/b/i;->a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->hasEnrolledFingerprints()Z

    move-result v0

    .line 239
    return v0
.end method

.method public final b(Landroid/content/Context;)Z
    .registers 3

    .prologue
    .line 244
    .line 1043
    invoke-static {p1}, Landroid/support/v4/f/b/i;->a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->isHardwareDetected()Z

    move-result v0

    .line 244
    return v0
.end method
