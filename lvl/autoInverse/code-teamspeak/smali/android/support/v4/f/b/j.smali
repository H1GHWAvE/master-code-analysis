.class final Landroid/support/v4/f/b/j;
.super Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v4/f/b/k;


# direct methods
.method constructor <init>(Landroid/support/v4/f/b/k;)V
    .registers 2

    .prologue
    .line 83
    iput-object p1, p0, Landroid/support/v4/f/b/j;->a:Landroid/support/v4/f/b/k;

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAuthenticationError(ILjava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 87
    return-void
.end method

.method public final onAuthenticationFailed()V
    .registers 1

    .prologue
    .line 103
    return-void
.end method

.method public final onAuthenticationHelp(ILjava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 92
    return-void
.end method

.method public final onAuthenticationSucceeded(Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V
    .registers 6

    .prologue
    .line 96
    iget-object v1, p0, Landroid/support/v4/f/b/j;->a:Landroid/support/v4/f/b/k;

    new-instance v2, Landroid/support/v4/f/b/l;

    invoke-virtual {p1}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;->getCryptoObject()Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    move-result-object v3

    .line 1068
    if-eqz v3, :cond_40

    .line 1070
    invoke-virtual {v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 1071
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Cipher;)V

    .line 96
    :goto_19
    invoke-direct {v2, v0}, Landroid/support/v4/f/b/l;-><init>(Landroid/support/v4/f/b/m;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/f/b/k;->a(Landroid/support/v4/f/b/l;)V

    .line 98
    return-void

    .line 1072
    :cond_20
    invoke-virtual {v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getSignature()Ljava/security/Signature;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 1073
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getSignature()Ljava/security/Signature;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v4/f/b/m;-><init>(Ljava/security/Signature;)V

    goto :goto_19

    .line 1074
    :cond_30
    invoke-virtual {v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getMac()Ljavax/crypto/Mac;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 1075
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {v3}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getMac()Ljavax/crypto/Mac;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_19

    .line 1077
    :cond_40
    const/4 v0, 0x0

    goto :goto_19
.end method
