.class public final Landroid/support/v4/f/b/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/security/Signature;

.field final b:Ljavax/crypto/Cipher;

.field final c:Ljavax/crypto/Mac;


# direct methods
.method public constructor <init>(Ljava/security/Signature;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 115
    iput-object v0, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 116
    iput-object v0, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 117
    return-void
.end method

.method public constructor <init>(Ljavax/crypto/Cipher;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p1, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 121
    iput-object v0, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 122
    iput-object v0, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 123
    return-void
.end method

.method public constructor <init>(Ljavax/crypto/Mac;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p1, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 127
    iput-object v0, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 128
    iput-object v0, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 129
    return-void
.end method

.method private a()Ljava/security/Signature;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    return-object v0
.end method

.method private b()Ljavax/crypto/Cipher;
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    return-object v0
.end method

.method private c()Ljavax/crypto/Mac;
    .registers 2

    .prologue
    .line 133
    iget-object v0, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    return-object v0
.end method
