.class final Landroid/support/v7/widget/b;
.super Landroid/support/v7/internal/view/menu/v;
.source "SourceFile"


# instance fields
.field final synthetic g:Landroid/support/v7/widget/ActionMenuPresenter;

.field private h:Landroid/support/v7/internal/view/menu/ad;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;Landroid/support/v7/internal/view/menu/ad;)V
    .registers 10

    .prologue
    const/4 v4, 0x0

    .line 715
    iput-object p1, p0, Landroid/support/v7/widget/b;->g:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 716
    const/4 v3, 0x0

    sget v5, Landroid/support/v7/a/d;->actionOverflowMenuStyle:I

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/internal/view/menu/v;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;ZI)V

    .line 718
    iput-object p3, p0, Landroid/support/v7/widget/b;->h:Landroid/support/v7/internal/view/menu/ad;

    .line 720
    invoke-virtual {p3}, Landroid/support/v7/internal/view/menu/ad;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 721
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->f()Z

    move-result v0

    if-nez v0, :cond_24

    .line 1053
    iget-object v0, p1, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    .line 723
    if-nez v0, :cond_43

    .line 2053
    iget-object v0, p1, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    .line 723
    check-cast v0, Landroid/view/View;

    .line 3113
    :goto_22
    iput-object v0, p0, Landroid/support/v7/internal/view/menu/v;->b:Landroid/view/View;

    .line 726
    :cond_24
    iget-object v0, p1, Landroid/support/v7/widget/ActionMenuPresenter;->s:Landroid/support/v7/widget/h;

    .line 3271
    iput-object v0, p0, Landroid/support/v7/internal/view/menu/v;->d:Landroid/support/v7/internal/view/menu/y;

    .line 729
    invoke-virtual {p3}, Landroid/support/v7/internal/view/menu/ad;->size()I

    move-result v1

    move v0, v4

    .line 730
    :goto_2d
    if-ge v0, v1, :cond_40

    .line 731
    invoke-virtual {p3, v0}, Landroid/support/v7/internal/view/menu/ad;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 732
    invoke-interface {v2}, Landroid/view/MenuItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_46

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_46

    .line 733
    const/4 v4, 0x1

    .line 4117
    :cond_40
    iput-boolean v4, p0, Landroid/support/v7/internal/view/menu/v;->e:Z

    .line 738
    return-void

    .line 3053
    :cond_43
    iget-object v0, p1, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    goto :goto_22

    .line 730
    :cond_46
    add-int/lit8 v0, v0, 0x1

    goto :goto_2d
.end method


# virtual methods
.method public final onDismiss()V
    .registers 3

    .prologue
    .line 742
    invoke-super {p0}, Landroid/support/v7/internal/view/menu/v;->onDismiss()V

    .line 743
    iget-object v0, p0, Landroid/support/v7/widget/b;->g:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 5053
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->q:Landroid/support/v7/widget/b;

    .line 744
    iget-object v0, p0, Landroid/support/v7/widget/b;->g:Landroid/support/v7/widget/ActionMenuPresenter;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->t:I

    .line 745
    return-void
.end method
