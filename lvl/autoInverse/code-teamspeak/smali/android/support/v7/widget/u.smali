.class final Landroid/support/v7/widget/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/content/res/ColorStateList;

.field b:Landroid/graphics/PorterDuff$Mode;

.field private final c:Landroid/widget/CompoundButton;

.field private final d:Landroid/support/v7/internal/widget/av;

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method constructor <init>(Landroid/widget/CompoundButton;Landroid/support/v7/internal/widget/av;)V
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v1, p0, Landroid/support/v7/widget/u;->a:Landroid/content/res/ColorStateList;

    .line 39
    iput-object v1, p0, Landroid/support/v7/widget/u;->b:Landroid/graphics/PorterDuff$Mode;

    .line 40
    iput-boolean v0, p0, Landroid/support/v7/widget/u;->e:Z

    .line 41
    iput-boolean v0, p0, Landroid/support/v7/widget/u;->f:Z

    .line 53
    iput-object p1, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    .line 54
    iput-object p2, p0, Landroid/support/v7/widget/u;->d:Landroid/support/v7/internal/widget/av;

    .line 55
    return-void
.end method

.method private b()Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Landroid/support/v7/widget/u;->a:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method private c()Landroid/graphics/PorterDuff$Mode;
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Landroid/support/v7/widget/u;->b:Landroid/graphics/PorterDuff$Mode;

    return-object v0
.end method

.method private d()V
    .registers 3

    .prologue
    .line 116
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    invoke-static {v0}, Landroid/support/v4/widget/g;->a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_3e

    iget-boolean v1, p0, Landroid/support/v7/widget/u;->e:Z

    if-nez v1, :cond_10

    iget-boolean v1, p0, Landroid/support/v7/widget/u;->f:Z

    if-eqz v1, :cond_3e

    .line 119
    :cond_10
    invoke-static {v0}, Landroid/support/v4/e/a/a;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 121
    iget-boolean v1, p0, Landroid/support/v7/widget/u;->e:Z

    if-eqz v1, :cond_21

    .line 122
    iget-object v1, p0, Landroid/support/v7/widget/u;->a:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 124
    :cond_21
    iget-boolean v1, p0, Landroid/support/v7/widget/u;->f:Z

    if-eqz v1, :cond_2a

    .line 125
    iget-object v1, p0, Landroid/support/v7/widget/u;->b:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v0, v1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 129
    :cond_2a
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 130
    iget-object v1, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 132
    :cond_39
    iget-object v1, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 134
    :cond_3e
    return-void
.end method


# virtual methods
.method final a(I)I
    .registers 4

    .prologue
    .line 137
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_13

    .line 140
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    invoke-static {v0}, Landroid/support/v4/widget/g;->a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_13

    .line 142
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    add-int/2addr p1, v0

    .line 145
    :cond_13
    return p1
.end method

.method final a()V
    .registers 2

    .prologue
    .line 106
    iget-boolean v0, p0, Landroid/support/v7/widget/u;->g:Z

    if-eqz v0, :cond_8

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/u;->g:Z

    .line 113
    :goto_7
    return-void

    .line 111
    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/u;->g:Z

    .line 112
    invoke-direct {p0}, Landroid/support/v7/widget/u;->d()V

    goto :goto_7
.end method

.method final a(Landroid/content/res/ColorStateList;)V
    .registers 3

    .prologue
    .line 84
    iput-object p1, p0, Landroid/support/v7/widget/u;->a:Landroid/content/res/ColorStateList;

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/u;->e:Z

    .line 87
    invoke-direct {p0}, Landroid/support/v7/widget/u;->d()V

    .line 88
    return-void
.end method

.method final a(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 95
    iput-object p1, p0, Landroid/support/v7/widget/u;->b:Landroid/graphics/PorterDuff$Mode;

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/u;->f:Z

    .line 98
    invoke-direct {p0}, Landroid/support/v7/widget/u;->d()V

    .line 99
    return-void
.end method

.method final a(Landroid/util/AttributeSet;I)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/a/n;->CompoundButton:[I

    invoke-virtual {v0, p1, v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 61
    :try_start_d
    sget v0, Landroid/support/v7/a/n;->CompoundButton_android_button:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 62
    sget v0, Landroid/support/v7/a/n;->CompoundButton_android_button:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 64
    if-eqz v0, :cond_2a

    .line 65
    iget-object v2, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    iget-object v3, p0, Landroid/support/v7/widget/u;->d:Landroid/support/v7/internal/widget/av;

    .line 1167
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 65
    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 68
    :cond_2a
    sget v0, Landroid/support/v7/a/n;->CompoundButton_buttonTint:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 69
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    sget v2, Landroid/support/v7/a/n;->CompoundButton_buttonTint:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/widget/g;->a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V

    .line 72
    :cond_3d
    sget v0, Landroid/support/v7/a/n;->CompoundButton_buttonTintMode:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 73
    iget-object v0, p0, Landroid/support/v7/widget/u;->c:Landroid/widget/CompoundButton;

    sget v2, Landroid/support/v7/a/n;->CompoundButton_buttonTintMode:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-static {v2}, Landroid/support/v7/b/a/a;->a(I)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/widget/g;->a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
    :try_end_55
    .catchall {:try_start_d .. :try_end_55} :catchall_59

    .line 79
    :cond_55
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 80
    return-void

    .line 79
    :catchall_59
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method
