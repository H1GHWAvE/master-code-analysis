.class final Landroid/support/v7/widget/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/aa;

.field final synthetic b:Landroid/support/v7/widget/ad;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/ad;Landroid/support/v7/widget/aa;)V
    .registers 3

    .prologue
    .line 695
    iput-object p1, p0, Landroid/support/v7/widget/ae;->b:Landroid/support/v7/widget/ad;

    iput-object p2, p0, Landroid/support/v7/widget/ae;->a:Landroid/support/v7/widget/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10

    .prologue
    .line 698
    iget-object v0, p0, Landroid/support/v7/widget/ae;->b:Landroid/support/v7/widget/ad;

    iget-object v0, v0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-virtual {v0, p3}, Landroid/support/v7/widget/aa;->setSelection(I)V

    .line 699
    iget-object v0, p0, Landroid/support/v7/widget/ae;->b:Landroid/support/v7/widget/ad;

    iget-object v0, v0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    invoke-virtual {v0}, Landroid/support/v7/widget/aa;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 700
    iget-object v0, p0, Landroid/support/v7/widget/ae;->b:Landroid/support/v7/widget/ad;

    iget-object v0, v0, Landroid/support/v7/widget/ad;->b:Landroid/support/v7/widget/aa;

    iget-object v1, p0, Landroid/support/v7/widget/ae;->b:Landroid/support/v7/widget/ad;

    invoke-static {v1}, Landroid/support/v7/widget/ad;->a(Landroid/support/v7/widget/ad;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v0, p2, p3, v2, v3}, Landroid/support/v7/widget/aa;->performItemClick(Landroid/view/View;IJ)Z

    .line 703
    :cond_22
    iget-object v0, p0, Landroid/support/v7/widget/ae;->b:Landroid/support/v7/widget/ad;

    invoke-virtual {v0}, Landroid/support/v7/widget/ad;->d()V

    .line 704
    return-void
.end method
