.class public Landroid/support/v7/widget/ai;
.super Landroid/widget/TextView;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v7/widget/ah;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ai;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 44
    const v0, 0x1010084

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/ai;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    new-instance v0, Landroid/support/v7/widget/ah;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ah;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/ah;

    .line 51
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/ah;->a(Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method


# virtual methods
.method public setTextAppearance(Landroid/content/Context;I)V
    .registers 4

    .prologue
    .line 56
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 57
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/ah;

    if-eqz v0, :cond_c

    .line 58
    iget-object v0, p0, Landroid/support/v7/widget/ai;->a:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/ah;->a(Landroid/content/Context;I)V

    .line 60
    :cond_c
    return-void
.end method
