.class final Landroid/support/v7/widget/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/support/v7/internal/widget/av;

.field private c:Landroid/support/v7/internal/widget/au;

.field private d:Landroid/support/v7/internal/widget/au;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/support/v7/internal/widget/av;)V
    .registers 3

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Landroid/support/v7/widget/q;->a:Landroid/view/View;

    .line 41
    iput-object p2, p0, Landroid/support/v7/widget/q;->b:Landroid/support/v7/internal/widget/av;

    .line 42
    return-void
.end method

.method private d()V
    .registers 2

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/q;->b(Landroid/content/res/ColorStateList;)V

    .line 78
    return-void
.end method


# virtual methods
.method final a()Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    iget-object v0, v0, Landroid/support/v7/internal/widget/au;->a:Landroid/content/res/ColorStateList;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method final a(I)V
    .registers 3

    .prologue
    .line 72
    iget-object v0, p0, Landroid/support/v7/widget/q;->b:Landroid/support/v7/internal/widget/av;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/q;->b:Landroid/support/v7/internal/widget/av;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/av;->a(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_a
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/q;->b(Landroid/content/res/ColorStateList;)V

    .line 73
    return-void

    .line 72
    :cond_e
    const/4 v0, 0x0

    goto :goto_a
.end method

.method final a(Landroid/content/res/ColorStateList;)V
    .registers 4

    .prologue
    .line 81
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    if-nez v0, :cond_b

    .line 82
    new-instance v0, Landroid/support/v7/internal/widget/au;

    invoke-direct {v0}, Landroid/support/v7/internal/widget/au;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    .line 84
    :cond_b
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    iput-object p1, v0, Landroid/support/v7/internal/widget/au;->a:Landroid/content/res/ColorStateList;

    .line 85
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/internal/widget/au;->d:Z

    .line 87
    invoke-virtual {p0}, Landroid/support/v7/widget/q;->c()V

    .line 88
    return-void
.end method

.method final a(Landroid/graphics/PorterDuff$Mode;)V
    .registers 4

    .prologue
    .line 95
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    if-nez v0, :cond_b

    .line 96
    new-instance v0, Landroid/support/v7/internal/widget/au;

    invoke-direct {v0}, Landroid/support/v7/internal/widget/au;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    .line 98
    :cond_b
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    iput-object p1, v0, Landroid/support/v7/internal/widget/au;->b:Landroid/graphics/PorterDuff$Mode;

    .line 99
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/internal/widget/au;->c:Z

    .line 101
    invoke-virtual {p0}, Landroid/support/v7/widget/q;->c()V

    .line 102
    return-void
.end method

.method final a(Landroid/util/AttributeSet;I)V
    .registers 7

    .prologue
    .line 45
    iget-object v0, p0, Landroid/support/v7/widget/q;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/a/n;->ViewBackgroundHelper:[I

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 48
    :try_start_d
    sget v0, Landroid/support/v7/a/n;->ViewBackgroundHelper_android_background:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 49
    iget-object v0, p0, Landroid/support/v7/widget/q;->b:Landroid/support/v7/internal/widget/av;

    sget v2, Landroid/support/v7/a/n;->ViewBackgroundHelper_android_background:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/av;->a(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_27

    .line 52
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/q;->b(Landroid/content/res/ColorStateList;)V

    .line 55
    :cond_27
    sget v0, Landroid/support/v7/a/n;->ViewBackgroundHelper_backgroundTint:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 56
    iget-object v0, p0, Landroid/support/v7/widget/q;->a:Landroid/view/View;

    sget v2, Landroid/support/v7/a/n;->ViewBackgroundHelper_backgroundTint:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 59
    :cond_3a
    sget v0, Landroid/support/v7/a/n;->ViewBackgroundHelper_backgroundTintMode:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 60
    iget-object v0, p0, Landroid/support/v7/widget/q;->a:Landroid/view/View;

    sget v2, Landroid/support/v7/a/n;->ViewBackgroundHelper_backgroundTintMode:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-static {v2}, Landroid/support/v7/b/a/a;->a(I)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
    :try_end_52
    .catchall {:try_start_d .. :try_end_52} :catchall_56

    .line 66
    :cond_52
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 67
    return-void

    .line 66
    :catchall_56
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method final b()Landroid/graphics/PorterDuff$Mode;
    .registers 2

    .prologue
    .line 105
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    iget-object v0, v0, Landroid/support/v7/internal/widget/au;->b:Landroid/graphics/PorterDuff$Mode;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method final b(Landroid/content/res/ColorStateList;)V
    .registers 4

    .prologue
    .line 119
    if-eqz p1, :cond_1a

    .line 120
    iget-object v0, p0, Landroid/support/v7/widget/q;->c:Landroid/support/v7/internal/widget/au;

    if-nez v0, :cond_d

    .line 121
    new-instance v0, Landroid/support/v7/internal/widget/au;

    invoke-direct {v0}, Landroid/support/v7/internal/widget/au;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/q;->c:Landroid/support/v7/internal/widget/au;

    .line 123
    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/q;->c:Landroid/support/v7/internal/widget/au;

    iput-object p1, v0, Landroid/support/v7/internal/widget/au;->a:Landroid/content/res/ColorStateList;

    .line 124
    iget-object v0, p0, Landroid/support/v7/widget/q;->c:Landroid/support/v7/internal/widget/au;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/internal/widget/au;->d:Z

    .line 128
    :goto_16
    invoke-virtual {p0}, Landroid/support/v7/widget/q;->c()V

    .line 129
    return-void

    .line 126
    :cond_1a
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/q;->c:Landroid/support/v7/internal/widget/au;

    goto :goto_16
.end method

.method final c()V
    .registers 3

    .prologue
    .line 109
    iget-object v0, p0, Landroid/support/v7/widget/q;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 110
    iget-object v0, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    if-eqz v0, :cond_14

    .line 111
    iget-object v0, p0, Landroid/support/v7/widget/q;->a:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/widget/q;->d:Landroid/support/v7/internal/widget/au;

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/av;->a(Landroid/view/View;Landroid/support/v7/internal/widget/au;)V

    .line 116
    :cond_13
    :goto_13
    return-void

    .line 112
    :cond_14
    iget-object v0, p0, Landroid/support/v7/widget/q;->c:Landroid/support/v7/internal/widget/au;

    if-eqz v0, :cond_13

    .line 113
    iget-object v0, p0, Landroid/support/v7/widget/q;->a:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/widget/q;->c:Landroid/support/v7/internal/widget/au;

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/av;->a(Landroid/view/View;Landroid/support/v7/internal/widget/au;)V

    goto :goto_13
.end method
