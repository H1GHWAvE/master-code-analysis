.class final Landroid/support/v7/widget/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/bu;


# direct methods
.method private constructor <init>(Landroid/support/v7/widget/bu;)V
    .registers 2

    .prologue
    .line 316
    iput-object p1, p0, Landroid/support/v7/widget/by;->a:Landroid/support/v7/widget/bu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/widget/bu;B)V
    .registers 3

    .prologue
    .line 316
    invoke-direct {p0, p1}, Landroid/support/v7/widget/by;-><init>(Landroid/support/v7/widget/bu;)V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .registers 5

    .prologue
    .line 319
    iget-object v0, p0, Landroid/support/v7/widget/by;->a:Landroid/support/v7/widget/bu;

    .line 1087
    iget-object v0, v0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    .line 319
    iget-object v1, p0, Landroid/support/v7/widget/by;->a:Landroid/support/v7/widget/bu;

    .line 2087
    iget-object v1, v1, Landroid/support/v7/widget/bu;->e:Ljava/lang/String;

    .line 319
    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/l;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;

    move-result-object v0

    .line 321
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 322
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/l;->b(I)Landroid/content/Intent;

    move-result-object v0

    .line 323
    if-eqz v0, :cond_34

    .line 324
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 325
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2a

    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 3087
    :cond_2a
    invoke-static {v0}, Landroid/support/v7/widget/bu;->a(Landroid/content/Intent;)V

    .line 329
    :cond_2d
    iget-object v1, p0, Landroid/support/v7/widget/by;->a:Landroid/support/v7/widget/bu;

    .line 4087
    iget-object v1, v1, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    .line 329
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 331
    :cond_34
    const/4 v0, 0x1

    return v0
.end method
