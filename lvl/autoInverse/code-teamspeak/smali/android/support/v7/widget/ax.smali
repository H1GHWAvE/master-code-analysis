.class final Landroid/support/v7/widget/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/an;


# direct methods
.method private constructor <init>(Landroid/support/v7/widget/an;)V
    .registers 2

    .prologue
    .line 1751
    iput-object p1, p0, Landroid/support/v7/widget/ax;->a:Landroid/support/v7/widget/an;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/widget/an;B)V
    .registers 3

    .prologue
    .line 1751
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ax;-><init>(Landroid/support/v7/widget/an;)V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .registers 5

    .prologue
    .line 1755
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .registers 5

    .prologue
    .line 1758
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2f

    iget-object v0, p0, Landroid/support/v7/widget/ax;->a:Landroid/support/v7/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/widget/an;->g()Z

    move-result v0

    if-nez v0, :cond_2f

    iget-object v0, p0, Landroid/support/v7/widget/ax;->a:Landroid/support/v7/widget/an;

    invoke-static {v0}, Landroid/support/v7/widget/an;->b(Landroid/support/v7/widget/an;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 1760
    iget-object v0, p0, Landroid/support/v7/widget/ax;->a:Landroid/support/v7/widget/an;

    invoke-static {v0}, Landroid/support/v7/widget/an;->d(Landroid/support/v7/widget/an;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ax;->a:Landroid/support/v7/widget/an;

    invoke-static {v1}, Landroid/support/v7/widget/an;->c(Landroid/support/v7/widget/an;)Landroid/support/v7/widget/az;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1761
    iget-object v0, p0, Landroid/support/v7/widget/ax;->a:Landroid/support/v7/widget/an;

    invoke-static {v0}, Landroid/support/v7/widget/an;->c(Landroid/support/v7/widget/an;)Landroid/support/v7/widget/az;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/az;->run()V

    .line 1763
    :cond_2f
    return-void
.end method
