.class final Landroid/support/v7/widget/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I


# instance fields
.field private final c:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-array v0, v3, [I

    const v1, 0x1010034

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/ah;->a:[I

    .line 32
    new-array v0, v3, [I

    sget v1, Landroid/support/v7/a/d;->textAllCaps:I

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/ah;->b:[I

    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;)V
    .registers 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Landroid/support/v7/widget/ah;->c:Landroid/widget/TextView;

    .line 38
    return-void
.end method


# virtual methods
.method final a(Landroid/content/Context;I)V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 89
    sget-object v0, Landroid/support/v7/widget/ah;->b:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 90
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 91
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ah;->a(Z)V

    .line 93
    :cond_14
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 94
    return-void
.end method

.method final a(Landroid/util/AttributeSet;I)V
    .registers 9

    .prologue
    const v5, 0x1010038

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 41
    iget-object v0, p0, Landroid/support/v7/widget/ah;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 44
    sget-object v1, Landroid/support/v7/widget/ah;->a:[I

    invoke-virtual {v0, p1, v1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 45
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 46
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    if-eq v2, v4, :cond_34

    .line 50
    sget-object v1, Landroid/support/v7/a/n;->TextAppearance:[I

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 51
    sget v2, Landroid/support/v7/a/n;->TextAppearance_textAllCaps:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 52
    sget v2, Landroid/support/v7/a/n;->TextAppearance_textAllCaps:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ah;->a(Z)V

    .line 54
    :cond_31
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    :cond_34
    sget-object v1, Landroid/support/v7/widget/ah;->b:[I

    invoke-virtual {v0, p1, v1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 59
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_47

    .line 60
    invoke-virtual {v1, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ah;->a(Z)V

    .line 62
    :cond_47
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 64
    iget-object v1, p0, Landroid/support/v7/widget/ah;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 65
    if-eqz v1, :cond_6f

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v2

    if-nez v2, :cond_6f

    .line 70
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_70

    .line 73
    invoke-static {v0, v5}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v0

    .line 83
    :goto_62
    iget-object v2, p0, Landroid/support/v7/widget/ah;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-static {v1, v0}, Landroid/support/v7/internal/widget/ar;->a(II)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 86
    :cond_6f
    return-void

    .line 79
    :cond_70
    invoke-static {v0, v5}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v0

    goto :goto_62
.end method

.method final a(Z)V
    .registers 5

    .prologue
    .line 97
    iget-object v1, p0, Landroid/support/v7/widget/ah;->c:Landroid/widget/TextView;

    if-eqz p1, :cond_13

    new-instance v0, Landroid/support/v7/internal/b/a;

    iget-object v2, p0, Landroid/support/v7/widget/ah;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/internal/b/a;-><init>(Landroid/content/Context;)V

    :goto_f
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 100
    return-void

    .line 97
    :cond_13
    const/4 v0, 0x0

    goto :goto_f
.end method
