.class public abstract Landroid/support/v7/widget/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:F

.field private final b:I

.field private final c:I

.field private final d:Landroid/view/View;

.field private e:Ljava/lang/Runnable;

.field private f:Ljava/lang/Runnable;

.field private g:Z

.field private h:Z

.field private i:I

.field private final j:[I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 1247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1245
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v7/widget/as;->j:[I

    .line 1248
    iput-object p1, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    .line 1249
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Landroid/support/v7/widget/as;->a:F

    .line 1250
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/as;->b:I

    .line 1252
    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/support/v7/widget/as;->c:I

    .line 1253
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/as;)Landroid/view/View;
    .registers 2

    .prologue
    .line 1214
    iget-object v0, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    return-object v0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1333
    iget-object v3, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    .line 1334
    invoke-virtual {v3}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_b

    .line 1373
    :cond_a
    :goto_a
    return v0

    .line 1338
    :cond_b
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1339
    packed-switch v2, :pswitch_data_92

    goto :goto_a

    .line 1341
    :pswitch_13
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/as;->i:I

    .line 1342
    iput-boolean v0, p0, Landroid/support/v7/widget/as;->h:Z

    .line 1344
    iget-object v1, p0, Landroid/support/v7/widget/as;->e:Ljava/lang/Runnable;

    if-nez v1, :cond_26

    .line 1345
    new-instance v1, Landroid/support/v7/widget/at;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/widget/at;-><init>(Landroid/support/v7/widget/as;B)V

    iput-object v1, p0, Landroid/support/v7/widget/as;->e:Ljava/lang/Runnable;

    .line 1347
    :cond_26
    iget-object v1, p0, Landroid/support/v7/widget/as;->e:Ljava/lang/Runnable;

    iget v2, p0, Landroid/support/v7/widget/as;->b:I

    int-to-long v4, v2

    invoke-virtual {v3, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1348
    iget-object v1, p0, Landroid/support/v7/widget/as;->f:Ljava/lang/Runnable;

    if-nez v1, :cond_39

    .line 1349
    new-instance v1, Landroid/support/v7/widget/au;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/widget/au;-><init>(Landroid/support/v7/widget/as;B)V

    iput-object v1, p0, Landroid/support/v7/widget/as;->f:Ljava/lang/Runnable;

    .line 1351
    :cond_39
    iget-object v1, p0, Landroid/support/v7/widget/as;->f:Ljava/lang/Runnable;

    iget v2, p0, Landroid/support/v7/widget/as;->c:I

    int-to-long v4, v2

    invoke-virtual {v3, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_a

    .line 1354
    :pswitch_42
    iget v2, p0, Landroid/support/v7/widget/as;->i:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    .line 1355
    if-ltz v2, :cond_a

    .line 1356
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    .line 1357
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 1358
    iget v5, p0, Landroid/support/v7/widget/as;->a:F

    .line 4449
    neg-float v6, v5

    cmpl-float v6, v4, v6

    if-ltz v6, :cond_8b

    neg-float v6, v5

    cmpl-float v6, v2, v6

    if-ltz v6, :cond_8b

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    add-float/2addr v6, v5

    cmpg-float v4, v4, v6

    if-gez v4, :cond_8b

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v4, v6

    int-to-float v4, v4

    add-float/2addr v4, v5

    cmpg-float v2, v2, v4

    if-gez v2, :cond_8b

    move v2, v1

    .line 1358
    :goto_7d
    if-nez v2, :cond_a

    .line 1359
    invoke-direct {p0}, Landroid/support/v7/widget/as;->d()V

    .line 1362
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    move v0, v1

    .line 1363
    goto :goto_a

    :cond_8b
    move v2, v0

    .line 4449
    goto :goto_7d

    .line 1369
    :pswitch_8d
    invoke-direct {p0}, Landroid/support/v7/widget/as;->d()V

    goto/16 :goto_a

    .line 1339
    :pswitch_data_92
    .packed-switch 0x0
        :pswitch_13
        :pswitch_8d
        :pswitch_42
        :pswitch_8d
    .end packed-switch
.end method

.method private static a(Landroid/view/View;FFF)Z
    .registers 6

    .prologue
    .line 1449
    neg-float v0, p3

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_2a

    neg-float v0, p3

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_2a

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2a

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float v0, p2, v0

    if-gez v0, :cond_2a

    const/4 v0, 0x1

    :goto_29
    return v0

    :cond_2a
    const/4 v0, 0x0

    goto :goto_29
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    .line 1459
    iget-object v0, p0, Landroid/support/v7/widget/as;->j:[I

    .line 1460
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1461
    const/4 v1, 0x0

    aget v1, v0, v1

    neg-int v1, v1

    int-to-float v1, v1

    aget v0, v0, v2

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p2, v1, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1462
    return v2
.end method

.method static synthetic b(Landroid/support/v7/widget/as;)V
    .registers 11

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 1214
    .line 7387
    invoke-direct {p0}, Landroid/support/v7/widget/as;->d()V

    .line 7389
    iget-object v8, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    .line 7390
    invoke-virtual {v8}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-virtual {v8}, Landroid/view/View;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 7393
    :cond_13
    :goto_13
    return-void

    .line 7396
    :cond_14
    invoke-virtual {p0}, Landroid/support/v7/widget/as;->b()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 7401
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 7404
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 7405
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 7406
    invoke-virtual {v8, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 7407
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 7409
    iput-boolean v9, p0, Landroid/support/v7/widget/as;->g:Z

    .line 7410
    iput-boolean v9, p0, Landroid/support/v7/widget/as;->h:Z

    goto :goto_13
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1420
    iget-object v2, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    .line 1421
    invoke-virtual {p0}, Landroid/support/v7/widget/as;->a()Landroid/support/v7/widget/an;

    move-result-object v3

    .line 1422
    if-eqz v3, :cond_12

    .line 4760
    iget-object v4, v3, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    .line 1422
    if-nez v4, :cond_14

    :cond_12
    move v0, v1

    .line 1445
    :cond_13
    :goto_13
    return v0

    .line 1426
    :cond_14
    invoke-static {v3}, Landroid/support/v7/widget/an;->a(Landroid/support/v7/widget/an;)Landroid/support/v7/widget/ar;

    move-result-object v3

    .line 1427
    if-eqz v3, :cond_20

    invoke-virtual {v3}, Landroid/support/v7/widget/ar;->isShown()Z

    move-result v4

    if-nez v4, :cond_22

    :cond_20
    move v0, v1

    .line 1428
    goto :goto_13

    .line 1432
    :cond_22
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    .line 5470
    iget-object v5, p0, Landroid/support/v7/widget/as;->j:[I

    .line 5471
    invoke-virtual {v2, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 5472
    aget v2, v5, v1

    int-to-float v2, v2

    aget v5, v5, v0

    int-to-float v5, v5

    invoke-virtual {v4, v2, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 6459
    iget-object v2, p0, Landroid/support/v7/widget/as;->j:[I

    .line 6460
    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 6461
    aget v5, v2, v1

    neg-int v5, v5

    int-to-float v5, v5

    aget v2, v2, v0

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v4, v5, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1437
    iget v2, p0, Landroid/support/v7/widget/as;->i:I

    invoke-virtual {v3, v4, v2}, Landroid/support/v7/widget/ar;->a(Landroid/view/MotionEvent;I)Z

    move-result v3

    .line 1438
    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    .line 1441
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1442
    if-eq v2, v0, :cond_5d

    const/4 v4, 0x3

    if-eq v2, v4, :cond_5d

    move v2, v0

    .line 1445
    :goto_57
    if-eqz v3, :cond_5b

    if-nez v2, :cond_13

    :cond_5b
    move v0, v1

    goto :goto_13

    :cond_5d
    move v2, v1

    .line 1442
    goto :goto_57
.end method

.method private b(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    .line 1470
    iget-object v0, p0, Landroid/support/v7/widget/as;->j:[I

    .line 1471
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1472
    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    aget v0, v0, v2

    int-to-float v0, v0

    invoke-virtual {p2, v1, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1473
    return v2
.end method

.method private d()V
    .registers 3

    .prologue
    .line 1377
    iget-object v0, p0, Landroid/support/v7/widget/as;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_b

    .line 1378
    iget-object v0, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/widget/as;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1381
    :cond_b
    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_16

    .line 1382
    iget-object v0, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/widget/as;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1384
    :cond_16
    return-void
.end method

.method private e()V
    .registers 11

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 1387
    invoke-direct {p0}, Landroid/support/v7/widget/as;->d()V

    .line 1389
    iget-object v8, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    .line 1390
    invoke-virtual {v8}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-virtual {v8}, Landroid/view/View;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1411
    :cond_13
    :goto_13
    return-void

    .line 1396
    :cond_14
    invoke-virtual {p0}, Landroid/support/v7/widget/as;->b()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1401
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1404
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1405
    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1406
    invoke-virtual {v8, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1407
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1409
    iput-boolean v9, p0, Landroid/support/v7/widget/as;->g:Z

    .line 1410
    iput-boolean v9, p0, Landroid/support/v7/widget/as;->h:Z

    goto :goto_13
.end method


# virtual methods
.method public abstract a()Landroid/support/v7/widget/an;
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 1305
    invoke-virtual {p0}, Landroid/support/v7/widget/as;->a()Landroid/support/v7/widget/an;

    move-result-object v0

    .line 1306
    if-eqz v0, :cond_11

    .line 2760
    iget-object v1, v0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    .line 1306
    if-nez v1, :cond_11

    .line 1307
    invoke-virtual {v0}, Landroid/support/v7/widget/an;->b()V

    .line 1309
    :cond_11
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 1319
    invoke-virtual {p0}, Landroid/support/v7/widget/as;->a()Landroid/support/v7/widget/an;

    move-result-object v0

    .line 1320
    if-eqz v0, :cond_11

    .line 3760
    iget-object v1, v0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    .line 1320
    if-eqz v1, :cond_11

    .line 1321
    invoke-virtual {v0}, Landroid/support/v7/widget/an;->d()V

    .line 1323
    :cond_11
    const/4 v0, 0x1

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 14

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1269
    iget-boolean v10, p0, Landroid/support/v7/widget/as;->g:Z

    .line 1271
    if-eqz v10, :cond_27

    .line 1272
    iget-boolean v0, p0, Landroid/support/v7/widget/as;->h:Z

    if-eqz v0, :cond_17

    .line 1276
    invoke-direct {p0, p2}, Landroid/support/v7/widget/as;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1293
    :goto_f
    iput-boolean v0, p0, Landroid/support/v7/widget/as;->g:Z

    .line 1294
    if-nez v0, :cond_15

    if-eqz v10, :cond_16

    :cond_15
    move v7, v8

    :cond_16
    return v7

    .line 1278
    :cond_17
    invoke-direct {p0, p2}, Landroid/support/v7/widget/as;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_23

    invoke-virtual {p0}, Landroid/support/v7/widget/as;->c()Z

    move-result v0

    if-nez v0, :cond_25

    :cond_23
    move v0, v8

    goto :goto_f

    :cond_25
    move v0, v7

    goto :goto_f

    .line 2333
    :cond_27
    iget-object v1, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    .line 2334
    invoke-virtual {v1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 2338
    invoke-static {p2}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 2339
    packed-switch v0, :pswitch_data_da

    :cond_36
    :goto_36
    move v0, v7

    .line 1281
    :goto_37
    if-eqz v0, :cond_d7

    invoke-virtual {p0}, Landroid/support/v7/widget/as;->b()Z

    move-result v0

    if-eqz v0, :cond_d7

    move v9, v8

    .line 1283
    :goto_40
    if-eqz v9, :cond_55

    .line 1285
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1286
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1288
    iget-object v1, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1289
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :cond_55
    move v0, v9

    goto :goto_f

    .line 2341
    :pswitch_57
    invoke-virtual {p2, v7}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/as;->i:I

    .line 2342
    iput-boolean v7, p0, Landroid/support/v7/widget/as;->h:Z

    .line 2344
    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Ljava/lang/Runnable;

    if-nez v0, :cond_6a

    .line 2345
    new-instance v0, Landroid/support/v7/widget/at;

    invoke-direct {v0, p0, v7}, Landroid/support/v7/widget/at;-><init>(Landroid/support/v7/widget/as;B)V

    iput-object v0, p0, Landroid/support/v7/widget/as;->e:Ljava/lang/Runnable;

    .line 2347
    :cond_6a
    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Ljava/lang/Runnable;

    iget v2, p0, Landroid/support/v7/widget/as;->b:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2348
    iget-object v0, p0, Landroid/support/v7/widget/as;->f:Ljava/lang/Runnable;

    if-nez v0, :cond_7d

    .line 2349
    new-instance v0, Landroid/support/v7/widget/au;

    invoke-direct {v0, p0, v7}, Landroid/support/v7/widget/au;-><init>(Landroid/support/v7/widget/as;B)V

    iput-object v0, p0, Landroid/support/v7/widget/as;->f:Ljava/lang/Runnable;

    .line 2351
    :cond_7d
    iget-object v0, p0, Landroid/support/v7/widget/as;->f:Ljava/lang/Runnable;

    iget v2, p0, Landroid/support/v7/widget/as;->c:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_36

    .line 2354
    :pswitch_86
    iget v0, p0, Landroid/support/v7/widget/as;->i:I

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 2355
    if-ltz v0, :cond_36

    .line 2356
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 2357
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 2358
    iget v3, p0, Landroid/support/v7/widget/as;->a:F

    .line 2449
    neg-float v4, v3

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_d0

    neg-float v4, v3

    cmpl-float v4, v0, v4

    if-ltz v4, :cond_d0

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v6

    sub-int/2addr v4, v6

    int-to-float v4, v4

    add-float/2addr v4, v3

    cmpg-float v2, v2, v4

    if-gez v2, :cond_d0

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpg-float v0, v0, v2

    if-gez v0, :cond_d0

    move v0, v8

    .line 2358
    :goto_c1
    if-nez v0, :cond_36

    .line 2359
    invoke-direct {p0}, Landroid/support/v7/widget/as;->d()V

    .line 2362
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    move v0, v8

    .line 2363
    goto/16 :goto_37

    :cond_d0
    move v0, v7

    .line 2449
    goto :goto_c1

    .line 2369
    :pswitch_d2
    invoke-direct {p0}, Landroid/support/v7/widget/as;->d()V

    goto/16 :goto_36

    :cond_d7
    move v9, v7

    .line 1281
    goto/16 :goto_40

    .line 2339
    :pswitch_data_da
    .packed-switch 0x0
        :pswitch_57
        :pswitch_d2
        :pswitch_86
        :pswitch_d2
    .end packed-switch
.end method
