.class final Landroid/support/v7/app/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/app/l;


# instance fields
.field final a:Landroid/app/Activity;

.field b:Landroid/support/v7/app/u;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .registers 2

    .prologue
    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 508
    iput-object p1, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    .line 509
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Activity;B)V
    .registers 3

    .prologue
    .line 502
    invoke-direct {p0, p1}, Landroid/support/v7/app/q;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 513
    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    invoke-static {v0}, Landroid/support/v7/app/t;->a(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .registers 4

    .prologue
    .line 545
    iget-object v0, p0, Landroid/support/v7/app/q;->b:Landroid/support/v7/app/u;

    iget-object v1, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    invoke-static {v0, v1, p1}, Landroid/support/v7/app/t;->a(Landroid/support/v7/app/u;Landroid/app/Activity;I)Landroid/support/v7/app/u;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/q;->b:Landroid/support/v7/app/u;

    .line 547
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;I)V
    .registers 5

    .prologue
    .line 537
    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 538
    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2}, Landroid/support/v7/app/t;->a(Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Landroid/support/v7/app/u;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/q;->b:Landroid/support/v7/app/u;

    .line 540
    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 541
    return-void
.end method

.method public final b()Landroid/content/Context;
    .registers 2

    .prologue
    .line 518
    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 520
    if-eqz v0, :cond_d

    .line 521
    invoke-virtual {v0}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    .line 525
    :goto_c
    return-object v0

    .line 523
    :cond_d
    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    goto :goto_c
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 530
    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 531
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
