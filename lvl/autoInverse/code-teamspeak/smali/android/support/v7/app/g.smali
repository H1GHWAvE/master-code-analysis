.class public abstract Landroid/support/v7/app/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = -0x1


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(I)Landroid/support/v7/app/g;
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param
.end method

.method public abstract a(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/app/g;
.end method

.method public abstract a(Landroid/support/v7/app/h;)Landroid/support/v7/app/g;
.end method

.method public abstract a(Landroid/view/View;)Landroid/support/v7/app/g;
.end method

.method public abstract a(Ljava/lang/CharSequence;)Landroid/support/v7/app/g;
.end method

.method public abstract a(Ljava/lang/Object;)Landroid/support/v7/app/g;
.end method

.method public abstract b()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract b(I)Landroid/support/v7/app/g;
.end method

.method public abstract b(Ljava/lang/CharSequence;)Landroid/support/v7/app/g;
.end method

.method public abstract c(I)Landroid/support/v7/app/g;
.end method

.method public abstract c()Ljava/lang/CharSequence;
.end method

.method public abstract d(I)Landroid/support/v7/app/g;
.end method

.method public abstract d()Landroid/view/View;
.end method

.method public abstract e()Ljava/lang/Object;
.end method

.method public abstract f()V
.end method

.method public abstract g()Ljava/lang/CharSequence;
.end method
