.class final Landroid/support/v7/app/bc;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/app/AppCompatDelegateImplV7;


# direct methods
.method public constructor <init>(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 1953
    iput-object p1, p0, Landroid/support/v7/app/bc;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    .line 1954
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1955
    return-void
.end method

.method private a(II)Z
    .registers 4

    .prologue
    const/4 v0, -0x5

    .line 1983
    if-lt p1, v0, :cond_15

    if-lt p2, v0, :cond_15

    invoke-virtual {p0}, Landroid/support/v7/app/bc;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    if-gt p1, v0, :cond_15

    invoke-virtual {p0}, Landroid/support/v7/app/bc;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    if-le p2, v0, :cond_17

    :cond_15
    const/4 v0, 0x1

    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method


# virtual methods
.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3

    .prologue
    .line 1959
    iget-object v0, p0, Landroid/support/v7/app/bc;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x5

    const/4 v0, 0x1

    .line 1965
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 1966
    if-nez v1, :cond_36

    .line 1967
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 1968
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 2983
    if-lt v1, v4, :cond_27

    if-lt v3, v4, :cond_27

    invoke-virtual {p0}, Landroid/support/v7/app/bc;->getWidth()I

    move-result v4

    add-int/lit8 v4, v4, 0x5

    if-gt v1, v4, :cond_27

    invoke-virtual {p0}, Landroid/support/v7/app/bc;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x5

    if-le v3, v1, :cond_34

    :cond_27
    move v1, v0

    .line 1969
    :goto_28
    if-eqz v1, :cond_36

    .line 1970
    iget-object v1, p0, Landroid/support/v7/app/bc;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    .line 3315
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    .line 1974
    :goto_33
    return v0

    :cond_34
    move v1, v2

    .line 2983
    goto :goto_28

    .line 1974
    :cond_36
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_33
.end method

.method public final setBackgroundResource(I)V
    .registers 3

    .prologue
    .line 1979
    invoke-virtual {p0}, Landroid/support/v7/app/bc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/app/bc;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1980
    return-void
.end method
