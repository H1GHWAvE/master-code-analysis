.class public final Landroid/support/v7/app/bj;
.super Landroid/support/v4/app/dm;
.source "SourceFile"


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/support/v4/app/dm;-><init>(Landroid/content/Context;)V

    .line 81
    return-void
.end method


# virtual methods
.method protected final d()Landroid/support/v4/app/dn;
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_d

    .line 86
    new-instance v0, Landroid/support/v7/app/bm;

    invoke-direct {v0, v2}, Landroid/support/v7/app/bm;-><init>(B)V

    .line 92
    :goto_c
    return-object v0

    .line 87
    :cond_d
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_19

    .line 88
    new-instance v0, Landroid/support/v7/app/bl;

    invoke-direct {v0, v2}, Landroid/support/v7/app/bl;-><init>(B)V

    goto :goto_c

    .line 89
    :cond_19
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_25

    .line 90
    new-instance v0, Landroid/support/v7/app/bk;

    invoke-direct {v0, v2}, Landroid/support/v7/app/bk;-><init>(B)V

    goto :goto_c

    .line 92
    :cond_25
    invoke-super {p0}, Landroid/support/v4/app/dm;->d()Landroid/support/v4/app/dn;

    move-result-object v0

    goto :goto_c
.end method
