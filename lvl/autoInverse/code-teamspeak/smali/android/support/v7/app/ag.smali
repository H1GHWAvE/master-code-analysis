.class public final Landroid/support/v7/app/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/support/v7/app/x;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 269
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v7/app/af;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/ag;-><init>(Landroid/content/Context;I)V

    .line 270
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 6

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    new-instance v0, Landroid/support/v7/app/x;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-static {p1, p2}, Landroid/support/v7/app/af;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/support/v7/app/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    .line 282
    iput p2, p0, Landroid/support/v7/app/ag;->b:I

    .line 283
    return-void
.end method

.method private a(I)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    .line 304
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/x;->f:Ljava/lang/CharSequence;

    .line 305
    return-object p0
.end method

.method private a(IILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 6

    .prologue
    .line 690
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    .line 691
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p3, v0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 692
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p2, v0, Landroid/support/v7/app/x;->F:I

    .line 693
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/x;->E:Z

    .line 694
    return-object p0
.end method

.method private a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 5

    .prologue
    .line 399
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/x;->i:Ljava/lang/CharSequence;

    .line 400
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->j:Landroid/content/DialogInterface$OnClickListener;

    .line 401
    return-object p0
.end method

.method private a(I[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/support/v7/app/ag;
    .registers 6

    .prologue
    .line 601
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    .line 602
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p3, v0, Landroid/support/v7/app/x;->G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    .line 603
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->C:[Z

    .line 604
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/x;->D:Z

    .line 605
    return-object p0
.end method

.method private a(Landroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 494
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->p:Landroid/content/DialogInterface$OnCancelListener;

    .line 495
    return-object p0
.end method

.method private a(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 504
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->q:Landroid/content/DialogInterface$OnDismissListener;

    .line 505
    return-object p0
.end method

.method private a(Landroid/content/DialogInterface$OnKeyListener;)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 514
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->r:Landroid/content/DialogInterface$OnKeyListener;

    .line 515
    return-object p0
.end method

.method private a(Landroid/database/Cursor;ILjava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 7

    .prologue
    .line 718
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    .line 719
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p4, v0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 720
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p2, v0, Landroid/support/v7/app/x;->F:I

    .line 721
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p3, v0, Landroid/support/v7/app/x;->I:Ljava/lang/String;

    .line 722
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/x;->E:Z

    .line 723
    return-object p0
.end method

.method private a(Landroid/database/Cursor;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;)Landroid/support/v7/app/ag;
    .registers 5

    .prologue
    .line 572
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    .line 573
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p3, v0, Landroid/support/v7/app/x;->I:Ljava/lang/String;

    .line 574
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 575
    return-object p0
.end method

.method private a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/support/v7/app/ag;
    .registers 7

    .prologue
    .line 663
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    .line 664
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p4, v0, Landroid/support/v7/app/x;->G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    .line 665
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->J:Ljava/lang/String;

    .line 666
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p3, v0, Landroid/support/v7/app/x;->I:Ljava/lang/String;

    .line 667
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/x;->D:Z

    .line 668
    return-object p0
.end method

.method private a(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 371
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->d:Landroid/graphics/drawable/Drawable;

    .line 372
    return-object p0
.end method

.method private a(Landroid/view/View;)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 329
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->g:Landroid/view/View;

    .line 330
    return-object p0
.end method

.method private a(Landroid/view/View;IIII)Landroid/support/v7/app/ag;
    .registers 8

    .prologue
    .line 844
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->w:Landroid/view/View;

    .line 845
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/app/x;->v:I

    .line 846
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/x;->B:Z

    .line 847
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p2, v0, Landroid/support/v7/app/x;->x:I

    .line 848
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p3, v0, Landroid/support/v7/app/x;->y:I

    .line 849
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p4, v0, Landroid/support/v7/app/x;->z:I

    .line 850
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p5, v0, Landroid/support/v7/app/x;->A:I

    .line 851
    return-object p0
.end method

.method private a(Landroid/widget/AdapterView$OnItemSelectedListener;)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 787
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->L:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 788
    return-object p0
.end method

.method private a(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 6

    .prologue
    .line 771
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    .line 772
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p3, v0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 773
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p2, v0, Landroid/support/v7/app/x;->F:I

    .line 774
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/x;->E:Z

    .line 775
    return-object p0
.end method

.method private a(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    .line 554
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    .line 555
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 556
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 314
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->f:Ljava/lang/CharSequence;

    .line 315
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    .line 412
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->i:Ljava/lang/CharSequence;

    .line 413
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->j:Landroid/content/DialogInterface$OnClickListener;

    .line 414
    return-object p0
.end method

.method private a(Z)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 475
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-boolean p1, v0, Landroid/support/v7/app/x;->o:Z

    .line 476
    return-object p0
.end method

.method private a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 6

    .prologue
    .line 745
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    .line 746
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p3, v0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 747
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p2, v0, Landroid/support/v7/app/x;->F:I

    .line 748
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/x;->E:Z

    .line 749
    return-object p0
.end method

.method private a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    .line 539
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    .line 540
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 541
    return-object p0
.end method

.method private a([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/support/v7/app/ag;
    .registers 6

    .prologue
    .line 630
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    .line 631
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p3, v0, Landroid/support/v7/app/x;->G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    .line 632
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->C:[Z

    .line 633
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/x;->D:Z

    .line 634
    return-object p0
.end method

.method private b()Landroid/content/Context;
    .registers 2

    .prologue
    .line 295
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v0, v0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    return-object v0
.end method

.method private b(I)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    .line 339
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/x;->h:Ljava/lang/CharSequence;

    .line 340
    return-object p0
.end method

.method private b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 5

    .prologue
    .line 425
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/x;->k:Ljava/lang/CharSequence;

    .line 426
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->l:Landroid/content/DialogInterface$OnClickListener;

    .line 427
    return-object p0
.end method

.method private b(Landroid/view/View;)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 814
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->w:Landroid/view/View;

    .line 815
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput v1, v0, Landroid/support/v7/app/x;->v:I

    .line 816
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-boolean v1, v0, Landroid/support/v7/app/x;->B:Z

    .line 817
    return-object p0
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 349
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->h:Ljava/lang/CharSequence;

    .line 350
    return-object p0
.end method

.method private b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    .line 438
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->k:Ljava/lang/CharSequence;

    .line 439
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->l:Landroid/content/DialogInterface$OnClickListener;

    .line 440
    return-object p0
.end method

.method private b(Z)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 862
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-boolean p1, v0, Landroid/support/v7/app/x;->K:Z

    .line 863
    return-object p0
.end method

.method private c()Landroid/support/v7/app/af;
    .registers 2

    .prologue
    .line 901
    invoke-virtual {p0}, Landroid/support/v7/app/ag;->a()Landroid/support/v7/app/af;

    move-result-object v0

    .line 902
    invoke-virtual {v0}, Landroid/support/v7/app/af;->show()V

    .line 903
    return-object v0
.end method

.method private c(I)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 361
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p1, v0, Landroid/support/v7/app/x;->c:I

    .line 362
    return-object p0
.end method

.method private c(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 5

    .prologue
    .line 451
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/x;->m:Ljava/lang/CharSequence;

    .line 452
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->n:Landroid/content/DialogInterface$OnClickListener;

    .line 453
    return-object p0
.end method

.method private c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    .line 464
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p1, v0, Landroid/support/v7/app/x;->m:Ljava/lang/CharSequence;

    .line 465
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->n:Landroid/content/DialogInterface$OnClickListener;

    .line 466
    return-object p0
.end method

.method private c(Z)Landroid/support/v7/app/ag;
    .registers 3

    .prologue
    .line 870
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-boolean p1, v0, Landroid/support/v7/app/x;->N:Z

    .line 871
    return-object p0
.end method

.method private d(I)Landroid/support/v7/app/ag;
    .registers 5

    .prologue
    .line 385
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 386
    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 387
    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    iput v0, v1, Landroid/support/v7/app/x;->c:I

    .line 388
    return-object p0
.end method

.method private d(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/ag;
    .registers 5

    .prologue
    .line 526
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    .line 527
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p2, v0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 528
    return-object p0
.end method

.method private e(I)Landroid/support/v7/app/ag;
    .registers 4

    .prologue
    .line 800
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/app/x;->w:Landroid/view/View;

    .line 801
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput p1, v0, Landroid/support/v7/app/x;->v:I

    .line 802
    iget-object v0, p0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/app/x;->B:Z

    .line 803
    return-object p0
.end method


# virtual methods
.method public final a()Landroid/support/v7/app/af;
    .registers 21

    .prologue
    .line 882
    new-instance v19, Landroid/support/v7/app/af;

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/app/ag;->b:I

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/app/af;-><init>(Landroid/content/Context;IB)V

    .line 883
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    invoke-static/range {v19 .. v19}, Landroid/support/v7/app/af;->a(Landroid/support/v7/app/af;)Landroid/support/v7/app/v;

    move-result-object v12

    .line 1677
    iget-object v1, v2, Landroid/support/v7/app/x;->g:Landroid/view/View;

    if-eqz v1, :cond_fd

    .line 1678
    iget-object v1, v2, Landroid/support/v7/app/x;->g:Landroid/view/View;

    .line 2241
    iput-object v1, v12, Landroid/support/v7/app/v;->C:Landroid/view/View;

    .line 1693
    :cond_22
    :goto_22
    iget-object v1, v2, Landroid/support/v7/app/x;->h:Ljava/lang/CharSequence;

    if-eqz v1, :cond_2b

    .line 1694
    iget-object v1, v2, Landroid/support/v7/app/x;->h:Ljava/lang/CharSequence;

    invoke-virtual {v12, v1}, Landroid/support/v7/app/v;->b(Ljava/lang/CharSequence;)V

    .line 1696
    :cond_2b
    iget-object v1, v2, Landroid/support/v7/app/x;->i:Ljava/lang/CharSequence;

    if-eqz v1, :cond_38

    .line 1697
    const/4 v1, -0x1

    iget-object v3, v2, Landroid/support/v7/app/x;->i:Ljava/lang/CharSequence;

    iget-object v4, v2, Landroid/support/v7/app/x;->j:Landroid/content/DialogInterface$OnClickListener;

    const/4 v5, 0x0

    invoke-virtual {v12, v1, v3, v4, v5}, Landroid/support/v7/app/v;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 1700
    :cond_38
    iget-object v1, v2, Landroid/support/v7/app/x;->k:Ljava/lang/CharSequence;

    if-eqz v1, :cond_45

    .line 1701
    const/4 v1, -0x2

    iget-object v3, v2, Landroid/support/v7/app/x;->k:Ljava/lang/CharSequence;

    iget-object v4, v2, Landroid/support/v7/app/x;->l:Landroid/content/DialogInterface$OnClickListener;

    const/4 v5, 0x0

    invoke-virtual {v12, v1, v3, v4, v5}, Landroid/support/v7/app/v;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 1704
    :cond_45
    iget-object v1, v2, Landroid/support/v7/app/x;->m:Ljava/lang/CharSequence;

    if-eqz v1, :cond_52

    .line 1705
    const/4 v1, -0x3

    iget-object v3, v2, Landroid/support/v7/app/x;->m:Ljava/lang/CharSequence;

    iget-object v4, v2, Landroid/support/v7/app/x;->n:Landroid/content/DialogInterface$OnClickListener;

    const/4 v5, 0x0

    invoke-virtual {v12, v1, v3, v4, v5}, Landroid/support/v7/app/v;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 1710
    :cond_52
    iget-object v1, v2, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    if-nez v1, :cond_5e

    iget-object v1, v2, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    if-nez v1, :cond_5e

    iget-object v1, v2, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_a1

    .line 2734
    :cond_5e
    iget-object v1, v2, Landroid/support/v7/app/x;->b:Landroid/view/LayoutInflater;

    .line 3057
    iget v3, v12, Landroid/support/v7/app/v;->H:I

    .line 2734
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 2737
    iget-boolean v1, v2, Landroid/support/v7/app/x;->D:Z

    if-eqz v1, :cond_142

    .line 2738
    iget-object v1, v2, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    if-nez v1, :cond_134

    .line 2739
    new-instance v1, Landroid/support/v7/app/y;

    iget-object v3, v2, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    .line 4057
    iget v4, v12, Landroid/support/v7/app/v;->I:I

    .line 2739
    iget-object v5, v2, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    invoke-direct/range {v1 .. v6}, Landroid/support/v7/app/y;-><init>(Landroid/support/v7/app/x;Landroid/content/Context;I[Ljava/lang/CharSequence;Landroid/widget/ListView;)V

    .line 7057
    :goto_7c
    iput-object v1, v12, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    .line 2801
    iget v1, v2, Landroid/support/v7/app/x;->F:I

    .line 8057
    iput v1, v12, Landroid/support/v7/app/v;->E:I

    .line 2803
    iget-object v1, v2, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v1, :cond_185

    .line 2804
    new-instance v1, Landroid/support/v7/app/aa;

    invoke-direct {v1, v2, v12}, Landroid/support/v7/app/aa;-><init>(Landroid/support/v7/app/x;Landroid/support/v7/app/v;)V

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2827
    :cond_8e
    :goto_8e
    iget-object v1, v2, Landroid/support/v7/app/x;->L:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v1, :cond_97

    .line 2828
    iget-object v1, v2, Landroid/support/v7/app/x;->L:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2831
    :cond_97
    iget-boolean v1, v2, Landroid/support/v7/app/x;->E:Z

    if-eqz v1, :cond_193

    .line 2832
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 9057
    :cond_9f
    :goto_9f
    iput-object v6, v12, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    .line 1713
    :cond_a1
    iget-object v1, v2, Landroid/support/v7/app/x;->w:Landroid/view/View;

    if-eqz v1, :cond_1a4

    .line 1714
    iget-boolean v1, v2, Landroid/support/v7/app/x;->B:Z

    if-eqz v1, :cond_19d

    .line 1715
    iget-object v13, v2, Landroid/support/v7/app/x;->w:Landroid/view/View;

    iget v14, v2, Landroid/support/v7/app/x;->x:I

    iget v15, v2, Landroid/support/v7/app/x;->y:I

    iget v0, v2, Landroid/support/v7/app/x;->z:I

    move/from16 v16, v0

    iget v0, v2, Landroid/support/v7/app/x;->A:I

    move/from16 v17, v0

    invoke-virtual/range {v12 .. v17}, Landroid/support/v7/app/v;->a(Landroid/view/View;IIII)V

    .line 884
    :cond_ba
    :goto_ba
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-boolean v1, v1, Landroid/support/v7/app/x;->o:Z

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/support/v7/app/af;->setCancelable(Z)V

    .line 885
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-boolean v1, v1, Landroid/support/v7/app/x;->o:Z

    if-eqz v1, :cond_d3

    .line 886
    const/4 v1, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/support/v7/app/af;->setCanceledOnTouchOutside(Z)V

    .line 888
    :cond_d3
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->p:Landroid/content/DialogInterface$OnCancelListener;

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/support/v7/app/af;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 889
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->q:Landroid/content/DialogInterface$OnDismissListener;

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/support/v7/app/af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 890
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->r:Landroid/content/DialogInterface$OnKeyListener;

    if-eqz v1, :cond_fc

    .line 891
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->r:Landroid/content/DialogInterface$OnKeyListener;

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/support/v7/app/af;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 893
    :cond_fc
    return-object v19

    .line 1680
    :cond_fd
    iget-object v1, v2, Landroid/support/v7/app/x;->f:Ljava/lang/CharSequence;

    if-eqz v1, :cond_106

    .line 1681
    iget-object v1, v2, Landroid/support/v7/app/x;->f:Ljava/lang/CharSequence;

    invoke-virtual {v12, v1}, Landroid/support/v7/app/v;->a(Ljava/lang/CharSequence;)V

    .line 1683
    :cond_106
    iget-object v1, v2, Landroid/support/v7/app/x;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_10f

    .line 1684
    iget-object v1, v2, Landroid/support/v7/app/x;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v1}, Landroid/support/v7/app/v;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1686
    :cond_10f
    iget v1, v2, Landroid/support/v7/app/x;->c:I

    if-eqz v1, :cond_118

    .line 1687
    iget v1, v2, Landroid/support/v7/app/x;->c:I

    invoke-virtual {v12, v1}, Landroid/support/v7/app/v;->a(I)V

    .line 1689
    :cond_118
    iget v1, v2, Landroid/support/v7/app/x;->e:I

    if-eqz v1, :cond_22

    .line 1690
    iget v1, v2, Landroid/support/v7/app/x;->e:I

    .line 2374
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 2375
    iget-object v4, v12, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v1, v3, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2376
    iget v1, v3, Landroid/util/TypedValue;->resourceId:I

    .line 1690
    invoke-virtual {v12, v1}, Landroid/support/v7/app/v;->a(I)V

    goto/16 :goto_22

    .line 2754
    :cond_134
    new-instance v7, Landroid/support/v7/app/z;

    iget-object v9, v2, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v10, v2, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    move-object v8, v2

    move-object v11, v6

    invoke-direct/range {v7 .. v12}, Landroid/support/v7/app/z;-><init>(Landroid/support/v7/app/x;Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ListView;Landroid/support/v7/app/v;)V

    move-object v1, v7

    goto/16 :goto_7c

    .line 2782
    :cond_142
    iget-boolean v1, v2, Landroid/support/v7/app/x;->E:Z

    if-eqz v1, :cond_154

    .line 5057
    iget v15, v12, Landroid/support/v7/app/v;->J:I

    .line 2784
    :goto_148
    iget-object v1, v2, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    if-nez v1, :cond_162

    .line 2785
    iget-object v1, v2, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_157

    iget-object v1, v2, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    goto/16 :goto_7c

    .line 6057
    :cond_154
    iget v15, v12, Landroid/support/v7/app/v;->K:I

    goto :goto_148

    .line 2785
    :cond_157
    new-instance v1, Landroid/support/v7/app/ae;

    iget-object v3, v2, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v4, v2, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    invoke-direct {v1, v3, v15, v4}, Landroid/support/v7/app/ae;-><init>(Landroid/content/Context;I[Ljava/lang/CharSequence;)V

    goto/16 :goto_7c

    .line 2788
    :cond_162
    new-instance v13, Landroid/widget/SimpleCursorAdapter;

    iget-object v14, v2, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v0, v2, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    move-object/from16 v16, v0

    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v1, 0x0

    iget-object v3, v2, Landroid/support/v7/app/x;->I:Ljava/lang/String;

    aput-object v3, v17, v1

    const/4 v1, 0x1

    new-array v0, v1, [I

    move-object/from16 v18, v0

    const/4 v1, 0x0

    const v3, 0x1020014

    aput v3, v18, v1

    invoke-direct/range {v13 .. v18}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    move-object v1, v13

    goto/16 :goto_7c

    .line 2813
    :cond_185
    iget-object v1, v2, Landroid/support/v7/app/x;->G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    if-eqz v1, :cond_8e

    .line 2814
    new-instance v1, Landroid/support/v7/app/ab;

    invoke-direct {v1, v2, v6, v12}, Landroid/support/v7/app/ab;-><init>(Landroid/support/v7/app/x;Landroid/widget/ListView;Landroid/support/v7/app/v;)V

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_8e

    .line 2833
    :cond_193
    iget-boolean v1, v2, Landroid/support/v7/app/x;->D:Z

    if-eqz v1, :cond_9f

    .line 2834
    const/4 v1, 0x2

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto/16 :goto_9f

    .line 1718
    :cond_19d
    iget-object v1, v2, Landroid/support/v7/app/x;->w:Landroid/view/View;

    invoke-virtual {v12, v1}, Landroid/support/v7/app/v;->b(Landroid/view/View;)V

    goto/16 :goto_ba

    .line 1720
    :cond_1a4
    iget v1, v2, Landroid/support/v7/app/x;->v:I

    if-eqz v1, :cond_ba

    .line 1721
    iget v1, v2, Landroid/support/v7/app/x;->v:I

    .line 9255
    const/4 v2, 0x0

    iput-object v2, v12, Landroid/support/v7/app/v;->g:Landroid/view/View;

    .line 9256
    iput v1, v12, Landroid/support/v7/app/v;->h:I

    .line 9257
    const/4 v1, 0x0

    iput-boolean v1, v12, Landroid/support/v7/app/v;->m:Z

    goto/16 :goto_ba
.end method
