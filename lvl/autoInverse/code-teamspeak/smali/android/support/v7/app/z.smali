.class final Landroid/support/v7/app/z;
.super Landroid/widget/CursorAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/widget/ListView;

.field final synthetic b:Landroid/support/v7/app/v;

.field final synthetic c:Landroid/support/v7/app/x;

.field private final d:I

.field private final e:I


# direct methods
.method constructor <init>(Landroid/support/v7/app/x;Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ListView;Landroid/support/v7/app/v;)V
    .registers 8

    .prologue
    .line 754
    iput-object p1, p0, Landroid/support/v7/app/z;->c:Landroid/support/v7/app/x;

    iput-object p4, p0, Landroid/support/v7/app/z;->a:Landroid/widget/ListView;

    iput-object p5, p0, Landroid/support/v7/app/z;->b:Landroid/support/v7/app/v;

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 759
    invoke-virtual {p0}, Landroid/support/v7/app/z;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 760
    iget-object v1, p0, Landroid/support/v7/app/z;->c:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->I:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Landroid/support/v7/app/z;->d:I

    .line 761
    iget-object v1, p0, Landroid/support/v7/app/z;->c:Landroid/support/v7/app/x;

    iget-object v1, v1, Landroid/support/v7/app/x;->J:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/app/z;->e:I

    .line 762
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 8

    .prologue
    const/4 v1, 0x1

    .line 766
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 768
    iget v2, p0, Landroid/support/v7/app/z;->d:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 769
    iget-object v2, p0, Landroid/support/v7/app/z;->a:Landroid/widget/ListView;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    iget v0, p0, Landroid/support/v7/app/z;->e:I

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_26

    move v0, v1

    :goto_22
    invoke-virtual {v2, v3, v0}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 771
    return-void

    .line 769
    :cond_26
    const/4 v0, 0x0

    goto :goto_22
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    .line 775
    iget-object v0, p0, Landroid/support/v7/app/z;->c:Landroid/support/v7/app/x;

    iget-object v0, v0, Landroid/support/v7/app/x;->b:Landroid/view/LayoutInflater;

    iget-object v1, p0, Landroid/support/v7/app/z;->b:Landroid/support/v7/app/v;

    .line 1057
    iget v1, v1, Landroid/support/v7/app/v;->I:I

    .line 775
    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
