.class public final Landroid/support/v7/internal/a/q;
.super Landroid/support/v7/app/g;
.source "SourceFile"


# instance fields
.field b:Landroid/support/v7/app/h;

.field c:I

.field final synthetic d:Landroid/support/v7/internal/a/l;

.field private e:Ljava/lang/Object;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Ljava/lang/CharSequence;

.field private h:Ljava/lang/CharSequence;

.field private i:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/a/l;)V
    .registers 3

    .prologue
    .line 1103
    iput-object p1, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-direct {p0}, Landroid/support/v7/app/g;-><init>()V

    .line 1109
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/internal/a/q;->c:I

    return-void
.end method

.method private e(I)V
    .registers 2

    .prologue
    .line 1164
    iput p1, p0, Landroid/support/v7/internal/a/q;->c:I

    .line 1165
    return-void
.end method

.method private h()Landroid/support/v7/app/h;
    .registers 2

    .prologue
    .line 1124
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->b:Landroid/support/v7/app/h;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 1160
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    return v0
.end method

.method public final a(I)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 1183
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    .line 2341
    iget-object v1, v0, Landroid/support/v7/internal/a/l;->n:Landroid/support/v7/internal/widget/av;

    if-nez v1, :cond_e

    .line 2342
    iget-object v1, v0, Landroid/support/v7/internal/a/l;->i:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/internal/a/l;->n:Landroid/support/v7/internal/widget/av;

    .line 2344
    :cond_e
    iget-object v0, v0, Landroid/support/v7/internal/a/l;->n:Landroid/support/v7/internal/widget/av;

    .line 3167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 3174
    iput-object v0, p0, Landroid/support/v7/internal/a/q;->f:Landroid/graphics/drawable/Drawable;

    .line 3175
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    if-ltz v0, :cond_26

    .line 3176
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/a/q;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->b(I)V

    .line 1183
    :cond_26
    return-object p0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 1174
    iput-object p1, p0, Landroid/support/v7/internal/a/q;->f:Landroid/graphics/drawable/Drawable;

    .line 1175
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    if-ltz v0, :cond_11

    .line 1176
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/a/q;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->b(I)V

    .line 1178
    :cond_11
    return-object p0
.end method

.method public final a(Landroid/support/v7/app/h;)Landroid/support/v7/app/g;
    .registers 2

    .prologue
    .line 1129
    iput-object p1, p0, Landroid/support/v7/internal/a/q;->b:Landroid/support/v7/app/h;

    .line 1130
    return-object p0
.end method

.method public final a(Landroid/view/View;)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 1140
    iput-object p1, p0, Landroid/support/v7/internal/a/q;->i:Landroid/view/View;

    .line 1141
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    if-ltz v0, :cond_11

    .line 1142
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/a/q;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->b(I)V

    .line 1144
    :cond_11
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 1188
    iput-object p1, p0, Landroid/support/v7/internal/a/q;->g:Ljava/lang/CharSequence;

    .line 1189
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    if-ltz v0, :cond_11

    .line 1190
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/a/q;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->b(I)V

    .line 1192
    :cond_11
    return-object p0
.end method

.method public final a(Ljava/lang/Object;)Landroid/support/v7/app/g;
    .registers 2

    .prologue
    .line 1119
    iput-object p1, p0, Landroid/support/v7/internal/a/q;->e:Ljava/lang/Object;

    .line 1120
    return-object p0
.end method

.method public final b()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 1155
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->f:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final b(I)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 1197
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 3188
    iput-object v0, p0, Landroid/support/v7/internal/a/q;->g:Ljava/lang/CharSequence;

    .line 3189
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    if-ltz v0, :cond_1f

    .line 3190
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/a/q;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->b(I)V

    .line 1197
    :cond_1f
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 1212
    iput-object p1, p0, Landroid/support/v7/internal/a/q;->h:Ljava/lang/CharSequence;

    .line 1213
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    if-ltz v0, :cond_11

    .line 1214
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/a/q;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->b(I)V

    .line 1216
    :cond_11
    return-object p0
.end method

.method public final c(I)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 1149
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/a/l;->r()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2140
    iput-object v0, p0, Landroid/support/v7/internal/a/q;->i:Landroid/view/View;

    .line 2141
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    if-ltz v0, :cond_20

    .line 2142
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/a/q;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->b(I)V

    .line 1149
    :cond_20
    return-object p0
.end method

.method public final c()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1169
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d(I)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 1207
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 3212
    iput-object v0, p0, Landroid/support/v7/internal/a/q;->h:Ljava/lang/CharSequence;

    .line 3213
    iget v0, p0, Landroid/support/v7/internal/a/q;->c:I

    if-ltz v0, :cond_1f

    .line 3214
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/a/q;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/al;->b(I)V

    .line 1207
    :cond_1f
    return-object p0
.end method

.method public final d()Landroid/view/View;
    .registers 2

    .prologue
    .line 1135
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->i:Landroid/view/View;

    return-object v0
.end method

.method public final e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1114
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 1202
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->d:Landroid/support/v7/internal/a/l;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/app/g;)V

    .line 1203
    return-void
.end method

.method public final g()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1221
    iget-object v0, p0, Landroid/support/v7/internal/a/q;->h:Ljava/lang/CharSequence;

    return-object v0
.end method
