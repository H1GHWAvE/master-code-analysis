.class public final Landroid/support/v7/internal/widget/ActivityChooserView;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/widget/n;


# static fields
.field private static final b:Ljava/lang/String; = "ActivityChooserView"


# instance fields
.field a:Landroid/support/v4/view/n;

.field private final c:Landroid/support/v7/internal/widget/y;

.field private final d:Landroid/support/v7/internal/widget/z;

.field private final e:Landroid/support/v7/widget/aj;

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:Landroid/widget/FrameLayout;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/FrameLayout;

.field private final j:Landroid/widget/ImageView;

.field private final k:I

.field private final l:Landroid/database/DataSetObserver;

.field private final m:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private n:Landroid/support/v7/widget/an;

.field private o:Landroid/widget/PopupWindow$OnDismissListener;

.field private p:Z

.field private q:I

.field private r:Z

.field private s:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 192
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;-><init>(Landroid/content/Context;B)V

    .line 193
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 202
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;-><init>(Landroid/content/Context;C)V

    .line 203
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x4

    const/4 v4, 0x0

    .line 213
    invoke-direct {p0, p1, v1, v4}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 126
    new-instance v0, Landroid/support/v7/internal/widget/u;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/u;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->l:Landroid/database/DataSetObserver;

    .line 140
    new-instance v0, Landroid/support/v7/internal/widget/v;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/v;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->m:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 174
    iput v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:I

    .line 215
    sget-object v0, Landroid/support/v7/a/n;->ActivityChooserView:[I

    invoke-virtual {p1, v1, v0, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 218
    sget v1, Landroid/support/v7/a/n;->ActivityChooserView_initialActivityCount:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:I

    .line 222
    sget v1, Landroid/support/v7/a/n;->ActivityChooserView_expandActivityOverflowButtonDrawable:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 225
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 227
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 228
    sget v2, Landroid/support/v7/a/k;->abc_activity_chooser_view:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 230
    new-instance v0, Landroid/support/v7/internal/widget/z;

    invoke-direct {v0, p0, v4}, Landroid/support/v7/internal/widget/z;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;B)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/support/v7/internal/widget/z;

    .line 232
    sget v0, Landroid/support/v7/a/i;->activity_chooser_view_content:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/support/v7/widget/aj;

    .line 233
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/graphics/drawable/Drawable;

    .line 235
    sget v0, Landroid/support/v7/a/i;->default_activity_button:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    .line 236
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/support/v7/internal/widget/z;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/support/v7/internal/widget/z;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 238
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    sget v2, Landroid/support/v7/a/i;->image:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->j:Landroid/widget/ImageView;

    .line 240
    sget v0, Landroid/support/v7/a/i;->expand_activities_button:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 241
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/support/v7/internal/widget/z;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    new-instance v2, Landroid/support/v7/internal/widget/w;

    invoke-direct {v2, p0, v0}, Landroid/support/v7/internal/widget/w;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 260
    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->g:Landroid/widget/FrameLayout;

    .line 261
    sget v2, Landroid/support/v7/a/i;->image:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->h:Landroid/widget/ImageView;

    .line 263
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 265
    new-instance v0, Landroid/support/v7/internal/widget/y;

    invoke-direct {v0, p0, v4}, Landroid/support/v7/internal/widget/y;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;B)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 266
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    new-instance v1, Landroid/support/v7/internal/widget/x;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/x;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/y;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 274
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Landroid/support/v7/a/g;->abc_config_prefDialogWidth:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->k:I

    .line 277
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    return-object v0
.end method

.method private a(I)V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 348
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 5808
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    .line 348
    if-nez v0, :cond_10

    .line 349
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data model. Did you call #setDataModel?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_10
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->m:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 354
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_83

    move v0, v1

    .line 357
    :goto_22
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 6800
    iget-object v3, v3, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v3}, Landroid/support/v7/internal/widget/l;->a()I

    move-result v4

    .line 358
    if-eqz v0, :cond_85

    move v3, v1

    .line 359
    :goto_2d
    const v5, 0x7fffffff

    if-eq p1, v5, :cond_87

    add-int/2addr v3, p1

    if-le v4, v3, :cond_87

    .line 361
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/y;->a(Z)V

    .line 362
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    add-int/lit8 v4, p1, -0x1

    invoke-virtual {v3, v4}, Landroid/support/v7/internal/widget/y;->a(I)V

    .line 368
    :goto_41
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getListPopupWindow()Landroid/support/v7/widget/an;

    move-result-object v3

    .line 7760
    iget-object v4, v3, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    .line 369
    if-nez v4, :cond_82

    .line 370
    iget-boolean v4, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->p:Z

    if-nez v4, :cond_53

    if-nez v0, :cond_92

    .line 371
    :cond_53
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v2, v1, v0}, Landroid/support/v7/internal/widget/y;->a(ZZ)V

    .line 375
    :goto_58
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/y;->a()I

    move-result v0

    iget v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->k:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 376
    invoke-virtual {v3, v0}, Landroid/support/v7/widget/an;->a(I)V

    .line 377
    invoke-virtual {v3}, Landroid/support/v7/widget/an;->b()V

    .line 378
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v4/view/n;

    if-eqz v0, :cond_73

    .line 379
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v4/view/n;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/n;->a(Z)V

    .line 7845
    :cond_73
    iget-object v0, v3, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 381
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Landroid/support/v7/a/l;->abc_activitychooserview_choose_application:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 384
    :cond_82
    return-void

    :cond_83
    move v0, v2

    .line 354
    goto :goto_22

    :cond_85
    move v3, v2

    .line 358
    goto :goto_2d

    .line 364
    :cond_87
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/y;->a(Z)V

    .line 365
    iget-object v3, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v3, p1}, Landroid/support/v7/internal/widget/y;->a(I)V

    goto :goto_41

    .line 373
    :cond_92
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v0, v2, v2}, Landroid/support/v7/internal/widget/y;->a(ZZ)V

    goto :goto_58
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ActivityChooserView;I)V
    .registers 2

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/ActivityChooserView;->a(I)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ActivityChooserView;Z)Z
    .registers 2

    .prologue
    .line 68
    iput-boolean p1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->p:Z

    return p1
.end method

.method static synthetic b(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/widget/an;
    .registers 2

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getListPopupWindow()Landroid/support/v7/widget/an;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Landroid/support/v7/internal/widget/ActivityChooserView;)V
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 68
    .line 14521
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/y;->getCount()I

    move-result v0

    if-lez v0, :cond_6e

    .line 14522
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 14527
    :goto_f
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 14800
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->a()I

    move-result v0

    .line 14528
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 14804
    iget-object v1, v1, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/l;->c()I

    move-result v1

    .line 14529
    if-eq v0, v4, :cond_25

    if-le v0, v4, :cond_74

    if-lez v1, :cond_74

    .line 14530
    :cond_25
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 14531
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 15789
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 14532
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 14533
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 14534
    iget v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->s:I

    if-eqz v2, :cond_5e

    .line 14535
    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 14536
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->s:I

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 14538
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 14544
    :cond_5e
    :goto_5e
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7c

    .line 14545
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/support/v7/widget/aj;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_6d
    return-void

    .line 14524
    :cond_6e
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    goto :goto_f

    .line 14541
    :cond_74
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_5e

    .line 14547
    :cond_7c
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/support/v7/widget/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_6d
.end method

.method private d()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 521
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/y;->getCount()I

    move-result v0

    if-lez v0, :cond_6e

    .line 522
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 527
    :goto_f
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 12800
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->a()I

    move-result v0

    .line 528
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 12804
    iget-object v1, v1, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/l;->c()I

    move-result v1

    .line 529
    if-eq v0, v4, :cond_25

    if-le v0, v4, :cond_74

    if-lez v1, :cond_74

    .line 530
    :cond_25
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 531
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 13789
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 532
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 533
    iget-object v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 534
    iget v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->s:I

    if-eqz v2, :cond_5e

    .line 535
    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 536
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->s:I

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 538
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 544
    :cond_5e
    :goto_5e
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7c

    .line 545
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/support/v7/widget/aj;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 549
    :goto_6d
    return-void

    .line 524
    :cond_6e
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    goto :goto_f

    .line 541
    :cond_74
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_5e

    .line 547
    :cond_7c
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/support/v7/widget/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_6d
.end method

.method static synthetic d(Landroid/support/v7/internal/widget/ActivityChooserView;)Z
    .registers 2

    .prologue
    .line 68
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->p:Z

    return v0
.end method

.method static synthetic e(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic f(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->g:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic g(Landroid/support/v7/internal/widget/ActivityChooserView;)I
    .registers 2

    .prologue
    .line 68
    iget v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:I

    return v0
.end method

.method private getListPopupWindow()Landroid/support/v7/widget/an;
    .registers 3

    .prologue
    .line 505
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/widget/an;

    if-nez v0, :cond_2c

    .line 506
    new-instance v0, Landroid/support/v7/widget/an;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/an;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/widget/an;

    .line 507
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/widget/an;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/an;->a(Landroid/widget/ListAdapter;)V

    .line 508
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/widget/an;

    .line 12435
    iput-object p0, v0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 509
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/widget/an;->c()V

    .line 510
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/widget/an;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/support/v7/internal/widget/z;

    .line 12541
    iput-object v1, v0, Landroid/support/v7/widget/an;->m:Landroid/widget/AdapterView$OnItemClickListener;

    .line 511
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/widget/an;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->d:Landroid/support/v7/internal/widget/z;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/an;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 513
    :cond_2c
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->n:Landroid/support/v7/widget/an;

    return-object v0
.end method

.method static synthetic h(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/PopupWindow$OnDismissListener;
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->o:Landroid/widget/PopupWindow$OnDismissListener;

    return-object v0
.end method

.method static synthetic i(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/database/DataSetObserver;
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->l:Landroid/database/DataSetObserver;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 334
    .line 5408
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getListPopupWindow()Landroid/support/v7/widget/an;

    move-result-object v1

    .line 5760
    iget-object v1, v1, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    .line 334
    if-nez v1, :cond_11

    iget-boolean v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->r:Z

    if-nez v1, :cond_12

    .line 339
    :cond_11
    :goto_11
    return v0

    .line 337
    :cond_12
    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->p:Z

    .line 338
    iget v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:I

    invoke-direct {p0, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->a(I)V

    .line 339
    const/4 v0, 0x1

    goto :goto_11
.end method

.method public final b()Z
    .registers 3

    .prologue
    .line 392
    .line 8408
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getListPopupWindow()Landroid/support/v7/widget/an;

    move-result-object v0

    .line 8760
    iget-object v0, v0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 392
    if-eqz v0, :cond_22

    .line 393
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getListPopupWindow()Landroid/support/v7/widget/an;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/an;->d()V

    .line 394
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 396
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->m:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 399
    :cond_22
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 408
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getListPopupWindow()Landroid/support/v7/widget/an;

    move-result-object v0

    .line 9760
    iget-object v0, v0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 408
    return v0
.end method

.method public final getDataModel()Landroid/support/v7/internal/widget/l;
    .registers 2

    .prologue
    .line 461
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 11808
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    .line 461
    return-object v0
.end method

.method protected final onAttachedToWindow()V
    .registers 3

    .prologue
    .line 413
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 414
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 9808
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    .line 415
    if-eqz v0, :cond_e

    .line 416
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->l:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/l;->registerObserver(Ljava/lang/Object;)V

    .line 418
    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->r:Z

    .line 419
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .registers 3

    .prologue
    .line 423
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 424
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 10808
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    .line 425
    if-eqz v0, :cond_e

    .line 426
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->l:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/l;->unregisterObserver(Ljava/lang/Object;)V

    .line 428
    :cond_e
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 429
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 430
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->m:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 432
    :cond_1d
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->c()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 433
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->b()Z

    .line 435
    :cond_26
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->r:Z

    .line 436
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .registers 10

    .prologue
    const/4 v3, 0x0

    .line 454
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/support/v7/widget/aj;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/support/v7/widget/aj;->layout(IIII)V

    .line 455
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->c()Z

    move-result v0

    if-nez v0, :cond_13

    .line 456
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->b()Z

    .line 458
    :cond_13
    return-void
.end method

.method protected final onMeasure(II)V
    .registers 6

    .prologue
    .line 440
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->e:Landroid/support/v7/widget/aj;

    .line 444
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_14

    .line 445
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 448
    :cond_14
    invoke-virtual {p0, v0, p1, p2}, Landroid/support/v7/internal/widget/ActivityChooserView;->measureChild(Landroid/view/View;II)V

    .line 449
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->setMeasuredDimension(II)V

    .line 450
    return-void
.end method

.method public final setActivityChooserModel(Landroid/support/v7/internal/widget/l;)V
    .registers 5

    .prologue
    .line 283
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 1662
    iget-object v1, v0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    .line 2068
    iget-object v1, v1, Landroid/support/v7/internal/widget/ActivityChooserView;->c:Landroid/support/v7/internal/widget/y;

    .line 2808
    iget-object v1, v1, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    .line 1663
    if-eqz v1, :cond_19

    iget-object v2, v0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActivityChooserView;->isShown()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1664
    iget-object v2, v0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    .line 3068
    iget-object v2, v2, Landroid/support/v7/internal/widget/ActivityChooserView;->l:Landroid/database/DataSetObserver;

    .line 1664
    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/l;->unregisterObserver(Ljava/lang/Object;)V

    .line 1666
    :cond_19
    iput-object p1, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    .line 1667
    if-eqz p1, :cond_2c

    iget-object v1, v0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActivityChooserView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 1668
    iget-object v1, v0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    .line 4068
    iget-object v1, v1, Landroid/support/v7/internal/widget/ActivityChooserView;->l:Landroid/database/DataSetObserver;

    .line 1668
    invoke-virtual {p1, v1}, Landroid/support/v7/internal/widget/l;->registerObserver(Ljava/lang/Object;)V

    .line 1670
    :cond_2c
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/y;->notifyDataSetChanged()V

    .line 4408
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getListPopupWindow()Landroid/support/v7/widget/an;

    move-result-object v0

    .line 4760
    iget-object v0, v0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 284
    if-eqz v0, :cond_41

    .line 285
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->b()Z

    .line 286
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->a()Z

    .line 288
    :cond_41
    return-void
.end method

.method public final setDefaultActionButtonContentDescription(I)V
    .registers 2

    .prologue
    .line 496
    iput p1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->s:I

    .line 497
    return-void
.end method

.method public final setExpandActivityOverflowButtonContentDescription(I)V
    .registers 4

    .prologue
    .line 316
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 317
    iget-object v1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 318
    return-void
.end method

.method public final setExpandActivityOverflowButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 302
    iget-object v0, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 303
    return-void
.end method

.method public final setInitialActivityCount(I)V
    .registers 2

    .prologue
    .line 482
    iput p1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->q:I

    .line 483
    return-void
.end method

.method public final setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .registers 2

    .prologue
    .line 470
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->o:Landroid/widget/PopupWindow$OnDismissListener;

    .line 471
    return-void
.end method

.method public final setProvider(Landroid/support/v4/view/n;)V
    .registers 2

    .prologue
    .line 325
    iput-object p1, p0, Landroid/support/v7/internal/widget/ActivityChooserView;->a:Landroid/support/v4/view/n;

    .line 326
    return-void
.end method
