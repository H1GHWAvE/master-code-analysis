.class public final Landroid/support/v7/internal/view/menu/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/g/a/a;)Landroid/view/Menu;
    .registers 4

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_c

    .line 37
    new-instance v0, Landroid/support/v7/internal/view/menu/ac;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/internal/view/menu/ac;-><init>(Landroid/content/Context;Landroid/support/v4/g/a/a;)V

    return-object v0

    .line 39
    :cond_c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/support/v4/g/a/b;)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_c

    .line 44
    new-instance v0, Landroid/support/v7/internal/view/menu/t;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/internal/view/menu/t;-><init>(Landroid/content/Context;Landroid/support/v4/g/a/b;)V

    .line 46
    :goto_b
    return-object v0

    .line 45
    :cond_c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_18

    .line 46
    new-instance v0, Landroid/support/v7/internal/view/menu/o;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/internal/view/menu/o;-><init>(Landroid/content/Context;Landroid/support/v4/g/a/b;)V

    goto :goto_b

    .line 48
    :cond_18
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private static a(Landroid/content/Context;Landroid/support/v4/g/a/c;)Landroid/view/SubMenu;
    .registers 4

    .prologue
    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_c

    .line 53
    new-instance v0, Landroid/support/v7/internal/view/menu/ae;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/internal/view/menu/ae;-><init>(Landroid/content/Context;Landroid/support/v4/g/a/c;)V

    return-object v0

    .line 55
    :cond_c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
