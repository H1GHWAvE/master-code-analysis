.class final Landroid/support/v7/internal/widget/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/widget/p;


# static fields
.field private static final b:F = 0.95f


# instance fields
.field final synthetic a:Landroid/support/v7/internal/widget/l;

.field private final c:Ljava/util/Map;


# direct methods
.method private constructor <init>(Landroid/support/v7/internal/widget/l;)V
    .registers 3

    .prologue
    .line 917
    iput-object p1, p0, Landroid/support/v7/internal/widget/q;->a:Landroid/support/v7/internal/widget/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 920
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/q;->c:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/internal/widget/l;B)V
    .registers 3

    .prologue
    .line 917
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/q;-><init>(Landroid/support/v7/internal/widget/l;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/List;)V
    .registers 10

    .prologue
    .line 925
    iget-object v4, p0, Landroid/support/v7/internal/widget/q;->c:Ljava/util/Map;

    .line 927
    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 929
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 930
    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v2, :cond_2e

    .line 931
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/o;

    .line 932
    const/4 v3, 0x0

    iput v3, v0, Landroid/support/v7/internal/widget/o;->b:F

    .line 933
    new-instance v3, Landroid/content/ComponentName;

    iget-object v5, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v6, v0, Landroid/support/v7/internal/widget/o;->a:Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 930
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 939
    :cond_2e
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 940
    const/high16 v2, 0x3f800000    # 1.0f

    move v3, v0

    .line 941
    :goto_37
    if-ltz v3, :cond_5b

    .line 942
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/support/v7/internal/widget/r;

    .line 943
    iget-object v0, v1, Landroid/support/v7/internal/widget/r;->a:Landroid/content/ComponentName;

    .line 944
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/o;

    .line 945
    if-eqz v0, :cond_5f

    .line 946
    iget v5, v0, Landroid/support/v7/internal/widget/o;->b:F

    iget v1, v1, Landroid/support/v7/internal/widget/r;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v5

    iput v1, v0, Landroid/support/v7/internal/widget/o;->b:F

    .line 947
    const v0, 0x3f733333    # 0.95f

    mul-float/2addr v0, v2

    .line 941
    :goto_56
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v2, v0

    goto :goto_37

    .line 951
    :cond_5b
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 958
    return-void

    :cond_5f
    move v0, v2

    goto :goto_56
.end method
