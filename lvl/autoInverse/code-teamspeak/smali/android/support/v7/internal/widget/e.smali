.class final Landroid/support/v7/internal/widget/e;
.super Landroid/support/v7/internal/widget/d;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/widget/ActionBarContainer;)V
    .registers 2

    .prologue
    .line 10
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/d;-><init>(Landroid/support/v7/internal/widget/ActionBarContainer;)V

    .line 11
    return-void
.end method


# virtual methods
.method public final getOutline(Landroid/graphics/Outline;)V
    .registers 3
    .param p1    # Landroid/graphics/Outline;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 15
    iget-object v0, p0, Landroid/support/v7/internal/widget/e;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-boolean v0, v0, Landroid/support/v7/internal/widget/ActionBarContainer;->d:Z

    if-eqz v0, :cond_14

    .line 16
    iget-object v0, p0, Landroid/support/v7/internal/widget/e;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_13

    .line 17
    iget-object v0, p0, Landroid/support/v7/internal/widget/e;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getOutline(Landroid/graphics/Outline;)V

    .line 25
    :cond_13
    :goto_13
    return-void

    .line 21
    :cond_14
    iget-object v0, p0, Landroid/support/v7/internal/widget/e;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarContainer;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_13

    .line 22
    iget-object v0, p0, Landroid/support/v7/internal/widget/e;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarContainer;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getOutline(Landroid/graphics/Outline;)V

    goto :goto_13
.end method
