.class abstract Landroid/support/v7/internal/view/menu/e;
.super Landroid/support/v7/internal/view/menu/f;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field b:Ljava/util/Map;

.field c:Ljava/util/Map;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 37
    invoke-direct {p0, p2}, Landroid/support/v7/internal/view/menu/f;-><init>(Ljava/lang/Object;)V

    .line 38
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/e;->a:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 95
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    if-nez v0, :cond_5

    .line 108
    :cond_4
    return-void

    .line 99
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 102
    :cond_f
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 103
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 104
    invoke-interface {v0}, Landroid/view/MenuItem;->getGroupId()I

    move-result v0

    if-ne p1, v0, :cond_f

    .line 105
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_f
.end method

.method private b()V
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    if-eqz v0, :cond_9

    .line 87
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 89
    :cond_9
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->c:Ljava/util/Map;

    if-eqz v0, :cond_12

    .line 90
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 92
    :cond_12
    return-void
.end method

.method private b(I)V
    .registers 4

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    if-nez v0, :cond_5

    .line 125
    :cond_4
    :goto_4
    return-void

    .line 115
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 118
    :cond_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 119
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 120
    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-ne p1, v0, :cond_f

    .line 121
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4
.end method


# virtual methods
.method final a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .registers 5

    .prologue
    .line 42
    instance-of v0, p1, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_28

    move-object v0, p1

    .line 43
    check-cast v0, Landroid/support/v4/g/a/b;

    .line 46
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    if-nez v1, :cond_12

    .line 47
    new-instance v1, Landroid/support/v4/n/a;

    invoke-direct {v1}, Landroid/support/v4/n/a;-><init>()V

    iput-object v1, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    .line 51
    :cond_12
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    .line 53
    if-nez v1, :cond_27

    .line 55
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/e;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/support/v7/internal/view/menu/ab;->a(Landroid/content/Context;Landroid/support/v4/g/a/b;)Landroid/view/MenuItem;

    move-result-object v1

    .line 56
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/e;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :cond_27
    :goto_27
    return-object v1

    :cond_28
    move-object v1, p1

    goto :goto_27
.end method

.method final a(Landroid/view/SubMenu;)Landroid/view/SubMenu;
    .registers 5

    .prologue
    .line 65
    instance-of v0, p1, Landroid/support/v4/g/a/c;

    if-eqz v0, :cond_34

    .line 66
    check-cast p1, Landroid/support/v4/g/a/c;

    .line 69
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->c:Ljava/util/Map;

    if-nez v0, :cond_11

    .line 70
    new-instance v0, Landroid/support/v4/n/a;

    invoke-direct {v0}, Landroid/support/v4/n/a;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/e;->c:Ljava/util/Map;

    .line 73
    :cond_11
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/e;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    .line 75
    if-nez v0, :cond_2d

    .line 76
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/e;->a:Landroid/content/Context;

    .line 1052
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_2e

    .line 1053
    new-instance v0, Landroid/support/v7/internal/view/menu/ae;

    invoke-direct {v0, v1, p1}, Landroid/support/v7/internal/view/menu/ae;-><init>(Landroid/content/Context;Landroid/support/v4/g/a/c;)V

    .line 77
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/e;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    :cond_2d
    :goto_2d
    return-object v0

    .line 1055
    :cond_2e
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :cond_34
    move-object v0, p1

    .line 81
    goto :goto_2d
.end method
