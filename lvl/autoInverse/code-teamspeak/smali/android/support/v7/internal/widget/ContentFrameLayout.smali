.class public Landroid/support/v7/internal/widget/ContentFrameLayout;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field public final a:Landroid/graphics/Rect;

.field private b:Landroid/util/TypedValue;

.field private c:Landroid/util/TypedValue;

.field private d:Landroid/util/TypedValue;

.field private e:Landroid/util/TypedValue;

.field private f:Landroid/util/TypedValue;

.field private g:Landroid/util/TypedValue;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    .line 56
    return-void
.end method

.method private a(IIII)V
    .registers 6

    .prologue
    .line 72
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 73
    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 74
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->requestLayout()V

    .line 76
    :cond_e
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;)V
    .registers 2

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/ContentFrameLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    .line 63
    return-void
.end method

.method public getFixedHeightMajor()Landroid/util/TypedValue;
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->f:Landroid/util/TypedValue;

    if-nez v0, :cond_b

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->f:Landroid/util/TypedValue;

    .line 177
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->f:Landroid/util/TypedValue;

    return-object v0
.end method

.method public getFixedHeightMinor()Landroid/util/TypedValue;
    .registers 2

    .prologue
    .line 181
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->g:Landroid/util/TypedValue;

    if-nez v0, :cond_b

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->g:Landroid/util/TypedValue;

    .line 182
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->g:Landroid/util/TypedValue;

    return-object v0
.end method

.method public getFixedWidthMajor()Landroid/util/TypedValue;
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->d:Landroid/util/TypedValue;

    if-nez v0, :cond_b

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->d:Landroid/util/TypedValue;

    .line 167
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->d:Landroid/util/TypedValue;

    return-object v0
.end method

.method public getFixedWidthMinor()Landroid/util/TypedValue;
    .registers 2

    .prologue
    .line 171
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->e:Landroid/util/TypedValue;

    if-nez v0, :cond_b

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->e:Landroid/util/TypedValue;

    .line 172
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->e:Landroid/util/TypedValue;

    return-object v0
.end method

.method public getMinWidthMajor()Landroid/util/TypedValue;
    .registers 2

    .prologue
    .line 156
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->b:Landroid/util/TypedValue;

    if-nez v0, :cond_b

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->b:Landroid/util/TypedValue;

    .line 157
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->b:Landroid/util/TypedValue;

    return-object v0
.end method

.method public getMinWidthMinor()Landroid/util/TypedValue;
    .registers 2

    .prologue
    .line 161
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->c:Landroid/util/TypedValue;

    if-nez v0, :cond_b

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->c:Landroid/util/TypedValue;

    .line 162
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->c:Landroid/util/TypedValue;

    return-object v0
.end method

.method protected onMeasure(II)V
    .registers 15

    .prologue
    const/4 v11, 0x5

    const/4 v1, 0x1

    const/high16 v10, -0x80000000

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 80
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 81
    iget v0, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v3, :cond_b8

    move v0, v1

    .line 83
    :goto_1a
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 84
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 87
    if-ne v6, v10, :cond_100

    .line 88
    if-eqz v0, :cond_bb

    iget-object v3, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->e:Landroid/util/TypedValue;

    .line 89
    :goto_28
    if-eqz v3, :cond_100

    iget v4, v3, Landroid/util/TypedValue;->type:I

    if-eqz v4, :cond_100

    .line 91
    iget v4, v3, Landroid/util/TypedValue;->type:I

    if-ne v4, v11, :cond_bf

    .line 92
    invoke-virtual {v3, v5}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 96
    :goto_37
    if-lez v3, :cond_100

    .line 97
    iget-object v4, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v8

    sub-int/2addr v3, v4

    .line 98
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 99
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    move v4, v1

    .line 106
    :goto_50
    if-ne v7, v10, :cond_7d

    .line 107
    if-eqz v0, :cond_d1

    iget-object v3, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->f:Landroid/util/TypedValue;

    .line 108
    :goto_56
    if-eqz v3, :cond_7d

    iget v7, v3, Landroid/util/TypedValue;->type:I

    if-eqz v7, :cond_7d

    .line 110
    iget v7, v3, Landroid/util/TypedValue;->type:I

    if-ne v7, v11, :cond_d4

    .line 111
    invoke-virtual {v3, v5}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 115
    :goto_65
    if-lez v3, :cond_7d

    .line 116
    iget-object v7, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v8

    sub-int/2addr v3, v7

    .line 117
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 118
    invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 124
    :cond_7d
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 126
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getMeasuredWidth()I

    move-result v7

    .line 129
    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 131
    if-nez v4, :cond_f9

    if-ne v6, v10, :cond_f9

    .line 132
    if-eqz v0, :cond_e5

    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->c:Landroid/util/TypedValue;

    .line 133
    :goto_90
    if-eqz v0, :cond_f9

    iget v4, v0, Landroid/util/TypedValue;->type:I

    if-eqz v4, :cond_f9

    .line 135
    iget v4, v0, Landroid/util/TypedValue;->type:I

    if-ne v4, v11, :cond_e8

    .line 136
    invoke-virtual {v0, v5}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 140
    :goto_9f
    if-lez v0, :cond_ab

    .line 141
    iget-object v4, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    sub-int/2addr v0, v4

    .line 143
    :cond_ab
    if-ge v7, v0, :cond_f9

    .line 144
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    move v2, v1

    .line 150
    :goto_b2
    if-eqz v2, :cond_b7

    .line 151
    invoke-super {p0, v0, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 153
    :cond_b7
    return-void

    :cond_b8
    move v0, v2

    .line 81
    goto/16 :goto_1a

    .line 88
    :cond_bb
    iget-object v3, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->d:Landroid/util/TypedValue;

    goto/16 :goto_28

    .line 93
    :cond_bf
    iget v4, v3, Landroid/util/TypedValue;->type:I

    const/4 v8, 0x6

    if-ne v4, v8, :cond_103

    .line 94
    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    iget v8, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v8, v8

    invoke-virtual {v3, v4, v8}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v3

    float-to-int v3, v3

    goto/16 :goto_37

    .line 107
    :cond_d1
    iget-object v3, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->g:Landroid/util/TypedValue;

    goto :goto_56

    .line 112
    :cond_d4
    iget v7, v3, Landroid/util/TypedValue;->type:I

    const/4 v8, 0x6

    if-ne v7, v8, :cond_fd

    .line 113
    iget v7, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v7, v7

    iget v8, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v8, v8

    invoke-virtual {v3, v7, v8}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v3

    float-to-int v3, v3

    goto :goto_65

    .line 132
    :cond_e5
    iget-object v0, p0, Landroid/support/v7/internal/widget/ContentFrameLayout;->b:Landroid/util/TypedValue;

    goto :goto_90

    .line 137
    :cond_e8
    iget v4, v0, Landroid/util/TypedValue;->type:I

    const/4 v6, 0x6

    if-ne v4, v6, :cond_fb

    .line 138
    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    goto :goto_9f

    :cond_f9
    move v0, v3

    goto :goto_b2

    :cond_fb
    move v0, v2

    goto :goto_9f

    :cond_fd
    move v3, v2

    goto/16 :goto_65

    :cond_100
    move v4, v2

    goto/16 :goto_50

    :cond_103
    move v3, v2

    goto/16 :goto_37
.end method
