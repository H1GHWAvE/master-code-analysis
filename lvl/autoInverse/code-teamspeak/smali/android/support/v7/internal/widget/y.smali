.class final Landroid/support/v7/internal/widget/y;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# static fields
.field public static final a:I = 0x7fffffff

.field public static final b:I = 0x4

.field private static final f:I = 0x0

.field private static final g:I = 0x1

.field private static final h:I = 0x3


# instance fields
.field c:Landroid/support/v7/internal/widget/l;

.field d:Z

.field final synthetic e:Landroid/support/v7/internal/widget/ActivityChooserView;

.field private i:I

.field private j:Z

.field private k:Z


# direct methods
.method private constructor <init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
    .registers 3

    .prologue
    .line 639
    iput-object p1, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 653
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/v7/internal/widget/y;->i:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/internal/widget/ActivityChooserView;B)V
    .registers 3

    .prologue
    .line 639
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/y;-><init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V

    return-void
.end method

.method private a(Landroid/support/v7/internal/widget/l;)V
    .registers 4

    .prologue
    .line 662
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-static {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;

    move-result-object v0

    .line 1808
    iget-object v0, v0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    .line 663
    if-eqz v0, :cond_1b

    iget-object v1, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActivityChooserView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 664
    iget-object v1, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-static {v1}, Landroid/support/v7/internal/widget/ActivityChooserView;->i(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/database/DataSetObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/l;->unregisterObserver(Ljava/lang/Object;)V

    .line 666
    :cond_1b
    iput-object p1, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    .line 667
    if-eqz p1, :cond_30

    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 668
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-static {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->i(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/database/DataSetObserver;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/widget/l;->registerObserver(Ljava/lang/Object;)V

    .line 670
    :cond_30
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/y;->notifyDataSetChanged()V

    .line 671
    return-void
.end method

.method private b()Landroid/content/pm/ResolveInfo;
    .registers 2

    .prologue
    .line 789
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    return-object v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 800
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->a()I

    move-result v0

    return v0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 804
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->c()I

    move-result v0

    return v0
.end method

.method private e()Landroid/support/v7/internal/widget/l;
    .registers 2

    .prologue
    .line 808
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    return-object v0
.end method

.method private f()Z
    .registers 2

    .prologue
    .line 822
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/y;->d:Z

    return v0
.end method


# virtual methods
.method public final a()I
    .registers 10

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 760
    iget v4, p0, Landroid/support/v7/internal/widget/y;->i:I

    .line 761
    const v1, 0x7fffffff

    iput v1, p0, Landroid/support/v7/internal/widget/y;->i:I

    .line 766
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 767
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 768
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/y;->getCount()I

    move-result v7

    move-object v1, v2

    move v3, v0

    .line 770
    :goto_17
    if-ge v0, v7, :cond_2b

    .line 771
    invoke-virtual {p0, v0, v1, v2}, Landroid/support/v7/internal/widget/y;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 772
    invoke-virtual {v1, v5, v6}, Landroid/view/View;->measure(II)V

    .line 773
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 770
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .line 776
    :cond_2b
    iput v4, p0, Landroid/support/v7/internal/widget/y;->i:I

    .line 778
    return v3
.end method

.method public final a(I)V
    .registers 3

    .prologue
    .line 782
    iget v0, p0, Landroid/support/v7/internal/widget/y;->i:I

    if-eq v0, p1, :cond_9

    .line 783
    iput p1, p0, Landroid/support/v7/internal/widget/y;->i:I

    .line 784
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/y;->notifyDataSetChanged()V

    .line 786
    :cond_9
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 793
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/y;->k:Z

    if-eq v0, p1, :cond_9

    .line 794
    iput-boolean p1, p0, Landroid/support/v7/internal/widget/y;->k:Z

    .line 795
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/y;->notifyDataSetChanged()V

    .line 797
    :cond_9
    return-void
.end method

.method public final a(ZZ)V
    .registers 4

    .prologue
    .line 813
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/y;->d:Z

    if-ne v0, p1, :cond_8

    iget-boolean v0, p0, Landroid/support/v7/internal/widget/y;->j:Z

    if-eq v0, p2, :cond_f

    .line 815
    :cond_8
    iput-boolean p1, p0, Landroid/support/v7/internal/widget/y;->d:Z

    .line 816
    iput-boolean p2, p0, Landroid/support/v7/internal/widget/y;->j:Z

    .line 817
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/y;->notifyDataSetChanged()V

    .line 819
    :cond_f
    return-void
.end method

.method public final getCount()I
    .registers 3

    .prologue
    .line 689
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->a()I

    move-result v0

    .line 690
    iget-boolean v1, p0, Landroid/support/v7/internal/widget/y;->d:Z

    if-nez v1, :cond_14

    iget-object v1, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/l;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 691
    add-int/lit8 v0, v0, -0x1

    .line 693
    :cond_14
    iget v1, p0, Landroid/support/v7/internal/widget/y;->i:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 694
    iget-boolean v1, p0, Landroid/support/v7/internal/widget/y;->k:Z

    if-eqz v1, :cond_20

    .line 695
    add-int/lit8 v0, v0, 0x1

    .line 697
    :cond_20
    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 701
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/y;->getItemViewType(I)I

    move-result v0

    .line 702
    packed-switch v0, :pswitch_data_24

    .line 711
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 704
    :pswitch_d
    const/4 v0, 0x0

    .line 709
    :goto_e
    return-object v0

    .line 706
    :pswitch_f
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/y;->d:Z

    if-nez v0, :cond_1d

    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->b()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 707
    add-int/lit8 p1, p1, 0x1

    .line 709
    :cond_1d
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->c:Landroid/support/v7/internal/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/l;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    goto :goto_e

    .line 702
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_f
        :pswitch_d
    .end packed-switch
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 716
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .registers 3

    .prologue
    .line 675
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/y;->k:Z

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/y;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_e

    .line 676
    const/4 v0, 0x1

    .line 678
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 720
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/y;->getItemViewType(I)I

    move-result v0

    .line 721
    packed-switch v0, :pswitch_data_9c

    .line 753
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 723
    :pswitch_f
    if-eqz p2, :cond_17

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, v5, :cond_41

    .line 724
    :cond_17
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/v7/a/k;->abc_activity_chooser_view_list_item:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 726
    invoke-virtual {p2, v5}, Landroid/view/View;->setId(I)V

    .line 727
    sget v0, Landroid/support/v7/a/i;->title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 728
    iget-object v1, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Landroid/support/v7/a/l;->abc_activity_chooser_view_see_all:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 751
    :cond_41
    :goto_41
    return-object p2

    .line 733
    :pswitch_42
    if-eqz p2, :cond_4c

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Landroid/support/v7/a/i;->list_item:I

    if-eq v0, v1, :cond_5c

    .line 734
    :cond_4c
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/v7/a/k;->abc_activity_chooser_view_list_item:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 737
    :cond_5c
    iget-object v0, p0, Landroid/support/v7/internal/widget/y;->e:Landroid/support/v7/internal/widget/ActivityChooserView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 739
    sget v0, Landroid/support/v7/a/i;->icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 740
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/y;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 741
    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 743
    sget v0, Landroid/support/v7/a/i;->title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 744
    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 746
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/y;->d:Z

    if-eqz v0, :cond_98

    if-nez p1, :cond_98

    iget-boolean v0, p0, Landroid/support/v7/internal/widget/y;->j:Z

    if-eqz v0, :cond_98

    .line 747
    invoke-static {p2, v5}, Landroid/support/v4/view/cx;->b(Landroid/view/View;Z)V

    goto :goto_41

    .line 749
    :cond_98
    invoke-static {p2, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;Z)V

    goto :goto_41

    .line 721
    :pswitch_data_9c
    .packed-switch 0x0
        :pswitch_42
        :pswitch_f
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 684
    const/4 v0, 0x3

    return v0
.end method
