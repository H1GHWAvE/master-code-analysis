.class public final Landroid/support/v7/internal/a/e;
.super Landroid/support/v7/app/a;
.source "SourceFile"


# instance fields
.field i:Landroid/support/v7/internal/widget/ad;

.field j:Z

.field public k:Landroid/view/Window$Callback;

.field l:Landroid/support/v7/internal/view/menu/g;

.field private m:Z

.field private n:Z

.field private o:Ljava/util/ArrayList;

.field private final p:Ljava/lang/Runnable;

.field private final q:Landroid/support/v7/widget/cl;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Ljava/lang/CharSequence;Landroid/view/Window$Callback;)V
    .registers 6

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/support/v7/app/a;-><init>()V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/a/e;->o:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Landroid/support/v7/internal/a/f;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/f;-><init>(Landroid/support/v7/internal/a/e;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/e;->p:Ljava/lang/Runnable;

    .line 69
    new-instance v0, Landroid/support/v7/internal/a/g;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/a/g;-><init>(Landroid/support/v7/internal/a/e;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/e;->q:Landroid/support/v7/widget/cl;

    .line 78
    new-instance v0, Landroid/support/v7/internal/widget/ay;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/support/v7/internal/widget/ay;-><init>(Landroid/support/v7/widget/Toolbar;Z)V

    iput-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    .line 79
    new-instance v0, Landroid/support/v7/internal/a/k;

    invoke-direct {v0, p0, p3}, Landroid/support/v7/internal/a/k;-><init>(Landroid/support/v7/internal/a/e;Landroid/view/Window$Callback;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    .line 80
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    iget-object v1, p0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    invoke-interface {v0, v1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/view/Window$Callback;)V

    .line 81
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->q:Landroid/support/v7/widget/cl;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/Toolbar;->setOnMenuItemClickListener(Landroid/support/v7/widget/cl;)V

    .line 82
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p2}, Landroid/support/v7/internal/widget/ad;->a(Ljava/lang/CharSequence;)V

    .line 83
    return-void
.end method

.method private B()Landroid/view/Window$Callback;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    return-object v0
.end method

.method private C()V
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 441
    invoke-virtual {p0}, Landroid/support/v7/internal/a/e;->A()Landroid/view/Menu;

    move-result-object v1

    .line 442
    instance-of v2, v1, Landroid/support/v7/internal/view/menu/i;

    if-eqz v2, :cond_31

    move-object v0, v1

    check-cast v0, Landroid/support/v7/internal/view/menu/i;

    move-object v2, v0

    .line 443
    :goto_d
    if-eqz v2, :cond_12

    .line 444
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 447
    :cond_12
    :try_start_12
    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 448
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    const/4 v3, 0x0

    invoke-interface {v0, v3, v1}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_28

    iget-object v0, p0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4, v1}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 450
    :cond_28
    invoke-interface {v1}, Landroid/view/Menu;->clear()V
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_33

    .line 453
    :cond_2b
    if-eqz v2, :cond_30

    .line 454
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/i;->e()V

    .line 457
    :cond_30
    return-void

    :cond_31
    move-object v2, v0

    .line 442
    goto :goto_d

    .line 453
    :catchall_33
    move-exception v0

    if-eqz v2, :cond_39

    .line 454
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/i;->e()V

    :cond_39
    throw v0
.end method

.method private static synthetic a(Landroid/support/v7/internal/a/e;Landroid/view/Menu;)Landroid/view/View;
    .registers 10

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 51
    .line 3516
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    if-nez v0, :cond_67

    instance-of v0, p1, Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_67

    move-object v0, p1

    .line 3517
    check-cast v0, Landroid/support/v7/internal/view/menu/i;

    .line 3519
    iget-object v2, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v2}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v2

    .line 3520
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 3521
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 3522
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 3525
    sget v5, Landroid/support/v7/a/d;->actionBarPopupTheme:I

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 3526
    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_36

    .line 3527
    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 3531
    :cond_36
    sget v5, Landroid/support/v7/a/d;->panelMenuListTheme:I

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 3532
    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_6f

    .line 3533
    iget v3, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v3, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 3538
    :goto_44
    new-instance v3, Landroid/view/ContextThemeWrapper;

    invoke-direct {v3, v2, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 3539
    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 3542
    new-instance v2, Landroid/support/v7/internal/view/menu/g;

    sget v4, Landroid/support/v7/a/k;->abc_list_menu_item_layout:I

    invoke-direct {v2, v3, v4}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    .line 3543
    iget-object v2, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    new-instance v3, Landroid/support/v7/internal/a/j;

    invoke-direct {v3, p0, v7}, Landroid/support/v7/internal/a/j;-><init>(Landroid/support/v7/internal/a/e;B)V

    .line 4134
    iput-object v3, v2, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 3544
    iget-object v2, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 3505
    :cond_67
    if-eqz p1, :cond_6d

    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    if-nez v0, :cond_75

    :cond_6d
    move-object v0, v1

    .line 3510
    :goto_6e
    return-object v0

    .line 3535
    :cond_6f
    sget v3, Landroid/support/v7/a/m;->Theme_AppCompat_CompactMenu:I

    invoke-virtual {v4, v3, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_44

    .line 3509
    :cond_75
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/g;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_90

    .line 3510
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    iget-object v1, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/g;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_6e

    :cond_90
    move-object v0, v1

    .line 51
    goto :goto_6e
.end method

.method private a(Landroid/view/Menu;)Landroid/view/View;
    .registers 10

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 503
    .line 1516
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    if-nez v0, :cond_67

    instance-of v0, p1, Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_67

    move-object v0, p1

    .line 1517
    check-cast v0, Landroid/support/v7/internal/view/menu/i;

    .line 1519
    iget-object v2, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v2}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v2

    .line 1520
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 1521
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 1522
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 1525
    sget v5, Landroid/support/v7/a/d;->actionBarPopupTheme:I

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1526
    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_36

    .line 1527
    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 1531
    :cond_36
    sget v5, Landroid/support/v7/a/d;->panelMenuListTheme:I

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1532
    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_6f

    .line 1533
    iget v3, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v3, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 1538
    :goto_44
    new-instance v3, Landroid/view/ContextThemeWrapper;

    invoke-direct {v3, v2, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1539
    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 1542
    new-instance v2, Landroid/support/v7/internal/view/menu/g;

    sget v4, Landroid/support/v7/a/k;->abc_list_menu_item_layout:I

    invoke-direct {v2, v3, v4}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    .line 1543
    iget-object v2, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    new-instance v3, Landroid/support/v7/internal/a/j;

    invoke-direct {v3, p0, v7}, Landroid/support/v7/internal/a/j;-><init>(Landroid/support/v7/internal/a/e;B)V

    .line 2134
    iput-object v3, v2, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 1544
    iget-object v2, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 505
    :cond_67
    if-eqz p1, :cond_6d

    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    if-nez v0, :cond_75

    :cond_6d
    move-object v0, v1

    .line 512
    :goto_6e
    return-object v0

    .line 1535
    :cond_6f
    sget v3, Landroid/support/v7/a/m;->Theme_AppCompat_CompactMenu:I

    invoke-virtual {v4, v3, v6}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_44

    .line 509
    :cond_75
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/g;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_90

    .line 510
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    iget-object v1, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/g;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_6e

    :cond_90
    move-object v0, v1

    .line 512
    goto :goto_6e
.end method

.method private static synthetic a(Landroid/support/v7/internal/a/e;)Landroid/view/Window$Callback;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    return-object v0
.end method

.method private b(Landroid/view/Menu;)V
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 516
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    if-nez v0, :cond_65

    instance-of v0, p1, Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_65

    .line 517
    check-cast p1, Landroid/support/v7/internal/view/menu/i;

    .line 519
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v0

    .line 520
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 521
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 522
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 525
    sget v3, Landroid/support/v7/a/d;->actionBarPopupTheme:I

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 526
    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v3, :cond_34

    .line 527
    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 531
    :cond_34
    sget v3, Landroid/support/v7/a/d;->panelMenuListTheme:I

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 532
    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v3, :cond_66

    .line 533
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 538
    :goto_42
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-direct {v1, v0, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 539
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 542
    new-instance v0, Landroid/support/v7/internal/view/menu/g;

    sget v2, Landroid/support/v7/a/k;->abc_list_menu_item_layout:I

    invoke-direct {v0, v1, v2}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    .line 543
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    new-instance v1, Landroid/support/v7/internal/a/j;

    invoke-direct {v1, p0, v5}, Landroid/support/v7/internal/a/j;-><init>(Landroid/support/v7/internal/a/e;B)V

    .line 3134
    iput-object v1, v0, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 544
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->l:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 546
    :cond_65
    return-void

    .line 535
    :cond_66
    sget v1, Landroid/support/v7/a/m;->Theme_AppCompat_CompactMenu:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_42
.end method

.method private static synthetic b(Landroid/support/v7/internal/a/e;)Z
    .registers 2

    .prologue
    .line 51
    iget-boolean v0, p0, Landroid/support/v7/internal/a/e;->j:Z

    return v0
.end method

.method private static synthetic c(Landroid/support/v7/internal/a/e;)Landroid/support/v7/internal/widget/ad;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    return-object v0
.end method

.method private static synthetic d(Landroid/support/v7/internal/a/e;)Z
    .registers 2

    .prologue
    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/a/e;->j:Z

    return v0
.end method


# virtual methods
.method final A()Landroid/view/Menu;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 578
    iget-boolean v0, p0, Landroid/support/v7/internal/a/e;->m:Z

    if-nez v0, :cond_17

    .line 579
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    new-instance v1, Landroid/support/v7/internal/a/h;

    invoke-direct {v1, p0, v3}, Landroid/support/v7/internal/a/h;-><init>(Landroid/support/v7/internal/a/e;B)V

    new-instance v2, Landroid/support/v7/internal/a/i;

    invoke-direct {v2, p0, v3}, Landroid/support/v7/internal/a/i;-><init>(Landroid/support/v7/internal/a/e;B)V

    invoke-interface {v0, v1, v2}, Landroid/support/v7/internal/widget/ad;->a(Landroid/support/v7/internal/view/menu/y;Landroid/support/v7/internal/view/menu/j;)V

    .line 581
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/a/e;->m:Z

    .line 583
    :cond_17
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->B()Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method

.method public final a()I
    .registers 2

    .prologue
    .line 217
    const/4 v0, -0x1

    return v0
.end method

.method public final a(F)V
    .registers 3

    .prologue
    .line 145
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 146
    return-void
.end method

.method public final a(I)V
    .registers 5

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 105
    iget-object v1, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/e;->a(Landroid/view/View;)V

    .line 106
    return-void
.end method

.method public final a(II)V
    .registers 7

    .prologue
    .line 257
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->r()I

    move-result v0

    .line 258
    iget-object v1, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, Landroid/support/v7/internal/widget/ad;->c(I)V

    .line 259
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .registers 2

    .prologue
    .line 195
    invoke-super {p0, p1}, Landroid/support/v7/app/a;->a(Landroid/content/res/Configuration;)V

    .line 196
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 115
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/graphics/drawable/Drawable;)V

    .line 116
    return-void
.end method

.method public final a(Landroid/support/v7/app/e;)V
    .registers 3

    .prologue
    .line 483
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    return-void
.end method

.method public final a(Landroid/support/v7/app/g;)V
    .registers 4

    .prologue
    .line 332
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/support/v7/app/g;I)V
    .registers 5

    .prologue
    .line 344
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/support/v7/app/g;IZ)V
    .registers 6

    .prologue
    .line 350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/support/v7/app/g;Z)V
    .registers 5

    .prologue
    .line 338
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 91
    new-instance v0, Landroid/support/v7/app/c;

    const/4 v1, -0x2

    invoke-direct {v0, v1}, Landroid/support/v7/app/c;-><init>(I)V

    .line 1096
    if-eqz p1, :cond_b

    .line 1097
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1099
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/view/View;)V

    .line 92
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/support/v7/app/c;)V
    .registers 4

    .prologue
    .line 96
    if-eqz p1, :cond_5

    .line 97
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    :cond_5
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/view/View;)V

    .line 100
    return-void
.end method

.method public final a(Landroid/widget/SpinnerAdapter;Landroid/support/v7/app/f;)V
    .registers 5

    .prologue
    .line 200
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    new-instance v1, Landroid/support/v7/internal/a/b;

    invoke-direct {v1, p2}, Landroid/support/v7/internal/a/b;-><init>(Landroid/support/v7/app/f;)V

    invoke-interface {v0, p1, v1}, Landroid/support/v7/internal/widget/ad;->a(Landroid/widget/SpinnerAdapter;Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 201
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 227
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->b(Ljava/lang/CharSequence;)V

    .line 228
    return-void
.end method

.method public final a(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 263
    if-eqz p1, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/e;->a(II)V

    .line 264
    return-void

    .line 263
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .registers 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 469
    invoke-virtual {p0}, Landroid/support/v7/internal/a/e;->A()Landroid/view/Menu;

    move-result-object v3

    .line 470
    if-eqz v3, :cond_1f

    .line 471
    if-eqz p2, :cond_20

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    :goto_e
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v0

    if-eq v0, v1, :cond_22

    move v0, v1

    :goto_19
    invoke-interface {v3, v0}, Landroid/view/Menu;->setQwertyMode(Z)V

    .line 474
    invoke-interface {v3, p1, p2, v2}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    .line 479
    :cond_1f
    return v1

    .line 471
    :cond_20
    const/4 v0, -0x1

    goto :goto_e

    :cond_22
    move v0, v2

    .line 473
    goto :goto_19
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 461
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_c

    .line 1421
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->n()Z

    .line 464
    :cond_c
    return v1
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method public final b(I)V
    .registers 3

    .prologue
    .line 110
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(I)V

    .line 111
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 125
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->b(Landroid/graphics/drawable/Drawable;)V

    .line 126
    return-void
.end method

.method public final b(Landroid/support/v7/app/e;)V
    .registers 3

    .prologue
    .line 487
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 488
    return-void
.end method

.method public final b(Landroid/support/v7/app/g;)V
    .registers 4

    .prologue
    .line 356
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 242
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->c(Ljava/lang/CharSequence;)V

    .line 243
    return-void
.end method

.method public final b(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x2

    .line 268
    if-eqz p1, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/e;->a(II)V

    .line 269
    return-void

    .line 268
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 136
    return-void
.end method

.method public final c(I)V
    .registers 3

    .prologue
    .line 120
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->b(I)V

    .line 121
    return-void
.end method

.method public final c(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 288
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->e(Landroid/graphics/drawable/Drawable;)V

    .line 289
    return-void
.end method

.method public final c(Landroid/support/v7/app/g;)V
    .registers 4

    .prologue
    .line 374
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 175
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->d(Ljava/lang/CharSequence;)V

    .line 176
    return-void
.end method

.method public final c(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x4

    .line 273
    if-eqz p1, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/e;->a(II)V

    .line 274
    return-void

    .line 273
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final d()Landroid/view/View;
    .registers 2

    .prologue
    .line 293
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->y()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)V
    .registers 4

    .prologue
    .line 205
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->v()I

    move-result v0

    packed-switch v0, :pswitch_data_18

    .line 210
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :pswitch_11
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->e(I)V

    .line 208
    return-void

    .line 205
    nop

    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_11
    .end packed-switch
.end method

.method public final d(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 131
    return-void
.end method

.method public final d(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 237
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->a(Ljava/lang/CharSequence;)V

    .line 238
    return-void
.end method

.method public final d(Z)V
    .registers 4

    .prologue
    const/16 v1, 0x8

    .line 278
    if-eqz p1, :cond_9

    move v0, v1

    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/e;->a(II)V

    .line 279
    return-void

    .line 278
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final e()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 298
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->e()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)V
    .registers 4

    .prologue
    .line 232
    iget-object v1, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    if-eqz p1, :cond_12

    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_e
    invoke-interface {v1, v0}, Landroid/support/v7/internal/widget/ad;->b(Ljava/lang/CharSequence;)V

    .line 233
    return-void

    .line 232
    :cond_12
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final e(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 165
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->c(Landroid/graphics/drawable/Drawable;)V

    .line 166
    return-void
.end method

.method public final e(Z)V
    .registers 4

    .prologue
    const/16 v1, 0x10

    .line 283
    if-eqz p1, :cond_9

    move v0, v1

    :goto_5
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/internal/a/e;->a(II)V

    .line 284
    return-void

    .line 283
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final f()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 303
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->f()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)V
    .registers 4

    .prologue
    .line 247
    iget-object v1, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    if-eqz p1, :cond_12

    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_e
    invoke-interface {v1, v0}, Landroid/support/v7/internal/widget/ad;->c(Ljava/lang/CharSequence;)V

    .line 248
    return-void

    .line 247
    :cond_12
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final f(Z)V
    .registers 2

    .prologue
    .line 181
    return-void
.end method

.method public final g()I
    .registers 2

    .prologue
    .line 308
    const/4 v0, 0x0

    return v0
.end method

.method public final g(I)V
    .registers 3

    .prologue
    .line 252
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/a/e;->a(II)V

    .line 253
    return-void
.end method

.method public final g(Z)V
    .registers 2

    .prologue
    .line 191
    return-void
.end method

.method public final h()I
    .registers 2

    .prologue
    .line 321
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->r()I

    move-result v0

    return v0
.end method

.method public final h(I)V
    .registers 4

    .prologue
    .line 313
    const/4 v0, 0x2

    if-ne p1, v0, :cond_b

    .line 314
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tabs not supported in this configuration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->d(I)V

    .line 317
    return-void
.end method

.method public final h(Z)V
    .registers 5

    .prologue
    .line 491
    iget-boolean v0, p0, Landroid/support/v7/internal/a/e;->n:Z

    if-ne p1, v0, :cond_5

    .line 500
    :cond_4
    return-void

    .line 494
    :cond_5
    iput-boolean p1, p0, Landroid/support/v7/internal/a/e;->n:Z

    .line 496
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 497
    const/4 v0, 0x0

    :goto_e
    if-ge v0, v1, :cond_4

    .line 498
    iget-object v2, p0, Landroid/support/v7/internal/a/e;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 497
    add-int/lit8 v0, v0, 0x1

    goto :goto_e
.end method

.method public final i()Landroid/support/v7/app/g;
    .registers 3

    .prologue
    .line 326
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final i(I)V
    .registers 4

    .prologue
    .line 362
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final j(I)Landroid/support/v7/app/g;
    .registers 4

    .prologue
    .line 386
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final j()V
    .registers 3

    .prologue
    .line 368
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final k()Landroid/support/v7/app/g;
    .registers 3

    .prologue
    .line 380
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tabs are not supported in toolbar action bars"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final k(I)V
    .registers 3

    .prologue
    .line 170
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->g(I)V

    .line 171
    return-void
.end method

.method public final l()I
    .registers 2

    .prologue
    .line 392
    const/4 v0, 0x0

    return v0
.end method

.method public final l(I)V
    .registers 3

    .prologue
    .line 185
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ad;->h(I)V

    .line 186
    return-void
.end method

.method public final m()I
    .registers 2

    .prologue
    .line 397
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->z()I

    move-result v0

    return v0
.end method

.method public final n()V
    .registers 3

    .prologue
    .line 404
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v7/internal/widget/ad;->j(I)V

    .line 405
    return-void
.end method

.method public final o()V
    .registers 3

    .prologue
    .line 411
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Landroid/support/v7/internal/widget/ad;->j(I)V

    .line 412
    return-void
.end method

.method public final p()Z
    .registers 2

    .prologue
    .line 416
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->A()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final q()V
    .registers 1

    .prologue
    .line 141
    return-void
.end method

.method public final r()Landroid/content/Context;
    .registers 2

    .prologue
    .line 155
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->b()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final s()Z
    .registers 2

    .prologue
    .line 160
    invoke-super {p0}, Landroid/support/v7/app/a;->s()Z

    move-result v0

    return v0
.end method

.method public final w()F
    .registers 2

    .prologue
    .line 150
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/cx;->r(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public final x()Z
    .registers 2

    .prologue
    .line 421
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->n()Z

    move-result v0

    return v0
.end method

.method public final y()Z
    .registers 3

    .prologue
    .line 426
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/a/e;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 427
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/a/e;->p:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 428
    const/4 v0, 0x1

    return v0
.end method

.method public final z()Z
    .registers 2

    .prologue
    .line 433
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 434
    iget-object v0, p0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->d()V

    .line 435
    const/4 v0, 0x1

    .line 437
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method
