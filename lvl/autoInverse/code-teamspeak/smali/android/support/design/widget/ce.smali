.class public Landroid/support/design/widget/ce;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field private static final a:I = 0xc8


# instance fields
.field private b:Landroid/widget/EditText;

.field private c:Ljava/lang/CharSequence;

.field private d:Landroid/graphics/Paint;

.field private e:Z

.field private f:Landroid/widget/TextView;

.field private g:I

.field private h:Landroid/content/res/ColorStateList;

.field private i:Landroid/content/res/ColorStateList;

.field private final j:Landroid/support/design/widget/l;

.field private k:Z

.field private l:Landroid/support/design/widget/ck;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/ce;-><init>(Landroid/content/Context;B)V

    .line 79
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/ce;-><init>(Landroid/content/Context;C)V

    .line 83
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    invoke-direct {p0, p1, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    new-instance v0, Landroid/support/design/widget/l;

    invoke-direct {v0, p0}, Landroid/support/design/widget/l;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    .line 89
    invoke-virtual {p0, v3}, Landroid/support/design/widget/ce;->setOrientation(I)V

    .line 90
    invoke-virtual {p0, v2}, Landroid/support/design/widget/ce;->setWillNotDraw(Z)V

    .line 91
    invoke-virtual {p0, v3}, Landroid/support/design/widget/ce;->setAddStatesFromChildren(Z)V

    .line 93
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->a(Landroid/view/animation/Interpolator;)V

    .line 94
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 1113
    iput-object v1, v0, Landroid/support/design/widget/l;->i:Landroid/view/animation/Interpolator;

    .line 1114
    invoke-virtual {v0}, Landroid/support/design/widget/l;->a()V

    .line 95
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    const v1, 0x800033

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->d(I)V

    .line 97
    sget-object v0, Landroid/support/design/n;->TextInputLayout:[I

    sget v1, Landroid/support/design/m;->Widget_Design_TextInputLayout:I

    invoke-virtual {p1, v5, v0, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 99
    sget v1, Landroid/support/design/n;->TextInputLayout_android_hint:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/ce;->c:Ljava/lang/CharSequence;

    .line 100
    sget v1, Landroid/support/design/n;->TextInputLayout_hintAnimationEnabled:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/design/widget/ce;->k:Z

    .line 103
    sget v1, Landroid/support/design/n;->TextInputLayout_android_textColorHint:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_5c

    .line 104
    sget v1, Landroid/support/design/n;->TextInputLayout_android_textColorHint:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/ce;->i:Landroid/content/res/ColorStateList;

    iput-object v1, p0, Landroid/support/design/widget/ce;->h:Landroid/content/res/ColorStateList;

    .line 108
    :cond_5c
    sget v1, Landroid/support/design/n;->TextInputLayout_hintTextAppearance:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 110
    if-eq v1, v4, :cond_6d

    .line 111
    sget v1, Landroid/support/design/n;->TextInputLayout_hintTextAppearance:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/ce;->setHintTextAppearance(I)V

    .line 115
    :cond_6d
    sget v1, Landroid/support/design/n;->TextInputLayout_errorTextAppearance:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/ce;->g:I

    .line 116
    sget v1, Landroid/support/design/n;->TextInputLayout_errorEnabled:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 117
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 119
    invoke-virtual {p0, v1}, Landroid/support/design/widget/ce;->setErrorEnabled(Z)V

    .line 121
    invoke-static {p0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_8a

    .line 124
    invoke-static {p0, v3}, Landroid/support/v4/view/cx;->c(Landroid/view/View;I)V

    .line 128
    :cond_8a
    new-instance v0, Landroid/support/design/widget/cj;

    invoke-direct {v0, p0, v2}, Landroid/support/design/widget/cj;-><init>(Landroid/support/design/widget/ce;B)V

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 129
    return-void
.end method

.method private a(I)I
    .registers 5

    .prologue
    .line 503
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 504
    invoke-virtual {p0}, Landroid/support/design/widget/ce;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 505
    iget v0, v0, Landroid/util/TypedValue;->data:I

    .line 507
    :goto_16
    return v0

    :cond_17
    const v0, -0xff01

    goto :goto_16
.end method

.method private a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 4

    .prologue
    .line 202
    instance-of v0, p1, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_32

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    .line 204
    :goto_6
    iget-object v0, p0, Landroid/support/design/widget/ce;->d:Landroid/graphics/Paint;

    if-nez v0, :cond_11

    .line 205
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/ce;->d:Landroid/graphics/Paint;

    .line 207
    :cond_11
    iget-object v0, p0, Landroid/support/design/widget/ce;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    .line 1229
    iget-object v1, v1, Landroid/support/design/widget/l;->h:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    .line 207
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 208
    iget-object v0, p0, Landroid/support/design/widget/ce;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    .line 1253
    iget v1, v1, Landroid/support/design/widget/l;->e:F

    .line 208
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 209
    iget-object v0, p0, Landroid/support/design/widget/ce;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v0

    neg-float v0, v0

    float-to-int v0, v0

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 211
    return-object p1

    .line 202
    :cond_32
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    move-object p1, v0

    goto :goto_6
.end method

.method private a(F)V
    .registers 4

    .prologue
    .line 484
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    .line 6249
    iget v0, v0, Landroid/support/design/widget/l;->a:F

    .line 484
    cmpl-float v0, v0, p1

    if-nez v0, :cond_9

    .line 500
    :goto_8
    return-void

    .line 487
    :cond_9
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    if-nez v0, :cond_2b

    .line 488
    invoke-static {}, Landroid/support/design/widget/dh;->a()Landroid/support/design/widget/ck;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 489
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    sget-object v1, Landroid/support/design/widget/a;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/ck;->a(Landroid/view/animation/Interpolator;)V

    .line 490
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/support/design/widget/ck;->a(I)V

    .line 491
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    new-instance v1, Landroid/support/design/widget/ci;

    invoke-direct {v1, p0}, Landroid/support/design/widget/ci;-><init>(Landroid/support/design/widget/ce;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/ck;->a(Landroid/support/design/widget/cp;)V

    .line 498
    :cond_2b
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    iget-object v1, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    .line 7249
    iget v1, v1, Landroid/support/design/widget/l;->a:F

    .line 498
    invoke-virtual {v0, v1, p1}, Landroid/support/design/widget/ck;->a(FF)V

    .line 499
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 8116
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->a()V

    goto :goto_8
.end method

.method static synthetic a(Landroid/support/design/widget/ce;)V
    .registers 2

    .prologue
    .line 56
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/design/widget/ce;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 12

    .prologue
    const/4 v1, 0x1

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 215
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_68

    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_68

    move v0, v1

    .line 216
    :goto_16
    invoke-virtual {p0}, Landroid/support/design/widget/ce;->getDrawableState()[I

    move-result-object v4

    .line 1549
    array-length v5, v4

    move v3, v2

    :goto_1c
    if-ge v3, v5, :cond_6d

    aget v6, v4, v3

    .line 1550
    const v7, 0x101009c

    if-ne v6, v7, :cond_6a

    .line 218
    :goto_25
    iget-object v2, p0, Landroid/support/design/widget/ce;->h:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_45

    iget-object v2, p0, Landroid/support/design/widget/ce;->i:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_45

    .line 219
    iget-object v2, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    iget-object v3, p0, Landroid/support/design/widget/ce;->h:Landroid/content/res/ColorStateList;

    invoke-virtual {v3}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/design/widget/l;->b(I)V

    .line 220
    iget-object v3, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    if-eqz v1, :cond_6f

    iget-object v2, p0, Landroid/support/design/widget/ce;->i:Landroid/content/res/ColorStateList;

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    :goto_42
    invoke-virtual {v3, v2}, Landroid/support/design/widget/l;->a(I)V

    .line 225
    :cond_45
    if-nez v0, :cond_49

    if-eqz v1, :cond_7c

    .line 2462
    :cond_49
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    if-eqz v0, :cond_5e

    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 3120
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->b()Z

    move-result v0

    .line 2462
    if-eqz v0, :cond_5e

    .line 2463
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 3184
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->e()V

    .line 2465
    :cond_5e
    if-eqz p1, :cond_76

    iget-boolean v0, p0, Landroid/support/design/widget/ce;->k:Z

    if-eqz v0, :cond_76

    .line 2466
    invoke-direct {p0, v9}, Landroid/support/design/widget/ce;->a(F)V

    .line 3477
    :goto_67
    return-void

    :cond_68
    move v0, v2

    .line 215
    goto :goto_16

    .line 1549
    :cond_6a
    add-int/lit8 v3, v3, 0x1

    goto :goto_1c

    :cond_6d
    move v1, v2

    .line 1554
    goto :goto_25

    .line 220
    :cond_6f
    iget-object v2, p0, Landroid/support/design/widget/ce;->h:Landroid/content/res/ColorStateList;

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    goto :goto_42

    .line 2468
    :cond_76
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0, v9}, Landroid/support/design/widget/l;->a(F)V

    goto :goto_67

    .line 3473
    :cond_7c
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    if-eqz v0, :cond_91

    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 4120
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->b()Z

    move-result v0

    .line 3473
    if-eqz v0, :cond_91

    .line 3474
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 4184
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->e()V

    .line 3476
    :cond_91
    if-eqz p1, :cond_9b

    iget-boolean v0, p0, Landroid/support/design/widget/ce;->k:Z

    if-eqz v0, :cond_9b

    .line 3477
    invoke-direct {p0, v8}, Landroid/support/design/widget/ce;->a(F)V

    goto :goto_67

    .line 3479
    :cond_9b
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0, v8}, Landroid/support/design/widget/l;->a(F)V

    goto :goto_67
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 323
    iget-boolean v0, p0, Landroid/support/design/widget/ce;->e:Z

    return v0
.end method

.method private static a([I)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 549
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_d

    aget v3, p0, v1

    .line 550
    const v4, 0x101009c

    if-ne v3, v4, :cond_e

    .line 551
    const/4 v0, 0x1

    .line 554
    :cond_d
    return v0

    .line 549
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method static synthetic b(Landroid/support/design/widget/ce;)Landroid/support/design/widget/l;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    return-object v0
.end method

.method private b(Z)V
    .registers 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 462
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 5120
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->b()Z

    move-result v0

    .line 462
    if-eqz v0, :cond_17

    .line 463
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 5184
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->e()V

    .line 465
    :cond_17
    if-eqz p1, :cond_21

    iget-boolean v0, p0, Landroid/support/design/widget/ce;->k:Z

    if-eqz v0, :cond_21

    .line 466
    invoke-direct {p0, v1}, Landroid/support/design/widget/ce;->a(F)V

    .line 470
    :goto_20
    return-void

    .line 468
    :cond_21
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->a(F)V

    goto :goto_20
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 412
    iget-boolean v0, p0, Landroid/support/design/widget/ce;->k:Z

    return v0
.end method

.method static synthetic c(Landroid/support/design/widget/ce;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method private c(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 473
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 6120
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->b()Z

    move-result v0

    .line 473
    if-eqz v0, :cond_16

    .line 474
    iget-object v0, p0, Landroid/support/design/widget/ce;->l:Landroid/support/design/widget/ck;

    .line 6184
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->e()V

    .line 476
    :cond_16
    if-eqz p1, :cond_20

    iget-boolean v0, p0, Landroid/support/design/widget/ce;->k:Z

    if-eqz v0, :cond_20

    .line 477
    invoke-direct {p0, v1}, Landroid/support/design/widget/ce;->a(F)V

    .line 481
    :goto_1f
    return-void

    .line 479
    :cond_20
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->a(F)V

    goto :goto_1f
.end method

.method static synthetic d(Landroid/support/design/widget/ce;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method private setEditText(Landroid/widget/EditText;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 153
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_d

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "We already have an EditText, can only have one"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_d
    iput-object p1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    .line 159
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->a(Landroid/graphics/Typeface;)V

    .line 160
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTextSize()F

    move-result v1

    .line 1118
    iget v2, v0, Landroid/support/design/widget/l;->d:F

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_2d

    .line 1119
    iput v1, v0, Landroid/support/design/widget/l;->d:F

    .line 1120
    invoke-virtual {v0}, Landroid/support/design/widget/l;->a()V

    .line 161
    :cond_2d
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getGravity()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/l;->c(I)V

    .line 164
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    new-instance v1, Landroid/support/design/widget/cf;

    invoke-direct {v1, p0}, Landroid/support/design/widget/cf;-><init>(Landroid/support/design/widget/ce;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 178
    iget-object v0, p0, Landroid/support/design/widget/ce;->h:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_4e

    .line 179
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/ce;->h:Landroid/content/res/ColorStateList;

    .line 183
    :cond_4e
    iget-object v0, p0, Landroid/support/design/widget/ce;->c:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 184
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/ce;->setHint(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 189
    :cond_65
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_80

    .line 191
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-static {v1}, Landroid/support/v4/view/cx;->k(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-static {v2}, Landroid/support/v4/view/cx;->l(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v3

    invoke-static {v0, v1, v4, v2, v3}, Landroid/support/v4/view/cx;->b(Landroid/view/View;IIII)V

    .line 196
    :cond_80
    invoke-direct {p0, v4}, Landroid/support/design/widget/ce;->a(Z)V

    .line 197
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 6

    .prologue
    .line 133
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_13

    move-object v0, p1

    .line 134
    check-cast v0, Landroid/widget/EditText;

    invoke-direct {p0, v0}, Landroid/support/design/widget/ce;->setEditText(Landroid/widget/EditText;)V

    .line 135
    const/4 v0, 0x0

    invoke-direct {p0, p3}, Landroid/support/design/widget/ce;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-super {p0, p1, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 140
    :goto_12
    return-void

    .line 138
    :cond_13
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_12
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 3

    .prologue
    .line 429
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 430
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->a(Landroid/graphics/Canvas;)V

    .line 431
    return-void
.end method

.method public getEditText()Landroid/widget/EditText;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method public getError()Ljava/lang/CharSequence;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 397
    iget-boolean v0, p0, Landroid/support/design/widget/ce;->e:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_17

    .line 398
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 400
    :goto_16
    return-object v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public getHint()Ljava/lang/CharSequence;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Landroid/support/design/widget/ce;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .registers 12

    .prologue
    .line 435
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 437
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_55

    .line 438
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLeft()I

    move-result v0

    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getCompoundPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 439
    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getRight()I

    move-result v1

    iget-object v2, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getCompoundPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 441
    iget-object v2, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    iget-object v3, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getTop()I

    move-result v3

    iget-object v4, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getCompoundPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getBottom()I

    move-result v4

    iget-object v5, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getCompoundPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/support/design/widget/l;->a(IIII)V

    .line 447
    iget-object v2, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {p0}, Landroid/support/design/widget/ce;->getPaddingTop()I

    move-result v3

    sub-int v4, p5, p3

    invoke-virtual {p0}, Landroid/support/design/widget/ce;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/support/design/widget/l;->b(IIII)V

    .line 450
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0}, Landroid/support/design/widget/l;->a()V

    .line 452
    :cond_55
    return-void
.end method

.method public refreshDrawableState()V
    .registers 2

    .prologue
    .line 456
    invoke-super {p0}, Landroid/widget/LinearLayout;->refreshDrawableState()V

    .line 458
    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/design/widget/ce;->a(Z)V

    .line 459
    return-void
.end method

.method public setError(Ljava/lang/CharSequence;)V
    .registers 6
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    const-wide/16 v2, 0xc8

    const/4 v1, 0x0

    .line 338
    iget-boolean v0, p0, Landroid/support/design/widget/ce;->e:Z

    if-nez v0, :cond_12

    .line 339
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 386
    :goto_d
    return-void

    .line 344
    :cond_e
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/design/widget/ce;->setErrorEnabled(Z)V

    .line 347
    :cond_12
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_59

    .line 348
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 349
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 350
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    move-result-object v0

    new-instance v1, Landroid/support/design/widget/cg;

    invoke-direct {v1, p0}, Landroid/support/design/widget/cg;-><init>(Landroid/support/design/widget/ce;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    .line 363
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    iget-object v1, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 385
    :cond_53
    :goto_53
    const/16 v0, 0x800

    invoke-virtual {p0, v0}, Landroid/support/design/widget/ce;->sendAccessibilityEvent(I)V

    goto :goto_d

    .line 366
    :cond_59
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_53

    .line 367
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    move-result-object v0

    new-instance v1, Landroid/support/design/widget/ch;

    invoke-direct {v1, p0}, Landroid/support/design/widget/ch;-><init>(Landroid/support/design/widget/ce;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    .line 379
    invoke-virtual {p0}, Landroid/support/design/widget/ce;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v0

    .line 380
    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    sget v2, Landroid/support/design/h;->abc_edit_text_material:I

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/av;->a(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    goto :goto_53
.end method

.method public setErrorEnabled(Z)V
    .registers 7

    .prologue
    .line 291
    iget-boolean v0, p0, Landroid/support/design/widget/ce;->e:Z

    if-eq v0, p1, :cond_52

    .line 292
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_11

    .line 293
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->a()V

    .line 296
    :cond_11
    if-eqz p1, :cond_53

    .line 297
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/design/widget/ce;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    .line 298
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/design/widget/ce;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Landroid/support/design/widget/ce;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 299
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/ce;->addView(Landroid/view/View;)V

    .line 302
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_50

    .line 304
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-static {v1}, Landroid/support/v4/view/cx;->k(Landroid/view/View;)I

    move-result v1

    const/4 v2, 0x0

    iget-object v3, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-static {v3}, Landroid/support/v4/view/cx;->l(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;IIII)V

    .line 311
    :cond_50
    :goto_50
    iput-boolean p1, p0, Landroid/support/design/widget/ce;->e:Z

    .line 313
    :cond_52
    return-void

    .line 308
    :cond_53
    iget-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/ce;->removeView(Landroid/view/View;)V

    .line 309
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/ce;->f:Landroid/widget/TextView;

    goto :goto_50
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 248
    iput-object p1, p0, Landroid/support/design/widget/ce;->c:Ljava/lang/CharSequence;

    .line 249
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->a(Ljava/lang/CharSequence;)V

    .line 251
    const/16 v0, 0x800

    invoke-virtual {p0, v0}, Landroid/support/design/widget/ce;->sendAccessibilityEvent(I)V

    .line 252
    return-void
.end method

.method public setHintAnimationEnabled(Z)V
    .registers 2

    .prologue
    .line 424
    iput-boolean p1, p0, Landroid/support/design/widget/ce;->k:Z

    .line 425
    return-void
.end method

.method public setHintTextAppearance(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/ai;
        .end annotation
    .end param

    .prologue
    .line 270
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->e(I)V

    .line 271
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    .line 4549
    iget v0, v0, Landroid/support/design/widget/l;->f:I

    .line 271
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/ce;->i:Landroid/content/res/ColorStateList;

    .line 273
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_2b

    .line 274
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/design/widget/ce;->a(Z)V

    .line 277
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/design/widget/ce;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    .line 278
    iget-object v1, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279
    iget-object v0, p0, Landroid/support/design/widget/ce;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestLayout()V

    .line 281
    :cond_2b
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .param p1    # Landroid/graphics/Typeface;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 148
    iget-object v0, p0, Landroid/support/design/widget/ce;->j:Landroid/support/design/widget/l;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/l;->a(Landroid/graphics/Typeface;)V

    .line 149
    return-void
.end method
