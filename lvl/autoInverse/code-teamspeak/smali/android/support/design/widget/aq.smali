.class final Landroid/support/design/widget/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Landroid/support/design/widget/NavigationView$SavedState;
    .registers 2

    .prologue
    .line 436
    new-instance v0, Landroid/support/design/widget/NavigationView$SavedState;

    invoke-direct {v0, p0}, Landroid/support/design/widget/NavigationView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method private static a(I)[Landroid/support/design/widget/NavigationView$SavedState;
    .registers 2

    .prologue
    .line 441
    new-array v0, p0, [Landroid/support/design/widget/NavigationView$SavedState;

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 2436
    new-instance v0, Landroid/support/design/widget/NavigationView$SavedState;

    invoke-direct {v0, p1}, Landroid/support/design/widget/NavigationView$SavedState;-><init>(Landroid/os/Parcel;)V

    .line 433
    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 433
    .line 1441
    new-array v0, p1, [Landroid/support/design/widget/NavigationView$SavedState;

    .line 433
    return-object v0
.end method
