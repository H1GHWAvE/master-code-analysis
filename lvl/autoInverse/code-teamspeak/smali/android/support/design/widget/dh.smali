.class final Landroid/support/design/widget/dh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/design/widget/cq;

.field private static final b:Landroid/support/design/widget/dj;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 24
    new-instance v0, Landroid/support/design/widget/di;

    invoke-direct {v0}, Landroid/support/design/widget/di;-><init>()V

    sput-object v0, Landroid/support/design/widget/dh;->a:Landroid/support/design/widget/cq;

    .line 55
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 56
    const/16 v1, 0x15

    if-lt v0, v1, :cond_16

    .line 57
    new-instance v0, Landroid/support/design/widget/dl;

    invoke-direct {v0, v2}, Landroid/support/design/widget/dl;-><init>(B)V

    sput-object v0, Landroid/support/design/widget/dh;->b:Landroid/support/design/widget/dj;

    .line 61
    :goto_15
    return-void

    .line 59
    :cond_16
    new-instance v0, Landroid/support/design/widget/dk;

    invoke-direct {v0, v2}, Landroid/support/design/widget/dk;-><init>(B)V

    sput-object v0, Landroid/support/design/widget/dh;->b:Landroid/support/design/widget/dj;

    goto :goto_15
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method static a()Landroid/support/design/widget/ck;
    .registers 1

    .prologue
    .line 68
    sget-object v0, Landroid/support/design/widget/dh;->a:Landroid/support/design/widget/cq;

    invoke-interface {v0}, Landroid/support/design/widget/cq;->a()Landroid/support/design/widget/ck;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 64
    sget-object v0, Landroid/support/design/widget/dh;->b:Landroid/support/design/widget/dj;

    invoke-interface {v0, p0}, Landroid/support/design/widget/dj;->a(Landroid/view/View;)V

    .line 65
    return-void
.end method
