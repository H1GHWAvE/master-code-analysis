.class final Landroid/support/design/widget/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/design/widget/i;


# instance fields
.field final synthetic a:Landroid/support/design/widget/m;


# direct methods
.method private constructor <init>(Landroid/support/design/widget/m;)V
    .registers 2

    .prologue
    .line 853
    iput-object p1, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/design/widget/m;B)V
    .registers 3

    .prologue
    .line 853
    invoke-direct {p0, p1}, Landroid/support/design/widget/q;-><init>(Landroid/support/design/widget/m;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/design/widget/AppBarLayout;I)V
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 856
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0, p2}, Landroid/support/design/widget/m;->b(Landroid/support/design/widget/m;I)I

    .line 858
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0}, Landroid/support/design/widget/m;->a(Landroid/support/design/widget/m;)Landroid/support/v4/view/gh;

    move-result-object v0

    if-eqz v0, :cond_3d

    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0}, Landroid/support/design/widget/m;->a(Landroid/support/design/widget/m;)Landroid/support/v4/view/gh;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/gh;->b()I

    move-result v0

    move v1, v0

    .line 859
    :goto_19
    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v3

    .line 861
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-virtual {v0}, Landroid/support/design/widget/m;->getChildCount()I

    move-result v4

    :goto_23
    if-ge v2, v4, :cond_5f

    .line 862
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/m;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 863
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/p;

    .line 864
    invoke-static {v5}, Landroid/support/design/widget/m;->a(Landroid/view/View;)Landroid/support/design/widget/dg;

    move-result-object v6

    .line 866
    iget v7, v0, Landroid/support/design/widget/p;->d:I

    packed-switch v7, :pswitch_data_cc

    .line 861
    :cond_3a
    :goto_3a
    add-int/lit8 v2, v2, 0x1

    goto :goto_23

    :cond_3d
    move v1, v2

    .line 858
    goto :goto_19

    .line 868
    :pswitch_3f
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-virtual {v0}, Landroid/support/design/widget/m;->getHeight()I

    move-result v0

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    if-lt v0, v5, :cond_3a

    .line 869
    neg-int v0, p2

    invoke-virtual {v6, v0}, Landroid/support/design/widget/dg;->a(I)Z

    goto :goto_3a

    .line 873
    :pswitch_52
    neg-int v5, p2

    int-to-float v5, v5

    iget v0, v0, Landroid/support/design/widget/p;->e:F

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/support/design/widget/dg;->a(I)Z

    goto :goto_3a

    .line 880
    :cond_5f
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0}, Landroid/support/design/widget/m;->b(Landroid/support/design/widget/m;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_6f

    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0}, Landroid/support/design/widget/m;->c(Landroid/support/design/widget/m;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_84

    .line 881
    :cond_6f
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-virtual {v0}, Landroid/support/design/widget/m;->getHeight()I

    move-result v0

    add-int/2addr v0, p2

    iget-object v2, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-virtual {v2}, Landroid/support/design/widget/m;->getScrimTriggerOffset()I

    move-result v2

    add-int/2addr v2, v1

    if-ge v0, v2, :cond_c0

    .line 882
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0}, Landroid/support/design/widget/m;->d(Landroid/support/design/widget/m;)V

    .line 888
    :cond_84
    :goto_84
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0}, Landroid/support/design/widget/m;->c(Landroid/support/design/widget/m;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_93

    if-lez v1, :cond_93

    .line 889
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 893
    :cond_93
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-virtual {v0}, Landroid/support/design/widget/m;->getHeight()I

    move-result v0

    iget-object v2, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v2}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    sub-int/2addr v0, v1

    .line 895
    iget-object v1, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v1}, Landroid/support/design/widget/m;->f(Landroid/support/design/widget/m;)Landroid/support/design/widget/l;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/support/design/widget/l;->a(F)V

    .line 898
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ne v0, v3, :cond_c6

    .line 901
    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->getTargetElevation()F

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 906
    :goto_bf
    return-void

    .line 884
    :cond_c0
    iget-object v0, p0, Landroid/support/design/widget/q;->a:Landroid/support/design/widget/m;

    invoke-static {v0}, Landroid/support/design/widget/m;->e(Landroid/support/design/widget/m;)V

    goto :goto_84

    .line 904
    :cond_c6
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    goto :goto_bf

    .line 866
    nop

    :pswitch_data_cc
    .packed-switch 0x1
        :pswitch_3f
        :pswitch_52
    .end packed-switch
.end method
