.class final Landroid/support/design/widget/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/ArrayList;

.field b:Landroid/support/design/widget/bn;

.field c:Landroid/view/animation/Animation;

.field d:Ljava/lang/ref/WeakReference;

.field private e:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    .line 30
    iput-object v1, p0, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    .line 31
    iput-object v1, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 34
    new-instance v0, Landroid/support/design/widget/bm;

    invoke-direct {v0, p0}, Landroid/support/design/widget/bm;-><init>(Landroid/support/design/widget/bl;)V

    iput-object v0, p0, Landroid/support/design/widget/bl;->e:Landroid/view/animation/Animation$AnimationListener;

    .line 172
    return-void
.end method

.method private static synthetic a(Landroid/support/design/widget/bl;)Landroid/view/animation/Animation;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private a(Landroid/support/design/widget/bn;)V
    .registers 4

    .prologue
    .line 134
    iget-object v0, p1, Landroid/support/design/widget/bn;->b:Landroid/view/animation/Animation;

    iput-object v0, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 136
    invoke-virtual {p0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_f

    .line 138
    iget-object v1, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 140
    :cond_f
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 82
    invoke-virtual {p0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v0

    .line 83
    if-ne v0, p1, :cond_8

    .line 92
    :cond_7
    :goto_7
    return-void

    .line 86
    :cond_8
    if-eqz v0, :cond_35

    .line 1095
    invoke-virtual {p0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v2

    .line 1096
    iget-object v0, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1097
    const/4 v0, 0x0

    move v1, v0

    :goto_16
    if-ge v1, v3, :cond_2f

    .line 1098
    iget-object v0, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bn;

    iget-object v0, v0, Landroid/support/design/widget/bn;->b:Landroid/view/animation/Animation;

    .line 1099
    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    if-ne v4, v0, :cond_2b

    .line 1100
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 1097
    :cond_2b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    .line 1103
    :cond_2f
    iput-object v5, p0, Landroid/support/design/widget/bl;->d:Ljava/lang/ref/WeakReference;

    .line 1104
    iput-object v5, p0, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    .line 1105
    iput-object v5, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 89
    :cond_35
    if-eqz p1, :cond_7

    .line 90
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/design/widget/bl;->d:Ljava/lang/ref/WeakReference;

    goto :goto_7
.end method

.method private a([I)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 112
    .line 113
    iget-object v0, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 114
    const/4 v0, 0x0

    move v2, v0

    :goto_9
    if-ge v2, v3, :cond_53

    .line 115
    iget-object v0, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bn;

    .line 116
    iget-object v4, v0, Landroid/support/design/widget/bn;->a:[I

    invoke-static {v4, p1}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 121
    :goto_1b
    iget-object v2, p0, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    if-ne v0, v2, :cond_24

    .line 131
    :cond_1f
    :goto_1f
    return-void

    .line 114
    :cond_20
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 124
    :cond_24
    iget-object v2, p0, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    if-eqz v2, :cond_3f

    .line 1143
    iget-object v2, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-eqz v2, :cond_3f

    .line 1144
    invoke-virtual {p0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v2

    .line 1145
    if-eqz v2, :cond_3d

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v3

    iget-object v4, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-ne v3, v4, :cond_3d

    .line 1146
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 1148
    :cond_3d
    iput-object v1, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 127
    :cond_3f
    iput-object v0, p0, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    .line 128
    if-eqz v0, :cond_1f

    .line 2134
    iget-object v0, v0, Landroid/support/design/widget/bn;->b:Landroid/view/animation/Animation;

    iput-object v0, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 2136
    invoke-virtual {p0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v0

    .line 2137
    if-eqz v0, :cond_1f

    .line 2138
    iget-object v1, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1f

    :cond_53
    move-object v0, v1

    goto :goto_1b
.end method

.method private b()Landroid/view/animation/Animation;
    .registers 2

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private static synthetic b(Landroid/support/design/widget/bl;)Landroid/view/animation/Animation;
    .registers 2

    .prologue
    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private c()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 95
    invoke-virtual {p0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v2

    .line 96
    iget-object v0, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 97
    const/4 v0, 0x0

    move v1, v0

    :goto_d
    if-ge v1, v3, :cond_26

    .line 98
    iget-object v0, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bn;

    iget-object v0, v0, Landroid/support/design/widget/bn;->b:Landroid/view/animation/Animation;

    .line 99
    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    if-ne v4, v0, :cond_22

    .line 100
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 97
    :cond_22
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 103
    :cond_26
    iput-object v5, p0, Landroid/support/design/widget/bl;->d:Ljava/lang/ref/WeakReference;

    .line 104
    iput-object v5, p0, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    .line 105
    iput-object v5, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 106
    return-void
.end method

.method private d()V
    .registers 4

    .prologue
    .line 143
    iget-object v0, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-eqz v0, :cond_18

    .line 144
    invoke-virtual {p0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    iget-object v2, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-ne v1, v2, :cond_15

    .line 146
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 148
    :cond_15
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 150
    :cond_18
    return-void
.end method

.method private e()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 156
    iget-object v0, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private f()V
    .registers 4

    .prologue
    .line 164
    iget-object v0, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-eqz v0, :cond_15

    .line 165
    invoke-virtual {p0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    iget-object v2, p0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-ne v1, v2, :cond_15

    .line 167
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 170
    :cond_15
    return-void
.end method


# virtual methods
.method final a()Landroid/view/View;
    .registers 2

    .prologue
    .line 78
    iget-object v0, p0, Landroid/support/design/widget/bl;->d:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Landroid/support/design/widget/bl;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_5
.end method

.method public final a([ILandroid/view/animation/Animation;)V
    .registers 5

    .prologue
    .line 61
    new-instance v0, Landroid/support/design/widget/bn;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Landroid/support/design/widget/bn;-><init>([ILandroid/view/animation/Animation;B)V

    .line 62
    iget-object v1, p0, Landroid/support/design/widget/bl;->e:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {p2, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 63
    iget-object v1, p0, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method
