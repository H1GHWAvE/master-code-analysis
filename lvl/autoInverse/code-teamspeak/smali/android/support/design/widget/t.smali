.class public abstract Landroid/support/design/widget/t;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1620
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3

    .prologue
    .line 1630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1631
    return-void
.end method

.method private static a()I
    .registers 1

    .prologue
    .line 1691
    const/high16 v0, -0x1000000

    return v0
.end method

.method private static a(Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 1

    .prologue
    .line 2112
    return-object p0
.end method

.method private static a(Landroid/view/View;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 1878
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 1879
    iput-object p1, v0, Landroid/support/design/widget/w;->m:Ljava/lang/Object;

    .line 1880
    return-void
.end method

.method private static b()F
    .registers 1

    .prologue
    .line 1708
    const/4 v0, 0x0

    return v0
.end method

.method private static c(Landroid/view/View;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1890
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 1891
    iget-object v0, v0, Landroid/support/design/widget/w;->m:Ljava/lang/Object;

    return-object v0
.end method

.method private static c()Z
    .registers 1

    .prologue
    .line 1724
    const/4 v0, 0x0

    return v0
.end method

.method private static d()Z
    .registers 1

    .prologue
    .line 1811
    const/4 v0, 0x0

    return v0
.end method

.method private static e()V
    .registers 0

    .prologue
    .line 1944
    return-void
.end method

.method private static f()Z
    .registers 1

    .prologue
    .line 2093
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
    .registers 4

    .prologue
    .line 2150
    sget-object v0, Landroid/view/View$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    return-object v0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I[I)V
    .registers 5

    .prologue
    .line 2034
    return-void
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
    .registers 4

    .prologue
    .line 2129
    return-void
.end method

.method public a(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1968
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1797
    return-void
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;FZ)Z
    .registers 6

    .prologue
    .line 2065
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .registers 5

    .prologue
    .line 1865
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)Z
    .registers 7

    .prologue
    .line 1837
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 5

    .prologue
    .line 1672
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .registers 5

    .prologue
    .line 1779
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;I)Z
    .registers 6

    .prologue
    .line 1918
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 2001
    return-void
.end method

.method public b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 5

    .prologue
    .line 1653
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1750
    const/4 v0, 0x0

    return v0
.end method
