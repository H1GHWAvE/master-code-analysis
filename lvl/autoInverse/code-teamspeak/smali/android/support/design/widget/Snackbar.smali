.class public final Landroid/support/design/widget/Snackbar;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = -0x2

.field public static final b:I = -0x1

.field public static final c:I = 0x0

.field private static final f:I = 0xfa

.field private static final g:I = 0xb4

.field private static final h:Landroid/os/Handler;

.field private static final i:I = 0x0

.field private static final j:I = 0x1


# instance fields
.field final d:Landroid/view/ViewGroup;

.field final e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

.field private final k:Landroid/content/Context;

.field private l:I

.field private m:Landroid/support/design/widget/bd;

.field private final n:Landroid/support/design/widget/bj;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 156
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Landroid/support/design/widget/at;

    invoke-direct {v2}, Landroid/support/design/widget/at;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v0, Landroid/support/design/widget/Snackbar;->h:Landroid/os/Handler;

    .line 170
    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;)V
    .registers 6

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410
    new-instance v0, Landroid/support/design/widget/av;

    invoke-direct {v0, p0}, Landroid/support/design/widget/av;-><init>(Landroid/support/design/widget/Snackbar;)V

    iput-object v0, p0, Landroid/support/design/widget/Snackbar;->n:Landroid/support/design/widget/bj;

    .line 179
    iput-object p1, p0, Landroid/support/design/widget/Snackbar;->d:Landroid/view/ViewGroup;

    .line 180
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/Snackbar;->k:Landroid/content/Context;

    .line 182
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 183
    sget v1, Landroid/support/design/k;->design_layout_snackbar:I

    iget-object v2, p0, Landroid/support/design/widget/Snackbar;->d:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    iput-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    .line 184
    return-void
.end method

.method private a(I)Landroid/support/design/widget/Snackbar;
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getActionView()Landroid/widget/Button;

    move-result-object v0

    .line 319
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 320
    return-object p0
.end method

.method private a(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;
    .registers 6
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->k:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 4281
    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getActionView()Landroid/widget/Button;

    move-result-object v1

    .line 4283
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_14

    if-nez p2, :cond_1e

    .line 4284
    :cond_14
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4285
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    :goto_1d
    return-object p0

    .line 4287
    :cond_1e
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4288
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4289
    new-instance v0, Landroid/support/design/widget/au;

    invoke-direct {v0, p0, p2}, Landroid/support/design/widget/au;-><init>(Landroid/support/design/widget/Snackbar;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1d
.end method

.method private a(Landroid/content/res/ColorStateList;)Landroid/support/design/widget/Snackbar;
    .registers 3
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 307
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getActionView()Landroid/widget/Button;

    move-result-object v0

    .line 308
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 309
    return-object p0
.end method

.method private a(Landroid/support/design/widget/bd;)Landroid/support/design/widget/Snackbar;
    .registers 2
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 399
    iput-object p1, p0, Landroid/support/design/widget/Snackbar;->m:Landroid/support/design/widget/bd;

    .line 400
    return-object p0
.end method

.method private static a(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;
    .registers 6
    .param p0    # Landroid/view/View;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 231
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 3206
    new-instance v1, Landroid/support/design/widget/Snackbar;

    invoke-static {p0}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/design/widget/Snackbar;-><init>(Landroid/view/ViewGroup;)V

    .line 3207
    invoke-direct {v1, v0}, Landroid/support/design/widget/Snackbar;->a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;

    .line 3354
    iput p2, v1, Landroid/support/design/widget/Snackbar;->l:I

    .line 231
    return-object v1
.end method

.method private static a(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;
    .registers 5
    .param p0    # Landroid/view/View;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 206
    new-instance v0, Landroid/support/design/widget/Snackbar;

    invoke-static {p0}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/design/widget/Snackbar;-><init>(Landroid/view/ViewGroup;)V

    .line 207
    invoke-direct {v0, p1}, Landroid/support/design/widget/Snackbar;->a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;

    .line 2354
    iput p2, v0, Landroid/support/design/widget/Snackbar;->l:I

    .line 209
    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;
    .registers 3
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getMessageView()Landroid/widget/TextView;

    move-result-object v0

    .line 331
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;
    .registers 5
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getActionView()Landroid/widget/Button;

    move-result-object v0

    .line 283
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    if-nez p2, :cond_18

    .line 284
    :cond_e
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    :goto_17
    return-object p0

    .line 287
    :cond_18
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    new-instance v1, Landroid/support/design/widget/au;

    invoke-direct {v1, p0, p2}, Landroid/support/design/widget/au;-><init>(Landroid/support/design/widget/Snackbar;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_17
.end method

.method static synthetic a(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bj;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->n:Landroid/support/design/widget/bj;

    return-object v0
.end method

.method private static a(Landroid/view/View;)Landroid/view/ViewGroup;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 235
    move-object v1, v2

    move-object v0, p0

    .line 237
    :cond_3
    instance-of v3, v0, Landroid/support/design/widget/CoordinatorLayout;

    if-eqz v3, :cond_a

    .line 239
    check-cast v0, Landroid/view/ViewGroup;

    .line 259
    :goto_9
    return-object v0

    .line 240
    :cond_a
    instance-of v3, v0, Landroid/widget/FrameLayout;

    if-eqz v3, :cond_1d

    .line 241
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    const v3, 0x1020002

    if-ne v1, v3, :cond_1a

    .line 244
    check-cast v0, Landroid/view/ViewGroup;

    goto :goto_9

    :cond_1a
    move-object v1, v0

    .line 247
    check-cast v1, Landroid/view/ViewGroup;

    .line 251
    :cond_1d
    if-eqz v0, :cond_29

    .line 253
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 254
    instance-of v3, v0, Landroid/view/View;

    if-eqz v3, :cond_2d

    check-cast v0, Landroid/view/View;

    .line 256
    :cond_29
    :goto_29
    if-nez v0, :cond_3

    move-object v0, v1

    .line 259
    goto :goto_9

    :cond_2d
    move-object v0, v2

    .line 254
    goto :goto_29
.end method

.method static synthetic a(Landroid/support/design/widget/Snackbar;I)V
    .registers 2

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/support/design/widget/Snackbar;->d(I)V

    return-void
.end method

.method private b(I)Landroid/support/design/widget/Snackbar;
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 342
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->k:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/design/widget/Snackbar;->a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Landroid/support/design/widget/Snackbar;)V
    .registers 1

    .prologue
    .line 67
    invoke-virtual {p0}, Landroid/support/design/widget/Snackbar;->a()V

    return-void
.end method

.method static synthetic c()Landroid/os/Handler;
    .registers 1

    .prologue
    .line 67
    sget-object v0, Landroid/support/design/widget/Snackbar;->h:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/Snackbar$SnackbarLayout;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    return-object v0
.end method

.method private c(I)Landroid/support/design/widget/Snackbar;
    .registers 2
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 354
    iput p1, p0, Landroid/support/design/widget/Snackbar;->l:I

    .line 355
    return-object p0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 365
    iget v0, p0, Landroid/support/design/widget/Snackbar;->l:I

    return v0
.end method

.method static synthetic d(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bd;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->m:Landroid/support/design/widget/bd;

    return-object v0
.end method

.method private d(I)V
    .registers 6

    .prologue
    .line 391
    invoke-static {}, Landroid/support/design/widget/bh;->a()Landroid/support/design/widget/bh;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->n:Landroid/support/design/widget/bj;

    .line 7103
    iget-object v2, v0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 7104
    :try_start_9
    invoke-virtual {v0, v1}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 7105
    iget-object v0, v0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-static {v0, p1}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;I)Z

    .line 7109
    :cond_14
    :goto_14
    monitor-exit v2

    return-void

    .line 7106
    :cond_16
    invoke-virtual {v0, v1}, Landroid/support/design/widget/bh;->e(Landroid/support/design/widget/bj;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 7107
    iget-object v0, v0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    invoke-static {v0, p1}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;I)Z

    goto :goto_14

    .line 7109
    :catchall_22
    move-exception v0

    monitor-exit v2
    :try_end_24
    .catchall {:try_start_9 .. :try_end_24} :catchall_22

    throw v0
.end method

.method private e()Landroid/view/View;
    .registers 2
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 373
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    return-object v0
.end method

.method private e(I)V
    .registers 6

    .prologue
    const-wide/16 v2, 0xfa

    .line 520
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_30

    .line 521
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    move-result-object v0

    new-instance v1, Landroid/support/design/widget/ba;

    invoke-direct {v1, p0, p1}, Landroid/support/design/widget/ba;-><init>(Landroid/support/design/widget/Snackbar;I)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    .line 553
    :goto_2f
    return-void

    .line 536
    :cond_30
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Landroid/support/design/c;->design_snackbar_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 537
    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 538
    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 539
    new-instance v1, Landroid/support/design/widget/bb;

    invoke-direct {v1, p0, p1}, Landroid/support/design/widget/bb;-><init>(Landroid/support/design/widget/Snackbar;I)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 551
    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2f
.end method

.method static synthetic e(Landroid/support/design/widget/Snackbar;)V
    .registers 1

    .prologue
    .line 67
    invoke-virtual {p0}, Landroid/support/design/widget/Snackbar;->b()V

    return-void
.end method

.method private f()V
    .registers 6

    .prologue
    .line 380
    invoke-static {}, Landroid/support/design/widget/bh;->a()Landroid/support/design/widget/bh;

    move-result-object v0

    iget v1, p0, Landroid/support/design/widget/Snackbar;->l:I

    iget-object v2, p0, Landroid/support/design/widget/Snackbar;->n:Landroid/support/design/widget/bj;

    .line 5071
    iget-object v3, v0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 5072
    :try_start_b
    invoke-virtual {v0, v2}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 5074
    iget-object v2, v0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    .line 5156
    iput v1, v2, Landroid/support/design/widget/bk;->b:I

    .line 5078
    iget-object v1, v0, Landroid/support/design/widget/bh;->b:Landroid/os/Handler;

    iget-object v2, v0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 5079
    iget-object v1, v0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;)V

    .line 5080
    monitor-exit v3

    .line 5099
    :goto_22
    return-void

    .line 5081
    :cond_23
    invoke-virtual {v0, v2}, Landroid/support/design/widget/bh;->e(Landroid/support/design/widget/bj;)Z

    move-result v4

    if-eqz v4, :cond_3f

    .line 5083
    iget-object v2, v0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    .line 6156
    iput v1, v2, Landroid/support/design/widget/bk;->b:I

    .line 5089
    :goto_2d
    iget-object v1, v0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    if-eqz v1, :cond_47

    iget-object v1, v0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bk;I)Z

    move-result v1

    if-eqz v1, :cond_47

    .line 5092
    monitor-exit v3

    goto :goto_22

    .line 5099
    :catchall_3c
    move-exception v0

    monitor-exit v3
    :try_end_3e
    .catchall {:try_start_b .. :try_end_3e} :catchall_3c

    throw v0

    .line 5086
    :cond_3f
    :try_start_3f
    new-instance v4, Landroid/support/design/widget/bk;

    invoke-direct {v4, v1, v2}, Landroid/support/design/widget/bk;-><init>(ILandroid/support/design/widget/bj;)V

    iput-object v4, v0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    goto :goto_2d

    .line 5095
    :cond_47
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    .line 5097
    invoke-virtual {v0}, Landroid/support/design/widget/bh;->b()V

    .line 5099
    monitor-exit v3
    :try_end_4e
    .catchall {:try_start_3f .. :try_end_4e} :catchall_3c

    goto :goto_22
.end method

.method private f(I)V
    .registers 8

    .prologue
    const-wide/16 v4, 0xfa

    const/4 v1, 0x0

    .line 556
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2c

    .line 9578
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 9580
    instance-of v2, v0, Landroid/support/design/widget/w;

    if-eqz v2, :cond_34

    .line 9581
    check-cast v0, Landroid/support/design/widget/w;

    .line 10281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 9584
    instance-of v2, v0, Landroid/support/design/widget/SwipeDismissBehavior;

    if-eqz v2, :cond_34

    .line 9585
    check-cast v0, Landroid/support/design/widget/SwipeDismissBehavior;

    .line 10369
    iget-object v2, v0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    if-eqz v2, :cond_30

    iget-object v0, v0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    .line 10421
    iget v0, v0, Landroid/support/v4/widget/eg;->m:I

    .line 9585
    :goto_27
    if-eqz v0, :cond_32

    const/4 v0, 0x1

    .line 556
    :goto_2a
    if-eqz v0, :cond_36

    .line 557
    :cond_2c
    invoke-virtual {p0}, Landroid/support/design/widget/Snackbar;->b()V

    .line 10521
    :goto_2f
    return-void

    :cond_30
    move v0, v1

    .line 10369
    goto :goto_27

    :cond_32
    move v0, v1

    .line 9585
    goto :goto_2a

    :cond_34
    move v0, v1

    .line 9589
    goto :goto_2a

    .line 10520
    :cond_36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_64

    .line 10521
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    move-result-object v0

    new-instance v1, Landroid/support/design/widget/ba;

    invoke-direct {v1, p0, p1}, Landroid/support/design/widget/ba;-><init>(Landroid/support/design/widget/Snackbar;I)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    goto :goto_2f

    .line 10536
    :cond_64
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Landroid/support/design/c;->design_snackbar_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 10537
    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 10538
    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 10539
    new-instance v1, Landroid/support/design/widget/bb;

    invoke-direct {v1, p0, p1}, Landroid/support/design/widget/bb;-><init>(Landroid/support/design/widget/Snackbar;I)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 10551
    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2f
.end method

.method private g()V
    .registers 2

    .prologue
    .line 387
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Landroid/support/design/widget/Snackbar;->d(I)V

    .line 388
    return-void
.end method

.method private h()Z
    .registers 2

    .prologue
    .line 407
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->isShown()Z

    move-result v0

    return v0
.end method

.method private i()V
    .registers 4

    .prologue
    .line 423
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_3f

    .line 424
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 426
    instance-of v1, v0, Landroid/support/design/widget/w;

    if-eqz v1, :cond_38

    .line 429
    new-instance v1, Landroid/support/design/widget/bc;

    invoke-direct {v1, p0}, Landroid/support/design/widget/bc;-><init>(Landroid/support/design/widget/Snackbar;)V

    .line 7143
    const v2, 0x3dcccccd    # 0.1f

    invoke-static {v2}, Landroid/support/design/widget/SwipeDismissBehavior;->a(F)F

    move-result v2

    iput v2, v1, Landroid/support/design/widget/SwipeDismissBehavior;->k:F

    .line 7152
    const v2, 0x3f19999a    # 0.6f

    invoke-static {v2}, Landroid/support/design/widget/SwipeDismissBehavior;->a(F)F

    move-result v2

    iput v2, v1, Landroid/support/design/widget/SwipeDismissBehavior;->l:F

    .line 8125
    const/4 v2, 0x0

    iput v2, v1, Landroid/support/design/widget/SwipeDismissBehavior;->j:I

    .line 433
    new-instance v2, Landroid/support/design/widget/aw;

    invoke-direct {v2, p0}, Landroid/support/design/widget/aw;-><init>(Landroid/support/design/widget/Snackbar;)V

    .line 9115
    iput-object v2, v1, Landroid/support/design/widget/SwipeDismissBehavior;->i:Landroid/support/design/widget/bp;

    .line 454
    check-cast v0, Landroid/support/design/widget/w;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/w;->a(Landroid/support/design/widget/t;)V

    .line 457
    :cond_38
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 460
    :cond_3f
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 462
    invoke-virtual {p0}, Landroid/support/design/widget/Snackbar;->a()V

    .line 473
    :goto_4a
    return-void

    .line 465
    :cond_4b
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    new-instance v1, Landroid/support/design/widget/ax;

    invoke-direct {v1, p0}, Landroid/support/design/widget/ax;-><init>(Landroid/support/design/widget/Snackbar;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->setOnLayoutChangeListener(Landroid/support/design/widget/bg;)V

    goto :goto_4a
.end method

.method private j()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 578
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 580
    instance-of v2, v0, Landroid/support/design/widget/w;

    if-eqz v2, :cond_25

    .line 581
    check-cast v0, Landroid/support/design/widget/w;

    .line 11281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 584
    instance-of v2, v0, Landroid/support/design/widget/SwipeDismissBehavior;

    if-eqz v2, :cond_25

    .line 585
    check-cast v0, Landroid/support/design/widget/SwipeDismissBehavior;

    .line 11369
    iget-object v2, v0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    if-eqz v2, :cond_21

    iget-object v0, v0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    .line 11421
    iget v0, v0, Landroid/support/v4/widget/eg;->m:I

    .line 585
    :goto_1d
    if-eqz v0, :cond_23

    const/4 v0, 0x1

    .line 589
    :goto_20
    return v0

    :cond_21
    move v0, v1

    .line 11369
    goto :goto_1d

    :cond_23
    move v0, v1

    .line 585
    goto :goto_20

    :cond_25
    move v0, v1

    .line 589
    goto :goto_20
.end method


# virtual methods
.method final a()V
    .registers 5

    .prologue
    const-wide/16 v2, 0xfa

    .line 476
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_36

    .line 477
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 478
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    move-result-object v0

    new-instance v1, Landroid/support/design/widget/ay;

    invoke-direct {v1, p0}, Landroid/support/design/widget/ay;-><init>(Landroid/support/design/widget/Snackbar;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    .line 517
    :goto_35
    return-void

    .line 497
    :cond_36
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Landroid/support/design/c;->design_snackbar_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 498
    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 499
    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 500
    new-instance v1, Landroid/support/design/widget/az;

    invoke-direct {v1, p0}, Landroid/support/design/widget/az;-><init>(Landroid/support/design/widget/Snackbar;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 515
    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_35
.end method

.method final b()V
    .registers 4

    .prologue
    .line 565
    iget-object v0, p0, Landroid/support/design/widget/Snackbar;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->e:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 571
    invoke-static {}, Landroid/support/design/widget/bh;->a()Landroid/support/design/widget/bh;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/Snackbar;->n:Landroid/support/design/widget/bj;

    .line 11117
    iget-object v2, v0, Landroid/support/design/widget/bh;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 11118
    :try_start_10
    invoke-virtual {v0, v1}, Landroid/support/design/widget/bh;->d(Landroid/support/design/widget/bj;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 11120
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/design/widget/bh;->c:Landroid/support/design/widget/bk;

    .line 11121
    iget-object v1, v0, Landroid/support/design/widget/bh;->d:Landroid/support/design/widget/bk;

    if-eqz v1, :cond_20

    .line 11122
    invoke-virtual {v0}, Landroid/support/design/widget/bh;->b()V

    .line 11125
    :cond_20
    monitor-exit v2

    return-void

    :catchall_22
    move-exception v0

    monitor-exit v2
    :try_end_24
    .catchall {:try_start_10 .. :try_end_24} :catchall_22

    throw v0
.end method
