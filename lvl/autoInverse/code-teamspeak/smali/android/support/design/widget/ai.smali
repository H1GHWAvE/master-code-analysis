.class Landroid/support/design/widget/ai;
.super Landroid/support/design/widget/ad;
.source "SourceFile"


# instance fields
.field private h:Z


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/support/design/widget/as;)V
    .registers 3

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/ad;-><init>(Landroid/view/View;Landroid/support/design/widget/as;)V

    .line 30
    return-void
.end method

.method static synthetic a(Landroid/support/design/widget/ai;Z)Z
    .registers 2

    .prologue
    .line 24
    iput-boolean p1, p0, Landroid/support/design/widget/ai;->h:Z

    return p1
.end method


# virtual methods
.method final b()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-boolean v0, p0, Landroid/support/design/widget/ai;->h:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_e

    .line 68
    :cond_d
    :goto_d
    return-void

    .line 39
    :cond_e
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 41
    :cond_1e
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_d

    .line 43
    :cond_26
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/support/design/widget/aj;

    invoke-direct {v1, p0}, Landroid/support/design/widget/aj;-><init>(Landroid/support/design/widget/ai;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_d
.end method

.method final c()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 72
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_50

    .line 73
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_51

    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_51

    .line 74
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 75
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 76
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 77
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/support/design/widget/ak;

    invoke-direct {v1, p0}, Landroid/support/design/widget/ak;-><init>(Landroid/support/design/widget/ai;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 96
    :cond_50
    :goto_50
    return-void

    .line 90
    :cond_51
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 92
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 93
    iget-object v0, p0, Landroid/support/design/widget/ai;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    goto :goto_50
.end method
