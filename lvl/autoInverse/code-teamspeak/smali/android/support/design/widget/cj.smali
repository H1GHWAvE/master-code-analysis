.class final Landroid/support/design/widget/cj;
.super Landroid/support/v4/view/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/design/widget/ce;


# direct methods
.method private constructor <init>(Landroid/support/design/widget/ce;)V
    .registers 2

    .prologue
    .line 511
    iput-object p1, p0, Landroid/support/design/widget/cj;->a:Landroid/support/design/widget/ce;

    invoke-direct {p0}, Landroid/support/v4/view/a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/design/widget/ce;B)V
    .registers 3

    .prologue
    .line 511
    invoke-direct {p0, p1}, Landroid/support/design/widget/cj;-><init>(Landroid/support/design/widget/ce;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
    .registers 6

    .prologue
    .line 530
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 531
    const-class v0, Landroid/support/design/widget/ce;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 533
    iget-object v0, p0, Landroid/support/design/widget/cj;->a:Landroid/support/design/widget/ce;

    invoke-static {v0}, Landroid/support/design/widget/ce;->b(Landroid/support/design/widget/ce;)Landroid/support/design/widget/l;

    move-result-object v0

    .line 4526
    iget-object v0, v0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    .line 534
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 5074
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v2, p2, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 537
    :cond_21
    iget-object v0, p0, Landroid/support/design/widget/cj;->a:Landroid/support/design/widget/ce;

    invoke-static {v0}, Landroid/support/design/widget/ce;->c(Landroid/support/design/widget/ce;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 538
    iget-object v0, p0, Landroid/support/design/widget/cj;->a:Landroid/support/design/widget/ce;

    invoke-static {v0}, Landroid/support/design/widget/ce;->c(Landroid/support/design/widget/ce;)Landroid/widget/EditText;

    move-result-object v0

    .line 5315
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v2, p2, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/w;->g(Ljava/lang/Object;Landroid/view/View;)V

    .line 540
    :cond_36
    iget-object v0, p0, Landroid/support/design/widget/cj;->a:Landroid/support/design/widget/ce;

    invoke-static {v0}, Landroid/support/design/widget/ce;->d(Landroid/support/design/widget/ce;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_5d

    iget-object v0, p0, Landroid/support/design/widget/cj;->a:Landroid/support/design/widget/ce;

    invoke-static {v0}, Landroid/support/design/widget/ce;->d(Landroid/support/design/widget/ce;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 541
    :goto_48
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5c

    .line 6270
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v2, p2, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v2}, Landroid/support/v4/view/a/w;->T(Ljava/lang/Object;)V

    .line 6296
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v2, p2, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 545
    :cond_5c
    return-void

    .line 540
    :cond_5d
    const/4 v0, 0x0

    goto :goto_48
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4

    .prologue
    .line 514
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 515
    const-class v0, Landroid/support/design/widget/ce;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 516
    return-void
.end method

.method public final b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5

    .prologue
    .line 520
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 522
    iget-object v0, p0, Landroid/support/design/widget/cj;->a:Landroid/support/design/widget/ce;

    invoke-static {v0}, Landroid/support/design/widget/ce;->b(Landroid/support/design/widget/ce;)Landroid/support/design/widget/l;

    move-result-object v0

    .line 3526
    iget-object v0, v0, Landroid/support/design/widget/l;->g:Ljava/lang/CharSequence;

    .line 523
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_18

    .line 524
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 526
    :cond_18
    return-void
.end method
