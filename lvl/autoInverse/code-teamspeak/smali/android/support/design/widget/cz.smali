.class final Landroid/support/design/widget/cz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/support/design/widget/db;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 48
    const/16 v1, 0xb

    if-lt v0, v1, :cond_f

    .line 49
    new-instance v0, Landroid/support/design/widget/dd;

    invoke-direct {v0, v2}, Landroid/support/design/widget/dd;-><init>(B)V

    sput-object v0, Landroid/support/design/widget/cz;->a:Landroid/support/design/widget/db;

    .line 53
    :goto_e
    return-void

    .line 51
    :cond_f
    new-instance v0, Landroid/support/design/widget/dc;

    invoke-direct {v0, v2}, Landroid/support/design/widget/dc;-><init>(B)V

    sput-object v0, Landroid/support/design/widget/cz;->a:Landroid/support/design/widget/db;

    goto :goto_e
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method static a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 1065
    sget-object v0, Landroid/support/design/widget/cz;->a:Landroid/support/design/widget/db;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/design/widget/db;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 78
    return-void
.end method

.method private static b(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 4

    .prologue
    .line 65
    sget-object v0, Landroid/support/design/widget/cz;->a:Landroid/support/design/widget/db;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/design/widget/db;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 66
    return-void
.end method
