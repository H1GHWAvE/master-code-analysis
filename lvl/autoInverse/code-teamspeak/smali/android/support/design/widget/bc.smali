.class final Landroid/support/design/widget/bc;
.super Landroid/support/design/widget/SwipeDismissBehavior;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/design/widget/Snackbar;


# direct methods
.method constructor <init>(Landroid/support/design/widget/Snackbar;)V
    .registers 2

    .prologue
    .line 744
    iput-object p1, p0, Landroid/support/design/widget/bc;->a:Landroid/support/design/widget/Snackbar;

    invoke-direct {p0}, Landroid/support/design/widget/SwipeDismissBehavior;-><init>()V

    return-void
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/Snackbar$SnackbarLayout;Landroid/view/MotionEvent;)Z
    .registers 6

    .prologue
    .line 750
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1, p2, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 751
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_38

    .line 762
    :cond_17
    :goto_17
    :pswitch_17
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/SwipeDismissBehavior;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 753
    :pswitch_1c
    invoke-static {}, Landroid/support/design/widget/bh;->a()Landroid/support/design/widget/bh;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/bc;->a:Landroid/support/design/widget/Snackbar;

    invoke-static {v1}, Landroid/support/design/widget/Snackbar;->a(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bj;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/bh;->b(Landroid/support/design/widget/bj;)V

    goto :goto_17

    .line 757
    :pswitch_2a
    invoke-static {}, Landroid/support/design/widget/bh;->a()Landroid/support/design/widget/bh;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/bc;->a:Landroid/support/design/widget/Snackbar;

    invoke-static {v1}, Landroid/support/design/widget/Snackbar;->a(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bj;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/bh;->c(Landroid/support/design/widget/bj;)V

    goto :goto_17

    .line 751
    :pswitch_data_38
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_2a
        :pswitch_17
        :pswitch_2a
    .end packed-switch
.end method


# virtual methods
.method public final synthetic b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 5

    .prologue
    .line 744
    check-cast p2, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/bc;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/Snackbar$SnackbarLayout;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
