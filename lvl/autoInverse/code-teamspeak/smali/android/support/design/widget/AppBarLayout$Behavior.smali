.class public Landroid/support/design/widget/AppBarLayout$Behavior;
.super Landroid/support/design/widget/df;
.source "SourceFile"


# static fields
.field private static final a:I = -0x1

.field private static final b:I = -0x1


# instance fields
.field private c:I

.field private d:Z

.field private e:Ljava/lang/Runnable;

.field private f:Landroid/support/v4/widget/ca;

.field private g:Landroid/support/design/widget/ck;

.field private h:I

.field private i:Z

.field private j:F

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 652
    invoke-direct {p0}, Landroid/support/design/widget/df;-><init>()V

    .line 641
    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    .line 646
    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    .line 648
    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    .line 652
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    const/4 v0, -0x1

    .line 655
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/df;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 641
    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    .line 646
    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    .line 648
    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    .line 656
    return-void
.end method

.method private static a(Landroid/support/design/widget/AppBarLayout;I)I
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 1052
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1054
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v4

    move v2, v1

    :goto_a
    if-ge v2, v4, :cond_62

    .line 1055
    invoke-virtual {p0, v2}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1056
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/g;

    .line 12621
    iget-object v6, v0, Landroid/support/design/widget/g;->g:Landroid/view/animation/Interpolator;

    .line 1059
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v7

    if-lt v3, v7, :cond_63

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v7

    if-gt v3, v7, :cond_63

    .line 1060
    if-eqz v6, :cond_62

    .line 13597
    iget v2, v0, Landroid/support/design/widget/g;->f:I

    .line 1063
    and-int/lit8 v4, v2, 0x1

    if-eqz v4, :cond_67

    .line 1065
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v4, v0, Landroid/support/design/widget/g;->topMargin:I

    add-int/2addr v1, v4

    iget v0, v0, Landroid/support/design/widget/g;->bottomMargin:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 1068
    and-int/lit8 v1, v2, 0x2

    if-eqz v1, :cond_41

    .line 1071
    invoke-static {v5}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 1075
    :cond_41
    :goto_41
    if-lez v0, :cond_62

    .line 1076
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v1, v3, v1

    .line 1077
    int-to-float v2, v0

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-interface {v6, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1081
    invoke-static {p1}, Ljava/lang/Integer;->signum(I)I

    move-result v1

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v0, v2

    mul-int p1, v1, v0

    .line 1091
    :cond_62
    return p1

    .line 1054
    :cond_63
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    :cond_67
    move v0, v1

    goto :goto_41
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
    .registers 12

    .prologue
    .line 982
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v0

    sub-int v3, v0, p3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I

    move-result v0

    return v0
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)Landroid/os/Parcelable;
    .registers 11

    .prologue
    const/4 v0, 0x0

    .line 1100
    invoke-super {p0, p1, p2}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;

    move-result-object v2

    .line 14629
    invoke-super {p0}, Landroid/support/design/widget/df;->c()I

    move-result v4

    .line 1104
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v5

    move v3, v0

    :goto_e
    if-ge v3, v5, :cond_42

    .line 1105
    invoke-virtual {p2, v3}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1106
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int v7, v1, v4

    .line 1108
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v1, v4

    if-gtz v1, :cond_3e

    if-ltz v7, :cond_3e

    .line 1109
    new-instance v1, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;

    invoke-direct {v1, v2}, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1110
    iput v3, v1, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->a:I

    .line 1111
    invoke-static {v6}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v2

    if-ne v7, v2, :cond_31

    const/4 v0, 0x1

    :cond_31
    iput-boolean v0, v1, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->c:Z

    .line 1113
    int-to-float v0, v7

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    iput v0, v1, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->b:F

    move-object v0, v1

    .line 1119
    :goto_3d
    return-object v0

    .line 1104
    :cond_3e
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_e

    :cond_42
    move-object v0, v2

    .line 1119
    goto :goto_3d
.end method

.method static synthetic a(Landroid/support/design/widget/AppBarLayout$Behavior;)Landroid/support/v4/widget/ca;
    .registers 2

    .prologue
    .line 629
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    return-object v0
.end method

.method private a(Landroid/support/design/widget/AppBarLayout;)V
    .registers 7

    .prologue
    .line 1039
    invoke-static {p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/support/design/widget/AppBarLayout;)Ljava/util/List;

    move-result-object v2

    .line 1043
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_a
    if-ge v1, v3, :cond_1f

    .line 1044
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/i;

    .line 1045
    if-eqz v0, :cond_1b

    .line 11629
    invoke-super {p0}, Landroid/support/design/widget/df;->c()I

    move-result v4

    .line 1046
    invoke-interface {v0, p1, v4}, Landroid/support/design/widget/i;->a(Landroid/support/design/widget/AppBarLayout;I)V

    .line 1043
    :cond_1b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 1049
    :cond_1f
    return-void
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I[I)V
    .registers 12

    .prologue
    .line 681
    if-eqz p3, :cond_1e

    iget-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:Z

    if-nez v0, :cond_1e

    .line 683
    if-gez p3, :cond_1f

    .line 685
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    neg-int v4, v0

    .line 686
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getDownNestedPreScrollRange()I

    move-result v0

    add-int v5, v4, v0

    .line 692
    :goto_13
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I

    move-result v0

    aput v0, p4, v6

    .line 694
    :cond_1e
    return-void

    .line 689
    :cond_1f
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getUpNestedPreScrollRange()I

    move-result v0

    neg-int v4, v0

    .line 690
    const/4 v5, 0x0

    goto :goto_13
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/os/Parcelable;)V
    .registers 5

    .prologue
    .line 1125
    instance-of v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;

    if-eqz v0, :cond_1a

    .line 1126
    check-cast p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;

    .line 1127
    invoke-virtual {p3}, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, p1, p2, v0}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 1128
    iget v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->a:I

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    .line 1129
    iget v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->b:F

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->j:F

    .line 1130
    iget-boolean v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->c:Z

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->i:Z

    .line 1135
    :goto_19
    return-void

    .line 1132
    :cond_1a
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 1133
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    goto :goto_19
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;FZ)Z
    .registers 16

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 835
    if-nez p4, :cond_4c

    .line 837
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    neg-int v7, v0

    neg-float v3, p3

    .line 3894
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_14

    .line 3895
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Landroid/support/design/widget/AppBarLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3898
    :cond_14
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    if-nez v0, :cond_22

    .line 3899
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 4246
    invoke-static {v0, v10}, Landroid/support/v4/widget/ca;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;

    move-result-object v0

    .line 3899
    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    .line 3902
    :cond_22
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v4

    move v3, v1

    move v5, v1

    move v6, v1

    move v8, v1

    invoke-virtual/range {v0 .. v8}, Landroid/support/v4/widget/ca;->a(IIIIIIII)V

    .line 3908
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->f()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 3909
    new-instance v0, Landroid/support/design/widget/e;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/design/widget/e;-><init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    .line 3910
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    invoke-static {p2, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    move v1, v9

    .line 3911
    :cond_48
    :goto_48
    return v1

    .line 3913
    :cond_49
    iput-object v10, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    goto :goto_48

    .line 842
    :cond_4c
    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6c

    .line 844
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    neg-int v0, v0

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getDownNestedPreScrollRange()I

    move-result v2

    add-int/2addr v0, v2

    .line 847
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    if-gt v2, v0, :cond_48

    .line 863
    :cond_61
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    if-eq v2, v0, :cond_48

    .line 864
    invoke-direct {p0, p1, p2, v0}, Landroid/support/design/widget/AppBarLayout$Behavior;->c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V

    move v1, v9

    .line 865
    goto :goto_48

    .line 854
    :cond_6c
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getUpNestedPreScrollRange()I

    move-result v0

    neg-int v0, v0

    .line 856
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    if-ge v2, v0, :cond_61

    goto :goto_48
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IF)Z
    .registers 15

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 894
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_b

    .line 895
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Landroid/support/design/widget/AppBarLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 898
    :cond_b
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    if-nez v0, :cond_19

    .line 899
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 6246
    invoke-static {v0, v9}, Landroid/support/v4/widget/ca;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;

    move-result-object v0

    .line 899
    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    .line 902
    :cond_19
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    invoke-static {p4}, Ljava/lang/Math;->round(F)I

    move-result v4

    move v3, v1

    move v5, v1

    move v6, v1

    move v7, p3

    move v8, v1

    invoke-virtual/range {v0 .. v8}, Landroid/support/v4/widget/ca;->a(IIIIIIII)V

    .line 908
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->f()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 909
    new-instance v0, Landroid/support/design/widget/e;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/design/widget/e;-><init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    .line 910
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    invoke-static {p2, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 911
    const/4 v1, 0x1

    .line 914
    :goto_40
    return v1

    .line 913
    :cond_41
    iput-object v9, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    goto :goto_40
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/view/MotionEvent;)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 725
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    if-gez v1, :cond_15

    .line 726
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    .line 729
    :cond_15
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 732
    const/4 v2, 0x2

    if-ne v1, v2, :cond_21

    iget-boolean v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    if-eqz v1, :cond_21

    .line 775
    :goto_20
    return v0

    .line 736
    :cond_21
    invoke-static {p3}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v1

    packed-switch v1, :pswitch_data_72

    .line 775
    :cond_28
    :goto_28
    iget-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    goto :goto_20

    .line 738
    :pswitch_2b
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    .line 739
    if-eq v1, v3, :cond_28

    .line 743
    invoke-static {p3, v1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 744
    if-eq v1, v3, :cond_28

    .line 748
    invoke-static {p3, v1}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    float-to-int v1, v1

    .line 749
    iget v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    sub-int v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 750
    iget v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    if-le v2, v3, :cond_28

    .line 751
    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 752
    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    goto :goto_28

    .line 758
    :pswitch_4b
    iput-boolean v4, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 759
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 760
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 761
    invoke-virtual {p1, p2, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-direct {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->d()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 762
    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    .line 763
    invoke-static {p3, v4}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    goto :goto_28

    .line 770
    :pswitch_6c
    iput-boolean v4, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 771
    iput v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    goto :goto_28

    .line 736
    nop

    :pswitch_data_72
    .packed-switch 0x0
        :pswitch_4b
        :pswitch_6c
        :pswitch_2b
        :pswitch_6c
    .end packed-switch
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/view/View;I)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 663
    and-int/lit8 v2, p4, 0x2

    if-eqz v2, :cond_31

    .line 2331
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v2

    if-eqz v2, :cond_2f

    move v2, v0

    .line 663
    :goto_d
    if-eqz v2, :cond_31

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v2

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getHeight()I

    move-result v3

    if-gt v2, v3, :cond_31

    .line 667
    :goto_1e
    if-eqz v0, :cond_2b

    iget-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    if-eqz v1, :cond_2b

    .line 669
    iget-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    .line 3184
    iget-object v1, v1, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v1}, Landroid/support/design/widget/cr;->e()V

    .line 673
    :cond_2b
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->o:Ljava/lang/ref/WeakReference;

    .line 675
    return v0

    :cond_2f
    move v2, v1

    .line 2331
    goto :goto_d

    :cond_31
    move v0, v1

    .line 663
    goto :goto_1e
.end method

.method private b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
    .registers 16

    .prologue
    const/4 v2, 0x0

    .line 1002
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v4

    .line 1005
    if-eqz p4, :cond_c5

    if-lt v4, p4, :cond_c5

    if-gt v4, p5, :cond_c5

    .line 1008
    invoke-static {p3, p4, p5}, Landroid/support/design/widget/an;->a(III)I

    move-result v1

    .line 1010
    if-eq v4, v1, :cond_c5

    .line 7287
    iget-boolean v0, p2, Landroid/support/design/widget/AppBarLayout;->a:Z

    .line 1011
    if-eqz v0, :cond_a5

    .line 8052
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 8054
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v6

    move v3, v2

    :goto_1e
    if-ge v3, v6, :cond_a3

    .line 8055
    invoke-virtual {p2, v3}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 8056
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/g;

    .line 8621
    iget-object v8, v0, Landroid/support/design/widget/g;->g:Landroid/view/animation/Interpolator;

    .line 8059
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v9

    if-lt v5, v9, :cond_9e

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v9

    if-gt v5, v9, :cond_9e

    .line 8060
    if-eqz v8, :cond_a3

    .line 9597
    iget v3, v0, Landroid/support/design/widget/g;->f:I

    .line 8063
    and-int/lit8 v6, v3, 0x1

    if-eqz v6, :cond_c3

    .line 8065
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v6

    iget v9, v0, Landroid/support/design/widget/g;->topMargin:I

    add-int/2addr v6, v9

    iget v0, v0, Landroid/support/design/widget/g;->bottomMargin:I

    add-int/2addr v0, v6

    add-int/lit8 v0, v0, 0x0

    .line 8068
    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_55

    .line 8071
    invoke-static {v7}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v0, v3

    .line 8075
    :cond_55
    :goto_55
    if-lez v0, :cond_a3

    .line 8076
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v3, v5, v3

    .line 8077
    int-to-float v5, v0

    int-to-float v3, v3

    int-to-float v0, v0

    div-float v0, v3, v0

    invoke-interface {v8, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 8081
    invoke-static {v1}, Ljava/lang/Integer;->signum(I)I

    move-result v3

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v5

    add-int/2addr v0, v5

    mul-int/2addr v0, v3

    .line 9629
    :goto_75
    invoke-super {p0, v0}, Landroid/support/design/widget/df;->b(I)Z

    move-result v5

    .line 1018
    sub-int v3, v4, v1

    .line 1020
    sub-int v0, v1, v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:I

    .line 1022
    if-nez v5, :cond_be

    .line 10287
    iget-boolean v0, p2, Landroid/support/design/widget/AppBarLayout;->a:Z

    .line 1022
    if-eqz v0, :cond_be

    .line 11170
    iget-object v0, p1, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v4, v2

    .line 11172
    :goto_8c
    if-ge v4, v5, :cond_be

    .line 11173
    iget-object v0, p1, Landroid/support/design/widget/CoordinatorLayout;->h:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 11174
    if-ne v0, p2, :cond_a7

    .line 11176
    const/4 v0, 0x1

    .line 11172
    :goto_99
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_8c

    .line 8054
    :cond_9e
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1e

    :cond_a3
    move v0, v1

    .line 1011
    goto :goto_75

    :cond_a5
    move v0, v1

    goto :goto_75

    .line 11179
    :cond_a7
    if-eqz v2, :cond_bc

    .line 11180
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/w;

    .line 11281
    iget-object v6, v1, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 11183
    if-eqz v6, :cond_bc

    invoke-virtual {v1, p2}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_bc

    .line 11184
    invoke-virtual {v6, p1, v0, p2}, Landroid/support/design/widget/t;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z

    :cond_bc
    move v0, v2

    goto :goto_99

    .line 1031
    :cond_be
    invoke-direct {p0, p2}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/AppBarLayout;)V

    move v0, v3

    .line 1035
    :goto_c2
    return v0

    :cond_c3
    move v0, v2

    goto :goto_55

    :cond_c5
    move v0, v2

    goto :goto_c2
.end method

.method private b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
    .registers 10

    .prologue
    const/4 v5, 0x0

    .line 700
    if-gez p3, :cond_13

    .line 703
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getDownNestedScrollRange()I

    move-result v0

    neg-int v4, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I

    .line 706
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:Z

    .line 711
    :goto_12
    return-void

    .line 709
    :cond_13
    iput-boolean v5, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:Z

    goto :goto_12
.end method

.method private b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/view/MotionEvent;)Z
    .registers 11

    .prologue
    const/4 v6, 0x1

    const/4 v3, -0x1

    const/4 v5, 0x0

    .line 780
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    if-gez v0, :cond_15

    .line 781
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    .line 784
    :cond_15
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 785
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 787
    invoke-static {p3}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v2

    packed-switch v2, :pswitch_data_7c

    :cond_26
    :goto_26
    move v5, v6

    .line 828
    :cond_27
    return v5

    .line 789
    :pswitch_28
    invoke-virtual {p1, p2, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-direct {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->d()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 790
    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    .line 791
    invoke-static {p3, v5}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    goto :goto_26

    .line 797
    :pswitch_3d
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    invoke-static {p3, v0}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 799
    if-eq v0, v3, :cond_27

    .line 803
    invoke-static {p3, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    float-to-int v0, v0

    .line 805
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    sub-int v3, v1, v0

    .line 806
    iget-boolean v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    if-nez v1, :cond_61

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    if-le v1, v2, :cond_61

    .line 807
    iput-boolean v6, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 808
    if-lez v3, :cond_73

    .line 809
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    sub-int/2addr v3, v1

    .line 815
    :cond_61
    :goto_61
    iget-boolean v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    if-eqz v1, :cond_26

    .line 816
    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    .line 818
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getDownNestedScrollRange()I

    move-result v0

    neg-int v4, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I

    goto :goto_26

    .line 811
    :cond_73
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    add-int/2addr v3, v1

    goto :goto_61

    .line 823
    :pswitch_77
    iput-boolean v5, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 824
    iput v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    goto :goto_26

    .line 787
    :pswitch_data_7c
    .packed-switch 0x0
        :pswitch_28
        :pswitch_77
        :pswitch_3d
        :pswitch_77
    .end packed-switch
.end method

.method private c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
    .registers 6

    .prologue
    .line 874
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    if-nez v0, :cond_2c

    .line 875
    invoke-static {}, Landroid/support/design/widget/dh;->a()Landroid/support/design/widget/ck;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    .line 876
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    sget-object v1, Landroid/support/design/widget/a;->c:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/ck;->a(Landroid/view/animation/Interpolator;)V

    .line 877
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    new-instance v1, Landroid/support/design/widget/d;

    invoke-direct {v1, p0, p1, p2}, Landroid/support/design/widget/d;-><init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/ck;->a(Landroid/support/design/widget/cp;)V

    .line 888
    :goto_1b
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v1

    invoke-virtual {v0, v1, p3}, Landroid/support/design/widget/ck;->a(II)V

    .line 889
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    .line 6116
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->a()V

    .line 890
    return-void

    .line 885
    :cond_2c
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    .line 5184
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->e()V

    goto :goto_1b
.end method

.method private c(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 717
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:Z

    .line 719
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->o:Ljava/lang/ref/WeakReference;

    .line 720
    return-void
.end method

.method private d()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 987
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->o:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_20

    .line 988
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 989
    if-eqz v0, :cond_1e

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_1e

    const/4 v2, -0x1

    invoke-static {v0, v2}, Landroid/support/v4/view/cx;->b(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    .line 991
    :goto_1d
    return v0

    :cond_1e
    move v0, v1

    .line 989
    goto :goto_1d

    :cond_20
    move v0, v1

    .line 991
    goto :goto_1d
.end method

.method private d(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)Z
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 941
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z

    move-result v2

    .line 943
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getPendingAction()I

    move-result v3

    .line 944
    if-eqz v3, :cond_38

    .line 945
    and-int/lit8 v0, v3, 0x4

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    .line 946
    :goto_10
    and-int/lit8 v4, v3, 0x2

    if-eqz v4, :cond_2a

    .line 947
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getUpNestedPreScrollRange()I

    move-result v3

    neg-int v3, v3

    .line 948
    if-eqz v0, :cond_26

    .line 949
    invoke-direct {p0, p1, p2, v3}, Landroid/support/design/widget/AppBarLayout$Behavior;->c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V

    .line 6462
    :cond_1e
    :goto_1e
    iput v1, p2, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 975
    :cond_20
    :goto_20
    invoke-direct {p0, p2}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/AppBarLayout;)V

    .line 977
    return v2

    :cond_24
    move v0, v1

    .line 945
    goto :goto_10

    .line 951
    :cond_26
    invoke-virtual {p0, p1, p2, v3}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I

    goto :goto_1e

    .line 953
    :cond_2a
    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1e

    .line 954
    if-eqz v0, :cond_34

    .line 955
    invoke-direct {p0, p1, p2, v1}, Landroid/support/design/widget/AppBarLayout$Behavior;->c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V

    goto :goto_1e

    .line 957
    :cond_34
    invoke-virtual {p0, p1, p2, v1}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I

    goto :goto_1e

    .line 962
    :cond_38
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    if-ltz v0, :cond_20

    .line 963
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    invoke-virtual {p2, v0}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 964
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    neg-int v1, v1

    .line 965
    iget-boolean v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->i:Z

    if-eqz v3, :cond_57

    .line 966
    invoke-static {v0}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v1

    .line 6629
    :goto_50
    invoke-super {p0, v0}, Landroid/support/design/widget/df;->b(I)Z

    .line 971
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    goto :goto_20

    .line 968
    :cond_57
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->j:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_50
.end method


# virtual methods
.method final a()I
    .registers 3

    .prologue
    .line 1095
    .line 13629
    invoke-super {p0}, Landroid/support/design/widget/df;->c()I

    move-result v0

    .line 1095
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:I

    add-int/2addr v0, v1

    return v0
.end method

.method final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I
    .registers 10

    .prologue
    .line 996
    const/high16 v4, -0x80000000

    const v5, 0x7fffffff

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I

    move-result v0

    return v0
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
    .registers 11

    .prologue
    const/4 v0, 0x0

    .line 629
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 16100
    invoke-super {p0, p1, p2}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;

    move-result-object v2

    .line 16629
    invoke-super {p0}, Landroid/support/design/widget/df;->c()I

    move-result v4

    .line 16104
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v5

    move v3, v0

    :goto_10
    if-ge v3, v5, :cond_44

    .line 16105
    invoke-virtual {p2, v3}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 16106
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int v7, v1, v4

    .line 16108
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v1, v4

    if-gtz v1, :cond_40

    if-ltz v7, :cond_40

    .line 16109
    new-instance v1, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;

    invoke-direct {v1, v2}, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 16110
    iput v3, v1, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->a:I

    .line 16111
    invoke-static {v6}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v2

    if-ne v7, v2, :cond_33

    const/4 v0, 0x1

    :cond_33
    iput-boolean v0, v1, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->c:Z

    .line 16113
    int-to-float v0, v7

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    iput v0, v1, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->b:F

    move-object v0, v1

    .line 16114
    :goto_3f
    return-object v0

    .line 16104
    :cond_40
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_10

    :cond_44
    move-object v0, v2

    .line 629
    goto :goto_3f
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I[I)V
    .registers 12

    .prologue
    .line 629
    move-object v2, p2

    check-cast v2, Landroid/support/design/widget/AppBarLayout;

    .line 18681
    if-eqz p3, :cond_20

    iget-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:Z

    if-nez v0, :cond_20

    .line 18683
    if-gez p3, :cond_21

    .line 18685
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    neg-int v4, v0

    .line 18686
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->getDownNestedPreScrollRange()I

    move-result v0

    add-int v5, v4, v0

    .line 18692
    :goto_16
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I

    move-result v0

    aput v0, p4, v6

    .line 629
    :cond_20
    return-void

    .line 18689
    :cond_21
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->getUpNestedPreScrollRange()I

    move-result v0

    neg-int v4, v0

    .line 18690
    const/4 v5, 0x0

    goto :goto_16
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
    .registers 5

    .prologue
    .line 629
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 17125
    instance-of v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;

    if-eqz v0, :cond_1c

    .line 17126
    check-cast p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;

    .line 17127
    invoke-virtual {p3}, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, p1, p2, v0}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 17128
    iget v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->a:I

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    .line 17129
    iget v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->b:F

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->j:F

    .line 17130
    iget-boolean v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$SavedState;->c:Z

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->i:Z

    .line 17131
    :goto_1b
    return-void

    .line 17132
    :cond_1c
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 17133
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    goto :goto_1b
.end method

.method public final synthetic a(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 629
    .line 18717
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:Z

    .line 18719
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->o:Ljava/lang/ref/WeakReference;

    .line 629
    return-void
.end method

.method public final bridge synthetic a(I)Z
    .registers 3

    .prologue
    .line 629
    invoke-super {p0, p1}, Landroid/support/design/widget/df;->a(I)Z

    move-result v0

    return v0
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;FZ)Z
    .registers 16

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 629
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 17835
    if-nez p4, :cond_4e

    .line 17837
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    neg-int v7, v0

    neg-float v3, p3

    .line 17894
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_16

    .line 17895
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Landroid/support/design/widget/AppBarLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 17898
    :cond_16
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    if-nez v0, :cond_24

    .line 17899
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 18246
    invoke-static {v0, v10}, Landroid/support/v4/widget/ca;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;

    move-result-object v0

    .line 17899
    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    .line 17902
    :cond_24
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v4

    move v3, v1

    move v5, v1

    move v6, v1

    move v8, v1

    invoke-virtual/range {v0 .. v8}, Landroid/support/v4/widget/ca;->a(IIIIIIII)V

    .line 17908
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->f()Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 17909
    new-instance v0, Landroid/support/design/widget/e;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/design/widget/e;-><init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    .line 17910
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    invoke-static {p2, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    move v1, v9

    .line 17911
    :cond_4a
    :goto_4a
    return v1

    .line 17913
    :cond_4b
    iput-object v10, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Ljava/lang/Runnable;

    goto :goto_4a

    .line 17842
    :cond_4e
    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_6e

    .line 17844
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    neg-int v0, v0

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getDownNestedPreScrollRange()I

    move-result v2

    add-int/2addr v0, v2

    .line 17847
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    if-gt v2, v0, :cond_4a

    .line 17863
    :goto_63
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    if-eq v2, v0, :cond_4a

    .line 17864
    invoke-direct {p0, p1, p2, v0}, Landroid/support/design/widget/AppBarLayout$Behavior;->c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V

    move v1, v9

    .line 17865
    goto :goto_4a

    .line 17854
    :cond_6e
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getUpNestedPreScrollRange()I

    move-result v0

    neg-int v0, v0

    .line 17856
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v2

    if-lt v2, v0, :cond_4a

    goto :goto_63
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 629
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 14941
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z

    move-result v2

    .line 14943
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getPendingAction()I

    move-result v3

    .line 14944
    if-eqz v3, :cond_3a

    .line 14945
    and-int/lit8 v0, v3, 0x4

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    .line 14946
    :goto_12
    and-int/lit8 v4, v3, 0x2

    if-eqz v4, :cond_2c

    .line 14947
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getUpNestedPreScrollRange()I

    move-result v3

    neg-int v3, v3

    .line 14948
    if-eqz v0, :cond_28

    .line 14949
    invoke-direct {p0, p1, p2, v3}, Landroid/support/design/widget/AppBarLayout$Behavior;->c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V

    .line 15462
    :cond_20
    :goto_20
    iput v1, p2, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 14975
    :cond_22
    :goto_22
    invoke-direct {p0, p2}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/AppBarLayout;)V

    .line 629
    return v2

    :cond_26
    move v0, v1

    .line 14945
    goto :goto_12

    .line 14951
    :cond_28
    invoke-virtual {p0, p1, p2, v3}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I

    goto :goto_20

    .line 14953
    :cond_2c
    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_20

    .line 14954
    if-eqz v0, :cond_36

    .line 14955
    invoke-direct {p0, p1, p2, v1}, Landroid/support/design/widget/AppBarLayout$Behavior;->c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V

    goto :goto_20

    .line 14957
    :cond_36
    invoke-virtual {p0, p1, p2, v1}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I

    goto :goto_20

    .line 14962
    :cond_3a
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    if-ltz v0, :cond_22

    .line 14963
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    invoke-virtual {p2, v0}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 14964
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    neg-int v1, v1

    .line 14965
    iget-boolean v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->i:Z

    if-eqz v3, :cond_59

    .line 14966
    invoke-static {v0}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v1

    .line 15629
    :goto_52
    invoke-super {p0, v0}, Landroid/support/design/widget/df;->b(I)Z

    .line 14971
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->h:I

    goto :goto_22

    .line 14968
    :cond_59
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->j:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_52
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 11

    .prologue
    const/4 v6, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    .line 629
    move-object v2, p2

    check-cast v2, Landroid/support/design/widget/AppBarLayout;

    .line 21780
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    if-gez v0, :cond_18

    .line 21781
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    .line 21784
    :cond_18
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 21785
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 21787
    invoke-static {p3}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v3

    packed-switch v3, :pswitch_data_7e

    :cond_29
    :goto_29
    move v5, v6

    .line 21800
    :cond_2a
    return v5

    .line 21789
    :pswitch_2b
    invoke-virtual {p1, v2, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-direct {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->d()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 21790
    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    .line 21791
    invoke-static {p3, v5}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    goto :goto_29

    .line 21797
    :pswitch_40
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    invoke-static {p3, v0}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 21799
    if-eq v0, v4, :cond_2a

    .line 21803
    invoke-static {p3, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    float-to-int v0, v0

    .line 21805
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    sub-int v3, v1, v0

    .line 21806
    iget-boolean v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    if-nez v1, :cond_64

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v4, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    if-le v1, v4, :cond_64

    .line 21807
    iput-boolean v6, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 21808
    if-lez v3, :cond_75

    .line 21809
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    sub-int/2addr v3, v1

    .line 21815
    :cond_64
    :goto_64
    iget-boolean v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    if-eqz v1, :cond_29

    .line 21816
    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    .line 21818
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->getDownNestedScrollRange()I

    move-result v0

    neg-int v4, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I

    goto :goto_29

    .line 21811
    :cond_75
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    add-int/2addr v3, v1

    goto :goto_64

    .line 21823
    :pswitch_79
    iput-boolean v5, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 21824
    iput v4, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    goto :goto_29

    .line 21787
    :pswitch_data_7e
    .packed-switch 0x0
        :pswitch_2b
        :pswitch_79
        :pswitch_40
        :pswitch_79
    .end packed-switch
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;I)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 629
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 19663
    and-int/lit8 v2, p4, 0x2

    if-eqz v2, :cond_33

    .line 20331
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v2

    if-eqz v2, :cond_31

    move v2, v0

    .line 19663
    :goto_f
    if-eqz v2, :cond_33

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v2

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getHeight()I

    move-result v3

    if-gt v2, v3, :cond_33

    .line 19667
    :goto_20
    if-eqz v0, :cond_2d

    iget-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    if-eqz v1, :cond_2d

    .line 19669
    iget-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Landroid/support/design/widget/ck;

    .line 21184
    iget-object v1, v1, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v1}, Landroid/support/design/widget/cr;->e()V

    .line 19673
    :cond_2d
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->o:Ljava/lang/ref/WeakReference;

    .line 629
    return v0

    :cond_31
    move v2, v1

    .line 20331
    goto :goto_f

    :cond_33
    move v0, v1

    .line 19663
    goto :goto_20
.end method

.method public final bridge synthetic b()I
    .registers 2

    .prologue
    .line 629
    invoke-super {p0}, Landroid/support/design/widget/df;->b()I

    move-result v0

    return v0
.end method

.method public final synthetic b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)V
    .registers 10

    .prologue
    const/4 v5, 0x0

    .line 629
    move-object v2, p2

    check-cast v2, Landroid/support/design/widget/AppBarLayout;

    .line 18700
    if-gez p3, :cond_15

    .line 18703
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->getDownNestedScrollRange()I

    move-result v0

    neg-int v4, v0

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I

    .line 18706
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:Z

    :goto_14
    return-void

    .line 18709
    :cond_15
    iput-boolean v5, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:Z

    goto :goto_14
.end method

.method public final bridge synthetic b(I)Z
    .registers 3

    .prologue
    .line 629
    invoke-super {p0, p1}, Landroid/support/design/widget/df;->b(I)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 629
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 22725
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    if-gez v1, :cond_17

    .line 22726
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    .line 22729
    :cond_17
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 22732
    const/4 v2, 0x2

    if-ne v1, v2, :cond_23

    iget-boolean v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    if-eqz v1, :cond_23

    .line 22733
    :goto_22
    return v0

    .line 22736
    :cond_23
    invoke-static {p3}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v1

    packed-switch v1, :pswitch_data_74

    .line 22775
    :cond_2a
    :goto_2a
    iget-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    goto :goto_22

    .line 22738
    :pswitch_2d
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    .line 22739
    if-eq v1, v3, :cond_2a

    .line 22743
    invoke-static {p3, v1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 22744
    if-eq v1, v3, :cond_2a

    .line 22748
    invoke-static {p3, v1}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    float-to-int v1, v1

    .line 22749
    iget v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    sub-int v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 22750
    iget v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->n:I

    if-le v2, v3, :cond_2a

    .line 22751
    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 22752
    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    goto :goto_2a

    .line 22758
    :pswitch_4d
    iput-boolean v4, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 22759
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 22760
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 22761
    invoke-virtual {p1, p2, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-direct {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->d()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 22762
    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->m:I

    .line 22763
    invoke-static {p3, v4}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    goto :goto_2a

    .line 22770
    :pswitch_6e
    iput-boolean v4, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->k:Z

    .line 22771
    iput v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->l:I

    goto :goto_2a

    .line 22736
    nop

    :pswitch_data_74
    .packed-switch 0x0
        :pswitch_4d
        :pswitch_6e
        :pswitch_2d
        :pswitch_6e
    .end packed-switch
.end method

.method public final bridge synthetic c()I
    .registers 2

    .prologue
    .line 629
    invoke-super {p0}, Landroid/support/design/widget/df;->c()I

    move-result v0

    return v0
.end method
