.class public final Landroid/support/design/widget/w;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SourceFile"


# instance fields
.field a:Landroid/support/design/widget/t;

.field b:Z

.field public c:I

.field public d:I

.field public e:I

.field f:I

.field g:Landroid/view/View;

.field h:Landroid/view/View;

.field i:Z

.field j:Z

.field k:Z

.field final l:Landroid/graphics/Rect;

.field m:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    const/4 v1, -0x2

    const/4 v0, 0x0

    .line 2203
    invoke-direct {p0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 2163
    iput-boolean v0, p0, Landroid/support/design/widget/w;->b:Z

    .line 2170
    iput v0, p0, Landroid/support/design/widget/w;->c:I

    .line 2176
    iput v0, p0, Landroid/support/design/widget/w;->d:I

    .line 2183
    iput v2, p0, Landroid/support/design/widget/w;->e:I

    .line 2189
    iput v2, p0, Landroid/support/design/widget/w;->f:I

    .line 2198
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    .line 2204
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2207
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2163
    iput-boolean v2, p0, Landroid/support/design/widget/w;->b:Z

    .line 2170
    iput v2, p0, Landroid/support/design/widget/w;->c:I

    .line 2176
    iput v2, p0, Landroid/support/design/widget/w;->d:I

    .line 2183
    iput v3, p0, Landroid/support/design/widget/w;->e:I

    .line 2189
    iput v3, p0, Landroid/support/design/widget/w;->f:I

    .line 2198
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    .line 2209
    sget-object v0, Landroid/support/design/n;->CoordinatorLayout_LayoutParams:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2212
    sget v1, Landroid/support/design/n;->CoordinatorLayout_LayoutParams_android_layout_gravity:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/w;->c:I

    .line 2215
    sget v1, Landroid/support/design/n;->CoordinatorLayout_LayoutParams_layout_anchor:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/w;->f:I

    .line 2217
    sget v1, Landroid/support/design/n;->CoordinatorLayout_LayoutParams_layout_anchorGravity:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/w;->d:I

    .line 2221
    sget v1, Landroid/support/design/n;->CoordinatorLayout_LayoutParams_layout_keyline:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/w;->e:I

    .line 2224
    sget v1, Landroid/support/design/n;->CoordinatorLayout_LayoutParams_layout_behavior:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/design/widget/w;->b:Z

    .line 2226
    iget-boolean v1, p0, Landroid/support/design/widget/w;->b:Z

    if-eqz v1, :cond_54

    .line 2227
    sget v1, Landroid/support/design/n;->CoordinatorLayout_LayoutParams_layout_behavior:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)Landroid/support/design/widget/t;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 2231
    :cond_54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2232
    return-void
.end method

.method public constructor <init>(Landroid/support/design/widget/w;)V
    .registers 4

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 2235
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 2163
    iput-boolean v0, p0, Landroid/support/design/widget/w;->b:Z

    .line 2170
    iput v0, p0, Landroid/support/design/widget/w;->c:I

    .line 2176
    iput v0, p0, Landroid/support/design/widget/w;->d:I

    .line 2183
    iput v1, p0, Landroid/support/design/widget/w;->e:I

    .line 2189
    iput v1, p0, Landroid/support/design/widget/w;->f:I

    .line 2198
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    .line 2236
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 2243
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2163
    iput-boolean v0, p0, Landroid/support/design/widget/w;->b:Z

    .line 2170
    iput v0, p0, Landroid/support/design/widget/w;->c:I

    .line 2176
    iput v0, p0, Landroid/support/design/widget/w;->d:I

    .line 2183
    iput v1, p0, Landroid/support/design/widget/w;->e:I

    .line 2189
    iput v1, p0, Landroid/support/design/widget/w;->f:I

    .line 2198
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    .line 2244
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 4

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 2239
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 2163
    iput-boolean v0, p0, Landroid/support/design/widget/w;->b:Z

    .line 2170
    iput v0, p0, Landroid/support/design/widget/w;->c:I

    .line 2176
    iput v0, p0, Landroid/support/design/widget/w;->d:I

    .line 2183
    iput v1, p0, Landroid/support/design/widget/w;->e:I

    .line 2189
    iput v1, p0, Landroid/support/design/widget/w;->f:I

    .line 2198
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    .line 2240
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 2256
    iget v0, p0, Landroid/support/design/widget/w;->f:I

    return v0
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/view/View;
    .registers 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2428
    iget v0, p0, Landroid/support/design/widget/w;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_d

    .line 2429
    iput-object v2, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v2, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    move-object v0, v2

    .line 2436
    :goto_c
    return-object v0

    .line 2433
    :cond_d
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v0, :cond_1e

    .line 4494
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, p0, Landroid/support/design/widget/w;->f:I

    if-eq v0, v1, :cond_45

    move v0, v3

    .line 2433
    :goto_1c
    if-nez v0, :cond_42

    .line 5458
    :cond_1e
    iget v0, p0, Landroid/support/design/widget/w;->f:I

    invoke-virtual {p1, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 5459
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v0, :cond_80

    .line 5460
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 5461
    iget-object v1, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 5462
    :goto_32
    if-eq v1, p1, :cond_7d

    if-eqz v1, :cond_7d

    .line 5464
    if-ne v1, p2, :cond_71

    .line 5465
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_69

    .line 5466
    iput-object v2, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v2, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 2436
    :cond_42
    :goto_42
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    goto :goto_c

    .line 4498
    :cond_45
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 4499
    iget-object v1, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 4500
    :goto_4d
    if-eq v1, p1, :cond_65

    .line 4502
    if-eqz v1, :cond_53

    if-ne v1, p2, :cond_59

    .line 4503
    :cond_53
    iput-object v2, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v2, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    move v0, v3

    .line 4504
    goto :goto_1c

    .line 4506
    :cond_59
    instance-of v4, v1, Landroid/view/View;

    if-eqz v4, :cond_60

    move-object v0, v1

    .line 4507
    check-cast v0, Landroid/view/View;

    .line 4501
    :cond_60
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_4d

    .line 4510
    :cond_65
    iput-object v0, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    .line 4511
    const/4 v0, 0x1

    goto :goto_1c

    .line 5469
    :cond_69
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Anchor must not be a descendant of the anchored view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5472
    :cond_71
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_78

    move-object v0, v1

    .line 5473
    check-cast v0, Landroid/view/View;

    .line 5463
    :cond_78
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_32

    .line 5476
    :cond_7d
    iput-object v0, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    goto :goto_42

    .line 5478
    :cond_80
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_8b

    .line 5479
    iput-object v2, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v2, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    goto :goto_42

    .line 5482
    :cond_8b
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find CoordinatorLayout descendant view with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Landroid/support/design/widget/w;->f:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to anchor view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 2270
    .line 3415
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 2271
    iput p1, p0, Landroid/support/design/widget/w;->f:I

    .line 2272
    return-void
.end method

.method private a(Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 2306
    iget-object v0, p0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2307
    return-void
.end method

.method private a(Landroid/view/View;Landroid/support/design/widget/CoordinatorLayout;)V
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 2458
    iget v0, p0, Landroid/support/design/widget/w;->f:I

    invoke-virtual {p2, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 2459
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-eqz v0, :cond_3d

    .line 2460
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 2461
    iget-object v1, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2462
    :goto_15
    if-eq v1, p2, :cond_3a

    if-eqz v1, :cond_3a

    .line 2464
    if-ne v1, p1, :cond_2e

    .line 2465
    invoke-virtual {p2}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 2466
    iput-object v3, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v3, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 2480
    :goto_25
    return-void

    .line 2469
    :cond_26
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Anchor must not be a descendant of the anchored view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2472
    :cond_2e
    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_35

    move-object v0, v1

    .line 2473
    check-cast v0, Landroid/view/View;

    .line 2463
    :cond_35
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_15

    .line 2476
    :cond_3a
    iput-object v0, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    goto :goto_25

    .line 2478
    :cond_3d
    invoke-virtual {p2}, Landroid/support/design/widget/CoordinatorLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 2479
    iput-object v3, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v3, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    goto :goto_25

    .line 2482
    :cond_48
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find CoordinatorLayout descendant view with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/support/design/widget/CoordinatorLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Landroid/support/design/widget/w;->f:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to anchor view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 2376
    iput-boolean p1, p0, Landroid/support/design/widget/w;->j:Z

    .line 2377
    return-void
.end method

.method private b()Landroid/support/design/widget/t;
    .registers 2

    .prologue
    .line 2281
    iget-object v0, p0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    return-object v0
.end method

.method private b(Z)V
    .registers 2

    .prologue
    .line 2388
    iput-boolean p1, p0, Landroid/support/design/widget/w;->k:Z

    .line 2389
    return-void
.end method

.method private b(Landroid/view/View;Landroid/support/design/widget/CoordinatorLayout;)Z
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 2494
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, p0, Landroid/support/design/widget/w;->f:I

    if-eq v0, v1, :cond_d

    move v0, v2

    .line 2511
    :goto_c
    return v0

    .line 2498
    :cond_d
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 2499
    iget-object v1, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2500
    :goto_15
    if-eq v1, p2, :cond_2e

    .line 2502
    if-eqz v1, :cond_1b

    if-ne v1, p1, :cond_22

    .line 2503
    :cond_1b
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    move v0, v2

    .line 2504
    goto :goto_c

    .line 2506
    :cond_22
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_29

    move-object v0, v1

    .line 2507
    check-cast v0, Landroid/view/View;

    .line 2501
    :cond_29
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_15

    .line 2510
    :cond_2e
    iput-object v0, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    .line 2511
    const/4 v0, 0x1

    goto :goto_c
.end method

.method private c()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 2314
    iget-object v0, p0, Landroid/support/design/widget/w;->l:Landroid/graphics/Rect;

    return-object v0
.end method

.method private d()Z
    .registers 3

    .prologue
    .line 2322
    iget-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    if-nez v0, :cond_b

    iget v0, p0, Landroid/support/design/widget/w;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 2333
    iget-object v0, p0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    if-nez v0, :cond_7

    .line 2334
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/w;->i:Z

    .line 2336
    :cond_7
    iget-boolean v0, p0, Landroid/support/design/widget/w;->i:Z

    return v0
.end method

.method private f()Z
    .registers 2

    .prologue
    .line 2351
    iget-boolean v0, p0, Landroid/support/design/widget/w;->i:Z

    if-eqz v0, :cond_6

    .line 2352
    const/4 v0, 0x1

    .line 2355
    :goto_5
    return v0

    :cond_6
    iget-boolean v0, p0, Landroid/support/design/widget/w;->i:Z

    or-int/lit8 v0, v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/w;->i:Z

    goto :goto_5
.end method

.method private g()V
    .registers 2

    .prologue
    .line 2368
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/w;->i:Z

    .line 2369
    return-void
.end method

.method private h()V
    .registers 2

    .prologue
    .line 2372
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/w;->j:Z

    .line 2373
    return-void
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 2380
    iget-boolean v0, p0, Landroid/support/design/widget/w;->j:Z

    return v0
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 2384
    iget-boolean v0, p0, Landroid/support/design/widget/w;->k:Z

    return v0
.end method

.method private k()V
    .registers 2

    .prologue
    .line 2392
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/w;->k:Z

    .line 2393
    return-void
.end method

.method private l()V
    .registers 2

    .prologue
    .line 2415
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    iput-object v0, p0, Landroid/support/design/widget/w;->g:Landroid/view/View;

    .line 2416
    return-void
.end method

.method private static m()Z
    .registers 1

    .prologue
    .line 2450
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a(Landroid/support/design/widget/t;)V
    .registers 3

    .prologue
    .line 2294
    iget-object v0, p0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    if-eq v0, p1, :cond_c

    .line 2295
    iput-object p1, p0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 2296
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/w;->m:Ljava/lang/Object;

    .line 2297
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/w;->b:Z

    .line 2299
    :cond_c
    return-void
.end method

.method final a(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 2404
    iget-object v0, p0, Landroid/support/design/widget/w;->h:Landroid/view/View;

    if-eq p1, v0, :cond_10

    iget-object v0, p0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    if-eqz v0, :cond_12

    iget-object v0, p0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/t;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
