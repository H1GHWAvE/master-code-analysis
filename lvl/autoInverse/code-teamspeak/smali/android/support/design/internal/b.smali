.class public final Landroid/support/design/internal/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/x;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final j:Ljava/lang/String; = "android:menu:list"

.field private static final k:Ljava/lang/String; = "android:menu:adapter"


# instance fields
.field public a:Landroid/support/design/internal/NavigationMenuView;

.field public b:Landroid/widget/LinearLayout;

.field public c:I

.field public d:Landroid/support/design/internal/c;

.field public e:Landroid/view/LayoutInflater;

.field public f:Landroid/content/res/ColorStateList;

.field public g:Landroid/content/res/ColorStateList;

.field public h:Landroid/graphics/drawable/Drawable;

.field public i:I

.field private l:Landroid/support/v7/internal/view/menu/y;

.field private m:Landroid/support/v7/internal/view/menu/i;

.field private n:I

.field private o:Z

.field private p:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    return-void
.end method

.method static synthetic a(Landroid/support/design/internal/b;)Landroid/view/LayoutInflater;
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/design/internal/b;->e:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 256
    iput-object p1, p0, Landroid/support/design/internal/b;->h:Landroid/graphics/drawable/Drawable;

    .line 257
    return-void
.end method

.method static synthetic b(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/design/internal/b;->g:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method private b(I)Landroid/view/View;
    .registers 5
    .param p1    # I
        .annotation build Landroid/support/a/v;
        .end annotation
    .end param

    .prologue
    .line 207
    iget-object v0, p0, Landroid/support/design/internal/b;->e:Landroid/view/LayoutInflater;

    iget-object v1, p0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 208
    invoke-virtual {p0, v0}, Landroid/support/design/internal/b;->a(Landroid/view/View;)V

    .line 209
    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .registers 6
    .param p1    # Landroid/view/View;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 219
    iget-object v0, p0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 220
    iget-object v0, p0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1b

    .line 221
    iget-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    iget v1, p0, Landroid/support/design/internal/b;->i:I

    iget-object v2, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    invoke-virtual {v2}, Landroid/support/design/internal/NavigationMenuView;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/support/design/internal/NavigationMenuView;->setPadding(IIII)V

    .line 223
    :cond_1b
    return-void
.end method

.method private c(Landroid/support/v7/internal/view/menu/m;)V
    .registers 3

    .prologue
    .line 203
    iget-object v0, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    invoke-virtual {v0, p1}, Landroid/support/design/internal/c;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 204
    return-void
.end method

.method static synthetic c(Landroid/support/design/internal/b;)Z
    .registers 2

    .prologue
    .line 52
    iget-boolean v0, p0, Landroid/support/design/internal/b;->o:Z

    return v0
.end method

.method static synthetic d(Landroid/support/design/internal/b;)I
    .registers 2

    .prologue
    .line 52
    iget v0, p0, Landroid/support/design/internal/b;->n:I

    return v0
.end method

.method private d()V
    .registers 2

    .prologue
    .line 157
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/design/internal/b;->c:I

    .line 158
    return-void
.end method

.method private e()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 227
    iget-object v0, p0, Landroid/support/design/internal/b;->g:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method static synthetic e(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/design/internal/b;->f:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method private f()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Landroid/support/design/internal/b;->f:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method static synthetic f(Landroid/support/design/internal/b;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/design/internal/b;->h:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private g()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 252
    iget-object v0, p0, Landroid/support/design/internal/b;->h:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic g(Landroid/support/design/internal/b;)Landroid/support/v7/internal/view/menu/i;
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/design/internal/b;->m:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method static synthetic h(Landroid/support/design/internal/b;)I
    .registers 2

    .prologue
    .line 52
    iget v0, p0, Landroid/support/design/internal/b;->p:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 97
    iget-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    if-nez v0, :cond_3e

    .line 98
    iget-object v0, p0, Landroid/support/design/internal/b;->e:Landroid/view/LayoutInflater;

    sget v1, Landroid/support/design/k;->design_navigation_menu:I

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/internal/NavigationMenuView;

    iput-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    .line 100
    iget-object v0, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    if-nez v0, :cond_1c

    .line 101
    new-instance v0, Landroid/support/design/internal/c;

    invoke-direct {v0, p0}, Landroid/support/design/internal/c;-><init>(Landroid/support/design/internal/b;)V

    iput-object v0, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    .line 103
    :cond_1c
    iget-object v0, p0, Landroid/support/design/internal/b;->e:Landroid/view/LayoutInflater;

    sget v1, Landroid/support/design/k;->design_navigation_item_header:I

    iget-object v2, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    .line 105
    iget-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    iget-object v1, p0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/design/internal/NavigationMenuView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 106
    iget-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    iget-object v1, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    invoke-virtual {v0, v1}, Landroid/support/design/internal/NavigationMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 107
    iget-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    invoke-virtual {v0, p0}, Landroid/support/design/internal/NavigationMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 109
    :cond_3e
    iget-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    return-object v0
.end method

.method public final a(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ai;
        .end annotation
    .end param

    .prologue
    .line 246
    iput p1, p0, Landroid/support/design/internal/b;->n:I

    .line 247
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/internal/b;->o:Z

    .line 248
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/internal/b;->a(Z)V

    .line 249
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
    .registers 5

    .prologue
    .line 86
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/internal/b;->e:Landroid/view/LayoutInflater;

    .line 87
    iput-object p2, p0, Landroid/support/design/internal/b;->m:Landroid/support/v7/internal/view/menu/i;

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 89
    sget v1, Landroid/support/design/g;->design_navigation_padding_top_default:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Landroid/support/design/internal/b;->i:I

    .line 91
    sget v1, Landroid/support/design/g;->design_navigation_separator_vertical_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Landroid/support/design/internal/b;->p:I

    .line 93
    return-void
.end method

.method public final a(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 231
    iput-object p1, p0, Landroid/support/design/internal/b;->g:Landroid/content/res/ColorStateList;

    .line 232
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/internal/b;->a(Z)V

    .line 233
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 176
    check-cast p1, Landroid/os/Bundle;

    .line 177
    const-string v0, "android:menu:list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_10

    .line 179
    iget-object v1, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    invoke-virtual {v1, v0}, Landroid/support/design/internal/NavigationMenuView;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 181
    :cond_10
    const-string v0, "android:menu:adapter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_49

    .line 183
    iget-object v1, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    .line 1477
    const-string v2, "android:menu:checked"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1478
    if-eqz v2, :cond_49

    .line 1479
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/support/design/internal/c;->c:Z

    .line 1480
    iget-object v0, v1, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_44

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/internal/d;

    .line 1539
    iget-object v0, v0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    .line 1482
    if-eqz v0, :cond_2b

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getItemId()I

    move-result v4

    if-ne v4, v2, :cond_2b

    .line 1483
    invoke-virtual {v1, v0}, Landroid/support/design/internal/c;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 1487
    :cond_44
    iput-boolean v5, v1, Landroid/support/design/internal/c;->c:Z

    .line 1488
    invoke-virtual {v1}, Landroid/support/design/internal/c;->a()V

    .line 185
    :cond_49
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
    .registers 4

    .prologue
    .line 131
    iget-object v0, p0, Landroid/support/design/internal/b;->l:Landroid/support/v7/internal/view/menu/y;

    if-eqz v0, :cond_9

    .line 132
    iget-object v0, p0, Landroid/support/design/internal/b;->l:Landroid/support/v7/internal/view/menu/y;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/y;->a(Landroid/support/v7/internal/view/menu/i;Z)V

    .line 134
    :cond_9
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;)V
    .registers 2

    .prologue
    .line 121
    iput-object p1, p0, Landroid/support/design/internal/b;->l:Landroid/support/v7/internal/view/menu/y;

    .line 122
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .registers 5
    .param p1    # Landroid/view/View;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 213
    iget-object v0, p0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 215
    iget-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    iget-object v1, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    invoke-virtual {v1}, Landroid/support/design/internal/NavigationMenuView;->getPaddingBottom()I

    move-result v1

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/support/design/internal/NavigationMenuView;->setPadding(IIII)V

    .line 216
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 114
    iget-object v0, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    if-eqz v0, :cond_9

    .line 115
    iget-object v0, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    invoke-virtual {v0}, Landroid/support/design/internal/c;->notifyDataSetChanged()V

    .line 117
    :cond_9
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)Z
    .registers 3

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
    .registers 3

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 153
    iget v0, p0, Landroid/support/design/internal/b;->c:I

    return v0
.end method

.method public final b(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 241
    iput-object p1, p0, Landroid/support/design/internal/b;->f:Landroid/content/res/ColorStateList;

    .line 242
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/internal/b;->a(Z)V

    .line 243
    return-void
.end method

.method public final b(Z)V
    .registers 3

    .prologue
    .line 260
    iget-object v0, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    if-eqz v0, :cond_8

    .line 261
    iget-object v0, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    .line 3493
    iput-boolean p1, v0, Landroid/support/design/internal/c;->c:Z

    .line 263
    :cond_8
    return-void
.end method

.method public final b(Landroid/support/v7/internal/view/menu/m;)Z
    .registers 3

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Landroid/os/Parcelable;
    .registers 6

    .prologue
    .line 162
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 163
    iget-object v1, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    if-eqz v1, :cond_18

    .line 164
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 165
    iget-object v2, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    invoke-virtual {v2, v1}, Landroid/support/design/internal/NavigationMenuView;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 166
    const-string v2, "android:menu:list"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 168
    :cond_18
    iget-object v1, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    if-eqz v1, :cond_37

    .line 169
    const-string v1, "android:menu:adapter"

    iget-object v2, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    .line 1469
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1470
    iget-object v4, v2, Landroid/support/design/internal/c;->b:Landroid/support/v7/internal/view/menu/m;

    if-eqz v4, :cond_34

    .line 1471
    const-string v4, "android:menu:checked"

    iget-object v2, v2, Landroid/support/design/internal/c;->b:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/m;->getItemId()I

    move-result v2

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    :cond_34
    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 171
    :cond_37
    return-object v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 189
    iget-object v0, p0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    invoke-virtual {v0}, Landroid/support/design/internal/NavigationMenuView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p3, v0

    .line 190
    if-ltz v0, :cond_2f

    .line 191
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/design/internal/b;->b(Z)V

    .line 192
    iget-object v1, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    invoke-virtual {v1, v0}, Landroid/support/design/internal/c;->a(I)Landroid/support/design/internal/d;

    move-result-object v0

    .line 2539
    iget-object v0, v0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    .line 193
    if-eqz v0, :cond_24

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isCheckable()Z

    move-result v1

    if-eqz v1, :cond_24

    .line 194
    iget-object v1, p0, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    invoke-virtual {v1, v0}, Landroid/support/design/internal/c;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 196
    :cond_24
    iget-object v1, p0, Landroid/support/design/internal/b;->m:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1, v0, p0, v2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z

    .line 197
    invoke-virtual {p0, v2}, Landroid/support/design/internal/b;->b(Z)V

    .line 198
    invoke-virtual {p0, v2}, Landroid/support/design/internal/b;->a(Z)V

    .line 200
    :cond_2f
    return-void
.end method
