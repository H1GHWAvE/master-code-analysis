.class public final Landroid/support/design/internal/e;
.super Landroid/support/v7/internal/view/menu/ad;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/design/internal/a;Landroid/support/v7/internal/view/menu/m;)V
    .registers 4

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/internal/view/menu/ad;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/internal/view/menu/m;)V

    .line 35
    return-void
.end method

.method private l()V
    .registers 3

    .prologue
    .line 66
    .line 1065
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ad;->p:Landroid/support/v7/internal/view/menu/i;

    .line 66
    check-cast v0, Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 67
    return-void
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/menu/ad;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 47
    invoke-direct {p0}, Landroid/support/design/internal/e;->l()V

    .line 48
    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .registers 6

    .prologue
    .line 60
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/internal/view/menu/ad;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 61
    invoke-direct {p0}, Landroid/support/design/internal/e;->l()V

    .line 62
    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 6

    .prologue
    .line 53
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/internal/view/menu/ad;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 54
    invoke-direct {p0}, Landroid/support/design/internal/e;->l()V

    .line 55
    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/menu/ad;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 40
    invoke-direct {p0}, Landroid/support/design/internal/e;->l()V

    .line 41
    return-object v0
.end method
