.class public Landroid/support/design/internal/f;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Landroid/graphics/Rect;

.field private c:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/internal/f;-><init>(Landroid/content/Context;B)V

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/internal/f;-><init>(Landroid/content/Context;C)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;C)V
    .registers 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 52
    invoke-direct {p0, p1, v3, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    .line 54
    sget-object v0, Landroid/support/design/n;->ScrimInsetsFrameLayout:[I

    sget v1, Landroid/support/design/m;->Widget_Design_ScrimInsetsFrameLayout:I

    invoke-virtual {p1, v3, v0, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    sget v1, Landroid/support/design/n;->ScrimInsetsFrameLayout_insetForeground:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/design/internal/f;->setWillNotDraw(Z)V

    .line 61
    new-instance v0, Landroid/support/design/internal/g;

    invoke-direct {v0, p0}, Landroid/support/design/internal/g;-><init>(Landroid/support/design/internal/f;)V

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/bx;)V

    .line 78
    return-void
.end method

.method static synthetic a(Landroid/support/design/internal/f;)Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic a(Landroid/support/design/internal/f;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 35
    iput-object p1, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic b(Landroid/support/design/internal/f;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 10
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 82
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 84
    invoke-virtual {p0}, Landroid/support/design/internal/f;->getWidth()I

    move-result v0

    .line 85
    invoke-virtual {p0}, Landroid/support/design/internal/f;->getHeight()I

    move-result v1

    .line 86
    iget-object v2, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    if-eqz v2, :cond_93

    iget-object v2, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_93

    .line 87
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 88
    invoke-virtual {p0}, Landroid/support/design/internal/f;->getScrollX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/support/design/internal/f;->getScrollY()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 91
    iget-object v3, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v7, v7, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 92
    iget-object v3, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 93
    iget-object v3, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 96
    iget-object v3, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int v4, v1, v4

    invoke-virtual {v3, v7, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 97
    iget-object v3, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 98
    iget-object v3, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 101
    iget-object v3, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    sub-int v6, v1, v6

    invoke-virtual {v3, v7, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 102
    iget-object v3, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 103
    iget-object v3, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 106
    iget-object v3, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int v4, v0, v4

    iget-object v5, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Landroid/support/design/internal/f;->b:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v6

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 107
    iget-object v0, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/design/internal/f;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 108
    iget-object v0, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 110
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 112
    :cond_93
    return-void
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 116
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 117
    iget-object v0, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_c

    .line 118
    iget-object v0, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 120
    :cond_c
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    .prologue
    .line 124
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 125
    iget-object v0, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_d

    .line 126
    iget-object v0, p0, Landroid/support/design/internal/f;->a:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 128
    :cond_d
    return-void
.end method
