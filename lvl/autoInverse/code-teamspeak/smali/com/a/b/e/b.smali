.class public final Lcom/a/b/e/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final b:[[C


# instance fields
.field final a:[[C


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 90
    filled-new-array {v0, v0}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[C

    sput-object v0, Lcom/a/b/e/b;->b:[[C

    return-void
.end method

.method private constructor <init>([[C)V
    .registers 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/a/b/e/b;->a:[[C

    .line 65
    return-void
.end method

.method public static a(Ljava/util/Map;)Lcom/a/b/e/b;
    .registers 3

    .prologue
    .line 56
    new-instance v0, Lcom/a/b/e/b;

    invoke-static {p0}, Lcom/a/b/e/b;->b(Ljava/util/Map;)[[C

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/e/b;-><init>([[C)V

    return-object v0
.end method

.method private a()[[C
    .registers 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/b/e/b;->a:[[C

    return-object v0
.end method

.method private static b(Ljava/util/Map;)[[C
    .registers 5
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 77
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 79
    sget-object v0, Lcom/a/b/e/b;->b:[[C

    .line 86
    :goto_b
    return-object v0

    .line 81
    :cond_c
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 82
    add-int/lit8 v0, v0, 0x1

    new-array v1, v0, [[C

    .line 83
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v3

    .line 84
    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    aput-object v0, v1, v3

    goto :goto_26

    :cond_47
    move-object v0, v1

    .line 86
    goto :goto_b
.end method
