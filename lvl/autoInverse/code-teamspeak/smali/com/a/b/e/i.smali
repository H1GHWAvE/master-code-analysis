.class public final Lcom/a/b/e/i;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:Lcom/a/b/e/g;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 51
    new-instance v0, Lcom/a/b/e/j;

    invoke-direct {v0}, Lcom/a/b/e/j;-><init>()V

    sput-object v0, Lcom/a/b/e/i;->a:Lcom/a/b/e/g;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/a/b/e/l;
    .registers 2

    .prologue
    .line 78
    new-instance v0, Lcom/a/b/e/l;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/e/l;-><init>(B)V

    return-object v0
.end method

.method private static a(Lcom/a/b/e/d;)Lcom/a/b/e/p;
    .registers 2

    .prologue
    .line 229
    new-instance v0, Lcom/a/b/e/k;

    invoke-direct {v0, p0}, Lcom/a/b/e/k;-><init>(Lcom/a/b/e/d;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/e/g;)Lcom/a/b/e/p;
    .registers 5

    .prologue
    .line 183
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    instance-of v0, p0, Lcom/a/b/e/p;

    if-eqz v0, :cond_a

    .line 185
    check-cast p0, Lcom/a/b/e/p;

    .line 187
    :goto_9
    return-object p0

    .line 186
    :cond_a
    instance-of v0, p0, Lcom/a/b/e/d;

    if-eqz v0, :cond_17

    .line 187
    check-cast p0, Lcom/a/b/e/d;

    .line 1229
    new-instance v0, Lcom/a/b/e/k;

    invoke-direct {v0, p0}, Lcom/a/b/e/k;-><init>(Lcom/a/b/e/d;)V

    move-object p0, v0

    .line 187
    goto :goto_9

    .line 191
    :cond_17
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot create a UnicodeEscaper from: "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_35

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_31
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_35
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_31
.end method

.method private static a(Lcom/a/b/e/d;C)Ljava/lang/String;
    .registers 3

    .prologue
    .line 206
    invoke-virtual {p0, p1}, Lcom/a/b/e/d;->a(C)[C

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/e/i;->a([C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/a/b/e/p;I)Ljava/lang/String;
    .registers 3

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Lcom/a/b/e/p;->a(I)[C

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/e/i;->a([C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a([C)Ljava/lang/String;
    .registers 2

    .prologue
    .line 224
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([C)V

    goto :goto_3
.end method

.method private static b()Lcom/a/b/e/g;
    .registers 1

    .prologue
    .line 46
    sget-object v0, Lcom/a/b/e/i;->a:Lcom/a/b/e/g;

    return-object v0
.end method
