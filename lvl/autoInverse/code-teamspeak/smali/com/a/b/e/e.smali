.class public final Lcom/a/b/e/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private final a:Ljava/util/Map;

.field private b:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/a/b/e/e;->b:I

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/b/e/e;->a:Ljava/util/Map;

    .line 83
    return-void
.end method

.method private a(CLjava/lang/String;)Lcom/a/b/e/e;
    .registers 6

    .prologue
    .line 89
    iget-object v0, p0, Lcom/a/b/e/e;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget v0, p0, Lcom/a/b/e/e;->b:I

    if-le p1, v0, :cond_13

    .line 91
    iput p1, p0, Lcom/a/b/e/e;->b:I

    .line 93
    :cond_13
    return-object p0
.end method

.method private a([CLjava/lang/String;)Lcom/a/b/e/e;
    .registers 9

    .prologue
    .line 100
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    array-length v1, p1

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_1f

    aget-char v2, p1, v0

    .line 1089
    iget-object v3, p0, Lcom/a/b/e/e;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1090
    iget v3, p0, Lcom/a/b/e/e;->b:I

    if-le v2, v3, :cond_1c

    .line 1091
    iput v2, p0, Lcom/a/b/e/e;->b:I

    .line 101
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 104
    :cond_1f
    return-object p0
.end method

.method private a()[[C
    .registers 5

    .prologue
    .line 115
    iget v0, p0, Lcom/a/b/e/e;->b:I

    add-int/lit8 v0, v0, 0x1

    new-array v2, v0, [[C

    .line 116
    iget-object v0, p0, Lcom/a/b/e/e;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_10
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 117
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    aput-object v0, v2, v1

    goto :goto_10

    .line 119
    :cond_33
    return-object v2
.end method

.method private b()Lcom/a/b/e/g;
    .registers 3

    .prologue
    .line 129
    new-instance v0, Lcom/a/b/e/f;

    invoke-direct {p0}, Lcom/a/b/e/e;->a()[[C

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/e/f;-><init>([[C)V

    return-object v0
.end method
