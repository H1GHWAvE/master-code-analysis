.class final Lcom/a/b/f/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/f/r;


# static fields
.field private static final a:Lcom/a/b/c/an;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 53
    invoke-static {}, Lcom/a/b/c/f;->a()Lcom/a/b/c/f;

    move-result-object v0

    .line 3518
    sget-object v1, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {v0, v1}, Lcom/a/b/c/f;->a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/a/b/f/c;

    invoke-direct {v1}, Lcom/a/b/f/c;-><init>()V

    invoke-virtual {v0, v1}, Lcom/a/b/c/f;->a(Lcom/a/b/c/ab;)Lcom/a/b/c/an;

    move-result-object v0

    sput-object v0, Lcom/a/b/f/b;->a:Lcom/a/b/c/an;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    return-void
.end method

.method static synthetic a(Ljava/lang/Class;)Lcom/a/b/d/jl;
    .registers 10

    .prologue
    .line 46
    .line 3114
    invoke-static {p0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/m/ae;->b()Lcom/a/b/m/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/m/aw;->d()Ljava/util/Set;

    move-result-object v0

    .line 3115
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 3116
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_89

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 3117
    invoke-virtual {v0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_26
    if-ge v0, v4, :cond_14

    aget-object v5, v3, v0

    .line 3118
    const-class v6, Lcom/a/b/f/o;

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_86

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->isBridge()Z

    move-result v6

    if-nez v6, :cond_86

    .line 3120
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v6

    .line 3121
    array-length v7, v6

    const/4 v8, 0x1

    if-eq v7, v8, :cond_78

    .line 3122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    array-length v2, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit16 v4, v4, 0x80

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Method "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " has @Subscribe annotation, but requires "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arguments.  Event subscriber methods must require a single argument."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3127
    :cond_78
    new-instance v6, Lcom/a/b/f/d;

    invoke-direct {v6, v5}, Lcom/a/b/f/d;-><init>(Ljava/lang/reflect/Method;)V

    .line 3128
    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_86

    .line 3129
    invoke-interface {v1, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3117
    :cond_86
    add-int/lit8 v0, v0, 0x1

    goto :goto_26

    .line 3134
    :cond_89
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/reflect/Method;)Lcom/a/b/f/n;
    .registers 3

    .prologue
    .line 150
    .line 2167
    const-class v0, Lcom/a/b/f/a;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    .line 150
    :goto_9
    if-eqz v0, :cond_13

    .line 151
    new-instance v0, Lcom/a/b/f/n;

    invoke-direct {v0, p0, p1}, Lcom/a/b/f/n;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    .line 155
    :goto_10
    return-object v0

    .line 2167
    :cond_11
    const/4 v0, 0x0

    goto :goto_9

    .line 153
    :cond_13
    new-instance v0, Lcom/a/b/f/s;

    invoke-direct {v0, p0, p1}, Lcom/a/b/f/s;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    goto :goto_10
.end method

.method private static a(Ljava/lang/reflect/Method;)Z
    .registers 2

    .prologue
    .line 167
    const-class v0, Lcom/a/b/f/a;

    invoke-virtual {p0, v0}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private static b(Ljava/lang/Class;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 83
    :try_start_0
    sget-object v0, Lcom/a/b/f/b;->a:Lcom/a/b/c/an;

    invoke-interface {v0, p0}, Lcom/a/b/c/an;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jl;
    :try_end_8
    .catch Lcom/a/b/n/a/gq; {:try_start_0 .. :try_end_8} :catch_9

    return-object v0

    .line 85
    :catch_9
    move-exception v0

    invoke-virtual {v0}, Lcom/a/b/n/a/gq;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private static c(Ljava/lang/Class;)Lcom/a/b/d/jl;
    .registers 10

    .prologue
    .line 114
    invoke-static {p0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/m/ae;->b()Lcom/a/b/m/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/m/aw;->d()Ljava/util/Set;

    move-result-object v0

    .line 115
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 116
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_89

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 117
    invoke-virtual {v0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_26
    if-ge v0, v4, :cond_14

    aget-object v5, v3, v0

    .line 118
    const-class v6, Lcom/a/b/f/o;

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_86

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->isBridge()Z

    move-result v6

    if-nez v6, :cond_86

    .line 120
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v6

    .line 121
    array-length v7, v6

    const/4 v8, 0x1

    if-eq v7, v8, :cond_78

    .line 122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    array-length v2, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit16 v4, v4, 0x80

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Method "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " has @Subscribe annotation, but requires "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arguments.  Event subscriber methods must require a single argument."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_78
    new-instance v6, Lcom/a/b/f/d;

    invoke-direct {v6, v5}, Lcom/a/b/f/d;-><init>(Ljava/lang/reflect/Method;)V

    .line 128
    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_86

    .line 129
    invoke-interface {v1, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    :cond_86
    add-int/lit8 v0, v0, 0x1

    goto :goto_26

    .line 134
    :cond_89
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/a/b/d/vi;
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-static {}, Lcom/a/b/d/io;->v()Lcom/a/b/d/io;

    move-result-object v3

    .line 71
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 72
    invoke-static {v0}, Lcom/a/b/f/b;->b(Ljava/lang/Class;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_11
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    .line 73
    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    .line 74
    aget-object v5, v1, v2

    .line 1167
    const-class v1, Lcom/a/b/f/a;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    if-eqz v1, :cond_38

    const/4 v1, 0x1

    .line 1150
    :goto_2c
    if-eqz v1, :cond_3a

    .line 1151
    new-instance v1, Lcom/a/b/f/n;

    invoke-direct {v1, p1, v0}, Lcom/a/b/f/n;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    move-object v0, v1

    .line 76
    :goto_34
    invoke-interface {v3, v5, v0}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_11

    :cond_38
    move v1, v2

    .line 1167
    goto :goto_2c

    .line 1153
    :cond_3a
    new-instance v1, Lcom/a/b/f/s;

    invoke-direct {v1, p1, v0}, Lcom/a/b/f/s;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    move-object v0, v1

    goto :goto_34

    .line 78
    :cond_41
    return-object v3
.end method
