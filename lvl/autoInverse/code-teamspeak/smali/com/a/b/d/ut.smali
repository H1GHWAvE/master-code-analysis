.class final Lcom/a/b/d/ut;
.super Lcom/a/b/d/uu;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableMap;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation


# direct methods
.method constructor <init>(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)V
    .registers 3

    .prologue
    .line 1981
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/uu;-><init>(Ljava/util/SortedMap;Lcom/a/b/d/tv;)V

    .line 1982
    return-void
.end method

.method private a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .registers 3
    .param p1    # Ljava/util/Map$Entry;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2074
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/a/b/d/ut;->b:Lcom/a/b/d/tv;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/d/tv;Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_3
.end method

.method private a(Ljava/lang/Object;)Ljava/util/NavigableMap;
    .registers 3

    .prologue
    .line 2012
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/ut;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 2060
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/ut;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Ljava/util/NavigableMap;
    .registers 3

    .prologue
    .line 2064
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/ut;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/util/NavigableMap;
    .registers 2

    .prologue
    .line 2078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic c()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 1974
    .line 23078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1974
    return-object v0
.end method

.method public final ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 1985
    .line 3078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1985
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ut;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1989
    .line 4078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1989
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final descendingKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 1993
    .line 5078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1993
    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final descendingMap()Ljava/util/NavigableMap;
    .registers 3

    .prologue
    .line 1997
    .line 6078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1997
    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ut;->b:Lcom/a/b/d/tv;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final firstEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 2001
    .line 7078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2001
    invoke-interface {v0}, Ljava/util/NavigableMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ut;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 2004
    .line 8078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2004
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ut;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final floorKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 2008
    .line 9078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2008
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 2016
    .line 10078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2016
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ut;->b:Lcom/a/b/d/tv;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 1974
    .line 23012
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/ut;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    .line 1974
    return-object v0
.end method

.method public final higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 2021
    .line 11078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2021
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ut;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final higherKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 2025
    .line 12078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2025
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final lastEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 2029
    .line 13078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2029
    invoke-interface {v0}, Ljava/util/NavigableMap;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ut;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 2033
    .line 14078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2033
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ut;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final lowerKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 2037
    .line 15078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2037
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final navigableKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 2041
    .line 16078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2041
    invoke-interface {v0}, Ljava/util/NavigableMap;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final pollFirstEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 2045
    .line 17078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2045
    invoke-interface {v0}, Ljava/util/NavigableMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ut;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final pollLastEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 2049
    .line 18078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2049
    invoke-interface {v0}, Ljava/util/NavigableMap;->pollLastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ut;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 2054
    .line 19078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2054
    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ut;->b:Lcom/a/b/d/tv;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 1974
    .line 22060
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/ut;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    .line 1974
    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 2068
    .line 20078
    invoke-super {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 2068
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ut;->b:Lcom/a/b/d/tv;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 1974
    .line 21064
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/ut;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    .line 1974
    return-object v0
.end method
