.class abstract Lcom/a/b/d/rc;
.super Lcom/a/b/d/gi;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:J = 0x3L


# instance fields
.field final b:Lcom/a/b/d/sh;

.field final c:Lcom/a/b/d/sh;

.field final d:Lcom/a/b/b/au;

.field final e:Lcom/a/b/b/au;

.field final f:J

.field final g:J

.field final h:I

.field final i:I

.field final j:Lcom/a/b/d/qw;

.field transient k:Ljava/util/concurrent/ConcurrentMap;


# direct methods
.method constructor <init>(Lcom/a/b/d/sh;Lcom/a/b/d/sh;Lcom/a/b/b/au;Lcom/a/b/b/au;JJIILcom/a/b/d/qw;Ljava/util/concurrent/ConcurrentMap;)V
    .registers 14

    .prologue
    .line 3916
    invoke-direct {p0}, Lcom/a/b/d/gi;-><init>()V

    .line 3917
    iput-object p1, p0, Lcom/a/b/d/rc;->b:Lcom/a/b/d/sh;

    .line 3918
    iput-object p2, p0, Lcom/a/b/d/rc;->c:Lcom/a/b/d/sh;

    .line 3919
    iput-object p3, p0, Lcom/a/b/d/rc;->d:Lcom/a/b/b/au;

    .line 3920
    iput-object p4, p0, Lcom/a/b/d/rc;->e:Lcom/a/b/b/au;

    .line 3921
    iput-wide p5, p0, Lcom/a/b/d/rc;->f:J

    .line 3922
    iput-wide p7, p0, Lcom/a/b/d/rc;->g:J

    .line 3923
    iput p9, p0, Lcom/a/b/d/rc;->h:I

    .line 3924
    iput p10, p0, Lcom/a/b/d/rc;->i:I

    .line 3925
    iput-object p11, p0, Lcom/a/b/d/rc;->j:Lcom/a/b/d/qw;

    .line 3926
    iput-object p12, p0, Lcom/a/b/d/rc;->k:Ljava/util/concurrent/ConcurrentMap;

    .line 3927
    return-void
.end method


# virtual methods
.method final a(Ljava/io/ObjectInputStream;)Lcom/a/b/d/ql;
    .registers 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 3945
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 3946
    new-instance v2, Lcom/a/b/d/ql;

    invoke-direct {v2}, Lcom/a/b/d/ql;-><init>()V

    invoke-virtual {v2, v0}, Lcom/a/b/d/ql;->d(I)Lcom/a/b/d/ql;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/rc;->b:Lcom/a/b/d/sh;

    invoke-virtual {v0, v2}, Lcom/a/b/d/ql;->a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/rc;->c:Lcom/a/b/d/sh;

    invoke-virtual {v0, v2}, Lcom/a/b/d/ql;->b(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/rc;->d:Lcom/a/b/b/au;

    invoke-virtual {v0, v2}, Lcom/a/b/d/ql;->b(Lcom/a/b/b/au;)Lcom/a/b/d/ql;

    move-result-object v0

    iget v2, p0, Lcom/a/b/d/rc;->i:I

    invoke-virtual {v0, v2}, Lcom/a/b/d/ql;->f(I)Lcom/a/b/d/ql;

    move-result-object v2

    .line 3952
    iget-object v3, p0, Lcom/a/b/d/rc;->j:Lcom/a/b/d/qw;

    .line 4482
    iget-object v0, v2, Lcom/a/b/d/ql;->a:Lcom/a/b/d/qw;

    if-nez v0, :cond_61

    move v0, v1

    :goto_2f
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 4487
    invoke-static {v3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/qw;

    iput-object v0, v2, Lcom/a/b/d/ht;->a:Lcom/a/b/d/qw;

    .line 4488
    iput-boolean v1, v2, Lcom/a/b/d/ql;->c:Z

    .line 3953
    iget-wide v0, p0, Lcom/a/b/d/rc;->f:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_49

    .line 3954
    iget-wide v0, p0, Lcom/a/b/d/rc;->f:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Lcom/a/b/d/ql;->c(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ql;

    .line 3956
    :cond_49
    iget-wide v0, p0, Lcom/a/b/d/rc;->g:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_56

    .line 3957
    iget-wide v0, p0, Lcom/a/b/d/rc;->g:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Lcom/a/b/d/ql;->d(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/d/ql;

    .line 3959
    :cond_56
    iget v0, p0, Lcom/a/b/d/rc;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_60

    .line 3960
    iget v0, p0, Lcom/a/b/d/rc;->h:I

    invoke-virtual {v2, v0}, Lcom/a/b/d/ql;->e(I)Lcom/a/b/d/ql;

    .line 3962
    :cond_60
    return-object v2

    .line 4482
    :cond_61
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method protected final bridge synthetic a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 3896
    .line 4931
    iget-object v0, p0, Lcom/a/b/d/rc;->k:Ljava/util/concurrent/ConcurrentMap;

    .line 3896
    return-object v0
.end method

.method final a(Ljava/io/ObjectOutputStream;)V
    .registers 5

    .prologue
    .line 3935
    iget-object v0, p0, Lcom/a/b/d/rc;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 3936
    iget-object v0, p0, Lcom/a/b/d/rc;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3937
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 3938
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_13

    .line 3940
    :cond_2e
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 3941
    return-void
.end method

.method protected final b()Ljava/util/concurrent/ConcurrentMap;
    .registers 2

    .prologue
    .line 3931
    iget-object v0, p0, Lcom/a/b/d/rc;->k:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method final b(Ljava/io/ObjectInputStream;)V
    .registers 5

    .prologue
    .line 3968
    :goto_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    .line 3969
    if-eqz v0, :cond_10

    .line 3972
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    .line 3973
    iget-object v2, p0, Lcom/a/b/d/rc;->k:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3975
    :cond_10
    return-void
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3896
    .line 5931
    iget-object v0, p0, Lcom/a/b/d/rc;->k:Ljava/util/concurrent/ConcurrentMap;

    .line 3896
    return-object v0
.end method
