.class final Lcom/a/b/d/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Ljava/util/Map$Entry;

.field final synthetic b:Ljava/util/Iterator;

.field final synthetic c:Lcom/a/b/d/c;


# direct methods
.method constructor <init>(Lcom/a/b/d/c;Ljava/util/Iterator;)V
    .registers 3

    .prologue
    .line 287
    iput-object p1, p0, Lcom/a/b/d/d;->c:Lcom/a/b/d/c;

    iput-object p2, p0, Lcom/a/b/d/d;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/a/b/d/d;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/a/b/d/d;->a:Ljava/util/Map$Entry;

    .line 296
    iget-object v0, p0, Lcom/a/b/d/d;->a:Ljava/util/Map$Entry;

    .line 298
    new-instance v1, Lcom/a/b/d/e;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/e;-><init>(Lcom/a/b/d/d;Ljava/util/Map$Entry;)V

    return-object v1
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/a/b/d/d;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 287
    .line 1295
    iget-object v0, p0, Lcom/a/b/d/d;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/a/b/d/d;->a:Ljava/util/Map$Entry;

    .line 1296
    iget-object v0, p0, Lcom/a/b/d/d;->a:Ljava/util/Map$Entry;

    .line 1298
    new-instance v1, Lcom/a/b/d/e;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/e;-><init>(Lcom/a/b/d/d;Ljava/util/Map$Entry;)V

    .line 287
    return-object v1
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/a/b/d/d;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    .line 1049
    :goto_5
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 323
    iget-object v0, p0, Lcom/a/b/d/d;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 324
    iget-object v1, p0, Lcom/a/b/d/d;->b:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 325
    iget-object v1, p0, Lcom/a/b/d/d;->c:Lcom/a/b/d/c;

    iget-object v1, v1, Lcom/a/b/d/c;->b:Lcom/a/b/d/a;

    invoke-static {v1, v0}, Lcom/a/b/d/a;->b(Lcom/a/b/d/a;Ljava/lang/Object;)V

    .line 326
    return-void

    .line 322
    :cond_1d
    const/4 v0, 0x0

    goto :goto_5
.end method
