.class final Lcom/a/b/d/afd;
.super Ljava/util/AbstractMap;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/afb;


# direct methods
.method private constructor <init>(Lcom/a/b/d/afb;)V
    .registers 2

    .prologue
    .line 211
    iput-object p1, p0, Lcom/a/b/d/afd;->a:Lcom/a/b/d/afb;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/afb;B)V
    .registers 3

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lcom/a/b/d/afd;-><init>(Lcom/a/b/d/afb;)V

    return-void
.end method


# virtual methods
.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 215
    invoke-virtual {p0, p1}, Lcom/a/b/d/afd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 232
    new-instance v0, Lcom/a/b/d/afe;

    invoke-direct {v0, p0}, Lcom/a/b/d/afe;-><init>(Lcom/a/b/d/afd;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 220
    instance-of v0, p1, Lcom/a/b/d/yl;

    if-eqz v0, :cond_23

    .line 221
    check-cast p1, Lcom/a/b/d/yl;

    .line 222
    iget-object v0, p0, Lcom/a/b/d/afd;->a:Lcom/a/b/d/afb;

    invoke-static {v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 223
    if-eqz v0, :cond_23

    .line 1084
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    .line 223
    invoke-virtual {v1, p1}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 224
    invoke-virtual {v0}, Lcom/a/b/d/aff;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 227
    :goto_22
    return-object v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_22
.end method
