.class final Lcom/a/b/d/ey;
.super Lcom/a/b/d/lo;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field static final a:Lcom/a/b/d/ey;

.field private static final c:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    new-instance v0, Lcom/a/b/d/ey;

    invoke-direct {v0}, Lcom/a/b/d/ey;-><init>()V

    sput-object v0, Lcom/a/b/d/ey;->a:Lcom/a/b/d/ey;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/a/b/d/lo;-><init>()V

    return-void
.end method

.method private static d()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 93
    sget-object v0, Lcom/a/b/d/ey;->a:Lcom/a/b/d/ey;

    return-object v0
.end method


# virtual methods
.method final a([Ljava/lang/Object;I)I
    .registers 3

    .prologue
    .line 64
    return p2
.end method

.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 55
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 51
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 73
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_b

    .line 74
    check-cast p1, Ljava/util/Set;

    .line 75
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    .line 77
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final f()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 69
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method final g_()Z
    .registers 2

    .prologue
    .line 85
    const/4 v0, 0x1

    return v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1055
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 31
    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 89
    const-string v0, "[]"

    return-object v0
.end method
