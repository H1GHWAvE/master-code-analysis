.class final Lcom/a/b/d/ci;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private final transient a:Lcom/a/b/d/jl;

.field private final transient b:[I


# direct methods
.method constructor <init>(Lcom/a/b/d/jl;)V
    .registers 6

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/a/b/d/ci;->a:Lcom/a/b/d/jl;

    .line 55
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v2, v0, [I

    .line 56
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    const/4 v1, 0x1

    aput v1, v2, v0

    .line 58
    :try_start_14
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1b
    if-ltz v1, :cond_3e

    .line 59
    add-int/lit8 v0, v1, 0x1

    aget v3, v2, v0

    invoke-virtual {p1, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v3, v0}, Lcom/a/b/j/g;->b(II)I

    move-result v0

    aput v0, v2, v1
    :try_end_31
    .catch Ljava/lang/ArithmeticException; {:try_start_14 .. :try_end_31} :catch_35

    .line 58
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1b

    .line 63
    :catch_35
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cartesian product too large; must have size at most Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_3e
    iput-object v2, p0, Lcom/a/b/d/ci;->b:[I

    .line 67
    return-void
.end method

.method private a(II)I
    .registers 5

    .prologue
    .line 70
    iget-object v0, p0, Lcom/a/b/d/ci;->b:[I

    add-int/lit8 v1, p2, 0x1

    aget v0, v0, v1

    div-int v1, p1, v0

    iget-object v0, p0, Lcom/a/b/d/ci;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0, p2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rem-int v0, v1, v0

    return v0
.end method

.method static synthetic a(Lcom/a/b/d/ci;II)I
    .registers 5

    .prologue
    .line 2070
    iget-object v0, p0, Lcom/a/b/d/ci;->b:[I

    add-int/lit8 v1, p2, 0x1

    aget v0, v0, v1

    div-int v1, p1, v0

    iget-object v0, p0, Lcom/a/b/d/ci;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0, p2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rem-int v0, v1, v0

    .line 35
    return v0
.end method

.method private a(I)Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/a/b/d/ci;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 76
    new-instance v0, Lcom/a/b/d/cj;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/cj;-><init>(Lcom/a/b/d/ci;I)V

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/ci;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/a/b/d/ci;->a:Lcom/a/b/d/jl;

    return-object v0
.end method

.method static a(Ljava/util/List;)Ljava/util/List;
    .registers 5

    .prologue
    .line 41
    new-instance v1, Lcom/a/b/d/jn;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Lcom/a/b/d/jn;-><init>(I)V

    .line 43
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 44
    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 45
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_28

    .line 46
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    .line 50
    :goto_27
    return-object v0

    .line 48
    :cond_28
    invoke-virtual {v1, v0}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    goto :goto_d

    .line 50
    :cond_2c
    new-instance v0, Lcom/a/b/d/ci;

    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/ci;-><init>(Lcom/a/b/d/jl;)V

    goto :goto_27
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 104
    instance-of v0, p1, Ljava/util/List;

    if-nez v0, :cond_7

    move v0, v1

    .line 118
    :goto_6
    return v0

    .line 107
    :cond_7
    check-cast p1, Ljava/util/List;

    .line 108
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/a/b/d/ci;->a:Lcom/a/b/d/jl;

    invoke-virtual {v2}, Lcom/a/b/d/jl;->size()I

    move-result v2

    if-eq v0, v2, :cond_17

    move v0, v1

    .line 109
    goto :goto_6

    .line 111
    :cond_17
    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 112
    :cond_1b
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 113
    invoke-interface {v2}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    .line 114
    iget-object v3, p0, Lcom/a/b/d/ci;->a:Lcom/a/b/d/jl;

    invoke-virtual {v3, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    move v0, v1

    .line 115
    goto :goto_6

    .line 118
    :cond_39
    const/4 v0, 0x1

    goto :goto_6
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 34
    .line 1075
    invoke-virtual {p0}, Lcom/a/b/d/ci;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 1076
    new-instance v0, Lcom/a/b/d/cj;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/cj;-><init>(Lcom/a/b/d/ci;I)V

    .line 34
    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/a/b/d/ci;->b:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method
