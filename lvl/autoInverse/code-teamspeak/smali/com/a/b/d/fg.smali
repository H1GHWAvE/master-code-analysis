.class public final Lcom/a/b/d/fg;
.super Lcom/a/b/d/hh;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/c;
    a = "java.util.ArrayDeque"
.end annotation


# static fields
.field private static final c:J


# instance fields
.field final a:I
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field private final b:Ljava/util/Queue;


# direct methods
.method private constructor <init>(I)V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Lcom/a/b/d/hh;-><init>()V

    .line 54
    if-ltz p1, :cond_1f

    move v0, v1

    :goto_8
    const-string v3, "maxSize (%s) must >= 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 55
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0, p1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lcom/a/b/d/fg;->b:Ljava/util/Queue;

    .line 56
    iput p1, p0, Lcom/a/b/d/fg;->a:I

    .line 57
    return-void

    :cond_1f
    move v0, v2

    .line 54
    goto :goto_8
.end method

.method private static a(I)Lcom/a/b/d/fg;
    .registers 2

    .prologue
    .line 66
    new-instance v0, Lcom/a/b/d/fg;

    invoke-direct {v0, p0}, Lcom/a/b/d/fg;-><init>(I)V

    return-object v0
.end method

.method private c()I
    .registers 3

    .prologue
    .line 76
    iget v0, p0, Lcom/a/b/d/fg;->a:I

    invoke-virtual {p0}, Lcom/a/b/d/fg;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method protected final a()Ljava/util/Queue;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/a/b/d/fg;->b:Ljava/util/Queue;

    return-object v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 100
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget v0, p0, Lcom/a/b/d/fg;->a:I

    if-nez v0, :cond_9

    .line 108
    :goto_8
    return v2

    .line 104
    :cond_9
    invoke-virtual {p0}, Lcom/a/b/d/fg;->size()I

    move-result v0

    iget v1, p0, Lcom/a/b/d/fg;->a:I

    if-ne v0, v1, :cond_16

    .line 105
    iget-object v0, p0, Lcom/a/b/d/fg;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 107
    :cond_16
    iget-object v0, p0, Lcom/a/b/d/fg;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lcom/a/b/d/fg;->a(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 44
    .line 3080
    iget-object v0, p0, Lcom/a/b/d/fg;->b:Ljava/util/Queue;

    .line 44
    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 117
    .line 1080
    iget-object v0, p0, Lcom/a/b/d/fg;->b:Ljava/util/Queue;

    .line 117
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 44
    .line 4080
    iget-object v0, p0, Lcom/a/b/d/fg;->b:Ljava/util/Queue;

    .line 44
    return-object v0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/a/b/d/fg;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 122
    .line 2080
    iget-object v0, p0, Lcom/a/b/d/fg;->b:Ljava/util/Queue;

    .line 122
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
