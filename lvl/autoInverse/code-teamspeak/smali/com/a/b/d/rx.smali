.class final Lcom/a/b/d/rx;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/qy;


# direct methods
.method constructor <init>(Lcom/a/b/d/qy;)V
    .registers 2

    .prologue
    .line 3773
    iput-object p1, p0, Lcom/a/b/d/rx;->a:Lcom/a/b/d/qy;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 3802
    iget-object v0, p0, Lcom/a/b/d/rx;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->clear()V

    .line 3803
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 3792
    iget-object v0, p0, Lcom/a/b/d/rx;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0, p1}, Lcom/a/b/d/qy;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 3787
    iget-object v0, p0, Lcom/a/b/d/rx;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3777
    new-instance v0, Lcom/a/b/d/rw;

    iget-object v1, p0, Lcom/a/b/d/rx;->a:Lcom/a/b/d/qy;

    invoke-direct {v0, v1}, Lcom/a/b/d/rw;-><init>(Lcom/a/b/d/qy;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 3797
    iget-object v0, p0, Lcom/a/b/d/rx;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0, p1}, Lcom/a/b/d/qy;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 3782
    iget-object v0, p0, Lcom/a/b/d/rx;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->size()I

    move-result v0

    return v0
.end method
