.class final Lcom/a/b/d/oz;
.super Ljava/util/AbstractList;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 760
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 761
    iput-object p1, p0, Lcom/a/b/d/oz;->a:Ljava/lang/CharSequence;

    .line 762
    return-void
.end method

.method private a(I)Ljava/lang/Character;
    .registers 3

    .prologue
    .line 765
    invoke-virtual {p0}, Lcom/a/b/d/oz;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 766
    iget-object v0, p0, Lcom/a/b/d/oz;->a:Ljava/lang/CharSequence;

    invoke-interface {v0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic get(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 756
    .line 1765
    invoke-virtual {p0}, Lcom/a/b/d/oz;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 1766
    iget-object v0, p0, Lcom/a/b/d/oz;->a:Ljava/lang/CharSequence;

    invoke-interface {v0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 756
    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 770
    iget-object v0, p0, Lcom/a/b/d/oz;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method
