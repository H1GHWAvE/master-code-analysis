.class final Lcom/a/b/d/afg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/yq;


# instance fields
.field final a:Lcom/a/b/d/yl;

.field final synthetic b:Lcom/a/b/d/afb;


# direct methods
.method constructor <init>(Lcom/a/b/d/afb;Lcom/a/b/d/yl;)V
    .registers 3

    .prologue
    .line 320
    iput-object p1, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    iput-object p2, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 322
    return-void
.end method

.method private static synthetic a(Lcom/a/b/d/afg;)Lcom/a/b/d/yl;
    .registers 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/yl;
    .registers 5

    .prologue
    .line 347
    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-static {v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_3f

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 2101
    iget-object v0, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 349
    iget-object v1, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-lez v0, :cond_3f

    .line 351
    iget-object v0, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    move-object v1, v0

    .line 360
    :goto_29
    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-static {v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    iget-object v2, v2, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-interface {v0, v2}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 362
    if-nez v2, :cond_61

    .line 363
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 353
    :cond_3f
    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-static {v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    .line 354
    if-eqz v0, :cond_5b

    iget-object v1, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-ltz v1, :cond_89

    .line 355
    :cond_5b
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 364
    :cond_61
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 3101
    iget-object v0, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 364
    iget-object v3, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    iget-object v3, v3, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0, v3}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-ltz v0, :cond_7e

    .line 365
    iget-object v0, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 369
    :goto_79
    invoke-static {v1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0

    .line 367
    :cond_7e
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 4101
    iget-object v0, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    goto :goto_79

    :cond_89
    move-object v1, v0

    goto :goto_29
.end method

.method public final a(Ljava/lang/Comparable;)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 327
    iget-object v0, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    .line 1108
    invoke-virtual {v0, p1}, Lcom/a/b/d/afb;->b(Ljava/lang/Comparable;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 1109
    if-eqz v0, :cond_15

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 327
    :goto_14
    return-object v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public final a(Lcom/a/b/d/yl;)V
    .registers 4

    .prologue
    .line 397
    iget-object v0, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 398
    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    iget-object v1, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {p1, v1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;)V

    .line 400
    :cond_13
    return-void
.end method

.method public final a(Lcom/a/b/d/yl;Ljava/lang/Object;)V
    .registers 8

    .prologue
    .line 374
    iget-object v0, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    const-string v1, "Cannot put range %s into a subRangeMap(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 376
    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;Ljava/lang/Object;)V

    .line 377
    return-void
.end method

.method public final a(Lcom/a/b/d/yq;)V
    .registers 7

    .prologue
    .line 381
    invoke-interface {p1}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 388
    :cond_a
    return-void

    .line 384
    :cond_b
    invoke-interface {p1}, Lcom/a/b/d/yq;->a()Lcom/a/b/d/yl;

    move-result-object v0

    .line 385
    iget-object v1, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v1

    const-string v2, "Cannot putAll rangeMap with span %s into a subRangeMap(%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 387
    iget-object v2, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    .line 4135
    invoke-interface {p1}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_33
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 4136
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/yl;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;Ljava/lang/Object;)V

    goto :goto_33
.end method

.method public final b(Ljava/lang/Comparable;)Ljava/util/Map$Entry;
    .registers 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 336
    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-virtual {v0, p1}, Lcom/a/b/d/afb;->b(Ljava/lang/Comparable;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 337
    if-eqz v1, :cond_25

    .line 338
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v2, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {v0, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 341
    :goto_24
    return-object v0

    :cond_25
    const/4 v0, 0x0

    goto :goto_24
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 392
    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    iget-object v1, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {v0, v1}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;)V

    .line 393
    return-void
.end method

.method public final c(Lcom/a/b/d/yl;)Lcom/a/b/d/yq;
    .registers 5

    .prologue
    .line 404
    iget-object v0, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 405
    invoke-static {}, Lcom/a/b/d/afb;->e()Lcom/a/b/d/yq;

    move-result-object v0

    .line 4251
    :cond_c
    :goto_c
    return-object v0

    .line 407
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    iget-object v1, p0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {p1, v1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v2

    .line 4250
    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 4253
    new-instance v1, Lcom/a/b/d/afg;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/afg;-><init>(Lcom/a/b/d/afb;Lcom/a/b/d/yl;)V

    move-object v0, v1

    .line 407
    goto :goto_c
.end method

.method public final d()Ljava/util/Map;
    .registers 2

    .prologue
    .line 413
    new-instance v0, Lcom/a/b/d/afh;

    invoke-direct {v0, p0}, Lcom/a/b/d/afh;-><init>(Lcom/a/b/d/afg;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 418
    instance-of v0, p1, Lcom/a/b/d/yq;

    if-eqz v0, :cond_13

    .line 419
    check-cast p1, Lcom/a/b/d/yq;

    .line 420
    invoke-virtual {p0}, Lcom/a/b/d/afg;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 422
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/a/b/d/afg;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/a/b/d/afg;->d()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
