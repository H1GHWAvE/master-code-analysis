.class public final Lcom/a/b/d/nj;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field static final a:Lcom/a/b/d/agj;

.field private static final b:Ljava/util/Iterator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 72
    new-instance v0, Lcom/a/b/d/nk;

    invoke-direct {v0}, Lcom/a/b/d/nk;-><init>()V

    sput-object v0, Lcom/a/b/d/nj;->a:Lcom/a/b/d/agj;

    .line 127
    new-instance v0, Lcom/a/b/d/nq;

    invoke-direct {v0}, Lcom/a/b/d/nq;-><init>()V

    sput-object v0, Lcom/a/b/d/nj;->b:Ljava/util/Iterator;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/a/b/d/agi;
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2124
    sget-object v0, Lcom/a/b/d/nj;->a:Lcom/a/b/d/agj;

    .line 112
    return-object v0
.end method

.method private static a(Lcom/a/b/d/agi;)Lcom/a/b/d/agi;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 181
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/agi;

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;Ljava/util/Comparator;)Lcom/a/b/d/agi;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1257
    const-string v0, "iterators"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1258
    const-string v0, "comparator"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1260
    new-instance v0, Lcom/a/b/d/ny;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ny;-><init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lcom/a/b/d/agi;
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1073
    new-instance v0, Lcom/a/b/d/nn;

    invoke-direct {v0, p0}, Lcom/a/b/d/nn;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Ljava/util/Enumeration;)Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 1100
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1101
    new-instance v0, Lcom/a/b/d/no;

    invoke-direct {v0, p0}, Lcom/a/b/d/no;-><init>(Ljava/util/Enumeration;)V

    return-object v0
.end method

.method public static a(Ljava/util/Iterator;)Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 157
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    instance-of v0, p0, Lcom/a/b/d/agi;

    if-eqz v0, :cond_a

    .line 159
    check-cast p0, Lcom/a/b/d/agi;

    .line 161
    :goto_9
    return-object p0

    :cond_a
    new-instance v0, Lcom/a/b/d/nr;

    invoke-direct {v0, p0}, Lcom/a/b/d/nr;-><init>(Ljava/util/Iterator;)V

    move-object p0, v0

    goto :goto_9
.end method

.method public static a(Ljava/util/Iterator;I)Lcom/a/b/d/agi;
    .registers 3

    .prologue
    .line 586
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;IZ)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;IZ)Lcom/a/b/d/agi;
    .registers 4

    .prologue
    .line 612
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 613
    if-lez p1, :cond_f

    const/4 v0, 0x1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 614
    new-instance v0, Lcom/a/b/d/nu;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/d/nu;-><init>(Ljava/util/Iterator;IZ)V

    return-object v0

    .line 613
    :cond_f
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public static a(Ljava/util/Iterator;Ljava/lang/Class;)Lcom/a/b/d/agi;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "Class.isInstance"
    .end annotation

    .prologue
    .line 675
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/lang/Class;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a([Ljava/lang/Object;)Lcom/a/b/d/agi;
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1031
    array-length v0, p0

    invoke-static {p0, v1, v0, v1}, Lcom/a/b/d/nj;->a([Ljava/lang/Object;III)Lcom/a/b/d/agj;

    move-result-object v0

    return-object v0
.end method

.method static a([Ljava/lang/Object;III)Lcom/a/b/d/agj;
    .registers 6

    .prologue
    .line 1043
    if-ltz p2, :cond_14

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 1044
    add-int v0, p1, p2

    .line 1047
    array-length v1, p0

    invoke-static {p1, v0, v1}, Lcom/a/b/b/cn;->a(III)V

    .line 1048
    invoke-static {p3, p2}, Lcom/a/b/b/cn;->b(II)I

    .line 1049
    if-nez p2, :cond_16

    .line 3124
    sget-object v0, Lcom/a/b/d/nj;->a:Lcom/a/b/d/agj;

    .line 1058
    :goto_13
    return-object v0

    .line 1043
    :cond_14
    const/4 v0, 0x0

    goto :goto_3

    .line 1058
    :cond_16
    new-instance v0, Lcom/a/b/d/nm;

    invoke-direct {v0, p2, p3, p0, p1}, Lcom/a/b/d/nm;-><init>(II[Ljava/lang/Object;I)V

    goto :goto_13
.end method

.method private static a(Lcom/a/b/d/yi;)Lcom/a/b/d/yi;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1237
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yi;

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 849
    invoke-static {p1}, Lcom/a/b/d/nj;->a(I)V

    .line 850
    invoke-static {p0, p1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;I)I

    .line 851
    invoke-static {p0, p2}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Iterator;Lcom/a/b/b/co;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 733
    invoke-static {p0, p1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 393
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    new-instance v0, Lcom/a/b/d/ns;

    invoke-direct {v0, p0}, Lcom/a/b/d/ns;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method public static a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 795
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    new-instance v0, Lcom/a/b/d/nw;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/nw;-><init>(Ljava/util/Iterator;Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 473
    invoke-static {p0, p1, p2}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->e(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;
    .registers 5

    .prologue
    .line 493
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->e(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a([Ljava/util/Iterator;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 512
    invoke-static {p0}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->e(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method static a(I)V
    .registers 4

    .prologue
    .line 826
    if-gez p0, :cond_23

    .line 827
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "position ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") must not be negative"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 830
    :cond_23
    return-void
.end method

.method public static a(Ljava/util/Collection;Ljava/util/Iterator;)Z
    .registers 4

    .prologue
    .line 358
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    const/4 v0, 0x0

    .line 361
    :goto_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 362
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_7

    .line 364
    :cond_17
    return v0
.end method

.method public static a(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
    .registers 4

    .prologue
    .line 232
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    const/4 v0, 0x0

    .line 234
    :cond_4
    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 235
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 236
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    .line 237
    const/4 v0, 0x1

    goto :goto_4

    .line 240
    :cond_19
    return v0
.end method

.method public static a(Ljava/util/Iterator;Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 202
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/lang/Object;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/nj;->c(Ljava/util/Iterator;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/Iterator;Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 216
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/Iterator;Ljava/util/Iterator;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 269
    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 270
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_e

    .line 279
    :cond_d
    :goto_d
    return v0

    .line 273
    :cond_e
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 274
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 275
    invoke-static {v1, v2}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_d

    .line 279
    :cond_1d
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v0, 0x1

    goto :goto_d
.end method

.method public static b(Ljava/util/Iterator;)I
    .registers 3

    .prologue
    .line 190
    const/4 v0, 0x0

    .line 191
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 192
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    :cond_d
    return v0
.end method

.method public static b(Ljava/util/Iterator;I)Lcom/a/b/d/agi;
    .registers 3

    .prologue
    .line 607
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;IZ)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
    .registers 3

    .prologue
    .line 646
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 648
    new-instance v0, Lcom/a/b/d/nv;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/nv;-><init>(Ljava/util/Iterator;Lcom/a/b/b/co;)V

    return-object v0
.end method

.method public static b(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 329
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p0}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object p1

    :cond_a
    return-object p1
.end method

.method static b()Ljava/util/Iterator;
    .registers 1

    .prologue
    .line 151
    sget-object v0, Lcom/a/b/d/nj;->b:Ljava/util/Iterator;

    return-object v0
.end method

.method public static b(Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 454
    invoke-static {p0, p1}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->e(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private static varargs b([Ljava/lang/Object;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 436
    invoke-static {p0}, Lcom/a/b/d/ov;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->a(Ljava/lang/Iterable;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Iterator;Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 254
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/util/Iterator;Ljava/lang/Class;)[Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "Array.newInstance(Class, int)"
    .end annotation

    .prologue
    .line 344
    invoke-static {p0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 345
    invoke-static {v0, p1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/Iterator;Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 375
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/lang/Object;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method

.method private static c()Lcom/a/b/d/agj;
    .registers 1

    .prologue
    .line 124
    sget-object v0, Lcom/a/b/d/nj;->a:Lcom/a/b/d/agj;

    return-object v0
.end method

.method public static c(Ljava/util/Iterator;I)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 815
    invoke-static {p1}, Lcom/a/b/d/nj;->a(I)V

    .line 816
    invoke-static {p0, p1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;I)I

    move-result v0

    .line 817
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_38

    .line 818
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x5b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "position ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") must be less than the number of elements that remained ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 822
    :cond_38
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/Iterator;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 288
    sget-object v0, Lcom/a/b/d/cm;->a:Lcom/a/b/b/bv;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, p0}, Lcom/a/b/b/bv;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
    .registers 4

    .prologue
    .line 684
    invoke-static {p0, p1}, Lcom/a/b/d/nj;->g(Ljava/util/Iterator;Lcom/a/b/b/co;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public static d(Ljava/util/Iterator;I)I
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 904
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 905
    if-ltz p1, :cond_1a

    const/4 v0, 0x1

    :goto_7
    const-string v2, "numberToAdvance must be nonnegative"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 908
    :goto_c
    if-ge v1, p1, :cond_1c

    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 909
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 908
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    :cond_1a
    move v0, v1

    .line 905
    goto :goto_7

    .line 911
    :cond_1c
    return v1
.end method

.method public static d(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 302
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 303
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_b

    .line 304
    return-object v0

    .line 307
    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "expected one element but was: <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const/4 v0, 0x0

    :goto_35
    const/4 v2, 0x4

    if-ge v0, v2, :cond_69

    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_69

    .line 310
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    add-int/lit8 v0, v0, 0x1

    goto :goto_35

    .line 312
    :cond_69
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_74

    .line 313
    const-string v0, ", ..."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    :cond_74
    const/16 v0, 0x3e

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 317
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 865
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    :cond_a
    return-object p1
.end method

.method public static d(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
    .registers 3

    .prologue
    .line 694
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    :cond_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 696
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 697
    invoke-interface {p1, v0}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 698
    const/4 v0, 0x0

    .line 701
    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x1

    goto :goto_14
.end method

.method public static e(Ljava/util/Iterator;Lcom/a/b/b/co;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 717
    invoke-static {p0, p1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/agi;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 893
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p0}, Lcom/a/b/d/nj;->f(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object p1

    :cond_a
    return-object p1
.end method

.method public static e(Ljava/util/Iterator;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 531
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    new-instance v0, Lcom/a/b/d/nt;

    invoke-direct {v0, p0}, Lcom/a/b/d/nt;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method public static e(Ljava/util/Iterator;I)Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 928
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 929
    if-ltz p1, :cond_11

    const/4 v0, 0x1

    :goto_6
    const-string v1, "limit is negative"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 930
    new-instance v0, Lcom/a/b/d/nx;

    invoke-direct {v0, p1, p0}, Lcom/a/b/d/nx;-><init>(ILjava/util/Iterator;)V

    return-object v0

    .line 929
    :cond_11
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public static f(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/b/ci;
    .registers 4

    .prologue
    .line 751
    invoke-static {p0, p1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    .line 752
    invoke-virtual {v0}, Lcom/a/b/d/agi;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-virtual {v0}, Lcom/a/b/d/agi;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ci;->b(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    :goto_12
    return-object v0

    :cond_13
    invoke-static {}, Lcom/a/b/b/ci;->f()Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_12
.end method

.method public static f(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 876
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 877
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 878
    return-object v0
.end method

.method public static g(Ljava/util/Iterator;Lcom/a/b/b/co;)I
    .registers 4

    .prologue
    .line 775
    const-string v0, "predicate"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 776
    const/4 v0, 0x0

    :goto_6
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 777
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 778
    invoke-interface {p1, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 782
    :goto_16
    return v0

    .line 776
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 782
    :cond_1a
    const/4 v0, -0x1

    goto :goto_16
.end method

.method public static g(Ljava/util/Iterator;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 968
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 969
    new-instance v0, Lcom/a/b/d/nl;

    invoke-direct {v0, p0}, Lcom/a/b/d/nl;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method static h(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 995
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 996
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 997
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    .line 1000
    :goto_d
    return-object v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method static i(Ljava/util/Iterator;)V
    .registers 2

    .prologue
    .line 1010
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011
    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1012
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 1013
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 1015
    :cond_10
    return-void
.end method

.method public static j(Ljava/util/Iterator;)Lcom/a/b/d/yi;
    .registers 2

    .prologue
    .line 1219
    instance-of v0, p0, Lcom/a/b/d/oa;

    if-eqz v0, :cond_7

    .line 1223
    check-cast p0, Lcom/a/b/d/oa;

    .line 1226
    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Lcom/a/b/d/oa;

    invoke-direct {v0, p0}, Lcom/a/b/d/oa;-><init>(Ljava/util/Iterator;)V

    move-object p0, v0

    goto :goto_6
.end method

.method static k(Ljava/util/Iterator;)Ljava/util/ListIterator;
    .registers 1

    .prologue
    .line 1316
    check-cast p0, Ljava/util/ListIterator;

    return-object p0
.end method

.method private static l(Ljava/util/Iterator;)Ljava/util/Enumeration;
    .registers 2

    .prologue
    .line 1121
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1122
    new-instance v0, Lcom/a/b/d/np;

    invoke-direct {v0, p0}, Lcom/a/b/d/np;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method
