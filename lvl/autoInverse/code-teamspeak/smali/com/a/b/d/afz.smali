.class final Lcom/a/b/d/afz;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Iterator;

.field final synthetic b:Lcom/a/b/d/afx;


# direct methods
.method constructor <init>(Lcom/a/b/d/afx;Ljava/util/Iterator;)V
    .registers 3

    .prologue
    .line 757
    iput-object p1, p0, Lcom/a/b/d/afz;->b:Lcom/a/b/d/afx;

    iput-object p2, p0, Lcom/a/b/d/afz;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    return-void
.end method

.method private c()Ljava/util/Map$Entry;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 760
    iget-object v0, p0, Lcom/a/b/d/afz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_e

    .line 761
    invoke-virtual {p0}, Lcom/a/b/d/afz;->b()Ljava/lang/Object;

    move-object v0, v1

    .line 771
    :goto_d
    return-object v0

    .line 763
    :cond_e
    iget-object v0, p0, Lcom/a/b/d/afz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 764
    iget-object v2, p0, Lcom/a/b/d/afz;->b:Lcom/a/b/d/afx;

    invoke-static {v2}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/afx;)Lcom/a/b/d/yl;

    move-result-object v2

    iget-object v2, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v3, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v2, v3}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v2

    if-ltz v2, :cond_2b

    .line 765
    invoke-virtual {p0}, Lcom/a/b/d/afz;->b()Ljava/lang/Object;

    move-object v0, v1

    goto :goto_d

    .line 767
    :cond_2b
    iget-object v2, p0, Lcom/a/b/d/afz;->b:Lcom/a/b/d/afx;

    invoke-static {v2}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/afx;)Lcom/a/b/d/yl;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 768
    iget-object v2, p0, Lcom/a/b/d/afz;->b:Lcom/a/b/d/afx;

    invoke-static {v2}, Lcom/a/b/d/afx;->b(Lcom/a/b/d/afx;)Lcom/a/b/d/yl;

    move-result-object v2

    iget-object v3, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v2, v3}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v2

    if-eqz v2, :cond_4a

    .line 769
    iget-object v1, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_d

    .line 771
    :cond_4a
    invoke-virtual {p0}, Lcom/a/b/d/afz;->b()Ljava/lang/Object;

    move-object v0, v1

    goto :goto_d
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 757
    .line 1760
    iget-object v0, p0, Lcom/a/b/d/afz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1761
    invoke-virtual {p0}, Lcom/a/b/d/afz;->b()Ljava/lang/Object;

    move-object v0, v1

    .line 1769
    :goto_d
    return-object v0

    .line 1763
    :cond_e
    iget-object v0, p0, Lcom/a/b/d/afz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 1764
    iget-object v2, p0, Lcom/a/b/d/afz;->b:Lcom/a/b/d/afx;

    invoke-static {v2}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/afx;)Lcom/a/b/d/yl;

    move-result-object v2

    iget-object v2, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v3, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v2, v3}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v2

    if-ltz v2, :cond_2b

    .line 1765
    invoke-virtual {p0}, Lcom/a/b/d/afz;->b()Ljava/lang/Object;

    move-object v0, v1

    goto :goto_d

    .line 1767
    :cond_2b
    iget-object v2, p0, Lcom/a/b/d/afz;->b:Lcom/a/b/d/afx;

    invoke-static {v2}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/afx;)Lcom/a/b/d/yl;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 1768
    iget-object v2, p0, Lcom/a/b/d/afz;->b:Lcom/a/b/d/afx;

    invoke-static {v2}, Lcom/a/b/d/afx;->b(Lcom/a/b/d/afx;)Lcom/a/b/d/yl;

    move-result-object v2

    iget-object v3, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v2, v3}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v2

    if-eqz v2, :cond_4a

    .line 1769
    iget-object v1, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_d

    .line 1771
    :cond_4a
    invoke-virtual {p0}, Lcom/a/b/d/afz;->b()Ljava/lang/Object;

    move-object v0, v1

    .line 757
    goto :goto_d
.end method
