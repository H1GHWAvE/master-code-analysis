.class final Lcom/a/b/d/fn;
.super Lcom/a/b/d/vb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/fj;


# direct methods
.method constructor <init>(Lcom/a/b/d/fj;Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 286
    iput-object p1, p0, Lcom/a/b/d/fn;->a:Lcom/a/b/d/fj;

    invoke-direct {p0, p2}, Lcom/a/b/d/vb;-><init>(Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final remove(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 289
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_60

    .line 290
    check-cast p1, Ljava/util/Collection;

    .line 291
    iget-object v0, p0, Lcom/a/b/d/fn;->a:Lcom/a/b/d/fj;

    iget-object v0, v0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    iget-object v0, v0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 293
    :cond_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_60

    .line 294
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 295
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 296
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    new-instance v4, Lcom/a/b/d/fr;

    iget-object v5, p0, Lcom/a/b/d/fn;->a:Lcom/a/b/d/fj;

    iget-object v5, v5, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-direct {v4, v5, v3}, Lcom/a/b/d/fr;-><init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V

    invoke-static {v1, v4}, Lcom/a/b/d/fi;->a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;

    move-result-object v1

    .line 298
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_18

    invoke-interface {p1, v1}, Ljava/util/Collection;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 299
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v3, v0, :cond_5c

    .line 300
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 304
    :goto_5a
    const/4 v0, 0x1

    .line 308
    :goto_5b
    return v0

    .line 302
    :cond_5c
    invoke-interface {v1}, Ljava/util/Collection;->clear()V

    goto :goto_5a

    .line 308
    :cond_60
    const/4 v0, 0x0

    goto :goto_5b
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 313
    iget-object v0, p0, Lcom/a/b/d/fn;->a:Lcom/a/b/d/fj;

    iget-object v0, v0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/sz;->b(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/fi;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 318
    iget-object v0, p0, Lcom/a/b/d/fn;->a:Lcom/a/b/d/fj;

    iget-object v0, v0, Lcom/a/b/d/fj;->a:Lcom/a/b/d/fi;

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/sz;->b(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/fi;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method
