.class final Lcom/a/b/d/x;
.super Lcom/a/b/d/aa;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableSet"
.end annotation


# instance fields
.field final synthetic b:Lcom/a/b/d/n;


# direct methods
.method constructor <init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
    .registers 3

    .prologue
    .line 1024
    iput-object p1, p0, Lcom/a/b/d/x;->b:Lcom/a/b/d/n;

    .line 1025
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/aa;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    .line 1026
    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/util/NavigableSet;
    .registers 3

    .prologue
    .line 1075
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/x;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/NavigableSet;
    .registers 5

    .prologue
    .line 1085
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/x;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Ljava/util/NavigableSet;
    .registers 3

    .prologue
    .line 1097
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/x;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method private c()Ljava/util/NavigableMap;
    .registers 2

    .prologue
    .line 1030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 1022
    .line 12030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1022
    return-object v0
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1045
    .line 4030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1045
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1070
    invoke-virtual {p0}, Lcom/a/b/d/x;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 1065
    new-instance v1, Lcom/a/b/d/x;

    iget-object v2, p0, Lcom/a/b/d/x;->b:Lcom/a/b/d/n;

    .line 6030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1065
    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/x;-><init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V

    return-object v1
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1040
    .line 3030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1040
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 1080
    new-instance v1, Lcom/a/b/d/x;

    iget-object v2, p0, Lcom/a/b/d/x;->b:Lcom/a/b/d/n;

    .line 7030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1080
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/x;-><init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V

    return-object v1
.end method

.method public final bridge synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1022
    .line 11075
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/x;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1022
    return-object v0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1050
    .line 5030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1050
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1035
    .line 2030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1035
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1055
    invoke-virtual {p0}, Lcom/a/b/d/x;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->h(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1060
    invoke-virtual {p0}, Lcom/a/b/d/x;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->h(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 8

    .prologue
    .line 1091
    new-instance v1, Lcom/a/b/d/x;

    iget-object v2, p0, Lcom/a/b/d/x;->b:Lcom/a/b/d/n;

    .line 8030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1091
    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/x;-><init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V

    return-object v1
.end method

.method public final bridge synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 1022
    .line 10085
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/x;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1022
    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 1102
    new-instance v1, Lcom/a/b/d/x;

    iget-object v2, p0, Lcom/a/b/d/x;->b:Lcom/a/b/d/n;

    .line 9030
    invoke-super {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1102
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/x;-><init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V

    return-object v1
.end method

.method public final bridge synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1022
    .line 9097
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/x;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1022
    return-object v0
.end method
