.class abstract Lcom/a/b/d/eg;
.super Lcom/a/b/d/jt;
.source "SourceFile"


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(I)V
    .registers 2

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/a/b/d/jt;-><init>()V

    .line 91
    iput p1, p0, Lcom/a/b/d/eg;->a:I

    .line 92
    return-void
.end method

.method private b(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/a/b/d/eg;->a()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private i()Z
    .registers 3

    .prologue
    .line 98
    iget v0, p0, Lcom/a/b/d/eg;->a:I

    invoke-virtual {p0}, Lcom/a/b/d/eg;->a()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/jt;->size()I

    move-result v1

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method abstract a()Lcom/a/b/d/jt;
.end method

.method abstract a(I)Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method final c()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 109
    .line 1098
    iget v0, p0, Lcom/a/b/d/eg;->a:I

    invoke-virtual {p0}, Lcom/a/b/d/eg;->a()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/jt;->size()I

    move-result v1

    if-ne v0, v1, :cond_18

    const/4 v0, 0x1

    .line 109
    :goto_d
    if-eqz v0, :cond_1a

    invoke-virtual {p0}, Lcom/a/b/d/eg;->a()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    :goto_17
    return-object v0

    .line 1098
    :cond_18
    const/4 v0, 0x0

    goto :goto_d

    .line 109
    :cond_1a
    invoke-super {p0}, Lcom/a/b/d/jt;->c()Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_17
.end method

.method final d()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 125
    new-instance v0, Lcom/a/b/d/eh;

    invoke-direct {v0, p0}, Lcom/a/b/d/eh;-><init>(Lcom/a/b/d/eg;)V

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/a/b/d/eg;->a()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 120
    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return-object v0

    :cond_e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/eg;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_d
.end method

.method public size()I
    .registers 2

    .prologue
    .line 114
    iget v0, p0, Lcom/a/b/d/eg;->a:I

    return v0
.end method
