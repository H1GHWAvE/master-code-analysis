.class public abstract Lcom/a/b/d/gr;
.super Lcom/a/b/d/gx;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ou;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/a/b/d/gx;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/a/b/d/ou;
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/a/b/d/gr;->a()Lcom/a/b/d/ou;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/ou;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 4

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/a/b/d/gr;->a()Lcom/a/b/d/ou;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/ou;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 34
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/gr;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/a/b/d/gr;->a()Lcom/a/b/d/ou;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/ou;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic c()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/gr;->a()Lcom/a/b/d/ou;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/a/b/d/gr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/a/b/d/gr;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/gr;->a()Lcom/a/b/d/ou;

    move-result-object v0

    return-object v0
.end method
