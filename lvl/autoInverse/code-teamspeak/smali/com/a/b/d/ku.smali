.class public abstract Lcom/a/b/d/ku;
.super Lcom/a/b/d/iz;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/xc;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final a:Lcom/a/b/d/ku;


# instance fields
.field private transient b:Lcom/a/b/d/lo;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 55
    new-instance v0, Lcom/a/b/d/zj;

    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zj;-><init>(Lcom/a/b/d/jt;I)V

    sput-object v0, Lcom/a/b/d/ku;->a:Lcom/a/b/d/ku;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/a/b/d/iz;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/d/xc;)Lcom/a/b/d/ku;
    .registers 2

    .prologue
    .line 195
    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/util/Collection;)Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
    .registers 3

    .prologue
    .line 174
    instance-of v0, p0, Lcom/a/b/d/ku;

    if-eqz v0, :cond_e

    move-object v0, p0

    .line 176
    check-cast v0, Lcom/a/b/d/ku;

    .line 177
    invoke-virtual {v0}, Lcom/a/b/d/ku;->h_()Z

    move-result v1

    if-nez v1, :cond_e

    .line 186
    :goto_d
    return-object v0

    .line 182
    :cond_e
    instance-of v0, p0, Lcom/a/b/d/xc;

    if-eqz v0, :cond_1b

    invoke-static {p0}, Lcom/a/b/d/xe;->b(Ljava/lang/Iterable;)Lcom/a/b/d/xc;

    move-result-object v0

    .line 186
    :goto_16
    invoke-static {v0}, Lcom/a/b/d/ku;->a(Lcom/a/b/d/xc;)Lcom/a/b/d/ku;

    move-result-object v0

    goto :goto_d

    .line 6076
    :cond_1b
    invoke-static {p0}, Lcom/a/b/d/xe;->a(Ljava/lang/Iterable;)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/oi;->a(I)Lcom/a/b/d/oi;

    move-result-object v0

    .line 6078
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_16
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ku;
    .registers 4

    .prologue
    .line 85
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 2190
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    .line 85
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ku;
    .registers 5

    .prologue
    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    .line 3190
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    .line 96
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ku;
    .registers 6

    .prologue
    .line 107
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    .line 4190
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    .line 107
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ku;
    .registers 7

    .prologue
    .line 118
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    .line 5190
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    .line 118
    return-object v0
.end method

.method private static varargs a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/ku;
    .registers 8

    .prologue
    .line 130
    new-instance v0, Lcom/a/b/d/kw;

    invoke-direct {v0}, Lcom/a/b/d/kw;-><init>()V

    invoke-virtual {v0, p0}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/a/b/d/kw;->b([Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/kw;->b()Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Collection;)Lcom/a/b/d/ku;
    .registers 10

    .prologue
    const-wide/16 v4, 0x0

    .line 200
    .line 201
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v6

    .line 202
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v4

    :goto_b
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 203
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v1

    .line 204
    if-lez v1, :cond_41

    .line 207
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v0, v8}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 208
    int-to-long v0, v1

    add-long/2addr v0, v2

    :goto_2a
    move-wide v2, v0

    .line 210
    goto :goto_b

    .line 212
    :cond_2c
    cmp-long v0, v2, v4

    if-nez v0, :cond_33

    .line 7063
    sget-object v0, Lcom/a/b/d/ku;->a:Lcom/a/b/d/ku;

    .line 215
    :goto_32
    return-object v0

    :cond_33
    new-instance v0, Lcom/a/b/d/zj;

    invoke-virtual {v6}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-static {v2, v3}, Lcom/a/b/l/q;->b(J)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zj;-><init>(Lcom/a/b/d/jt;I)V

    goto :goto_32

    :cond_41
    move-wide v0, v2

    goto :goto_2a
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/d/ku;
    .registers 2

    .prologue
    .line 8052
    new-instance v0, Lcom/a/b/d/oi;

    invoke-direct {v0}, Lcom/a/b/d/oi;-><init>()V

    .line 232
    invoke-static {v0, p0}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 233
    invoke-static {v0}, Lcom/a/b/d/ku;->a(Lcom/a/b/d/xc;)Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method

.method private static a([Ljava/lang/Object;)Lcom/a/b/d/ku;
    .registers 2

    .prologue
    .line 152
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method

.method private static b()Lcom/a/b/d/ku;
    .registers 1

    .prologue
    .line 63
    sget-object v0, Lcom/a/b/d/ku;->a:Lcom/a/b/d/ku;

    return-object v0
.end method

.method private static b(Ljava/lang/Object;)Lcom/a/b/d/ku;
    .registers 3

    .prologue
    .line 74
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 1190
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    .line 74
    return-object v0
.end method

.method private static varargs b([Ljava/lang/Object;)Lcom/a/b/d/ku;
    .registers 2

    .prologue
    .line 190
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method

.method private final e()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/a/b/d/ku;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/kx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/kx;-><init>(Lcom/a/b/d/ku;B)V

    goto :goto_a
.end method

.method private static p()Lcom/a/b/d/kw;
    .registers 1

    .prologue
    .line 466
    new-instance v0, Lcom/a/b/d/kw;

    invoke-direct {v0}, Lcom/a/b/d/kw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;I)I
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 281
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method a([Ljava/lang/Object;I)I
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "not present in emulated superclass"
    .end annotation

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/a/b/d/ku;->o()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 324
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v2

    add-int/2addr v2, p2

    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-static {p1, p2, v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 325
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    add-int/2addr p2, v0

    .line 326
    goto :goto_8

    .line 327
    :cond_26
    return p2
.end method

.method abstract a(I)Lcom/a/b/d/xd;
.end method

.method public final synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/a/b/d/ku;->o()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .registers 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 317
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 293
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 305
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()Lcom/a/b/d/agi;
    .registers 3

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/a/b/d/ku;->o()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->c()Lcom/a/b/d/agi;

    move-result-object v0

    .line 240
    new-instance v1, Lcom/a/b/d/kv;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/kv;-><init>(Lcom/a/b/d/ku;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 264
    invoke-virtual {p0, p1}, Lcom/a/b/d/ku;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/a/b/d/ku;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 331
    invoke-static {p0, p1}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/xc;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method g()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 458
    new-instance v0, Lcom/a/b/d/la;

    invoke-direct {v0, p0}, Lcom/a/b/d/la;-><init>(Lcom/a/b/d/xc;)V

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/a/b/d/ku;->o()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/aad;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/a/b/d/ku;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 346
    iget-object v0, p0, Lcom/a/b/d/ku;->b:Lcom/a/b/d/lo;

    .line 347
    if-nez v0, :cond_10

    .line 8351
    invoke-virtual {p0}, Lcom/a/b/d/ku;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    .line 347
    :goto_e
    iput-object v0, p0, Lcom/a/b/d/ku;->b:Lcom/a/b/d/lo;

    :cond_10
    return-object v0

    .line 8351
    :cond_11
    new-instance v0, Lcom/a/b/d/kx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/kx;-><init>(Lcom/a/b/d/ku;B)V

    goto :goto_e
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/a/b/d/ku;->o()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
