.class public Lcom/a/b/d/kw;
.super Lcom/a/b/d/jb;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/xc;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 495
    invoke-static {}, Lcom/a/b/d/oi;->g()Lcom/a/b/d/oi;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/kw;-><init>(Lcom/a/b/d/xc;)V

    .line 496
    return-void
.end method

.method constructor <init>(Lcom/a/b/d/xc;)V
    .registers 2

    .prologue
    .line 498
    invoke-direct {p0}, Lcom/a/b/d/jb;-><init>()V

    .line 499
    iput-object p1, p0, Lcom/a/b/d/kw;->a:Lcom/a/b/d/xc;

    .line 500
    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 487
    invoke-virtual {p0}, Lcom/a/b/d/kw;->b()Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 487
    invoke-virtual {p0, p1}, Lcom/a/b/d/kw;->b(Ljava/lang/Iterable;)Lcom/a/b/d/kw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/util/Iterator;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 487
    invoke-virtual {p0, p1}, Lcom/a/b/d/kw;->b(Ljava/util/Iterator;)Lcom/a/b/d/kw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 487
    invoke-virtual {p0, p1}, Lcom/a/b/d/kw;->b([Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lcom/a/b/d/kw;
    .registers 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/a/b/d/kw;->a:Lcom/a/b/d/xc;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/d/xc;->add(Ljava/lang/Object;)Z

    .line 511
    return-object p0
.end method

.method public a(Ljava/lang/Object;I)Lcom/a/b/d/kw;
    .registers 5

    .prologue
    .line 528
    iget-object v0, p0, Lcom/a/b/d/kw;->a:Lcom/a/b/d/xc;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;I)I

    .line 529
    return-object p0
.end method

.method public synthetic b(Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 487
    invoke-virtual {p0, p1}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/a/b/d/ku;
    .registers 2

    .prologue
    .line 599
    iget-object v0, p0, Lcom/a/b/d/kw;->a:Lcom/a/b/d/xc;

    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Iterable;)Lcom/a/b/d/kw;
    .registers 5

    .prologue
    .line 570
    instance-of v0, p1, Lcom/a/b/d/xc;

    if-eqz v0, :cond_28

    .line 571
    invoke-static {p1}, Lcom/a/b/d/xe;->b(Ljava/lang/Iterable;)Lcom/a/b/d/xc;

    move-result-object v0

    .line 572
    invoke-interface {v0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 573
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;I)Lcom/a/b/d/kw;

    goto :goto_10

    .line 576
    :cond_28
    invoke-super {p0, p1}, Lcom/a/b/d/jb;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;

    .line 578
    :cond_2b
    return-object p0
.end method

.method public b(Ljava/lang/Object;I)Lcom/a/b/d/kw;
    .registers 5

    .prologue
    .line 543
    iget-object v0, p0, Lcom/a/b/d/kw;->a:Lcom/a/b/d/xc;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/a/b/d/xc;->c(Ljava/lang/Object;I)I

    .line 544
    return-object p0
.end method

.method public b(Ljava/util/Iterator;)Lcom/a/b/d/kw;
    .registers 2

    .prologue
    .line 590
    invoke-super {p0, p1}, Lcom/a/b/d/jb;->a(Ljava/util/Iterator;)Lcom/a/b/d/jb;

    .line 591
    return-object p0
.end method

.method public varargs b([Ljava/lang/Object;)Lcom/a/b/d/kw;
    .registers 2

    .prologue
    .line 556
    invoke-super {p0, p1}, Lcom/a/b/d/jb;->a([Ljava/lang/Object;)Lcom/a/b/d/jb;

    .line 557
    return-object p0
.end method
