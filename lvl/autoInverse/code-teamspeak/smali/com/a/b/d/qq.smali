.class abstract enum Lcom/a/b/d/qq;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/d/qq;

.field public static final enum b:Lcom/a/b/d/qq;

.field public static final enum c:Lcom/a/b/d/qq;

.field public static final enum d:Lcom/a/b/d/qq;

.field public static final enum e:Lcom/a/b/d/qq;

.field private static final synthetic f:[Lcom/a/b/d/qq;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 690
    new-instance v0, Lcom/a/b/d/qr;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1}, Lcom/a/b/d/qr;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/qq;->a:Lcom/a/b/d/qq;

    .line 703
    new-instance v0, Lcom/a/b/d/qs;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1}, Lcom/a/b/d/qs;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/qq;->b:Lcom/a/b/d/qq;

    .line 714
    new-instance v0, Lcom/a/b/d/qt;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1}, Lcom/a/b/d/qt;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    .line 725
    new-instance v0, Lcom/a/b/d/qu;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1}, Lcom/a/b/d/qu;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/qq;->d:Lcom/a/b/d/qq;

    .line 736
    new-instance v0, Lcom/a/b/d/qv;

    const-string v1, "SIZE"

    invoke-direct {v0, v1}, Lcom/a/b/d/qv;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/qq;->e:Lcom/a/b/d/qq;

    .line 685
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/a/b/d/qq;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/d/qq;->a:Lcom/a/b/d/qq;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/d/qq;->b:Lcom/a/b/d/qq;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/b/d/qq;->c:Lcom/a/b/d/qq;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/a/b/d/qq;->d:Lcom/a/b/d/qq;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/a/b/d/qq;->e:Lcom/a/b/d/qq;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/d/qq;->f:[Lcom/a/b/d/qq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 685
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 685
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/qq;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/d/qq;
    .registers 2

    .prologue
    .line 685
    const-class v0, Lcom/a/b/d/qq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/qq;

    return-object v0
.end method

.method public static values()[Lcom/a/b/d/qq;
    .registers 1

    .prologue
    .line 685
    sget-object v0, Lcom/a/b/d/qq;->f:[Lcom/a/b/d/qq;

    invoke-virtual {v0}, [Lcom/a/b/d/qq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/d/qq;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
