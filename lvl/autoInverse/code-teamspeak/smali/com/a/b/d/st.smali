.class Lcom/a/b/d/st;
.super Ljava/lang/ref/WeakReference;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/rz;


# instance fields
.field final a:I

.field final b:Lcom/a/b/d/rz;

.field volatile c:Lcom/a/b/d/sr;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/a/b/d/rz;)V
    .registers 6
    .param p4    # Lcom/a/b/d/rz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1430
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1498
    invoke-static {}, Lcom/a/b/d/qy;->g()Lcom/a/b/d/sr;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/st;->c:Lcom/a/b/d/sr;

    .line 1431
    iput p3, p0, Lcom/a/b/d/st;->a:I

    .line 1432
    iput-object p4, p0, Lcom/a/b/d/st;->b:Lcom/a/b/d/rz;

    .line 1433
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/d/sr;
    .registers 2

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/a/b/d/st;->c:Lcom/a/b/d/sr;

    return-object v0
.end method

.method public a(J)V
    .registers 4

    .prologue
    .line 1449
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 1459
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/a/b/d/sr;)V
    .registers 3

    .prologue
    .line 1507
    iget-object v0, p0, Lcom/a/b/d/st;->c:Lcom/a/b/d/sr;

    .line 1508
    iput-object p1, p0, Lcom/a/b/d/st;->c:Lcom/a/b/d/sr;

    .line 1509
    invoke-interface {v0, p1}, Lcom/a/b/d/sr;->a(Lcom/a/b/d/sr;)V

    .line 1510
    return-void
.end method

.method public final b()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1519
    iget-object v0, p0, Lcom/a/b/d/st;->b:Lcom/a/b/d/rz;

    return-object v0
.end method

.method public b(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 1469
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 1514
    iget v0, p0, Lcom/a/b/d/st;->a:I

    return v0
.end method

.method public c(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 1481
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1437
    invoke-virtual {p0}, Lcom/a/b/d/st;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 1491
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 1444
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1454
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1464
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1476
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1486
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
