.class final Lcom/a/b/d/acr;
.super Lcom/a/b/d/act;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/acq;


# direct methods
.method constructor <init>(Lcom/a/b/d/acq;)V
    .registers 4

    .prologue
    .line 720
    iput-object p1, p0, Lcom/a/b/d/acr;->a:Lcom/a/b/d/acq;

    iget-object v0, p1, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/act;-><init>(Lcom/a/b/d/abx;B)V

    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 735
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_26

    .line 736
    check-cast p1, Ljava/util/Map$Entry;

    .line 737
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_26

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/util/Map;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/a/b/d/acr;->a:Lcom/a/b/d/acq;

    iget-object v1, v1, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    iget-object v1, v1, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_26

    const/4 v0, 0x1

    .line 741
    :cond_26
    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 722
    iget-object v0, p0, Lcom/a/b/d/acr;->a:Lcom/a/b/d/acq;

    iget-object v0, v0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/acs;

    invoke-direct {v1, p0}, Lcom/a/b/d/acs;-><init>(Lcom/a/b/d/acr;)V

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Set;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 745
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_26

    .line 746
    check-cast p1, Ljava/util/Map$Entry;

    .line 747
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_26

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/util/Map;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/a/b/d/acr;->a:Lcom/a/b/d/acq;

    iget-object v1, v1, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    iget-object v1, v1, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_26

    const/4 v0, 0x1

    .line 751
    :cond_26
    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 731
    iget-object v0, p0, Lcom/a/b/d/acr;->a:Lcom/a/b/d/acq;

    iget-object v0, v0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
