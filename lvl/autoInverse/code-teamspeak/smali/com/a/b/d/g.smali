.class final Lcom/a/b/d/g;
.super Lcom/a/b/d/hi;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/a;


# direct methods
.method private constructor <init>(Lcom/a/b/d/a;)V
    .registers 2

    .prologue
    .line 185
    iput-object p1, p0, Lcom/a/b/d/g;->a:Lcom/a/b/d/a;

    invoke-direct {p0}, Lcom/a/b/d/hi;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/a;B)V
    .registers 3

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/a/b/d/g;-><init>(Lcom/a/b/d/a;)V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/a/b/d/g;->a:Lcom/a/b/d/a;

    invoke-static {v0}, Lcom/a/b/d/a;->a(Lcom/a/b/d/a;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/a/b/d/g;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/a/b/d/g;->a:Lcom/a/b/d/a;

    invoke-virtual {v0}, Lcom/a/b/d/a;->clear()V

    .line 192
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/a/b/d/g;->a:Lcom/a/b/d/a;

    invoke-virtual {v0}, Lcom/a/b/d/a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/a/b/d/g;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 195
    invoke-virtual {p0, p1}, Lcom/a/b/d/g;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 196
    const/4 v0, 0x0

    .line 199
    :goto_7
    return v0

    .line 198
    :cond_8
    iget-object v0, p0, Lcom/a/b/d/g;->a:Lcom/a/b/d/a;

    invoke-static {v0, p1}, Lcom/a/b/d/a;->a(Lcom/a/b/d/a;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    const/4 v0, 0x1

    goto :goto_7
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 203
    invoke-virtual {p0, p1}, Lcom/a/b/d/g;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 207
    invoke-virtual {p0, p1}, Lcom/a/b/d/g;->c(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
