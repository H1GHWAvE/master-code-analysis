.class final Lcom/a/b/d/adm;
.super Lcom/a/b/d/adt;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableSet"
.end annotation

.annotation build Lcom/a/b/a/d;
.end annotation


# static fields
.field private static final b:J


# instance fields
.field transient a:Ljava/util/NavigableSet;


# direct methods
.method constructor <init>(Ljava/util/NavigableSet;Ljava/lang/Object;)V
    .registers 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1229
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/adt;-><init>(Ljava/util/SortedSet;Ljava/lang/Object;)V

    .line 1230
    return-void
.end method

.method private e()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 1233
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 1224
    .line 13233
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1224
    return-object v0
.end method

.method final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1224
    .line 15233
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1224
    return-object v0
.end method

.method final synthetic c()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1224
    .line 14233
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1224
    return-object v0
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1237
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1238
    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1239
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1224
    .line 16233
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1224
    return-object v0
.end method

.method public final descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1243
    .line 3233
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1243
    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 1249
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1250
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adm;->a:Ljava/util/NavigableSet;

    if-nez v0, :cond_1b

    .line 4233
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1251
    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1253
    iput-object v0, p0, Lcom/a/b/d/adm;->a:Ljava/util/NavigableSet;

    .line 1254
    monitor-exit v1

    .line 1256
    :goto_1a
    return-object v0

    :cond_1b
    iget-object v0, p0, Lcom/a/b/d/adm;->a:Ljava/util/NavigableSet;

    monitor-exit v1

    goto :goto_1a

    .line 1257
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    throw v0
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1261
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 5233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1262
    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1263
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 1267
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 6233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1268
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1270
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1313
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/adm;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1274
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 7233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1275
    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->higher(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1276
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1280
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 8233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1281
    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->lower(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1282
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final pollFirst()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1286
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 9233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1287
    invoke-interface {v0}, Ljava/util/NavigableSet;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1288
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final pollLast()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1292
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 10233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1293
    invoke-interface {v0}, Ljava/util/NavigableSet;->pollLast()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1294
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 8

    .prologue
    .line 1299
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 11233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1300
    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableSet;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1302
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 1317
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/adm;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 1306
    iget-object v1, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 12233
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adt;->a()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 1307
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adm;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1309
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1321
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/adm;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method
