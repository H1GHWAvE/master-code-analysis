.class final Lcom/a/b/d/el;
.super Lcom/a/b/d/ma;
.source "SourceFile"


# instance fields
.field private final transient b:Lcom/a/b/d/ma;


# direct methods
.method constructor <init>(Lcom/a/b/d/ma;)V
    .registers 2

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/a/b/d/ma;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 34
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0, p1}, Lcom/a/b/d/ma;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
    .registers 4

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/ma;->b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/ma;->e()Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method final a(I)Lcom/a/b/d/xd;
    .registers 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0}, Lcom/a/b/d/ma;->o()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->e()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
    .registers 4

    .prologue
    .line 74
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/ma;->a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/ma;->e()Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0}, Lcom/a/b/d/ma;->b()Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/el;->b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/el;->a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/a/b/d/ma;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    return-object v0
.end method

.method public final synthetic e_()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/a/b/d/el;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0}, Lcom/a/b/d/ma;->i()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0}, Lcom/a/b/d/ma;->h_()Z

    move-result v0

    return v0
.end method

.method public final i()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0}, Lcom/a/b/d/ma;->h()Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic m()Lcom/a/b/d/abn;
    .registers 2

    .prologue
    .line 24
    .line 1064
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    .line 24
    return-object v0
.end method

.method public final synthetic n()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/a/b/d/el;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/a/b/d/el;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/b/d/el;->b:Lcom/a/b/d/ma;

    invoke-virtual {v0}, Lcom/a/b/d/ma;->size()I

    move-result v0

    return v0
.end method
