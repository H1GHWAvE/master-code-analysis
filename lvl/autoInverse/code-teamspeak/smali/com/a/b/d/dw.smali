.class abstract Lcom/a/b/d/dw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final b:J


# instance fields
.field final a:Ljava/lang/Comparable;


# direct methods
.method constructor <init>(Ljava/lang/Comparable;)V
    .registers 2
    .param p1    # Ljava/lang/Comparable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/a/b/d/dw;->a:Ljava/lang/Comparable;

    .line 42
    return-void
.end method

.method static b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;
    .registers 2

    .prologue
    .line 235
    new-instance v0, Lcom/a/b/d/eb;

    invoke-direct {v0, p0}, Lcom/a/b/d/eb;-><init>(Ljava/lang/Comparable;)V

    return-object v0
.end method

.method static c(Ljava/lang/Comparable;)Lcom/a/b/d/dw;
    .registers 2

    .prologue
    .line 296
    new-instance v0, Lcom/a/b/d/dz;

    invoke-direct {v0, p0}, Lcom/a/b/d/dz;-><init>(Ljava/lang/Comparable;)V

    return-object v0
.end method

.method static d()Lcom/a/b/d/dw;
    .registers 1

    .prologue
    .line 108
    invoke-static {}, Lcom/a/b/d/ea;->f()Lcom/a/b/d/ea;

    move-result-object v0

    return-object v0
.end method

.method static e()Lcom/a/b/d/dw;
    .registers 1

    .prologue
    .line 179
    invoke-static {}, Lcom/a/b/d/dy;->f()Lcom/a/b/d/dy;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/a/b/d/dw;)I
    .registers 4

    .prologue
    .line 69
    .line 1108
    invoke-static {}, Lcom/a/b/d/ea;->f()Lcom/a/b/d/ea;

    move-result-object v0

    .line 69
    if-ne p1, v0, :cond_8

    .line 70
    const/4 v0, 0x1

    .line 80
    :cond_7
    :goto_7
    return v0

    .line 1179
    :cond_8
    invoke-static {}, Lcom/a/b/d/dy;->f()Lcom/a/b/d/dy;

    move-result-object v0

    .line 72
    if-ne p1, v0, :cond_10

    .line 73
    const/4 v0, -0x1

    goto :goto_7

    .line 75
    :cond_10
    iget-object v0, p0, Lcom/a/b/d/dw;->a:Ljava/lang/Comparable;

    iget-object v1, p1, Lcom/a/b/d/dw;->a:Ljava/lang/Comparable;

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    .line 76
    if-nez v0, :cond_7

    .line 80
    instance-of v0, p0, Lcom/a/b/d/dz;

    instance-of v1, p1, Lcom/a/b/d/dz;

    invoke-static {v0, v1}, Lcom/a/b/l/a;->a(ZZ)I

    move-result v0

    goto :goto_7
.end method

.method abstract a()Lcom/a/b/d/ce;
.end method

.method abstract a(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
.end method

.method abstract a(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
.end method

.method abstract a(Ljava/lang/StringBuilder;)V
.end method

.method abstract a(Ljava/lang/Comparable;)Z
.end method

.method abstract b()Lcom/a/b/d/ce;
.end method

.method abstract b(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
.end method

.method abstract b(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
.end method

.method abstract b(Ljava/lang/StringBuilder;)V
.end method

.method c(Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 2

    .prologue
    .line 63
    return-object p0
.end method

.method c()Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/a/b/d/dw;->a:Ljava/lang/Comparable;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 36
    check-cast p1, Lcom/a/b/d/dw;

    invoke-virtual {p0, p1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 90
    instance-of v1, p1, Lcom/a/b/d/dw;

    if-eqz v1, :cond_e

    .line 92
    check-cast p1, Lcom/a/b/d/dw;

    .line 94
    :try_start_7
    invoke-virtual {p0, p1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I
    :try_end_a
    .catch Ljava/lang/ClassCastException; {:try_start_7 .. :try_end_a} :catch_f

    move-result v1

    .line 95
    if-nez v1, :cond_e

    const/4 v0, 0x1

    .line 99
    :cond_e
    :goto_e
    return v0

    :catch_f
    move-exception v1

    goto :goto_e
.end method
