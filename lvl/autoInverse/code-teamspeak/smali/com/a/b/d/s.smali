.class final Lcom/a/b/d/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field b:Ljava/util/Collection;

.field final synthetic c:Lcom/a/b/d/q;


# direct methods
.method constructor <init>(Lcom/a/b/d/q;)V
    .registers 3

    .prologue
    .line 1337
    iput-object p1, p0, Lcom/a/b/d/s;->c:Lcom/a/b/d/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338
    iget-object v0, p0, Lcom/a/b/d/s;->c:Lcom/a/b/d/q;

    iget-object v0, v0, Lcom/a/b/d/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/s;->a:Ljava/util/Iterator;

    return-void
.end method

.method private a()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 1349
    iget-object v0, p0, Lcom/a/b/d/s;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1350
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    iput-object v1, p0, Lcom/a/b/d/s;->b:Ljava/util/Collection;

    .line 1351
    iget-object v1, p0, Lcom/a/b/d/s;->c:Lcom/a/b/d/q;

    invoke-virtual {v1, v0}, Lcom/a/b/d/q;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/a/b/d/s;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1337
    .line 2349
    iget-object v0, p0, Lcom/a/b/d/s;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2350
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    iput-object v1, p0, Lcom/a/b/d/s;->b:Ljava/util/Collection;

    .line 2351
    iget-object v1, p0, Lcom/a/b/d/s;->c:Lcom/a/b/d/q;

    invoke-virtual {v1, v0}, Lcom/a/b/d/q;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 1337
    return-object v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 1356
    iget-object v0, p0, Lcom/a/b/d/s;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1357
    iget-object v0, p0, Lcom/a/b/d/s;->c:Lcom/a/b/d/q;

    iget-object v0, v0, Lcom/a/b/d/q;->b:Lcom/a/b/d/n;

    iget-object v1, p0, Lcom/a/b/d/s;->b:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {v0, v1}, Lcom/a/b/d/n;->b(Lcom/a/b/d/n;I)I

    .line 1358
    iget-object v0, p0, Lcom/a/b/d/s;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1359
    return-void
.end method
