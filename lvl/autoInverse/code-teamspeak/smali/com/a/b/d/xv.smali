.class final Lcom/a/b/d/xv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private final a:Lcom/a/b/d/xc;

.field private final b:Ljava/util/Iterator;

.field private c:Lcom/a/b/d/xd;

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method constructor <init>(Lcom/a/b/d/xc;Ljava/util/Iterator;)V
    .registers 3

    .prologue
    .line 1025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1026
    iput-object p1, p0, Lcom/a/b/d/xv;->a:Lcom/a/b/d/xc;

    .line 1027
    iput-object p2, p0, Lcom/a/b/d/xv;->b:Ljava/util/Iterator;

    .line 1028
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 1032
    iget v0, p0, Lcom/a/b/d/xv;->d:I

    if-gtz v0, :cond_c

    iget-object v0, p0, Lcom/a/b/d/xv;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1037
    invoke-virtual {p0}, Lcom/a/b/d/xv;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1038
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1040
    :cond_c
    iget v0, p0, Lcom/a/b/d/xv;->d:I

    if-nez v0, :cond_24

    .line 1041
    iget-object v0, p0, Lcom/a/b/d/xv;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    iput-object v0, p0, Lcom/a/b/d/xv;->c:Lcom/a/b/d/xd;

    .line 1042
    iget-object v0, p0, Lcom/a/b/d/xv;->c:Lcom/a/b/d/xd;

    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    iput v0, p0, Lcom/a/b/d/xv;->d:I

    iput v0, p0, Lcom/a/b/d/xv;->e:I

    .line 1044
    :cond_24
    iget v0, p0, Lcom/a/b/d/xv;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/xv;->d:I

    .line 1045
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/b/d/xv;->f:Z

    .line 1046
    iget-object v0, p0, Lcom/a/b/d/xv;->c:Lcom/a/b/d/xd;

    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 1051
    iget-boolean v0, p0, Lcom/a/b/d/xv;->f:Z

    .line 2049
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1052
    iget v0, p0, Lcom/a/b/d/xv;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1b

    .line 1053
    iget-object v0, p0, Lcom/a/b/d/xv;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1057
    :goto_11
    iget v0, p0, Lcom/a/b/d/xv;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/xv;->e:I

    .line 1058
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/a/b/d/xv;->f:Z

    .line 1059
    return-void

    .line 1055
    :cond_1b
    iget-object v0, p0, Lcom/a/b/d/xv;->a:Lcom/a/b/d/xc;

    iget-object v1, p0, Lcom/a/b/d/xv;->c:Lcom/a/b/d/xd;

    invoke-interface {v1}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/d/xc;->remove(Ljava/lang/Object;)Z

    goto :goto_11
.end method
