.class Lcom/a/b/d/adj;
.super Lcom/a/b/d/adn;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/vi;


# static fields
.field private static final f:J


# instance fields
.field transient a:Ljava/util/Set;

.field transient b:Ljava/util/Collection;

.field transient c:Ljava/util/Collection;

.field transient d:Ljava/util/Map;

.field transient e:Lcom/a/b/d/xc;


# direct methods
.method constructor <init>(Lcom/a/b/d/vi;)V
    .registers 3

    .prologue
    .line 528
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/adn;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 529
    return-void
.end method


# virtual methods
.method a()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 524
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    return-object v0
.end method

.method public final a(Lcom/a/b/d/vi;)Z
    .registers 4

    .prologue
    .line 589
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 590
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->a(Lcom/a/b/d/vi;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 591
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 575
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 576
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 577
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 5

    .prologue
    .line 596
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 597
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 598
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final b()Ljava/util/Map;
    .registers 5

    .prologue
    .line 654
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 655
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adj;->d:Ljava/util/Map;

    if-nez v0, :cond_18

    .line 656
    new-instance v0, Lcom/a/b/d/acw;

    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v2

    invoke-interface {v2}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/a/b/d/acw;-><init>(Ljava/util/Map;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/a/b/d/adj;->d:Ljava/util/Map;

    .line 658
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/adj;->d:Ljava/util/Map;

    monitor-exit v1

    return-object v0

    .line 659
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    throw v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 561
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 562
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 563
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 5

    .prologue
    .line 568
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 569
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 570
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 5

    .prologue
    .line 582
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 583
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 584
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 603
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 604
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 605
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 610
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 611
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 612
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 673
    if-ne p1, p0, :cond_4

    .line 674
    const/4 v0, 0x1

    .line 677
    :goto_3
    return v0

    .line 676
    :cond_4
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 677
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_3

    .line 678
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_7 .. :try_end_13} :catchall_11

    throw v0
.end method

.method public final f()I
    .registers 3

    .prologue
    .line 533
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 534
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->f()I

    move-result v0

    monitor-exit v1

    return v0

    .line 535
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 547
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 548
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->f(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 549
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 617
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 618
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->g()V

    .line 619
    monitor-exit v1

    return-void

    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public final g(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 554
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 555
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->g(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 556
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 682
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 683
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->hashCode()I

    move-result v0

    monitor-exit v1

    return v0

    .line 684
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final i()Ljava/util/Collection;
    .registers 4

    .prologue
    .line 634
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 635
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adj;->b:Ljava/util/Collection;

    if-nez v0, :cond_17

    .line 636
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->i()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    .line 1060
    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 636
    iput-object v0, p0, Lcom/a/b/d/adj;->b:Ljava/util/Collection;

    .line 638
    :cond_17
    iget-object v0, p0, Lcom/a/b/d/adj;->b:Ljava/util/Collection;

    monitor-exit v1

    return-object v0

    .line 639
    :catchall_1b
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    throw v0
.end method

.method public k()Ljava/util/Collection;
    .registers 4

    .prologue
    .line 644
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 645
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adj;->c:Ljava/util/Collection;

    if-nez v0, :cond_17

    .line 646
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adj;->c:Ljava/util/Collection;

    .line 648
    :cond_17
    iget-object v0, p0, Lcom/a/b/d/adj;->c:Ljava/util/Collection;

    monitor-exit v1

    return-object v0

    .line 649
    :catchall_1b
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    throw v0
.end method

.method public final n()Z
    .registers 3

    .prologue
    .line 540
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 541
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->n()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 542
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final p()Ljava/util/Set;
    .registers 4

    .prologue
    .line 624
    iget-object v1, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 625
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adj;->a:Ljava/util/Set;

    if-nez v0, :cond_17

    .line 626
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->b(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adj;->a:Ljava/util/Set;

    .line 628
    :cond_17
    iget-object v0, p0, Lcom/a/b/d/adj;->a:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 629
    :catchall_1b
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    throw v0
.end method

.method public final q()Lcom/a/b/d/xc;
    .registers 5

    .prologue
    .line 664
    iget-object v2, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 665
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adj;->e:Lcom/a/b/d/xc;

    if-nez v0, :cond_1c

    .line 666
    invoke-virtual {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->q()Lcom/a/b/d/xc;

    move-result-object v1

    iget-object v3, p0, Lcom/a/b/d/adj;->h:Ljava/lang/Object;

    .line 1412
    instance-of v0, v1, Lcom/a/b/d/adk;

    if-nez v0, :cond_19

    instance-of v0, v1, Lcom/a/b/d/ku;

    if-eqz v0, :cond_20

    :cond_19
    move-object v0, v1

    .line 666
    :goto_1a
    iput-object v0, p0, Lcom/a/b/d/adj;->e:Lcom/a/b/d/xc;

    .line 668
    :cond_1c
    iget-object v0, p0, Lcom/a/b/d/adj;->e:Lcom/a/b/d/xc;

    monitor-exit v2

    return-object v0

    .line 1416
    :cond_20
    new-instance v0, Lcom/a/b/d/adk;

    invoke-direct {v0, v1, v3}, Lcom/a/b/d/adk;-><init>(Lcom/a/b/d/xc;Ljava/lang/Object;)V

    goto :goto_1a

    .line 669
    :catchall_26
    move-exception v0

    monitor-exit v2
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_26

    throw v0
.end method
