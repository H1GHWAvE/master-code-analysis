.class abstract Lcom/a/b/d/kc;
.super Lcom/a/b/d/lo;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/a/b/d/lo;-><init>()V

    return-void
.end method


# virtual methods
.method abstract b()Lcom/a/b/d/jt;
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 46
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_20

    .line 47
    check-cast p1, Ljava/util/Map$Entry;

    .line 48
    invoke-virtual {p0}, Lcom/a/b/d/kc;->b()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 49
    if-eqz v1, :cond_20

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    const/4 v0, 0x1

    .line 51
    :cond_20
    return v0
.end method

.method final g()Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "serialization"
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/a/b/d/kd;

    invoke-virtual {p0}, Lcom/a/b/d/kc;->b()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/kd;-><init>(Lcom/a/b/d/jt;)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/a/b/d/kc;->b()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->i_()Z

    move-result v0

    return v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/d/kc;->b()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    return v0
.end method
