.class abstract Lcom/a/b/d/xp;
.super Lcom/a/b/d/aan;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 921
    invoke-direct {p0}, Lcom/a/b/d/aan;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lcom/a/b/d/xc;
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/a/b/d/xp;->a()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/xc;->clear()V

    .line 926
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 929
    invoke-virtual {p0}, Lcom/a/b/d/xp;->a()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 933
    invoke-virtual {p0}, Lcom/a/b/d/xp;->a()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 937
    invoke-virtual {p0}, Lcom/a/b/d/xp;->a()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/xc;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 941
    new-instance v0, Lcom/a/b/d/xq;

    invoke-virtual {p0}, Lcom/a/b/d/xp;->a()Lcom/a/b/d/xc;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/xq;-><init>(Lcom/a/b/d/xp;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 951
    invoke-virtual {p0}, Lcom/a/b/d/xp;->a()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v0

    .line 952
    if-lez v0, :cond_13

    .line 953
    invoke-virtual {p0}, Lcom/a/b/d/xp;->a()Lcom/a/b/d/xc;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/a/b/d/xc;->b(Ljava/lang/Object;I)I

    .line 954
    const/4 v0, 0x1

    .line 956
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public size()I
    .registers 2

    .prologue
    .line 960
    invoke-virtual {p0}, Lcom/a/b/d/xp;->a()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
