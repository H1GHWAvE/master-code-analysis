.class public abstract Lcom/a/b/d/kk;
.super Lcom/a/b/d/an;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final a:J


# instance fields
.field public final transient b:Lcom/a/b/d/jt;

.field final transient c:I


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;I)V
    .registers 3

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/a/b/d/an;-><init>()V

    .line 322
    iput-object p1, p0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    .line 323
    iput p2, p0, Lcom/a/b/d/kk;->c:I

    .line 324
    return-void
.end method

.method private A()Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 477
    iget-object v0, p0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    return-object v0
.end method

.method private B()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 497
    new-instance v0, Lcom/a/b/d/kp;

    invoke-direct {v0, p0}, Lcom/a/b/d/kp;-><init>(Lcom/a/b/d/kk;)V

    return-object v0
.end method

.method private C()Lcom/a/b/d/ku;
    .registers 2

    .prologue
    .line 573
    invoke-super {p0}, Lcom/a/b/d/an;->q()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ku;

    return-object v0
.end method

.method private D()Lcom/a/b/d/ku;
    .registers 2

    .prologue
    .line 578
    new-instance v0, Lcom/a/b/d/ks;

    invoke-direct {v0, p0}, Lcom/a/b/d/ks;-><init>(Lcom/a/b/d/kk;)V

    return-object v0
.end method

.method private E()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 628
    new-instance v0, Lcom/a/b/d/kt;

    invoke-direct {v0, p0}, Lcom/a/b/d/kt;-><init>(Lcom/a/b/d/kk;)V

    return-object v0
.end method

.method private static a()Lcom/a/b/d/kk;
    .registers 1

    .prologue
    .line 1064
    sget-object v0, Lcom/a/b/d/ex;->a:Lcom/a/b/d/ex;

    .line 70
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
    .registers 5

    .prologue
    .line 1137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 1083
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 1084
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 1085
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 84
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
    .registers 7

    .prologue
    .line 92
    .line 2137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 2095
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 2096
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 2097
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 2098
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 92
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
    .registers 9

    .prologue
    .line 100
    .line 3137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 3108
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 3109
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 3110
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 3111
    invoke-virtual {v0, p6, p7}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 3112
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 100
    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
    .registers 11

    .prologue
    .line 108
    .line 4137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 4122
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 4123
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 4124
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 4125
    invoke-virtual {v0, p6, p7}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 4126
    invoke-virtual {v0, p8, p9}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 4127
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 108
    return-object v0
.end method

.method public static b(Lcom/a/b/d/vi;)Lcom/a/b/d/kk;
    .registers 7

    .prologue
    .line 290
    instance-of v0, p0, Lcom/a/b/d/kk;

    if-eqz v0, :cond_10

    move-object v0, p0

    .line 292
    check-cast v0, Lcom/a/b/d/kk;

    .line 4438
    iget-object v1, v0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->i_()Z

    move-result v1

    .line 294
    if-nez v1, :cond_10

    .line 5252
    :cond_f
    :goto_f
    return-object v0

    .line 5242
    :cond_10
    invoke-interface {p0}, Lcom/a/b/d/vi;->n()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 6064
    sget-object v0, Lcom/a/b/d/ex;->a:Lcom/a/b/d/ex;

    goto :goto_f

    .line 5247
    :cond_19
    instance-of v0, p0, Lcom/a/b/d/jr;

    if-eqz v0, :cond_28

    move-object v0, p0

    .line 5249
    check-cast v0, Lcom/a/b/d/jr;

    .line 6438
    iget-object v1, v0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->i_()Z

    move-result v1

    .line 5251
    if-eqz v1, :cond_f

    .line 5256
    :cond_28
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v3

    .line 5257
    const/4 v0, 0x0

    .line 5260
    invoke-interface {p0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_3a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_64

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 5261
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v1

    .line 5262
    invoke-virtual {v1}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6e

    .line 5263
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 5264
    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    add-int/2addr v0, v2

    :goto_62
    move v2, v0

    .line 5266
    goto :goto_3a

    .line 5268
    :cond_64
    new-instance v0, Lcom/a/b/d/jr;

    invoke-virtual {v3}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/jr;-><init>(Lcom/a/b/d/jt;I)V

    goto :goto_f

    :cond_6e
    move v0, v2

    goto :goto_62
.end method

.method private static c()Lcom/a/b/d/kn;
    .registers 1

    .prologue
    .line 118
    new-instance v0, Lcom/a/b/d/kn;

    invoke-direct {v0}, Lcom/a/b/d/kn;-><init>()V

    return-object v0
.end method

.method private static d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kk;
    .registers 3

    .prologue
    .line 77
    invoke-static {p0, p1}, Lcom/a/b/d/jr;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;

    move-result-object v0

    return-object v0
.end method

.method private u()Z
    .registers 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->i_()Z

    move-result v0

    return v0
.end method

.method private z()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/d/vi;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 416
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 392
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/d/kk;->e()Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 64
    .line 6477
    iget-object v0, p0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    .line 64
    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Lcom/a/b/d/an;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/a/b/d/kk;->h(Ljava/lang/Object;)Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 404
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 428
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract d()Lcom/a/b/d/kk;
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/d/kk;->t()Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/a/b/d/iz;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/a/b/d/an;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 455
    iget v0, p0, Lcom/a/b/d/kk;->c:I

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 445
    iget-object v0, p0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 362
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final g(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 450
    if-eqz p1, :cond_a

    invoke-super {p0, p1}, Lcom/a/b/d/an;->g(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public abstract h(Ljava/lang/Object;)Lcom/a/b/d/iz;
.end method

.method public bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/a/b/d/an;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 64
    .line 7623
    invoke-super {p0}, Lcom/a/b/d/an;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/iz;

    .line 64
    return-object v0
.end method

.method final synthetic j()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/d/kk;->y()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/d/kk;->v()Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method

.method final synthetic l()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/d/kk;->w()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method final m()Ljava/util/Map;
    .registers 3

    .prologue
    .line 482
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final bridge synthetic n()Z
    .registers 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/a/b/d/an;->n()Z

    move-result v0

    return v0
.end method

.method final synthetic o()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 64
    .line 10497
    new-instance v0, Lcom/a/b/d/kp;

    invoke-direct {v0, p0}, Lcom/a/b/d/kp;-><init>(Lcom/a/b/d/kk;)V

    .line 64
    return-object v0
.end method

.method public final synthetic p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 64
    .line 10467
    iget-object v0, p0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 64
    return-object v0
.end method

.method public final bridge synthetic q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 64
    .line 9573
    invoke-super {p0}, Lcom/a/b/d/an;->q()Lcom/a/b/d/xc;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ku;

    .line 64
    return-object v0
.end method

.method final synthetic r()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 64
    .line 8578
    new-instance v0, Lcom/a/b/d/ks;

    invoke-direct {v0, p0}, Lcom/a/b/d/ks;-><init>(Lcom/a/b/d/kk;)V

    .line 64
    return-object v0
.end method

.method final synthetic s()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 64
    .line 6628
    new-instance v0, Lcom/a/b/d/kt;

    invoke-direct {v0, p0}, Lcom/a/b/d/kt;-><init>(Lcom/a/b/d/kk;)V

    .line 64
    return-object v0
.end method

.method public t()Lcom/a/b/d/iz;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 337
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/a/b/d/an;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 492
    invoke-super {p0}, Lcom/a/b/d/an;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/iz;

    return-object v0
.end method

.method final w()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 557
    new-instance v0, Lcom/a/b/d/kl;

    invoke-direct {v0, p0}, Lcom/a/b/d/kl;-><init>(Lcom/a/b/d/kk;)V

    return-object v0
.end method

.method public final x()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 623
    invoke-super {p0}, Lcom/a/b/d/an;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/iz;

    return-object v0
.end method

.method final y()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 633
    new-instance v0, Lcom/a/b/d/km;

    invoke-direct {v0, p0}, Lcom/a/b/d/km;-><init>(Lcom/a/b/d/kk;)V

    return-object v0
.end method
