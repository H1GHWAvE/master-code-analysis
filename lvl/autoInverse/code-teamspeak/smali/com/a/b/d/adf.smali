.class final Lcom/a/b/d/adf;
.super Lcom/a/b/d/adn;
.source "SourceFile"

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "works but is needed only for NavigableMap"
.end annotation


# static fields
.field private static final a:J


# direct methods
.method constructor <init>(Ljava/util/Map$Entry;Ljava/lang/Object;)V
    .registers 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1527
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/adn;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1528
    return-void
.end method

.method private a()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 1532
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1522
    .line 7532
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1522
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1536
    iget-object v1, p0, Lcom/a/b/d/adf;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2532
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1537
    invoke-interface {v0, p1}, Ljava/util/Map$Entry;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1538
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final getKey()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1548
    iget-object v1, p0, Lcom/a/b/d/adf;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 4532
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1549
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1550
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final getValue()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1554
    iget-object v1, p0, Lcom/a/b/d/adf;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 5532
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1555
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1556
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 1542
    iget-object v1, p0, Lcom/a/b/d/adf;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 3532
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1543
    invoke-interface {v0}, Ljava/util/Map$Entry;->hashCode()I

    move-result v0

    monitor-exit v1

    return v0

    .line 1544
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1560
    iget-object v1, p0, Lcom/a/b/d/adf;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 6532
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adn;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1561
    invoke-interface {v0, p1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1562
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method
