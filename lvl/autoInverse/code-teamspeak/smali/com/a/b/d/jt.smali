.class public abstract Lcom/a/b/d/jt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Map;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final a:[Ljava/util/Map$Entry;


# instance fields
.field private transient b:Lcom/a/b/d/lo;

.field private transient c:Lcom/a/b/d/lo;

.field private transient d:Lcom/a/b/d/iz;

.field private transient e:Lcom/a/b/d/lr;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 313
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/util/Map$Entry;

    sput-object v0, Lcom/a/b/d/jt;->a:[Ljava/util/Map$Entry;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;
    .registers 8

    .prologue
    .line 89
    new-instance v0, Lcom/a/b/d/zf;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/a/b/d/kb;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/a/b/d/zf;-><init>([Lcom/a/b/d/kb;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;
    .registers 10

    .prologue
    .line 99
    new-instance v0, Lcom/a/b/d/zf;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/a/b/d/kb;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/a/b/d/zf;-><init>([Lcom/a/b/d/kb;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;
    .registers 12

    .prologue
    .line 110
    new-instance v0, Lcom/a/b/d/zf;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/a/b/d/kb;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p6, p7}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/a/b/d/zf;-><init>([Lcom/a/b/d/kb;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;
    .registers 14

    .prologue
    .line 121
    new-instance v0, Lcom/a/b/d/zf;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/a/b/d/kb;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p6, p7}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {p8, p9}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/a/b/d/zf;-><init>([Lcom/a/b/d/kb;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;)Lcom/a/b/d/jt;
    .registers 5

    .prologue
    .line 273
    instance-of v0, p0, Lcom/a/b/d/jt;

    if-eqz v0, :cond_12

    instance-of v0, p0, Lcom/a/b/d/lw;

    if-nez v0, :cond_12

    move-object v0, p0

    .line 278
    check-cast v0, Lcom/a/b/d/jt;

    .line 279
    invoke-virtual {v0}, Lcom/a/b/d/jt;->i_()Z

    move-result v1

    if-nez v1, :cond_42

    .line 294
    :goto_11
    return-object v0

    .line 282
    :cond_12
    instance-of v0, p0, Ljava/util/EnumMap;

    if-eqz v0, :cond_42

    .line 1301
    check-cast p0, Ljava/util/EnumMap;

    .line 1306
    new-instance v1, Ljava/util/EnumMap;

    invoke-direct {v1, p0}, Ljava/util/EnumMap;-><init>(Ljava/util/Map;)V

    .line 1307
    invoke-virtual {v1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_25
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1308
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_25

    .line 1310
    :cond_3d
    invoke-static {v1}, Lcom/a/b/d/jd;->a(Ljava/util/EnumMap;)Lcom/a/b/d/jt;

    move-result-object v0

    goto :goto_11

    .line 285
    :cond_42
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/a/b/d/jt;->a:[Ljava/util/Map$Entry;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/Map$Entry;

    .line 286
    array-length v1, v0

    packed-switch v1, :pswitch_data_6e

    .line 294
    new-instance v1, Lcom/a/b/d/zf;

    invoke-direct {v1, v0}, Lcom/a/b/d/zf;-><init>([Ljava/util/Map$Entry;)V

    move-object v0, v1

    goto :goto_11

    .line 2070
    :pswitch_59
    invoke-static {}, Lcom/a/b/d/it;->i()Lcom/a/b/d/it;

    move-result-object v0

    goto :goto_11

    .line 291
    :pswitch_5e
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 292
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 2080
    invoke-static {v1, v0}, Lcom/a/b/d/it;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;

    move-result-object v0

    goto :goto_11

    .line 286
    :pswitch_data_6e
    .packed-switch 0x0
        :pswitch_59
        :pswitch_5e
    .end packed-switch
.end method

.method private a()Lcom/a/b/d/lr;
    .registers 5

    .prologue
    .line 441
    .line 3446
    new-instance v0, Lcom/a/b/d/jv;

    invoke-direct {v0, p0}, Lcom/a/b/d/jv;-><init>(Lcom/a/b/d/jt;)V

    .line 442
    new-instance v1, Lcom/a/b/d/lr;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/a/b/d/lr;-><init>(Lcom/a/b/d/jt;ILjava/util/Comparator;)V

    return-object v1
.end method

.method static a(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V
    .registers 11

    .prologue
    .line 149
    if-nez p0, :cond_57

    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x22

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Multiple entries with same "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_57
    return-void
.end method

.method private static b(Ljava/util/Map;)Lcom/a/b/d/jt;
    .registers 5

    .prologue
    .line 301
    check-cast p0, Ljava/util/EnumMap;

    .line 2306
    new-instance v1, Ljava/util/EnumMap;

    invoke-direct {v1, p0}, Ljava/util/EnumMap;-><init>(Ljava/util/Map;)V

    .line 2307
    invoke-virtual {v1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2308
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_f

    .line 2310
    :cond_27
    invoke-static {v1}, Lcom/a/b/d/jd;->a(Ljava/util/EnumMap;)Lcom/a/b/d/jt;

    move-result-object v0

    .line 301
    return-object v0
.end method

.method public static c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;
    .registers 3

    .prologue
    .line 80
    invoke-static {p0, p1}, Lcom/a/b/d/it;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/util/Map;)Lcom/a/b/d/jt;
    .registers 5

    .prologue
    .line 306
    new-instance v1, Ljava/util/EnumMap;

    invoke-direct {v1, p0}, Ljava/util/EnumMap;-><init>(Ljava/util/Map;)V

    .line 307
    invoke-virtual {v1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 308
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_d

    .line 310
    :cond_25
    invoke-static {v1}, Lcom/a/b/d/jd;->a(Ljava/util/EnumMap;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method static d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;
    .registers 3

    .prologue
    .line 135
    invoke-static {p0, p1}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 136
    new-instance v0, Lcom/a/b/d/kb;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/kb;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private i()Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 446
    new-instance v0, Lcom/a/b/d/jv;

    invoke-direct {v0, p0}, Lcom/a/b/d/jv;-><init>(Lcom/a/b/d/jt;)V

    return-object v0
.end method

.method public static k()Lcom/a/b/d/jt;
    .registers 1

    .prologue
    .line 70
    invoke-static {}, Lcom/a/b/d/it;->i()Lcom/a/b/d/it;

    move-result-object v0

    return-object v0
.end method

.method public static l()Lcom/a/b/d/ju;
    .registers 1

    .prologue
    .line 144
    new-instance v0, Lcom/a/b/d/ju;

    invoke-direct {v0}, Lcom/a/b/d/ju;-><init>()V

    return-object v0
.end method


# virtual methods
.method c()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 411
    new-instance v0, Lcom/a/b/d/ke;

    invoke-direct {v0, p0}, Lcom/a/b/d/ke;-><init>(Lcom/a/b/d/jt;)V

    return-object v0
.end method

.method public final clear()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 362
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 372
    invoke-virtual {p0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/a/b/d/jt;->h()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/iz;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method abstract d()Lcom/a/b/d/lo;
.end method

.method public e()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/a/b/d/jt;->b:Lcom/a/b/d/lo;

    .line 393
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/jt;->d()Lcom/a/b/d/lo;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/jt;->b:Lcom/a/b/d/lo;

    :cond_a
    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 507
    invoke-static {p0, p1}, Lcom/a/b/d/sz;->f(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Lcom/a/b/d/lr;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 436
    iget-object v0, p0, Lcom/a/b/d/jt;->e:Lcom/a/b/d/lr;

    .line 437
    if-nez v0, :cond_15

    .line 2446
    new-instance v1, Lcom/a/b/d/jv;

    invoke-direct {v1, p0}, Lcom/a/b/d/jv;-><init>(Lcom/a/b/d/jt;)V

    .line 2442
    new-instance v0, Lcom/a/b/d/lr;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->size()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/d/lr;-><init>(Lcom/a/b/d/jt;ILjava/util/Comparator;)V

    .line 437
    iput-object v0, p0, Lcom/a/b/d/jt;->e:Lcom/a/b/d/lr;

    :cond_15
    return-object v0
.end method

.method public g()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/a/b/d/jt;->c:Lcom/a/b/d/lo;

    .line 407
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/jt;->c()Lcom/a/b/d/lo;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/jt;->c:Lcom/a/b/d/lo;

    :cond_a
    return-object v0
.end method

.method public abstract get(Ljava/lang/Object;)Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public h()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/a/b/d/jt;->d:Lcom/a/b/d/iz;

    .line 423
    if-nez v0, :cond_b

    new-instance v0, Lcom/a/b/d/kh;

    invoke-direct {v0, p0}, Lcom/a/b/d/kh;-><init>(Lcom/a/b/d/jt;)V

    iput-object v0, p0, Lcom/a/b/d/jt;->d:Lcom/a/b/d/iz;

    :cond_b
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->hashCode()I

    move-result v0

    return v0
.end method

.method abstract i_()Z
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method j()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 554
    new-instance v0, Lcom/a/b/d/jz;

    invoke-direct {v0, p0}, Lcom/a/b/d/jz;-><init>(Lcom/a/b/d/jt;)V

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 326
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 338
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 519
    invoke-static {p0}, Lcom/a/b/d/sz;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/a/b/d/jt;->h()Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method
