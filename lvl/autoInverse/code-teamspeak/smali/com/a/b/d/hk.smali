.class public abstract Lcom/a/b/d/hk;
.super Lcom/a/b/d/gs;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/a/b/d/hk;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 114
    if-nez v0, :cond_d

    .line 115
    check-cast p1, Ljava/lang/Comparable;

    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 117
    :goto_c
    return v0

    :cond_d
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_c
.end method


# virtual methods
.method protected synthetic a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/a/b/d/hk;->c()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/hk;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_15

    const/4 v0, 0x1

    :goto_7
    const-string v1, "fromKey must be <= toKey"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 155
    invoke-virtual {p0, p1}, Lcom/a/b/d/hk;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0

    .line 154
    :cond_15
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected abstract c()Ljava/util/SortedMap;
.end method

.method protected final c(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 133
    .line 134
    :try_start_1
    invoke-interface {p0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v1

    .line 135
    invoke-direct {p0, v1, p1}, Lcom/a/b/d/hk;->b(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_c
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_c} :catch_15
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_c} :catch_13
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_c} :catch_11

    move-result v1

    if-nez v1, :cond_10

    const/4 v0, 0x1

    .line 141
    :cond_10
    :goto_10
    return v0

    :catch_11
    move-exception v1

    goto :goto_10

    .line 139
    :catch_13
    move-exception v1

    goto :goto_10

    .line 137
    :catch_15
    move-exception v1

    goto :goto_10
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/a/b/d/hk;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/a/b/d/hk;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/a/b/d/hk;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/a/b/d/hk;->c()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/a/b/d/hk;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 4

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/a/b/d/hk;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/a/b/d/hk;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method
