.class public abstract Lcom/a/b/d/gj;
.super Lcom/a/b/d/hh;
.source "SourceFile"

# interfaces
.implements Ljava/util/Deque;


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/a/b/d/hh;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic a()Ljava/util/Queue;
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    return-object v0
.end method

.method public addFirst(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 48
    return-void
.end method

.method public addLast(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method protected synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    return-object v0
.end method

.method protected abstract d()Ljava/util/Deque;
.end method

.method public descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    return-object v0
.end method

.method public offerFirst(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Deque;->offerFirst(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public offerLast(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Deque;->offerLast(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public peekFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public peekLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->peekLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->pollLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pop()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public push(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 108
    return-void
.end method

.method public removeFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public removeFirstOccurrence(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Deque;->removeFirstOccurrence(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public removeLastOccurrence(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/a/b/d/gj;->d()Ljava/util/Deque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Deque;->removeLastOccurrence(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
