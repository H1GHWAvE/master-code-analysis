.class final Lcom/a/b/d/acj;
.super Lcom/a/b/d/act;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/aci;


# direct methods
.method constructor <init>(Lcom/a/b/d/aci;)V
    .registers 4

    .prologue
    .line 791
    iput-object p1, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    iget-object v0, p1, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/act;-><init>(Lcom/a/b/d/abx;B)V

    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 806
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_27

    .line 807
    check-cast p1, Ljava/util/Map$Entry;

    .line 808
    iget-object v0, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    iget-object v0, v0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 812
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 813
    iget-object v1, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    invoke-virtual {v1, v0}, Lcom/a/b/d/aci;->a(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 816
    :goto_26
    return v0

    :cond_27
    const/4 v0, 0x0

    goto :goto_26
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 793
    iget-object v0, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    iget-object v0, v0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0}, Lcom/a/b/d/abx;->b()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/ack;

    invoke-direct {v1, p0}, Lcom/a/b/d/ack;-><init>(Lcom/a/b/d/acj;)V

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Set;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 820
    invoke-virtual {p0, p1}, Lcom/a/b/d/acj;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 821
    check-cast p1, Ljava/util/Map$Entry;

    .line 822
    iget-object v0, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    iget-object v0, v0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/abx;->a(Lcom/a/b/d/abx;Ljava/lang/Object;)Ljava/util/Map;

    .line 823
    const/4 v0, 0x1

    .line 825
    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 835
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 836
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/util/Iterator;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 6

    .prologue
    .line 840
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 841
    const/4 v0, 0x0

    .line 842
    iget-object v1, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    iget-object v1, v1, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v1}, Lcom/a/b/d/abx;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_18
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 843
    iget-object v3, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    iget-object v3, v3, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v3, v2}, Lcom/a/b/d/abx;->d(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 844
    iget-object v0, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    iget-object v0, v0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-static {v0, v2}, Lcom/a/b/d/abx;->a(Lcom/a/b/d/abx;Ljava/lang/Object;)Ljava/util/Map;

    .line 845
    const/4 v0, 0x1

    goto :goto_18

    .line 848
    :cond_3d
    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 802
    iget-object v0, p0, Lcom/a/b/d/acj;->a:Lcom/a/b/d/aci;

    iget-object v0, v0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0}, Lcom/a/b/d/abx;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
