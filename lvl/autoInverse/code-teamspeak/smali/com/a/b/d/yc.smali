.class public final Lcom/a/b/d/yc;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field static final a:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 37
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/a/b/d/yc;->a:[Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 239
    if-nez p0, :cond_1d

    .line 240
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_1d
    return-object p0
.end method

.method static a([Ljava/lang/Object;II)V
    .registers 5

    .prologue
    .line 220
    aget-object v0, p0, p1

    .line 221
    aget-object v1, p0, p2

    aput-object v1, p0, p1

    .line 222
    aput-object v0, p0, p2

    .line 223
    return-void
.end method

.method public static a(Ljava/lang/Class;I)[Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "Array.newInstance(Class, int)"
    .end annotation

    .prologue
    .line 50
    invoke-static {p0, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 6

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 210
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 211
    add-int/lit8 v1, v0, 0x1

    aput-object v3, p1, v0

    move v0, v1

    .line 212
    goto :goto_5

    .line 213
    :cond_15
    return-object p1
.end method

.method private static a(Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 6
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 89
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    invoke-static {p1, v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 90
    aput-object p0, v0, v3

    .line 91
    const/4 v1, 0x1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    return-object v0
.end method

.method static a(Ljava/util/Collection;)[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 191
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v0}, Lcom/a/b/d/yc;->a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 4

    .prologue
    .line 143
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 144
    array-length v1, p1

    if-ge v1, v0, :cond_b

    .line 145
    invoke-static {p1, v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 147
    :cond_b
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;

    .line 148
    array-length v1, p1

    if-le v1, v0, :cond_14

    .line 149
    const/4 v1, 0x0

    aput-object v1, p1, v0

    .line 151
    :cond_14
    return-object p1
.end method

.method static varargs a([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 226
    array-length v0, p0

    invoke-static {p0, v0}, Lcom/a/b/d/yc;->c([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a([Ljava/lang/Object;I)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 61
    .line 1048
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    .line 1053
    invoke-static {v0, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 61
    return-object v0
.end method

.method private static a([Ljava/lang/Object;II[Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 6

    .prologue
    .line 166
    add-int v0, p1, p2

    array-length v1, p0

    invoke-static {p1, v0, v1}, Lcom/a/b/b/cn;->a(III)V

    .line 167
    array-length v0, p3

    if-ge v0, p2, :cond_12

    .line 168
    invoke-static {p3, p2}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p3

    .line 172
    :cond_d
    :goto_d
    const/4 v0, 0x0

    invoke-static {p0, p1, p3, v0, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 173
    return-object p3

    .line 169
    :cond_12
    array-length v0, p3

    if-le v0, p2, :cond_d

    .line 170
    const/4 v0, 0x0

    aput-object v0, p3, p2

    goto :goto_d
.end method

.method private static a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lcom/a/b/d/yc;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 106
    array-length v1, p0

    aput-object p1, v0, v1

    .line 107
    return-object v0
.end method

.method public static a([Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "Array.newInstance(Class, int)"
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 73
    array-length v0, p0

    array-length v1, p1

    add-int/2addr v0, v1

    invoke-static {p2, v0}, Lcom/a/b/d/yc;->a(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v0

    .line 74
    array-length v1, p0

    invoke-static {p0, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    array-length v1, p0

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 76
    return-object v0
.end method

.method static b([Ljava/lang/Object;I)[Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 112
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 113
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 115
    return-object v0
.end method

.method private static b([Ljava/lang/Object;II)[Ljava/lang/Object;
    .registers 5

    .prologue
    .line 199
    add-int v0, p1, p2

    array-length v1, p0

    invoke-static {p1, v0, v1}, Lcom/a/b/b/cn;->a(III)V

    .line 200
    if-nez p2, :cond_b

    .line 201
    sget-object v0, Lcom/a/b/d/yc;->a:[Ljava/lang/Object;

    .line 205
    :goto_a
    return-object v0

    .line 203
    :cond_b
    new-array v0, p2, [Ljava/lang/Object;

    .line 204
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_a
.end method

.method static c([Ljava/lang/Object;I)[Ljava/lang/Object;
    .registers 4

    .prologue
    .line 230
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_b

    .line 231
    aget-object v1, p0, v0

    invoke-static {v1, v0}, Lcom/a/b/d/yc;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 233
    :cond_b
    return-object p0
.end method
