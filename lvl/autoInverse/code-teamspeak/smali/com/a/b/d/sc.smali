.class Lcom/a/b/d/sc;
.super Ljava/lang/ref/SoftReference;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/rz;


# instance fields
.field final a:I

.field final b:Lcom/a/b/d/rz;

.field volatile c:Lcom/a/b/d/sr;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/a/b/d/rz;)V
    .registers 6
    .param p4    # Lcom/a/b/d/rz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1173
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1240
    invoke-static {}, Lcom/a/b/d/qy;->g()Lcom/a/b/d/sr;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/sc;->c:Lcom/a/b/d/sr;

    .line 1174
    iput p3, p0, Lcom/a/b/d/sc;->a:I

    .line 1175
    iput-object p4, p0, Lcom/a/b/d/sc;->b:Lcom/a/b/d/rz;

    .line 1176
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/d/sr;
    .registers 2

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/a/b/d/sc;->c:Lcom/a/b/d/sr;

    return-object v0
.end method

.method public a(J)V
    .registers 4

    .prologue
    .line 1191
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 1201
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/a/b/d/sr;)V
    .registers 3

    .prologue
    .line 1249
    iget-object v0, p0, Lcom/a/b/d/sc;->c:Lcom/a/b/d/sr;

    .line 1250
    iput-object p1, p0, Lcom/a/b/d/sc;->c:Lcom/a/b/d/sr;

    .line 1251
    invoke-interface {v0, p1}, Lcom/a/b/d/sr;->a(Lcom/a/b/d/sr;)V

    .line 1252
    return-void
.end method

.method public final b()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1261
    iget-object v0, p0, Lcom/a/b/d/sc;->b:Lcom/a/b/d/rz;

    return-object v0
.end method

.method public b(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 1211
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 1256
    iget v0, p0, Lcom/a/b/d/sc;->a:I

    return v0
.end method

.method public c(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 1223
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1180
    invoke-virtual {p0}, Lcom/a/b/d/sc;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 1233
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 1186
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1196
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1218
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1228
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
