.class Lcom/a/b/d/uk;
.super Lcom/a/b/d/aan;
.source "SourceFile"


# instance fields
.field final d:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 3484
    invoke-direct {p0}, Lcom/a/b/d/aan;-><init>()V

    .line 3485
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/a/b/d/uk;->d:Ljava/util/Map;

    .line 3486
    return-void
.end method


# virtual methods
.method b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 3489
    iget-object v0, p0, Lcom/a/b/d/uk;->d:Ljava/util/Map;

    return-object v0
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 3517
    invoke-virtual {p0}, Lcom/a/b/d/uk;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 3518
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 3505
    invoke-virtual {p0}, Lcom/a/b/d/uk;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 3501
    invoke-virtual {p0}, Lcom/a/b/d/uk;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 3493
    invoke-virtual {p0}, Lcom/a/b/d/uk;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 3509
    invoke-virtual {p0, p1}, Lcom/a/b/d/uk;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 3510
    invoke-virtual {p0}, Lcom/a/b/d/uk;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3511
    const/4 v0, 0x1

    .line 3513
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public size()I
    .registers 2

    .prologue
    .line 3497
    invoke-virtual {p0}, Lcom/a/b/d/uk;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
