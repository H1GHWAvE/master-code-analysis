.class public final Lcom/a/b/d/yk;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/concurrent/BlockingQueue;Ljava/util/Collection;IJLjava/util/concurrent/TimeUnit;)I
    .registers 13
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 262
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    invoke-virtual {p5, p3, p4}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 269
    const/4 v0, 0x0

    .line 270
    :cond_d
    :goto_d
    if-ge v0, p2, :cond_2c

    .line 273
    sub-int v1, p2, v0

    invoke-interface {p0, p1, v1}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;I)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    if-ge v0, p2, :cond_d

    .line 275
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long v4, v2, v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v4, v5, v1}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    .line 276
    if-eqz v1, :cond_2c

    .line 279
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 280
    add-int/lit8 v0, v0, 0x1

    .line 281
    goto :goto_d

    .line 283
    :cond_2c
    return v0
.end method

.method private static a()Ljava/util/ArrayDeque;
    .registers 1

    .prologue
    .line 62
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/util/ArrayDeque;
    .registers 3

    .prologue
    .line 72
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    .line 73
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(Ljava/util/Collection;)V

    .line 77
    :goto_d
    return-object v0

    .line 75
    :cond_e
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    .line 76
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_d
.end method

.method private static a(Ljava/util/Deque;)Ljava/util/Deque;
    .registers 2

    .prologue
    .line 2625
    new-instance v0, Lcom/a/b/d/ade;

    invoke-direct {v0, p0}, Lcom/a/b/d/ade;-><init>(Ljava/util/Deque;)V

    .line 395
    return-object v0
.end method

.method private static a(Ljava/util/Queue;)Ljava/util/Queue;
    .registers 2

    .prologue
    .line 364
    .line 2569
    instance-of v0, p0, Lcom/a/b/d/ado;

    if-eqz v0, :cond_5

    :goto_4
    return-object p0

    :cond_5
    new-instance v0, Lcom/a/b/d/ado;

    invoke-direct {v0, p0}, Lcom/a/b/d/ado;-><init>(Ljava/util/Queue;)V

    move-object p0, v0

    .line 364
    goto :goto_4
.end method

.method private static a(I)Ljava/util/concurrent/ArrayBlockingQueue;
    .registers 2

    .prologue
    .line 51
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, p0}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    return-object v0
.end method

.method private static b(Ljava/util/concurrent/BlockingQueue;Ljava/util/Collection;IJLjava/util/concurrent/TimeUnit;)I
    .registers 13
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 302
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {p5, p3, p4}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    move v0, v1

    .line 307
    :cond_e
    :goto_e
    if-ge v0, p2, :cond_30

    .line 310
    sub-int v4, p2, v0

    :try_start_12
    invoke-interface {p0, p1, v4}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;I)I
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_3a

    move-result v4

    add-int/2addr v0, v4

    .line 311
    if-ge v0, p2, :cond_e

    .line 315
    :goto_19
    :try_start_19
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long v4, v2, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v4, v5, v6}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_24
    .catch Ljava/lang/InterruptedException; {:try_start_19 .. :try_end_24} :catch_2d
    .catchall {:try_start_19 .. :try_end_24} :catchall_3a

    move-result-object v4

    .line 321
    if-eqz v4, :cond_30

    .line 324
    :try_start_27
    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_2a
    .catchall {:try_start_27 .. :try_end_2a} :catchall_3a

    .line 325
    add-int/lit8 v0, v0, 0x1

    .line 326
    goto :goto_e

    .line 318
    :catch_2d
    move-exception v1

    const/4 v1, 0x1

    .line 319
    goto :goto_19

    .line 329
    :cond_30
    if-eqz v1, :cond_39

    .line 330
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 333
    :cond_39
    return v0

    .line 329
    :catchall_3a
    move-exception v0

    if-eqz v1, :cond_44

    .line 330
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_44
    throw v0
.end method

.method private static b()Ljava/util/concurrent/ConcurrentLinkedQueue;
    .registers 1

    .prologue
    .line 86
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .registers 3

    .prologue
    .line 95
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    .line 96
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>(Ljava/util/Collection;)V

    .line 100
    :goto_d
    return-object v0

    .line 98
    :cond_e
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    .line 99
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_d
.end method

.method private static b(I)Ljava/util/concurrent/LinkedBlockingDeque;
    .registers 2

    .prologue
    .line 121
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0, p0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>(I)V

    return-object v0
.end method

.method private static c()Ljava/util/concurrent/LinkedBlockingDeque;
    .registers 1

    .prologue
    .line 111
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    return-object v0
.end method

.method private static c(Ljava/lang/Iterable;)Ljava/util/concurrent/LinkedBlockingDeque;
    .registers 3

    .prologue
    .line 132
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    .line 133
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>(Ljava/util/Collection;)V

    .line 137
    :goto_d
    return-object v0

    .line 135
    :cond_e
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    .line 136
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_d
.end method

.method private static c(I)Ljava/util/concurrent/LinkedBlockingQueue;
    .registers 2

    .prologue
    .line 155
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0, p0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    return-object v0
.end method

.method private static d()Ljava/util/concurrent/LinkedBlockingQueue;
    .registers 1

    .prologue
    .line 146
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    return-object v0
.end method

.method private static d(Ljava/lang/Iterable;)Ljava/util/concurrent/LinkedBlockingQueue;
    .registers 3

    .prologue
    .line 167
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    .line 168
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(Ljava/util/Collection;)V

    .line 172
    :goto_d
    return-object v0

    .line 170
    :cond_e
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 171
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_d
.end method

.method private static e()Ljava/util/concurrent/PriorityBlockingQueue;
    .registers 1

    .prologue
    .line 186
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    return-object v0
.end method

.method private static e(Ljava/lang/Iterable;)Ljava/util/concurrent/PriorityBlockingQueue;
    .registers 3

    .prologue
    .line 199
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    .line 200
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(Ljava/util/Collection;)V

    .line 204
    :goto_d
    return-object v0

    .line 202
    :cond_e
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    .line 203
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_d
.end method

.method private static f()Ljava/util/PriorityQueue;
    .registers 1

    .prologue
    .line 216
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    return-object v0
.end method

.method private static f(Ljava/lang/Iterable;)Ljava/util/PriorityQueue;
    .registers 3

    .prologue
    .line 229
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    .line 230
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/PriorityQueue;-><init>(Ljava/util/Collection;)V

    .line 234
    :goto_d
    return-object v0

    .line 232
    :cond_e
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    .line 233
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_d
.end method

.method private static g()Ljava/util/concurrent/SynchronousQueue;
    .registers 1

    .prologue
    .line 243
    new-instance v0, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v0}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    return-object v0
.end method
