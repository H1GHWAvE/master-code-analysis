.class final Lcom/a/b/d/li;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Iterator;

.field b:Ljava/util/Iterator;

.field final synthetic c:Lcom/a/b/d/lh;


# direct methods
.method constructor <init>(Lcom/a/b/d/lh;)V
    .registers 3

    .prologue
    .line 399
    iput-object p1, p0, Lcom/a/b/d/li;->c:Lcom/a/b/d/lh;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 400
    iget-object v0, p0, Lcom/a/b/d/li;->c:Lcom/a/b/d/lh;

    iget-object v0, v0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/li;->a:Ljava/util/Iterator;

    .line 401
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/li;->b:Ljava/util/Iterator;

    return-void
.end method

.method private c()Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 405
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/li;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 406
    iget-object v0, p0, Lcom/a/b/d/li;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 407
    iget-object v0, p0, Lcom/a/b/d/li;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v1, p0, Lcom/a/b/d/li;->c:Lcom/a/b/d/lh;

    invoke-static {v1}, Lcom/a/b/d/lh;->a(Lcom/a/b/d/lh;)Lcom/a/b/d/ep;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/du;->a(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)Lcom/a/b/d/du;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/du;->c()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/li;->b:Ljava/util/Iterator;

    goto :goto_0

    .line 409
    :cond_29
    invoke-virtual {p0}, Lcom/a/b/d/li;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 412
    :goto_2d
    return-object v0

    :cond_2e
    iget-object v0, p0, Lcom/a/b/d/li;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    goto :goto_2d
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 399
    .line 1405
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/li;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 1406
    iget-object v0, p0, Lcom/a/b/d/li;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1407
    iget-object v0, p0, Lcom/a/b/d/li;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v1, p0, Lcom/a/b/d/li;->c:Lcom/a/b/d/lh;

    invoke-static {v1}, Lcom/a/b/d/lh;->a(Lcom/a/b/d/lh;)Lcom/a/b/d/ep;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/du;->a(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)Lcom/a/b/d/du;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/du;->c()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/li;->b:Ljava/util/Iterator;

    goto :goto_0

    .line 1409
    :cond_29
    invoke-virtual {p0}, Lcom/a/b/d/li;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_2d
    return-object v0

    .line 1412
    :cond_2e
    iget-object v0, p0, Lcom/a/b/d/li;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    goto :goto_2d
.end method
