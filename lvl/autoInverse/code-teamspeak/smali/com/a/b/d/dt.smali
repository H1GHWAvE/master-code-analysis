.class final Lcom/a/b/d/dt;
.super Lcom/a/b/d/hp;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/SortedSet;

.field final b:Lcom/a/b/d/dm;


# direct methods
.method constructor <init>(Ljava/util/SortedSet;Lcom/a/b/d/dm;)V
    .registers 4

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/a/b/d/hp;-><init>()V

    .line 141
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    iput-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    .line 142
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dm;

    iput-object v0, p0, Lcom/a/b/d/dt;->b:Lcom/a/b/d/dm;

    .line 143
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 135
    .line 1145
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    .line 135
    return-object v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 158
    iget-object v0, p0, Lcom/a/b/d/dt;->b:Lcom/a/b/d/dm;

    invoke-interface {v0, p1}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    iget-object v1, p0, Lcom/a/b/d/dt;->b:Lcom/a/b/d/dm;

    invoke-static {p1, v1}, Lcom/a/b/d/dn;->b(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedSet;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 135
    .line 2145
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    .line 135
    return-object v0
.end method

.method protected final c()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 148
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/dt;->b:Lcom/a/b/d/dm;

    invoke-static {v0, v1}, Lcom/a/b/d/dn;->a(Ljava/util/SortedSet;Lcom/a/b/d/dm;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 135
    .line 3145
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    .line 135
    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 151
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/dt;->b:Lcom/a/b/d/dm;

    invoke-static {v0, v1}, Lcom/a/b/d/dn;->a(Ljava/util/SortedSet;Lcom/a/b/d/dm;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 155
    iget-object v0, p0, Lcom/a/b/d/dt;->a:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/dt;->b:Lcom/a/b/d/dm;

    invoke-static {v0, v1}, Lcom/a/b/d/dn;->a(Ljava/util/SortedSet;Lcom/a/b/d/dm;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method
