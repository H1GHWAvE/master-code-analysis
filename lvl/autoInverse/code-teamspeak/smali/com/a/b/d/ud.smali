.class final Lcom/a/b/d/ud;
.super Lcom/a/b/d/av;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation


# instance fields
.field private final a:Ljava/util/NavigableMap;

.field private final b:Lcom/a/b/b/co;

.field private final c:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/NavigableMap;Lcom/a/b/b/co;)V
    .registers 4

    .prologue
    .line 2896
    invoke-direct {p0}, Lcom/a/b/d/av;-><init>()V

    .line 2897
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    iput-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    .line 2898
    iput-object p2, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    .line 2899
    new-instance v0, Lcom/a/b/d/ty;

    invoke-direct {v0, p1, p2}, Lcom/a/b/d/ty;-><init>(Ljava/util/Map;Lcom/a/b/b/co;)V

    iput-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    .line 2900
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/ud;)Lcom/a/b/b/co;
    .registers 2

    .prologue
    .line 2884
    iget-object v0, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/d/ud;)Ljava/util/NavigableMap;
    .registers 2

    .prologue
    .line 2884
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    return-object v0
.end method


# virtual methods
.method final a()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 2931
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method final b()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 2936
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 2977
    iget-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2978
    return-void
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 2904
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2957
    iget-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final descendingMap()Ljava/util/NavigableMap;
    .registers 3

    .prologue
    .line 2997
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 2982
    iget-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2952
    iget-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 3009
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .registers 3

    .prologue
    .line 2946
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->d(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final navigableKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 2909
    new-instance v0, Lcom/a/b/d/ue;

    invoke-direct {v0, p0, p0}, Lcom/a/b/d/ue;-><init>(Lcom/a/b/d/ud;Ljava/util/NavigableMap;)V

    return-object v0
.end method

.method public final pollFirstEntry()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 2987
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    return-object v0
.end method

.method public final pollLastEntry()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 2992
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 2962
    iget-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 2972
    iget-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2973
    return-void
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2967
    iget-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 2941
    iget-object v0, p0, Lcom/a/b/d/ud;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public final subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 3003
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 3014
    iget-object v0, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final values()Ljava/util/Collection;
    .registers 4

    .prologue
    .line 2926
    new-instance v0, Lcom/a/b/d/ui;

    iget-object v1, p0, Lcom/a/b/d/ud;->a:Ljava/util/NavigableMap;

    iget-object v2, p0, Lcom/a/b/d/ud;->b:Lcom/a/b/b/co;

    invoke-direct {v0, p0, v1, v2}, Lcom/a/b/d/ui;-><init>(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/co;)V

    return-object v0
.end method
