.class final Lcom/a/b/d/xj;
.super Lcom/a/b/d/as;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/xc;

.field final synthetic b:Lcom/a/b/d/xc;


# direct methods
.method constructor <init>(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)V
    .registers 3

    .prologue
    .line 522
    iput-object p1, p0, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    iput-object p2, p0, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-direct {p0}, Lcom/a/b/d/as;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 540
    iget-object v0, p0, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-interface {v1, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method final b()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 550
    iget-object v0, p0, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 552
    iget-object v1, p0, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-interface {v1}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 554
    new-instance v2, Lcom/a/b/d/xk;

    invoke-direct {v2, p0, v0, v1}, Lcom/a/b/d/xk;-><init>(Lcom/a/b/d/xj;Ljava/util/Iterator;Ljava/util/Iterator;)V

    return-object v2
.end method

.method final c()I
    .registers 2

    .prologue
    .line 577
    invoke-virtual {p0}, Lcom/a/b/d/xj;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 525
    iget-object v0, p0, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method final e()Ljava/util/Set;
    .registers 3

    .prologue
    .line 545
    iget-object v0, p0, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-interface {v1}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 530
    iget-object v0, p0, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 535
    iget-object v0, p0, Lcom/a/b/d/xj;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->size()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/xj;->b:Lcom/a/b/d/xc;

    invoke-interface {v1}, Lcom/a/b/d/xc;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
