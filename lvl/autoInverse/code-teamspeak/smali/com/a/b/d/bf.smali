.class abstract Lcom/a/b/d/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/adv;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private transient a:Ljava/util/Set;

.field private transient b:Ljava/util/Collection;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 96
    invoke-virtual {p0, p1}, Lcom/a/b/d/bf;->e(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/a/b/d/adv;)V
    .registers 6

    .prologue
    .line 101
    invoke-interface {p1}, Lcom/a/b/d/adv;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adw;

    .line 102
    invoke-interface {v0}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0}, Lcom/a/b/d/bf;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 104
    :cond_24
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 69
    if-eqz v0, :cond_14

    invoke-static {v0, p2}, Lcom/a/b/d/sz;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 75
    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return-object v0

    :cond_e
    invoke-static {v0, p2}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_d
.end method

.method public b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/a/b/d/bf;->l()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/a/b/d/bf;->l()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 91
    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return-object v0

    :cond_e
    invoke-static {v0, p2}, Lcom/a/b/d/sz;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_d
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/a/b/d/bf;->k()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public c(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 59
    invoke-interface {v0, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 60
    const/4 v0, 0x1

    .line 63
    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public d()V
    .registers 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/a/b/d/bf;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->i(Ljava/util/Iterator;)V

    .line 86
    return-void
.end method

.method public e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/a/b/d/bf;->a:Ljava/util/Set;

    .line 111
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/bf;->f()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/bf;->a:Ljava/util/Set;

    :cond_a
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 203
    invoke-static {p0, p1}, Lcom/a/b/d/adx;->a(Lcom/a/b/d/adv;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method f()Ljava/util/Set;
    .registers 2

    .prologue
    .line 115
    new-instance v0, Lcom/a/b/d/bh;

    invoke-direct {v0, p0}, Lcom/a/b/d/bh;-><init>(Lcom/a/b/d/bf;)V

    return-object v0
.end method

.method abstract g()Ljava/util/Iterator;
.end method

.method public h()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/a/b/d/bf;->b:Ljava/util/Collection;

    .line 164
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/bf;->i()Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/bf;->b:Ljava/util/Collection;

    :cond_a
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/a/b/d/bf;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method

.method i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 168
    new-instance v0, Lcom/a/b/d/bi;

    invoke-direct {v0, p0}, Lcom/a/b/d/bi;-><init>(Lcom/a/b/d/bf;)V

    return-object v0
.end method

.method m_()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 172
    new-instance v0, Lcom/a/b/d/bg;

    invoke-virtual {p0}, Lcom/a/b/d/bf;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/bg;-><init>(Lcom/a/b/d/bf;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
