.class final Lcom/a/b/d/cr;
.super Ljava/util/AbstractCollection;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>(Lcom/a/b/d/jl;)V
    .registers 2

    .prologue
    .line 566
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 567
    iput-object p1, p0, Lcom/a/b/d/cr;->a:Lcom/a/b/d/jl;

    .line 568
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 583
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_d

    .line 584
    check-cast p1, Ljava/util/List;

    .line 585
    iget-object v0, p0, Lcom/a/b/d/cr;->a:Lcom/a/b/d/jl;

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    .line 587
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 575
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 579
    new-instance v0, Lcom/a/b/d/cs;

    iget-object v1, p0, Lcom/a/b/d/cr;->a:Lcom/a/b/d/jl;

    invoke-direct {v0, v1}, Lcom/a/b/d/cs;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 571
    iget-object v0, p0, Lcom/a/b/d/cr;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/j/g;->a(I)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 591
    iget-object v0, p0, Lcom/a/b/d/cr;->a:Lcom/a/b/d/jl;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "permutations("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
