.class Lcom/a/b/d/aa;
.super Lcom/a/b/d/u;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field final synthetic c:Lcom/a/b/d/n;


# direct methods
.method constructor <init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
    .registers 3

    .prologue
    .line 983
    iput-object p1, p0, Lcom/a/b/d/aa;->c:Lcom/a/b/d/n;

    .line 984
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/u;-><init>(Lcom/a/b/d/n;Ljava/util/Map;)V

    .line 985
    return-void
.end method


# virtual methods
.method a()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 988
    invoke-super {p0}, Lcom/a/b/d/u;->b()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 993
    invoke-virtual {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 998
    invoke-virtual {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 1003
    new-instance v0, Lcom/a/b/d/aa;

    iget-object v1, p0, Lcom/a/b/d/aa;->c:Lcom/a/b/d/n;

    invoke-virtual {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aa;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1008
    invoke-virtual {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 6

    .prologue
    .line 1013
    new-instance v0, Lcom/a/b/d/aa;

    iget-object v1, p0, Lcom/a/b/d/aa;->c:Lcom/a/b/d/n;

    invoke-virtual {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aa;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 1018
    new-instance v0, Lcom/a/b/d/aa;

    iget-object v1, p0, Lcom/a/b/d/aa;->c:Lcom/a/b/d/n;

    invoke-virtual {p0}, Lcom/a/b/d/aa;->a()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aa;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    return-object v0
.end method
