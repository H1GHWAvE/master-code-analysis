.class public abstract Lcom/a/b/d/gp;
.super Lcom/a/b/d/gh;
.source "SourceFile"

# interfaces
.implements Ljava/util/List;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    return-void
.end method

.method private a(II)Ljava/util/List;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 215
    .line 2010
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_e

    .line 2011
    new-instance v0, Lcom/a/b/d/ow;

    invoke-direct {v0, p0}, Lcom/a/b/d/ow;-><init>(Ljava/util/List;)V

    .line 2027
    :goto_9
    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 215
    return-object v0

    .line 2019
    :cond_e
    new-instance v0, Lcom/a/b/d/ox;

    invoke-direct {v0, p0}, Lcom/a/b/d/ox;-><init>(Ljava/util/List;)V

    goto :goto_9
.end method

.method private a(I)Ljava/util/ListIterator;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 204
    .line 2001
    new-instance v0, Lcom/a/b/d/oy;

    invoke-direct {v0, p0}, Lcom/a/b/d/oy;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, p1}, Lcom/a/b/d/oy;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 204
    return-object v0
.end method

.method private a(ILjava/lang/Iterable;)Z
    .registers 7

    .prologue
    .line 145
    .line 1962
    const/4 v0, 0x0

    .line 1963
    invoke-interface {p0, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    .line 1964
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_18

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1965
    invoke-interface {v1, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 1966
    const/4 v0, 0x1

    .line 1967
    goto :goto_9

    .line 145
    :cond_18
    return v0
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/a/b/d/gp;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/a/b/d/gp;->add(ILjava/lang/Object;)V

    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method private c()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/a/b/d/gp;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 156
    invoke-static {p0, p1}, Lcom/a/b/d/ov;->b(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private d()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/b/d/gp;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method private e()I
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 237
    .line 2930
    const/4 v0, 0x1

    .line 2931
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2932
    mul-int/lit8 v3, v0, 0x1f

    if-nez v2, :cond_1a

    const/4 v0, 0x0

    :goto_14
    add-int/2addr v0, v3

    .line 2934
    xor-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    .line 2936
    goto :goto_5

    .line 2932
    :cond_1a
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_14

    .line 237
    :cond_1f
    return v0
.end method

.method private e(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 168
    invoke-static {p0, p1}, Lcom/a/b/d/ov;->c(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private f(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 226
    invoke-static {p0, p1}, Lcom/a/b/d/ov;->a(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected abstract a()Ljava/util/List;
.end method

.method public add(ILjava/lang/Object;)V
    .registers 4

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 67
    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .registers 4

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method protected synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115
    if-eq p1, p0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public get(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subList(II)Ljava/util/List;
    .registers 4

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/a/b/d/gp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
