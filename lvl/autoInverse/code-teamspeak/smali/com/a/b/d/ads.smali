.class Lcom/a/b/d/ads;
.super Lcom/a/b/d/adi;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# static fields
.field private static final a:J


# direct methods
.method constructor <init>(Ljava/util/SortedMap;Ljava/lang/Object;)V
    .registers 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1057
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/adi;-><init>(Ljava/util/Map;Ljava/lang/Object;)V

    .line 1058
    return-void
.end method


# virtual methods
.method synthetic a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 1053
    invoke-virtual {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method b()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 1061
    invoke-super {p0}, Lcom/a/b/d/adi;->a()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 3

    .prologue
    .line 1065
    iget-object v1, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1066
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1067
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1053
    invoke-virtual {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1071
    iget-object v1, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1072
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1073
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 1077
    iget-object v1, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1078
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/SortedMap;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1079
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public lastKey()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1083
    iget-object v1, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1084
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1085
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 6

    .prologue
    .line 1089
    iget-object v1, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1090
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/SortedMap;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1091
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 1095
    iget-object v1, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1096
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/ads;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/SortedMap;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1097
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method
