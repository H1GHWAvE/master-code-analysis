.class public Lcom/a/b/d/afm;
.super Lcom/a/b/d/ay;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/c;
    a = "uses NavigableMap"
.end annotation


# instance fields
.field final a:Ljava/util/NavigableMap;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field private transient b:Ljava/util/Set;

.field private transient c:Lcom/a/b/d/yr;


# direct methods
.method private constructor <init>(Ljava/util/NavigableMap;)V
    .registers 2

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/a/b/d/ay;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/NavigableMap;B)V
    .registers 3

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/a/b/d/afm;-><init>(Ljava/util/NavigableMap;)V

    return-void
.end method

.method private static synthetic a(Lcom/a/b/d/afm;Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 1117
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1118
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 1119
    if-eqz v1, :cond_20

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    :goto_1f
    return-object v0

    :cond_20
    const/4 v0, 0x0

    .line 44
    goto :goto_1f
.end method

.method public static c()Lcom/a/b/d/afm;
    .registers 2

    .prologue
    .line 54
    new-instance v0, Lcom/a/b/d/afm;

    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    invoke-direct {v0, v1}, Lcom/a/b/d/afm;-><init>(Ljava/util/NavigableMap;)V

    return-object v0
.end method

.method private static d(Lcom/a/b/d/yr;)Lcom/a/b/d/afm;
    .registers 2

    .prologue
    .line 61
    invoke-static {}, Lcom/a/b/d/afm;->c()Lcom/a/b/d/afm;

    move-result-object v0

    .line 1042
    invoke-super {v0, p0}, Lcom/a/b/d/ay;->b(Lcom/a/b/d/yr;)V

    .line 63
    return-object v0
.end method

.method private d(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 117
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 119
    if-eqz v1, :cond_20

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    :goto_1f
    return-object v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method private f(Lcom/a/b/d/yl;)V
    .registers 4

    .prologue
    .line 225
    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 226
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    :goto_d
    return-void

    .line 228
    :cond_e
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1, p1}, Ljava/util/NavigableMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d
.end method


# virtual methods
.method public a(Lcom/a/b/d/yl;)V
    .registers 7

    .prologue
    .line 136
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 179
    :goto_9
    return-void

    .line 144
    :cond_a
    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    .line 145
    iget-object v1, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 147
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, v2}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_5b

    .line 150
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 151
    iget-object v3, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v3, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v3

    if-ltz v3, :cond_5b

    .line 153
    iget-object v2, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v2, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v2

    if-ltz v2, :cond_2e

    .line 155
    iget-object v1, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 161
    :cond_2e
    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    move-object v2, v1

    move-object v1, v0

    .line 165
    :goto_32
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, v2}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_4a

    .line 168
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 169
    iget-object v3, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v3, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v3

    if-ltz v3, :cond_4a

    .line 171
    iget-object v2, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 176
    :cond_4a
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    .line 178
    invoke-static {v1, v2}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afm;->f(Lcom/a/b/d/yl;)V

    goto :goto_9

    :cond_5b
    move-object v4, v2

    move-object v2, v1

    move-object v1, v4

    goto :goto_32
.end method

.method public final bridge synthetic a()Z
    .registers 2

    .prologue
    .line 42
    invoke-super {p0}, Lcom/a/b/d/ay;->a()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/yr;)Z
    .registers 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/a/b/d/ay;->a(Lcom/a/b/d/yr;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/a/b/d/ay;->a(Ljava/lang/Comparable;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 98
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    invoke-static {p1}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 100
    if-eqz v1, :cond_22

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 101
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 104
    :goto_21
    return-object v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public bridge synthetic b()V
    .registers 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/a/b/d/ay;->b()V

    return-void
.end method

.method public b(Lcom/a/b/d/yl;)V
    .registers 5

    .prologue
    .line 183
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 222
    :goto_9
    return-void

    .line 192
    :cond_a
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_4a

    .line 195
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 196
    iget-object v1, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-ltz v1, :cond_4a

    .line 198
    invoke-virtual {p1}, Lcom/a/b/d/yl;->e()Z

    move-result v1

    if-eqz v1, :cond_3f

    iget-object v1, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-ltz v1, :cond_3f

    .line 201
    iget-object v1, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v1, v2}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/a/b/d/afm;->f(Lcom/a/b/d/yl;)V

    .line 204
    :cond_3f
    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afm;->f(Lcom/a/b/d/yl;)V

    .line 209
    :cond_4a
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_75

    .line 212
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 213
    invoke-virtual {p1}, Lcom/a/b/d/yl;->e()Z

    move-result v1

    if-eqz v1, :cond_75

    iget-object v1, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-ltz v1, :cond_75

    .line 216
    iget-object v1, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afm;->f(Lcom/a/b/d/yl;)V

    .line 221
    :cond_75
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    goto :goto_9
.end method

.method public final bridge synthetic b(Lcom/a/b/d/yr;)V
    .registers 2

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/a/b/d/ay;->b(Lcom/a/b/d/yr;)V

    return-void
.end method

.method public final bridge synthetic c(Lcom/a/b/d/yr;)V
    .registers 2

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/a/b/d/ay;->c(Lcom/a/b/d/yr;)V

    return-void
.end method

.method public c(Lcom/a/b/d/yl;)Z
    .registers 4

    .prologue
    .line 110
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_1b

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public final e()Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 126
    iget-object v0, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    invoke-interface {v1}, Ljava/util/NavigableMap;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v1

    .line 128
    if-nez v0, :cond_14

    .line 129
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 131
    :cond_14
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v2, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v2, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public e(Lcom/a/b/d/yl;)Lcom/a/b/d/yr;
    .registers 3

    .prologue
    .line 785
    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    :goto_a
    return-object p0

    :cond_b
    new-instance v0, Lcom/a/b/d/afw;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/afw;-><init>(Lcom/a/b/d/afm;Lcom/a/b/d/yl;)V

    move-object p0, v0

    goto :goto_a
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/a/b/d/ay;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Lcom/a/b/d/yr;
    .registers 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/a/b/d/afm;->c:Lcom/a/b/d/yr;

    .line 237
    if-nez v0, :cond_b

    new-instance v0, Lcom/a/b/d/afp;

    invoke-direct {v0, p0}, Lcom/a/b/d/afp;-><init>(Lcom/a/b/d/afm;)V

    iput-object v0, p0, Lcom/a/b/d/afm;->c:Lcom/a/b/d/yr;

    :cond_b
    return-object v0
.end method

.method public final g()Ljava/util/Set;
    .registers 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/a/b/d/afm;->b:Ljava/util/Set;

    .line 75
    if-nez v0, :cond_b

    new-instance v0, Lcom/a/b/d/afo;

    invoke-direct {v0, p0}, Lcom/a/b/d/afo;-><init>(Lcom/a/b/d/afm;)V

    iput-object v0, p0, Lcom/a/b/d/afm;->b:Ljava/util/Set;

    :cond_b
    return-object v0
.end method
