.class Lcom/a/b/d/wv;
.super Lcom/a/b/d/an;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/vi;

.field final b:Lcom/a/b/d/tv;


# direct methods
.method constructor <init>(Lcom/a/b/d/vi;Lcom/a/b/d/tv;)V
    .registers 4

    .prologue
    .line 1181
    invoke-direct {p0}, Lcom/a/b/d/an;-><init>()V

    .line 1182
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    iput-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    .line 1183
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/tv;

    iput-object v0, p0, Lcom/a/b/d/wv;->b:Lcom/a/b/d/tv;

    .line 1184
    return-void
.end method


# virtual methods
.method a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .registers 5

    .prologue
    .line 1187
    iget-object v0, p0, Lcom/a/b/d/wv;->b:Lcom/a/b/d/tv;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/d/tv;Ljava/lang/Object;)Lcom/a/b/b/bj;

    move-result-object v0

    .line 1189
    instance-of v1, p2, Ljava/util/List;

    if-eqz v1, :cond_11

    .line 1190
    check-cast p2, Ljava/util/List;

    invoke-static {p2, v0}, Lcom/a/b/d/ov;->a(Ljava/util/List;Lcom/a/b/b/bj;)Ljava/util/List;

    move-result-object v0

    .line 1192
    :goto_10
    return-object v0

    :cond_11
    invoke-static {p2, v0}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_10
.end method

.method public final a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 1247
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1238
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 1262
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/wv;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 1242
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1252
    invoke-virtual {p0, p1}, Lcom/a/b/d/wv;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 1257
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/wv;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->f()I

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1212
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 1208
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->g()V

    .line 1209
    return-void
.end method

.method final l()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/wv;->b:Lcom/a/b/d/tv;

    invoke-static {v1}, Lcom/a/b/d/sz;->b(Lcom/a/b/d/tv;)Lcom/a/b/b/bj;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method final m()Ljava/util/Map;
    .registers 3

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/ww;

    invoke-direct {v1, p0}, Lcom/a/b/d/ww;-><init>(Lcom/a/b/d/wv;)V

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/d/tv;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .registers 2

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->n()Z

    move-result v0

    return v0
.end method

.method public final p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 1234
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->q()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method final s()Ljava/util/Collection;
    .registers 3

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/a/b/d/wv;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/wv;->b:Lcom/a/b/d/tv;

    invoke-static {v1}, Lcom/a/b/d/sz;->a(Lcom/a/b/d/tv;)Lcom/a/b/b/bj;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
