.class final Lcom/a/b/d/aco;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final synthetic a:Ljava/util/Iterator;

.field final synthetic b:Lcom/a/b/d/acn;


# direct methods
.method constructor <init>(Lcom/a/b/d/acn;Ljava/util/Iterator;)V
    .registers 3

    .prologue
    .line 355
    iput-object p1, p0, Lcom/a/b/d/aco;->b:Lcom/a/b/d/acn;

    iput-object p2, p0, Lcom/a/b/d/aco;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 360
    iget-object v0, p0, Lcom/a/b/d/aco;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 361
    new-instance v1, Lcom/a/b/d/acp;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/acp;-><init>(Lcom/a/b/d/aco;Ljava/util/Map$Entry;)V

    return-object v1
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/a/b/d/aco;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 355
    .line 1360
    iget-object v0, p0, Lcom/a/b/d/aco;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1361
    new-instance v1, Lcom/a/b/d/acp;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/acp;-><init>(Lcom/a/b/d/aco;Ljava/util/Map$Entry;)V

    .line 355
    return-object v1
.end method

.method public final remove()V
    .registers 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/a/b/d/aco;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 379
    iget-object v0, p0, Lcom/a/b/d/aco;->b:Lcom/a/b/d/acn;

    iget-object v0, v0, Lcom/a/b/d/acn;->a:Lcom/a/b/d/acm;

    invoke-virtual {v0}, Lcom/a/b/d/acm;->f()V

    .line 380
    return-void
.end method
