.class public final Lcom/a/b/d/aad;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/Set;)I
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 1340
    .line 1341
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v0, v1

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1342
    if-eqz v2, :cond_1c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_16
    add-int/2addr v0, v2

    .line 1344
    xor-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    .line 1346
    goto :goto_6

    :cond_1c
    move v2, v1

    .line 1342
    goto :goto_16

    .line 1347
    :cond_1e
    return v0
.end method

.method public static a(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
    .registers 4

    .prologue
    .line 581
    const-string v0, "set1"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    const-string v0, "set2"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    invoke-static {p1, p0}, Lcom/a/b/d/aad;->c(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;

    move-result-object v0

    .line 586
    new-instance v1, Lcom/a/b/d/aae;

    invoke-direct {v1, p0, v0, p1}, Lcom/a/b/d/aae;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-object v1
.end method

.method private static varargs a(Ljava/lang/Enum;[Ljava/lang/Enum;)Lcom/a/b/d/lo;
    .registers 3
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 98
    invoke-static {p0, p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ji;->a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;
    .registers 3
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 116
    instance-of v0, p0, Lcom/a/b/d/ji;

    if-eqz v0, :cond_7

    .line 117
    check-cast p0, Lcom/a/b/d/ji;

    .line 132
    :goto_6
    return-object p0

    .line 118
    :cond_7
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_21

    .line 119
    check-cast p0, Ljava/util/Collection;

    .line 120
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 121
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object p0

    goto :goto_6

    .line 123
    :cond_18
    invoke-static {p0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ji;->a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;

    move-result-object p0

    goto :goto_6

    .line 126
    :cond_21
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 127
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 128
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 129
    invoke-static {v0, v1}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 130
    invoke-static {v0}, Lcom/a/b/d/ji;->a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;

    move-result-object p0

    goto :goto_6

    .line 132
    :cond_3d
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object p0

    goto :goto_6
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/util/EnumSet;
    .registers 3

    .prologue
    .line 145
    invoke-static {p1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 146
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 147
    return-object v0
.end method

.method private static a(Ljava/util/Collection;)Ljava/util/EnumSet;
    .registers 3

    .prologue
    .line 447
    instance-of v0, p0, Ljava/util/EnumSet;

    if-eqz v0, :cond_b

    .line 448
    check-cast p0, Ljava/util/EnumSet;

    invoke-static {p0}, Ljava/util/EnumSet;->complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    .line 453
    :goto_a
    return-object v0

    .line 450
    :cond_b
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2a

    const/4 v0, 0x1

    :goto_12
    const-string v1, "collection is empty; use the other version of this method"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 452
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    .line 453
    invoke-static {p0, v0}, Lcom/a/b/d/aad;->b(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_a

    .line 450
    :cond_2a
    const/4 v0, 0x0

    goto :goto_12
.end method

.method private static a(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/EnumSet;
    .registers 3

    .prologue
    .line 470
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    instance-of v0, p0, Ljava/util/EnumSet;

    if-eqz v0, :cond_e

    check-cast p0, Ljava/util/EnumSet;

    invoke-static {p0}, Ljava/util/EnumSet;->complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    :goto_d
    return-object v0

    :cond_e
    invoke-static {p0, p1}, Lcom/a/b/d/aad;->b(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_d
.end method

.method private static a()Ljava/util/HashSet;
    .registers 1

    .prologue
    .line 164
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    return-object v0
.end method

.method public static a(I)Ljava/util/HashSet;
    .registers 3

    .prologue
    .line 201
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p0}, Lcom/a/b/d/sz;->b(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;)Ljava/util/HashSet;
    .registers 2

    .prologue
    .line 4164
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 238
    invoke-static {v0, p0}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 239
    return-object v0
.end method

.method private static varargs a([Ljava/lang/Object;)Ljava/util/HashSet;
    .registers 2

    .prologue
    .line 182
    array-length v0, p0

    invoke-static {v0}, Lcom/a/b/d/aad;->a(I)Ljava/util/HashSet;

    move-result-object v0

    .line 183
    invoke-static {v0, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 184
    return-object v0
.end method

.method public static a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 1390
    instance-of v0, p0, Lcom/a/b/d/me;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/aat;

    if-eqz v0, :cond_9

    .line 1394
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/aat;

    invoke-direct {v0, p0}, Lcom/a/b/d/aat;-><init>(Ljava/util/NavigableSet;)V

    move-object p0, v0

    goto :goto_8
.end method

.method public static a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 908
    instance-of v0, p0, Lcom/a/b/d/aal;

    if-eqz v0, :cond_17

    .line 911
    check-cast p0, Lcom/a/b/d/aal;

    .line 912
    iget-object v0, p0, Lcom/a/b/d/aal;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 914
    new-instance v1, Lcom/a/b/d/aak;

    iget-object v0, p0, Lcom/a/b/d/aal;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aak;-><init>(Ljava/util/NavigableSet;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 918
    :goto_16
    return-object v0

    :cond_17
    new-instance v2, Lcom/a/b/d/aak;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-direct {v2, v0, v1}, Lcom/a/b/d/aak;-><init>(Ljava/util/NavigableSet;Lcom/a/b/b/co;)V

    move-object v0, v2

    goto :goto_16
.end method

.method private static a(Ljava/util/List;)Ljava/util/Set;
    .registers 2

    .prologue
    .line 1055
    invoke-static {p0}, Lcom/a/b/d/aah;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Map;)Ljava/util/Set;
    .registers 2

    .prologue
    .line 515
    .line 8058
    invoke-static {p0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 515
    return-object v0
.end method

.method public static a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;
    .registers 5

    .prologue
    .line 747
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_3b

    .line 748
    check-cast p0, Ljava/util/SortedSet;

    .line 9088
    instance-of v0, p0, Ljava/util/NavigableSet;

    if-eqz v0, :cond_11

    check-cast p0, Ljava/util/NavigableSet;

    invoke-static {p0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 9821
    :goto_10
    return-object v0

    .line 9815
    :cond_11
    instance-of v0, p0, Lcom/a/b/d/aal;

    if-eqz v0, :cond_28

    .line 9818
    check-cast p0, Lcom/a/b/d/aal;

    .line 9819
    iget-object v0, p0, Lcom/a/b/d/aal;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 9821
    new-instance v1, Lcom/a/b/d/aam;

    iget-object v0, p0, Lcom/a/b/d/aal;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_10

    .line 9825
    :cond_28
    new-instance v2, Lcom/a/b/d/aam;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-direct {v2, v0, v1}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    move-object v0, v2

    .line 748
    goto :goto_10

    .line 750
    :cond_3b
    instance-of v0, p0, Lcom/a/b/d/aal;

    if-eqz v0, :cond_52

    .line 753
    check-cast p0, Lcom/a/b/d/aal;

    .line 754
    iget-object v0, p0, Lcom/a/b/d/aal;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 756
    new-instance v1, Lcom/a/b/d/aal;

    iget-object v0, p0, Lcom/a/b/d/aal;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/Set;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aal;-><init>(Ljava/util/Set;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_10

    .line 760
    :cond_52
    new-instance v2, Lcom/a/b/d/aal;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-direct {v2, v0, v1}, Lcom/a/b/d/aal;-><init>(Ljava/util/Set;Lcom/a/b/b/co;)V

    move-object v0, v2

    goto :goto_10
.end method

.method private static varargs a([Ljava/util/Set;)Ljava/util/Set;
    .registers 2

    .prologue
    .line 1115
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 11055
    invoke-static {v0}, Lcom/a/b/d/aah;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    .line 1115
    return-object v0
.end method

.method private static a(Ljava/util/SortedSet;Lcom/a/b/b/co;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 810
    .line 10088
    instance-of v0, p0, Ljava/util/NavigableSet;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/NavigableSet;

    invoke-static {p0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 10821
    :goto_a
    return-object v0

    .line 10815
    :cond_b
    instance-of v0, p0, Lcom/a/b/d/aal;

    if-eqz v0, :cond_22

    .line 10818
    check-cast p0, Lcom/a/b/d/aal;

    .line 10819
    iget-object v0, p0, Lcom/a/b/d/aal;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 10821
    new-instance v1, Lcom/a/b/d/aam;

    iget-object v0, p0, Lcom/a/b/d/aal;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_a

    .line 10825
    :cond_22
    new-instance v2, Lcom/a/b/d/aam;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-direct {v2, v0, v1}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    move-object v0, v2

    .line 810
    goto :goto_a
.end method

.method private static a(Ljava/util/Comparator;)Ljava/util/TreeSet;
    .registers 3

    .prologue
    .line 381
    new-instance v1, Ljava/util/TreeSet;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    return-object v1
.end method

.method static a(Ljava/util/Set;Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1354
    if-ne p0, p1, :cond_5

    .line 1368
    :cond_4
    :goto_4
    return v0

    .line 1357
    :cond_5
    instance-of v2, p1, Ljava/util/Set;

    if-eqz v2, :cond_23

    .line 1358
    check-cast p1, Ljava/util/Set;

    .line 1361
    :try_start_b
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v2, v3, :cond_1b

    invoke-interface {p0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z
    :try_end_18
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_18} :catch_1d
    .catch Ljava/lang/ClassCastException; {:try_start_b .. :try_end_18} :catch_20

    move-result v2

    if-nez v2, :cond_4

    :cond_1b
    move v0, v1

    goto :goto_4

    .line 1363
    :catch_1d
    move-exception v0

    move v0, v1

    goto :goto_4

    .line 1365
    :catch_20
    move-exception v0

    move v0, v1

    goto :goto_4

    :cond_23
    move v0, v1

    .line 1368
    goto :goto_4
.end method

.method static a(Ljava/util/Set;Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 1547
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1548
    instance-of v0, p1, Lcom/a/b/d/xc;

    if-eqz v0, :cond_d

    .line 1549
    check-cast p1, Lcom/a/b/d/xc;

    invoke-interface {p1}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object p1

    .line 1558
    :cond_d
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_24

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v1

    if-le v0, v1, :cond_24

    .line 1559
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    .line 1561
    :goto_23
    return v0

    :cond_24
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/util/Iterator;)Z

    move-result v0

    goto :goto_23
.end method

.method static a(Ljava/util/Set;Ljava/util/Iterator;)Z
    .registers 4

    .prologue
    .line 1539
    const/4 v0, 0x0

    .line 1540
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1541
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_1

    .line 1543
    :cond_11
    return v0
.end method

.method public static b(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
    .registers 4

    .prologue
    .line 640
    const-string v0, "set1"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    const-string v0, "set2"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 643
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    .line 644
    new-instance v1, Lcom/a/b/d/aaf;

    invoke-direct {v1, p0, v0, p1}, Lcom/a/b/d/aaf;-><init>(Ljava/util/Set;Lcom/a/b/b/co;Ljava/util/Set;)V

    return-object v1
.end method

.method private static b(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/EnumSet;
    .registers 3

    .prologue
    .line 478
    invoke-static {p1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 479
    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->removeAll(Ljava/util/Collection;)Z

    .line 480
    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/HashSet;
    .registers 3

    .prologue
    .line 218
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/util/HashSet;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :goto_d
    return-object v0

    :cond_e
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 3164
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2238
    invoke-static {v0, v1}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    goto :goto_d
.end method

.method private static b(I)Ljava/util/LinkedHashSet;
    .registers 3

    .prologue
    .line 308
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-static {p0}, Lcom/a/b/d/sz;->b(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    return-object v0
.end method

.method private static b(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 1532
    .line 11335
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1532
    return-object v0
.end method

.method private static b()Ljava/util/Set;
    .registers 1

    .prologue
    .line 254
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 5058
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 254
    return-object v0
.end method

.method private static b(Ljava/util/Set;)Ljava/util/Set;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = false
    .end annotation

    .prologue
    .line 1229
    new-instance v0, Lcom/a/b/d/aao;

    invoke-direct {v0, p0}, Lcom/a/b/d/aao;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method private static b(Ljava/util/SortedSet;Lcom/a/b/b/co;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 815
    instance-of v0, p0, Lcom/a/b/d/aal;

    if-eqz v0, :cond_17

    .line 818
    check-cast p0, Lcom/a/b/d/aal;

    .line 819
    iget-object v0, p0, Lcom/a/b/d/aal;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 821
    new-instance v1, Lcom/a/b/d/aam;

    iget-object v0, p0, Lcom/a/b/d/aal;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 825
    :goto_16
    return-object v0

    :cond_17
    new-instance v2, Lcom/a/b/d/aam;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-direct {v2, v0, v1}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    move-object v0, v2

    goto :goto_16
.end method

.method private static c(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
    .registers 4

    .prologue
    .line 677
    const-string v0, "set1"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 678
    const-string v0, "set2"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    .line 681
    new-instance v1, Lcom/a/b/d/aag;

    invoke-direct {v1, p0, v0, p1}, Lcom/a/b/d/aag;-><init>(Ljava/util/Set;Lcom/a/b/b/co;Ljava/util/Set;)V

    return-object v1
.end method

.method private static c()Ljava/util/LinkedHashSet;
    .registers 1

    .prologue
    .line 289
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    return-object v0
.end method

.method private static c(Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 2

    .prologue
    .line 5254
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 6058
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 274
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 275
    return-object v0
.end method

.method private static d(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
    .registers 4

    .prologue
    .line 711
    const-string v0, "set1"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 712
    const-string v0, "set2"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    invoke-static {p0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/a/b/d/aad;->b(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->c(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;
    .registers 3

    .prologue
    .line 324
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    .line 325
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 329
    :goto_d
    return-object v0

    .line 6289
    :cond_e
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 328
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_d
.end method

.method private static d()Ljava/util/TreeSet;
    .registers 1

    .prologue
    .line 344
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    return-object v0
.end method

.method private static e()Ljava/util/Set;
    .registers 1

    .prologue
    .line 395
    invoke-static {}, Lcom/a/b/d/sz;->f()Ljava/util/IdentityHashMap;

    move-result-object v0

    .line 7058
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 395
    return-object v0
.end method

.method private static e(Ljava/lang/Iterable;)Ljava/util/TreeSet;
    .registers 2

    .prologue
    .line 6344
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 365
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 366
    return-object v0
.end method

.method private static f()Ljava/util/concurrent/CopyOnWriteArraySet;
    .registers 1
    .annotation build Lcom/a/b/a/c;
        a = "CopyOnWriteArraySet"
    .end annotation

    .prologue
    .line 409
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    return-object v0
.end method

.method private static f(Ljava/lang/Iterable;)Ljava/util/concurrent/CopyOnWriteArraySet;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "CopyOnWriteArraySet"
    .end annotation

    .prologue
    .line 424
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    .line 427
    :goto_8
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v1, v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>(Ljava/util/Collection;)V

    return-object v1

    .line 424
    :cond_e
    invoke-static {p0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_8
.end method
