.class final Lcom/a/b/d/ke;
.super Lcom/a/b/d/lo;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field private final a:Lcom/a/b/d/jt;


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;)V
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/a/b/d/lo;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/a/b/d/ke;->a:Lcom/a/b/d/jt;

    .line 39
    return-void
.end method


# virtual methods
.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/a/b/d/ke;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/b/d/ke;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final g()Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "serialization"
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lcom/a/b/d/kg;

    iget-object v1, p0, Lcom/a/b/d/ke;->a:Lcom/a/b/d/jt;

    invoke-direct {v0, v1}, Lcom/a/b/d/kg;-><init>(Lcom/a/b/d/jt;)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 33
    .line 1048
    invoke-virtual {p0}, Lcom/a/b/d/ke;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    .line 33
    return-object v0
.end method

.method final l()Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/a/b/d/ke;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/a/b/d/kf;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/kf;-><init>(Lcom/a/b/d/ke;Lcom/a/b/d/jl;)V

    return-object v1
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/a/b/d/ke;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    return v0
.end method
