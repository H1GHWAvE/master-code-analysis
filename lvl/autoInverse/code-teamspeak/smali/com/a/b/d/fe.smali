.class public final Lcom/a/b/d/fe;
.super Lcom/a/b/d/a;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final c:J
    .annotation build Lcom/a/b/a/c;
        a = "only needed in emulated source."
    .end annotation
.end field


# instance fields
.field transient b:Ljava/lang/Class;


# direct methods
.method private constructor <init>(Ljava/lang/Class;)V
    .registers 4

    .prologue
    .line 79
    new-instance v0, Ljava/util/EnumMap;

    invoke-direct {v0, p1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/a/b/d/agm;->a(Ljava/util/Map;)Lcom/a/b/d/agm;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    array-length v0, v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/a/b/d/a;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    .line 83
    iput-object p1, p0, Lcom/a/b/d/fe;->b:Ljava/lang/Class;

    .line 84
    return-void
.end method

.method private static a(Ljava/lang/Class;)Lcom/a/b/d/fe;
    .registers 2

    .prologue
    .line 58
    new-instance v0, Lcom/a/b/d/fe;

    invoke-direct {v0, p0}, Lcom/a/b/d/fe;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/d/fe;
    .registers 3

    .prologue
    .line 73
    invoke-static {p0}, Lcom/a/b/d/fd;->a(Ljava/util/Map;)Ljava/lang/Class;

    move-result-object v0

    .line 1058
    new-instance v1, Lcom/a/b/d/fe;

    invoke-direct {v1, v0}, Lcom/a/b/d/fe;-><init>(Ljava/lang/Class;)V

    .line 74
    invoke-virtual {v1, p0}, Lcom/a/b/d/fe;->putAll(Ljava/util/Map;)V

    .line 75
    return-object v1
.end method

.method private static a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .registers 2

    .prologue
    .line 90
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    return-object v0
.end method

.method private a(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lcom/a/b/d/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 122
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/a/b/d/fe;->b:Ljava/lang/Class;

    .line 123
    new-instance v0, Ljava/util/EnumMap;

    iget-object v1, p0, Lcom/a/b/d/fe;->b:Ljava/lang/Class;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/a/b/d/agm;->a(Ljava/util/Map;)Lcom/a/b/d/agm;

    move-result-object v1

    new-instance v2, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/a/b/d/fe;->b:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    invoke-virtual {p0, v1, v2}, Lcom/a/b/d/fe;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 125
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Ljava/util/Map;Ljava/io/ObjectInputStream;)V

    .line 126
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 112
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 113
    iget-object v0, p0, Lcom/a/b/d/fe;->b:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 114
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Ljava/util/Map;Ljava/io/ObjectOutputStream;)V

    .line 115
    return-void
.end method

.method private b(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/a/b/d/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/lang/Class;
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/a/b/d/fe;->b:Ljava/lang/Class;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 46
    check-cast p1, Ljava/lang/Enum;

    .line 3090
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    .line 46
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 46
    check-cast p1, Ljava/lang/Enum;

    .line 1098
    invoke-super {p0, p1, p2}, Lcom/a/b/d/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method public final bridge synthetic b()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/a/b/d/a;->b()Lcom/a/b/d/bw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()V
    .registers 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/a/b/d/a;->clear()V

    return-void
.end method

.method public final bridge synthetic containsValue(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/a/b/d/a;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/a/b/d/a;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic j_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/a/b/d/a;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/a/b/d/a;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 46
    check-cast p1, Ljava/lang/Enum;

    .line 2094
    invoke-super {p0, p1, p2}, Lcom/a/b/d/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method public final bridge synthetic putAll(Ljava/util/Map;)V
    .registers 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/a/b/d/a;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/a/b/d/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
