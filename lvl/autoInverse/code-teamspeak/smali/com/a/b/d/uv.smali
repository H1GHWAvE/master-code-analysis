.class final Lcom/a/b/d/uv;
.super Lcom/a/b/d/gs;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/bw;
.implements Ljava/io/Serializable;


# static fields
.field private static final e:J


# instance fields
.field final a:Ljava/util/Map;

.field final b:Lcom/a/b/d/bw;

.field c:Lcom/a/b/d/bw;

.field transient d:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/a/b/d/bw;Lcom/a/b/d/bw;)V
    .registers 4
    .param p2    # Lcom/a/b/d/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1423
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    .line 1424
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/uv;->a:Ljava/util/Map;

    .line 1425
    iput-object p1, p0, Lcom/a/b/d/uv;->b:Lcom/a/b/d/bw;

    .line 1426
    iput-object p2, p0, Lcom/a/b/d/uv;->c:Lcom/a/b/d/bw;

    .line 1427
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1435
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 1430
    iget-object v0, p0, Lcom/a/b/d/uv;->a:Ljava/util/Map;

    return-object v0
.end method

.method public final b()Lcom/a/b/d/bw;
    .registers 3

    .prologue
    .line 1440
    iget-object v0, p0, Lcom/a/b/d/uv;->c:Lcom/a/b/d/bw;

    .line 1441
    if-nez v0, :cond_11

    new-instance v0, Lcom/a/b/d/uv;

    iget-object v1, p0, Lcom/a/b/d/uv;->b:Lcom/a/b/d/bw;

    invoke-interface {v1}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/a/b/d/uv;-><init>(Lcom/a/b/d/bw;Lcom/a/b/d/bw;)V

    iput-object v0, p0, Lcom/a/b/d/uv;->c:Lcom/a/b/d/bw;

    :cond_11
    return-object v0
.end method

.method public final j_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/a/b/d/uv;->d:Ljava/util/Set;

    .line 1448
    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/a/b/d/uv;->b:Lcom/a/b/d/bw;

    invoke-interface {v0}, Lcom/a/b/d/bw;->j_()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/uv;->d:Ljava/util/Set;

    :cond_10
    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1415
    .line 2430
    iget-object v0, p0, Lcom/a/b/d/uv;->a:Ljava/util/Map;

    .line 1415
    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1415
    invoke-virtual {p0}, Lcom/a/b/d/uv;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
