.class Lcom/a/b/d/adn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:J
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source"
    .end annotation
.end field


# instance fields
.field final g:Ljava/lang/Object;

.field final h:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adn;->g:Ljava/lang/Object;

    .line 69
    if-nez p2, :cond_c

    move-object p2, p0

    :cond_c
    iput-object p2, p0, Lcom/a/b/d/adn;->h:Ljava/lang/Object;

    .line 70
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 91
    iget-object v1, p0, Lcom/a/b/d/adn;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_3
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 93
    monitor-exit v1

    return-void

    :catchall_8
    move-exception v0

    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    throw v0
.end method


# virtual methods
.method d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/a/b/d/adn;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 79
    iget-object v1, p0, Lcom/a/b/d/adn;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 80
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adn;->g:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 81
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method
