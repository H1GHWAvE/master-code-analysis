.class final Lcom/a/b/d/cw;
.super Lcom/a/b/d/cv;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/d/cv;-><init>(B)V

    return-void
.end method

.method private static a(I)Lcom/a/b/d/cv;
    .registers 2

    .prologue
    .line 101
    if-gez p0, :cond_5

    .line 1061
    sget-object v0, Lcom/a/b/d/cv;->b:Lcom/a/b/d/cv;

    .line 101
    :goto_4
    return-object v0

    :cond_5
    if-lez p0, :cond_a

    .line 2061
    sget-object v0, Lcom/a/b/d/cv;->c:Lcom/a/b/d/cv;

    goto :goto_4

    .line 3061
    :cond_a
    sget-object v0, Lcom/a/b/d/cv;->a:Lcom/a/b/d/cv;

    goto :goto_4
.end method


# virtual methods
.method public final a(DD)Lcom/a/b/d/cv;
    .registers 6

    .prologue
    .line 92
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cw;->a(I)Lcom/a/b/d/cv;

    move-result-object v0

    return-object v0
.end method

.method public final a(FF)Lcom/a/b/d/cv;
    .registers 4

    .prologue
    .line 89
    invoke-static {p1, p2}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cw;->a(I)Lcom/a/b/d/cv;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)Lcom/a/b/d/cv;
    .registers 4

    .prologue
    .line 83
    invoke-static {p1, p2}, Lcom/a/b/l/q;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cw;->a(I)Lcom/a/b/d/cv;

    move-result-object v0

    return-object v0
.end method

.method public final a(JJ)Lcom/a/b/d/cv;
    .registers 6

    .prologue
    .line 86
    invoke-static {p1, p2, p3, p4}, Lcom/a/b/l/u;->a(JJ)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cw;->a(I)Lcom/a/b/d/cv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/cv;
    .registers 4

    .prologue
    .line 76
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cw;->a(I)Lcom/a/b/d/cv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/a/b/d/cv;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 80
    invoke-interface {p3, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cw;->a(I)Lcom/a/b/d/cv;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZZ)Lcom/a/b/d/cv;
    .registers 4

    .prologue
    .line 95
    invoke-static {p2, p1}, Lcom/a/b/l/a;->a(ZZ)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cw;->a(I)Lcom/a/b/d/cv;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public final b(ZZ)Lcom/a/b/d/cv;
    .registers 4

    .prologue
    .line 98
    invoke-static {p1, p2}, Lcom/a/b/l/a;->a(ZZ)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cw;->a(I)Lcom/a/b/d/cv;

    move-result-object v0

    return-object v0
.end method
