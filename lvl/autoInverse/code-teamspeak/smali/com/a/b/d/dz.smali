.class final Lcom/a/b/d/dz;
.super Lcom/a/b/d/dw;
.source "SourceFile"


# static fields
.field private static final b:J


# direct methods
.method constructor <init>(Ljava/lang/Comparable;)V
    .registers 3

    .prologue
    .line 301
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    invoke-direct {p0, v0}, Lcom/a/b/d/dw;-><init>(Ljava/lang/Comparable;)V

    .line 302
    return-void
.end method


# virtual methods
.method final a()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 308
    sget-object v0, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    return-object v0
.end method

.method final a(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 5

    .prologue
    .line 314
    sget-object v0, Lcom/a/b/d/dx;->a:[I

    invoke-virtual {p1}, Lcom/a/b/d/ce;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_24

    .line 321
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 318
    :pswitch_11
    iget-object v0, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    invoke-virtual {p2, v0}, Lcom/a/b/d/ep;->a(Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    .line 319
    if-nez v0, :cond_1e

    .line 1108
    invoke-static {}, Lcom/a/b/d/ea;->f()Lcom/a/b/d/ea;

    move-result-object p0

    .line 319
    :goto_1d
    :pswitch_1d
    return-object p0

    :cond_1e
    invoke-static {v0}, Lcom/a/b/d/dz;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object p0

    goto :goto_1d

    .line 314
    nop

    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_11
        :pswitch_1d
    .end packed-switch
.end method

.method final a(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 342
    iget-object v0, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    invoke-virtual {p1, v0}, Lcom/a/b/d/ep;->a(Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/StringBuilder;)V
    .registers 4

    .prologue
    .line 336
    const/16 v0, 0x28

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 337
    return-void
.end method

.method final a(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 305
    iget-object v0, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    invoke-static {v0, p1}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-gez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final b()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 311
    sget-object v0, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    return-object v0
.end method

.method final b(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 5

    .prologue
    .line 325
    sget-object v0, Lcom/a/b/d/dx;->a:[I

    invoke-virtual {p1}, Lcom/a/b/d/ce;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_24

    .line 332
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 327
    :pswitch_11
    iget-object v0, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    invoke-virtual {p2, v0}, Lcom/a/b/d/ep;->a(Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    .line 328
    if-nez v0, :cond_1e

    .line 1179
    invoke-static {}, Lcom/a/b/d/dy;->f()Lcom/a/b/d/dy;

    move-result-object p0

    .line 330
    :goto_1d
    :pswitch_1d
    return-object p0

    .line 328
    :cond_1e
    invoke-static {v0}, Lcom/a/b/d/dz;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object p0

    goto :goto_1d

    .line 325
    nop

    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_11
    .end packed-switch
.end method

.method final b(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    return-object v0
.end method

.method final b(Ljava/lang/StringBuilder;)V
    .registers 4

    .prologue
    .line 339
    iget-object v0, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 340
    return-void
.end method

.method final c(Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 3

    .prologue
    .line 348
    invoke-virtual {p0, p1}, Lcom/a/b/d/dz;->a(Lcom/a/b/d/ep;)Ljava/lang/Comparable;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_b

    invoke-static {v0}, Lcom/a/b/d/dz;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v0

    :goto_a
    return-object v0

    .line 2179
    :cond_b
    invoke-static {}, Lcom/a/b/d/dy;->f()Lcom/a/b/d/dy;

    move-result-object v0

    goto :goto_a
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 299
    check-cast p1, Lcom/a/b/d/dw;

    invoke-super {p0, p1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 355
    iget-object v0, p0, Lcom/a/b/d/dz;->a:Ljava/lang/Comparable;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
