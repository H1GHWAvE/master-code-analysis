.class Lcom/a/b/d/acm;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;

.field b:Ljava/util/Map;

.field final synthetic c:Lcom/a/b/d/abx;


# direct methods
.method constructor <init>(Lcom/a/b/d/abx;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 261
    iput-object p1, p0, Lcom/a/b/d/acm;->c:Lcom/a/b/d/abx;

    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    .line 262
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/acm;->a:Ljava/lang/Object;

    .line 263
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 3

    .prologue
    .line 333
    new-instance v0, Lcom/a/b/d/acn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/acn;-><init>(Lcom/a/b/d/acm;B)V

    return-object v0
.end method

.method c()Ljava/util/Map;
    .registers 3

    .prologue
    .line 268
    iget-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/a/b/d/acm;->c:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/a/b/d/acm;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    :cond_18
    invoke-virtual {p0}, Lcom/a/b/d/acm;->d()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    :goto_1e
    return-object v0

    :cond_1f
    iget-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    goto :goto_1e
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    .line 325
    if-eqz v0, :cond_9

    .line 326
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 328
    :cond_9
    invoke-virtual {p0}, Lcom/a/b/d/acm;->f()V

    .line 329
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    .line 289
    if-eqz p1, :cond_10

    if-eqz v0, :cond_10

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method d()Ljava/util/Map;
    .registers 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/a/b/d/acm;->c:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/a/b/d/acm;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method f()V
    .registers 3

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 281
    iget-object v0, p0, Lcom/a/b/d/acm;->c:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/a/b/d/acm;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    .line 284
    :cond_1a
    return-void
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    .line 296
    if-eqz p1, :cond_d

    if-eqz v0, :cond_d

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 303
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    iget-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_19

    .line 306
    iget-object v0, p0, Lcom/a/b/d/acm;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 308
    :goto_18
    return-object v0

    :cond_19
    iget-object v0, p0, Lcom/a/b/d/acm;->c:Lcom/a/b/d/abx;

    iget-object v1, p0, Lcom/a/b/d/acm;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/a/b/d/abx;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_18
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/a/b/d/acm;->c()Ljava/util/Map;

    move-result-object v0

    .line 314
    if-nez v0, :cond_8

    .line 315
    const/4 v0, 0x0

    .line 319
    :goto_7
    return-object v0

    .line 317
    :cond_8
    invoke-static {v0, p1}, Lcom/a/b/d/sz;->c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 318
    invoke-virtual {p0}, Lcom/a/b/d/acm;->f()V

    goto :goto_7
.end method
