.class final Lcom/a/b/d/jd;
.super Lcom/a/b/d/jt;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# instance fields
.field private final transient a:Ljava/util/EnumMap;


# direct methods
.method private constructor <init>(Ljava/util/EnumMap;)V
    .registers 3

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/a/b/d/jt;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/a/b/d/jd;->a:Ljava/util/EnumMap;

    .line 54
    invoke-virtual {p1}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_c
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 55
    return-void

    .line 54
    :cond_10
    const/4 v0, 0x0

    goto :goto_c
.end method

.method synthetic constructor <init>(Ljava/util/EnumMap;B)V
    .registers 3

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/a/b/d/jd;-><init>(Ljava/util/EnumMap;)V

    return-void
.end method

.method static a(Ljava/util/EnumMap;)Lcom/a/b/d/jt;
    .registers 3

    .prologue
    .line 38
    invoke-virtual {p0}, Ljava/util/EnumMap;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_2a

    .line 46
    new-instance v0, Lcom/a/b/d/jd;

    invoke-direct {v0, p0}, Lcom/a/b/d/jd;-><init>(Ljava/util/EnumMap;)V

    :goto_c
    return-object v0

    .line 1070
    :pswitch_d
    invoke-static {}, Lcom/a/b/d/it;->i()Lcom/a/b/d/it;

    move-result-object v0

    goto :goto_c

    .line 42
    :pswitch_12
    invoke-virtual {p0}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 43
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 1080
    invoke-static {v1, v0}, Lcom/a/b/d/it;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;

    move-result-object v0

    goto :goto_c

    .line 38
    nop

    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_d
        :pswitch_12
    .end packed-switch
.end method

.method static synthetic a(Lcom/a/b/d/jd;)Ljava/util/EnumMap;
    .registers 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/a/b/d/jd;->a:Ljava/util/EnumMap;

    return-object v0
.end method


# virtual methods
.method final c()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 59
    new-instance v0, Lcom/a/b/d/je;

    invoke-direct {v0, p0}, Lcom/a/b/d/je;-><init>(Lcom/a/b/d/jd;)V

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 90
    iget-object v0, p0, Lcom/a/b/d/jd;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final d()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 100
    new-instance v0, Lcom/a/b/d/jf;

    invoke-direct {v0, p0}, Lcom/a/b/d/jf;-><init>(Lcom/a/b/d/jd;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/a/b/d/jd;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final i_()Z
    .registers 2

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method final j()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 134
    new-instance v0, Lcom/a/b/d/jh;

    iget-object v1, p0, Lcom/a/b/d/jd;->a:Ljava/util/EnumMap;

    invoke-direct {v0, v1}, Lcom/a/b/d/jh;-><init>(Ljava/util/EnumMap;)V

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/a/b/d/jd;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->size()I

    move-result v0

    return v0
.end method
