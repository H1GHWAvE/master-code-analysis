.class final Lcom/a/b/d/qn;
.super Lcom/a/b/d/da;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final C:J


# direct methods
.method constructor <init>(Lcom/a/b/d/ql;Lcom/a/b/b/bj;)V
    .registers 3

    .prologue
    .line 877
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/da;-><init>(Lcom/a/b/d/ql;Lcom/a/b/b/bj;)V

    .line 878
    return-void
.end method


# virtual methods
.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 8

    .prologue
    .line 885
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/a/b/d/qn;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_3} :catch_46

    move-result-object v0

    .line 892
    if-nez v0, :cond_56

    .line 893
    new-instance v0, Ljava/lang/NullPointerException;

    iget-object v1, p0, Lcom/a/b/d/qn;->a:Lcom/a/b/b/bj;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " returned null for key "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 887
    :catch_46
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 888
    const-class v1, Lcom/a/b/d/cz;

    invoke-static {v0, v1}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 889
    new-instance v1, Lcom/a/b/d/cz;

    invoke-direct {v1, v0}, Lcom/a/b/d/cz;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 895
    :cond_56
    return-object v0
.end method
