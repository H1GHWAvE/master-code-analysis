.class Lcom/a/b/d/un;
.super Lcom/a/b/d/up;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation


# direct methods
.method constructor <init>(Ljava/util/NavigableMap;)V
    .registers 2

    .prologue
    .line 3575
    invoke-direct {p0, p1}, Lcom/a/b/d/up;-><init>(Ljava/util/SortedMap;)V

    .line 3576
    return-void
.end method

.method private c()Ljava/util/NavigableMap;
    .registers 2

    .prologue
    .line 3580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 3572
    .line 14580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3572
    return-object v0
.end method

.method final bridge synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 3572
    .line 15580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3572
    return-object v0
.end method

.method public ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3595
    .line 6580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3595
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 3620
    invoke-virtual {p0}, Lcom/a/b/d/un;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public descendingSet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 3615
    .line 10580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3615
    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3590
    .line 5580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3590
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 3634
    .line 12580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3634
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 3649
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/un;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3600
    .line 7580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3600
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3585
    .line 4580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3585
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3605
    .line 8580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3605
    invoke-interface {v0}, Ljava/util/NavigableMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3610
    .line 9580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3610
    invoke-interface {v0}, Ljava/util/NavigableMap;->pollLastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 3629
    .line 11580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3629
    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 3644
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/un;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 3639
    .line 13580
    iget-object v0, p0, Lcom/a/b/d/un;->d:Ljava/util/Map;

    check-cast v0, Ljava/util/NavigableMap;

    .line 3639
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 3654
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/un;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method
