.class public abstract Lcom/a/b/d/hj;
.super Lcom/a/b/d/gx;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/aac;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/a/b/d/gx;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/a/b/d/aac;
.end method

.method public a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/hj;->a()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/aac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/a/b/d/hj;->a()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/aac;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/hj;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/a/b/d/hj;->a()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/aac;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/a/b/d/hj;->a()Lcom/a/b/d/aac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/a/b/d/hj;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/a/b/d/hj;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/a/b/d/hj;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/a/b/d/hj;->a()Lcom/a/b/d/aac;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/a/b/d/hj;->a()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/aac;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
