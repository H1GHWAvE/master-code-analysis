.class Lcom/a/b/d/yw;
.super Lcom/a/b/d/ir;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field private final a:Lcom/a/b/d/iz;

.field private final b:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>(Lcom/a/b/d/iz;Lcom/a/b/d/jl;)V
    .registers 3

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/a/b/d/ir;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/a/b/d/yw;->a:Lcom/a/b/d/iz;

    .line 36
    iput-object p2, p0, Lcom/a/b/d/yw;->b:Lcom/a/b/d/jl;

    .line 37
    return-void
.end method

.method constructor <init>(Lcom/a/b/d/iz;[Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 40
    .line 1312
    array-length v0, p2

    invoke-static {p2, v0}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v0

    .line 40
    invoke-direct {p0, p1, v0}, Lcom/a/b/d/yw;-><init>(Lcom/a/b/d/iz;Lcom/a/b/d/jl;)V

    .line 41
    return-void
.end method

.method private i()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/b/d/yw;->b:Lcom/a/b/d/jl;

    return-object v0
.end method


# virtual methods
.method final a([Ljava/lang/Object;I)I
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "not present in emulated superclass"
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/a/b/d/yw;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/a/b/d/agj;
    .registers 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/a/b/d/yw;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->a(I)Lcom/a/b/d/agj;

    move-result-object v0

    return-object v0
.end method

.method b()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/a/b/d/yw;->a:Lcom/a/b/d/iz;

    return-object v0
.end method

.method public get(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/a/b/d/yw;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/a/b/d/yw;->a(I)Lcom/a/b/d/agj;

    move-result-object v0

    return-object v0
.end method
