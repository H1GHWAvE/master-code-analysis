.class public abstract Lcom/a/b/d/me;
.super Lcom/a/b/d/mh;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/aay;
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final a:Ljava/util/Comparator;

.field static final c:Lcom/a/b/d/me;


# instance fields
.field final transient d:Ljava/util/Comparator;

.field transient e:Lcom/a/b/d/me;
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 97
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    sput-object v0, Lcom/a/b/d/me;->a:Ljava/util/Comparator;

    .line 100
    new-instance v0, Lcom/a/b/d/fc;

    sget-object v1, Lcom/a/b/d/me;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/a/b/d/fc;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/a/b/d/me;->c:Lcom/a/b/d/me;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .registers 2

    .prologue
    .line 584
    invoke-direct {p0}, Lcom/a/b/d/mh;-><init>()V

    .line 585
    iput-object p1, p0, Lcom/a/b/d/me;->d:Ljava/util/Comparator;

    .line 586
    return-void
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 578
    .line 579
    invoke-interface {p0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/Comparable;)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 129
    new-instance v0, Lcom/a/b/d/zq;

    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/me;
    .registers 6

    .prologue
    const/4 v3, 0x2

    .line 143
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/me;
    .registers 7

    .prologue
    const/4 v3, 0x3

    .line 156
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/me;
    .registers 8

    .prologue
    const/4 v3, 0x4

    .line 169
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/me;
    .registers 9

    .prologue
    const/4 v3, 0x5

    .line 182
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    const/4 v2, 0x4

    aput-object p4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;[Ljava/lang/Comparable;)Lcom/a/b/d/me;
    .registers 11

    .prologue
    const/4 v3, 0x0

    .line 196
    array-length v0, p6

    add-int/lit8 v0, v0, 0x6

    new-array v0, v0, [Ljava/lang/Comparable;

    .line 197
    aput-object p0, v0, v3

    .line 198
    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 199
    const/4 v1, 0x2

    aput-object p2, v0, v1

    .line 200
    const/4 v1, 0x3

    aput-object p3, v0, v1

    .line 201
    const/4 v1, 0x4

    aput-object p4, v0, v1

    .line 202
    const/4 v1, 0x5

    aput-object p5, v0, v1

    .line 203
    const/4 v1, 0x6

    array-length v2, p6

    invoke-static {p6, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 204
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    array-length v2, v0

    check-cast v0, [Ljava/lang/Comparable;

    invoke-static {v1, v2, v0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Comparator;)Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 110
    sget-object v0, Lcom/a/b/d/me;->a:Ljava/util/Comparator;

    invoke-interface {v0, p0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1105
    sget-object v0, Lcom/a/b/d/me;->c:Lcom/a/b/d/me;

    .line 113
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/fc;

    invoke-direct {v0, p0}, Lcom/a/b/d/fc;-><init>(Ljava/util/Comparator;)V

    goto :goto_a
.end method

.method static varargs a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;
    .registers 7

    .prologue
    const/4 v0, 0x1

    .line 424
    if-nez p1, :cond_8

    .line 425
    invoke-static {p0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;)Lcom/a/b/d/me;

    move-result-object v0

    .line 438
    :goto_7
    return-object v0

    .line 427
    :cond_8
    invoke-static {p2, p1}, Lcom/a/b/d/yc;->c([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 428
    const/4 v1, 0x0

    invoke-static {p2, v1, p1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    move v2, v0

    move v1, v0

    .line 430
    :goto_11
    if-ge v2, p1, :cond_28

    .line 431
    aget-object v3, p2, v2

    .line 432
    add-int/lit8 v0, v1, -0x1

    aget-object v0, p2, v0

    .line 433
    invoke-interface {p0, v3, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_36

    .line 434
    add-int/lit8 v0, v1, 0x1

    aput-object v3, p2, v1

    .line 430
    :goto_23
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_11

    .line 437
    :cond_28
    const/4 v0, 0x0

    invoke-static {p2, v1, p1, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 438
    new-instance v0, Lcom/a/b/d/zq;

    invoke-static {p2, v1}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;I)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    goto :goto_7

    :cond_36
    move v0, v1

    goto :goto_23
.end method

.method public static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 344
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    invoke-static {p0, p1}, Lcom/a/b/d/aaz;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    .line 348
    if-eqz v0, :cond_17

    instance-of v0, p1, Lcom/a/b/d/me;

    if-eqz v0, :cond_17

    move-object v0, p1

    .line 350
    check-cast v0, Lcom/a/b/d/me;

    .line 351
    invoke-virtual {v0}, Lcom/a/b/d/me;->h_()Z

    move-result v1

    if-nez v1, :cond_17

    .line 357
    :goto_16
    return-object v0

    .line 356
    :cond_17
    invoke-static {p1}, Lcom/a/b/d/mq;->c(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 357
    array-length v1, v0

    invoke-static {p0, v1, v0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    goto :goto_16
.end method

.method public static a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 380
    invoke-static {p0, p1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Iterator;)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 326
    new-instance v0, Lcom/a/b/d/mf;

    invoke-direct {v0, p0}, Lcom/a/b/d/mf;-><init>(Ljava/util/Comparator;)V

    invoke-virtual {v0, p1}, Lcom/a/b/d/mf;->c(Ljava/util/Iterator;)Lcom/a/b/d/mf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/mf;->c()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 311
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 3326
    new-instance v1, Lcom/a/b/d/mf;

    invoke-direct {v1, v0}, Lcom/a/b/d/mf;-><init>(Ljava/util/Comparator;)V

    invoke-virtual {v1, p0}, Lcom/a/b/d/mf;->c(Ljava/util/Iterator;)Lcom/a/b/d/mf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/mf;->c()Lcom/a/b/d/me;

    move-result-object v0

    .line 312
    return-object v0
.end method

.method private static a(Ljava/util/SortedSet;)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 401
    invoke-static {p0}, Lcom/a/b/d/aaz;->a(Ljava/util/SortedSet;)Ljava/util/Comparator;

    move-result-object v1

    .line 402
    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v2

    .line 403
    invoke-virtual {v2}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 404
    invoke-static {v1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;)Lcom/a/b/d/me;

    move-result-object v0

    .line 406
    :goto_12
    return-object v0

    :cond_13
    new-instance v0, Lcom/a/b/d/zq;

    invoke-direct {v0, v2, v1}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    goto :goto_12
.end method

.method private static a([Ljava/lang/Comparable;)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 219
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    array-length v2, p0

    invoke-virtual {p0}, [Ljava/lang/Comparable;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 253
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 254
    invoke-static {v0, p0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/Collection;)Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 291
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 2380
    invoke-static {v0, p0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/me;

    move-result-object v0

    .line 292
    return-object v0
.end method

.method private static b(Ljava/util/Comparator;)Lcom/a/b/d/mf;
    .registers 2

    .prologue
    .line 451
    new-instance v0, Lcom/a/b/d/mf;

    invoke-direct {v0, p0}, Lcom/a/b/d/mf;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static j()Lcom/a/b/d/me;
    .registers 1

    .prologue
    .line 2105
    sget-object v0, Lcom/a/b/d/me;->c:Lcom/a/b/d/me;

    .line 121
    return-object v0
.end method

.method private static k()Lcom/a/b/d/me;
    .registers 1

    .prologue
    .line 105
    sget-object v0, Lcom/a/b/d/me;->c:Lcom/a/b/d/me;

    return-object v0
.end method

.method private static m()Lcom/a/b/d/mf;
    .registers 2

    .prologue
    .line 459
    new-instance v0, Lcom/a/b/d/mf;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/mf;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static n()Lcom/a/b/d/mf;
    .registers 2

    .prologue
    .line 470
    new-instance v0, Lcom/a/b/d/mf;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/mf;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static o()V
    .registers 2

    .prologue
    .line 829
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 672
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/me;->c(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/me;
    .registers 5

    .prologue
    .line 643
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/me;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Ljava/lang/Object;Z)Lcom/a/b/d/me;
.end method

.method abstract a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
.end method

.method public b()Lcom/a/b/d/me;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 779
    iget-object v0, p0, Lcom/a/b/d/me;->e:Lcom/a/b/d/me;

    .line 780
    if-nez v0, :cond_c

    .line 781
    invoke-virtual {p0}, Lcom/a/b/d/me;->e()Lcom/a/b/d/me;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/me;->e:Lcom/a/b/d/me;

    .line 782
    iput-object p0, v0, Lcom/a/b/d/me;->e:Lcom/a/b/d/me;

    .line 784
    :cond_c
    return-object v0
.end method

.method public b(Ljava/lang/Object;)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 616
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/me;->d(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method abstract b(Ljava/lang/Object;Z)Lcom/a/b/d/me;
.end method

.method public b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 6
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 653
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 654
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 655
    iget-object v0, p0, Lcom/a/b/d/me;->d:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_17

    const/4 v0, 0x1

    :goto_f
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 656
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/a/b/d/me;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0

    .line 655
    :cond_17
    const/4 v0, 0x0

    goto :goto_f
.end method

.method abstract c(Ljava/lang/Object;)I
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method final c(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 569
    iget-object v0, p0, Lcom/a/b/d/me;->d:Ljava/util/Comparator;

    .line 3579
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 569
    return v0
.end method

.method public abstract c()Lcom/a/b/d/agi;
.end method

.method public c(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 681
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/a/b/d/me;->a(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 719
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/me;->c(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->f(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 597
    iget-object v0, p0, Lcom/a/b/d/me;->d:Ljava/util/Comparator;

    return-object v0
.end method

.method public abstract d()Lcom/a/b/d/agi;
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation
.end method

.method public d(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 625
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/a/b/d/me;->b(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public synthetic descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/a/b/d/me;->d()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public synthetic descendingSet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/a/b/d/me;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method e()Lcom/a/b/d/me;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 789
    new-instance v0, Lcom/a/b/d/em;

    invoke-direct {v0, p0}, Lcom/a/b/d/em;-><init>(Lcom/a/b/d/me;)V

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 733
    invoke-virtual {p0}, Lcom/a/b/d/me;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/agi;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 710
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/me;->d(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->d()Lcom/a/b/d/agi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method g()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 833
    new-instance v0, Lcom/a/b/d/mg;

    iget-object v1, p0, Lcom/a/b/d/me;->d:Ljava/util/Comparator;

    invoke-virtual {p0}, Lcom/a/b/d/me;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/mg;-><init>(Ljava/util/Comparator;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/me;->d(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/a/b/d/me;->b(Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 728
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/me;->c(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->f(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/a/b/d/me;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 738
    invoke-virtual {p0}, Lcom/a/b/d/me;->d()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/agi;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 701
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/me;->d(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->d()Lcom/a/b/d/agi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollFirst()Ljava/lang/Object;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 752
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final pollLast()Ljava/lang/Object;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 766
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/a/b/d/me;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/me;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/me;->c(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/a/b/d/me;->a(Ljava/lang/Object;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method
