.class final Lcom/a/b/d/cp;
.super Ljava/util/AbstractCollection;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/jl;

.field final b:Ljava/util/Comparator;

.field final c:I


# direct methods
.method constructor <init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V
    .registers 4

    .prologue
    .line 416
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 417
    invoke-static {p2}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/yd;->b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/cp;->a:Lcom/a/b/d/jl;

    .line 418
    iput-object p2, p0, Lcom/a/b/d/cp;->b:Ljava/util/Comparator;

    .line 419
    iget-object v0, p0, Lcom/a/b/d/cp;->a:Lcom/a/b/d/jl;

    invoke-static {v0, p2}, Lcom/a/b/d/cp;->a(Ljava/util/List;Ljava/util/Comparator;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/cp;->c:I

    .line 420
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/Comparator;)I
    .registers 10

    .prologue
    const v4, 0x7fffffff

    const/4 v0, 0x1

    .line 433
    const-wide/16 v2, 0x1

    move v1, v0

    .line 436
    :goto_7
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_30

    .line 437
    add-int/lit8 v5, v1, -0x1

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {p1, v5, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    .line 439
    if-gez v5, :cond_2b

    .line 441
    invoke-static {v1, v0}, Lcom/a/b/j/i;->a(II)J

    move-result-wide v6

    mul-long/2addr v2, v6

    .line 442
    const/4 v0, 0x0

    .line 443
    invoke-static {v2, v3}, Lcom/a/b/d/cm;->a(J)Z

    move-result v5

    if-nez v5, :cond_2b

    move v0, v4

    .line 454
    :goto_2a
    return v0

    .line 447
    :cond_2b
    add-int/lit8 v1, v1, 0x1

    .line 448
    add-int/lit8 v0, v0, 0x1

    .line 449
    goto :goto_7

    .line 450
    :cond_30
    invoke-static {v1, v0}, Lcom/a/b/j/i;->a(II)J

    move-result-wide v0

    mul-long/2addr v0, v2

    .line 451
    invoke-static {v0, v1}, Lcom/a/b/d/cm;->a(J)Z

    move-result v2

    if-nez v2, :cond_3d

    move v0, v4

    .line 452
    goto :goto_2a

    .line 454
    :cond_3d
    long-to-int v0, v0

    goto :goto_2a
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 470
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_d

    .line 471
    check-cast p1, Ljava/util/List;

    .line 472
    iget-object v0, p0, Lcom/a/b/d/cp;->a:Lcom/a/b/d/jl;

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    .line 474
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 462
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 466
    new-instance v0, Lcom/a/b/d/cq;

    iget-object v1, p0, Lcom/a/b/d/cp;->a:Lcom/a/b/d/jl;

    iget-object v2, p0, Lcom/a/b/d/cp;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/cq;-><init>(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 458
    iget v0, p0, Lcom/a/b/d/cp;->c:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 478
    iget-object v0, p0, Lcom/a/b/d/cp;->a:Lcom/a/b/d/jl;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "orderedPermutationCollection("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
