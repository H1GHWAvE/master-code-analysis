.class final Lcom/a/b/d/f;
.super Lcom/a/b/d/a;
.source "SourceFile"


# static fields
.field private static final b:J
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in emulated source."
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Map;Lcom/a/b/d/a;)V
    .registers 4

    .prologue
    .line 355
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/a;-><init>(Ljava/util/Map;Lcom/a/b/d/a;B)V

    .line 356
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map;Lcom/a/b/d/a;B)V
    .registers 4

    .prologue
    .line 353
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/f;-><init>(Ljava/util/Map;Lcom/a/b/d/a;)V

    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 390
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 391
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/a;

    .line 1099
    iput-object v0, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    .line 392
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOuputStream"
    .end annotation

    .prologue
    .line 382
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 383
    invoke-virtual {p0}, Lcom/a/b/d/f;->b()Lcom/a/b/d/bw;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 384
    return-void
.end method

.method private d()Ljava/lang/Object;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in the emulated source."
    .end annotation

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/a/b/d/f;->b()Lcom/a/b/d/bw;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 369
    iget-object v0, p0, Lcom/a/b/d/f;->a:Lcom/a/b/d/a;

    invoke-virtual {v0, p1}, Lcom/a/b/d/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 374
    iget-object v0, p0, Lcom/a/b/d/f;->a:Lcom/a/b/d/a;

    invoke-virtual {v0, p1}, Lcom/a/b/d/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 353
    invoke-super {p0}, Lcom/a/b/d/a;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 353
    invoke-super {p0}, Lcom/a/b/d/a;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
