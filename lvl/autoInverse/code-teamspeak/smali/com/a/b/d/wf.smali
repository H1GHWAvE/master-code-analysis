.class final Lcom/a/b/d/wf;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/vi;


# direct methods
.method constructor <init>(Lcom/a/b/d/vi;)V
    .registers 3

    .prologue
    .line 1675
    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    .line 1676
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    iput-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    .line 1677
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/wf;)Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 1671
    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    return-object v0
.end method

.method private a(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 1688
    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1689
    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 1717
    invoke-virtual {p0, p1}, Lcom/a/b/d/wf;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 1721
    invoke-virtual {p0, p1}, Lcom/a/b/d/wf;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1684
    new-instance v0, Lcom/a/b/d/wg;

    invoke-direct {v0, p0}, Lcom/a/b/d/wg;-><init>(Lcom/a/b/d/wf;)V

    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 1737
    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->g()V

    .line 1738
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1733
    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1671
    .line 3717
    invoke-virtual {p0, p1}, Lcom/a/b/d/wf;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    .line 1671
    goto :goto_c
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 1729
    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->n()Z

    move-result v0

    return v0
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1725
    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1671
    .line 2721
    invoke-virtual {p0, p1}, Lcom/a/b/d/wf;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    .line 1671
    goto :goto_c
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/a/b/d/wf;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
