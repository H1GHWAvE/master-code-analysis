.class final Lcom/a/b/d/aas;
.super Lcom/a/b/d/agi;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/jl;

.field b:I

.field final synthetic c:Lcom/a/b/d/aar;


# direct methods
.method constructor <init>(Lcom/a/b/d/aar;)V
    .registers 3

    .prologue
    .line 1243
    iput-object p1, p0, Lcom/a/b/d/aas;->c:Lcom/a/b/d/aar;

    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 1244
    iget-object v0, p0, Lcom/a/b/d/aas;->c:Lcom/a/b/d/aar;

    invoke-static {v0}, Lcom/a/b/d/aar;->a(Lcom/a/b/d/aar;)Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aas;->a:Lcom/a/b/d/jl;

    .line 1245
    iget-object v0, p0, Lcom/a/b/d/aas;->c:Lcom/a/b/d/aar;

    invoke-static {v0}, Lcom/a/b/d/aar;->b(Lcom/a/b/d/aar;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/aas;->b:I

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 1249
    iget v0, p0, Lcom/a/b/d/aas;->b:I

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1254
    iget v0, p0, Lcom/a/b/d/aas;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v0

    .line 1255
    const/16 v1, 0x20

    if-ne v0, v1, :cond_10

    .line 1256
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1258
    :cond_10
    iget v1, p0, Lcom/a/b/d/aas;->b:I

    const/4 v2, 0x1

    shl-int/2addr v2, v0

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lcom/a/b/d/aas;->b:I

    .line 1259
    iget-object v1, p0, Lcom/a/b/d/aas;->a:Lcom/a/b/d/jl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
