.class public final Lcom/a/b/l/ao;
.super Ljava/lang/Number;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field public static final a:Lcom/a/b/l/ao;

.field public static final b:Lcom/a/b/l/ao;

.field public static final c:Lcom/a/b/l/ao;

.field private static final d:J = 0x7fffffffffffffffL


# instance fields
.field private final e:J


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 47
    new-instance v0, Lcom/a/b/l/ao;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/a/b/l/ao;-><init>(J)V

    sput-object v0, Lcom/a/b/l/ao;->a:Lcom/a/b/l/ao;

    .line 48
    new-instance v0, Lcom/a/b/l/ao;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Lcom/a/b/l/ao;-><init>(J)V

    sput-object v0, Lcom/a/b/l/ao;->b:Lcom/a/b/l/ao;

    .line 49
    new-instance v0, Lcom/a/b/l/ao;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Lcom/a/b/l/ao;-><init>(J)V

    sput-object v0, Lcom/a/b/l/ao;->c:Lcom/a/b/l/ao;

    return-void
.end method

.method private constructor <init>(J)V
    .registers 4

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 54
    iput-wide p1, p0, Lcom/a/b/l/ao;->e:J

    .line 55
    return-void
.end method

.method private static a(J)Lcom/a/b/l/ao;
    .registers 4

    .prologue
    .line 72
    new-instance v0, Lcom/a/b/l/ao;

    invoke-direct {v0, p0, p1}, Lcom/a/b/l/ao;-><init>(J)V

    return-object v0
.end method

.method private a(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
    .registers 6

    .prologue
    .line 130
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/ao;

    iget-wide v0, v0, Lcom/a/b/l/ao;->e:J

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/l/ao;
    .registers 3

    .prologue
    .line 108
    .line 1120
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lcom/a/b/l/ap;->a(Ljava/lang/String;I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    .line 108
    return-object v0
.end method

.method private static a(Ljava/math/BigInteger;)Lcom/a/b/l/ao;
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    invoke-virtual {p0}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-ltz v0, :cond_26

    invoke-virtual {p0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    const/16 v3, 0x40

    if-gt v0, v3, :cond_26

    move v0, v1

    :goto_14
    const-string v3, "value (%s) is outside the range for an unsigned long value"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 97
    invoke-virtual {p0}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    return-object v0

    :cond_26
    move v0, v2

    .line 95
    goto :goto_14
.end method

.method private a(I)Ljava/lang/String;
    .registers 4

    .prologue
    .line 267
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    invoke-static {v0, v1, p1}, Lcom/a/b/l/ap;->a(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/math/BigInteger;
    .registers 7

    .prologue
    .line 226
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    .line 227
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1a

    .line 228
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->setBit(I)Ljava/math/BigInteger;

    move-result-object v0

    .line 230
    :cond_1a
    return-object v0
.end method

.method private static b(J)Lcom/a/b/l/ao;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    const-wide/16 v4, 0x0

    cmp-long v0, p0, v4

    if-ltz v0, :cond_1b

    move v0, v1

    :goto_9
    const-string v3, "value (%s) is outside the range for an unsigned long value"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 84
    invoke-static {p0, p1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    return-object v0

    :cond_1b
    move v0, v2

    .line 82
    goto :goto_9
.end method

.method private b(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
    .registers 6

    .prologue
    .line 140
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/ao;

    iget-wide v0, v0, Lcom/a/b/l/ao;->e:J

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/l/ao;
    .registers 3

    .prologue
    .line 120
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lcom/a/b/l/ap;->a(Ljava/lang/String;I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
    .registers 6
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 151
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/ao;

    iget-wide v0, v0, Lcom/a/b/l/ao;->e:J

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
    .registers 6
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 161
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/ao;

    iget-wide v0, v0, Lcom/a/b/l/ao;->e:J

    invoke-static {v2, v3, v0, v1}, Lcom/a/b/l/ap;->b(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    return-object v0
.end method

.method private e(Lcom/a/b/l/ao;)Lcom/a/b/l/ao;
    .registers 6
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 171
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/ao;

    iget-wide v0, v0, Lcom/a/b/l/ao;->e:J

    invoke-static {v2, v3, v0, v1}, Lcom/a/b/l/ap;->c(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/ao;->a(J)Lcom/a/b/l/ao;

    move-result-object v0

    return-object v0
.end method

.method private f(Lcom/a/b/l/ao;)I
    .registers 6

    .prologue
    .line 235
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    iget-wide v2, p1, Lcom/a/b/l/ao;->e:J

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 42
    check-cast p1, Lcom/a/b/l/ao;

    .line 2235
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    iget-wide v2, p1, Lcom/a/b/l/ao;->e:J

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v0

    .line 42
    return v0
.end method

.method public final doubleValue()D
    .registers 7

    .prologue
    .line 215
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    long-to-double v0, v0

    .line 216
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_14

    .line 217
    const-wide/high16 v2, 0x43e0000000000000L    # 9.223372036854776E18

    add-double/2addr v0, v2

    .line 219
    :cond_14
    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 246
    instance-of v1, p1, Lcom/a/b/l/ao;

    if-eqz v1, :cond_10

    .line 247
    check-cast p1, Lcom/a/b/l/ao;

    .line 248
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    iget-wide v4, p1, Lcom/a/b/l/ao;->e:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_10

    const/4 v0, 0x1

    .line 250
    :cond_10
    return v0
.end method

.method public final floatValue()F
    .registers 7

    .prologue
    .line 201
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    long-to-float v0, v0

    .line 202
    iget-wide v2, p0, Lcom/a/b/l/ao;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_14

    .line 203
    const/high16 v1, 0x5f000000

    add-float/2addr v0, v1

    .line 205
    :cond_14
    return v0
.end method

.method public final hashCode()I
    .registers 5

    .prologue
    .line 241
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    .line 2078
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 241
    return v0
.end method

.method public final intValue()I
    .registers 3

    .prologue
    .line 179
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    long-to-int v0, v0

    return v0
.end method

.method public final longValue()J
    .registers 3

    .prologue
    .line 191
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/a/b/l/ao;->e:J

    invoke-static {v0, v1}, Lcom/a/b/l/ap;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
