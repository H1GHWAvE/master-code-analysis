.class public final Lcom/a/b/l/u;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field public static final a:I = 0x8

.field public static final b:J = 0x4000000000000000L


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(J)I
    .registers 4

    .prologue
    .line 78
    const/16 v0, 0x20

    ushr-long v0, p0, v0

    xor-long/2addr v0, p0

    long-to-int v0, v0

    return v0
.end method

.method public static a(JJ)I
    .registers 6

    .prologue
    .line 94
    cmp-long v0, p0, p2

    if-gez v0, :cond_6

    const/4 v0, -0x1

    :goto_5
    return v0

    :cond_6
    cmp-long v0, p0, p2

    if-lez v0, :cond_c

    const/4 v0, 0x1

    goto :goto_5

    :cond_c
    const/4 v0, 0x0

    goto :goto_5
.end method

.method static a([JJII)I
    .registers 10

    .prologue
    .line 185
    add-int/lit8 v0, p4, -0x1

    :goto_2
    if-lt v0, p3, :cond_e

    .line 186
    aget-wide v2, p0, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_b

    .line 190
    :goto_a
    return v0

    .line 185
    :cond_b
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 190
    :cond_e
    const/4 v0, -0x1

    goto :goto_a
.end method

.method private static a([J[J)I
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 151
    const-string v0, "array"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v0, "target"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    array-length v0, p1

    if-nez v0, :cond_f

    .line 166
    :goto_e
    return v1

    :cond_f
    move v0, v1

    .line 158
    :goto_10
    array-length v2, p0

    array-length v3, p1

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_2d

    move v2, v1

    .line 159
    :goto_18
    array-length v3, p1

    if-ge v2, v3, :cond_28

    .line 160
    add-int v3, v0, v2

    aget-wide v4, p0, v3

    aget-wide v6, p1, v2

    cmp-long v3, v4, v6

    if-nez v3, :cond_2a

    .line 159
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    :cond_28
    move v1, v0

    .line 164
    goto :goto_e

    .line 158
    :cond_2a
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 166
    :cond_2d
    const/4 v1, -0x1

    goto :goto_e
.end method

.method public static a(BBBBBBBB)J
    .registers 16

    .prologue
    const-wide/16 v6, 0xff

    .line 305
    int-to-long v0, p0

    and-long/2addr v0, v6

    const/16 v2, 0x38

    shl-long/2addr v0, v2

    int-to-long v2, p1

    and-long/2addr v2, v6

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p2

    and-long/2addr v2, v6

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p3

    and-long/2addr v2, v6

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p4

    and-long/2addr v2, v6

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p5

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p6

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, p7

    and-long/2addr v2, v6

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private static a([B)J
    .registers 9

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 290
    array-length v0, p0

    if-lt v0, v7, :cond_37

    move v0, v1

    :goto_9
    const-string v3, "array too small: %s < %s"

    new-array v4, v6, [Ljava/lang/Object;

    array-length v5, p0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 292
    aget-byte v0, p0, v2

    aget-byte v1, p0, v1

    aget-byte v2, p0, v6

    const/4 v3, 0x3

    aget-byte v3, p0, v3

    const/4 v4, 0x4

    aget-byte v4, p0, v4

    const/4 v5, 0x5

    aget-byte v5, p0, v5

    const/4 v6, 0x6

    aget-byte v6, p0, v6

    const/4 v7, 0x7

    aget-byte v7, p0, v7

    invoke-static/range {v0 .. v7}, Lcom/a/b/l/u;->a(BBBBBBBB)J

    move-result-wide v0

    return-wide v0

    :cond_37
    move v0, v2

    .line 290
    goto :goto_9
.end method

.method private static varargs a([J)J
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 202
    array-length v0, p0

    if-lez v0, :cond_19

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 203
    aget-wide v2, p0, v2

    .line 204
    :goto_b
    array-length v0, p0

    if-ge v1, v0, :cond_1b

    .line 205
    aget-wide v4, p0, v1

    cmp-long v0, v4, v2

    if-gez v0, :cond_16

    .line 206
    aget-wide v2, p0, v1

    .line 204
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_19
    move v0, v2

    .line 202
    goto :goto_6

    .line 209
    :cond_1b
    return-wide v2
.end method

.method private static a()Lcom/a/b/b/ak;
    .registers 1
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 403
    sget-object v0, Lcom/a/b/l/x;->a:Lcom/a/b/l/x;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Long;
    .registers 13
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const-wide/high16 v10, -0x8000000000000000L

    const/16 v8, 0x9

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 337
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    move-object v0, v4

    .line 367
    :goto_14
    return-object v0

    .line 340
    :cond_15
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x2d

    if-ne v0, v3, :cond_29

    move v5, v1

    .line 341
    :goto_1e
    if-eqz v5, :cond_2b

    move v0, v1

    .line 342
    :goto_21
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_2d

    move-object v0, v4

    .line 343
    goto :goto_14

    :cond_29
    move v5, v2

    .line 340
    goto :goto_1e

    :cond_2b
    move v0, v2

    .line 341
    goto :goto_21

    .line 345
    :cond_2d
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    .line 346
    if-ltz v0, :cond_39

    if-le v0, v8, :cond_3b

    :cond_39
    move-object v0, v4

    .line 347
    goto :goto_14

    .line 349
    :cond_3b
    neg-int v0, v0

    int-to-long v0, v0

    .line 350
    :goto_3d
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_69

    .line 351
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    add-int/lit8 v2, v2, -0x30

    .line 352
    if-ltz v2, :cond_58

    if-gt v2, v8, :cond_58

    const-wide v6, -0xcccccccccccccccL

    cmp-long v6, v0, v6

    if-gez v6, :cond_5a

    :cond_58
    move-object v0, v4

    .line 353
    goto :goto_14

    .line 355
    :cond_5a
    const-wide/16 v6, 0xa

    mul-long/2addr v0, v6

    .line 356
    int-to-long v6, v2

    add-long/2addr v6, v10

    cmp-long v6, v0, v6

    if-gez v6, :cond_65

    move-object v0, v4

    .line 357
    goto :goto_14

    .line 359
    :cond_65
    int-to-long v6, v2

    sub-long/2addr v0, v6

    move v2, v3

    goto :goto_3d

    .line 362
    :cond_69
    if-eqz v5, :cond_70

    .line 363
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_14

    .line 364
    :cond_70
    cmp-long v2, v0, v10

    if-nez v2, :cond_76

    move-object v0, v4

    .line 365
    goto :goto_14

    .line 367
    :cond_76
    neg-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_14
.end method

.method private static varargs a(Ljava/lang/String;[J)Ljava/lang/String;
    .registers 8

    .prologue
    .line 448
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    array-length v0, p1

    if-nez v0, :cond_9

    .line 450
    const-string v0, ""

    .line 459
    :goto_8
    return-object v0

    .line 454
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0xa

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 455
    const/4 v0, 0x0

    aget-wide v2, p1, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 456
    const/4 v0, 0x1

    :goto_18
    array-length v2, p1

    if-ge v0, v2, :cond_27

    .line 457
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-wide v4, p1, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 456
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 459
    :cond_27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a([JJ)Z
    .registers 10

    .prologue
    const/4 v0, 0x0

    .line 107
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_c

    aget-wide v4, p0, v1

    .line 108
    cmp-long v3, v4, p1

    if-nez v3, :cond_d

    .line 109
    const/4 v0, 0x1

    .line 112
    :cond_c
    return v0

    .line 107
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private static a(Ljava/util/Collection;)[J
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 514
    instance-of v0, p0, Lcom/a/b/l/w;

    if-eqz v0, :cond_15

    .line 515
    check-cast p0, Lcom/a/b/l/w;

    .line 1663
    invoke-virtual {p0}, Lcom/a/b/l/w;->size()I

    move-result v1

    .line 1664
    new-array v0, v1, [J

    .line 1665
    iget-object v3, p0, Lcom/a/b/l/w;->a:[J

    iget v4, p0, Lcom/a/b/l/w;->b:I

    invoke-static {v3, v4, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 525
    :goto_14
    return-object v0

    .line 518
    :cond_15
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 519
    array-length v4, v3

    .line 520
    new-array v1, v4, [J

    .line 521
    :goto_1c
    if-ge v2, v4, :cond_30

    .line 523
    aget-object v0, v3, v2

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    aput-wide v6, v1, v2

    .line 521
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    :cond_30
    move-object v0, v1

    .line 525
    goto :goto_14
.end method

.method private static a([JI)[J
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 433
    new-array v0, p1, [J

    .line 434
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 435
    return-object v0
.end method

.method private static a([JII)[J
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 424
    if-ltz p1, :cond_33

    move v0, v1

    :goto_5
    const-string v3, "Invalid minLength: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 425
    if-ltz p2, :cond_35

    move v0, v1

    :goto_15
    const-string v3, "Invalid padding: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 426
    array-length v0, p0

    if-ge v0, p1, :cond_32

    add-int v1, p1, p2

    .line 1433
    new-array v0, v1, [J

    .line 1434
    array-length v3, p0

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 426
    :cond_32
    return-object p0

    :cond_33
    move v0, v2

    .line 424
    goto :goto_5

    :cond_35
    move v0, v2

    .line 425
    goto :goto_15
.end method

.method private static varargs a([[J)[J
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 241
    .line 242
    array-length v3, p0

    move v0, v1

    move v2, v1

    :goto_4
    if-ge v0, v3, :cond_d

    aget-object v4, p0, v0

    .line 243
    array-length v4, v4

    add-int/2addr v2, v4

    .line 242
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 245
    :cond_d
    new-array v3, v2, [J

    .line 247
    array-length v4, p0

    move v0, v1

    move v2, v1

    :goto_12
    if-ge v2, v4, :cond_1f

    aget-object v5, p0, v2

    .line 248
    array-length v6, v5

    invoke-static {v5, v1, v3, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 249
    array-length v5, v5

    add-int/2addr v0, v5

    .line 247
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 251
    :cond_1f
    return-object v3
.end method

.method private static b([JJ)I
    .registers 6

    .prologue
    .line 125
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/a/b/l/u;->c([JJII)I

    move-result v0

    return v0
.end method

.method static synthetic b([JJII)I
    .registers 6

    .prologue
    .line 49
    invoke-static {p0, p1, p2, p3, p4}, Lcom/a/b/l/u;->c([JJII)I

    move-result v0

    return v0
.end method

.method private static varargs b([J)J
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    array-length v0, p0

    if-lez v0, :cond_19

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 222
    aget-wide v2, p0, v2

    .line 223
    :goto_b
    array-length v0, p0

    if-ge v1, v0, :cond_1b

    .line 224
    aget-wide v4, p0, v1

    cmp-long v0, v4, v2

    if-lez v0, :cond_16

    .line 225
    aget-wide v2, p0, v1

    .line 223
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_19
    move v0, v2

    .line 221
    goto :goto_6

    .line 228
    :cond_1b
    return-wide v2
.end method

.method private static b()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 479
    sget-object v0, Lcom/a/b/l/v;->a:Lcom/a/b/l/v;

    return-object v0
.end method

.method private static b(J)[B
    .registers 8

    .prologue
    const/16 v4, 0x8

    .line 268
    new-array v1, v4, [B

    .line 269
    const/4 v0, 0x7

    :goto_5
    if-ltz v0, :cond_12

    .line 270
    const-wide/16 v2, 0xff

    and-long/2addr v2, p0

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 271
    shr-long/2addr p0, v4

    .line 269
    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    .line 273
    :cond_12
    return-object v1
.end method

.method private static c([JJ)I
    .registers 6

    .prologue
    .line 179
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/a/b/l/u;->a([JJII)I

    move-result v0

    return v0
.end method

.method private static c([JJII)I
    .registers 10

    .prologue
    .line 131
    move v0, p3

    :goto_1
    if-ge v0, p4, :cond_d

    .line 132
    aget-wide v2, p0, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_a

    .line 136
    :goto_9
    return v0

    .line 131
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 136
    :cond_d
    const/4 v0, -0x1

    goto :goto_9
.end method

.method private static varargs c([J)Ljava/util/List;
    .registers 2

    .prologue
    .line 543
    array-length v0, p0

    if-nez v0, :cond_8

    .line 544
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 546
    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Lcom/a/b/l/w;

    invoke-direct {v0, p0}, Lcom/a/b/l/w;-><init>([J)V

    goto :goto_7
.end method

.method private static synthetic d([JJII)I
    .registers 6

    .prologue
    .line 49
    invoke-static {p0, p1, p2, p3, p4}, Lcom/a/b/l/u;->a([JJII)I

    move-result v0

    return v0
.end method
