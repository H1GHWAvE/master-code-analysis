.class public final Lcom/a/b/l/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field public static final a:I = 0x2


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(BB)C
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    .line 325
    shl-int/lit8 v0, p0, 0x8

    and-int/lit16 v1, p1, 0xff

    or-int/2addr v0, v1

    int-to-char v0, v0

    return v0
.end method

.method private static a(J)C
    .registers 6

    .prologue
    .line 80
    long-to-int v0, p0

    int-to-char v0, v0

    .line 81
    int-to-long v2, v0

    cmp-long v1, v2, p0

    if-eqz v1, :cond_22

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Out of range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_22
    return v0
.end method

.method private static a([B)C
    .registers 8
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 311
    array-length v0, p0

    if-lt v0, v6, :cond_26

    move v0, v1

    :goto_7
    const-string v3, "array too small: %s < %s"

    new-array v4, v6, [Ljava/lang/Object;

    array-length v5, p0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 313
    aget-byte v0, p0, v2

    aget-byte v1, p0, v1

    .line 1325
    shl-int/lit8 v0, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-char v0, v0

    .line 313
    return v0

    :cond_26
    move v0, v2

    .line 311
    goto :goto_7
.end method

.method private static varargs a([C)C
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 227
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 228
    aget-char v0, p0, v2

    .line 229
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 230
    aget-char v2, p0, v1

    if-ge v2, v0, :cond_14

    .line 231
    aget-char v0, p0, v1

    .line 229
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 227
    goto :goto_6

    .line 234
    :cond_19
    return v0
.end method

.method private static a(C)I
    .registers 1

    .prologue
    .line 68
    return p0
.end method

.method private static a(CC)I
    .registers 3

    .prologue
    .line 119
    sub-int v0, p0, p1

    return v0
.end method

.method static a([CCII)I
    .registers 6

    .prologue
    .line 156
    move v0, p2

    :goto_1
    if-ge v0, p3, :cond_b

    .line 157
    aget-char v1, p0, v0

    if-ne v1, p1, :cond_8

    .line 161
    :goto_7
    return v0

    .line 156
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 161
    :cond_b
    const/4 v0, -0x1

    goto :goto_7
.end method

.method private static a([C[C)I
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 176
    const-string v0, "array"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    const-string v0, "target"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    array-length v0, p1

    if-nez v0, :cond_f

    .line 191
    :goto_e
    return v1

    :cond_f
    move v0, v1

    .line 183
    :goto_10
    array-length v2, p0

    array-length v3, p1

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_2b

    move v2, v1

    .line 184
    :goto_18
    array-length v3, p1

    if-ge v2, v3, :cond_26

    .line 185
    add-int v3, v0, v2

    aget-char v3, p0, v3

    aget-char v4, p1, v2

    if-ne v3, v4, :cond_28

    .line 184
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    :cond_26
    move v1, v0

    .line 189
    goto :goto_e

    .line 183
    :cond_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 191
    :cond_2b
    const/4 v1, -0x1

    goto :goto_e
.end method

.method private static varargs a(Ljava/lang/String;[C)Ljava/lang/String;
    .registers 7

    .prologue
    .line 370
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    array-length v1, p1

    .line 372
    if-nez v1, :cond_9

    .line 373
    const-string v0, ""

    .line 382
    :goto_8
    return-object v0

    .line 376
    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v3, v1, -0x1

    mul-int/2addr v0, v3

    add-int/2addr v0, v1

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 378
    const/4 v0, 0x0

    aget-char v0, p1, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 379
    const/4 v0, 0x1

    :goto_1d
    if-ge v0, v1, :cond_2b

    .line 380
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-char v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 379
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 382
    :cond_2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 402
    sget-object v0, Lcom/a/b/l/h;->a:Lcom/a/b/l/h;

    return-object v0
.end method

.method private static a([CC)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 132
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_a

    aget-char v3, p0, v1

    .line 133
    if-ne v3, p1, :cond_b

    .line 134
    const/4 v0, 0x1

    .line 137
    :cond_a
    return v0

    .line 132
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private static a(Ljava/util/Collection;)[C
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 436
    instance-of v0, p0, Lcom/a/b/l/g;

    if-eqz v0, :cond_15

    .line 437
    check-cast p0, Lcom/a/b/l/g;

    .line 1585
    invoke-virtual {p0}, Lcom/a/b/l/g;->size()I

    move-result v1

    .line 1586
    new-array v0, v1, [C

    .line 1587
    iget-object v3, p0, Lcom/a/b/l/g;->a:[C

    iget v4, p0, Lcom/a/b/l/g;->b:I

    invoke-static {v3, v4, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 447
    :goto_14
    return-object v0

    .line 440
    :cond_15
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 441
    array-length v4, v3

    .line 442
    new-array v1, v4, [C

    .line 443
    :goto_1c
    if-ge v2, v4, :cond_30

    .line 445
    aget-object v0, v3, v2

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    aput-char v0, v1, v2

    .line 443
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    :cond_30
    move-object v0, v1

    .line 447
    goto :goto_14
.end method

.method private static a([CI)[C
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 355
    new-array v0, p1, [C

    .line 356
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 357
    return-object v0
.end method

.method private static a([CII)[C
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 346
    if-ltz p1, :cond_33

    move v0, v1

    :goto_5
    const-string v3, "Invalid minLength: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 347
    if-ltz p2, :cond_35

    move v0, v1

    :goto_15
    const-string v3, "Invalid padding: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 348
    array-length v0, p0

    if-ge v0, p1, :cond_32

    add-int v1, p1, p2

    .line 1355
    new-array v0, v1, [C

    .line 1356
    array-length v3, p0

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 348
    :cond_32
    return-object p0

    :cond_33
    move v0, v2

    .line 346
    goto :goto_5

    :cond_35
    move v0, v2

    .line 347
    goto :goto_15
.end method

.method private static varargs a([[C)[C
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 266
    .line 267
    array-length v3, p0

    move v0, v1

    move v2, v1

    :goto_4
    if-ge v0, v3, :cond_d

    aget-object v4, p0, v0

    .line 268
    array-length v4, v4

    add-int/2addr v2, v4

    .line 267
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 270
    :cond_d
    new-array v3, v2, [C

    .line 272
    array-length v4, p0

    move v0, v1

    move v2, v1

    :goto_12
    if-ge v2, v4, :cond_1f

    aget-object v5, p0, v2

    .line 273
    array-length v6, v5

    invoke-static {v5, v1, v3, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 274
    array-length v5, v5

    add-int/2addr v0, v5

    .line 272
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 276
    :cond_1f
    return-object v3
.end method

.method private static b(J)C
    .registers 4

    .prologue
    .line 97
    const-wide/32 v0, 0xffff

    cmp-long v0, p0, v0

    if-lez v0, :cond_b

    .line 98
    const v0, 0xffff

    .line 103
    :goto_a
    return v0

    .line 100
    :cond_b
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_13

    .line 101
    const/4 v0, 0x0

    goto :goto_a

    .line 103
    :cond_13
    long-to-int v0, p0

    int-to-char v0, v0

    goto :goto_a
.end method

.method private static varargs b([C)C
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 246
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 247
    aget-char v0, p0, v2

    .line 248
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 249
    aget-char v2, p0, v1

    if-le v2, v0, :cond_14

    .line 250
    aget-char v0, p0, v1

    .line 248
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 246
    goto :goto_6

    .line 253
    :cond_19
    return v0
.end method

.method private static b([CC)I
    .registers 4

    .prologue
    .line 150
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/f;->a([CCII)I

    move-result v0

    return v0
.end method

.method static b([CCII)I
    .registers 6

    .prologue
    .line 210
    add-int/lit8 v0, p3, -0x1

    :goto_2
    if-lt v0, p2, :cond_c

    .line 211
    aget-char v1, p0, v0

    if-ne v1, p1, :cond_9

    .line 215
    :goto_8
    return v0

    .line 210
    :cond_9
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 215
    :cond_c
    const/4 v0, -0x1

    goto :goto_8
.end method

.method private static b(C)[B
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    .line 292
    const/4 v0, 0x2

    new-array v0, v0, [B

    const/4 v1, 0x0

    shr-int/lit8 v2, p0, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    int-to-byte v2, p0

    aput-byte v2, v0, v1

    return-object v0
.end method

.method private static c([CC)I
    .registers 4

    .prologue
    .line 204
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/f;->b([CCII)I

    move-result v0

    return v0
.end method

.method private static synthetic c([CCII)I
    .registers 5

    .prologue
    .line 51
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/f;->a([CCII)I

    move-result v0

    return v0
.end method

.method private static varargs c([C)Ljava/util/List;
    .registers 2

    .prologue
    .line 465
    array-length v0, p0

    if-nez v0, :cond_8

    .line 466
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 468
    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Lcom/a/b/l/g;

    invoke-direct {v0, p0}, Lcom/a/b/l/g;-><init>([C)V

    goto :goto_7
.end method

.method private static synthetic d([CCII)I
    .registers 5

    .prologue
    .line 51
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/f;->b([CCII)I

    move-result v0

    return v0
.end method
