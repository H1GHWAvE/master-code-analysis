.class public final Lcom/a/b/l/i;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field public static final a:I = 0x8

.field static final b:Ljava/util/regex/Pattern;
    .annotation build Lcom/a/b/a/c;
        a = "regular expressions"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    .line 1592
    const-string v0, "(?:\\d++(?:\\.\\d*+)?|\\.\\d++)"

    .line 1593
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(?:[eE][+-]?\\d++)?[fFdD]?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1594
    const-string v1, "(?:\\p{XDigit}++(?:\\.\\p{XDigit}*+)?|\\.\\p{XDigit}++)"

    .line 1595
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "0[xX]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[pP][+-]?\\d++[fFdD]?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1596
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "[+-]?(?:NaN|Infinity|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "|"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1597
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 588
    sput-object v0, Lcom/a/b/l/i;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static varargs a([D)D
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 223
    aget-wide v2, p0, v2

    .line 224
    :goto_b
    array-length v0, p0

    if-ge v1, v0, :cond_19

    .line 225
    aget-wide v4, p0, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 222
    goto :goto_6

    .line 227
    :cond_19
    return-wide v2
.end method

.method public static a(D)I
    .registers 4

    .prologue
    .line 74
    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    return v0
.end method

.method private static a(DD)I
    .registers 6

    .prologue
    .line 96
    invoke-static {p0, p1, p2, p3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method

.method static synthetic a([DDII)I
    .registers 6

    .prologue
    .line 55
    invoke-static {p0, p1, p2, p3, p4}, Lcom/a/b/l/i;->c([DDII)I

    move-result v0

    return v0
.end method

.method private static a([D[D)I
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 169
    const-string v0, "array"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    const-string v0, "target"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    array-length v0, p1

    if-nez v0, :cond_f

    .line 184
    :goto_e
    return v1

    :cond_f
    move v0, v1

    .line 176
    :goto_10
    array-length v2, p0

    array-length v3, p1

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_2d

    move v2, v1

    .line 177
    :goto_18
    array-length v3, p1

    if-ge v2, v3, :cond_28

    .line 178
    add-int v3, v0, v2

    aget-wide v4, p0, v3

    aget-wide v6, p1, v2

    cmpl-double v3, v4, v6

    if-nez v3, :cond_2a

    .line 177
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    :cond_28
    move v1, v0

    .line 182
    goto :goto_e

    .line 176
    :cond_2a
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 184
    :cond_2d
    const/4 v1, -0x1

    goto :goto_e
.end method

.method private static a()Lcom/a/b/b/ak;
    .registers 1
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 304
    sget-object v0, Lcom/a/b/l/k;->a:Lcom/a/b/l/k;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Double;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/c;
        a = "regular expressions"
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 623
    sget-object v0, Lcom/a/b/l/i;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 627
    :try_start_c
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_13
    .catch Ljava/lang/NumberFormatException; {:try_start_c .. :try_end_13} :catch_15

    move-result-object v0

    .line 633
    :goto_14
    return-object v0

    :catch_15
    move-exception v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private static varargs a(Ljava/lang/String;[D)Ljava/lang/String;
    .registers 8

    .prologue
    .line 354
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    array-length v0, p1

    if-nez v0, :cond_9

    .line 356
    const-string v0, ""

    .line 365
    :goto_8
    return-object v0

    .line 360
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0xc

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 361
    const/4 v0, 0x0

    aget-wide v2, p1, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 362
    const/4 v0, 0x1

    :goto_18
    array-length v2, p1

    if-ge v0, v2, :cond_27

    .line 363
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-wide v4, p1, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 362
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 365
    :cond_27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a([DD)Z
    .registers 10

    .prologue
    const/4 v0, 0x0

    .line 121
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_c

    aget-wide v4, p0, v1

    .line 122
    cmpl-double v3, v4, p1

    if-nez v3, :cond_d

    .line 123
    const/4 v0, 0x1

    .line 126
    :cond_c
    return v0

    .line 121
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private static a(Ljava/util/Collection;)[D
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 420
    instance-of v0, p0, Lcom/a/b/l/j;

    if-eqz v0, :cond_15

    .line 421
    check-cast p0, Lcom/a/b/l/j;

    .line 1572
    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v1

    .line 1573
    new-array v0, v1, [D

    .line 1574
    iget-object v3, p0, Lcom/a/b/l/j;->a:[D

    iget v4, p0, Lcom/a/b/l/j;->b:I

    invoke-static {v3, v4, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 431
    :goto_14
    return-object v0

    .line 424
    :cond_15
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 425
    array-length v4, v3

    .line 426
    new-array v1, v4, [D

    .line 427
    :goto_1c
    if-ge v2, v4, :cond_30

    .line 429
    aget-object v0, v3, v2

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    aput-wide v6, v1, v2

    .line 427
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    :cond_30
    move-object v0, v1

    .line 431
    goto :goto_14
.end method

.method private static a([DI)[D
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 334
    new-array v0, p1, [D

    .line 335
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 336
    return-object v0
.end method

.method private static a([DII)[D
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    if-ltz p1, :cond_33

    move v0, v1

    :goto_5
    const-string v3, "Invalid minLength: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 326
    if-ltz p2, :cond_35

    move v0, v1

    :goto_15
    const-string v3, "Invalid padding: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 327
    array-length v0, p0

    if-ge v0, p1, :cond_32

    add-int v1, p1, p2

    .line 1334
    new-array v0, v1, [D

    .line 1335
    array-length v3, p0

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 327
    :cond_32
    return-object p0

    :cond_33
    move v0, v2

    .line 325
    goto :goto_5

    :cond_35
    move v0, v2

    .line 326
    goto :goto_15
.end method

.method private static varargs a([[D)[D
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 258
    .line 259
    array-length v3, p0

    move v0, v1

    move v2, v1

    :goto_4
    if-ge v0, v3, :cond_d

    aget-object v4, p0, v0

    .line 260
    array-length v4, v4

    add-int/2addr v2, v4

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 262
    :cond_d
    new-array v3, v2, [D

    .line 264
    array-length v4, p0

    move v0, v1

    move v2, v1

    :goto_12
    if-ge v2, v4, :cond_1f

    aget-object v5, p0, v2

    .line 265
    array-length v6, v5

    invoke-static {v5, v1, v3, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 266
    array-length v5, v5

    add-int/2addr v0, v5

    .line 264
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 268
    :cond_1f
    return-object v3
.end method

.method private static varargs b([D)D
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 240
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 241
    aget-wide v2, p0, v2

    .line 242
    :goto_b
    array-length v0, p0

    if-ge v1, v0, :cond_19

    .line 243
    aget-wide v4, p0, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 242
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 240
    goto :goto_6

    .line 245
    :cond_19
    return-wide v2
.end method

.method private static b([DD)I
    .registers 6

    .prologue
    .line 140
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/a/b/l/i;->c([DDII)I

    move-result v0

    return v0
.end method

.method static synthetic b([DDII)I
    .registers 6

    .prologue
    .line 55
    invoke-static {p0, p1, p2, p3, p4}, Lcom/a/b/l/i;->d([DDII)I

    move-result v0

    return v0
.end method

.method private static b()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 385
    sget-object v0, Lcom/a/b/l/l;->a:Lcom/a/b/l/l;

    return-object v0
.end method

.method private static b(D)Z
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 107
    const-wide/high16 v2, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    cmpg-double v2, v2, p0

    if-gez v2, :cond_11

    move v2, v0

    :goto_9
    const-wide/high16 v4, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    cmpg-double v3, p0, v4

    if-gez v3, :cond_13

    :goto_f
    and-int/2addr v0, v2

    return v0

    :cond_11
    move v2, v1

    goto :goto_9

    :cond_13
    move v0, v1

    goto :goto_f
.end method

.method private static c([DD)I
    .registers 6

    .prologue
    .line 198
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/a/b/l/i;->d([DDII)I

    move-result v0

    return v0
.end method

.method private static c([DDII)I
    .registers 10

    .prologue
    .line 146
    move v0, p3

    :goto_1
    if-ge v0, p4, :cond_d

    .line 147
    aget-wide v2, p0, v0

    cmpl-double v1, v2, p1

    if-nez v1, :cond_a

    .line 151
    :goto_9
    return v0

    .line 146
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 151
    :cond_d
    const/4 v0, -0x1

    goto :goto_9
.end method

.method private static varargs c([D)Ljava/util/List;
    .registers 2

    .prologue
    .line 452
    array-length v0, p0

    if-nez v0, :cond_8

    .line 453
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 455
    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Lcom/a/b/l/j;

    invoke-direct {v0, p0}, Lcom/a/b/l/j;-><init>([D)V

    goto :goto_7
.end method

.method private static c()Ljava/util/regex/Pattern;
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "regular expressions"
    .end annotation

    .prologue
    .line 592
    const-string v0, "(?:\\d++(?:\\.\\d*+)?|\\.\\d++)"

    .line 593
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(?:[eE][+-]?\\d++)?[fFdD]?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 594
    const-string v1, "(?:\\p{XDigit}++(?:\\.\\p{XDigit}*+)?|\\.\\p{XDigit}++)"

    .line 595
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "0[xX]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[pP][+-]?\\d++[fFdD]?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 596
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "[+-]?(?:NaN|Infinity|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "|"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 597
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0
.end method

.method private static d([DDII)I
    .registers 10

    .prologue
    .line 204
    add-int/lit8 v0, p4, -0x1

    :goto_2
    if-lt v0, p3, :cond_e

    .line 205
    aget-wide v2, p0, v0

    cmpl-double v1, v2, p1

    if-nez v1, :cond_b

    .line 209
    :goto_a
    return v0

    .line 204
    :cond_b
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 209
    :cond_e
    const/4 v0, -0x1

    goto :goto_a
.end method
