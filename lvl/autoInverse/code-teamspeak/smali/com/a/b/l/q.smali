.class public final Lcom/a/b/l/q;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field public static final a:I = 0x4

.field public static final b:I = 0x40000000

.field private static final c:[B


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 639
    const/16 v1, 0x80

    new-array v1, v1, [B

    .line 642
    sput-object v1, Lcom/a/b/l/q;->c:[B

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([BB)V

    move v1, v0

    .line 643
    :goto_c
    const/16 v2, 0x9

    if-gt v1, v2, :cond_1a

    .line 644
    sget-object v2, Lcom/a/b/l/q;->c:[B

    add-int/lit8 v3, v1, 0x30

    int-to-byte v4, v1

    aput-byte v4, v2, v3

    .line 643
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 646
    :cond_1a
    :goto_1a
    const/16 v1, 0x1a

    if-gt v0, v1, :cond_33

    .line 647
    sget-object v1, Lcom/a/b/l/q;->c:[B

    add-int/lit8 v2, v0, 0x41

    add-int/lit8 v3, v0, 0xa

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 648
    sget-object v1, Lcom/a/b/l/q;->c:[B

    add-int/lit8 v2, v0, 0x61

    add-int/lit8 v3, v0, 0xa

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 646
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 650
    :cond_33
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(BBBB)I
    .registers 6
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    .line 335
    shl-int/lit8 v0, p0, 0x18

    and-int/lit16 v1, p1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, p2, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, p3, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method private static a(C)I
    .registers 2

    .prologue
    .line 653
    const/16 v0, 0x80

    if-ge p0, v0, :cond_9

    sget-object v0, Lcom/a/b/l/q;->c:[B

    aget-byte v0, v0, p0

    :goto_8
    return v0

    :cond_9
    const/4 v0, -0x1

    goto :goto_8
.end method

.method public static a(I)I
    .registers 1

    .prologue
    .line 76
    return p0
.end method

.method public static a(II)I
    .registers 3

    .prologue
    .line 127
    if-ge p0, p1, :cond_4

    const/4 v0, -0x1

    :goto_3
    return v0

    :cond_4
    if-le p0, p1, :cond_8

    const/4 v0, 0x1

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(J)I
    .registers 6

    .prologue
    .line 88
    long-to-int v0, p0

    .line 89
    int-to-long v2, v0

    cmp-long v1, v2, p0

    if-eqz v1, :cond_21

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Out of range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_21
    return v0
.end method

.method private static a([B)I
    .registers 9
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 321
    array-length v0, p0

    if-lt v0, v7, :cond_2a

    move v0, v1

    :goto_8
    const-string v3, "array too small: %s < %s"

    new-array v4, v6, [Ljava/lang/Object;

    array-length v5, p0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 323
    aget-byte v0, p0, v2

    aget-byte v1, p0, v1

    aget-byte v2, p0, v6

    const/4 v3, 0x3

    aget-byte v3, p0, v3

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/q;->a(BBBB)I

    move-result v0

    return v0

    :cond_2a
    move v0, v2

    .line 321
    goto :goto_8
.end method

.method public static varargs a([I)I
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 235
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Z)V

    .line 236
    const/4 v0, 0x0

    aget v0, p0, v0

    move v1, v2

    .line 237
    :goto_8
    const/4 v3, 0x2

    if-ge v1, v3, :cond_14

    .line 238
    aget v3, p0, v2

    if-ge v3, v0, :cond_11

    .line 239
    aget v0, p0, v2

    .line 237
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 242
    :cond_14
    return v0
.end method

.method static synthetic a([IIII)I
    .registers 5

    .prologue
    .line 52
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/q;->c([IIII)I

    move-result v0

    return v0
.end method

.method private static a([I[I)I
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 184
    const-string v0, "array"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    const-string v0, "target"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    array-length v0, p1

    if-nez v0, :cond_f

    .line 199
    :goto_e
    return v1

    :cond_f
    move v0, v1

    .line 191
    :goto_10
    array-length v2, p0

    array-length v3, p1

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_2b

    move v2, v1

    .line 192
    :goto_18
    array-length v3, p1

    if-ge v2, v3, :cond_26

    .line 193
    add-int v3, v0, v2

    aget v3, p0, v3

    aget v4, p1, v2

    if-ne v3, v4, :cond_28

    .line 192
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    :cond_26
    move v1, v0

    .line 197
    goto :goto_e

    .line 191
    :cond_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 199
    :cond_2b
    const/4 v1, -0x1

    goto :goto_e
.end method

.method private static a()Lcom/a/b/b/ak;
    .registers 1
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 371
    sget-object v0, Lcom/a/b/l/s;->a:Lcom/a/b/l/s;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Integer;
    .registers 9
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation runtime Ljavax/annotation/CheckForNull;
    .end annotation

    .prologue
    const/16 v7, 0xa

    const/4 v1, 0x1

    const/high16 v6, -0x80000000

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 679
    .line 1706
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    move-object v0, v3

    .line 1741
    :goto_14
    return-object v0

    .line 1713
    :cond_15
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v4, 0x2d

    if-ne v0, v4, :cond_29

    move v4, v1

    .line 1714
    :goto_1e
    if-eqz v4, :cond_2b

    move v0, v1

    .line 1715
    :goto_21
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_2d

    move-object v0, v3

    .line 1716
    goto :goto_14

    :cond_29
    move v4, v2

    .line 1713
    goto :goto_1e

    :cond_2b
    move v0, v2

    .line 1714
    goto :goto_21

    .line 1718
    :cond_2d
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/a/b/l/q;->a(C)I

    move-result v0

    .line 1719
    if-ltz v0, :cond_3b

    if-lt v0, v7, :cond_3d

    :cond_3b
    move-object v0, v3

    .line 1720
    goto :goto_14

    .line 1722
    :cond_3d
    neg-int v0, v0

    .line 1726
    :goto_3e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_64

    .line 1727
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/a/b/l/q;->a(C)I

    move-result v1

    .line 1728
    if-ltz v1, :cond_57

    if-ge v1, v7, :cond_57

    const v5, -0xccccccc

    if-ge v0, v5, :cond_59

    :cond_57
    move-object v0, v3

    .line 1729
    goto :goto_14

    .line 1731
    :cond_59
    mul-int/lit8 v0, v0, 0xa

    .line 1732
    add-int v5, v6, v1

    if-ge v0, v5, :cond_61

    move-object v0, v3

    .line 1733
    goto :goto_14

    .line 1735
    :cond_61
    sub-int/2addr v0, v1

    move v1, v2

    goto :goto_3e

    .line 1738
    :cond_64
    if-eqz v4, :cond_6b

    .line 1739
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_14

    .line 1740
    :cond_6b
    if-ne v0, v6, :cond_6f

    move-object v0, v3

    .line 1741
    goto :goto_14

    .line 1743
    :cond_6f
    neg-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_14
.end method

.method private static varargs a(Ljava/lang/String;[I)Ljava/lang/String;
    .registers 6

    .prologue
    .line 416
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    array-length v0, p1

    if-nez v0, :cond_9

    .line 418
    const-string v0, ""

    .line 427
    :goto_8
    return-object v0

    .line 422
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 423
    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 424
    const/4 v0, 0x1

    :goto_18
    array-length v2, p1

    if-ge v0, v2, :cond_27

    .line 425
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 424
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 427
    :cond_27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a([II)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 140
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_a

    aget v3, p0, v1

    .line 141
    if-ne v3, p1, :cond_b

    .line 142
    const/4 v0, 0x1

    .line 145
    :cond_a
    return v0

    .line 140
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private static a(Ljava/util/Collection;)[I
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 481
    instance-of v0, p0, Lcom/a/b/l/r;

    if-eqz v0, :cond_15

    .line 482
    check-cast p0, Lcom/a/b/l/r;

    .line 1630
    invoke-virtual {p0}, Lcom/a/b/l/r;->size()I

    move-result v1

    .line 1631
    new-array v0, v1, [I

    .line 1632
    iget-object v3, p0, Lcom/a/b/l/r;->a:[I

    iget v4, p0, Lcom/a/b/l/r;->b:I

    invoke-static {v3, v4, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 492
    :goto_14
    return-object v0

    .line 485
    :cond_15
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 486
    array-length v4, v3

    .line 487
    new-array v1, v4, [I

    .line 488
    :goto_1c
    if-ge v2, v4, :cond_30

    .line 490
    aget-object v0, v3, v2

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    aput v0, v1, v2

    .line 488
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    :cond_30
    move-object v0, v1

    .line 492
    goto :goto_14
.end method

.method private static a([III)[I
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 392
    if-ltz p1, :cond_33

    move v0, v1

    :goto_5
    const-string v3, "Invalid minLength: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 393
    if-ltz p2, :cond_35

    move v0, v1

    :goto_15
    const-string v3, "Invalid padding: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 394
    array-length v0, p0

    if-ge v0, p1, :cond_32

    add-int v1, p1, p2

    .line 1401
    new-array v0, v1, [I

    .line 1402
    array-length v3, p0

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 394
    :cond_32
    return-object p0

    :cond_33
    move v0, v2

    .line 392
    goto :goto_5

    :cond_35
    move v0, v2

    .line 393
    goto :goto_15
.end method

.method private static varargs a([[I)[I
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 274
    .line 275
    array-length v3, p0

    move v0, v1

    move v2, v1

    :goto_4
    if-ge v0, v3, :cond_d

    aget-object v4, p0, v0

    .line 276
    array-length v4, v4

    add-int/2addr v2, v4

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 278
    :cond_d
    new-array v3, v2, [I

    .line 280
    array-length v4, p0

    move v0, v1

    move v2, v1

    :goto_12
    if-ge v2, v4, :cond_1f

    aget-object v5, p0, v2

    .line 281
    array-length v6, v5

    invoke-static {v5, v1, v3, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 282
    array-length v5, v5

    add-int/2addr v0, v5

    .line 280
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 284
    :cond_1f
    return-object v3
.end method

.method public static b(J)I
    .registers 4

    .prologue
    .line 105
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p0, v0

    if-lez v0, :cond_b

    .line 106
    const v0, 0x7fffffff

    .line 111
    :goto_a
    return v0

    .line 108
    :cond_b
    const-wide/32 v0, -0x80000000

    cmp-long v0, p0, v0

    if-gez v0, :cond_15

    .line 109
    const/high16 v0, -0x80000000

    goto :goto_a

    .line 111
    :cond_15
    long-to-int v0, p0

    goto :goto_a
.end method

.method private static varargs b([I)I
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 254
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 255
    aget v0, p0, v2

    .line 256
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 257
    aget v2, p0, v1

    if-le v2, v0, :cond_14

    .line 258
    aget v0, p0, v1

    .line 256
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 254
    goto :goto_6

    .line 261
    :cond_19
    return v0
.end method

.method private static b([II)I
    .registers 4

    .prologue
    .line 158
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/q;->c([IIII)I

    move-result v0

    return v0
.end method

.method static synthetic b([IIII)I
    .registers 5

    .prologue
    .line 52
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/q;->d([IIII)I

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/Integer;
    .registers 9
    .annotation runtime Ljavax/annotation/CheckForNull;
    .end annotation

    .prologue
    const/16 v7, 0xa

    const/4 v1, 0x1

    const/high16 v6, -0x80000000

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 706
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    move-object v0, v3

    .line 743
    :goto_14
    return-object v0

    .line 713
    :cond_15
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v4, 0x2d

    if-ne v0, v4, :cond_29

    move v4, v1

    .line 714
    :goto_1e
    if-eqz v4, :cond_2b

    move v0, v1

    .line 715
    :goto_21
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_2d

    move-object v0, v3

    .line 716
    goto :goto_14

    :cond_29
    move v4, v2

    .line 713
    goto :goto_1e

    :cond_2b
    move v0, v2

    .line 714
    goto :goto_21

    .line 718
    :cond_2d
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/a/b/l/q;->a(C)I

    move-result v0

    .line 719
    if-ltz v0, :cond_3b

    if-lt v0, v7, :cond_3d

    :cond_3b
    move-object v0, v3

    .line 720
    goto :goto_14

    .line 722
    :cond_3d
    neg-int v0, v0

    .line 726
    :goto_3e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_64

    .line 727
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/a/b/l/q;->a(C)I

    move-result v1

    .line 728
    if-ltz v1, :cond_57

    if-ge v1, v7, :cond_57

    const v5, -0xccccccc

    if-ge v0, v5, :cond_59

    :cond_57
    move-object v0, v3

    .line 729
    goto :goto_14

    .line 731
    :cond_59
    mul-int/lit8 v0, v0, 0xa

    .line 732
    add-int v5, v6, v1

    if-ge v0, v5, :cond_61

    move-object v0, v3

    .line 733
    goto :goto_14

    .line 735
    :cond_61
    sub-int/2addr v0, v1

    move v1, v2

    goto :goto_3e

    .line 738
    :cond_64
    if-eqz v4, :cond_6b

    .line 739
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_14

    .line 740
    :cond_6b
    if-ne v0, v6, :cond_6f

    move-object v0, v3

    .line 741
    goto :goto_14

    .line 743
    :cond_6f
    neg-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_14
.end method

.method private static b()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 446
    sget-object v0, Lcom/a/b/l/t;->a:Lcom/a/b/l/t;

    return-object v0
.end method

.method public static b(I)[B
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    .line 300
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    shr-int/lit8 v2, p0, 0x18

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    shr-int/lit8 v2, p0, 0x10

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    shr-int/lit8 v2, p0, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    int-to-byte v2, p0

    aput-byte v2, v0, v1

    return-object v0
.end method

.method private static c([II)I
    .registers 4

    .prologue
    .line 212
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/q;->d([IIII)I

    move-result v0

    return v0
.end method

.method private static c([IIII)I
    .registers 6

    .prologue
    .line 164
    move v0, p2

    :goto_1
    if-ge v0, p3, :cond_b

    .line 165
    aget v1, p0, v0

    if-ne v1, p1, :cond_8

    .line 169
    :goto_7
    return v0

    .line 164
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 169
    :cond_b
    const/4 v0, -0x1

    goto :goto_7
.end method

.method private static varargs c([I)Ljava/util/List;
    .registers 2

    .prologue
    .line 510
    array-length v0, p0

    if-nez v0, :cond_8

    .line 511
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 513
    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Lcom/a/b/l/r;

    invoke-direct {v0, p0}, Lcom/a/b/l/r;-><init>([I)V

    goto :goto_7
.end method

.method private static d([IIII)I
    .registers 6

    .prologue
    .line 218
    add-int/lit8 v0, p3, -0x1

    :goto_2
    if-lt v0, p2, :cond_c

    .line 219
    aget v1, p0, v0

    if-ne v1, p1, :cond_9

    .line 223
    :goto_8
    return v0

    .line 218
    :cond_9
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 223
    :cond_c
    const/4 v0, -0x1

    goto :goto_8
.end method

.method private static d([II)[I
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 401
    new-array v0, p1, [I

    .line 402
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 403
    return-object v0
.end method
