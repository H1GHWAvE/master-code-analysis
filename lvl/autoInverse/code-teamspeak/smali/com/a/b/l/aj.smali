.class final enum Lcom/a/b/l/aj;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# static fields
.field public static final enum a:Lcom/a/b/l/aj;

.field static final b:Z

.field static final c:Lsun/misc/Unsafe;

.field static final d:I

.field private static final synthetic e:[Lcom/a/b/l/aj;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    .line 299
    new-instance v0, Lcom/a/b/l/aj;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/aj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/aj;->a:Lcom/a/b/l/aj;

    .line 297
    new-array v0, v3, [Lcom/a/b/l/aj;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/aj;->a:Lcom/a/b/l/aj;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/aj;->e:[Lcom/a/b/l/aj;

    .line 301
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/a/b/l/aj;->b:Z

    .line 329
    invoke-static {}, Lcom/a/b/l/aj;->a()Lsun/misc/Unsafe;

    move-result-object v0

    .line 331
    sput-object v0, Lcom/a/b/l/aj;->c:Lsun/misc/Unsafe;

    const-class v1, [B

    invoke-virtual {v0, v1}, Lsun/misc/Unsafe;->arrayBaseOffset(Ljava/lang/Class;)I

    move-result v0

    sput v0, Lcom/a/b/l/aj;->d:I

    .line 334
    sget-object v0, Lcom/a/b/l/aj;->c:Lsun/misc/Unsafe;

    const-class v1, [B

    invoke-virtual {v0, v1}, Lsun/misc/Unsafe;->arrayIndexScale(Ljava/lang/Class;)I

    move-result v0

    if-eq v0, v3, :cond_3d

    .line 335
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 337
    :cond_3d
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 298
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([B[B)I
    .registers 14

    .prologue
    const-wide/16 v10, 0xff

    .line 370
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 371
    div-int/lit8 v1, v2, 0x8

    .line 378
    const/4 v0, 0x0

    :goto_b
    mul-int/lit8 v3, v1, 0x8

    if-ge v0, v3, :cond_47

    .line 379
    sget-object v3, Lcom/a/b/l/aj;->c:Lsun/misc/Unsafe;

    sget v4, Lcom/a/b/l/aj;->d:I

    int-to-long v4, v4

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-virtual {v3, p0, v4, v5}, Lsun/misc/Unsafe;->getLong(Ljava/lang/Object;J)J

    move-result-wide v4

    .line 380
    sget-object v3, Lcom/a/b/l/aj;->c:Lsun/misc/Unsafe;

    sget v6, Lcom/a/b/l/aj;->d:I

    int-to-long v6, v6

    int-to-long v8, v0

    add-long/2addr v6, v8

    invoke-virtual {v3, p1, v6, v7}, Lsun/misc/Unsafe;->getLong(Ljava/lang/Object;J)J

    move-result-wide v6

    .line 381
    cmp-long v3, v4, v6

    if-eqz v3, :cond_44

    .line 382
    sget-boolean v0, Lcom/a/b/l/aj;->b:Z

    if-eqz v0, :cond_32

    .line 383
    invoke-static {v4, v5, v6, v7}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v0

    .line 405
    :cond_31
    :goto_31
    return v0

    .line 393
    :cond_32
    xor-long v0, v4, v6

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v0

    and-int/lit8 v0, v0, -0x8

    .line 394
    ushr-long v2, v4, v0

    and-long/2addr v2, v10

    ushr-long v0, v6, v0

    and-long/2addr v0, v10

    sub-long v0, v2, v0

    long-to-int v0, v0

    goto :goto_31

    .line 378
    :cond_44
    add-int/lit8 v0, v0, 0x8

    goto :goto_b

    .line 399
    :cond_47
    mul-int/lit8 v0, v1, 0x8

    move v1, v0

    :goto_4a
    if-ge v1, v2, :cond_5a

    .line 400
    aget-byte v0, p0, v1

    aget-byte v3, p1, v1

    invoke-static {v0, v3}, Lcom/a/b/l/ag;->a(BB)I

    move-result v0

    .line 401
    if-nez v0, :cond_31

    .line 399
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4a

    .line 405
    :cond_5a
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_31
.end method

.method private static a()Lsun/misc/Unsafe;
    .registers 3

    .prologue
    .line 348
    :try_start_0
    invoke-static {}, Lsun/misc/Unsafe;->getUnsafe()Lsun/misc/Unsafe;
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    .line 351
    :goto_4
    return-object v0

    :catch_5
    move-exception v0

    :try_start_6
    new-instance v0, Lcom/a/b/l/ak;

    invoke-direct {v0}, Lcom/a/b/l/ak;-><init>()V

    invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsun/misc/Unsafe;
    :try_end_11
    .catch Ljava/security/PrivilegedActionException; {:try_start_6 .. :try_end_11} :catch_12

    goto :goto_4

    .line 363
    :catch_12
    move-exception v0

    .line 364
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not initialize intrinsics"

    invoke-virtual {v0}, Ljava/security/PrivilegedActionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/aj;
    .registers 2

    .prologue
    .line 297
    const-class v0, Lcom/a/b/l/aj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/aj;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/aj;
    .registers 1

    .prologue
    .line 297
    sget-object v0, Lcom/a/b/l/aj;->e:[Lcom/a/b/l/aj;

    invoke-virtual {v0}, [Lcom/a/b/l/aj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/aj;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 15

    .prologue
    const-wide/16 v10, 0xff

    .line 297
    check-cast p1, [B

    check-cast p2, [B

    .line 1370
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1371
    div-int/lit8 v1, v2, 0x8

    .line 1378
    const/4 v0, 0x0

    :goto_f
    mul-int/lit8 v3, v1, 0x8

    if-ge v0, v3, :cond_4b

    .line 1379
    sget-object v3, Lcom/a/b/l/aj;->c:Lsun/misc/Unsafe;

    sget v4, Lcom/a/b/l/aj;->d:I

    int-to-long v4, v4

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-virtual {v3, p1, v4, v5}, Lsun/misc/Unsafe;->getLong(Ljava/lang/Object;J)J

    move-result-wide v4

    .line 1380
    sget-object v3, Lcom/a/b/l/aj;->c:Lsun/misc/Unsafe;

    sget v6, Lcom/a/b/l/aj;->d:I

    int-to-long v6, v6

    int-to-long v8, v0

    add-long/2addr v6, v8

    invoke-virtual {v3, p2, v6, v7}, Lsun/misc/Unsafe;->getLong(Ljava/lang/Object;J)J

    move-result-wide v6

    .line 1381
    cmp-long v3, v4, v6

    if-eqz v3, :cond_48

    .line 1382
    sget-boolean v0, Lcom/a/b/l/aj;->b:Z

    if-eqz v0, :cond_36

    .line 1383
    invoke-static {v4, v5, v6, v7}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v0

    .line 1402
    :cond_35
    :goto_35
    return v0

    .line 1393
    :cond_36
    xor-long v0, v4, v6

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v0

    and-int/lit8 v0, v0, -0x8

    .line 1394
    ushr-long v2, v4, v0

    and-long/2addr v2, v10

    ushr-long v0, v6, v0

    and-long/2addr v0, v10

    sub-long v0, v2, v0

    long-to-int v0, v0

    goto :goto_35

    .line 1378
    :cond_48
    add-int/lit8 v0, v0, 0x8

    goto :goto_f

    .line 1399
    :cond_4b
    mul-int/lit8 v0, v1, 0x8

    move v1, v0

    :goto_4e
    if-ge v1, v2, :cond_5e

    .line 1400
    aget-byte v0, p1, v1

    aget-byte v3, p2, v1

    invoke-static {v0, v3}, Lcom/a/b/l/ag;->a(BB)I

    move-result v0

    .line 1401
    if-nez v0, :cond_35

    .line 1399
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4e

    .line 1405
    :cond_5e
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 297
    goto :goto_35
.end method
