.class Lcom/a/b/m/g;
.super Ljava/lang/reflect/AccessibleObject;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/Member;


# instance fields
.field private final a:Ljava/lang/reflect/AccessibleObject;

.field private final b:Ljava/lang/reflect/Member;


# direct methods
.method constructor <init>(Ljava/lang/reflect/AccessibleObject;)V
    .registers 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/reflect/AccessibleObject;-><init>()V

    .line 43
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iput-object p1, p0, Lcom/a/b/m/g;->a:Ljava/lang/reflect/AccessibleObject;

    .line 45
    check-cast p1, Ljava/lang/reflect/Member;

    iput-object p1, p0, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    .line 46
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v0

    return v0
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isProtected(I)Z

    move-result v0

    return v0
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 104
    .line 1109
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v0

    .line 104
    if-nez v0, :cond_20

    .line 2094
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v0

    .line 104
    if-nez v0, :cond_20

    .line 2099
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isProtected(I)Z

    move-result v0

    .line 104
    if-nez v0, :cond_20

    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v0

    return v0
.end method

.method private f()Z
    .registers 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    return v0
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v0

    return v0
.end method

.method private h()Z
    .registers 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isAbstract(I)Z

    move-result v0

    return v0
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isNative(I)Z

    move-result v0

    return v0
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isSynchronized(I)Z

    move-result v0

    return v0
.end method

.method private k()Z
    .registers 2

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isVolatile(I)Z

    move-result v0

    return v0
.end method

.method private l()Z
    .registers 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isTransient(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Lcom/a/b/m/ae;
    .registers 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/a/b/m/g;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 154
    instance-of v1, p1, Lcom/a/b/m/g;

    if-eqz v1, :cond_20

    .line 155
    check-cast p1, Lcom/a/b/m/g;

    .line 156
    invoke-virtual {p0}, Lcom/a/b/m/g;->a()Lcom/a/b/m/ae;

    move-result-object v1

    invoke-virtual {p1}, Lcom/a/b/m/g;->a()Lcom/a/b/m/ae;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/b/m/ae;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    iget-object v2, p1, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    const/4 v0, 0x1

    .line 158
    :cond_20
    return v0
.end method

.method public final getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .registers 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/b/m/g;->a:Ljava/lang/reflect/AccessibleObject;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/AccessibleObject;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public final getAnnotations()[Ljava/lang/annotation/Annotation;
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/a/b/m/g;->a:Ljava/lang/reflect/AccessibleObject;

    invoke-virtual {v0}, Ljava/lang/reflect/AccessibleObject;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public final getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/m/g;->a:Ljava/lang/reflect/AccessibleObject;

    invoke-virtual {v0}, Ljava/lang/reflect/AccessibleObject;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public getDeclaringClass()Ljava/lang/Class;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    invoke-interface {v0}, Ljava/lang/reflect/Member;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final getModifiers()I
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    invoke-interface {v0}, Ljava/lang/reflect/Member;->getModifiers()I

    move-result v0

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    invoke-interface {v0}, Ljava/lang/reflect/Member;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isAccessible()Z
    .registers 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/a/b/m/g;->a:Ljava/lang/reflect/AccessibleObject;

    invoke-virtual {v0}, Ljava/lang/reflect/AccessibleObject;->isAccessible()Z

    move-result v0

    return v0
.end method

.method public final isAnnotationPresent(Ljava/lang/Class;)Z
    .registers 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/b/m/g;->a:Ljava/lang/reflect/AccessibleObject;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/AccessibleObject;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public final isSynthetic()Z
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    invoke-interface {v0}, Ljava/lang/reflect/Member;->isSynthetic()Z

    move-result v0

    return v0
.end method

.method public final setAccessible(Z)V
    .registers 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/b/m/g;->a:Ljava/lang/reflect/AccessibleObject;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 70
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/a/b/m/g;->b:Ljava/lang/reflect/Member;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
