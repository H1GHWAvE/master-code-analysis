.class final Lcom/a/b/m/ak;
.super Lcom/a/b/m/aw;
.source "SourceFile"


# static fields
.field private static final e:J


# instance fields
.field final synthetic a:Lcom/a/b/m/ae;

.field private final transient c:Lcom/a/b/m/aw;

.field private transient d:Lcom/a/b/d/lo;


# direct methods
.method constructor <init>(Lcom/a/b/m/ae;Lcom/a/b/m/aw;)V
    .registers 3

    .prologue
    .line 595
    iput-object p1, p0, Lcom/a/b/m/ak;->a:Lcom/a/b/m/ae;

    invoke-direct {p0, p1}, Lcom/a/b/m/aw;-><init>(Lcom/a/b/m/ae;)V

    .line 596
    iput-object p2, p0, Lcom/a/b/m/ak;->c:Lcom/a/b/m/aw;

    .line 597
    return-void
.end method

.method private f()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 633
    iget-object v0, p0, Lcom/a/b/m/ak;->a:Lcom/a/b/m/ae;

    invoke-virtual {v0}, Lcom/a/b/m/ae;->b()Lcom/a/b/m/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/m/aw;->e()Lcom/a/b/m/aw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 3

    .prologue
    .line 600
    iget-object v0, p0, Lcom/a/b/m/ak;->d:Lcom/a/b/d/lo;

    .line 601
    if-nez v0, :cond_18

    .line 602
    iget-object v0, p0, Lcom/a/b/m/ak;->c:Lcom/a/b/m/aw;

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    sget-object v1, Lcom/a/b/m/at;->b:Lcom/a/b/m/at;

    invoke-virtual {v0, v1}, Lcom/a/b/d/gd;->a(Lcom/a/b/b/co;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 1396
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 602
    iput-object v0, p0, Lcom/a/b/m/ak;->d:Lcom/a/b/d/lo;

    .line 606
    :cond_18
    return-object v0
.end method

.method protected final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 590
    invoke-virtual {p0}, Lcom/a/b/m/ak;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/a/b/m/aw;
    .registers 3

    .prologue
    .line 629
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "interfaces().classes() not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Ljava/util/Set;
    .registers 3

    .prologue
    .line 617
    sget-object v0, Lcom/a/b/m/an;->b:Lcom/a/b/m/an;

    iget-object v1, p0, Lcom/a/b/m/ak;->a:Lcom/a/b/m/ae;

    .line 2190
    iget-object v1, v1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/a/b/m/ae;->f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;

    move-result-object v1

    .line 617
    invoke-virtual {v0, v1}, Lcom/a/b/m/an;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 619
    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    new-instance v1, Lcom/a/b/m/al;

    invoke-direct {v1, p0}, Lcom/a/b/m/al;-><init>(Lcom/a/b/m/ak;)V

    invoke-virtual {v0, v1}, Lcom/a/b/d/gd;->a(Lcom/a/b/b/co;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 2396
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 619
    return-object v0
.end method

.method public final e()Lcom/a/b/m/aw;
    .registers 1

    .prologue
    .line 611
    return-object p0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 590
    invoke-virtual {p0}, Lcom/a/b/m/ak;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
