.class final Lcom/a/b/m/ao;
.super Lcom/a/b/m/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 1070
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/m/an;-><init>(B)V

    return-void
.end method

.method private static a(Lcom/a/b/m/ae;)Ljava/lang/Class;
    .registers 2

    .prologue
    .line 1072
    .line 1177
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 1072
    return-object v0
.end method

.method private static b(Lcom/a/b/m/ae;)Ljava/lang/Iterable;
    .registers 6

    .prologue
    .line 1076
    .line 1327
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_13

    .line 1328
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 1331
    :goto_12
    return-object v0

    .line 1330
    :cond_13
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_26

    .line 1331
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_12

    .line 1333
    :cond_26
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v1

    .line 2177
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 1334
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_36
    if-ge v0, v3, :cond_44

    aget-object v4, v2, v0

    .line 1336
    invoke-virtual {p0, v4}, Lcom/a/b/m/ae;->c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v4

    .line 1338
    invoke-virtual {v1, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 1334
    add-int/lit8 v0, v0, 0x1

    goto :goto_36

    .line 1340
    :cond_44
    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_12
.end method

.method private static c(Lcom/a/b/m/ae;)Lcom/a/b/m/ae;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1081
    .line 2287
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_16

    .line 2289
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/b/m/ae;->d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 2297
    :goto_15
    return-object v0

    .line 2291
    :cond_16
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_2b

    .line 2293
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/b/m/ae;->d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_15

    .line 3177
    :cond_2b
    iget-object v0, p0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 2295
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 2296
    if-nez v0, :cond_39

    .line 2297
    const/4 v0, 0x0

    goto :goto_15

    .line 2300
    :cond_39
    invoke-virtual {p0, v0}, Lcom/a/b/m/ae;->c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_15
.end method


# virtual methods
.method final synthetic b(Ljava/lang/Object;)Ljava/lang/Class;
    .registers 3

    .prologue
    .line 1070
    check-cast p1, Lcom/a/b/m/ae;

    .line 8177
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 1070
    return-object v0
.end method

.method final synthetic c(Ljava/lang/Object;)Ljava/lang/Iterable;
    .registers 7

    .prologue
    .line 1070
    check-cast p1, Lcom/a/b/m/ae;

    .line 6327
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_15

    .line 6328
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 6331
    :goto_14
    return-object v0

    .line 6330
    :cond_15
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_28

    .line 6331
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_14

    .line 6333
    :cond_28
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v1

    .line 7177
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 6334
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_38
    if-ge v0, v3, :cond_46

    aget-object v4, v2, v0

    .line 6336
    invoke-virtual {p1, v4}, Lcom/a/b/m/ae;->c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v4

    .line 6338
    invoke-virtual {v1, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 6334
    add-int/lit8 v0, v0, 0x1

    goto :goto_38

    .line 6340
    :cond_46
    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_14
.end method

.method final synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 1070
    check-cast p1, Lcom/a/b/m/ae;

    .line 4287
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_18

    .line 4289
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/b/m/ae;->d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 4297
    :goto_17
    return-object v0

    .line 4291
    :cond_18
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_2d

    .line 4293
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/b/m/ae;->d(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_17

    .line 5177
    :cond_2d
    iget-object v0, p1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/b/m/ae;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 4295
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 4296
    if-nez v0, :cond_3b

    .line 4297
    const/4 v0, 0x0

    goto :goto_17

    .line 4300
    :cond_3b
    invoke-virtual {p1, v0}, Lcom/a/b/m/ae;->c(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    goto :goto_17
.end method
