.class public abstract Lcom/a/b/i/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Readable;)J
    .registers 6

    .prologue
    .line 149
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 153
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/ag;->a()Ljava/io/Writer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Writer;

    .line 154
    invoke-static {p1, v0}, Lcom/a/b/i/an;->a(Ljava/lang/Readable;Ljava/lang/Appendable;)J

    move-result-wide v2

    .line 155
    invoke-virtual {v0}, Ljava/io/Writer;->flush()V
    :try_end_18
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_18} :catch_1c
    .catchall {:try_start_7 .. :try_end_18} :catchall_22

    .line 160
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-wide v2

    .line 157
    :catch_1c
    move-exception v0

    .line 158
    :try_start_1d
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_22

    .line 160
    :catchall_22
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method private a(Ljava/lang/Iterable;)V
    .registers 7

    .prologue
    .line 113
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1124
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1125
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1127
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v3

    .line 2079
    :try_start_10
    invoke-virtual {p0}, Lcom/a/b/i/ag;->a()Ljava/io/Writer;

    move-result-object v0

    .line 2080
    instance-of v1, v0, Ljava/io/BufferedWriter;

    if-eqz v1, :cond_43

    check-cast v0, Ljava/io/BufferedWriter;

    .line 1129
    :goto_1a
    invoke-virtual {v3, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Writer;

    .line 1130
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_24
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 1131
    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_37} :catch_38
    .catchall {:try_start_10 .. :try_end_37} :catchall_3e

    goto :goto_24

    .line 1134
    :catch_38
    move-exception v0

    .line 1135
    :try_start_39
    invoke-virtual {v3, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_3e
    .catchall {:try_start_39 .. :try_end_3e} :catchall_3e

    .line 1137
    :catchall_3e
    move-exception v0

    invoke-virtual {v3}, Lcom/a/b/i/ar;->close()V

    throw v0

    .line 2080
    :cond_43
    :try_start_43
    new-instance v1, Ljava/io/BufferedWriter;

    invoke-direct {v1, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object v0, v1

    goto :goto_1a

    .line 1133
    :cond_4a
    invoke-virtual {v0}, Ljava/io/Writer;->flush()V
    :try_end_4d
    .catch Ljava/lang/Throwable; {:try_start_43 .. :try_end_4d} :catch_38
    .catchall {:try_start_43 .. :try_end_4d} :catchall_3e

    .line 1137
    invoke-virtual {v3}, Lcom/a/b/i/ar;->close()V

    .line 1138
    return-void
.end method

.method private a(Ljava/lang/Iterable;Ljava/lang/String;)V
    .registers 7

    .prologue
    .line 124
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v2

    .line 3079
    :try_start_a
    invoke-virtual {p0}, Lcom/a/b/i/ag;->a()Ljava/io/Writer;

    move-result-object v0

    .line 3080
    instance-of v1, v0, Ljava/io/BufferedWriter;

    if-eqz v1, :cond_3d

    check-cast v0, Ljava/io/BufferedWriter;

    .line 129
    :goto_14
    invoke-virtual {v2, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Writer;

    .line 130
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_44

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 131
    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    :try_end_31
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_31} :catch_32
    .catchall {:try_start_a .. :try_end_31} :catchall_38

    goto :goto_1e

    .line 134
    :catch_32
    move-exception v0

    .line 135
    :try_start_33
    invoke-virtual {v2, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_38
    .catchall {:try_start_33 .. :try_end_38} :catchall_38

    .line 137
    :catchall_38
    move-exception v0

    invoke-virtual {v2}, Lcom/a/b/i/ar;->close()V

    throw v0

    .line 3080
    :cond_3d
    :try_start_3d
    new-instance v1, Ljava/io/BufferedWriter;

    invoke-direct {v1, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object v0, v1

    goto :goto_14

    .line 133
    :cond_44
    invoke-virtual {v0}, Ljava/io/Writer;->flush()V
    :try_end_47
    .catch Ljava/lang/Throwable; {:try_start_3d .. :try_end_47} :catch_32
    .catchall {:try_start_3d .. :try_end_47} :catchall_38

    .line 137
    invoke-virtual {v2}, Lcom/a/b/i/ar;->close()V

    .line 138
    return-void
.end method

.method private b()Ljava/io/Writer;
    .registers 3

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/a/b/i/ag;->a()Ljava/io/Writer;

    move-result-object v0

    .line 80
    instance-of v1, v0, Ljava/io/BufferedWriter;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/io/BufferedWriter;

    :goto_a
    return-object v0

    :cond_b
    new-instance v1, Ljava/io/BufferedWriter;

    invoke-direct {v1, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object v0, v1

    goto :goto_a
.end method


# virtual methods
.method public abstract a()Ljava/io/Writer;
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 91
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 95
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/ag;->a()Ljava/io/Writer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Writer;

    .line 96
    invoke-virtual {v0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 97
    invoke-virtual {v0}, Ljava/io/Writer;->flush()V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_17} :catch_1b
    .catchall {:try_start_7 .. :try_end_17} :catchall_21

    .line 101
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    .line 102
    return-void

    .line 98
    :catch_1b
    move-exception v0

    .line 99
    :try_start_1c
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_21

    .line 101
    :catchall_21
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method
