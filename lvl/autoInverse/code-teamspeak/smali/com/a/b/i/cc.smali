.class public final Lcom/a/b/i/cc;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"

# interfaces
.implements Ljava/io/DataOutput;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method private constructor <init>(Ljava/io/OutputStream;)V
    .registers 4

    .prologue
    .line 52
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, v1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 53
    return-void
.end method


# virtual methods
.method public final close()V
    .registers 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 170
    return-void
.end method

.method public final write([BII)V
    .registers 5

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 58
    return-void
.end method

.method public final writeBoolean(Z)V
    .registers 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    check-cast v0, Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 62
    return-void
.end method

.method public final writeByte(I)V
    .registers 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    check-cast v0, Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 66
    return-void
.end method

.method public final writeBytes(Ljava/lang/String;)V
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    check-cast v0, Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public final writeChar(I)V
    .registers 2

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/a/b/i/cc;->writeShort(I)V

    .line 86
    return-void
.end method

.method public final writeChars(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 96
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_11

    .line 97
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lcom/a/b/i/cc;->writeChar(I)V

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_11
    return-void
.end method

.method public final writeDouble(D)V
    .registers 6

    .prologue
    .line 109
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/a/b/i/cc;->writeLong(J)V

    .line 110
    return-void
.end method

.method public final writeFloat(F)V
    .registers 3

    .prologue
    .line 120
    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/i/cc;->writeInt(I)V

    .line 121
    return-void
.end method

.method public final writeInt(I)V
    .registers 4

    .prologue
    .line 131
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    and-int/lit16 v1, p1, 0xff

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 132
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 133
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 134
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 135
    return-void
.end method

.method public final writeLong(J)V
    .registers 10

    .prologue
    const/16 v6, 0x8

    .line 145
    invoke-static {p1, p2}, Ljava/lang/Long;->reverseBytes(J)J

    move-result-wide v0

    .line 1268
    new-array v3, v6, [B

    .line 1269
    const/4 v2, 0x7

    :goto_9
    if-ltz v2, :cond_16

    .line 1270
    const-wide/16 v4, 0xff

    and-long/2addr v4, v0

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v2

    .line 1271
    shr-long/2addr v0, v6

    .line 1269
    add-int/lit8 v2, v2, -0x1

    goto :goto_9

    .line 146
    :cond_16
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0, v6}, Lcom/a/b/i/cc;->write([BII)V

    .line 147
    return-void
.end method

.method public final writeShort(I)V
    .registers 4

    .prologue
    .line 157
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    and-int/lit16 v1, p1, 0xff

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 158
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 159
    return-void
.end method

.method public final writeUTF(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/a/b/i/cc;->out:Ljava/io/OutputStream;

    check-cast v0, Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 163
    return-void
.end method
