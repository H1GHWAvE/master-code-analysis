.class final Lcom/a/b/i/ao;
.super Ljava/io/Reader;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/Readable;


# direct methods
.method constructor <init>(Ljava/lang/Readable;)V
    .registers 2

    .prologue
    .line 270
    iput-object p1, p0, Lcom/a/b/i/ao;->a:Ljava/lang/Readable;

    invoke-direct {p0}, Ljava/io/Reader;-><init>()V

    return-void
.end method


# virtual methods
.method public final close()V
    .registers 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/a/b/i/ao;->a:Ljava/lang/Readable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_d

    .line 284
    iget-object v0, p0, Lcom/a/b/i/ao;->a:Ljava/lang/Readable;

    check-cast v0, Ljava/io/Closeable;

    invoke-interface {v0}, Ljava/io/Closeable;->close()V

    .line 286
    :cond_d
    return-void
.end method

.method public final read(Ljava/nio/CharBuffer;)I
    .registers 3

    .prologue
    .line 278
    iget-object v0, p0, Lcom/a/b/i/ao;->a:Ljava/lang/Readable;

    invoke-interface {v0, p1}, Ljava/lang/Readable;->read(Ljava/nio/CharBuffer;)I

    move-result v0

    return v0
.end method

.method public final read([CII)I
    .registers 5

    .prologue
    .line 273
    invoke-static {p1, p2, p3}, Ljava/nio/CharBuffer;->wrap([CII)Ljava/nio/CharBuffer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/i/ao;->read(Ljava/nio/CharBuffer;)I

    move-result v0

    return v0
.end method
