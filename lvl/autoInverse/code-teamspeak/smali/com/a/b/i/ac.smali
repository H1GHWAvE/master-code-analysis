.class final Lcom/a/b/i/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/i/n;


# instance fields
.field final a:Ljava/io/DataOutput;

.field final b:Ljava/io/ByteArrayOutputStream;


# direct methods
.method constructor <init>(Ljava/io/ByteArrayOutputStream;)V
    .registers 3

    .prologue
    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    iput-object p1, p0, Lcom/a/b/i/ac;->b:Ljava/io/ByteArrayOutputStream;

    .line 381
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    .line 382
    return-void
.end method


# virtual methods
.method public final a()[B
    .registers 2

    .prologue
    .line 497
    iget-object v0, p0, Lcom/a/b/i/ac;->b:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public final write(I)V
    .registers 4

    .prologue
    .line 386
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->write(I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 389
    return-void

    .line 387
    :catch_6
    move-exception v0

    .line 388
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final write([B)V
    .registers 4

    .prologue
    .line 394
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->write([B)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 397
    return-void

    .line 395
    :catch_6
    move-exception v0

    .line 396
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final write([BII)V
    .registers 6

    .prologue
    .line 402
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1, p2, p3}, Ljava/io/DataOutput;->write([BII)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 405
    return-void

    .line 403
    :catch_6
    move-exception v0

    .line 404
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeBoolean(Z)V
    .registers 4

    .prologue
    .line 410
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeBoolean(Z)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 413
    return-void

    .line 411
    :catch_6
    move-exception v0

    .line 412
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeByte(I)V
    .registers 4

    .prologue
    .line 418
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeByte(I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 421
    return-void

    .line 419
    :catch_6
    move-exception v0

    .line 420
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeBytes(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 426
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeBytes(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 429
    return-void

    .line 427
    :catch_6
    move-exception v0

    .line 428
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeChar(I)V
    .registers 4

    .prologue
    .line 434
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeChar(I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 437
    return-void

    .line 435
    :catch_6
    move-exception v0

    .line 436
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeChars(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 442
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeChars(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 445
    return-void

    .line 443
    :catch_6
    move-exception v0

    .line 444
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeDouble(D)V
    .registers 6

    .prologue
    .line 450
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1, p2}, Ljava/io/DataOutput;->writeDouble(D)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 453
    return-void

    .line 451
    :catch_6
    move-exception v0

    .line 452
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeFloat(F)V
    .registers 4

    .prologue
    .line 458
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeFloat(F)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 461
    return-void

    .line 459
    :catch_6
    move-exception v0

    .line 460
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeInt(I)V
    .registers 4

    .prologue
    .line 466
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeInt(I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 469
    return-void

    .line 467
    :catch_6
    move-exception v0

    .line 468
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeLong(J)V
    .registers 6

    .prologue
    .line 474
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1, p2}, Ljava/io/DataOutput;->writeLong(J)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 477
    return-void

    .line 475
    :catch_6
    move-exception v0

    .line 476
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeShort(I)V
    .registers 4

    .prologue
    .line 482
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeShort(I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 485
    return-void

    .line 483
    :catch_6
    move-exception v0

    .line 484
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final writeUTF(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 490
    :try_start_0
    iget-object v0, p0, Lcom/a/b/i/ac;->a:Ljava/io/DataOutput;

    invoke-interface {v0, p1}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 493
    return-void

    .line 491
    :catch_6
    move-exception v0

    .line 492
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method
