.class Lcom/a/b/i/ai;
.super Lcom/a/b/i/ah;
.source "SourceFile"


# static fields
.field private static final a:Lcom/a/b/b/di;


# instance fields
.field private final b:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 327
    const-string v0, "\r\n|\n|\r"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/di;->a(Ljava/util/regex/Pattern;)Lcom/a/b/b/di;

    move-result-object v0

    sput-object v0, Lcom/a/b/i/ai;->a:Lcom/a/b/b/di;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 332
    invoke-direct {p0}, Lcom/a/b/i/ah;-><init>()V

    .line 333
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/a/b/i/ai;->b:Ljava/lang/CharSequence;

    .line 334
    return-void
.end method

.method static synthetic a(Lcom/a/b/i/ai;)Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/a/b/i/ai;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic f()Lcom/a/b/b/di;
    .registers 1

    .prologue
    .line 325
    sget-object v0, Lcom/a/b/i/ai;->a:Lcom/a/b/b/di;

    return-object v0
.end method

.method private g()Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 357
    new-instance v0, Lcom/a/b/i/aj;

    invoke-direct {v0, p0}, Lcom/a/b/i/aj;-><init>(Lcom/a/b/i/ai;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/io/Reader;
    .registers 3

    .prologue
    .line 338
    new-instance v0, Lcom/a/b/i/af;

    iget-object v1, p0, Lcom/a/b/i/ai;->b:Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/a/b/i/af;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public final a(Lcom/a/b/i/by;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 392
    invoke-direct {p0}, Lcom/a/b/i/ai;->g()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 393
    invoke-interface {p1, v0}, Lcom/a/b/i/by;->a(Ljava/lang/String;)Z

    goto :goto_8

    .line 397
    :cond_18
    invoke-interface {p1}, Lcom/a/b/i/by;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/a/b/i/ai;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/a/b/i/ai;->g()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 382
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_14
    return-object v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public final d()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/a/b/i/ai;->g()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/a/b/i/ai;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 402
    iget-object v0, p0, Lcom/a/b/i/ai;->b:Ljava/lang/CharSequence;

    const-string v1, "..."

    invoke-static {v0, v1}, Lcom/a/b/b/e;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CharSource.wrap("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
