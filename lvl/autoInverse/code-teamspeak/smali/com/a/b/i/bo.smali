.class final Lcom/a/b/i/bo;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/i/bs;


# direct methods
.method constructor <init>(Lcom/a/b/i/bs;)V
    .registers 2

    .prologue
    .line 105
    iput-object p1, p0, Lcom/a/b/i/bo;->a:Lcom/a/b/i/bs;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method


# virtual methods
.method public final close()V
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/a/b/i/bo;->a:Lcom/a/b/i/bs;

    invoke-interface {v0}, Lcom/a/b/i/bs;->b()V

    .line 136
    return-void
.end method

.method public final read()I
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/a/b/i/bo;->a:Lcom/a/b/i/bs;

    invoke-interface {v0}, Lcom/a/b/i/bs;->a()I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .registers 8

    .prologue
    const/4 v0, -0x1

    .line 113
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    add-int v1, p2, p3

    array-length v2, p1

    invoke-static {p2, v1, v2}, Lcom/a/b/b/cn;->a(III)V

    .line 115
    if-nez p3, :cond_e

    .line 116
    const/4 p3, 0x0

    .line 130
    :cond_d
    :goto_d
    return p3

    .line 118
    :cond_e
    invoke-virtual {p0}, Lcom/a/b/i/bo;->read()I

    move-result v1

    .line 119
    if-ne v1, v0, :cond_16

    move p3, v0

    .line 120
    goto :goto_d

    .line 122
    :cond_16
    int-to-byte v1, v1

    aput-byte v1, p1, p2

    .line 123
    const/4 v1, 0x1

    :goto_1a
    if-ge v1, p3, :cond_d

    .line 124
    invoke-virtual {p0}, Lcom/a/b/i/bo;->read()I

    move-result v2

    .line 125
    if-ne v2, v0, :cond_24

    move p3, v1

    .line 126
    goto :goto_d

    .line 128
    :cond_24
    add-int v3, p2, v1

    int-to-byte v2, v2

    aput-byte v2, p1, v3

    .line 123
    add-int/lit8 v1, v1, 0x1

    goto :goto_1a
.end method
