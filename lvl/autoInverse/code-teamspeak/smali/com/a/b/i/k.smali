.class final Lcom/a/b/i/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/i/bt;


# instance fields
.field a:I

.field b:I

.field c:I

.field final synthetic d:Lcom/a/b/i/bv;

.field final synthetic e:Lcom/a/b/i/j;


# direct methods
.method constructor <init>(Lcom/a/b/i/j;Lcom/a/b/i/bv;)V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 594
    iput-object p1, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    iput-object p2, p0, Lcom/a/b/i/k;->d:Lcom/a/b/i/bv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 595
    iput v0, p0, Lcom/a/b/i/k;->a:I

    .line 596
    iput v0, p0, Lcom/a/b/i/k;->b:I

    .line 597
    iput v0, p0, Lcom/a/b/i/k;->c:I

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/a/b/i/k;->d:Lcom/a/b/i/bv;

    invoke-interface {v0}, Lcom/a/b/i/bv;->a()V

    .line 616
    return-void
.end method

.method public final a(B)V
    .registers 5

    .prologue
    .line 601
    iget v0, p0, Lcom/a/b/i/k;->a:I

    shl-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/a/b/i/k;->a:I

    .line 602
    iget v0, p0, Lcom/a/b/i/k;->a:I

    and-int/lit16 v1, p1, 0xff

    or-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/i/k;->a:I

    .line 603
    iget v0, p0, Lcom/a/b/i/k;->b:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/a/b/i/k;->b:I

    .line 604
    :goto_13
    iget v0, p0, Lcom/a/b/i/k;->b:I

    iget-object v1, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v1

    iget v1, v1, Lcom/a/b/i/g;->v:I

    if-lt v0, v1, :cond_59

    .line 605
    iget v0, p0, Lcom/a/b/i/k;->a:I

    iget v1, p0, Lcom/a/b/i/k;->b:I

    iget-object v2, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v2}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v2

    iget v2, v2, Lcom/a/b/i/g;->v:I

    sub-int/2addr v1, v2

    shr-int/2addr v0, v1

    iget-object v1, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v1

    iget v1, v1, Lcom/a/b/i/g;->u:I

    and-int/2addr v0, v1

    .line 607
    iget-object v1, p0, Lcom/a/b/i/k;->d:Lcom/a/b/i/bv;

    iget-object v2, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v2}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v2

    .line 1494
    iget-object v2, v2, Lcom/a/b/i/g;->t:[C

    aget-char v0, v2, v0

    .line 607
    invoke-interface {v1, v0}, Lcom/a/b/i/bv;->a(C)V

    .line 608
    iget v0, p0, Lcom/a/b/i/k;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/i/k;->c:I

    .line 609
    iget v0, p0, Lcom/a/b/i/k;->b:I

    iget-object v1, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v1

    iget v1, v1, Lcom/a/b/i/g;->v:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/i/k;->b:I

    goto :goto_13

    .line 611
    :cond_59
    return-void
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 620
    iget v0, p0, Lcom/a/b/i/k;->b:I

    if-lez v0, :cond_5b

    .line 621
    iget v0, p0, Lcom/a/b/i/k;->a:I

    iget-object v1, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v1

    iget v1, v1, Lcom/a/b/i/g;->v:I

    iget v2, p0, Lcom/a/b/i/k;->b:I

    sub-int/2addr v1, v2

    shl-int/2addr v0, v1

    iget-object v1, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v1

    iget v1, v1, Lcom/a/b/i/g;->u:I

    and-int/2addr v0, v1

    .line 623
    iget-object v1, p0, Lcom/a/b/i/k;->d:Lcom/a/b/i/bv;

    iget-object v2, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v2}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v2

    .line 2494
    iget-object v2, v2, Lcom/a/b/i/g;->t:[C

    aget-char v0, v2, v0

    .line 623
    invoke-interface {v1, v0}, Lcom/a/b/i/bv;->a(C)V

    .line 624
    iget v0, p0, Lcom/a/b/i/k;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/i/k;->c:I

    .line 625
    iget-object v0, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v0}, Lcom/a/b/i/j;->b(Lcom/a/b/i/j;)Ljava/lang/Character;

    move-result-object v0

    if-eqz v0, :cond_5b

    .line 626
    :goto_38
    iget v0, p0, Lcom/a/b/i/k;->c:I

    iget-object v1, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v1

    iget v1, v1, Lcom/a/b/i/g;->w:I

    rem-int/2addr v0, v1

    if-eqz v0, :cond_5b

    .line 627
    iget-object v0, p0, Lcom/a/b/i/k;->d:Lcom/a/b/i/bv;

    iget-object v1, p0, Lcom/a/b/i/k;->e:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->b(Lcom/a/b/i/j;)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-interface {v0, v1}, Lcom/a/b/i/bv;->a(C)V

    .line 628
    iget v0, p0, Lcom/a/b/i/k;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/i/k;->c:I

    goto :goto_38

    .line 632
    :cond_5b
    iget-object v0, p0, Lcom/a/b/i/k;->d:Lcom/a/b/i/bv;

    invoke-interface {v0}, Lcom/a/b/i/bv;->b()V

    .line 633
    return-void
.end method
