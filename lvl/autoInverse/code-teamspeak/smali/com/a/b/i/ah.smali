.class public abstract Lcom/a/b/i/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/a/b/i/ag;)J
    .registers 5

    .prologue
    .line 125
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v2

    .line 129
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/ah;->a()Ljava/io/Reader;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Reader;

    .line 130
    invoke-virtual {p1}, Lcom/a/b/i/ag;->a()Ljava/io/Writer;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v1

    check-cast v1, Ljava/io/Writer;

    .line 131
    invoke-static {v0, v1}, Lcom/a/b/i/an;->a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_1e} :catch_23
    .catchall {:try_start_7 .. :try_end_1e} :catchall_29

    move-result-wide v0

    .line 135
    invoke-virtual {v2}, Lcom/a/b/i/ar;->close()V

    return-wide v0

    .line 132
    :catch_23
    move-exception v0

    .line 133
    :try_start_24
    invoke-virtual {v2, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_29
    .catchall {:try_start_24 .. :try_end_29} :catchall_29

    .line 135
    :catchall_29
    move-exception v0

    invoke-virtual {v2}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method private static a(Ljava/lang/CharSequence;)Lcom/a/b/i/ah;
    .registers 2

    .prologue
    .line 313
    new-instance v0, Lcom/a/b/i/ai;

    invoke-direct {v0, p0}, Lcom/a/b/i/ai;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/i/ah;
    .registers 2

    .prologue
    .line 264
    new-instance v0, Lcom/a/b/i/al;

    invoke-direct {v0, p0}, Lcom/a/b/i/al;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/i/ah;
    .registers 2

    .prologue
    .line 286
    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/util/Iterator;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/i/ah;->a(Ljava/lang/Iterable;)Lcom/a/b/i/ah;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a([Lcom/a/b/i/ah;)Lcom/a/b/i/ah;
    .registers 2

    .prologue
    .line 302
    invoke-static {p0}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/i/ah;->a(Ljava/lang/Iterable;)Lcom/a/b/i/ah;

    move-result-object v0

    return-object v0
.end method

.method private f()Ljava/io/BufferedReader;
    .registers 3

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/a/b/i/ah;->a()Ljava/io/Reader;

    move-result-object v0

    .line 92
    instance-of v1, v0, Ljava/io/BufferedReader;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/io/BufferedReader;

    :goto_a
    return-object v0

    :cond_b
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v0, v1

    goto :goto_a
.end method

.method private static g()Lcom/a/b/i/ah;
    .registers 1

    .prologue
    .line 322
    invoke-static {}, Lcom/a/b/i/am;->g()Lcom/a/b/i/am;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Appendable;)J
    .registers 6

    .prologue
    .line 105
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 109
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/ah;->a()Ljava/io/Reader;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Reader;

    .line 110
    invoke-static {v0, p1}, Lcom/a/b/i/an;->a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_14} :catch_19
    .catchall {:try_start_7 .. :try_end_14} :catchall_1f

    move-result-wide v2

    .line 114
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-wide v2

    .line 111
    :catch_19
    move-exception v0

    .line 112
    :try_start_1a
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1f
    .catchall {:try_start_1a .. :try_end_1f} :catchall_1f

    .line 114
    :catchall_1f
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public abstract a()Ljava/io/Reader;
.end method

.method public a(Lcom/a/b/i/by;)Ljava/lang/Object;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 220
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 224
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/ah;->a()Ljava/io/Reader;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Reader;

    .line 2138
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2139
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2141
    new-instance v2, Lcom/a/b/i/bz;

    invoke-direct {v2, v0}, Lcom/a/b/i/bz;-><init>(Ljava/lang/Readable;)V

    .line 2143
    :goto_1c
    invoke-virtual {v2}, Lcom/a/b/i/bz;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 2144
    invoke-interface {p1, v0}, Lcom/a/b/i/by;->a(Ljava/lang/String;)Z
    :try_end_25
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_25} :catch_26
    .catchall {:try_start_7 .. :try_end_25} :catchall_2c

    goto :goto_1c

    .line 226
    :catch_26
    move-exception v0

    .line 227
    :try_start_27
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_2c

    .line 229
    :catchall_2c
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0

    .line 2148
    :cond_31
    :try_start_31
    invoke-interface {p1}, Lcom/a/b/i/by;->a()Ljava/lang/Object;
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_31 .. :try_end_34} :catch_26
    .catchall {:try_start_31 .. :try_end_34} :catchall_2c

    move-result-object v0

    .line 229
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .registers 4

    .prologue
    .line 145
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 147
    :try_start_4
    invoke-virtual {p0}, Lcom/a/b/i/ah;->a()Ljava/io/Reader;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Reader;

    .line 1098
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1099
    invoke-static {v0, v2}, Lcom/a/b/i/an;->a(Ljava/lang/Readable;Ljava/lang/Appendable;)J

    .line 1086
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_19} :catch_1e
    .catchall {:try_start_4 .. :try_end_19} :catchall_24

    move-result-object v0

    .line 152
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-object v0

    .line 149
    :catch_1e
    move-exception v0

    .line 150
    :try_start_1f
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_24
    .catchall {:try_start_1f .. :try_end_24} :catchall_24

    .line 152
    :catchall_24
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public c()Ljava/lang/String;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 166
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 168
    :try_start_4
    invoke-direct {p0}, Lcom/a/b/i/ah;->f()Ljava/io/BufferedReader;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/BufferedReader;

    .line 169
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_11} :catch_16
    .catchall {:try_start_4 .. :try_end_11} :catchall_1c

    move-result-object v0

    .line 173
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-object v0

    .line 170
    :catch_16
    move-exception v0

    .line 171
    :try_start_17
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1c
    .catchall {:try_start_17 .. :try_end_1c} :catchall_1c

    .line 173
    :catchall_1c
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public d()Lcom/a/b/d/jl;
    .registers 5

    .prologue
    .line 188
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 190
    :try_start_4
    invoke-direct {p0}, Lcom/a/b/i/ah;->f()Ljava/io/BufferedReader;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/BufferedReader;

    .line 2088
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 193
    :goto_13
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_28

    .line 194
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1c
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_1c} :catch_1d
    .catchall {:try_start_4 .. :try_end_1c} :catchall_23

    goto :goto_13

    .line 197
    :catch_1d
    move-exception v0

    .line 198
    :try_start_1e
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_23
    .catchall {:try_start_1e .. :try_end_23} :catchall_23

    .line 200
    :catchall_23
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0

    .line 196
    :cond_28
    :try_start_28
    invoke-static {v2}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_2b} :catch_1d
    .catchall {:try_start_28 .. :try_end_2b} :catchall_23

    move-result-object v0

    .line 200
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-object v0
.end method

.method public e()Z
    .registers 4

    .prologue
    .line 241
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 243
    :try_start_4
    invoke-virtual {p0}, Lcom/a/b/i/ah;->a()Ljava/io/Reader;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/Reader;

    .line 244
    invoke-virtual {v0}, Ljava/io/Reader;->read()I
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_11} :catch_1c
    .catchall {:try_start_4 .. :try_end_11} :catchall_22

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1a

    const/4 v0, 0x1

    .line 248
    :goto_16
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return v0

    .line 244
    :cond_1a
    const/4 v0, 0x0

    goto :goto_16

    .line 245
    :catch_1c
    move-exception v0

    .line 246
    :try_start_1d
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_22

    .line 248
    :catchall_22
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method
