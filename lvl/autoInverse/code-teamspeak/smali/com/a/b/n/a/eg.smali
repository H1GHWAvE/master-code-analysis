.class final Lcom/a/b/n/a/eg;
.super Lcom/a/b/n/a/o;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/locks/Lock;

.field private final b:Ljava/util/concurrent/locks/Condition;

.field private c:I

.field private d:Z


# direct methods
.method private constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 274
    invoke-direct {p0}, Lcom/a/b/n/a/o;-><init>()V

    .line 280
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    .line 283
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/eg;->b:Ljava/util/concurrent/locks/Condition;

    .line 292
    iput v1, p0, Lcom/a/b/n/a/eg;->c:I

    .line 293
    iput-boolean v1, p0, Lcom/a/b/n/a/eg;->d:Z

    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/a/b/n/a/eg;-><init>()V

    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 370
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 372
    :try_start_5
    invoke-virtual {p0}, Lcom/a/b/n/a/eg;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 373
    new-instance v0, Ljava/util/concurrent/RejectedExecutionException;

    const-string v1, "Executor already shutdown"

    invoke-direct {v0, v1}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_13
    .catchall {:try_start_5 .. :try_end_13} :catchall_13

    .line 377
    :catchall_13
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 375
    :cond_1a
    :try_start_1a
    iget v0, p0, Lcom/a/b/n/a/eg;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/n/a/eg;->c:I
    :try_end_20
    .catchall {:try_start_1a .. :try_end_20} :catchall_13

    .line 377
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 378
    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    .line 385
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 387
    :try_start_5
    iget v0, p0, Lcom/a/b/n/a/eg;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/n/a/eg;->c:I

    .line 388
    invoke-virtual {p0}, Lcom/a/b/n/a/eg;->isTerminated()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 389
    iget-object v0, p0, Lcom/a/b/n/a/eg;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_16
    .catchall {:try_start_5 .. :try_end_16} :catchall_1c

    .line 392
    :cond_16
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 393
    return-void

    .line 392
    :catchall_1c
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .registers 9

    .prologue
    .line 345
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 346
    iget-object v2, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 349
    :goto_9
    :try_start_9
    invoke-virtual {p0}, Lcom/a/b/n/a/eg;->isTerminated()Z
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_2a

    move-result v2

    if-eqz v2, :cond_16

    .line 358
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x1

    :goto_15
    return v0

    .line 351
    :cond_16
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_23

    .line 358
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x0

    goto :goto_15

    .line 354
    :cond_23
    :try_start_23
    iget-object v2, p0, Lcom/a/b/n/a/eg;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_28
    .catchall {:try_start_23 .. :try_end_28} :catchall_2a

    move-result-wide v0

    goto :goto_9

    .line 358
    :catchall_2a
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .registers 4

    .prologue
    .line 297
    .line 1370
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1372
    :try_start_5
    invoke-virtual {p0}, Lcom/a/b/n/a/eg;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1373
    new-instance v0, Ljava/util/concurrent/RejectedExecutionException;

    const-string v1, "Executor already shutdown"

    invoke-direct {v0, v1}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_13
    .catchall {:try_start_5 .. :try_end_13} :catchall_13

    .line 1377
    :catchall_13
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 1375
    :cond_1a
    :try_start_1a
    iget v0, p0, Lcom/a/b/n/a/eg;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/n/a/eg;->c:I
    :try_end_20
    .catchall {:try_start_1a .. :try_end_20} :catchall_13

    .line 1377
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 299
    :try_start_25
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_28
    .catchall {:try_start_25 .. :try_end_28} :catchall_2c

    .line 301
    invoke-direct {p0}, Lcom/a/b/n/a/eg;->b()V

    .line 302
    return-void

    .line 301
    :catchall_2c
    move-exception v0

    invoke-direct {p0}, Lcom/a/b/n/a/eg;->b()V

    throw v0
.end method

.method public final isShutdown()Z
    .registers 3

    .prologue
    .line 307
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 309
    :try_start_5
    iget-boolean v0, p0, Lcom/a/b/n/a/eg;->d:Z
    :try_end_7
    .catchall {:try_start_5 .. :try_end_7} :catchall_d

    .line 311
    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_d
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final isTerminated()Z
    .registers 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 336
    :try_start_5
    iget-boolean v0, p0, Lcom/a/b/n/a/eg;->d:Z

    if-eqz v0, :cond_14

    iget v0, p0, Lcom/a/b/n/a/eg;->c:I
    :try_end_b
    .catchall {:try_start_5 .. :try_end_b} :catchall_16

    if-nez v0, :cond_14

    const/4 v0, 0x1

    .line 338
    :goto_e
    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    .line 336
    :cond_14
    const/4 v0, 0x0

    goto :goto_e

    .line 338
    :catchall_16
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final shutdown()V
    .registers 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 319
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, p0, Lcom/a/b/n/a/eg;->d:Z
    :try_end_8
    .catchall {:try_start_6 .. :try_end_8} :catchall_e

    .line 321
    iget-object v0, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 322
    return-void

    .line 321
    :catchall_e
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/eg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final shutdownNow()Ljava/util/List;
    .registers 2

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/a/b/n/a/eg;->shutdown()V

    .line 329
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
