.class final Lcom/a/b/n/a/gf;
.super Lcom/a/b/n/a/gi;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/ConcurrentMap;

.field final b:Lcom/a/b/b/dz;

.field final c:I


# direct methods
.method constructor <init>(ILcom/a/b/b/dz;)V
    .registers 5

    .prologue
    .line 416
    invoke-direct {p0, p1}, Lcom/a/b/n/a/gi;-><init>(I)V

    .line 417
    iget v0, p0, Lcom/a/b/n/a/gf;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_21

    const v0, 0x7fffffff

    :goto_b
    iput v0, p0, Lcom/a/b/n/a/gf;->c:I

    .line 418
    iput-object p2, p0, Lcom/a/b/n/a/gf;->b:Lcom/a/b/b/dz;

    .line 419
    new-instance v0, Lcom/a/b/d/ql;

    invoke-direct {v0}, Lcom/a/b/d/ql;-><init>()V

    .line 1303
    sget-object v1, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {v0, v1}, Lcom/a/b/d/ql;->b(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 419
    invoke-virtual {v0}, Lcom/a/b/d/ql;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/gf;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 420
    return-void

    .line 417
    :cond_21
    iget v0, p0, Lcom/a/b/n/a/gf;->d:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_b
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 436
    iget v0, p0, Lcom/a/b/n/a/gf;->c:I

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 423
    iget v0, p0, Lcom/a/b/n/a/gf;->c:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_c

    .line 1436
    iget v0, p0, Lcom/a/b/n/a/gf;->c:I

    .line 424
    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 426
    :cond_c
    iget-object v0, p0, Lcom/a/b/n/a/gf;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 427
    if-eqz v0, :cond_19

    .line 432
    :goto_18
    return-object v0

    .line 430
    :cond_19
    iget-object v0, p0, Lcom/a/b/n/a/gf;->b:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    .line 431
    iget-object v1, p0, Lcom/a/b/n/a/gf;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 432
    invoke-static {v1, v0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_18
.end method
