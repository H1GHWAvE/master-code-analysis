.class final Lcom/a/b/n/a/fw;
.super Lcom/a/b/n/a/fu;
.source "SourceFile"


# instance fields
.field final d:D


# direct methods
.method constructor <init>(Lcom/a/b/n/a/em;)V
    .registers 4

    .prologue
    .line 285
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/a/b/n/a/fu;-><init>(Lcom/a/b/n/a/em;B)V

    .line 286
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/a/b/n/a/fw;->d:D

    .line 287
    return-void
.end method


# virtual methods
.method final a(DD)V
    .registers 12

    .prologue
    const-wide/16 v0, 0x0

    .line 291
    iget-wide v2, p0, Lcom/a/b/n/a/fw;->b:D

    .line 292
    iget-wide v4, p0, Lcom/a/b/n/a/fw;->d:D

    mul-double/2addr v4, p1

    iput-wide v4, p0, Lcom/a/b/n/a/fw;->b:D

    .line 293
    const-wide/high16 v4, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    cmpl-double v4, v2, v4

    if-nez v4, :cond_14

    .line 295
    iget-wide v0, p0, Lcom/a/b/n/a/fw;->b:D

    .line 297
    :cond_11
    :goto_11
    iput-wide v0, p0, Lcom/a/b/n/a/fw;->a:D

    .line 301
    return-void

    .line 297
    :cond_14
    cmpl-double v4, v2, v0

    if-eqz v4, :cond_11

    iget-wide v0, p0, Lcom/a/b/n/a/fw;->a:D

    iget-wide v4, p0, Lcom/a/b/n/a/fw;->b:D

    mul-double/2addr v0, v4

    div-double/2addr v0, v2

    goto :goto_11
.end method

.method final b(DD)J
    .registers 7

    .prologue
    .line 305
    const-wide/16 v0, 0x0

    return-wide v0
.end method
