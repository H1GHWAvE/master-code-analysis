.class public final Lcom/a/b/n/a/br;
.super Lcom/a/b/n/a/bk;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field final c:Lcom/a/b/n/a/bk;


# direct methods
.method private constructor <init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;)V
    .registers 4

    .prologue
    .line 577
    invoke-direct {p0, p1, p2}, Lcom/a/b/n/a/bk;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V

    .line 578
    iput-object p3, p0, Lcom/a/b/n/a/br;->c:Lcom/a/b/n/a/bk;

    .line 579
    invoke-virtual {p0, p3}, Lcom/a/b/n/a/br;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 580
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V
    .registers 5

    .prologue
    .line 568
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/n/a/br;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;)V

    return-void
.end method

.method private a()Lcom/a/b/n/a/bk;
    .registers 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/a/b/n/a/br;->c:Lcom/a/b/n/a/bk;

    return-object v0
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .registers 5

    .prologue
    .line 592
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lcom/a/b/n/a/bk;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 593
    iget-object v0, p0, Lcom/a/b/n/a/br;->c:Lcom/a/b/n/a/bk;

    :goto_b
    if-eqz v0, :cond_1f

    .line 594
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_b

    .line 596
    :cond_1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
