.class Lcom/a/b/n/a/eh;
.super Lcom/a/b/n/a/o;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .registers 3

    .prologue
    .line 518
    invoke-direct {p0}, Lcom/a/b/n/a/o;-><init>()V

    .line 519
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lcom/a/b/n/a/eh;->a:Ljava/util/concurrent/ExecutorService;

    .line 520
    return-void
.end method


# virtual methods
.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .registers 5

    .prologue
    .line 525
    iget-object v0, p0, Lcom/a/b/n/a/eh;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .registers 3

    .prologue
    .line 550
    iget-object v0, p0, Lcom/a/b/n/a/eh;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 551
    return-void
.end method

.method public isShutdown()Z
    .registers 2

    .prologue
    .line 530
    iget-object v0, p0, Lcom/a/b/n/a/eh;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    return v0
.end method

.method public isTerminated()Z
    .registers 2

    .prologue
    .line 535
    iget-object v0, p0, Lcom/a/b/n/a/eh;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isTerminated()Z

    move-result v0

    return v0
.end method

.method public shutdown()V
    .registers 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/a/b/n/a/eh;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 541
    return-void
.end method

.method public shutdownNow()Ljava/util/List;
    .registers 2

    .prologue
    .line 545
    iget-object v0, p0, Lcom/a/b/n/a/eh;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
