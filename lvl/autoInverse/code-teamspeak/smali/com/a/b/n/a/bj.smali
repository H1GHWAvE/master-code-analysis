.class final Lcom/a/b/n/a/bj;
.super Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/n/a/bi;

.field final synthetic b:Lcom/a/b/n/a/bd;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bi;)V
    .registers 3

    .prologue
    .line 979
    iput-object p1, p0, Lcom/a/b/n/a/bj;->b:Lcom/a/b/n/a/bd;

    .line 980
    invoke-direct {p0, p2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;-><init>(Ljava/util/concurrent/locks/ReentrantReadWriteLock;)V

    .line 981
    iput-object p2, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    .line 982
    return-void
.end method


# virtual methods
.method public final lock()V
    .registers 3

    .prologue
    .line 986
    iget-object v0, p0, Lcom/a/b/n/a/bj;->b:Lcom/a/b/n/a/bd;

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0, v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 988
    :try_start_7
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_10

    .line 990
    iget-object v0, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 991
    return-void

    .line 990
    :catchall_10
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final lockInterruptibly()V
    .registers 3

    .prologue
    .line 996
    iget-object v0, p0, Lcom/a/b/n/a/bj;->b:Lcom/a/b/n/a/bd;

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0, v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 998
    :try_start_7
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lockInterruptibly()V
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_10

    .line 1000
    iget-object v0, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 1001
    return-void

    .line 1000
    :catchall_10
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final tryLock()Z
    .registers 3

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/a/b/n/a/bj;->b:Lcom/a/b/n/a/bd;

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0, v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 1008
    :try_start_7
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->tryLock()Z
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_11

    move-result v0

    .line 1010
    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    return v0

    :catchall_11
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final tryLock(JLjava/util/concurrent/TimeUnit;)Z
    .registers 7

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/a/b/n/a/bj;->b:Lcom/a/b/n/a/bd;

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0, v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 1019
    :try_start_7
    invoke-super {p0, p1, p2, p3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_11

    move-result v0

    .line 1021
    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    return v0

    :catchall_11
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final unlock()V
    .registers 3

    .prologue
    .line 1028
    :try_start_0
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_9

    .line 1030
    iget-object v0, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 1031
    return-void

    .line 1030
    :catchall_9
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/bj;->a:Lcom/a/b/n/a/bi;

    invoke-static {v1}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method
