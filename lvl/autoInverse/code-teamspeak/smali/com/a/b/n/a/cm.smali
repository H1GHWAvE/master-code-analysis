.class final Lcom/a/b/n/a/cm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Future;


# instance fields
.field final synthetic a:Ljava/util/concurrent/Future;

.field final synthetic b:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Future;Lcom/a/b/b/bj;)V
    .registers 3

    .prologue
    .line 793
    iput-object p1, p0, Lcom/a/b/n/a/cm;->a:Ljava/util/concurrent/Future;

    iput-object p2, p0, Lcom/a/b/n/a/cm;->b:Lcom/a/b/b/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 823
    :try_start_0
    iget-object v0, p0, Lcom/a/b/n/a/cm;->b:Lcom/a/b/b/bj;

    invoke-interface {v0, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    .line 824
    :catch_7
    move-exception v0

    .line 825
    new-instance v1, Ljava/util/concurrent/ExecutionException;

    invoke-direct {v1, v0}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final cancel(Z)Z
    .registers 3

    .prologue
    .line 797
    iget-object v0, p0, Lcom/a/b/n/a/cm;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 812
    iget-object v0, p0, Lcom/a/b/n/a/cm;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/n/a/cm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 818
    iget-object v0, p0, Lcom/a/b/n/a/cm;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/n/a/cm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isCancelled()Z
    .registers 2

    .prologue
    .line 802
    iget-object v0, p0, Lcom/a/b/n/a/cm;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public final isDone()Z
    .registers 2

    .prologue
    .line 807
    iget-object v0, p0, Lcom/a/b/n/a/cm;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    return v0
.end method
