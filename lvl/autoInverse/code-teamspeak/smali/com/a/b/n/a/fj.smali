.class final Lcom/a/b/n/a/fj;
.super Lcom/a/b/n/a/ev;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/n/a/et;

.field final b:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/et;Ljava/lang/ref/WeakReference;)V
    .registers 3

    .prologue
    .line 729
    invoke-direct {p0}, Lcom/a/b/n/a/ev;-><init>()V

    .line 730
    iput-object p1, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    .line 731
    iput-object p2, p0, Lcom/a/b/n/a/fj;->b:Ljava/lang/ref/WeakReference;

    .line 732
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 5

    .prologue
    .line 735
    iget-object v0, p0, Lcom/a/b/n/a/fj;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/fk;

    .line 736
    if-eqz v0, :cond_26

    .line 737
    iget-object v1, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    sget-object v2, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    sget-object v3, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/n/a/fk;->a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V

    .line 738
    iget-object v0, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    instance-of v0, v0, Lcom/a/b/n/a/fi;

    if-nez v0, :cond_26

    .line 739
    invoke-static {}, Lcom/a/b/n/a/fd;->a()Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "Starting {0}."

    iget-object v3, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    .line 742
    :cond_26
    return-void
.end method

.method public final a(Lcom/a/b/n/a/ew;)V
    .registers 9

    .prologue
    .line 759
    iget-object v0, p0, Lcom/a/b/n/a/fj;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/fk;

    .line 760
    if-eqz v0, :cond_2d

    .line 761
    iget-object v1, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    instance-of v1, v1, Lcom/a/b/n/a/fi;

    if-nez v1, :cond_26

    .line 762
    invoke-static {}, Lcom/a/b/n/a/fd;->a()Ljava/util/logging/Logger;

    move-result-object v1

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v3, "Service {0} has terminated. Previous state was: {1}"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 765
    :cond_26
    iget-object v1, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    sget-object v2, Lcom/a/b/n/a/ew;->e:Lcom/a/b/n/a/ew;

    invoke-virtual {v0, v1, p1, v2}, Lcom/a/b/n/a/fk;->a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V

    .line 767
    :cond_2d
    return-void
.end method

.method public final a(Lcom/a/b/n/a/ew;Ljava/lang/Throwable;)V
    .registers 11

    .prologue
    .line 770
    iget-object v0, p0, Lcom/a/b/n/a/fj;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/fk;

    .line 771
    if-eqz v0, :cond_60

    .line 774
    iget-object v1, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    instance-of v1, v1, Lcom/a/b/n/a/fi;

    if-nez v1, :cond_59

    .line 775
    invoke-static {}, Lcom/a/b/n/a/fd;->a()Ljava/util/logging/Logger;

    move-result-object v1

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    iget-object v3, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x22

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Service "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " has failed in the "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " state."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 778
    :cond_59
    iget-object v1, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    sget-object v2, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    invoke-virtual {v0, v1, p1, v2}, Lcom/a/b/n/a/fk;->a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V

    .line 780
    :cond_60
    return-void
.end method

.method public final b()V
    .registers 5

    .prologue
    .line 745
    iget-object v0, p0, Lcom/a/b/n/a/fj;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/fk;

    .line 746
    if-eqz v0, :cond_13

    .line 747
    iget-object v1, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    sget-object v2, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    sget-object v3, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/n/a/fk;->a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V

    .line 749
    :cond_13
    return-void
.end method

.method public final b(Lcom/a/b/n/a/ew;)V
    .registers 5

    .prologue
    .line 752
    iget-object v0, p0, Lcom/a/b/n/a/fj;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/fk;

    .line 753
    if-eqz v0, :cond_11

    .line 754
    iget-object v1, p0, Lcom/a/b/n/a/fj;->a:Lcom/a/b/n/a/et;

    sget-object v2, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    invoke-virtual {v0, v1, p1, v2}, Lcom/a/b/n/a/fk;->a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V

    .line 756
    :cond_11
    return-void
.end method
