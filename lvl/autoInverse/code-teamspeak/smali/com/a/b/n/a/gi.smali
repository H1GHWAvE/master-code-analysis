.class abstract Lcom/a/b/n/a/gi;
.super Lcom/a/b/n/a/fy;
.source "SourceFile"


# instance fields
.field final d:I


# direct methods
.method constructor <init>(I)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 293
    invoke-direct {p0, v0}, Lcom/a/b/n/a/fy;-><init>(B)V

    .line 294
    if-lez p1, :cond_8

    move v0, v1

    :cond_8
    const-string v2, "Stripes must be positive"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 295
    const/high16 v0, 0x40000000    # 2.0f

    if-le p1, v0, :cond_15

    const/4 v0, -0x1

    :goto_12
    iput v0, p0, Lcom/a/b/n/a/gi;->d:I

    .line 296
    return-void

    .line 1446
    :cond_15
    sget-object v0, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {p1, v0}, Lcom/a/b/j/g;->a(ILjava/math/RoundingMode;)I

    move-result v0

    shl-int v0, v1, v0

    .line 295
    add-int/lit8 v0, v0, -0x1

    goto :goto_12
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 304
    invoke-virtual {p0, p1}, Lcom/a/b/n/a/gi;->b(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/gi;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/Object;)I
    .registers 5

    .prologue
    .line 299
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 2459
    ushr-int/lit8 v1, v0, 0x14

    ushr-int/lit8 v2, v0, 0xc

    xor-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 2460
    ushr-int/lit8 v1, v0, 0x7

    xor-int/2addr v1, v0

    ushr-int/lit8 v0, v0, 0x4

    xor-int/2addr v0, v1

    .line 300
    iget v1, p0, Lcom/a/b/n/a/gi;->d:I

    and-int/2addr v0, v1

    return v0
.end method
