.class public abstract Lcom/a/b/n/a/dx;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field final b:Lcom/a/b/n/a/dw;

.field final c:Ljava/util/concurrent/locks/Condition;

.field d:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor.lock"
    .end annotation
.end field

.field e:Lcom/a/b/n/a/dx;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor.lock"
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/a/b/n/a/dw;)V
    .registers 3

    .prologue
    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/n/a/dx;->d:I

    .line 304
    const-string v0, "monitor"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dw;

    iput-object v0, p0, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    .line 1202
    iget-object v0, p1, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 305
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/dx;->c:Ljava/util/concurrent/locks/Condition;

    .line 306
    return-void
.end method


# virtual methods
.method public abstract a()Z
.end method
