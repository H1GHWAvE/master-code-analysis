.class public final Lcom/a/b/c/l;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final n:Lcom/a/b/b/di;

.field private static final o:Lcom/a/b/b/di;

.field private static final p:Lcom/a/b/d/jt;


# instance fields
.field a:Ljava/lang/Integer;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field b:Ljava/lang/Long;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field c:Ljava/lang/Long;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field d:Ljava/lang/Integer;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field e:Lcom/a/b/c/bw;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field f:Lcom/a/b/c/bw;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field g:Ljava/lang/Boolean;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field h:J
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field i:Ljava/util/concurrent/TimeUnit;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field j:J
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field k:Ljava/util/concurrent/TimeUnit;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field l:J
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field m:Ljava/util/concurrent/TimeUnit;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field private final q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 90
    const/16 v0, 0x2c

    invoke-static {v0}, Lcom/a/b/b/di;->a(C)Lcom/a/b/b/di;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/b/di;->b()Lcom/a/b/b/di;

    move-result-object v0

    sput-object v0, Lcom/a/b/c/l;->n:Lcom/a/b/b/di;

    .line 93
    const/16 v0, 0x3d

    invoke-static {v0}, Lcom/a/b/b/di;->a(C)Lcom/a/b/b/di;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/b/di;->b()Lcom/a/b/b/di;

    move-result-object v0

    sput-object v0, Lcom/a/b/c/l;->o:Lcom/a/b/b/di;

    .line 96
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "initialCapacity"

    new-instance v2, Lcom/a/b/c/q;

    invoke-direct {v2}, Lcom/a/b/c/q;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "maximumSize"

    new-instance v2, Lcom/a/b/c/u;

    invoke-direct {v2}, Lcom/a/b/c/u;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "maximumWeight"

    new-instance v2, Lcom/a/b/c/v;

    invoke-direct {v2}, Lcom/a/b/c/v;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "concurrencyLevel"

    new-instance v2, Lcom/a/b/c/o;

    invoke-direct {v2}, Lcom/a/b/c/o;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "weakKeys"

    new-instance v2, Lcom/a/b/c/s;

    sget-object v3, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-direct {v2, v3}, Lcom/a/b/c/s;-><init>(Lcom/a/b/c/bw;)V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "softValues"

    new-instance v2, Lcom/a/b/c/z;

    sget-object v3, Lcom/a/b/c/bw;->b:Lcom/a/b/c/bw;

    invoke-direct {v2, v3}, Lcom/a/b/c/z;-><init>(Lcom/a/b/c/bw;)V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "weakValues"

    new-instance v2, Lcom/a/b/c/z;

    sget-object v3, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-direct {v2, v3}, Lcom/a/b/c/z;-><init>(Lcom/a/b/c/bw;)V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "recordStats"

    new-instance v2, Lcom/a/b/c/w;

    invoke-direct {v2}, Lcom/a/b/c/w;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "expireAfterAccess"

    new-instance v2, Lcom/a/b/c/n;

    invoke-direct {v2}, Lcom/a/b/c/n;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "expireAfterWrite"

    new-instance v2, Lcom/a/b/c/aa;

    invoke-direct {v2}, Lcom/a/b/c/aa;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "refreshAfterWrite"

    new-instance v2, Lcom/a/b/c/x;

    invoke-direct {v2}, Lcom/a/b/c/x;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    const-string v1, "refreshInterval"

    new-instance v2, Lcom/a/b/c/x;

    invoke-direct {v2}, Lcom/a/b/c/x;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    sput-object v0, Lcom/a/b/c/l;->p:Lcom/a/b/d/jt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p1, p0, Lcom/a/b/c/l;->q:Ljava/lang/String;

    .line 130
    return-void
.end method

.method private static a()Lcom/a/b/c/l;
    .registers 1

    .prologue
    .line 164
    const-string v0, "maximumSize=0"

    invoke-static {v0}, Lcom/a/b/c/l;->a(Ljava/lang/String;)Lcom/a/b/c/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/a/b/c/l;
    .registers 11

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 138
    new-instance v5, Lcom/a/b/c/l;

    invoke-direct {v5, p0}, Lcom/a/b/c/l;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7c

    .line 140
    sget-object v0, Lcom/a/b/c/l;->n:Lcom/a/b/b/di;

    invoke-virtual {v0, p0}, Lcom/a/b/b/di;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_17
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 141
    sget-object v1, Lcom/a/b/c/l;->o:Lcom/a/b/b/di;

    invoke-virtual {v1, v0}, Lcom/a/b/b/di;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v7

    .line 142
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6f

    move v1, v3

    :goto_34
    const-string v2, "blank key-value pair"

    invoke-static {v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 143
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_71

    move v1, v3

    :goto_41
    const-string v2, "key-value pair %s with more than one equals sign"

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v0, v8, v4

    invoke-static {v1, v2, v8}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 147
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 148
    sget-object v1, Lcom/a/b/c/l;->p:Lcom/a/b/d/jt;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/c/y;

    .line 149
    if-eqz v1, :cond_73

    move v2, v3

    :goto_5b
    const-string v8, "unknown key %s"

    new-array v9, v3, [Ljava/lang/Object;

    aput-object v0, v9, v4

    invoke-static {v2, v8, v9}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 151
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_75

    const/4 v2, 0x0

    .line 152
    :goto_6b
    invoke-interface {v1, v5, v0, v2}, Lcom/a/b/c/y;->a(Lcom/a/b/c/l;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_17

    :cond_6f
    move v1, v4

    .line 142
    goto :goto_34

    :cond_71
    move v1, v4

    .line 143
    goto :goto_41

    :cond_73
    move v2, v4

    .line 149
    goto :goto_5b

    .line 151
    :cond_75
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_6b

    .line 156
    :cond_7c
    return-object v5
.end method

.method private static a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;
    .registers 5
    .param p2    # Ljava/util/concurrent/TimeUnit;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 284
    if-nez p2, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    invoke-virtual {p2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3
.end method

.method private b()Lcom/a/b/c/f;
    .registers 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    invoke-static {}, Lcom/a/b/c/f;->a()Lcom/a/b/c/f;

    move-result-object v3

    .line 172
    iget-object v0, p0, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2d

    .line 173
    iget-object v0, p0, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1332
    iget v0, v3, Lcom/a/b/c/f;->g:I

    const/4 v5, -0x1

    if-ne v0, v5, :cond_6b

    move v0, v1

    :goto_16
    const-string v5, "initial capacity was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget v7, v3, Lcom/a/b/c/f;->g:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1334
    if-ltz v4, :cond_6d

    move v0, v1

    :goto_28
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 1335
    iput v4, v3, Lcom/a/b/c/f;->g:I

    .line 175
    :cond_2d
    iget-object v0, p0, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    if-eqz v0, :cond_3a

    .line 176
    iget-object v0, p0, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/a/b/c/f;->a(J)Lcom/a/b/c/f;

    .line 178
    :cond_3a
    iget-object v0, p0, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    if-eqz v0, :cond_47

    .line 179
    iget-object v0, p0, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/a/b/c/f;->b(J)Lcom/a/b/c/f;

    .line 181
    :cond_47
    iget-object v0, p0, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_54

    .line 182
    iget-object v0, p0, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->a(I)Lcom/a/b/c/f;

    .line 184
    :cond_54
    iget-object v0, p0, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    if-eqz v0, :cond_74

    .line 185
    sget-object v0, Lcom/a/b/c/m;->a:[I

    iget-object v4, p0, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    invoke-virtual {v4}, Lcom/a/b/c/bw;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_102

    .line 190
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_6b
    move v0, v2

    .line 1332
    goto :goto_16

    :cond_6d
    move v0, v2

    .line 1334
    goto :goto_28

    .line 1518
    :pswitch_6f
    sget-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    .line 193
    :cond_74
    iget-object v0, p0, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    if-eqz v0, :cond_90

    .line 194
    sget-object v0, Lcom/a/b/c/m;->a:[I

    iget-object v4, p0, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    invoke-virtual {v4}, Lcom/a/b/c/bw;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_108

    .line 202
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1573
    :pswitch_8b
    sget-object v0, Lcom/a/b/c/bw;->b:Lcom/a/b/c/bw;

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    .line 205
    :cond_90
    :goto_90
    iget-object v0, p0, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_a0

    iget-object v0, p0, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 2762
    sget-object v0, Lcom/a/b/c/f;->c:Lcom/a/b/b/dz;

    iput-object v0, v3, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    .line 208
    :cond_a0
    iget-object v0, p0, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_ab

    .line 209
    iget-wide v4, p0, Lcom/a/b/c/l;->h:J

    iget-object v0, p0, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v0}, Lcom/a/b/c/f;->a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;

    .line 211
    :cond_ab
    iget-object v0, p0, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_b6

    .line 212
    iget-wide v4, p0, Lcom/a/b/c/l;->j:J

    iget-object v0, p0, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v0}, Lcom/a/b/c/f;->b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;

    .line 214
    :cond_b6
    iget-object v0, p0, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_f6

    .line 215
    iget-wide v4, p0, Lcom/a/b/c/l;->l:J

    iget-object v6, p0, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    .line 3680
    invoke-static {v6}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3681
    iget-wide v8, v3, Lcom/a/b/c/f;->p:J

    const-wide/16 v10, -0x1

    cmp-long v0, v8, v10

    if-nez v0, :cond_fd

    move v0, v1

    :goto_ca
    const-string v7, "refresh was already set to %s ns"

    new-array v8, v1, [Ljava/lang/Object;

    iget-wide v10, v3, Lcom/a/b/c/f;->p:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v0, v7, v8}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3682
    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-lez v0, :cond_ff

    move v0, v1

    :goto_e0
    const-string v7, "duration must be positive: %s %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    aput-object v6, v8, v1

    invoke-static {v0, v7, v8}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3683
    invoke-virtual {v6, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, v3, Lcom/a/b/c/f;->p:J

    .line 218
    :cond_f6
    return-object v3

    .line 2549
    :pswitch_f7
    sget-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    goto :goto_90

    :cond_fd
    move v0, v2

    .line 3681
    goto :goto_ca

    :cond_ff
    move v0, v2

    .line 3682
    goto :goto_e0

    .line 185
    nop

    :pswitch_data_102
    .packed-switch 0x1
        :pswitch_6f
    .end packed-switch

    .line 194
    :pswitch_data_108
    .packed-switch 0x1
        :pswitch_f7
        :pswitch_8b
    .end packed-switch
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/a/b/c/l;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 257
    if-ne p0, p1, :cond_5

    .line 264
    :cond_4
    :goto_4
    return v0

    .line 260
    :cond_5
    instance-of v2, p1, Lcom/a/b/c/l;

    if-nez v2, :cond_b

    move v0, v1

    .line 261
    goto :goto_4

    .line 263
    :cond_b
    check-cast p1, Lcom/a/b/c/l;

    .line 264
    iget-object v2, p0, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-object v2, p0, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    iget-object v3, p1, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-object v2, p0, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    iget-object v3, p1, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-object v2, p0, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-object v2, p0, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    iget-object v3, p1, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-object v2, p0, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    iget-object v3, p1, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-object v2, p0, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-wide v2, p0, Lcom/a/b/c/l;->h:J

    iget-object v4, p0, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/a/b/c/l;->h:J

    iget-object v3, p1, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    invoke-static {v4, v5, v3}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-wide v2, p0, Lcom/a/b/c/l;->j:J

    iget-object v4, p0, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/a/b/c/l;->j:J

    iget-object v3, p1, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    invoke-static {v4, v5, v3}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_95

    iget-wide v2, p0, Lcom/a/b/c/l;->l:J

    iget-object v4, p0, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/a/b/c/l;->l:J

    iget-object v3, p1, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    invoke-static {v4, v5, v3}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_95
    move v0, v1

    goto/16 :goto_4
.end method

.method public final hashCode()I
    .registers 6

    .prologue
    .line 242
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/a/b/c/l;->h:J

    iget-object v4, p0, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/a/b/c/l;->j:J

    iget-object v4, p0, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/a/b/c/l;->l:J

    iget-object v4, p0, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4}, Lcom/a/b/c/l;->a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 5084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 242
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 237
    invoke-static {p0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    .line 4228
    iget-object v1, p0, Lcom/a/b/c/l;->q:Ljava/lang/String;

    .line 4257
    invoke-virtual {v0, v1}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Lcom/a/b/b/cc;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
