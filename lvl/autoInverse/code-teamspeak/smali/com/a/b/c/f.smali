.class public final Lcom/a/b/c/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field static final a:Lcom/a/b/b/dz;

.field static final b:Lcom/a/b/c/ai;

.field static final c:Lcom/a/b/b/dz;

.field static final d:Lcom/a/b/b/ej;

.field static final e:I = -0x1

.field private static final v:I = 0x10

.field private static final w:I = 0x4

.field private static final x:I

.field private static final y:I

.field private static final z:Ljava/util/logging/Logger;


# instance fields
.field f:Z

.field g:I

.field h:I

.field i:J

.field j:J

.field k:Lcom/a/b/c/do;

.field l:Lcom/a/b/c/bw;

.field m:Lcom/a/b/c/bw;

.field n:J

.field o:J

.field p:J

.field q:Lcom/a/b/b/au;

.field r:Lcom/a/b/b/au;

.field s:Lcom/a/b/c/dg;

.field t:Lcom/a/b/b/ej;

.field u:Lcom/a/b/b/dz;


# direct methods
.method static constructor <clinit>()V
    .registers 14

    .prologue
    const-wide/16 v2, 0x0

    .line 158
    new-instance v0, Lcom/a/b/c/g;

    invoke-direct {v0}, Lcom/a/b/c/g;-><init>()V

    .line 16219
    new-instance v1, Lcom/a/b/b/eg;

    invoke-direct {v1, v0}, Lcom/a/b/b/eg;-><init>(Ljava/lang/Object;)V

    .line 158
    sput-object v1, Lcom/a/b/c/f;->a:Lcom/a/b/b/dz;

    .line 180
    new-instance v1, Lcom/a/b/c/ai;

    move-wide v4, v2

    move-wide v6, v2

    move-wide v8, v2

    move-wide v10, v2

    move-wide v12, v2

    invoke-direct/range {v1 .. v13}, Lcom/a/b/c/ai;-><init>(JJJJJJ)V

    sput-object v1, Lcom/a/b/c/f;->b:Lcom/a/b/c/ai;

    .line 182
    new-instance v0, Lcom/a/b/c/h;

    invoke-direct {v0}, Lcom/a/b/c/h;-><init>()V

    sput-object v0, Lcom/a/b/c/f;->c:Lcom/a/b/b/dz;

    .line 206
    new-instance v0, Lcom/a/b/c/i;

    invoke-direct {v0}, Lcom/a/b/c/i;-><init>()V

    sput-object v0, Lcom/a/b/c/f;->d:Lcom/a/b/b/ej;

    .line 213
    const-class v0, Lcom/a/b/c/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/a/b/c/f;->z:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>()V
    .registers 5

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/b/c/f;->f:Z

    .line 219
    iput v1, p0, Lcom/a/b/c/f;->g:I

    .line 220
    iput v1, p0, Lcom/a/b/c/f;->h:I

    .line 221
    iput-wide v2, p0, Lcom/a/b/c/f;->i:J

    .line 222
    iput-wide v2, p0, Lcom/a/b/c/f;->j:J

    .line 228
    iput-wide v2, p0, Lcom/a/b/c/f;->n:J

    .line 229
    iput-wide v2, p0, Lcom/a/b/c/f;->o:J

    .line 230
    iput-wide v2, p0, Lcom/a/b/c/f;->p:J

    .line 238
    sget-object v0, Lcom/a/b/c/f;->a:Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    .line 241
    return-void
.end method

.method private a(Z)Lcom/a/b/b/ej;
    .registers 3

    .prologue
    .line 707
    iget-object v0, p0, Lcom/a/b/c/f;->t:Lcom/a/b/b/ej;

    if-eqz v0, :cond_7

    .line 708
    iget-object v0, p0, Lcom/a/b/c/f;->t:Lcom/a/b/b/ej;

    .line 710
    :goto_6
    return-object v0

    :cond_7
    if-eqz p1, :cond_e

    invoke-static {}, Lcom/a/b/b/ej;->b()Lcom/a/b/b/ej;

    move-result-object v0

    goto :goto_6

    :cond_e
    sget-object v0, Lcom/a/b/c/f;->d:Lcom/a/b/b/ej;

    goto :goto_6
.end method

.method public static a()Lcom/a/b/c/f;
    .registers 1

    .prologue
    .line 248
    new-instance v0, Lcom/a/b/c/f;

    invoke-direct {v0}, Lcom/a/b/c/f;-><init>()V

    return-object v0
.end method

.method private a(Lcom/a/b/b/au;)Lcom/a/b/c/f;
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 293
    iget-object v0, p0, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    if-nez v0, :cond_1b

    move v0, v1

    :goto_7
    const-string v3, "key equivalence was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 294
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, p0, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    .line 295
    return-object p0

    :cond_1b
    move v0, v2

    .line 293
    goto :goto_7
.end method

.method private a(Lcom/a/b/b/ej;)Lcom/a/b/c/f;
    .registers 3

    .prologue
    .line 701
    iget-object v0, p0, Lcom/a/b/c/f;->t:Lcom/a/b/b/ej;

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 702
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/ej;

    iput-object v0, p0, Lcom/a/b/c/f;->t:Lcom/a/b/b/ej;

    .line 703
    return-object p0

    .line 701
    :cond_11
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private a(Lcom/a/b/c/dg;)Lcom/a/b/c/f;
    .registers 3
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 737
    iget-object v0, p0, Lcom/a/b/c/f;->s:Lcom/a/b/c/dg;

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 742
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/dg;

    iput-object v0, p0, Lcom/a/b/c/f;->s:Lcom/a/b/c/dg;

    .line 743
    return-object p0

    .line 737
    :cond_11
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private a(Lcom/a/b/c/do;)Lcom/a/b/c/f;
    .registers 10
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 477
    iget-object v0, p0, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    if-nez v0, :cond_2f

    move v0, v1

    :goto_7
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 478
    iget-boolean v0, p0, Lcom/a/b/c/f;->f:Z

    if-eqz v0, :cond_26

    .line 479
    iget-wide v4, p0, Lcom/a/b/c/f;->i:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_31

    move v0, v1

    :goto_17
    const-string v3, "weigher can not be combined with maximum size"

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/a/b/c/f;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 486
    :cond_26
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/do;

    iput-object v0, p0, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    .line 487
    return-object p0

    :cond_2f
    move v0, v2

    .line 477
    goto :goto_7

    :cond_31
    move v0, v2

    .line 479
    goto :goto_17
.end method

.method private static a(Lcom/a/b/c/l;)Lcom/a/b/c/f;
    .registers 13
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1248
    new-instance v3, Lcom/a/b/c/f;

    invoke-direct {v3}, Lcom/a/b/c/f;-><init>()V

    .line 1172
    iget-object v0, p0, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2e

    .line 1173
    iget-object v0, p0, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1332
    iget v0, v3, Lcom/a/b/c/f;->g:I

    const/4 v5, -0x1

    if-ne v0, v5, :cond_6c

    move v0, v1

    :goto_17
    const-string v5, "initial capacity was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget v7, v3, Lcom/a/b/c/f;->g:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1334
    if-ltz v4, :cond_6e

    move v0, v1

    :goto_29
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 1335
    iput v4, v3, Lcom/a/b/c/f;->g:I

    .line 1175
    :cond_2e
    iget-object v0, p0, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    if-eqz v0, :cond_3b

    .line 1176
    iget-object v0, p0, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/a/b/c/f;->a(J)Lcom/a/b/c/f;

    .line 1178
    :cond_3b
    iget-object v0, p0, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    if-eqz v0, :cond_48

    .line 1179
    iget-object v0, p0, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/a/b/c/f;->b(J)Lcom/a/b/c/f;

    .line 1181
    :cond_48
    iget-object v0, p0, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_55

    .line 1182
    iget-object v0, p0, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->a(I)Lcom/a/b/c/f;

    .line 1184
    :cond_55
    iget-object v0, p0, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    if-eqz v0, :cond_75

    .line 1185
    sget-object v0, Lcom/a/b/c/m;->a:[I

    iget-object v4, p0, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    invoke-virtual {v4}, Lcom/a/b/c/bw;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_104

    .line 1190
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_6c
    move v0, v2

    .line 1332
    goto :goto_17

    :cond_6e
    move v0, v2

    .line 1334
    goto :goto_29

    .line 1518
    :pswitch_70
    sget-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    .line 1193
    :cond_75
    iget-object v0, p0, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    if-eqz v0, :cond_91

    .line 1194
    sget-object v0, Lcom/a/b/c/m;->a:[I

    iget-object v4, p0, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    invoke-virtual {v4}, Lcom/a/b/c/bw;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_10a

    .line 1202
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1573
    :pswitch_8c
    sget-object v0, Lcom/a/b/c/bw;->b:Lcom/a/b/c/bw;

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    .line 1205
    :cond_91
    :goto_91
    iget-object v0, p0, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_a1

    iget-object v0, p0, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a1

    .line 2762
    sget-object v0, Lcom/a/b/c/f;->c:Lcom/a/b/b/dz;

    iput-object v0, v3, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    .line 1208
    :cond_a1
    iget-object v0, p0, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_ac

    .line 1209
    iget-wide v4, p0, Lcom/a/b/c/l;->h:J

    iget-object v0, p0, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v0}, Lcom/a/b/c/f;->a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;

    .line 1211
    :cond_ac
    iget-object v0, p0, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_b7

    .line 1212
    iget-wide v4, p0, Lcom/a/b/c/l;->j:J

    iget-object v0, p0, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v0}, Lcom/a/b/c/f;->b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;

    .line 1214
    :cond_b7
    iget-object v0, p0, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_f7

    .line 1215
    iget-wide v4, p0, Lcom/a/b/c/l;->l:J

    iget-object v6, p0, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    .line 3680
    invoke-static {v6}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3681
    iget-wide v8, v3, Lcom/a/b/c/f;->p:J

    const-wide/16 v10, -0x1

    cmp-long v0, v8, v10

    if-nez v0, :cond_100

    move v0, v1

    :goto_cb
    const-string v7, "refresh was already set to %s ns"

    new-array v8, v1, [Ljava/lang/Object;

    iget-wide v10, v3, Lcom/a/b/c/f;->p:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v0, v7, v8}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3682
    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-lez v0, :cond_102

    move v0, v1

    :goto_e1
    const-string v7, "duration must be positive: %s %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    aput-object v6, v8, v1

    invoke-static {v0, v7, v8}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3683
    invoke-virtual {v6, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, v3, Lcom/a/b/c/f;->p:J

    .line 4281
    :cond_f7
    iput-boolean v2, v3, Lcom/a/b/c/f;->f:Z

    .line 259
    return-object v3

    .line 2549
    :pswitch_fa
    sget-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    goto :goto_91

    :cond_100
    move v0, v2

    .line 3681
    goto :goto_cb

    :cond_102
    move v0, v2

    .line 3682
    goto :goto_e1

    .line 1185
    :pswitch_data_104
    .packed-switch 0x1
        :pswitch_70
    .end packed-switch

    .line 1194
    :pswitch_data_10a
    .packed-switch 0x1
        :pswitch_fa
        :pswitch_8c
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/c/f;
    .registers 13
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 273
    invoke-static {p0}, Lcom/a/b/c/l;->a(Ljava/lang/String;)Lcom/a/b/c/l;

    move-result-object v3

    .line 6248
    new-instance v4, Lcom/a/b/c/f;

    invoke-direct {v4}, Lcom/a/b/c/f;-><init>()V

    .line 6172
    iget-object v0, v3, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_32

    .line 6173
    iget-object v0, v3, Lcom/a/b/c/l;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 6332
    iget v0, v4, Lcom/a/b/c/f;->g:I

    const/4 v6, -0x1

    if-ne v0, v6, :cond_70

    move v0, v1

    :goto_1b
    const-string v6, "initial capacity was already set to %s"

    new-array v7, v1, [Ljava/lang/Object;

    iget v8, v4, Lcom/a/b/c/f;->g:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v0, v6, v7}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 6334
    if-ltz v5, :cond_72

    move v0, v1

    :goto_2d
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 6335
    iput v5, v4, Lcom/a/b/c/f;->g:I

    .line 6175
    :cond_32
    iget-object v0, v3, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    if-eqz v0, :cond_3f

    .line 6176
    iget-object v0, v3, Lcom/a/b/c/l;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/a/b/c/f;->a(J)Lcom/a/b/c/f;

    .line 6178
    :cond_3f
    iget-object v0, v3, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    if-eqz v0, :cond_4c

    .line 6179
    iget-object v0, v3, Lcom/a/b/c/l;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/a/b/c/f;->b(J)Lcom/a/b/c/f;

    .line 6181
    :cond_4c
    iget-object v0, v3, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_59

    .line 6182
    iget-object v0, v3, Lcom/a/b/c/l;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/a/b/c/f;->a(I)Lcom/a/b/c/f;

    .line 6184
    :cond_59
    iget-object v0, v3, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    if-eqz v0, :cond_79

    .line 6185
    sget-object v0, Lcom/a/b/c/m;->a:[I

    iget-object v5, v3, Lcom/a/b/c/l;->e:Lcom/a/b/c/bw;

    invoke-virtual {v5}, Lcom/a/b/c/bw;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_108

    .line 6190
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_70
    move v0, v2

    .line 6332
    goto :goto_1b

    :cond_72
    move v0, v2

    .line 6334
    goto :goto_2d

    .line 6518
    :pswitch_74
    sget-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {v4, v0}, Lcom/a/b/c/f;->a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    .line 6193
    :cond_79
    iget-object v0, v3, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    if-eqz v0, :cond_95

    .line 6194
    sget-object v0, Lcom/a/b/c/m;->a:[I

    iget-object v5, v3, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    invoke-virtual {v5}, Lcom/a/b/c/bw;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_10e

    .line 6202
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 6573
    :pswitch_90
    sget-object v0, Lcom/a/b/c/bw;->b:Lcom/a/b/c/bw;

    invoke-virtual {v4, v0}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    .line 6205
    :cond_95
    :goto_95
    iget-object v0, v3, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_a5

    iget-object v0, v3, Lcom/a/b/c/l;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 7762
    sget-object v0, Lcom/a/b/c/f;->c:Lcom/a/b/b/dz;

    iput-object v0, v4, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    .line 6208
    :cond_a5
    iget-object v0, v3, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_b0

    .line 6209
    iget-wide v6, v3, Lcom/a/b/c/l;->h:J

    iget-object v0, v3, Lcom/a/b/c/l;->i:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v0}, Lcom/a/b/c/f;->a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;

    .line 6211
    :cond_b0
    iget-object v0, v3, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_bb

    .line 6212
    iget-wide v6, v3, Lcom/a/b/c/l;->j:J

    iget-object v0, v3, Lcom/a/b/c/l;->k:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v0}, Lcom/a/b/c/f;->b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;

    .line 6214
    :cond_bb
    iget-object v0, v3, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    if-eqz v0, :cond_fb

    .line 6215
    iget-wide v6, v3, Lcom/a/b/c/l;->l:J

    iget-object v3, v3, Lcom/a/b/c/l;->m:Ljava/util/concurrent/TimeUnit;

    .line 8680
    invoke-static {v3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8681
    iget-wide v8, v4, Lcom/a/b/c/f;->p:J

    const-wide/16 v10, -0x1

    cmp-long v0, v8, v10

    if-nez v0, :cond_104

    move v0, v1

    :goto_cf
    const-string v5, "refresh was already set to %s ns"

    new-array v8, v1, [Ljava/lang/Object;

    iget-wide v10, v4, Lcom/a/b/c/f;->p:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v0, v5, v8}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8682
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_106

    move v0, v1

    :goto_e5
    const-string v5, "duration must be positive: %s %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    aput-object v3, v8, v1

    invoke-static {v0, v5, v8}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8683
    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, v4, Lcom/a/b/c/f;->p:J

    .line 9281
    :cond_fb
    iput-boolean v2, v4, Lcom/a/b/c/f;->f:Z

    .line 273
    return-object v4

    .line 7549
    :pswitch_fe
    sget-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {v4, v0}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    goto :goto_95

    :cond_104
    move v0, v2

    .line 8681
    goto :goto_cf

    :cond_106
    move v0, v2

    .line 8682
    goto :goto_e5

    .line 6185
    :pswitch_data_108
    .packed-switch 0x1
        :pswitch_74
    .end packed-switch

    .line 6194
    :pswitch_data_10e
    .packed-switch 0x1
        :pswitch_fe
        :pswitch_90
    .end packed-switch
.end method

.method private b(I)Lcom/a/b/c/f;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 332
    iget v0, p0, Lcom/a/b/c/f;->g:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1f

    move v0, v1

    :goto_8
    const-string v3, "initial capacity was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/a/b/c/f;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 334
    if-ltz p1, :cond_21

    :goto_19
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Z)V

    .line 335
    iput p1, p0, Lcom/a/b/c/f;->g:I

    .line 336
    return-object p0

    :cond_1f
    move v0, v2

    .line 332
    goto :goto_8

    :cond_21
    move v1, v2

    .line 334
    goto :goto_19
.end method

.method private b(Lcom/a/b/b/au;)Lcom/a/b/c/f;
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 311
    iget-object v0, p0, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    if-nez v0, :cond_1b

    move v0, v1

    :goto_7
    const-string v3, "value equivalence was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 313
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, p0, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    .line 314
    return-object p0

    :cond_1b
    move v0, v2

    .line 311
    goto :goto_7
.end method

.method private c(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
    .registers 13
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/c;
        a = "To be supported (synchronously)."
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 680
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 681
    iget-wide v4, p0, Lcom/a/b/c/f;->p:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_3b

    move v0, v1

    :goto_e
    const-string v3, "refresh was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/c/f;->p:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 682
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_3d

    move v0, v1

    :goto_24
    const-string v3, "duration must be positive: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 683
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/c/f;->p:J

    .line 684
    return-object p0

    :cond_3b
    move v0, v2

    .line 681
    goto :goto_e

    :cond_3d
    move v0, v2

    .line 682
    goto :goto_24
.end method

.method private e()Lcom/a/b/c/f;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    .line 281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/a/b/c/f;->f:Z

    .line 282
    return-object p0
.end method

.method private f()Lcom/a/b/b/au;
    .registers 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    invoke-virtual {p0}, Lcom/a/b/c/f;->b()Lcom/a/b/c/bw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/c/bw;->a()Lcom/a/b/b/au;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    return-object v0
.end method

.method private g()Lcom/a/b/b/au;
    .registers 3

    .prologue
    .line 318
    iget-object v0, p0, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    invoke-virtual {p0}, Lcom/a/b/c/f;->c()Lcom/a/b/c/bw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/c/bw;->a()Lcom/a/b/b/au;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    return-object v0
.end method

.method private h()I
    .registers 3

    .prologue
    .line 340
    iget v0, p0, Lcom/a/b/c/f;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    const/16 v0, 0x10

    :goto_7
    return v0

    :cond_8
    iget v0, p0, Lcom/a/b/c/f;->g:I

    goto :goto_7
.end method

.method private i()I
    .registers 3

    .prologue
    .line 382
    iget v0, p0, Lcom/a/b/c/f;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    const/4 v0, 0x4

    :goto_6
    return v0

    :cond_7
    iget v0, p0, Lcom/a/b/c/f;->h:I

    goto :goto_6
.end method

.method private j()J
    .registers 5

    .prologue
    const-wide/16 v0, 0x0

    .line 491
    iget-wide v2, p0, Lcom/a/b/c/f;->n:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_e

    iget-wide v2, p0, Lcom/a/b/c/f;->o:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_f

    .line 494
    :cond_e
    :goto_e
    return-wide v0

    :cond_f
    iget-object v0, p0, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    if-nez v0, :cond_16

    iget-wide v0, p0, Lcom/a/b/c/f;->i:J

    goto :goto_e

    :cond_16
    iget-wide v0, p0, Lcom/a/b/c/f;->j:J

    goto :goto_e
.end method

.method private k()Lcom/a/b/c/do;
    .registers 3

    .prologue
    .line 500
    iget-object v0, p0, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    sget-object v1, Lcom/a/b/c/k;->a:Lcom/a/b/c/k;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/do;

    return-object v0
.end method

.method private l()Lcom/a/b/c/f;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.WeakReference"
    .end annotation

    .prologue
    .line 518
    sget-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {p0, v0}, Lcom/a/b/c/f;->a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method private m()Lcom/a/b/c/f;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.WeakReference"
    .end annotation

    .prologue
    .line 549
    sget-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {p0, v0}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method private n()Lcom/a/b/c/f;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.ref.SoftReference"
    .end annotation

    .prologue
    .line 573
    sget-object v0, Lcom/a/b/c/bw;->b:Lcom/a/b/c/bw;

    invoke-virtual {p0, v0}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method private o()J
    .registers 5

    .prologue
    .line 614
    iget-wide v0, p0, Lcom/a/b/c/f;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, Lcom/a/b/c/f;->n:J

    goto :goto_a
.end method

.method private p()J
    .registers 5

    .prologue
    .line 648
    iget-wide v0, p0, Lcom/a/b/c/f;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, Lcom/a/b/c/f;->o:J

    goto :goto_a
.end method

.method private q()J
    .registers 5

    .prologue
    .line 688
    iget-wide v0, p0, Lcom/a/b/c/f;->p:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    :goto_a
    return-wide v0

    :cond_b
    iget-wide v0, p0, Lcom/a/b/c/f;->p:J

    goto :goto_a
.end method

.method private r()Lcom/a/b/c/dg;
    .registers 3

    .prologue
    .line 749
    iget-object v0, p0, Lcom/a/b/c/f;->s:Lcom/a/b/c/dg;

    sget-object v1, Lcom/a/b/c/j;->a:Lcom/a/b/c/j;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/dg;

    return-object v0
.end method

.method private s()Lcom/a/b/c/f;
    .registers 2

    .prologue
    .line 762
    sget-object v0, Lcom/a/b/c/f;->c:Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    .line 763
    return-object p0
.end method

.method private t()Z
    .registers 3

    .prologue
    .line 767
    iget-object v0, p0, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    sget-object v1, Lcom/a/b/c/f;->c:Lcom/a/b/b/dz;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private u()Lcom/a/b/b/dz;
    .registers 2

    .prologue
    .line 771
    iget-object v0, p0, Lcom/a/b/c/f;->u:Lcom/a/b/b/dz;

    return-object v0
.end method

.method private v()Lcom/a/b/c/e;
    .registers 5

    .prologue
    .line 805
    invoke-virtual {p0}, Lcom/a/b/c/f;->d()V

    .line 9811
    iget-wide v0, p0, Lcom/a/b/c/f;->p:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_c
    const-string v1, "refreshAfterWrite requires a LoadingCache"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 807
    new-instance v0, Lcom/a/b/c/bo;

    invoke-direct {v0, p0}, Lcom/a/b/c/bo;-><init>(Lcom/a/b/c/f;)V

    return-object v0

    .line 9811
    :cond_17
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private w()V
    .registers 5

    .prologue
    .line 811
    iget-wide v0, p0, Lcom/a/b/c/f;->p:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_9
    const-string v1, "refreshAfterWrite requires a LoadingCache"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 812
    return-void

    .line 811
    :cond_f
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public final a(Lcom/a/b/c/ab;)Lcom/a/b/c/an;
    .registers 3

    .prologue
    .line 788
    invoke-virtual {p0}, Lcom/a/b/c/f;->d()V

    .line 789
    new-instance v0, Lcom/a/b/c/bn;

    invoke-direct {v0, p0, p1}, Lcom/a/b/c/bn;-><init>(Lcom/a/b/c/f;Lcom/a/b/c/ab;)V

    return-object v0
.end method

.method public final a(I)Lcom/a/b/c/f;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 374
    iget v0, p0, Lcom/a/b/c/f;->h:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1f

    move v0, v1

    :goto_8
    const-string v3, "concurrency level was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/a/b/c/f;->h:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 376
    if-lez p1, :cond_21

    :goto_19
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Z)V

    .line 377
    iput p1, p0, Lcom/a/b/c/f;->h:I

    .line 378
    return-object p0

    :cond_1f
    move v0, v2

    .line 374
    goto :goto_8

    :cond_21
    move v1, v2

    .line 376
    goto :goto_19
.end method

.method public final a(J)Lcom/a/b/c/f;
    .registers 14

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 401
    iget-wide v4, p0, Lcom/a/b/c/f;->i:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_48

    move v0, v1

    :goto_b
    const-string v3, "maximum size was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/c/f;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 403
    iget-wide v4, p0, Lcom/a/b/c/f;->j:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_4a

    move v0, v1

    :goto_21
    const-string v3, "maximum weight was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/c/f;->j:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 405
    iget-object v0, p0, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    if-nez v0, :cond_4c

    move v0, v1

    :goto_35
    const-string v3, "maximum size can not be combined with weigher"

    invoke-static {v0, v3}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 406
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_4e

    :goto_40
    const-string v0, "maximum size must not be negative"

    invoke-static {v1, v0}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 407
    iput-wide p1, p0, Lcom/a/b/c/f;->i:J

    .line 408
    return-object p0

    :cond_48
    move v0, v2

    .line 401
    goto :goto_b

    :cond_4a
    move v0, v2

    .line 403
    goto :goto_21

    :cond_4c
    move v0, v2

    .line 405
    goto :goto_35

    :cond_4e
    move v1, v2

    .line 406
    goto :goto_40
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
    .registers 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 606
    iget-wide v4, p0, Lcom/a/b/c/f;->n:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_38

    move v0, v1

    :goto_b
    const-string v3, "expireAfterWrite was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/c/f;->n:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 608
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_3a

    move v0, v1

    :goto_21
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 609
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/c/f;->n:J

    .line 610
    return-object p0

    :cond_38
    move v0, v2

    .line 606
    goto :goto_b

    :cond_3a
    move v0, v2

    .line 608
    goto :goto_21
.end method

.method public final a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 522
    iget-object v0, p0, Lcom/a/b/c/f;->l:Lcom/a/b/c/bw;

    if-nez v0, :cond_1b

    move v0, v1

    :goto_7
    const-string v3, "Key strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/c/f;->l:Lcom/a/b/c/bw;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 523
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/bw;

    iput-object v0, p0, Lcom/a/b/c/f;->l:Lcom/a/b/c/bw;

    .line 524
    return-object p0

    :cond_1b
    move v0, v2

    .line 522
    goto :goto_7
.end method

.method final b()Lcom/a/b/c/bw;
    .registers 3

    .prologue
    .line 528
    iget-object v0, p0, Lcom/a/b/c/f;->l:Lcom/a/b/c/bw;

    sget-object v1, Lcom/a/b/c/bw;->a:Lcom/a/b/c/bw;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/bw;

    return-object v0
.end method

.method public final b(J)Lcom/a/b/c/f;
    .registers 14
    .annotation build Lcom/a/b/a/c;
        a = "To be supported"
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 437
    iget-wide v4, p0, Lcom/a/b/c/f;->j:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_3e

    move v0, v1

    :goto_b
    const-string v3, "maximum weight was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/c/f;->j:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 439
    iget-wide v4, p0, Lcom/a/b/c/f;->i:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_40

    move v0, v1

    :goto_21
    const-string v3, "maximum size was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/c/f;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 441
    iput-wide p1, p0, Lcom/a/b/c/f;->j:J

    .line 442
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_42

    :goto_38
    const-string v0, "maximum weight must not be negative"

    invoke-static {v1, v0}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 443
    return-object p0

    :cond_3e
    move v0, v2

    .line 437
    goto :goto_b

    :cond_40
    move v0, v2

    .line 439
    goto :goto_21

    :cond_42
    move v1, v2

    .line 442
    goto :goto_38
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
    .registers 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 640
    iget-wide v4, p0, Lcom/a/b/c/f;->o:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_38

    move v0, v1

    :goto_b
    const-string v3, "expireAfterAccess was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/a/b/c/f;->o:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 642
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_3a

    move v0, v1

    :goto_21
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 643
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/c/f;->o:J

    .line 644
    return-object p0

    :cond_38
    move v0, v2

    .line 640
    goto :goto_b

    :cond_3a
    move v0, v2

    .line 642
    goto :goto_21
.end method

.method final b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 577
    iget-object v0, p0, Lcom/a/b/c/f;->m:Lcom/a/b/c/bw;

    if-nez v0, :cond_1b

    move v0, v1

    :goto_7
    const-string v3, "Value strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/c/f;->m:Lcom/a/b/c/bw;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 578
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/bw;

    iput-object v0, p0, Lcom/a/b/c/f;->m:Lcom/a/b/c/bw;

    .line 579
    return-object p0

    :cond_1b
    move v0, v2

    .line 577
    goto :goto_7
.end method

.method final c()Lcom/a/b/c/bw;
    .registers 3

    .prologue
    .line 583
    iget-object v0, p0, Lcom/a/b/c/f;->m:Lcom/a/b/c/bw;

    sget-object v1, Lcom/a/b/c/bw;->a:Lcom/a/b/c/bw;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/bw;

    return-object v0
.end method

.method final d()V
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    .line 815
    iget-object v2, p0, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    if-nez v2, :cond_16

    .line 816
    iget-wide v2, p0, Lcom/a/b/c/f;->j:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_14

    :goto_e
    const-string v1, "maximumWeight requires weigher"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 826
    :cond_13
    :goto_13
    return-void

    :cond_14
    move v0, v1

    .line 816
    goto :goto_e

    .line 818
    :cond_16
    iget-boolean v2, p0, Lcom/a/b/c/f;->f:Z

    if-eqz v2, :cond_28

    .line 819
    iget-wide v2, p0, Lcom/a/b/c/f;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_26

    :goto_20
    const-string v1, "weigher requires maximumWeight"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    goto :goto_13

    :cond_26
    move v0, v1

    goto :goto_20

    .line 821
    :cond_28
    iget-wide v0, p0, Lcom/a/b/c/f;->j:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_13

    .line 822
    sget-object v0, Lcom/a/b/c/f;->z:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "ignoring weigher specified without maximumWeight"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_13
.end method

.method public final toString()Ljava/lang/String;
    .registers 9

    .prologue
    const/16 v5, 0x16

    const/4 v3, -0x1

    const-wide/16 v6, -0x1

    .line 834
    invoke-static {p0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    .line 835
    iget v1, p0, Lcom/a/b/c/f;->g:I

    if-eq v1, v3, :cond_14

    .line 836
    const-string v1, "initialCapacity"

    iget v2, p0, Lcom/a/b/c/f;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;I)Lcom/a/b/b/cc;

    .line 838
    :cond_14
    iget v1, p0, Lcom/a/b/c/f;->h:I

    if-eq v1, v3, :cond_1f

    .line 839
    const-string v1, "concurrencyLevel"

    iget v2, p0, Lcom/a/b/c/f;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;I)Lcom/a/b/b/cc;

    .line 841
    :cond_1f
    iget-wide v2, p0, Lcom/a/b/c/f;->i:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2c

    .line 842
    const-string v1, "maximumSize"

    iget-wide v2, p0, Lcom/a/b/c/f;->i:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/b/cc;->a(Ljava/lang/String;J)Lcom/a/b/b/cc;

    .line 844
    :cond_2c
    iget-wide v2, p0, Lcom/a/b/c/f;->j:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_39

    .line 845
    const-string v1, "maximumWeight"

    iget-wide v2, p0, Lcom/a/b/c/f;->j:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/b/cc;->a(Ljava/lang/String;J)Lcom/a/b/b/cc;

    .line 847
    :cond_39
    iget-wide v2, p0, Lcom/a/b/c/f;->n:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_59

    .line 848
    const-string v1, "expireAfterWrite"

    iget-wide v2, p0, Lcom/a/b/c/f;->n:J

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 10185
    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 850
    :cond_59
    iget-wide v2, p0, Lcom/a/b/c/f;->o:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_79

    .line 851
    const-string v1, "expireAfterAccess"

    iget-wide v2, p0, Lcom/a/b/c/f;->o:J

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 11185
    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 853
    :cond_79
    iget-object v1, p0, Lcom/a/b/c/f;->l:Lcom/a/b/c/bw;

    if-eqz v1, :cond_8c

    .line 854
    const-string v1, "keyStrength"

    iget-object v2, p0, Lcom/a/b/c/f;->l:Lcom/a/b/c/bw;

    invoke-virtual {v2}, Lcom/a/b/c/bw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 12185
    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 856
    :cond_8c
    iget-object v1, p0, Lcom/a/b/c/f;->m:Lcom/a/b/c/bw;

    if-eqz v1, :cond_9f

    .line 857
    const-string v1, "valueStrength"

    iget-object v2, p0, Lcom/a/b/c/f;->m:Lcom/a/b/c/bw;

    invoke-virtual {v2}, Lcom/a/b/c/bw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 13185
    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 859
    :cond_9f
    iget-object v1, p0, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    if-eqz v1, :cond_a8

    .line 860
    const-string v1, "keyEquivalence"

    .line 13257
    invoke-virtual {v0, v1}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 862
    :cond_a8
    iget-object v1, p0, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    if-eqz v1, :cond_b1

    .line 863
    const-string v1, "valueEquivalence"

    .line 14257
    invoke-virtual {v0, v1}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 865
    :cond_b1
    iget-object v1, p0, Lcom/a/b/c/f;->s:Lcom/a/b/c/dg;

    if-eqz v1, :cond_ba

    .line 866
    const-string v1, "removalListener"

    .line 15257
    invoke-virtual {v0, v1}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    .line 868
    :cond_ba
    invoke-virtual {v0}, Lcom/a/b/b/cc;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
