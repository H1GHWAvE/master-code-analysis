.class final Lcom/a/b/c/cz;
.super Lcom/a/b/c/dl;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/cu;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final g:J = 0x6499de12a37d0a3dL


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/a/b/c/dl;-><init>()V

    .line 62
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4

    .prologue
    .line 204
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 205
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/c/cz;->f:I

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/c/cz;->d:[Lcom/a/b/c/dn;

    .line 207
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/c/cz;->e:J

    .line 208
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 4

    .prologue
    .line 198
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 199
    invoke-virtual {p0}, Lcom/a/b/c/cz;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 200
    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 92
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/c/cz;->a(J)V

    .line 93
    return-void
.end method

.method private f()V
    .registers 7

    .prologue
    const-wide/16 v4, 0x0

    .line 126
    .line 1280
    iget-object v1, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    .line 1281
    iput-wide v4, p0, Lcom/a/b/c/dl;->e:J

    .line 1282
    if-eqz v1, :cond_15

    .line 1283
    array-length v2, v1

    .line 1284
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_15

    .line 1285
    aget-object v3, v1, v0

    .line 1286
    if-eqz v3, :cond_12

    .line 1287
    iput-wide v4, v3, Lcom/a/b/c/dn;->h:J

    .line 1284
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 127
    :cond_15
    return-void
.end method

.method private g()J
    .registers 11

    .prologue
    const-wide/16 v8, 0x0

    .line 140
    iget-wide v0, p0, Lcom/a/b/c/cz;->e:J

    .line 141
    iget-object v3, p0, Lcom/a/b/c/cz;->d:[Lcom/a/b/c/dn;

    .line 142
    iput-wide v8, p0, Lcom/a/b/c/cz;->e:J

    .line 143
    if-eqz v3, :cond_1a

    .line 144
    array-length v4, v3

    .line 145
    const/4 v2, 0x0

    :goto_c
    if-ge v2, v4, :cond_1a

    .line 146
    aget-object v5, v3, v2

    .line 147
    if-eqz v5, :cond_17

    .line 148
    iget-wide v6, v5, Lcom/a/b/c/dn;->h:J

    add-long/2addr v0, v6

    .line 149
    iput-wide v8, v5, Lcom/a/b/c/dn;->h:J

    .line 145
    :cond_17
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 153
    :cond_1a
    return-wide v0
.end method


# virtual methods
.method final a(JJ)J
    .registers 8

    .prologue
    .line 56
    add-long v0, p1, p3

    return-wide v0
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 85
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/c/cz;->a(J)V

    .line 86
    return-void
.end method

.method public final a(J)V
    .registers 16

    .prologue
    .line 71
    iget-object v1, p0, Lcom/a/b/c/cz;->d:[Lcom/a/b/c/dn;

    if-nez v1, :cond_e

    iget-wide v2, p0, Lcom/a/b/c/cz;->e:J

    add-long v4, v2, p1

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/a/b/c/cz;->b(JJ)Z

    move-result v0

    if-nez v0, :cond_83

    .line 72
    :cond_e
    const/4 v3, 0x1

    .line 73
    sget-object v0, Lcom/a/b/c/cz;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    if-eqz v0, :cond_32

    if-eqz v1, :cond_32

    array-length v2, v1

    if-lez v2, :cond_32

    add-int/lit8 v2, v2, -0x1

    const/4 v4, 0x0

    aget v4, v0, v4

    and-int/2addr v2, v4

    aget-object v1, v1, v2

    if-eqz v1, :cond_32

    iget-wide v2, v1, Lcom/a/b/c/dn;->h:J

    add-long v4, v2, p1

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/a/b/c/dn;->a(JJ)Z

    move-result v3

    if-nez v3, :cond_83

    .line 1195
    :cond_32
    if-nez v0, :cond_84

    .line 1196
    sget-object v0, Lcom/a/b/c/dl;->a:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    new-array v1, v1, [I

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 1197
    sget-object v0, Lcom/a/b/c/dl;->b:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 1198
    const/4 v2, 0x0

    if-nez v0, :cond_46

    const/4 v0, 0x1

    :cond_46
    aput v0, v1, v2

    .line 1202
    :goto_48
    const/4 v2, 0x0

    move v4, v3

    move v3, v0

    .line 1205
    :cond_4b
    :goto_4b
    iget-object v5, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-eqz v5, :cond_ec

    array-length v6, v5

    if-lez v6, :cond_ec

    .line 1206
    add-int/lit8 v0, v6, -0x1

    and-int/2addr v0, v3

    aget-object v0, v5, v0

    if-nez v0, :cond_a1

    .line 1207
    iget v0, p0, Lcom/a/b/c/dl;->f:I

    if-nez v0, :cond_90

    .line 1208
    new-instance v5, Lcom/a/b/c/dn;

    invoke-direct {v5, p1, p2}, Lcom/a/b/c/dn;-><init>(J)V

    .line 1209
    iget v0, p0, Lcom/a/b/c/dl;->f:I

    if-nez v0, :cond_90

    invoke-virtual {p0}, Lcom/a/b/c/dl;->c()Z

    move-result v0

    if-eqz v0, :cond_90

    .line 1210
    const/4 v0, 0x0

    .line 1213
    :try_start_6d
    iget-object v6, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-eqz v6, :cond_7e

    array-length v7, v6

    if-lez v7, :cond_7e

    add-int/lit8 v7, v7, -0x1

    and-int/2addr v7, v3

    aget-object v8, v6, v7

    if-nez v8, :cond_7e

    .line 1216
    aput-object v5, v6, v7
    :try_end_7d
    .catchall {:try_start_6d .. :try_end_7d} :catchall_8b

    .line 1217
    const/4 v0, 0x1

    .line 1220
    :cond_7e
    const/4 v5, 0x0

    iput v5, p0, Lcom/a/b/c/dl;->f:I

    .line 1222
    if-eqz v0, :cond_4b

    .line 1223
    :cond_83
    :goto_83
    return-void

    .line 1201
    :cond_84
    const/4 v1, 0x0

    aget v1, v0, v1

    move v12, v1

    move-object v1, v0

    move v0, v12

    goto :goto_48

    .line 1220
    :catchall_8b
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, Lcom/a/b/c/dl;->f:I

    throw v0

    .line 1227
    :cond_90
    const/4 v2, 0x0

    move v0, v2

    .line 1251
    :goto_92
    shl-int/lit8 v2, v3, 0xd

    xor-int/2addr v2, v3

    .line 1252
    ushr-int/lit8 v3, v2, 0x11

    xor-int/2addr v2, v3

    .line 1253
    shl-int/lit8 v3, v2, 0x5

    xor-int/2addr v2, v3

    .line 1254
    const/4 v3, 0x0

    aput v2, v1, v3

    move v3, v2

    move v2, v0

    goto :goto_4b

    .line 1229
    :cond_a1
    if-nez v4, :cond_a6

    .line 1230
    const/4 v4, 0x1

    move v0, v2

    goto :goto_92

    .line 1231
    :cond_a6
    iget-wide v8, v0, Lcom/a/b/c/dn;->h:J

    invoke-virtual {p0, v8, v9, p1, p2}, Lcom/a/b/c/dl;->a(JJ)J

    move-result-wide v10

    invoke-virtual {v0, v8, v9, v10, v11}, Lcom/a/b/c/dn;->a(JJ)Z

    move-result v0

    if-nez v0, :cond_83

    .line 1233
    sget v0, Lcom/a/b/c/dl;->c:I

    if-ge v6, v0, :cond_ba

    iget-object v0, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-eq v0, v5, :cond_bd

    .line 1234
    :cond_ba
    const/4 v2, 0x0

    move v0, v2

    goto :goto_92

    .line 1235
    :cond_bd
    if-nez v2, :cond_c2

    .line 1236
    const/4 v2, 0x1

    move v0, v2

    goto :goto_92

    .line 1237
    :cond_c2
    iget v0, p0, Lcom/a/b/c/dl;->f:I

    if-nez v0, :cond_128

    invoke-virtual {p0}, Lcom/a/b/c/dl;->c()Z

    move-result v0

    if-eqz v0, :cond_128

    .line 1239
    :try_start_cc
    iget-object v0, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-ne v0, v5, :cond_e0

    .line 1240
    shl-int/lit8 v0, v6, 0x1

    new-array v2, v0, [Lcom/a/b/c/dn;

    .line 1241
    const/4 v0, 0x0

    :goto_d5
    if-ge v0, v6, :cond_de

    .line 1242
    aget-object v7, v5, v0

    aput-object v7, v2, v0

    .line 1241
    add-int/lit8 v0, v0, 0x1

    goto :goto_d5

    .line 1243
    :cond_de
    iput-object v2, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;
    :try_end_e0
    .catchall {:try_start_cc .. :try_end_e0} :catchall_e7

    .line 1246
    :cond_e0
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/c/dl;->f:I

    .line 1248
    const/4 v0, 0x0

    move v2, v0

    .line 1249
    goto/16 :goto_4b

    .line 1246
    :catchall_e7
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, Lcom/a/b/c/dl;->f:I

    throw v0

    .line 1256
    :cond_ec
    iget v0, p0, Lcom/a/b/c/dl;->f:I

    if-nez v0, :cond_11a

    iget-object v0, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-ne v0, v5, :cond_11a

    invoke-virtual {p0}, Lcom/a/b/c/dl;->c()Z

    move-result v0

    if-eqz v0, :cond_11a

    .line 1257
    const/4 v0, 0x0

    .line 1259
    :try_start_fb
    iget-object v6, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;

    if-ne v6, v5, :cond_10e

    .line 1260
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/c/dn;

    .line 1261
    and-int/lit8 v5, v3, 0x1

    new-instance v6, Lcom/a/b/c/dn;

    invoke-direct {v6, p1, p2}, Lcom/a/b/c/dn;-><init>(J)V

    aput-object v6, v0, v5

    .line 1262
    iput-object v0, p0, Lcom/a/b/c/dl;->d:[Lcom/a/b/c/dn;
    :try_end_10d
    .catchall {:try_start_fb .. :try_end_10d} :catchall_115

    .line 1263
    const/4 v0, 0x1

    .line 1266
    :cond_10e
    const/4 v5, 0x0

    iput v5, p0, Lcom/a/b/c/dl;->f:I

    .line 1268
    if-nez v0, :cond_83

    goto/16 :goto_4b

    .line 1266
    :catchall_115
    move-exception v0

    const/4 v1, 0x0

    iput v1, p0, Lcom/a/b/c/dl;->f:I

    throw v0

    .line 1271
    :cond_11a
    iget-wide v6, p0, Lcom/a/b/c/dl;->e:J

    invoke-virtual {p0, v6, v7, p1, p2}, Lcom/a/b/c/dl;->a(JJ)J

    move-result-wide v8

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/a/b/c/dl;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_4b

    goto/16 :goto_83

    :cond_128
    move v0, v2

    goto/16 :goto_92
.end method

.method public final b()J
    .registers 9

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/a/b/c/cz;->e:J

    .line 106
    iget-object v3, p0, Lcom/a/b/c/cz;->d:[Lcom/a/b/c/dn;

    .line 107
    if-eqz v3, :cond_14

    .line 108
    array-length v4, v3

    .line 109
    const/4 v2, 0x0

    :goto_8
    if-ge v2, v4, :cond_14

    .line 110
    aget-object v5, v3, v2

    .line 111
    if-eqz v5, :cond_11

    .line 112
    iget-wide v6, v5, Lcom/a/b/c/dn;->h:J

    add-long/2addr v0, v6

    .line 109
    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 115
    :cond_14
    return-wide v0
.end method

.method public final doubleValue()D
    .registers 3

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/a/b/c/cz;->b()J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method public final floatValue()F
    .registers 3

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/a/b/c/cz;->b()J

    move-result-wide v0

    long-to-float v0, v0

    return v0
.end method

.method public final intValue()I
    .registers 3

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/a/b/c/cz;->b()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final longValue()J
    .registers 3

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/a/b/c/cz;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/a/b/c/cz;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
