.class final Lcom/a/b/c/bk;
.super Lcom/a/b/c/bq;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/an;
.implements Ljava/io/Serializable;


# static fields
.field private static final o:J = 0x1L


# instance fields
.field transient a:Lcom/a/b/c/an;


# direct methods
.method constructor <init>(Lcom/a/b/c/ao;)V
    .registers 2

    .prologue
    .line 4678
    invoke-direct {p0, p1}, Lcom/a/b/c/bq;-><init>(Lcom/a/b/c/ao;)V

    .line 4679
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4

    .prologue
    .line 4682
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 4683
    invoke-virtual {p0}, Lcom/a/b/c/bk;->h()Lcom/a/b/c/f;

    move-result-object v0

    .line 4684
    iget-object v1, p0, Lcom/a/b/c/bk;->m:Lcom/a/b/c/ab;

    invoke-virtual {v0, v1}, Lcom/a/b/c/f;->a(Lcom/a/b/c/ab;)Lcom/a/b/c/an;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/c/bk;->a:Lcom/a/b/c/an;

    .line 4685
    return-void
.end method

.method private i()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 4713
    iget-object v0, p0, Lcom/a/b/c/bk;->a:Lcom/a/b/c/an;

    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 4694
    iget-object v0, p0, Lcom/a/b/c/bk;->a:Lcom/a/b/c/an;

    invoke-interface {v0, p1}, Lcom/a/b/c/an;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Iterable;)Lcom/a/b/d/jt;
    .registers 3

    .prologue
    .line 4699
    iget-object v0, p0, Lcom/a/b/c/bk;->a:Lcom/a/b/c/an;

    invoke-interface {v0, p1}, Lcom/a/b/c/an;->c(Ljava/lang/Iterable;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 4709
    iget-object v0, p0, Lcom/a/b/c/bk;->a:Lcom/a/b/c/an;

    invoke-interface {v0, p1}, Lcom/a/b/c/an;->c(Ljava/lang/Object;)V

    .line 4710
    return-void
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 4704
    iget-object v0, p0, Lcom/a/b/c/bk;->a:Lcom/a/b/c/an;

    invoke-interface {v0, p1}, Lcom/a/b/c/an;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 4689
    iget-object v0, p0, Lcom/a/b/c/bk;->a:Lcom/a/b/c/an;

    invoke-interface {v0, p1}, Lcom/a/b/c/an;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
