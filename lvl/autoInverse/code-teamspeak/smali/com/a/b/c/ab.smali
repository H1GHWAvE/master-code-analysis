.class public abstract Lcom/a/b/c/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/b/bj;)Lcom/a/b/c/ab;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 138
    new-instance v0, Lcom/a/b/c/ae;

    invoke-direct {v0, p0}, Lcom/a/b/c/ae;-><init>(Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/b/dz;)Lcom/a/b/c/ab;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 168
    new-instance v0, Lcom/a/b/c/ag;

    invoke-direct {v0, p0}, Lcom/a/b/c/ag;-><init>(Lcom/a/b/b/dz;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/c/ab;Ljava/util/concurrent/Executor;)Lcom/a/b/c/ab;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/c;
        a = "Executor + Futures"
    .end annotation

    .prologue
    .line 184
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    new-instance v0, Lcom/a/b/c/ac;

    invoke-direct {v0, p0, p1}, Lcom/a/b/c/ac;-><init>(Lcom/a/b/c/ab;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/n/a/dp;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "Futures"
    .end annotation

    .prologue
    .line 95
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    invoke-virtual {p0, p1}, Lcom/a/b/c/ab;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/n/a/ci;->a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public a(Ljava/lang/Iterable;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 125
    new-instance v0, Lcom/a/b/c/ah;

    invoke-direct {v0}, Lcom/a/b/c/ah;-><init>()V

    throw v0
.end method
