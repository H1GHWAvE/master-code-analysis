.class abstract enum Lcom/a/b/c/aw;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/c/aw;

.field public static final enum b:Lcom/a/b/c/aw;

.field public static final enum c:Lcom/a/b/c/aw;

.field public static final enum d:Lcom/a/b/c/aw;

.field public static final enum e:Lcom/a/b/c/aw;

.field public static final enum f:Lcom/a/b/c/aw;

.field public static final enum g:Lcom/a/b/c/aw;

.field public static final enum h:Lcom/a/b/c/aw;

.field static final i:I = 0x1

.field static final j:I = 0x2

.field static final k:I = 0x4

.field static final l:[Lcom/a/b/c/aw;

.field private static final synthetic m:[Lcom/a/b/c/aw;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 443
    new-instance v0, Lcom/a/b/c/ax;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lcom/a/b/c/ax;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/aw;->a:Lcom/a/b/c/aw;

    .line 450
    new-instance v0, Lcom/a/b/c/ay;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1}, Lcom/a/b/c/ay;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/aw;->b:Lcom/a/b/c/aw;

    .line 465
    new-instance v0, Lcom/a/b/c/az;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1}, Lcom/a/b/c/az;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/aw;->c:Lcom/a/b/c/aw;

    .line 480
    new-instance v0, Lcom/a/b/c/ba;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1}, Lcom/a/b/c/ba;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/aw;->d:Lcom/a/b/c/aw;

    .line 497
    new-instance v0, Lcom/a/b/c/bb;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lcom/a/b/c/bb;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/aw;->e:Lcom/a/b/c/aw;

    .line 504
    new-instance v0, Lcom/a/b/c/bc;

    const-string v1, "WEAK_ACCESS"

    invoke-direct {v0, v1}, Lcom/a/b/c/bc;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/aw;->f:Lcom/a/b/c/aw;

    .line 519
    new-instance v0, Lcom/a/b/c/bd;

    const-string v1, "WEAK_WRITE"

    invoke-direct {v0, v1}, Lcom/a/b/c/bd;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/aw;->g:Lcom/a/b/c/aw;

    .line 534
    new-instance v0, Lcom/a/b/c/be;

    const-string v1, "WEAK_ACCESS_WRITE"

    invoke-direct {v0, v1}, Lcom/a/b/c/be;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/aw;->h:Lcom/a/b/c/aw;

    .line 442
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/a/b/c/aw;

    sget-object v1, Lcom/a/b/c/aw;->a:Lcom/a/b/c/aw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/a/b/c/aw;->b:Lcom/a/b/c/aw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/a/b/c/aw;->c:Lcom/a/b/c/aw;

    aput-object v1, v0, v5

    sget-object v1, Lcom/a/b/c/aw;->d:Lcom/a/b/c/aw;

    aput-object v1, v0, v6

    sget-object v1, Lcom/a/b/c/aw;->e:Lcom/a/b/c/aw;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/a/b/c/aw;->f:Lcom/a/b/c/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/a/b/c/aw;->g:Lcom/a/b/c/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/a/b/c/aw;->h:Lcom/a/b/c/aw;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/c/aw;->m:[Lcom/a/b/c/aw;

    .line 561
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/a/b/c/aw;

    sget-object v1, Lcom/a/b/c/aw;->a:Lcom/a/b/c/aw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/a/b/c/aw;->b:Lcom/a/b/c/aw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/a/b/c/aw;->c:Lcom/a/b/c/aw;

    aput-object v1, v0, v5

    sget-object v1, Lcom/a/b/c/aw;->d:Lcom/a/b/c/aw;

    aput-object v1, v0, v6

    sget-object v1, Lcom/a/b/c/aw;->e:Lcom/a/b/c/aw;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/a/b/c/aw;->f:Lcom/a/b/c/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/a/b/c/aw;->g:Lcom/a/b/c/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/a/b/c/aw;->h:Lcom/a/b/c/aw;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/c/aw;->l:[Lcom/a/b/c/aw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 442
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 442
    invoke-direct {p0, p1, p2}, Lcom/a/b/c/aw;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lcom/a/b/c/bw;ZZ)Lcom/a/b/c/aw;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 568
    sget-object v1, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    if-ne p0, v1, :cond_14

    const/4 v1, 0x4

    move v2, v1

    :goto_7
    if-eqz p1, :cond_16

    const/4 v1, 0x1

    :goto_a
    or-int/2addr v1, v2

    if-eqz p2, :cond_e

    const/4 v0, 0x2

    :cond_e
    or-int/2addr v0, v1

    .line 571
    sget-object v1, Lcom/a/b/c/aw;->l:[Lcom/a/b/c/aw;

    aget-object v0, v1, v0

    return-object v0

    :cond_14
    move v2, v0

    .line 568
    goto :goto_7

    :cond_16
    move v1, v0

    goto :goto_a
.end method

.method static a(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
    .registers 4

    .prologue
    .line 601
    invoke-interface {p0}, Lcom/a/b/c/bs;->e()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/a/b/c/bs;->a(J)V

    .line 603
    invoke-interface {p0}, Lcom/a/b/c/bs;->g()Lcom/a/b/c/bs;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/c/ao;->a(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 604
    invoke-interface {p0}, Lcom/a/b/c/bs;->f()Lcom/a/b/c/bs;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/c/ao;->a(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 606
    invoke-static {p0}, Lcom/a/b/c/ao;->a(Lcom/a/b/c/bs;)V

    .line 607
    return-void
.end method

.method static b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
    .registers 4

    .prologue
    .line 613
    invoke-interface {p0}, Lcom/a/b/c/bs;->h()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/a/b/c/bs;->b(J)V

    .line 615
    invoke-interface {p0}, Lcom/a/b/c/bs;->j()Lcom/a/b/c/bs;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 616
    invoke-interface {p0}, Lcom/a/b/c/bs;->i()Lcom/a/b/c/bs;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V

    .line 618
    invoke-static {p0}, Lcom/a/b/c/ao;->b(Lcom/a/b/c/bs;)V

    .line 619
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/c/aw;
    .registers 2

    .prologue
    .line 442
    const-class v0, Lcom/a/b/c/aw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/aw;

    return-object v0
.end method

.method public static values()[Lcom/a/b/c/aw;
    .registers 1

    .prologue
    .line 442
    sget-object v0, Lcom/a/b/c/aw;->m:[Lcom/a/b/c/aw;

    invoke-virtual {v0}, [Lcom/a/b/c/aw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/c/aw;

    return-object v0
.end method


# virtual methods
.method a(Lcom/a/b/c/bt;Lcom/a/b/c/bs;Lcom/a/b/c/bs;)Lcom/a/b/c/bs;
    .registers 6

    .prologue
    .line 594
    invoke-interface {p2}, Lcom/a/b/c/bs;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/a/b/c/bs;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/a/b/c/aw;->a(Lcom/a/b/c/bt;Ljava/lang/Object;ILcom/a/b/c/bs;)Lcom/a/b/c/bs;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lcom/a/b/c/bt;Ljava/lang/Object;ILcom/a/b/c/bs;)Lcom/a/b/c/bs;
    .param p4    # Lcom/a/b/c/bs;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method
