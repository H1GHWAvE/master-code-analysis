.class abstract Lcom/a/b/g/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/g/ak;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    return-void
.end method


# virtual methods
.method public final a(J)Lcom/a/b/g/ag;
    .registers 4

    .prologue
    .line 66
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/a/b/g/e;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/g/al;->a(J)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/a/b/g/ag;
    .registers 6

    .prologue
    .line 49
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 50
    mul-int/lit8 v0, v1, 0x2

    invoke-virtual {p0, v0}, Lcom/a/b/g/e;->a(I)Lcom/a/b/g/al;

    move-result-object v2

    .line 51
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v1, :cond_17

    .line 52
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-interface {v2, v3}, Lcom/a/b/g/al;->a(C)Lcom/a/b/g/al;

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 54
    :cond_17
    invoke-interface {v2}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/ag;
    .registers 5

    .prologue
    .line 58
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 1070
    array-length v1, v0

    invoke-virtual {p0, v0, v1}, Lcom/a/b/g/e;->a([BI)Lcom/a/b/g/ag;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/ag;
    .registers 4

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/a/b/g/e;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/g/al;->a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a([B)Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 70
    array-length v0, p1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/g/e;->a([BI)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 35
    new-instance v0, Lcom/a/b/g/f;

    const/16 v1, 0x20

    invoke-direct {v0, p0, v1}, Lcom/a/b/g/f;-><init>(Lcom/a/b/g/e;I)V

    return-object v0
.end method

.method public final a(I)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 40
    if-ltz p1, :cond_c

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 41
    new-instance v0, Lcom/a/b/g/f;

    invoke-direct {v0, p0, p1}, Lcom/a/b/g/f;-><init>(Lcom/a/b/g/e;I)V

    return-object v0

    .line 40
    :cond_c
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final b(I)Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 62
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/a/b/g/e;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/g/al;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method
