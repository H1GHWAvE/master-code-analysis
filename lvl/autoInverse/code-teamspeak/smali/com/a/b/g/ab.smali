.class Lcom/a/b/g/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/g/w;
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Lcom/a/b/g/w;


# direct methods
.method constructor <init>(Lcom/a/b/g/w;)V
    .registers 3

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/w;

    iput-object v0, p0, Lcom/a/b/g/ab;->a:Lcom/a/b/g/w;

    .line 170
    return-void
.end method

.method private a(Ljava/lang/Iterable;Lcom/a/b/g/bn;)V
    .registers 6

    .prologue
    .line 173
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 174
    iget-object v2, p0, Lcom/a/b/g/ab;->a:Lcom/a/b/g/w;

    invoke-interface {v2, v1, p2}, Lcom/a/b/g/w;->a(Ljava/lang/Object;Lcom/a/b/g/bn;)V

    goto :goto_4

    .line 176
    :cond_14
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/a/b/g/bn;)V
    .registers 6

    .prologue
    .line 165
    check-cast p1, Ljava/lang/Iterable;

    .line 1173
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1174
    iget-object v2, p0, Lcom/a/b/g/ab;->a:Lcom/a/b/g/w;

    invoke-interface {v2, v1, p2}, Lcom/a/b/g/w;->a(Ljava/lang/Object;Lcom/a/b/g/bn;)V

    goto :goto_6

    .line 165
    :cond_16
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 183
    instance-of v0, p1, Lcom/a/b/g/ab;

    if-eqz v0, :cond_f

    .line 184
    check-cast p1, Lcom/a/b/g/ab;

    .line 185
    iget-object v0, p0, Lcom/a/b/g/ab;->a:Lcom/a/b/g/w;

    iget-object v1, p1, Lcom/a/b/g/ab;->a:Lcom/a/b/g/w;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 187
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 191
    const-class v0, Lcom/a/b/g/ab;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/g/ab;->a:Lcom/a/b/g/w;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 179
    iget-object v0, p0, Lcom/a/b/g/ab;->a:Lcom/a/b/g/w;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Funnels.sequentialFunnel("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
