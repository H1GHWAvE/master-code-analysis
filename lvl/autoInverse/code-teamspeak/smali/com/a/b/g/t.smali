.class final Lcom/a/b/g/t;
.super Lcom/a/b/g/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/g/r;

.field private final b:Ljava/util/zip/Checksum;


# direct methods
.method private constructor <init>(Lcom/a/b/g/r;Ljava/util/zip/Checksum;)V
    .registers 4

    .prologue
    .line 65
    iput-object p1, p0, Lcom/a/b/g/t;->a:Lcom/a/b/g/r;

    invoke-direct {p0}, Lcom/a/b/g/a;-><init>()V

    .line 66
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/Checksum;

    iput-object v0, p0, Lcom/a/b/g/t;->b:Ljava/util/zip/Checksum;

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/g/r;Ljava/util/zip/Checksum;B)V
    .registers 4

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/a/b/g/t;-><init>(Lcom/a/b/g/r;Ljava/util/zip/Checksum;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/g/ag;
    .registers 5

    .prologue
    .line 81
    iget-object v0, p0, Lcom/a/b/g/t;->b:Ljava/util/zip/Checksum;

    invoke-interface {v0}, Ljava/util/zip/Checksum;->getValue()J

    move-result-wide v0

    .line 82
    iget-object v2, p0, Lcom/a/b/g/t;->a:Lcom/a/b/g/r;

    invoke-static {v2}, Lcom/a/b/g/r;->a(Lcom/a/b/g/r;)I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_16

    .line 88
    long-to-int v0, v0

    invoke-static {v0}, Lcom/a/b/g/ag;->a(I)Lcom/a/b/g/ag;

    move-result-object v0

    .line 90
    :goto_15
    return-object v0

    :cond_16
    invoke-static {v0, v1}, Lcom/a/b/g/ag;->a(J)Lcom/a/b/g/ag;

    move-result-object v0

    goto :goto_15
.end method

.method protected final a(B)V
    .registers 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/a/b/g/t;->b:Ljava/util/zip/Checksum;

    invoke-interface {v0, p1}, Ljava/util/zip/Checksum;->update(I)V

    .line 72
    return-void
.end method

.method protected final a([BII)V
    .registers 5

    .prologue
    .line 76
    iget-object v0, p0, Lcom/a/b/g/t;->b:Ljava/util/zip/Checksum;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/zip/Checksum;->update([BII)V

    .line 77
    return-void
.end method
