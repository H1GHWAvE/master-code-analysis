.class final Lcom/a/b/g/aj;
.super Lcom/a/b/g/ag;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J


# instance fields
.field final a:J


# direct methods
.method constructor <init>(J)V
    .registers 4

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/a/b/g/ag;-><init>()V

    .line 187
    iput-wide p1, p0, Lcom/a/b/g/aj;->a:J

    .line 188
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 192
    const/16 v0, 0x40

    return v0
.end method

.method final a([BII)V
    .registers 9

    .prologue
    .line 225
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p3, :cond_11

    .line 226
    add-int v1, p2, v0

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    mul-int/lit8 v4, v0, 0x8

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 228
    :cond_11
    return-void
.end method

.method final a(Lcom/a/b/g/ag;)Z
    .registers 6

    .prologue
    .line 232
    iget-wide v0, p0, Lcom/a/b/g/aj;->a:J

    invoke-virtual {p1}, Lcom/a/b/g/ag;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final b()I
    .registers 3

    .prologue
    .line 210
    iget-wide v0, p0, Lcom/a/b/g/aj;->a:J

    long-to-int v0, v0

    return v0
.end method

.method public final c()J
    .registers 3

    .prologue
    .line 215
    iget-wide v0, p0, Lcom/a/b/g/aj;->a:J

    return-wide v0
.end method

.method public final d()J
    .registers 3

    .prologue
    .line 220
    iget-wide v0, p0, Lcom/a/b/g/aj;->a:J

    return-wide v0
.end method

.method public final e()[B
    .registers 6

    .prologue
    const/16 v4, 0x8

    .line 197
    new-array v0, v4, [B

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    const/16 v4, 0x10

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    const/16 v4, 0x18

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    const/16 v4, 0x28

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    const/16 v4, 0x30

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/a/b/g/aj;->a:J

    const/16 v4, 0x38

    shr-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    return-object v0
.end method
