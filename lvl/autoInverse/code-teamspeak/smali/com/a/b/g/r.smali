.class final Lcom/a/b/g/r;
.super Lcom/a/b/g/h;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final d:J


# instance fields
.field private final a:Lcom/a/b/b/dz;

.field private final b:I

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/a/b/b/dz;ILjava/lang/String;)V
    .registers 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Lcom/a/b/g/h;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/g/r;->a:Lcom/a/b/b/dz;

    .line 38
    const/16 v0, 0x20

    if-eq p2, v0, :cond_15

    const/16 v0, 0x40

    if-ne p2, v0, :cond_2e

    :cond_15
    move v0, v2

    :goto_16
    const-string v3, "bits (%s) must be either 32 or 64"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iput p2, p0, Lcom/a/b/g/r;->b:I

    .line 40
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/b/g/r;->c:Ljava/lang/String;

    .line 41
    return-void

    :cond_2e
    move v0, v1

    .line 38
    goto :goto_16
.end method

.method static synthetic a(Lcom/a/b/g/r;)I
    .registers 2

    .prologue
    .line 30
    iget v0, p0, Lcom/a/b/g/r;->b:I

    return v0
.end method


# virtual methods
.method public final a()Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 50
    new-instance v1, Lcom/a/b/g/t;

    iget-object v0, p0, Lcom/a/b/g/r;->a:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/Checksum;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/a/b/g/t;-><init>(Lcom/a/b/g/r;Ljava/util/zip/Checksum;B)V

    return-object v1
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 45
    iget v0, p0, Lcom/a/b/g/r;->b:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/a/b/g/r;->c:Ljava/lang/String;

    return-object v0
.end method
