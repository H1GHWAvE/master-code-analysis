.class final Lcom/a/b/b/ad;
.super Lcom/a/b/b/ae;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/c;
    a = "java.util.BitSet"
.end annotation


# instance fields
.field private final s:Ljava/util/BitSet;


# direct methods
.method private constructor <init>(Ljava/util/BitSet;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 889
    invoke-direct {p0, p2}, Lcom/a/b/b/ae;-><init>(Ljava/lang/String;)V

    .line 890
    invoke-virtual {p1}, Ljava/util/BitSet;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x40

    invoke-virtual {p1}, Ljava/util/BitSet;->size()I

    move-result v1

    if-ge v0, v1, :cond_18

    .line 891
    invoke-virtual {p1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/BitSet;

    .line 894
    :goto_15
    iput-object v0, p0, Lcom/a/b/b/ad;->s:Ljava/util/BitSet;

    .line 895
    return-void

    :cond_18
    move-object v0, p1

    goto :goto_15
.end method

.method synthetic constructor <init>(Ljava/util/BitSet;Ljava/lang/String;B)V
    .registers 4

    .prologue
    .line 885
    invoke-direct {p0, p1, p2}, Lcom/a/b/b/ad;-><init>(Ljava/util/BitSet;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method final a(Ljava/util/BitSet;)V
    .registers 3

    .prologue
    .line 903
    iget-object v0, p0, Lcom/a/b/b/ad;->s:Ljava/util/BitSet;

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    .line 904
    return-void
.end method

.method public final c(C)Z
    .registers 3

    .prologue
    .line 898
    iget-object v0, p0, Lcom/a/b/b/ad;->s:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method
