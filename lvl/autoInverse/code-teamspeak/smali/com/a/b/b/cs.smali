.class final Lcom/a/b/b/cs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "Class.isAssignableFrom"
.end annotation


# static fields
.field private static final b:J


# instance fields
.field private final a:Ljava/lang/Class;


# direct methods
.method private constructor <init>(Ljava/lang/Class;)V
    .registers 3

    .prologue
    .line 479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 480
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/a/b/b/cs;->a:Ljava/lang/Class;

    .line 481
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Class;B)V
    .registers 3

    .prologue
    .line 475
    invoke-direct {p0, p1}, Lcom/a/b/b/cs;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private a(Ljava/lang/Class;)Z
    .registers 3

    .prologue
    .line 484
    iget-object v0, p0, Lcom/a/b/b/cs;->a:Ljava/lang/Class;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 474
    check-cast p1, Ljava/lang/Class;

    .line 1484
    iget-object v0, p0, Lcom/a/b/b/cs;->a:Ljava/lang/Class;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    .line 474
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 490
    instance-of v1, p1, Lcom/a/b/b/cs;

    if-eqz v1, :cond_e

    .line 491
    check-cast p1, Lcom/a/b/b/cs;

    .line 492
    iget-object v1, p0, Lcom/a/b/b/cs;->a:Ljava/lang/Class;

    iget-object v2, p1, Lcom/a/b/b/cs;->a:Ljava/lang/Class;

    if-ne v1, v2, :cond_e

    const/4 v0, 0x1

    .line 494
    :cond_e
    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/a/b/b/cs;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 497
    iget-object v0, p0, Lcom/a/b/b/cs;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Predicates.assignableFrom("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
