.class final Lcom/a/b/b/ac;
.super Lcom/a/b/b/m;
.source "SourceFile"


# instance fields
.field final s:Lcom/a/b/b/m;

.field final t:Lcom/a/b/b/m;


# direct methods
.method constructor <init>(Lcom/a/b/b/m;Lcom/a/b/b/m;)V
    .registers 8

    .prologue
    .line 685
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "CharMatcher.and("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/b/ac;-><init>(Lcom/a/b/b/m;Lcom/a/b/b/m;Ljava/lang/String;)V

    .line 686
    return-void
.end method

.method private constructor <init>(Lcom/a/b/b/m;Lcom/a/b/b/m;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 689
    invoke-direct {p0, p3}, Lcom/a/b/b/m;-><init>(Ljava/lang/String;)V

    .line 690
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/m;

    iput-object v0, p0, Lcom/a/b/b/ac;->s:Lcom/a/b/b/m;

    .line 691
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/m;

    iput-object v0, p0, Lcom/a/b/b/ac;->t:Lcom/a/b/b/m;

    .line 692
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lcom/a/b/b/m;
    .registers 5

    .prologue
    .line 712
    new-instance v0, Lcom/a/b/b/ac;

    iget-object v1, p0, Lcom/a/b/b/ac;->s:Lcom/a/b/b/m;

    iget-object v2, p0, Lcom/a/b/b/ac;->t:Lcom/a/b/b/m;

    invoke-direct {v0, v1, v2, p1}, Lcom/a/b/b/ac;-><init>(Lcom/a/b/b/m;Lcom/a/b/b/m;Ljava/lang/String;)V

    return-object v0
.end method

.method final a(Ljava/util/BitSet;)V
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.util.BitSet"
    .end annotation

    .prologue
    .line 702
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    .line 703
    iget-object v1, p0, Lcom/a/b/b/ac;->s:Lcom/a/b/b/m;

    invoke-virtual {v1, v0}, Lcom/a/b/b/m;->a(Ljava/util/BitSet;)V

    .line 704
    new-instance v1, Ljava/util/BitSet;

    invoke-direct {v1}, Ljava/util/BitSet;-><init>()V

    .line 705
    iget-object v2, p0, Lcom/a/b/b/ac;->t:Lcom/a/b/b/m;

    invoke-virtual {v2, v1}, Lcom/a/b/b/m;->a(Ljava/util/BitSet;)V

    .line 706
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->and(Ljava/util/BitSet;)V

    .line 707
    invoke-virtual {p1, v0}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    .line 708
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 680
    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, Lcom/a/b/b/m;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public final c(C)Z
    .registers 3

    .prologue
    .line 696
    iget-object v0, p0, Lcom/a/b/b/ac;->s:Lcom/a/b/b/m;

    invoke-virtual {v0, p1}, Lcom/a/b/b/m;->c(C)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/b/ac;->t:Lcom/a/b/b/m;

    invoke-virtual {v0, p1}, Lcom/a/b/b/m;->c(C)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
