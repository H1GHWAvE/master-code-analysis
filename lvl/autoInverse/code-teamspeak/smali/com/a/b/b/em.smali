.class public final Lcom/a/b/b/em;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 134
    const-string v1, "expected a non-null reference"

    new-array v2, v0, [Ljava/lang/Object;

    .line 1156
    if-eqz p0, :cond_8

    const/4 v0, 0x1

    .line 2122
    :cond_8
    if-nez v0, :cond_14

    .line 2123
    new-instance v0, Lcom/a/b/b/en;

    invoke-static {v1, v2}, Lcom/a/b/b/cn;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/b/en;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_14
    return-object p0
.end method

.method private static varargs a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # [Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 156
    if-eqz p0, :cond_f

    const/4 v0, 0x1

    .line 3122
    :goto_3
    if-nez v0, :cond_11

    .line 3123
    new-instance v0, Lcom/a/b/b/en;

    invoke-static {p1, p2}, Lcom/a/b/b/cn;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/b/en;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_f
    const/4 v0, 0x0

    goto :goto_3

    .line 157
    :cond_11
    return-object p0
.end method

.method private static a(Z)V
    .registers 2

    .prologue
    .line 97
    if-nez p0, :cond_8

    .line 98
    new-instance v0, Lcom/a/b/b/en;

    invoke-direct {v0}, Lcom/a/b/b/en;-><init>()V

    throw v0

    .line 100
    :cond_8
    return-void
.end method

.method private static varargs a(ZLjava/lang/String;[Ljava/lang/Object;)V
    .registers 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # [Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 122
    if-nez p0, :cond_c

    .line 123
    new-instance v0, Lcom/a/b/b/en;

    invoke-static {p1, p2}, Lcom/a/b/b/cn;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/b/en;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_c
    return-void
.end method
