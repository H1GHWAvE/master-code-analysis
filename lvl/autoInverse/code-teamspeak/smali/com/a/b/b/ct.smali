.class final Lcom/a/b/b/ct;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field final a:Lcom/a/b/b/co;

.field final b:Lcom/a/b/b/bj;


# direct methods
.method private constructor <init>(Lcom/a/b/b/co;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 546
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    iput-object v0, p0, Lcom/a/b/b/ct;->a:Lcom/a/b/b/co;

    .line 547
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/b/ct;->b:Lcom/a/b/b/bj;

    .line 548
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/b/co;Lcom/a/b/b/bj;B)V
    .registers 4

    .prologue
    .line 540
    invoke-direct {p0, p1, p2}, Lcom/a/b/b/ct;-><init>(Lcom/a/b/b/co;Lcom/a/b/b/bj;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 552
    iget-object v0, p0, Lcom/a/b/b/ct;->a:Lcom/a/b/b/co;

    iget-object v1, p0, Lcom/a/b/b/ct;->b:Lcom/a/b/b/bj;

    invoke-interface {v1, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 556
    instance-of v1, p1, Lcom/a/b/b/ct;

    if-eqz v1, :cond_1c

    .line 557
    check-cast p1, Lcom/a/b/b/ct;

    .line 558
    iget-object v1, p0, Lcom/a/b/b/ct;->b:Lcom/a/b/b/bj;

    iget-object v2, p1, Lcom/a/b/b/ct;->b:Lcom/a/b/b/bj;

    invoke-interface {v1, v2}, Lcom/a/b/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/a/b/b/ct;->a:Lcom/a/b/b/co;

    iget-object v2, p1, Lcom/a/b/b/ct;->a:Lcom/a/b/b/co;

    invoke-interface {v1, v2}, Lcom/a/b/b/co;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    .line 560
    :cond_1c
    return v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 564
    iget-object v0, p0, Lcom/a/b/b/ct;->b:Lcom/a/b/b/bj;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/b/ct;->a:Lcom/a/b/b/co;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 568
    iget-object v0, p0, Lcom/a/b/b/ct;->a:Lcom/a/b/b/co;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/b/ct;->b:Lcom/a/b/b/bj;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
