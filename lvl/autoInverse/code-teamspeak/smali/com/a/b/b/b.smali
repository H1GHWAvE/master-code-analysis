.class abstract Lcom/a/b/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .registers 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget v0, Lcom/a/b/b/d;->b:I

    iput v0, p0, Lcom/a/b/b/b;->a:I

    .line 34
    return-void
.end method

.method private c()Z
    .registers 3

    .prologue
    .line 63
    sget v0, Lcom/a/b/b/d;->d:I

    iput v0, p0, Lcom/a/b/b/b;->a:I

    .line 64
    invoke-virtual {p0}, Lcom/a/b/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/b/b;->b:Ljava/lang/Object;

    .line 65
    iget v0, p0, Lcom/a/b/b/b;->a:I

    sget v1, Lcom/a/b/b/d;->c:I

    if-eq v0, v1, :cond_16

    .line 66
    sget v0, Lcom/a/b/b/d;->a:I

    iput v0, p0, Lcom/a/b/b/b;->a:I

    .line 67
    const/4 v0, 0x1

    .line 69
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method protected final b()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 45
    sget v0, Lcom/a/b/b/d;->c:I

    iput v0, p0, Lcom/a/b/b/b;->a:I

    .line 46
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    iget v0, p0, Lcom/a/b/b/b;->a:I

    sget v3, Lcom/a/b/b/d;->d:I

    if-eq v0, v3, :cond_2d

    move v0, v1

    :goto_9
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 52
    sget-object v0, Lcom/a/b/b/c;->a:[I

    iget v3, p0, Lcom/a/b/b/b;->a:I

    add-int/lit8 v3, v3, -0x1

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_32

    .line 1063
    sget v0, Lcom/a/b/b/d;->d:I

    iput v0, p0, Lcom/a/b/b/b;->a:I

    .line 1064
    invoke-virtual {p0}, Lcom/a/b/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/b/b;->b:Ljava/lang/Object;

    .line 1065
    iget v0, p0, Lcom/a/b/b/b;->a:I

    sget v3, Lcom/a/b/b/d;->c:I

    if-eq v0, v3, :cond_2c

    .line 1066
    sget v0, Lcom/a/b/b/d;->a:I

    iput v0, p0, Lcom/a/b/b/b;->a:I

    move v2, v1

    .line 1067
    :cond_2c
    :goto_2c
    :pswitch_2c
    return v2

    :cond_2d
    move v0, v2

    .line 51
    goto :goto_9

    :pswitch_2f
    move v2, v1

    .line 56
    goto :goto_2c

    .line 52
    nop

    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_2c
        :pswitch_2f
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/a/b/b/b;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 75
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 77
    :cond_c
    sget v0, Lcom/a/b/b/d;->b:I

    iput v0, p0, Lcom/a/b/b/b;->a:I

    .line 78
    iget-object v0, p0, Lcom/a/b/b/b;->b:Ljava/lang/Object;

    .line 79
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/b/b;->b:Ljava/lang/Object;

    .line 80
    return-object v0
.end method

.method public final remove()V
    .registers 2

    .prologue
    .line 84
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
