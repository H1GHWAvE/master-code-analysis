.class final Lcom/a/b/b/cz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J


# instance fields
.field final a:Lcom/a/b/b/co;


# direct methods
.method constructor <init>(Lcom/a/b/b/co;)V
    .registers 3

    .prologue
    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 324
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    iput-object v0, p0, Lcom/a/b/b/cz;->a:Lcom/a/b/b/co;

    .line 325
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 328
    iget-object v0, p0, Lcom/a/b/b/cz;->a:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 334
    instance-of v0, p1, Lcom/a/b/b/cz;

    if-eqz v0, :cond_f

    .line 335
    check-cast p1, Lcom/a/b/b/cz;

    .line 336
    iget-object v0, p0, Lcom/a/b/b/cz;->a:Lcom/a/b/b/co;

    iget-object v1, p1, Lcom/a/b/b/cz;->a:Lcom/a/b/b/co;

    invoke-interface {v0, v1}, Lcom/a/b/b/co;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 338
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/a/b/b/cz;->a:Lcom/a/b/b/co;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 341
    iget-object v0, p0, Lcom/a/b/b/cz;->a:Lcom/a/b/b/co;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Predicates.not("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
