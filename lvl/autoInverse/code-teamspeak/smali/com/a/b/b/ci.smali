.class public abstract Lcom/a/b/b/ci;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field private static final a:J


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 218
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    new-instance v0, Lcom/a/b/b/cj;

    invoke-direct {v0, p0}, Lcom/a/b/b/cj;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)Lcom/a/b/b/ci;
    .registers 3

    .prologue
    .line 85
    new-instance v0, Lcom/a/b/b/dg;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/b/dg;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static c(Ljava/lang/Object;)Lcom/a/b/b/ci;
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 93
    if-nez p0, :cond_7

    .line 1078
    invoke-static {}, Lcom/a/b/b/a;->a()Lcom/a/b/b/ci;

    move-result-object v0

    .line 93
    :goto_6
    return-object v0

    :cond_7
    new-instance v0, Lcom/a/b/b/dg;

    invoke-direct {v0, p0}, Lcom/a/b/b/dg;-><init>(Ljava/lang/Object;)V

    goto :goto_6
.end method

.method public static f()Lcom/a/b/b/ci;
    .registers 1

    .prologue
    .line 78
    invoke-static {}, Lcom/a/b/b/a;->a()Lcom/a/b/b/ci;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/a/b/b/bj;)Lcom/a/b/b/ci;
.end method

.method public abstract a(Lcom/a/b/b/ci;)Lcom/a/b/b/ci;
.end method

.method public abstract a(Lcom/a/b/b/dz;)Ljava/lang/Object;
    .annotation build Lcom/a/b/a/a;
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract b()Z
.end method

.method public abstract c()Ljava/lang/Object;
.end method

.method public abstract d()Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()Ljava/util/Set;
.end method

.method public abstract equals(Ljava/lang/Object;)Z
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract hashCode()I
.end method

.method public abstract toString()Ljava/lang/String;
.end method
