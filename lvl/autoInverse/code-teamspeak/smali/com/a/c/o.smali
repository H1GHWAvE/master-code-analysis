.class final Lcom/a/c/o;
.super Lcom/a/c/an;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/c/k;


# direct methods
.method constructor <init>(Lcom/a/c/k;)V
    .registers 2

    .prologue
    .line 276
    iput-object p1, p0, Lcom/a/c/o;->a:Lcom/a/c/k;

    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/lang/Number;)V
    .registers 4

    .prologue
    .line 285
    if-nez p1, :cond_6

    .line 286
    invoke-virtual {p0}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 292
    :goto_5
    return-void

    .line 289
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 290
    float-to-double v0, v0

    invoke-static {v0, v1}, Lcom/a/c/k;->a(D)V

    .line 291
    invoke-virtual {p0, p1}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    goto :goto_5
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/lang/Float;
    .registers 3

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 279
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 280
    const/4 v0, 0x0

    .line 282
    :goto_c
    return-object v0

    :cond_d
    invoke-virtual {p0}, Lcom/a/c/d/a;->l()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_c
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 276
    .line 1278
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 1279
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1280
    const/4 v0, 0x0

    :goto_c
    return-object v0

    .line 1282
    :cond_d
    invoke-virtual {p1}, Lcom/a/c/d/a;->l()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_c
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 276
    check-cast p2, Ljava/lang/Number;

    .line 1285
    if-nez p2, :cond_8

    .line 1286
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 1287
    :goto_7
    return-void

    .line 1289
    :cond_8
    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 1290
    float-to-double v0, v0

    invoke-static {v0, v1}, Lcom/a/c/k;->a(D)V

    .line 1291
    invoke-virtual {p1, p2}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    goto :goto_7
.end method
