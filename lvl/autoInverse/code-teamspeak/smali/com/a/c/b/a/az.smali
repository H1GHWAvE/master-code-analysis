.class final Lcom/a/c/b/a/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/c/ap;


# instance fields
.field final synthetic a:Ljava/lang/Class;

.field final synthetic b:Lcom/a/c/an;


# direct methods
.method constructor <init>(Ljava/lang/Class;Lcom/a/c/an;)V
    .registers 3

    .prologue
    .line 822
    iput-object p1, p0, Lcom/a/c/b/a/az;->a:Ljava/lang/Class;

    iput-object p2, p0, Lcom/a/c/b/a/az;->b:Lcom/a/c/an;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 5

    .prologue
    .line 825
    iget-object v0, p0, Lcom/a/c/b/a/az;->a:Ljava/lang/Class;

    .line 1094
    iget-object v1, p2, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 825
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/a/c/b/a/az;->b:Lcom/a/c/an;

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 828
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Factory[typeHierarchy="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/a/c/b/a/az;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",adapter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/a/c/b/a/az;->b:Lcom/a/c/an;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
