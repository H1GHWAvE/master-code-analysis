.class final Lcom/a/c/b/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/a/c/b/af;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Lcom/a/c/b/af;
    .registers 3

    .prologue
    .line 752
    iget-object v0, p0, Lcom/a/c/b/y;->a:Lcom/a/c/b/af;

    .line 753
    iget-object v1, v0, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    if-eqz v1, :cond_c

    .line 754
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 756
    :cond_c
    return-object v0
.end method

.method final a(I)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 679
    invoke-static {p1}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    .line 680
    sub-int/2addr v0, p1

    iput v0, p0, Lcom/a/c/b/y;->b:I

    .line 681
    iput v1, p0, Lcom/a/c/b/y;->d:I

    .line 682
    iput v1, p0, Lcom/a/c/b/y;->c:I

    .line 683
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/b/y;->a:Lcom/a/c/b/af;

    .line 684
    return-void
.end method

.method final a(Lcom/a/c/b/af;)V
    .registers 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 687
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    iput-object v0, p1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    iput-object v0, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 688
    iput v6, p1, Lcom/a/c/b/af;->i:I

    .line 691
    iget v0, p0, Lcom/a/c/b/y;->b:I

    if-lez v0, :cond_27

    iget v0, p0, Lcom/a/c/b/y;->d:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_27

    .line 692
    iget v0, p0, Lcom/a/c/b/y;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/b/y;->d:I

    .line 693
    iget v0, p0, Lcom/a/c/b/y;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/b/y;->b:I

    .line 694
    iget v0, p0, Lcom/a/c/b/y;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/b/y;->c:I

    .line 697
    :cond_27
    iget-object v0, p0, Lcom/a/c/b/y;->a:Lcom/a/c/b/af;

    iput-object v0, p1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 698
    iput-object p1, p0, Lcom/a/c/b/y;->a:Lcom/a/c/b/af;

    .line 699
    iget v0, p0, Lcom/a/c/b/y;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/b/y;->d:I

    .line 702
    iget v0, p0, Lcom/a/c/b/y;->b:I

    if-lez v0, :cond_4f

    iget v0, p0, Lcom/a/c/b/y;->d:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_4f

    .line 703
    iget v0, p0, Lcom/a/c/b/y;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/b/y;->d:I

    .line 704
    iget v0, p0, Lcom/a/c/b/y;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/b/y;->b:I

    .line 705
    iget v0, p0, Lcom/a/c/b/y;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/b/y;->c:I

    .line 721
    :cond_4f
    const/4 v0, 0x4

    :goto_50
    iget v1, p0, Lcom/a/c/b/y;->d:I

    add-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    add-int/lit8 v2, v0, -0x1

    if-ne v1, v2, :cond_99

    .line 722
    iget v1, p0, Lcom/a/c/b/y;->c:I

    if-nez v1, :cond_7a

    .line 724
    iget-object v1, p0, Lcom/a/c/b/y;->a:Lcom/a/c/b/af;

    .line 725
    iget-object v2, v1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 726
    iget-object v3, v2, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 727
    iget-object v4, v3, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    iput-object v4, v2, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 728
    iput-object v2, p0, Lcom/a/c/b/y;->a:Lcom/a/c/b/af;

    .line 730
    iput-object v3, v2, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 731
    iput-object v1, v2, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 732
    iget v4, v1, Lcom/a/c/b/af;->i:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/a/c/b/af;->i:I

    .line 733
    iput-object v2, v3, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 734
    iput-object v2, v1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 721
    :cond_77
    :goto_77
    mul-int/lit8 v0, v0, 0x2

    goto :goto_50

    .line 735
    :cond_7a
    iget v1, p0, Lcom/a/c/b/y;->c:I

    if-ne v1, v6, :cond_91

    .line 737
    iget-object v1, p0, Lcom/a/c/b/y;->a:Lcom/a/c/b/af;

    .line 738
    iget-object v2, v1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 739
    iput-object v2, p0, Lcom/a/c/b/y;->a:Lcom/a/c/b/af;

    .line 741
    iput-object v1, v2, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 742
    iget v3, v1, Lcom/a/c/b/af;->i:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/a/c/b/af;->i:I

    .line 743
    iput-object v2, v1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 744
    iput v5, p0, Lcom/a/c/b/y;->c:I

    goto :goto_77

    .line 745
    :cond_91
    iget v1, p0, Lcom/a/c/b/y;->c:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_77

    .line 746
    iput v5, p0, Lcom/a/c/b/y;->c:I

    goto :goto_77

    .line 749
    :cond_99
    return-void
.end method
