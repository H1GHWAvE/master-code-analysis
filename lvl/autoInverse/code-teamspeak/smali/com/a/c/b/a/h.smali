.class public final Lcom/a/c/b/a/h;
.super Lcom/a/c/d/a;
.source "SourceFile"


# static fields
.field private static final c:Ljava/io/Reader;

.field private static final d:Ljava/lang/Object;


# instance fields
.field public final a:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 40
    new-instance v0, Lcom/a/c/b/a/i;

    invoke-direct {v0}, Lcom/a/c/b/a/i;-><init>()V

    sput-object v0, Lcom/a/c/b/a/h;->c:Ljava/io/Reader;

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/a/c/b/a/h;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/a/c/w;)V
    .registers 3

    .prologue
    .line 53
    sget-object v0, Lcom/a/c/b/a/h;->c:Ljava/io/Reader;

    invoke-direct {p0, v0}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    .line 54
    iget-object v0, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    return-void
.end method

.method private q()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private r()V
    .registers 4

    .prologue
    .line 220
    sget-object v0, Lcom/a/c/d/d;->e:Lcom/a/c/d/d;

    invoke-virtual {p0, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 221
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 222
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 223
    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    new-instance v2, Lcom/a/c/ac;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/a/c/ac;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 58
    sget-object v0, Lcom/a/c/d/d;->a:Lcom/a/c/d/d;

    invoke-virtual {p0, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 59
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/t;

    .line 60
    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-virtual {v0}, Lcom/a/c/t;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    return-void
.end method

.method public final a(Lcom/a/c/d/d;)V
    .registers 5

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->f()Lcom/a/c/d/d;

    move-result-object v0

    if-eq v0, p1, :cond_29

    .line 139
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/b/a/h;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_29
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 64
    sget-object v0, Lcom/a/c/d/d;->b:Lcom/a/c/d/d;

    invoke-virtual {p0, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 65
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    .line 66
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    .line 67
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 70
    sget-object v0, Lcom/a/c/d/d;->c:Lcom/a/c/d/d;

    invoke-virtual {p0, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 71
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/z;

    .line 72
    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    .line 1132
    iget-object v0, v0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0}, Lcom/a/c/b/ag;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 72
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public final close()V
    .registers 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 204
    iget-object v0, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    sget-object v1, Lcom/a/c/b/a/h;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 76
    sget-object v0, Lcom/a/c/d/d;->d:Lcom/a/c/d/d;

    invoke-virtual {p0, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 77
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    .line 78
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public final e()Z
    .registers 3

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->f()Lcom/a/c/d/d;

    move-result-object v0

    .line 83
    sget-object v1, Lcom/a/c/d/d;->d:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_e

    sget-object v1, Lcom/a/c/d/d;->b:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final f()Lcom/a/c/d/d;
    .registers 4

    .prologue
    .line 87
    :goto_0
    iget-object v0, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 88
    sget-object v0, Lcom/a/c/d/d;->j:Lcom/a/c/d/d;

    .line 121
    :goto_a
    return-object v0

    .line 91
    :cond_b
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    .line 92
    instance-of v1, v0, Ljava/util/Iterator;

    if-eqz v1, :cond_42

    .line 93
    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/a/c/z;

    .line 94
    check-cast v0, Ljava/util/Iterator;

    .line 95
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3a

    .line 96
    if-eqz v1, :cond_30

    .line 97
    sget-object v0, Lcom/a/c/d/d;->e:Lcom/a/c/d/d;

    goto :goto_a

    .line 99
    :cond_30
    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 103
    :cond_3a
    if-eqz v1, :cond_3f

    sget-object v0, Lcom/a/c/d/d;->d:Lcom/a/c/d/d;

    goto :goto_a

    :cond_3f
    sget-object v0, Lcom/a/c/d/d;->b:Lcom/a/c/d/d;

    goto :goto_a

    .line 105
    :cond_42
    instance-of v1, v0, Lcom/a/c/z;

    if-eqz v1, :cond_49

    .line 106
    sget-object v0, Lcom/a/c/d/d;->c:Lcom/a/c/d/d;

    goto :goto_a

    .line 107
    :cond_49
    instance-of v1, v0, Lcom/a/c/t;

    if-eqz v1, :cond_50

    .line 108
    sget-object v0, Lcom/a/c/d/d;->a:Lcom/a/c/d/d;

    goto :goto_a

    .line 109
    :cond_50
    instance-of v1, v0, Lcom/a/c/ac;

    if-eqz v1, :cond_77

    .line 110
    check-cast v0, Lcom/a/c/ac;

    .line 1166
    iget-object v1, v0, Lcom/a/c/ac;->a:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    .line 111
    if-eqz v1, :cond_5f

    .line 112
    sget-object v0, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    goto :goto_a

    .line 2112
    :cond_5f
    iget-object v1, v0, Lcom/a/c/ac;->a:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Boolean;

    .line 113
    if-eqz v1, :cond_68

    .line 114
    sget-object v0, Lcom/a/c/d/d;->h:Lcom/a/c/d/d;

    goto :goto_a

    .line 2146
    :cond_68
    iget-object v0, v0, Lcom/a/c/ac;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    .line 115
    if-eqz v0, :cond_71

    .line 116
    sget-object v0, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    goto :goto_a

    .line 118
    :cond_71
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 120
    :cond_77
    instance-of v1, v0, Lcom/a/c/y;

    if-eqz v1, :cond_7e

    .line 121
    sget-object v0, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    goto :goto_a

    .line 122
    :cond_7e
    sget-object v1, Lcom/a/c/b/a/h;->d:Ljava/lang/Object;

    if-ne v0, v1, :cond_8a

    .line 123
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonReader is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_8a
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final g()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .registers 4

    .prologue
    .line 144
    sget-object v0, Lcom/a/c/d/d;->e:Lcom/a/c/d/d;

    invoke-virtual {p0, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 145
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 146
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 147
    iget-object v1, p0, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .registers 5

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->f()Lcom/a/c/d/d;

    move-result-object v0

    .line 153
    sget-object v1, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_2d

    sget-object v1, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_2d

    .line 154
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 156
    :cond_2d
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ac;

    invoke-virtual {v0}, Lcom/a/c/ac;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()Z
    .registers 2

    .prologue
    .line 160
    sget-object v0, Lcom/a/c/d/d;->h:Lcom/a/c/d/d;

    invoke-virtual {p0, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 161
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ac;

    invoke-virtual {v0}, Lcom/a/c/ac;->l()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .registers 2

    .prologue
    .line 165
    sget-object v0, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    invoke-virtual {p0, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 166
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    .line 167
    return-void
.end method

.method public final l()D
    .registers 6

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->f()Lcom/a/c/d/d;

    move-result-object v0

    .line 171
    sget-object v1, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_2d

    sget-object v1, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_2d

    .line 172
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 174
    :cond_2d
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ac;

    invoke-virtual {v0}, Lcom/a/c/ac;->c()D

    move-result-wide v0

    .line 2333
    iget-boolean v2, p0, Lcom/a/c/d/a;->b:Z

    .line 175
    if-nez v2, :cond_5c

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_47

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v2

    if-eqz v2, :cond_5c

    .line 176
    :cond_47
    new-instance v2, Ljava/lang/NumberFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JSON forbids NaN and infinities: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 178
    :cond_5c
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    .line 179
    return-wide v0
.end method

.method public final m()J
    .registers 5

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->f()Lcom/a/c/d/d;

    move-result-object v0

    .line 184
    sget-object v1, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_2d

    sget-object v1, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_2d

    .line 185
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 187
    :cond_2d
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ac;

    invoke-virtual {v0}, Lcom/a/c/ac;->g()J

    move-result-wide v0

    .line 188
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    .line 189
    return-wide v0
.end method

.method public final n()I
    .registers 5

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->f()Lcom/a/c/d/d;

    move-result-object v0

    .line 194
    sget-object v1, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_2d

    sget-object v1, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_2d

    .line 195
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 197
    :cond_2d
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ac;

    invoke-virtual {v0}, Lcom/a/c/ac;->h()I

    move-result v0

    .line 198
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    .line 199
    return v0
.end method

.method public final o()V
    .registers 3

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->e:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_c

    .line 209
    invoke-virtual {p0}, Lcom/a/c/b/a/h;->h()Ljava/lang/String;

    .line 213
    :goto_b
    return-void

    .line 211
    :cond_c
    invoke-direct {p0}, Lcom/a/c/b/a/h;->q()Ljava/lang/Object;

    goto :goto_b
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 216
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
