.class final Lcom/a/c/b/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Map$Entry;


# instance fields
.field a:Lcom/a/c/b/an;

.field b:Lcom/a/c/b/an;

.field c:Lcom/a/c/b/an;

.field d:Lcom/a/c/b/an;

.field e:Lcom/a/c/b/an;

.field final f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:I


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/b/an;->f:Ljava/lang/Object;

    .line 452
    iput-object p0, p0, Lcom/a/c/b/an;->e:Lcom/a/c/b/an;

    iput-object p0, p0, Lcom/a/c/b/an;->d:Lcom/a/c/b/an;

    .line 453
    return-void
.end method

.method constructor <init>(Lcom/a/c/b/an;Ljava/lang/Object;Lcom/a/c/b/an;Lcom/a/c/b/an;)V
    .registers 6

    .prologue
    .line 456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457
    iput-object p1, p0, Lcom/a/c/b/an;->a:Lcom/a/c/b/an;

    .line 458
    iput-object p2, p0, Lcom/a/c/b/an;->f:Ljava/lang/Object;

    .line 459
    const/4 v0, 0x1

    iput v0, p0, Lcom/a/c/b/an;->h:I

    .line 460
    iput-object p3, p0, Lcom/a/c/b/an;->d:Lcom/a/c/b/an;

    .line 461
    iput-object p4, p0, Lcom/a/c/b/an;->e:Lcom/a/c/b/an;

    .line 462
    iput-object p0, p4, Lcom/a/c/b/an;->d:Lcom/a/c/b/an;

    .line 463
    iput-object p0, p3, Lcom/a/c/b/an;->e:Lcom/a/c/b/an;

    .line 464
    return-void
.end method

.method private a()Lcom/a/c/b/an;
    .registers 3

    .prologue
    .line 503
    .line 504
    iget-object v0, p0, Lcom/a/c/b/an;->b:Lcom/a/c/b/an;

    .line 505
    :goto_2
    if-eqz v0, :cond_9

    .line 507
    iget-object v1, v0, Lcom/a/c/b/an;->b:Lcom/a/c/b/an;

    move-object p0, v0

    move-object v0, v1

    goto :goto_2

    .line 509
    :cond_9
    return-object p0
.end method

.method private b()Lcom/a/c/b/an;
    .registers 3

    .prologue
    .line 516
    .line 517
    iget-object v0, p0, Lcom/a/c/b/an;->c:Lcom/a/c/b/an;

    .line 518
    :goto_2
    if-eqz v0, :cond_9

    .line 520
    iget-object v1, v0, Lcom/a/c/b/an;->c:Lcom/a/c/b/an;

    move-object p0, v0

    move-object v0, v1

    goto :goto_2

    .line 522
    :cond_9
    return-object p0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 482
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_1c

    .line 483
    check-cast p1, Ljava/util/Map$Entry;

    .line 484
    iget-object v1, p0, Lcom/a/c/b/an;->f:Ljava/lang/Object;

    if-nez v1, :cond_1d

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1c

    :goto_11
    iget-object v1, p0, Lcom/a/c/b/an;->g:Ljava/lang/Object;

    if-nez v1, :cond_2a

    .line 485
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1c

    :goto_1b
    const/4 v0, 0x1

    .line 487
    :cond_1c
    return v0

    .line 484
    :cond_1d
    iget-object v1, p0, Lcom/a/c/b/an;->f:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    goto :goto_11

    .line 485
    :cond_2a
    iget-object v1, p0, Lcom/a/c/b/an;->g:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    goto :goto_1b
.end method

.method public final getKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/a/c/b/an;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/a/c/b/an;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 491
    iget-object v0, p0, Lcom/a/c/b/an;->f:Ljava/lang/Object;

    if-nez v0, :cond_c

    move v0, v1

    :goto_6
    iget-object v2, p0, Lcom/a/c/b/an;->g:Ljava/lang/Object;

    if-nez v2, :cond_13

    .line 492
    :goto_a
    xor-int/2addr v0, v1

    return v0

    .line 491
    :cond_c
    iget-object v0, p0, Lcom/a/c/b/an;->f:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_13
    iget-object v1, p0, Lcom/a/c/b/an;->g:Ljava/lang/Object;

    .line 492
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_a
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 475
    iget-object v0, p0, Lcom/a/c/b/an;->g:Ljava/lang/Object;

    .line 476
    iput-object p1, p0, Lcom/a/c/b/an;->g:Ljava/lang/Object;

    .line 477
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 496
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/a/c/b/an;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/a/c/b/an;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
