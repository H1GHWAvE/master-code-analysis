.class public final Lcom/a/c/b/a/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/c/ap;


# instance fields
.field private final a:Lcom/a/c/b/f;

.field private final b:Lcom/a/c/j;

.field private final c:Lcom/a/c/b/s;


# direct methods
.method public constructor <init>(Lcom/a/c/b/f;Lcom/a/c/j;Lcom/a/c/b/s;)V
    .registers 4

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/a/c/b/a/q;->a:Lcom/a/c/b/f;

    .line 54
    iput-object p2, p0, Lcom/a/c/b/a/q;->b:Lcom/a/c/j;

    .line 55
    iput-object p3, p0, Lcom/a/c/b/a/q;->c:Lcom/a/c/b/s;

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/a/c/b/a/q;Lcom/a/c/k;Ljava/lang/reflect/Field;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 6

    .prologue
    .line 6104
    const-class v0, Lcom/a/c/a/b;

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/b;

    .line 6105
    if-eqz v0, :cond_13

    .line 6106
    iget-object v1, p0, Lcom/a/c/b/a/q;->a:Lcom/a/c/b/f;

    invoke-static {v1, p1, p3, v0}, Lcom/a/c/b/a/g;->a(Lcom/a/c/b/f;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/a/b;)Lcom/a/c/an;

    move-result-object v0

    .line 6107
    if-eqz v0, :cond_13

    :goto_12
    return-object v0

    .line 6109
    :cond_13
    invoke-virtual {p1, p3}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    goto :goto_12
.end method

.method private a(Lcom/a/c/k;Ljava/lang/reflect/Field;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 6

    .prologue
    .line 104
    const-class v0, Lcom/a/c/a/b;

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/b;

    .line 105
    if-eqz v0, :cond_13

    .line 106
    iget-object v1, p0, Lcom/a/c/b/a/q;->a:Lcom/a/c/b/f;

    invoke-static {v1, p1, p3, v0}, Lcom/a/c/b/a/g;->a(Lcom/a/c/b/f;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/a/b;)Lcom/a/c/an;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_13

    .line 109
    :goto_12
    return-object v0

    :cond_13
    invoke-virtual {p1, p3}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    goto :goto_12
.end method

.method private a(Lcom/a/c/k;Ljava/lang/reflect/Field;Ljava/lang/String;Lcom/a/c/c/a;ZZ)Lcom/a/c/b/a/t;
    .registers 16

    .prologue
    .line 81
    .line 3094
    iget-object v0, p4, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 81
    invoke-static {v0}, Lcom/a/c/b/ap;->a(Ljava/lang/reflect/Type;)Z

    move-result v8

    .line 83
    new-instance v0, Lcom/a/c/b/a/r;

    move-object v1, p0

    move-object v2, p3

    move v3, p5

    move v4, p6

    move-object v5, p1

    move-object v6, p2

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lcom/a/c/b/a/r;-><init>(Lcom/a/c/b/a/q;Ljava/lang/String;ZZLcom/a/c/k;Ljava/lang/reflect/Field;Lcom/a/c/c/a;Z)V

    return-object v0
.end method

.method private a(Ljava/lang/reflect/Field;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 63
    const-class v0, Lcom/a/c/a/c;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/c;

    .line 64
    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/a/c/b/a/q;->b:Lcom/a/c/j;

    invoke-interface {v0, p1}, Lcom/a/c/j;->a(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v0

    :goto_10
    return-object v0

    :cond_11
    invoke-interface {v0}, Lcom/a/c/a/c;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_10
.end method

.method private a(Lcom/a/c/k;Lcom/a/c/c/a;Ljava/lang/Class;)Ljava/util/Map;
    .registers 19

    .prologue
    .line 113
    new-instance v10, Ljava/util/LinkedHashMap;

    invoke-direct {v10}, Ljava/util/LinkedHashMap;-><init>()V

    .line 114
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-eqz v1, :cond_d

    move-object v1, v10

    .line 140
    :goto_c
    return-object v1

    .line 3101
    :cond_d
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 119
    :goto_11
    const-class v1, Ljava/lang/Object;

    move-object/from16 v0, p3

    if-eq v0, v1, :cond_b0

    .line 120
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v13

    .line 121
    array-length v14, v13

    const/4 v1, 0x0

    move v11, v1

    :goto_1e
    if-ge v11, v14, :cond_96

    aget-object v7, v13, v11

    .line 122
    const/4 v1, 0x1

    invoke-direct {p0, v7, v1}, Lcom/a/c/b/a/q;->a(Ljava/lang/reflect/Field;Z)Z

    move-result v4

    .line 123
    const/4 v1, 0x0

    invoke-direct {p0, v7, v1}, Lcom/a/c/b/a/q;->a(Ljava/lang/reflect/Field;Z)Z

    move-result v5

    .line 124
    if-nez v4, :cond_30

    if-eqz v5, :cond_92

    .line 127
    :cond_30
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 4101
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 128
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v1, v0, v2}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v2

    .line 5063
    const-class v1, Lcom/a/c/a/c;

    invoke-virtual {v7, v1}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/a/c/a/c;

    .line 5064
    if-nez v1, :cond_8d

    iget-object v1, p0, Lcom/a/c/b/a/q;->b:Lcom/a/c/j;

    invoke-interface {v1, v7}, Lcom/a/c/j;->a(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v3

    .line 130
    :goto_52
    invoke-static {v2}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v8

    .line 5094
    iget-object v1, v8, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 5081
    invoke-static {v1}, Lcom/a/c/b/ap;->a(Ljava/lang/reflect/Type;)Z

    move-result v9

    .line 5083
    new-instance v1, Lcom/a/c/b/a/r;

    move-object v2, p0

    move-object/from16 v6, p1

    invoke-direct/range {v1 .. v9}, Lcom/a/c/b/a/r;-><init>(Lcom/a/c/b/a/q;Ljava/lang/String;ZZLcom/a/c/k;Ljava/lang/reflect/Field;Lcom/a/c/c/a;Z)V

    .line 131
    iget-object v2, v1, Lcom/a/c/b/a/t;->g:Ljava/lang/String;

    invoke-interface {v10, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/c/b/a/t;

    .line 132
    if-eqz v1, :cond_92

    .line 133
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " declares multiple JSON fields named "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Lcom/a/c/b/a/t;->g:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 5064
    :cond_8d
    invoke-interface {v1}, Lcom/a/c/a/c;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_52

    .line 121
    :cond_92
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_1e

    .line 5101
    :cond_96
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 137
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v1, v0, v2}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object p2

    .line 6094
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    move-object/from16 p3, v0

    goto/16 :goto_11

    :cond_b0
    move-object v1, v10

    .line 140
    goto/16 :goto_c
.end method

.method private a(Ljava/lang/reflect/Field;Z)Z
    .registers 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 59
    iget-object v0, p0, Lcom/a/c/b/a/q;->c:Lcom/a/c/b/s;

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/a/c/b/s;->a(Ljava/lang/Class;Z)Z

    move-result v0

    if-nez v0, :cond_ae

    iget-object v4, p0, Lcom/a/c/b/a/q;->c:Lcom/a/c/b/s;

    .line 1150
    iget v0, v4, Lcom/a/c/b/s;->c:I

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_1e

    move v0, v2

    .line 59
    :goto_1a
    if-nez v0, :cond_ae

    move v0, v2

    :goto_1d
    return v0

    .line 1154
    :cond_1e
    iget-wide v0, v4, Lcom/a/c/b/s;->b:D

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v6

    if-eqz v0, :cond_3e

    const-class v0, Lcom/a/c/a/d;

    .line 1155
    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/d;

    const-class v1, Lcom/a/c/a/e;

    invoke-virtual {p1, v1}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/a/c/a/e;

    invoke-virtual {v4, v0, v1}, Lcom/a/c/b/s;->a(Lcom/a/c/a/d;Lcom/a/c/a/e;)Z

    move-result v0

    if-nez v0, :cond_3e

    move v0, v2

    .line 1156
    goto :goto_1a

    .line 1159
    :cond_3e
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->isSynthetic()Z

    move-result v0

    if-eqz v0, :cond_46

    move v0, v2

    .line 1160
    goto :goto_1a

    .line 1163
    :cond_46
    iget-boolean v0, v4, Lcom/a/c/b/s;->e:Z

    if-eqz v0, :cond_64

    .line 1164
    const-class v0, Lcom/a/c/a/a;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/a;

    .line 1165
    if-eqz v0, :cond_5c

    if-eqz p2, :cond_5e

    invoke-interface {v0}, Lcom/a/c/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_64

    :cond_5c
    move v0, v2

    .line 1166
    goto :goto_1a

    .line 1165
    :cond_5e
    invoke-interface {v0}, Lcom/a/c/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 1170
    :cond_64
    iget-boolean v0, v4, Lcom/a/c/b/s;->d:Z

    if-nez v0, :cond_74

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/c/b/s;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_74

    move v0, v2

    .line 1171
    goto :goto_1a

    .line 1174
    :cond_74
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/c/b/s;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_80

    move v0, v2

    .line 1175
    goto :goto_1a

    .line 1178
    :cond_80
    if-eqz p2, :cond_a8

    iget-object v0, v4, Lcom/a/c/b/s;->f:Ljava/util/List;

    .line 1179
    :goto_84
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_ab

    .line 1180
    new-instance v1, Lcom/a/c/c;

    invoke-direct {v1, p1}, Lcom/a/c/c;-><init>(Ljava/lang/reflect/Field;)V

    .line 1181
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_93
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ab

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/b;

    .line 1182
    invoke-interface {v0}, Lcom/a/c/b;->a()Z

    move-result v0

    if-eqz v0, :cond_93

    move v0, v2

    .line 1183
    goto/16 :goto_1a

    .line 1178
    :cond_a8
    iget-object v0, v4, Lcom/a/c/b/s;->g:Ljava/util/List;

    goto :goto_84

    :cond_ab
    move v0, v3

    .line 1188
    goto/16 :goto_1a

    :cond_ae
    move v0, v3

    .line 59
    goto/16 :goto_1d
.end method


# virtual methods
.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 7

    .prologue
    .line 68
    .line 2094
    iget-object v1, p2, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 70
    const-class v0, Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 71
    const/4 v0, 0x0

    .line 75
    :goto_b
    return-object v0

    .line 74
    :cond_c
    iget-object v0, p0, Lcom/a/c/b/a/q;->a:Lcom/a/c/b/f;

    invoke-virtual {v0, p2}, Lcom/a/c/b/f;->a(Lcom/a/c/c/a;)Lcom/a/c/b/ao;

    move-result-object v2

    .line 75
    new-instance v0, Lcom/a/c/b/a/s;

    invoke-direct {p0, p1, p2, v1}, Lcom/a/c/b/a/q;->a(Lcom/a/c/k;Lcom/a/c/c/a;Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3}, Lcom/a/c/b/a/s;-><init>(Lcom/a/c/b/ao;Ljava/util/Map;B)V

    goto :goto_b
.end method
