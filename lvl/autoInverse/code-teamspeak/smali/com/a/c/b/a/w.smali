.class public final Lcom/a/c/b/a/w;
.super Lcom/a/c/an;
.source "SourceFile"


# static fields
.field public static final a:Lcom/a/c/ap;


# instance fields
.field private final b:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 41
    new-instance v0, Lcom/a/c/b/a/x;

    invoke-direct {v0}, Lcom/a/c/b/a/x;-><init>()V

    sput-object v0, Lcom/a/c/b/a/w;->a:Lcom/a/c/ap;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    .line 48
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm:ss a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/c/b/a/w;->b:Ljava/text/DateFormat;

    return-void
.end method

.method private declared-synchronized a(Lcom/a/c/d/e;Ljava/sql/Time;)V
    .registers 4

    .prologue
    .line 64
    monitor-enter p0

    if-nez p2, :cond_9

    const/4 v0, 0x0

    :goto_4
    :try_start_4
    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_10

    .line 65
    monitor-exit p0

    return-void

    .line 64
    :cond_9
    :try_start_9
    iget-object v0, p0, Lcom/a/c/b/a/w;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_10

    move-result-object v0

    goto :goto_4

    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/a/c/d/a;)Ljava/sql/Time;
    .registers 6

    .prologue
    .line 51
    monitor-enter p0

    :try_start_1
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_f

    .line 52
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_2a

    .line 53
    const/4 v0, 0x0

    .line 57
    :goto_d
    monitor-exit p0

    return-object v0

    .line 56
    :cond_f
    :try_start_f
    iget-object v0, p0, Lcom/a/c/b/a/w;->b:Ljava/text/DateFormat;

    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 57
    new-instance v0, Ljava/sql/Time;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/sql/Time;-><init>(J)V
    :try_end_22
    .catch Ljava/text/ParseException; {:try_start_f .. :try_end_22} :catch_23
    .catchall {:try_start_f .. :try_end_22} :catchall_2a

    goto :goto_d

    .line 58
    :catch_23
    move-exception v0

    .line 59
    :try_start_24
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2a
    .catchall {:try_start_24 .. :try_end_2a} :catchall_2a

    .line 51
    :catchall_2a
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/a/c/b/a/w;->b(Lcom/a/c/d/a;)Ljava/sql/Time;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 40
    check-cast p2, Ljava/sql/Time;

    invoke-direct {p0, p1, p2}, Lcom/a/c/b/a/w;->a(Lcom/a/c/d/e;Ljava/sql/Time;)V

    return-void
.end method
