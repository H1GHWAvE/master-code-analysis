.class abstract Lcom/a/c/b/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field b:Lcom/a/c/b/af;

.field c:Lcom/a/c/b/af;

.field d:I

.field final synthetic e:Lcom/a/c/b/w;


# direct methods
.method private constructor <init>(Lcom/a/c/b/w;)V
    .registers 3

    .prologue
    .line 760
    iput-object p1, p0, Lcom/a/c/b/ae;->e:Lcom/a/c/b/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 761
    iget-object v0, p0, Lcom/a/c/b/ae;->e:Lcom/a/c/b/w;

    iget-object v0, v0, Lcom/a/c/b/w;->c:Lcom/a/c/b/af;

    iget-object v0, v0, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    iput-object v0, p0, Lcom/a/c/b/ae;->b:Lcom/a/c/b/af;

    .line 762
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/b/ae;->c:Lcom/a/c/b/af;

    .line 763
    iget-object v0, p0, Lcom/a/c/b/ae;->e:Lcom/a/c/b/w;

    iget v0, v0, Lcom/a/c/b/w;->e:I

    iput v0, p0, Lcom/a/c/b/ae;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/c/b/w;B)V
    .registers 3

    .prologue
    .line 760
    invoke-direct {p0, p1}, Lcom/a/c/b/ae;-><init>(Lcom/a/c/b/w;)V

    return-void
.end method


# virtual methods
.method final a()Lcom/a/c/b/af;
    .registers 4

    .prologue
    .line 770
    iget-object v0, p0, Lcom/a/c/b/ae;->b:Lcom/a/c/b/af;

    .line 771
    iget-object v1, p0, Lcom/a/c/b/ae;->e:Lcom/a/c/b/w;

    iget-object v1, v1, Lcom/a/c/b/w;->c:Lcom/a/c/b/af;

    if-ne v0, v1, :cond_e

    .line 772
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 774
    :cond_e
    iget-object v1, p0, Lcom/a/c/b/ae;->e:Lcom/a/c/b/w;

    iget v1, v1, Lcom/a/c/b/w;->e:I

    iget v2, p0, Lcom/a/c/b/ae;->d:I

    if-eq v1, v2, :cond_1c

    .line 775
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 777
    :cond_1c
    iget-object v1, v0, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    iput-object v1, p0, Lcom/a/c/b/ae;->b:Lcom/a/c/b/af;

    .line 778
    iput-object v0, p0, Lcom/a/c/b/ae;->c:Lcom/a/c/b/af;

    return-object v0
.end method

.method public final hasNext()Z
    .registers 3

    .prologue
    .line 766
    iget-object v0, p0, Lcom/a/c/b/ae;->b:Lcom/a/c/b/af;

    iget-object v1, p0, Lcom/a/c/b/ae;->e:Lcom/a/c/b/w;

    iget-object v1, v1, Lcom/a/c/b/w;->c:Lcom/a/c/b/af;

    if-eq v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final remove()V
    .registers 4

    .prologue
    .line 782
    iget-object v0, p0, Lcom/a/c/b/ae;->c:Lcom/a/c/b/af;

    if-nez v0, :cond_a

    .line 783
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 785
    :cond_a
    iget-object v0, p0, Lcom/a/c/b/ae;->e:Lcom/a/c/b/w;

    iget-object v1, p0, Lcom/a/c/b/ae;->c:Lcom/a/c/b/af;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Z)V

    .line 786
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/b/ae;->c:Lcom/a/c/b/af;

    .line 787
    iget-object v0, p0, Lcom/a/c/b/ae;->e:Lcom/a/c/b/w;

    iget v0, v0, Lcom/a/c/b/w;->e:I

    iput v0, p0, Lcom/a/c/b/ae;->d:I

    .line 788
    return-void
.end method
