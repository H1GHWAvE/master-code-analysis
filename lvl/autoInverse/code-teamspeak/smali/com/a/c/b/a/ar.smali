.class final Lcom/a/c/b/a/ar;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 605
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/util/Locale;)V
    .registers 3

    .prologue
    .line 636
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 637
    return-void

    .line 636
    :cond_7
    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/util/Locale;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 608
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v2, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v2, :cond_d

    .line 609
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 631
    :goto_c
    return-object v1

    .line 612
    :cond_d
    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    .line 613
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v2, "_"

    invoke-direct {v3, v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 618
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 620
    :goto_22
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_51

    .line 621
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 623
    :goto_2c
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_4f

    .line 624
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 626
    :goto_37
    if-nez v2, :cond_41

    if-nez v3, :cond_41

    .line 627
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    goto :goto_c

    .line 628
    :cond_41
    if-nez v3, :cond_49

    .line 629
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 631
    :cond_49
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    :cond_4f
    move-object v3, v1

    goto :goto_37

    :cond_51
    move-object v2, v1

    goto :goto_2c

    :cond_53
    move-object v0, v1

    goto :goto_22
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 605
    .line 1608
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v2, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v2, :cond_d

    .line 1609
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1629
    :goto_c
    return-object v1

    .line 1612
    :cond_d
    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    .line 1613
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v2, "_"

    invoke-direct {v3, v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1617
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 1618
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 1620
    :goto_22
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_51

    .line 1621
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 1623
    :goto_2c
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_4f

    .line 1624
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 1626
    :goto_37
    if-nez v2, :cond_41

    if-nez v3, :cond_41

    .line 1627
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    goto :goto_c

    .line 1628
    :cond_41
    if-nez v3, :cond_49

    .line 1629
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 1631
    :cond_49
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    :cond_4f
    move-object v3, v1

    goto :goto_37

    :cond_51
    move-object v2, v1

    goto :goto_2c

    :cond_53
    move-object v0, v1

    goto :goto_22
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 605
    check-cast p2, Ljava/util/Locale;

    .line 1636
    if-nez p2, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 605
    return-void

    .line 1636
    :cond_9
    invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method
