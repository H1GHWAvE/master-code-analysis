.class final Lcom/a/c/b/a/ag;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 384
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/math/BigInteger;)V
    .registers 2

    .prologue
    .line 398
    invoke-virtual {p0, p1}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    .line 399
    return-void
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/math/BigInteger;
    .registers 3

    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 387
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 388
    const/4 v0, 0x0

    .line 391
    :goto_c
    return-object v0

    :cond_d
    :try_start_d
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_16} :catch_17

    goto :goto_c

    .line 392
    :catch_17
    move-exception v0

    .line 393
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 384
    invoke-static {p1}, Lcom/a/c/b/a/ag;->b(Lcom/a/c/d/a;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 384
    check-cast p2, Ljava/math/BigInteger;

    .line 1398
    invoke-virtual {p1, p2}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    .line 384
    return-void
.end method
