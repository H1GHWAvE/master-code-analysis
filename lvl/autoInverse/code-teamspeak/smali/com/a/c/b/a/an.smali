.class final Lcom/a/c/b/a/an;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 499
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/util/UUID;)V
    .registers 3

    .prologue
    .line 510
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 511
    return-void

    .line 510
    :cond_7
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/util/UUID;
    .registers 3

    .prologue
    .line 502
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 503
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 504
    const/4 v0, 0x0

    .line 506
    :goto_c
    return-object v0

    :cond_d
    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    goto :goto_c
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 499
    .line 1502
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 1503
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1504
    const/4 v0, 0x0

    :goto_c
    return-object v0

    .line 1506
    :cond_d
    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    goto :goto_c
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 499
    check-cast p2, Ljava/util/UUID;

    .line 1510
    if-nez p2, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 499
    return-void

    .line 1510
    :cond_9
    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method
