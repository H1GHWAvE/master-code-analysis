.class final Lcom/a/c/b/a/aj;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 440
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/net/URL;)V
    .registers 3

    .prologue
    .line 452
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 453
    return-void

    .line 452
    :cond_7
    invoke-virtual {p1}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/net/URL;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 443
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v1

    sget-object v2, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v1, v2, :cond_d

    .line 444
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 448
    :cond_c
    :goto_c
    return-object v0

    .line 447
    :cond_d
    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    .line 448
    const-string v2, "null"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_c
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 440
    .line 1443
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v1

    sget-object v2, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v1, v2, :cond_d

    .line 1444
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1448
    :cond_c
    :goto_c
    return-object v0

    .line 1447
    :cond_d
    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    .line 1448
    const-string v2, "null"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_c
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 440
    check-cast p2, Ljava/net/URL;

    .line 1452
    if-nez p2, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 440
    return-void

    .line 1452
    :cond_9
    invoke-virtual {p2}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method
