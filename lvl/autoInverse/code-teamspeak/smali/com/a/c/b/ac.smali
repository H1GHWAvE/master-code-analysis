.class final Lcom/a/c/b/ac;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/c/b/w;


# direct methods
.method constructor <init>(Lcom/a/c/b/w;)V
    .registers 2

    .prologue
    .line 826
    iput-object p1, p0, Lcom/a/c/b/ac;->a:Lcom/a/c/b/w;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 848
    iget-object v0, p0, Lcom/a/c/b/ac;->a:Lcom/a/c/b/w;

    invoke-virtual {v0}, Lcom/a/c/b/w;->clear()V

    .line 849
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 840
    iget-object v0, p0, Lcom/a/c/b/ac;->a:Lcom/a/c/b/w;

    invoke-virtual {v0, p1}, Lcom/a/c/b/w;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 832
    new-instance v0, Lcom/a/c/b/ad;

    invoke-direct {v0, p0}, Lcom/a/c/b/ad;-><init>(Lcom/a/c/b/ac;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 844
    iget-object v0, p0, Lcom/a/c/b/ac;->a:Lcom/a/c/b/w;

    invoke-virtual {v0, p1}, Lcom/a/c/b/w;->a(Ljava/lang/Object;)Lcom/a/c/b/af;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 828
    iget-object v0, p0, Lcom/a/c/b/ac;->a:Lcom/a/c/b/w;

    iget v0, v0, Lcom/a/c/b/w;->d:I

    return v0
.end method
