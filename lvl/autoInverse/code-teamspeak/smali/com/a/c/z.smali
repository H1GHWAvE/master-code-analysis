.class public final Lcom/a/c/z;
.super Lcom/a/c/w;
.source "SourceFile"


# instance fields
.field public final a:Lcom/a/c/b/ag;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/a/c/w;-><init>()V

    .line 33
    new-instance v0, Lcom/a/c/b/ag;

    invoke-direct {v0}, Lcom/a/c/b/ag;-><init>()V

    iput-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    return-void
.end method

.method private static a(Ljava/lang/Object;)Lcom/a/c/w;
    .registers 2

    .prologue
    .line 122
    if-nez p0, :cond_5

    sget-object v0, Lcom/a/c/y;->a:Lcom/a/c/y;

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/c/ac;

    invoke-direct {v0, p0}, Lcom/a/c/ac;-><init>(Ljava/lang/Object;)V

    goto :goto_4
.end method

.method private a(Ljava/lang/String;)Lcom/a/c/w;
    .registers 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/w;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 4

    .prologue
    .line 101
    invoke-static {p2}, Lcom/a/c/z;->a(Ljava/lang/Object;)Lcom/a/c/w;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/c/z;->a(Ljava/lang/String;Lcom/a/c/w;)V

    .line 102
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Character;)V
    .registers 4

    .prologue
    .line 112
    invoke-static {p2}, Lcom/a/c/z;->a(Ljava/lang/Object;)Lcom/a/c/w;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/c/z;->a(Ljava/lang/String;Lcom/a/c/w;)V

    .line 113
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Number;)V
    .registers 4

    .prologue
    .line 90
    invoke-static {p2}, Lcom/a/c/z;->a(Ljava/lang/Object;)Lcom/a/c/w;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/c/z;->a(Ljava/lang/String;Lcom/a/c/w;)V

    .line 91
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 79
    invoke-static {p2}, Lcom/a/c/z;->a(Ljava/lang/Object;)Lcom/a/c/w;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/c/z;->a(Ljava/lang/String;Lcom/a/c/w;)V

    .line 80
    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .registers 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c(Ljava/lang/String;)Lcom/a/c/w;
    .registers 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/w;

    return-object v0
.end method

.method private d(Ljava/lang/String;)Lcom/a/c/ac;
    .registers 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ac;

    return-object v0
.end method

.method private e(Ljava/lang/String;)Lcom/a/c/t;
    .registers 3

    .prologue
    .line 172
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/t;

    return-object v0
.end method

.method private f(Ljava/lang/String;)Lcom/a/c/z;
    .registers 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/z;

    return-object v0
.end method

.method private p()Lcom/a/c/z;
    .registers 5

    .prologue
    .line 38
    new-instance v2, Lcom/a/c/z;

    invoke-direct {v2}, Lcom/a/c/z;-><init>()V

    .line 39
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0}, Lcom/a/c/b/ag;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 40
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/w;

    invoke-virtual {v0}, Lcom/a/c/w;->m()Lcom/a/c/w;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/a/c/z;->a(Ljava/lang/String;Lcom/a/c/w;)V

    goto :goto_f

    .line 42
    :cond_2f
    return-object v2
.end method

.method private q()Ljava/util/Set;
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0}, Lcom/a/c/b/ag;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/a/c/w;)V
    .registers 4

    .prologue
    .line 54
    if-nez p2, :cond_4

    .line 55
    sget-object p2, Lcom/a/c/y;->a:Lcom/a/c/y;

    .line 57
    :cond_4
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1, p2}, Lcom/a/c/b/ag;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 187
    if-eq p1, p0, :cond_12

    instance-of v0, p1, Lcom/a/c/z;

    if-eqz v0, :cond_14

    check-cast p1, Lcom/a/c/z;

    iget-object v0, p1, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    iget-object v1, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    .line 188
    invoke-virtual {v0, v1}, Lcom/a/c/b/ag;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0}, Lcom/a/c/b/ag;->hashCode()I

    move-result v0

    return v0
.end method

.method final synthetic m()Lcom/a/c/w;
    .registers 5

    .prologue
    .line 32
    .line 1038
    new-instance v2, Lcom/a/c/z;

    invoke-direct {v2}, Lcom/a/c/z;-><init>()V

    .line 1039
    iget-object v0, p0, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0}, Lcom/a/c/b/ag;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1040
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/w;

    invoke-virtual {v0}, Lcom/a/c/w;->m()Lcom/a/c/w;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/a/c/z;->a(Ljava/lang/String;Lcom/a/c/w;)V

    goto :goto_f

    .line 32
    :cond_2f
    return-object v2
.end method
