.class public final Lcom/a/c/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/Class;

.field public final b:Ljava/lang/reflect/Type;

.field final c:I


# direct methods
.method protected constructor <init>()V
    .registers 3

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 1082
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1083
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_17

    .line 1084
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Missing type parameter."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1086
    :cond_17
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 1087
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 62
    iput-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 63
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/c/b/b;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 64
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/a/c/c/a;->c:I

    .line 65
    return-void
.end method

.method private constructor <init>(Ljava/lang/reflect/Type;)V
    .registers 3

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p1}, Lcom/a/c/b/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 73
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/c/b/b;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 74
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/a/c/c/a;->c:I

    .line 75
    return-void
.end method

.method public static a(Ljava/lang/Class;)Lcom/a/c/c/a;
    .registers 2

    .prologue
    .line 303
    new-instance v0, Lcom/a/c/c/a;

    invoke-direct {v0, p0}, Lcom/a/c/c/a;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method public static a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
    .registers 2

    .prologue
    .line 296
    new-instance v0, Lcom/a/c/c/a;

    invoke-direct {v0, p0}, Lcom/a/c/c/a;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method private static varargs a(Ljava/lang/reflect/Type;[Ljava/lang/Class;)Ljava/lang/AssertionError;
    .registers 6

    .prologue
    .line 257
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Unexpected type. Expected one of: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 259
    const/4 v0, 0x0

    :goto_8
    const/4 v2, 0x3

    if-ge v0, v2, :cond_1d

    aget-object v2, p1, v0

    .line 260
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 262
    :cond_1d
    const-string v0, "but got: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", for type token: "

    .line 263
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 265
    new-instance v0, Ljava/lang/AssertionError;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private a()Ljava/lang/Class;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    return-object v0
.end method

.method private a(Lcom/a/c/c/a;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 153
    .line 1101
    iget-object v0, p1, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 153
    invoke-direct {p0, v0}, Lcom/a/c/c/a;->b(Ljava/lang/reflect/Type;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/reflect/ParameterizedType;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 240
    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 241
    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v4

    .line 242
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v5

    move v1, v2

    .line 243
    :goto_19
    array-length v0, v4

    if-ge v1, v0, :cond_44

    .line 244
    aget-object v0, v4, v1

    aget-object v6, v5, v1

    .line 2273
    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3a

    instance-of v7, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v7, :cond_3e

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    .line 2275
    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    :cond_3a
    move v0, v3

    .line 244
    :goto_3b
    if-nez v0, :cond_40

    .line 250
    :cond_3d
    :goto_3d
    return v2

    :cond_3e
    move v0, v2

    .line 2275
    goto :goto_3b

    .line 243
    :cond_40
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_19

    :cond_44
    move v2, v3

    .line 248
    goto :goto_3d
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/GenericArrayType;)Z
    .registers 4

    .prologue
    .line 161
    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 162
    instance-of v1, v0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v1, :cond_2f

    .line 164
    instance-of v1, p0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v1, :cond_1e

    .line 165
    check-cast p0, Ljava/lang/reflect/GenericArrayType;

    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object p0

    .line 173
    :cond_12
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z

    move-result v0

    .line 178
    :goto_1d
    return v0

    .line 166
    :cond_1e
    instance-of v1, p0, Ljava/lang/Class;

    if-eqz v1, :cond_12

    .line 167
    check-cast p0, Ljava/lang/Class;

    .line 168
    :goto_24
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 169
    invoke-virtual {p0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object p0

    goto :goto_24

    .line 178
    :cond_2f
    const/4 v0, 0x1

    goto :goto_1d
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z
    .registers 13

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 188
    move-object v0, p0

    :goto_3
    if-nez v0, :cond_6

    .line 225
    :goto_5
    return v2

    .line 192
    :cond_6
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    move v2, v3

    .line 193
    goto :goto_5

    .line 197
    :cond_e
    invoke-static {v0}, Lcom/a/c/b/b;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v5

    .line 198
    const/4 v1, 0x0

    .line 199
    instance-of v4, v0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v4, :cond_b9

    .line 200
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    move-object v4, v0

    .line 204
    :goto_1a
    if-eqz v4, :cond_91

    .line 205
    invoke-interface {v4}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v6

    .line 206
    invoke-virtual {v5}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v7

    move v1, v2

    .line 207
    :goto_25
    array-length v0, v6

    if-ge v1, v0, :cond_48

    .line 208
    aget-object v0, v6, v1

    .line 209
    aget-object v8, v7, v1

    .line 210
    :goto_2c
    instance-of v9, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v9, :cond_3d

    .line 211
    check-cast v0, Ljava/lang/reflect/TypeVariable;

    .line 212
    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    goto :goto_2c

    .line 214
    :cond_3d
    invoke-interface {v8}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_25

    .line 1240
    :cond_48
    invoke-interface {v4}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 1241
    invoke-interface {v4}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v4

    .line 1242
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v6

    move v1, v2

    .line 1243
    :goto_5f
    array-length v0, v4

    if-ge v1, v0, :cond_89

    .line 1244
    aget-object v0, v4, v1

    aget-object v7, v6, v1

    .line 1273
    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_80

    instance-of v8, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v8, :cond_87

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    .line 1275
    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_87

    :cond_80
    move v0, v3

    .line 1244
    :goto_81
    if-eqz v0, :cond_8f

    .line 1243
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5f

    :cond_87
    move v0, v2

    .line 1275
    goto :goto_81

    :cond_89
    move v0, v3

    .line 218
    :goto_8a
    if-eqz v0, :cond_91

    move v2, v3

    .line 219
    goto/16 :goto_5

    :cond_8f
    move v0, v2

    .line 1250
    goto :goto_8a

    .line 223
    :cond_91
    invoke-virtual {v5}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v1

    array-length v4, v1

    move v0, v2

    :goto_97
    if-ge v0, v4, :cond_ac

    aget-object v6, v1, v0

    .line 224
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-static {v6, p1, v7}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z

    move-result v6

    if-eqz v6, :cond_a9

    move v2, v3

    .line 225
    goto/16 :goto_5

    .line 223
    :cond_a9
    add-int/lit8 v0, v0, 0x1

    goto :goto_97

    .line 230
    :cond_ac
    invoke-virtual {v5}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object p0

    .line 231
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object p2, v0

    move-object v0, p0

    goto/16 :goto_3

    :cond_b9
    move-object v4, v1

    goto/16 :goto_1a
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;Ljava/util/Map;)Z
    .registers 4

    .prologue
    .line 273
    invoke-virtual {p1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    instance-of v0, p0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_1c

    check-cast p0, Ljava/lang/reflect/TypeVariable;

    .line 275
    invoke-interface {p0}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_1a
    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method private b()Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method private static b(Ljava/lang/Class;)Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 82
    invoke-virtual {p0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 83
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_10

    .line 84
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Missing type parameter."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_10
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 87
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/reflect/Type;)Z
    .registers 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 123
    if-nez p1, :cond_6

    move v0, v1

    .line 138
    :goto_5
    return v0

    .line 127
    :cond_6
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v2

    .line 128
    goto :goto_5

    .line 131
    :cond_10
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_21

    .line 132
    iget-object v0, p0, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    invoke-static {p1}, Lcom/a/c/b/b;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    goto :goto_5

    .line 133
    :cond_21
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_35

    .line 134
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z

    move-result v0

    goto :goto_5

    .line 136
    :cond_35
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_55

    .line 137
    iget-object v0, p0, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    invoke-static {p1}, Lcom/a/c/b/b;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_53

    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/GenericArrayType;

    .line 138
    invoke-static {p1, v0}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/GenericArrayType;)Z

    move-result v0

    if-eqz v0, :cond_53

    move v0, v2

    goto :goto_5

    :cond_53
    move v0, v1

    goto :goto_5

    .line 140
    :cond_55
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/Class;

    aput-object v4, v3, v1

    const-class v1, Ljava/lang/reflect/ParameterizedType;

    aput-object v1, v3, v2

    const/4 v1, 0x2

    const-class v2, Ljava/lang/reflect/GenericArrayType;

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;[Ljava/lang/Class;)Ljava/lang/AssertionError;

    move-result-object v0

    throw v0
.end method

.method private c(Ljava/lang/Class;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/a/c/c/a;->b(Ljava/lang/reflect/Type;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 284
    instance-of v0, p1, Lcom/a/c/c/a;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    check-cast p1, Lcom/a/c/c/a;

    iget-object v1, p1, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 285
    invoke-static {v0, v1}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 280
    iget v0, p0, Lcom/a/c/c/a;->c:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lcom/a/c/b/b;->c(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
