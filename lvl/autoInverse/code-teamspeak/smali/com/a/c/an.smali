.class public abstract Lcom/a/c/an;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/a/c/an;
    .registers 2

    .prologue
    .line 186
    new-instance v0, Lcom/a/c/ao;

    invoke-direct {v0, p0}, Lcom/a/c/ao;-><init>(Lcom/a/c/an;)V

    return-object v0
.end method

.method private a(Lcom/a/c/w;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 280
    :try_start_0
    new-instance v0, Lcom/a/c/b/a/h;

    invoke-direct {v0, p1}, Lcom/a/c/b/a/h;-><init>(Lcom/a/c/w;)V

    .line 281
    invoke-virtual {p0, v0}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_8} :catch_a

    move-result-object v0

    return-object v0

    .line 282
    :catch_a
    move-exception v0

    .line 283
    new-instance v1, Lcom/a/c/x;

    invoke-direct {v1, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Ljava/io/Reader;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 255
    new-instance v0, Lcom/a/c/d/a;

    invoke-direct {v0, p1}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 256
    invoke-virtual {p0, v0}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 269
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1255
    new-instance v1, Lcom/a/c/d/a;

    invoke-direct {v1, v0}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 1256
    invoke-virtual {p0, v1}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;

    move-result-object v0

    .line 269
    return-object v0
.end method

.method private a(Ljava/io/Writer;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 141
    new-instance v0, Lcom/a/c/d/e;

    invoke-direct {v0, p1}, Lcom/a/c/d/e;-><init>(Ljava/io/Writer;)V

    .line 142
    invoke-virtual {p0, v0, p2}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V

    .line 143
    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 215
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1141
    new-instance v1, Lcom/a/c/d/e;

    invoke-direct {v1, v0}, Lcom/a/c/d/e;-><init>(Ljava/io/Writer;)V

    .line 1142
    invoke-virtual {p0, v1, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V

    .line 217
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/a/c/w;
    .registers 4

    .prologue
    .line 229
    :try_start_0
    new-instance v0, Lcom/a/c/b/a/j;

    invoke-direct {v0}, Lcom/a/c/b/a/j;-><init>()V

    .line 230
    invoke-virtual {p0, v0, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V

    .line 231
    invoke-virtual {v0}, Lcom/a/c/b/a/j;->a()Lcom/a/c/w;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_b} :catch_d

    move-result-object v0

    return-object v0

    .line 232
    :catch_d
    move-exception v0

    .line 233
    new-instance v1, Lcom/a/c/x;

    invoke-direct {v1, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public abstract a(Lcom/a/c/d/a;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/a/c/d/e;Ljava/lang/Object;)V
.end method
