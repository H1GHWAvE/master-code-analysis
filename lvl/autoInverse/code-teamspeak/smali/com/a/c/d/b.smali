.class final Lcom/a/c/d/b;
.super Lcom/a/c/b/u;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1598
    invoke-direct {p0}, Lcom/a/c/b/u;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/a/c/d/a;)V
    .registers 5

    .prologue
    .line 1600
    instance-of v0, p1, Lcom/a/c/b/a/h;

    if-eqz v0, :cond_31

    .line 1601
    check-cast p1, Lcom/a/c/b/a/h;

    .line 2220
    sget-object v0, Lcom/a/c/d/d;->e:Lcom/a/c/d/d;

    invoke-virtual {p1, v0}, Lcom/a/c/b/a/h;->a(Lcom/a/c/d/d;)V

    .line 2221
    invoke-virtual {p1}, Lcom/a/c/b/a/h;->g()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 2222
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2223
    iget-object v1, p1, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2224
    iget-object v1, p1, Lcom/a/c/b/a/h;->a:Ljava/util/List;

    new-instance v2, Lcom/a/c/ac;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/a/c/ac;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1613
    :goto_30
    return-void

    .line 1604
    :cond_31
    invoke-static {p1}, Lcom/a/c/d/a;->a(Lcom/a/c/d/a;)I

    move-result v0

    .line 1605
    if-nez v0, :cond_3b

    .line 1606
    invoke-static {p1}, Lcom/a/c/d/a;->b(Lcom/a/c/d/a;)I

    move-result v0

    .line 1608
    :cond_3b
    const/16 v1, 0xd

    if-ne v0, v1, :cond_45

    .line 1609
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/a/c/d/a;->a(Lcom/a/c/d/a;I)I

    goto :goto_30

    .line 1610
    :cond_45
    const/16 v1, 0xc

    if-ne v0, v1, :cond_4f

    .line 1611
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/a/c/d/a;->a(Lcom/a/c/d/a;I)I

    goto :goto_30

    .line 1612
    :cond_4f
    const/16 v1, 0xe

    if-ne v0, v1, :cond_59

    .line 1613
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/a/c/d/a;->a(Lcom/a/c/d/a;I)I

    goto :goto_30

    .line 1615
    :cond_59
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a name but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1616
    invoke-static {p1}, Lcom/a/c/d/a;->c(Lcom/a/c/d/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/a/c/d/a;->d(Lcom/a/c/d/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1617
    invoke-virtual {p1}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
