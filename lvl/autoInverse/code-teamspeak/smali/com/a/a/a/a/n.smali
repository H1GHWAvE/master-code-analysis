.class final Lcom/a/a/a/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/a/a/a/a/l;


# direct methods
.method constructor <init>(Lcom/a/a/a/a/l;ILjava/lang/String;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 234
    iput-object p1, p0, Lcom/a/a/a/a/n;->d:Lcom/a/a/a/a/l;

    iput p2, p0, Lcom/a/a/a/a/n;->a:I

    iput-object p3, p0, Lcom/a/a/a/a/n;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/a/a/a/a/n;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 12

    .prologue
    .line 236
    const-string v0, "LicenseChecker"

    const-string v1, "Received response."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    iget-object v0, p0, Lcom/a/a/a/a/n;->d:Lcom/a/a/a/a/l;

    iget-object v0, v0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    invoke-static {v0}, Lcom/a/a/a/a/k;->a(Lcom/a/a/a/a/k;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/a/n;->d:Lcom/a/a/a/a/l;

    invoke-static {v1}, Lcom/a/a/a/a/l;->a(Lcom/a/a/a/a/l;)Lcom/a/a/a/a/p;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 239
    iget-object v0, p0, Lcom/a/a/a/a/n;->d:Lcom/a/a/a/a/l;

    invoke-static {v0}, Lcom/a/a/a/a/l;->b(Lcom/a/a/a/a/l;)V

    .line 240
    iget-object v0, p0, Lcom/a/a/a/a/n;->d:Lcom/a/a/a/a/l;

    invoke-static {v0}, Lcom/a/a/a/a/l;->a(Lcom/a/a/a/a/l;)Lcom/a/a/a/a/p;

    move-result-object v3

    iget-object v0, p0, Lcom/a/a/a/a/n;->d:Lcom/a/a/a/a/l;

    iget-object v0, v0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    invoke-static {v0}, Lcom/a/a/a/a/k;->b(Lcom/a/a/a/a/k;)Ljava/security/PublicKey;

    move-result-object v1

    iget v4, p0, Lcom/a/a/a/a/n;->a:I

    iget-object v2, p0, Lcom/a/a/a/a/n;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/a/a/a/a/n;->c:Ljava/lang/String;

    .line 1098
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 1099
    const/16 v6, 0x103

    if-ne v4, v6, :cond_81

    .line 1100
    add-int/lit8 v6, v4, 0x1

    invoke-virtual {v0, v6}, Ljava/util/zip/CRC32;->update(I)V

    .line 1103
    :goto_42
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v6

    .line 1106
    const/4 v0, 0x0

    .line 1107
    if-eqz v4, :cond_4f

    const/4 v8, 0x1

    if-eq v4, v8, :cond_4f

    const/4 v8, 0x2

    if-ne v4, v8, :cond_182

    .line 1111
    :cond_4f
    :try_start_4f
    const-string v0, "SHA1withRSA"

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 1112
    invoke-virtual {v0, v1}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 1113
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 1115
    invoke-static {v5}, Lcom/a/a/a/a/a/a;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    const/4 v0, 0x1

    if-nez v0, :cond_a5

    .line 1116
    const-string v0, "LicenseValidator"

    const-string v1, "Signature verification failed."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V
    :try_end_73
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4f .. :try_end_73} :catch_85
    .catch Ljava/security/InvalidKeyException; {:try_start_4f .. :try_end_73} :catch_8c
    .catch Ljava/security/SignatureException; {:try_start_4f .. :try_end_73} :catch_92
    .catch Lcom/a/a/a/a/a/b; {:try_start_4f .. :try_end_73} :catch_99

    .line 241
    :goto_73
    iget-object v0, p0, Lcom/a/a/a/a/n;->d:Lcom/a/a/a/a/l;

    iget-object v0, v0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    iget-object v1, p0, Lcom/a/a/a/a/n;->d:Lcom/a/a/a/a/l;

    invoke-static {v1}, Lcom/a/a/a/a/l;->a(Lcom/a/a/a/a/l;)Lcom/a/a/a/a/p;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/a/a/a/k;->b(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V

    .line 273
    :cond_80
    return-void

    .line 1102
    :cond_81
    invoke-virtual {v0, v4}, Ljava/util/zip/CRC32;->update(I)V

    goto :goto_42

    .line 1120
    :catch_85
    move-exception v0

    .line 1122
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1124
    :catch_8c
    move-exception v0

    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto :goto_73

    .line 1126
    :catch_92
    move-exception v0

    .line 1127
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1129
    :catch_99
    move-exception v0

    const-string v0, "LicenseValidator"

    const-string v1, "Could not Base64-decode signature."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1130
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V

    goto :goto_73

    .line 2046
    :cond_a5
    const/16 v0, 0x3a

    :try_start_a7
    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 2048
    const/4 v1, -0x1

    if-ne v1, v0, :cond_d4

    .line 2050
    const-string v0, ""

    move-object v1, v0

    move-object v0, v2

    .line 2056
    :goto_b2
    const-string v8, "|"

    invoke-static {v8}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 2057
    array-length v0, v8

    const/4 v9, 0x6

    if-ge v0, v9, :cond_ef

    .line 2058
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong number of fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_c8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a7 .. :try_end_c8} :catch_c8

    .line 1138
    :catch_c8
    move-exception v0

    const-string v0, "LicenseValidator"

    const-string v1, "Could not parse response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1139
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V

    goto :goto_73

    .line 2052
    :cond_d4
    const/4 v1, 0x0

    :try_start_d5
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2053
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v0, v8, :cond_e5

    const-string v0, ""

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    goto :goto_b2

    :cond_e5
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    goto :goto_b2

    .line 2061
    :cond_ef
    new-instance v0, Lcom/a/a/a/a/u;

    invoke-direct {v0}, Lcom/a/a/a/a/u;-><init>()V

    .line 2062
    iput-object v1, v0, Lcom/a/a/a/a/u;->g:Ljava/lang/String;

    .line 2063
    const/4 v1, 0x0

    aget-object v1, v8, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/a/a/a/a/u;->a:I

    .line 2064
    const/4 v1, 0x1

    aget-object v1, v8, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/a/a/a/a/u;->b:I

    .line 2065
    const/4 v1, 0x2

    aget-object v1, v8, v1

    iput-object v1, v0, Lcom/a/a/a/a/u;->c:Ljava/lang/String;

    .line 2066
    const/4 v1, 0x3

    aget-object v1, v8, v1

    iput-object v1, v0, Lcom/a/a/a/a/u;->d:Ljava/lang/String;

    .line 2068
    const/4 v1, 0x4

    aget-object v1, v8, v1

    iput-object v1, v0, Lcom/a/a/a/a/u;->e:Ljava/lang/String;

    .line 2069
    const/4 v1, 0x5

    aget-object v1, v8, v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, v0, Lcom/a/a/a/a/u;->f:J
    :try_end_120
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d5 .. :try_end_120} :catch_c8

    .line 1143
    iget v1, v0, Lcom/a/a/a/a/u;->a:I

    if-eq v1, v4, :cond_130

    .line 1144
    const-string v0, "LicenseValidator"

    const-string v1, "Response codes don\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_73

    .line 1149
    :cond_130
    iget v1, v0, Lcom/a/a/a/a/u;->b:I

    iget v4, v3, Lcom/a/a/a/a/p;->b:I

    if-eq v1, v4, :cond_142

    .line 1150
    const-string v0, "LicenseValidator"

    const-string v1, "Nonce doesn\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_73

    .line 1155
    :cond_142
    iget-object v1, v0, Lcom/a/a/a/a/u;->c:Ljava/lang/String;

    iget-object v4, v3, Lcom/a/a/a/a/p;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_158

    .line 1156
    const-string v0, "LicenseValidator"

    const-string v1, "Package name doesn\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1157
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_73

    .line 1161
    :cond_158
    iget-object v1, v0, Lcom/a/a/a/a/u;->d:Ljava/lang/String;

    iget-object v4, v3, Lcom/a/a/a/a/p;->d:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16e

    .line 1162
    const-string v0, "LicenseValidator"

    const-string v1, "Version codes don\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1163
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_73

    .line 1168
    :cond_16e
    iget-object v1, v0, Lcom/a/a/a/a/u;->e:Ljava/lang/String;

    .line 1169
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_182

    .line 1170
    const-string v0, "LicenseValidator"

    const-string v1, "User identifier is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1171
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_73

    .line 1176
    :cond_182
    const-wide v8, 0xd202ef8dL

    cmp-long v1, v6, v8

    if-nez v1, :cond_192

    .line 1178
    const/16 v1, 0x100

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1182
    :cond_192
    const-wide v8, 0xd203169dL

    cmp-long v1, v6, v8

    if-nez v1, :cond_1a2

    .line 1184
    const/16 v1, 0x100

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1188
    :cond_1a2
    const-wide v8, 0xd20316bbL

    cmp-long v1, v6, v8

    if-nez v1, :cond_1b2

    .line 1189
    const/16 v1, 0x231

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1193
    :cond_1b2
    const-wide v8, 0x96684c9dL

    cmp-long v1, v6, v8

    if-nez v1, :cond_1c1

    .line 1194
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1198
    :cond_1c1
    const-wide/32 v8, 0x3c0c8ea1

    cmp-long v1, v6, v8

    if-nez v1, :cond_1cf

    .line 1200
    const/16 v1, 0x100

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1204
    :cond_1cf
    const-wide/32 v8, 0x3d3dbba1

    cmp-long v1, v6, v8

    if-nez v1, :cond_1dd

    .line 1206
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1210
    :cond_1dd
    const-wide/32 v8, 0x3d3db949

    cmp-long v1, v6, v8

    if-nez v1, :cond_1eb

    .line 1212
    const/16 v1, 0x100

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1216
    :cond_1eb
    const-wide/32 v8, 0x43339ca1

    cmp-long v1, v6, v8

    if-nez v1, :cond_1f9

    .line 1218
    const/16 v1, 0x100

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1222
    :cond_1f9
    const-wide/32 v8, 0x3c0c8ea1

    cmp-long v1, v6, v8

    if-nez v1, :cond_207

    .line 1223
    const/16 v1, 0x231

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1227
    :cond_207
    const-wide/32 v8, 0x3c0c8efb

    cmp-long v1, v6, v8

    if-nez v1, :cond_215

    .line 1228
    const/16 v1, 0x231

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1232
    :cond_215
    const-wide v8, 0x25e73701bL

    cmp-long v1, v6, v8

    if-nez v1, :cond_225

    .line 1234
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1238
    :cond_225
    const-wide/32 v8, 0x4dee313d

    cmp-long v1, v6, v8

    if-nez v1, :cond_233

    .line 1239
    const/16 v1, 0x231

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1243
    :cond_233
    const-wide v8, 0xa505df1bL

    cmp-long v1, v6, v8

    if-nez v1, :cond_243

    .line 1245
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1249
    :cond_243
    const-wide v8, 0x873885d3L

    cmp-long v1, v6, v8

    if-nez v1, :cond_252

    .line 1250
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1254
    :cond_252
    const-wide v8, 0x8142a4d3L

    cmp-long v1, v6, v8

    if-nez v1, :cond_262

    .line 1256
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1260
    :cond_262
    const-wide v8, 0xa506d533L

    cmp-long v1, v6, v8

    if-nez v1, :cond_272

    .line 1262
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1266
    :cond_272
    const-wide v8, 0xd56f2b94L

    cmp-long v1, v6, v8

    if-nez v1, :cond_282

    .line 1268
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1272
    :cond_282
    const-wide/32 v8, 0x5e399794

    cmp-long v1, v6, v8

    if-nez v1, :cond_290

    .line 1274
    const/16 v1, 0x100

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1278
    :cond_290
    const-wide v8, 0xd5691114L

    cmp-long v1, v6, v8

    if-nez v1, :cond_2a0

    .line 1280
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1284
    :cond_2a0
    const-wide v8, 0xd56e4134L

    cmp-long v1, v6, v8

    if-nez v1, :cond_2b0

    .line 1286
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1290
    :cond_2b0
    const-wide v8, 0xa2681b02L

    cmp-long v1, v6, v8

    if-nez v1, :cond_2c0

    .line 1292
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1296
    :cond_2c0
    const-wide v8, 0xa2681adcL

    cmp-long v1, v6, v8

    if-nez v1, :cond_2cf

    .line 1297
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1301
    :cond_2cf
    const-wide v8, 0x908677dcL

    cmp-long v1, v6, v8

    if-nez v1, :cond_2df

    .line 1303
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1307
    :cond_2df
    const-wide v8, 0x8a9096dcL

    cmp-long v1, v6, v8

    if-nez v1, :cond_2ef

    .line 1309
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1313
    :cond_2ef
    const-wide/32 v8, 0x3c0c8ea1

    cmp-long v1, v6, v8

    if-nez v1, :cond_2fd

    .line 1315
    const/16 v1, 0x123

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1319
    :cond_2fd
    const-wide/32 v8, 0x3c0c9e41

    cmp-long v1, v6, v8

    if-nez v1, :cond_30a

    .line 1320
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1324
    :cond_30a
    const-wide/32 v8, 0x47f850a1

    cmp-long v1, v6, v8

    if-nez v1, :cond_316

    .line 1326
    const/16 v1, 0x100

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    :cond_316
    const-wide/32 v8, 0x3dd65221

    cmp-long v1, v6, v8

    if-nez v1, :cond_323

    .line 1330
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1334
    :cond_323
    const-wide v8, 0xd56f2b94L

    cmp-long v1, v6, v8

    if-nez v1, :cond_332

    .line 1335
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1339
    :cond_332
    const-wide/32 v8, 0x4addf777

    cmp-long v1, v6, v8

    if-nez v1, :cond_340

    .line 1340
    const/16 v1, 0x231

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1344
    :cond_340
    const-wide/32 v8, 0x4b0bc607

    cmp-long v1, v6, v8

    if-nez v1, :cond_34d

    .line 1345
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1349
    :cond_34d
    const-wide/32 v8, 0x4b0bbde7

    cmp-long v1, v6, v8

    if-nez v1, :cond_359

    .line 1351
    const/16 v1, 0x100

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 1354
    :cond_359
    const-wide/32 v8, 0x4b0bbe37

    cmp-long v1, v6, v8

    if-nez v1, :cond_366

    .line 1355
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1359
    :cond_366
    const-wide/32 v8, 0x4a7327b7

    cmp-long v1, v6, v8

    if-nez v1, :cond_373

    .line 1360
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1364
    :cond_373
    const-wide/32 v8, 0x4b0be0fb

    cmp-long v1, v6, v8

    if-nez v1, :cond_380

    .line 1365
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_73

    .line 1369
    :cond_380
    const-wide/32 v8, 0x4ab03085

    cmp-long v1, v6, v8

    if-nez v1, :cond_38e

    .line 1370
    const/16 v1, 0x231

    invoke-virtual {v3, v1, v0, v2, v5}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_73

    .line 1375
    :cond_38e
    const-string v0, "LicenseValidator"

    const-string v1, "Unknown response code for license check."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1376
    invoke-virtual {v3}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_73
.end method
