.class public final Lcom/a/a/a/a/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/a/a/a/s;


# static fields
.field private static final a:Ljava/lang/String; = "ServerManagedPolicy"

.field private static final b:Ljava/lang/String; = "com.android.vending.licensing.ServerManagedPolicy"

.field private static final f:Ljava/lang/String; = "lastResponse"

.field private static final g:Ljava/lang/String; = "validityTimestamp"

.field private static final h:Ljava/lang/String; = "retryUntil"

.field private static final i:Ljava/lang/String; = "maxRetries"

.field private static final j:Ljava/lang/String; = "retryCount"

.field private static final k:Ljava/lang/String; = "rawData"

.field private static final l:Ljava/lang/String; = "0"

.field private static final m:Ljava/lang/String; = "0"

.field private static final n:Ljava/lang/String; = "0"

.field private static final o:Ljava/lang/String; = "0"

.field private static final p:J = 0xea60L


# instance fields
.field private q:J

.field private r:J

.field private s:J

.field private t:J

.field private u:J

.field private v:I

.field private w:Lcom/a/a/a/a/t;

.field private x:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/a/a/a/a/r;)V
    .registers 6

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->u:J

    .line 78
    const-string v0, "com.android.vending.licensing.ServerManagedPolicy"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 79
    new-instance v1, Lcom/a/a/a/a/t;

    invoke-direct {v1, v0, p2}, Lcom/a/a/a/a/t;-><init>(Landroid/content/SharedPreferences;Lcom/a/a/a/a/r;)V

    iput-object v1, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    .line 80
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "rawData"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/v;->x:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "lastResponse"

    const/16 v2, 0x123

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/a/a/a/a/v;->v:I

    .line 83
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "validityTimestamp"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->q:J

    .line 85
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "retryUntil"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->r:J

    .line 86
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "maxRetries"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->s:J

    .line 87
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "retryCount"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->t:J

    .line 89
    return-void
.end method

.method private a(I)V
    .registers 5

    .prologue
    .line 146
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->u:J

    .line 147
    iput p1, p0, Lcom/a/a/a/a/v;->v:I

    .line 148
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "lastResponse"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    return-void
.end method

.method private a(J)V
    .registers 6

    .prologue
    .line 158
    iput-wide p1, p0, Lcom/a/a/a/a/v;->t:J

    .line 159
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "retryCount"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 135
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "rawData"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 176
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_16

    move-result-object v0

    .line 184
    :goto_8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->q:J

    .line 185
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "validityTimestamp"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void

    .line 179
    :catch_16
    move-exception v0

    const-string v0, "ServerManagedPolicy"

    const-string v1, "License validity timestamp (VT) missing, caching for a minute"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p1

    goto :goto_8
.end method

.method private c()J
    .registers 3

    .prologue
    .line 163
    iget-wide v0, p0, Lcom/a/a/a/a/v;->t:J

    return-wide v0
.end method

.method private c(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 202
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_16

    move-result-object v0

    .line 210
    :goto_8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->r:J

    .line 211
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "retryUntil"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-void

    .line 205
    :catch_16
    move-exception v0

    const-string v0, "ServerManagedPolicy"

    const-string v1, "License retry timestamp (GT) missing, grace period disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const-string p1, "0"

    .line 207
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_8
.end method

.method private d()J
    .registers 3

    .prologue
    .line 189
    iget-wide v0, p0, Lcom/a/a/a/a/v;->q:J

    return-wide v0
.end method

.method private d(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 228
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_16

    move-result-object v0

    .line 236
    :goto_8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->s:J

    .line 237
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "maxRetries"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    return-void

    .line 231
    :catch_16
    move-exception v0

    const-string v0, "ServerManagedPolicy"

    const-string v1, "Licence retry count (GR) missing, grace period disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const-string p1, "0"

    .line 233
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_8
.end method

.method private e()J
    .registers 3

    .prologue
    .line 215
    iget-wide v0, p0, Lcom/a/a/a/a/v;->r:J

    return-wide v0
.end method

.method private static e(Ljava/lang/String;)Ljava/util/Map;
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 272
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 274
    :try_start_6
    const-string v2, "&"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 275
    array-length v3, v2

    :goto_d
    if-ge v0, v3, :cond_2b

    aget-object v4, v2, v0

    .line 276
    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 277
    const/4 v5, 0x0

    aget-object v5, v4, v5

    const/4 v6, 0x1

    aget-object v4, v4, v6

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_20} :catch_23

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 280
    :catch_23
    move-exception v0

    const-string v0, "ServerManagedPolicy"

    const-string v2, "Invalid syntax error while decoding extras data from server."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_2b
    return-object v1
.end method

.method private f()J
    .registers 3

    .prologue
    .line 241
    iget-wide v0, p0, Lcom/a/a/a/a/v;->s:J

    return-wide v0
.end method


# virtual methods
.method public final a(ILcom/a/a/a/a/u;)V
    .registers 7

    .prologue
    .line 109
    const/16 v0, 0x123

    if-eq p1, v0, :cond_4f

    .line 110
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/v;->a(J)V

    .line 115
    :goto_9
    const/16 v0, 0x100

    if-ne p1, v0, :cond_58

    .line 117
    iget-object v0, p2, Lcom/a/a/a/a/u;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/a/a/v;->e(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 118
    iput p1, p0, Lcom/a/a/a/a/v;->v:I

    .line 119
    const-string v0, "VT"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/a/a/a/a/v;->b(Ljava/lang/String;)V

    .line 120
    const-string v0, "GT"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/a/a/a/a/v;->c(Ljava/lang/String;)V

    .line 121
    const-string v0, "GR"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/a/a/a/a/v;->d(Ljava/lang/String;)V

    .line 1146
    :cond_36
    :goto_36
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/v;->u:J

    .line 1147
    iput p1, p0, Lcom/a/a/a/a/v;->v:I

    .line 1148
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    const-string v1, "lastResponse"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    invoke-virtual {v0}, Lcom/a/a/a/a/t;->a()V

    .line 132
    return-void

    .line 112
    :cond_4f
    iget-wide v0, p0, Lcom/a/a/a/a/v;->t:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/v;->a(J)V

    goto :goto_9

    .line 122
    :cond_58
    const/16 v0, 0x231

    if-ne p1, v0, :cond_36

    .line 124
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/v;->b(Ljava/lang/String;)V

    .line 125
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/v;->c(Ljava/lang/String;)V

    .line 126
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/v;->d(Ljava/lang/String;)V

    .line 127
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/a/a/a/a/v;->a(Ljava/lang/String;)V

    goto :goto_36
.end method

.method public final a(Lcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 294
    new-instance v0, Lcom/a/a/a/a/w;

    invoke-direct {v0, p1, p2, p3}, Lcom/a/a/a/a/w;-><init>(Lcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    new-instance v1, Lcom/a/c/k;

    invoke-direct {v1}, Lcom/a/c/k;-><init>()V

    .line 2509
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 2528
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 2529
    invoke-virtual {v1, v0, v2, v3}, Lcom/a/c/k;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    .line 2530
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 296
    invoke-direct {p0, v0}, Lcom/a/a/a/a/v;->a(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/a/a/a/a/v;->w:Lcom/a/a/a/a/t;

    invoke-virtual {v0}, Lcom/a/a/a/a/t;->a()V

    .line 298
    return-void
.end method

.method public final a()Z
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 255
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 256
    iget v4, p0, Lcom/a/a/a/a/v;->v:I

    const/16 v5, 0x100

    if-ne v4, v5, :cond_13

    .line 258
    iget-wide v4, p0, Lcom/a/a/a/a/v;->q:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_33

    .line 268
    :cond_12
    :goto_12
    return v0

    .line 262
    :cond_13
    iget v4, p0, Lcom/a/a/a/a/v;->v:I

    const/16 v5, 0x123

    if-ne v4, v5, :cond_33

    iget-wide v4, p0, Lcom/a/a/a/a/v;->u:J

    const-wide/32 v6, 0xea60

    add-long/2addr v4, v6

    cmp-long v4, v2, v4

    if-gez v4, :cond_33

    .line 266
    iget-wide v4, p0, Lcom/a/a/a/a/v;->r:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_12

    iget-wide v2, p0, Lcom/a/a/a/a/v;->t:J

    iget-wide v4, p0, Lcom/a/a/a/a/v;->s:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_12

    move v0, v1

    goto :goto_12

    :cond_33
    move v0, v1

    .line 268
    goto :goto_12
.end method

.method public final b()Lcom/a/a/a/a/w;
    .registers 5

    .prologue
    .line 287
    new-instance v0, Lcom/a/c/k;

    invoke-direct {v0}, Lcom/a/c/k;-><init>()V

    .line 288
    iget-object v1, p0, Lcom/a/a/a/a/v;->x:Ljava/lang/String;

    const-class v2, Lcom/a/a/a/a/w;

    .line 1715
    if-nez v1, :cond_17

    .line 1716
    const/4 v0, 0x0

    .line 1692
    :goto_c
    invoke-static {v2}, Lcom/a/c/b/ap;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 288
    check-cast v0, Lcom/a/a/a/a/w;

    .line 289
    return-object v0

    .line 1718
    :cond_17
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1769
    new-instance v1, Lcom/a/c/d/a;

    invoke-direct {v1, v3}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 1770
    invoke-virtual {v0, v1, v2}, Lcom/a/c/k;->a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 1771
    invoke-static {v0, v1}, Lcom/a/c/k;->a(Ljava/lang/Object;Lcom/a/c/d/a;)V

    goto :goto_c
.end method
