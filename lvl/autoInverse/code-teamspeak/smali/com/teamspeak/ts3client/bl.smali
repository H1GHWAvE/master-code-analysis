.class public final Lcom/teamspeak/ts3client/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ANDROID_HOLO_BLUE:I = 0x7f0b0000

.field public static final AliceBlue:I = 0x7f0b0001

.field public static final AntiqueWhite:I = 0x7f0b0002

.field public static final Aqua:I = 0x7f0b0003

.field public static final Aquamarine:I = 0x7f0b0004

.field public static final Azure:I = 0x7f0b0005

.field public static final Beige:I = 0x7f0b0006

.field public static final Bisque:I = 0x7f0b0007

.field public static final Black:I = 0x7f0b0008

.field public static final BlanchedAlmond:I = 0x7f0b0009

.field public static final Blue:I = 0x7f0b000a

.field public static final BlueViolet:I = 0x7f0b000b

.field public static final Brown:I = 0x7f0b000c

.field public static final BurlyWood:I = 0x7f0b000d

.field public static final CadetBlue:I = 0x7f0b000e

.field public static final Chartreuse:I = 0x7f0b000f

.field public static final Chocolate:I = 0x7f0b0010

.field public static final Coral:I = 0x7f0b0011

.field public static final CornflowerBlue:I = 0x7f0b0012

.field public static final Cornsilk:I = 0x7f0b0013

.field public static final Crimson:I = 0x7f0b0014

.field public static final Cyan:I = 0x7f0b0015

.field public static final DarkBlue:I = 0x7f0b0016

.field public static final DarkCyan:I = 0x7f0b0017

.field public static final DarkGoldenrod:I = 0x7f0b0018

.field public static final DarkGray:I = 0x7f0b0019

.field public static final DarkGreen:I = 0x7f0b001a

.field public static final DarkKhaki:I = 0x7f0b001b

.field public static final DarkMagenta:I = 0x7f0b001c

.field public static final DarkOliveGreen:I = 0x7f0b001d

.field public static final DarkOrange:I = 0x7f0b001e

.field public static final DarkOrchid:I = 0x7f0b001f

.field public static final DarkRed:I = 0x7f0b0020

.field public static final DarkSalmon:I = 0x7f0b0021

.field public static final DarkSeaGreen:I = 0x7f0b0022

.field public static final DarkSlateBlue:I = 0x7f0b0023

.field public static final DarkSlateGray:I = 0x7f0b0024

.field public static final DarkTurquoise:I = 0x7f0b0025

.field public static final DarkViolet:I = 0x7f0b0026

.field public static final DeepPink:I = 0x7f0b0027

.field public static final DeepSkyBlue:I = 0x7f0b0028

.field public static final DimGray:I = 0x7f0b0029

.field public static final DodgerBlue:I = 0x7f0b002a

.field public static final FireBrick:I = 0x7f0b002b

.field public static final FloralWhite:I = 0x7f0b002c

.field public static final ForestGreen:I = 0x7f0b002d

.field public static final Fuchsia:I = 0x7f0b002e

.field public static final Gainsboro:I = 0x7f0b002f

.field public static final GhostWhite:I = 0x7f0b0030

.field public static final Gold:I = 0x7f0b0031

.field public static final Goldenrod:I = 0x7f0b0032

.field public static final Gray:I = 0x7f0b0033

.field public static final Green:I = 0x7f0b0034

.field public static final GreenYellow:I = 0x7f0b0035

.field public static final Honeydew:I = 0x7f0b0036

.field public static final HotPink:I = 0x7f0b0037

.field public static final IndianRed:I = 0x7f0b0038

.field public static final Indigo:I = 0x7f0b0039

.field public static final Ivory:I = 0x7f0b003a

.field public static final Khaki:I = 0x7f0b003b

.field public static final Lavender:I = 0x7f0b003c

.field public static final LavenderBlush:I = 0x7f0b003d

.field public static final LawnGreen:I = 0x7f0b003e

.field public static final LemonChiffon:I = 0x7f0b003f

.field public static final LightBlue:I = 0x7f0b0040

.field public static final LightCoral:I = 0x7f0b0041

.field public static final LightCyan:I = 0x7f0b0042

.field public static final LightGoldenrodYellow:I = 0x7f0b0043

.field public static final LightGreen:I = 0x7f0b0044

.field public static final LightGrey:I = 0x7f0b0045

.field public static final LightPink:I = 0x7f0b0046

.field public static final LightSalmon:I = 0x7f0b0047

.field public static final LightSeaGreen:I = 0x7f0b0048

.field public static final LightSkyBlue:I = 0x7f0b0049

.field public static final LightSlateGray:I = 0x7f0b004a

.field public static final LightSteelBlue:I = 0x7f0b004b

.field public static final LightYellow:I = 0x7f0b004c

.field public static final Lime:I = 0x7f0b004d

.field public static final LimeGreen:I = 0x7f0b004e

.field public static final Linen:I = 0x7f0b004f

.field public static final Magenta:I = 0x7f0b0050

.field public static final Maroon:I = 0x7f0b0051

.field public static final MediumAquamarine:I = 0x7f0b0052

.field public static final MediumBlue:I = 0x7f0b0053

.field public static final MediumOrchid:I = 0x7f0b0054

.field public static final MediumPurple:I = 0x7f0b0055

.field public static final MediumSeaGreen:I = 0x7f0b0056

.field public static final MediumSlateBlue:I = 0x7f0b0057

.field public static final MediumSpringGreen:I = 0x7f0b0058

.field public static final MediumTurquoise:I = 0x7f0b0059

.field public static final MediumVioletRed:I = 0x7f0b005a

.field public static final MidnightBlue:I = 0x7f0b005b

.field public static final MintCream:I = 0x7f0b005c

.field public static final MistyRose:I = 0x7f0b005d

.field public static final Moccasin:I = 0x7f0b005e

.field public static final NavajoWhite:I = 0x7f0b005f

.field public static final Navy:I = 0x7f0b0060

.field public static final OldLace:I = 0x7f0b0061

.field public static final Olive:I = 0x7f0b0062

.field public static final OliveDrab:I = 0x7f0b0063

.field public static final Orange:I = 0x7f0b0064

.field public static final OrangeRed:I = 0x7f0b0065

.field public static final Orchid:I = 0x7f0b0066

.field public static final PaleGoldenrod:I = 0x7f0b0067

.field public static final PaleGreen:I = 0x7f0b0068

.field public static final PaleTurquoise:I = 0x7f0b0069

.field public static final PaleVioletRed:I = 0x7f0b006a

.field public static final PapayaWhip:I = 0x7f0b006b

.field public static final PeachPuff:I = 0x7f0b006c

.field public static final Peru:I = 0x7f0b006d

.field public static final Pink:I = 0x7f0b006e

.field public static final Plum:I = 0x7f0b006f

.field public static final PowderBlue:I = 0x7f0b0070

.field public static final Purple:I = 0x7f0b0071

.field public static final Red:I = 0x7f0b0072

.field public static final RosyBrown:I = 0x7f0b0073

.field public static final RoyalBlue:I = 0x7f0b0074

.field public static final SaddleBrown:I = 0x7f0b0075

.field public static final Salmon:I = 0x7f0b0076

.field public static final SandyBrown:I = 0x7f0b0077

.field public static final SeaGreen:I = 0x7f0b0078

.field public static final Seashell:I = 0x7f0b0079

.field public static final SettingsBLUE:I = 0x7f0b007a

.field public static final SettingsBlack:I = 0x7f0b007b

.field public static final Sienna:I = 0x7f0b007c

.field public static final Silver:I = 0x7f0b007d

.field public static final SkyBlue:I = 0x7f0b007e

.field public static final SlateBlue:I = 0x7f0b007f

.field public static final SlateGray:I = 0x7f0b0080

.field public static final Snow:I = 0x7f0b0081

.field public static final SpringGreen:I = 0x7f0b0082

.field public static final SteelBlue:I = 0x7f0b0083

.field public static final Tan:I = 0x7f0b0084

.field public static final Teal:I = 0x7f0b0085

.field public static final Thistle:I = 0x7f0b0086

.field public static final Tomato:I = 0x7f0b0087

.field public static final Turquoise:I = 0x7f0b0088

.field public static final Violet:I = 0x7f0b0089

.field public static final Wheat:I = 0x7f0b008a

.field public static final White:I = 0x7f0b008b

.field public static final WhiteSmoke:I = 0x7f0b008c

.field public static final Yellow:I = 0x7f0b008d

.field public static final YellowGreen:I = 0x7f0b008e

.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0b00e9

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0b00ea

.field public static final abc_color_highlight_material:I = 0x7f0b00eb

.field public static final abc_input_method_navigation_guard:I = 0x7f0b008f

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0b00ec

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0b00ed

.field public static final abc_primary_text_material_dark:I = 0x7f0b00ee

.field public static final abc_primary_text_material_light:I = 0x7f0b00ef

.field public static final abc_search_url_text:I = 0x7f0b00f0

.field public static final abc_search_url_text_normal:I = 0x7f0b0090

.field public static final abc_search_url_text_pressed:I = 0x7f0b0091

.field public static final abc_search_url_text_selected:I = 0x7f0b0092

.field public static final abc_secondary_text_material_dark:I = 0x7f0b00f1

.field public static final abc_secondary_text_material_light:I = 0x7f0b00f2

.field public static final accent_material_dark:I = 0x7f0b0093

.field public static final accent_material_light:I = 0x7f0b0094

.field public static final aqua:I = 0x7f0b0095

.field public static final background_floating_material_dark:I = 0x7f0b0096

.field public static final background_floating_material_light:I = 0x7f0b0097

.field public static final background_material_dark:I = 0x7f0b0098

.field public static final background_material_light:I = 0x7f0b0099

.field public static final black:I = 0x7f0b009a

.field public static final blue:I = 0x7f0b009b

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0b009c

.field public static final bright_foreground_disabled_material_light:I = 0x7f0b009d

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0b009e

.field public static final bright_foreground_inverse_material_light:I = 0x7f0b009f

.field public static final bright_foreground_material_dark:I = 0x7f0b00a0

.field public static final bright_foreground_material_light:I = 0x7f0b00a1

.field public static final button_material_dark:I = 0x7f0b00a2

.field public static final button_material_light:I = 0x7f0b00a3

.field public static final design_fab_shadow_end_color:I = 0x7f0b00a4

.field public static final design_fab_shadow_mid_color:I = 0x7f0b00a5

.field public static final design_fab_shadow_start_color:I = 0x7f0b00a6

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0b00a7

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0b00a8

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0b00a9

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0b00aa

.field public static final design_snackbar_background_color:I = 0x7f0b00ab

.field public static final design_textinput_error_color:I = 0x7f0b00ac

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0b00ad

.field public static final dim_foreground_disabled_material_light:I = 0x7f0b00ae

.field public static final dim_foreground_material_dark:I = 0x7f0b00af

.field public static final dim_foreground_material_light:I = 0x7f0b00b0

.field public static final foreground_material_dark:I = 0x7f0b00b1

.field public static final foreground_material_light:I = 0x7f0b00b2

.field public static final fuchsia:I = 0x7f0b00b3

.field public static final gray:I = 0x7f0b00b4

.field public static final green:I = 0x7f0b00b5

.field public static final highlighted_text_material_dark:I = 0x7f0b00b6

.field public static final highlighted_text_material_light:I = 0x7f0b00b7

.field public static final hint_foreground_material_dark:I = 0x7f0b00b8

.field public static final hint_foreground_material_light:I = 0x7f0b00b9

.field public static final lime:I = 0x7f0b00ba

.field public static final maroon:I = 0x7f0b00bb

.field public static final material_blue_grey_800:I = 0x7f0b00bc

.field public static final material_blue_grey_900:I = 0x7f0b00bd

.field public static final material_blue_grey_950:I = 0x7f0b00be

.field public static final material_deep_teal_200:I = 0x7f0b00bf

.field public static final material_deep_teal_500:I = 0x7f0b00c0

.field public static final material_grey_100:I = 0x7f0b00c1

.field public static final material_grey_300:I = 0x7f0b00c2

.field public static final material_grey_50:I = 0x7f0b00c3

.field public static final material_grey_600:I = 0x7f0b00c4

.field public static final material_grey_800:I = 0x7f0b00c5

.field public static final material_grey_850:I = 0x7f0b00c6

.field public static final material_grey_900:I = 0x7f0b00c7

.field public static final navy:I = 0x7f0b00c8

.field public static final olive:I = 0x7f0b00c9

.field public static final pressed_teamspeak:I = 0x7f0b00ca

.field public static final primary:I = 0x7f0b00cb

.field public static final primary_button:I = 0x7f0b00cc

.field public static final primary_button_pressed:I = 0x7f0b00cd

.field public static final primary_button_shadow:I = 0x7f0b00ce

.field public static final primary_button_shadow_highlite:I = 0x7f0b00cf

.field public static final primary_dark:I = 0x7f0b00d0

.field public static final primary_dark_material_dark:I = 0x7f0b00d1

.field public static final primary_dark_material_light:I = 0x7f0b00d2

.field public static final primary_material_dark:I = 0x7f0b00d3

.field public static final primary_material_light:I = 0x7f0b00d4

.field public static final primary_text_default_material_dark:I = 0x7f0b00d5

.field public static final primary_text_default_material_light:I = 0x7f0b00d6

.field public static final primary_text_disabled_material_dark:I = 0x7f0b00d7

.field public static final primary_text_disabled_material_light:I = 0x7f0b00d8

.field public static final purple:I = 0x7f0b00d9

.field public static final red:I = 0x7f0b00da

.field public static final ripple_material_dark:I = 0x7f0b00db

.field public static final ripple_material_light:I = 0x7f0b00dc

.field public static final secondary_text_default_material_dark:I = 0x7f0b00dd

.field public static final secondary_text_default_material_light:I = 0x7f0b00de

.field public static final secondary_text_disabled_material_dark:I = 0x7f0b00df

.field public static final secondary_text_disabled_material_light:I = 0x7f0b00e0

.field public static final silver:I = 0x7f0b00e1

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0b00e2

.field public static final switch_thumb_disabled_material_light:I = 0x7f0b00e3

.field public static final switch_thumb_material_dark:I = 0x7f0b00f3

.field public static final switch_thumb_material_light:I = 0x7f0b00f4

.field public static final switch_thumb_normal_material_dark:I = 0x7f0b00e4

.field public static final switch_thumb_normal_material_light:I = 0x7f0b00e5

.field public static final teal:I = 0x7f0b00e6

.field public static final white:I = 0x7f0b00e7

.field public static final yellow:I = 0x7f0b00e8


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
