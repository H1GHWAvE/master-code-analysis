.class public final Lcom/teamspeak/ts3client/c;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/d/c;
.implements Lcom/teamspeak/ts3client/data/w;


# static fields
.field public static a:Lcom/teamspeak/ts3client/c;

.field public static b:Lcom/teamspeak/ts3client/data/a;


# instance fields
.field private at:Landroid/widget/TableLayout;

.field private au:Landroid/webkit/WebView;

.field private av:Ljava/lang/String;

.field private aw:Landroid/widget/RelativeLayout;

.field private ax:Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private c:Lcom/teamspeak/ts3client/Ts3Application;

.field private d:Landroid/support/v4/app/bi;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TableRow;

.field private m:Landroid/widget/TableRow;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/c;)Landroid/support/v4/app/bi;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->d:Landroid/support/v4/app/bi;

    return-object v0
.end method

.method private static a(Lcom/teamspeak/ts3client/data/a;)Lcom/teamspeak/ts3client/c;
    .registers 2

    .prologue
    .line 66
    sget-object v0, Lcom/teamspeak/ts3client/c;->b:Lcom/teamspeak/ts3client/data/a;

    if-ne v0, p0, :cond_7

    .line 67
    sget-object v0, Lcom/teamspeak/ts3client/c;->a:Lcom/teamspeak/ts3client/c;

    .line 72
    :goto_6
    return-object v0

    .line 69
    :cond_7
    new-instance v0, Lcom/teamspeak/ts3client/c;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/c;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/c;->a:Lcom/teamspeak/ts3client/c;

    .line 70
    sput-object p0, Lcom/teamspeak/ts3client/c;->b:Lcom/teamspeak/ts3client/data/a;

    .line 72
    sget-object v0, Lcom/teamspeak/ts3client/c;->a:Lcom/teamspeak/ts3client/c;

    goto :goto_6
.end method

.method static synthetic a()Lcom/teamspeak/ts3client/data/a;
    .registers 1

    .prologue
    .line 42
    sget-object v0, Lcom/teamspeak/ts3client/c;->b:Lcom/teamspeak/ts3client/data/a;

    return-object v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/c;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 42
    iput-object p1, p0, Lcom/teamspeak/ts3client/c;->av:Ljava/lang/String;

    return-object p1
.end method

.method private a(Z)V
    .registers 4

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 215
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/g;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/g;-><init>(Lcom/teamspeak/ts3client/c;Z)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 287
    :cond_12
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/c;)Landroid/webkit/WebView;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic b()Lcom/teamspeak/ts3client/c;
    .registers 1

    .prologue
    .line 42
    sget-object v0, Lcom/teamspeak/ts3client/c;->a:Lcom/teamspeak/ts3client/c;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 203
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/f;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/f;-><init>(Lcom/teamspeak/ts3client/c;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 211
    :cond_12
    return-void
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/c;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/c;)Landroid/widget/TableRow;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->m:Landroid/widget/TableRow;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/c;)Landroid/widget/TableLayout;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->at:Landroid/widget/TableLayout;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/c;)Landroid/widget/TableRow;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->l:Landroid/widget/TableRow;

    return-object v0
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->j:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/c;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->av:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x41b00000    # 22.0f

    const/4 v3, 0x0

    .line 113
    const v0, 0x7f030024

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 10688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 114
    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->d:Landroid/support/v4/app/bi;

    .line 115
    const v0, 0x7f0c00cf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->aw:Landroid/widget/RelativeLayout;

    .line 116
    const v0, 0x7f0c00d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->at:Landroid/widget/TableLayout;

    .line 117
    const v0, 0x7f0c00d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->e:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f0c00da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->f:Landroid/widget/TextView;

    .line 119
    const v0, 0x7f0c00dd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->g:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f0c00de

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->l:Landroid/widget/TableRow;

    .line 121
    const v0, 0x7f0c00e0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->h:Landroid/widget/TextView;

    .line 122
    const v0, 0x7f0c00d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->m:Landroid/widget/TableRow;

    .line 123
    const v0, 0x7f0c00d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->i:Landroid/widget/TextView;

    .line 124
    const v0, 0x7f0c00e3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->j:Landroid/widget/TextView;

    .line 125
    const v0, 0x7f0c00e6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->k:Landroid/widget/TextView;

    .line 126
    const v0, 0x7f0c00e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->ax:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 127
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->ax:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020075

    invoke-static {v2, v4, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 128
    const v0, 0x7f0c00e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    .line 130
    const-string v0, "channelinfo.name"

    const v2, 0x7f0c00d3

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 131
    const-string v0, "channelinfo.topic"

    const v2, 0x7f0c00d6

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 132
    const-string v0, "channelinfo.codec"

    const v2, 0x7f0c00d9

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 133
    const-string v0, "channelinfo.codecquality"

    const v2, 0x7f0c00dc

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 134
    const-string v0, "channelinfo.type"

    const v2, 0x7f0c00df

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 135
    const-string v0, "channelinfo.currentclients"

    const v2, 0x7f0c00e2

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 136
    const-string v0, "channelinfo.subscription"

    const v2, 0x7f0c00e5

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 137
    const-string v0, "channelinfo.description"

    const v2, 0x7f0c00e7

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/View;I)V

    .line 139
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 140
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 141
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v2, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 142
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 143
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 144
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_124

    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 147
    :cond_124
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    new-instance v2, Lcom/teamspeak/ts3client/d;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d;-><init>(Lcom/teamspeak/ts3client/c;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 187
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->ax:Lcom/teamspeak/ts3client/customs/FloatingButton;

    new-instance v2, Lcom/teamspeak/ts3client/e;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/e;-><init>(Lcom/teamspeak/ts3client/c;)V

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c;->n()V

    .line 196
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 196
    const-string v2, "dialog.channel.info.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 197
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 198
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 107
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 108
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 291
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 292
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 293
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 6

    .prologue
    .line 77
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/UpdateChannel;

    if-eqz v0, :cond_1e

    .line 78
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/UpdateChannel;

    .line 1023
    iget-wide v0, p1, Lcom/teamspeak/ts3client/jni/events/UpdateChannel;->a:J

    .line 79
    sget-object v2, Lcom/teamspeak/ts3client/c;->b:Lcom/teamspeak/ts3client/data/a;

    .line 1087
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 79
    cmp-long v0, v0, v2

    if-nez v0, :cond_1d

    .line 80
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/c;->a(Z)V

    .line 81
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 81
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 92
    :cond_1d
    :goto_1d
    return-void

    .line 85
    :cond_1e
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;

    if-eqz v0, :cond_1d

    .line 86
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;

    .line 3033
    iget v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->a:I

    .line 86
    iget-object v1, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 86
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bK:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    if-ne v0, v1, :cond_1d

    .line 87
    const-string v0, "channelinfo.desc.permerror"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4202
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c;->l()Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 4203
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/f;

    invoke-direct {v2, p0, v0}, Lcom/teamspeak/ts3client/f;-><init>(Lcom/teamspeak/ts3client/c;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 88
    :cond_4c
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 88
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    goto :goto_1d
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9

    .prologue
    .line 297
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->av:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<img src=\"file://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/c;->av:Ljava/lang/String;

    .line 298
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->au:Landroid/webkit/WebView;

    const-string v1, ""

    iget-object v2, p0, Lcom/teamspeak/ts3client/c;->av:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const-string v5, "about:blank"

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->aw:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestFocus()Z

    .line 300
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 9

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 98
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 98
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 99
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 99
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 99
    sget-object v0, Lcom/teamspeak/ts3client/c;->b:Lcom/teamspeak/ts3client/data/a;

    .line 9087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 99
    const-string v6, "ChannelInfoFragment"

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelDescription(JJLjava/lang/String;)I

    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/c;->c:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/c;->a(Z)V

    .line 102
    return-void
.end method
