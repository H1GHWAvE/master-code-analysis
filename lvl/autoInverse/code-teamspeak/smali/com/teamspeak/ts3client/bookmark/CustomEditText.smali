.class public Lcom/teamspeak/ts3client/bookmark/CustomEditText;
.super Landroid/widget/EditText;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->a:Z

    .line 16
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->b:Landroid/graphics/Paint;

    .line 19
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->b:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 20
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->b:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 21
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->b:Landroid/graphics/Paint;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 26
    new-instance v0, Lcom/teamspeak/ts3client/bookmark/d;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/bookmark/d;-><init>(Lcom/teamspeak/ts3client/bookmark/CustomEditText;)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 46
    return-void
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->a:Z

    return v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 8

    .prologue
    const/high16 v1, 0x40a00000    # 5.0f

    .line 52
    invoke-super {p0, p1}, Landroid/widget/EditText;->draw(Landroid/graphics/Canvas;)V

    .line 53
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->a:Z

    if-eqz v0, :cond_1e

    .line 54
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0x5

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x5

    int-to-float v4, v0

    iget-object v5, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->b:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 55
    :cond_1e
    return-void
.end method

.method public setInputerror(Z)V
    .registers 2

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->a:Z

    .line 63
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->requestLayout()V

    .line 64
    return-void
.end method
