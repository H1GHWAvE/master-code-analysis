.class final Lcom/teamspeak/ts3client/bookmark/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/ab;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/teamspeak/ts3client/bookmark/e;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/bookmark/e;Lcom/teamspeak/ts3client/data/ab;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 262
    iput-object p1, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    iput-object p2, p0, Lcom/teamspeak/ts3client/bookmark/n;->a:Lcom/teamspeak/ts3client/data/ab;

    iput-object p3, p0, Lcom/teamspeak/ts3client/bookmark/n;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 7

    .prologue
    const/4 v2, 0x1

    .line 265
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/bookmark/e;->c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 265
    if-nez v0, :cond_c

    .line 294
    :goto_b
    return-void

    .line 267
    :cond_c
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/n;->a:Lcom/teamspeak/ts3client/data/ab;

    .line 1199
    iget v0, v0, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 267
    if-lez v0, :cond_3f

    .line 269
    new-instance v0, Lcom/teamspeak/ts3client/tsdns/f;

    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/n;->a:Lcom/teamspeak/ts3client/data/ab;

    .line 1211
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    .line 269
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/n;->a:Lcom/teamspeak/ts3client/data/ab;

    .line 1219
    iget v2, v2, Lcom/teamspeak/ts3client/data/ab;->p:I

    .line 269
    iget-object v3, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-static {v3}, Lcom/teamspeak/ts3client/bookmark/e;->c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 2061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2198
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 269
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/d/u;->b(Landroid/content/Context;)Z

    move-result v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-static {v5}, Lcom/teamspeak/ts3client/bookmark/e;->c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v5

    .line 3085
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 269
    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/tsdns/f;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/tsdns/i;ZLjava/util/logging/Logger;)V

    .line 270
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/tsdns/f;->a()V

    goto :goto_b

    .line 273
    :cond_3f
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/n;->b:Ljava/lang/String;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 275
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/n;->b:Ljava/lang/String;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 277
    array-length v0, v1

    if-le v0, v2, :cond_9a

    .line 278
    aget-object v0, v1, v2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 282
    :goto_5a
    const-string v2, "\\D"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c7

    .line 284
    const-string v0, "9987"

    move-object v2, v0

    .line 286
    :goto_6d
    new-instance v0, Lcom/teamspeak/ts3client/tsdns/f;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-static {v3}, Lcom/teamspeak/ts3client/bookmark/e;->c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 4061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4198
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 286
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/d/u;->b(Landroid/content/Context;)Z

    move-result v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-static {v5}, Lcom/teamspeak/ts3client/bookmark/e;->c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v5

    .line 5085
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 286
    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/tsdns/f;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/tsdns/i;ZLjava/util/logging/Logger;)V

    .line 287
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/tsdns/f;->a()V

    goto/16 :goto_b

    .line 280
    :cond_9a
    const-string v0, "9987"

    goto :goto_5a

    .line 291
    :cond_9d
    new-instance v0, Lcom/teamspeak/ts3client/tsdns/f;

    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/n;->b:Ljava/lang/String;

    const/16 v2, 0x2703

    iget-object v3, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-static {v3}, Lcom/teamspeak/ts3client/bookmark/e;->c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 6061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6198
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 291
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/d/u;->b(Landroid/content/Context;)Z

    move-result v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/bookmark/n;->c:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-static {v5}, Lcom/teamspeak/ts3client/bookmark/e;->c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v5

    .line 7085
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 291
    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/tsdns/f;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/tsdns/i;ZLjava/util/logging/Logger;)V

    .line 292
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/tsdns/f;->a()V

    goto/16 :goto_b

    :cond_c7
    move-object v2, v0

    goto :goto_6d
.end method
