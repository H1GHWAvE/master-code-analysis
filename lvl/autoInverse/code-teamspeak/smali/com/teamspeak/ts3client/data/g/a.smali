.class public final Lcom/teamspeak/ts3client/data/g/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:I

.field public f:I

.field private g:J

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(JJLjava/lang/String;IJIIIIII)V
    .registers 20

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide p1, p0, Lcom/teamspeak/ts3client/data/g/a;->g:J

    .line 20
    iput-wide p3, p0, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 21
    iput-object p5, p0, Lcom/teamspeak/ts3client/data/g/a;->b:Ljava/lang/String;

    .line 22
    iput p6, p0, Lcom/teamspeak/ts3client/data/g/a;->c:I

    .line 23
    const-wide v2, 0xffffffffL

    and-long/2addr v2, p7

    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/g/a;->d:J

    .line 25
    iput p9, p0, Lcom/teamspeak/ts3client/data/g/a;->h:I

    .line 26
    iput p10, p0, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 27
    move/from16 v0, p11

    iput v0, p0, Lcom/teamspeak/ts3client/data/g/a;->j:I

    .line 28
    move/from16 v0, p12

    iput v0, p0, Lcom/teamspeak/ts3client/data/g/a;->k:I

    .line 29
    move/from16 v0, p13

    iput v0, p0, Lcom/teamspeak/ts3client/data/g/a;->e:I

    .line 30
    move/from16 v0, p14

    iput v0, p0, Lcom/teamspeak/ts3client/data/g/a;->f:I

    .line 31
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/data/g/a;)I
    .registers 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 79
    .line 1070
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 81
    if-eqz v0, :cond_1d

    .line 2070
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 3070
    :goto_8
    iget v3, p1, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 88
    if-eqz v3, :cond_21

    .line 4070
    iget v3, p1, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 93
    :goto_e
    if-ne v0, v3, :cond_27

    .line 6066
    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 7066
    iget-wide v6, p1, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 93
    cmp-long v0, v4, v6

    if-gez v0, :cond_25

    move v0, v1

    .line 95
    :goto_19
    if-eqz v0, :cond_2d

    .line 96
    const/4 v0, -0x1

    .line 98
    :goto_1c
    return v0

    .line 3066
    :cond_1d
    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 84
    long-to-int v0, v4

    goto :goto_8

    .line 5066
    :cond_21
    iget-wide v4, p1, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 91
    long-to-int v3, v4

    goto :goto_e

    :cond_25
    move v0, v2

    .line 93
    goto :goto_19

    :cond_27
    if-ge v0, v3, :cond_2b

    move v0, v1

    goto :goto_19

    :cond_2b
    move v0, v2

    goto :goto_19

    :cond_2d
    move v0, v1

    .line 98
    goto :goto_1c
.end method

.method private b()J
    .registers 3

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/g/a;->d:J

    return-wide v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 42
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->j:I

    return v0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 46
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->e:I

    return v0
.end method

.method private e()I
    .registers 2

    .prologue
    .line 50
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->f:I

    return v0
.end method

.method private f()I
    .registers 2

    .prologue
    .line 54
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->k:I

    return v0
.end method

.method private g()I
    .registers 2

    .prologue
    .line 58
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->h:I

    return v0
.end method

.method private h()J
    .registers 3

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/g/a;->g:J

    return-wide v0
.end method

.method private i()J
    .registers 3

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/g/a;->a:J

    return-wide v0
.end method

.method private j()I
    .registers 2

    .prologue
    .line 70
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->i:I

    return v0
.end method

.method private k()I
    .registers 2

    .prologue
    .line 74
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->c:I

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3
    check-cast p1, Lcom/teamspeak/ts3client/data/g/a;

    .line 8070
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 7081
    if-eqz v0, :cond_1f

    .line 9070
    iget v0, p0, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 10070
    :goto_a
    iget v3, p1, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 7088
    if-eqz v3, :cond_23

    .line 11070
    iget v3, p1, Lcom/teamspeak/ts3client/data/g/a;->i:I

    .line 7093
    :goto_10
    if-ne v0, v3, :cond_29

    .line 13066
    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 14066
    iget-wide v6, p1, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 7093
    cmp-long v0, v4, v6

    if-gez v0, :cond_27

    move v0, v1

    .line 7095
    :goto_1b
    if-eqz v0, :cond_2f

    .line 7096
    const/4 v0, -0x1

    :goto_1e
    return v0

    .line 10066
    :cond_1f
    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 7084
    long-to-int v0, v4

    goto :goto_a

    .line 12066
    :cond_23
    iget-wide v4, p1, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 7091
    long-to-int v3, v4

    goto :goto_10

    :cond_27
    move v0, v2

    .line 7093
    goto :goto_1b

    :cond_29
    if-ge v0, v3, :cond_2d

    move v0, v1

    goto :goto_1b

    :cond_2d
    move v0, v2

    goto :goto_1b

    :cond_2f
    move v0, v1

    .line 3
    goto :goto_1e
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g/a;->b:Ljava/lang/String;

    return-object v0
.end method
