.class public final Lcom/teamspeak/ts3client/data/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Ljava/util/concurrent/CopyOnWriteArrayList;

.field public d:Z

.field public e:J

.field public f:I

.field public g:J

.field public h:I

.field public i:I

.field public j:Z

.field public k:Z

.field public l:I

.field public m:Z

.field n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field s:J

.field private t:Ljava/util/ArrayList;

.field private u:Z

.field private v:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 16
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 20
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    .line 21
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 22
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 23
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 24
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 25
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->i:I

    .line 26
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->j:Z

    .line 27
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 28
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->l:I

    .line 29
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->m:Z

    .line 30
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->n:Z

    .line 31
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 33
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 34
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 35
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->r:Z

    .line 36
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->s:J

    .line 37
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->u:Z

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->v:Ljava/lang/Object;

    .line 42
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;JJIJ)V
    .registers 15

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 16
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 20
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    .line 21
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 22
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 23
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 24
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 25
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->i:I

    .line 26
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->j:Z

    .line 27
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 28
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->l:I

    .line 29
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->m:Z

    .line 30
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->n:Z

    .line 31
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 33
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 34
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 35
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->r:Z

    .line 36
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->s:J

    .line 37
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->u:Z

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->v:Ljava/lang/Object;

    .line 46
    invoke-static {p1}, Lcom/teamspeak/ts3client/data/d/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 47
    iput-wide p2, p0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 48
    iput p6, p0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 49
    iput-wide p4, p0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 50
    iput-wide p7, p0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 51
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJJ)V
    .registers 14

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 16
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 20
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    .line 21
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 22
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 23
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 24
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 25
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->i:I

    .line 26
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->j:Z

    .line 27
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 28
    iput v1, p0, Lcom/teamspeak/ts3client/data/a;->l:I

    .line 29
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->m:Z

    .line 30
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->n:Z

    .line 31
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 33
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 34
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 35
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->r:Z

    .line 36
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/a;->s:J

    .line 37
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->u:Z

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->v:Ljava/lang/Object;

    .line 57
    invoke-static {p1}, Lcom/teamspeak/ts3client/data/d/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 58
    iput-wide p2, p0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 59
    iput-wide p4, p0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 60
    iput-wide p6, p0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 61
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 63
    return-void
.end method

.method private f(Z)V
    .registers 2

    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 156
    return-void
.end method

.method private f(I)Z
    .registers 4

    .prologue
    .line 240
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private g(Z)V
    .registers 2

    .prologue
    .line 163
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 164
    return-void
.end method

.method private l()I
    .registers 2

    .prologue
    .line 71
    iget v0, p0, Lcom/teamspeak/ts3client/data/a;->i:I

    return v0
.end method

.method private m()I
    .registers 2

    .prologue
    .line 79
    iget v0, p0, Lcom/teamspeak/ts3client/data/a;->h:I

    return v0
.end method

.method private n()V
    .registers 3

    .prologue
    .line 91
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 92
    return-void
.end method

.method private o()Z
    .registers 2

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->r:Z

    return v0
.end method

.method private p()Z
    .registers 2

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->j:Z

    return v0
.end method

.method private q()Z
    .registers 2

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->o:Z

    return v0
.end method

.method private r()Z
    .registers 2

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->p:Z

    return v0
.end method

.method private s()Z
    .registers 2

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->q:Z

    return v0
.end method

.method private t()Z
    .registers 2

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->k:Z

    return v0
.end method

.method private u()J
    .registers 3

    .prologue
    .line 188
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/a;->s:J

    return-wide v0
.end method

.method private v()Z
    .registers 2

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->n:Z

    return v0
.end method

.method private w()I
    .registers 2

    .prologue
    .line 210
    iget v0, p0, Lcom/teamspeak/ts3client/data/a;->l:I

    return v0
.end method

.method private x()V
    .registers 2

    .prologue
    .line 279
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    .line 280
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 66
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    .line 68
    return-void
.end method

.method public final a(I)V
    .registers 2

    .prologue
    .line 75
    iput p1, p0, Lcom/teamspeak/ts3client/data/a;->i:I

    .line 76
    return-void
.end method

.method public final a(J)V
    .registers 4

    .prologue
    .line 115
    iput-wide p1, p0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 116
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 107
    invoke-static {p1}, Lcom/teamspeak/ts3client/data/d/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public final a(Z)V
    .registers 2

    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/a;->r:Z

    .line 132
    return-void
.end method

.method public final b()J
    .registers 3

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/a;->b:J

    return-wide v0
.end method

.method public final b(I)V
    .registers 2

    .prologue
    .line 83
    iput p1, p0, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 84
    return-void
.end method

.method public final b(J)V
    .registers 4

    .prologue
    .line 123
    iput-wide p1, p0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 124
    return-void
.end method

.method public final b(Z)V
    .registers 2

    .prologue
    .line 139
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/a;->j:Z

    .line 140
    return-void
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 95
    iget v0, p0, Lcom/teamspeak/ts3client/data/a;->f:I

    return v0
.end method

.method public final c(I)V
    .registers 2

    .prologue
    .line 214
    iput p1, p0, Lcom/teamspeak/ts3client/data/a;->l:I

    .line 215
    return-void
.end method

.method public final c(J)V
    .registers 6

    .prologue
    .line 192
    const-wide v0, 0xffffffffL

    and-long/2addr v0, p1

    iput-wide v0, p0, Lcom/teamspeak/ts3client/data/a;->s:J

    .line 193
    return-void
.end method

.method public final c(Z)V
    .registers 2

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 176
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 100
    return-void
.end method

.method public final d(I)V
    .registers 5

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->u:Z

    if-eqz v0, :cond_24

    .line 219
    const-string v0, "Channel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Add Client "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_24
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 221
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    .line 223
    return-void
.end method

.method public final d(Z)V
    .registers 2

    .prologue
    .line 179
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/a;->m:Z

    .line 180
    return-void
.end method

.method public final e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final e(I)V
    .registers 5

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->u:Z

    if-eqz v0, :cond_24

    .line 227
    const-string v0, "Channel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removed Client "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_24
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    .line 230
    return-void
.end method

.method public final e(Z)V
    .registers 2

    .prologue
    .line 200
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/a;->n:Z

    .line 201
    return-void
.end method

.method public final f()J
    .registers 3

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/a;->g:J

    return-wide v0
.end method

.method public final g()J
    .registers 3

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/a;->e:J

    return-wide v0
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 148
    return-void
.end method

.method public final i()Z
    .registers 2

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->m:Z

    return v0
.end method

.method public final j()Ljava/util/concurrent/CopyOnWriteArrayList;
    .registers 4

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->u:Z

    if-eqz v0, :cond_2a

    .line 234
    const-string v0, "Channel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Clients in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_2a
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public final k()Ljava/util/ArrayList;
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 244
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    if-nez v0, :cond_8

    .line 245
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->t:Ljava/util/ArrayList;

    .line 275
    :goto_7
    return-object v0

    .line 246
    :cond_8
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 246
    const-string v2, "show_squeryclient"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 249
    :try_start_14
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 249
    if-eqz v0, :cond_cb

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 249
    if-eqz v0, :cond_cb

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 5082
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6250
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 5082
    if-eqz v2, :cond_98

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7250
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 8163
    iget-boolean v2, v2, Lcom/teamspeak/ts3client/data/d/v;->c:Z

    .line 5082
    if-nez v2, :cond_98

    move v0, v1

    .line 249
    :goto_41
    if-eqz v0, :cond_cb

    .line 250
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 250
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 10061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10250
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 250
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->cK:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v2, v4}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/f/a;->b(I)I
    :try_end_5c
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_5c} :catch_9b

    move-result v0

    :goto_5d
    move v2, v0

    .line 254
    :goto_5e
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11364
    iget-object v4, v0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 255
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/a;->t:Ljava/util/ArrayList;

    .line 256
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 258
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_78
    :goto_78
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 259
    invoke-virtual {v4, v0}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_78

    .line 262
    if-eqz v3, :cond_9e

    .line 12358
    iget v7, v0, Lcom/teamspeak/ts3client/data/c;->e:I

    .line 263
    if-gt v7, v2, :cond_78

    .line 264
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_78

    .line 5084
    :cond_98
    :try_start_98
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/f/a;->c:Z
    :try_end_9a
    .catch Ljava/lang/Exception; {:try_start_98 .. :try_end_9a} :catch_9b

    goto :goto_41

    :catch_9b
    move-exception v0

    move v2, v1

    goto :goto_5e

    .line 13180
    :cond_9e
    iget v7, v0, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 266
    if-nez v7, :cond_78

    .line 268
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_78

    .line 271
    :cond_a6
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/a;->d:Z

    .line 272
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 273
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_af
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/c;

    .line 274
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/a;->t:Ljava/util/ArrayList;

    .line 13235
    iget v0, v0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 274
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_af

    .line 275
    :cond_c7
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->t:Ljava/util/ArrayList;

    goto/16 :goto_7

    :cond_cb
    move v0, v1

    goto :goto_5d
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    return-object v0
.end method
