.class public final Lcom/teamspeak/ts3client/data/d/i;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/d/h;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/d/h;)V
    .registers 2

    .prologue
    .line 40
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Boolean;
    .registers 9

    .prologue
    const/4 v7, 0x1

    .line 44
    new-array v0, v7, [B

    .line 47
    :try_start_3
    new-instance v1, Ljava/net/DatagramSocket;

    invoke-direct {v1}, Ljava/net/DatagramSocket;-><init>()V

    .line 48
    const/16 v2, 0x2710

    invoke-virtual {v1, v2}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    .line 49
    sget-object v2, Lcom/teamspeak/ts3client/data/d/h;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    .line 50
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ip4:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/d/h;->a(Lcom/teamspeak/ts3client/data/d/h;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 51
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 52
    new-instance v4, Ljava/net/DatagramPacket;

    array-length v5, v3

    sget v6, Lcom/teamspeak/ts3client/data/d/h;->a:I

    invoke-direct {v4, v3, v5, v2, v6}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 53
    invoke-virtual {v1, v4}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 55
    new-instance v2, Ljava/net/DatagramPacket;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 56
    invoke-virtual {v1, v2}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 57
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/net/DatagramPacket;->getLength()I

    move-result v2

    invoke-direct {v0, v3, v4, v2}, Ljava/lang/String;-><init>([BII)V

    .line 58
    invoke-virtual {v1}, Ljava/net/DatagramSocket;->close()V

    .line 59
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 60
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_65
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_65} :catch_67
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_65} :catch_6d
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_65} :catch_73

    move-result-object v0

    .line 69
    :goto_66
    return-object v0

    .line 62
    :catch_67
    move-exception v0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_66

    .line 64
    :catch_6d
    move-exception v0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_66

    .line 66
    :catch_73
    move-exception v0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_66

    .line 69
    :cond_79
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_66
.end method

.method private a(Ljava/lang/Boolean;)V
    .registers 9

    .prologue
    const/4 v4, 0x1

    .line 75
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5c

    .line 78
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/h;->b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 78
    if-eqz v0, :cond_5c

    .line 79
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/h;->b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 79
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Server is blacklisted!"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/h;->b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 80
    const-string v1, "blacklist.warning"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "blacklist.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/h;->b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 81
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 84
    :cond_5c
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/i;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 9

    .prologue
    const/4 v4, 0x1

    .line 40
    check-cast p1, Ljava/lang/Boolean;

    .line 4075
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 4077
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5e

    .line 4078
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/h;->b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4078
    if-eqz v0, :cond_5e

    .line 4079
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/h;->b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 5085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 4079
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Server is blacklisted!"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 4080
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/h;->b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 4080
    const-string v1, "blacklist.warning"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "blacklist.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 4081
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/i;->a:Lcom/teamspeak/ts3client/data/d/h;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/h;->b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 4081
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 40
    :cond_5e
    return-void
.end method
