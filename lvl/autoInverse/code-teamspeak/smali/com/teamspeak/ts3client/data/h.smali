.class final Lcom/teamspeak/ts3client/data/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/c;

.field final synthetic b:Lcom/teamspeak/ts3client/data/g;

.field private c:I

.field private d:I

.field private e:I

.field private f:Landroid/view/View;

.field private g:I


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/data/g;Lcom/teamspeak/ts3client/data/c;)V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 338
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/h;->b:Lcom/teamspeak/ts3client/data/g;

    iput-object p2, p0, Lcom/teamspeak/ts3client/data/h;->a:Lcom/teamspeak/ts3client/data/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    iput v0, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    .line 340
    iput v0, p0, Lcom/teamspeak/ts3client/data/h;->d:I

    .line 341
    iput v0, p0, Lcom/teamspeak/ts3client/data/h;->e:I

    .line 343
    iput v0, p0, Lcom/teamspeak/ts3client/data/h;->g:I

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 12

    .prologue
    const/16 v3, 0x55

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 347
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_24

    .line 348
    iput v7, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    .line 349
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iput v1, p0, Lcom/teamspeak/ts3client/data/h;->g:I

    .line 350
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/teamspeak/ts3client/data/h;->d:I

    .line 351
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/teamspeak/ts3client/data/h;->e:I

    .line 352
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/h;->f:Landroid/view/View;

    .line 354
    :cond_24
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_39

    .line 355
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/teamspeak/ts3client/data/h;->e:I

    .line 356
    iget v1, p0, Lcom/teamspeak/ts3client/data/h;->e:I

    iget v2, p0, Lcom/teamspeak/ts3client/data/h;->d:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    .line 359
    :cond_39
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v1, v0, :cond_45

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v4, :cond_6a

    .line 360
    :cond_45
    iget v0, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    if-le v0, v3, :cond_5b

    .line 361
    new-instance v0, Lcom/teamspeak/ts3client/d/b/a;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/h;->a:Lcom/teamspeak/ts3client/data/c;

    invoke-direct {v0, v1, v7}, Lcom/teamspeak/ts3client/d/b/a;-><init>(Lcom/teamspeak/ts3client/data/c;Z)V

    .line 362
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/h;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/g;->a(Lcom/teamspeak/ts3client/data/g;)Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v2, "ClientActionDialog"

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/d/b/a;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 364
    :cond_5b
    iput v7, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    .line 365
    iput v7, p0, Lcom/teamspeak/ts3client/data/h;->d:I

    .line 366
    iput v7, p0, Lcom/teamspeak/ts3client/data/h;->e:I

    .line 367
    iget v0, p0, Lcom/teamspeak/ts3client/data/h;->g:I

    invoke-virtual {p1, v0, v7, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 368
    invoke-virtual {p1, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 388
    :goto_69
    return v7

    .line 372
    :cond_6a
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/h;->f:Landroid/view/View;

    if-eqz v1, :cond_ba

    .line 373
    iget v1, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    if-nez v1, :cond_78

    .line 374
    const v1, 0x332e64fe

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 376
    :cond_78
    iget v1, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    if-le v1, v3, :cond_82

    .line 377
    const v1, 0x330000ff

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 379
    :cond_82
    iget v1, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    const/16 v2, -0x55

    if-ge v1, v2, :cond_b2

    .line 380
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/h;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/g;->b(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/data/customExpandableListView;

    move-result-object v8

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/teamspeak/ts3client/data/customExpandableListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 381
    iget v0, p0, Lcom/teamspeak/ts3client/data/h;->g:I

    invoke-virtual {p1, v0, v7, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 382
    invoke-virtual {p1, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 383
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/h;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/g;->c(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 383
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/h;->a:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->a(Lcom/teamspeak/ts3client/data/c;)V

    goto :goto_69

    .line 386
    :cond_b2
    iget v1, p0, Lcom/teamspeak/ts3client/data/h;->c:I

    iget v2, p0, Lcom/teamspeak/ts3client/data/h;->g:I

    add-int/2addr v1, v2

    invoke-virtual {p1, v1, v7, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    :cond_ba
    move v7, v0

    .line 388
    goto :goto_69
.end method
