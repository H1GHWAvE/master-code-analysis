.class public final Lcom/teamspeak/ts3client/data/c/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Landroid/graphics/drawable/Drawable;

.field c:Ljava/util/HashMap;

.field private d:Ljava/io/File;

.field private e:Lcom/teamspeak/ts3client/Ts3Application;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 10

    .prologue
    const-wide/16 v6, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/TS3/cache/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->a:Ljava/lang/String;

    .line 16
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->c:Ljava/util/HashMap;

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/d;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1287
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 22
    const-string v2, "/"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/icons/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->a:Ljava/lang/String;

    .line 23
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/d;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->d:Ljava/io/File;

    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_6c

    .line 25
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 26
    :cond_6c
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->c:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/data/c/c;

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/d;->d:Ljava/io/File;

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/d;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v2, v6, v7, v3, v4}, Lcom/teamspeak/ts3client/data/c/c;-><init>(JLjava/io/File;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method

.method private b(J)Z
    .registers 6

    .prologue
    .line 30
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(J)Lcom/teamspeak/ts3client/data/c/c;
    .registers 6

    .prologue
    .line 34
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 35
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/c/c;

    .line 39
    :goto_18
    return-object v0

    .line 37
    :cond_19
    new-instance v0, Lcom/teamspeak/ts3client/data/c/c;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/d;->d:Ljava/io/File;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/d;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/teamspeak/ts3client/data/c/c;-><init>(JLjava/io/File;Lcom/teamspeak/ts3client/Ts3Application;)V

    .line 38
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/d;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/d;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/c/c;

    goto :goto_18
.end method
