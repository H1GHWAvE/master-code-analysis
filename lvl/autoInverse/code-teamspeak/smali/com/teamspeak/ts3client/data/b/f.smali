.class public final Lcom/teamspeak/ts3client/data/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String; = "create table ident (ident_id integer primary key autoincrement, name text not null, identity text not null, nickname text not null, defaultid int not null);"

.field private static final c:Ljava/lang/String; = "ident"

.field private static final d:Ljava/lang/String; = "Teamspeak-Ident"

.field private static final e:I = 0x1

.field private static f:Lcom/teamspeak/ts3client/data/b/e;

.field private static g:Lcom/teamspeak/ts3client/data/b/f;


# instance fields
.field public a:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/teamspeak/ts3client/data/b/f;->f:Lcom/teamspeak/ts3client/data/b/e;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lcom/teamspeak/ts3client/data/b/g;

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/teamspeak/ts3client/data/b/g;-><init>(Lcom/teamspeak/ts3client/data/b/f;Landroid/content/Context;)V

    .line 29
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 30
    return-void
.end method

.method public static declared-synchronized a()Lcom/teamspeak/ts3client/data/b/f;
    .registers 2

    .prologue
    .line 33
    const-class v1, Lcom/teamspeak/ts3client/data/b/f;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/teamspeak/ts3client/data/b/f;->g:Lcom/teamspeak/ts3client/data/b/f;

    if-nez v0, :cond_10

    .line 34
    new-instance v0, Lcom/teamspeak/ts3client/data/b/f;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/b/f;-><init>()V

    .line 35
    sput-object v0, Lcom/teamspeak/ts3client/data/b/f;->g:Lcom/teamspeak/ts3client/data/b/f;
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_13

    .line 37
    :goto_e
    monitor-exit v1

    return-object v0

    :cond_10
    :try_start_10
    sget-object v0, Lcom/teamspeak/ts3client/data/b/f;->g:Lcom/teamspeak/ts3client/data/b/f;
    :try_end_12
    .catchall {:try_start_10 .. :try_end_12} :catchall_13

    goto :goto_e

    .line 33
    :catchall_13
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(I)V
    .registers 12

    .prologue
    .line 195
    new-instance v9, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v9}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 199
    :try_start_5
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    const-string v3, "defaultid=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 200
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6f

    .line 201
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 20045
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 202
    const-string v1, "identity"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 21037
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 203
    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 21053
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 21061
    const/4 v1, 0x0

    iput-boolean v1, v9, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 205
    const-string v1, "ident_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 22029
    iput v1, v9, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 23025
    iget v1, v9, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 206
    int-to-long v2, v1

    invoke-virtual {p0, v2, v3, v9}, Lcom/teamspeak/ts3client/data/b/f;->a(JLcom/teamspeak/ts3client/e/a;)Z

    .line 208
    :cond_6f
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 209
    new-instance v9, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v9}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 210
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ident_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_ee

    .line 212
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 23045
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 213
    const-string v1, "identity"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 24037
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 214
    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 24053
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 24061
    const/4 v1, 0x1

    iput-boolean v1, v9, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 216
    const-string v1, "ident_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 25029
    iput v1, v9, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 26025
    iget v1, v9, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 217
    int-to-long v2, v1

    invoke-virtual {p0, v2, v3, v9}, Lcom/teamspeak/ts3client/data/b/f;->a(JLcom/teamspeak/ts3client/e/a;)Z

    .line 219
    :cond_ee
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_f1
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_f1} :catch_f2

    .line 222
    :goto_f1
    return-void

    :catch_f2
    move-exception v0

    goto :goto_f1
.end method

.method public static a(Lcom/teamspeak/ts3client/data/b/e;)V
    .registers 1

    .prologue
    .line 191
    sput-object p0, Lcom/teamspeak/ts3client/data/b/f;->f:Lcom/teamspeak/ts3client/data/b/e;

    .line 192
    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 61
    return-void
.end method

.method private static f()V
    .registers 1

    .prologue
    .line 225
    sget-object v0, Lcom/teamspeak/ts3client/data/b/f;->f:Lcom/teamspeak/ts3client/data/b/e;

    if-eqz v0, :cond_9

    .line 226
    sget-object v0, Lcom/teamspeak/ts3client/data/b/f;->f:Lcom/teamspeak/ts3client/data/b/e;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/b/e;->a()V

    .line 227
    :cond_9
    return-void
.end method

.method private static g()V
    .registers 1

    .prologue
    .line 230
    const/4 v0, 0x0

    sput-object v0, Lcom/teamspeak/ts3client/data/b/f;->f:Lcom/teamspeak/ts3client/data/b/e;

    .line 231
    return-void
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/e/a;)J
    .registers 6

    .prologue
    .line 46
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 47
    const-string v0, "name"

    .line 1041
    iget-object v2, p1, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 47
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v0, "identity"

    .line 2033
    iget-object v2, p1, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 48
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v0, "nickname"

    .line 2049
    iget-object v2, p1, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 49
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v2, "defaultid"

    .line 2057
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 50
    if-eqz v0, :cond_32

    const/4 v0, 0x1

    :goto_21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 52
    :try_start_28
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "ident"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_30} :catch_34

    move-result-wide v0

    .line 55
    :goto_31
    return-wide v0

    .line 50
    :cond_32
    const/4 v0, 0x0

    goto :goto_21

    .line 54
    :catch_34
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 55
    const-wide/16 v0, -0x1

    goto :goto_31
.end method

.method public final a(J)Z
    .registers 16

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 66
    new-instance v11, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v11}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 69
    :try_start_7
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ident_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 70
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8a

    .line 71
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3045
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 72
    const-string v1, "identity"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4037
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 73
    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4053
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 74
    const-string v1, "defaultid"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_11f

    .line 4061
    const/4 v1, 0x0

    iput-boolean v1, v11, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 78
    :goto_78
    const-string v1, "ident_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 6029
    iput v1, v11, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 7025
    iget v1, v11, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 79
    int-to-long v2, v1

    invoke-virtual {p0, v2, v3, v11}, Lcom/teamspeak/ts3client/data/b/f;->a(JLcom/teamspeak/ts3client/e/a;)Z

    .line 81
    :cond_8a
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 7057
    iget-boolean v0, v11, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 82
    if-eqz v0, :cond_103

    .line 83
    new-instance v11, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v11}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 84
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    const-string v3, "ident_id=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 85
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_100

    .line 86
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 8045
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 87
    const-string v1, "identity"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 9037
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 88
    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 9053
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 9061
    const/4 v1, 0x1

    iput-boolean v1, v11, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 90
    const-string v1, "ident_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 10029
    iput v1, v11, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 11025
    iget v1, v11, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 91
    int-to-long v2, v1

    invoke-virtual {p0, v2, v3, v11}, Lcom/teamspeak/ts3client/data/b/f;->a(JLcom/teamspeak/ts3client/e/a;)Z

    .line 93
    :cond_100
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_103
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_103} :catch_124

    .line 100
    :cond_103
    :goto_103
    :try_start_103
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ident_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 101
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->f()V
    :try_end_11d
    .catch Ljava/lang/Exception; {:try_start_103 .. :try_end_11d} :catch_126

    move v0, v9

    .line 105
    :goto_11e
    return v0

    .line 5061
    :cond_11f
    const/4 v1, 0x1

    :try_start_120
    iput-boolean v1, v11, Lcom/teamspeak/ts3client/e/a;->e:Z
    :try_end_122
    .catch Landroid/database/SQLException; {:try_start_120 .. :try_end_122} :catch_124

    goto/16 :goto_78

    :catch_124
    move-exception v0

    goto :goto_103

    .line 104
    :catch_126
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v10

    .line 105
    goto :goto_11e
.end method

.method public final a(JLcom/teamspeak/ts3client/e/a;)Z
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 235
    const-string v0, "name"

    .line 26041
    iget-object v4, p3, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 235
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v0, "identity"

    .line 27033
    iget-object v4, p3, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 236
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v0, "nickname"

    .line 27049
    iget-object v4, p3, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 237
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v4, "defaultid"

    .line 27057
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 238
    if-eqz v0, :cond_45

    move v0, v1

    :goto_23
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 240
    :try_start_2a
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "ident"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ident_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 241
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->f()V
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_44} :catch_47

    .line 245
    :goto_44
    return v1

    :cond_45
    move v0, v2

    .line 238
    goto :goto_23

    .line 244
    :catch_47
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v1, v2

    .line 245
    goto :goto_44
.end method

.method public final b(J)Lcom/teamspeak/ts3client/e/a;
    .registers 14

    .prologue
    const/4 v10, 0x0

    .line 166
    new-instance v9, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v9}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 171
    :try_start_6
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ident_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 172
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_83

    .line 173
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 17045
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 174
    const-string v1, "identity"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 18037
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 175
    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 18053
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 176
    const-string v1, "defaultid"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_88

    .line 18061
    const/4 v1, 0x0

    iput-boolean v1, v9, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 180
    :goto_77
    const-string v1, "ident_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 20029
    iput v1, v9, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 182
    :cond_83
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v9

    .line 187
    :goto_87
    return-object v0

    .line 19061
    :cond_88
    const/4 v1, 0x1

    iput-boolean v1, v9, Lcom/teamspeak/ts3client/e/a;->e:Z
    :try_end_8b
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_8b} :catch_8c

    goto :goto_77

    .line 184
    :catch_8c
    move-exception v0

    move-object v0, v10

    goto :goto_87
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 41
    new-instance v0, Lcom/teamspeak/ts3client/data/b/g;

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/teamspeak/ts3client/data/b/g;-><init>(Lcom/teamspeak/ts3client/data/b/f;Landroid/content/Context;)V

    .line 42
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b/g;->a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 43
    return-void
.end method

.method public final c()Ljava/util/ArrayList;
    .registers 11

    .prologue
    const/4 v9, 0x0

    .line 110
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 114
    :try_start_6
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 116
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 118
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_85

    .line 120
    :cond_38
    new-instance v1, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 121
    const-string v2, "name"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 11045
    iput-object v2, v1, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 122
    const-string v2, "identity"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 12037
    iput-object v2, v1, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 123
    const-string v2, "nickname"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 12053
    iput-object v2, v1, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 124
    const-string v2, "defaultid"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_8a

    .line 12061
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 128
    :goto_70
    const-string v2, "ident_id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 14029
    iput v2, v1, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 129
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_38

    .line 132
    :cond_85
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v8

    .line 137
    :goto_89
    return-object v0

    .line 13061
    :cond_8a
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/teamspeak/ts3client/e/a;->e:Z
    :try_end_8d
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_8d} :catch_8e

    goto :goto_70

    .line 134
    :catch_8e
    move-exception v0

    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    move-object v0, v9

    .line 135
    goto :goto_89
.end method

.method public final d()Lcom/teamspeak/ts3client/e/a;
    .registers 12

    .prologue
    const/4 v10, 0x0

    .line 141
    new-instance v9, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v9}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 146
    :try_start_6
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    const-string v3, "defaultid=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 147
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_76

    .line 148
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 14045
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 149
    const-string v1, "identity"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 15037
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 150
    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 15053
    iput-object v1, v9, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 151
    const-string v1, "defaultid"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_7b

    .line 15061
    const/4 v1, 0x0

    iput-boolean v1, v9, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 155
    :goto_6a
    const-string v1, "ident_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 17029
    iput v1, v9, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 157
    :cond_76
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v9

    .line 162
    :goto_7a
    return-object v0

    .line 16061
    :cond_7b
    const/4 v1, 0x1

    iput-boolean v1, v9, Lcom/teamspeak/ts3client/e/a;->e:Z
    :try_end_7e
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_7e} :catch_7f

    goto :goto_6a

    .line 159
    :catch_7f
    move-exception v0

    move-object v0, v10

    goto :goto_7a
.end method
