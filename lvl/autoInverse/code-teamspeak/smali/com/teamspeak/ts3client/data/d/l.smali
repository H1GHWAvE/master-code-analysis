.class public final Lcom/teamspeak/ts3client/data/d/l;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# static fields
.field private static final b:J = 0x100000L


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/d/j;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:I


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/d/j;)V
    .registers 3

    .prologue
    .line 80
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 86
    const-string v0, "ContentUPDATER"

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->d:Ljava/lang/String;

    .line 87
    const/4 v0, 0x1

    iput v0, p0, Lcom/teamspeak/ts3client/data/d/l;->e:I

    .line 89
    const/16 v0, 0x64

    iput v0, p0, Lcom/teamspeak/ts3client/data/d/l;->g:I

    return-void
.end method

.method private varargs a()Ljava/lang/Boolean;
    .registers 13

    .prologue
    const/4 v11, -0x1

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 96
    :try_start_3
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/TS3/update/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_28

    .line 98
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 99
    :cond_28
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 1031
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/d/j;->b:Ljava/lang/String;

    .line 99
    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 101
    invoke-virtual {v1}, Ljava/net/URLConnection;->connect()V

    .line 102
    invoke-virtual {v1}, Ljava/net/URLConnection;->getContentLength()I

    move-result v1

    iput v1, p0, Lcom/teamspeak/ts3client/data/d/l;->c:I

    .line 103
    iget v1, p0, Lcom/teamspeak/ts3client/data/d/l;->c:I

    if-gtz v1, :cond_48

    .line 104
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 2031
    iget v1, v1, Lcom/teamspeak/ts3client/data/d/j;->h:I

    .line 104
    iput v1, p0, Lcom/teamspeak/ts3client/data/d/l;->c:I

    .line 105
    :cond_48
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 106
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/TS3/update/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 3031
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/d/j;->d:Ljava/lang/String;

    .line 106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 4031
    iget v1, v1, Lcom/teamspeak/ts3client/data/d/j;->c:I

    .line 106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 107
    const/16 v0, 0x400

    new-array v5, v0, [B

    .line 108
    const-wide/16 v0, 0x0

    .line 110
    :goto_8f
    invoke-virtual {v3, v5}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-eq v6, v11, :cond_bf

    .line 111
    int-to-long v8, v6

    add-long/2addr v0, v8

    .line 112
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Integer;

    const/4 v8, 0x0

    long-to-int v9, v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/teamspeak/ts3client/data/d/l;->publishProgress([Ljava/lang/Object;)V

    .line 113
    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7, v6}, Ljava/io/OutputStream;->write([BII)V
    :try_end_a9
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_a9} :catch_aa
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_a9} :catch_19f

    goto :goto_8f

    .line 118
    :catch_aa
    move-exception v0

    .line 119
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 5031
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 119
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 120
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 168
    :goto_be
    return-object v0

    .line 115
    :cond_bf
    :try_start_bf
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 116
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 117
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_c8
    .catch Ljava/net/MalformedURLException; {:try_start_bf .. :try_end_c8} :catch_aa
    .catch Ljava/io/IOException; {:try_start_bf .. :try_end_c8} :catch_19f

    .line 125
    new-instance v3, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/TS3/update/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 7031
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/d/j;->d:Ljava/lang/String;

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 8031
    iget v1, v1, Lcom/teamspeak/ts3client/data/d/j;->c:I

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x2

    :try_start_101
    iput v0, p0, Lcom/teamspeak/ts3client/data/d/l;->e:I

    .line 129
    new-instance v4, Ljava/util/zip/ZipFile;

    invoke-direct {v4, v3}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 130
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->size()I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/d/l;->g:I

    .line 131
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 9031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->a:Ljava/io/File;

    .line 131
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_11f

    .line 132
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 10031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->a:Ljava/io/File;

    .line 132
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 133
    :cond_11f
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v5

    move v1, v2

    .line 134
    :cond_124
    :goto_124
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1c0

    .line 135
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/ZipEntry;

    .line 136
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 11031
    iget-object v7, v7, Lcom/teamspeak/ts3client/data/d/j;->a:Ljava/io/File;

    .line 136
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 137
    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 138
    add-int/lit8 v1, v1, 0x1

    .line 139
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_124

    .line 142
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/teamspeak/ts3client/data/d/l;->f:Ljava/lang/String;

    .line 143
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Integer;

    const/4 v8, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/teamspeak/ts3client/data/d/l;->publishProgress([Ljava/lang/Object;)V

    .line 144
    new-instance v7, Ljava/io/BufferedInputStream;

    invoke-virtual {v4, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 147
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 148
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 149
    new-instance v6, Ljava/io/BufferedOutputStream;

    const/16 v9, 0x400

    invoke-direct {v6, v8, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 151
    :goto_178
    const/4 v8, 0x0

    const/16 v9, 0x400

    invoke-virtual {v7, v0, v8, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v8

    if-eq v8, v11, :cond_1b5

    .line 152
    const/4 v9, 0x0

    invoke-virtual {v6, v0, v9, v8}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_185
    .catch Ljava/io/IOException; {:try_start_101 .. :try_end_185} :catch_186

    goto :goto_178

    .line 162
    :catch_186
    move-exception v0

    .line 163
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 12031
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 163
    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 164
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 165
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_be

    .line 121
    :catch_19f
    move-exception v0

    .line 122
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 6031
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 122
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 123
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_be

    .line 155
    :cond_1b5
    :try_start_1b5
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->flush()V

    .line 156
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V

    .line 158
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    goto/16 :goto_124

    .line 161
    :cond_1c0
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V
    :try_end_1c3
    .catch Ljava/io/IOException; {:try_start_1b5 .. :try_end_1c3} :catch_186

    .line 167
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 168
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_be
.end method

.method private a(Ljava/lang/Boolean;)V
    .registers 5

    .prologue
    .line 173
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 174
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 175
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 13031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 175
    const-string v1, "contentdownloader.error"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 14031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 176
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 182
    :goto_1e
    return-void

    .line 179
    :cond_1f
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 15031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 179
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 180
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 16031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 17031
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/d/j;->d:Ljava/lang/String;

    .line 180
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 18031
    iget v2, v2, Lcom/teamspeak/ts3client/data/d/j;->c:I

    .line 180
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1e
.end method

.method private varargs a([Ljava/lang/Integer;)V
    .registers 14

    .prologue
    const-wide v10, 0x412e848000000000L    # 1000000.0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 205
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 28031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 205
    iget v1, p0, Lcom/teamspeak/ts3client/data/d/l;->g:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 206
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 29031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 206
    aget-object v1, p1, v6

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 207
    aget-object v0, p1, v6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4f

    .line 208
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 30031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 208
    const-string v1, "Download Failed\nPlease try again."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 31031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 209
    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 213
    :goto_37
    iget v0, p0, Lcom/teamspeak/ts3client/data/d/l;->e:I

    if-ne v0, v8, :cond_4e

    .line 214
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 33031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 214
    const-string v1, "contentdownloader.extracting"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/d/l;->f:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 215
    :cond_4e
    return-void

    .line 211
    :cond_4f
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 32031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Downloaded "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%.2f %s"

    new-array v3, v8, [Ljava/lang/Object;

    aget-object v4, p1, v6

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "MB"

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%.2f %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/teamspeak/ts3client/data/d/l;->c:I

    int-to-double v4, v4

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "MB"

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_37
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/l;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 80
    check-cast p1, Ljava/lang/Boolean;

    .line 39173
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 39174
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_21

    .line 39175
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 40031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 39175
    const-string v1, "contentdownloader.error"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 39176
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 41031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 39176
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 39177
    :goto_20
    return-void

    .line 39179
    :cond_21
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 42031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 39179
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 39180
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 43031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 43093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 39180
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 44031
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/d/j;->d:Ljava/lang/String;

    .line 39180
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 45031
    iget v2, v2, Lcom/teamspeak/ts3client/data/d/j;->c:I

    .line 39180
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_20
.end method

.method protected final onPreExecute()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 187
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 19031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Update "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 20031
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/d/j;->g:Ljava/lang/String;

    .line 187
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 21031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 188
    const v1, 0x7f020076

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIcon(I)V

    .line 189
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 22031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 189
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 190
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 23031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 190
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 191
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 24031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 191
    const-string v1, "contentdownloader.info"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 25031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 192
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 193
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 26031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 193
    new-instance v1, Lcom/teamspeak/ts3client/data/d/m;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/data/d/m;-><init>(Lcom/teamspeak/ts3client/data/d/l;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 200
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 27031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 200
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 201
    return-void
.end method

.method protected final synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 14

    .prologue
    const-wide v10, 0x412e848000000000L    # 1000000.0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 80
    check-cast p1, [Ljava/lang/Integer;

    .line 33205
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 34031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 33205
    iget v1, p0, Lcom/teamspeak/ts3client/data/d/l;->g:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 33206
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 35031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 33206
    aget-object v1, p1, v6

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 33207
    aget-object v0, p1, v6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_51

    .line 33208
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 36031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 33208
    const-string v1, "Download Failed\nPlease try again."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 33209
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 37031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 33209
    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 33213
    :goto_39
    iget v0, p0, Lcom/teamspeak/ts3client/data/d/l;->e:I

    if-ne v0, v8, :cond_50

    .line 33214
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 39031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 33214
    const-string v1, "contentdownloader.extracting"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/d/l;->f:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 80
    :cond_50
    return-void

    .line 33211
    :cond_51
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/l;->a:Lcom/teamspeak/ts3client/data/d/j;

    .line 38031
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 33211
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Downloaded "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%.2f %s"

    new-array v3, v8, [Ljava/lang/Object;

    aget-object v4, p1, v6

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "MB"

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%.2f %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/teamspeak/ts3client/data/d/l;->c:I

    int-to-double v4, v4

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "MB"

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_39
.end method
