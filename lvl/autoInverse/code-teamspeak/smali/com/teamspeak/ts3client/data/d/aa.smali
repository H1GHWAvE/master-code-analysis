.class public final enum Lcom/teamspeak/ts3client/data/d/aa;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/teamspeak/ts3client/data/d/aa;

.field public static final enum b:Lcom/teamspeak/ts3client/data/d/aa;

.field public static final enum c:Lcom/teamspeak/ts3client/data/d/aa;

.field public static final enum d:Lcom/teamspeak/ts3client/data/d/aa;

.field private static final synthetic e:[Lcom/teamspeak/ts3client/data/d/aa;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 119
    new-instance v0, Lcom/teamspeak/ts3client/data/d/aa;

    const-string v1, "PRESET_CUSTOM"

    invoke-direct {v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/teamspeak/ts3client/data/d/aa;->a:Lcom/teamspeak/ts3client/data/d/aa;

    new-instance v0, Lcom/teamspeak/ts3client/data/d/aa;

    const-string v1, "PRESET_VOICE_MOBILE"

    invoke-direct {v0, v1, v3}, Lcom/teamspeak/ts3client/data/d/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/teamspeak/ts3client/data/d/aa;->b:Lcom/teamspeak/ts3client/data/d/aa;

    new-instance v0, Lcom/teamspeak/ts3client/data/d/aa;

    const-string v1, "PRESET_VOICE_DESKTOP"

    invoke-direct {v0, v1, v4}, Lcom/teamspeak/ts3client/data/d/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/teamspeak/ts3client/data/d/aa;->c:Lcom/teamspeak/ts3client/data/d/aa;

    new-instance v0, Lcom/teamspeak/ts3client/data/d/aa;

    const-string v1, "PRESET_MUSIC"

    invoke-direct {v0, v1, v5}, Lcom/teamspeak/ts3client/data/d/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/teamspeak/ts3client/data/d/aa;->d:Lcom/teamspeak/ts3client/data/d/aa;

    .line 118
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/teamspeak/ts3client/data/d/aa;

    sget-object v1, Lcom/teamspeak/ts3client/data/d/aa;->a:Lcom/teamspeak/ts3client/data/d/aa;

    aput-object v1, v0, v2

    sget-object v1, Lcom/teamspeak/ts3client/data/d/aa;->b:Lcom/teamspeak/ts3client/data/d/aa;

    aput-object v1, v0, v3

    sget-object v1, Lcom/teamspeak/ts3client/data/d/aa;->c:Lcom/teamspeak/ts3client/data/d/aa;

    aput-object v1, v0, v4

    sget-object v1, Lcom/teamspeak/ts3client/data/d/aa;->d:Lcom/teamspeak/ts3client/data/d/aa;

    aput-object v1, v0, v5

    sput-object v0, Lcom/teamspeak/ts3client/data/d/aa;->e:[Lcom/teamspeak/ts3client/data/d/aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/teamspeak/ts3client/data/d/aa;
    .registers 2

    .prologue
    .line 118
    const-class v0, Lcom/teamspeak/ts3client/data/d/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/d/aa;

    return-object v0
.end method

.method public static values()[Lcom/teamspeak/ts3client/data/d/aa;
    .registers 1

    .prologue
    .line 118
    sget-object v0, Lcom/teamspeak/ts3client/data/d/aa;->e:[Lcom/teamspeak/ts3client/data/d/aa;

    invoke-virtual {v0}, [Lcom/teamspeak/ts3client/data/d/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/teamspeak/ts3client/data/d/aa;

    return-object v0
.end method
