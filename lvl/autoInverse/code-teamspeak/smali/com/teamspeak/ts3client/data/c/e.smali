.class public final Lcom/teamspeak/ts3client/data/c/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# static fields
.field private static c:Ljava/util/regex/Pattern;


# instance fields
.field a:Ljava/util/concurrent/CountDownLatch;

.field b:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:Ljava/io/File;

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 21
    const-string v0, "ts3image://(.*)\\?channel=(\\d*)(?:&|&amp;)path=(.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/data/c/e;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v1, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    .line 23
    iput-object v1, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    .line 26
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/c/e;->f:Z

    .line 27
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/c/e;->g:Z

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->h:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->i:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/io/File;)Landroid/graphics/drawable/Drawable;
    .registers 15

    .prologue
    .line 32
    sget-object v0, Lcom/teamspeak/ts3client/data/c/e;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 33
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_2c5

    .line 34
    const/4 v0, 0x2

    invoke-virtual {v9, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 35
    const/4 v1, 0x3

    invoke-virtual {v9, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_100

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/data/c/e;->i:Ljava/lang/String;

    .line 39
    :goto_48
    const-string v6, ""

    .line 40
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1356
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->j:Ljava/util/HashMap;

    .line 40
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_67

    .line 41
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2356
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->j:Ljava/util/HashMap;

    .line 41
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    .line 42
    :cond_67
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->e:Ljava/io/File;

    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1b0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_1b0

    .line 46
    :try_start_9a
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    .line 47
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 48
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 48
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 49
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    .line 50
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->h:Ljava/lang/String;

    .line 51
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 51
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 51
    const/4 v0, 0x2

    invoke-virtual {v9, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/c/e;->i:Ljava/lang/String;

    iget-object v8, p0, Lcom/teamspeak/ts3client/data/c/e;->h:Ljava/lang/String;

    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestFileInfo(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f4
    .catch Ljava/lang/Exception; {:try_start_9a .. :try_end_f4} :catch_195

    .line 53
    :try_start_f4
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_f9
    .catch Ljava/lang/InterruptedException; {:try_start_f4 .. :try_end_f9} :catch_11f
    .catch Ljava/lang/Exception; {:try_start_f4 .. :try_end_f9} :catch_195

    .line 60
    :try_start_f9
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c/e;->f:Z

    if-eqz v0, :cond_165

    .line 61
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;
    :try_end_ff
    .catch Ljava/lang/Exception; {:try_start_f9 .. :try_end_ff} :catch_195

    .line 101
    :goto_ff
    return-object v0

    .line 38
    :cond_100
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/data/c/e;->i:Ljava/lang/String;

    goto/16 :goto_48

    .line 55
    :catch_11f
    move-exception v0

    :try_start_120
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 6085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 55
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load Image1: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 56
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    .line 57
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_ff

    .line 62
    :cond_165
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c/e;->g:Z

    if-eqz v0, :cond_1b0

    .line 63
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    .line 64
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 65
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;
    :try_end_193
    .catch Ljava/lang/Exception; {:try_start_120 .. :try_end_193} :catch_195

    goto/16 :goto_ff

    .line 69
    :catch_195
    move-exception v0

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 69
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load Image2: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 73
    :cond_1b0
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 73
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 74
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    .line 75
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 75
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 10061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 75
    const/4 v0, 0x2

    invoke-virtual {v9, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/c/e;->i:Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v11, "loadImage:"

    invoke-direct {v0, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/teamspeak/ts3client/data/c/e;->i:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v1 .. v11}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/c/e;->d:I

    .line 77
    :try_start_1fb
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_200
    .catch Ljava/lang/InterruptedException; {:try_start_1fb .. :try_end_200} :catch_237

    .line 84
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2c5

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_2c5

    .line 86
    :try_start_210
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    .line 87
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 88
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;
    :try_end_235
    .catch Ljava/lang/Exception; {:try_start_210 .. :try_end_235} :catch_27e

    goto/16 :goto_ff

    .line 79
    :catch_237
    move-exception v0

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 11085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 79
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load Image3: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 80
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    .line 81
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 82
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_ff

    .line 91
    :catch_27e
    move-exception v0

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 12085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 91
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load Image4: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    .line 93
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 94
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_ff

    .line 99
    :cond_2c5
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 101
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->b:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_ff
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 7

    .prologue
    const/4 v4, 0x1

    .line 106
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    if-eqz v0, :cond_1e

    move-object v0, p1

    .line 107
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    .line 13049
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->a:I

    .line 107
    iget v1, p0, Lcom/teamspeak/ts3client/data/c/e;->d:I

    if-ne v0, v1, :cond_1e

    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 109
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 13061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 109
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 112
    :cond_1e
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;

    if-eqz v0, :cond_4d

    move-object v0, p1

    .line 113
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;

    .line 14036
    iget-object v1, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;->a:Ljava/lang/String;

    .line 114
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/e;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4d

    .line 14040
    iget-wide v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;->b:J

    .line 115
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/e;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_b5

    .line 116
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 117
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/c/e;->f:Z

    .line 118
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 14061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 118
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 125
    :cond_4d
    :goto_4d
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    if-eqz v0, :cond_b4

    move-object v0, p1

    .line 126
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 16041
    iget-object v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/e;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b4

    move-object v0, p1

    .line 127
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 17029
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    .line 127
    const/16 v1, 0x803

    if-ne v0, v1, :cond_7b

    move-object v0, p1

    .line 128
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 17041
    iget-object v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/e;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 129
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/c/e;->g:Z

    .line 130
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_7b
    move-object v0, p1

    .line 132
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 18029
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    .line 132
    const/16 v1, 0x806

    if-ne v0, v1, :cond_98

    move-object v0, p1

    .line 133
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 18041
    iget-object v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/e;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_98

    .line 134
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/c/e;->g:Z

    .line 135
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_98
    move-object v0, p1

    .line 137
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 19029
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    .line 137
    const/16 v1, 0x30d

    if-ne v0, v1, :cond_b4

    .line 138
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 19041
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/e;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 139
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/data/c/e;->g:Z

    .line 140
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 144
    :cond_b4
    return-void

    .line 120
    :cond_b5
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/e;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 121
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 15061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 121
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    goto :goto_4d
.end method
