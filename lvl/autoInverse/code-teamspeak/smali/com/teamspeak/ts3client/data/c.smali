.class public final Lcom/teamspeak/ts3client/data/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;
.implements Ljava/lang/Comparable;


# instance fields
.field public A:I

.field private B:Lcom/teamspeak/ts3client/Ts3Application;

.field private C:Ljava/lang/String;

.field private D:Ljava/util/ArrayList;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:I

.field public f:Z

.field g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field l:Z

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:I

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Z

.field public s:Z

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:I

.field public w:Lcom/teamspeak/ts3client/c/a;

.field x:Ljava/lang/String;

.field public y:Landroid/graphics/Bitmap;

.field z:J


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/c/a;)V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 23
    iput v1, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 24
    iput v1, p0, Lcom/teamspeak/ts3client/data/c;->d:I

    .line 25
    iput v1, p0, Lcom/teamspeak/ts3client/data/c;->e:I

    .line 26
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->f:Z

    .line 27
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->g:Z

    .line 28
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->h:Z

    .line 29
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->i:Z

    .line 30
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->j:Z

    .line 31
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->k:Z

    .line 32
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->l:Z

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 34
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->n:Z

    .line 35
    iput v1, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 36
    const-string v0, "0"

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->q:Ljava/lang/String;

    .line 38
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 39
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->C:Ljava/lang/String;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/teamspeak/ts3client/data/c;->z:J

    .line 57
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 1030
    iget-object v0, p1, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 2022
    iget-object v0, p1, Lcom/teamspeak/ts3client/c/a;->c:Ljava/lang/String;

    .line 59
    const-string v1, "\'"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V
    .registers 5

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;Z)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;Z)V
    .registers 15

    .prologue
    const-wide/16 v8, 0x1

    const-wide/16 v6, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 23
    iput v1, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 24
    iput v1, p0, Lcom/teamspeak/ts3client/data/c;->d:I

    .line 25
    iput v1, p0, Lcom/teamspeak/ts3client/data/c;->e:I

    .line 26
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->f:Z

    .line 27
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->g:Z

    .line 28
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->h:Z

    .line 29
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->i:Z

    .line 30
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->j:Z

    .line 31
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->k:Z

    .line 32
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->l:Z

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 34
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->n:Z

    .line 35
    iput v1, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 36
    const-string v0, "0"

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->q:Ljava/lang/String;

    .line 38
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 39
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->C:Ljava/lang/String;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    .line 49
    iput-wide v6, p0, Lcom/teamspeak/ts3client/data/c;->z:J

    .line 65
    invoke-static {p1}, Lcom/teamspeak/ts3client/data/d/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 66
    iput p2, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 67
    iput-object p3, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 68
    if-nez p4, :cond_5e

    .line 2061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 3061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 70
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->ag:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->x:Ljava/lang/String;

    .line 4061
    :cond_5e
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 5061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 73
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_195

    move v0, v1

    .line 6263
    :goto_71
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->f:Z

    .line 7061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 8061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 74
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_198

    move v0, v1

    :goto_86
    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/data/c;->a(Z)V

    .line 9061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 10061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 75
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->j:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-nez v0, :cond_19b

    move v0, v1

    .line 10295
    :goto_9c
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->i:Z

    .line 11061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 12061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 76
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->i:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-nez v0, :cond_19e

    move v0, v1

    .line 13255
    :goto_b1
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->h:Z

    .line 14061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 15061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 77
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->I:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 16231
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->t:Ljava/lang/String;

    .line 17061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 18061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 78
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->H:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 19131
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->u:Ljava/lang/String;

    .line 20061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 21061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 79
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->O:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    .line 22184
    iput v0, p0, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 23061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 24061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 80
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    if-ne v0, v2, :cond_1a1

    move v0, v2

    .line 25247
    :goto_f4
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->j:Z

    .line 26061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 27061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 81
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->N:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 28139
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 29061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 29234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 30061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 82
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->af:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    if-ne v0, v2, :cond_1a4

    move v0, v2

    .line 30271
    :goto_117
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->n:Z

    .line 31061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 32061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 32267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 83
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->a:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/data/c;->b(Ljava/lang/String;)V

    .line 33061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 33234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 34061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 84
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->Z:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_1a7

    move v0, v1

    .line 34279
    :goto_13d
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->k:Z

    .line 35061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 35234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 36061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 36267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 85
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->Q:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    .line 37160
    iput v0, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 38061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 38234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 39061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 39267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 86
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->R:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 40168
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 41061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 41234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 42061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 42267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 87
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->S:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 43176
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->q:Ljava/lang/String;

    .line 44061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 45061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 45267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 88
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->ae:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/teamspeak/ts3client/data/c;->a(J)V

    .line 46061
    iget-object v0, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 46234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 47061
    iget-object v3, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 47267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 89
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->r:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_1a9

    .line 47422
    :goto_192
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 92
    return-void

    :cond_195
    move v0, v2

    .line 73
    goto/16 :goto_71

    :cond_198
    move v0, v2

    .line 74
    goto/16 :goto_86

    :cond_19b
    move v0, v2

    .line 75
    goto/16 :goto_9c

    :cond_19e
    move v0, v2

    .line 76
    goto/16 :goto_b1

    :cond_1a1
    move v0, v1

    .line 80
    goto/16 :goto_f4

    :cond_1a4
    move v0, v1

    .line 82
    goto/16 :goto_117

    :cond_1a7
    move v0, v2

    .line 84
    goto :goto_13d

    :cond_1a9
    move v1, v2

    .line 89
    goto :goto_192
.end method

.method private A()V
    .registers 9

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 331
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 50084
    iget v0, v0, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 331
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_21

    .line 332
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50086
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 332
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50087
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50088
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 332
    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 50089
    iget v4, v4, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 332
    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setClientVolumeModifier(JIF)I

    .line 333
    :cond_21
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 50090
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 333
    if-eqz v0, :cond_5c

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50091
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50092
    iget v0, v0, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 333
    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    if-eq v0, v1, :cond_5c

    .line 334
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50094
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 334
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50095
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50096
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 334
    new-array v1, v5, [I

    iget v4, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    aput v4, v1, v6

    aput v6, v1, v7

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Mute: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestMuteClients(J[ILjava/lang/String;)I

    .line 335
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/data/c;->l:Z

    .line 340
    :goto_5b
    return-void

    .line 337
    :cond_5c
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50097
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50098
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 337
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50099
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50100
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 337
    new-array v1, v5, [I

    iget v4, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    aput v4, v1, v6

    aput v6, v1, v7

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UnMute: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestUnmuteClients(J[ILjava/lang/String;)I

    .line 338
    iput-boolean v6, p0, Lcom/teamspeak/ts3client/data/c;->l:Z

    goto :goto_5b
.end method

.method private B()I
    .registers 2

    .prologue
    .line 358
    iget v0, p0, Lcom/teamspeak/ts3client/data/c;->e:I

    return v0
.end method

.method private C()J
    .registers 3

    .prologue
    .line 366
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/c;->z:J

    return-wide v0
.end method

.method private D()Z
    .registers 2

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->l:Z

    return v0
.end method

.method private E()I
    .registers 2

    .prologue
    .line 382
    iget v0, p0, Lcom/teamspeak/ts3client/data/c;->A:I

    return v0
.end method

.method private F()Z
    .registers 2

    .prologue
    .line 418
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->s:Z

    return v0
.end method

.method private a(Lcom/teamspeak/ts3client/data/c;)I
    .registers 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 392
    iget v0, p1, Lcom/teamspeak/ts3client/data/c;->o:I

    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    sub-int/2addr v0, v1

    if-nez v0, :cond_58

    .line 401
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->r:Z

    if-eqz v0, :cond_3b

    iget-boolean v0, p1, Lcom/teamspeak/ts3client/data/c;->r:Z

    if-eqz v0, :cond_3b

    .line 402
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 403
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 414
    :goto_29
    return v0

    .line 405
    :cond_2a
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_29

    .line 407
    :cond_3b
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->r:Z

    if-eqz v0, :cond_41

    .line 408
    const/4 v0, -0x1

    goto :goto_29

    .line 409
    :cond_41
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/data/c;->r:Z

    if-eqz v0, :cond_47

    .line 410
    const/4 v0, 0x1

    goto :goto_29

    .line 412
    :cond_47
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_29

    .line 414
    :cond_58
    iget v0, p1, Lcom/teamspeak/ts3client/data/c;->o:I

    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    sub-int/2addr v0, v1

    goto :goto_29
.end method

.method private b(Z)V
    .registers 2

    .prologue
    .line 247
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->j:Z

    .line 248
    return-void
.end method

.method private c(I)V
    .registers 2

    .prologue
    .line 160
    iput p1, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 161
    return-void
.end method

.method private c(Z)V
    .registers 2

    .prologue
    .line 255
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->h:Z

    .line 256
    return-void
.end method

.method private d(I)V
    .registers 2

    .prologue
    .line 184
    iput p1, p0, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 185
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 139
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 140
    return-void
.end method

.method private d(Z)V
    .registers 2

    .prologue
    .line 263
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->f:Z

    .line 264
    return-void
.end method

.method private e(I)V
    .registers 2

    .prologue
    .line 239
    iput p1, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 240
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 168
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 169
    return-void
.end method

.method private e(Z)V
    .registers 2

    .prologue
    .line 271
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->n:Z

    .line 272
    return-void
.end method

.method private f(I)V
    .registers 2

    .prologue
    .line 362
    iput p1, p0, Lcom/teamspeak/ts3client/data/c;->e:I

    .line 363
    return-void
.end method

.method private f(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 176
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c;->q:Ljava/lang/String;

    .line 177
    return-void
.end method

.method private f(Z)V
    .registers 2

    .prologue
    .line 279
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->k:Z

    .line 280
    return-void
.end method

.method private g(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 207
    invoke-static {p1}, Lcom/teamspeak/ts3client/data/d/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 208
    return-void
.end method

.method private g(Z)V
    .registers 2

    .prologue
    .line 287
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 288
    return-void
.end method

.method private h()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private h(Z)V
    .registers 2

    .prologue
    .line 295
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->i:Z

    .line 296
    return-void
.end method

.method private i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->u:Ljava/lang/String;

    return-object v0
.end method

.method private i(Z)V
    .registers 2

    .prologue
    .line 378
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->l:Z

    .line 379
    return-void
.end method

.method private j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    return-object v0
.end method

.method private j(Z)V
    .registers 2

    .prologue
    .line 422
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 423
    return-void
.end method

.method private k()I
    .registers 2

    .prologue
    .line 156
    iget v0, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    return v0
.end method

.method private l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->q:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .registers 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->x:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private p()I
    .registers 2

    .prologue
    .line 219
    iget v0, p0, Lcom/teamspeak/ts3client/data/c;->d:I

    return v0
.end method

.method private q()Ljava/lang/String;
    .registers 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->t:Ljava/lang/String;

    return-object v0
.end method

.method private r()Z
    .registers 2

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->j:Z

    return v0
.end method

.method private s()Z
    .registers 2

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->h:Z

    return v0
.end method

.method private t()Z
    .registers 2

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->f:Z

    return v0
.end method

.method private u()Z
    .registers 2

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->n:Z

    return v0
.end method

.method private v()Z
    .registers 2

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->k:Z

    return v0
.end method

.method private w()Z
    .registers 2

    .prologue
    .line 283
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->r:Z

    return v0
.end method

.method private x()Z
    .registers 2

    .prologue
    .line 291
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->i:Z

    return v0
.end method

.method private y()Z
    .registers 2

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->g:Z

    return v0
.end method

.method private z()V
    .registers 3

    .prologue
    .line 324
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/d/s;

    .line 325
    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/d/s;->y()V

    goto :goto_6

    .line 327
    :cond_16
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 328
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 99
    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    if-eqz v0, :cond_63

    move v0, v1

    .line 48042
    :goto_7
    sget-object v3, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 102
    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b/c;->a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v3

    iput-object v3, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 103
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 48061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 103
    if-eqz v3, :cond_1e

    .line 104
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    if-eqz v3, :cond_1f

    .line 105
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c;->A()V

    .line 115
    :cond_1e
    :goto_1e
    return-void

    .line 107
    :cond_1f
    if-eqz v0, :cond_33

    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 49061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 108
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50062
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 108
    iget v3, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setClientVolumeModifier(JIF)I

    .line 109
    :cond_33
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->l:Z

    if-eqz v0, :cond_1e

    .line 110
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/data/c;->l:Z

    .line 111
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50063
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50064
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 111
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50065
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50066
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 111
    const/4 v3, 0x2

    new-array v3, v3, [I

    iget v6, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    aput v6, v3, v2

    aput v2, v3, v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unmute: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v5, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestUnmuteClients(J[ILjava/lang/String;)I

    goto :goto_1e

    :cond_63
    move v0, v2

    goto :goto_7
.end method

.method public final a(I)V
    .registers 2

    .prologue
    .line 223
    iput p1, p0, Lcom/teamspeak/ts3client/data/c;->d:I

    .line 224
    return-void
.end method

.method public final a(J)V
    .registers 6

    .prologue
    .line 370
    const-wide v0, 0xffffffffL

    and-long/2addr v0, p1

    iput-wide v0, p0, Lcom/teamspeak/ts3client/data/c;->z:J

    .line 371
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .registers 4

    .prologue
    .line 122
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    .line 50067
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/d/s;

    .line 50068
    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/d/s;->y()V

    goto :goto_8

    .line 50070
    :cond_18
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 124
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/data/d/s;)V
    .registers 4

    .prologue
    .line 311
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1f

    .line 313
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 314
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    :cond_11
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1f

    .line 318
    new-instance v0, Lcom/teamspeak/ts3client/data/c/a;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/c/a;-><init>(Lcom/teamspeak/ts3client/data/c;)V

    .line 321
    :cond_1f
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 4

    .prologue
    .line 349
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;

    if-eqz v0, :cond_1c

    move-object v0, p1

    .line 350
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;

    .line 50107
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->a:I

    .line 350
    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    if-ne v0, v1, :cond_1c

    .line 351
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;

    .line 50108
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b:Ljava/lang/String;

    .line 351
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 352
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50109
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50110
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 352
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 355
    :cond_1c
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 131
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c;->u:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public final a(Z)V
    .registers 7

    .prologue
    .line 303
    if-nez p1, :cond_24

    .line 304
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    if-eqz v0, :cond_24

    .line 305
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 50078
    iget v0, v0, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 305
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_24

    .line 306
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50079
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50080
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 306
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50081
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50082
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 306
    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 50083
    iget v4, v4, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 306
    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setClientVolumeModifier(JIF)I

    .line 307
    :cond_24
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/c;->g:Z

    .line 308
    return-void
.end method

.method public final b()Ljava/lang/String;
    .registers 7

    .prologue
    .line 143
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->C:Ljava/lang/String;

    if-eqz v0, :cond_44

    .line 144
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->C:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50072
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50073
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 145
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50074
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50075
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 145
    iget v4, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    sget-object v5, Lcom/teamspeak/ts3client/jni/d;->P:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/data/c;->C:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_44

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    .line 148
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/d;->c(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 149
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/d;->d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/a;->a()V

    .line 152
    :cond_44
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)V
    .registers 2

    .prologue
    .line 386
    iput p1, p0, Lcom/teamspeak/ts3client/data/c;->A:I

    .line 387
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 192
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 50076
    sget-object v0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 193
    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/b/c;->a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 195
    :try_start_a
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 195
    if-eqz v0, :cond_17

    .line 196
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    if-eqz v0, :cond_17

    .line 197
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c;->A()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_17} :catch_18

    .line 200
    :cond_17
    :goto_17
    return-void

    :catch_18
    move-exception v0

    goto :goto_17
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 180
    iget v0, p0, Lcom/teamspeak/ts3client/data/c;->v:I

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 231
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c;->t:Ljava/lang/String;

    .line 232
    return-void
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 19
    check-cast p1, Lcom/teamspeak/ts3client/data/c;

    .line 50111
    iget v0, p1, Lcom/teamspeak/ts3client/data/c;->o:I

    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    sub-int/2addr v0, v1

    if-nez v0, :cond_5a

    .line 50120
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->r:Z

    if-eqz v0, :cond_3d

    iget-boolean v0, p1, Lcom/teamspeak/ts3client/data/c;->r:Z

    if-eqz v0, :cond_3d

    .line 50121
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 50122
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 50131
    :goto_2b
    return v0

    .line 50124
    :cond_2c
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_2b

    .line 50126
    :cond_3d
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/c;->r:Z

    if-eqz v0, :cond_43

    .line 50127
    const/4 v0, -0x1

    goto :goto_2b

    .line 50128
    :cond_43
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/data/c;->r:Z

    if-eqz v0, :cond_49

    .line 50129
    const/4 v0, 0x1

    goto :goto_2b

    .line 50131
    :cond_49
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_2b

    .line 50133
    :cond_5a
    iget v0, p1, Lcom/teamspeak/ts3client/data/c;->o:I

    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->o:I

    sub-int/2addr v0, v1

    .line 19
    goto :goto_2b
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/teamspeak/ts3client/c/a;
    .registers 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    return-object v0
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 235
    iget v0, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    return v0
.end method

.method public final g()V
    .registers 7

    .prologue
    .line 343
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50101
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50102
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 343
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 344
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50103
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50104
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 344
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c;->B:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50105
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50106
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 344
    iget v1, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Request UID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/teamspeak/ts3client/data/c;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestClientUIDfromClientID(JILjava/lang/String;)I

    .line 345
    return-void
.end method
