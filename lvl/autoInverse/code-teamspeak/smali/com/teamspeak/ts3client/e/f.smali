.class final Lcom/teamspeak/ts3client/e/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/teamspeak/ts3client/e/e;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/e/e;I)V
    .registers 3

    .prologue
    .line 67
    iput-object p1, p0, Lcom/teamspeak/ts3client/e/f;->b:Lcom/teamspeak/ts3client/e/e;

    iput p2, p0, Lcom/teamspeak/ts3client/e/f;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 6

    .prologue
    .line 71
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/teamspeak/ts3client/e/f;->b:Lcom/teamspeak/ts3client/e/e;

    invoke-static {v1}, Lcom/teamspeak/ts3client/e/e;->a(Lcom/teamspeak/ts3client/e/e;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 72
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 73
    const-string v1, "identity.info"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 74
    const-string v1, "identity.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 75
    const/4 v1, -0x1

    const-string v2, "button.delete"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/e/g;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/e/g;-><init>(Lcom/teamspeak/ts3client/e/f;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 82
    const/4 v1, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/e/h;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/e/h;-><init>(Lcom/teamspeak/ts3client/e/f;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 89
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 90
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 92
    return-void
.end method
