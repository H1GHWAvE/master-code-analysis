.class final Lcom/teamspeak/ts3client/ci;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Landroid/view/animation/Animation;

.field final synthetic b:Lcom/teamspeak/ts3client/StartGUIFragment;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/animation/Animation;)V
    .registers 3

    .prologue
    .line 493
    iput-object p1, p0, Lcom/teamspeak/ts3client/ci;->b:Lcom/teamspeak/ts3client/StartGUIFragment;

    iput-object p2, p0, Lcom/teamspeak/ts3client/ci;->a:Landroid/view/animation/Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .registers 10

    .prologue
    const/4 v7, 0x0

    .line 496
    iget-object v0, p0, Lcom/teamspeak/ts3client/ci;->b:Lcom/teamspeak/ts3client/StartGUIFragment;

    new-instance v1, Lcom/teamspeak/ts3client/cj;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/cj;-><init>(Lcom/teamspeak/ts3client/ci;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 504
    iget-object v0, p0, Lcom/teamspeak/ts3client/ci;->b:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->h(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    .line 506
    iget-object v1, p0, Lcom/teamspeak/ts3client/ci;->b:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1539
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "UrlStart"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 1540
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "uri"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1541
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 1542
    const-string v3, "gui.extern.info"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1543
    const-string v3, "gui.extern.text"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "address"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1544
    const/4 v3, -0x1

    const-string v4, "gui.extern.button1"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/teamspeak/ts3client/bv;

    invoke-direct {v5, v1, v0, v2}, Lcom/teamspeak/ts3client/bv;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;Landroid/app/AlertDialog;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1552
    const/4 v3, -0x3

    const-string v4, "gui.extern.button2"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/teamspeak/ts3client/bw;

    invoke-direct {v5, v1, v0, v2}, Lcom/teamspeak/ts3client/bw;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;Landroid/app/AlertDialog;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1560
    const/4 v0, -0x2

    const-string v3, "button.cancel"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/teamspeak/ts3client/bx;

    invoke-direct {v4, v1, v2}, Lcom/teamspeak/ts3client/bx;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v2, v0, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1568
    invoke-virtual {v2, v7}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 1569
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 507
    :cond_8f
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .registers 2

    .prologue
    .line 513
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .registers 2

    .prologue
    .line 519
    return-void
.end method
