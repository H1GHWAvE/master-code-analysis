.class public final Lcom/teamspeak/ts3client/a/s;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/a/r;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/a/r;)V
    .registers 2

    .prologue
    .line 86
    iput-object p1, p0, Lcom/teamspeak/ts3client/a/s;->a:Lcom/teamspeak/ts3client/a/r;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .registers 9

    .prologue
    .line 90
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 95
    :try_start_8
    const-string v1, "sound/speech/settings.ini"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 97
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 98
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 100
    :cond_1d
    :goto_1d
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8f

    .line 101
    const-string v4, "(.*= say\\(\".*\"\\))"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 102
    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 103
    const/4 v4, 0x0

    const/4 v5, 0x0

    aget-object v5, v3, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 104
    const/4 v4, 0x1

    aget-object v4, v3, v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1d

    .line 105
    const/4 v4, 0x1

    const/4 v5, 0x1

    aget-object v5, v3, v5

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 106
    const/4 v4, 0x1

    const/4 v5, 0x1

    aget-object v5, v3, v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    aget-object v7, v3, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 107
    iget-object v4, p0, Lcom/teamspeak/ts3client/a/s;->a:Lcom/teamspeak/ts3client/a/r;

    invoke-static {v4}, Lcom/teamspeak/ts3client/a/r;->a(Lcom/teamspeak/ts3client/a/r;)Ljava/util/HashMap;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v3, v5

    const/4 v6, 0x1

    aget-object v3, v3, v6

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_78
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_78} :catch_79
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_78} :catch_99

    goto :goto_1d

    .line 116
    :catch_79
    move-exception v0

    .line 117
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 117
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 122
    :goto_8d
    const/4 v0, 0x0

    return-object v0

    .line 112
    :cond_8f
    :try_start_8f
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 113
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 115
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_98
    .catch Ljava/io/FileNotFoundException; {:try_start_8f .. :try_end_98} :catch_79
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_98} :catch_99

    goto :goto_8d

    .line 119
    :catch_99
    move-exception v0

    .line 120
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 120
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_8d
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/teamspeak/ts3client/a/s;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
