.class public final Lcom/teamspeak/ts3client/a/u;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/a/t;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/a/t;)V
    .registers 2

    .prologue
    .line 93
    iput-object p1, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .registers 11

    .prologue
    .line 97
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 102
    :try_start_8
    const-string v1, "sound/female/settings.ini"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 103
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 104
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 106
    :cond_1d
    :goto_1d
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1a4

    .line 107
    const-string v5, "(.*= play\\(\".*\"\\))"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 108
    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 109
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 110
    const/4 v5, 0x1

    aget-object v5, v4, v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1d

    .line 111
    const/4 v5, 0x1

    const/4 v6, 0x1

    aget-object v6, v4, v6

    const/4 v7, 0x7

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 112
    const/4 v5, 0x1

    const/4 v6, 0x1

    aget-object v6, v4, v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    aget-object v8, v4, v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 114
    const/4 v5, 0x1

    aget-object v5, v4, v5

    const-string v6, "${clientType}"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_15d

    .line 115
    const/4 v5, 0x1

    aget-object v5, v4, v5

    const-string v6, "${clientType}"

    const-string v7, "neutral"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 116
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sound/female/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v5

    .line 117
    iget-object v6, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    iget-object v6, v6, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "neutral_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    aget-object v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    .line 1025
    iget-object v8, v8, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    .line 117
    const/4 v9, 0x1

    invoke-virtual {v8, v5, v9}, Landroid/media/SoundPool;->load(Landroid/content/res/AssetFileDescriptor;I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->close()V

    .line 119
    const/4 v5, 0x1

    aget-object v5, v4, v5

    const-string v6, "${clientType}"

    const-string v7, "blocked_user"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 120
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sound/female/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v5

    .line 121
    iget-object v6, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    iget-object v6, v6, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "blocked_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    aget-object v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    .line 2025
    iget-object v8, v8, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    .line 121
    const/4 v9, 0x1

    invoke-virtual {v8, v5, v9}, Landroid/media/SoundPool;->load(Landroid/content/res/AssetFileDescriptor;I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->close()V

    .line 123
    const/4 v5, 0x1

    aget-object v5, v4, v5

    const-string v6, "${clientType}"

    const-string v7, "friend"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 124
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sound/female/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v5

    .line 125
    iget-object v6, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    iget-object v6, v6, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "friend_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    aget-object v4, v4, v8

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    .line 3025
    iget-object v7, v7, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    .line 125
    const/4 v8, 0x1

    invoke-virtual {v7, v5, v8}, Landroid/media/SoundPool;->load(Landroid/content/res/AssetFileDescriptor;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_145
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_145} :catch_147
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_145} :catch_18f

    goto/16 :goto_1d

    .line 140
    :catch_147
    move-exception v0

    .line 141
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 4085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 141
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 146
    :goto_15b
    const/4 v0, 0x0

    return-object v0

    .line 129
    :cond_15d
    :try_start_15d
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sound/female/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x1

    aget-object v6, v4, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v5

    .line 130
    iget-object v6, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    iget-object v6, v6, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    const/4 v7, 0x0

    aget-object v4, v4, v7

    iget-object v7, p0, Lcom/teamspeak/ts3client/a/u;->a:Lcom/teamspeak/ts3client/a/t;

    .line 4025
    iget-object v7, v7, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    .line 130
    const/4 v8, 0x1

    invoke-virtual {v7, v5, v8}, Landroid/media/SoundPool;->load(Landroid/content/res/AssetFileDescriptor;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-virtual {v5}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_18d
    .catch Ljava/io/FileNotFoundException; {:try_start_15d .. :try_end_18d} :catch_147
    .catch Ljava/io/IOException; {:try_start_15d .. :try_end_18d} :catch_18f

    goto/16 :goto_1d

    .line 143
    :catch_18f
    move-exception v0

    .line 144
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 5085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 144
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_15b

    .line 137
    :cond_1a4
    :try_start_1a4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    .line 138
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    .line 139
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1ad
    .catch Ljava/io/FileNotFoundException; {:try_start_1a4 .. :try_end_1ad} :catch_147
    .catch Ljava/io/IOException; {:try_start_1a4 .. :try_end_1ad} :catch_18f

    goto :goto_15b
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/teamspeak/ts3client/a/u;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
