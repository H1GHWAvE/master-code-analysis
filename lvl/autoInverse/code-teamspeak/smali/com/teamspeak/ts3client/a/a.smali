.class public final Lcom/teamspeak/ts3client/a/a;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# static fields
.field private static au:Lcom/teamspeak/ts3client/a/a;


# instance fields
.field private aA:I

.field private aB:Landroid/widget/Spinner;

.field private aC:Landroid/widget/Spinner;

.field private aD:Z

.field private aE:Landroid/widget/Spinner;

.field private aF:Landroid/widget/Spinner;

.field private aG:Landroid/widget/Button;

.field private aH:Landroid/widget/Button;

.field private aI:Z

.field private aJ:Landroid/widget/Button;

.field private aK:Landroid/widget/TextView;

.field private aL:Landroid/widget/TextView;

.field private aM:Landroid/widget/TextView;

.field private aN:Landroid/widget/TextView;

.field private aO:Z

.field private aP:Landroid/content/BroadcastReceiver;

.field private aQ:Landroid/widget/TextView;

.field public at:Z

.field private av:Lcom/teamspeak/ts3client/Ts3Application;

.field private aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

.field private ax:Lcom/teamspeak/ts3client/a/k;

.field private ay:J

.field private az:I


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 56
    new-instance v0, Lcom/teamspeak/ts3client/a/b;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/b;-><init>(Lcom/teamspeak/ts3client/a/a;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aP:Landroid/content/BroadcastReceiver;

    .line 74
    return-void
.end method

.method private A()V
    .registers 3

    .prologue
    .line 316
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aG:Landroid/widget/Button;

    const-string v1, "audiosettings.button.start"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aH:Landroid/widget/Button;

    const-string v1, "audiosettings.button.stop"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aJ:Landroid/widget/Button;

    const-string v1, "button.save"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aN:Landroid/widget/TextView;

    const-string v1, "audiosettings.text.play"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aM:Landroid/widget/TextView;

    const-string v1, "audiosettings.text.rec"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aK:Landroid/widget/TextView;

    const-string v1, "audiosettings.text.playbackstream"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aL:Landroid/widget/TextView;

    const-string v1, "audiosettings.text.recordstream"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aQ:Landroid/widget/TextView;

    const-string v1, "audiosettings.error"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    return-void
.end method

.method private B()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 373
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aD:Z

    if-nez v0, :cond_7

    .line 400
    :cond_6
    :goto_6
    return-void

    .line 375
    :cond_7
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/a/a;->aD:Z

    .line 377
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 377
    if-eqz v0, :cond_3a

    .line 378
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    if-nez v0, :cond_19

    .line 379
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 380
    :cond_19
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 381
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 382
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setLocalTestMode(JI)I

    .line 383
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    .line 385
    iput-object v1, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    .line 386
    iput-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    goto :goto_6

    .line 389
    :cond_3a
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    if-eqz v0, :cond_6

    .line 392
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->e()V

    .line 393
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->f()V

    .line 394
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->b()V

    .line 395
    iput-object v1, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    .line 396
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 397
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 398
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    .line 399
    iput-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    goto :goto_6
.end method

.method private C()V
    .registers 4

    .prologue
    .line 403
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/a/a;->a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    .line 404
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 404
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "recordStream"

    iget v2, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 405
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 405
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "playbackStream"

    iget v2, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 406
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 406
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "sampleRec"

    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aE:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 407
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 407
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "samplePlay"

    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aF:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 18413
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->b()V

    .line 410
    return-void
.end method

.method private D()V
    .registers 1

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->b()V

    .line 414
    return-void
.end method

.method private E()Z
    .registers 2

    .prologue
    .line 447
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->at:Z

    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;J)J
    .registers 4

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    return-wide p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;Lcom/teamspeak/ts3client/jni/Ts3Jni;)Lcom/teamspeak/ts3client/jni/Ts3Jni;
    .registers 2

    .prologue
    .line 32
    iput-object p1, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    return-object p1
.end method

.method private a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
    .registers 11

    .prologue
    const-wide/16 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 113
    iput v2, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    .line 114
    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_11

    .line 115
    iput v2, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    .line 116
    :cond_11
    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_1c

    .line 117
    const/4 v0, 0x3

    iput v0, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    .line 118
    :cond_1c
    iput v2, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    .line 119
    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_28

    .line 120
    iput v2, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    .line 121
    :cond_28
    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_33

    .line 122
    const/4 v0, 0x1

    iput v0, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    .line 123
    :cond_33
    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v0

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_40

    .line 124
    const/4 v0, 0x7

    iput v0, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    .line 125
    :cond_40
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;Landroid/widget/Spinner;Landroid/widget/Spinner;)V
    .registers 11

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 32
    .line 20334
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aD:Z

    if-eqz v0, :cond_80

    .line 20336
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/a/a;->a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    .line 20338
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 20339
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 20340
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_prepareAudioDevice(II)I

    .line 20341
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    .line 20342
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 21110
    iput-object v1, v0, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 20343
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget v3, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    iget v4, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/teamspeak/ts3client/a/k;->a(IIII)Z

    move-result v0

    if-nez v0, :cond_81

    .line 20344
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->b()V

    .line 20345
    iput-object v7, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    .line 20346
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    .line 20347
    iput-object v7, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 20348
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aH:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 20349
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aG:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 20350
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aQ:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 20351
    :cond_80
    :goto_80
    return-void

    .line 20353
    :cond_81
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(Ljava/lang/Boolean;)V

    .line 20354
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aO:Z

    if-eqz v0, :cond_e3

    .line 20355
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/a/j;->a(I)V

    .line 20358
    :goto_95
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_activateCaptureDevice(J)I

    .line 20359
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    const-string v1, ""

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I

    .line 20360
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    const-string v1, ""

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_openPlaybackDevice(JLjava/lang/String;Ljava/lang/String;)I

    .line 20361
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 20362
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    const-string v1, "vad"

    const-string v4, "true"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 20363
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    const-string v1, "voiceactivation_level"

    const-string v4, "-50"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 20364
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setLocalTestMode(JI)I

    .line 20366
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->d()V

    .line 20367
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->c()V

    goto :goto_80

    .line 20357
    :cond_e3
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/a/j;->b(I)V

    goto :goto_95
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;)Z
    .registers 2

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aO:Z

    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;Z)Z
    .registers 2

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/a/a;->aO:Z

    return p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/a;Ljava/util/Vector;)[Ljava/lang/String;
    .registers 3

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/a/a;->a(Ljava/util/Vector;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Vector;)[Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    if-gtz v0, :cond_a

    .line 92
    new-array v0, v3, [Ljava/lang/String;

    .line 108
    :goto_9
    return-object v0

    .line 93
    :cond_a
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 94
    invoke-virtual {p1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 95
    if-eqz v0, :cond_13

    .line 97
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 99
    :cond_2d
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_4a

    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aG:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 101
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aJ:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 102
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aQ:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 104
    const-string v1, "ERROR"

    aput-object v1, v0, v3

    goto :goto_9

    .line 107
    :cond_4a
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_9
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aQ:Landroid/widget/TextView;

    return-object v0
.end method

.method private b(Landroid/widget/Spinner;Landroid/widget/Spinner;)V
    .registers 11

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 334
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aD:Z

    if-nez v0, :cond_8

    .line 370
    :goto_7
    return-void

    .line 336
    :cond_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/a/a;->a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    .line 338
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 339
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 340
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_prepareAudioDevice(II)I

    .line 341
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    .line 342
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 14110
    iput-object v1, v0, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 343
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget v3, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    iget v4, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/teamspeak/ts3client/a/k;->a(IIII)Z

    move-result v0

    if-nez v0, :cond_82

    .line 344
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->b()V

    .line 345
    iput-object v7, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    .line 346
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    .line 347
    iput-object v7, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 348
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aH:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 349
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aG:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 350
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aQ:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    .line 353
    :cond_82
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(Ljava/lang/Boolean;)V

    .line 354
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aO:Z

    if-eqz v0, :cond_e5

    .line 355
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/a/j;->a(I)V

    .line 358
    :goto_96
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_activateCaptureDevice(J)I

    .line 359
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    const-string v1, ""

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I

    .line 360
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    const-string v1, ""

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_openPlaybackDevice(JLjava/lang/String;Ljava/lang/String;)I

    .line 361
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 362
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    const-string v1, "vad"

    const-string v4, "true"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 363
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    const-string v1, "voiceactivation_level"

    const-string v4, "-50"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 364
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/a/a;->ay:J

    invoke-virtual {v0, v2, v3, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setLocalTestMode(JI)I

    .line 366
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->d()V

    .line 367
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->c()V

    goto/16 :goto_7

    .line 357
    :cond_e5
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/a/j;->b(I)V

    goto :goto_96
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/a/a;Landroid/widget/Spinner;Landroid/widget/Spinner;)V
    .registers 3

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/teamspeak/ts3client/a/a;->a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    return-void
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/a/a;)Z
    .registers 2

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aI:Z

    return v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/jni/Ts3Jni;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aw:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/a/a;)Z
    .registers 2

    .prologue
    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aD:Z

    return v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aF:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aE:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Button;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aH:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Button;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aG:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/a/a;)V
    .registers 4

    .prologue
    .line 32
    .line 21403
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/a/a;->a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    .line 21404
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 21404
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "recordStream"

    iget v2, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 21405
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 21405
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "playbackStream"

    iget v2, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 21406
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 21406
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "sampleRec"

    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aE:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 21407
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 21407
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "samplePlay"

    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aF:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 25413
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->b()V

    .line 32
    return-void
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/a/a;)Z
    .registers 2

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->aI:Z

    return v0
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/a/k;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    return-object v0
.end method

.method static synthetic p(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/a/k;
    .registers 2

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->ax:Lcom/teamspeak/ts3client/a/k;

    return-object v0
.end method

.method static synthetic q(Lcom/teamspeak/ts3client/a/a;)I
    .registers 2

    .prologue
    .line 32
    iget v0, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    return v0
.end method

.method static synthetic r(Lcom/teamspeak/ts3client/a/a;)I
    .registers 2

    .prologue
    .line 32
    iget v0, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    return v0
.end method

.method static synthetic s(Lcom/teamspeak/ts3client/a/a;)V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/teamspeak/ts3client/a/a;->B()V

    return-void
.end method

.method public static y()Lcom/teamspeak/ts3client/a/a;
    .registers 1

    .prologue
    .line 77
    sget-object v0, Lcom/teamspeak/ts3client/a/a;->au:Lcom/teamspeak/ts3client/a/a;

    if-nez v0, :cond_b

    .line 78
    new-instance v0, Lcom/teamspeak/ts3client/a/a;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/a/a;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/a/a;->au:Lcom/teamspeak/ts3client/a/a;

    .line 80
    :cond_b
    sget-object v0, Lcom/teamspeak/ts3client/a/a;->au:Lcom/teamspeak/ts3client/a/a;

    return-object v0
.end method

.method private z()V
    .registers 9

    .prologue
    const v7, 0x1090009

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 129
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 129
    const-string v1, "playbackStream"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 130
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 130
    const-string v1, "playbackStream"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_c9

    .line 131
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 134
    :goto_22
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 134
    const-string v1, "recordStream"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_d0

    .line 135
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 140
    :cond_33
    :goto_33
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/a/a;->a(Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    .line 141
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    iget v3, p0, Lcom/teamspeak/ts3client/a/a;->aA:I

    invoke-static {v0, v1, v3}, Lcom/teamspeak/ts3client/a/k;->a(Landroid/content/Context;II)Ljava/util/Vector;

    move-result-object v3

    .line 142
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/a/a;->az:I

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(Landroid/content/Context;I)Ljava/util/Vector;

    move-result-object v4

    .line 143
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aF:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/teamspeak/ts3client/a/a;->a(Ljava/util/Vector;)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v5, v7, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 144
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aE:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v5

    invoke-direct {p0, v3}, Lcom/teamspeak/ts3client/a/a;->a(Ljava/util/Vector;)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v5, v7, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    move v1, v2

    .line 146
    :goto_75
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_9e

    .line 147
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v5, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6093
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 147
    const-string v6, "samplePlay"

    invoke-interface {v5, v6, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-ne v0, v5, :cond_f8

    .line 148
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aF:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_9e
    move v1, v2

    .line 152
    :goto_9f
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_c8

    .line 153
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7093
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 153
    const-string v5, "sampleRec"

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v0, v4, :cond_fd

    .line 154
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aE:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 159
    :cond_c8
    return-void

    .line 133
    :cond_c9
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_22

    .line 136
    :cond_d0
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 136
    const-string v1, "recordStream"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_e3

    .line 137
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_33

    .line 138
    :cond_e3
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 138
    const-string v1, "recordStream"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_33

    .line 139
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_33

    .line 146
    :cond_f8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_75

    .line 152
    :cond_fd
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9f
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10

    .prologue
    const v6, 0x1090009

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 164
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/a/a;->at:Z

    .line 7207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 165
    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 8207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 166
    const-string v1, "audiosettings.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 167
    if-eqz v0, :cond_3b

    .line 168
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 168
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 168
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 169
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 169
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 169
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 171
    :cond_3b
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/a/a;->aI:Z

    .line 172
    const v0, 0x7f030018

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 174
    const v0, 0x7f0c0075

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aQ:Landroid/widget/TextView;

    .line 175
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aQ:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    const v0, 0x7f0c0067

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aG:Landroid/widget/Button;

    .line 178
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aG:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/a/c;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/a/c;-><init>(Lcom/teamspeak/ts3client/a/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    const v0, 0x7f0c0076

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aJ:Landroid/widget/Button;

    .line 198
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aJ:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/a/d;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/a/d;-><init>(Lcom/teamspeak/ts3client/a/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    const v0, 0x7f0c006a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aK:Landroid/widget/TextView;

    .line 206
    const v0, 0x7f0c006b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aL:Landroid/widget/TextView;

    .line 207
    const v0, 0x7f0c0071

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aM:Landroid/widget/TextView;

    .line 208
    const v0, 0x7f0c0070

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aN:Landroid/widget/TextView;

    .line 210
    const v0, 0x7f0c0073

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aF:Landroid/widget/Spinner;

    .line 211
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aF:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-static {v4, v5, v5}, Lcom/teamspeak/ts3client/a/k;->a(Landroid/content/Context;II)Ljava/util/Vector;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/teamspeak/ts3client/a/a;->a(Ljava/util/Vector;)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 212
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aF:Landroid/widget/Spinner;

    new-instance v2, Lcom/teamspeak/ts3client/a/e;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/a/e;-><init>(Lcom/teamspeak/ts3client/a/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 227
    const v0, 0x7f0c0074

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aE:Landroid/widget/Spinner;

    .line 228
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aE:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-static {v4, v5}, Lcom/teamspeak/ts3client/a/k;->a(Landroid/content/Context;I)Ljava/util/Vector;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/teamspeak/ts3client/a/a;->a(Ljava/util/Vector;)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 229
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aE:Landroid/widget/Spinner;

    new-instance v2, Lcom/teamspeak/ts3client/a/f;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/a/f;-><init>(Lcom/teamspeak/ts3client/a/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 244
    const v0, 0x7f0c006d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    .line 245
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    const-string v2, "audiosettings.array.play"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 246
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aB:Landroid/widget/Spinner;

    new-instance v2, Lcom/teamspeak/ts3client/a/g;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/a/g;-><init>(Lcom/teamspeak/ts3client/a/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 271
    const v0, 0x7f0c006e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    .line 272
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    const-string v2, "audiosettings.array.rec"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 273
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aC:Landroid/widget/Spinner;

    new-instance v2, Lcom/teamspeak/ts3client/a/h;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/a/h;-><init>(Lcom/teamspeak/ts3client/a/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 298
    const v0, 0x7f0c0068

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aH:Landroid/widget/Button;

    .line 299
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aH:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/a/i;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/a/i;-><init>(Lcom/teamspeak/ts3client/a/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aH:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 13316
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aG:Landroid/widget/Button;

    const-string v2, "audiosettings.button.start"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 13317
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aH:Landroid/widget/Button;

    const-string v2, "audiosettings.button.stop"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 13318
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aJ:Landroid/widget/Button;

    const-string v2, "button.save"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 13319
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aN:Landroid/widget/TextView;

    const-string v2, "audiosettings.text.play"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13320
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aM:Landroid/widget/TextView;

    const-string v2, "audiosettings.text.rec"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13321
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aK:Landroid/widget/TextView;

    const-string v2, "audiosettings.text.playbackstream"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13322
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aL:Landroid/widget/TextView;

    const-string v2, "audiosettings.text.recordstream"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13323
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->aQ:Landroid/widget/TextView;

    const-string v2, "audiosettings.error"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    invoke-direct {p0}, Lcom/teamspeak/ts3client/a/a;->z()V

    .line 312
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->a(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 88
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 330
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->c(Landroid/os/Bundle;)V

    .line 331
    return-void
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 424
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 425
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 3

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/teamspeak/ts3client/a/a;->B()V

    .line 430
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 430
    if-eqz v0, :cond_10

    .line 431
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/a;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 431
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->r()V

    .line 433
    :cond_10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/a;->at:Z

    .line 434
    const/4 v0, 0x0

    sput-object v0, Lcom/teamspeak/ts3client/a/a;->au:Lcom/teamspeak/ts3client/a/a;

    .line 435
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->onDismiss(Landroid/content/DialogInterface;)V

    .line 436
    return-void
.end method

.method public final s()V
    .registers 4

    .prologue
    .line 440
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 441
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 442
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/a/a;->aP:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/bb;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 443
    invoke-super {p0}, Landroid/support/v4/app/ax;->s()V

    .line 444
    return-void
.end method

.method public final t()V
    .registers 3

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/a;->aP:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 419
    invoke-super {p0}, Landroid/support/v4/app/ax;->t()V

    .line 420
    return-void
.end method
