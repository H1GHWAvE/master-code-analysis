.class final Lcom/teamspeak/ts3client/co;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private a:Lcom/teamspeak/ts3client/Ts3Application;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 2

    .prologue
    .line 1202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1203
    iput-object p1, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1204
    return-void
.end method


# virtual methods
.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 8

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1208
    const-string v0, "screen_rotation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1209
    const-string v0, "screen_rotation"

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1210
    packed-switch v0, :pswitch_data_c2

    .line 1221
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1221
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    .line 1225
    :cond_1b
    :goto_1b
    const-string v0, "android_overlay_setting"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 1226
    const-string v0, "android_overlay_setting"

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 1227
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_5b

    .line 1228
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1228
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_overlay"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1243
    :cond_42
    :goto_42
    return-void

    .line 1212
    :pswitch_43
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1212
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto :goto_1b

    .line 1215
    :pswitch_4b
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1215
    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto :goto_1b

    .line 1218
    :pswitch_53
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1218
    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto :goto_1b

    .line 1231
    :cond_5b
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-static {v0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8d

    .line 1232
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1232
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.action.MANAGE_OVERLAY_PERMISSION"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "package:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/16 v2, 0x1d32

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/StartGUIFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_42

    .line 1235
    :cond_8d
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1235
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_overlay"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_42

    .line 1239
    :cond_9f
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1239
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_overlay"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1240
    iget-object v0, p0, Lcom/teamspeak/ts3client/co;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1240
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_overlay_setting"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_42

    .line 1210
    :pswitch_data_c2
    .packed-switch 0x0
        :pswitch_43
        :pswitch_4b
        :pswitch_53
    .end packed-switch
.end method
