.class final Lcom/teamspeak/ts3client/f/r;
.super Landroid/support/v4/app/ax;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/y;


# instance fields
.field final at:Ljava/util/BitSet;

.field au:Landroid/view/KeyCharacterMap;

.field av:Z

.field final synthetic aw:Lcom/teamspeak/ts3client/f/p;

.field private ax:Landroid/widget/TextView;

.field private ay:Landroid/widget/CheckBox;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/f/p;)V
    .registers 3

    .prologue
    .line 81
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 83
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    .line 84
    const/4 v0, 0x3

    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/r;->au:Landroid/view/KeyCharacterMap;

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/f/r;->av:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/teamspeak/ts3client/f/p;B)V
    .registers 3

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/f/r;-><init>(Lcom/teamspeak/ts3client/f/p;)V

    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/r;)Landroid/widget/CheckBox;
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/r;->ay:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private a(Ljava/util/BitSet;)V
    .registers 7

    .prologue
    .line 204
    const-string v1, ""

    .line 205
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_a
    if-ltz v1, :cond_50

    .line 206
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/r;->au:Landroid/view/KeyCharacterMap;

    invoke-virtual {v2, v1}, Landroid/view/KeyCharacterMap;->getDisplayLabel(I)C

    move-result v2

    .line 207
    if-nez v2, :cond_38

    .line 208
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " <"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ">"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    :goto_31
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v1

    goto :goto_a

    .line 210
    :cond_38
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_31

    .line 212
    :cond_50
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/r;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14

    .prologue
    const/4 v10, 0x3

    const/4 v2, 0x0

    const/4 v9, -0x1

    const/4 v8, -0x2

    .line 91
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 92
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/p;->a(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2093
    iget-object v3, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 93
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-static {v5}, Lcom/teamspeak/ts3client/f/p;->a(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_intercept"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 94
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_59

    .line 95
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 96
    array-length v5, v4

    move v1, v2

    :goto_49
    if-ge v1, v5, :cond_59

    aget-object v6, v4, v1

    .line 98
    :try_start_4d
    iget-object v7, p0, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v7, v6}, Ljava/util/BitSet;->set(I)V
    :try_end_56
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_56} :catch_17c

    .line 96
    :goto_56
    add-int/lit8 v1, v1, 0x1

    goto :goto_49

    .line 105
    :cond_59
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 106
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 109
    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 110
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/r;->h()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 111
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setId(I)V

    .line 112
    iget-object v6, p0, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-static {v6}, Lcom/teamspeak/ts3client/f/p;->b(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    invoke-virtual {v1, v5, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 115
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 116
    invoke-virtual {v5}, Landroid/widget/TextView;->getId()I

    move-result v5

    invoke-virtual {v4, v10, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 118
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    .line 119
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setId(I)V

    .line 120
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    const-string v6, "Key"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    const/high16 v6, -0x10000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 122
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 123
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    invoke-direct {p0, v5}, Lcom/teamspeak/ts3client/f/r;->a(Ljava/util/BitSet;)V

    .line 125
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    invoke-virtual {v1, v5, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 128
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ax:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getId()I

    move-result v5

    invoke-virtual {v4, v10, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 130
    new-instance v5, Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ay:Landroid/widget/CheckBox;

    .line 131
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ay:Landroid/widget/CheckBox;

    iget-object v6, p0, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-static {v6}, Lcom/teamspeak/ts3client/f/p;->c(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ay:Landroid/widget/CheckBox;

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setId(I)V

    .line 133
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/r;->ay:Landroid/widget/CheckBox;

    invoke-virtual {v5, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 134
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/r;->ay:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/r;->h()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 137
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-direct {v4, v2, v8, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 139
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 140
    iget-object v6, p0, Lcom/teamspeak/ts3client/f/r;->ay:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->getId()I

    move-result v6

    invoke-virtual {v5, v10, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 141
    new-instance v6, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 142
    const-string v7, "button.save"

    invoke-static {v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 143
    new-instance v7, Lcom/teamspeak/ts3client/f/s;

    invoke-direct {v7, p0, v0, v1}, Lcom/teamspeak/ts3client/f/s;-><init>(Lcom/teamspeak/ts3client/f/r;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 164
    const-string v7, "button.clear"

    invoke-static {v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 165
    new-instance v7, Lcom/teamspeak/ts3client/f/t;

    invoke-direct {v7, p0}, Lcom/teamspeak/ts3client/f/t;-><init>(Lcom/teamspeak/ts3client/f/r;)V

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    invoke-virtual {v6, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 175
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 176
    invoke-virtual {v1, v3, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 177
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/p;->e(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 178
    invoke-virtual {p0, v2}, Lcom/teamspeak/ts3client/f/r;->a(Z)V

    .line 179
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v0

    .line 3076
    iput-object p0, v0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    .line 3207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 180
    new-instance v2, Lcom/teamspeak/ts3client/f/u;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/f/u;-><init>(Lcom/teamspeak/ts3client/f/r;)V

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 188
    return-object v1

    :catch_17c
    move-exception v6

    goto/16 :goto_56
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/f/r;->av:Z

    if-eqz v0, :cond_19

    .line 194
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 195
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v1

    .line 4084
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    .line 195
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    .line 196
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/f/r;->a(Ljava/util/BitSet;)V

    .line 198
    :cond_19
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v0

    .line 5084
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/v;->a:Ljava/util/BitSet;

    .line 198
    invoke-virtual {v0}, Ljava/util/BitSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/f/r;->av:Z

    .line 201
    :cond_28
    return-void
.end method

.method public final p_()V
    .registers 2

    .prologue
    .line 217
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/f/r;->av:Z

    .line 218
    return-void
.end method
