.class final Lcom/teamspeak/ts3client/d/a/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/a/a;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a/a;)V
    .registers 2

    .prologue
    .line 259
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/l;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 12

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 263
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/l;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 1171
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 263
    if-eqz v0, :cond_65

    .line 264
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 264
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 3061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 264
    new-array v1, v4, [J

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/l;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    .line 4087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 264
    aput-wide v4, v1, v6

    aput-wide v8, v1, v7

    const-string v4, "Unsubscribe Channel"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I

    .line 265
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 6159
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 265
    if-nez v0, :cond_5f

    .line 266
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 266
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/l;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    .line 8087
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 266
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 8167
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 8168
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 271
    :cond_5f
    :goto_5f
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/l;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/a/a;->b()V

    .line 272
    return-void

    .line 269
    :cond_65
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 269
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 10061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 269
    new-array v1, v4, [J

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/l;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    .line 11087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 269
    aput-wide v4, v1, v6

    aput-wide v8, v1, v7

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Subscribe Channel:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/a/l;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    .line 12087
    iget-wide v6, v5, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 269
    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I

    goto :goto_5f
.end method
