.class public final Lcom/teamspeak/ts3client/d/a/a;
.super Landroid/support/v4/app/ax;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# static fields
.field private static at:Lcom/teamspeak/ts3client/Ts3Application;


# instance fields
.field private au:Lcom/teamspeak/ts3client/data/a;

.field private av:Z


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/a;Z)V
    .registers 3

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 44
    iput-boolean p2, p0, Lcom/teamspeak/ts3client/d/a/a;->av:Z

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    return-object v0
.end method

.method static synthetic y()Lcom/teamspeak/ts3client/Ts3Application;
    .registers 1

    .prologue
    .line 35
    sget-object v0, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9

    .prologue
    const/4 v3, -0x1

    .line 49
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 50
    sput-object v0, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 50
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 51
    new-instance v0, Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 52
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 53
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 54
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 2207
    iget-object v2, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 57
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 3103
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 57
    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 58
    sget-object v2, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4214
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 58
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 5087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 58
    cmp-long v2, v2, v4

    if-eqz v2, :cond_69

    .line 59
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 60
    const-string v3, "dialog.channel.join.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 61
    new-instance v3, Lcom/teamspeak/ts3client/d/a/b;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/a/b;-><init>(Lcom/teamspeak/ts3client/d/a/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 84
    :cond_69
    iget-boolean v2, p0, Lcom/teamspeak/ts3client/d/a/a;->av:Z

    if-eqz v2, :cond_8a

    .line 85
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 86
    const-string v3, "dialog.channel.info.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 87
    new-instance v3, Lcom/teamspeak/ts3client/d/a/c;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/a/c;-><init>(Lcom/teamspeak/ts3client/d/a/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 98
    :cond_8a
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 99
    const-string v3, "dialog.channel.edit.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 100
    new-instance v3, Lcom/teamspeak/ts3client/d/a/d;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/a/d;-><init>(Lcom/teamspeak/ts3client/d/a/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 111
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 5127
    iget-boolean v2, v2, Lcom/teamspeak/ts3client/data/a;->r:Z

    .line 111
    if-nez v2, :cond_ca

    .line 112
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 113
    const-string v3, "dialog.channel.delete.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 114
    new-instance v3, Lcom/teamspeak/ts3client/d/a/e;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/a/e;-><init>(Lcom/teamspeak/ts3client/d/a/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 169
    :cond_ca
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 170
    const-string v3, "dialog.channel.move.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 171
    new-instance v3, Lcom/teamspeak/ts3client/d/a/h;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/a/h;-><init>(Lcom/teamspeak/ts3client/d/a/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 253
    sget-object v2, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6214
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 253
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 7087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 253
    cmp-long v2, v2, v4

    if-eqz v2, :cond_118

    .line 254
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 255
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 7171
    iget-boolean v3, v3, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 255
    if-eqz v3, :cond_119

    .line 256
    const-string v3, "dialog.channel.subscribe.text2"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :goto_10d
    new-instance v3, Lcom/teamspeak/ts3client/d/a/l;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/a/l;-><init>(Lcom/teamspeak/ts3client/d/a/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 276
    :cond_118
    return-object v0

    .line 258
    :cond_119
    const-string v3, "dialog.channel.subscribe.text1"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_10d
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 12

    .prologue
    .line 296
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    if-eqz v0, :cond_5e

    move-object v0, p1

    .line 297
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 10029
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    .line 297
    if-nez v0, :cond_5e

    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 10041
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 297
    const-string v1, "checkPW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 298
    sget-object v0, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 298
    sget-object v1, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11267
    iget-wide v1, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 298
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 12087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 298
    sget-object v6, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13061
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13356
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/e;->j:Ljava/util/HashMap;

    .line 298
    iget-object v7, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 14087
    iget-wide v8, v7, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 298
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "join "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/teamspeak/ts3client/d/a/a;->au:Lcom/teamspeak/ts3client/data/a;

    .line 15087
    iget-wide v8, v8, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 298
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestClientMove(JIJLjava/lang/String;Ljava/lang/String;)I

    .line 299
    sget-object v0, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 299
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 300
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/a;->b()V

    .line 303
    :cond_5e
    return-void
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 281
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 282
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 283
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 287
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 288
    sget-object v0, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 288
    if-eqz v0, :cond_12

    .line 289
    sget-object v0, Lcom/teamspeak/ts3client/d/a/a;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 289
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 290
    :cond_12
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 292
    return-void
.end method
