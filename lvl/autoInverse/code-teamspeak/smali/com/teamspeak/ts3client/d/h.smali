.class public final Lcom/teamspeak/ts3client/d/h;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private aA:Landroid/text/Spanned;

.field private aB:Landroid/widget/TextView;

.field private aC:Z

.field public at:Z

.field private au:Landroid/widget/TextView;

.field private av:Lcom/teamspeak/ts3client/Ts3Application;

.field private aw:Landroid/widget/Button;

.field private ax:Ljava/lang/String;

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 29
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/h;->at:Z

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->ax:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->ay:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->az:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aA:Landroid/text/Spanned;

    .line 35
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/h;->aC:Z

    .line 48
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    .line 49
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    .line 50
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 51
    iput-object p2, p0, Lcom/teamspeak/ts3client/d/h;->aA:Landroid/text/Spanned;

    .line 52
    iput-object p3, p0, Lcom/teamspeak/ts3client/d/h;->ay:Ljava/lang/String;

    .line 53
    iput-object p4, p0, Lcom/teamspeak/ts3client/d/h;->az:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 29
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/h;->at:Z

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->ax:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->ay:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->az:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aA:Landroid/text/Spanned;

    .line 35
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/h;->aC:Z

    .line 39
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    .line 40
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    .line 41
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 42
    iput-object p2, p0, Lcom/teamspeak/ts3client/d/h;->ax:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/teamspeak/ts3client/d/h;->ay:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/teamspeak/ts3client/d/h;->az:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 29
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/h;->at:Z

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->ax:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->ay:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->az:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aA:Landroid/text/Spanned;

    .line 35
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/h;->aC:Z

    .line 57
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    .line 58
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    .line 59
    iput-object p2, p0, Lcom/teamspeak/ts3client/d/h;->ax:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/teamspeak/ts3client/d/h;->ay:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Lcom/teamspeak/ts3client/d/h;->az:Ljava/lang/String;

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/h;->aC:Z

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/h;)V
    .registers 3

    .prologue
    .line 24
    .line 4066
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/h;->at:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/h;->aC:Z

    if-eqz v0, :cond_18

    .line 4067
    :cond_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4067
    if-eqz v0, :cond_18

    .line 4068
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 6337
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->d(Z)V

    .line 4069
    :cond_18
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/h;->aC:Z

    if-eqz v0, :cond_23

    .line 4070
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 4070
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 24
    :cond_23
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/h;)V
    .registers 1

    .prologue
    .line 24
    .line 8074
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/h;->b()V

    .line 24
    return-void
.end method

.method private d(Z)V
    .registers 2

    .prologue
    .line 151
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/d/h;->at:Z

    .line 152
    return-void
.end method

.method private y()V
    .registers 3

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/h;->at:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/h;->aC:Z

    if-eqz v0, :cond_18

    .line 67
    :cond_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 67
    if-eqz v0, :cond_18

    .line 68
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 2337
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->d(Z)V

    .line 69
    :cond_18
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/h;->aC:Z

    if-eqz v0, :cond_23

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 70
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 71
    :cond_23
    return-void
.end method

.method private z()V
    .registers 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/h;->b()V

    .line 75
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12

    .prologue
    const/16 v8, 0xf

    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, -0x2

    .line 79
    .line 3207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 79
    invoke-virtual {v0, v6}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 80
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/h;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/h;->av:Lcom/teamspeak/ts3client/Ts3Application;

    .line 81
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 82
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/h;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    .line 84
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setId(I)V

    .line 86
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/h;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/bb;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020049

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 87
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/h;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    const v3, 0x7f0700b3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 88
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 89
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/h;->ay:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 91
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    invoke-virtual {v1, v8, v8, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 93
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 94
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 95
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setId(I)V

    .line 98
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 99
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 100
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->aA:Landroid/text/Spanned;

    if-eqz v1, :cond_e6

    .line 101
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/h;->aA:Landroid/text/Spanned;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    invoke-static {}, Lcom/teamspeak/ts3client/data/d/n;->a()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 105
    :goto_a7
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 106
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 107
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/h;->aB:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 108
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 110
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 111
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 112
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/h;->az:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/i;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/i;-><init>(Lcom/teamspeak/ts3client/d/h;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    return-object v0

    .line 104
    :cond_e6
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/h;->ax:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a7
.end method

.method public final b(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 139
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 141
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 142
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->au:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/h;->at:Z

    if-eqz v0, :cond_f

    .line 147
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/h;->aw:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 148
    :cond_f
    return-void
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 127
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 128
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 129
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 133
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 134
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 136
    return-void
.end method
