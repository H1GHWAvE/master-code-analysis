.class public final Lcom/teamspeak/ts3client/d/r;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private at:Lcom/teamspeak/ts3client/Ts3Application;

.field private au:Landroid/widget/Button;

.field private av:Lcom/teamspeak/ts3client/data/a;

.field private aw:Landroid/widget/EditText;

.field private ax:I

.field private ay:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/teamspeak/ts3client/data/a;I)V
    .registers 5

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 35
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/r;->au:Landroid/widget/Button;

    .line 36
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/r;->aw:Landroid/widget/EditText;

    .line 37
    iput-object p2, p0, Lcom/teamspeak/ts3client/d/r;->av:Lcom/teamspeak/ts3client/data/a;

    .line 38
    iput p3, p0, Lcom/teamspeak/ts3client/d/r;->ax:I

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/r;)Lcom/teamspeak/ts3client/data/a;
    .registers 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/r;->av:Lcom/teamspeak/ts3client/data/a;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/r;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/r;->aw:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/r;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/r;->at:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/r;)I
    .registers 2

    .prologue
    .line 24
    iget v0, p0, Lcom/teamspeak/ts3client/d/r;->ax:I

    return v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/r;)V
    .registers 1

    .prologue
    .line 24
    .line 3042
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/r;->b()V

    .line 24
    return-void
.end method

.method private y()V
    .registers 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/r;->b()V

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13

    .prologue
    const/4 v9, 0x3

    const/4 v4, 0x1

    const/16 v8, 0xf

    const/4 v7, -0x1

    const/4 v6, -0x2

    .line 48
    .line 1207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 48
    invoke-virtual {v0, v4}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 49
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/r;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/r;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 51
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/r;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    .line 53
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setId(I)V

    .line 55
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/r;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/bb;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020049

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 56
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/r;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    const v3, 0x7f0700b3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 57
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 58
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    const-string v2, "passworddialog.text"

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/r;->av:Lcom/teamspeak/ts3client/data/a;

    .line 2103
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 58
    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 60
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    invoke-virtual {v1, v8, v8, v8, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 62
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 63
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 64
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->aw:Landroid/widget/EditText;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setId(I)V

    .line 68
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/r;->aw:Landroid/widget/EditText;

    const v2, 0x80001

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 69
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 70
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/r;->ay:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getId()I

    move-result v2

    invoke-virtual {v1, v9, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 71
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/r;->aw:Landroid/widget/EditText;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 74
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/r;->aw:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getId()I

    move-result v2

    invoke-virtual {v1, v9, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 75
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/r;->au:Landroid/widget/Button;

    const-string v3, "dialog.channel.join.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/r;->au:Landroid/widget/Button;

    invoke-virtual {v2, v9}, Landroid/widget/Button;->setId(I)V

    .line 77
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/r;->au:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/s;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/s;-><init>(Lcom/teamspeak/ts3client/d/r;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/r;->au:Landroid/widget/Button;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    return-object v0
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 96
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 97
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 98
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 102
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 103
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 105
    return-void
.end method
