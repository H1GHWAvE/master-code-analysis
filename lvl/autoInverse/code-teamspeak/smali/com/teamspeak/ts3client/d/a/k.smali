.class final Lcom/teamspeak/ts3client/d/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ArrayAdapter;

.field final synthetic b:Landroid/widget/Spinner;

.field final synthetic c:Landroid/app/Dialog;

.field final synthetic d:Lcom/teamspeak/ts3client/d/a/h;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a/h;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/app/Dialog;)V
    .registers 5

    .prologue
    .line 233
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/k;->d:Lcom/teamspeak/ts3client/d/a/h;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/a/k;->a:Landroid/widget/ArrayAdapter;

    iput-object p3, p0, Lcom/teamspeak/ts3client/d/a/k;->b:Landroid/widget/Spinner;

    iput-object p4, p0, Lcom/teamspeak/ts3client/d/a/k;->c:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 13

    .prologue
    .line 238
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/k;->a:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/k;->b:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 239
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 239
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 2061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 239
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/k;->d:Lcom/teamspeak/ts3client/d/a/h;

    iget-object v4, v4, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    .line 3087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 4087
    iget-wide v6, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 239
    const-wide/16 v8, 0x0

    const-string v10, "Move Channel"

    invoke-virtual/range {v1 .. v10}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelMove(JJJJLjava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/k;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 241
    return-void
.end method
