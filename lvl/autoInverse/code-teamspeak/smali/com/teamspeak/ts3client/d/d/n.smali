.class public final Lcom/teamspeak/ts3client/d/d/n;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field at:Lcom/teamspeak/ts3client/d/d/c;

.field private au:Lcom/teamspeak/ts3client/Ts3Application;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/d/d/c;)V
    .registers 2

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 26
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/d/n;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/n;->au:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 19

    .prologue
    .line 30
    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/Ts3Application;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/teamspeak/ts3client/d/d/n;->au:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31
    const v2, 0x7f030057

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    .line 1207
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 32
    const-string v4, "temppass.title"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 33
    const v3, 0x7f0c01d6

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 34
    const v4, 0x7f0c01d8

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 35
    const v5, 0x7f0c01da

    invoke-virtual {v2, v5}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 36
    const v6, 0x7f0c01dc

    invoke-virtual {v2, v6}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 37
    const v7, 0x7f0c01de

    invoke-virtual {v2, v7}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 38
    const v8, 0x7f0c01e0

    invoke-virtual {v2, v8}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 39
    const-string v9, "temppass.info.password"

    const v10, 0x7f0c01d5

    invoke-static {v9, v2, v10}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 40
    const-string v9, "temppass.info.creator"

    const v10, 0x7f0c01d7

    invoke-static {v9, v2, v10}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 41
    const-string v9, "temppass.info.channel"

    const v10, 0x7f0c01d9

    invoke-static {v9, v2, v10}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 42
    const-string v9, "temppass.info.channelpassword"

    const v10, 0x7f0c01db

    invoke-static {v9, v2, v10}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 43
    const-string v9, "temppass.info.valid"

    const v10, 0x7f0c01dd

    invoke-static {v9, v2, v10}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 44
    const-string v9, "temppass.info.description"

    const v10, 0x7f0c01df

    invoke-static {v9, v2, v10}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 45
    const v9, 0x7f0c01e1

    invoke-virtual {v2, v9}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    .line 46
    const v10, 0x7f0c01e2

    invoke-virtual {v2, v10}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 47
    const-string v11, "temppass.info.button.share"

    invoke-static {v11}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const-string v11, "button.delete"

    invoke-static {v11}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 49
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 2050
    iget-object v11, v11, Lcom/teamspeak/ts3client/d/d/c;->c:Ljava/lang/String;

    .line 49
    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 3032
    iget-object v3, v3, Lcom/teamspeak/ts3client/d/d/c;->a:Ljava/lang/String;

    .line 50
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 3058
    iget-wide v12, v3, Lcom/teamspeak/ts3client/d/d/c;->f:J

    .line 51
    const-wide/16 v14, 0x0

    cmp-long v3, v12, v14

    if-eqz v3, :cond_ee

    .line 52
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/d/d/n;->au:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 52
    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 4058
    iget-wide v12, v4, Lcom/teamspeak/ts3client/d/d/c;->f:J

    .line 52
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    .line 4103
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 52
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    :cond_ee
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 5062
    iget-object v3, v3, Lcom/teamspeak/ts3client/d/d/c;->g:Ljava/lang/String;

    .line 53
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 6045
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    iget-wide v12, v3, Lcom/teamspeak/ts3client/d/d/c;->d:J

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-direct {v6, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    iget-wide v12, v3, Lcom/teamspeak/ts3client/d/d/c;->e:J

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-direct {v6, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 54
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 7036
    iget-object v3, v3, Lcom/teamspeak/ts3client/d/d/c;->b:Ljava/lang/String;

    .line 55
    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    new-instance v3, Lcom/teamspeak/ts3client/d/d/o;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/teamspeak/ts3client/d/d/o;-><init>(Lcom/teamspeak/ts3client/d/d/n;)V

    invoke-virtual {v9, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    new-instance v3, Lcom/teamspeak/ts3client/d/d/p;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/teamspeak/ts3client/d/d/p;-><init>(Lcom/teamspeak/ts3client/d/d/n;)V

    invoke-virtual {v10, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    return-object v2
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 82
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 83
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 84
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 88
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 89
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 91
    return-void
.end method
