.class final Lcom/teamspeak/ts3client/bb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/teamspeak/ts3client/t;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/t;Z)V
    .registers 3

    .prologue
    .line 303
    iput-object p1, p0, Lcom/teamspeak/ts3client/bb;->b:Lcom/teamspeak/ts3client/t;

    iput-boolean p2, p0, Lcom/teamspeak/ts3client/bb;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    .line 306
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/teamspeak/ts3client/bb;->b:Lcom/teamspeak/ts3client/t;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    const v2, 0x7f0700b5

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 307
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 308
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/teamspeak/ts3client/bb;->b:Lcom/teamspeak/ts3client/t;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 309
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 310
    iget-boolean v2, p0, Lcom/teamspeak/ts3client/bb;->a:Z

    if-eqz v2, :cond_3b

    .line 311
    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/teamspeak/ts3client/bb;->b:Lcom/teamspeak/ts3client/t;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 312
    const-string v3, "messages.newserver.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 315
    :cond_3b
    new-instance v2, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/teamspeak/ts3client/bb;->b:Lcom/teamspeak/ts3client/t;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 316
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 317
    new-instance v3, Landroid/widget/Button;

    iget-object v4, p0, Lcom/teamspeak/ts3client/bb;->b:Lcom/teamspeak/ts3client/t;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 318
    const-string v4, "button.ok"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 319
    new-instance v4, Lcom/teamspeak/ts3client/bc;

    invoke-direct {v4, p0, v0, v2}, Lcom/teamspeak/ts3client/bc;-><init>(Lcom/teamspeak/ts3client/bb;Landroid/app/Dialog;Landroid/widget/EditText;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 329
    const-string v2, "messages.newserver.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 330
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 331
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 332
    return-void
.end method
