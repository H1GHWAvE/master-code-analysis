.class public Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;
.super Landroid/webkit/WebView;
.source "SourceFile"


# instance fields
.field private a:Lcom/teamspeak/ts3client/customs/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 11
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .registers 7

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->a:Lcom/teamspeak/ts3client/customs/f;

    if-eqz v0, :cond_20

    .line 29
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->getContentHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->getScale()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->getHeight()I

    move-result v1

    add-int/2addr v1, p2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 30
    const/high16 v1, 0x40a00000    # 5.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_20

    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->a:Lcom/teamspeak/ts3client/customs/f;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/customs/f;->a()V

    .line 33
    :cond_20
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->onScrollChanged(IIII)V

    .line 34
    return-void
.end method

.method public setOnBottomReachedListener(Lcom/teamspeak/ts3client/customs/f;)V
    .registers 2

    .prologue
    .line 23
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;->a:Lcom/teamspeak/ts3client/customs/f;

    .line 24
    return-void
.end method
