.class public final Lcom/teamspeak/ts3client/chat/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/d/s;


# instance fields
.field a:Ljava/lang/String;

.field public b:Z

.field c:Ljava/util/ArrayList;

.field d:I

.field e:Ljava/lang/String;

.field f:Lcom/teamspeak/ts3client/chat/h;

.field g:Lcom/teamspeak/ts3client/data/c;

.field h:Ljava/util/Date;

.field private i:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V
    .registers 5

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/teamspeak/ts3client/chat/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;Z)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;Z)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/chat/a;->b:Z

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->c:Ljava/util/ArrayList;

    .line 15
    iput v1, p0, Lcom/teamspeak/ts3client/chat/a;->d:I

    .line 19
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->h:Ljava/util/Date;

    .line 20
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/chat/a;->i:Z

    .line 26
    iput-object p2, p0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/a;->a:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 29
    iput-boolean p4, p0, Lcom/teamspeak/ts3client/chat/a;->i:Z

    .line 30
    if-nez p4, :cond_30

    .line 31
    if-eqz p3, :cond_30

    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 1118
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    .line 32
    if-nez v0, :cond_2d

    .line 33
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/a;->a()V

    .line 35
    :cond_2d
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/a;->c()V

    .line 38
    :cond_30
    return-void
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/chat/a;)Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 10
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/chat/a;Ljava/util/Date;)Ljava/util/Date;
    .registers 2

    .prologue
    .line 10
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/a;->h:Ljava/util/Date;

    return-object p1
.end method

.method private a(Lcom/teamspeak/ts3client/chat/h;)V
    .registers 2

    .prologue
    .line 109
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/a;->f:Lcom/teamspeak/ts3client/chat/h;

    .line 110
    return-void
.end method

.method private static synthetic b(Lcom/teamspeak/ts3client/chat/a;)V
    .registers 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/a;->c()V

    return-void
.end method

.method private d()Ljava/util/Date;
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->h:Ljava/util/Date;

    return-object v0
.end method

.method private e()I
    .registers 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method private f()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method private g()I
    .registers 2

    .prologue
    .line 84
    iget v0, p0, Lcom/teamspeak/ts3client/chat/a;->d:I

    return v0
.end method

.method private h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method private i()Lcom/teamspeak/ts3client/data/c;
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    return-object v0
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/chat/a;->b:Z

    return v0
.end method

.method private k()V
    .registers 2

    .prologue
    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/chat/a;->b:Z

    .line 106
    return-void
.end method

.method private l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    if-eqz v0, :cond_1f

    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 47
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/data/c;->a(Lcom/teamspeak/ts3client/data/d/s;)V

    .line 48
    :cond_1f
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/chat/y;)V
    .registers 4

    .prologue
    .line 52
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 52
    if-eqz v0, :cond_1a

    .line 53
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 53
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/chat/b;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/chat/b;-><init>(Lcom/teamspeak/ts3client/chat/a;Lcom/teamspeak/ts3client/chat/y;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 63
    :cond_1a
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 92
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/a;->a:Ljava/lang/String;

    .line 93
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/a;->c()V

    .line 94
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/chat/a;->d:I

    .line 114
    return-void
.end method

.method final c()V
    .registers 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->f:Lcom/teamspeak/ts3client/chat/h;

    if-eqz v0, :cond_9

    .line 118
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->f:Lcom/teamspeak/ts3client/chat/h;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/h;->a()V

    .line 119
    :cond_9
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->d()V

    .line 120
    return-void
.end method

.method public final y()V
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->f:Lcom/teamspeak/ts3client/chat/h;

    if-eqz v0, :cond_9

    .line 68
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/a;->f:Lcom/teamspeak/ts3client/chat/h;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/h;->a()V

    .line 69
    :cond_9
    return-void
.end method
