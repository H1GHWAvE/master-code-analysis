.class public final Lcom/teamspeak/ts3client/chat/c;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/teamspeak/ts3client/Ts3Application;

.field private b:Landroid/widget/ListView;

.field private c:Lcom/teamspeak/ts3client/chat/d;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 33
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/c;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 35
    const v0, 0x7f030025

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 36
    const v0, 0x7f0c00ea

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->b:Landroid/widget/ListView;

    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->c:Lcom/teamspeak/ts3client/chat/d;

    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->b:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/chat/c;->c:Lcom/teamspeak/ts3client/chat/d;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 39
    if-eqz v0, :cond_45

    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 40
    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/t;->e(Z)V

    .line 42
    :cond_45
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/c;->n()V

    .line 43
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 29
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 61
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 62
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 63
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 24
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 57
    return-void
.end method

.method public final t()V
    .registers 3

    .prologue
    .line 49
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->t()V

    .line 50
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50
    if-eqz v0, :cond_13

    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/c;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 51
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->e(Z)V

    .line 52
    :cond_13
    return-void
.end method
