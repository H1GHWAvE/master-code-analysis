.class public final Lcom/teamspeak/ts3client/chat/d;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# static fields
.field private static e:Lcom/teamspeak/ts3client/chat/d;

.field private static f:Landroid/support/v4/app/bi;


# instance fields
.field public a:Ljava/util/ArrayList;

.field public b:Ljava/util/HashMap;

.field public c:Lcom/teamspeak/ts3client/chat/a;

.field public d:Lcom/teamspeak/ts3client/chat/a;

.field private g:Landroid/view/LayoutInflater;

.field private h:Landroid/content/Context;

.field private i:Lcom/teamspeak/ts3client/Ts3Application;

.field private j:Ljava/text/DateFormat;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays",
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    .line 42
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->g:Landroid/view/LayoutInflater;

    .line 43
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/d;->h:Landroid/content/Context;

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->i:Lcom/teamspeak/ts3client/Ts3Application;

    .line 45
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->j:Ljava/text/DateFormat;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/chat/d;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->i:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method public static a()Lcom/teamspeak/ts3client/chat/d;
    .registers 1

    .prologue
    .line 49
    sget-object v0, Lcom/teamspeak/ts3client/chat/d;->e:Lcom/teamspeak/ts3client/chat/d;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
    .registers 2

    .prologue
    .line 53
    sget-object v0, Lcom/teamspeak/ts3client/chat/d;->e:Lcom/teamspeak/ts3client/chat/d;

    if-nez v0, :cond_b

    .line 54
    new-instance v0, Lcom/teamspeak/ts3client/chat/d;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/chat/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/teamspeak/ts3client/chat/d;->e:Lcom/teamspeak/ts3client/chat/d;

    .line 56
    :cond_b
    sget-object v0, Lcom/teamspeak/ts3client/chat/d;->e:Lcom/teamspeak/ts3client/chat/d;

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/bi;)V
    .registers 1

    .prologue
    .line 60
    sput-object p0, Lcom/teamspeak/ts3client/chat/d;->f:Landroid/support/v4/app/bi;

    .line 61
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 1049
    sget-object v0, Lcom/teamspeak/ts3client/chat/d;->e:Lcom/teamspeak/ts3client/chat/d;

    .line 64
    const-string v1, "CHANNEL"

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/d;->d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;

    move-result-object v6

    new-instance v0, Lcom/teamspeak/ts3client/chat/y;

    const/4 v1, 0x0

    const-string v2, ""

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/chat/y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    invoke-virtual {v6, v0}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 65
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/chat/d;)Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Lcom/teamspeak/ts3client/chat/a;)V
    .registers 4

    .prologue
    .line 174
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 175
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 176
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    .line 7097
    iget-object v1, p1, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 7188
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 176
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/d;->d()V

    .line 179
    :cond_19
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 2049
    sget-object v0, Lcom/teamspeak/ts3client/chat/d;->e:Lcom/teamspeak/ts3client/chat/d;

    .line 68
    const-string v1, "SERVER"

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/d;->d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;

    move-result-object v6

    new-instance v0, Lcom/teamspeak/ts3client/chat/y;

    const/4 v1, 0x0

    const-string v2, ""

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/chat/y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    invoke-virtual {v6, v0}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 69
    return-void
.end method

.method static synthetic e()Landroid/support/v4/app/bi;
    .registers 1

    .prologue
    .line 24
    sget-object v0, Lcom/teamspeak/ts3client/chat/d;->f:Landroid/support/v4/app/bi;

    return-object v0
.end method

.method private f()V
    .registers 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 80
    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    .line 81
    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    .line 82
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 83
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    .line 87
    return-void
.end method

.method private g()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 165
    new-instance v0, Lcom/teamspeak/ts3client/chat/a;

    const-string v1, "Server Tab"

    const-string v2, "SERVER"

    invoke-direct {v0, v1, v2, v4}, Lcom/teamspeak/ts3client/chat/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    .line 166
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    .line 5105
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/chat/a;->b:Z

    .line 167
    new-instance v0, Lcom/teamspeak/ts3client/chat/a;

    const-string v1, "Channel Tab"

    const-string v2, "CHANNEL"

    invoke-direct {v0, v1, v2, v4}, Lcom/teamspeak/ts3client/chat/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    .line 168
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    .line 6105
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/chat/a;->b:Z

    .line 169
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/chat/d;->a(Lcom/teamspeak/ts3client/chat/a;)V

    .line 170
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/chat/d;->a(Lcom/teamspeak/ts3client/chat/a;)V

    .line 171
    return-void
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/chat/a;)V
    .registers 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 73
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    .line 2123
    iget-object v1, p1, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 74
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    :cond_14
    return-void
.end method

.method public final b()Lcom/teamspeak/ts3client/chat/a;
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    return-object v0
.end method

.method public final c()Lcom/teamspeak/ts3client/chat/a;
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/Boolean;
    .registers 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 91
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 93
    :goto_d
    return-object v0

    :cond_e
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_d
.end method

.method public final d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;
    .registers 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 102
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/a;

    .line 104
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->i:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 182
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/chat/g;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/chat/g;-><init>(Lcom/teamspeak/ts3client/chat/d;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 188
    return-void
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 119
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10

    .prologue
    .line 128
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f030026

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 129
    const v0, 0x7f0c00ed

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 130
    const v1, 0x7f0c00ec

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 131
    const v2, 0x7f0c00ee

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 132
    const v3, 0x7f0c00ef

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 133
    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/teamspeak/ts3client/chat/a;

    .line 3101
    iget-boolean v4, v4, Lcom/teamspeak/ts3client/chat/a;->b:Z

    .line 133
    if-eqz v4, :cond_3e

    .line 134
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    :cond_3e
    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/teamspeak/ts3client/chat/a;

    .line 4088
    iget-object v4, v4, Lcom/teamspeak/ts3client/chat/a;->a:Ljava/lang/String;

    .line 135
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/d;->j:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/a;

    .line 5072
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->h:Ljava/util/Date;

    .line 136
    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/a;

    .line 5076
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 138
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/a;

    .line 5084
    iget v0, v0, Lcom/teamspeak/ts3client/chat/a;->d:I

    .line 138
    sub-int v0, v2, v0

    if-lez v0, :cond_83

    .line 139
    const-string v0, "chat.newmessages"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    :cond_83
    new-instance v0, Lcom/teamspeak/ts3client/chat/e;

    invoke-direct {v0, p0, p1}, Lcom/teamspeak/ts3client/chat/e;-><init>(Lcom/teamspeak/ts3client/chat/d;I)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    const v0, 0x7f0c00eb

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 150
    new-instance v1, Lcom/teamspeak/ts3client/chat/f;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/chat/f;-><init>(Lcom/teamspeak/ts3client/chat/d;I)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    return-object v5
.end method
