.class public final Lcom/teamspeak/ts3client/t;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# static fields
.field private static i:Lcom/teamspeak/ts3client/customs/FloatingButton;


# instance fields
.field public a:Lcom/teamspeak/ts3client/Ts3Application;

.field private aA:Z

.field private aB:Lcom/teamspeak/ts3client/bf;

.field private aC:Z

.field private aD:Landroid/content/BroadcastReceiver;

.field private aE:Lcom/teamspeak/ts3client/d/c/d;

.field private aF:Z

.field private aG:Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private aH:Landroid/telephony/PhoneStateListener;

.field private aI:Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private at:I

.field private au:Z

.field private av:Lcom/teamspeak/ts3client/chat/c;

.field private aw:Z

.field private ax:Z

.field private ay:Z

.field private az:Z

.field b:Z

.field c:Z

.field d:I

.field e:Z

.field f:Z

.field g:Lcom/teamspeak/ts3client/customs/FloatingButton;

.field h:Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private j:Lcom/teamspeak/ts3client/data/customExpandableListView;

.field private k:Lcom/teamspeak/ts3client/d/h;

.field private l:Lcom/teamspeak/ts3client/data/g;

.field private m:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 71
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 73
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/t;->b:Z

    .line 74
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/t;->c:Z

    .line 77
    const/4 v0, 0x2

    iput v0, p0, Lcom/teamspeak/ts3client/t;->at:I

    .line 80
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/t;->aw:Z

    .line 81
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/t;->ax:Z

    .line 82
    const/16 v0, 0x16

    iput v0, p0, Lcom/teamspeak/ts3client/t;->d:I

    .line 90
    new-instance v0, Lcom/teamspeak/ts3client/u;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/u;-><init>(Lcom/teamspeak/ts3client/t;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->aD:Landroid/content/BroadcastReceiver;

    .line 106
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/t;->aF:Z

    .line 110
    new-instance v0, Lcom/teamspeak/ts3client/ah;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/ah;-><init>(Lcom/teamspeak/ts3client/t;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->aH:Landroid/telephony/PhoneStateListener;

    .line 1125
    return-void
.end method

.method static synthetic J()Lcom/teamspeak/ts3client/customs/FloatingButton;
    .registers 1

    .prologue
    .line 67
    sget-object v0, Lcom/teamspeak/ts3client/t;->i:Lcom/teamspeak/ts3client/customs/FloatingButton;

    return-object v0
.end method

.method private K()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 187
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->f:Z

    if-nez v0, :cond_a4

    .line 188
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/t;->c:Z

    .line 189
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    .line 2117
    iput-boolean v4, v0, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 190
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 190
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 190
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 191
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 191
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 191
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 192
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020096

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 193
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 8243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 193
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 198
    :goto_4f
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->e:Z

    if-nez v0, :cond_b7

    .line 199
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/t;->b:Z

    .line 200
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 200
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 200
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 201
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 201
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 201
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 202
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020081

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 203
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 14243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 203
    invoke-virtual {v0, v4}, Ljava/util/BitSet;->set(I)V

    .line 208
    :goto_94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 209
    new-instance v1, Lcom/teamspeak/ts3client/as;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/as;-><init>(Lcom/teamspeak/ts3client/t;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 219
    return-void

    .line 195
    :cond_a4
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020098

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_4f

    .line 205
    :cond_b7
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020084

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_94
.end method

.method private L()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 222
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->b:Z

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->e:Z

    .line 223
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->c:Z

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->f:Z

    .line 224
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    .line 15117
    iput-boolean v4, v0, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 225
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020084

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 226
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020098

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 228
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/t;->c:Z

    .line 230
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 230
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 230
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 231
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 231
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 231
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 232
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/t;->b:Z

    .line 235
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 235
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 235
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 236
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 236
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 236
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 237
    return-void
.end method

.method private M()V
    .registers 5

    .prologue
    .line 276
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 277
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 278
    const-string v1, "disconnect.info"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 279
    const-string v1, "disconnect.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 280
    const/4 v1, -0x1

    const-string v2, "disconnect.button"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/az;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/az;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 291
    const/4 v1, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/ba;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/ba;-><init>(Lcom/teamspeak/ts3client/t;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 298
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 299
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 300
    return-void
.end method

.method private N()V
    .registers 2

    .prologue
    .line 337
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/t;->d(Z)V

    .line 338
    return-void
.end method

.method private O()Z
    .registers 2

    .prologue
    .line 500
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->au:Z

    return v0
.end method

.method private P()V
    .registers 3

    .prologue
    .line 666
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/ai;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ai;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 672
    return-void
.end method

.method private Q()V
    .registers 3

    .prologue
    .line 839
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aG:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 50165
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/customs/FloatingButton;->c:Z

    if-nez v1, :cond_d

    .line 50167
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->a(Z)V

    .line 50168
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->invalidate()V

    .line 840
    :cond_d
    return-void
.end method

.method private R()V
    .registers 3

    .prologue
    .line 870
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/ap;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ap;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 878
    return-void
.end method

.method private S()V
    .registers 6

    .prologue
    const/4 v1, 0x1

    .line 1076
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50243
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50244
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50245
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 1076
    if-nez v0, :cond_25

    .line 1077
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50246
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50247
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 50248
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    .line 1079
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50250
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50251
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1079
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50252
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50253
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1079
    const-string v1, "SubscribeAll"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelSubscribeAll(JLjava/lang/String;)I

    .line 1087
    :cond_24
    :goto_24
    return-void

    .line 1081
    :cond_25
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50254
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50255
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 50256
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    .line 1083
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50258
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50259
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1083
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50260
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50261
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1083
    const-string v1, "UnsubscribeAll"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelUnsubscribeAll(JLjava/lang/String;)I

    .line 1084
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50262
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50263
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 1084
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->e()[J

    move-result-object v0

    if-eqz v0, :cond_24

    .line 1085
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50264
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50265
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1085
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50266
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1085
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50268
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50269
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 1085
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/ab;->e()[J

    move-result-object v1

    const-string v4, "Subscribe after UnsubscribeAll"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I

    goto :goto_24
.end method

.method private T()Z
    .registers 2

    .prologue
    .line 1105
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->ax:Z

    return v0
.end method

.method private static a(F)I
    .registers 3

    .prologue
    .line 182
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 183
    mul-float/2addr v0, p0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private a(Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 848
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/ao;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/teamspeak/ts3client/ao;-><init>(Lcom/teamspeak/ts3client/t;Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 867
    return-void
.end method

.method private a([Lcom/teamspeak/ts3client/data/a;)V
    .registers 4

    .prologue
    .line 454
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 455
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/v;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/v;-><init>(Lcom/teamspeak/ts3client/t;[Lcom/teamspeak/ts3client/data/a;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 465
    :cond_12
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/t;)Z
    .registers 2

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->aC:Z

    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/t;Z)Z
    .registers 2

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/t;->aC:Z

    return p1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method private b(Lcom/teamspeak/ts3client/data/a;)V
    .registers 6

    .prologue
    .line 961
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->l()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 962
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50176
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 962
    instance-of v0, v0, Lcom/teamspeak/ts3client/c;

    if-eqz v0, :cond_f

    .line 972
    :cond_e
    :goto_e
    return-void

    .line 964
    :cond_f
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50177
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 964
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Channelinfo: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 50178
    sget-object v0, Lcom/teamspeak/ts3client/c;->b:Lcom/teamspeak/ts3client/data/a;

    if-ne v0, p1, :cond_44

    .line 50179
    sget-object v0, Lcom/teamspeak/ts3client/c;->a:Lcom/teamspeak/ts3client/c;

    .line 50185
    :goto_2d
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 966
    invoke-virtual {v1}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v1

    .line 967
    const-string v2, "ChannelInfo"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 968
    invoke-virtual {v1}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 969
    const/16 v0, 0x1001

    invoke-virtual {v1, v0}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 970
    invoke-virtual {v1}, Landroid/support/v4/app/cd;->i()I

    goto :goto_e

    .line 50181
    :cond_44
    new-instance v0, Lcom/teamspeak/ts3client/c;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/c;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/c;->a:Lcom/teamspeak/ts3client/c;

    .line 50182
    sput-object p1, Lcom/teamspeak/ts3client/c;->b:Lcom/teamspeak/ts3client/data/a;

    .line 50184
    sget-object v0, Lcom/teamspeak/ts3client/c;->a:Lcom/teamspeak/ts3client/c;

    goto :goto_2d
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/d/h;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->k:Lcom/teamspeak/ts3client/d/h;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/data/g;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->l:Lcom/teamspeak/ts3client/data/g;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/data/customExpandableListView;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/t;)V
    .registers 6

    .prologue
    const/4 v1, 0x1

    .line 67
    .line 50273
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50285
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50286
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50287
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 50273
    if-nez v0, :cond_25

    .line 50274
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50288
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50289
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 50290
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    .line 50276
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50292
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50293
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50276
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50294
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50295
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50276
    const-string v1, "SubscribeAll"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelSubscribeAll(JLjava/lang/String;)I

    :cond_24
    :goto_24
    return-void

    .line 50278
    :cond_25
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50296
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50297
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 50298
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    .line 50280
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50300
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50301
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50280
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50302
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50303
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50280
    const-string v1, "UnsubscribeAll"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelUnsubscribeAll(JLjava/lang/String;)I

    .line 50281
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50304
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50305
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50281
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->e()[J

    move-result-object v0

    if-eqz v0, :cond_24

    .line 50282
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50306
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50307
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50282
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50308
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50309
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50282
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50310
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50311
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50282
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/ab;->e()[J

    move-result-object v1

    const-string v4, "Subscribe after UnsubscribeAll"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I

    goto :goto_24
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/chat/c;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->av:Lcom/teamspeak/ts3client/chat/c;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/t;)V
    .registers 3

    .prologue
    .line 67
    .line 50312
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aG:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 50314
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/customs/FloatingButton;->c:Z

    if-nez v1, :cond_d

    .line 50316
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->a(Z)V

    .line 50317
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/customs/FloatingButton;->invalidate()V

    .line 67
    :cond_d
    return-void
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/t;)Z
    .registers 2

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->ay:Z

    return v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/t;)Z
    .registers 2

    .prologue
    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->b:Z

    return v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/t;)I
    .registers 2

    .prologue
    .line 67
    iget v0, p0, Lcom/teamspeak/ts3client/t;->d:I

    return v0
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/customs/FloatingButton;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aI:Lcom/teamspeak/ts3client/customs/FloatingButton;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .registers 2

    .prologue
    .line 511
    .line 50133
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->au:Z

    .line 511
    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->az:Z

    if-eqz v0, :cond_c

    .line 512
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/t;->g(Z)V

    .line 513
    :cond_c
    return-void
.end method

.method final B()V
    .registers 8

    .prologue
    const v6, 0x7f020096

    const v5, 0x7f020081

    const/4 v4, 0x0

    .line 523
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->aw:Z

    if-eqz v0, :cond_c

    .line 663
    :goto_b
    return-void

    .line 525
    :cond_c
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    const v1, 0x7f0c0160

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->aI:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 526
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aI:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f02008d

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 527
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aI:Lcom/teamspeak/ts3client/customs/FloatingButton;

    new-instance v1, Lcom/teamspeak/ts3client/y;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/y;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 535
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    const v1, 0x7f0c015d

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->aG:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 536
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aG:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020088

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 537
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aG:Lcom/teamspeak/ts3client/customs/FloatingButton;

    new-instance v1, Lcom/teamspeak/ts3client/z;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/z;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 552
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    const v1, 0x7f0c015e

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 553
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    iget v1, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v1, v1

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    invoke-static {v5, v1, v2}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 554
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    new-instance v1, Lcom/teamspeak/ts3client/aa;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/aa;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 569
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->ay:Z

    if-eqz v0, :cond_9a

    .line 570
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/ac;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ac;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 578
    :cond_9a
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    const v1, 0x7f0c015f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 579
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    iget v1, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v1, v1

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    invoke-static {v6, v1, v2}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 580
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    new-instance v1, Lcom/teamspeak/ts3client/ad;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ad;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 593
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    const v1, 0x7f0c0161

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 594
    sput-object v0, Lcom/teamspeak/ts3client/t;->i:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f02008c

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 595
    sget-object v0, Lcom/teamspeak/ts3client/t;->i:Lcom/teamspeak/ts3client/customs/FloatingButton;

    new-instance v1, Lcom/teamspeak/ts3client/af;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/af;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 623
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    iget v1, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v1, v1

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    invoke-static {v6, v1, v2}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 624
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    iget v1, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v1, v1

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    invoke-static {v5, v1, v2}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 625
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->b:Z

    if-eqz v0, :cond_11f

    .line 626
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020084

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 630
    :cond_11f
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->c:Z

    if-eqz v0, :cond_135

    .line 631
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020098

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 634
    :cond_135
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50134
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 634
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->az:Z

    .line 635
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->az:Z

    if-nez v0, :cond_148

    .line 636
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->C()V

    .line 639
    :cond_148
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    const v1, 0x7f0c015c

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/customExpandableListView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    .line 640
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    new-instance v1, Lcom/teamspeak/ts3client/ag;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ag;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/customExpandableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 656
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->l:Lcom/teamspeak/ts3client/data/g;

    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    .line 50135
    iput-object v1, v0, Lcom/teamspeak/ts3client/data/g;->b:Lcom/teamspeak/ts3client/data/customExpandableListView;

    .line 657
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50137
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 657
    const-string v1, "trenn_linie"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_178

    .line 658
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/customExpandableListView;->setDividerHeight(I)V

    .line 659
    :cond_178
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/customExpandableListView;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 660
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->l:Lcom/teamspeak/ts3client/data/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/customExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 661
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/customExpandableListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 662
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50138
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50139
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 662
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    goto/16 :goto_b
.end method

.method public final C()V
    .registers 3

    .prologue
    .line 675
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->ax:Z

    if-eqz v0, :cond_5

    .line 683
    :goto_4
    return-void

    .line 677
    :cond_5
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/aj;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/aj;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_4
.end method

.method public final D()V
    .registers 3

    .prologue
    .line 708
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/al;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/al;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 714
    return-void
.end method

.method public final E()V
    .registers 4

    .prologue
    .line 887
    :try_start_0
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/aq;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/aq;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_c} :catch_d

    .line 896
    :goto_c
    return-void

    .line 894
    :catch_d
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50171
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 894
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "Error: Dialog Exception"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_c
.end method

.method public final F()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1022
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->b:Z

    if-nez v0, :cond_3d

    .line 1023
    iput-boolean v5, p0, Lcom/teamspeak/ts3client/t;->b:Z

    .line 1024
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020084

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1025
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50189
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50190
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1025
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50191
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50192
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1025
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1026
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50193
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50194
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1026
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50195
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50196
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1026
    const-string v1, "InputMute"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 1033
    :goto_3c
    return-void

    .line 1029
    :cond_3d
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/t;->b:Z

    .line 1030
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020081

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1031
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50197
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50198
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1031
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50199
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50200
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1031
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1032
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50201
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50202
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1032
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50203
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50204
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1032
    const-string v1, "InputUnMute"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    goto :goto_3c
.end method

.method public final G()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1054
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->c:Z

    if-nez v0, :cond_5c

    .line 1055
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/t;->c:Z

    .line 1056
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020098

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1057
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50217
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50218
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1057
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50219
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50220
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1057
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1058
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50221
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50222
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1058
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50223
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50224
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1058
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 1059
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aT:Lcom/teamspeak/ts3client/jni/h;

    invoke-virtual {v0, v1, v6}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1060
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    .line 50225
    iput-boolean v4, v0, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 1061
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50227
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50228
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50229
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 1061
    invoke-virtual {v0, v4}, Ljava/util/BitSet;->set(I)V

    .line 1071
    :goto_5b
    return-void

    .line 1064
    :cond_5c
    iput-boolean v5, p0, Lcom/teamspeak/ts3client/t;->c:Z

    .line 1065
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v1, 0x7f020096

    iget v2, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v2, v2

    iget v3, p0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1066
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50230
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50231
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1066
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50232
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50233
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1066
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1067
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50234
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50235
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1067
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50236
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50237
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1067
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 1068
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    .line 50238
    iput-boolean v5, v0, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 1069
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aU:Lcom/teamspeak/ts3client/jni/h;

    invoke-virtual {v0, v1, v6}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1070
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50240
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50241
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50242
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 1070
    invoke-virtual {v0, v4}, Ljava/util/BitSet;->clear(I)V

    goto :goto_5b
.end method

.method public final H()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 1115
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->aA:Z

    .line 1116
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->ax:Z

    .line 1118
    return-void
.end method

.method public final I()V
    .registers 2

    .prologue
    .line 1121
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->B()V

    .line 1122
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50271
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 1123
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12

    .prologue
    const v6, 0x7f030030

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 730
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aE:Lcom/teamspeak/ts3client/d/c/d;

    if-nez v0, :cond_10

    .line 731
    new-instance v0, Lcom/teamspeak/ts3client/d/c/d;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/d/c/d;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->aE:Lcom/teamspeak/ts3client/d/c/d;

    .line 732
    :cond_10
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->aw:Z

    if-eqz v0, :cond_19

    .line 733
    invoke-virtual {p1, v6, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 767
    :goto_18
    return-object v0

    .line 734
    :cond_19
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50140
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 734
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50141
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50142
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50143
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 734
    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 735
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50144
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 735
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50145
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 735
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 737
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->n()V

    .line 739
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 740
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 741
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x400000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 742
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 743
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50146
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 744
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50148
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 744
    new-instance v1, Lcom/teamspeak/ts3client/data/g;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    .line 50149
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 744
    invoke-direct {v1, v2, v3}, Lcom/teamspeak/ts3client/data/g;-><init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V

    .line 50150
    iput-object v1, v0, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 745
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50152
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50153
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 745
    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->l:Lcom/teamspeak/ts3client/data/g;

    .line 746
    invoke-virtual {p1, v6, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 747
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    if-nez v0, :cond_115

    .line 748
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50154
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50155
    iget v0, v0, Lcom/teamspeak/ts3client/data/e;->n:I

    .line 748
    if-eqz v0, :cond_ec

    .line 749
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50156
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50157
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 749
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    const v3, 0x7f050028

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bb;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50158
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50159
    iget v2, v2, Lcom/teamspeak/ts3client/data/e;->n:I

    .line 749
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    const v3, 0x7f050027

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bb;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v6

    const v8, 0x7f05001e

    invoke-virtual {v6, v8}, Landroid/support/v4/app/bb;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    move-object v0, v7

    .line 750
    goto/16 :goto_18

    .line 752
    :cond_ec
    new-instance v0, Lcom/teamspeak/ts3client/d/h;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    const-string v2, "connectiondialog.step0"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "connectiondialog.info"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "button.cancel"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/d/h;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->k:Lcom/teamspeak/ts3client/d/h;

    .line 753
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/am;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/am;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 763
    :cond_115
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->m:Landroid/telephony/TelephonyManager;

    .line 764
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->m:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->aH:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 765
    new-instance v0, Lcom/teamspeak/ts3client/bf;

    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1}, Lcom/teamspeak/ts3client/bf;-><init>(Lcom/teamspeak/ts3client/Ts3Application;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->aB:Lcom/teamspeak/ts3client/bf;

    .line 766
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50160
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 766
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->aB:Lcom/teamspeak/ts3client/bf;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    move-object v0, v7

    .line 767
    goto/16 :goto_18
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 240
    if-eqz v0, :cond_16

    .line 241
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 241
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/ay;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ay;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 253
    :cond_16
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 703
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 704
    new-instance v0, Lcom/teamspeak/ts3client/chat/c;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/chat/c;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/t;->av:Lcom/teamspeak/ts3client/chat/c;

    .line 705
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 5

    .prologue
    .line 718
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 719
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aE:Lcom/teamspeak/ts3client/d/c/d;

    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0, p1, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;Lcom/teamspeak/ts3client/Ts3Application;)V

    .line 720
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 721
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/data/a;)V
    .registers 3

    .prologue
    .line 931
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/teamspeak/ts3client/t;->a(Lcom/teamspeak/ts3client/data/a;I)V

    .line 932
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/data/a;I)V
    .registers 5

    .prologue
    .line 935
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/at;

    invoke-direct {v1, p0, p1, p2}, Lcom/teamspeak/ts3client/at;-><init>(Lcom/teamspeak/ts3client/t;Lcom/teamspeak/ts3client/data/a;I)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 945
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/data/c;)V
    .registers 6

    .prologue
    .line 975
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->l()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 976
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50186
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 976
    instance-of v0, v0, Lcom/teamspeak/ts3client/h;

    if-eqz v0, :cond_f

    .line 986
    :cond_e
    :goto_e
    return-void

    .line 978
    :cond_f
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50187
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 978
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Clientinfo: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 979
    invoke-static {p1}, Lcom/teamspeak/ts3client/h;->a(Lcom/teamspeak/ts3client/data/c;)Lcom/teamspeak/ts3client/h;

    move-result-object v0

    .line 50188
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 980
    invoke-virtual {v1}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v1

    .line 981
    const-string v2, "ClientInfo"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 982
    invoke-virtual {v1}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 983
    const/16 v0, 0x1001

    invoke-virtual {v1, v0}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 984
    invoke-virtual {v1}, Landroid/support/v4/app/cd;->i()I

    goto :goto_e
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 5

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->l()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 469
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50132
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 469
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/w;

    invoke-direct {v1, p0, p2, p1}, Lcom/teamspeak/ts3client/w;-><init>(Lcom/teamspeak/ts3client/t;Ljava/lang/Boolean;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 481
    :cond_16
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
    .registers 16

    .prologue
    .line 900
    :try_start_0
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v8

    new-instance v0, Lcom/teamspeak/ts3client/ar;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p6

    move-object v5, p5

    move-object v6, p4

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/teamspeak/ts3client/ar;-><init>(Lcom/teamspeak/ts3client/t;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    invoke-virtual {v8, v0}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_13} :catch_14

    .line 928
    :goto_13
    return-void

    .line 926
    :catch_14
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50172
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 926
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "Error: Dialog Exception"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_13
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V
    .registers 14

    .prologue
    .line 881
    invoke-virtual/range {p0 .. p6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 882
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/a/o;

    const-string v2, ""

    const/4 v3, 0x0

    const-string v4, ""

    iget-object v5, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50170
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 882
    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p7, v1}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 883
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10

    .prologue
    const/4 v3, 0x0

    .line 948
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    const-class v2, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 949
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 950
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 951
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 953
    new-instance v1, Landroid/support/v4/app/dm;

    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/dm;-><init>(Landroid/content/Context;)V

    .line 954
    const v2, 0x7f020076

    invoke-virtual {v1, v2}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/support/v4/app/dm;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/app/dm;->a(J)Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/dm;->a()Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/support/v4/app/dm;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/support/v4/app/dm;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    .line 50173
    iput-object v0, v2, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    .line 955
    invoke-virtual {v1}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    .line 957
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50175
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 957
    const/4 v2, 0x0

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 958
    return-void
.end method

.method public final a(Z)V
    .registers 4

    .prologue
    .line 303
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 303
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/bb;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/bb;-><init>(Lcom/teamspeak/ts3client/t;Z)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 334
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 725
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aE:Lcom/teamspeak/ts3client/d/c/d;

    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0, p1, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/MenuItem;Lcom/teamspeak/ts3client/Ts3Application;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .registers 6

    .prologue
    .line 256
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 256
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 27061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 256
    sget-object v1, Lcom/teamspeak/ts3client/jni/i;->x:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/i;)I

    move-result v0

    .line 257
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 257
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 29061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 29267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 257
    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->w:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v1

    .line 258
    packed-switch v0, :pswitch_data_5e

    .line 272
    :cond_27
    :goto_27
    const/4 v0, 0x0

    :goto_28
    return v0

    .line 260
    :pswitch_29
    invoke-static {v1}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto :goto_27

    .line 263
    :pswitch_2d
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 264
    invoke-static {v1}, Lcom/teamspeak/ts3client/data/d/a;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    const-string v1, "messages.hostmessage"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "button.close"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/teamspeak/ts3client/t;->a(Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_27

    .line 267
    :pswitch_49
    invoke-static {v1}, Lcom/teamspeak/ts3client/data/d/a;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    const-string v1, "messages.hostmessage"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "button.exit"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/teamspeak/ts3client/t;->a(Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v0, 0x1

    goto :goto_28

    .line 258
    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_29
        :pswitch_2d
        :pswitch_49
    .end packed-switch
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 517
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 518
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->B()V

    .line 519
    return-void
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 420
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50131
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 420
    if-eqz v0, :cond_18

    .line 421
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->l()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 422
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/bd;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/bd;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 434
    :cond_18
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 845
    return-void
.end method

.method public final d(Z)V
    .registers 15

    .prologue
    const/16 v4, 0x2002

    const/16 v12, 0x65

    const/4 v11, 0x1

    const/4 v1, 0x0

    const/4 v10, 0x0

    .line 341
    iput-boolean v11, p0, Lcom/teamspeak/ts3client/t;->aF:Z

    .line 343
    :try_start_9
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->aD:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1c

    .line 344
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->aD:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bb;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_1c} :catch_2d0

    .line 348
    :cond_1c
    :goto_1c
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->k:Lcom/teamspeak/ts3client/d/h;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/h;->l()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 349
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->k:Lcom/teamspeak/ts3client/d/h;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/h;->b()V

    .line 350
    :cond_29
    iput-boolean v11, p0, Lcom/teamspeak/ts3client/t;->aw:Z

    .line 352
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30139
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->g:Z

    .line 352
    if-eqz v0, :cond_2de

    .line 30688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 353
    invoke-virtual {v0}, Landroid/support/v4/app/bi;->f()I

    move-result v0

    if-lez v0, :cond_5b

    .line 31688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 354
    invoke-virtual {v0}, Landroid/support/v4/app/bi;->g()Landroid/support/v4/app/bj;

    move-result-object v0

    invoke-interface {v0}, Landroid/support/v4/app/bj;->a()I

    move-result v0

    .line 32688
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 355
    invoke-virtual {v2, v0}, Landroid/support/v4/app/bi;->b(I)V

    .line 33688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 356
    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 357
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34077
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 357
    invoke-virtual {v0, v2}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 358
    invoke-virtual {v0, v4}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 359
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 34688
    :cond_5b
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 361
    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 363
    :try_start_61
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 35061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 35206
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 363
    invoke-virtual {v0, v2}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    :try_end_6a
    .catch Ljava/lang/Exception; {:try_start_61 .. :try_end_6a} :catch_363

    .line 35688
    :goto_6a
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 367
    const-string v3, "Bookmark"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/cd;->f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 368
    invoke-virtual {v0, v4}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 369
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 374
    :cond_7b
    :goto_7b
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v2, 0x8000

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 375
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x80000

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 376
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x400000

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 377
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v2, 0x80

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 378
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->m:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_bb

    .line 379
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->m:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->aH:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v2, v10}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 380
    :cond_bb
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 37093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 380
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->aB:Lcom/teamspeak/ts3client/bf;

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 381
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 37196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 381
    if-eqz v0, :cond_d1

    .line 382
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 38196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 382
    invoke-virtual {v0, v11}, Landroid/app/NotificationManager;->cancel(I)V

    .line 384
    :cond_d1
    :try_start_d1
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 39061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 39348
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 40085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 39348
    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v4, "Stop AUDIO"

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 39349
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/a/k;->f()V

    .line 39351
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->e()V

    .line 385
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 41061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 385
    const-string v2, "....TS3 Android...."

    .line 41140
    invoke-static {}, Lcom/teamspeak/ts3client/data/d/q;->a()V

    .line 41141
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v3

    .line 42092
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/teamspeak/ts3client/data/v;->b:Z

    .line 41142
    iget-object v3, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 42107
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 42299
    iget-object v3, v3, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    .line 43086
    iget-object v3, v3, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    .line 41142
    iget-object v4, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 44063
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/ab;->l:J

    .line 41142
    iget-object v6, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    invoke-virtual {v3, v4, v5, v6}, Lcom/teamspeak/ts3client/data/b/a;->a(JLcom/teamspeak/ts3client/data/ab;)Z

    .line 41143
    iget-object v3, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 44085
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 41143
    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v5, "Disconnecting from Server"

    invoke-virtual {v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 41144
    iget-object v3, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v3, v4, v5, v2}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_stopConnection(JLjava/lang/String;)I

    .line 41145
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v2, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 41146
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v2, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 41147
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v2, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    .line 41148
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v3, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 44188
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->k:Landroid/content/Intent;

    .line 41148
    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/Ts3Application;->stopService(Landroid/content/Intent;)Z

    .line 44306
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/a/k;->b()V

    .line 44307
    new-instance v2, Lcom/teamspeak/ts3client/data/f;

    invoke-direct {v2, v0}, Lcom/teamspeak/ts3client/data/f;-><init>(Lcom/teamspeak/ts3client/data/e;)V

    .line 44313
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 44314
    const-wide/16 v4, 0xbb8

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 41150
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_unregisterCustomDevice(Ljava/lang/String;)I

    .line 41151
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 41152
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v2

    .line 45080
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    .line 45081
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    .line 45082
    iget-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 45083
    iget-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 45084
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    .line 45085
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    .line 41153
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 46085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 41153
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "ClientLib closed"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 47085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 386
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "Connect status: STATUS_DISCONNECTED"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_197
    .catch Ljava/lang/Exception; {:try_start_d1 .. :try_end_197} :catch_360

    .line 48042
    :goto_197
    sget-object v0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 48071
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, v0, Lcom/teamspeak/ts3client/data/b/c;->b:Ljava/util/Vector;

    .line 392
    :try_start_1a0
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 49061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49198
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 49562
    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50062
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 49562
    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 49563
    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50063
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 49563
    iget-object v0, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->f:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    :try_end_1b8
    .catch Ljava/lang/Exception; {:try_start_1a0 .. :try_end_1b8} :catch_35d

    .line 396
    :goto_1b8
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50064
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->b:Landroid/os/PowerManager$WakeLock;

    .line 396
    if-eqz v0, :cond_1cf

    .line 397
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50065
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->b:Landroid/os/PowerManager$WakeLock;

    .line 397
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1cf

    .line 398
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50066
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->b:Landroid/os/PowerManager$WakeLock;

    .line 398
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 399
    :cond_1cf
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50067
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 399
    if-eqz v0, :cond_1e6

    .line 400
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50068
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 400
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1e6

    .line 401
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50069
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 401
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 402
    :cond_1e6
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50070
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->n:Landroid/net/wifi/WifiManager$WifiLock;

    .line 402
    if-eqz v0, :cond_1fd

    .line 403
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50071
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->n:Landroid/net/wifi/WifiManager$WifiLock;

    .line 403
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1fd

    .line 404
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50072
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->n:Landroid/net/wifi/WifiManager$WifiLock;

    .line 404
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 406
    :cond_1fd
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50073
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 406
    if-eqz v0, :cond_366

    .line 407
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50074
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50075
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 408
    :goto_209
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50076
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 408
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/StartGUIFragment;->j()V

    .line 409
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50077
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 409
    if-eqz v2, :cond_21c

    .line 410
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50078
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50079
    iput-object v1, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 411
    :cond_21c
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50081
    iput-object v1, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 412
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50083
    iput-object p0, v2, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 413
    if-eqz p1, :cond_2c6

    if-eqz v0, :cond_2c6

    .line 414
    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 50116
    iget v3, v0, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 50086
    iget-object v4, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50117
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 50086
    const-string v5, "reconnect.limit"

    const-string v6, "15"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-le v3, v4, :cond_2e8

    .line 50087
    new-instance v0, Landroid/content/Intent;

    iget-object v3, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-class v4, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50088
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 50089
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 50090
    iget-object v3, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-static {v3, v10, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 50092
    new-instance v3, Landroid/support/v4/app/dm;

    iget-object v4, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/v4/app/dm;-><init>(Landroid/content/Context;)V

    .line 50093
    const v4, 0x7f020076

    invoke-virtual {v3, v4}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    move-result-object v4

    const-string v5, "connection.reconnect"

    invoke-static {v5}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/dm;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/support/v4/app/dm;->a(J)Landroid/support/v4/app/dm;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/dm;->a()Landroid/support/v4/app/dm;

    move-result-object v4

    const-string v5, "connection.reconnect"

    invoke-static {v5}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/dm;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v4

    const-string v5, "connection.reconnect.text"

    new-array v6, v11, [Ljava/lang/Object;

    iget-object v7, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50118
    iget-object v7, v7, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 50093
    const-string v8, "reconnect.limit"

    const-string v9, "15"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/dm;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v4

    .line 50119
    iput-object v0, v4, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    .line 50121
    invoke-virtual {v3}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    .line 50095
    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x14

    iput v3, v0, Landroid/app/Notification;->flags:I

    .line 50097
    iget-object v3, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50122
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 50097
    invoke-virtual {v3, v1, v12, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 50098
    iput-boolean v10, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->p:Z

    .line 50099
    iput-boolean v11, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->s:Z

    .line 416
    :cond_2c6
    :goto_2c6
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50130
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 416
    const-string v1, "TeamSpeak"

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 417
    return-void

    .line 346
    :catch_2d0
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 346
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "java.lang.IllegalArgumentException: Receiver not registered"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_1c

    .line 371
    :cond_2de
    if-nez p1, :cond_7b

    .line 372
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 36107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 36899
    iput-boolean v11, v0, Lcom/teamspeak/ts3client/StartGUIFragment;->r:Z

    goto/16 :goto_7b

    .line 50102
    :cond_2e8
    new-instance v3, Landroid/content/Intent;

    iget-object v4, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-class v5, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50103
    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 50104
    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 50105
    iget-object v4, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-static {v4, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 50107
    new-instance v4, Landroid/support/v4/app/dm;

    iget-object v5, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/support/v4/app/dm;-><init>(Landroid/content/Context;)V

    .line 50108
    const v5, 0x7f020076

    invoke-virtual {v4, v5}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    move-result-object v5

    const-string v6, "connection.reconnect.retry"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/dm;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/dm;->a(J)Landroid/support/v4/app/dm;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/dm;->a()Landroid/support/v4/app/dm;

    move-result-object v5

    const-string v6, "connection.reconnect.retry"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/dm;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v5

    const-string v6, "connection.reconnect.retry"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/dm;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v5

    .line 50123
    iput-object v3, v5, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    .line 50125
    invoke-virtual {v4}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v3

    .line 50110
    iget v4, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v4, v4, 0x14

    iput v4, v3, Landroid/app/Notification;->flags:I

    .line 50112
    iget-object v4, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50126
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 50112
    invoke-virtual {v4, v1, v12, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 50127
    iget v1, v0, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 50113
    add-int/lit8 v1, v1, 0x1

    .line 50128
    iput v1, v0, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 50114
    iget-object v1, v2, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/bookmark/e;->a(Lcom/teamspeak/ts3client/data/ab;)V

    goto/16 :goto_2c6

    :catch_35d
    move-exception v0

    goto/16 :goto_1b8

    :catch_360
    move-exception v0

    goto/16 :goto_197

    :catch_363
    move-exception v2

    goto/16 :goto_6a

    :cond_366
    move-object v0, v1

    goto/16 :goto_209
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 773
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->e()V

    .line 775
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 776
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 777
    const-string v1, "android.intent.action.NEW_OUTGOING_CALL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 778
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/t;->aD:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/bb;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 779
    return-void
.end method

.method public final e(Z)V
    .registers 4

    .prologue
    .line 686
    if-eqz p1, :cond_12

    .line 687
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/ak;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ak;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 695
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->ay:Z

    .line 699
    :goto_11
    return-void

    .line 697
    :cond_12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->ay:Z

    goto :goto_11
.end method

.method public final f(Z)V
    .registers 4

    .prologue
    .line 989
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/au;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/au;-><init>(Lcom/teamspeak/ts3client/t;Z)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1019
    return-void
.end method

.method public final g(Z)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1038
    if-eqz p1, :cond_21

    .line 1039
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50205
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1039
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50207
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50208
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1039
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1040
    iput-boolean v5, p0, Lcom/teamspeak/ts3client/t;->au:Z

    .line 1041
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50209
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50210
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 1041
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->d()V

    .line 1051
    :goto_20
    return-void

    .line 1043
    :cond_21
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->aA:Z

    if-eqz v0, :cond_28

    .line 1044
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/t;->au:Z

    goto :goto_20

    .line 1047
    :cond_28
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50211
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50212
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1047
    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50213
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50214
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1047
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1048
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/t;->au:Z

    .line 1049
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50215
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50216
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 1049
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->f()V

    goto :goto_20
.end method

.method public final h(Z)V
    .registers 4

    .prologue
    .line 1090
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/ax;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/ax;-><init>(Lcom/teamspeak/ts3client/t;Z)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1102
    return-void
.end method

.method public final i(Z)V
    .registers 3

    .prologue
    .line 1109
    if-nez p1, :cond_9

    .line 50270
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->au:Z

    .line 1109
    if-eqz v0, :cond_9

    .line 1110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/t;->aA:Z

    .line 1111
    :cond_9
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/t;->ax:Z

    .line 1112
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2

    .prologue
    .line 506
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 507
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->A()V

    .line 508
    return-void
.end method

.method public final s()V
    .registers 3

    .prologue
    .line 800
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->s()V

    .line 822
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->j:Lcom/teamspeak/ts3client/data/customExpandableListView;

    if-eqz v0, :cond_18

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->aw:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_18

    .line 823
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/an;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/an;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 836
    :cond_18
    return-void
.end method

.method public final t()V
    .registers 1

    .prologue
    .line 794
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->t()V

    .line 795
    return-void
.end method

.method public final u()V
    .registers 3

    .prologue
    .line 783
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->u()V

    .line 784
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/t;->aF:Z

    if-nez v0, :cond_1d

    .line 785
    iget-object v0, p0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50161
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50162
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 785
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->d()V

    .line 786
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/t;->aD:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 50163
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/t;->d(Z)V

    .line 790
    :cond_1d
    return-void
.end method

.method public final y()V
    .registers 3

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 438
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/be;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/be;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 450
    :cond_12
    return-void
.end method

.method public final z()V
    .registers 3

    .prologue
    .line 487
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 488
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/x;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/x;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 497
    :cond_12
    return-void
.end method
