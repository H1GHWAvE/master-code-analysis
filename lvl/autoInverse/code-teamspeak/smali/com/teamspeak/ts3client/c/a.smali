.class public final Lcom/teamspeak/ts3client/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:I

.field public e:I

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:F


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 8
    iput v1, p0, Lcom/teamspeak/ts3client/c/a;->b:I

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/a;->c:Ljava/lang/String;

    .line 10
    const/4 v0, 0x2

    iput v0, p0, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 11
    iput v1, p0, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 12
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 13
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 14
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 15
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 16
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 17
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 18
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/c/a;->m:F

    return-void
.end method

.method private a(F)V
    .registers 2

    .prologue
    .line 122
    iput p1, p0, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 123
    return-void
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 42
    iput p1, p0, Lcom/teamspeak/ts3client/c/a;->b:I

    .line 43
    return-void
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 67
    return-void
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 50
    iput p1, p0, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 51
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 34
    iput-object p1, p0, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 35
    return-void
.end method

.method private b(Z)V
    .registers 2

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 75
    return-void
.end method

.method private c(I)V
    .registers 2

    .prologue
    .line 58
    iput p1, p0, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 59
    return-void
.end method

.method private c(Z)V
    .registers 2

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 83
    return-void
.end method

.method private d(Z)V
    .registers 2

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 91
    return-void
.end method

.method private e(Z)V
    .registers 2

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 99
    return-void
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method private f(Z)V
    .registers 2

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 107
    return-void
.end method

.method private g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method private g(Z)V
    .registers 2

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 115
    return-void
.end method

.method private h()I
    .registers 2

    .prologue
    .line 38
    iget v0, p0, Lcom/teamspeak/ts3client/c/a;->b:I

    return v0
.end method

.method private i()I
    .registers 2

    .prologue
    .line 46
    iget v0, p0, Lcom/teamspeak/ts3client/c/a;->d:I

    return v0
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/c/a;->k:Z

    return v0
.end method

.method private k()Z
    .registers 2

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/c/a;->j:Z

    return v0
.end method

.method private l()Z
    .registers 2

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/c/a;->f:Z

    return v0
.end method

.method private m()F
    .registers 2

    .prologue
    .line 118
    iget v0, p0, Lcom/teamspeak/ts3client/c/a;->m:F

    return v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 54
    iget v0, p0, Lcom/teamspeak/ts3client/c/a;->e:I

    return v0
.end method

.method public final a(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 26
    const-string v0, "\'"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/a;->c:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/c/a;->i:Z

    return v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/c/a;->h:Z

    return v0
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/c/a;->g:Z

    return v0
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/c/a;->l:Z

    return v0
.end method
