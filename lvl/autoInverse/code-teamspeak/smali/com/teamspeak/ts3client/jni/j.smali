.class public final enum Lcom/teamspeak/ts3client/jni/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/teamspeak/ts3client/jni/j;

.field public static final enum b:Lcom/teamspeak/ts3client/jni/j;

.field public static final enum c:Lcom/teamspeak/ts3client/jni/j;

.field private static final synthetic e:[Lcom/teamspeak/ts3client/jni/j;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 563
    new-instance v0, Lcom/teamspeak/ts3client/jni/j;

    const-string v1, "ENTER_VISIBILITY"

    invoke-direct {v0, v1, v2, v2}, Lcom/teamspeak/ts3client/jni/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    new-instance v0, Lcom/teamspeak/ts3client/jni/j;

    const-string v1, "RETAIN_VISIBILITY"

    invoke-direct {v0, v1, v3, v3}, Lcom/teamspeak/ts3client/jni/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    new-instance v0, Lcom/teamspeak/ts3client/jni/j;

    const-string v1, "LEAVE_VISIBILITY"

    invoke-direct {v0, v1, v4, v4}, Lcom/teamspeak/ts3client/jni/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    .line 562
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/teamspeak/ts3client/jni/j;

    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    aput-object v1, v0, v4

    sput-object v0, Lcom/teamspeak/ts3client/jni/j;->e:[Lcom/teamspeak/ts3client/jni/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4

    .prologue
    .line 567
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 568
    iput p3, p0, Lcom/teamspeak/ts3client/jni/j;->d:I

    .line 569
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 572
    iget v0, p0, Lcom/teamspeak/ts3client/jni/j;->d:I

    return v0
.end method

.method private static a(I)I
    .registers 1

    .prologue
    .line 576
    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/teamspeak/ts3client/jni/j;
    .registers 2

    .prologue
    .line 562
    const-class v0, Lcom/teamspeak/ts3client/jni/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/jni/j;

    return-object v0
.end method

.method public static values()[Lcom/teamspeak/ts3client/jni/j;
    .registers 1

    .prologue
    .line 562
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->e:[Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {v0}, [Lcom/teamspeak/ts3client/jni/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/teamspeak/ts3client/jni/j;

    return-object v0
.end method
