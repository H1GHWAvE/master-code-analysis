.class public final enum Lcom/teamspeak/ts3client/jni/b;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Lcom/teamspeak/ts3client/jni/b;

.field private static final synthetic C:[Lcom/teamspeak/ts3client/jni/b;

.field public static final enum a:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum b:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum c:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum d:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum e:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum f:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum g:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum h:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum i:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum j:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum k:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum l:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum m:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum n:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum o:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum p:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum q:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum r:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum s:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum t:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum u:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum v:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum w:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum x:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum y:Lcom/teamspeak/ts3client/jni/b;

.field public static final enum z:Lcom/teamspeak/ts3client/jni/b;


# instance fields
.field public B:I


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/16 v9, 0x10

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 582
    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_B"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v5}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->a:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_I"

    invoke-direct {v0, v1, v5, v6}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->b:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_U"

    invoke-direct {v0, v1, v6, v7}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->c:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_S"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v8}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->d:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_SUP"

    invoke-direct {v0, v1, v7, v9}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->e:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_SUB"

    const/4 v2, 0x5

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->f:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_COLOR"

    const/4 v2, 0x6

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->g:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_SIZE"

    const/4 v2, 0x7

    const/16 v3, 0x80

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->h:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_group_text"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v8, v2}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->i:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_LEFT"

    const/16 v2, 0x9

    const/16 v3, 0x1000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->j:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_RIGHT"

    const/16 v2, 0xa

    const/16 v3, 0x2000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->k:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_CENTER"

    const/16 v2, 0xb

    const/16 v3, 0x4000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->l:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_group_align"

    const/16 v2, 0xc

    const/16 v3, 0x7000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->m:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_URL"

    const/16 v2, 0xd

    const/high16 v3, 0x10000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->n:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_IMAGE"

    const/16 v2, 0xe

    const/high16 v3, 0x20000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->o:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_HR"

    const/16 v2, 0xf

    const/high16 v3, 0x40000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->p:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_LIST"

    const/high16 v2, 0x100000

    invoke-direct {v0, v1, v9, v2}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->q:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_LISTITEM"

    const/16 v2, 0x11

    const/high16 v3, 0x200000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->r:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_group_list"

    const/16 v2, 0x12

    const/high16 v3, 0x300000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->s:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_TABLE"

    const/16 v2, 0x13

    const/high16 v3, 0x400000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->t:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_TR"

    const/16 v2, 0x14

    const/high16 v3, 0x800000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->u:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_TH"

    const/16 v2, 0x15

    const/high16 v3, 0x1000000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->v:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_TD"

    const/16 v2, 0x16

    const/high16 v3, 0x2000000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->w:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_group_table"

    const/16 v2, 0x17

    const/high16 v3, 0x3c00000

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->x:Lcom/teamspeak/ts3client/jni/b;

    .line 584
    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_def_simple"

    const/16 v2, 0x18

    sget-object v3, Lcom/teamspeak/ts3client/jni/b;->a:Lcom/teamspeak/ts3client/jni/b;

    .line 1593
    iget v3, v3, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->b:Lcom/teamspeak/ts3client/jni/b;

    .line 2593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->c:Lcom/teamspeak/ts3client/jni/b;

    .line 3593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->d:Lcom/teamspeak/ts3client/jni/b;

    .line 4593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->e:Lcom/teamspeak/ts3client/jni/b;

    .line 5593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->f:Lcom/teamspeak/ts3client/jni/b;

    .line 6593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->g:Lcom/teamspeak/ts3client/jni/b;

    .line 7593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->n:Lcom/teamspeak/ts3client/jni/b;

    .line 8593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->y:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_def_simple_Img"

    const/16 v2, 0x19

    sget-object v3, Lcom/teamspeak/ts3client/jni/b;->y:Lcom/teamspeak/ts3client/jni/b;

    .line 9593
    iget v3, v3, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->o:Lcom/teamspeak/ts3client/jni/b;

    .line 10593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->z:Lcom/teamspeak/ts3client/jni/b;

    new-instance v0, Lcom/teamspeak/ts3client/jni/b;

    const-string v1, "BBCodeTag_def_extended"

    const/16 v2, 0x1a

    sget-object v3, Lcom/teamspeak/ts3client/jni/b;->i:Lcom/teamspeak/ts3client/jni/b;

    .line 11593
    iget v3, v3, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->m:Lcom/teamspeak/ts3client/jni/b;

    .line 12593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->n:Lcom/teamspeak/ts3client/jni/b;

    .line 13593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->o:Lcom/teamspeak/ts3client/jni/b;

    .line 14593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->p:Lcom/teamspeak/ts3client/jni/b;

    .line 15593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->s:Lcom/teamspeak/ts3client/jni/b;

    .line 16593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    sget-object v4, Lcom/teamspeak/ts3client/jni/b;->x:Lcom/teamspeak/ts3client/jni/b;

    .line 17593
    iget v4, v4, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 584
    or-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->A:Lcom/teamspeak/ts3client/jni/b;

    .line 581
    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/teamspeak/ts3client/jni/b;

    const/4 v1, 0x0

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->a:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    sget-object v1, Lcom/teamspeak/ts3client/jni/b;->b:Lcom/teamspeak/ts3client/jni/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/teamspeak/ts3client/jni/b;->c:Lcom/teamspeak/ts3client/jni/b;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->d:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    sget-object v1, Lcom/teamspeak/ts3client/jni/b;->e:Lcom/teamspeak/ts3client/jni/b;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->f:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->g:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->h:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    sget-object v1, Lcom/teamspeak/ts3client/jni/b;->i:Lcom/teamspeak/ts3client/jni/b;

    aput-object v1, v0, v8

    const/16 v1, 0x9

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->j:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->k:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->l:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->m:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->n:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->o:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->p:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    sget-object v1, Lcom/teamspeak/ts3client/jni/b;->q:Lcom/teamspeak/ts3client/jni/b;

    aput-object v1, v0, v9

    const/16 v1, 0x11

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->r:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->s:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->t:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->u:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->v:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->w:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->x:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->y:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->z:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->A:Lcom/teamspeak/ts3client/jni/b;

    aput-object v2, v0, v1

    sput-object v0, Lcom/teamspeak/ts3client/jni/b;->C:[Lcom/teamspeak/ts3client/jni/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4

    .prologue
    .line 588
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 589
    iput p3, p0, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 590
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 593
    iget v0, p0, Lcom/teamspeak/ts3client/jni/b;->B:I

    return v0
.end method

.method private static a(I)I
    .registers 1

    .prologue
    .line 597
    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/teamspeak/ts3client/jni/b;
    .registers 2

    .prologue
    .line 581
    const-class v0, Lcom/teamspeak/ts3client/jni/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/jni/b;

    return-object v0
.end method

.method public static values()[Lcom/teamspeak/ts3client/jni/b;
    .registers 1

    .prologue
    .line 581
    sget-object v0, Lcom/teamspeak/ts3client/jni/b;->C:[Lcom/teamspeak/ts3client/jni/b;

    invoke-virtual {v0}, [Lcom/teamspeak/ts3client/jni/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/teamspeak/ts3client/jni/b;

    return-object v0
.end method
