.class public Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:J

.field public h:J

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method private constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V
    .registers 15

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->a:J

    .line 26
    iput-object p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->b:Ljava/lang/String;

    .line 27
    iput-object p4, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->c:Ljava/lang/String;

    .line 28
    iput-object p5, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->d:Ljava/lang/String;

    .line 29
    iput-object p6, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->e:Ljava/lang/String;

    .line 30
    iput-wide p7, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->f:J

    .line 31
    iput-wide p9, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->g:J

    .line 32
    iput-wide p11, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->h:J

    .line 33
    iput-object p13, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->i:Ljava/lang/String;

    .line 34
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 35
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->d:Ljava/lang/String;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->e:Ljava/lang/String;

    return-object v0
.end method

.method private d()J
    .registers 3

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->a:J

    return-wide v0
.end method

.method private e()J
    .registers 3

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->h:J

    return-wide v0
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->i:Ljava/lang/String;

    return-object v0
.end method

.method private g()J
    .registers 3

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->g:J

    return-wide v0
.end method

.method private h()J
    .registers 3

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->f:J

    return-wide v0
.end method

.method private i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerTemporaryPasswordList [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientNickname="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uniqueClientIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", password="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timestampStart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timestampEnd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetChannelPW="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
