.class public Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:I

.field private c:J

.field private d:J

.field private e:Lcom/teamspeak/ts3client/jni/j;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:J

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method private constructor <init>(JIJJIILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .registers 21

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a:J

    .line 28
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->b:I

    .line 29
    iput-wide p4, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->c:J

    .line 30
    iput-wide p6, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->d:J

    .line 31
    if-nez p8, :cond_11

    .line 32
    sget-object v2, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 33
    :cond_11
    const/4 v2, 0x1

    if-ne p8, v2, :cond_18

    .line 34
    sget-object v2, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 35
    :cond_18
    const/4 v2, 0x2

    if-ne p8, v2, :cond_1f

    .line 36
    sget-object v2, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 37
    :cond_1f
    iput p9, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->f:I

    .line 38
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->g:Ljava/lang/String;

    .line 39
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->h:Ljava/lang/String;

    .line 40
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->i:J

    .line 41
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->j:Ljava/lang/String;

    .line 42
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 43
    return-void
.end method

.method private f()I
    .registers 2

    .prologue
    .line 50
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->f:I

    return v0
.end method

.method private g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->h:Ljava/lang/String;

    return-object v0
.end method

.method private h()J
    .registers 3

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->d:J

    return-wide v0
.end method

.method private i()J
    .registers 3

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a:J

    return-wide v0
.end method

.method private j()Lcom/teamspeak/ts3client/jni/j;
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->e:Lcom/teamspeak/ts3client/jni/j;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 46
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->b:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .registers 3

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->c:J

    return-wide v0
.end method

.method public final e()J
    .registers 3

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->i:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientBanFromServer [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->e:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kickerID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kickerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kickerUniqueIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kickMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
