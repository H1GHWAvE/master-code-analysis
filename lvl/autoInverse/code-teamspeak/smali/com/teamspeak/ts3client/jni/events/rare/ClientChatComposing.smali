.class public Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method private constructor <init>(JILjava/lang/String;)V
    .registers 6

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->a:J

    .line 18
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->b:I

    .line 19
    iput-object p4, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->c:Ljava/lang/String;

    .line 20
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 21
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 24
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->b:I

    return v0
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->c:Ljava/lang/String;

    return-object v0
.end method

.method private c()J
    .registers 3

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->a:J

    return-wide v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientChatComposing [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientUniqueIdentity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatComposing;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
