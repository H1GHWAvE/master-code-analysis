.class public Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method private constructor <init>(JILjava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;)V
    .registers 11

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->a:J

    .line 23
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->b:I

    .line 24
    iput-object p4, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->c:Ljava/lang/String;

    .line 25
    iput-object p5, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->d:Ljava/lang/String;

    .line 26
    iput-wide p6, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->e:J

    .line 27
    iput p8, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->f:I

    .line 28
    iput-object p9, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->g:Ljava/lang/String;

    .line 29
    iput-object p10, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->h:Ljava/lang/String;

    .line 30
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 31
    return-void
.end method

.method private e()J
    .registers 3

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->a:J

    return-wide v0
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->d:Ljava/lang/String;

    return-object v0
.end method

.method private g()I
    .registers 2

    .prologue
    .line 54
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->f:I

    return v0
.end method

.method private h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 38
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->b:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()J
    .registers 3

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->e:J

    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->g:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerGroupClientAdded [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientUniqueIdentity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serverGroupID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerClientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerUniqueIdentity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
