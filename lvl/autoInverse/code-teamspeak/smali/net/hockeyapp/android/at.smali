.class public Lnet/hockeyapp/android/at;
.super Landroid/app/DialogFragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lnet/hockeyapp/android/ax;


# instance fields
.field private downloadTask:Lnet/hockeyapp/android/d/l;

.field private urlString:Ljava/lang/String;

.field private versionHelper:Lnet/hockeyapp/android/e/y;

.field private versionInfo:Lorg/json/JSONArray;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lnet/hockeyapp/android/at;Landroid/app/Activity;)V
    .registers 2

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lnet/hockeyapp/android/at;->startDownloadTask(Landroid/app/Activity;)V

    return-void
.end method

.method public static newInstance(Lorg/json/JSONArray;Ljava/lang/String;)Lnet/hockeyapp/android/at;
    .registers 5

    .prologue
    .line 98
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 99
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v1, "versionInfo"

    invoke-virtual {p0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance v1, Lnet/hockeyapp/android/at;

    invoke-direct {v1}, Lnet/hockeyapp/android/at;-><init>()V

    .line 103
    invoke-virtual {v1, v0}, Lnet/hockeyapp/android/at;->setArguments(Landroid/os/Bundle;)V

    .line 104
    return-object v1
.end method

.method private startDownloadTask(Landroid/app/Activity;)V
    .registers 5

    .prologue
    .line 244
    new-instance v0, Lnet/hockeyapp/android/d/l;

    iget-object v1, p0, Lnet/hockeyapp/android/at;->urlString:Ljava/lang/String;

    new-instance v2, Lnet/hockeyapp/android/aw;

    invoke-direct {v2, p0, p1}, Lnet/hockeyapp/android/aw;-><init>(Lnet/hockeyapp/android/at;Landroid/app/Activity;)V

    invoke-direct {v0, p1, v1, v2}, Lnet/hockeyapp/android/d/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V

    iput-object v0, p0, Lnet/hockeyapp/android/at;->downloadTask:Lnet/hockeyapp/android/d/l;

    .line 265
    iget-object v0, p0, Lnet/hockeyapp/android/at;->downloadTask:Lnet/hockeyapp/android/d/l;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 266
    return-void
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .registers 4

    .prologue
    .line 293
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 296
    :try_start_4
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 297
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 298
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_18
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_18} :catch_1a

    move-result-object v0

    .line 301
    :goto_19
    return-object v0

    :catch_1a
    move-exception v0

    const-string v0, ""

    goto :goto_19
.end method

.method public getCurrentVersionCode()I
    .registers 5

    .prologue
    .line 274
    const/4 v0, -0x1

    .line 277
    :try_start_1
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_19
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_19} :catch_1c
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_19} :catch_1a

    .line 284
    :goto_19
    return v0

    :catch_1a
    move-exception v1

    goto :goto_19

    .line 282
    :catch_1c
    move-exception v1

    goto :goto_19
.end method

.method public getLayoutView()Landroid/view/View;
    .registers 5

    .prologue
    .line 311
    new-instance v0, Lnet/hockeyapp/android/f/m;

    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lnet/hockeyapp/android/f/m;-><init>(Landroid/content/Context;ZZ)V

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->prepareDownload()V

    .line 186
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 119
    :try_start_3
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/at;->urlString:Ljava/lang/String;

    .line 120
    new-instance v0, Lorg/json/JSONArray;

    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "versionInfo"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/hockeyapp/android/at;->versionInfo:Lorg/json/JSONArray;
    :try_end_20
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_20} :catch_28

    .line 127
    const/4 v0, 0x1

    const v1, 0x1030073

    invoke-virtual {p0, v0, v1}, Lnet/hockeyapp/android/at;->setStyle(II)V

    .line 128
    :goto_27
    return-void

    .line 123
    :catch_28
    move-exception v0

    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->dismiss()V

    goto :goto_27
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 15

    .prologue
    const/4 v10, 0x1

    .line 138
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getLayoutView()Landroid/view/View;

    move-result-object v6

    .line 140
    new-instance v0, Lnet/hockeyapp/android/e/y;

    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lnet/hockeyapp/android/at;->versionInfo:Lorg/json/JSONArray;

    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lnet/hockeyapp/android/e/y;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/ax;)V

    iput-object v0, p0, Lnet/hockeyapp/android/at;->versionHelper:Lnet/hockeyapp/android/e/y;

    .line 142
    const/16 v0, 0x1002

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 143
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    const/16 v0, 0x1003

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 146
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/hockeyapp/android/at;->versionHelper:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v2}, Lnet/hockeyapp/android/e/y;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 147
    iget-object v1, p0, Lnet/hockeyapp/android/at;->versionHelper:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v1}, Lnet/hockeyapp/android/e/y;->b()Ljava/lang/String;

    move-result-object v3

    .line 149
    const-string v1, "Unknown size"

    .line 150
    iget-object v4, p0, Lnet/hockeyapp/android/at;->versionHelper:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v4}, Lnet/hockeyapp/android/e/y;->c()J

    move-result-wide v4

    .line 151
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-ltz v7, :cond_ca

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%.2f"

    new-array v8, v10, [Ljava/lang/Object;

    const/4 v9, 0x0

    long-to-float v4, v4

    const/high16 v5, 0x49800000    # 1048576.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " MB"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 167
    :goto_7c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const/16 v0, 0x1004

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 170
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    const/16 v0, 0x1005

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 173
    invoke-virtual {v0, v10}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 174
    invoke-virtual {v0}, Landroid/webkit/WebView;->destroyDrawingCache()V

    .line 175
    const-string v1, "https://sdk.hockeyapp.net/"

    iget-object v2, p0, Lnet/hockeyapp/android/at;->versionHelper:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v2}, Lnet/hockeyapp/android/e/y;->d()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    return-object v6

    .line 155
    :cond_ca
    new-instance v4, Lnet/hockeyapp/android/d/o;

    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v7, p0, Lnet/hockeyapp/android/at;->urlString:Ljava/lang/String;

    new-instance v8, Lnet/hockeyapp/android/au;

    invoke-direct {v8, p0, v0, v2, v3}, Lnet/hockeyapp/android/au;-><init>(Lnet/hockeyapp/android/at;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, v5, v7, v8}, Lnet/hockeyapp/android/d/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V

    .line 165
    invoke-static {v4}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    goto :goto_7c
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 7

    .prologue
    .line 204
    array-length v0, p2

    if-eqz v0, :cond_6

    array-length v0, p3

    if-nez v0, :cond_7

    .line 237
    :cond_6
    :goto_6
    return-void

    .line 209
    :cond_7
    const/4 v0, 0x1

    if-ne p1, v0, :cond_6

    .line 211
    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_17

    .line 213
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lnet/hockeyapp/android/at;->startDownloadTask(Landroid/app/Activity;)V

    goto :goto_6

    .line 216
    :cond_17
    const-string v0, "HockeyApp"

    const-string v1, "User denied write permission, can\'t continue with updater task."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-static {}, Lnet/hockeyapp/android/ay;->a()Lnet/hockeyapp/android/az;

    move-result-object v0

    .line 219
    if-nez v0, :cond_6

    .line 223
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x700

    .line 224
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x701

    .line 225
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x702

    .line 226
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x703

    .line 227
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lnet/hockeyapp/android/av;

    invoke-direct {v2, p0, p0}, Lnet/hockeyapp/android/av;-><init>(Lnet/hockeyapp/android/at;Lnet/hockeyapp/android/at;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 233
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_6
.end method

.method public prepareDownload()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 189
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1e

    .line 191
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->checkSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1e

    .line 193
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0, v3}, Lnet/hockeyapp/android/at;->requestPermissions([Ljava/lang/String;I)V

    .line 200
    :goto_1d
    return-void

    .line 198
    :cond_1e
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lnet/hockeyapp/android/at;->startDownloadTask(Landroid/app/Activity;)V

    .line 199
    invoke-virtual {p0}, Lnet/hockeyapp/android/at;->dismiss()V

    goto :goto_1d
.end method
