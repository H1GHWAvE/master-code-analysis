.class public final Lnet/hockeyapp/android/z;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 49
    const-string v0, "hockeyapp_crash_dialog_title"

    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 50
    const-string v0, "hockeyapp_crash_dialog_message"

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 51
    const-string v0, "hockeyapp_crash_dialog_negative_button"

    const/4 v1, 0x2

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 52
    const-string v0, "hockeyapp_crash_dialog_neutral_button"

    const/4 v1, 0x3

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 53
    const-string v0, "hockeyapp_crash_dialog_positive_button"

    const/4 v1, 0x4

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 56
    const-string v0, "hockeyapp_download_failed_dialog_title"

    const/16 v1, 0x100

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 57
    const-string v0, "hockeyapp_download_failed_dialog_message"

    const/16 v1, 0x101

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 58
    const-string v0, "hockeyapp_download_failed_dialog_negative_button"

    const/16 v1, 0x102

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 59
    const-string v0, "hockeyapp_download_failed_dialog_positive_button"

    const/16 v1, 0x103

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 62
    const-string v0, "hockeyapp_update_mandatory_toast"

    const/16 v1, 0x200

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 63
    const-string v0, "hockeyapp_update_dialog_title"

    const/16 v1, 0x201

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 64
    const-string v0, "hockeyapp_update_dialog_message"

    const/16 v1, 0x202

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 65
    const-string v0, "hockeyapp_update_dialog_negative_button"

    const/16 v1, 0x203

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 66
    const-string v0, "hockeyapp_update_dialog_positive_button"

    const/16 v1, 0x204

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 69
    const-string v0, "hockeyapp_expiry_info_title"

    const/16 v1, 0x300

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 70
    const-string v0, "hockeyapp_expiry_info_text"

    const/16 v1, 0x301

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 73
    const-string v0, "hockeyapp_feedback_failed_title"

    const/16 v1, 0x400

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 74
    const-string v0, "hockeyapp_feedback_failed_text"

    const/16 v1, 0x401

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 75
    const-string v0, "hockeyapp_feedback_name_hint"

    const/16 v1, 0x402

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 76
    const-string v0, "hockeyapp_feedback_email_hint"

    const/16 v1, 0x403

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 77
    const-string v0, "hockeyapp_feedback_subject_hint"

    const/16 v1, 0x404

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 78
    const-string v0, "hockeyapp_feedback_message_hint"

    const/16 v1, 0x405

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 79
    const-string v0, "hockeyapp_feedback_last_updated_text"

    const/16 v1, 0x406

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 80
    const-string v0, "hockeyapp_feedback_attachment_button_text"

    const/16 v1, 0x407

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 81
    const-string v0, "hockeyapp_feedback_send_button_text"

    const/16 v1, 0x408

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 82
    const-string v0, "hockeyapp_feedback_response_button_text"

    const/16 v1, 0x409

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 83
    const-string v0, "hockeyapp_feedback_refresh_button_text"

    const/16 v1, 0x40a

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 84
    const-string v0, "hockeyapp_feedback_title"

    const/16 v1, 0x40b

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 85
    const-string v0, "hockeyapp_feedback_send_generic_error"

    const/16 v1, 0x40c

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 86
    const-string v0, "hockeyapp_feedback_send_network_error"

    const/16 v1, 0x40d

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 87
    const-string v0, "hockeyapp_feedback_validate_subject_error"

    const/16 v1, 0x40e

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 88
    const-string v0, "hockeyapp_feedback_validate_email_error"

    const/16 v1, 0x40f

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 89
    const-string v0, "hockeyapp_feedback_validate_email_empty"

    const/16 v1, 0x412

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 90
    const-string v0, "hockeyapp_feedback_validate_name_error"

    const/16 v1, 0x411

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 91
    const-string v0, "hockeyapp_feedback_validate_text_error"

    const/16 v1, 0x413

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 92
    const-string v0, "hockeyapp_feedback_generic_error"

    const/16 v1, 0x410

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 93
    const-string v0, "hockeyapp_feedback_attach_file"

    const/16 v1, 0x414

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 94
    const-string v0, "hockeyapp_feedback_attach_picture"

    const/16 v1, 0x415

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 95
    const-string v0, "hockeyapp_feedback_select_file"

    const/16 v1, 0x416

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 96
    const-string v0, "hockeyapp_feedback_select_picture"

    const/16 v1, 0x417

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 97
    const-string v0, "hockeyapp_feedback_max_attachments_allowed"

    const/16 v1, 0x418

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 101
    const-string v0, "hockeyapp_login_headline_text"

    const/16 v1, 0x500

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 102
    const-string v0, "hockeyapp_login_missing_credentials_toast"

    const/16 v1, 0x501

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 103
    const-string v0, "hockeyapp_login_email_hint"

    const/16 v1, 0x502

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 104
    const-string v0, "hockeyapp_login_password_hint"

    const/16 v1, 0x503

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 105
    const-string v0, "hockeyapp_login_login_button_text"

    const/16 v1, 0x504

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 108
    const-string v0, "hockeyapp_paint_indicator_toast"

    const/16 v1, 0x600

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 109
    const-string v0, "hockeyapp_paint_menu_save"

    const/16 v1, 0x601

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 110
    const-string v0, "hockeyapp_paint_menu_undo"

    const/16 v1, 0x602

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 111
    const-string v0, "hockeyapp_paint_menu_clear"

    const/16 v1, 0x603

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 112
    const-string v0, "hockeyapp_paint_dialog_message"

    const/16 v1, 0x604

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 113
    const-string v0, "hockeyapp_paint_dialog_negative_button"

    const/16 v1, 0x605

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 114
    const-string v0, "hockeyapp_paint_dialog_positive_button"

    const/16 v1, 0x606

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 117
    const-string v0, "hockeyapp_permission_update_title"

    const/16 v1, 0x700

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 118
    const-string v0, "hockeyapp_permission_update_message"

    const/16 v1, 0x701

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 119
    const-string v0, "hockeyapp_permission_dialog_negative_button"

    const/16 v1, 0x702

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 120
    const-string v0, "hockeyapp_permission_dialog_positive_button"

    const/16 v1, 0x703

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 123
    const-string v0, "hockeyapp_dialog_positive_button"

    const/16 v1, 0x800

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 124
    const-string v0, "hockeyapp_dialog_negative_button"

    const/16 v1, 0x801

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 125
    const-string v0, "hockeyapp_dialog_error_title"

    const/16 v1, 0x802

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 126
    const-string v0, "hockeyapp_dialog_error_message"

    const/16 v1, 0x803

    invoke-static {v0, v1, p0}, Lnet/hockeyapp/android/z;->a(Ljava/lang/String;ILandroid/content/Context;)V

    .line 127
    return-void
.end method

.method private static a(Ljava/lang/String;ILandroid/content/Context;)V
    .registers 6

    .prologue
    .line 130
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "string"

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 131
    if-nez v0, :cond_11

    .line 139
    :cond_10
    :goto_10
    return-void

    .line 135
    :cond_11
    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 136
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 137
    invoke-static {p1, v0}, Lnet/hockeyapp/android/aj;->a(ILjava/lang/String;)V

    goto :goto_10
.end method
