.class final Lnet/hockeyapp/android/o;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lnet/hockeyapp/android/FeedbackActivity;


# direct methods
.method constructor <init>(Lnet/hockeyapp/android/FeedbackActivity;)V
    .registers 2

    .prologue
    .line 625
    iput-object p1, p0, Lnet/hockeyapp/android/o;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 628
    .line 629
    iget-object v0, p0, Lnet/hockeyapp/android/o;->a:Lnet/hockeyapp/android/FeedbackActivity;

    new-instance v3, Lnet/hockeyapp/android/c/c;

    invoke-direct {v3}, Lnet/hockeyapp/android/c/c;-><init>()V

    invoke-static {v0, v3}, Lnet/hockeyapp/android/FeedbackActivity;->a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/c;)Lnet/hockeyapp/android/c/c;

    .line 631
    if-eqz p1, :cond_5e

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_5e

    .line 632
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 633
    const-string v3, "parse_feedback_response"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/c/h;

    .line 634
    if-eqz v0, :cond_5e

    .line 1047
    iget-object v3, v0, Lnet/hockeyapp/android/c/h;->a:Ljava/lang/String;

    .line 635
    const-string v4, "success"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5a

    .line 1063
    iget-object v2, v0, Lnet/hockeyapp/android/c/h;->c:Ljava/lang/String;

    .line 639
    if-eqz v2, :cond_5c

    .line 1065
    sget-object v2, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 641
    iget-object v3, p0, Lnet/hockeyapp/android/o;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-static {v3}, Lnet/hockeyapp/android/FeedbackActivity;->c(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/content/Context;

    move-result-object v3

    .line 2063
    iget-object v4, v0, Lnet/hockeyapp/android/c/h;->c:Ljava/lang/String;

    .line 641
    invoke-virtual {v2, v3, v4}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 643
    iget-object v2, p0, Lnet/hockeyapp/android/o;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-static {v2, v0}, Lnet/hockeyapp/android/FeedbackActivity;->a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/h;)V

    .line 644
    iget-object v0, p0, Lnet/hockeyapp/android/o;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->d(Lnet/hockeyapp/android/FeedbackActivity;)Z

    move v0, v1

    .line 654
    :goto_48
    if-nez v0, :cond_54

    .line 655
    iget-object v0, p0, Lnet/hockeyapp/android/o;->a:Lnet/hockeyapp/android/FeedbackActivity;

    new-instance v2, Lnet/hockeyapp/android/p;

    invoke-direct {v2, p0}, Lnet/hockeyapp/android/p;-><init>(Lnet/hockeyapp/android/o;)V

    invoke-virtual {v0, v2}, Lnet/hockeyapp/android/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 665
    :cond_54
    iget-object v0, p0, Lnet/hockeyapp/android/o;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Z)V

    .line 666
    return-void

    :cond_5a
    move v0, v2

    .line 648
    goto :goto_48

    :cond_5c
    move v0, v1

    goto :goto_48

    :cond_5e
    move v0, v2

    goto :goto_48
.end method
