.class public final Lnet/hockeyapp/android/c/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final f:J = 0x23f22471d131d6e8L


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lnet/hockeyapp/android/c/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 70
    iput p1, p0, Lnet/hockeyapp/android/c/d;->c:I

    .line 71
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 54
    iput-object p1, p0, Lnet/hockeyapp/android/c/d;->a:Ljava/lang/String;

    .line 55
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .registers 2

    .prologue
    .line 86
    iput-object p1, p0, Lnet/hockeyapp/android/c/d;->e:Ljava/util/ArrayList;

    .line 87
    return-void
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lnet/hockeyapp/android/c/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 62
    iput-object p1, p0, Lnet/hockeyapp/android/c/d;->b:Ljava/lang/String;

    .line 63
    return-void
.end method

.method private c()I
    .registers 2

    .prologue
    .line 66
    iget v0, p0, Lnet/hockeyapp/android/c/d;->c:I

    return v0
.end method

.method private c(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 78
    iput-object p1, p0, Lnet/hockeyapp/android/c/d;->d:Ljava/lang/String;

    .line 79
    return-void
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 74
    iget-object v0, p0, Lnet/hockeyapp/android/c/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method private e()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, Lnet/hockeyapp/android/c/d;->e:Ljava/util/ArrayList;

    return-object v0
.end method
