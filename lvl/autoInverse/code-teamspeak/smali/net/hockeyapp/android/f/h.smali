.class public final Lnet/hockeyapp/android/f/h;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field public static final a:I = 0x3001

.field public static final b:I = 0x3002

.field public static final c:I = 0x3003

.field public static final d:I = 0x3004


# instance fields
.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Lnet/hockeyapp/android/f/a;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lnet/hockeyapp/android/f/h;-><init>(Landroid/content/Context;B)V

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 11

    .prologue
    const/4 v7, -0x1

    const/4 v6, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 66
    iput-boolean v4, p0, Lnet/hockeyapp/android/f/h;->i:Z

    .line 1076
    invoke-virtual {p0, v4}, Lnet/hockeyapp/android/f/h;->setOrientation(I)V

    .line 1077
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->setGravity(I)V

    .line 1078
    const v0, -0x333334

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->setBackgroundColor(I)V

    .line 1082
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    .line 1083
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const/16 v1, 0x3001

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1085
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1086
    const/high16 v1, 0x41a00000    # 20.0f

    .line 1087
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1086
    invoke-static {v4, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 1089
    invoke-virtual {v0, v1, v1, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1090
    iget-object v1, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1091
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, v5, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1092
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1093
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1094
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1095
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1097
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->addView(Landroid/view/View;)V

    .line 1117
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    .line 1118
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const/16 v1, 0x3002

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1120
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1121
    const/high16 v1, 0x41a00000    # 20.0f

    .line 1122
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1121
    invoke-static {v4, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 1124
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1125
    iget-object v1, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1126
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, v5, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1127
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1128
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1129
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1130
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1132
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->addView(Landroid/view/View;)V

    .line 1152
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    .line 1153
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/16 v1, 0x3003

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1155
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1156
    const/high16 v1, 0x41a00000    # 20.0f

    .line 1157
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1156
    invoke-static {v4, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 1159
    invoke-virtual {v0, v1, v3, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1160
    iget-object v1, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1161
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, v5, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1162
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1163
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1164
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1165
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1167
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->addView(Landroid/view/View;)V

    .line 1183
    new-instance v0, Lnet/hockeyapp/android/f/a;

    invoke-direct {v0, p1}, Lnet/hockeyapp/android/f/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/h;->h:Lnet/hockeyapp/android/f/a;

    .line 1184
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->h:Lnet/hockeyapp/android/f/a;

    const/16 v1, 0x3004

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/f/a;->setId(I)V

    .line 1186
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1187
    const/high16 v1, 0x41a00000    # 20.0f

    .line 1188
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1187
    invoke-static {v4, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 1190
    invoke-virtual {v0, v1, v3, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1191
    iget-object v1, p0, Lnet/hockeyapp/android/f/h;->h:Lnet/hockeyapp/android/f/a;

    invoke-virtual {v1, v0}, Lnet/hockeyapp/android/f/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1193
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->h:Lnet/hockeyapp/android/f/a;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->addView(Landroid/view/View;)V

    .line 73
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->setOrientation(I)V

    .line 77
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->setGravity(I)V

    .line 78
    const v0, -0x333334

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->setBackgroundColor(I)V

    .line 79
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x2

    const/high16 v3, 0x3f800000    # 1.0f

    .line 82
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    .line 83
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const/16 v1, 0x3001

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 85
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 86
    const/high16 v1, 0x41a00000    # 20.0f

    .line 87
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 86
    invoke-static {v5, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 89
    invoke-virtual {v0, v1, v1, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 90
    iget-object v1, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 92
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 93
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 94
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 95
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 97
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->addView(Landroid/view/View;)V

    .line 98
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .registers 9

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x2

    const/high16 v3, 0x3f800000    # 1.0f

    .line 117
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    .line 118
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const/16 v1, 0x3002

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 120
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 121
    const/high16 v1, 0x41a00000    # 20.0f

    .line 122
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 121
    invoke-static {v5, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 124
    invoke-virtual {v0, v1, v4, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 125
    iget-object v1, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 126
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 127
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 128
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 129
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v6, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 130
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 132
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->addView(Landroid/view/View;)V

    .line 133
    return-void
.end method

.method private c(Landroid/content/Context;)V
    .registers 8

    .prologue
    const/4 v2, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 152
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    .line 153
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/16 v1, 0x3003

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 155
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 156
    const/4 v1, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    .line 157
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 156
    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 159
    invoke-virtual {v0, v1, v4, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 160
    iget-object v1, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v5, v1, v5, v2}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 162
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 163
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 164
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 165
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 167
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->addView(Landroid/view/View;)V

    .line 168
    return-void
.end method

.method private d(Landroid/content/Context;)V
    .registers 6

    .prologue
    const/4 v2, -0x1

    .line 183
    new-instance v0, Lnet/hockeyapp/android/f/a;

    invoke-direct {v0, p1}, Lnet/hockeyapp/android/f/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/h;->h:Lnet/hockeyapp/android/f/a;

    .line 184
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->h:Lnet/hockeyapp/android/f/a;

    const/16 v1, 0x3004

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/f/a;->setId(I)V

    .line 186
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 187
    const/4 v1, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    .line 188
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/h;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 187
    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 190
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 191
    iget-object v1, p0, Lnet/hockeyapp/android/f/h;->h:Lnet/hockeyapp/android/f/a;

    invoke-virtual {v1, v0}, Lnet/hockeyapp/android/f/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 193
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->h:Lnet/hockeyapp/android/f/a;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/h;->addView(Landroid/view/View;)V

    .line 194
    return-void
.end method

.method private setAuthorLaberColor(I)V
    .registers 3

    .prologue
    .line 111
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 112
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 114
    :cond_9
    return-void
.end method

.method private setDateLaberColor(I)V
    .registers 3

    .prologue
    .line 146
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 147
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 149
    :cond_9
    return-void
.end method

.method private setMessageLaberColor(I)V
    .registers 3

    .prologue
    .line 177
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 178
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 180
    :cond_9
    return-void
.end method


# virtual methods
.method public final setAuthorLabelText(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 105
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    if-eqz p1, :cond_b

    .line 106
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    :cond_b
    return-void
.end method

.method public final setDateLabelText(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 140
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    if-eqz p1, :cond_b

    .line 141
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :cond_b
    return-void
.end method

.method public final setFeedbackMessageViewBgAndTextColor(I)V
    .registers 5

    .prologue
    const/4 v2, -0x1

    const v1, -0x333334

    .line 203
    if-nez p1, :cond_15

    .line 204
    invoke-virtual {p0, v1}, Lnet/hockeyapp/android/f/h;->setBackgroundColor(I)V

    .line 205
    invoke-direct {p0, v2}, Lnet/hockeyapp/android/f/h;->setAuthorLaberColor(I)V

    .line 206
    invoke-direct {p0, v2}, Lnet/hockeyapp/android/f/h;->setDateLaberColor(I)V

    .line 213
    :cond_f
    :goto_f
    const/high16 v0, -0x1000000

    invoke-direct {p0, v0}, Lnet/hockeyapp/android/f/h;->setMessageLaberColor(I)V

    .line 214
    return-void

    .line 207
    :cond_15
    const/4 v0, 0x1

    if-ne p1, v0, :cond_f

    .line 208
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/f/h;->setBackgroundColor(I)V

    .line 209
    invoke-direct {p0, v1}, Lnet/hockeyapp/android/f/h;->setAuthorLaberColor(I)V

    .line 210
    invoke-direct {p0, v1}, Lnet/hockeyapp/android/f/h;->setDateLaberColor(I)V

    goto :goto_f
.end method

.method public final setMessageLabelText(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 171
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    if-eqz p1, :cond_b

    .line 172
    iget-object v0, p0, Lnet/hockeyapp/android/f/h;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    :cond_b
    return-void
.end method
