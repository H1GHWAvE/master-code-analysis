.class public final Lnet/hockeyapp/android/e/r;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Lnet/hockeyapp/android/e/r;-><init>()V

    return-void
.end method

.method private static a()Lnet/hockeyapp/android/e/r;
    .registers 1

    .prologue
    .line 51
    sget-object v0, Lnet/hockeyapp/android/e/v;->a:Lnet/hockeyapp/android/e/r;

    return-object v0
.end method

.method private a(Ljava/lang/ref/WeakReference;Landroid/app/ProgressDialog;)V
    .registers 5

    .prologue
    .line 76
    if-eqz p1, :cond_12

    .line 77
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 78
    if-eqz v0, :cond_12

    .line 79
    new-instance v1, Lnet/hockeyapp/android/e/t;

    invoke-direct {v1, p0, p2}, Lnet/hockeyapp/android/e/t;-><init>(Lnet/hockeyapp/android/e/r;Landroid/app/ProgressDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 90
    :cond_12
    return-void
.end method

.method private a(Ljava/lang/ref/WeakReference;Landroid/app/ProgressDialog;I)V
    .registers 6

    .prologue
    .line 56
    if-eqz p1, :cond_12

    .line 57
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 58
    if-eqz v0, :cond_12

    .line 59
    new-instance v1, Lnet/hockeyapp/android/e/s;

    invoke-direct {v1, p0, p2, v0, p3}, Lnet/hockeyapp/android/e/s;-><init>(Lnet/hockeyapp/android/e/r;Landroid/app/ProgressDialog;Landroid/app/Activity;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 73
    :cond_12
    return-void
.end method

.method private a(Ljava/lang/ref/WeakReference;Ljava/lang/String;I)V
    .registers 6

    .prologue
    .line 93
    if-eqz p1, :cond_12

    .line 94
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 95
    if-eqz v0, :cond_12

    .line 96
    new-instance v1, Lnet/hockeyapp/android/e/u;

    invoke-direct {v1, p0, v0, p2, p3}, Lnet/hockeyapp/android/e/u;-><init>(Lnet/hockeyapp/android/e/r;Landroid/app/Activity;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 104
    :cond_12
    return-void
.end method
