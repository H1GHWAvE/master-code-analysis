.class public Lnet/hockeyapp/android/e/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x4

.field public static final e:I = 0x8

.field static final synthetic f:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 30
    const-class v0, Lnet/hockeyapp/android/e/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lnet/hockeyapp/android/e/b;->f:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([B)Ljava/lang/String;
    .registers 5

    .prologue
    .line 456
    :try_start_0
    new-instance v0, Ljava/lang/String;

    .line 2496
    const/4 v1, 0x0

    array-length v2, p0

    const/4 v3, 0x2

    invoke-static {p0, v1, v2, v3}, Lnet/hockeyapp/android/e/b;->b([BIII)[B

    move-result-object v1

    .line 456
    const-string v2, "US-ASCII"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_e} :catch_f

    return-object v0

    .line 457
    :catch_f
    move-exception v0

    .line 459
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a([BIII)Ljava/lang/String;
    .registers 7

    .prologue
    .line 478
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-static {p0, p1, p2, p3}, Lnet/hockeyapp/android/e/b;->b([BIII)[B

    move-result-object v1

    const-string v2, "US-ASCII"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_b} :catch_c

    return-object v0

    .line 479
    :catch_c
    move-exception v0

    .line 481
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a(Ljava/lang/String;I)[B
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 115
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 1134
    array-length v1, v0

    .line 1157
    new-instance v2, Lnet/hockeyapp/android/e/d;

    mul-int/lit8 v3, v1, 0x3

    div-int/lit8 v3, v3, 0x4

    new-array v3, v3, [B

    invoke-direct {v2, p1, v3}, Lnet/hockeyapp/android/e/d;-><init>(I[B)V

    .line 1159
    invoke-virtual {v2, v0, v4, v1}, Lnet/hockeyapp/android/e/d;->a([BII)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 1160
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bad base-64"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1164
    :cond_1f
    iget v0, v2, Lnet/hockeyapp/android/e/d;->b:I

    iget-object v1, v2, Lnet/hockeyapp/android/e/d;->a:[B

    array-length v1, v1

    if-ne v0, v1, :cond_29

    .line 1165
    iget-object v0, v2, Lnet/hockeyapp/android/e/d;->a:[B

    :goto_28
    return-object v0

    .line 1170
    :cond_29
    iget v0, v2, Lnet/hockeyapp/android/e/d;->b:I

    new-array v0, v0, [B

    .line 1171
    iget-object v1, v2, Lnet/hockeyapp/android/e/d;->a:[B

    iget v2, v2, Lnet/hockeyapp/android/e/d;->b:I

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_28
.end method

.method private static a([BI)[B
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 134
    array-length v0, p0

    .line 2157
    new-instance v1, Lnet/hockeyapp/android/e/d;

    mul-int/lit8 v2, v0, 0x3

    div-int/lit8 v2, v2, 0x4

    new-array v2, v2, [B

    invoke-direct {v1, p1, v2}, Lnet/hockeyapp/android/e/d;-><init>(I[B)V

    .line 2159
    invoke-virtual {v1, p0, v3, v0}, Lnet/hockeyapp/android/e/d;->a([BII)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 2160
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bad base-64"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164
    :cond_1b
    iget v0, v1, Lnet/hockeyapp/android/e/d;->b:I

    iget-object v2, v1, Lnet/hockeyapp/android/e/d;->a:[B

    array-length v2, v2

    if-ne v0, v2, :cond_25

    .line 2165
    iget-object v0, v1, Lnet/hockeyapp/android/e/d;->a:[B

    :goto_24
    return-object v0

    .line 2170
    :cond_25
    iget v0, v1, Lnet/hockeyapp/android/e/d;->b:I

    new-array v0, v0, [B

    .line 2171
    iget-object v2, v1, Lnet/hockeyapp/android/e/d;->a:[B

    iget v1, v1, Lnet/hockeyapp/android/e/d;->b:I

    invoke-static {v2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_24
.end method

.method private static a([BII)[B
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 157
    new-instance v1, Lnet/hockeyapp/android/e/d;

    mul-int/lit8 v0, p1, 0x3

    div-int/lit8 v0, v0, 0x4

    new-array v0, v0, [B

    invoke-direct {v1, p2, v0}, Lnet/hockeyapp/android/e/d;-><init>(I[B)V

    .line 159
    invoke-virtual {v1, p0, v3, p1}, Lnet/hockeyapp/android/e/d;->a([BII)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 160
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bad base-64"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_1a
    iget v0, v1, Lnet/hockeyapp/android/e/d;->b:I

    iget-object v2, v1, Lnet/hockeyapp/android/e/d;->a:[B

    array-length v2, v2

    if-ne v0, v2, :cond_24

    .line 165
    iget-object v0, v1, Lnet/hockeyapp/android/e/d;->a:[B

    .line 172
    :goto_23
    return-object v0

    .line 170
    :cond_24
    iget v0, v1, Lnet/hockeyapp/android/e/d;->b:I

    new-array v0, v0, [B

    .line 171
    iget-object v2, v1, Lnet/hockeyapp/android/e/d;->a:[B

    iget v1, v1, Lnet/hockeyapp/android/e/d;->b:I

    invoke-static {v2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_23
.end method

.method private static b([BI)[B
    .registers 4

    .prologue
    .line 496
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lnet/hockeyapp/android/e/b;->b([BIII)[B

    move-result-object v0

    return-object v0
.end method

.method private static b([BIII)[B
    .registers 8

    .prologue
    .line 513
    new-instance v2, Lnet/hockeyapp/android/e/e;

    invoke-direct {v2, p3}, Lnet/hockeyapp/android/e/e;-><init>(I)V

    .line 516
    div-int/lit8 v0, p2, 0x3

    mul-int/lit8 v0, v0, 0x4

    .line 519
    iget-boolean v1, v2, Lnet/hockeyapp/android/e/e;->e:Z

    if-eqz v1, :cond_3b

    .line 520
    rem-int/lit8 v1, p2, 0x3

    if-lez v1, :cond_13

    .line 521
    add-int/lit8 v0, v0, 0x4

    .line 532
    :cond_13
    :goto_13
    :pswitch_13
    iget-boolean v1, v2, Lnet/hockeyapp/android/e/e;->f:Z

    if-eqz v1, :cond_26

    if-lez p2, :cond_26

    .line 533
    add-int/lit8 v1, p2, -0x1

    div-int/lit8 v1, v1, 0x39

    add-int/lit8 v3, v1, 0x1

    iget-boolean v1, v2, Lnet/hockeyapp/android/e/e;->g:Z

    if-eqz v1, :cond_47

    const/4 v1, 0x2

    :goto_24
    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 537
    :cond_26
    new-array v1, v0, [B

    iput-object v1, v2, Lnet/hockeyapp/android/e/e;->a:[B

    .line 538
    invoke-virtual {v2, p0, p1, p2}, Lnet/hockeyapp/android/e/e;->a([BII)Z

    .line 540
    sget-boolean v1, Lnet/hockeyapp/android/e/b;->f:Z

    if-nez v1, :cond_49

    iget v1, v2, Lnet/hockeyapp/android/e/e;->b:I

    if-eq v1, v0, :cond_49

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 524
    :cond_3b
    rem-int/lit8 v1, p2, 0x3

    packed-switch v1, :pswitch_data_4c

    goto :goto_13

    .line 526
    :pswitch_41
    add-int/lit8 v0, v0, 0x2

    goto :goto_13

    .line 527
    :pswitch_44
    add-int/lit8 v0, v0, 0x3

    goto :goto_13

    .line 533
    :cond_47
    const/4 v1, 0x1

    goto :goto_24

    .line 542
    :cond_49
    iget-object v0, v2, Lnet/hockeyapp/android/e/e;->a:[B

    return-object v0

    .line 524
    :pswitch_data_4c
    .packed-switch 0x0
        :pswitch_13
        :pswitch_41
        :pswitch_44
    .end packed-switch
.end method
