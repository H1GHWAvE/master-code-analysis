.class public final Lnet/hockeyapp/android/d/q;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# static fields
.field public static final a:I = 0x2

.field public static final b:Ljava/lang/String; = "net.hockeyapp.android.feedback"

.field public static final c:Ljava/lang/String; = "idLastMessageSend"

.field public static final d:Ljava/lang/String; = "idLastMessageProcessed"


# instance fields
.field public e:Ljava/lang/String;

.field private f:Landroid/content/Context;

.field private g:Ljava/lang/String;

.field private h:Landroid/os/Handler;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 73
    iput-object p1, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lnet/hockeyapp/android/d/q;->g:Ljava/lang/String;

    .line 75
    iput-object p3, p0, Lnet/hockeyapp/android/d/q;->h:Landroid/os/Handler;

    .line 76
    iput-object p4, p0, Lnet/hockeyapp/android/d/q;->i:Ljava/lang/String;

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    .line 78
    return-void
.end method

.method private varargs a()Lnet/hockeyapp/android/c/h;
    .registers 10

    .prologue
    const/4 v1, 0x0

    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 86
    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    if-eqz v0, :cond_df

    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->g:Ljava/lang/String;

    if-eqz v0, :cond_df

    .line 1064
    sget-object v0, Lnet/hockeyapp/android/e/k;->a:Lnet/hockeyapp/android/e/i;

    .line 87
    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->g:Ljava/lang/String;

    invoke-static {v0}, Lnet/hockeyapp/android/e/i;->a(Ljava/lang/String;)Lnet/hockeyapp/android/c/h;

    move-result-object v2

    .line 89
    if-eqz v2, :cond_58

    .line 2055
    iget-object v0, v2, Lnet/hockeyapp/android/c/h;->b:Lnet/hockeyapp/android/c/d;

    .line 91
    if-eqz v0, :cond_58

    .line 3055
    iget-object v0, v2, Lnet/hockeyapp/android/c/h;->b:Lnet/hockeyapp/android/c/d;

    .line 3082
    iget-object v0, v0, Lnet/hockeyapp/android/c/d;->e:Ljava/util/ArrayList;

    .line 93
    if-eqz v0, :cond_58

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_58

    .line 3119
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/c/g;

    .line 4107
    iget v0, v0, Lnet/hockeyapp/android/c/g;->g:I

    .line 3122
    iget-object v4, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    const-string v5, "net.hockeyapp.android.feedback"

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 3124
    iget-object v5, p0, Lnet/hockeyapp/android/d/q;->i:Ljava/lang/String;

    const-string v6, "send"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5a

    .line 3125
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "idLastMessageSend"

    .line 3126
    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "idLastMessageProcessed"

    .line 3127
    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3125
    invoke-static {v0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_58
    :goto_58
    move-object v0, v2

    .line 102
    :goto_59
    return-object v0

    .line 3129
    :cond_5a
    iget-object v5, p0, Lnet/hockeyapp/android/d/q;->i:Ljava/lang/String;

    const-string v6, "fetch"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_58

    .line 3130
    const-string v5, "idLastMessageSend"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 3131
    const-string v6, "idLastMessageProcessed"

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 3133
    if-eq v0, v5, :cond_58

    if-eq v0, v6, :cond_58

    .line 3135
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "idLastMessageProcessed"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 3138
    invoke-static {}, Lnet/hockeyapp/android/t;->a()Lnet/hockeyapp/android/y;

    move-result-object v0

    .line 3139
    if-eqz v0, :cond_e2

    .line 3140
    invoke-virtual {v0}, Lnet/hockeyapp/android/y;->a()Z

    move-result v0

    .line 3143
    :goto_8b
    if-nez v0, :cond_58

    .line 3144
    iget-object v4, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    .line 4152
    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    if-eqz v0, :cond_58

    .line 4156
    const-string v0, "notification"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 4157
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "ic_menu_refresh"

    const-string v7, "drawable"

    const-string v8, "android"

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 4160
    invoke-static {}, Lnet/hockeyapp/android/t;->a()Lnet/hockeyapp/android/y;

    move-result-object v6

    if-eqz v6, :cond_b1

    .line 5046
    const-class v1, Lnet/hockeyapp/android/FeedbackActivity;

    .line 4163
    :cond_b1
    if-nez v1, :cond_b5

    .line 4164
    const-class v1, Lnet/hockeyapp/android/FeedbackActivity;

    .line 4167
    :cond_b5
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 4168
    const/high16 v7, 0x30000000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 4169
    invoke-virtual {v6, v4, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 4170
    const-string v1, "url"

    iget-object v7, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    invoke-virtual {v6, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4172
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v4, v3, v6, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 4174
    const-string v3, "HockeyApp Feedback"

    const-string v6, "A new answer to your feedback is available."

    invoke-static {v4, v1, v3, v6, v5}, Lnet/hockeyapp/android/e/w;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object v1

    .line 4176
    if-eqz v1, :cond_58

    .line 4177
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_58

    :cond_df
    move-object v0, v1

    .line 102
    goto/16 :goto_59

    :cond_e2
    move v0, v3

    goto :goto_8b
.end method

.method private a(Landroid/content/Context;)V
    .registers 7

    .prologue
    .line 152
    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 179
    :cond_4
    :goto_4
    return-void

    .line 156
    :cond_5
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "ic_menu_refresh"

    const-string v3, "drawable"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 159
    const/4 v1, 0x0

    .line 160
    invoke-static {}, Lnet/hockeyapp/android/t;->a()Lnet/hockeyapp/android/y;

    move-result-object v3

    if-eqz v3, :cond_24

    .line 7046
    const-class v1, Lnet/hockeyapp/android/FeedbackActivity;

    .line 163
    :cond_24
    if-nez v1, :cond_28

    .line 164
    const-class v1, Lnet/hockeyapp/android/FeedbackActivity;

    .line 167
    :cond_28
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 168
    const/high16 v4, 0x30000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 169
    invoke-virtual {v3, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 170
    const-string v1, "url"

    iget-object v4, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const/4 v1, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {p1, v1, v3, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 174
    const-string v3, "HockeyApp Feedback"

    const-string v4, "A new answer to your feedback is available."

    invoke-static {p1, v1, v3, v4, v2}, Lnet/hockeyapp/android/e/w;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object v1

    .line 176
    if-eqz v1, :cond_4

    .line 177
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_4
.end method

.method private a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 81
    iput-object p1, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    .line 82
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .registers 9

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 119
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/c/g;

    .line 5107
    iget v0, v0, Lnet/hockeyapp/android/c/g;->g:I

    .line 122
    iget-object v1, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    const-string v3, "net.hockeyapp.android.feedback"

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 124
    iget-object v3, p0, Lnet/hockeyapp/android/d/q;->i:Ljava/lang/String;

    const-string v4, "send"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 125
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "idLastMessageSend"

    .line 126
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "idLastMessageProcessed"

    .line 127
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 125
    invoke-static {v0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 148
    :cond_35
    :goto_35
    return-void

    .line 129
    :cond_36
    iget-object v3, p0, Lnet/hockeyapp/android/d/q;->i:Ljava/lang/String;

    const-string v4, "fetch"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 130
    const-string v3, "idLastMessageSend"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 131
    const-string v4, "idLastMessageProcessed"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 133
    if-eq v0, v3, :cond_35

    if-eq v0, v4, :cond_35

    .line 135
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "idLastMessageProcessed"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 138
    invoke-static {}, Lnet/hockeyapp/android/t;->a()Lnet/hockeyapp/android/y;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_bc

    .line 140
    invoke-virtual {v0}, Lnet/hockeyapp/android/y;->a()Z

    move-result v0

    .line 143
    :goto_67
    if-nez v0, :cond_35

    .line 144
    iget-object v3, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    .line 5152
    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    if-eqz v0, :cond_35

    .line 5156
    const-string v0, "notification"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 5157
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v4, "ic_menu_refresh"

    const-string v5, "drawable"

    const-string v6, "android"

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 5159
    const/4 v1, 0x0

    .line 5160
    invoke-static {}, Lnet/hockeyapp/android/t;->a()Lnet/hockeyapp/android/y;

    move-result-object v5

    if-eqz v5, :cond_8e

    .line 6046
    const-class v1, Lnet/hockeyapp/android/FeedbackActivity;

    .line 5163
    :cond_8e
    if-nez v1, :cond_92

    .line 5164
    const-class v1, Lnet/hockeyapp/android/FeedbackActivity;

    .line 5167
    :cond_92
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 5168
    const/high16 v6, 0x30000000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5169
    invoke-virtual {v5, v3, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 5170
    const-string v1, "url"

    iget-object v6, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    invoke-virtual {v5, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5172
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v3, v2, v5, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 5174
    const-string v2, "HockeyApp Feedback"

    const-string v5, "A new answer to your feedback is available."

    invoke-static {v3, v1, v2, v5, v4}, Lnet/hockeyapp/android/e/w;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object v1

    .line 5176
    if-eqz v1, :cond_35

    .line 5177
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_35

    :cond_bc
    move v0, v2

    goto :goto_67
.end method

.method private a(Lnet/hockeyapp/android/c/h;)V
    .registers 5

    .prologue
    .line 107
    if-eqz p1, :cond_1d

    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->h:Landroid/os/Handler;

    if-eqz v0, :cond_1d

    .line 108
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 109
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 111
    const-string v2, "parse_feedback_response"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 112
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 114
    iget-object v1, p0, Lnet/hockeyapp/android/d/q;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 116
    :cond_1d
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 11

    .prologue
    const/4 v1, 0x0

    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 60
    .line 8086
    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    if-eqz v0, :cond_df

    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->g:Ljava/lang/String;

    if-eqz v0, :cond_df

    .line 9064
    sget-object v0, Lnet/hockeyapp/android/e/k;->a:Lnet/hockeyapp/android/e/i;

    .line 8087
    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->g:Ljava/lang/String;

    invoke-static {v0}, Lnet/hockeyapp/android/e/i;->a(Ljava/lang/String;)Lnet/hockeyapp/android/c/h;

    move-result-object v2

    .line 8089
    if-eqz v2, :cond_58

    .line 10055
    iget-object v0, v2, Lnet/hockeyapp/android/c/h;->b:Lnet/hockeyapp/android/c/d;

    .line 8091
    if-eqz v0, :cond_58

    .line 11055
    iget-object v0, v2, Lnet/hockeyapp/android/c/h;->b:Lnet/hockeyapp/android/c/d;

    .line 11082
    iget-object v0, v0, Lnet/hockeyapp/android/c/d;->e:Ljava/util/ArrayList;

    .line 8093
    if-eqz v0, :cond_58

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_58

    .line 11119
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/c/g;

    .line 12107
    iget v0, v0, Lnet/hockeyapp/android/c/g;->g:I

    .line 11122
    iget-object v4, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    const-string v5, "net.hockeyapp.android.feedback"

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 11124
    iget-object v5, p0, Lnet/hockeyapp/android/d/q;->i:Ljava/lang/String;

    const-string v6, "send"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5a

    .line 11125
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "idLastMessageSend"

    .line 11126
    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "idLastMessageProcessed"

    .line 11127
    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 11125
    invoke-static {v0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_58
    :goto_58
    move-object v0, v2

    .line 8099
    :goto_59
    return-object v0

    .line 11129
    :cond_5a
    iget-object v5, p0, Lnet/hockeyapp/android/d/q;->i:Ljava/lang/String;

    const-string v6, "fetch"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_58

    .line 11130
    const-string v5, "idLastMessageSend"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 11131
    const-string v6, "idLastMessageProcessed"

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 11133
    if-eq v0, v5, :cond_58

    if-eq v0, v6, :cond_58

    .line 11135
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "idLastMessageProcessed"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 11138
    invoke-static {}, Lnet/hockeyapp/android/t;->a()Lnet/hockeyapp/android/y;

    move-result-object v0

    .line 11139
    if-eqz v0, :cond_e2

    .line 11140
    invoke-virtual {v0}, Lnet/hockeyapp/android/y;->a()Z

    move-result v0

    .line 11143
    :goto_8b
    if-nez v0, :cond_58

    .line 11144
    iget-object v4, p0, Lnet/hockeyapp/android/d/q;->f:Landroid/content/Context;

    .line 12152
    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    if-eqz v0, :cond_58

    .line 12156
    const-string v0, "notification"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 12157
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "ic_menu_refresh"

    const-string v7, "drawable"

    const-string v8, "android"

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 12160
    invoke-static {}, Lnet/hockeyapp/android/t;->a()Lnet/hockeyapp/android/y;

    move-result-object v6

    if-eqz v6, :cond_b1

    .line 13046
    const-class v1, Lnet/hockeyapp/android/FeedbackActivity;

    .line 12163
    :cond_b1
    if-nez v1, :cond_b5

    .line 12164
    const-class v1, Lnet/hockeyapp/android/FeedbackActivity;

    .line 12167
    :cond_b5
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 12168
    const/high16 v7, 0x30000000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 12169
    invoke-virtual {v6, v4, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 12170
    const-string v1, "url"

    iget-object v7, p0, Lnet/hockeyapp/android/d/q;->e:Ljava/lang/String;

    invoke-virtual {v6, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 12172
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v4, v3, v6, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 12174
    const-string v3, "HockeyApp Feedback"

    const-string v6, "A new answer to your feedback is available."

    invoke-static {v4, v1, v3, v6, v5}, Lnet/hockeyapp/android/e/w;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object v1

    .line 12176
    if-eqz v1, :cond_58

    .line 12177
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_58

    :cond_df
    move-object v0, v1

    .line 60
    goto/16 :goto_59

    :cond_e2
    move v0, v3

    goto :goto_8b
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 60
    check-cast p1, Lnet/hockeyapp/android/c/h;

    .line 7107
    if-eqz p1, :cond_1f

    iget-object v0, p0, Lnet/hockeyapp/android/d/q;->h:Landroid/os/Handler;

    if-eqz v0, :cond_1f

    .line 7108
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 7109
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 7111
    const-string v2, "parse_feedback_response"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 7112
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 7114
    iget-object v1, p0, Lnet/hockeyapp/android/d/q;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 60
    :cond_1f
    return-void
.end method
