.class public final Lnet/hockeyapp/android/d/p;
.super Lnet/hockeyapp/android/d/k;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/os/Handler;

.field public c:Landroid/app/ProgressDialog;

.field public d:Z

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V
    .registers 7

    .prologue
    .line 81
    invoke-direct {p0}, Lnet/hockeyapp/android/d/k;-><init>()V

    .line 82
    iput-object p1, p0, Lnet/hockeyapp/android/d/p;->a:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    .line 84
    iput-object p3, p0, Lnet/hockeyapp/android/d/p;->f:Ljava/lang/String;

    .line 85
    iput p4, p0, Lnet/hockeyapp/android/d/p;->e:I

    .line 86
    iput-object p5, p0, Lnet/hockeyapp/android/d/p;->g:Ljava/util/Map;

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/p;->d:Z

    .line 89
    if-eqz p1, :cond_15

    .line 90
    invoke-static {p1}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 92
    :cond_15
    return-void
.end method

.method private a(ILjava/util/Map;)Ljava/net/HttpURLConnection;
    .registers 8

    .prologue
    .line 170
    const/4 v0, 0x1

    if-ne p1, v0, :cond_17

    .line 172
    new-instance v0, Lnet/hockeyapp/android/e/l;

    iget-object v1, p0, Lnet/hockeyapp/android/d/p;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    const-string v1, "POST"

    .line 4071
    iput-object v1, v0, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    .line 174
    invoke-virtual {v0, p2}, Lnet/hockeyapp/android/e/l;->a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 190
    :goto_16
    return-object v0

    .line 177
    :cond_17
    const/4 v0, 0x2

    if-ne p1, v0, :cond_6d

    .line 179
    new-instance v2, Lnet/hockeyapp/android/e/l;

    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->f:Ljava/lang/String;

    invoke-direct {v2, v0}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    const-string v0, "POST"

    .line 5071
    iput-object v0, v2, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    .line 180
    const-string v0, "email"

    .line 181
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "password"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 5133
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Basic "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5134
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 5133
    invoke-static {v0}, Lnet/hockeyapp/android/e/b;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5136
    const-string v1, "Authorization"

    invoke-virtual {v2, v1, v0}, Lnet/hockeyapp/android/e/l;->a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;

    .line 182
    invoke-virtual {v2}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;

    move-result-object v0

    goto :goto_16

    .line 184
    :cond_6d
    const/4 v0, 0x3

    if-ne p1, v0, :cond_ae

    .line 185
    const-string v0, "type"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 186
    const-string v1, "id"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 187
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lnet/hockeyapp/android/d/p;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    new-instance v1, Lnet/hockeyapp/android/e/l;

    invoke-direct {v1, v0}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    .line 190
    invoke-virtual {v1}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;

    move-result-object v0

    goto/16 :goto_16

    .line 193
    :cond_ae
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Login mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a()V
    .registers 2

    .prologue
    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/p;->d:Z

    .line 96
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 3

    .prologue
    .line 99
    iput-object p1, p0, Lnet/hockeyapp/android/d/p;->a:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    .line 101
    return-void
.end method

.method private a(Ljava/lang/Boolean;)V
    .registers 6

    .prologue
    .line 149
    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_9

    .line 151
    :try_start_4
    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_9} :catch_29

    .line 159
    :cond_9
    :goto_9
    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    if-eqz v0, :cond_28

    .line 160
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 161
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 162
    const-string v2, "success"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 164
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 165
    iget-object v1, p0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 167
    :cond_28
    return-void

    .line 154
    :catch_29
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_9
.end method

.method private a(Ljava/lang/String;)Z
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 198
    iget-object v2, p0, Lnet/hockeyapp/android/d/p;->a:Landroid/content/Context;

    const-string v3, "net.hockeyapp.android.login"

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 201
    :try_start_a
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 202
    const-string v4, "status"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 204
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 242
    :cond_1b
    :goto_1b
    return v0

    .line 208
    :cond_1c
    iget v5, p0, Lnet/hockeyapp/android/d/p;->e:I

    if-ne v5, v1, :cond_43

    .line 209
    const-string v5, "identified"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 210
    const-string v4, "iuid"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 211
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1b

    .line 212
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "iuid"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    move v0, v1

    .line 213
    goto :goto_1b

    .line 217
    :cond_43
    iget v5, p0, Lnet/hockeyapp/android/d/p;->e:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6b

    .line 218
    const-string v5, "authorized"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 219
    const-string v4, "auid"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 220
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1b

    .line 221
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "auid"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v2}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    move v0, v1

    .line 222
    goto :goto_1b

    .line 226
    :cond_6b
    iget v3, p0, Lnet/hockeyapp/android/d/p;->e:I

    const/4 v5, 0x3

    if-ne v3, v5, :cond_93

    .line 227
    const-string v3, "validated"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7a

    move v0, v1

    .line 228
    goto :goto_1b

    .line 231
    :cond_7a
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "iuid"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "auid"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_8d
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_8d} :catch_8e

    goto :goto_1b

    .line 241
    :catch_8e
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1b

    .line 235
    :cond_93
    :try_start_93
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Login mode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lnet/hockeyapp/android/d/p;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_b0
    .catch Lorg/json/JSONException; {:try_start_93 .. :try_end_b0} :catch_8e
.end method

.method private b()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 104
    iput-object v0, p0, Lnet/hockeyapp/android/d/p;->a:Landroid/content/Context;

    .line 105
    iput-object v0, p0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    .line 106
    iput-object v0, p0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    .line 107
    return-void
.end method

.method private varargs c()Ljava/lang/Boolean;
    .registers 7

    .prologue
    .line 118
    const/4 v2, 0x0

    .line 121
    :try_start_1
    iget v0, p0, Lnet/hockeyapp/android/d/p;->e:I

    iget-object v1, p0, Lnet/hockeyapp/android/d/p;->g:Ljava/util/Map;

    .line 1170
    const/4 v3, 0x1

    if-ne v0, v3, :cond_3e

    .line 1172
    new-instance v0, Lnet/hockeyapp/android/e/l;

    iget-object v3, p0, Lnet/hockeyapp/android/d/p;->f:Ljava/lang/String;

    invoke-direct {v0, v3}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    const-string v3, "POST"

    .line 2071
    iput-object v3, v0, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    .line 1174
    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/e/l;->a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;

    move-result-object v0

    .line 1175
    invoke-virtual {v0}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;
    :try_end_1a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1a} :catch_f0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1a} :catch_107
    .catchall {:try_start_1 .. :try_end_1a} :catchall_111

    move-result-object v1

    .line 122
    :goto_1b
    :try_start_1b
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 124
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v2, 0xc8

    if-ne v0, v2, :cond_101

    .line 125
    invoke-static {v1}, Lnet/hockeyapp/android/d/p;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_101

    .line 128
    invoke-direct {p0, v0}, Lnet/hockeyapp/android/d/p;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_37
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1b .. :try_end_37} :catch_11e
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_37} :catch_11b
    .catchall {:try_start_1b .. :try_end_37} :catchall_118

    move-result-object v0

    .line 139
    if-eqz v1, :cond_3d

    .line 140
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 144
    :cond_3d
    :goto_3d
    return-object v0

    .line 1177
    :cond_3e
    const/4 v3, 0x2

    if-ne v0, v3, :cond_94

    .line 1179
    :try_start_41
    new-instance v3, Lnet/hockeyapp/android/e/l;

    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->f:Ljava/lang/String;

    invoke-direct {v3, v0}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    const-string v0, "POST"

    .line 3071
    iput-object v0, v3, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    .line 1180
    const-string v0, "email"

    .line 1181
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "password"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3133
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Basic "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3134
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 3133
    invoke-static {v0}, Lnet/hockeyapp/android/e/b;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3136
    const-string v1, "Authorization"

    invoke-virtual {v3, v1, v0}, Lnet/hockeyapp/android/e/l;->a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;

    .line 1182
    invoke-virtual {v3}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;

    move-result-object v1

    goto :goto_1b

    .line 1184
    :cond_94
    const/4 v3, 0x3

    if-ne v0, v3, :cond_d5

    .line 1185
    const-string v0, "type"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1186
    const-string v3, "id"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1187
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lnet/hockeyapp/android/d/p;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1189
    new-instance v1, Lnet/hockeyapp/android/e/l;

    invoke-direct {v1, v0}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    .line 1190
    invoke-virtual {v1}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;

    move-result-object v1

    goto/16 :goto_1b

    .line 1193
    :cond_d5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Login mode "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " not supported."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_f0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_41 .. :try_end_f0} :catch_f0
    .catch Ljava/io/IOException; {:try_start_41 .. :try_end_f0} :catch_107
    .catchall {:try_start_41 .. :try_end_f0} :catchall_111

    .line 133
    :catch_f0
    move-exception v0

    move-object v1, v2

    :goto_f2
    :try_start_f2
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
    :try_end_f5
    .catchall {:try_start_f2 .. :try_end_f5} :catchall_118

    .line 139
    if-eqz v1, :cond_fa

    .line 140
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 144
    :cond_fa
    :goto_fa
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_3d

    .line 139
    :cond_101
    if-eqz v1, :cond_fa

    .line 140
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_fa

    .line 136
    :catch_107
    move-exception v0

    :goto_108
    :try_start_108
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10b
    .catchall {:try_start_108 .. :try_end_10b} :catchall_111

    .line 139
    if-eqz v2, :cond_fa

    .line 140
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_fa

    .line 139
    :catchall_111
    move-exception v0

    :goto_112
    if-eqz v2, :cond_117

    .line 140
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_117
    throw v0

    .line 139
    :catchall_118
    move-exception v0

    move-object v2, v1

    goto :goto_112

    .line 136
    :catch_11b
    move-exception v0

    move-object v2, v1

    goto :goto_108

    .line 133
    :catch_11e
    move-exception v0

    goto :goto_f2
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 59
    invoke-direct {p0}, Lnet/hockeyapp/android/d/p;->c()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 59
    check-cast p1, Ljava/lang/Boolean;

    .line 5149
    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_b

    .line 5151
    :try_start_6
    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_b} :catch_2b

    .line 5159
    :cond_b
    :goto_b
    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    if-eqz v0, :cond_2a

    .line 5160
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 5161
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 5162
    const-string v2, "success"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 5164
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 5165
    iget-object v1, p0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 59
    :cond_2a
    return-void

    .line 5154
    :catch_2b
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_b
.end method

.method protected final onPreExecute()V
    .registers 6

    .prologue
    .line 111
    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1e

    :cond_c
    iget-boolean v0, p0, Lnet/hockeyapp/android/d/p;->d:Z

    if-eqz v0, :cond_1e

    .line 112
    iget-object v0, p0, Lnet/hockeyapp/android/d/p;->a:Landroid/content/Context;

    const-string v1, ""

    const-string v2, "Please wait..."

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    .line 114
    :cond_1e
    return-void
.end method
