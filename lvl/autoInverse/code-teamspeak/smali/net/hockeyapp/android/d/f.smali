.class final Lnet/hockeyapp/android/d/f;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field private final a:Lnet/hockeyapp/android/d/e;

.field private final b:Landroid/os/Handler;

.field private c:Ljava/io/File;

.field private d:Landroid/graphics/Bitmap;

.field private e:I


# direct methods
.method public constructor <init>(Lnet/hockeyapp/android/d/e;Landroid/os/Handler;)V
    .registers 4

    .prologue
    .line 166
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 167
    iput-object p1, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    .line 168
    iput-object p2, p0, Lnet/hockeyapp/android/d/f;->b:Landroid/os/Handler;

    .line 169
    invoke-static {}, Lnet/hockeyapp/android/a;->a()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/d/f;->c:Ljava/io/File;

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/d/f;->d:Landroid/graphics/Bitmap;

    .line 171
    const/4 v0, 0x0

    iput v0, p0, Lnet/hockeyapp/android/d/f;->e:I

    .line 172
    return-void
.end method

.method private varargs a()Ljava/lang/Boolean;
    .registers 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 180
    iget-object v2, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    .line 1131
    iget-object v2, v2, Lnet/hockeyapp/android/d/e;->a:Lnet/hockeyapp/android/c/e;

    .line 2109
    invoke-static {}, Lnet/hockeyapp/android/a;->a()Ljava/io/File;

    move-result-object v3

    .line 2110
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_25

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_25

    .line 2111
    new-instance v4, Lnet/hockeyapp/android/c/f;

    invoke-direct {v4, v2}, Lnet/hockeyapp/android/c/f;-><init>(Lnet/hockeyapp/android/c/e;)V

    invoke-virtual {v3, v4}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    .line 2118
    if-eqz v3, :cond_25

    array-length v3, v3

    if-ne v3, v1, :cond_25

    move v0, v1

    .line 182
    :cond_25
    if-eqz v0, :cond_36

    .line 183
    const-string v0, "HockeyApp"

    const-string v2, "Cached..."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-direct {p0}, Lnet/hockeyapp/android/d/f;->c()V

    .line 185
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 193
    :goto_35
    return-object v0

    .line 188
    :cond_36
    const-string v0, "HockeyApp"

    const-string v1, "Downloading..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3075
    iget-object v0, v2, Lnet/hockeyapp/android/c/e;->d:Ljava/lang/String;

    .line 189
    invoke-virtual {v2}, Lnet/hockeyapp/android/c/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/d/f;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 190
    if-eqz v0, :cond_4c

    .line 191
    invoke-direct {p0}, Lnet/hockeyapp/android/d/f;->c()V

    .line 193
    :cond_4c
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_35
.end method

.method private static a(Ljava/net/URL;)Ljava/net/URLConnection;
    .registers 4

    .prologue
    .line 277
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 278
    const-string v1, "User-Agent"

    const-string v2, "HockeySDK/Android"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 281
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-gt v1, v2, :cond_1e

    .line 282
    const-string v1, "connection"

    const-string v2, "close"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_1e
    return-object v0
.end method

.method private a(Ljava/lang/Boolean;)V
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 203
    iget-object v2, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    .line 3135
    iget-object v2, v2, Lnet/hockeyapp/android/d/e;->b:Lnet/hockeyapp/android/f/b;

    .line 204
    iget-object v3, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 3140
    iput-boolean v4, v3, Lnet/hockeyapp/android/d/e;->c:Z

    .line 206
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_30

    .line 207
    iget-object v3, p0, Lnet/hockeyapp/android/d/f;->d:Landroid/graphics/Bitmap;

    iget v4, p0, Lnet/hockeyapp/android/d/f;->e:I

    .line 3166
    iget-object v5, v2, Lnet/hockeyapp/android/f/b;->c:Landroid/widget/TextView;

    iget-object v6, v2, Lnet/hockeyapp/android/f/b;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3167
    iput v4, v2, Lnet/hockeyapp/android/f/b;->d:I

    .line 3169
    if-nez v3, :cond_2c

    .line 3170
    invoke-virtual {v2, v0}, Lnet/hockeyapp/android/f/b;->a(Z)V

    .line 215
    :cond_26
    :goto_26
    iget-object v0, p0, Lnet/hockeyapp/android/d/f;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 216
    return-void

    .line 3173
    :cond_2c
    invoke-virtual {v2, v3, v0}, Lnet/hockeyapp/android/f/b;->a(Landroid/graphics/Bitmap;Z)V

    goto :goto_26

    .line 210
    :cond_30
    iget-object v3, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    .line 4143
    iget v3, v3, Lnet/hockeyapp/android/d/e;->d:I

    if-lez v3, :cond_40

    .line 210
    :goto_36
    if-nez v0, :cond_26

    .line 4178
    iget-object v0, v2, Lnet/hockeyapp/android/f/b;->c:Landroid/widget/TextView;

    const-string v2, "Error"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_26

    :cond_40
    move v0, v1

    .line 4143
    goto :goto_36
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 19

    .prologue
    .line 239
    :try_start_0
    new-instance v2, Ljava/net/URL;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 6277
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 6278
    const-string v3, "User-Agent"

    const-string v4, "HockeySDK/Android"

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 6279
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 6281
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x9

    if-gt v3, v4, :cond_25

    .line 6282
    const-string v3, "connection"

    const-string v4, "close"

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_25
    invoke-virtual {v2}, Ljava/net/URLConnection;->connect()V

    .line 243
    invoke-virtual {v2}, Ljava/net/URLConnection;->getContentLength()I

    move-result v4

    .line 244
    const-string v3, "Status"

    invoke-virtual {v2, v3}, Ljava/net/URLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 246
    if-eqz v3, :cond_3e

    .line 247
    const-string v5, "200"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3e

    .line 248
    const/4 v2, 0x0

    .line 272
    :goto_3d
    return v2

    .line 252
    :cond_3e
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/hockeyapp/android/d/f;->c:Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v3, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 253
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 254
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 256
    const/16 v2, 0x400

    new-array v7, v2, [B

    .line 258
    const-wide/16 v2, 0x0

    .line 259
    :goto_5d
    invoke-virtual {v5, v7}, Ljava/io/InputStream;->read([B)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_86

    .line 260
    int-to-long v10, v8

    add-long/2addr v2, v10

    .line 261
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Integer;

    const/4 v10, 0x0

    const-wide/16 v12, 0x64

    mul-long/2addr v12, v2

    int-to-long v14, v4

    div-long/2addr v12, v14

    long-to-int v11, v12

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lnet/hockeyapp/android/d/f;->publishProgress([Ljava/lang/Object;)V

    .line 262
    const/4 v9, 0x0

    invoke-virtual {v6, v7, v9, v8}, Ljava/io/OutputStream;->write([BII)V
    :try_end_7f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7f} :catch_80

    goto :goto_5d

    .line 271
    :catch_80
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 272
    const/4 v2, 0x0

    goto :goto_3d

    .line 265
    :cond_86
    :try_start_86
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V

    .line 266
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 267
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_8f
    .catch Ljava/lang/Exception; {:try_start_86 .. :try_end_8f} :catch_80

    .line 268
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_97

    const/4 v2, 0x1

    goto :goto_3d

    :cond_97
    const/4 v2, 0x0

    goto :goto_3d
.end method

.method private static varargs b()V
    .registers 0

    .prologue
    .line 199
    return-void
.end method

.method private c()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 220
    :try_start_1
    iget-object v0, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    .line 5131
    iget-object v0, v0, Lnet/hockeyapp/android/d/e;->a:Lnet/hockeyapp/android/c/e;

    .line 220
    invoke-virtual {v0}, Lnet/hockeyapp/android/c/e;->a()Ljava/lang/String;

    move-result-object v2

    .line 221
    iget-object v0, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    .line 5135
    iget-object v3, v0, Lnet/hockeyapp/android/d/e;->b:Lnet/hockeyapp/android/f/b;

    .line 223
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lnet/hockeyapp/android/d/f;->c:Ljava/io/File;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lnet/hockeyapp/android/e/m;->a(Ljava/io/File;)I

    move-result v0

    iput v0, p0, Lnet/hockeyapp/android/d/f;->e:I

    .line 224
    iget v0, p0, Lnet/hockeyapp/android/d/f;->e:I

    if-ne v0, v4, :cond_55

    .line 225
    invoke-virtual {v3}, Lnet/hockeyapp/android/f/b;->getWidthLandscape()I

    move-result v0

    move v1, v0

    .line 226
    :goto_23
    iget v0, p0, Lnet/hockeyapp/android/d/f;->e:I

    if-ne v0, v4, :cond_5b

    .line 227
    invoke-virtual {v3}, Lnet/hockeyapp/android/f/b;->getMaxHeightLandscape()I

    move-result v0

    .line 229
    :goto_2b
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lnet/hockeyapp/android/d/f;->c:Ljava/io/File;

    invoke-direct {v3, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 6126
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 6127
    const/4 v4, 0x1

    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 6129
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 6132
    invoke-static {v2, v1, v0}, Lnet/hockeyapp/android/e/m;->a(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v0

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 6135
    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 6136
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 229
    iput-object v0, p0, Lnet/hockeyapp/android/d/f;->d:Landroid/graphics/Bitmap;

    .line 235
    :goto_54
    return-void

    .line 225
    :cond_55
    invoke-virtual {v3}, Lnet/hockeyapp/android/f/b;->getWidthPortrait()I

    move-result v0

    move v1, v0

    goto :goto_23

    .line 227
    :cond_5b
    invoke-virtual {v3}, Lnet/hockeyapp/android/f/b;->getMaxHeightPortrait()I
    :try_end_5e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_5e} :catch_60

    move-result v0

    goto :goto_2b

    .line 232
    :catch_60
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/d/f;->d:Landroid/graphics/Bitmap;

    goto :goto_54
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 154
    invoke-direct {p0}, Lnet/hockeyapp/android/d/f;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 154
    check-cast p1, Ljava/lang/Boolean;

    .line 7203
    iget-object v2, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    .line 8135
    iget-object v2, v2, Lnet/hockeyapp/android/d/e;->b:Lnet/hockeyapp/android/f/b;

    .line 7204
    iget-object v3, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 8140
    iput-boolean v4, v3, Lnet/hockeyapp/android/d/e;->c:Z

    .line 7206
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_32

    .line 7207
    iget-object v3, p0, Lnet/hockeyapp/android/d/f;->d:Landroid/graphics/Bitmap;

    iget v4, p0, Lnet/hockeyapp/android/d/f;->e:I

    .line 8166
    iget-object v5, v2, Lnet/hockeyapp/android/f/b;->c:Landroid/widget/TextView;

    iget-object v6, v2, Lnet/hockeyapp/android/f/b;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8167
    iput v4, v2, Lnet/hockeyapp/android/f/b;->d:I

    .line 8169
    if-nez v3, :cond_2e

    .line 8170
    invoke-virtual {v2, v0}, Lnet/hockeyapp/android/f/b;->a(Z)V

    .line 7215
    :cond_28
    :goto_28
    iget-object v0, p0, Lnet/hockeyapp/android/d/f;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 154
    return-void

    .line 8173
    :cond_2e
    invoke-virtual {v2, v3, v0}, Lnet/hockeyapp/android/f/b;->a(Landroid/graphics/Bitmap;Z)V

    goto :goto_28

    .line 7210
    :cond_32
    iget-object v3, p0, Lnet/hockeyapp/android/d/f;->a:Lnet/hockeyapp/android/d/e;

    .line 9143
    iget v3, v3, Lnet/hockeyapp/android/d/e;->d:I

    if-lez v3, :cond_42

    .line 7210
    :goto_38
    if-nez v0, :cond_28

    .line 9178
    iget-object v0, v2, Lnet/hockeyapp/android/f/b;->c:Landroid/widget/TextView;

    const-string v2, "Error"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_28

    :cond_42
    move v0, v1

    .line 9143
    goto :goto_38
.end method

.method protected final onPreExecute()V
    .registers 1

    .prologue
    .line 176
    return-void
.end method

.method protected final bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 154
    return-void
.end method
