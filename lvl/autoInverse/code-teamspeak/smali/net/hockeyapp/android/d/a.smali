.class public final Lnet/hockeyapp/android/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/Queue;

.field b:Z


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/d/a;->a:Ljava/util/Queue;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/a;->b:Z

    .line 77
    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 56
    invoke-direct {p0}, Lnet/hockeyapp/android/d/a;-><init>()V

    return-void
.end method

.method private static synthetic a(Lnet/hockeyapp/android/d/a;)Ljava/util/Queue;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Lnet/hockeyapp/android/d/a;->a:Ljava/util/Queue;

    return-object v0
.end method

.method private a(Lnet/hockeyapp/android/c/e;Lnet/hockeyapp/android/f/b;)V
    .registers 6

    .prologue
    .line 80
    iget-object v0, p0, Lnet/hockeyapp/android/d/a;->a:Ljava/util/Queue;

    new-instance v1, Lnet/hockeyapp/android/d/e;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2}, Lnet/hockeyapp/android/d/e;-><init>(Lnet/hockeyapp/android/c/e;Lnet/hockeyapp/android/f/b;B)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-virtual {p0}, Lnet/hockeyapp/android/d/a;->a()V

    .line 82
    return-void
.end method

.method private static b()Lnet/hockeyapp/android/d/a;
    .registers 1

    .prologue
    .line 67
    sget-object v0, Lnet/hockeyapp/android/d/d;->a:Lnet/hockeyapp/android/d/a;

    return-object v0
.end method

.method private static synthetic b(Lnet/hockeyapp/android/d/a;)V
    .registers 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lnet/hockeyapp/android/d/a;->a()V

    return-void
.end method

.method private static synthetic c(Lnet/hockeyapp/android/d/a;)Z
    .registers 2

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/a;->b:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    .line 85
    iget-boolean v0, p0, Lnet/hockeyapp/android/d/a;->b:Z

    if-eqz v0, :cond_5

    .line 111
    :cond_4
    :goto_4
    return-void

    .line 89
    :cond_5
    iget-object v0, p0, Lnet/hockeyapp/android/d/a;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/d/e;

    .line 90
    if-eqz v0, :cond_4

    .line 91
    new-instance v1, Lnet/hockeyapp/android/d/f;

    new-instance v2, Lnet/hockeyapp/android/d/b;

    invoke-direct {v2, p0}, Lnet/hockeyapp/android/d/b;-><init>(Lnet/hockeyapp/android/d/a;)V

    invoke-direct {v1, v0, v2}, Lnet/hockeyapp/android/d/f;-><init>(Lnet/hockeyapp/android/d/e;Landroid/os/Handler;)V

    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/a;->b:Z

    .line 109
    invoke-static {v1}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    goto :goto_4
.end method
