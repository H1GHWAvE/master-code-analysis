.class public Lorg/xbill/DNS/utils/base64;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final Base64:Ljava/lang/String; = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static formatString([BILjava/lang/String;Z)Ljava/lang/String;
    .registers 9

    .prologue
    .line 71
    invoke-static {p0}, Lorg/xbill/DNS/utils/base64;->toString([B)Ljava/lang/String;

    move-result-object v1

    .line 72
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 73
    const/4 v0, 0x0

    :goto_a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_3a

    .line 74
    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 75
    add-int v3, v0, p1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v3, v4, :cond_2b

    .line 76
    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    if-eqz p3, :cond_29

    .line 78
    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    :cond_29
    :goto_29
    add-int/2addr v0, p1

    goto :goto_a

    .line 81
    :cond_2b
    add-int v3, v0, p1

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 82
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_29

    .line 85
    :cond_3a
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static fromString(Ljava/lang/String;)[B
    .registers 15

    .prologue
    const/4 v2, 0x0

    const/4 v13, 0x3

    const/4 v1, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x2

    .line 96
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 97
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    move v0, v1

    .line 98
    :goto_f
    array-length v5, v3

    if-ge v0, v5, :cond_23

    .line 99
    aget-byte v5, v3, v0

    int-to-char v5, v5

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v5

    if-nez v5, :cond_20

    .line 100
    aget-byte v5, v3, v0

    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 98
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 102
    :cond_23
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    .line 103
    array-length v0, v5

    rem-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2e

    move-object v0, v2

    .line 142
    :goto_2d
    return-object v0

    .line 107
    :cond_2e
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 108
    new-instance v6, Ljava/io/DataOutputStream;

    invoke-direct {v6, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move v0, v1

    .line 110
    :goto_37
    array-length v3, v5

    add-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x4

    if-ge v0, v3, :cond_c4

    .line 111
    const/4 v3, 0x4

    new-array v7, v3, [S

    .line 112
    new-array v8, v13, [S

    move v3, v1

    .line 114
    :goto_44
    const/4 v9, 0x4

    if-ge v3, v9, :cond_58

    .line 115
    const-string v9, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="

    mul-int/lit8 v10, v0, 0x4

    add-int/2addr v10, v3

    aget-byte v10, v5, v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    int-to-short v9, v9

    aput-short v9, v7, v3

    .line 114
    add-int/lit8 v3, v3, 0x1

    goto :goto_44

    .line 117
    :cond_58
    aget-short v3, v7, v1

    shl-int/lit8 v3, v3, 0x2

    aget-short v9, v7, v12

    shr-int/lit8 v9, v9, 0x4

    add-int/2addr v3, v9

    int-to-short v3, v3

    aput-short v3, v8, v1

    .line 118
    aget-short v3, v7, v11

    const/16 v9, 0x40

    if-ne v3, v9, :cond_77

    .line 119
    const/4 v3, -0x1

    aput-short v3, v8, v11

    aput-short v3, v8, v12

    .line 120
    aget-short v3, v7, v12

    and-int/lit8 v3, v3, 0xf

    if-eqz v3, :cond_b0

    move-object v0, v2

    .line 121
    goto :goto_2d

    .line 123
    :cond_77
    aget-short v3, v7, v13

    const/16 v9, 0x40

    if-ne v3, v9, :cond_96

    .line 124
    aget-short v3, v7, v12

    shl-int/lit8 v3, v3, 0x4

    aget-short v9, v7, v11

    shr-int/lit8 v9, v9, 0x2

    add-int/2addr v3, v9

    and-int/lit16 v3, v3, 0xff

    int-to-short v3, v3

    aput-short v3, v8, v12

    .line 125
    const/4 v3, -0x1

    aput-short v3, v8, v11

    .line 126
    aget-short v3, v7, v11

    and-int/lit8 v3, v3, 0x3

    if-eqz v3, :cond_b0

    move-object v0, v2

    .line 127
    goto :goto_2d

    .line 130
    :cond_96
    aget-short v3, v7, v12

    shl-int/lit8 v3, v3, 0x4

    aget-short v9, v7, v11

    shr-int/lit8 v9, v9, 0x2

    add-int/2addr v3, v9

    and-int/lit16 v3, v3, 0xff

    int-to-short v3, v3

    aput-short v3, v8, v12

    .line 131
    aget-short v3, v7, v11

    shl-int/lit8 v3, v3, 0x6

    aget-short v7, v7, v13

    add-int/2addr v3, v7

    and-int/lit16 v3, v3, 0xff

    int-to-short v3, v3

    aput-short v3, v8, v11

    :cond_b0
    move v3, v1

    .line 135
    :goto_b1
    if-ge v3, v13, :cond_c0

    .line 136
    :try_start_b3
    aget-short v7, v8, v3

    if-ltz v7, :cond_bc

    .line 137
    aget-short v7, v8, v3

    invoke-virtual {v6, v7}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_bc
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_bc} :catch_bf

    .line 135
    :cond_bc
    add-int/lit8 v3, v3, 0x1

    goto :goto_b1

    :catch_bf
    move-exception v3

    .line 110
    :cond_c0
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_37

    .line 142
    :cond_c4
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto/16 :goto_2d
.end method

.method public static toString([B)Ljava/lang/String;
    .registers 13

    .prologue
    const/4 v11, -0x1

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 28
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move v0, v1

    .line 30
    :goto_b
    array-length v2, p0

    add-int/lit8 v2, v2, 0x2

    div-int/lit8 v2, v2, 0x3

    if-ge v0, v2, :cond_9b

    .line 31
    new-array v4, v10, [S

    .line 32
    const/4 v2, 0x4

    new-array v5, v2, [S

    move v2, v1

    .line 33
    :goto_18
    if-ge v2, v10, :cond_30

    .line 34
    mul-int/lit8 v6, v0, 0x3

    add-int/2addr v6, v2

    array-length v7, p0

    if-ge v6, v7, :cond_2d

    .line 35
    mul-int/lit8 v6, v0, 0x3

    add-int/2addr v6, v2

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    int-to-short v6, v6

    aput-short v6, v4, v2

    .line 33
    :goto_2a
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 37
    :cond_2d
    aput-short v11, v4, v2

    goto :goto_2a

    .line 40
    :cond_30
    aget-short v2, v4, v1

    shr-int/lit8 v2, v2, 0x2

    int-to-short v2, v2

    aput-short v2, v5, v1

    .line 41
    aget-short v2, v4, v8

    if-ne v2, v11, :cond_60

    .line 42
    aget-short v2, v4, v1

    and-int/lit8 v2, v2, 0x3

    shl-int/lit8 v2, v2, 0x4

    int-to-short v2, v2

    aput-short v2, v5, v8

    .line 45
    :goto_44
    aget-short v2, v4, v8

    if-ne v2, v11, :cond_6f

    .line 46
    const/16 v2, 0x40

    aput-short v2, v5, v10

    aput-short v2, v5, v9

    :goto_4e
    move v2, v1

    .line 55
    :goto_4f
    const/4 v4, 0x4

    if-ge v2, v4, :cond_97

    .line 56
    const-string v4, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="

    aget-short v6, v5, v2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 55
    add-int/lit8 v2, v2, 0x1

    goto :goto_4f

    .line 44
    :cond_60
    aget-short v2, v4, v1

    and-int/lit8 v2, v2, 0x3

    shl-int/lit8 v2, v2, 0x4

    aget-short v6, v4, v8

    shr-int/lit8 v6, v6, 0x4

    add-int/2addr v2, v6

    int-to-short v2, v2

    aput-short v2, v5, v8

    goto :goto_44

    .line 47
    :cond_6f
    aget-short v2, v4, v9

    if-ne v2, v11, :cond_81

    .line 48
    aget-short v2, v4, v8

    and-int/lit8 v2, v2, 0xf

    shl-int/lit8 v2, v2, 0x2

    int-to-short v2, v2

    aput-short v2, v5, v9

    .line 49
    const/16 v2, 0x40

    aput-short v2, v5, v10

    goto :goto_4e

    .line 52
    :cond_81
    aget-short v2, v4, v8

    and-int/lit8 v2, v2, 0xf

    shl-int/lit8 v2, v2, 0x2

    aget-short v6, v4, v9

    shr-int/lit8 v6, v6, 0x6

    add-int/2addr v2, v6

    int-to-short v2, v2

    aput-short v2, v5, v9

    .line 53
    aget-short v2, v4, v9

    and-int/lit8 v2, v2, 0x3f

    int-to-short v2, v2

    aput-short v2, v5, v10

    goto :goto_4e

    .line 30
    :cond_97
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_b

    .line 58
    :cond_9b
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method
