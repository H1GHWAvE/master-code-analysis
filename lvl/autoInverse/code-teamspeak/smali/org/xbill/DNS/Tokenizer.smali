.class public Lorg/xbill/DNS/Tokenizer;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final COMMENT:I = 0x5

.field public static final EOF:I = 0x0

.field public static final EOL:I = 0x1

.field public static final IDENTIFIER:I = 0x3

.field public static final QUOTED_STRING:I = 0x4

.field public static final WHITESPACE:I = 0x2

.field private static delim:Ljava/lang/String;

.field private static quotes:Ljava/lang/String;


# instance fields
.field private current:Lorg/xbill/DNS/Tokenizer$Token;

.field private delimiters:Ljava/lang/String;

.field private filename:Ljava/lang/String;

.field private is:Ljava/io/PushbackInputStream;

.field private line:I

.field private multiline:I

.field private quoting:Z

.field private sb:Ljava/lang/StringBuffer;

.field private ungottenToken:Z

.field private wantClose:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 34
    const-string v0, " \t\n;()\""

    sput-object v0, Lorg/xbill/DNS/Tokenizer;->delim:Ljava/lang/String;

    .line 35
    const-string v0, "\""

    sput-object v0, Lorg/xbill/DNS/Tokenizer;->quotes:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .registers 3

    .prologue
    .line 175
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;-><init>(Ljava/io/InputStream;)V

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/xbill/DNS/Tokenizer;->wantClose:Z

    .line 177
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer;->filename:Ljava/lang/String;

    .line 178
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    instance-of v0, p1, Ljava/io/BufferedInputStream;

    if-nez v0, :cond_e

    .line 148
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p1, v0

    .line 149
    :cond_e
    new-instance v0, Ljava/io/PushbackInputStream;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer;->is:Ljava/io/PushbackInputStream;

    .line 150
    iput-boolean v2, p0, Lorg/xbill/DNS/Tokenizer;->ungottenToken:Z

    .line 151
    iput v2, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    .line 152
    iput-boolean v2, p0, Lorg/xbill/DNS/Tokenizer;->quoting:Z

    .line 153
    sget-object v0, Lorg/xbill/DNS/Tokenizer;->delim:Ljava/lang/String;

    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer;->delimiters:Ljava/lang/String;

    .line 154
    new-instance v0, Lorg/xbill/DNS/Tokenizer$Token;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/xbill/DNS/Tokenizer$Token;-><init>(Lorg/xbill/DNS/Tokenizer$1;)V

    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    .line 155
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    .line 156
    const-string v0, "<none>"

    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer;->filename:Ljava/lang/String;

    .line 157
    const/4 v0, 0x1

    iput v0, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    .line 158
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 166
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;-><init>(Ljava/io/InputStream;)V

    .line 167
    return-void
.end method

.method private _getIdentifier(Ljava/lang/String;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 381
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 382
    iget v1, v0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1d

    .line 383
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "expected "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 384
    :cond_1d
    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    return-object v0
.end method

.method private checkUnbalancedParens()V
    .registers 2

    .prologue
    .line 220
    iget v0, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    if-lez v0, :cond_b

    .line 221
    const-string v0, "unbalanced parentheses"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 222
    :cond_b
    return-void
.end method

.method private getChar()I
    .registers 4

    .prologue
    const/16 v1, 0xa

    .line 182
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->is:Ljava/io/PushbackInputStream;

    invoke-virtual {v0}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 183
    const/16 v2, 0xd

    if-ne v0, v2, :cond_1a

    .line 184
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->is:Ljava/io/PushbackInputStream;

    invoke-virtual {v0}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 185
    if-eq v0, v1, :cond_19

    .line 186
    iget-object v2, p0, Lorg/xbill/DNS/Tokenizer;->is:Ljava/io/PushbackInputStream;

    invoke-virtual {v2, v0}, Ljava/io/PushbackInputStream;->unread(I)V

    :cond_19
    move v0, v1

    .line 189
    :cond_1a
    if-ne v0, v1, :cond_22

    .line 190
    iget v1, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    .line 191
    :cond_22
    return v0
.end method

.method private remainingStrings()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 581
    move-object v0, v1

    .line 583
    :goto_2
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v2

    .line 584
    invoke-virtual {v2}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 586
    if-nez v0, :cond_13

    .line 587
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 588
    :cond_13
    iget-object v2, v2, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 590
    :cond_19
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->unget()V

    .line 591
    if-nez v0, :cond_1f

    .line 593
    :goto_1e
    return-object v1

    :cond_1f
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1e
.end method

.method private skipWhitespace()I
    .registers 4

    .prologue
    .line 205
    const/4 v0, 0x0

    .line 207
    :goto_1
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->getChar()I

    move-result v1

    .line 208
    const/16 v2, 0x20

    if-eq v1, v2, :cond_19

    const/16 v2, 0x9

    if-eq v1, v2, :cond_19

    .line 209
    const/16 v2, 0xa

    if-ne v1, v2, :cond_15

    iget v2, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    if-gtz v2, :cond_19

    .line 210
    :cond_15
    invoke-direct {p0, v1}, Lorg/xbill/DNS/Tokenizer;->ungetChar(I)V

    .line 211
    return v0

    .line 214
    :cond_19
    add-int/lit8 v0, v0, 0x1

    .line 215
    goto :goto_1
.end method

.method private ungetChar(I)V
    .registers 3

    .prologue
    .line 196
    const/4 v0, -0x1

    if-ne p1, v0, :cond_4

    .line 201
    :cond_3
    :goto_3
    return-void

    .line 198
    :cond_4
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->is:Ljava/io/PushbackInputStream;

    invoke-virtual {v0, p1}, Ljava/io/PushbackInputStream;->unread(I)V

    .line 199
    const/16 v0, 0xa

    if-ne p1, v0, :cond_3

    .line 200
    iget v0, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    goto :goto_3
.end method


# virtual methods
.method public close()V
    .registers 2

    .prologue
    .line 718
    iget-boolean v0, p0, Lorg/xbill/DNS/Tokenizer;->wantClose:Z

    if-eqz v0, :cond_9

    .line 720
    :try_start_4
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->is:Ljava/io/PushbackInputStream;

    invoke-virtual {v0}, Ljava/io/PushbackInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_9} :catch_a

    .line 725
    :cond_9
    :goto_9
    return-void

    :catch_a
    move-exception v0

    goto :goto_9
.end method

.method public exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
    .registers 5

    .prologue
    .line 710
    new-instance v0, Lorg/xbill/DNS/Tokenizer$TokenizerException;

    iget-object v1, p0, Lorg/xbill/DNS/Tokenizer;->filename:Ljava/lang/String;

    iget v2, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    invoke-direct {v0, v1, v2, p1}, Lorg/xbill/DNS/Tokenizer$TokenizerException;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method

.method protected finalize()V
    .registers 1

    .prologue
    .line 729
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->close()V

    .line 730
    return-void
.end method

.method public get()Lorg/xbill/DNS/Tokenizer$Token;
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 346
    invoke-virtual {p0, v0, v0}, Lorg/xbill/DNS/Tokenizer;->get(ZZ)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    return-object v0
.end method

.method public get(ZZ)Lorg/xbill/DNS/Tokenizer$Token;
    .registers 12

    .prologue
    const/4 v1, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 238
    iget-boolean v0, p0, Lorg/xbill/DNS/Tokenizer;->ungottenToken:Z

    if-eqz v0, :cond_32

    .line 239
    iput-boolean v5, p0, Lorg/xbill/DNS/Tokenizer;->ungottenToken:Z

    .line 240
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    iget v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_17

    .line 241
    if-eqz p1, :cond_32

    .line 242
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    .line 335
    :goto_16
    return-object v0

    .line 243
    :cond_17
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    iget v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_23

    .line 244
    if-eqz p2, :cond_32

    .line 245
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    goto :goto_16

    .line 247
    :cond_23
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    iget v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    if-ne v0, v8, :cond_2f

    .line 248
    iget v0, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    .line 249
    :cond_2f
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    goto :goto_16

    .line 252
    :cond_32
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->skipWhitespace()I

    move-result v0

    .line 253
    if-lez v0, :cond_42

    if-eqz p1, :cond_42

    .line 254
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    const/4 v1, 0x2

    invoke-static {v0, v1, v7}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto :goto_16

    .line 255
    :cond_42
    const/4 v0, 0x3

    .line 256
    iget-object v2, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 258
    :goto_48
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->getChar()I

    move-result v2

    .line 259
    if-eq v2, v6, :cond_56

    iget-object v3, p0, Lorg/xbill/DNS/Tokenizer;->delimiters:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v6, :cond_13a

    .line 260
    :cond_56
    if-ne v2, v6, :cond_7b

    .line 261
    iget-boolean v1, p0, Lorg/xbill/DNS/Tokenizer;->quoting:Z

    if-eqz v1, :cond_63

    .line 262
    const-string v0, "EOF in quoted string"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 264
    :cond_63
    iget-object v1, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-nez v1, :cond_72

    .line 265
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    invoke-static {v0, v5, v7}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto :goto_16

    .line 267
    :cond_72
    iget-object v1, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    iget-object v2, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    invoke-static {v1, v0, v2}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto :goto_16

    .line 269
    :cond_7b
    iget-object v3, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-nez v3, :cond_122

    if-eq v0, v1, :cond_122

    .line 270
    const/16 v3, 0x28

    if-ne v2, v3, :cond_93

    .line 271
    iget v2, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    .line 272
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->skipWhitespace()I

    goto :goto_48

    .line 274
    :cond_93
    const/16 v3, 0x29

    if-ne v2, v3, :cond_ac

    .line 275
    iget v2, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    if-gtz v2, :cond_a2

    .line 276
    const-string v0, "invalid close parenthesis"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 279
    :cond_a2
    iget v2, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    .line 280
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->skipWhitespace()I

    goto :goto_48

    .line 282
    :cond_ac
    const/16 v3, 0x22

    if-ne v2, v3, :cond_c6

    .line 283
    iget-boolean v2, p0, Lorg/xbill/DNS/Tokenizer;->quoting:Z

    if-nez v2, :cond_bc

    .line 284
    iput-boolean v8, p0, Lorg/xbill/DNS/Tokenizer;->quoting:Z

    .line 285
    sget-object v0, Lorg/xbill/DNS/Tokenizer;->quotes:Ljava/lang/String;

    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer;->delimiters:Ljava/lang/String;

    move v0, v1

    .line 286
    goto :goto_48

    .line 288
    :cond_bc
    iput-boolean v5, p0, Lorg/xbill/DNS/Tokenizer;->quoting:Z

    .line 289
    sget-object v2, Lorg/xbill/DNS/Tokenizer;->delim:Ljava/lang/String;

    iput-object v2, p0, Lorg/xbill/DNS/Tokenizer;->delimiters:Ljava/lang/String;

    .line 290
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->skipWhitespace()I

    goto :goto_48

    .line 293
    :cond_c6
    const/16 v3, 0xa

    if-ne v2, v3, :cond_d2

    .line 294
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    invoke-static {v0, v8, v7}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto/16 :goto_16

    .line 295
    :cond_d2
    const/16 v3, 0x3b

    if-ne v2, v3, :cond_11c

    .line 297
    :goto_d6
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->getChar()I

    move-result v2

    .line 298
    const/16 v3, 0xa

    if-eq v2, v3, :cond_e7

    if-eq v2, v6, :cond_e7

    .line 300
    iget-object v3, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    int-to-char v2, v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_d6

    .line 302
    :cond_e7
    if-eqz p2, :cond_f7

    .line 303
    invoke-direct {p0, v2}, Lorg/xbill/DNS/Tokenizer;->ungetChar(I)V

    .line 304
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    const/4 v1, 0x5

    iget-object v2, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    invoke-static {v0, v1, v2}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto/16 :goto_16

    .line 305
    :cond_f7
    if-ne v2, v6, :cond_106

    if-eq v0, v1, :cond_106

    .line 308
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->checkUnbalancedParens()V

    .line 309
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    invoke-static {v0, v5, v7}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto/16 :goto_16

    .line 310
    :cond_106
    iget v2, p0, Lorg/xbill/DNS/Tokenizer;->multiline:I

    if-lez v2, :cond_114

    .line 311
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->skipWhitespace()I

    .line 312
    iget-object v2, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    goto/16 :goto_48

    .line 315
    :cond_114
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    invoke-static {v0, v8, v7}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto/16 :goto_16

    .line 317
    :cond_11c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 319
    :cond_122
    invoke-direct {p0, v2}, Lorg/xbill/DNS/Tokenizer;->ungetChar(I)V

    .line 331
    iget-object v2, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-nez v2, :cond_169

    if-eq v0, v1, :cond_169

    .line 332
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->checkUnbalancedParens()V

    .line 333
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    invoke-static {v0, v5, v7}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto/16 :goto_16

    .line 321
    :cond_13a
    const/16 v3, 0x5c

    if-ne v2, v3, :cond_15a

    .line 322
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->getChar()I

    move-result v2

    .line 323
    if-ne v2, v6, :cond_14b

    .line 324
    const-string v0, "unterminated escape sequence"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 325
    :cond_14b
    iget-object v3, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    const/16 v4, 0x5c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 329
    :cond_152
    iget-object v3, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    int-to-char v2, v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_48

    .line 326
    :cond_15a
    iget-boolean v3, p0, Lorg/xbill/DNS/Tokenizer;->quoting:Z

    if-eqz v3, :cond_152

    const/16 v3, 0xa

    if-ne v2, v3, :cond_152

    .line 327
    const-string v0, "newline in quoted string"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 335
    :cond_169
    iget-object v1, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    iget-object v2, p0, Lorg/xbill/DNS/Tokenizer;->sb:Ljava/lang/StringBuffer;

    invoke-static {v1, v0, v2}, Lorg/xbill/DNS/Tokenizer$Token;->access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    goto/16 :goto_16
.end method

.method public getAddress(I)Ljava/net/InetAddress;
    .registers 3

    .prologue
    .line 554
    const-string v0, "an address"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 556
    :try_start_6
    invoke-static {v0, p1}, Lorg/xbill/DNS/Address;->getByAddress(Ljava/lang/String;I)Ljava/net/InetAddress;
    :try_end_9
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_9} :catch_b

    move-result-object v0

    return-object v0

    .line 558
    :catch_b
    move-exception v0

    .line 559
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method public getAddressBytes(I)[B
    .registers 5

    .prologue
    .line 537
    const-string v0, "an address"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 538
    invoke-static {v0, p1}, Lorg/xbill/DNS/Address;->toByteArray(Ljava/lang/String;I)[B

    move-result-object v1

    .line 539
    if-nez v1, :cond_20

    .line 540
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "Invalid address: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 541
    :cond_20
    return-object v1
.end method

.method public getBase32String(Lorg/xbill/DNS/utils/base32;)[B
    .registers 3

    .prologue
    .line 696
    const-string v0, "a base32 string"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 697
    invoke-virtual {p1, v0}, Lorg/xbill/DNS/utils/base32;->fromString(Ljava/lang/String;)[B

    move-result-object v0

    .line 698
    if-nez v0, :cond_13

    .line 699
    const-string v0, "invalid base32 encoding"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 700
    :cond_13
    return-object v0
.end method

.method public getBase64()[B
    .registers 2

    .prologue
    .line 631
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->getBase64(Z)[B

    move-result-object v0

    return-object v0
.end method

.method public getBase64(Z)[B
    .registers 3

    .prologue
    .line 608
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->remainingStrings()Ljava/lang/String;

    move-result-object v0

    .line 609
    if-nez v0, :cond_11

    .line 610
    if-eqz p1, :cond_f

    .line 611
    const-string v0, "expected base64 encoded string"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 613
    :cond_f
    const/4 v0, 0x0

    .line 618
    :cond_10
    return-object v0

    .line 615
    :cond_11
    invoke-static {v0}, Lorg/xbill/DNS/utils/base64;->fromString(Ljava/lang/String;)[B

    move-result-object v0

    .line 616
    if-nez v0, :cond_10

    .line 617
    const-string v0, "invalid base64 encoding"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method public getEOL()V
    .registers 4

    .prologue
    .line 570
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 571
    iget v1, v0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_14

    iget v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    if-eqz v0, :cond_14

    .line 572
    const-string v0, "expected EOL or EOF"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 574
    :cond_14
    return-void
.end method

.method public getHex()[B
    .registers 2

    .prologue
    .line 669
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->getHex(Z)[B

    move-result-object v0

    return-object v0
.end method

.method public getHex(Z)[B
    .registers 3

    .prologue
    .line 646
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer;->remainingStrings()Ljava/lang/String;

    move-result-object v0

    .line 647
    if-nez v0, :cond_11

    .line 648
    if-eqz p1, :cond_f

    .line 649
    const-string v0, "expected hex encoded string"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 651
    :cond_f
    const/4 v0, 0x0

    .line 656
    :cond_10
    return-object v0

    .line 653
    :cond_11
    invoke-static {v0}, Lorg/xbill/DNS/utils/base16;->fromString(Ljava/lang/String;)[B

    move-result-object v0

    .line 654
    if-nez v0, :cond_10

    .line 655
    const-string v0, "invalid hex encoding"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method public getHexString()[B
    .registers 2

    .prologue
    .line 680
    const-string v0, "a hex string"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 681
    invoke-static {v0}, Lorg/xbill/DNS/utils/base16;->fromString(Ljava/lang/String;)[B

    move-result-object v0

    .line 682
    if-nez v0, :cond_13

    .line 683
    const-string v0, "invalid hex encoding"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 684
    :cond_13
    return-object v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .registers 2

    .prologue
    .line 396
    const-string v0, "an identifier"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLong()J
    .registers 3

    .prologue
    .line 407
    const-string v0, "an integer"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 408
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-nez v1, :cond_18

    .line 409
    const-string v0, "expected an integer"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 411
    :cond_18
    :try_start_18
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1b
    .catch Ljava/lang/NumberFormatException; {:try_start_18 .. :try_end_1b} :catch_1d

    move-result-wide v0

    return-wide v0

    .line 413
    :catch_1d
    move-exception v0

    const-string v0, "expected an integer"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method public getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
    .registers 4

    .prologue
    .line 513
    const-string v0, "a name"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 515
    :try_start_6
    invoke-static {v0, p1}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v0

    .line 516
    invoke-virtual {v0}, Lorg/xbill/DNS/Name;->isAbsolute()Z

    move-result v1

    if-nez v1, :cond_20

    .line 517
    new-instance v1, Lorg/xbill/DNS/RelativeNameException;

    invoke-direct {v1, v0}, Lorg/xbill/DNS/RelativeNameException;-><init>(Lorg/xbill/DNS/Name;)V

    throw v1
    :try_end_16
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_6 .. :try_end_16} :catch_16

    .line 520
    :catch_16
    move-exception v0

    .line 521
    invoke-virtual {v0}, Lorg/xbill/DNS/TextParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 518
    :cond_20
    return-object v0
.end method

.method public getString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 372
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v1

    if-nez v1, :cond_11

    .line 374
    const-string v0, "expected a string"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 376
    :cond_11
    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    return-object v0
.end method

.method public getTTL()J
    .registers 3

    .prologue
    .line 474
    const-string v0, "a TTL value"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 476
    :try_start_6
    invoke-static {v0}, Lorg/xbill/DNS/TTL;->parseTTL(Ljava/lang/String;)J
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_9} :catch_b

    move-result-wide v0

    return-wide v0

    .line 479
    :catch_b
    move-exception v0

    const-string v0, "expected a TTL value"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method public getTTLLike()J
    .registers 3

    .prologue
    .line 492
    const-string v0, "a TTL-like value"

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Tokenizer;->_getIdentifier(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 494
    const/4 v1, 0x0

    :try_start_7
    invoke-static {v0, v1}, Lorg/xbill/DNS/TTL;->parse(Ljava/lang/String;Z)J
    :try_end_a
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_a} :catch_c

    move-result-wide v0

    return-wide v0

    .line 497
    :catch_c
    move-exception v0

    const-string v0, "expected a TTL-like value"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method public getUInt16()I
    .registers 5

    .prologue
    .line 443
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->getLong()J

    move-result-wide v0

    .line 444
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_11

    const-wide/32 v2, 0xffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_18

    .line 445
    :cond_11
    const-string v0, "expected an 16 bit unsigned integer"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 446
    :cond_18
    long-to-int v0, v0

    return v0
.end method

.method public getUInt32()J
    .registers 5

    .prologue
    .line 427
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->getLong()J

    move-result-wide v0

    .line 428
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_13

    const-wide v2, 0xffffffffL

    cmp-long v2, v0, v2

    if-lez v2, :cond_1a

    .line 429
    :cond_13
    const-string v0, "expected an 32 bit unsigned integer"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 430
    :cond_1a
    return-wide v0
.end method

.method public getUInt8()I
    .registers 5

    .prologue
    .line 459
    invoke-virtual {p0}, Lorg/xbill/DNS/Tokenizer;->getLong()J

    move-result-wide v0

    .line 460
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_10

    const-wide/16 v2, 0xff

    cmp-long v2, v0, v2

    if-lez v2, :cond_17

    .line 461
    :cond_10
    const-string v0, "expected an 8 bit unsigned integer"

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 462
    :cond_17
    long-to-int v0, v0

    return v0
.end method

.method public unget()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 356
    iget-boolean v0, p0, Lorg/xbill/DNS/Tokenizer;->ungottenToken:Z

    if-eqz v0, :cond_d

    .line 357
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot unget multiple tokens"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_d
    iget-object v0, p0, Lorg/xbill/DNS/Tokenizer;->current:Lorg/xbill/DNS/Tokenizer$Token;

    iget v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    if-ne v0, v1, :cond_19

    .line 360
    iget v0, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/xbill/DNS/Tokenizer;->line:I

    .line 361
    :cond_19
    iput-boolean v1, p0, Lorg/xbill/DNS/Tokenizer;->ungottenToken:Z

    .line 362
    return-void
.end method
