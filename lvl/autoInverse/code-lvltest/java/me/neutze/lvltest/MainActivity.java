package me.neutze.lvltest;
public class MainActivity extends android.app.Activity {
    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo/iY2nt0OvE0DsOj0TBg+ue1s7NDjyzL+XXV8bz4EaWWIayFHFaMjJ6yX/XsL1uLpE6WqvVFrFwjcnGx4uP4YHpE3eTKMkV4ubMdUCEKkGu4t4NxnS46rOdEn8kouLx2katPF2HN5padsDgDj+51p0CEDewG3A2QST+EQK/fhds0TTP/nOgv8Qm8P7l0iayf6KbzY8E8oI422Ep7xqz1uFxmQn0VXETZnjcTh6SaA6k67lxk+vVHwDB44zgFp9HBGRAZC5gQEji483AyQCKs5z9WaFo2StEDiyeI11v+LT/bTEVhZZAGQHdR8nuJxh1Zfw6anuHcTtTfsuNzU7wn/wIDAQAB";
    private static final byte[] SALT;
    private String android_id;
    private String deviceId;
    private android.widget.Button mCheckLicenseButton;
    private com.google.android.vending.licensing.LicenseChecker mChecker;
    private android.os.Handler mHandler;
    private me.neutze.lvltest.MainActivity$MyLicenseCheckerCallback mLicenseCheckerCallback;
    private com.google.android.vending.licensing.AESObfuscator mObsfuscator;
    private android.widget.TextView mStatusText;

    static MainActivity()
    {
        byte[] v0_1 = new byte[20];
        v0_1 = {-20, 30, 50, -70, 33, -100, 32, -90, -88, 104, 12, -10, 72, -34, 115, 21, 62, 35, -12, 97};
        me.neutze.lvltest.MainActivity.SALT = v0_1;
        return;
    }

    public MainActivity()
    {
        return;
    }

    static synthetic android.os.Handler access$102(me.neutze.lvltest.MainActivity p0, android.os.Handler p1)
    {
        p0.mHandler = p1;
        return p1;
    }

    static synthetic void access$200(me.neutze.lvltest.MainActivity p0)
    {
        p0.doCheck();
        return;
    }

    static synthetic android.widget.TextView access$300(me.neutze.lvltest.MainActivity p1)
    {
        return p1.mStatusText;
    }

    static synthetic android.widget.Button access$400(me.neutze.lvltest.MainActivity p1)
    {
        return p1.mCheckLicenseButton;
    }

    static synthetic void access$500(me.neutze.lvltest.MainActivity p0, String p1)
    {
        p0.displayResult(p1);
        return;
    }

    private void displayResult(String p3)
    {
        this.mHandler.post(new me.neutze.lvltest.MainActivity$2(this, p3));
        return;
    }

    private void doCheck()
    {
        this.mCheckLicenseButton.setEnabled(0);
        this.setProgressBarIndeterminateVisibility(1);
        this.mStatusText.setText(2131099651);
        this.mChecker.checkAccess(this.mLicenseCheckerCallback);
        return;
    }

    public void onCreate(android.os.Bundle p7)
    {
        super.onCreate(p7);
        this.setContentView(2130903040);
        this.mStatusText = ((android.widget.TextView) this.findViewById(2131230720));
        this.mCheckLicenseButton = ((android.widget.Button) this.findViewById(2131230721));
        this.deviceId = android.provider.Settings$Secure.getString(this.getContentResolver(), "android_id");
        this.mObsfuscator = new com.google.android.vending.licensing.AESObfuscator(me.neutze.lvltest.MainActivity.SALT, this.getPackageName(), this.android_id);
        com.google.android.vending.licensing.ServerManagedPolicy v1_1 = new com.google.android.vending.licensing.ServerManagedPolicy(this, this.mObsfuscator);
        this.mLicenseCheckerCallback = new me.neutze.lvltest.MainActivity$MyLicenseCheckerCallback(this, 0);
        this.mChecker = new com.google.android.vending.licensing.LicenseChecker(this, v1_1, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo/iY2nt0OvE0DsOj0TBg+ue1s7NDjyzL+XXV8bz4EaWWIayFHFaMjJ6yX/XsL1uLpE6WqvVFrFwjcnGx4uP4YHpE3eTKMkV4ubMdUCEKkGu4t4NxnS46rOdEn8kouLx2katPF2HN5padsDgDj+51p0CEDewG3A2QST+EQK/fhds0TTP/nOgv8Qm8P7l0iayf6KbzY8E8oI422Ep7xqz1uFxmQn0VXETZnjcTh6SaA6k67lxk+vVHwDB44zgFp9HBGRAZC5gQEji483AyQCKs5z9WaFo2StEDiyeI11v+LT/bTEVhZZAGQHdR8nuJxh1Zfw6anuHcTtTfsuNzU7wn/wIDAQAB");
        try {
            android.util.Log.e("JOHANNES", new StringBuilder().append("getField ").append(com.google.android.vending.licensing.LicenseChecker.getField("mPolicy")).toString());
        } catch (NoSuchFieldException v0) {
            v0.printStackTrace();
        }
        this.mCheckLicenseButton.setOnClickListener(new me.neutze.lvltest.MainActivity$1(this));
        return;
    }

    protected void onDestroy()
    {
        super.onDestroy();
        this.mChecker.onDestroy();
        return;
    }
}
