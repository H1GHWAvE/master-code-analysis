package com.google.android.vending.licensing;
public class StrictPolicy implements com.google.android.vending.licensing.Policy {
    private int mLastResponse;

    public StrictPolicy()
    {
        this.mLastResponse = 291;
        return;
    }

    public boolean allowAccess()
    {
        int v0_1;
        if (this.mLastResponse != 256) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public void processServerResponse(int p1, com.google.android.vending.licensing.ResponseData p2)
    {
        this.mLastResponse = p1;
        return;
    }
}
