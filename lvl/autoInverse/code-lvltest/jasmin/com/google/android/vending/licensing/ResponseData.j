.bytecode 50.0
.class public synchronized com/google/android/vending/licensing/ResponseData
.super java/lang/Object

.field public 'extra' Ljava/lang/String;

.field public 'nonce' I

.field public 'packageName' Ljava/lang/String;

.field public 'responseCode' I

.field public 'timestamp' J

.field public 'userId' Ljava/lang/String;

.field public 'versionCode' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static parse(Ljava/lang/String;)Lcom/google/android/vending/licensing/ResponseData;
aload 0
bipush 58
invokevirtual java/lang/String/indexOf(I)I
istore 1
iconst_m1
iload 1
if_icmpne L0
ldc ""
astore 3
aload 0
astore 2
aload 3
astore 0
L1:
aload 2
ldc "|"
invokestatic java/util/regex/Pattern/quote(Ljava/lang/String;)Ljava/lang/String;
invokestatic android/text/TextUtils/split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
astore 2
aload 2
arraylength
bipush 6
if_icmpge L2
new java/lang/IllegalArgumentException
dup
ldc "Wrong number of fields."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iconst_0
iload 1
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 2
iload 1
aload 0
invokevirtual java/lang/String/length()I
if_icmplt L3
ldc ""
astore 0
L4:
goto L1
L3:
aload 0
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 0
goto L4
L2:
new com/google/android/vending/licensing/ResponseData
dup
invokespecial com/google/android/vending/licensing/ResponseData/<init>()V
astore 3
aload 3
aload 0
putfield com/google/android/vending/licensing/ResponseData/extra Ljava/lang/String;
aload 3
aload 2
iconst_0
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/google/android/vending/licensing/ResponseData/responseCode I
aload 3
aload 2
iconst_1
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/google/android/vending/licensing/ResponseData/nonce I
aload 3
aload 2
iconst_2
aaload
putfield com/google/android/vending/licensing/ResponseData/packageName Ljava/lang/String;
aload 3
aload 2
iconst_3
aaload
putfield com/google/android/vending/licensing/ResponseData/versionCode Ljava/lang/String;
aload 3
aload 2
iconst_4
aaload
putfield com/google/android/vending/licensing/ResponseData/userId Ljava/lang/String;
aload 3
aload 2
iconst_5
aaload
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/google/android/vending/licensing/ResponseData/timestamp J
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method public toString()Ljava/lang/String;
ldc "|"
bipush 6
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/google/android/vending/licensing/ResponseData/responseCode I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 0
getfield com/google/android/vending/licensing/ResponseData/nonce I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 0
getfield com/google/android/vending/licensing/ResponseData/packageName Ljava/lang/String;
aastore
dup
iconst_3
aload 0
getfield com/google/android/vending/licensing/ResponseData/versionCode Ljava/lang/String;
aastore
dup
iconst_4
aload 0
getfield com/google/android/vending/licensing/ResponseData/userId Ljava/lang/String;
aastore
dup
iconst_5
aload 0
getfield com/google/android/vending/licensing/ResponseData/timestamp J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic android/text/TextUtils/join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 6
.end method
