.bytecode 50.0
.class public abstract interface com/google/android/vending/licensing/Obfuscator
.super java/lang/Object

.method public abstract obfuscate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract unobfuscate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.throws com/google/android/vending/licensing/ValidationException
.end method
