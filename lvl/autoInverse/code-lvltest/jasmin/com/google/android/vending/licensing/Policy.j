.bytecode 50.0
.class public abstract interface com/google/android/vending/licensing/Policy
.super java/lang/Object

.field public static final 'LICENSED' I = 256


.field public static final 'NOT_LICENSED' I = 561


.field public static final 'RETRY' I = 291


.method public abstract allowAccess()Z
.end method

.method public abstract processServerResponse(ILcom/google/android/vending/licensing/ResponseData;)V
.end method
