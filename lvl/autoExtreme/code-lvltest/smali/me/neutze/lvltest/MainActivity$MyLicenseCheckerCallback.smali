.class Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Lcom/google/android/vending/licensing/LicenseCheckerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lme/neutze/lvltest/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyLicenseCheckerCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lme/neutze/lvltest/MainActivity;


# direct methods
.method private constructor <init>(Lme/neutze/lvltest/MainActivity;)V
    .registers 2

    .prologue
    .line 86
    iput-object p1, p0, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;->this$0:Lme/neutze/lvltest/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lme/neutze/lvltest/MainActivity;Lme/neutze/lvltest/MainActivity$1;)V
    .registers 3
    .param p1, "x0"    # Lme/neutze/lvltest/MainActivity;
    .param p2, "x1"    # Lme/neutze/lvltest/MainActivity$1;

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;-><init>(Lme/neutze/lvltest/MainActivity;)V

    return-void
.end method


# virtual methods
.method public allow(I)V
    .registers 4
    .param p1, "reason"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;->this$0:Lme/neutze/lvltest/MainActivity;

    invoke-virtual {v0}, Lme/neutze/lvltest/MainActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 95
    :goto_8
    return-void

    .line 93
    :cond_9
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;->this$0:Lme/neutze/lvltest/MainActivity;

    const-string v1, "allowed"

    # invokes: Lme/neutze/lvltest/MainActivity;->displayResult(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lme/neutze/lvltest/MainActivity;->access$500(Lme/neutze/lvltest/MainActivity;Ljava/lang/String;)V

    .line 94
    const-string v0, "JOHANNES"

    const-string v1, "allow"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method

.method public applicationError(I)V
    .registers 4
    .param p1, "errorCode"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;->this$0:Lme/neutze/lvltest/MainActivity;

    const-string v1, "error"

    # invokes: Lme/neutze/lvltest/MainActivity;->displayResult(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lme/neutze/lvltest/MainActivity;->access$500(Lme/neutze/lvltest/MainActivity;Ljava/lang/String;)V

    .line 109
    const-string v0, "JOHANNES"

    const-string v1, "error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    return-void
.end method

.method public dontAllow(I)V
    .registers 4
    .param p1, "reason"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;->this$0:Lme/neutze/lvltest/MainActivity;

    invoke-virtual {v0}, Lme/neutze/lvltest/MainActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 104
    :goto_8
    return-void

    .line 102
    :cond_9
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;->this$0:Lme/neutze/lvltest/MainActivity;

    const-string v1, "not allowed"

    # invokes: Lme/neutze/lvltest/MainActivity;->displayResult(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lme/neutze/lvltest/MainActivity;->access$500(Lme/neutze/lvltest/MainActivity;Ljava/lang/String;)V

    .line 103
    const-string v0, "JOHANNES"

    const-string v1, "dontAllow"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method
