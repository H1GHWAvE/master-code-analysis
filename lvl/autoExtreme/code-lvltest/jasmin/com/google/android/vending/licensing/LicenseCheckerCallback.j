.bytecode 50.0
.class public abstract interface com/google/android/vending/licensing/LicenseCheckerCallback
.super java/lang/Object

.field public static final 'ERROR_CHECK_IN_PROGRESS' I = 4


.field public static final 'ERROR_INVALID_PACKAGE_NAME' I = 1


.field public static final 'ERROR_INVALID_PUBLIC_KEY' I = 5


.field public static final 'ERROR_MISSING_PERMISSION' I = 6


.field public static final 'ERROR_NON_MATCHING_UID' I = 2


.field public static final 'ERROR_NOT_MARKET_MANAGED' I = 3


.method public abstract allow(I)V
.end method

.method public abstract applicationError(I)V
.end method

.method public abstract dontAllow(I)V
.end method
