package com.google.android.vending.licensing;
public class ServerManagedPolicy implements com.google.android.vending.licensing.Policy {
    private static final String DEFAULT_MAX_RETRIES = "0";
    private static final String DEFAULT_RETRY_COUNT = "0";
    private static final String DEFAULT_RETRY_UNTIL = "0";
    private static final String DEFAULT_VALIDITY_TIMESTAMP = "0";
    private static final long MILLIS_PER_MINUTE = 60000;
    private static final String PREFS_FILE = "com.android.vending.licensing.ServerManagedPolicy";
    private static final String PREF_LAST_RESPONSE = "lastResponse";
    private static final String PREF_MAX_RETRIES = "maxRetries";
    private static final String PREF_RETRY_COUNT = "retryCount";
    private static final String PREF_RETRY_UNTIL = "retryUntil";
    private static final String PREF_VALIDITY_TIMESTAMP = "validityTimestamp";
    private static final String TAG = "ServerManagedPolicy";
    private int mLastResponse;
    private long mLastResponseTime;
    private long mMaxRetries;
    private com.google.android.vending.licensing.PreferenceObfuscator mPreferences;
    private long mRetryCount;
    private long mRetryUntil;
    private long mValidityTimestamp;

    public ServerManagedPolicy(android.content.Context p5, com.google.android.vending.licensing.Obfuscator p6)
    {
        this.mLastResponseTime = 0;
        this.mPreferences = new com.google.android.vending.licensing.PreferenceObfuscator(p5.getSharedPreferences("com.android.vending.licensing.ServerManagedPolicy", 0), p6);
        this.mLastResponse = Integer.parseInt(this.mPreferences.getString("lastResponse", Integer.toString(291)));
        this.mValidityTimestamp = Long.parseLong(this.mPreferences.getString("validityTimestamp", "0"));
        this.mRetryUntil = Long.parseLong(this.mPreferences.getString("retryUntil", "0"));
        this.mMaxRetries = Long.parseLong(this.mPreferences.getString("maxRetries", "0"));
        this.mRetryCount = Long.parseLong(this.mPreferences.getString("retryCount", "0"));
        return;
    }

    private java.util.Map decodeExtras(String p9)
    {
        java.util.HashMap v4_1 = new java.util.HashMap();
        try {
            String v5_6 = org.apache.http.client.utils.URLEncodedUtils.parse(new java.net.URI(new StringBuilder().append("?").append(p9).toString()), "UTF-8").iterator();
        } catch (java.net.URISyntaxException v0) {
            android.util.Log.w("ServerManagedPolicy", "Invalid syntax error while decoding extras data from server.");
            return v4_1;
        }
        while (v5_6.hasNext()) {
            org.apache.http.NameValuePair v2_1 = ((org.apache.http.NameValuePair) v5_6.next());
            v4_1.put(v2_1.getName(), v2_1.getValue());
        }
        return v4_1;
    }

    private void setLastResponse(int p4)
    {
        this.mLastResponseTime = System.currentTimeMillis();
        this.mLastResponse = p4;
        this.mPreferences.putString("lastResponse", Integer.toString(p4));
        return;
    }

    private void setMaxRetries(String p5)
    {
        try {
            Long v1 = Long.valueOf(Long.parseLong(p5));
        } catch (NumberFormatException v0) {
            android.util.Log.w("ServerManagedPolicy", "Licence retry count (GR) missing, grace period disabled");
            p5 = "0";
            v1 = Long.valueOf(0);
        }
        this.mMaxRetries = v1.longValue();
        this.mPreferences.putString("maxRetries", p5);
        return;
    }

    private void setRetryCount(long p4)
    {
        this.mRetryCount = p4;
        this.mPreferences.putString("retryCount", Long.toString(p4));
        return;
    }

    private void setRetryUntil(String p5)
    {
        try {
            Long v1 = Long.valueOf(Long.parseLong(p5));
        } catch (NumberFormatException v0) {
            android.util.Log.w("ServerManagedPolicy", "License retry timestamp (GT) missing, grace period disabled");
            p5 = "0";
            v1 = Long.valueOf(0);
        }
        this.mRetryUntil = v1.longValue();
        this.mPreferences.putString("retryUntil", p5);
        return;
    }

    private void setValidityTimestamp(String p7)
    {
        try {
            Long v1 = Long.valueOf(Long.parseLong(p7));
        } catch (NumberFormatException v0) {
            android.util.Log.w("ServerManagedPolicy", "License validity timestamp (VT) missing, caching for a minute");
            v1 = Long.valueOf((System.currentTimeMillis() + 60000));
            p7 = Long.toString(v1.longValue());
        }
        this.mValidityTimestamp = v1.longValue();
        this.mPreferences.putString("validityTimestamp", p7);
        return;
    }

    public boolean allowAccess()
    {
        int v2 = 1;
        long v0 = System.currentTimeMillis();
        if (this.mLastResponse != 256) {
            if ((this.mLastResponse == 291) && ((v0 < (this.mLastResponseTime + 60000)) && ((v0 <= this.mRetryUntil) || (this.mRetryCount <= this.mMaxRetries)))) {
                v2 = 1;
            }
        } else {
            if (v0 <= this.mValidityTimestamp) {
                v2 = 1;
            }
        }
        return v2;
    }

    public long getMaxRetries()
    {
        return this.mMaxRetries;
    }

    public long getRetryCount()
    {
        return this.mRetryCount;
    }

    public long getRetryUntil()
    {
        return this.mRetryUntil;
    }

    public long getValidityTimestamp()
    {
        return this.mValidityTimestamp;
    }

    public void processServerResponse(int p7, com.google.android.vending.licensing.ResponseData p8)
    {
        if (p7 == 291) {
            this.setRetryCount((this.mRetryCount + 1));
        } else {
            this.setRetryCount(0);
        }
        if (p7 != 256) {
            if (p7 == 561) {
                this.setValidityTimestamp("0");
                this.setRetryUntil("0");
                this.setMaxRetries("0");
            }
        } else {
            java.util.Map v0 = this.decodeExtras(p8.extra);
            this.mLastResponse = p7;
            this.setValidityTimestamp(((String) v0.get("VT")));
            this.setRetryUntil(((String) v0.get("GT")));
            this.setMaxRetries(((String) v0.get("GR")));
        }
        this.setLastResponse(p7);
        this.mPreferences.commit();
        return;
    }
}
