package com.google.android.vending.licensing;
public class APKExpansionPolicy implements com.google.android.vending.licensing.Policy {
    private static final String DEFAULT_MAX_RETRIES = "0";
    private static final String DEFAULT_RETRY_COUNT = "0";
    private static final String DEFAULT_RETRY_UNTIL = "0";
    private static final String DEFAULT_VALIDITY_TIMESTAMP = "0";
    public static final int MAIN_FILE_URL_INDEX = 0;
    private static final long MILLIS_PER_MINUTE = 60000;
    public static final int PATCH_FILE_URL_INDEX = 1;
    private static final String PREFS_FILE = "com.android.vending.licensing.APKExpansionPolicy";
    private static final String PREF_LAST_RESPONSE = "lastResponse";
    private static final String PREF_MAX_RETRIES = "maxRetries";
    private static final String PREF_RETRY_COUNT = "retryCount";
    private static final String PREF_RETRY_UNTIL = "retryUntil";
    private static final String PREF_VALIDITY_TIMESTAMP = "validityTimestamp";
    private static final String TAG = "APKExpansionPolicy";
    private java.util.Vector mExpansionFileNames;
    private java.util.Vector mExpansionFileSizes;
    private java.util.Vector mExpansionURLs;
    private int mLastResponse;
    private long mLastResponseTime;
    private long mMaxRetries;
    private com.google.android.vending.licensing.PreferenceObfuscator mPreferences;
    private long mRetryCount;
    private long mRetryUntil;
    private long mValidityTimestamp;

    public APKExpansionPolicy(android.content.Context p5, com.google.android.vending.licensing.Obfuscator p6)
    {
        this.mLastResponseTime = 0;
        this.mExpansionURLs = new java.util.Vector();
        this.mExpansionFileNames = new java.util.Vector();
        this.mExpansionFileSizes = new java.util.Vector();
        this.mPreferences = new com.google.android.vending.licensing.PreferenceObfuscator(p5.getSharedPreferences("com.android.vending.licensing.APKExpansionPolicy", 0), p6);
        this.mLastResponse = Integer.parseInt(this.mPreferences.getString("lastResponse", Integer.toString(291)));
        this.mValidityTimestamp = Long.parseLong(this.mPreferences.getString("validityTimestamp", "0"));
        this.mRetryUntil = Long.parseLong(this.mPreferences.getString("retryUntil", "0"));
        this.mMaxRetries = Long.parseLong(this.mPreferences.getString("maxRetries", "0"));
        this.mRetryCount = Long.parseLong(this.mPreferences.getString("retryCount", "0"));
        return;
    }

    private java.util.Map decodeExtras(String p11)
    {
        java.util.HashMap v6_1 = new java.util.HashMap();
        try {
            String v7_6 = org.apache.http.client.utils.URLEncodedUtils.parse(new java.net.URI(new StringBuilder().append("?").append(p11).toString()), "UTF-8").iterator();
        } catch (java.net.URISyntaxException v0) {
            android.util.Log.w("APKExpansionPolicy", "Invalid syntax error while decoding extras data from server.");
            return v6_1;
        }
        while (v7_6.hasNext()) {
            org.apache.http.NameValuePair v3_1 = ((org.apache.http.NameValuePair) v7_6.next());
            String v4 = v3_1.getName();
            int v2 = 0;
            while (v6_1.containsKey(v4)) {
                v2++;
                v4 = new StringBuilder().append(v3_1.getName()).append(v2).toString();
            }
            v6_1.put(v4, v3_1.getValue());
        }
        return v6_1;
    }

    private void setLastResponse(int p4)
    {
        this.mLastResponseTime = System.currentTimeMillis();
        this.mLastResponse = p4;
        this.mPreferences.putString("lastResponse", Integer.toString(p4));
        return;
    }

    private void setMaxRetries(String p5)
    {
        try {
            Long v1 = Long.valueOf(Long.parseLong(p5));
        } catch (NumberFormatException v0) {
            android.util.Log.w("APKExpansionPolicy", "Licence retry count (GR) missing, grace period disabled");
            p5 = "0";
            v1 = Long.valueOf(0);
        }
        this.mMaxRetries = v1.longValue();
        this.mPreferences.putString("maxRetries", p5);
        return;
    }

    private void setRetryCount(long p4)
    {
        this.mRetryCount = p4;
        this.mPreferences.putString("retryCount", Long.toString(p4));
        return;
    }

    private void setRetryUntil(String p5)
    {
        try {
            Long v1 = Long.valueOf(Long.parseLong(p5));
        } catch (NumberFormatException v0) {
            android.util.Log.w("APKExpansionPolicy", "License retry timestamp (GT) missing, grace period disabled");
            p5 = "0";
            v1 = Long.valueOf(0);
        }
        this.mRetryUntil = v1.longValue();
        this.mPreferences.putString("retryUntil", p5);
        return;
    }

    private void setValidityTimestamp(String p7)
    {
        try {
            Long v1 = Long.valueOf(Long.parseLong(p7));
        } catch (NumberFormatException v0) {
            android.util.Log.w("APKExpansionPolicy", "License validity timestamp (VT) missing, caching for a minute");
            v1 = Long.valueOf((System.currentTimeMillis() + 60000));
            p7 = Long.toString(v1.longValue());
        }
        this.mValidityTimestamp = v1.longValue();
        this.mPreferences.putString("validityTimestamp", p7);
        return;
    }

    public boolean allowAccess()
    {
        int v2 = 1;
        long v0 = System.currentTimeMillis();
        if (this.mLastResponse != 256) {
            if ((this.mLastResponse == 291) && ((v0 < (this.mLastResponseTime + 60000)) && ((v0 <= this.mRetryUntil) || (this.mRetryCount <= this.mMaxRetries)))) {
                v2 = 1;
            }
        } else {
            if (v0 <= this.mValidityTimestamp) {
                v2 = 1;
            }
        }
        return v2;
    }

    public String getExpansionFileName(int p2)
    {
        int v0_2;
        if (p2 >= this.mExpansionFileNames.size()) {
            v0_2 = 0;
        } else {
            v0_2 = ((String) this.mExpansionFileNames.elementAt(p2));
        }
        return v0_2;
    }

    public long getExpansionFileSize(int p3)
    {
        long v0_2;
        if (p3 >= this.mExpansionFileSizes.size()) {
            v0_2 = -1;
        } else {
            v0_2 = ((Long) this.mExpansionFileSizes.elementAt(p3)).longValue();
        }
        return v0_2;
    }

    public String getExpansionURL(int p2)
    {
        int v0_2;
        if (p2 >= this.mExpansionURLs.size()) {
            v0_2 = 0;
        } else {
            v0_2 = ((String) this.mExpansionURLs.elementAt(p2));
        }
        return v0_2;
    }

    public int getExpansionURLCount()
    {
        return this.mExpansionURLs.size();
    }

    public long getMaxRetries()
    {
        return this.mMaxRetries;
    }

    public long getRetryCount()
    {
        return this.mRetryCount;
    }

    public long getRetryUntil()
    {
        return this.mRetryUntil;
    }

    public long getValidityTimestamp()
    {
        return this.mValidityTimestamp;
    }

    public void processServerResponse(int p9, com.google.android.vending.licensing.ResponseData p10)
    {
        if (p9 == 291) {
            this.setRetryCount((this.mRetryCount + 1));
        } else {
            this.setRetryCount(0);
        }
        if (p9 != 256) {
            if (p9 == 561) {
                this.setValidityTimestamp("0");
                this.setRetryUntil("0");
                this.setMaxRetries("0");
            }
        } else {
            java.util.Map v0 = this.decodeExtras(p10.extra);
            this.mLastResponse = p9;
            this.setValidityTimestamp(Long.toString((System.currentTimeMillis() + 60000)));
            java.util.Iterator v5 = v0.keySet().iterator();
            while (v5.hasNext()) {
                String v2_1 = ((String) v5.next());
                if (!v2_1.equals("VT")) {
                    if (!v2_1.equals("GT")) {
                        if (!v2_1.equals("GR")) {
                            if (!v2_1.startsWith("FILE_URL")) {
                                if (!v2_1.startsWith("FILE_NAME")) {
                                    if (v2_1.startsWith("FILE_SIZE")) {
                                        this.setExpansionFileSize((Integer.parseInt(v2_1.substring("FILE_SIZE".length())) - 1), Long.parseLong(((String) v0.get(v2_1))));
                                    }
                                } else {
                                    this.setExpansionFileName((Integer.parseInt(v2_1.substring("FILE_NAME".length())) - 1), ((String) v0.get(v2_1)));
                                }
                            } else {
                                this.setExpansionURL((Integer.parseInt(v2_1.substring("FILE_URL".length())) - 1), ((String) v0.get(v2_1)));
                            }
                        } else {
                            this.setMaxRetries(((String) v0.get(v2_1)));
                        }
                    } else {
                        this.setRetryUntil(((String) v0.get(v2_1)));
                    }
                } else {
                    this.setValidityTimestamp(((String) v0.get(v2_1)));
                }
            }
        }
        this.setLastResponse(p9);
        this.mPreferences.commit();
        return;
    }

    public void resetPolicy()
    {
        this.mPreferences.putString("lastResponse", Integer.toString(291));
        this.setRetryUntil("0");
        this.setMaxRetries("0");
        this.setRetryCount(Long.parseLong("0"));
        this.setValidityTimestamp("0");
        this.mPreferences.commit();
        return;
    }

    public void setExpansionFileName(int p3, String p4)
    {
        if (p3 >= this.mExpansionFileNames.size()) {
            this.mExpansionFileNames.setSize((p3 + 1));
        }
        this.mExpansionFileNames.set(p3, p4);
        return;
    }

    public void setExpansionFileSize(int p3, long p4)
    {
        if (p3 >= this.mExpansionFileSizes.size()) {
            this.mExpansionFileSizes.setSize((p3 + 1));
        }
        this.mExpansionFileSizes.set(p3, Long.valueOf(p4));
        return;
    }

    public void setExpansionURL(int p3, String p4)
    {
        if (p3 >= this.mExpansionURLs.size()) {
            this.mExpansionURLs.setSize((p3 + 1));
        }
        this.mExpansionURLs.set(p3, p4);
        return;
    }
}
