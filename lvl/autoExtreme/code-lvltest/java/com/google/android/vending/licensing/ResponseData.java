package com.google.android.vending.licensing;
public class ResponseData {
    public String extra;
    public int nonce;
    public String packageName;
    public int responseCode;
    public long timestamp;
    public String userId;
    public String versionCode;

    public ResponseData()
    {
        return;
    }

    public static com.google.android.vending.licensing.ResponseData parse(String p8)
    {
        String v4;
        String v1;
        int v3 = p8.indexOf(58);
        if (-1 != v3) {
            v4 = p8.substring(0, v3);
            if (v3 < p8.length()) {
                v1 = p8.substring((v3 + 1));
            } else {
                v1 = "";
            }
        } else {
            v4 = p8;
            v1 = "";
        }
        String[] v2 = android.text.TextUtils.split(v4, java.util.regex.Pattern.quote("|"));
        if (v2.length >= 6) {
            com.google.android.vending.licensing.ResponseData v0_1 = new com.google.android.vending.licensing.ResponseData();
            v0_1.extra = v1;
            Integer.parseInt(v2[0]);
            v0_1.responseCode = 0;
            v0_1.nonce = Integer.parseInt(v2[1]);
            v0_1.packageName = v2[2];
            v0_1.versionCode = v2[3];
            v0_1.userId = v2[4];
            v0_1.timestamp = Long.parseLong(v2[5]);
            return v0_1;
        } else {
            throw new IllegalArgumentException("Wrong number of fields.");
        }
    }

    public String toString()
    {
        Object[] v1_1 = new Object[6];
        v1_1[0] = Integer.valueOf(this.responseCode);
        v1_1[1] = Integer.valueOf(this.nonce);
        v1_1[2] = this.packageName;
        v1_1[3] = this.versionCode;
        v1_1[4] = this.userId;
        v1_1[5] = Long.valueOf(this.timestamp);
        return android.text.TextUtils.join("|", v1_1);
    }
}
