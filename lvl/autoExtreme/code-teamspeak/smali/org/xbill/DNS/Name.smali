.class public Lorg/xbill/DNS/Name;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# static fields
.field private static final LABEL_COMPRESSION:I = 0xc0

.field private static final LABEL_MASK:I = 0xc0

.field private static final LABEL_NORMAL:I = 0x0

.field private static final MAXLABEL:I = 0x3f

.field private static final MAXLABELS:I = 0x80

.field private static final MAXNAME:I = 0xff

.field private static final MAXOFFSETS:I = 0x7

.field private static final byteFormat:Ljava/text/DecimalFormat;

.field public static final empty:Lorg/xbill/DNS/Name;

.field private static final emptyLabel:[B

.field private static final lowercase:[B

.field public static final root:Lorg/xbill/DNS/Name;

.field private static final serialVersionUID:J = -0x64b61d2fdd88b60cL

.field private static final wild:Lorg/xbill/DNS/Name;

.field private static final wildLabel:[B


# instance fields
.field private hashcode:I

.field private name:[B

.field private offsets:J


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 35
    new-array v0, v4, [B

    aput-byte v1, v0, v1

    sput-object v0, Lorg/xbill/DNS/Name;->emptyLabel:[B

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_68

    sput-object v0, Lorg/xbill/DNS/Name;->wildLabel:[B

    .line 57
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    sput-object v0, Lorg/xbill/DNS/Name;->byteFormat:Ljava/text/DecimalFormat;

    .line 60
    const/16 v0, 0x100

    new-array v0, v0, [B

    sput-object v0, Lorg/xbill/DNS/Name;->lowercase:[B

    .line 66
    sget-object v0, Lorg/xbill/DNS/Name;->byteFormat:Ljava/text/DecimalFormat;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setMinimumIntegerDigits(I)V

    move v0, v1

    .line 67
    :goto_24
    sget-object v2, Lorg/xbill/DNS/Name;->lowercase:[B

    array-length v2, v2

    if-ge v0, v2, :cond_43

    .line 68
    const/16 v2, 0x41

    if-lt v0, v2, :cond_31

    const/16 v2, 0x5a

    if-le v0, v2, :cond_39

    .line 69
    :cond_31
    sget-object v2, Lorg/xbill/DNS/Name;->lowercase:[B

    int-to-byte v3, v0

    aput-byte v3, v2, v0

    .line 67
    :goto_36
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 71
    :cond_39
    sget-object v2, Lorg/xbill/DNS/Name;->lowercase:[B

    add-int/lit8 v3, v0, -0x41

    add-int/lit8 v3, v3, 0x61

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    goto :goto_36

    .line 73
    :cond_43
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0}, Lorg/xbill/DNS/Name;-><init>()V

    .line 74
    sput-object v0, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    sget-object v2, Lorg/xbill/DNS/Name;->emptyLabel:[B

    invoke-direct {v0, v2, v1, v4}, Lorg/xbill/DNS/Name;->appendSafe([BII)V

    .line 75
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0}, Lorg/xbill/DNS/Name;-><init>()V

    .line 76
    sput-object v0, Lorg/xbill/DNS/Name;->empty:Lorg/xbill/DNS/Name;

    new-array v2, v1, [B

    iput-object v2, v0, Lorg/xbill/DNS/Name;->name:[B

    .line 77
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0}, Lorg/xbill/DNS/Name;-><init>()V

    .line 78
    sput-object v0, Lorg/xbill/DNS/Name;->wild:Lorg/xbill/DNS/Name;

    sget-object v2, Lorg/xbill/DNS/Name;->wildLabel:[B

    invoke-direct {v0, v2, v1, v4}, Lorg/xbill/DNS/Name;->appendSafe([BII)V

    .line 79
    return-void

    .line 36
    nop

    :array_68
    .array-data 1
        0x1t
        0x2at
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 288
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/xbill/DNS/Name;-><init>(Ljava/lang/String;Lorg/xbill/DNS/Name;)V

    .line 289
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/xbill/DNS/Name;)V
    .registers 14

    .prologue
    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 207
    const-string v0, "empty name"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 208
    :cond_12
    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 209
    if-nez p2, :cond_22

    .line 210
    sget-object v0, Lorg/xbill/DNS/Name;->empty:Lorg/xbill/DNS/Name;

    invoke-static {v0, p0}, Lorg/xbill/DNS/Name;->copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V

    .line 278
    :cond_21
    :goto_21
    return-void

    .line 212
    :cond_22
    invoke-static {p2, p0}, Lorg/xbill/DNS/Name;->copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V

    goto :goto_21

    .line 214
    :cond_26
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 215
    sget-object v0, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    invoke-static {v0, p0}, Lorg/xbill/DNS/Name;->copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V

    goto :goto_21

    .line 218
    :cond_34
    const/4 v5, -0x1

    .line 219
    const/4 v4, 0x1

    .line 220
    const/16 v0, 0x40

    new-array v9, v0, [B

    .line 221
    const/4 v3, 0x0

    .line 222
    const/4 v2, 0x0

    .line 223
    const/4 v1, 0x0

    .line 224
    const/4 v7, 0x0

    .line 225
    const/4 v0, 0x0

    :goto_3f
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v0, v6, :cond_c9

    .line 226
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    int-to-byte v8, v6

    .line 227
    if-eqz v3, :cond_8f

    .line 228
    const/16 v6, 0x30

    if-lt v8, v6, :cond_78

    const/16 v6, 0x39

    if-gt v8, v6, :cond_78

    const/4 v6, 0x3

    if-ge v2, v6, :cond_78

    .line 229
    add-int/lit8 v2, v2, 0x1

    .line 230
    mul-int/lit8 v1, v1, 0xa

    .line 231
    add-int/lit8 v6, v8, -0x30

    add-int/2addr v1, v6

    .line 232
    const/16 v6, 0xff

    if-le v1, v6, :cond_69

    .line 233
    const-string v0, "bad escape"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 234
    :cond_69
    const/4 v6, 0x3

    if-lt v2, v6, :cond_8c

    .line 236
    int-to-byte v3, v1

    .line 240
    :goto_6d
    const/16 v5, 0x3f

    if-le v4, v5, :cond_84

    .line 241
    const-string v0, "label too long"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 238
    :cond_78
    if-lez v2, :cond_10c

    const/4 v3, 0x3

    if-ge v2, v3, :cond_10c

    .line 239
    const-string v0, "bad escape"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 243
    :cond_84
    add-int/lit8 v5, v4, 0x1

    aput-byte v3, v9, v4

    .line 244
    const/4 v3, 0x0

    move v10, v5

    move v5, v4

    move v4, v10

    .line 225
    :cond_8c
    :goto_8c
    add-int/lit8 v0, v0, 0x1

    goto :goto_3f

    .line 245
    :cond_8f
    const/16 v6, 0x5c

    if-ne v8, v6, :cond_97

    .line 246
    const/4 v3, 0x1

    .line 247
    const/4 v2, 0x0

    .line 248
    const/4 v1, 0x0

    goto :goto_8c

    .line 249
    :cond_97
    const/16 v6, 0x2e

    if-ne v8, v6, :cond_b3

    .line 250
    const/4 v6, -0x1

    if-ne v5, v6, :cond_a5

    .line 251
    const-string v0, "invalid empty label"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 252
    :cond_a5
    const/4 v5, 0x0

    add-int/lit8 v4, v4, -0x1

    int-to-byte v4, v4

    aput-byte v4, v9, v5

    .line 253
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {p0, p1, v9, v4, v5}, Lorg/xbill/DNS/Name;->appendFromString(Ljava/lang/String;[BII)V

    .line 254
    const/4 v5, -0x1

    .line 255
    const/4 v4, 0x1

    goto :goto_8c

    .line 257
    :cond_b3
    const/4 v6, -0x1

    if-ne v5, v6, :cond_10a

    move v6, v0

    .line 259
    :goto_b7
    const/16 v5, 0x3f

    if-le v4, v5, :cond_c2

    .line 260
    const-string v0, "label too long"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 261
    :cond_c2
    add-int/lit8 v5, v4, 0x1

    aput-byte v8, v9, v4

    move v4, v5

    move v5, v6

    goto :goto_8c

    .line 264
    :cond_c9
    if-lez v2, :cond_d5

    const/4 v0, 0x3

    if-ge v2, v0, :cond_d5

    .line 265
    const-string v0, "bad escape"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 266
    :cond_d5
    if-eqz v3, :cond_de

    .line 267
    const-string v0, "bad escape"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0

    .line 268
    :cond_de
    const/4 v0, -0x1

    if-ne v5, v0, :cond_fd

    .line 269
    sget-object v0, Lorg/xbill/DNS/Name;->emptyLabel:[B

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/xbill/DNS/Name;->appendFromString(Ljava/lang/String;[BII)V

    .line 270
    const/4 v0, 0x1

    .line 275
    :goto_e9
    if-eqz p2, :cond_21

    if-nez v0, :cond_21

    .line 276
    iget-object v0, p2, Lorg/xbill/DNS/Name;->name:[B

    const/4 v1, 0x0

    invoke-direct {p2, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v1

    invoke-direct {p2}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/xbill/DNS/Name;->appendFromString(Ljava/lang/String;[BII)V

    goto/16 :goto_21

    .line 272
    :cond_fd
    const/4 v0, 0x0

    add-int/lit8 v1, v4, -0x1

    int-to-byte v1, v1

    aput-byte v1, v9, v0

    .line 273
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v9, v0, v1}, Lorg/xbill/DNS/Name;->appendFromString(Ljava/lang/String;[BII)V

    move v0, v7

    goto :goto_e9

    :cond_10a
    move v6, v5

    goto :goto_b7

    :cond_10c
    move v3, v8

    goto/16 :goto_6d
.end method

.method public constructor <init>(Lorg/xbill/DNS/DNSInput;)V
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348
    const/16 v0, 0x40

    new-array v4, v0, [B

    move v0, v2

    move v3, v2

    .line 351
    :cond_b
    :goto_b
    if-nez v3, :cond_b5

    .line 352
    invoke-virtual {p1}, Lorg/xbill/DNS/DNSInput;->readU8()I

    move-result v5

    .line 353
    and-int/lit16 v6, v5, 0xc0

    sparse-switch v6, :sswitch_data_bc

    .line 385
    new-instance v0, Lorg/xbill/DNS/WireParseException;

    const-string v1, "bad label type"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :sswitch_1e
    invoke-direct {p0}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v6

    const/16 v7, 0x80

    if-lt v6, v7, :cond_2e

    .line 356
    new-instance v0, Lorg/xbill/DNS/WireParseException;

    const-string v1, "too many labels"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_2e
    if-nez v5, :cond_37

    .line 358
    sget-object v3, Lorg/xbill/DNS/Name;->emptyLabel:[B

    invoke-direct {p0, v3, v2, v1}, Lorg/xbill/DNS/Name;->append([BII)V

    move v3, v1

    .line 359
    goto :goto_b

    .line 361
    :cond_37
    int-to-byte v6, v5

    aput-byte v6, v4, v2

    .line 362
    invoke-virtual {p1, v4, v1, v5}, Lorg/xbill/DNS/DNSInput;->readByteArray([BII)V

    .line 363
    invoke-direct {p0, v4, v2, v1}, Lorg/xbill/DNS/Name;->append([BII)V

    goto :goto_b

    .line 367
    :sswitch_41
    invoke-virtual {p1}, Lorg/xbill/DNS/DNSInput;->readU8()I

    move-result v6

    .line 368
    and-int/lit16 v5, v5, -0xc1

    shl-int/lit8 v5, v5, 0x8

    add-int/2addr v5, v6

    .line 369
    const-string v6, "verbosecompression"

    invoke-static {v6}, Lorg/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_74

    .line 370
    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    const-string v8, "currently "

    invoke-direct {v7, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/xbill/DNS/DNSInput;->current()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, ", pointer to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 373
    :cond_74
    invoke-virtual {p1}, Lorg/xbill/DNS/DNSInput;->current()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    if-lt v5, v6, :cond_84

    .line 374
    new-instance v0, Lorg/xbill/DNS/WireParseException;

    const-string v1, "bad compression"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_84
    if-nez v0, :cond_8a

    .line 376
    invoke-virtual {p1}, Lorg/xbill/DNS/DNSInput;->save()V

    move v0, v1

    .line 379
    :cond_8a
    invoke-virtual {p1, v5}, Lorg/xbill/DNS/DNSInput;->jump(I)V

    .line 380
    const-string v6, "verbosecompression"

    invoke-static {v6}, Lorg/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 381
    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    const-string v8, "current name \'"

    invoke-direct {v7, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "\', seeking to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 388
    :cond_b5
    if-eqz v0, :cond_ba

    .line 389
    invoke-virtual {p1}, Lorg/xbill/DNS/DNSInput;->restore()V

    .line 391
    :cond_ba
    return-void

    .line 353
    nop

    :sswitch_data_bc
    .sparse-switch
        0x0 -> :sswitch_1e
        0xc0 -> :sswitch_41
    .end sparse-switch
.end method

.method public constructor <init>(Lorg/xbill/DNS/Name;I)V
    .registers 6

    .prologue
    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409
    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->labels()I

    move-result v1

    .line 410
    if-le p2, v1, :cond_11

    .line 411
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "attempted to remove too many labels"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 413
    :cond_11
    iget-object v0, p1, Lorg/xbill/DNS/Name;->name:[B

    iput-object v0, p0, Lorg/xbill/DNS/Name;->name:[B

    .line 414
    sub-int v0, v1, p2

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;->setlabels(I)V

    .line 415
    const/4 v0, 0x0

    :goto_1b
    const/4 v2, 0x7

    if-ge v0, v2, :cond_2e

    sub-int v2, v1, p2

    if-ge v0, v2, :cond_2e

    .line 416
    add-int v2, v0, p2

    invoke-direct {p1, v2}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v2

    invoke-direct {p0, v0, v2}, Lorg/xbill/DNS/Name;->setoffset(II)V

    .line 415
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 417
    :cond_2e
    return-void
.end method

.method public constructor <init>([B)V
    .registers 3

    .prologue
    .line 399
    new-instance v0, Lorg/xbill/DNS/DNSInput;

    invoke-direct {v0, p1}, Lorg/xbill/DNS/DNSInput;-><init>([B)V

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;-><init>(Lorg/xbill/DNS/DNSInput;)V

    .line 400
    return-void
.end method

.method private final append([BII)V
    .registers 12

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lorg/xbill/DNS/Name;->name:[B

    if-nez v0, :cond_19

    move v0, v1

    :goto_6
    move v2, p2

    move v3, v1

    move v4, v1

    .line 143
    :goto_9
    if-ge v3, p3, :cond_29

    .line 144
    aget-byte v5, p1, v2

    .line 145
    const/16 v6, 0x3f

    if-le v5, v6, :cond_22

    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid label"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_19
    iget-object v0, p0, Lorg/xbill/DNS/Name;->name:[B

    array-length v0, v0

    invoke-direct {p0, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_6

    .line 147
    :cond_22
    add-int/lit8 v5, v5, 0x1

    .line 148
    add-int/2addr v2, v5

    .line 149
    add-int/2addr v4, v5

    .line 143
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 151
    :cond_29
    add-int v2, v0, v4

    .line 152
    const/16 v3, 0xff

    if-le v2, v3, :cond_35

    .line 153
    new-instance v0, Lorg/xbill/DNS/NameTooLongException;

    invoke-direct {v0}, Lorg/xbill/DNS/NameTooLongException;-><init>()V

    throw v0

    .line 154
    :cond_35
    invoke-direct {p0}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v3

    .line 155
    add-int v5, v3, p3

    .line 156
    const/16 v6, 0x80

    if-le v5, v6, :cond_47

    .line 157
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "too many labels"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_47
    new-array v2, v2, [B

    .line 159
    if-eqz v0, :cond_54

    .line 160
    iget-object v6, p0, Lorg/xbill/DNS/Name;->name:[B

    invoke-direct {p0, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v7

    invoke-static {v6, v7, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 161
    :cond_54
    invoke-static {p1, p2, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 162
    iput-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    .line 163
    :goto_59
    if-ge v1, p3, :cond_68

    .line 164
    add-int v4, v3, v1

    invoke-direct {p0, v4, v0}, Lorg/xbill/DNS/Name;->setoffset(II)V

    .line 165
    aget-byte v4, v2, v0

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v4

    .line 163
    add-int/lit8 v1, v1, 0x1

    goto :goto_59

    .line 167
    :cond_68
    invoke-direct {p0, v5}, Lorg/xbill/DNS/Name;->setlabels(I)V

    .line 168
    return-void
.end method

.method private final appendFromString(Ljava/lang/String;[BII)V
    .registers 6

    .prologue
    .line 180
    :try_start_0
    invoke-direct {p0, p2, p3, p4}, Lorg/xbill/DNS/Name;->append([BII)V
    :try_end_3
    .catch Lorg/xbill/DNS/NameTooLongException; {:try_start_0 .. :try_end_3} :catch_4

    .line 184
    return-void

    .line 183
    :catch_4
    move-exception v0

    const-string v0, "Name too long"

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method private final appendSafe([BII)V
    .registers 5

    .prologue
    .line 190
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lorg/xbill/DNS/Name;->append([BII)V
    :try_end_3
    .catch Lorg/xbill/DNS/NameTooLongException; {:try_start_0 .. :try_end_3} :catch_4

    .line 194
    :goto_3
    return-void

    :catch_4
    move-exception v0

    goto :goto_3
.end method

.method private byteString([BI)Ljava/lang/String;
    .registers 12

    .prologue
    const/16 v8, 0x5c

    .line 592
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 593
    add-int/lit8 v1, p2, 0x1

    aget-byte v3, p1, p2

    move v0, v1

    .line 594
    :goto_c
    add-int v4, v1, v3

    if-ge v0, v4, :cond_57

    .line 595
    aget-byte v4, p1, v0

    and-int/lit16 v4, v4, 0xff

    .line 596
    const/16 v5, 0x20

    if-le v4, v5, :cond_1c

    const/16 v5, 0x7f

    if-lt v4, v5, :cond_2c

    .line 597
    :cond_1c
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 598
    sget-object v5, Lorg/xbill/DNS/Name;->byteFormat:Ljava/text/DecimalFormat;

    int-to-long v6, v4

    invoke-virtual {v5, v6, v7}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 594
    :goto_29
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 600
    :cond_2c
    const/16 v5, 0x22

    if-eq v4, v5, :cond_4a

    const/16 v5, 0x28

    if-eq v4, v5, :cond_4a

    const/16 v5, 0x29

    if-eq v4, v5, :cond_4a

    const/16 v5, 0x2e

    if-eq v4, v5, :cond_4a

    const/16 v5, 0x3b

    if-eq v4, v5, :cond_4a

    if-eq v4, v8, :cond_4a

    const/16 v5, 0x40

    if-eq v4, v5, :cond_4a

    const/16 v5, 0x24

    if-ne v4, v5, :cond_52

    .line 603
    :cond_4a
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 604
    int-to-char v4, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_29

    .line 607
    :cond_52
    int-to-char v4, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_29

    .line 609
    :cond_57
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static concatenate(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
    .registers 6

    .prologue
    .line 428
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->isAbsolute()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 433
    :goto_6
    return-object p0

    .line 430
    :cond_7
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0}, Lorg/xbill/DNS/Name;-><init>()V

    .line 431
    invoke-static {p0, v0}, Lorg/xbill/DNS/Name;->copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V

    .line 432
    iget-object v1, p1, Lorg/xbill/DNS/Name;->name:[B

    const/4 v2, 0x0

    invoke-direct {p1, v2}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v2

    invoke-direct {p1}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/xbill/DNS/Name;->append([BII)V

    move-object p0, v0

    .line 433
    goto :goto_6
.end method

.method private static final copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 124
    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v1

    if-nez v1, :cond_10

    .line 125
    iget-object v0, p0, Lorg/xbill/DNS/Name;->name:[B

    iput-object v0, p1, Lorg/xbill/DNS/Name;->name:[B

    .line 126
    iget-wide v0, p0, Lorg/xbill/DNS/Name;->offsets:J

    iput-wide v0, p1, Lorg/xbill/DNS/Name;->offsets:J

    .line 137
    :goto_f
    return-void

    .line 128
    :cond_10
    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v1

    .line 129
    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    array-length v2, v2

    sub-int/2addr v2, v1

    .line 130
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v3

    .line 131
    new-array v4, v2, [B

    iput-object v4, p1, Lorg/xbill/DNS/Name;->name:[B

    .line 132
    iget-object v4, p0, Lorg/xbill/DNS/Name;->name:[B

    iget-object v5, p1, Lorg/xbill/DNS/Name;->name:[B

    invoke-static {v4, v1, v5, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 133
    :goto_27
    if-ge v0, v3, :cond_37

    const/4 v2, 0x7

    if-ge v0, v2, :cond_37

    .line 134
    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v2

    sub-int/2addr v2, v1

    invoke-direct {p1, v0, v2}, Lorg/xbill/DNS/Name;->setoffset(II)V

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_27

    .line 135
    :cond_37
    invoke-direct {p1, v3}, Lorg/xbill/DNS/Name;->setlabels(I)V

    goto :goto_f
.end method

.method private final equals([BI)Z
    .registers 13

    .prologue
    const/4 v0, 0x0

    .line 772
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v6

    .line 773
    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v2

    move v5, v0

    :goto_a
    if-ge v5, v6, :cond_50

    .line 774
    iget-object v1, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v1, v1, v2

    aget-byte v3, p1, p2

    if-eq v1, v3, :cond_15

    .line 785
    :cond_14
    :goto_14
    return v0

    .line 776
    :cond_15
    iget-object v3, p0, Lorg/xbill/DNS/Name;->name:[B

    add-int/lit8 v1, v2, 0x1

    aget-byte v7, v3, v2

    .line 777
    add-int/lit8 v2, p2, 0x1

    .line 778
    const/16 v3, 0x3f

    if-le v7, v3, :cond_29

    .line 779
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid label"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_29
    move v4, v2

    move v2, v1

    move v1, v0

    .line 780
    :goto_2c
    if-ge v1, v7, :cond_4b

    .line 781
    sget-object v8, Lorg/xbill/DNS/Name;->lowercase:[B

    iget-object v9, p0, Lorg/xbill/DNS/Name;->name:[B

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v9, v2

    and-int/lit16 v2, v2, 0xff

    aget-byte v8, v8, v2

    sget-object v9, Lorg/xbill/DNS/Name;->lowercase:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    aget-byte v4, v9, v4

    if-ne v8, v4, :cond_14

    .line 780
    add-int/lit8 v1, v1, 0x1

    move v4, v2

    move v2, v3

    goto :goto_2c

    .line 773
    :cond_4b
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move p2, v4

    goto :goto_a

    .line 785
    :cond_50
    const/4 v0, 0x1

    goto :goto_14
.end method

.method public static fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
    .registers 4

    .prologue
    .line 332
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0, v0}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
    :try_end_4
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_1 .. :try_end_4} :catch_6

    move-result-object v0

    return-object v0

    .line 335
    :catch_6
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "Invalid name \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static fromString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
    .registers 2

    .prologue
    .line 320
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v0

    return-object v0
.end method

.method public static fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
    .registers 3

    .prologue
    .line 302
    const-string v0, "@"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    if-eqz p1, :cond_b

    .line 307
    :goto_a
    return-object p1

    .line 304
    :cond_b
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 305
    sget-object p1, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    goto :goto_a

    .line 307
    :cond_16
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0, p0, p1}, Lorg/xbill/DNS/Name;-><init>(Ljava/lang/String;Lorg/xbill/DNS/Name;)V

    move-object p1, v0

    goto :goto_a
.end method

.method private final getlabels()I
    .registers 5

    .prologue
    .line 119
    iget-wide v0, p0, Lorg/xbill/DNS/Name;->offsets:J

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private final offset(I)I
    .registers 7

    .prologue
    const/4 v0, 0x6

    .line 96
    if-nez p1, :cond_b

    invoke-direct {p0}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v1

    if-nez v1, :cond_b

    .line 97
    const/4 v0, 0x0

    .line 107
    :cond_a
    :goto_a
    return v0

    .line 98
    :cond_b
    if-ltz p1, :cond_13

    invoke-direct {p0}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v1

    if-lt p1, v1, :cond_1b

    .line 99
    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "label out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1b
    const/4 v1, 0x7

    if-ge p1, v1, :cond_2a

    .line 101
    rsub-int/lit8 v0, p1, 0x7

    mul-int/lit8 v0, v0, 0x8

    .line 102
    iget-wide v2, p0, Lorg/xbill/DNS/Name;->offsets:J

    ushr-long v0, v2, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_a

    .line 104
    :cond_2a
    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v1

    move v4, v0

    move v0, v1

    move v1, v4

    .line 105
    :goto_31
    if-ge v1, p1, :cond_a

    .line 106
    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v2, v2, v0

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 105
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_31
.end method

.method private static parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
    .registers 5

    .prologue
    .line 172
    new-instance v0, Lorg/xbill/DNS/TextParseException;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\': "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private final setlabels(I)V
    .registers 6

    .prologue
    .line 113
    iget-wide v0, p0, Lorg/xbill/DNS/Name;->offsets:J

    const-wide/16 v2, -0x100

    and-long/2addr v0, v2

    iput-wide v0, p0, Lorg/xbill/DNS/Name;->offsets:J

    .line 114
    iget-wide v0, p0, Lorg/xbill/DNS/Name;->offsets:J

    int-to-long v2, p1

    or-long/2addr v0, v2

    iput-wide v0, p0, Lorg/xbill/DNS/Name;->offsets:J

    .line 115
    return-void
.end method

.method private final setoffset(II)V
    .registers 11

    .prologue
    .line 87
    const/4 v0, 0x7

    if-lt p1, v0, :cond_4

    .line 92
    :goto_3
    return-void

    .line 89
    :cond_4
    rsub-int/lit8 v0, p1, 0x7

    mul-int/lit8 v0, v0, 0x8

    .line 90
    iget-wide v2, p0, Lorg/xbill/DNS/Name;->offsets:J

    const-wide/16 v4, 0xff

    shl-long/2addr v4, v0

    const-wide/16 v6, -0x1

    xor-long/2addr v4, v6

    and-long/2addr v2, v4

    iput-wide v2, p0, Lorg/xbill/DNS/Name;->offsets:J

    .line 91
    iget-wide v2, p0, Lorg/xbill/DNS/Name;->offsets:J

    int-to-long v4, p2

    shl-long v0, v4, v0

    or-long/2addr v0, v2

    iput-wide v0, p0, Lorg/xbill/DNS/Name;->offsets:J

    goto :goto_3
.end method


# virtual methods
.method public canonicalize()Lorg/xbill/DNS/Name;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 483
    const/4 v2, 0x1

    move v0, v1

    .line 484
    :goto_3
    iget-object v3, p0, Lorg/xbill/DNS/Name;->name:[B

    array-length v3, v3

    if-ge v0, v3, :cond_49

    .line 485
    sget-object v3, Lorg/xbill/DNS/Name;->lowercase:[B

    iget-object v4, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v4, v4, v0

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    iget-object v4, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v4, v4, v0

    if-eq v3, v4, :cond_1c

    move v0, v1

    .line 490
    :goto_19
    if-eqz v0, :cond_1f

    .line 498
    :goto_1b
    return-object p0

    .line 484
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 493
    :cond_1f
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0}, Lorg/xbill/DNS/Name;-><init>()V

    .line 494
    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    invoke-direct {p0, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v3

    invoke-direct {p0}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lorg/xbill/DNS/Name;->appendSafe([BII)V

    .line 495
    :goto_31
    iget-object v2, v0, Lorg/xbill/DNS/Name;->name:[B

    array-length v2, v2

    if-ge v1, v2, :cond_47

    .line 496
    iget-object v2, v0, Lorg/xbill/DNS/Name;->name:[B

    sget-object v3, Lorg/xbill/DNS/Name;->lowercase:[B

    iget-object v4, v0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v4, v4, v1

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    aput-byte v3, v2, v1

    .line 495
    add-int/lit8 v1, v1, 0x1

    goto :goto_31

    :cond_47
    move-object p0, v0

    .line 498
    goto :goto_1b

    :cond_49
    move v0, v2

    goto :goto_19
.end method

.method public compareTo(Ljava/lang/Object;)I
    .registers 16

    .prologue
    const/4 v3, 0x0

    .line 834
    check-cast p1, Lorg/xbill/DNS/Name;

    .line 836
    if-ne p0, p1, :cond_7

    move v0, v3

    .line 857
    :goto_6
    return v0

    .line 839
    :cond_7
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v2

    .line 840
    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->labels()I

    move-result v1

    .line 841
    if-le v2, v1, :cond_50

    move v0, v1

    .line 843
    :goto_12
    const/4 v4, 0x1

    move v6, v4

    :goto_14
    if-gt v6, v0, :cond_5f

    .line 844
    sub-int v4, v2, v6

    invoke-direct {p0, v4}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v7

    .line 845
    sub-int v4, v1, v6

    invoke-direct {p1, v4}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v8

    .line 846
    iget-object v4, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v9, v4, v7

    .line 847
    iget-object v4, p1, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v10, v4, v8

    move v5, v3

    .line 848
    :goto_2b
    if-ge v5, v9, :cond_56

    if-ge v5, v10, :cond_56

    .line 849
    sget-object v4, Lorg/xbill/DNS/Name;->lowercase:[B

    iget-object v11, p0, Lorg/xbill/DNS/Name;->name:[B

    add-int v12, v5, v7

    add-int/lit8 v12, v12, 0x1

    aget-byte v11, v11, v12

    and-int/lit16 v11, v11, 0xff

    aget-byte v4, v4, v11

    sget-object v11, Lorg/xbill/DNS/Name;->lowercase:[B

    iget-object v12, p1, Lorg/xbill/DNS/Name;->name:[B

    add-int v13, v5, v8

    add-int/lit8 v13, v13, 0x1

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    aget-byte v11, v11, v12

    sub-int/2addr v4, v11

    .line 851
    if-eqz v4, :cond_52

    move v0, v4

    .line 852
    goto :goto_6

    :cond_50
    move v0, v2

    .line 841
    goto :goto_12

    .line 848
    :cond_52
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2b

    .line 854
    :cond_56
    if-eq v9, v10, :cond_5b

    .line 855
    sub-int v0, v9, v10

    goto :goto_6

    .line 843
    :cond_5b
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_14

    .line 857
    :cond_5f
    sub-int v0, v2, v1

    goto :goto_6
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 793
    if-ne p1, p0, :cond_5

    .line 794
    const/4 v0, 0x1

    .line 806
    :cond_4
    :goto_4
    return v0

    .line 795
    :cond_5
    if-eqz p1, :cond_4

    instance-of v1, p1, Lorg/xbill/DNS/Name;

    if-eqz v1, :cond_4

    .line 797
    check-cast p1, Lorg/xbill/DNS/Name;

    .line 798
    iget v1, p1, Lorg/xbill/DNS/Name;->hashcode:I

    if-nez v1, :cond_14

    .line 799
    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->hashCode()I

    .line 800
    :cond_14
    iget v1, p0, Lorg/xbill/DNS/Name;->hashcode:I

    if-nez v1, :cond_1b

    .line 801
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->hashCode()I

    .line 802
    :cond_1b
    iget v1, p1, Lorg/xbill/DNS/Name;->hashcode:I

    iget v2, p0, Lorg/xbill/DNS/Name;->hashcode:I

    if-ne v1, v2, :cond_4

    .line 804
    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->labels()I

    move-result v1

    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 806
    iget-object v1, p1, Lorg/xbill/DNS/Name;->name:[B

    invoke-direct {p1, v0}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v0

    invoke-direct {p0, v1, v0}, Lorg/xbill/DNS/Name;->equals([BI)Z

    move-result v0

    goto :goto_4
.end method

.method public fromDNAME(Lorg/xbill/DNS/DNAMERecord;)Lorg/xbill/DNS/Name;
    .registers 12

    .prologue
    const/4 v1, 0x0

    .line 509
    invoke-virtual {p1}, Lorg/xbill/DNS/DNAMERecord;->getName()Lorg/xbill/DNS/Name;

    move-result-object v0

    .line 510
    invoke-virtual {p1}, Lorg/xbill/DNS/DNAMERecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v2

    .line 511
    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Name;->subdomain(Lorg/xbill/DNS/Name;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 512
    const/4 v0, 0x0

    .line 534
    :cond_10
    return-object v0

    .line 514
    :cond_11
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v3

    invoke-virtual {v0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v4

    sub-int/2addr v3, v4

    .line 515
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->length()S

    move-result v4

    invoke-virtual {v0}, Lorg/xbill/DNS/Name;->length()S

    move-result v0

    sub-int/2addr v4, v0

    .line 516
    invoke-direct {p0, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v5

    .line 518
    invoke-virtual {v2}, Lorg/xbill/DNS/Name;->labels()I

    move-result v6

    .line 519
    invoke-virtual {v2}, Lorg/xbill/DNS/Name;->length()S

    move-result v7

    .line 521
    add-int v0, v4, v7

    const/16 v8, 0xff

    if-le v0, v8, :cond_3b

    .line 522
    new-instance v0, Lorg/xbill/DNS/NameTooLongException;

    invoke-direct {v0}, Lorg/xbill/DNS/NameTooLongException;-><init>()V

    throw v0

    .line 524
    :cond_3b
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0}, Lorg/xbill/DNS/Name;-><init>()V

    .line 525
    add-int v8, v3, v6

    invoke-direct {v0, v8}, Lorg/xbill/DNS/Name;->setlabels(I)V

    .line 526
    add-int v8, v4, v7

    new-array v8, v8, [B

    iput-object v8, v0, Lorg/xbill/DNS/Name;->name:[B

    .line 527
    iget-object v8, p0, Lorg/xbill/DNS/Name;->name:[B

    iget-object v9, v0, Lorg/xbill/DNS/Name;->name:[B

    invoke-static {v8, v5, v9, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 528
    iget-object v2, v2, Lorg/xbill/DNS/Name;->name:[B

    iget-object v5, v0, Lorg/xbill/DNS/Name;->name:[B

    invoke-static {v2, v1, v5, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v1

    .line 530
    :goto_5a
    const/4 v4, 0x7

    if-ge v2, v4, :cond_10

    add-int v4, v3, v6

    if-ge v2, v4, :cond_10

    .line 531
    invoke-direct {v0, v2, v1}, Lorg/xbill/DNS/Name;->setoffset(II)V

    .line 532
    iget-object v4, v0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v4, v4, v1

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v1, v4

    .line 530
    add-int/lit8 v2, v2, 0x1

    goto :goto_5a
.end method

.method public getLabel(I)[B
    .registers 7

    .prologue
    .line 658
    invoke-direct {p0, p1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v0

    .line 659
    iget-object v1, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v1, v1, v0

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    .line 660
    new-array v2, v1, [B

    .line 661
    iget-object v3, p0, Lorg/xbill/DNS/Name;->name:[B

    const/4 v4, 0x0

    invoke-static {v3, v0, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 662
    return-object v2
.end method

.method public getLabelString(I)Ljava/lang/String;
    .registers 4

    .prologue
    .line 672
    invoke-direct {p0, p1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v0

    .line 673
    iget-object v1, p0, Lorg/xbill/DNS/Name;->name:[B

    invoke-direct {p0, v1, v0}, Lorg/xbill/DNS/Name;->byteString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 814
    iget v0, p0, Lorg/xbill/DNS/Name;->hashcode:I

    if-eqz v0, :cond_8

    .line 815
    iget v0, p0, Lorg/xbill/DNS/Name;->hashcode:I

    .line 820
    :goto_7
    return v0

    .line 817
    :cond_8
    invoke-direct {p0, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v0

    :goto_c
    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    array-length v2, v2

    if-ge v0, v2, :cond_22

    .line 818
    shl-int/lit8 v2, v1, 0x3

    sget-object v3, Lorg/xbill/DNS/Name;->lowercase:[B

    iget-object v4, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v4, v4, v0

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 817
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 819
    :cond_22
    iput v1, p0, Lorg/xbill/DNS/Name;->hashcode:I

    .line 820
    iget v0, p0, Lorg/xbill/DNS/Name;->hashcode:I

    goto :goto_7
.end method

.method public isAbsolute()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 552
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v1

    .line 553
    if-nez v1, :cond_8

    .line 555
    :cond_7
    :goto_7
    return v0

    :cond_8
    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v1

    aget-byte v1, v2, v1

    if-nez v1, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method public isWild()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 542
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v2

    if-nez v2, :cond_9

    .line 544
    :cond_8
    :goto_8
    return v0

    :cond_9
    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v2, v2, v0

    if-ne v2, v1, :cond_8

    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v2, v2, v1

    const/16 v3, 0x2a

    if-ne v2, v3, :cond_8

    move v0, v1

    goto :goto_8
.end method

.method public labels()I
    .registers 2

    .prologue
    .line 573
    invoke-direct {p0}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v0

    return v0
.end method

.method public length()S
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 563
    invoke-direct {p0}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v1

    if-nez v1, :cond_8

    .line 565
    :goto_7
    return v0

    :cond_8
    iget-object v1, p0, Lorg/xbill/DNS/Name;->name:[B

    array-length v1, v1

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v0

    sub-int v0, v1, v0

    int-to-short v0, v0

    goto :goto_7
.end method

.method public relativize(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 444
    if-eqz p1, :cond_9

    invoke-virtual {p0, p1}, Lorg/xbill/DNS/Name;->subdomain(Lorg/xbill/DNS/Name;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 453
    :cond_9
    :goto_9
    return-object p0

    .line 446
    :cond_a
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0}, Lorg/xbill/DNS/Name;-><init>()V

    .line 447
    invoke-static {p0, v0}, Lorg/xbill/DNS/Name;->copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V

    .line 448
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->length()S

    move-result v1

    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->length()S

    move-result v2

    sub-int/2addr v1, v2

    .line 449
    invoke-virtual {v0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v2

    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->labels()I

    move-result v3

    sub-int/2addr v2, v3

    .line 450
    invoke-direct {v0, v2}, Lorg/xbill/DNS/Name;->setlabels(I)V

    .line 451
    new-array v2, v1, [B

    iput-object v2, v0, Lorg/xbill/DNS/Name;->name:[B

    .line 452
    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    invoke-direct {p0, v5}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v3

    iget-object v4, v0, Lorg/xbill/DNS/Name;->name:[B

    invoke-static {v2, v3, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 453
    goto :goto_9
.end method

.method public subdomain(Lorg/xbill/DNS/Name;)Z
    .registers 5

    .prologue
    .line 581
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v0

    .line 582
    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->labels()I

    move-result v1

    .line 583
    if-le v1, v0, :cond_c

    .line 584
    const/4 v0, 0x0

    .line 587
    :goto_b
    return v0

    .line 585
    :cond_c
    if-ne v1, v0, :cond_13

    .line 586
    invoke-virtual {p0, p1}, Lorg/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_b

    .line 587
    :cond_13
    iget-object v2, p0, Lorg/xbill/DNS/Name;->name:[B

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v0

    invoke-direct {p1, v2, v0}, Lorg/xbill/DNS/Name;->equals([BI)Z

    move-result v0

    goto :goto_b
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 648
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/xbill/DNS/Name;->toString(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Z)Ljava/lang/String;
    .registers 9

    .prologue
    const/16 v6, 0x2e

    const/4 v1, 0x0

    .line 619
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v2

    .line 620
    if-nez v2, :cond_c

    .line 621
    const-string v0, "@"

    .line 639
    :goto_b
    return-object v0

    .line 622
    :cond_c
    const/4 v0, 0x1

    if-ne v2, v0, :cond_1c

    iget-object v0, p0, Lorg/xbill/DNS/Name;->name:[B

    invoke-direct {p0, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v3

    aget-byte v0, v0, v3

    if-nez v0, :cond_1c

    .line 623
    const-string v0, "."

    goto :goto_b

    .line 624
    :cond_1c
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 625
    invoke-direct {p0, v1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v0

    :goto_25
    if-ge v1, v2, :cond_3e

    .line 626
    iget-object v4, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v4, v4, v0

    .line 627
    const/16 v5, 0x3f

    if-le v4, v5, :cond_37

    .line 628
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid label"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 629
    :cond_37
    if-nez v4, :cond_43

    .line 630
    if-nez p1, :cond_3e

    .line 631
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 639
    :cond_3e
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    .line 634
    :cond_43
    if-lez v1, :cond_48

    .line 635
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 636
    :cond_48
    iget-object v5, p0, Lorg/xbill/DNS/Name;->name:[B

    invoke-direct {p0, v5, v0}, Lorg/xbill/DNS/Name;->byteString([BI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 637
    add-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v4

    .line 625
    add-int/lit8 v1, v1, 0x1

    goto :goto_25
.end method

.method public toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;)V
    .registers 9

    .prologue
    const/4 v3, 0x0

    .line 684
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->isAbsolute()Z

    move-result v0

    if-nez v0, :cond_f

    .line 685
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "toWire() called on non-absolute name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 688
    :cond_f
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v4

    move v2, v3

    .line 689
    :goto_14
    add-int/lit8 v0, v4, -0x1

    if-ge v2, v0, :cond_4f

    .line 691
    if-nez v2, :cond_2c

    move-object v1, p0

    .line 695
    :goto_1b
    const/4 v0, -0x1

    .line 696
    if-eqz p2, :cond_22

    .line 697
    invoke-virtual {p2, v1}, Lorg/xbill/DNS/Compression;->get(Lorg/xbill/DNS/Name;)I

    move-result v0

    .line 698
    :cond_22
    if-ltz v0, :cond_33

    .line 699
    const v1, 0xc000

    or-int/2addr v0, v1

    .line 700
    invoke-virtual {p1, v0}, Lorg/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 710
    :goto_2b
    return-void

    .line 694
    :cond_2c
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0, p0, v2}, Lorg/xbill/DNS/Name;-><init>(Lorg/xbill/DNS/Name;I)V

    move-object v1, v0

    goto :goto_1b

    .line 703
    :cond_33
    if-eqz p2, :cond_3c

    .line 704
    invoke-virtual {p1}, Lorg/xbill/DNS/DNSOutput;->current()I

    move-result v0

    invoke-virtual {p2, v0, v1}, Lorg/xbill/DNS/Compression;->add(ILorg/xbill/DNS/Name;)V

    .line 705
    :cond_3c
    invoke-direct {p0, v2}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v0

    .line 706
    iget-object v1, p0, Lorg/xbill/DNS/Name;->name:[B

    iget-object v5, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v5, v5, v0

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v1, v0, v5}, Lorg/xbill/DNS/DNSOutput;->writeByteArray([BII)V

    .line 689
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_14

    .line 709
    :cond_4f
    invoke-virtual {p1, v3}, Lorg/xbill/DNS/DNSOutput;->writeU8(I)V

    goto :goto_2b
.end method

.method public toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
    .registers 4

    .prologue
    .line 764
    if-eqz p3, :cond_6

    .line 765
    invoke-virtual {p0, p1}, Lorg/xbill/DNS/Name;->toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V

    .line 768
    :goto_5
    return-void

    .line 767
    :cond_6
    invoke-virtual {p0, p1, p2}, Lorg/xbill/DNS/Name;->toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;)V

    goto :goto_5
.end method

.method public toWire()[B
    .registers 3

    .prologue
    .line 718
    new-instance v0, Lorg/xbill/DNS/DNSOutput;

    invoke-direct {v0}, Lorg/xbill/DNS/DNSOutput;-><init>()V

    .line 719
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/xbill/DNS/Name;->toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;)V

    .line 720
    invoke-virtual {v0}, Lorg/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
    .registers 3

    .prologue
    .line 729
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->toWireCanonical()[B

    move-result-object v0

    .line 730
    invoke-virtual {p1, v0}, Lorg/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 731
    return-void
.end method

.method public toWireCanonical()[B
    .registers 13

    .prologue
    const/4 v2, 0x0

    .line 739
    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->labels()I

    move-result v8

    .line 740
    if-nez v8, :cond_a

    .line 741
    new-array v0, v2, [B

    .line 751
    :cond_9
    return-object v0

    .line 742
    :cond_a
    iget-object v0, p0, Lorg/xbill/DNS/Name;->name:[B

    array-length v0, v0

    invoke-direct {p0, v2}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v1

    sub-int/2addr v0, v1

    new-array v0, v0, [B

    .line 743
    invoke-direct {p0, v2}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v5

    move v4, v2

    move v7, v2

    :goto_1a
    if-ge v7, v8, :cond_9

    .line 744
    iget-object v1, p0, Lorg/xbill/DNS/Name;->name:[B

    aget-byte v9, v1, v5

    .line 745
    const/16 v1, 0x3f

    if-le v9, v1, :cond_2c

    .line 746
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid label"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 747
    :cond_2c
    add-int/lit8 v1, v4, 0x1

    iget-object v6, p0, Lorg/xbill/DNS/Name;->name:[B

    add-int/lit8 v3, v5, 0x1

    aget-byte v5, v6, v5

    aput-byte v5, v0, v4

    move v5, v3

    move v3, v1

    move v1, v2

    .line 748
    :goto_39
    if-ge v1, v9, :cond_50

    .line 749
    add-int/lit8 v4, v3, 0x1

    sget-object v10, Lorg/xbill/DNS/Name;->lowercase:[B

    iget-object v11, p0, Lorg/xbill/DNS/Name;->name:[B

    add-int/lit8 v6, v5, 0x1

    aget-byte v5, v11, v5

    and-int/lit16 v5, v5, 0xff

    aget-byte v5, v10, v5

    aput-byte v5, v0, v3

    .line 748
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    move v5, v6

    goto :goto_39

    .line 743
    :cond_50
    add-int/lit8 v1, v7, 0x1

    move v4, v3

    move v7, v1

    goto :goto_1a
.end method

.method public wild(I)Lorg/xbill/DNS/Name;
    .registers 6

    .prologue
    .line 462
    if-gtz p1, :cond_a

    .line 463
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "must replace 1 or more labels"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 466
    :cond_a
    :try_start_a
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0}, Lorg/xbill/DNS/Name;-><init>()V

    .line 467
    sget-object v1, Lorg/xbill/DNS/Name;->wild:Lorg/xbill/DNS/Name;

    invoke-static {v1, v0}, Lorg/xbill/DNS/Name;->copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V

    .line 468
    iget-object v1, p0, Lorg/xbill/DNS/Name;->name:[B

    invoke-direct {p0, p1}, Lorg/xbill/DNS/Name;->offset(I)I

    move-result v2

    invoke-direct {p0}, Lorg/xbill/DNS/Name;->getlabels()I

    move-result v3

    sub-int/2addr v3, p1

    invoke-direct {v0, v1, v2, v3}, Lorg/xbill/DNS/Name;->append([BII)V
    :try_end_22
    .catch Lorg/xbill/DNS/NameTooLongException; {:try_start_a .. :try_end_22} :catch_23

    .line 469
    return-object v0

    .line 472
    :catch_23
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Name.wild: concatenate failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
