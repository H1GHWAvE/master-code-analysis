.class public final Lnet/hockeyapp/android/d/r;
.super Lnet/hockeyapp/android/d/k;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field private c:Landroid/content/Context;

.field private d:Landroid/os/Handler;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/util/List;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Landroid/app/ProgressDialog;

.field private n:Ljava/net/HttpURLConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
    .registers 12

    .prologue
    .line 93
    invoke-direct {p0}, Lnet/hockeyapp/android/d/k;-><init>()V

    .line 95
    iput-object p1, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    .line 96
    iput-object p2, p0, Lnet/hockeyapp/android/d/r;->e:Ljava/lang/String;

    .line 97
    iput-object p3, p0, Lnet/hockeyapp/android/d/r;->f:Ljava/lang/String;

    .line 98
    iput-object p4, p0, Lnet/hockeyapp/android/d/r;->g:Ljava/lang/String;

    .line 99
    iput-object p5, p0, Lnet/hockeyapp/android/d/r;->h:Ljava/lang/String;

    .line 100
    iput-object p6, p0, Lnet/hockeyapp/android/d/r;->i:Ljava/lang/String;

    .line 101
    iput-object p7, p0, Lnet/hockeyapp/android/d/r;->j:Ljava/util/List;

    .line 102
    iput-object p8, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    .line 103
    iput-object p9, p0, Lnet/hockeyapp/android/d/r;->d:Landroid/os/Handler;

    .line 104
    iput-boolean p10, p0, Lnet/hockeyapp/android/d/r;->l:Z

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/r;->a:Z

    .line 106
    const/4 v0, -0x1

    iput v0, p0, Lnet/hockeyapp/android/d/r;->b:I

    .line 108
    if-eqz p1, :cond_22

    .line 109
    invoke-static {p1}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 111
    :cond_22
    return-void
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 118
    iput p1, p0, Lnet/hockeyapp/android/d/r;->b:I

    .line 119
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 2

    .prologue
    .line 122
    iput-object p1, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    .line 123
    return-void
.end method

.method private a(Ljava/util/HashMap;)V
    .registers 6

    .prologue
    .line 180
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_9

    .line 182
    :try_start_4
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_9} :catch_49

    .line 189
    :cond_9
    :goto_9
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->d:Landroid/os/Handler;

    if-eqz v0, :cond_48

    .line 190
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 191
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 193
    if-eqz p1, :cond_4e

    .line 194
    const-string v3, "request_type"

    const-string v0, "type"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v3, "feedback_response"

    const-string v0, "response"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v3, "feedback_status"

    const-string v0, "status"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :goto_40
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 203
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 205
    :cond_48
    return-void

    .line 184
    :catch_49
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_9

    .line 198
    :cond_4e
    const-string v0, "request_type"

    const-string v3, "unknown"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_40
.end method

.method private b()V
    .registers 2

    .prologue
    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/r;->a:Z

    .line 115
    return-void
.end method

.method private varargs c()Ljava/util/HashMap;
    .registers 6

    .prologue
    .line 147
    iget-boolean v0, p0, Lnet/hockeyapp/android/d/r;->l:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 149
    invoke-direct {p0}, Lnet/hockeyapp/android/d/r;->f()Ljava/util/HashMap;

    move-result-object v0

    .line 175
    :goto_c
    return-object v0

    .line 150
    :cond_d
    iget-boolean v0, p0, Lnet/hockeyapp/android/d/r;->l:Z

    if-nez v0, :cond_5d

    .line 155
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 156
    invoke-direct {p0}, Lnet/hockeyapp/android/d/r;->d()Ljava/util/HashMap;

    move-result-object v0

    goto :goto_c

    .line 158
    :cond_1e
    invoke-direct {p0}, Lnet/hockeyapp/android/d/r;->e()Ljava/util/HashMap;

    move-result-object v1

    .line 161
    const-string v0, "status"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 162
    if-eqz v0, :cond_5b

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    if-eqz v0, :cond_5b

    .line 163
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "HockeyApp"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 164
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 165
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_51
    if-ge v0, v3, :cond_5b

    aget-object v4, v2, v0

    .line 166
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_51

    :cond_5b
    move-object v0, v1

    .line 171
    goto :goto_c

    .line 175
    :cond_5d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private d()Ljava/util/HashMap;
    .registers 6

    .prologue
    .line 213
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 214
    const-string v0, "type"

    const-string v1, "send"

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    const/4 v1, 0x0

    .line 218
    :try_start_d
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 219
    const-string v0, "name"

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->f:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    const-string v0, "email"

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->g:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    const-string v0, "subject"

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->h:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    const-string v0, "text"

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->i:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    const-string v0, "bundle_identifier"

    sget-object v4, Lnet/hockeyapp/android/a;->d:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    const-string v0, "bundle_short_version"

    sget-object v4, Lnet/hockeyapp/android/a;->c:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    const-string v0, "bundle_version"

    sget-object v4, Lnet/hockeyapp/android/a;->b:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    const-string v0, "os_version"

    sget-object v4, Lnet/hockeyapp/android/a;->e:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    const-string v0, "oem"

    sget-object v4, Lnet/hockeyapp/android/a;->g:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    const-string v0, "model"

    sget-object v4, Lnet/hockeyapp/android/a;->f:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    if-eqz v0, :cond_79

    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/d/r;->e:Ljava/lang/String;

    .line 234
    :cond_79
    new-instance v4, Lnet/hockeyapp/android/e/l;

    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->e:Ljava/lang/String;

    invoke-direct {v4, v0}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    if-eqz v0, :cond_af

    const-string v0, "PUT"

    .line 1071
    :goto_86
    iput-object v0, v4, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    .line 236
    invoke-virtual {v4, v3}, Lnet/hockeyapp/android/e/l;->a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 239
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 241
    const-string v0, "status"

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const-string v0, "response"

    invoke-static {v1}, Lnet/hockeyapp/android/d/r;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_a9
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_a9} :catch_b2
    .catchall {:try_start_d .. :try_end_a9} :catchall_bc

    .line 246
    if-eqz v1, :cond_ae

    .line 247
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 251
    :cond_ae
    :goto_ae
    return-object v2

    .line 234
    :cond_af
    :try_start_af
    const-string v0, "POST"
    :try_end_b1
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_b1} :catch_b2
    .catchall {:try_start_af .. :try_end_b1} :catchall_bc

    goto :goto_86

    .line 244
    :catch_b2
    move-exception v0

    :try_start_b3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b6
    .catchall {:try_start_b3 .. :try_end_b6} :catchall_bc

    .line 246
    if-eqz v1, :cond_ae

    .line 247
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_ae

    .line 246
    :catchall_bc
    move-exception v0

    if-eqz v1, :cond_c2

    .line 247
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_c2
    throw v0
.end method

.method private e()Ljava/util/HashMap;
    .registers 7

    .prologue
    .line 260
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 261
    const-string v0, "type"

    const-string v1, "send"

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    const/4 v1, 0x0

    .line 265
    :try_start_d
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 266
    const-string v0, "name"

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->f:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    const-string v0, "email"

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->g:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    const-string v0, "subject"

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->h:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    const-string v0, "text"

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->i:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    const-string v0, "bundle_identifier"

    sget-object v4, Lnet/hockeyapp/android/a;->d:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    const-string v0, "bundle_short_version"

    sget-object v4, Lnet/hockeyapp/android/a;->c:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    const-string v0, "bundle_version"

    sget-object v4, Lnet/hockeyapp/android/a;->b:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    const-string v0, "os_version"

    sget-object v4, Lnet/hockeyapp/android/a;->e:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    const-string v0, "oem"

    sget-object v4, Lnet/hockeyapp/android/a;->g:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    const-string v0, "model"

    sget-object v4, Lnet/hockeyapp/android/a;->f:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    if-eqz v0, :cond_79

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/d/r;->e:Ljava/lang/String;

    .line 281
    :cond_79
    new-instance v4, Lnet/hockeyapp/android/e/l;

    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->e:Ljava/lang/String;

    invoke-direct {v4, v0}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    if-eqz v0, :cond_b3

    const-string v0, "PUT"

    .line 2071
    :goto_86
    iput-object v0, v4, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    .line 282
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    iget-object v5, p0, Lnet/hockeyapp/android/d/r;->j:Ljava/util/List;

    .line 283
    invoke-virtual {v4, v3, v0, v5}, Lnet/hockeyapp/android/e/l;->a(Ljava/util/Map;Landroid/content/Context;Ljava/util/List;)Lnet/hockeyapp/android/e/l;

    move-result-object v0

    .line 284
    invoke-virtual {v0}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 286
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 288
    const-string v0, "status"

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    const-string v0, "response"

    invoke-static {v1}, Lnet/hockeyapp/android/d/r;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_ad
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_ad} :catch_b6
    .catchall {:try_start_d .. :try_end_ad} :catchall_c0

    .line 294
    if-eqz v1, :cond_b2

    .line 295
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 299
    :cond_b2
    :goto_b2
    return-object v2

    .line 281
    :cond_b3
    :try_start_b3
    const-string v0, "POST"
    :try_end_b5
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_b5} :catch_b6
    .catchall {:try_start_b3 .. :try_end_b5} :catchall_c0

    goto :goto_86

    .line 292
    :catch_b6
    move-exception v0

    :try_start_b7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_ba
    .catchall {:try_start_b7 .. :try_end_ba} :catchall_c0

    .line 294
    if-eqz v1, :cond_b2

    .line 295
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_b2

    .line 294
    :catchall_c0
    move-exception v0

    if-eqz v1, :cond_c6

    .line 295
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_c6
    throw v0
.end method

.method private f()Ljava/util/HashMap;
    .registers 5

    .prologue
    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lnet/hockeyapp/android/d/r;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    invoke-static {v2}, Lnet/hockeyapp/android/e/w;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    iget v1, p0, Lnet/hockeyapp/android/d/r;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3a

    .line 312
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "?last_message_id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lnet/hockeyapp/android/d/r;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    :cond_3a
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 317
    const/4 v1, 0x0

    .line 320
    :try_start_40
    new-instance v3, Lnet/hockeyapp/android/e/l;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    .line 321
    invoke-virtual {v3}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 323
    const-string v0, "type"

    const-string v3, "fetch"

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 327
    const-string v0, "status"

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    const-string v0, "response"

    invoke-static {v1}, Lnet/hockeyapp/android/d/r;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6d
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_6d} :catch_73
    .catchall {:try_start_40 .. :try_end_6d} :catchall_7d

    .line 332
    if-eqz v1, :cond_72

    .line 333
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 337
    :cond_72
    :goto_72
    return-object v2

    .line 330
    :catch_73
    move-exception v0

    :try_start_74
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_77
    .catchall {:try_start_74 .. :try_end_77} :catchall_7d

    .line 332
    if-eqz v1, :cond_72

    .line 333
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_72

    .line 332
    :catchall_7d
    move-exception v0

    if-eqz v1, :cond_83

    .line 333
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_83
    throw v0
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 126
    iput-object v1, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    .line 127
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_c

    .line 128
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 130
    :cond_c
    iput-object v1, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    .line 131
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    .line 59
    .line 3147
    iget-boolean v0, p0, Lnet/hockeyapp/android/d/r;->l:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->k:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 3149
    invoke-direct {p0}, Lnet/hockeyapp/android/d/r;->f()Ljava/util/HashMap;

    move-result-object v0

    .line 3171
    :goto_c
    return-object v0

    .line 3150
    :cond_d
    iget-boolean v0, p0, Lnet/hockeyapp/android/d/r;->l:Z

    if-nez v0, :cond_5d

    .line 3155
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 3156
    invoke-direct {p0}, Lnet/hockeyapp/android/d/r;->d()Ljava/util/HashMap;

    move-result-object v0

    goto :goto_c

    .line 3158
    :cond_1e
    invoke-direct {p0}, Lnet/hockeyapp/android/d/r;->e()Ljava/util/HashMap;

    move-result-object v1

    .line 3161
    const-string v0, "status"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3162
    if-eqz v0, :cond_5b

    const-string v2, "2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    if-eqz v0, :cond_5b

    .line 3163
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "HockeyApp"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 3164
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 3165
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_51
    if-ge v0, v3, :cond_5b

    aget-object v4, v2, v0

    .line 3166
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 3165
    add-int/lit8 v0, v0, 0x1

    goto :goto_51

    :cond_5b
    move-object v0, v1

    .line 3171
    goto :goto_c

    .line 3175
    :cond_5d
    const/4 v0, 0x0

    .line 59
    goto :goto_c
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 59
    check-cast p1, Ljava/util/HashMap;

    .line 2180
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_b

    .line 2182
    :try_start_6
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_b} :catch_4b

    .line 2189
    :cond_b
    :goto_b
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->d:Landroid/os/Handler;

    if-eqz v0, :cond_4a

    .line 2190
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 2191
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2193
    if-eqz p1, :cond_50

    .line 2194
    const-string v3, "request_type"

    const-string v0, "type"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2195
    const-string v3, "feedback_response"

    const-string v0, "response"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2196
    const-string v3, "feedback_status"

    const-string v0, "status"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2201
    :goto_42
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 2203
    iget-object v0, p0, Lnet/hockeyapp/android/d/r;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 59
    :cond_4a
    return-void

    .line 2184
    :catch_4b
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_b

    .line 2198
    :cond_50
    const-string v0, "request_type"

    const-string v3, "unknown"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_42
.end method

.method protected final onPreExecute()V
    .registers 6

    .prologue
    .line 135
    const-string v0, "Sending feedback.."

    .line 136
    iget-boolean v1, p0, Lnet/hockeyapp/android/d/r;->l:Z

    if-eqz v1, :cond_8

    .line 137
    const-string v0, "Retrieving discussions..."

    .line 140
    :cond_8
    iget-object v1, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_24

    :cond_14
    iget-boolean v1, p0, Lnet/hockeyapp/android/d/r;->a:Z

    if-eqz v1, :cond_24

    .line 141
    iget-object v1, p0, Lnet/hockeyapp/android/d/r;->c:Landroid/content/Context;

    const-string v2, ""

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v2, v0, v3, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/d/r;->m:Landroid/app/ProgressDialog;

    .line 143
    :cond_24
    return-void
.end method
