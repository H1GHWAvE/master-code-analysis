.class public final Lnet/hockeyapp/android/d/h;
.super Lnet/hockeyapp/android/d/g;
.source "SourceFile"


# instance fields
.field protected h:Z

.field private i:Landroid/app/Activity;

.field private j:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0, p1, p2, p3, p4}, Lnet/hockeyapp/android/d/g;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V

    .line 65
    iput-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    .line 66
    iput-object v0, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/h;->h:Z

    .line 72
    if-eqz p1, :cond_15

    .line 73
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    .line 76
    :cond_15
    iput-boolean p5, p0, Lnet/hockeyapp/android/d/h;->h:Z

    .line 77
    return-void
.end method

.method static synthetic a(Lnet/hockeyapp/android/d/h;)Landroid/app/Activity;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lnet/hockeyapp/android/d/h;Lorg/json/JSONArray;)V
    .registers 9

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 63
    .line 3151
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    if-eqz v0, :cond_5f

    .line 3152
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 3153
    const/16 v0, 0x1001

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 3155
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "hockey_update_dialog"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 3156
    if-eqz v0, :cond_26

    .line 3157
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 3159
    :cond_26
    invoke-virtual {v1, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 3162
    const-class v0, Lnet/hockeyapp/android/at;

    .line 3163
    iget-object v2, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    if-eqz v2, :cond_31

    .line 4060
    const-class v0, Lnet/hockeyapp/android/at;

    .line 3168
    :cond_31
    :try_start_31
    const-string v2, "newInstance"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lorg/json/JSONArray;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 3169
    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    const-string v5, "apk"

    invoke-virtual {p0, v5}, Lnet/hockeyapp/android/d/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 3170
    const-string v2, "hockey_update_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    :try_end_5f
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_5f} :catch_60

    .line 3177
    :cond_5f
    :goto_5f
    return-void

    .line 3172
    :catch_60
    move-exception v0

    .line 3173
    const-string v1, "HockeyApp"

    const-string v2, "An exception happened while showing the update fragment:"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3174
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3175
    const-string v0, "HockeyApp"

    const-string v1, "Showing update activity instead."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3176
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lnet/hockeyapp/android/d/h;->a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V

    goto :goto_5f
.end method

.method static synthetic a(Lnet/hockeyapp/android/d/h;Lorg/json/JSONArray;Ljava/lang/Boolean;)V
    .registers 3

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lnet/hockeyapp/android/d/h;->a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V

    return-void
.end method

.method private a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V
    .registers 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 183
    const/4 v0, 0x0

    .line 184
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    if-eqz v1, :cond_7

    .line 3051
    const-class v0, Lnet/hockeyapp/android/al;

    .line 187
    :cond_7
    if-nez v0, :cond_b

    .line 188
    const-class v0, Lnet/hockeyapp/android/al;

    .line 191
    :cond_b
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    if-eqz v1, :cond_3d

    .line 192
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 193
    iget-object v2, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 194
    const-string v0, "json"

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v0, "url"

    const-string v2, "apk"

    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/d/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 198
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 199
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 203
    :cond_3d
    invoke-virtual {p0}, Lnet/hockeyapp/android/d/h;->b()V

    .line 204
    return-void
.end method

.method private b(Lorg/json/JSONArray;)V
    .registers 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 102
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lnet/hockeyapp/android/e/x;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 147
    :cond_16
    :goto_16
    return-void

    .line 109
    :cond_17
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 110
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x201

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 112
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_68

    .line 113
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x202

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 115
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x203

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lnet/hockeyapp/android/d/i;

    invoke-direct {v2, p0}, Lnet/hockeyapp/android/d/i;-><init>(Lnet/hockeyapp/android/d/h;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 124
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x204

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lnet/hockeyapp/android/d/j;

    invoke-direct {v2, p0, p1}, Lnet/hockeyapp/android/d/j;-><init>(Lnet/hockeyapp/android/d/h;Lorg/json/JSONArray;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 140
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    .line 141
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_16

    .line 144
    :cond_68
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x200

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 145
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lnet/hockeyapp/android/d/h;->a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V

    goto :goto_16
.end method

.method private c(Lorg/json/JSONArray;)V
    .registers 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 151
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    if-eqz v0, :cond_5f

    .line 152
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 153
    const/16 v0, 0x1001

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 155
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "hockey_update_dialog"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_26

    .line 157
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 159
    :cond_26
    invoke-virtual {v1, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 162
    const-class v0, Lnet/hockeyapp/android/at;

    .line 163
    iget-object v2, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    if-eqz v2, :cond_31

    .line 2060
    const-class v0, Lnet/hockeyapp/android/at;

    .line 168
    :cond_31
    :try_start_31
    const-string v2, "newInstance"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lorg/json/JSONArray;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 169
    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    const-string v5, "apk"

    invoke-virtual {p0, v5}, Lnet/hockeyapp/android/d/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 170
    const-string v2, "hockey_update_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    :try_end_5f
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_5f} :catch_60

    .line 179
    :cond_5f
    :goto_5f
    return-void

    .line 172
    :catch_60
    move-exception v0

    .line 173
    const-string v1, "HockeyApp"

    const-string v2, "An exception happened while showing the update fragment:"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 175
    const-string v0, "HockeyApp"

    const-string v1, "Showing update activity instead."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lnet/hockeyapp/android/d/h;->a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V

    goto :goto_5f
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-super {p0}, Lnet/hockeyapp/android/d/g;->a()V

    .line 83
    iput-object v1, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    .line 85
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    if-eqz v0, :cond_11

    .line 86
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 87
    iput-object v1, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    .line 89
    :cond_11
    return-void
.end method

.method protected final a(Lorg/json/JSONArray;)V
    .registers 6

    .prologue
    const/4 v3, 0x1

    .line 93
    invoke-super {p0, p1}, Lnet/hockeyapp/android/d/g;->a(Lorg/json/JSONArray;)V

    .line 94
    if-eqz p1, :cond_1f

    iget-boolean v0, p0, Lnet/hockeyapp/android/d/h;->h:Z

    if-eqz v0, :cond_1f

    .line 1102
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lnet/hockeyapp/android/e/x;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1105
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1141
    :cond_1f
    :goto_1f
    return-void

    .line 1109
    :cond_20
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1110
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x201

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1112
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_71

    .line 1113
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x202

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1115
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x203

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lnet/hockeyapp/android/d/i;

    invoke-direct {v2, p0}, Lnet/hockeyapp/android/d/i;-><init>(Lnet/hockeyapp/android/d/h;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1124
    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x204

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lnet/hockeyapp/android/d/j;

    invoke-direct {v2, p0, p1}, Lnet/hockeyapp/android/d/j;-><init>(Lnet/hockeyapp/android/d/h;Lorg/json/JSONArray;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1140
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    .line 1141
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_1f

    .line 1144
    :cond_71
    iget-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    iget-object v1, p0, Lnet/hockeyapp/android/d/h;->g:Lnet/hockeyapp/android/az;

    const/16 v2, 0x200

    invoke-static {v1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1145
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lnet/hockeyapp/android/d/h;->a(Lorg/json/JSONArray;Ljava/lang/Boolean;)V

    goto :goto_1f
.end method

.method protected final b()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 208
    invoke-super {p0}, Lnet/hockeyapp/android/d/g;->b()V

    .line 209
    iput-object v0, p0, Lnet/hockeyapp/android/d/h;->i:Landroid/app/Activity;

    .line 210
    iput-object v0, p0, Lnet/hockeyapp/android/d/h;->j:Landroid/app/AlertDialog;

    .line 211
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 63
    check-cast p1, Lorg/json/JSONArray;

    invoke-virtual {p0, p1}, Lnet/hockeyapp/android/d/h;->a(Lorg/json/JSONArray;)V

    return-void
.end method
