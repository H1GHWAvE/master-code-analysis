.class public final Lnet/hockeyapp/android/d/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lnet/hockeyapp/android/c/e;

.field final b:Lnet/hockeyapp/android/f/b;

.field c:Z

.field d:I


# direct methods
.method private constructor <init>(Lnet/hockeyapp/android/c/e;Lnet/hockeyapp/android/f/b;)V
    .registers 4

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lnet/hockeyapp/android/d/e;->a:Lnet/hockeyapp/android/c/e;

    .line 125
    iput-object p2, p0, Lnet/hockeyapp/android/d/e;->b:Lnet/hockeyapp/android/f/b;

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/d/e;->c:Z

    .line 127
    const/4 v0, 0x2

    iput v0, p0, Lnet/hockeyapp/android/d/e;->d:I

    .line 128
    return-void
.end method

.method public synthetic constructor <init>(Lnet/hockeyapp/android/c/e;Lnet/hockeyapp/android/f/b;B)V
    .registers 4

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lnet/hockeyapp/android/d/e;-><init>(Lnet/hockeyapp/android/c/e;Lnet/hockeyapp/android/f/b;)V

    return-void
.end method

.method private a()Lnet/hockeyapp/android/c/e;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lnet/hockeyapp/android/d/e;->a:Lnet/hockeyapp/android/c/e;

    return-object v0
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 140
    iput-boolean p1, p0, Lnet/hockeyapp/android/d/e;->c:Z

    return-void
.end method

.method private b()Lnet/hockeyapp/android/f/b;
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lnet/hockeyapp/android/d/e;->b:Lnet/hockeyapp/android/f/b;

    return-object v0
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 138
    iget-boolean v0, p0, Lnet/hockeyapp/android/d/e;->c:Z

    return v0
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 143
    iget v0, p0, Lnet/hockeyapp/android/d/e;->d:I

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 147
    iget v0, p0, Lnet/hockeyapp/android/d/e;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lnet/hockeyapp/android/d/e;->d:I

    if-gez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x1

    goto :goto_9
.end method
