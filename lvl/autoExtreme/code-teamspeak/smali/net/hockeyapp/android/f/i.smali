.class public final Lnet/hockeyapp/android/f/i;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field public static final a:I = 0x2000

.field public static final b:I = 0x2002

.field public static final c:I = 0x2004

.field public static final d:I = 0x2006

.field public static final e:I = 0x2008

.field public static final f:I = 0x2009

.field public static final g:I = 0x2010

.field public static final h:I = 0x2011

.field public static final i:I = 0x20010

.field public static final j:I = 0x20011

.field public static final k:I = 0x20012

.field public static final l:I = 0x20013

.field public static final m:I = 0x20014

.field public static final n:I = 0x20015

.field public static final o:I = 0x20016

.field public static final p:I = 0x20017


# instance fields
.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/ScrollView;

.field private s:Landroid/widget/LinearLayout;

.field private t:Landroid/widget/LinearLayout;

.field private u:Landroid/widget/LinearLayout;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 12

    .prologue
    const/4 v9, -0x2

    const/high16 v8, 0x41200000    # 10.0f

    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1123
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1124
    invoke-virtual {p0, v7}, Lnet/hockeyapp/android/f/i;->setBackgroundColor(I)V

    .line 1125
    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/i;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1129
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    .line 1130
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    const v2, 0x20012

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setId(I)V

    .line 1132
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1133
    const/16 v2, 0x31

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1135
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1136
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1137
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1139
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/i;->addView(Landroid/view/View;)V

    .line 1143
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    .line 1144
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    const v2, 0x20017

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setId(I)V

    .line 1146
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1147
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 1148
    const/16 v3, 0x11

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1150
    iget-object v3, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    invoke-virtual {v3, v0}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1151
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2, v1, v2, v1}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 1153
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1157
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    .line 1158
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    const v2, 0x20013

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setId(I)V

    .line 1160
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1161
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 1162
    const/4 v3, 0x3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1164
    iget-object v3, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1165
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1166
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    const/16 v2, 0x30

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1167
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1169
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 1173
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    .line 1174
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    const v2, 0x20015

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setId(I)V

    .line 1176
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1177
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 1178
    const/16 v3, 0x11

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1180
    iget-object v3, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1181
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1182
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    const/16 v2, 0x30

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1183
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1185
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1219
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1220
    const/16 v0, 0x2002

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setId(I)V

    .line 1222
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1223
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 1224
    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v0, v1, v4, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1226
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1227
    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1228
    const/16 v0, 0x4001

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 1229
    invoke-virtual {v2, v6}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1230
    const v0, -0x777778

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1231
    const/4 v0, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v0, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 1232
    const/4 v0, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1233
    const/16 v0, 0x402

    invoke-static {v0}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1234
    const v0, -0x333334

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1235
    invoke-static {p1, v2}, Lnet/hockeyapp/android/f/i;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 1237
    invoke-static {}, Lnet/hockeyapp/android/t;->b()Lnet/hockeyapp/android/c/i;

    move-result-object v0

    sget-object v3, Lnet/hockeyapp/android/c/i;->a:Lnet/hockeyapp/android/c/i;

    if-ne v0, v3, :cond_516

    const/16 v0, 0x8

    :goto_157
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1239
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1243
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1244
    const/16 v0, 0x2004

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setId(I)V

    .line 1246
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1247
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 1248
    invoke-virtual {v0, v1, v1, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1250
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1251
    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1252
    const/16 v0, 0x21

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 1253
    invoke-virtual {v2, v6}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1254
    const v0, -0x777778

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1255
    const/4 v0, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v0, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 1256
    const/4 v0, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1257
    const/16 v0, 0x403

    invoke-static {v0}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1258
    const v0, -0x333334

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1259
    invoke-static {p1, v2}, Lnet/hockeyapp/android/f/i;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 1260
    invoke-static {}, Lnet/hockeyapp/android/t;->c()Lnet/hockeyapp/android/c/i;

    move-result-object v0

    sget-object v3, Lnet/hockeyapp/android/c/i;->a:Lnet/hockeyapp/android/c/i;

    if-ne v0, v3, :cond_519

    const/16 v0, 0x8

    :goto_1bb
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1262
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1266
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1267
    const/16 v2, 0x2006

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setId(I)V

    .line 1269
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1270
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 1271
    invoke-virtual {v2, v1, v1, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1273
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1274
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1275
    const/16 v2, 0x4031

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 1276
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1277
    const v2, -0x777778

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1278
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 1279
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1280
    const/16 v2, 0x404

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1281
    const v2, -0x333334

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1282
    invoke-static {p1, v0}, Lnet/hockeyapp/android/f/i;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 1284
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1288
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1289
    const/16 v2, 0x2008

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setId(I)V

    .line 1291
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1293
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 1294
    const/high16 v4, 0x42c80000    # 100.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v6, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    .line 1295
    invoke-virtual {v2, v1, v1, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1297
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1298
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1299
    const/16 v2, 0x4001

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 1300
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 1301
    const v2, -0x777778

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1302
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 1303
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1304
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setMinimumHeight(I)V

    .line 1305
    const/16 v2, 0x405

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1306
    const v2, -0x333334

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1307
    invoke-static {p1, v0}, Lnet/hockeyapp/android/f/i;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 1309
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1334
    new-instance v0, Lnet/hockeyapp/android/f/a;

    invoke-direct {v0, p1}, Lnet/hockeyapp/android/f/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    .line 1335
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    const/16 v2, 0x2011

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setId(I)V

    .line 1337
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1338
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 1339
    const/4 v3, 0x3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1341
    iget-object v3, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1342
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 1344
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1349
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1350
    const/16 v2, 0x2010

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setId(I)V

    .line 1352
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 1353
    const/high16 v3, 0x41f00000    # 30.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 1354
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v8, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    .line 1356
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1358
    invoke-virtual {v5, v1, v1, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1359
    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1361
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1362
    invoke-direct {p0}, Lnet/hockeyapp/android/f/i;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1363
    invoke-virtual {v0, v3, v2, v3, v2}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1364
    const/16 v2, 0x407

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1365
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setTextColor(I)V

    .line 1366
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1368
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1373
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 1374
    const/16 v2, 0x2009

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setId(I)V

    .line 1376
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 1377
    const/high16 v3, 0x41f00000    # 30.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 1378
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v8, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    .line 1380
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1382
    invoke-virtual {v5, v1, v1, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1383
    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1385
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1386
    invoke-direct {p0}, Lnet/hockeyapp/android/f/i;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1387
    invoke-virtual {v0, v3, v2, v3, v2}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1388
    const/16 v2, 0x408

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1389
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setTextColor(I)V

    .line 1390
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1392
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2313
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2314
    const/16 v2, 0x2000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setId(I)V

    .line 2316
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2317
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v8, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 2318
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2320
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2321
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2322
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2323
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 2324
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2325
    const/16 v2, 0x406

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2326
    const v2, -0x777778

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2327
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2328
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2330
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3189
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    .line 3190
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    const v2, 0x20014

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setId(I)V

    .line 3192
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3193
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 3194
    const/4 v3, 0x3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 3196
    iget-object v3, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3197
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v1, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 3198
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    const/16 v2, 0x30

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 3199
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 3201
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3397
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3398
    const v2, 0x20010

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setId(I)V

    .line 3400
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 3401
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v8, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 3402
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v6, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    .line 3404
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3406
    invoke-virtual {v5, v1, v1, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 3407
    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 3408
    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, v5, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 3410
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3411
    invoke-direct {p0}, Lnet/hockeyapp/android/f/i;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3412
    invoke-virtual {v0, v1, v2, v1, v2}, Landroid/widget/Button;->setPadding(IIII)V

    .line 3413
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 3414
    const/16 v2, 0x409

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3415
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setTextColor(I)V

    .line 3416
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 3418
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3423
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3424
    const v2, 0x20011

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setId(I)V

    .line 3426
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 3427
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v8, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 3428
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v6, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    .line 3430
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 3432
    invoke-virtual {v5, v4, v1, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 3433
    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 3435
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3436
    invoke-direct {p0}, Lnet/hockeyapp/android/f/i;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3437
    invoke-virtual {v0, v1, v2, v1, v2}, Landroid/widget/Button;->setPadding(IIII)V

    .line 3438
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setGravity(I)V

    .line 3439
    const/16 v2, 0x40a

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3440
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setTextColor(I)V

    .line 3441
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 3442
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v5, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 3444
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 4205
    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    .line 4206
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    const v2, 0x20016

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setId(I)V

    .line 4208
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 4210
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v6, v8, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 4212
    iget-object v3, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4213
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, v1, v2}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 4215
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 120
    return-void

    :cond_516
    move v0, v1

    .line 1237
    goto/16 :goto_157

    :cond_519
    move v0, v1

    .line 1260
    goto/16 :goto_1bb
.end method

.method private a()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 123
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 124
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/f/i;->setBackgroundColor(I)V

    .line 125
    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/i;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 126
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 6

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 129
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    .line 130
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    const v1, 0x20012

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 132
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 133
    const/16 v1, 0x31

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 135
    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 137
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 139
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/f/i;->addView(Landroid/view/View;)V

    .line 140
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/EditText;)V
    .registers 10

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 457
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_72

    .line 4467
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 4468
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v2}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 4469
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    .line 4470
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 4471
    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 4472
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4473
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/drawable/ShapeDrawable;->setPadding(IIII)V

    .line 4475
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 4476
    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v3}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 4477
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    .line 4478
    const v4, -0xbbbbbc

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 4479
    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 4480
    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 4481
    invoke-virtual {v2, v6, v6, v6, v0}, Landroid/graphics/drawable/ShapeDrawable;->setPadding(IIII)V

    .line 4483
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    aput-object v2, v3, v6

    const/4 v2, 0x1

    aput-object v1, v3, v2

    invoke-direct {v0, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 459
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 464
    :cond_72
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 143
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    .line 144
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    const v1, 0x20017

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setId(I)V

    .line 146
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 147
    const/4 v1, 0x1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 148
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 150
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    invoke-virtual {v2, v0}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 151
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1, v4, v1, v4}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 153
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 154
    return-void
.end method

.method private c(Landroid/content/Context;)V
    .registers 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 157
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    .line 158
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    const v1, 0x20013

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 160
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 161
    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 162
    const/4 v2, 0x3

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 164
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 165
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 166
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 167
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 169
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->r:Landroid/widget/ScrollView;

    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 170
    return-void
.end method

.method private d(Landroid/content/Context;)V
    .registers 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 173
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    .line 174
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    const v1, 0x20015

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 176
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 177
    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 178
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 180
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 181
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 182
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 183
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 185
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 186
    return-void
.end method

.method private e(Landroid/content/Context;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 189
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    .line 190
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    const v1, 0x20014

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 192
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 193
    const/4 v1, 0x1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 194
    const/4 v2, 0x3

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 196
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4, v1, v4, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 198
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 199
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 201
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 202
    return-void
.end method

.method private f(Landroid/content/Context;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 205
    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    .line 206
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    const v1, 0x20016

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setId(I)V

    .line 208
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 210
    const/4 v1, 0x1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 212
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 213
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    invoke-virtual {v0, v4, v1, v4, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 215
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->w:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 216
    return-void
.end method

.method private g(Landroid/content/Context;)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 219
    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 220
    const/16 v2, 0x2002

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setId(I)V

    .line 222
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 223
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v5, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 224
    div-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v0, v4, v0, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 226
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 227
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 228
    const/16 v2, 0x4001

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 229
    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 230
    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 231
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 232
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 233
    const/16 v2, 0x402

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 234
    const v2, -0x333334

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 235
    invoke-static {p1, v1}, Lnet/hockeyapp/android/f/i;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 237
    invoke-static {}, Lnet/hockeyapp/android/t;->b()Lnet/hockeyapp/android/c/i;

    move-result-object v2

    sget-object v3, Lnet/hockeyapp/android/c/i;->a:Lnet/hockeyapp/android/c/i;

    if-ne v2, v3, :cond_62

    const/16 v0, 0x8

    :cond_62
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 240
    return-void
.end method

.method private getButtonSelector()Landroid/graphics/drawable/Drawable;
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 448
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 449
    new-array v1, v5, [I

    const v2, -0x10100a7

    aput v2, v1, v4

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v3, -0x1000000

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 450
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_3c

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, -0xbbbbbc

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 451
    new-array v1, v5, [I

    const v2, 0x10100a7

    aput v2, v1, v4

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, -0x777778

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 452
    return-object v0

    .line 450
    :array_3c
    .array-data 4
        -0x10100a7
        0x101009c
    .end array-data
.end method

.method private h(Landroid/content/Context;)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 243
    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 244
    const/16 v2, 0x2004

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setId(I)V

    .line 246
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 247
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v5, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 248
    invoke-virtual {v2, v0, v0, v0, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 250
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 252
    const/16 v2, 0x21

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 253
    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 254
    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTextColor(I)V

    .line 255
    const/4 v2, 0x2

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 256
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 257
    const/16 v2, 0x403

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 258
    const v2, -0x333334

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 259
    invoke-static {p1, v1}, Lnet/hockeyapp/android/f/i;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 260
    invoke-static {}, Lnet/hockeyapp/android/t;->c()Lnet/hockeyapp/android/c/i;

    move-result-object v2

    sget-object v3, Lnet/hockeyapp/android/c/i;->a:Lnet/hockeyapp/android/c/i;

    if-ne v2, v3, :cond_60

    const/16 v0, 0x8

    :cond_60
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 263
    return-void
.end method

.method private i(Landroid/content/Context;)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 266
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 267
    const/16 v1, 0x2006

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setId(I)V

    .line 269
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 270
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v5, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 271
    invoke-virtual {v1, v4, v4, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 273
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 274
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 275
    const/16 v1, 0x4031

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 276
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 277
    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 278
    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 279
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 280
    const/16 v1, 0x404

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 281
    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 282
    invoke-static {p1, v0}, Lnet/hockeyapp/android/f/i;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 284
    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 285
    return-void
.end method

.method private j(Landroid/content/Context;)V
    .registers 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 288
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 289
    const/16 v1, 0x2008

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setId(I)V

    .line 291
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 293
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 294
    const/high16 v3, 0x42c80000    # 100.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v6, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 295
    invoke-virtual {v1, v5, v5, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 297
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 298
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 299
    const/16 v1, 0x4001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 300
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 301
    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 302
    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 303
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 304
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setMinimumHeight(I)V

    .line 305
    const/16 v1, 0x405

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 306
    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 307
    invoke-static {p1, v0}, Lnet/hockeyapp/android/f/i;->a(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 309
    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 310
    return-void
.end method

.method private k(Landroid/content/Context;)V
    .registers 9

    .prologue
    const/4 v6, 0x1

    const/4 v2, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 313
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 314
    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 316
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 317
    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 318
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 320
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 321
    invoke-virtual {v0, v4, v2, v4, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 322
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 323
    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v5, v1, v5, v2}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 324
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 325
    const/16 v1, 0x406

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 327
    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 328
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 330
    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 331
    return-void
.end method

.method private l(Landroid/content/Context;)V
    .registers 7

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 334
    new-instance v0, Lnet/hockeyapp/android/f/a;

    invoke-direct {v0, p1}, Lnet/hockeyapp/android/f/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    .line 335
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    const/16 v1, 0x2011

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    .line 337
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 338
    const/4 v1, 0x1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 339
    const/4 v2, 0x3

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 341
    iget-object v2, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 342
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 344
    iget-object v0, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 345
    return-void
.end method

.method private m(Landroid/content/Context;)V
    .registers 11

    .prologue
    const/4 v8, -0x1

    const/high16 v4, 0x41200000    # 10.0f

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 349
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 350
    const/16 v1, 0x2010

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 352
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v6, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 353
    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 354
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v4, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 356
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v8, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 358
    invoke-virtual {v4, v7, v7, v7, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 359
    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 361
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 362
    invoke-direct {p0}, Lnet/hockeyapp/android/f/i;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 363
    invoke-virtual {v0, v2, v1, v2, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 364
    const/16 v1, 0x407

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 365
    invoke-virtual {v0, v8}, Landroid/widget/Button;->setTextColor(I)V

    .line 366
    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 368
    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 369
    return-void
.end method

.method private n(Landroid/content/Context;)V
    .registers 11

    .prologue
    const/4 v8, -0x1

    const/high16 v4, 0x41200000    # 10.0f

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 373
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 374
    const/16 v1, 0x2009

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 376
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v6, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 377
    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 378
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v6, v4, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 380
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v8, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 382
    invoke-virtual {v4, v7, v7, v7, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 383
    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 385
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 386
    invoke-direct {p0}, Lnet/hockeyapp/android/f/i;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 387
    invoke-virtual {v0, v2, v1, v2, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 388
    const/16 v1, 0x408

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 389
    invoke-virtual {v0, v8}, Landroid/widget/Button;->setTextColor(I)V

    .line 390
    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 392
    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 393
    return-void
.end method

.method private o(Landroid/content/Context;)V
    .registers 11

    .prologue
    const/4 v8, -0x1

    const/high16 v3, 0x41200000    # 10.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 397
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 398
    const v1, 0x20010

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 400
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v7, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 401
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v7, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 402
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v7, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 404
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v8, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 406
    invoke-virtual {v4, v6, v6, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 407
    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 408
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v4, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 410
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 411
    invoke-direct {p0}, Lnet/hockeyapp/android/f/i;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 412
    invoke-virtual {v0, v6, v1, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 413
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setGravity(I)V

    .line 414
    const/16 v1, 0x409

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 415
    invoke-virtual {v0, v8}, Landroid/widget/Button;->setTextColor(I)V

    .line 416
    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 418
    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 419
    return-void
.end method

.method private p(Landroid/content/Context;)V
    .registers 11

    .prologue
    const/4 v8, -0x1

    const/high16 v3, 0x41200000    # 10.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 423
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 424
    const v1, 0x20011

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 426
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v7, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 427
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v7, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    .line 428
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lnet/hockeyapp/android/f/i;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v7, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    .line 430
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v4, v8, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 432
    invoke-virtual {v4, v3, v6, v6, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 433
    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 435
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 436
    invoke-direct {p0}, Lnet/hockeyapp/android/f/i;->getButtonSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 437
    invoke-virtual {v0, v6, v1, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 438
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setGravity(I)V

    .line 439
    const/16 v1, 0x40a

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 440
    invoke-virtual {v0, v8}, Landroid/widget/Button;->setTextColor(I)V

    .line 441
    const/4 v1, 0x2

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 442
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v4, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 444
    iget-object v1, p0, Lnet/hockeyapp/android/f/i;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 445
    return-void
.end method

.method private static q(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .registers 9

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 467
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 468
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v2}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 469
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    .line 470
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 471
    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 472
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 473
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/drawable/ShapeDrawable;->setPadding(IIII)V

    .line 475
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 476
    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v3}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 477
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    .line 478
    const v4, -0xbbbbbc

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 479
    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 480
    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 481
    invoke-virtual {v2, v6, v6, v6, v0}, Landroid/graphics/drawable/ShapeDrawable;->setPadding(IIII)V

    .line 483
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    aput-object v2, v3, v6

    const/4 v2, 0x1

    aput-object v1, v3, v2

    invoke-direct {v0, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method
