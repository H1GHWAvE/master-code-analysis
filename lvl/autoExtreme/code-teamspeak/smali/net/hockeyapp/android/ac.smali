.class public final Lnet/hockeyapp/android/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x3

.field static final e:Ljava/lang/String; = "net.hockeyapp.android.EXIT"

.field static f:Ljava/lang/Class;

.field static g:Lnet/hockeyapp/android/ae;

.field private static h:Ljava/lang/String;

.field private static i:Ljava/lang/String;

.field private static j:Landroid/os/Handler;

.field private static k:Ljava/lang/String;

.field private static l:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/4 v0, 0x0

    .line 76
    sput-object v0, Lnet/hockeyapp/android/ac;->h:Ljava/lang/String;

    .line 81
    sput-object v0, Lnet/hockeyapp/android/ac;->i:Ljava/lang/String;

    .line 86
    sput-object v0, Lnet/hockeyapp/android/ac;->j:Landroid/os/Handler;

    .line 91
    sput-object v0, Lnet/hockeyapp/android/ac;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(I)Ljava/lang/String;
    .registers 4

    .prologue
    .line 229
    const-string v0, ""

    .line 230
    const/4 v1, 0x2

    if-ne p0, v1, :cond_2d

    .line 231
    const-string v0, "authorize"

    .line 240
    :cond_7
    :goto_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lnet/hockeyapp/android/ac;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "api/3/apps/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lnet/hockeyapp/android/ac;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/identity/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 233
    :cond_2d
    const/4 v1, 0x1

    if-ne p0, v1, :cond_33

    .line 234
    const-string v0, "check"

    goto :goto_7

    .line 236
    :cond_33
    const/4 v1, 0x3

    if-ne p0, v1, :cond_7

    .line 237
    const-string v0, "validate"

    goto :goto_7
.end method

.method private static a(Landroid/app/Activity;Landroid/content/Intent;)V
    .registers 10

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 171
    if-eqz p1, :cond_12

    .line 172
    const-string v1, "net.hockeyapp.android.EXIT"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 173
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 216
    :cond_11
    :goto_11
    return-void

    .line 178
    :cond_12
    if-eqz p0, :cond_11

    sget v1, Lnet/hockeyapp/android/ac;->l:I

    if-eqz v1, :cond_11

    sget v1, Lnet/hockeyapp/android/ac;->l:I

    if-eq v1, v4, :cond_11

    .line 182
    const-string v1, "net.hockeyapp.android.login"

    invoke-virtual {p0, v1, v6}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 183
    const-string v2, "mode"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 184
    sget v3, Lnet/hockeyapp/android/ac;->l:I

    if-eq v2, v3, :cond_48

    .line 185
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "auid"

    .line 186
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "iuid"

    .line 187
    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "mode"

    sget v5, Lnet/hockeyapp/android/ac;->l:I

    .line 188
    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 185
    invoke-static {v2}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 191
    :cond_48
    const-string v2, "auid"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 192
    const-string v2, "iuid"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 194
    if-nez v3, :cond_71

    if-nez v7, :cond_71

    move v2, v0

    .line 195
    :goto_59
    if-nez v3, :cond_73

    sget v1, Lnet/hockeyapp/android/ac;->l:I

    const/4 v5, 0x2

    if-ne v1, v5, :cond_73

    move v1, v0

    .line 196
    :goto_61
    if-nez v7, :cond_75

    sget v5, Lnet/hockeyapp/android/ac;->l:I

    if-ne v5, v0, :cond_75

    .line 198
    :goto_67
    if-nez v2, :cond_6d

    if-nez v1, :cond_6d

    if-eqz v0, :cond_77

    .line 199
    :cond_6d
    invoke-static {p0}, Lnet/hockeyapp/android/ac;->b(Landroid/content/Context;)V

    goto :goto_11

    :cond_71
    move v2, v6

    .line 194
    goto :goto_59

    :cond_73
    move v1, v6

    .line 195
    goto :goto_61

    :cond_75
    move v0, v6

    .line 196
    goto :goto_67

    .line 203
    :cond_77
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 204
    if-eqz v3, :cond_9d

    .line 205
    const-string v0, "type"

    const-string v1, "auid"

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    const-string v0, "id"

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    :cond_8a
    :goto_8a
    new-instance v0, Lnet/hockeyapp/android/d/p;

    sget-object v2, Lnet/hockeyapp/android/ac;->j:Landroid/os/Handler;

    invoke-static {v4}, Lnet/hockeyapp/android/ac;->a(I)Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lnet/hockeyapp/android/d/p;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V

    .line 3095
    iput-boolean v6, v0, Lnet/hockeyapp/android/d/p;->d:Z

    .line 215
    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    goto/16 :goto_11

    .line 208
    :cond_9d
    if-eqz v7, :cond_8a

    .line 209
    const-string v0, "type"

    const-string v1, "iuid"

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    const-string v0, "id"

    invoke-interface {v5, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8a
.end method

.method static synthetic a(Landroid/content/Context;)V
    .registers 1

    .prologue
    .line 52
    invoke-static {p0}, Lnet/hockeyapp/android/ac;->b(Landroid/content/Context;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 6

    .prologue
    .line 122
    const-string v0, "https://sdk.hockeyapp.net/"

    .line 2136
    if-eqz p0, :cond_21

    .line 2137
    invoke-static {p1}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/hockeyapp/android/ac;->h:Ljava/lang/String;

    .line 2138
    sput-object p2, Lnet/hockeyapp/android/ac;->i:Ljava/lang/String;

    .line 2139
    sput-object v0, Lnet/hockeyapp/android/ac;->k:Ljava/lang/String;

    .line 2140
    sput p3, Lnet/hockeyapp/android/ac;->l:I

    .line 2141
    const/4 v0, 0x0

    sput-object v0, Lnet/hockeyapp/android/ac;->f:Ljava/lang/Class;

    .line 2143
    sget-object v0, Lnet/hockeyapp/android/ac;->j:Landroid/os/Handler;

    if-nez v0, :cond_1e

    .line 2144
    new-instance v0, Lnet/hockeyapp/android/ad;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/ad;-><init>(Landroid/content/Context;)V

    sput-object v0, Lnet/hockeyapp/android/ac;->j:Landroid/os/Handler;

    .line 2157
    :cond_1e
    invoke-static {p0}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 123
    :cond_21
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILnet/hockeyapp/android/ae;)V
    .registers 7

    .prologue
    .line 108
    sput-object p4, Lnet/hockeyapp/android/ac;->g:Lnet/hockeyapp/android/ae;

    .line 1122
    const-string v0, "https://sdk.hockeyapp.net/"

    .line 1136
    if-eqz p0, :cond_23

    .line 1137
    invoke-static {p1}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/hockeyapp/android/ac;->h:Ljava/lang/String;

    .line 1138
    sput-object p2, Lnet/hockeyapp/android/ac;->i:Ljava/lang/String;

    .line 1139
    sput-object v0, Lnet/hockeyapp/android/ac;->k:Ljava/lang/String;

    .line 1140
    sput p3, Lnet/hockeyapp/android/ac;->l:I

    .line 1141
    const/4 v0, 0x0

    sput-object v0, Lnet/hockeyapp/android/ac;->f:Ljava/lang/Class;

    .line 1143
    sget-object v0, Lnet/hockeyapp/android/ac;->j:Landroid/os/Handler;

    if-nez v0, :cond_20

    .line 1144
    new-instance v0, Lnet/hockeyapp/android/ad;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/ad;-><init>(Landroid/content/Context;)V

    sput-object v0, Lnet/hockeyapp/android/ac;->j:Landroid/os/Handler;

    .line 1157
    :cond_20
    invoke-static {p0}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 110
    :cond_23
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Class;)V
    .registers 7

    .prologue
    .line 136
    if-eqz p0, :cond_1e

    .line 137
    invoke-static {p1}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/hockeyapp/android/ac;->h:Ljava/lang/String;

    .line 138
    sput-object p2, Lnet/hockeyapp/android/ac;->i:Ljava/lang/String;

    .line 139
    sput-object p3, Lnet/hockeyapp/android/ac;->k:Ljava/lang/String;

    .line 140
    sput p4, Lnet/hockeyapp/android/ac;->l:I

    .line 141
    sput-object p5, Lnet/hockeyapp/android/ac;->f:Ljava/lang/Class;

    .line 143
    sget-object v0, Lnet/hockeyapp/android/ac;->j:Landroid/os/Handler;

    if-nez v0, :cond_1b

    .line 144
    new-instance v0, Lnet/hockeyapp/android/ad;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/ad;-><init>(Landroid/content/Context;)V

    sput-object v0, Lnet/hockeyapp/android/ac;->j:Landroid/os/Handler;

    .line 157
    :cond_1b
    invoke-static {p0}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 159
    :cond_1e
    return-void
.end method

.method private static b(Landroid/content/Context;)V
    .registers 4

    .prologue
    .line 219
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 220
    const/high16 v1, 0x50000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 221
    const-class v1, Lnet/hockeyapp/android/aa;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 222
    const-string v1, "url"

    sget v2, Lnet/hockeyapp/android/ac;->l:I

    invoke-static {v2}, Lnet/hockeyapp/android/ac;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const-string v1, "mode"

    sget v2, Lnet/hockeyapp/android/ac;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 224
    const-string v1, "secret"

    sget-object v2, Lnet/hockeyapp/android/ac;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 226
    return-void
.end method
