.class final Lnet/hockeyapp/android/e/e;
.super Lnet/hockeyapp/android/e/c;
.source "SourceFile"


# static fields
.field public static final c:I = 0x13

.field static final synthetic h:Z

.field private static final i:[B

.field private static final j:[B


# instance fields
.field d:I

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field private final k:[B

.field private l:I

.field private final m:[B


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/16 v1, 0x40

    .line 545
    const-class v0, Lnet/hockeyapp/android/e/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    :goto_b
    sput-boolean v0, Lnet/hockeyapp/android/e/e;->h:Z

    .line 557
    new-array v0, v1, [B

    fill-array-data v0, :array_1e

    sput-object v0, Lnet/hockeyapp/android/e/e;->i:[B

    .line 568
    new-array v0, v1, [B

    fill-array-data v0, :array_42

    sput-object v0, Lnet/hockeyapp/android/e/e;->j:[B

    return-void

    .line 545
    :cond_1c
    const/4 v0, 0x0

    goto :goto_b

    .line 557
    :array_1e
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
        0x67t
        0x68t
        0x69t
        0x6at
        0x6bt
        0x6ct
        0x6dt
        0x6et
        0x6ft
        0x70t
        0x71t
        0x72t
        0x73t
        0x74t
        0x75t
        0x76t
        0x77t
        0x78t
        0x79t
        0x7at
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x2bt
        0x2ft
    .end array-data

    .line 568
    :array_42
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
        0x67t
        0x68t
        0x69t
        0x6at
        0x6bt
        0x6ct
        0x6dt
        0x6et
        0x6ft
        0x70t
        0x71t
        0x72t
        0x73t
        0x74t
        0x75t
        0x76t
        0x77t
        0x78t
        0x79t
        0x7at
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x2dt
        0x5ft
    .end array-data
.end method

.method public constructor <init>(I)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 584
    invoke-direct {p0}, Lnet/hockeyapp/android/e/c;-><init>()V

    .line 585
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/e/e;->a:[B

    .line 587
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_34

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lnet/hockeyapp/android/e/e;->e:Z

    .line 588
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_36

    move v0, v1

    :goto_14
    iput-boolean v0, p0, Lnet/hockeyapp/android/e/e;->f:Z

    .line 589
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_38

    :goto_1a
    iput-boolean v1, p0, Lnet/hockeyapp/android/e/e;->g:Z

    .line 590
    and-int/lit8 v0, p1, 0x8

    if-nez v0, :cond_3a

    sget-object v0, Lnet/hockeyapp/android/e/e;->i:[B

    :goto_22
    iput-object v0, p0, Lnet/hockeyapp/android/e/e;->m:[B

    .line 592
    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lnet/hockeyapp/android/e/e;->k:[B

    .line 593
    iput v2, p0, Lnet/hockeyapp/android/e/e;->d:I

    .line 595
    iget-boolean v0, p0, Lnet/hockeyapp/android/e/e;->f:Z

    if-eqz v0, :cond_3d

    const/16 v0, 0x13

    :goto_31
    iput v0, p0, Lnet/hockeyapp/android/e/e;->l:I

    .line 596
    return-void

    :cond_34
    move v0, v2

    .line 587
    goto :goto_d

    :cond_36
    move v0, v2

    .line 588
    goto :goto_14

    :cond_38
    move v1, v2

    .line 589
    goto :goto_1a

    .line 590
    :cond_3a
    sget-object v0, Lnet/hockeyapp/android/e/e;->j:[B

    goto :goto_22

    .line 595
    :cond_3d
    const/4 v0, -0x1

    goto :goto_31
.end method


# virtual methods
.method public final a(I)I
    .registers 3

    .prologue
    .line 603
    mul-int/lit8 v0, p1, 0x8

    div-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public final a([BII)Z
    .registers 14

    .prologue
    .line 608
    iget-object v6, p0, Lnet/hockeyapp/android/e/e;->m:[B

    .line 609
    iget-object v7, p0, Lnet/hockeyapp/android/e/e;->a:[B

    .line 610
    const/4 v4, 0x0

    .line 611
    iget v1, p0, Lnet/hockeyapp/android/e/e;->l:I

    .line 614
    add-int v8, p3, p2

    .line 615
    const/4 v0, -0x1

    .line 621
    iget v2, p0, Lnet/hockeyapp/android/e/e;->d:I

    packed-switch v2, :pswitch_data_216

    :cond_f
    move v2, p2

    move v3, v0

    .line 648
    :goto_11
    const/4 v0, -0x1

    if-eq v3, v0, :cond_212

    .line 649
    const/4 v0, 0x0

    shr-int/lit8 v4, v3, 0x12

    and-int/lit8 v4, v4, 0x3f

    aget-byte v4, v6, v4

    aput-byte v4, v7, v0

    .line 650
    const/4 v0, 0x1

    shr-int/lit8 v4, v3, 0xc

    and-int/lit8 v4, v4, 0x3f

    aget-byte v4, v6, v4

    aput-byte v4, v7, v0

    .line 651
    const/4 v0, 0x2

    shr-int/lit8 v4, v3, 0x6

    and-int/lit8 v4, v4, 0x3f

    aget-byte v4, v6, v4

    aput-byte v4, v7, v0

    .line 652
    const/4 v4, 0x3

    const/4 v0, 0x4

    and-int/lit8 v3, v3, 0x3f

    aget-byte v3, v6, v3

    aput-byte v3, v7, v4

    .line 653
    add-int/lit8 v1, v1, -0x1

    if-nez v1, :cond_20e

    .line 654
    iget-boolean v1, p0, Lnet/hockeyapp/android/e/e;->g:Z

    if-eqz v1, :cond_45

    const/4 v1, 0x4

    const/4 v0, 0x5

    const/16 v3, 0xd

    aput-byte v3, v7, v1

    .line 655
    :cond_45
    add-int/lit8 v4, v0, 0x1

    const/16 v1, 0xa

    aput-byte v1, v7, v0

    .line 656
    const/16 v0, 0x13

    move v5, v0

    .line 665
    :goto_4e
    add-int/lit8 v0, v2, 0x3

    if-gt v0, v8, :cond_f4

    .line 666
    aget-byte v0, p1, v2

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    add-int/lit8 v1, v2, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, v2, 0x2

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 669
    shr-int/lit8 v1, v0, 0x12

    and-int/lit8 v1, v1, 0x3f

    aget-byte v1, v6, v1

    aput-byte v1, v7, v4

    .line 670
    add-int/lit8 v1, v4, 0x1

    shr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v3, v6, v3

    aput-byte v3, v7, v1

    .line 671
    add-int/lit8 v1, v4, 0x2

    shr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-byte v3, v6, v3

    aput-byte v3, v7, v1

    .line 672
    add-int/lit8 v1, v4, 0x3

    and-int/lit8 v0, v0, 0x3f

    aget-byte v0, v6, v0

    aput-byte v0, v7, v1

    .line 673
    add-int/lit8 v2, v2, 0x3

    .line 674
    add-int/lit8 v1, v4, 0x4

    .line 675
    add-int/lit8 v0, v5, -0x1

    if-nez v0, :cond_20a

    .line 676
    iget-boolean v0, p0, Lnet/hockeyapp/android/e/e;->g:Z

    if-eqz v0, :cond_207

    add-int/lit8 v0, v1, 0x1

    const/16 v3, 0xd

    aput-byte v3, v7, v1

    .line 677
    :goto_9e
    add-int/lit8 v4, v0, 0x1

    const/16 v1, 0xa

    aput-byte v1, v7, v0

    .line 678
    const/16 v0, 0x13

    move v5, v0

    goto :goto_4e

    :pswitch_a8
    move v2, p2

    move v3, v0

    .line 624
    goto/16 :goto_11

    .line 627
    :pswitch_ac
    add-int/lit8 v2, p2, 0x2

    if-gt v2, v8, :cond_f

    .line 630
    iget-object v0, p0, Lnet/hockeyapp/android/e/e;->k:[B

    const/4 v2, 0x0

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    add-int/lit8 v2, p2, 0x1

    aget-byte v3, p1, p2

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v0, v3

    add-int/lit8 p2, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v0, v2

    .line 633
    const/4 v2, 0x0

    iput v2, p0, Lnet/hockeyapp/android/e/e;->d:I

    move v2, p2

    move v3, v0

    goto/16 :goto_11

    .line 638
    :pswitch_d0
    add-int/lit8 v2, p2, 0x1

    if-gt v2, v8, :cond_f

    .line 640
    iget-object v0, p0, Lnet/hockeyapp/android/e/e;->k:[B

    const/4 v2, 0x0

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    iget-object v2, p0, Lnet/hockeyapp/android/e/e;->k:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    add-int/lit8 v2, p2, 0x1

    aget-byte v3, p1, p2

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    .line 643
    const/4 v3, 0x0

    iput v3, p0, Lnet/hockeyapp/android/e/e;->d:I

    move v3, v0

    goto/16 :goto_11

    .line 688
    :cond_f4
    iget v0, p0, Lnet/hockeyapp/android/e/e;->d:I

    sub-int v0, v2, v0

    add-int/lit8 v1, v8, -0x1

    if-ne v0, v1, :cond_15d

    .line 689
    const/4 v1, 0x0

    .line 690
    iget v0, p0, Lnet/hockeyapp/android/e/e;->d:I

    if-lez v0, :cond_157

    iget-object v0, p0, Lnet/hockeyapp/android/e/e;->k:[B

    const/4 v3, 0x0

    const/4 v1, 0x1

    aget-byte v0, v0, v3

    :goto_107
    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v3, v0, 0x4

    .line 691
    iget v0, p0, Lnet/hockeyapp/android/e/e;->d:I

    sub-int/2addr v0, v1

    iput v0, p0, Lnet/hockeyapp/android/e/e;->d:I

    .line 692
    add-int/lit8 v1, v4, 0x1

    shr-int/lit8 v0, v3, 0x6

    and-int/lit8 v0, v0, 0x3f

    aget-byte v0, v6, v0

    aput-byte v0, v7, v4

    .line 693
    add-int/lit8 v0, v1, 0x1

    and-int/lit8 v3, v3, 0x3f

    aget-byte v3, v6, v3

    aput-byte v3, v7, v1

    .line 694
    iget-boolean v1, p0, Lnet/hockeyapp/android/e/e;->e:Z

    if-eqz v1, :cond_132

    .line 695
    add-int/lit8 v1, v0, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v0

    .line 696
    add-int/lit8 v0, v1, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v1

    .line 698
    :cond_132
    iget-boolean v1, p0, Lnet/hockeyapp/android/e/e;->f:Z

    if-eqz v1, :cond_148

    .line 699
    iget-boolean v1, p0, Lnet/hockeyapp/android/e/e;->g:Z

    if-eqz v1, :cond_141

    add-int/lit8 v1, v0, 0x1

    const/16 v3, 0xd

    aput-byte v3, v7, v0

    move v0, v1

    .line 700
    :cond_141
    add-int/lit8 v1, v0, 0x1

    const/16 v3, 0xa

    aput-byte v3, v7, v0

    move v0, v1

    :cond_148
    move v4, v0

    .line 722
    :cond_149
    :goto_149
    sget-boolean v0, Lnet/hockeyapp/android/e/e;->h:Z

    if-nez v0, :cond_1f1

    iget v0, p0, Lnet/hockeyapp/android/e/e;->d:I

    if-eqz v0, :cond_1f1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 690
    :cond_157
    add-int/lit8 v3, v2, 0x1

    aget-byte v0, p1, v2

    move v2, v3

    goto :goto_107

    .line 702
    :cond_15d
    iget v0, p0, Lnet/hockeyapp/android/e/e;->d:I

    sub-int v0, v2, v0

    add-int/lit8 v1, v8, -0x2

    if-ne v0, v1, :cond_1d5

    .line 703
    const/4 v1, 0x0

    .line 704
    iget v0, p0, Lnet/hockeyapp/android/e/e;->d:I

    const/4 v3, 0x1

    if-le v0, v3, :cond_1c9

    iget-object v0, p0, Lnet/hockeyapp/android/e/e;->k:[B

    const/4 v3, 0x0

    const/4 v1, 0x1

    aget-byte v0, v0, v3

    :goto_171
    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v9, v0, 0xa

    iget v0, p0, Lnet/hockeyapp/android/e/e;->d:I

    if-lez v0, :cond_1cf

    iget-object v0, p0, Lnet/hockeyapp/android/e/e;->k:[B

    add-int/lit8 v3, v1, 0x1

    aget-byte v0, v0, v1

    move v1, v3

    :goto_180
    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x2

    or-int/2addr v0, v9

    .line 706
    iget v3, p0, Lnet/hockeyapp/android/e/e;->d:I

    sub-int v1, v3, v1

    iput v1, p0, Lnet/hockeyapp/android/e/e;->d:I

    .line 707
    add-int/lit8 v1, v4, 0x1

    shr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v3, v6, v3

    aput-byte v3, v7, v4

    .line 708
    add-int/lit8 v3, v1, 0x1

    shr-int/lit8 v4, v0, 0x6

    and-int/lit8 v4, v4, 0x3f

    aget-byte v4, v6, v4

    aput-byte v4, v7, v1

    .line 709
    add-int/lit8 v1, v3, 0x1

    and-int/lit8 v0, v0, 0x3f

    aget-byte v0, v6, v0

    aput-byte v0, v7, v3

    .line 710
    iget-boolean v0, p0, Lnet/hockeyapp/android/e/e;->e:Z

    if-eqz v0, :cond_205

    .line 711
    add-int/lit8 v0, v1, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v1

    .line 713
    :goto_1b1
    iget-boolean v1, p0, Lnet/hockeyapp/android/e/e;->f:Z

    if-eqz v1, :cond_1c7

    .line 714
    iget-boolean v1, p0, Lnet/hockeyapp/android/e/e;->g:Z

    if-eqz v1, :cond_1c0

    add-int/lit8 v1, v0, 0x1

    const/16 v3, 0xd

    aput-byte v3, v7, v0

    move v0, v1

    .line 715
    :cond_1c0
    add-int/lit8 v1, v0, 0x1

    const/16 v3, 0xa

    aput-byte v3, v7, v0

    move v0, v1

    :cond_1c7
    move v4, v0

    .line 717
    goto :goto_149

    .line 704
    :cond_1c9
    add-int/lit8 v3, v2, 0x1

    aget-byte v0, p1, v2

    move v2, v3

    goto :goto_171

    :cond_1cf
    add-int/lit8 v3, v2, 0x1

    aget-byte v0, p1, v2

    move v2, v3

    goto :goto_180

    .line 717
    :cond_1d5
    iget-boolean v0, p0, Lnet/hockeyapp/android/e/e;->f:Z

    if-eqz v0, :cond_149

    if-lez v4, :cond_149

    const/16 v0, 0x13

    if-eq v5, v0, :cond_149

    .line 718
    iget-boolean v0, p0, Lnet/hockeyapp/android/e/e;->g:Z

    if-eqz v0, :cond_203

    add-int/lit8 v0, v4, 0x1

    const/16 v1, 0xd

    aput-byte v1, v7, v4

    .line 719
    :goto_1e9
    add-int/lit8 v4, v0, 0x1

    const/16 v1, 0xa

    aput-byte v1, v7, v0

    goto/16 :goto_149

    .line 723
    :cond_1f1
    sget-boolean v0, Lnet/hockeyapp/android/e/e;->h:Z

    if-nez v0, :cond_1fd

    if-eq v2, v8, :cond_1fd

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 736
    :cond_1fd
    iput v4, p0, Lnet/hockeyapp/android/e/e;->b:I

    .line 737
    iput v5, p0, Lnet/hockeyapp/android/e/e;->l:I

    .line 739
    const/4 v0, 0x1

    return v0

    :cond_203
    move v0, v4

    goto :goto_1e9

    :cond_205
    move v0, v1

    goto :goto_1b1

    :cond_207
    move v0, v1

    goto/16 :goto_9e

    :cond_20a
    move v5, v0

    move v4, v1

    goto/16 :goto_4e

    :cond_20e
    move v5, v1

    move v4, v0

    goto/16 :goto_4e

    :cond_212
    move v5, v1

    goto/16 :goto_4e

    .line 621
    nop

    :pswitch_data_216
    .packed-switch 0x0
        :pswitch_a8
        :pswitch_ac
        :pswitch_d0
    .end packed-switch
.end method
