.class public Ljnamed;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:I = 0x1

.field static final b:I = 0x2


# instance fields
.field c:Ljava/util/Map;

.field d:Ljava/util/Map;

.field e:Ljava/util/Map;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 12

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 30
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 32
    :try_start_d
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 33
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 34
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_1c} :catch_66

    .line 42
    :try_start_1c
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Ljnamed;->c:Ljava/util/Map;

    .line 43
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Ljnamed;->d:Ljava/util/Map;

    .line 44
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Ljnamed;->e:Ljava/util/Map;

    .line 47
    :cond_31
    :goto_31
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_154

    .line 48
    new-instance v6, Ljava/util/StringTokenizer;

    invoke-direct {v6, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_31

    .line 51
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    .line 52
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-nez v8, :cond_7c

    .line 53
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    const-string v8, "Invalid line: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_60
    .catchall {:try_start_1c .. :try_end_60} :catchall_61

    goto :goto_31

    .line 106
    :catchall_61
    move-exception v1

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    throw v1

    .line 37
    :catch_66
    move-exception v1

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Cannot open "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 107
    :goto_7b
    return-void

    .line 56
    :cond_7c
    const/4 v1, 0x0

    :try_start_7d
    invoke-virtual {v7, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v8, 0x23

    if-eq v1, v8, :cond_31

    .line 58
    const-string v1, "primary"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_ad

    .line 59
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 1112
    const/4 v1, 0x0

    .line 1113
    if-eqz v7, :cond_9e

    .line 1114
    sget-object v1, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    invoke-static {v7, v1}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 1115
    :cond_9e
    new-instance v7, Lorg/xbill/DNS/Zone;

    invoke-direct {v7, v1, v6}, Lorg/xbill/DNS/Zone;-><init>(Lorg/xbill/DNS/Name;Ljava/lang/String;)V

    .line 1116
    iget-object v1, p0, Ljnamed;->d:Ljava/util/Map;

    invoke-virtual {v7}, Lorg/xbill/DNS/Zone;->getOrigin()Lorg/xbill/DNS/Name;

    move-result-object v6

    invoke-interface {v1, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_31

    .line 60
    :cond_ad
    const-string v1, "secondary"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d0

    .line 61
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 1123
    sget-object v7, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    invoke-static {v1, v7}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 1124
    new-instance v7, Lorg/xbill/DNS/Zone;

    const/4 v8, 0x1

    invoke-direct {v7, v1, v8, v6}, Lorg/xbill/DNS/Zone;-><init>(Lorg/xbill/DNS/Name;ILjava/lang/String;)V

    .line 1125
    iget-object v6, p0, Ljnamed;->d:Ljava/util/Map;

    invoke-interface {v6, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_31

    .line 63
    :cond_d0
    const-string v1, "cache"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_ee

    .line 64
    new-instance v1, Lorg/xbill/DNS/Cache;

    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Lorg/xbill/DNS/Cache;-><init>(Ljava/lang/String;)V

    .line 65
    iget-object v6, p0, Ljnamed;->c:Ljava/util/Map;

    new-instance v7, Ljava/lang/Integer;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_31

    .line 66
    :cond_ee
    const-string v1, "key"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_114

    .line 67
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    .line 69
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_10d

    .line 70
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v7, v6}, Ljnamed;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_31

    .line 72
    :cond_10d
    const-string v6, "hmac-md5"

    invoke-direct {p0, v6, v1, v7}, Ljnamed;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_31

    .line 73
    :cond_114
    const-string v1, "port"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_129

    .line 74
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_31

    .line 75
    :cond_129
    const-string v1, "address"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13e

    .line 76
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-static {v1}, Lorg/xbill/DNS/Address;->getByAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_31

    .line 79
    :cond_13e
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    const-string v8, "unknown keyword: "

    invoke-direct {v6, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_31

    .line 85
    :cond_154
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_164

    .line 86
    new-instance v1, Ljava/lang/Integer;

    const/16 v5, 0x35

    invoke-direct {v1, v5}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_164
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_173

    .line 89
    const-string v1, "0.0.0.0"

    invoke-static {v1}, Lorg/xbill/DNS/Address;->getByAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_173
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 92
    :cond_177
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1cc

    .line 93
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/net/InetAddress;

    move-object v2, v0

    .line 94
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 95
    :goto_189
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_177

    .line 96
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1613
    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lc;

    invoke-direct {v8, p0, v2, v1}, Lc;-><init>(Ljnamed;Ljava/net/InetAddress;I)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1615
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 2605
    new-instance v7, Ljava/lang/Thread;

    new-instance v8, Lb;

    invoke-direct {v8, p0, v2, v1}, Lb;-><init>(Ljnamed;Ljava/net/InetAddress;I)V

    invoke-direct {v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2607
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 99
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuffer;

    const-string v9, "jnamed: listening on "

    invoke-direct {v8, v9}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v1}, Ljnamed;->a(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_189

    .line 103
    :cond_1cc
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "jnamed: running"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1d3
    .catchall {:try_start_7d .. :try_end_1d3} :catchall_61

    .line 106
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_7b
.end method

.method private a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Name;IIII)B
    .registers 22

    .prologue
    .line 259
    const/4 v3, 0x0

    .line 261
    const/4 v2, 0x6

    move/from16 v0, p5

    if-le v0, v2, :cond_8

    .line 262
    const/4 v2, 0x0

    .line 341
    :goto_7
    return v2

    .line 264
    :cond_8
    const/16 v2, 0x18

    move/from16 v0, p3

    if-eq v0, v2, :cond_14

    const/16 v2, 0x2e

    move/from16 v0, p3

    if-ne v0, v2, :cond_167

    .line 265
    :cond_14
    const/16 v5, 0xff

    .line 266
    or-int/lit8 v8, p6, 0x2

    .line 269
    :goto_18
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Ljnamed;->a(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Zone;

    move-result-object v4

    .line 270
    if-eqz v4, :cond_5a

    .line 271
    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v5}, Lorg/xbill/DNS/Zone;->findRecords(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/SetResponse;

    move-result-object v2

    .line 277
    :goto_26
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->isUnknown()Z

    move-result v6

    if-eqz v6, :cond_39

    .line 278
    move/from16 v0, p4

    invoke-direct {p0, v0}, Ljnamed;->a(I)Lorg/xbill/DNS/Cache;

    move-result-object v6

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v6, v1}, Ljnamed;->a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Cache;Lorg/xbill/DNS/Name;)V

    .line 280
    :cond_39
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->isNXDOMAIN()Z

    move-result v6

    if-eqz v6, :cond_68

    .line 281
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lorg/xbill/DNS/Header;->setRcode(I)V

    .line 282
    if-eqz v4, :cond_58

    .line 283
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ljnamed;->a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Zone;)V

    .line 284
    if-nez p5, :cond_58

    .line 285
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lorg/xbill/DNS/Header;->setFlag(I)V

    .line 287
    :cond_58
    const/4 v2, 0x3

    goto :goto_7

    .line 273
    :cond_5a
    move/from16 v0, p4

    invoke-direct {p0, v0}, Ljnamed;->a(I)Lorg/xbill/DNS/Cache;

    move-result-object v2

    .line 274
    const/4 v6, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v5, v6}, Lorg/xbill/DNS/Cache;->lookupRecords(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/SetResponse;

    move-result-object v2

    goto :goto_26

    .line 289
    :cond_68
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->isNXRRSET()Z

    move-result v6

    if-eqz v6, :cond_81

    .line 290
    if-eqz v4, :cond_164

    .line 291
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ljnamed;->a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Zone;)V

    .line 292
    if-nez p5, :cond_164

    .line 293
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v2

    const/4 v4, 0x5

    invoke-virtual {v2, v4}, Lorg/xbill/DNS/Header;->setFlag(I)V

    move v2, v3

    goto :goto_7

    .line 296
    :cond_81
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->isDelegation()Z

    move-result v6

    if-eqz v6, :cond_98

    .line 297
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->getNS()Lorg/xbill/DNS/RRset;

    move-result-object v2

    .line 298
    invoke-virtual {v2}, Lorg/xbill/DNS/RRset;->getName()Lorg/xbill/DNS/Name;

    move-result-object v4

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-static {v4, v0, v2, v5, v8}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    move v2, v3

    .line 300
    goto/16 :goto_7

    .line 301
    :cond_98
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->isCNAME()Z

    move-result v6

    if-eqz v6, :cond_cc

    .line 302
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->getCNAME()Lorg/xbill/DNS/CNAMERecord;

    move-result-object v2

    .line 303
    new-instance v3, Lorg/xbill/DNS/RRset;

    invoke-direct {v3, v2}, Lorg/xbill/DNS/RRset;-><init>(Lorg/xbill/DNS/Record;)V

    .line 304
    const/4 v6, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3, v6, v8}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    .line 305
    if-eqz v4, :cond_bb

    if-nez p5, :cond_bb

    .line 306
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lorg/xbill/DNS/Header;->setFlag(I)V

    .line 307
    :cond_bb
    invoke-virtual {v2}, Lorg/xbill/DNS/CNAMERecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v4

    add-int/lit8 v7, p5, 0x1

    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v6, p4

    invoke-direct/range {v2 .. v8}, Ljnamed;->a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Name;IIII)B

    move-result v2

    goto/16 :goto_7

    .line 310
    :cond_cc
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->isDNAME()Z

    move-result v6

    if-eqz v6, :cond_11f

    .line 311
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->getDNAME()Lorg/xbill/DNS/DNAMERecord;

    move-result-object v2

    .line 312
    new-instance v3, Lorg/xbill/DNS/RRset;

    invoke-direct {v3, v2}, Lorg/xbill/DNS/RRset;-><init>(Lorg/xbill/DNS/Record;)V

    .line 313
    const/4 v6, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3, v6, v8}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    .line 316
    :try_start_e3
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lorg/xbill/DNS/Name;->fromDNAME(Lorg/xbill/DNS/DNAMERecord;)Lorg/xbill/DNS/Name;
    :try_end_e8
    .catch Lorg/xbill/DNS/NameTooLongException; {:try_start_e3 .. :try_end_e8} :catch_11b

    move-result-object v14

    .line 321
    new-instance v2, Lorg/xbill/DNS/RRset;

    new-instance v9, Lorg/xbill/DNS/CNAMERecord;

    const-wide/16 v12, 0x0

    move-object/from16 v10, p2

    move/from16 v11, p4

    invoke-direct/range {v9 .. v14}, Lorg/xbill/DNS/CNAMERecord;-><init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;)V

    invoke-direct {v2, v9}, Lorg/xbill/DNS/RRset;-><init>(Lorg/xbill/DNS/Record;)V

    .line 322
    const/4 v3, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2, v3, v8}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    .line 323
    if-eqz v4, :cond_10d

    if-nez p5, :cond_10d

    .line 324
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lorg/xbill/DNS/Header;->setFlag(I)V

    .line 325
    :cond_10d
    add-int/lit8 v7, p5, 0x1

    move-object v2, p0

    move-object/from16 v3, p1

    move-object v4, v14

    move/from16 v6, p4

    invoke-direct/range {v2 .. v8}, Ljnamed;->a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Name;IIII)B

    move-result v2

    goto/16 :goto_7

    .line 319
    :catch_11b
    move-exception v2

    const/4 v2, 0x6

    goto/16 :goto_7

    .line 328
    :cond_11f
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->isSuccessful()Z

    move-result v5

    if-eqz v5, :cond_164

    .line 329
    invoke-virtual {v2}, Lorg/xbill/DNS/SetResponse;->answers()[Lorg/xbill/DNS/RRset;

    move-result-object v5

    .line 330
    const/4 v2, 0x0

    :goto_12a
    array-length v6, v5

    if-ge v2, v6, :cond_13a

    .line 331
    aget-object v6, v5, v2

    const/4 v7, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1, v6, v7, v8}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    .line 330
    add-int/lit8 v2, v2, 0x1

    goto :goto_12a

    .line 333
    :cond_13a
    if-eqz v4, :cond_157

    .line 4211
    invoke-virtual {v4}, Lorg/xbill/DNS/Zone;->getNS()Lorg/xbill/DNS/RRset;

    move-result-object v2

    .line 4212
    invoke-virtual {v2}, Lorg/xbill/DNS/RRset;->getName()Lorg/xbill/DNS/Name;

    move-result-object v4

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-static {v4, v0, v2, v5, v8}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    .line 335
    if-nez p5, :cond_164

    .line 336
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v2

    const/4 v4, 0x5

    invoke-virtual {v2, v4}, Lorg/xbill/DNS/Header;->setFlag(I)V

    move v2, v3

    goto/16 :goto_7

    .line 339
    :cond_157
    move/from16 v0, p4

    invoke-direct {p0, v0}, Ljnamed;->a(I)Lorg/xbill/DNS/Cache;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v2, v1}, Ljnamed;->a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Cache;Lorg/xbill/DNS/Name;)V

    :cond_164
    move v2, v3

    goto/16 :goto_7

    :cond_167
    move/from16 v8, p6

    move/from16 v5, p3

    goto/16 :goto_18
.end method

.method static a(Ljava/net/InetAddress;I)Ljava/lang/String;
    .registers 4

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lorg/xbill/DNS/Cache;
    .registers 5

    .prologue
    .line 136
    iget-object v0, p0, Ljnamed;->c:Ljava/util/Map;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xbill/DNS/Cache;

    .line 137
    if-nez v0, :cond_1e

    .line 138
    new-instance v0, Lorg/xbill/DNS/Cache;

    invoke-direct {v0, p1}, Lorg/xbill/DNS/Cache;-><init>(I)V

    .line 139
    iget-object v1, p0, Ljnamed;->c:Ljava/util/Map;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    :cond_1e
    return-object v0
.end method

.method private a(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Zone;
    .registers 6

    .prologue
    .line 147
    iget-object v0, p0, Ljnamed;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xbill/DNS/Zone;

    .line 148
    if-eqz v0, :cond_b

    .line 157
    :cond_a
    :goto_a
    return-object v0

    .line 150
    :cond_b
    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->labels()I

    move-result v2

    .line 151
    const/4 v0, 0x1

    move v1, v0

    :goto_11
    if-ge v1, v2, :cond_26

    .line 152
    new-instance v0, Lorg/xbill/DNS/Name;

    invoke-direct {v0, p1, v1}, Lorg/xbill/DNS/Name;-><init>(Lorg/xbill/DNS/Name;I)V

    .line 153
    iget-object v3, p0, Ljnamed;->d:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xbill/DNS/Zone;

    .line 154
    if-nez v0, :cond_a

    .line 151
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    .line 157
    :cond_26
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 112
    const/4 v0, 0x0

    .line 113
    if-eqz p1, :cond_9

    .line 114
    sget-object v0, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v0

    .line 115
    :cond_9
    new-instance v1, Lorg/xbill/DNS/Zone;

    invoke-direct {v1, v0, p2}, Lorg/xbill/DNS/Zone;-><init>(Lorg/xbill/DNS/Name;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Ljnamed;->d:Ljava/util/Map;

    invoke-virtual {v1}, Lorg/xbill/DNS/Zone;->getOrigin()Lorg/xbill/DNS/Name;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7

    .prologue
    .line 130
    sget-object v0, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    invoke-static {p2, v0}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v0

    .line 131
    iget-object v1, p0, Ljnamed;->e:Ljava/util/Map;

    new-instance v2, Lorg/xbill/DNS/TSIG;

    invoke-direct {v2, p1, p2, p3}, Lorg/xbill/DNS/TSIG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    return-void
.end method

.method private a(Ljava/net/Socket;)V
    .registers 7

    .prologue
    .line 500
    :try_start_0
    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 501
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 502
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 503
    new-array v2, v0, [B

    .line 504
    invoke-virtual {v1, v2}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_12} :catch_3c
    .catchall {:try_start_0 .. :try_end_12} :catchall_6d

    .line 509
    :try_start_12
    new-instance v1, Lorg/xbill/DNS/Message;

    invoke-direct {v1, v2}, Lorg/xbill/DNS/Message;-><init>([B)V

    .line 510
    invoke-virtual {p0, v1, v2, v0, p1}, Ljnamed;->a(Lorg/xbill/DNS/Message;[BILjava/net/Socket;)[B
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_1a} :catch_21
    .catchall {:try_start_12 .. :try_end_1a} :catchall_6d

    move-result-object v0

    .line 511
    if-nez v0, :cond_26

    .line 529
    :try_start_1d
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_20} :catch_72

    .line 532
    :goto_20
    return-void

    .line 515
    :catch_21
    move-exception v0

    :try_start_22
    invoke-static {v2}, Ljnamed;->a([B)[B

    move-result-object v0

    .line 517
    :cond_26
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 518
    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 519
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_36} :catch_3c
    .catchall {:try_start_22 .. :try_end_36} :catchall_6d

    .line 529
    :try_start_36
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_39} :catch_3a

    goto :goto_20

    .line 532
    :catch_3a
    move-exception v0

    goto :goto_20

    .line 521
    :catch_3c
    move-exception v0

    .line 522
    :try_start_3d
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "TCPclient("

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {p1}, Ljava/net/Socket;->getLocalPort()I

    move-result v4

    invoke-static {v3, v4}, Ljnamed;->a(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_67
    .catchall {:try_start_3d .. :try_end_67} :catchall_6d

    .line 529
    :try_start_67
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_6a
    .catch Ljava/io/IOException; {:try_start_67 .. :try_end_6a} :catch_6b

    goto :goto_20

    .line 532
    :catch_6b
    move-exception v0

    goto :goto_20

    .line 528
    :catchall_6d
    move-exception v0

    .line 529
    :try_start_6e
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_71
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_71} :catch_74

    .line 531
    :goto_71
    throw v0

    :catch_72
    move-exception v0

    goto :goto_20

    :catch_74
    move-exception v1

    goto :goto_71
.end method

.method private final a(Lorg/xbill/DNS/Message;I)V
    .registers 4

    .prologue
    .line 250
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Ljnamed;->a(Lorg/xbill/DNS/Message;II)V

    .line 251
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0, p2}, Ljnamed;->a(Lorg/xbill/DNS/Message;II)V

    .line 252
    return-void
.end method

.method private a(Lorg/xbill/DNS/Message;II)V
    .registers 11

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 239
    invoke-virtual {p1, p2}, Lorg/xbill/DNS/Message;->getSectionArray(I)[Lorg/xbill/DNS/Record;

    move-result-object v3

    move v0, v1

    .line 240
    :goto_7
    array-length v2, v3

    if-ge v0, v2, :cond_34

    .line 241
    aget-object v2, v3, v0

    .line 242
    invoke-virtual {v2}, Lorg/xbill/DNS/Record;->getAdditionalName()Lorg/xbill/DNS/Name;

    move-result-object v4

    .line 243
    if-eqz v4, :cond_22

    .line 4162
    invoke-direct {p0, v4}, Ljnamed;->a(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Zone;

    move-result-object v2

    .line 4163
    if-eqz v2, :cond_25

    .line 4164
    invoke-virtual {v2, v4, v6}, Lorg/xbill/DNS/Zone;->findExactMatch(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/RRset;

    move-result-object v2

    .line 3232
    :goto_1c
    if-eqz v2, :cond_22

    .line 3234
    const/4 v5, 0x3

    invoke-static {v4, p1, v2, v5, p3}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    .line 240
    :cond_22
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 4167
    :cond_25
    invoke-direct {p0, v6}, Ljnamed;->a(I)Lorg/xbill/DNS/Cache;

    move-result-object v2

    .line 4169
    invoke-virtual {v2, v4, v6}, Lorg/xbill/DNS/Cache;->findAnyRecords(Lorg/xbill/DNS/Name;I)[Lorg/xbill/DNS/RRset;

    move-result-object v2

    .line 4172
    if-nez v2, :cond_31

    .line 4173
    const/4 v2, 0x0

    goto :goto_1c

    .line 4175
    :cond_31
    aget-object v2, v2, v1

    goto :goto_1c

    .line 246
    :cond_34
    return-void
.end method

.method private static a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Cache;Lorg/xbill/DNS/Name;)V
    .registers 6

    .prologue
    const/4 v2, 0x2

    .line 218
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v2, v0}, Lorg/xbill/DNS/Cache;->lookupRecords(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/SetResponse;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Lorg/xbill/DNS/SetResponse;->isDelegation()Z

    move-result v1

    if-nez v1, :cond_d

    .line 227
    :cond_c
    return-void

    .line 221
    :cond_d
    invoke-virtual {v0}, Lorg/xbill/DNS/SetResponse;->getNS()Lorg/xbill/DNS/RRset;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lorg/xbill/DNS/RRset;->rrs()Ljava/util/Iterator;

    move-result-object v1

    .line 223
    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 224
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xbill/DNS/Record;

    .line 225
    invoke-virtual {p0, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    goto :goto_15
.end method

.method private a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Name;I)V
    .registers 6

    .prologue
    const/4 v1, 0x1

    .line 231
    .line 3162
    invoke-direct {p0, p2}, Ljnamed;->a(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Zone;

    move-result-object v0

    .line 3163
    if-eqz v0, :cond_e

    .line 3164
    invoke-virtual {v0, p2, v1}, Lorg/xbill/DNS/Zone;->findExactMatch(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/RRset;

    move-result-object v0

    .line 232
    :goto_b
    if-nez v0, :cond_1e

    .line 235
    :goto_d
    return-void

    .line 3167
    :cond_e
    invoke-direct {p0, v1}, Ljnamed;->a(I)Lorg/xbill/DNS/Cache;

    move-result-object v0

    .line 3169
    invoke-virtual {v0, p2, v1}, Lorg/xbill/DNS/Cache;->findAnyRecords(Lorg/xbill/DNS/Name;I)[Lorg/xbill/DNS/RRset;

    move-result-object v0

    .line 3172
    if-nez v0, :cond_1a

    .line 3173
    const/4 v0, 0x0

    goto :goto_b

    .line 3175
    :cond_1a
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_b

    .line 234
    :cond_1e
    const/4 v1, 0x3

    invoke-static {p2, p1, v0, v1, p3}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    goto :goto_d
.end method

.method private static a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Zone;)V
    .registers 4

    .prologue
    .line 206
    invoke-virtual {p1}, Lorg/xbill/DNS/Zone;->getSOA()Lorg/xbill/DNS/SOARecord;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 207
    return-void
.end method

.method private static a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Zone;I)V
    .registers 6

    .prologue
    .line 211
    invoke-virtual {p1}, Lorg/xbill/DNS/Zone;->getNS()Lorg/xbill/DNS/RRset;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Lorg/xbill/DNS/RRset;->getName()Lorg/xbill/DNS/Name;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, p0, v0, v2, p2}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    .line 214
    return-void
.end method

.method private static a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V
    .registers 8

    .prologue
    .line 181
    const/4 v0, 0x1

    :goto_1
    if-gt v0, p3, :cond_11

    .line 182
    invoke-virtual {p2}, Lorg/xbill/DNS/RRset;->getType()I

    move-result v1

    invoke-virtual {p1, p0, v1, v0}, Lorg/xbill/DNS/Message;->findRRset(Lorg/xbill/DNS/Name;II)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 202
    :cond_d
    return-void

    .line 181
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 184
    :cond_11
    and-int/lit8 v0, p4, 0x2

    if-nez v0, :cond_3d

    .line 185
    invoke-virtual {p2}, Lorg/xbill/DNS/RRset;->rrs()Ljava/util/Iterator;

    move-result-object v1

    .line 186
    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 187
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xbill/DNS/Record;

    .line 188
    invoke-virtual {v0}, Lorg/xbill/DNS/Record;->getName()Lorg/xbill/DNS/Name;

    move-result-object v2

    invoke-virtual {v2}, Lorg/xbill/DNS/Name;->isWild()Z

    move-result v2

    if-eqz v2, :cond_39

    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->isWild()Z

    move-result v2

    if-nez v2, :cond_39

    .line 189
    invoke-virtual {v0, p0}, Lorg/xbill/DNS/Record;->withName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 190
    :cond_39
    invoke-virtual {p1, v0, p3}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    goto :goto_19

    .line 193
    :cond_3d
    and-int/lit8 v0, p4, 0x3

    if-eqz v0, :cond_d

    .line 194
    invoke-virtual {p2}, Lorg/xbill/DNS/RRset;->sigs()Ljava/util/Iterator;

    move-result-object v1

    .line 195
    :goto_45
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 196
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xbill/DNS/Record;

    .line 197
    invoke-virtual {v0}, Lorg/xbill/DNS/Record;->getName()Lorg/xbill/DNS/Name;

    move-result-object v2

    invoke-virtual {v2}, Lorg/xbill/DNS/Name;->isWild()Z

    move-result v2

    if-eqz v2, :cond_65

    invoke-virtual {p0}, Lorg/xbill/DNS/Name;->isWild()Z

    move-result v2

    if-nez v2, :cond_65

    .line 198
    invoke-virtual {v0, p0}, Lorg/xbill/DNS/Record;->withName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 199
    :cond_65
    invoke-virtual {p1, v0, p3}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    goto :goto_45
.end method

.method private static a(Lorg/xbill/DNS/Header;ILorg/xbill/DNS/Record;)[B
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 464
    new-instance v2, Lorg/xbill/DNS/Message;

    invoke-direct {v2}, Lorg/xbill/DNS/Message;-><init>()V

    .line 465
    invoke-virtual {v2, p0}, Lorg/xbill/DNS/Message;->setHeader(Lorg/xbill/DNS/Header;)V

    move v0, v1

    .line 466
    :goto_a
    const/4 v3, 0x4

    if-ge v0, v3, :cond_13

    .line 467
    invoke-virtual {v2, v0}, Lorg/xbill/DNS/Message;->removeAllRecords(I)V

    .line 466
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 468
    :cond_13
    const/4 v0, 0x2

    if-ne p1, v0, :cond_19

    .line 469
    invoke-virtual {v2, p2, v1}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 470
    :cond_19
    invoke-virtual {p0, p1}, Lorg/xbill/DNS/Header;->setRcode(I)V

    .line 471
    invoke-virtual {v2}, Lorg/xbill/DNS/Message;->toWire()[B

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/TSIG;Lorg/xbill/DNS/TSIGRecord;Ljava/net/Socket;)[B
    .registers 16

    .prologue
    .line 346
    iget-object v0, p0, Ljnamed;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xbill/DNS/Zone;

    .line 347
    const/4 v1, 0x1

    .line 348
    if-nez v0, :cond_11

    .line 349
    const/4 v0, 0x5

    invoke-static {p2, v0}, Ljnamed;->b(Lorg/xbill/DNS/Message;I)[B

    move-result-object v0

    .line 381
    :goto_10
    return-object v0

    .line 350
    :cond_11
    invoke-virtual {v0}, Lorg/xbill/DNS/Zone;->AXFR()Ljava/util/Iterator;

    move-result-object v3

    .line 353
    :try_start_15
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-virtual {p5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 354
    invoke-virtual {p2}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v0

    invoke-virtual {v0}, Lorg/xbill/DNS/Header;->getID()I

    move-result v5

    move v2, v1

    move-object v1, p4

    .line 355
    :goto_28
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 356
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xbill/DNS/RRset;

    .line 357
    new-instance v6, Lorg/xbill/DNS/Message;

    invoke-direct {v6, v5}, Lorg/xbill/DNS/Message;-><init>(I)V

    .line 358
    invoke-virtual {v6}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v7

    .line 359
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lorg/xbill/DNS/Header;->setFlag(I)V

    .line 360
    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Lorg/xbill/DNS/Header;->setFlag(I)V

    .line 361
    invoke-virtual {v0}, Lorg/xbill/DNS/RRset;->getName()Lorg/xbill/DNS/Name;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-static {v7, v6, v0, v8, v9}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/RRset;II)V

    .line 363
    if-eqz p3, :cond_75

    .line 364
    invoke-virtual {p3, v6, v1, v2}, Lorg/xbill/DNS/TSIG;->applyStream(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/TSIGRecord;Z)V

    .line 365
    invoke-virtual {v6}, Lorg/xbill/DNS/Message;->getTSIG()Lorg/xbill/DNS/TSIGRecord;

    move-result-object v0

    .line 367
    :goto_57
    const/4 v1, 0x0

    .line 368
    invoke-virtual {v6}, Lorg/xbill/DNS/Message;->toWire()[B

    move-result-object v2

    .line 369
    array-length v6, v2

    invoke-virtual {v4, v6}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 370
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_63
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_63} :catch_66

    move v2, v1

    move-object v1, v0

    .line 371
    goto :goto_28

    .line 374
    :catch_66
    move-exception v0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "AXFR failed"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 377
    :cond_6e
    :try_start_6e
    invoke-virtual {p5}, Ljava/net/Socket;->close()V
    :try_end_71
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_71} :catch_73

    .line 381
    :goto_71
    const/4 v0, 0x0

    goto :goto_10

    :catch_73
    move-exception v0

    goto :goto_71

    :cond_75
    move-object v0, v1

    goto :goto_57
.end method

.method public static a([B)[B
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 478
    :try_start_1
    new-instance v1, Lorg/xbill/DNS/Header;

    invoke-direct {v1, p0}, Lorg/xbill/DNS/Header;-><init>([B)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_c

    .line 483
    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Ljnamed;->a(Lorg/xbill/DNS/Header;ILorg/xbill/DNS/Record;)[B

    move-result-object v0

    :goto_b
    return-object v0

    .line 481
    :catch_c
    move-exception v1

    goto :goto_b
.end method

.method private b(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/RRset;
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 162
    invoke-direct {p0, p1}, Ljnamed;->a(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Zone;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_c

    .line 164
    invoke-virtual {v0, p1, v1}, Lorg/xbill/DNS/Zone;->findExactMatch(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/RRset;

    move-result-object v0

    .line 175
    :goto_b
    return-object v0

    .line 167
    :cond_c
    invoke-direct {p0, v1}, Ljnamed;->a(I)Lorg/xbill/DNS/Cache;

    move-result-object v0

    .line 169
    invoke-virtual {v0, p1, v1}, Lorg/xbill/DNS/Cache;->findAnyRecords(Lorg/xbill/DNS/Name;I)[Lorg/xbill/DNS/RRset;

    move-result-object v0

    .line 172
    if-nez v0, :cond_18

    .line 173
    const/4 v0, 0x0

    goto :goto_b

    .line 175
    :cond_18
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_b
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 123
    sget-object v0, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    invoke-static {p1, v0}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v0

    .line 124
    new-instance v1, Lorg/xbill/DNS/Zone;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2, p2}, Lorg/xbill/DNS/Zone;-><init>(Lorg/xbill/DNS/Name;ILjava/lang/String;)V

    .line 125
    iget-object v2, p0, Ljnamed;->d:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    return-void
.end method

.method private b(Ljava/net/InetAddress;I)V
    .registers 7

    .prologue
    .line 538
    :try_start_0
    new-instance v0, Ljava/net/ServerSocket;

    const/16 v1, 0x80

    invoke-direct {v0, p2, v1, p1}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    .line 540
    :goto_7
    invoke-virtual {v0}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1

    .line 542
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, La;

    invoke-direct {v3, p0, v1}, La;-><init>(Ljnamed;Ljava/net/Socket;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 544
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_18} :catch_19

    goto :goto_7

    .line 547
    :catch_19
    move-exception v0

    .line 548
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "serveTCP("

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljnamed;->a(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 551
    return-void
.end method

.method private static b(Lorg/xbill/DNS/Message;I)[B
    .registers 4

    .prologue
    .line 488
    invoke-virtual {p0}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v0

    invoke-virtual {p0}, Lorg/xbill/DNS/Message;->getQuestion()Lorg/xbill/DNS/Record;

    move-result-object v1

    invoke-static {v0, p1, v1}, Ljnamed;->a(Lorg/xbill/DNS/Header;ILorg/xbill/DNS/Record;)[B

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/net/InetAddress;I)V
    .registers 11

    .prologue
    const/4 v0, 0x0

    .line 556
    :try_start_1
    new-instance v2, Ljava/net/DatagramSocket;

    invoke-direct {v2, p2, p1}, Ljava/net/DatagramSocket;-><init>(ILjava/net/InetAddress;)V

    .line 558
    const/16 v1, 0x200

    new-array v3, v1, [B

    .line 559
    new-instance v4, Ljava/net/DatagramPacket;

    const/16 v1, 0x200

    invoke-direct {v4, v3, v1}, Ljava/net/DatagramPacket;-><init>([BI)V

    move-object v1, v0

    .line 562
    :cond_12
    :goto_12
    const/16 v0, 0x200

    invoke-virtual {v4, v0}, Ljava/net/DatagramPacket;->setLength(I)V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_17} :catch_5d

    .line 564
    :try_start_17
    invoke-virtual {v2, v4}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V
    :try_end_1a
    .catch Ljava/io/InterruptedIOException; {:try_start_17 .. :try_end_1a} :catch_81
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_1a} :catch_5d

    .line 572
    :try_start_1a
    new-instance v0, Lorg/xbill/DNS/Message;

    invoke-direct {v0, v3}, Lorg/xbill/DNS/Message;-><init>([B)V

    .line 573
    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getLength()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {p0, v0, v3, v5, v6}, Ljnamed;->a(Lorg/xbill/DNS/Message;[BILjava/net/Socket;)[B
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_27} :catch_40

    move-result-object v0

    .line 576
    if-eqz v0, :cond_12

    .line 582
    :goto_2a
    if-nez v1, :cond_46

    .line 583
    :try_start_2c
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v5, v0

    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getPort()I

    move-result v7

    invoke-direct {v1, v0, v5, v6, v7}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    move-object v0, v1

    .line 593
    :goto_3b
    invoke-virtual {v2, v0}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    move-object v1, v0

    .line 594
    goto :goto_12

    .line 580
    :catch_40
    move-exception v0

    invoke-static {v3}, Ljnamed;->a([B)[B

    move-result-object v0

    goto :goto_2a

    .line 588
    :cond_46
    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setData([B)V

    .line 589
    array-length v0, v0

    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setLength(I)V

    .line 590
    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 591
    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getPort()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setPort(I)V
    :try_end_5b
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_5b} :catch_5d

    move-object v0, v1

    goto :goto_3b

    .line 596
    :catch_5d
    move-exception v0

    .line 597
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "serveUDP("

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljnamed;->a(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 600
    return-void

    .line 567
    :catch_81
    move-exception v0

    goto :goto_12
.end method

.method private d(Ljava/net/InetAddress;I)V
    .registers 5

    .prologue
    .line 605
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lb;

    invoke-direct {v1, p0, p1, p2}, Lb;-><init>(Ljnamed;Ljava/net/InetAddress;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 607
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 608
    return-void
.end method

.method private e(Ljava/net/InetAddress;I)V
    .registers 5

    .prologue
    .line 613
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lc;

    invoke-direct {v1, p0, p1, p2}, Lc;-><init>(Ljnamed;Ljava/net/InetAddress;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 615
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 616
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 619
    array-length v0, p0

    if-le v0, v3, :cond_f

    .line 620
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "usage: jnamed [conf]"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 621
    invoke-static {v2}, Ljava/lang/System;->exit(I)V

    .line 626
    :cond_f
    :try_start_f
    array-length v0, p0

    if-ne v0, v3, :cond_1b

    .line 627
    const/4 v0, 0x0

    aget-object v0, p0, v0

    .line 630
    :goto_15
    new-instance v1, Ljnamed;

    invoke-direct {v1, v0}, Ljnamed;-><init>(Ljava/lang/String;)V

    .line 638
    :goto_1a
    return-void

    .line 629
    :cond_1b
    const-string v0, "jnamed.conf"
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_1d} :catch_1e
    .catch Lorg/xbill/DNS/ZoneTransferException; {:try_start_f .. :try_end_1d} :catch_25

    goto :goto_15

    .line 632
    :catch_1e
    move-exception v0

    .line 633
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1a

    .line 635
    :catch_25
    move-exception v0

    .line 636
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1a
.end method


# virtual methods
.method final a(Lorg/xbill/DNS/Message;[BILjava/net/Socket;)[B
    .registers 21

    .prologue
    .line 396
    const/4 v14, 0x0

    .line 398
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v3

    .line 399
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/xbill/DNS/Header;->getFlag(I)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 400
    const/4 v3, 0x0

    .line 459
    :goto_d
    return-object v3

    .line 401
    :cond_e
    invoke-virtual {v3}, Lorg/xbill/DNS/Header;->getRcode()I

    move-result v4

    if-eqz v4, :cond_1c

    .line 402
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Ljnamed;->b(Lorg/xbill/DNS/Message;I)[B

    move-result-object v3

    goto :goto_d

    .line 403
    :cond_1c
    invoke-virtual {v3}, Lorg/xbill/DNS/Header;->getOpcode()I

    move-result v3

    if-eqz v3, :cond_2a

    .line 404
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Ljnamed;->b(Lorg/xbill/DNS/Message;I)[B

    move-result-object v3

    goto :goto_d

    .line 406
    :cond_2a
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getQuestion()Lorg/xbill/DNS/Record;

    move-result-object v8

    .line 408
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getTSIG()Lorg/xbill/DNS/TSIGRecord;

    move-result-object v7

    .line 409
    const/4 v6, 0x0

    .line 410
    if-eqz v7, :cond_58

    .line 411
    move-object/from16 v0, p0

    iget-object v3, v0, Ljnamed;->e:Ljava/util/Map;

    invoke-virtual {v7}, Lorg/xbill/DNS/TSIGRecord;->getName()Lorg/xbill/DNS/Name;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/xbill/DNS/TSIG;

    .line 412
    if-eqz v3, :cond_52

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v3, v0, v1, v2, v4}, Lorg/xbill/DNS/TSIG;->verify(Lorg/xbill/DNS/Message;[BILorg/xbill/DNS/TSIGRecord;)B

    move-result v4

    if-eqz v4, :cond_57

    .line 414
    :cond_52
    invoke-static/range {p2 .. p2}, Ljnamed;->a([B)[B

    move-result-object v3

    goto :goto_d

    :cond_57
    move-object v6, v3

    .line 417
    :cond_58
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getOPT()Lorg/xbill/DNS/OPTRecord;

    move-result-object v15

    .line 418
    if-eqz v15, :cond_61

    invoke-virtual {v15}, Lorg/xbill/DNS/OPTRecord;->getVersion()I

    .line 421
    :cond_61
    if-eqz p4, :cond_be

    .line 422
    const v3, 0xffff

    move v5, v3

    .line 428
    :goto_67
    if-eqz v15, :cond_74

    invoke-virtual {v15}, Lorg/xbill/DNS/OPTRecord;->getFlags()I

    move-result v3

    const v4, 0x8000

    and-int/2addr v3, v4

    if-eqz v3, :cond_74

    .line 429
    const/4 v14, 0x1

    .line 431
    :cond_74
    new-instance v9, Lorg/xbill/DNS/Message;

    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v3

    invoke-virtual {v3}, Lorg/xbill/DNS/Header;->getID()I

    move-result v3

    invoke-direct {v9, v3}, Lorg/xbill/DNS/Message;-><init>(I)V

    .line 432
    invoke-virtual {v9}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/xbill/DNS/Header;->setFlag(I)V

    .line 433
    invoke-virtual/range {p1 .. p1}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Lorg/xbill/DNS/Header;->getFlag(I)Z

    move-result v3

    if-eqz v3, :cond_9c

    .line 434
    invoke-virtual {v9}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Lorg/xbill/DNS/Header;->setFlag(I)V

    .line 435
    :cond_9c
    const/4 v3, 0x0

    invoke-virtual {v9, v8, v3}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 437
    invoke-virtual {v8}, Lorg/xbill/DNS/Record;->getName()Lorg/xbill/DNS/Name;

    move-result-object v4

    .line 438
    invoke-virtual {v8}, Lorg/xbill/DNS/Record;->getType()I

    move-result v11

    .line 439
    invoke-virtual {v8}, Lorg/xbill/DNS/Record;->getDClass()I

    move-result v12

    .line 440
    const/16 v3, 0xfc

    if-ne v11, v3, :cond_d0

    if-eqz p4, :cond_d0

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    move-object/from16 v8, p4

    .line 441
    invoke-direct/range {v3 .. v8}, Ljnamed;->a(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Message;Lorg/xbill/DNS/TSIG;Lorg/xbill/DNS/TSIGRecord;Ljava/net/Socket;)[B

    move-result-object v3

    goto/16 :goto_d

    .line 423
    :cond_be
    if-eqz v15, :cond_cc

    .line 424
    invoke-virtual {v15}, Lorg/xbill/DNS/OPTRecord;->getPayloadSize()I

    move-result v3

    const/16 v4, 0x200

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v5, v3

    goto :goto_67

    .line 426
    :cond_cc
    const/16 v3, 0x200

    move v5, v3

    goto :goto_67

    .line 442
    :cond_d0
    invoke-static {v11}, Lorg/xbill/DNS/Type;->isRR(I)Z

    move-result v3

    if-nez v3, :cond_e3

    const/16 v3, 0xff

    if-eq v11, v3, :cond_e3

    .line 443
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Ljnamed;->b(Lorg/xbill/DNS/Message;I)[B

    move-result-object v3

    goto/16 :goto_d

    .line 445
    :cond_e3
    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object v10, v4

    invoke-direct/range {v8 .. v14}, Ljnamed;->a(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Name;IIII)B

    move-result v4

    .line 446
    if-eqz v4, :cond_f8

    const/4 v3, 0x3

    if-eq v4, v3, :cond_f8

    .line 447
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ljnamed;->b(Lorg/xbill/DNS/Message;I)[B

    move-result-object v3

    goto/16 :goto_d

    .line 4250
    :cond_f8
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v3, v14}, Ljnamed;->a(Lorg/xbill/DNS/Message;II)V

    .line 4251
    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v3, v14}, Ljnamed;->a(Lorg/xbill/DNS/Message;II)V

    .line 451
    if-eqz v15, :cond_118

    .line 452
    const/4 v3, 0x1

    if-ne v14, v3, :cond_122

    const v3, 0x8000

    .line 453
    :goto_10c
    new-instance v8, Lorg/xbill/DNS/OPTRecord;

    const/16 v10, 0x1000

    const/4 v11, 0x0

    invoke-direct {v8, v10, v4, v11, v3}, Lorg/xbill/DNS/OPTRecord;-><init>(IIII)V

    .line 455
    const/4 v3, 0x3

    invoke-virtual {v9, v8, v3}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 458
    :cond_118
    const/4 v3, 0x0

    invoke-virtual {v9, v6, v3, v7}, Lorg/xbill/DNS/Message;->setTSIG(Lorg/xbill/DNS/TSIG;ILorg/xbill/DNS/TSIGRecord;)V

    .line 459
    invoke-virtual {v9, v5}, Lorg/xbill/DNS/Message;->toWire(I)[B

    move-result-object v3

    goto/16 :goto_d

    .line 452
    :cond_122
    const/4 v3, 0x0

    goto :goto_10c
.end method
