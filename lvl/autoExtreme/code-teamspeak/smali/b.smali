.class final Lb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/net/InetAddress;

.field private final b:I

.field private final c:Ljnamed;


# direct methods
.method constructor <init>(Ljnamed;Ljava/net/InetAddress;I)V
    .registers 4

    .prologue
    .line 605
    iput-object p1, p0, Lb;->c:Ljnamed;

    iput-object p2, p0, Lb;->a:Ljava/net/InetAddress;

    iput p3, p0, Lb;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 8

    .prologue
    .line 606
    iget-object v0, p0, Lb;->c:Ljnamed;

    iget-object v1, p0, Lb;->a:Ljava/net/InetAddress;

    iget v2, p0, Lb;->b:I

    .line 1538
    :try_start_6
    new-instance v3, Ljava/net/ServerSocket;

    const/16 v4, 0x80

    invoke-direct {v3, v2, v4, v1}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    .line 1540
    :goto_d
    invoke-virtual {v3}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v4

    .line 1542
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, La;

    invoke-direct {v6, v0, v4}, La;-><init>(Ljnamed;Ljava/net/Socket;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1544
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_1e} :catch_1f

    goto :goto_d

    .line 1547
    :catch_1f
    move-exception v0

    .line 1548
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    const-string v5, "serveTCP("

    invoke-direct {v4, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Ljnamed;->a(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 606
    return-void
.end method
