.class public final Ljavax/annotation/Nonnull$Checker;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/annotation/meta/TypeQualifierValidator;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static forConstantValue$4791c4f5(Ljava/lang/Object;)Ljavax/annotation/meta/When;
    .registers 2

    .prologue
    .line 21
    if-nez p0, :cond_5

    .line 22
    sget-object v0, Ljavax/annotation/meta/When;->NEVER:Ljavax/annotation/meta/When;

    .line 23
    :goto_4
    return-object v0

    :cond_5
    sget-object v0, Ljavax/annotation/meta/When;->ALWAYS:Ljavax/annotation/meta/When;

    goto :goto_4
.end method


# virtual methods
.method public final bridge synthetic forConstantValue(Ljava/lang/annotation/Annotation;Ljava/lang/Object;)Ljavax/annotation/meta/When;
    .registers 4

    .prologue
    .line 17
    .line 1021
    if-nez p2, :cond_5

    .line 1022
    sget-object v0, Ljavax/annotation/meta/When;->NEVER:Ljavax/annotation/meta/When;

    :goto_4
    return-object v0

    .line 1023
    :cond_5
    sget-object v0, Ljavax/annotation/meta/When;->ALWAYS:Ljavax/annotation/meta/When;

    goto :goto_4
.end method
