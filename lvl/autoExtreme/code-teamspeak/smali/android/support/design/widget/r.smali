.class final Landroid/support/design/widget/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic a:Landroid/support/design/widget/CoordinatorLayout;


# direct methods
.method constructor <init>(Landroid/support/design/widget/CoordinatorLayout;)V
    .registers 2

    .prologue
    .line 111
    iput-object p1, p0, Landroid/support/design/widget/r;->a:Landroid/support/design/widget/CoordinatorLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 114
    if-ne p0, p1, :cond_5

    move v0, v1

    .line 123
    :goto_4
    return v0

    .line 116
    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 118
    const/4 v0, 0x1

    goto :goto_4

    .line 119
    :cond_13
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    invoke-virtual {v0, p0}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 121
    const/4 v0, -0x1

    goto :goto_4

    :cond_21
    move v0, v1

    .line 123
    goto :goto_4
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 111
    check-cast p1, Landroid/view/View;

    check-cast p2, Landroid/view/View;

    .line 1114
    if-eq p1, p2, :cond_22

    .line 1116
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    invoke-virtual {v0, p2}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1118
    const/4 v0, 0x1

    .line 1121
    :goto_13
    return v0

    .line 1119
    :cond_14
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/w;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1121
    const/4 v0, -0x1

    goto :goto_13

    .line 1123
    :cond_22
    const/4 v0, 0x0

    .line 111
    goto :goto_13
.end method
