.class final Landroid/support/design/widget/bo;
.super Landroid/support/v4/widget/ej;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/design/widget/SwipeDismissBehavior;

.field private b:I


# direct methods
.method constructor <init>(Landroid/support/design/widget/SwipeDismissBehavior;)V
    .registers 2

    .prologue
    .line 201
    iput-object p1, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-direct {p0}, Landroid/support/v4/widget/ej;-><init>()V

    return-void
.end method

.method private b(Landroid/view/View;F)Z
    .registers 9

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 241
    cmpl-float v0, p2, v5

    if-eqz v0, :cond_46

    .line 242
    invoke-static {p1}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    if-ne v0, v1, :cond_18

    move v0, v1

    .line 245
    :goto_e
    iget-object v3, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v3}, Landroid/support/design/widget/SwipeDismissBehavior;->c(Landroid/support/design/widget/SwipeDismissBehavior;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1a

    .line 263
    :cond_17
    :goto_17
    return v1

    :cond_18
    move v0, v2

    .line 242
    goto :goto_e

    .line 248
    :cond_1a
    iget-object v3, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v3}, Landroid/support/design/widget/SwipeDismissBehavior;->c(Landroid/support/design/widget/SwipeDismissBehavior;)I

    move-result v3

    if-nez v3, :cond_30

    .line 251
    if-eqz v0, :cond_2a

    cmpg-float v0, p2, v5

    if-ltz v0, :cond_17

    move v1, v2

    goto :goto_17

    :cond_2a
    cmpl-float v0, p2, v5

    if-gtz v0, :cond_17

    move v1, v2

    goto :goto_17

    .line 252
    :cond_30
    iget-object v3, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v3}, Landroid/support/design/widget/SwipeDismissBehavior;->c(Landroid/support/design/widget/SwipeDismissBehavior;)I

    move-result v3

    if-ne v3, v1, :cond_65

    .line 255
    if-eqz v0, :cond_40

    cmpl-float v0, p2, v5

    if-gtz v0, :cond_17

    move v1, v2

    goto :goto_17

    :cond_40
    cmpg-float v0, p2, v5

    if-ltz v0, :cond_17

    move v1, v2

    goto :goto_17

    .line 258
    :cond_46
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v3, p0, Landroid/support/design/widget/bo;->b:I

    sub-int/2addr v0, v3

    .line 259
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v4}, Landroid/support/design/widget/SwipeDismissBehavior;->d(Landroid/support/design/widget/SwipeDismissBehavior;)F

    move-result v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 260
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ge v0, v3, :cond_17

    move v1, v2

    goto :goto_17

    :cond_65
    move v1, v2

    .line 263
    goto :goto_17
.end method


# virtual methods
.method public final a(Landroid/view/View;I)I
    .registers 6

    .prologue
    const/4 v1, 0x1

    .line 273
    invoke-static {p1}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    if-ne v0, v1, :cond_20

    move v0, v1

    .line 277
    :goto_8
    iget-object v2, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v2}, Landroid/support/design/widget/SwipeDismissBehavior;->c(Landroid/support/design/widget/SwipeDismissBehavior;)I

    move-result v2

    if-nez v2, :cond_2c

    .line 278
    if-eqz v0, :cond_22

    .line 279
    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 280
    iget v1, p0, Landroid/support/design/widget/bo;->b:I

    .line 298
    :goto_1b
    invoke-static {v0, p2, v1}, Landroid/support/design/widget/SwipeDismissBehavior;->a(III)I

    move-result v0

    return v0

    .line 273
    :cond_20
    const/4 v0, 0x0

    goto :goto_8

    .line 282
    :cond_22
    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    .line 283
    iget v1, p0, Landroid/support/design/widget/bo;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_1b

    .line 285
    :cond_2c
    iget-object v2, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v2}, Landroid/support/design/widget/SwipeDismissBehavior;->c(Landroid/support/design/widget/SwipeDismissBehavior;)I

    move-result v2

    if-ne v2, v1, :cond_4a

    .line 286
    if-eqz v0, :cond_40

    .line 287
    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    .line 288
    iget v1, p0, Landroid/support/design/widget/bo;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_1b

    .line 290
    :cond_40
    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 291
    iget v1, p0, Landroid/support/design/widget/bo;->b:I

    goto :goto_1b

    .line 294
    :cond_4a
    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 295
    iget v1, p0, Landroid/support/design/widget/bo;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_1b
.end method

.method public final a(I)V
    .registers 3

    .prologue
    .line 212
    iget-object v0, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v0}, Landroid/support/design/widget/SwipeDismissBehavior;->a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 213
    iget-object v0, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v0}, Landroid/support/design/widget/SwipeDismissBehavior;->a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/design/widget/bp;->a(I)V

    .line 215
    :cond_11
    return-void
.end method

.method public final a(Landroid/view/View;F)V
    .registers 10

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1241
    cmpl-float v0, p2, v6

    if-eqz v0, :cond_7a

    .line 1242
    invoke-static {p1}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    if-ne v0, v1, :cond_44

    move v0, v1

    .line 1245
    :goto_12
    iget-object v4, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v4}, Landroid/support/design/widget/SwipeDismissBehavior;->c(Landroid/support/design/widget/SwipeDismissBehavior;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_46

    move v0, v1

    .line 223
    :goto_1c
    if-eqz v0, :cond_a1

    .line 224
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v2, p0, Landroid/support/design/widget/bo;->b:I

    if-ge v0, v2, :cond_9d

    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    sub-int/2addr v0, v3

    .line 233
    :goto_29
    iget-object v2, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v2}, Landroid/support/design/widget/SwipeDismissBehavior;->b(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/v4/widget/eg;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/widget/eg;->a(II)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 234
    new-instance v0, Landroid/support/design/widget/bq;

    iget-object v2, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-direct {v0, v2, p1, v1}, Landroid/support/design/widget/bq;-><init>(Landroid/support/design/widget/SwipeDismissBehavior;Landroid/view/View;Z)V

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 238
    :cond_43
    :goto_43
    return-void

    :cond_44
    move v0, v2

    .line 1242
    goto :goto_12

    .line 1248
    :cond_46
    iget-object v4, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v4}, Landroid/support/design/widget/SwipeDismissBehavior;->c(Landroid/support/design/widget/SwipeDismissBehavior;)I

    move-result v4

    if-nez v4, :cond_60

    .line 1251
    if-eqz v0, :cond_58

    cmpg-float v0, p2, v6

    if-gez v0, :cond_56

    move v0, v1

    goto :goto_1c

    :cond_56
    move v0, v2

    goto :goto_1c

    :cond_58
    cmpl-float v0, p2, v6

    if-lez v0, :cond_5e

    move v0, v1

    goto :goto_1c

    :cond_5e
    move v0, v2

    goto :goto_1c

    .line 1252
    :cond_60
    iget-object v4, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v4}, Landroid/support/design/widget/SwipeDismissBehavior;->c(Landroid/support/design/widget/SwipeDismissBehavior;)I

    move-result v4

    if-ne v4, v1, :cond_9b

    .line 1255
    if-eqz v0, :cond_72

    cmpl-float v0, p2, v6

    if-lez v0, :cond_70

    move v0, v1

    goto :goto_1c

    :cond_70
    move v0, v2

    goto :goto_1c

    :cond_72
    cmpg-float v0, p2, v6

    if-gez v0, :cond_78

    move v0, v1

    goto :goto_1c

    :cond_78
    move v0, v2

    goto :goto_1c

    .line 1258
    :cond_7a
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v4, p0, Landroid/support/design/widget/bo;->b:I

    sub-int/2addr v0, v4

    .line 1259
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v5}, Landroid/support/design/widget/SwipeDismissBehavior;->d(Landroid/support/design/widget/SwipeDismissBehavior;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 1260
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lt v0, v4, :cond_99

    move v0, v1

    goto :goto_1c

    :cond_99
    move v0, v2

    goto :goto_1c

    :cond_9b
    move v0, v2

    .line 1263
    goto :goto_1c

    .line 224
    :cond_9d
    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    add-int/2addr v0, v3

    goto :goto_29

    .line 230
    :cond_a1
    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    move v1, v2

    goto :goto_29

    .line 235
    :cond_a5
    if-eqz v1, :cond_43

    iget-object v0, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v0}, Landroid/support/design/widget/SwipeDismissBehavior;->a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;

    move-result-object v0

    if-eqz v0, :cond_43

    .line 236
    iget-object v0, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v0}, Landroid/support/design/widget/SwipeDismissBehavior;->a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;

    move-result-object v0

    invoke-interface {v0}, Landroid/support/design/widget/bp;->a()V

    goto :goto_43
.end method

.method public final a(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 206
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/bo;->b:I

    .line 207
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 268
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/View;I)V
    .registers 8

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 308
    iget v0, p0, Landroid/support/design/widget/bo;->b:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v2}, Landroid/support/design/widget/SwipeDismissBehavior;->e(Landroid/support/design/widget/SwipeDismissBehavior;)F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 310
    iget v1, p0, Landroid/support/design/widget/bo;->b:I

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/design/widget/bo;->a:Landroid/support/design/widget/SwipeDismissBehavior;

    invoke-static {v3}, Landroid/support/design/widget/SwipeDismissBehavior;->f(Landroid/support/design/widget/SwipeDismissBehavior;)F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 313
    int-to-float v2, p2

    cmpg-float v2, v2, v0

    if-gtz v2, :cond_2b

    .line 314
    invoke-static {p1, v4}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 322
    :goto_2a
    return-void

    .line 315
    :cond_2b
    int-to-float v2, p2

    cmpl-float v2, v2, v1

    if-ltz v2, :cond_35

    .line 316
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    goto :goto_2a

    .line 319
    :cond_35
    int-to-float v2, p2

    invoke-static {v0, v1, v2}, Landroid/support/design/widget/SwipeDismissBehavior;->a(FFF)F

    move-result v0

    .line 320
    sub-float v0, v4, v0

    invoke-static {v0}, Landroid/support/design/widget/SwipeDismissBehavior;->b(F)F

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    goto :goto_2a
.end method

.method public final c(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 303
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method
