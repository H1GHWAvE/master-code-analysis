.class public final Landroid/support/design/widget/br;
.super Landroid/widget/HorizontalScrollView;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x0

.field public static final d:I = 0x1

.field private static final e:I = 0x30

.field private static final f:I = 0x38

.field private static final g:I = 0x10

.field private static final h:I = 0x18

.field private static final i:I = 0x12c


# instance fields
.field private A:Landroid/view/View$OnClickListener;

.field private B:Landroid/support/design/widget/ck;

.field private C:Landroid/support/design/widget/ck;

.field private final j:Ljava/util/ArrayList;

.field private k:Landroid/support/design/widget/bz;

.field private final l:Landroid/support/design/widget/bw;

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Landroid/content/res/ColorStateList;

.field private final s:I

.field private final t:I

.field private u:I

.field private final v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:Landroid/support/design/widget/bv;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/br;-><init>(Landroid/content/Context;B)V

    .line 216
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/br;-><init>(Landroid/content/Context;C)V

    .line 220
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 223
    invoke-direct {p0, p1, v5, v3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    .line 200
    const v0, 0x7fffffff

    iput v0, p0, Landroid/support/design/widget/br;->u:I

    .line 226
    invoke-virtual {p0, v3}, Landroid/support/design/widget/br;->setHorizontalScrollBarEnabled(Z)V

    .line 228
    invoke-virtual {p0, v4}, Landroid/support/design/widget/br;->setFillViewport(Z)V

    .line 231
    new-instance v0, Landroid/support/design/widget/bw;

    invoke-direct {v0, p0, p1}, Landroid/support/design/widget/bw;-><init>(Landroid/support/design/widget/br;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    .line 232
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/support/design/widget/br;->addView(Landroid/view/View;II)V

    .line 234
    sget-object v0, Landroid/support/design/n;->TabLayout:[I

    sget v1, Landroid/support/design/m;->Widget_Design_TabLayout:I

    invoke-virtual {p1, v5, v0, v3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 237
    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    sget v2, Landroid/support/design/n;->TabLayout_tabIndicatorHeight:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/bw;->b(I)V

    .line 239
    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    sget v2, Landroid/support/design/n;->TabLayout_tabIndicatorColor:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/bw;->a(I)V

    .line 241
    sget v1, Landroid/support/design/n;->TabLayout_tabTextAppearance:I

    sget v2, Landroid/support/design/m;->TextAppearance_Design_Tab:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->q:I

    .line 244
    sget v1, Landroid/support/design/n;->TabLayout_tabPadding:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->p:I

    iput v1, p0, Landroid/support/design/widget/br;->o:I

    iput v1, p0, Landroid/support/design/widget/br;->n:I

    iput v1, p0, Landroid/support/design/widget/br;->m:I

    .line 246
    sget v1, Landroid/support/design/n;->TabLayout_tabPaddingStart:I

    iget v2, p0, Landroid/support/design/widget/br;->m:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->m:I

    .line 248
    sget v1, Landroid/support/design/n;->TabLayout_tabPaddingTop:I

    iget v2, p0, Landroid/support/design/widget/br;->n:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->n:I

    .line 250
    sget v1, Landroid/support/design/n;->TabLayout_tabPaddingEnd:I

    iget v2, p0, Landroid/support/design/widget/br;->o:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->o:I

    .line 252
    sget v1, Landroid/support/design/n;->TabLayout_tabPaddingBottom:I

    iget v2, p0, Landroid/support/design/widget/br;->p:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->p:I

    .line 256
    iget v1, p0, Landroid/support/design/widget/br;->q:I

    invoke-direct {p0, v1}, Landroid/support/design/widget/br;->g(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/br;->r:Landroid/content/res/ColorStateList;

    .line 258
    sget v1, Landroid/support/design/n;->TabLayout_tabTextColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_9c

    .line 260
    sget v1, Landroid/support/design/n;->TabLayout_tabTextColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/br;->r:Landroid/content/res/ColorStateList;

    .line 263
    :cond_9c
    sget v1, Landroid/support/design/n;->TabLayout_tabSelectedTextColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_b6

    .line 267
    sget v1, Landroid/support/design/n;->TabLayout_tabSelectedTextColor:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 268
    iget-object v2, p0, Landroid/support/design/widget/br;->r:Landroid/content/res/ColorStateList;

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-static {v2, v1}, Landroid/support/design/widget/br;->b(II)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/br;->r:Landroid/content/res/ColorStateList;

    .line 271
    :cond_b6
    sget v1, Landroid/support/design/n;->TabLayout_tabMinWidth:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->t:I

    .line 272
    sget v1, Landroid/support/design/n;->TabLayout_tabMaxWidth:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->v:I

    .line 273
    sget v1, Landroid/support/design/n;->TabLayout_tabBackground:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->s:I

    .line 274
    sget v1, Landroid/support/design/n;->TabLayout_tabContentStart:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->w:I

    .line 275
    sget v1, Landroid/support/design/n;->TabLayout_tabMode:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->y:I

    .line 276
    sget v1, Landroid/support/design/n;->TabLayout_tabGravity:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/br;->x:I

    .line 277
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 280
    invoke-direct {p0}, Landroid/support/design/widget/br;->e()V

    .line 281
    return-void
.end method

.method private a(IF)I
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 843
    iget v1, p0, Landroid/support/design/widget/br;->y:I

    if-nez v1, :cond_44

    .line 844
    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v1, p1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 845
    add-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v2}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_45

    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 848
    :goto_1e
    if-eqz v3, :cond_48

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 849
    :goto_24
    if-eqz v2, :cond_2a

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 851
    :cond_2a
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr v0, v2

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/design/widget/br;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 856
    :cond_44
    return v0

    .line 845
    :cond_45
    const/4 v1, 0x0

    move-object v2, v1

    goto :goto_1e

    :cond_48
    move v1, v0

    .line 848
    goto :goto_24
.end method

.method static synthetic a(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->s:I

    return v0
.end method

.method private a()Landroid/support/design/widget/bz;
    .registers 2
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 410
    new-instance v0, Landroid/support/design/widget/bz;

    invoke-direct {v0, p0}, Landroid/support/design/widget/bz;-><init>(Landroid/support/design/widget/br;)V

    return-object v0
.end method

.method static synthetic a(Landroid/support/design/widget/br;Landroid/support/design/widget/ck;)Landroid/support/design/widget/ck;
    .registers 2

    .prologue
    .line 96
    iput-object p1, p0, Landroid/support/design/widget/br;->C:Landroid/support/design/widget/ck;

    return-object p1
.end method

.method private a(II)V
    .registers 4

    .prologue
    .line 568
    invoke-static {p1, p2}, Landroid/support/design/widget/br;->b(II)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/br;->setTabTextColors(Landroid/content/res/ColorStateList;)V

    .line 569
    return-void
.end method

.method static synthetic a(Landroid/support/design/widget/br;I)V
    .registers 2

    .prologue
    .line 96
    invoke-direct {p0, p1}, Landroid/support/design/widget/br;->c(I)V

    return-void
.end method

.method private a(Landroid/support/design/widget/bz;)V
    .registers 6
    .param p1    # Landroid/support/design/widget/bz;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 340
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    .line 3890
    iget-object v1, p1, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    .line 3361
    if-eq v1, p0, :cond_12

    .line 3362
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tab belongs to a different TabLayout."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4669
    :cond_12
    invoke-direct {p0, p1}, Landroid/support/design/widget/br;->c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;

    move-result-object v1

    .line 4670
    iget-object v2, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-direct {p0}, Landroid/support/design/widget/br;->d()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/support/design/widget/bw;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4671
    if-eqz v0, :cond_25

    .line 4672
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/design/widget/cc;->setSelected(Z)V

    .line 3366
    :cond_25
    iget-object v1, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, p1, v1}, Landroid/support/design/widget/br;->b(Landroid/support/design/widget/bz;I)V

    .line 3367
    if-eqz v0, :cond_33

    .line 3368
    invoke-virtual {p1}, Landroid/support/design/widget/bz;->a()V

    .line 341
    :cond_33
    return-void
.end method

.method private a(Landroid/support/design/widget/bz;I)V
    .registers 7
    .param p1    # Landroid/support/design/widget/bz;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 351
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    .line 5890
    iget-object v1, p1, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    .line 5380
    if-eq v1, p0, :cond_12

    .line 5381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tab belongs to a different TabLayout."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6677
    :cond_12
    invoke-direct {p0, p1}, Landroid/support/design/widget/br;->c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;

    move-result-object v1

    .line 6678
    iget-object v2, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-direct {p0}, Landroid/support/design/widget/br;->d()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, v1, p2, v3}, Landroid/support/design/widget/bw;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 6679
    if-eqz v0, :cond_25

    .line 6680
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/design/widget/cc;->setSelected(Z)V

    .line 5385
    :cond_25
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/br;->b(Landroid/support/design/widget/bz;I)V

    .line 5386
    if-eqz v0, :cond_2d

    .line 5387
    invoke-virtual {p1}, Landroid/support/design/widget/bz;->a()V

    .line 352
    :cond_2d
    return-void
.end method

.method private a(Landroid/support/design/widget/bz;IZ)V
    .registers 7
    .param p1    # Landroid/support/design/widget/bz;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 380
    .line 7890
    iget-object v0, p1, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    .line 380
    if-eq v0, p0, :cond_c

    .line 381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tab belongs to a different TabLayout."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8677
    :cond_c
    invoke-direct {p0, p1}, Landroid/support/design/widget/br;->c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;

    move-result-object v0

    .line 8678
    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-direct {p0}, Landroid/support/design/widget/br;->d()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, p2, v2}, Landroid/support/design/widget/bw;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 8679
    if-eqz p3, :cond_1f

    .line 8680
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/cc;->setSelected(Z)V

    .line 385
    :cond_1f
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/br;->b(Landroid/support/design/widget/bz;I)V

    .line 386
    if-eqz p3, :cond_27

    .line 387
    invoke-virtual {p1}, Landroid/support/design/widget/bz;->a()V

    .line 389
    :cond_27
    return-void
.end method

.method private a(Landroid/widget/LinearLayout$LayoutParams;)V
    .registers 4

    .prologue
    .line 692
    iget v0, p0, Landroid/support/design/widget/br;->y:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_11

    iget v0, p0, Landroid/support/design/widget/br;->x:I

    if-nez v0, :cond_11

    .line 693
    const/4 v0, 0x0

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 694
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 699
    :goto_10
    return-void

    .line 696
    :cond_11
    const/4 v0, -0x2

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 697
    const/4 v0, 0x0

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_10
.end method

.method static synthetic b(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->m:I

    return v0
.end method

.method static synthetic b(Landroid/support/design/widget/br;I)I
    .registers 3

    .prologue
    .line 96
    invoke-direct {p0, p1}, Landroid/support/design/widget/br;->d(I)I

    move-result v0

    return v0
.end method

.method private static b(II)Landroid/content/res/ColorStateList;
    .registers 7

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1537
    new-array v0, v1, [[I

    .line 1538
    new-array v1, v1, [I

    .line 1541
    sget-object v2, Landroid/support/design/widget/br;->SELECTED_STATE_SET:[I

    aput-object v2, v0, v3

    .line 1542
    aput p1, v1, v3

    .line 1546
    sget-object v2, Landroid/support/design/widget/br;->EMPTY_STATE_SET:[I

    aput-object v2, v0, v4

    .line 1547
    aput p0, v1, v4

    .line 1550
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method

.method private b()V
    .registers 4

    .prologue
    .line 483
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0}, Landroid/support/design/widget/bw;->removeAllViews()V

    .line 485
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 486
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bz;

    .line 14006
    const/4 v2, -0x1

    iput v2, v0, Landroid/support/design/widget/bz;->e:I

    .line 488
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_b

    .line 491
    :cond_1e
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    .line 492
    return-void
.end method

.method private b(I)V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 460
    iget-object v0, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    if-eqz v0, :cond_36

    iget-object v0, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    .line 11002
    iget v0, v0, Landroid/support/design/widget/bz;->e:I

    move v1, v0

    .line 11757
    :goto_a
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/bw;->removeViewAt(I)V

    .line 11758
    invoke-virtual {p0}, Landroid/support/design/widget/br;->requestLayout()V

    .line 463
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bz;

    .line 464
    if-eqz v0, :cond_1f

    .line 12006
    const/4 v3, -0x1

    iput v3, v0, Landroid/support/design/widget/bz;->e:I

    .line 468
    :cond_1f
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, p1

    .line 469
    :goto_26
    if-ge v3, v4, :cond_38

    .line 470
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bz;

    .line 13006
    iput v3, v0, Landroid/support/design/widget/bz;->e:I

    .line 469
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_26

    :cond_36
    move v1, v2

    .line 460
    goto :goto_a

    .line 473
    :cond_38
    if-ne v1, p1, :cond_47

    .line 474
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_48

    const/4 v0, 0x0

    .line 13809
    :goto_43
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/bz;Z)V

    .line 476
    :cond_47
    return-void

    .line 474
    :cond_48
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    add-int/lit8 v1, p1, -0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bz;

    goto :goto_43
.end method

.method private b(Landroid/support/design/widget/bz;)V
    .registers 4

    .prologue
    .line 446
    .line 9890
    iget-object v0, p1, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    .line 446
    if-eq v0, p0, :cond_c

    .line 447
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tab does not belong to this TabLayout."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10002
    :cond_c
    iget v0, p1, Landroid/support/design/widget/bz;->e:I

    .line 450
    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->b(I)V

    .line 451
    return-void
.end method

.method private b(Landroid/support/design/widget/bz;I)V
    .registers 6

    .prologue
    .line 652
    .line 20006
    iput p2, p1, Landroid/support/design/widget/bz;->e:I

    .line 653
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 655
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 656
    add-int/lit8 v0, p2, 0x1

    move v1, v0

    :goto_10
    if-ge v1, v2, :cond_20

    .line 657
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bz;

    .line 21006
    iput v1, v0, Landroid/support/design/widget/bz;->e:I

    .line 656
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 659
    :cond_20
    return-void
.end method

.method private b(Landroid/support/design/widget/bz;IZ)V
    .registers 7

    .prologue
    .line 677
    invoke-direct {p0, p1}, Landroid/support/design/widget/br;->c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;

    move-result-object v0

    .line 678
    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-direct {p0}, Landroid/support/design/widget/br;->d()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, p2, v2}, Landroid/support/design/widget/bw;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 679
    if-eqz p3, :cond_13

    .line 680
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/cc;->setSelected(Z)V

    .line 682
    :cond_13
    return-void
.end method

.method private b(Landroid/support/design/widget/bz;Z)V
    .registers 6
    .param p1    # Landroid/support/design/widget/bz;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 361
    .line 6890
    iget-object v0, p1, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    .line 361
    if-eq v0, p0, :cond_c

    .line 362
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tab belongs to a different TabLayout."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7669
    :cond_c
    invoke-direct {p0, p1}, Landroid/support/design/widget/br;->c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;

    move-result-object v0

    .line 7670
    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-direct {p0}, Landroid/support/design/widget/br;->d()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/support/design/widget/bw;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 7671
    if-eqz p2, :cond_1f

    .line 7672
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/cc;->setSelected(Z)V

    .line 366
    :cond_1f
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/br;->b(Landroid/support/design/widget/bz;I)V

    .line 367
    if-eqz p2, :cond_2d

    .line 368
    invoke-virtual {p1}, Landroid/support/design/widget/bz;->a()V

    .line 370
    :cond_2d
    return-void
.end method

.method static synthetic c(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->n:I

    return v0
.end method

.method private c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;
    .registers 4

    .prologue
    .line 635
    new-instance v0, Landroid/support/design/widget/cc;

    invoke-virtual {p0}, Landroid/support/design/widget/br;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Landroid/support/design/widget/cc;-><init>(Landroid/support/design/widget/br;Landroid/content/Context;Landroid/support/design/widget/bz;)V

    .line 636
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/cc;->setFocusable(Z)V

    .line 638
    iget-object v1, p0, Landroid/support/design/widget/br;->A:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_18

    .line 639
    new-instance v1, Landroid/support/design/widget/bs;

    invoke-direct {v1, p0}, Landroid/support/design/widget/bs;-><init>(Landroid/support/design/widget/br;)V

    iput-object v1, p0, Landroid/support/design/widget/br;->A:Landroid/view/View$OnClickListener;

    .line 647
    :cond_18
    iget-object v1, p0, Landroid/support/design/widget/br;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/cc;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 648
    return-object v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 629
    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v1}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v1

    :goto_7
    if-ge v0, v1, :cond_f

    .line 630
    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->c(I)V

    .line 629
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 632
    :cond_f
    return-void
.end method

.method private c(I)V
    .registers 3

    .prologue
    .line 662
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/cc;

    .line 663
    if-eqz v0, :cond_d

    .line 664
    invoke-virtual {v0}, Landroid/support/design/widget/cc;->a()V

    .line 666
    :cond_d
    return-void
.end method

.method private c(Landroid/support/design/widget/bz;Z)V
    .registers 6

    .prologue
    .line 669
    invoke-direct {p0, p1}, Landroid/support/design/widget/br;->c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;

    move-result-object v0

    .line 670
    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-direct {p0}, Landroid/support/design/widget/br;->d()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/support/design/widget/bw;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 671
    if-eqz p2, :cond_13

    .line 672
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/cc;->setSelected(Z)V

    .line 674
    :cond_13
    return-void
.end method

.method private d(I)I
    .registers 4

    .prologue
    .line 702
    invoke-virtual {p0}, Landroid/support/design/widget/br;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    int-to-float v1, p1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method static synthetic d(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->o:I

    return v0
.end method

.method private d()Landroid/widget/LinearLayout$LayoutParams;
    .registers 4

    .prologue
    .line 685
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 687
    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->a(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 688
    return-object v0
.end method

.method private d(Landroid/support/design/widget/bz;)V
    .registers 3

    .prologue
    .line 809
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/bz;Z)V

    .line 810
    return-void
.end method

.method static synthetic e(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->p:I

    return v0
.end method

.method private e()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 860
    .line 861
    iget v0, p0, Landroid/support/design/widget/br;->y:I

    if-nez v0, :cond_2c

    .line 863
    iget v0, p0, Landroid/support/design/widget/br;->w:I

    iget v2, p0, Landroid/support/design/widget/br;->m:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 865
    :goto_e
    iget-object v2, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-static {v2, v0, v1, v1, v1}, Landroid/support/v4/view/cx;->b(Landroid/view/View;IIII)V

    .line 867
    iget v0, p0, Landroid/support/design/widget/br;->y:I

    packed-switch v0, :pswitch_data_2e

    .line 876
    :goto_18
    invoke-direct {p0}, Landroid/support/design/widget/br;->f()V

    .line 877
    return-void

    .line 869
    :pswitch_1c
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/bw;->setGravity(I)V

    goto :goto_18

    .line 872
    :pswitch_23
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/design/widget/bw;->setGravity(I)V

    goto :goto_18

    :cond_2c
    move v0, v1

    goto :goto_e

    .line 867
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_23
        :pswitch_1c
    .end packed-switch
.end method

.method private e(I)V
    .registers 3

    .prologue
    .line 757
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/bw;->removeViewAt(I)V

    .line 758
    invoke-virtual {p0}, Landroid/support/design/widget/br;->requestLayout()V

    .line 759
    return-void
.end method

.method static synthetic f(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->t:I

    return v0
.end method

.method private f()V
    .registers 4

    .prologue
    .line 880
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_20

    .line 881
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 882
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->a(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 883
    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 880
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 885
    :cond_20
    return-void
.end method

.method private f(I)V
    .registers 11

    .prologue
    const/16 v8, 0x12c

    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 762
    const/4 v1, -0x1

    if-ne p1, v1, :cond_9

    .line 796
    :cond_8
    :goto_8
    return-void

    .line 766
    :cond_9
    invoke-virtual {p0}, Landroid/support/design/widget/br;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_2b

    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2b

    iget-object v3, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    .line 21351
    invoke-virtual {v3}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v4

    move v1, v0

    :goto_1c
    if-ge v1, v4, :cond_32

    .line 21352
    invoke-virtual {v3, v1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 21353
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    if-gtz v5, :cond_2f

    move v1, v2

    .line 766
    :goto_29
    if-eqz v1, :cond_34

    .line 770
    :cond_2b
    invoke-virtual {p0, p1, v7, v2}, Landroid/support/design/widget/br;->a(IFZ)V

    goto :goto_8

    .line 21351
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    goto :goto_1c

    :cond_32
    move v1, v0

    .line 21357
    goto :goto_29

    .line 774
    :cond_34
    invoke-virtual {p0}, Landroid/support/design/widget/br;->getScrollX()I

    move-result v1

    .line 775
    invoke-direct {p0, p1, v7}, Landroid/support/design/widget/br;->a(IF)I

    move-result v3

    .line 777
    if-eq v1, v3, :cond_6a

    .line 778
    iget-object v4, p0, Landroid/support/design/widget/br;->B:Landroid/support/design/widget/ck;

    if-nez v4, :cond_5e

    .line 779
    invoke-static {}, Landroid/support/design/widget/dh;->a()Landroid/support/design/widget/ck;

    move-result-object v4

    iput-object v4, p0, Landroid/support/design/widget/br;->B:Landroid/support/design/widget/ck;

    .line 780
    iget-object v4, p0, Landroid/support/design/widget/br;->B:Landroid/support/design/widget/ck;

    sget-object v5, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/support/design/widget/ck;->a(Landroid/view/animation/Interpolator;)V

    .line 781
    iget-object v4, p0, Landroid/support/design/widget/br;->B:Landroid/support/design/widget/ck;

    invoke-virtual {v4, v8}, Landroid/support/design/widget/ck;->a(I)V

    .line 782
    iget-object v4, p0, Landroid/support/design/widget/br;->B:Landroid/support/design/widget/ck;

    new-instance v5, Landroid/support/design/widget/bt;

    invoke-direct {v5, p0}, Landroid/support/design/widget/bt;-><init>(Landroid/support/design/widget/br;)V

    invoke-virtual {v4, v5}, Landroid/support/design/widget/ck;->a(Landroid/support/design/widget/cp;)V

    .line 790
    :cond_5e
    iget-object v4, p0, Landroid/support/design/widget/br;->B:Landroid/support/design/widget/ck;

    invoke-virtual {v4, v1, v3}, Landroid/support/design/widget/ck;->a(II)V

    .line 791
    iget-object v1, p0, Landroid/support/design/widget/br;->B:Landroid/support/design/widget/ck;

    .line 22116
    iget-object v1, v1, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v1}, Landroid/support/design/widget/cr;->a()V

    .line 795
    :cond_6a
    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    .line 22460
    invoke-static {v1}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v3

    if-ne v3, v2, :cond_73

    move v0, v2

    .line 22463
    :cond_73
    invoke-virtual {v1, p1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 22464
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 22465
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v5

    .line 22469
    iget v4, v1, Landroid/support/design/widget/bw;->a:I

    sub-int v4, p1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-gt v4, v2, :cond_c4

    .line 22471
    iget v2, v1, Landroid/support/design/widget/bw;->c:I

    .line 22472
    iget v4, v1, Landroid/support/design/widget/bw;->d:I

    .line 22493
    :goto_8d
    if-ne v2, v3, :cond_91

    if-eq v4, v5, :cond_8

    .line 22494
    :cond_91
    iget-object v0, v1, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    invoke-static {}, Landroid/support/design/widget/dh;->a()Landroid/support/design/widget/ck;

    move-result-object v6

    .line 24096
    iput-object v6, v0, Landroid/support/design/widget/br;->C:Landroid/support/design/widget/ck;

    .line 22495
    sget-object v0, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v6, v0}, Landroid/support/design/widget/ck;->a(Landroid/view/animation/Interpolator;)V

    .line 22496
    invoke-virtual {v6, v8}, Landroid/support/design/widget/ck;->a(I)V

    .line 22497
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7, v0}, Landroid/support/design/widget/ck;->a(FF)V

    .line 22498
    new-instance v0, Landroid/support/design/widget/bx;

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/bx;-><init>(Landroid/support/design/widget/bw;IIII)V

    invoke-virtual {v6, v0}, Landroid/support/design/widget/ck;->a(Landroid/support/design/widget/cp;)V

    .line 22507
    new-instance v0, Landroid/support/design/widget/by;

    invoke-direct {v0, v1, p1}, Landroid/support/design/widget/by;-><init>(Landroid/support/design/widget/bw;I)V

    .line 24142
    iget-object v1, v6, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    new-instance v2, Landroid/support/design/widget/cm;

    invoke-direct {v2, v6, v0}, Landroid/support/design/widget/cm;-><init>(Landroid/support/design/widget/ck;Landroid/support/design/widget/cn;)V

    invoke-virtual {v1, v2}, Landroid/support/design/widget/cr;->a(Landroid/support/design/widget/cs;)V

    .line 25116
    iget-object v0, v6, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->a()V

    goto/16 :goto_8

    .line 22475
    :cond_c4
    iget-object v2, v1, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    .line 23096
    const/16 v4, 0x18

    invoke-direct {v2, v4}, Landroid/support/design/widget/br;->d(I)I

    move-result v2

    .line 22476
    iget v4, v1, Landroid/support/design/widget/bw;->a:I

    if-ge p1, v4, :cond_d6

    .line 22478
    if-nez v0, :cond_dc

    .line 22481
    add-int v4, v5, v2

    move v2, v4

    goto :goto_8d

    .line 22485
    :cond_d6
    if-eqz v0, :cond_dc

    .line 22486
    add-int v4, v5, v2

    move v2, v4

    goto :goto_8d

    .line 22488
    :cond_dc
    sub-int v4, v3, v2

    move v2, v4

    goto :goto_8d
.end method

.method static synthetic g(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->u:I

    return v0
.end method

.method private g(I)Landroid/content/res/ColorStateList;
    .registers 4

    .prologue
    .line 1554
    invoke-virtual {p0}, Landroid/support/design/widget/br;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/design/n;->TextAppearance:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1557
    :try_start_a
    sget v0, Landroid/support/design/n;->TextAppearance_android_textColor:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;
    :try_end_f
    .catchall {:try_start_a .. :try_end_f} :catchall_14

    move-result-object v0

    .line 1559
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-object v0

    :catchall_14
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private getScrollPosition()F
    .registers 3

    .prologue
    .line 330
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    .line 2367
    iget v1, v0, Landroid/support/design/widget/bw;->a:I

    int-to-float v1, v1

    iget v0, v0, Landroid/support/design/widget/bw;->b:F

    add-float/2addr v0, v1

    .line 330
    return v0
.end method

.method static synthetic h(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->q:I

    return v0
.end method

.method static synthetic i(Landroid/support/design/widget/br;)Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 96
    iget-object v0, p0, Landroid/support/design/widget/br;->r:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method static synthetic j(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->y:I

    return v0
.end method

.method static synthetic k(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Landroid/support/design/widget/br;->x:I

    return v0
.end method

.method static synthetic l(Landroid/support/design/widget/br;)I
    .registers 2

    .prologue
    .line 96
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/br;->x:I

    return v0
.end method

.method static synthetic m(Landroid/support/design/widget/br;)V
    .registers 1

    .prologue
    .line 96
    invoke-direct {p0}, Landroid/support/design/widget/br;->f()V

    return-void
.end method

.method private setSelectedTabView(I)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 799
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v3

    .line 800
    if-ge p1, v3, :cond_2a

    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_2a

    move v2, v1

    .line 801
    :goto_16
    if-ge v2, v3, :cond_2a

    .line 802
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 803
    if-ne v2, p1, :cond_28

    const/4 v0, 0x1

    :goto_21
    invoke-virtual {v4, v0}, Landroid/view/View;->setSelected(Z)V

    .line 801
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_16

    :cond_28
    move v0, v1

    .line 803
    goto :goto_21

    .line 806
    :cond_2a
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/support/design/widget/bz;
    .registers 3
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 427
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bz;

    return-object v0
.end method

.method public final a(IFZ)V
    .registers 6

    .prologue
    .line 312
    iget-object v0, p0, Landroid/support/design/widget/br;->C:Landroid/support/design/widget/ck;

    if-eqz v0, :cond_f

    iget-object v0, p0, Landroid/support/design/widget/br;->C:Landroid/support/design/widget/ck;

    .line 2120
    iget-object v0, v0, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->b()Z

    move-result v0

    .line 312
    if-eqz v0, :cond_f

    .line 327
    :cond_e
    :goto_e
    return-void

    .line 315
    :cond_f
    if-ltz p1, :cond_e

    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_e

    .line 320
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    .line 2361
    iput p1, v0, Landroid/support/design/widget/bw;->a:I

    .line 2362
    iput p2, v0, Landroid/support/design/widget/bw;->b:F

    .line 2363
    invoke-virtual {v0}, Landroid/support/design/widget/bw;->a()V

    .line 321
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/br;->a(IF)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/support/design/widget/br;->scrollTo(II)V

    .line 324
    if-eqz p3, :cond_e

    .line 325
    int-to-float v0, p1

    add-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->setSelectedTabView(I)V

    goto :goto_e
.end method

.method final a(Landroid/support/design/widget/bz;Z)V
    .registers 6

    .prologue
    const/4 v1, -0x1

    .line 813
    iget-object v0, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    if-ne v0, p1, :cond_f

    .line 814
    iget-object v0, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    if-eqz v0, :cond_e

    .line 26002
    iget v0, p1, Landroid/support/design/widget/bz;->e:I

    .line 818
    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->f(I)V

    .line 840
    :cond_e
    :goto_e
    return-void

    .line 821
    :cond_f
    if-eqz p1, :cond_3b

    .line 27002
    iget v0, p1, Landroid/support/design/widget/bz;->e:I

    .line 822
    :goto_13
    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->setSelectedTabView(I)V

    .line 823
    if-eqz p2, :cond_29

    .line 824
    iget-object v2, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    if-eqz v2, :cond_22

    iget-object v2, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    .line 28002
    iget v2, v2, Landroid/support/design/widget/bz;->e:I

    .line 824
    if-ne v2, v1, :cond_3d

    :cond_22
    if-eq v0, v1, :cond_3d

    .line 827
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/support/design/widget/br;->a(IFZ)V

    .line 835
    :cond_29
    :goto_29
    iput-object p1, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    .line 836
    iget-object v0, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/design/widget/br;->z:Landroid/support/design/widget/bv;

    if-eqz v0, :cond_e

    .line 837
    iget-object v0, p0, Landroid/support/design/widget/br;->z:Landroid/support/design/widget/bv;

    iget-object v1, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    invoke-interface {v0, v1}, Landroid/support/design/widget/bv;->a(Landroid/support/design/widget/bz;)V

    goto :goto_e

    :cond_3b
    move v0, v1

    .line 821
    goto :goto_13

    .line 829
    :cond_3d
    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->f(I)V

    goto :goto_29
.end method

.method public final getSelectedTabPosition()I
    .registers 2

    .prologue
    .line 436
    iget-object v0, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    .line 9002
    iget v0, v0, Landroid/support/design/widget/bz;->e:I

    .line 436
    :goto_8
    return v0

    :cond_9
    const/4 v0, -0x1

    goto :goto_8
.end method

.method public final getTabCount()I
    .registers 2

    .prologue
    .line 419
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getTabGravity()I
    .registers 2

    .prologue
    .line 543
    iget v0, p0, Landroid/support/design/widget/br;->x:I

    return v0
.end method

.method public final getTabMode()I
    .registers 2

    .prologue
    .line 521
    iget v0, p0, Landroid/support/design/widget/br;->y:I

    return v0
.end method

.method public final getTabTextColors()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 561
    iget-object v0, p0, Landroid/support/design/widget/br;->r:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method protected final onMeasure(II)V
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    .line 709
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->d(I)I

    move-result v0

    invoke-virtual {p0}, Landroid/support/design/widget/br;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/design/widget/br;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 710
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_7e

    .line 722
    :goto_1a
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 724
    iget v0, p0, Landroid/support/design/widget/br;->y:I

    if-ne v0, v2, :cond_50

    invoke-virtual {p0}, Landroid/support/design/widget/br;->getChildCount()I

    move-result v0

    if-ne v0, v2, :cond_50

    .line 727
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/br;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 728
    invoke-virtual {p0}, Landroid/support/design/widget/br;->getMeasuredWidth()I

    move-result v1

    .line 730
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-le v2, v1, :cond_50

    .line 733
    invoke-virtual {p0}, Landroid/support/design/widget/br;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/design/widget/br;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v2, v3}, Landroid/support/design/widget/br;->getChildMeasureSpec(III)I

    move-result v2

    .line 735
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 736
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 742
    :cond_50
    iget v0, p0, Landroid/support/design/widget/br;->v:I

    .line 743
    invoke-virtual {p0}, Landroid/support/design/widget/br;->getMeasuredWidth()I

    move-result v1

    const/16 v2, 0x38

    invoke-direct {p0, v2}, Landroid/support/design/widget/br;->d(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 744
    if-eqz v0, :cond_61

    if-le v0, v1, :cond_62

    :cond_61
    move v0, v1

    .line 749
    :cond_62
    iget v1, p0, Landroid/support/design/widget/br;->u:I

    if-eq v1, v0, :cond_6b

    .line 751
    iput v0, p0, Landroid/support/design/widget/br;->u:I

    .line 752
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 754
    :cond_6b
    return-void

    .line 712
    :sswitch_6c
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_1a

    .line 717
    :sswitch_79
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_1a

    .line 710
    :sswitch_data_7e
    .sparse-switch
        -0x80000000 -> :sswitch_6c
        0x0 -> :sswitch_79
    .end sparse-switch
.end method

.method public final setOnTabSelectedListener(Landroid/support/design/widget/bv;)V
    .registers 2

    .prologue
    .line 398
    iput-object p1, p0, Landroid/support/design/widget/br;->z:Landroid/support/design/widget/bv;

    .line 399
    return-void
.end method

.method public final setSelectedTabIndicatorColor(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 289
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/bw;->a(I)V

    .line 290
    return-void
.end method

.method public final setSelectedTabIndicatorHeight(I)V
    .registers 3

    .prologue
    .line 298
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/bw;->b(I)V

    .line 299
    return-void
.end method

.method public final setTabGravity(I)V
    .registers 3

    .prologue
    .line 530
    iget v0, p0, Landroid/support/design/widget/br;->x:I

    if-eq v0, p1, :cond_9

    .line 531
    iput p1, p0, Landroid/support/design/widget/br;->x:I

    .line 532
    invoke-direct {p0}, Landroid/support/design/widget/br;->e()V

    .line 534
    :cond_9
    return-void
.end method

.method public final setTabMode(I)V
    .registers 3

    .prologue
    .line 508
    iget v0, p0, Landroid/support/design/widget/br;->y:I

    if-eq p1, v0, :cond_9

    .line 509
    iput p1, p0, Landroid/support/design/widget/br;->y:I

    .line 510
    invoke-direct {p0}, Landroid/support/design/widget/br;->e()V

    .line 512
    :cond_9
    return-void
.end method

.method public final setTabTextColors(Landroid/content/res/ColorStateList;)V
    .registers 4
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 550
    iget-object v0, p0, Landroid/support/design/widget/br;->r:Landroid/content/res/ColorStateList;

    if-eq v0, p1, :cond_15

    .line 551
    iput-object p1, p0, Landroid/support/design/widget/br;->r:Landroid/content/res/ColorStateList;

    .line 14629
    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v1}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v1

    :goto_d
    if-ge v0, v1, :cond_15

    .line 14630
    invoke-direct {p0, v0}, Landroid/support/design/widget/br;->c(I)V

    .line 14629
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 554
    :cond_15
    return-void
.end method

.method public final setTabsFromPagerAdapter(Landroid/support/v4/view/by;)V
    .registers 10
    .param p1    # Landroid/support/v4/view/by;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 622
    .line 16483
    iget-object v0, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-virtual {v0}, Landroid/support/design/widget/bw;->removeAllViews()V

    .line 16485
    iget-object v0, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 16486
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bz;

    .line 17006
    const/4 v2, -0x1

    iput v2, v0, Landroid/support/design/widget/bz;->e:I

    .line 16488
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_c

    .line 16491
    :cond_1f
    iput-object v7, p0, Landroid/support/design/widget/br;->k:Landroid/support/design/widget/bz;

    .line 623
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/support/v4/view/by;->e()I

    move-result v1

    :goto_26
    if-ge v0, v1, :cond_67

    .line 17410
    new-instance v2, Landroid/support/design/widget/bz;

    invoke-direct {v2, p0}, Landroid/support/design/widget/bz;-><init>(Landroid/support/design/widget/br;)V

    .line 624
    invoke-virtual {v2, v7}, Landroid/support/design/widget/bz;->a(Ljava/lang/CharSequence;)Landroid/support/design/widget/bz;

    move-result-object v2

    .line 18340
    iget-object v3, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    .line 18890
    iget-object v4, v2, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    .line 18361
    if-eq v4, p0, :cond_43

    .line 18362
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tab belongs to a different TabLayout."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19669
    :cond_43
    invoke-direct {p0, v2}, Landroid/support/design/widget/br;->c(Landroid/support/design/widget/bz;)Landroid/support/design/widget/cc;

    move-result-object v4

    .line 19670
    iget-object v5, p0, Landroid/support/design/widget/br;->l:Landroid/support/design/widget/bw;

    invoke-direct {p0}, Landroid/support/design/widget/br;->d()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/support/design/widget/bw;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 19671
    if-eqz v3, :cond_56

    .line 19672
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/support/design/widget/cc;->setSelected(Z)V

    .line 18366
    :cond_56
    iget-object v4, p0, Landroid/support/design/widget/br;->j:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {p0, v2, v4}, Landroid/support/design/widget/br;->b(Landroid/support/design/widget/bz;I)V

    .line 18367
    if-eqz v3, :cond_64

    .line 18368
    invoke-virtual {v2}, Landroid/support/design/widget/bz;->a()V

    .line 623
    :cond_64
    add-int/lit8 v0, v0, 0x1

    goto :goto_26

    .line 626
    :cond_67
    return-void
.end method

.method public final setupWithViewPager(Landroid/support/v4/view/ViewPager;)V
    .registers 5
    .param p1    # Landroid/support/v4/view/ViewPager;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 589
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/by;

    move-result-object v0

    .line 590
    if-nez v0, :cond_e

    .line 591
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ViewPager does not have a PagerAdapter set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 595
    :cond_e
    invoke-virtual {p0, v0}, Landroid/support/design/widget/br;->setTabsFromPagerAdapter(Landroid/support/v4/view/by;)V

    .line 598
    new-instance v1, Landroid/support/design/widget/cb;

    invoke-direct {v1, p0}, Landroid/support/design/widget/cb;-><init>(Landroid/support/design/widget/br;)V

    .line 15606
    iget-object v2, p1, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    if-nez v2, :cond_21

    .line 15607
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p1, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    .line 15609
    :cond_21
    iget-object v2, p1, Landroid/support/v4/view/ViewPager;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 601
    new-instance v1, Landroid/support/design/widget/cd;

    invoke-direct {v1, p1}, Landroid/support/design/widget/cd;-><init>(Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {p0, v1}, Landroid/support/design/widget/br;->setOnTabSelectedListener(Landroid/support/design/widget/bv;)V

    .line 604
    invoke-virtual {v0}, Landroid/support/v4/view/by;->e()I

    move-result v0

    if-lez v0, :cond_46

    .line 605
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 606
    invoke-virtual {p0}, Landroid/support/design/widget/br;->getSelectedTabPosition()I

    move-result v1

    if-eq v1, v0, :cond_46

    .line 607
    invoke-virtual {p0, v0}, Landroid/support/design/widget/br;->a(I)Landroid/support/design/widget/bz;

    move-result-object v0

    .line 15809
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/bz;Z)V

    .line 610
    :cond_46
    return-void
.end method
