.class public final Landroid/support/design/widget/cb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/ev;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/support/design/widget/br;)V
    .registers 3

    .prologue
    .line 1578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1579
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/design/widget/cb;->a:Ljava/lang/ref/WeakReference;

    .line 1580
    return-void
.end method


# virtual methods
.method public final a(I)V
    .registers 3

    .prologue
    .line 1584
    iget v0, p0, Landroid/support/design/widget/cb;->c:I

    iput v0, p0, Landroid/support/design/widget/cb;->b:I

    .line 1585
    iput p1, p0, Landroid/support/design/widget/cb;->c:I

    .line 1586
    return-void
.end method

.method public final a(IF)V
    .registers 7

    .prologue
    const/4 v1, 0x1

    .line 1591
    iget-object v0, p0, Landroid/support/design/widget/cb;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/br;

    .line 1592
    if-eqz v0, :cond_1b

    .line 1595
    iget v2, p0, Landroid/support/design/widget/cb;->c:I

    if-eq v2, v1, :cond_18

    iget v2, p0, Landroid/support/design/widget/cb;->c:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1c

    iget v2, p0, Landroid/support/design/widget/cb;->b:I

    if-ne v2, v1, :cond_1c

    .line 1598
    :cond_18
    :goto_18
    invoke-virtual {v0, p1, p2, v1}, Landroid/support/design/widget/br;->a(IFZ)V

    .line 1600
    :cond_1b
    return-void

    .line 1595
    :cond_1c
    const/4 v1, 0x0

    goto :goto_18
.end method

.method public final b(I)V
    .registers 5

    .prologue
    .line 1604
    iget-object v0, p0, Landroid/support/design/widget/cb;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/br;

    .line 1605
    if-eqz v0, :cond_16

    .line 1608
    invoke-virtual {v0, p1}, Landroid/support/design/widget/br;->a(I)Landroid/support/design/widget/bz;

    move-result-object v2

    iget v1, p0, Landroid/support/design/widget/cb;->c:I

    if-nez v1, :cond_17

    const/4 v1, 0x1

    :goto_13
    invoke-virtual {v0, v2, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/bz;Z)V

    .line 1611
    :cond_16
    return-void

    .line 1608
    :cond_17
    const/4 v1, 0x0

    goto :goto_13
.end method
