.class Landroid/support/design/widget/ad;
.super Landroid/support/design/widget/al;
.source "SourceFile"


# instance fields
.field a:Landroid/support/design/widget/ar;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:F

.field private l:F

.field private m:I

.field private n:Landroid/support/design/widget/bl;

.field private o:Z


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/support/design/widget/as;)V
    .registers 11

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/al;-><init>(Landroid/view/View;Landroid/support/design/widget/as;)V

    .line 51
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/ad;->m:I

    .line 53
    new-instance v0, Landroid/support/design/widget/bl;

    invoke-direct {v0}, Landroid/support/design/widget/bl;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/ad;->n:Landroid/support/design/widget/bl;

    .line 54
    iget-object v3, p0, Landroid/support/design/widget/ad;->n:Landroid/support/design/widget/bl;

    .line 1082
    invoke-virtual {v3}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v0

    .line 1083
    if-eq v0, p1, :cond_55

    .line 1086
    if-eqz v0, :cond_4c

    .line 1095
    invoke-virtual {v3}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v4

    .line 1096
    iget-object v0, v3, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    .line 1097
    :goto_2d
    if-ge v1, v5, :cond_46

    .line 1098
    iget-object v0, v3, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bn;

    iget-object v0, v0, Landroid/support/design/widget/bn;->b:Landroid/view/animation/Animation;

    .line 1099
    invoke-virtual {v4}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v6

    if-ne v6, v0, :cond_42

    .line 1100
    invoke-virtual {v4}, Landroid/view/View;->clearAnimation()V

    .line 1097
    :cond_42
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2d

    .line 1103
    :cond_46
    iput-object v7, v3, Landroid/support/design/widget/bl;->d:Ljava/lang/ref/WeakReference;

    .line 1104
    iput-object v7, v3, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    .line 1105
    iput-object v7, v3, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 1089
    :cond_4c
    if-eqz p1, :cond_55

    .line 1090
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, v3, Landroid/support/design/widget/bl;->d:Ljava/lang/ref/WeakReference;

    .line 57
    :cond_55
    iget-object v0, p0, Landroid/support/design/widget/ad;->n:Landroid/support/design/widget/bl;

    sget-object v1, Landroid/support/design/widget/ad;->c:[I

    new-instance v3, Landroid/support/design/widget/ag;

    invoke-direct {v3, p0, v2}, Landroid/support/design/widget/ag;-><init>(Landroid/support/design/widget/ad;B)V

    invoke-direct {p0, v3}, Landroid/support/design/widget/ad;->a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/support/design/widget/bl;->a([ILandroid/view/animation/Animation;)V

    .line 59
    iget-object v0, p0, Landroid/support/design/widget/ad;->n:Landroid/support/design/widget/bl;

    sget-object v1, Landroid/support/design/widget/ad;->d:[I

    new-instance v3, Landroid/support/design/widget/ag;

    invoke-direct {v3, p0, v2}, Landroid/support/design/widget/ag;-><init>(Landroid/support/design/widget/ad;B)V

    invoke-direct {p0, v3}, Landroid/support/design/widget/ad;->a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/support/design/widget/bl;->a([ILandroid/view/animation/Animation;)V

    .line 62
    iget-object v0, p0, Landroid/support/design/widget/ad;->n:Landroid/support/design/widget/bl;

    sget-object v1, Landroid/support/design/widget/ad;->e:[I

    new-instance v3, Landroid/support/design/widget/ah;

    invoke-direct {v3, p0, v2}, Landroid/support/design/widget/ah;-><init>(Landroid/support/design/widget/ad;B)V

    invoke-direct {p0, v3}, Landroid/support/design/widget/ad;->a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/bl;->a([ILandroid/view/animation/Animation;)V

    .line 64
    return-void
.end method

.method static synthetic a(Landroid/support/design/widget/ad;)F
    .registers 2

    .prologue
    .line 32
    iget v0, p0, Landroid/support/design/widget/ad;->k:F

    return v0
.end method

.method private a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .registers 4

    .prologue
    .line 207
    sget-object v0, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 208
    iget v0, p0, Landroid/support/design/widget/ad;->m:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 209
    return-object p1
.end method

.method static synthetic a(Landroid/support/design/widget/ad;Z)Z
    .registers 2

    .prologue
    .line 32
    iput-boolean p1, p0, Landroid/support/design/widget/ad;->o:Z

    return p1
.end method

.method static synthetic b(Landroid/support/design/widget/ad;)F
    .registers 2

    .prologue
    .line 32
    iget v0, p0, Landroid/support/design/widget/ad;->l:F

    return v0
.end method

.method private static b(I)Landroid/content/res/ColorStateList;
    .registers 7

    .prologue
    const/4 v1, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 250
    new-array v0, v1, [[I

    .line 251
    new-array v1, v1, [I

    .line 254
    sget-object v2, Landroid/support/design/widget/ad;->d:[I

    aput-object v2, v0, v3

    .line 255
    aput p0, v1, v3

    .line 258
    sget-object v2, Landroid/support/design/widget/ad;->c:[I

    aput-object v2, v0, v4

    .line 259
    aput p0, v1, v4

    .line 263
    new-array v2, v3, [I

    aput-object v2, v0, v5

    .line 264
    aput v3, v1, v5

    .line 267
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method

.method private e()V
    .registers 6

    .prologue
    .line 201
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 202
    iget-object v1, p0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/ar;->getPadding(Landroid/graphics/Rect;)Z

    .line 203
    iget-object v1, p0, Landroid/support/design/widget/ad;->g:Landroid/support/design/widget/as;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-interface {v1, v2, v3, v4, v0}, Landroid/support/design/widget/as;->a(IIII)V

    .line 204
    return-void
.end method


# virtual methods
.method a()V
    .registers 4

    .prologue
    .line 156
    iget-object v0, p0, Landroid/support/design/widget/ad;->n:Landroid/support/design/widget/bl;

    .line 4164
    iget-object v1, v0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-eqz v1, :cond_17

    .line 4165
    invoke-virtual {v0}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v1

    .line 4166
    if-eqz v1, :cond_17

    invoke-virtual {v1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    iget-object v0, v0, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-ne v2, v0, :cond_17

    .line 4167
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 157
    :cond_17
    return-void
.end method

.method a(F)V
    .registers 4

    .prologue
    .line 133
    iget v0, p0, Landroid/support/design/widget/ad;->k:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    if-eqz v0, :cond_17

    .line 134
    iget-object v0, p0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    iget v1, p0, Landroid/support/design/widget/ad;->l:F

    add-float/2addr v1, p1

    invoke-virtual {v0, p1, v1}, Landroid/support/design/widget/ar;->a(FF)V

    .line 135
    iput p1, p0, Landroid/support/design/widget/ad;->k:F

    .line 136
    invoke-direct {p0}, Landroid/support/design/widget/ad;->e()V

    .line 138
    :cond_17
    return-void
.end method

.method a(I)V
    .registers 4

    .prologue
    .line 128
    iget-object v0, p0, Landroid/support/design/widget/ad;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {p1}, Landroid/support/design/widget/ad;->b(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 129
    return-void
.end method

.method a(Landroid/content/res/ColorStateList;)V
    .registers 3

    .prologue
    .line 115
    iget-object v0, p0, Landroid/support/design/widget/ad;->h:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 116
    iget-object v0, p0, Landroid/support/design/widget/ad;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_e

    .line 117
    iget-object v0, p0, Landroid/support/design/widget/ad;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 119
    :cond_e
    return-void
.end method

.method a(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    .prologue
    .line 123
    iget-object v0, p0, Landroid/support/design/widget/ad;->h:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 124
    return-void
.end method

.method a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;II)V
    .registers 14

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 71
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/e/a/a;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/ad;->h:Landroid/graphics/drawable/Drawable;

    .line 72
    iget-object v0, p0, Landroid/support/design/widget/ad;->h:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p2}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 73
    if-eqz p3, :cond_19

    .line 74
    iget-object v0, p0, Landroid/support/design/widget/ad;->h:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p3}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 80
    :cond_19
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 81
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 82
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 83
    iget-object v1, p0, Landroid/support/design/widget/ad;->g:Landroid/support/design/widget/as;

    invoke-interface {v1}, Landroid/support/design/widget/as;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 87
    invoke-static {v0}, Landroid/support/v4/e/a/a;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/ad;->i:Landroid/graphics/drawable/Drawable;

    .line 88
    iget-object v0, p0, Landroid/support/design/widget/ad;->i:Landroid/graphics/drawable/Drawable;

    invoke-static {p4}, Landroid/support/design/widget/ad;->b(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 89
    iget-object v0, p0, Landroid/support/design/widget/ad;->i:Landroid/graphics/drawable/Drawable;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v0, v1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 92
    if-lez p5, :cond_8d

    .line 93
    invoke-virtual {p0, p5, p2}, Landroid/support/design/widget/ad;->a(ILandroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/ad;->j:Landroid/graphics/drawable/Drawable;

    .line 94
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/design/widget/ad;->j:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v7

    iget-object v1, p0, Landroid/support/design/widget/ad;->h:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v2

    iget-object v1, p0, Landroid/support/design/widget/ad;->i:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v3

    move-object v3, v0

    .line 100
    :goto_5c
    new-instance v0, Landroid/support/design/widget/ar;

    iget-object v1, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Landroid/support/design/widget/ad;->g:Landroid/support/design/widget/as;

    invoke-interface {v3}, Landroid/support/design/widget/as;->a()F

    move-result v3

    iget v4, p0, Landroid/support/design/widget/ad;->k:F

    iget v5, p0, Landroid/support/design/widget/ad;->k:F

    iget v6, p0, Landroid/support/design/widget/ad;->l:F

    add-float/2addr v5, v6

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/ar;-><init>(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;FFF)V

    iput-object v0, p0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    .line 106
    iget-object v0, p0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    .line 2105
    iput-boolean v7, v0, Landroid/support/design/widget/ar;->o:Z

    .line 2106
    invoke-virtual {v0}, Landroid/support/design/widget/ar;->invalidateSelf()V

    .line 108
    iget-object v0, p0, Landroid/support/design/widget/ad;->g:Landroid/support/design/widget/as;

    iget-object v1, p0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    invoke-interface {v0, v1}, Landroid/support/design/widget/as;->a(Landroid/graphics/drawable/Drawable;)V

    .line 110
    invoke-direct {p0}, Landroid/support/design/widget/ad;->e()V

    .line 111
    return-void

    .line 96
    :cond_8d
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/ad;->j:Landroid/graphics/drawable/Drawable;

    .line 97
    new-array v0, v3, [Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/design/widget/ad;->h:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v7

    iget-object v1, p0, Landroid/support/design/widget/ad;->i:Landroid/graphics/drawable/Drawable;

    aput-object v1, v0, v2

    move-object v3, v0

    goto :goto_5c
.end method

.method a([I)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 151
    iget-object v3, p0, Landroid/support/design/widget/ad;->n:Landroid/support/design/widget/bl;

    .line 3113
    iget-object v0, v3, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 3114
    const/4 v0, 0x0

    move v2, v0

    :goto_b
    if-ge v2, v4, :cond_54

    .line 3115
    iget-object v0, v3, Landroid/support/design/widget/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/bn;

    .line 3116
    iget-object v5, v0, Landroid/support/design/widget/bn;->a:[I

    invoke-static {v5, p1}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v5

    if-eqz v5, :cond_50

    .line 3121
    :goto_1d
    iget-object v2, v3, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    if-eq v0, v2, :cond_4f

    .line 3124
    iget-object v2, v3, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    if-eqz v2, :cond_3c

    .line 3143
    iget-object v2, v3, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-eqz v2, :cond_3c

    .line 3144
    invoke-virtual {v3}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v2

    .line 3145
    if-eqz v2, :cond_3a

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    iget-object v5, v3, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    if-ne v4, v5, :cond_3a

    .line 3146
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 3148
    :cond_3a
    iput-object v1, v3, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 3127
    :cond_3c
    iput-object v0, v3, Landroid/support/design/widget/bl;->b:Landroid/support/design/widget/bn;

    .line 3128
    if-eqz v0, :cond_4f

    .line 4134
    iget-object v0, v0, Landroid/support/design/widget/bn;->b:Landroid/view/animation/Animation;

    iput-object v0, v3, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    .line 4136
    invoke-virtual {v3}, Landroid/support/design/widget/bl;->a()Landroid/view/View;

    move-result-object v0

    .line 4137
    if-eqz v0, :cond_4f

    .line 4138
    iget-object v1, v3, Landroid/support/design/widget/bl;->c:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 152
    :cond_4f
    return-void

    .line 3114
    :cond_50
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_b

    :cond_54
    move-object v0, v1

    goto :goto_1d
.end method

.method b()V
    .registers 5

    .prologue
    .line 161
    iget-boolean v0, p0, Landroid/support/design/widget/ad;->o:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_d

    .line 183
    :cond_c
    :goto_c
    return-void

    .line 166
    :cond_d
    iget-object v0, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Landroid/support/design/c;->design_fab_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 168
    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 169
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 170
    new-instance v1, Landroid/support/design/widget/ae;

    invoke-direct {v1, p0}, Landroid/support/design/widget/ae;-><init>(Landroid/support/design/widget/ad;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 182
    iget-object v1, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_c
.end method

.method b(F)V
    .registers 5

    .prologue
    .line 142
    iget v0, p0, Landroid/support/design/widget/ad;->l:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_19

    iget-object v0, p0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    if-eqz v0, :cond_19

    .line 143
    iput p1, p0, Landroid/support/design/widget/ad;->l:F

    .line 144
    iget-object v0, p0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    iget v1, p0, Landroid/support/design/widget/ad;->k:F

    add-float/2addr v1, p1

    .line 2328
    iget v2, v0, Landroid/support/design/widget/ar;->n:F

    invoke-virtual {v0, v2, v1}, Landroid/support/design/widget/ar;->a(FF)V

    .line 145
    invoke-direct {p0}, Landroid/support/design/widget/ad;->e()V

    .line 147
    :cond_19
    return-void
.end method

.method c()V
    .registers 5

    .prologue
    .line 187
    iget-object v0, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_c

    iget-boolean v0, p0, Landroid/support/design/widget/ad;->o:Z

    if-eqz v0, :cond_32

    .line 190
    :cond_c
    iget-object v0, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 191
    iget-object v0, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Landroid/support/design/c;->design_fab_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 194
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 195
    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 196
    iget-object v1, p0, Landroid/support/design/widget/ad;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 198
    :cond_32
    return-void
.end method
