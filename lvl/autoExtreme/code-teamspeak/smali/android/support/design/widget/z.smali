.class final Landroid/support/design/widget/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)I
    .registers 5

    .prologue
    .line 1581
    invoke-static {p0}, Landroid/support/v4/view/cx;->C(Landroid/view/View;)F

    move-result v0

    .line 1582
    invoke-static {p1}, Landroid/support/v4/view/cx;->C(Landroid/view/View;)F

    move-result v1

    .line 1583
    cmpl-float v2, v0, v1

    if-lez v2, :cond_e

    .line 1584
    const/4 v0, -0x1

    .line 1588
    :goto_d
    return v0

    .line 1585
    :cond_e
    cmpg-float v0, v0, v1

    if-gez v0, :cond_14

    .line 1586
    const/4 v0, 0x1

    goto :goto_d

    .line 1588
    :cond_14
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 1578
    check-cast p1, Landroid/view/View;

    check-cast p2, Landroid/view/View;

    .line 2581
    invoke-static {p1}, Landroid/support/v4/view/cx;->C(Landroid/view/View;)F

    move-result v0

    .line 2582
    invoke-static {p2}, Landroid/support/v4/view/cx;->C(Landroid/view/View;)F

    move-result v1

    .line 2583
    cmpl-float v2, v0, v1

    if-lez v2, :cond_12

    .line 2584
    const/4 v0, -0x1

    .line 2586
    :goto_11
    return v0

    .line 2585
    :cond_12
    cmpg-float v0, v0, v1

    if-gez v0, :cond_18

    .line 2586
    const/4 v0, 0x1

    goto :goto_11

    .line 2588
    :cond_18
    const/4 v0, 0x0

    .line 1578
    goto :goto_11
.end method
