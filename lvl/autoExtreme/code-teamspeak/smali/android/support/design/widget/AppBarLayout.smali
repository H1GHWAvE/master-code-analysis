.class public final Landroid/support/design/widget/AppBarLayout;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation runtime Landroid/support/design/widget/u;
    a = Landroid/support/design/widget/AppBarLayout$Behavior;
.end annotation


# static fields
.field private static final d:I = 0x0

.field private static final e:I = 0x1

.field private static final f:I = 0x2

.field private static final g:I = 0x4

.field private static final h:I = -0x1


# instance fields
.field a:Z

.field b:I

.field final c:Ljava/util/List;

.field private i:I

.field private j:I

.field private k:I

.field private l:F

.field private m:Landroid/support/v4/view/gh;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/AppBarLayout;-><init>(Landroid/content/Context;B)V

    .line 139
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 7

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 142
    invoke-direct {p0, p1, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 123
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->i:I

    .line 124
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->j:I

    .line 125
    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->k:I

    .line 131
    iput v2, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 143
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/design/widget/AppBarLayout;->setOrientation(I)V

    .line 145
    sget-object v0, Landroid/support/design/n;->AppBarLayout:[I

    sget v1, Landroid/support/design/m;->Widget_Design_AppBarLayout:I

    invoke-virtual {p1, v3, v0, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 147
    sget v1, Landroid/support/design/n;->AppBarLayout_elevation:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/design/widget/AppBarLayout;->l:F

    .line 148
    sget v1, Landroid/support/design/n;->AppBarLayout_android_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/AppBarLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 149
    sget v1, Landroid/support/design/n;->AppBarLayout_expanded:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 150
    sget v1, Landroid/support/design/n;->AppBarLayout_expanded:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/AppBarLayout;->setExpanded(Z)V

    .line 152
    :cond_3d
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 155
    invoke-static {p0}, Landroid/support/design/widget/dh;->a(Landroid/view/View;)V

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout;->c:Ljava/util/List;

    .line 159
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->l:F

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 161
    new-instance v0, Landroid/support/design/widget/c;

    invoke-direct {v0, p0}, Landroid/support/design/widget/c;-><init>(Landroid/support/design/widget/AppBarLayout;)V

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/bx;)V

    .line 170
    return-void
.end method

.method private static a()Landroid/support/design/widget/g;
    .registers 1

    .prologue
    .line 268
    new-instance v0, Landroid/support/design/widget/g;

    invoke-direct {v0}, Landroid/support/design/widget/g;-><init>()V

    return-object v0
.end method

.method private a(Landroid/util/AttributeSet;)Landroid/support/design/widget/g;
    .registers 4

    .prologue
    .line 273
    new-instance v0, Landroid/support/design/widget/g;

    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/design/widget/g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/design/widget/g;
    .registers 2

    .prologue
    .line 278
    instance-of v0, p0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_c

    .line 279
    new-instance v0, Landroid/support/design/widget/g;

    check-cast p0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p0}, Landroid/support/design/widget/g;-><init>(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 283
    :goto_b
    return-object v0

    .line 280
    :cond_c
    instance-of v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_18

    .line 281
    new-instance v0, Landroid/support/design/widget/g;

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p0}, Landroid/support/design/widget/g;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_b

    .line 283
    :cond_18
    new-instance v0, Landroid/support/design/widget/g;

    invoke-direct {v0, p0}, Landroid/support/design/widget/g;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_b
.end method

.method static synthetic a(Landroid/support/design/widget/AppBarLayout;)Ljava/util/List;
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Landroid/support/design/widget/AppBarLayout;Landroid/support/v4/view/gh;)V
    .registers 2

    .prologue
    .line 98
    invoke-direct {p0, p1}, Landroid/support/design/widget/AppBarLayout;->setWindowInsets(Landroid/support/v4/view/gh;)V

    return-void
.end method

.method private a(Landroid/support/design/widget/i;)V
    .registers 3

    .prologue
    .line 180
    if-eqz p1, :cond_f

    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 181
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_f
    return-void
.end method

.method private a(ZZ)V
    .registers 5

    .prologue
    .line 256
    if-eqz p1, :cond_e

    const/4 v0, 0x1

    move v1, v0

    :goto_4
    if-eqz p2, :cond_11

    const/4 v0, 0x4

    :goto_7
    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 258
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->requestLayout()V

    .line 259
    return-void

    .line 256
    :cond_e
    const/4 v0, 0x2

    move v1, v0

    goto :goto_4

    :cond_11
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private b(Landroid/support/design/widget/i;)V
    .registers 3

    .prologue
    .line 191
    if-eqz p1, :cond_7

    .line 192
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 194
    :cond_7
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 287
    iget-boolean v0, p0, Landroid/support/design/widget/AppBarLayout;->a:Z

    return v0
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 331
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private d()V
    .registers 2

    .prologue
    .line 462
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 463
    return-void
.end method

.method private setWindowInsets(Landroid/support/v4/view/gh;)V
    .registers 5

    .prologue
    .line 467
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->i:I

    .line 468
    iput-object p1, p0, Landroid/support/design/widget/AppBarLayout;->m:Landroid/support/v4/view/gh;

    .line 471
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v1

    :goto_a
    if-ge v0, v1, :cond_1d

    .line 472
    invoke-virtual {p0, v0}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 473
    invoke-static {v2, p1}, Landroid/support/v4/view/cx;->b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;

    move-result-object p1

    .line 474
    invoke-virtual {p1}, Landroid/support/v4/view/gh;->g()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 471
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 478
    :cond_1d
    return-void
.end method


# virtual methods
.method protected final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3

    .prologue
    .line 263
    instance-of v0, p1, Landroid/support/design/widget/g;

    return v0
.end method

.method protected final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    .prologue
    .line 3268
    new-instance v0, Landroid/support/design/widget/g;

    invoke-direct {v0}, Landroid/support/design/widget/g;-><init>()V

    .line 97
    return-object v0
.end method

.method protected final synthetic generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .registers 2

    .prologue
    .line 2268
    new-instance v0, Landroid/support/design/widget/g;

    invoke-direct {v0}, Landroid/support/design/widget/g;-><init>()V

    .line 97
    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 97
    invoke-direct {p0, p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/util/AttributeSet;)Landroid/support/design/widget/g;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 97
    invoke-static {p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/design/widget/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3

    .prologue
    .line 97
    invoke-direct {p0, p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/util/AttributeSet;)Landroid/support/design/widget/g;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .registers 3

    .prologue
    .line 97
    invoke-static {p1}, Landroid/support/design/widget/AppBarLayout;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/design/widget/g;

    move-result-object v0

    return-object v0
.end method

.method final getDownNestedPreScrollRange()I
    .registers 9

    .prologue
    .line 345
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    .line 347
    iget v2, p0, Landroid/support/design/widget/AppBarLayout;->j:I

    .line 376
    :goto_7
    return v2

    .line 350
    :cond_8
    const/4 v2, 0x0

    .line 351
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_10
    if-ltz v3, :cond_4c

    .line 352
    invoke-virtual {p0, v3}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 353
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/g;

    .line 354
    invoke-static {v4}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_41

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 357
    :goto_26
    iget v5, v0, Landroid/support/design/widget/g;->f:I

    .line 359
    and-int/lit8 v6, v5, 0x5

    const/4 v7, 0x5

    if-ne v6, v7, :cond_48

    .line 361
    iget v6, v0, Landroid/support/design/widget/g;->topMargin:I

    iget v0, v0, Landroid/support/design/widget/g;->bottomMargin:I

    add-int/2addr v0, v6

    add-int/2addr v0, v2

    .line 363
    and-int/lit8 v2, v5, 0x8

    if-eqz v2, :cond_46

    .line 365
    invoke-static {v4}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :goto_3c
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v2, v0

    goto :goto_10

    .line 354
    :cond_41
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    goto :goto_26

    .line 368
    :cond_46
    add-int/2addr v0, v1

    goto :goto_3c

    .line 370
    :cond_48
    if-gtz v2, :cond_4c

    move v0, v2

    goto :goto_3c

    .line 376
    :cond_4c
    iput v2, p0, Landroid/support/design/widget/AppBarLayout;->j:I

    goto :goto_7
.end method

.method final getDownNestedScrollRange()I
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 383
    iget v1, p0, Landroid/support/design/widget/AppBarLayout;->k:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_9

    .line 385
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->k:I

    .line 415
    :goto_8
    return v0

    .line 389
    :cond_9
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_f
    if-ge v3, v4, :cond_46

    .line 390
    invoke-virtual {p0, v3}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 391
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/g;

    .line 392
    invoke-static {v5}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3d

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 395
    :goto_25
    iget v6, v0, Landroid/support/design/widget/g;->topMargin:I

    iget v7, v0, Landroid/support/design/widget/g;->bottomMargin:I

    add-int/2addr v6, v7

    add-int/2addr v1, v6

    .line 397
    iget v0, v0, Landroid/support/design/widget/g;->f:I

    .line 399
    and-int/lit8 v6, v0, 0x1

    if-eqz v6, :cond_46

    .line 401
    add-int/2addr v2, v1

    .line 403
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_42

    .line 407
    invoke-static {v5}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v0

    sub-int v0, v2, v0

    goto :goto_8

    .line 392
    :cond_3d
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    goto :goto_25

    .line 389
    :cond_42
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_f

    .line 415
    :cond_46
    iput v2, p0, Landroid/support/design/widget/AppBarLayout;->k:I

    move v0, v2

    goto :goto_8
.end method

.method final getMinimumHeightForVisibleOverlappingContent()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 419
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->m:Landroid/support/v4/view/gh;

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout;->m:Landroid/support/v4/view/gh;

    invoke-virtual {v0}, Landroid/support/v4/view/gh;->b()I

    move-result v0

    .line 420
    :goto_b
    invoke-static {p0}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v2

    .line 421
    if-eqz v2, :cond_17

    .line 423
    mul-int/lit8 v1, v2, 0x2

    add-int/2addr v1, v0

    .line 428
    :cond_14
    :goto_14
    return v1

    :cond_15
    move v0, v1

    .line 419
    goto :goto_b

    .line 427
    :cond_17
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v2

    .line 428
    if-lez v2, :cond_14

    add-int/lit8 v1, v2, -0x1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    goto :goto_14
.end method

.method final getPendingAction()I
    .registers 2

    .prologue
    .line 458
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    return v0
.end method

.method public final getTargetElevation()F
    .registers 2

    .prologue
    .line 454
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->l:F

    return v0
.end method

.method public final getTotalScrollRange()I
    .registers 10

    .prologue
    const/4 v3, 0x0

    .line 296
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->i:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    .line 297
    iget v0, p0, Landroid/support/design/widget/AppBarLayout;->i:I

    .line 327
    :goto_8
    return v0

    .line 301
    :cond_9
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v5

    move v4, v3

    move v2, v3

    :goto_f
    if-ge v4, v5, :cond_55

    .line 302
    invoke-virtual {p0, v4}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 303
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/g;

    .line 304
    invoke-static {v6}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_4a

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 307
    :goto_25
    iget v7, v0, Landroid/support/design/widget/g;->f:I

    .line 309
    and-int/lit8 v8, v7, 0x1

    if-eqz v8, :cond_55

    .line 311
    iget v8, v0, Landroid/support/design/widget/g;->topMargin:I

    add-int/2addr v1, v8

    iget v0, v0, Landroid/support/design/widget/g;->bottomMargin:I

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 313
    and-int/lit8 v0, v7, 0x2

    if-eqz v0, :cond_4f

    .line 317
    invoke-static {v6}, Landroid/support/v4/view/cx;->o(Landroid/view/View;)I

    move-result v0

    sub-int v0, v2, v0

    .line 326
    :goto_3c
    iget-object v1, p0, Landroid/support/design/widget/AppBarLayout;->m:Landroid/support/v4/view/gh;

    if-eqz v1, :cond_53

    iget-object v1, p0, Landroid/support/design/widget/AppBarLayout;->m:Landroid/support/v4/view/gh;

    invoke-virtual {v1}, Landroid/support/v4/view/gh;->b()I

    move-result v1

    .line 327
    :goto_46
    sub-int/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->i:I

    goto :goto_8

    .line 304
    :cond_4a
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    goto :goto_25

    .line 301
    :cond_4f
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_f

    :cond_53
    move v1, v3

    .line 326
    goto :goto_46

    :cond_55
    move v0, v2

    goto :goto_3c
.end method

.method final getUpNestedPreScrollRange()I
    .registers 2

    .prologue
    .line 338
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v0

    return v0
.end method

.method protected final onLayout(ZIIII)V
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 198
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 201
    iput v1, p0, Landroid/support/design/widget/AppBarLayout;->i:I

    .line 202
    iput v1, p0, Landroid/support/design/widget/AppBarLayout;->j:I

    .line 203
    iput v1, p0, Landroid/support/design/widget/AppBarLayout;->j:I

    .line 205
    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout;->a:Z

    .line 206
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_12
    if-ge v1, v2, :cond_25

    .line 207
    invoke-virtual {p0, v1}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/g;

    .line 1621
    iget-object v0, v0, Landroid/support/design/widget/g;->g:Landroid/view/animation/Interpolator;

    .line 211
    if-eqz v0, :cond_26

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout;->a:Z

    .line 216
    :cond_25
    return-void

    .line 206
    :cond_26
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12
.end method

.method public final setExpanded(Z)V
    .registers 5

    .prologue
    .line 240
    invoke-static {p0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v2

    .line 2256
    if-eqz p1, :cond_12

    const/4 v0, 0x1

    move v1, v0

    :goto_8
    if-eqz v2, :cond_15

    const/4 v0, 0x4

    :goto_b
    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 2258
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout;->requestLayout()V

    .line 241
    return-void

    .line 2256
    :cond_12
    const/4 v0, 0x2

    move v1, v0

    goto :goto_8

    :cond_15
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final setOrientation(I)V
    .registers 4

    .prologue
    .line 220
    const/4 v0, 0x1

    if-eq p1, v0, :cond_b

    .line 221
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AppBarLayout is always vertical and does not support horizontal orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_b
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 225
    return-void
.end method

.method public final setTargetElevation(F)V
    .registers 2

    .prologue
    .line 446
    iput p1, p0, Landroid/support/design/widget/AppBarLayout;->l:F

    .line 447
    return-void
.end method
