.class final Landroid/support/design/internal/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/bx;


# instance fields
.field final synthetic a:Landroid/support/design/internal/f;


# direct methods
.method constructor <init>(Landroid/support/design/internal/f;)V
    .registers 2

    .prologue
    .line 62
    iput-object p1, p0, Landroid/support/design/internal/g;->a:Landroid/support/design/internal/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 8

    .prologue
    .line 66
    iget-object v0, p0, Landroid/support/design/internal/g;->a:Landroid/support/design/internal/f;

    invoke-static {v0}, Landroid/support/design/internal/f;->a(Landroid/support/design/internal/f;)Landroid/graphics/Rect;

    move-result-object v0

    if-nez v0, :cond_12

    .line 67
    iget-object v0, p0, Landroid/support/design/internal/g;->a:Landroid/support/design/internal/f;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-static {v0, v1}, Landroid/support/design/internal/f;->a(Landroid/support/design/internal/f;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 69
    :cond_12
    iget-object v0, p0, Landroid/support/design/internal/g;->a:Landroid/support/design/internal/f;

    invoke-static {v0}, Landroid/support/design/internal/f;->a(Landroid/support/design/internal/f;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2}, Landroid/support/v4/view/gh;->a()I

    move-result v1

    invoke-virtual {p2}, Landroid/support/v4/view/gh;->b()I

    move-result v2

    invoke-virtual {p2}, Landroid/support/v4/view/gh;->c()I

    move-result v3

    invoke-virtual {p2}, Landroid/support/v4/view/gh;->d()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 73
    iget-object v1, p0, Landroid/support/design/internal/g;->a:Landroid/support/design/internal/f;

    iget-object v0, p0, Landroid/support/design/internal/g;->a:Landroid/support/design/internal/f;

    invoke-static {v0}, Landroid/support/design/internal/f;->a(Landroid/support/design/internal/f;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_41

    iget-object v0, p0, Landroid/support/design/internal/g;->a:Landroid/support/design/internal/f;

    invoke-static {v0}, Landroid/support/design/internal/f;->b(Landroid/support/design/internal/f;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_4f

    :cond_41
    const/4 v0, 0x1

    :goto_42
    invoke-virtual {v1, v0}, Landroid/support/design/internal/f;->setWillNotDraw(Z)V

    .line 74
    iget-object v0, p0, Landroid/support/design/internal/g;->a:Landroid/support/design/internal/f;

    invoke-static {v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 75
    invoke-virtual {p2}, Landroid/support/v4/view/gh;->i()Landroid/support/v4/view/gh;

    move-result-object v0

    return-object v0

    .line 73
    :cond_4f
    const/4 v0, 0x0

    goto :goto_42
.end method
