.class abstract Landroid/support/v7/app/ak;
.super Landroid/support/v7/app/aj;
.source "SourceFile"


# instance fields
.field final e:Landroid/content/Context;

.field final f:Landroid/view/Window;

.field final g:Landroid/view/Window$Callback;

.field final h:Landroid/view/Window$Callback;

.field final i:Landroid/support/v7/app/ai;

.field j:Landroid/support/v7/app/a;

.field k:Landroid/view/MenuInflater;

.field l:Z

.field m:Z

.field n:Z

.field o:Z

.field p:Z

.field q:Ljava/lang/CharSequence;

.field r:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/app/ai;)V
    .registers 6

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/support/v7/app/aj;-><init>()V

    .line 63
    iput-object p1, p0, Landroid/support/v7/app/ak;->e:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    .line 65
    iput-object p3, p0, Landroid/support/v7/app/ak;->i:Landroid/support/v7/app/ai;

    .line 67
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/ak;->g:Landroid/view/Window$Callback;

    .line 68
    iget-object v0, p0, Landroid/support/v7/app/ak;->g:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/support/v7/app/an;

    if-eqz v0, :cond_1f

    .line 69
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AppCompat has already installed itself into the Window"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_1f
    iget-object v0, p0, Landroid/support/v7/app/ak;->g:Landroid/view/Window$Callback;

    invoke-virtual {p0, v0}, Landroid/support/v7/app/ak;->a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/ak;->h:Landroid/view/Window$Callback;

    .line 74
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    iget-object v1, p0, Landroid/support/v7/app/ak;->h:Landroid/view/Window$Callback;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    .line 75
    return-void
.end method

.method private n()Landroid/support/v7/app/a;
    .registers 2

    .prologue
    .line 92
    iget-object v0, p0, Landroid/support/v7/app/ak;->j:Landroid/support/v7/app/a;

    return-object v0
.end method

.method private o()Z
    .registers 2

    .prologue
    .line 193
    iget-boolean v0, p0, Landroid/support/v7/app/ak;->r:Z

    return v0
.end method

.method private p()Landroid/view/Window$Callback;
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    return-object v0
.end method

.method private q()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 210
    iget-object v0, p0, Landroid/support/v7/app/ak;->g:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_f

    .line 211
    iget-object v0, p0, Landroid/support/v7/app/ak;->g:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 214
    :goto_e
    return-object v0

    :cond_f
    iget-object v0, p0, Landroid/support/v7/app/ak;->q:Ljava/lang/CharSequence;

    goto :goto_e
.end method


# virtual methods
.method public final a()Landroid/support/v7/app/a;
    .registers 2

    .prologue
    .line 87
    invoke-virtual {p0}, Landroid/support/v7/app/ak;->l()V

    .line 88
    iget-object v0, p0, Landroid/support/v7/app/ak;->j:Landroid/support/v7/app/a;

    return-object v0
.end method

.method a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;
    .registers 3

    .prologue
    .line 80
    new-instance v0, Landroid/support/v7/app/an;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/app/an;-><init>(Landroid/support/v7/app/ak;Landroid/view/Window$Callback;)V

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 202
    iput-object p1, p0, Landroid/support/v7/app/ak;->q:Ljava/lang/CharSequence;

    .line 203
    invoke-virtual {p0, p1}, Landroid/support/v7/app/ak;->b(Ljava/lang/CharSequence;)V

    .line 204
    return-void
.end method

.method public a(Z)V
    .registers 2

    .prologue
    .line 184
    return-void
.end method

.method abstract a(ILandroid/view/KeyEvent;)Z
.end method

.method abstract a(Landroid/view/KeyEvent;)Z
.end method

.method abstract b(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
.end method

.method public final b()Landroid/view/MenuInflater;
    .registers 3

    .prologue
    .line 98
    iget-object v0, p0, Landroid/support/v7/app/ak;->k:Landroid/view/MenuInflater;

    if-nez v0, :cond_18

    .line 99
    invoke-virtual {p0}, Landroid/support/v7/app/ak;->l()V

    .line 100
    new-instance v1, Landroid/support/v7/internal/view/f;

    iget-object v0, p0, Landroid/support/v7/app/ak;->j:Landroid/support/v7/app/a;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Landroid/support/v7/app/ak;->j:Landroid/support/v7/app/a;

    invoke-virtual {v0}, Landroid/support/v7/app/a;->r()Landroid/content/Context;

    move-result-object v0

    :goto_13
    invoke-direct {v1, v0}, Landroid/support/v7/internal/view/f;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/app/ak;->k:Landroid/view/MenuInflater;

    .line 103
    :cond_18
    iget-object v0, p0, Landroid/support/v7/app/ak;->k:Landroid/view/MenuInflater;

    return-object v0

    .line 100
    :cond_1b
    iget-object v0, p0, Landroid/support/v7/app/ak;->e:Landroid/content/Context;

    goto :goto_13
.end method

.method abstract b(Ljava/lang/CharSequence;)V
.end method

.method abstract d(I)V
.end method

.method abstract e(I)Z
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 178
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/ak;->r:Z

    .line 179
    return-void
.end method

.method public final i()Landroid/support/v7/app/l;
    .registers 3

    .prologue
    .line 117
    new-instance v0, Landroid/support/v7/app/am;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/am;-><init>(Landroid/support/v7/app/ak;B)V

    return-object v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 189
    const/4 v0, 0x0

    return v0
.end method

.method abstract l()V
.end method

.method final m()Landroid/content/Context;
    .registers 3

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 124
    invoke-virtual {p0}, Landroid/support/v7/app/ak;->a()Landroid/support/v7/app/a;

    move-result-object v1

    .line 125
    if-eqz v1, :cond_b

    .line 126
    invoke-virtual {v1}, Landroid/support/v7/app/a;->r()Landroid/content/Context;

    move-result-object v0

    .line 129
    :cond_b
    if-nez v0, :cond_f

    .line 130
    iget-object v0, p0, Landroid/support/v7/app/ak;->e:Landroid/content/Context;

    .line 132
    :cond_f
    return-object v0
.end method
