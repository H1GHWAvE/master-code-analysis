.class public final Landroid/support/v7/app/af;
.super Landroid/support/v7/app/bf;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface;


# static fields
.field static final a:I = 0x0

.field static final b:I = 0x1


# instance fields
.field private c:Landroid/support/v7/app/v;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-static {p1, v1}, Landroid/support/v7/app/af;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/app/af;-><init>(Landroid/content/Context;IB)V

    .line 79
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 4

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/app/af;-><init>(Landroid/content/Context;IB)V

    .line 89
    return-void
.end method

.method constructor <init>(Landroid/content/Context;IB)V
    .registers 7

    .prologue
    .line 92
    invoke-static {p1, p2}, Landroid/support/v7/app/af;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/bf;-><init>(Landroid/content/Context;I)V

    .line 93
    new-instance v0, Landroid/support/v7/app/v;

    invoke-virtual {p0}, Landroid/support/v7/app/af;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/app/af;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Landroid/support/v7/app/v;-><init>(Landroid/content/Context;Landroid/support/v7/app/bf;Landroid/view/Window;)V

    iput-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 94
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .registers 6

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v7/app/af;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/bf;-><init>(Landroid/content/Context;I)V

    .line 98
    invoke-virtual {p0, p2}, Landroid/support/v7/app/af;->setCancelable(Z)V

    .line 99
    invoke-virtual {p0, p3}, Landroid/support/v7/app/af;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 100
    new-instance v0, Landroid/support/v7/app/v;

    invoke-virtual {p0}, Landroid/support/v7/app/af;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-direct {v0, p1, p0, v1}, Landroid/support/v7/app/v;-><init>(Landroid/content/Context;Landroid/support/v7/app/bf;Landroid/view/Window;)V

    iput-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 101
    return-void
.end method

.method static a(Landroid/content/Context;I)I
    .registers 6

    .prologue
    .line 104
    const/high16 v0, 0x1000000

    if-lt p1, v0, :cond_5

    .line 109
    :goto_4
    return p1

    .line 107
    :cond_5
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 108
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Landroid/support/v7/a/d;->alertDialogTheme:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 109
    iget p1, v0, Landroid/util/TypedValue;->resourceId:I

    goto :goto_4
.end method

.method static synthetic a(Landroid/support/v7/app/af;)Landroid/support/v7/app/v;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    return-object v0
.end method

.method private a(I)Landroid/widget/Button;
    .registers 3

    .prologue
    .line 124
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 1384
    packed-switch p1, :pswitch_data_10

    .line 1392
    const/4 v0, 0x0

    .line 1390
    :goto_6
    return-object v0

    .line 1386
    :pswitch_7
    iget-object v0, v0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    goto :goto_6

    .line 1388
    :pswitch_a
    iget-object v0, v0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    goto :goto_6

    .line 1390
    :pswitch_d
    iget-object v0, v0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    goto :goto_6

    .line 1384
    :pswitch_data_10
    .packed-switch -0x3
        :pswitch_d
        :pswitch_a
        :pswitch_7
    .end packed-switch
.end method

.method private a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 6

    .prologue
    .line 209
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Landroid/support/v7/app/v;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 210
    return-void
.end method

.method private a(ILjava/lang/CharSequence;Landroid/os/Message;)V
    .registers 6

    .prologue
    .line 195
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, p3}, Landroid/support/v7/app/v;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 196
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 223
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/v;->a(Landroid/graphics/drawable/Drawable;)V

    .line 224
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 146
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 3241
    iput-object p1, v0, Landroid/support/v7/app/v;->C:Landroid/view/View;

    .line 147
    return-void
.end method

.method private a(Landroid/view/View;IIII)V
    .registers 12

    .prologue
    .line 172
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/app/v;->a(Landroid/view/View;IIII)V

    .line 173
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 150
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/v;->b(Ljava/lang/CharSequence;)V

    .line 151
    return-void
.end method

.method private b()Landroid/widget/ListView;
    .registers 2

    .prologue
    .line 133
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 2380
    iget-object v0, v0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    .line 133
    return-object v0
.end method

.method private b(I)V
    .registers 3

    .prologue
    .line 181
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 3287
    iput p1, v0, Landroid/support/v7/app/v;->L:I

    .line 182
    return-void
.end method

.method private b(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 157
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/v;->b(Landroid/view/View;)V

    .line 158
    return-void
.end method

.method private c(I)V
    .registers 3

    .prologue
    .line 219
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/v;->a(I)V

    .line 220
    return-void
.end method

.method private d(I)V
    .registers 5

    .prologue
    .line 232
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 233
    invoke-virtual {p0}, Landroid/support/v7/app/af;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 234
    iget-object v1, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/support/v7/app/v;->a(I)V

    .line 235
    return-void
.end method


# virtual methods
.method protected final onCreate(Landroid/os/Bundle;)V
    .registers 15

    .prologue
    const/high16 v12, 0x20000

    const/4 v11, -0x1

    const/4 v3, 0x1

    const/16 v10, 0x8

    const/4 v2, 0x0

    .line 239
    invoke-super {p0, p1}, Landroid/support/v7/app/bf;->onCreate(Landroid/os/Bundle;)V

    .line 240
    iget-object v5, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 4213
    iget-object v0, v5, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/bf;

    invoke-virtual {v0}, Landroid/support/v7/app/bf;->a()Z

    .line 4221
    iget v0, v5, Landroid/support/v7/app/v;->G:I

    if-eqz v0, :cond_193

    .line 4224
    iget v0, v5, Landroid/support/v7/app/v;->L:I

    if-ne v0, v3, :cond_193

    .line 4225
    iget v0, v5, Landroid/support/v7/app/v;->G:I

    .line 4216
    :goto_1b
    iget-object v1, v5, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/bf;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/bf;->setContentView(I)V

    .line 4407
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->contentPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 4522
    iget-object v1, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v4, Landroid/support/v7/a/i;->scrollView:I

    invoke-virtual {v1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, v5, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    .line 4523
    iget-object v1, v5, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 4526
    iget-object v1, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102000b

    invoke-virtual {v1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v5, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    .line 4527
    iget-object v1, v5, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    if-eqz v1, :cond_57

    .line 4531
    iget-object v1, v5, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_197

    .line 4532
    iget-object v0, v5, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    iget-object v1, v5, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4554
    :cond_57
    :goto_57
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v1, 0x1020019

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v5, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    .line 4555
    iget-object v0, v5, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    iget-object v1, v5, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4557
    iget-object v0, v5, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1c9

    .line 4558
    iget-object v0, v5, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setVisibility(I)V

    move v1, v2

    .line 4565
    :goto_79
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102001a

    invoke-virtual {v0, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v5, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    .line 4566
    iget-object v0, v5, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    iget-object v4, v5, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4568
    iget-object v0, v5, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1d8

    .line 4569
    iget-object v0, v5, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 4577
    :goto_9a
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102001b

    invoke-virtual {v0, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v5, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    .line 4578
    iget-object v0, v5, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    iget-object v4, v5, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4580
    iget-object v0, v5, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1e8

    .line 4581
    iget-object v0, v5, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 4589
    :goto_bb
    iget-object v0, v5, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    .line 5163
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 5164
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v6, Landroid/support/v7/a/d;->alertDialogCenterButtons:I

    invoke-virtual {v0, v6, v4, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 5165
    iget v0, v4, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_1f8

    move v0, v3

    .line 4589
    :goto_d0
    if-eqz v0, :cond_d9

    .line 4594
    if-ne v1, v3, :cond_1fb

    .line 4595
    iget-object v0, v5, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    .line 4603
    :cond_d9
    :goto_d9
    if-eqz v1, :cond_20f

    move v4, v3

    .line 4411
    :goto_dc
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->topPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 4412
    iget-object v1, v5, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    const/4 v6, 0x0

    sget-object v7, Landroid/support/v7/a/n;->AlertDialog:[I

    sget v8, Landroid/support/v7/a/d;->alertDialogStyle:I

    invoke-static {v1, v6, v7, v8}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v6

    .line 5474
    iget-object v1, v5, Landroid/support/v7/app/v;->C:Landroid/view/View;

    if-eqz v1, :cond_212

    .line 5476
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v1, v11, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 5479
    iget-object v7, v5, Landroid/support/v7/app/v;->C:Landroid/view/View;

    invoke-virtual {v0, v7, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 5482
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->title_template:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 5483
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 4416
    :goto_10b
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->buttonPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4417
    if-nez v4, :cond_125

    .line 4418
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 4419
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->textSpacerNoButtons:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4420
    if-eqz v0, :cond_125

    .line 4421
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4425
    :cond_125
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->customPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 4427
    iget-object v1, v5, Landroid/support/v7/app/v;->g:Landroid/view/View;

    if-eqz v1, :cond_292

    .line 4428
    iget-object v1, v5, Landroid/support/v7/app/v;->g:Landroid/view/View;

    move-object v4, v1

    .line 4436
    :goto_136
    if-eqz v4, :cond_139

    move v2, v3

    .line 4437
    :cond_139
    if-eqz v2, :cond_141

    invoke-static {v4}, Landroid/support/v7/app/v;->a(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_146

    .line 4438
    :cond_141
    iget-object v1, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    invoke-virtual {v1, v12, v12}, Landroid/view/Window;->setFlags(II)V

    .line 4442
    :cond_146
    if-eqz v2, :cond_2a9

    .line 4443
    iget-object v1, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v2, Landroid/support/v7/a/i;->custom:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 4444
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v11, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4446
    iget-boolean v2, v5, Landroid/support/v7/app/v;->m:Z

    if-eqz v2, :cond_169

    .line 4447
    iget v2, v5, Landroid/support/v7/app/v;->i:I

    iget v4, v5, Landroid/support/v7/app/v;->j:I

    iget v7, v5, Landroid/support/v7/app/v;->k:I

    iget v8, v5, Landroid/support/v7/app/v;->l:I

    invoke-virtual {v1, v2, v4, v7, v8}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 4451
    :cond_169
    iget-object v1, v5, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    if-eqz v1, :cond_176

    .line 4452
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 4458
    :cond_176
    :goto_176
    iget-object v0, v5, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    .line 4459
    if-eqz v0, :cond_18d

    iget-object v1, v5, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_18d

    .line 4460
    iget-object v1, v5, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 4461
    iget v1, v5, Landroid/support/v7/app/v;->E:I

    .line 4462
    if-ltz v1, :cond_18d

    .line 4463
    invoke-virtual {v0, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 4464
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 6183
    :cond_18d
    iget-object v0, v6, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 241
    return-void

    .line 4227
    :cond_193
    iget v0, v5, Landroid/support/v7/app/v;->F:I

    goto/16 :goto_1b

    .line 4534
    :cond_197
    iget-object v1, v5, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 4535
    iget-object v1, v5, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    iget-object v4, v5, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/ScrollView;->removeView(Landroid/view/View;)V

    .line 4537
    iget-object v1, v5, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    if-eqz v1, :cond_1c4

    .line 4538
    iget-object v0, v5, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 4539
    iget-object v1, v5, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 4540
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 4541
    iget-object v4, v5, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v6, v11, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4, v1, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_57

    .line 4544
    :cond_1c4
    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_57

    .line 4560
    :cond_1c9
    iget-object v0, v5, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    iget-object v1, v5, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4561
    iget-object v0, v5, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    move v1, v3

    .line 4562
    goto/16 :goto_79

    .line 4571
    :cond_1d8
    iget-object v0, v5, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    iget-object v4, v5, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4572
    iget-object v0, v5, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 4574
    or-int/lit8 v1, v1, 0x2

    goto/16 :goto_9a

    .line 4583
    :cond_1e8
    iget-object v0, v5, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    iget-object v4, v5, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4584
    iget-object v0, v5, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 4586
    or-int/lit8 v1, v1, 0x4

    goto/16 :goto_bb

    :cond_1f8
    move v0, v2

    .line 5165
    goto/16 :goto_d0

    .line 4596
    :cond_1fb
    const/4 v0, 0x2

    if-ne v1, v0, :cond_205

    .line 4597
    iget-object v0, v5, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    goto/16 :goto_d9

    .line 4598
    :cond_205
    const/4 v0, 0x4

    if-ne v1, v0, :cond_d9

    .line 4599
    iget-object v0, v5, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    goto/16 :goto_d9

    :cond_20f
    move v4, v2

    .line 4603
    goto/16 :goto_dc

    .line 5485
    :cond_212
    iget-object v1, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v7, 0x1020006

    invoke-virtual {v1, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    .line 5487
    iget-object v1, v5, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_24a

    move v1, v3

    .line 5488
    :goto_228
    if-eqz v1, :cond_27d

    .line 5490
    iget-object v0, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->alertTitle:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v5, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    .line 5491
    iget-object v0, v5, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    iget-object v1, v5, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5496
    iget v0, v5, Landroid/support/v7/app/v;->x:I

    if-eqz v0, :cond_24c

    .line 5497
    iget-object v0, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget v1, v5, Landroid/support/v7/app/v;->x:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_10b

    :cond_24a
    move v1, v2

    .line 5487
    goto :goto_228

    .line 5498
    :cond_24c
    iget-object v0, v5, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_259

    .line 5499
    iget-object v0, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget-object v1, v5, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_10b

    .line 5503
    :cond_259
    iget-object v0, v5, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    iget-object v1, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v1

    iget-object v7, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v7

    iget-object v8, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v8

    iget-object v9, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v9

    invoke-virtual {v0, v1, v7, v8, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 5507
    iget-object v0, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_10b

    .line 5511
    :cond_27d
    iget-object v1, v5, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v7, Landroid/support/v7/a/i;->title_template:I

    invoke-virtual {v1, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 5512
    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    .line 5513
    iget-object v1, v5, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5514
    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_10b

    .line 4429
    :cond_292
    iget v1, v5, Landroid/support/v7/app/v;->h:I

    if-eqz v1, :cond_2a5

    .line 4430
    iget-object v1, v5, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 4431
    iget v4, v5, Landroid/support/v7/app/v;->h:I

    invoke-virtual {v1, v4, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    .line 4432
    goto/16 :goto_136

    .line 4433
    :cond_2a5
    const/4 v1, 0x0

    move-object v4, v1

    goto/16 :goto_136

    .line 4455
    :cond_2a9
    invoke-virtual {v0, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_176
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 245
    iget-object v1, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 6398
    iget-object v2, v1, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    if-eqz v2, :cond_13

    iget-object v1, v1, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v1, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_13

    move v1, v0

    .line 245
    :goto_10
    if-eqz v1, :cond_15

    .line 248
    :goto_12
    return v0

    .line 6398
    :cond_13
    const/4 v1, 0x0

    goto :goto_10

    .line 248
    :cond_15
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/bf;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_12
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 253
    iget-object v1, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    .line 6403
    iget-object v2, v1, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    if-eqz v2, :cond_13

    iget-object v1, v1, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v1, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_13

    move v1, v0

    .line 253
    :goto_10
    if-eqz v1, :cond_15

    .line 256
    :goto_12
    return v0

    .line 6403
    :cond_13
    const/4 v1, 0x0

    goto :goto_10

    .line 256
    :cond_15
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/bf;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_12
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/support/v7/app/bf;->setTitle(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Landroid/support/v7/app/af;->c:Landroid/support/v7/app/v;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/v;->a(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method
