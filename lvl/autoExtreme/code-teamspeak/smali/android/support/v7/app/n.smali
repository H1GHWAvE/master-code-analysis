.class final Landroid/support/v7/app/n;
.super Landroid/support/v7/b/a/c;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/app/o;


# instance fields
.field private final f:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 471
    invoke-direct {p0, p2}, Landroid/support/v7/b/a/c;-><init>(Landroid/content/Context;)V

    .line 472
    iput-object p1, p0, Landroid/support/v7/app/n;->f:Landroid/app/Activity;

    .line 473
    return-void
.end method


# virtual methods
.method public final a()F
    .registers 2

    .prologue
    .line 485
    .line 2432
    iget v0, p0, Landroid/support/v7/b/a/c;->e:F

    .line 485
    return v0
.end method

.method public final a(F)V
    .registers 3

    .prologue
    .line 476
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_16

    .line 477
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/app/n;->a(Z)V

    .line 1443
    :cond_a
    :goto_a
    iget v0, p0, Landroid/support/v7/b/a/c;->e:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_15

    .line 1444
    iput p1, p0, Landroid/support/v7/b/a/c;->e:F

    .line 1445
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 482
    :cond_15
    return-void

    .line 478
    :cond_16
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_a

    .line 479
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/app/n;->a(Z)V

    goto :goto_a
.end method
