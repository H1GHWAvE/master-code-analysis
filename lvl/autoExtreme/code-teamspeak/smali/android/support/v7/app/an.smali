.class Landroid/support/v7/app/an;
.super Landroid/support/v7/internal/view/k;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/app/ak;


# direct methods
.method constructor <init>(Landroid/support/v7/app/ak;Landroid/view/Window$Callback;)V
    .registers 3

    .prologue
    .line 218
    iput-object p1, p0, Landroid/support/v7/app/an;->a:Landroid/support/v7/app/ak;

    .line 219
    invoke-direct {p0, p2}, Landroid/support/v7/internal/view/k;-><init>(Landroid/view/Window$Callback;)V

    .line 220
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3

    .prologue
    .line 224
    iget-object v0, p0, Landroid/support/v7/app/an;->a:Landroid/support/v7/app/ak;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ak;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-super {p0, p1}, Landroid/support/v7/internal/view/k;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 230
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/k;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/app/an;->a:Landroid/support/v7/app/ak;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/app/ak;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public onContentChanged()V
    .registers 1

    .prologue
    .line 248
    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .registers 4

    .prologue
    .line 236
    if-nez p1, :cond_8

    instance-of v0, p2, Landroid/support/v7/internal/view/menu/i;

    if-nez v0, :cond_8

    .line 239
    const/4 v0, 0x0

    .line 241
    :goto_7
    return v0

    :cond_8
    invoke-super {p0, p1, p2}, Landroid/support/v7/internal/view/k;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_7
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .registers 4

    .prologue
    .line 279
    invoke-super {p0, p1, p2}, Landroid/support/v7/internal/view/k;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/app/an;->a:Landroid/support/v7/app/ak;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ak;->e(I)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .registers 4

    .prologue
    .line 285
    invoke-super {p0, p1, p2}, Landroid/support/v7/internal/view/k;->onPanelClosed(ILandroid/view/Menu;)V

    .line 286
    iget-object v0, p0, Landroid/support/v7/app/an;->a:Landroid/support/v7/app/ak;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ak;->d(I)V

    .line 287
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 252
    instance-of v0, p3, Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_f

    move-object v0, p3

    check-cast v0, Landroid/support/v7/internal/view/menu/i;

    move-object v2, v0

    .line 254
    :goto_9
    if-nez p1, :cond_12

    if-nez v2, :cond_12

    move v0, v1

    .line 274
    :cond_e
    :goto_e
    return v0

    .line 252
    :cond_f
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_9

    .line 264
    :cond_12
    if-eqz v2, :cond_17

    .line 2367
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/v7/internal/view/menu/i;->o:Z

    .line 268
    :cond_17
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/internal/view/k;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    .line 270
    if-eqz v2, :cond_e

    .line 3367
    iput-boolean v1, v2, Landroid/support/v7/internal/view/menu/i;->o:Z

    goto :goto_e
.end method
