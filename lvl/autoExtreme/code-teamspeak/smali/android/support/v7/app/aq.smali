.class Landroid/support/v7/app/aq;
.super Landroid/support/v7/app/an;
.source "SourceFile"


# instance fields
.field final synthetic b:Landroid/support/v7/app/ap;


# direct methods
.method constructor <init>(Landroid/support/v7/app/ap;Landroid/view/Window$Callback;)V
    .registers 3

    .prologue
    .line 50
    iput-object p1, p0, Landroid/support/v7/app/aq;->b:Landroid/support/v7/app/ap;

    .line 51
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/an;-><init>(Landroid/support/v7/app/ak;Landroid/view/Window$Callback;)V

    .line 52
    return-void
.end method


# virtual methods
.method final a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 4

    .prologue
    .line 70
    new-instance v0, Landroid/support/v7/internal/view/e;

    iget-object v1, p0, Landroid/support/v7/app/aq;->b:Landroid/support/v7/app/ap;

    iget-object v1, v1, Landroid/support/v7/app/ap;->e:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Landroid/support/v7/internal/view/e;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    .line 74
    iget-object v1, p0, Landroid/support/v7/app/aq;->b:Landroid/support/v7/app/ap;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ap;->a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;

    move-result-object v1

    .line 77
    if-eqz v1, :cond_16

    .line 79
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/e;->b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;

    move-result-object v0

    .line 81
    :goto_15
    return-object v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 3

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/aq;->b:Landroid/support/v7/app/ap;

    .line 1046
    iget-boolean v0, v0, Landroid/support/v7/app/ap;->s:Z

    .line 57
    if-eqz v0, :cond_b

    .line 58
    invoke-virtual {p0, p1}, Landroid/support/v7/app/aq;->a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    .line 61
    :goto_a
    return-object v0

    :cond_b
    invoke-super {p0, p1}, Landroid/support/v7/app/an;->onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    goto :goto_a
.end method
