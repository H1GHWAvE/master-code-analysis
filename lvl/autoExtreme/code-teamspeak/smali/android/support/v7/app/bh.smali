.class public final Landroid/support/v7/app/bh;
.super Landroid/support/v4/app/dd;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/dd;-><init>()V

    .line 168
    return-void
.end method

.method static synthetic a(Landroid/app/Notification;Landroid/support/v4/app/dm;)V
    .registers 20

    .prologue
    .line 5060
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    instance-of v2, v2, Landroid/support/v7/app/bn;

    if-eqz v2, :cond_a7

    .line 5061
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    move-object v12, v2

    check-cast v12, Landroid/support/v7/app/bn;

    .line 5062
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget v6, v0, Landroid/support/v4/app/dm;->i:I

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-boolean v9, v0, Landroid/support/v4/app/dm;->l:Z

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget-wide v10, v10, Landroid/app/Notification;->when:J

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    iget-boolean v15, v12, Landroid/support/v7/app/bn;->c:Z

    iget-object v0, v12, Landroid/support/v7/app/bn;->h:Landroid/app/PendingIntent;

    move-object/from16 v16, v0

    .line 5119
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v12

    const/4 v13, 0x5

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 5158
    const/4 v12, 0x3

    move/from16 v0, v17

    if-gt v0, v12, :cond_74

    .line 5159
    sget v12, Landroid/support/v7/a/k;->notification_template_big_media_narrow:I

    .line 5120
    :goto_4f
    const/4 v13, 0x0

    invoke-static/range {v2 .. v13}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;

    move-result-object v5

    .line 5124
    sget v3, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v5, v3}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 5125
    if-lez v17, :cond_77

    .line 5126
    const/4 v3, 0x0

    move v4, v3

    :goto_5d
    move/from16 v0, v17

    if-ge v4, v0, :cond_77

    .line 5127
    invoke-interface {v14, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/ek;

    invoke-static {v2, v3}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;

    move-result-object v3

    .line 5128
    sget v6, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v5, v6, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 5126
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5d

    .line 5161
    :cond_74
    sget v12, Landroid/support/v7/a/k;->notification_template_big_media:I

    goto :goto_4f

    .line 5131
    :cond_77
    if-eqz v15, :cond_a8

    .line 5132
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    const/4 v4, 0x0

    invoke-virtual {v5, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 5133
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    const-string v4, "setAlpha"

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Landroid/support/v7/a/j;->cancel_button_image_alpha:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v5, v3, v4, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 5135
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    move-object/from16 v0, v16

    invoke-virtual {v5, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 5106
    :goto_97
    move-object/from16 v0, p0

    iput-object v5, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 5109
    if-eqz v15, :cond_a7

    .line 5110
    move-object/from16 v0, p0

    iget v2, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Landroid/app/Notification;->flags:I

    .line 33
    :cond_a7
    return-void

    .line 5137
    :cond_a8
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/16 v3, 0x8

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_97
.end method

.method static synthetic a(Landroid/support/v4/app/dc;Landroid/support/v4/app/dm;)V
    .registers 20

    .prologue
    .line 3047
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    instance-of v2, v2, Landroid/support/v7/app/bn;

    if-eqz v2, :cond_d3

    .line 3048
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    move-object v12, v2

    check-cast v12, Landroid/support/v7/app/bn;

    .line 3049
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget v6, v0, Landroid/support/v4/app/dm;->i:I

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-boolean v9, v0, Landroid/support/v4/app/dm;->l:Z

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget-wide v10, v10, Landroid/app/Notification;->when:J

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    iget-object v15, v12, Landroid/support/v7/app/bn;->a:[I

    iget-boolean v0, v12, Landroid/support/v7/app/bn;->c:Z

    move/from16 v16, v0

    iget-object v0, v12, Landroid/support/v7/app/bn;->h:Landroid/app/PendingIntent;

    move-object/from16 v17, v0

    .line 4066
    sget v12, Landroid/support/v7/a/k;->notification_template_media:I

    const/4 v13, 0x1

    invoke-static/range {v2 .. v13}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;

    move-result-object v6

    .line 4070
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v7

    .line 4071
    if-nez v15, :cond_7e

    const/4 v3, 0x0

    move v4, v3

    .line 4074
    :goto_52
    sget v3, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v6, v3}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 4075
    if-lez v4, :cond_9b

    .line 4076
    const/4 v3, 0x0

    move v5, v3

    :goto_5b
    if-ge v5, v4, :cond_9b

    .line 4077
    if-lt v5, v7, :cond_86

    .line 4078
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "setShowActionsInCompactView: action %d out of bounds (max %d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    add-int/lit8 v6, v7, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4071
    :cond_7e
    array-length v3, v15

    const/4 v4, 0x3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v4, v3

    goto :goto_52

    .line 4083
    :cond_86
    aget v3, v15, v5

    invoke-interface {v14, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/ek;

    .line 4084
    invoke-static {v2, v3}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;

    move-result-object v3

    .line 4085
    sget v8, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v6, v8, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 4076
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_5b

    .line 4088
    :cond_9b
    if-eqz v16, :cond_d4

    .line 4089
    sget v3, Landroid/support/v7/a/i;->end_padder:I

    const/16 v4, 0x8

    invoke-virtual {v6, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 4090
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    const/4 v4, 0x0

    invoke-virtual {v6, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 4091
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    move-object/from16 v0, v17

    invoke-virtual {v6, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 4092
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    const-string v4, "setAlpha"

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Landroid/support/v7/a/j;->cancel_button_image_alpha:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v6, v3, v4, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 4055
    :goto_c2
    invoke-interface/range {p0 .. p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    .line 4056
    if-eqz v16, :cond_d3

    .line 4057
    invoke-interface/range {p0 .. p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 33
    :cond_d3
    return-void

    .line 4095
    :cond_d4
    sget v2, Landroid/support/v7/a/i;->end_padder:I

    const/4 v3, 0x0

    invoke-virtual {v6, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 4096
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/16 v3, 0x8

    invoke-virtual {v6, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_c2
.end method

.method private static b(Landroid/app/Notification;Landroid/support/v4/app/dm;)V
    .registers 20

    .prologue
    .line 60
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    instance-of v2, v2, Landroid/support/v7/app/bn;

    if-eqz v2, :cond_a7

    .line 61
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    move-object v12, v2

    check-cast v12, Landroid/support/v7/app/bn;

    .line 62
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget v6, v0, Landroid/support/v4/app/dm;->i:I

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-boolean v9, v0, Landroid/support/v4/app/dm;->l:Z

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget-wide v10, v10, Landroid/app/Notification;->when:J

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    iget-boolean v15, v12, Landroid/support/v7/app/bn;->c:Z

    iget-object v0, v12, Landroid/support/v7/app/bn;->h:Landroid/app/PendingIntent;

    move-object/from16 v16, v0

    .line 2119
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v12

    const/4 v13, 0x5

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 2158
    const/4 v12, 0x3

    move/from16 v0, v17

    if-gt v0, v12, :cond_74

    .line 2159
    sget v12, Landroid/support/v7/a/k;->notification_template_big_media_narrow:I

    .line 2120
    :goto_4f
    const/4 v13, 0x0

    invoke-static/range {v2 .. v13}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;

    move-result-object v5

    .line 2124
    sget v3, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v5, v3}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 2125
    if-lez v17, :cond_77

    .line 2126
    const/4 v3, 0x0

    move v4, v3

    :goto_5d
    move/from16 v0, v17

    if-ge v4, v0, :cond_77

    .line 2127
    invoke-interface {v14, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/ek;

    invoke-static {v2, v3}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;

    move-result-object v3

    .line 2128
    sget v6, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v5, v6, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 2126
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5d

    .line 2161
    :cond_74
    sget v12, Landroid/support/v7/a/k;->notification_template_big_media:I

    goto :goto_4f

    .line 2131
    :cond_77
    if-eqz v15, :cond_a8

    .line 2132
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    const/4 v4, 0x0

    invoke-virtual {v5, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 2133
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    const-string v4, "setAlpha"

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Landroid/support/v7/a/j;->cancel_button_image_alpha:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v5, v3, v4, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 2135
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    move-object/from16 v0, v16

    invoke-virtual {v5, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 2106
    :goto_97
    move-object/from16 v0, p0

    iput-object v5, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 2109
    if-eqz v15, :cond_a7

    .line 2110
    move-object/from16 v0, p0

    iget v2, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Landroid/app/Notification;->flags:I

    .line 68
    :cond_a7
    return-void

    .line 2137
    :cond_a8
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/16 v3, 0x8

    invoke-virtual {v5, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_97
.end method

.method private static b(Landroid/support/v4/app/dc;Landroid/support/v4/app/dm;)V
    .registers 20

    .prologue
    .line 47
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    instance-of v2, v2, Landroid/support/v7/app/bn;

    if-eqz v2, :cond_d3

    .line 48
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    move-object v12, v2

    check-cast v12, Landroid/support/v7/app/bn;

    .line 49
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget v6, v0, Landroid/support/v4/app/dm;->i:I

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-boolean v9, v0, Landroid/support/v4/app/dm;->l:Z

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    iget-wide v10, v10, Landroid/app/Notification;->when:J

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    iget-object v15, v12, Landroid/support/v7/app/bn;->a:[I

    iget-boolean v0, v12, Landroid/support/v7/app/bn;->c:Z

    move/from16 v16, v0

    iget-object v0, v12, Landroid/support/v7/app/bn;->h:Landroid/app/PendingIntent;

    move-object/from16 v17, v0

    .line 2066
    sget v12, Landroid/support/v7/a/k;->notification_template_media:I

    const/4 v13, 0x1

    invoke-static/range {v2 .. v13}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/graphics/Bitmap;Ljava/lang/CharSequence;ZJIZ)Landroid/widget/RemoteViews;

    move-result-object v6

    .line 2070
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v7

    .line 2071
    if-nez v15, :cond_7e

    const/4 v3, 0x0

    move v4, v3

    .line 2074
    :goto_52
    sget v3, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v6, v3}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 2075
    if-lez v4, :cond_9b

    .line 2076
    const/4 v3, 0x0

    move v5, v3

    :goto_5b
    if-ge v5, v4, :cond_9b

    .line 2077
    if-lt v5, v7, :cond_86

    .line 2078
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "setShowActionsInCompactView: action %d out of bounds (max %d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    add-int/lit8 v6, v7, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2071
    :cond_7e
    array-length v3, v15

    const/4 v4, 0x3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v4, v3

    goto :goto_52

    .line 2083
    :cond_86
    aget v3, v15, v5

    invoke-interface {v14, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/ek;

    .line 2084
    invoke-static {v2, v3}, Landroid/support/v7/internal/a/d;->a(Landroid/content/Context;Landroid/support/v4/app/ek;)Landroid/widget/RemoteViews;

    move-result-object v3

    .line 2085
    sget v8, Landroid/support/v7/a/i;->media_actions:I

    invoke-virtual {v6, v8, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 2076
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_5b

    .line 2088
    :cond_9b
    if-eqz v16, :cond_d4

    .line 2089
    sget v3, Landroid/support/v7/a/i;->end_padder:I

    const/16 v4, 0x8

    invoke-virtual {v6, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 2090
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    const/4 v4, 0x0

    invoke-virtual {v6, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 2091
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    move-object/from16 v0, v17

    invoke-virtual {v6, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 2092
    sget v3, Landroid/support/v7/a/i;->cancel_action:I

    const-string v4, "setAlpha"

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Landroid/support/v7/a/j;->cancel_button_image_alpha:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v6, v3, v4, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 2055
    :goto_c2
    invoke-interface/range {p0 .. p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    .line 2056
    if-eqz v16, :cond_d3

    .line 2057
    invoke-interface/range {p0 .. p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 56
    :cond_d3
    return-void

    .line 2095
    :cond_d4
    sget v2, Landroid/support/v7/a/i;->end_padder:I

    const/4 v3, 0x0

    invoke-virtual {v6, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 2096
    sget v2, Landroid/support/v7/a/i;->cancel_action:I

    const/16 v3, 0x8

    invoke-virtual {v6, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_c2
.end method

.method static synthetic b(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
    .registers 6

    .prologue
    .line 6037
    instance-of v0, p1, Landroid/support/v7/app/bn;

    if-eqz v0, :cond_25

    .line 6038
    check-cast p1, Landroid/support/v7/app/bn;

    .line 6039
    iget-object v1, p1, Landroid/support/v7/app/bn;->a:[I

    iget-object v0, p1, Landroid/support/v7/app/bn;->b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    if-eqz v0, :cond_26

    iget-object v0, p1, Landroid/support/v7/app/bn;->b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 6745
    iget-object v0, v0, Landroid/support/v4/media/session/MediaSessionCompat$Token;->a:Ljava/lang/Object;

    .line 7031
    :goto_10
    new-instance v2, Landroid/app/Notification$MediaStyle;

    invoke-interface {p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/Notification$MediaStyle;-><init>(Landroid/app/Notification$Builder;)V

    .line 7032
    if-eqz v1, :cond_1e

    .line 7033
    invoke-virtual {v2, v1}, Landroid/app/Notification$MediaStyle;->setShowActionsInCompactView([I)Landroid/app/Notification$MediaStyle;

    .line 7035
    :cond_1e
    if-eqz v0, :cond_25

    .line 7036
    check-cast v0, Landroid/media/session/MediaSession$Token;

    invoke-virtual {v2, v0}, Landroid/app/Notification$MediaStyle;->setMediaSession(Landroid/media/session/MediaSession$Token;)Landroid/app/Notification$MediaStyle;

    .line 33
    :cond_25
    return-void

    .line 6039
    :cond_26
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private static c(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
    .registers 6

    .prologue
    .line 37
    instance-of v0, p1, Landroid/support/v7/app/bn;

    if-eqz v0, :cond_25

    .line 38
    check-cast p1, Landroid/support/v7/app/bn;

    .line 39
    iget-object v1, p1, Landroid/support/v7/app/bn;->a:[I

    iget-object v0, p1, Landroid/support/v7/app/bn;->b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    if-eqz v0, :cond_26

    iget-object v0, p1, Landroid/support/v7/app/bn;->b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 1745
    iget-object v0, v0, Landroid/support/v4/media/session/MediaSessionCompat$Token;->a:Ljava/lang/Object;

    .line 2031
    :goto_10
    new-instance v2, Landroid/app/Notification$MediaStyle;

    invoke-interface {p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/Notification$MediaStyle;-><init>(Landroid/app/Notification$Builder;)V

    .line 2032
    if-eqz v1, :cond_1e

    .line 2033
    invoke-virtual {v2, v1}, Landroid/app/Notification$MediaStyle;->setShowActionsInCompactView([I)Landroid/app/Notification$MediaStyle;

    .line 2035
    :cond_1e
    if-eqz v0, :cond_25

    .line 2036
    check-cast v0, Landroid/media/session/MediaSession$Token;

    invoke-virtual {v2, v0}, Landroid/app/Notification$MediaStyle;->setMediaSession(Landroid/media/session/MediaSession$Token;)Landroid/app/Notification$MediaStyle;

    .line 43
    :cond_25
    return-void

    .line 39
    :cond_26
    const/4 v0, 0x0

    goto :goto_10
.end method
