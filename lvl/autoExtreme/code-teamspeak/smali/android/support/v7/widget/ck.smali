.class public final Landroid/support/v7/widget/ck;
.super Landroid/support/v7/app/c;
.source "SourceFile"


# static fields
.field static final b:I = 0x0

.field static final c:I = 0x1

.field static final d:I = 0x2


# instance fields
.field e:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 1873
    const/4 v0, -0x2

    invoke-direct {p0, v0}, Landroid/support/v7/app/c;-><init>(I)V

    .line 1866
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ck;->e:I

    .line 1874
    const v0, 0x800013

    iput v0, p0, Landroid/support/v7/widget/ck;->a:I

    .line 1875
    return-void
.end method

.method private constructor <init>(I)V
    .registers 3

    .prologue
    .line 1883
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1}, Landroid/support/v7/widget/ck;-><init>(II)V

    .line 1884
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4

    .prologue
    .line 1878
    invoke-direct {p0, p1}, Landroid/support/v7/app/c;-><init>(I)V

    .line 1866
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ck;->e:I

    .line 1879
    iput p2, p0, Landroid/support/v7/widget/ck;->a:I

    .line 1880
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 1869
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/c;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1866
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ck;->e:I

    .line 1870
    return-void
.end method

.method public constructor <init>(Landroid/support/v7/app/c;)V
    .registers 3

    .prologue
    .line 1893
    invoke-direct {p0, p1}, Landroid/support/v7/app/c;-><init>(Landroid/support/v7/app/c;)V

    .line 1866
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ck;->e:I

    .line 1894
    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/ck;)V
    .registers 3

    .prologue
    .line 1887
    invoke-direct {p0, p1}, Landroid/support/v7/app/c;-><init>(Landroid/support/v7/app/c;)V

    .line 1866
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ck;->e:I

    .line 1889
    iget v0, p1, Landroid/support/v7/widget/ck;->e:I

    iput v0, p0, Landroid/support/v7/widget/ck;->e:I

    .line 1890
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3

    .prologue
    .line 1904
    invoke-direct {p0, p1}, Landroid/support/v7/app/c;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1866
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ck;->e:I

    .line 1905
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3

    .prologue
    .line 1897
    invoke-direct {p0, p1}, Landroid/support/v7/app/c;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1866
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ck;->e:I

    .line 2908
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v0, p0, Landroid/support/v7/widget/ck;->leftMargin:I

    .line 2909
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v0, p0, Landroid/support/v7/widget/ck;->topMargin:I

    .line 2910
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v0, p0, Landroid/support/v7/widget/ck;->rightMargin:I

    .line 2911
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v0, p0, Landroid/support/v7/widget/ck;->bottomMargin:I

    .line 1901
    return-void
.end method

.method private a(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3

    .prologue
    .line 1908
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v0, p0, Landroid/support/v7/widget/ck;->leftMargin:I

    .line 1909
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v0, p0, Landroid/support/v7/widget/ck;->topMargin:I

    .line 1910
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v0, p0, Landroid/support/v7/widget/ck;->rightMargin:I

    .line 1911
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v0, p0, Landroid/support/v7/widget/ck;->bottomMargin:I

    .line 1912
    return-void
.end method
