.class final Landroid/support/v7/widget/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/y;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/ActionMenuPresenter;


# direct methods
.method private constructor <init>(Landroid/support/v7/widget/ActionMenuPresenter;)V
    .registers 2

    .prologue
    .line 748
    iput-object p1, p0, Landroid/support/v7/widget/h;->a:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/widget/ActionMenuPresenter;B)V
    .registers 3

    .prologue
    .line 748
    invoke-direct {p0, p1}, Landroid/support/v7/widget/h;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
    .registers 5

    .prologue
    .line 761
    instance-of v0, p1, Landroid/support/v7/internal/view/menu/ad;

    if-eqz v0, :cond_d

    move-object v0, p1

    .line 762
    check-cast v0, Landroid/support/v7/internal/view/menu/ad;

    .line 2079
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/ad;->p:Landroid/support/v7/internal/view/menu/i;

    .line 762
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 764
    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/h;->a:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 2152
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/d;->f:Landroid/support/v7/internal/view/menu/y;

    .line 765
    if-eqz v0, :cond_16

    .line 766
    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/y;->a(Landroid/support/v7/internal/view/menu/i;Z)V

    .line 768
    :cond_16
    return-void
.end method

.method public final a_(Landroid/support/v7/internal/view/menu/i;)Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 752
    if-nez p1, :cond_5

    move v0, v1

    .line 756
    :goto_4
    return v0

    .line 754
    :cond_5
    iget-object v2, p0, Landroid/support/v7/widget/h;->a:Landroid/support/v7/widget/ActionMenuPresenter;

    move-object v0, p1

    check-cast v0, Landroid/support/v7/internal/view/menu/ad;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ad;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iput v0, v2, Landroid/support/v7/widget/ActionMenuPresenter;->t:I

    .line 755
    iget-object v0, p0, Landroid/support/v7/widget/h;->a:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 1152
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/d;->f:Landroid/support/v7/internal/view/menu/y;

    .line 756
    if-eqz v0, :cond_1f

    invoke-interface {v0, p1}, Landroid/support/v7/internal/view/menu/y;->a_(Landroid/support/v7/internal/view/menu/i;)Z

    move-result v0

    goto :goto_4

    :cond_1f
    move v0, v1

    goto :goto_4
.end method
