.class public final Landroid/support/v7/widget/ActionMenuPresenter;
.super Landroid/support/v7/internal/view/menu/d;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/o;


# static fields
.field private static final u:Ljava/lang/String; = "ActionMenuPresenter"


# instance fields
.field private A:I

.field private final B:Landroid/util/SparseBooleanArray;

.field private C:Landroid/view/View;

.field private D:Landroid/support/v7/widget/c;

.field i:Landroid/support/v7/widget/e;

.field j:Landroid/graphics/drawable/Drawable;

.field k:Z

.field public l:Z

.field public m:I

.field public n:Z

.field public o:Z

.field p:Landroid/support/v7/widget/g;

.field q:Landroid/support/v7/widget/b;

.field r:Landroid/support/v7/widget/d;

.field final s:Landroid/support/v7/widget/h;

.field t:I

.field private v:Z

.field private w:I

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    .line 88
    sget v0, Landroid/support/v7/a/k;->abc_action_menu_layout:I

    sget v1, Landroid/support/v7/a/k;->abc_action_menu_item_layout:I

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/internal/view/menu/d;-><init>(Landroid/content/Context;II)V

    .line 74
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->B:Landroid/util/SparseBooleanArray;

    .line 84
    new-instance v0, Landroid/support/v7/widget/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/h;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;B)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->s:Landroid/support/v7/widget/h;

    .line 89
    return-void
.end method

.method private static synthetic a(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/g;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->p:Landroid/support/v7/widget/g;

    return-object v0
.end method

.method private static synthetic a(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/support/v7/widget/g;)Landroid/support/v7/widget/g;
    .registers 2

    .prologue
    .line 53
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->p:Landroid/support/v7/widget/g;

    return-object p1
.end method

.method private a(Landroid/view/MenuItem;)Landroid/view/View;
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 307
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/view/ViewGroup;

    .line 308
    if-nez v0, :cond_9

    move-object v2, v3

    .line 318
    :cond_8
    :goto_8
    return-object v2

    .line 310
    :cond_9
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    .line 311
    const/4 v1, 0x0

    move v4, v1

    :goto_f
    if-ge v4, v5, :cond_26

    .line 312
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 313
    instance-of v1, v2, Landroid/support/v7/internal/view/menu/aa;

    if-eqz v1, :cond_22

    move-object v1, v2

    check-cast v1, Landroid/support/v7/internal/view/menu/aa;

    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/aa;->getItemData()Landroid/support/v7/internal/view/menu/m;

    move-result-object v1

    if-eq v1, p1, :cond_8

    .line 311
    :cond_22
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_f

    :cond_26
    move-object v2, v3

    .line 318
    goto :goto_8
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 158
    iput p1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->m:I

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Z

    .line 160
    return-void
.end method

.method private a(IZ)V
    .registers 4

    .prologue
    .line 147
    iput p1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->w:I

    .line 148
    iput-boolean p2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->y:Z

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->z:Z

    .line 150
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 167
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-eqz v0, :cond_a

    .line 168
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/e;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 173
    :goto_9
    return-void

    .line 170
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->k:Z

    .line 171
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_9
.end method

.method private static synthetic b(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/d;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    return-object v0
.end method

.method private static synthetic c(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/internal/view/menu/i;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method private c(Z)V
    .registers 2

    .prologue
    .line 163
    iput-boolean p1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->o:Z

    .line 164
    return-void
.end method

.method private static synthetic d(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/e;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    return-object v0
.end method

.method private static synthetic e(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/internal/view/menu/z;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    return-object v0
.end method

.method private static synthetic f(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/b;
    .registers 2

    .prologue
    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->q:Landroid/support/v7/widget/b;

    return-object v0
.end method

.method private static synthetic g(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/internal/view/menu/i;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method private static synthetic h(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/internal/view/menu/z;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    return-object v0
.end method

.method private static synthetic i(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/d;
    .registers 2

    .prologue
    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    return-object v0
.end method

.method private static synthetic j(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/b;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->q:Landroid/support/v7/widget/b;

    return-object v0
.end method

.method private k()V
    .registers 3

    .prologue
    .line 137
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Z

    if-nez v0, :cond_12

    .line 138
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v7/a/j;->abc_max_action_buttons:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->m:I

    .line 141
    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_1c

    .line 142
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 144
    :cond_1c
    return-void
.end method

.method private l()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-eqz v0, :cond_b

    .line 177
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 181
    :goto_a
    return-object v0

    .line 178
    :cond_b
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->k:Z

    if-eqz v0, :cond_12

    .line 179
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_a

    .line 181
    :cond_12
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private m()Z
    .registers 2

    .prologue
    .line 400
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
    .registers 4

    .prologue
    .line 186
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;

    move-result-object v1

    move-object v0, v1

    .line 187
    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionMenuView;->setPresenter(Landroid/support/v7/widget/ActionMenuPresenter;)V

    .line 188
    return-object v1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    .line 193
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_c

    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->i()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 195
    :cond_c
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 197
    :cond_10
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_2f

    const/16 v1, 0x8

    :goto_18
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 199
    check-cast p3, Landroid/support/v7/widget/ActionMenuView;

    .line 200
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 201
    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_2e

    .line 202
    invoke-static {v1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 204
    :cond_2e
    return-object v0

    .line 197
    :cond_2f
    const/4 v1, 0x0

    goto :goto_18
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
    .registers 10

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 93
    invoke-super {p0, p1, p2}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 97
    invoke-static {p1}, Landroid/support/v7/internal/view/a;->a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;

    move-result-object v3

    .line 98
    iget-boolean v4, p0, Landroid/support/v7/widget/ActionMenuPresenter;->v:Z

    if-nez v4, :cond_1a

    .line 2050
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_83

    .line 99
    :cond_18
    :goto_18
    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    .line 102
    :cond_1a
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->z:Z

    if-nez v0, :cond_2e

    .line 2058
    iget-object v0, v3, Landroid/support/v7/internal/view/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    .line 103
    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->w:I

    .line 107
    :cond_2e
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Z

    if-nez v0, :cond_40

    .line 3046
    iget-object v0, v3, Landroid/support/v7/internal/view/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Landroid/support/v7/a/j;->abc_max_action_buttons:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 108
    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->m:I

    .line 111
    :cond_40
    iget v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->w:I

    .line 112
    iget-boolean v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    if-eqz v3, :cond_91

    .line 113
    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-nez v3, :cond_6b

    .line 114
    new-instance v3, Landroid/support/v7/widget/e;

    iget-object v4, p0, Landroid/support/v7/widget/ActionMenuPresenter;->a:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Landroid/support/v7/widget/e;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;)V

    iput-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    .line 115
    iget-boolean v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->k:Z

    if-eqz v3, :cond_62

    .line 116
    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    iget-object v4, p0, Landroid/support/v7/widget/ActionMenuPresenter;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/e;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 117
    iput-object v6, p0, Landroid/support/v7/widget/ActionMenuPresenter;->j:Landroid/graphics/drawable/Drawable;

    .line 118
    iput-boolean v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->k:Z

    .line 120
    :cond_62
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 121
    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v3, v1, v1}, Landroid/support/v7/widget/e;->measure(II)V

    .line 123
    :cond_6b
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v1}, Landroid/support/v7/widget/e;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 128
    :goto_72
    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->x:I

    .line 130
    const/high16 v0, 0x42600000    # 56.0f

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->A:I

    .line 133
    iput-object v6, p0, Landroid/support/v7/widget/ActionMenuPresenter;->C:Landroid/view/View;

    .line 134
    return-void

    .line 2053
    :cond_83
    iget-object v4, v3, Landroid/support/v7/internal/view/a;->a:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-static {v4}, Landroid/support/v4/view/du;->b(Landroid/view/ViewConfiguration;)Z

    move-result v4

    if-eqz v4, :cond_18

    move v0, v1

    goto :goto_18

    .line 125
    :cond_91
    iput-object v6, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    goto :goto_72
.end method

.method public final a(Landroid/os/Parcelable;)V
    .registers 4

    .prologue
    .line 552
    check-cast p1, Landroid/support/v7/widget/ActionMenuPresenter$SavedState;

    .line 553
    iget v0, p1, Landroid/support/v7/widget/ActionMenuPresenter$SavedState;->a:I

    if-lez v0, :cond_19

    .line 554
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    iget v1, p1, Landroid/support/v7/widget/ActionMenuPresenter$SavedState;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 555
    if-eqz v0, :cond_19

    .line 556
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ad;

    .line 557
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/support/v7/internal/view/menu/ad;)Z

    .line 560
    :cond_19
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
    .registers 3

    .prologue
    .line 539
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuPresenter;->g()Z

    .line 540
    invoke-super {p0, p1, p2}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/i;Z)V

    .line 541
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;Landroid/support/v7/internal/view/menu/aa;)V
    .registers 5

    .prologue
    .line 209
    invoke-interface {p2, p1}, Landroid/support/v7/internal/view/menu/aa;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 211
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    .line 212
    check-cast p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 213
    invoke-virtual {p2, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setItemInvoker(Landroid/support/v7/internal/view/menu/k;)V

    .line 215
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->D:Landroid/support/v7/widget/c;

    if-nez v0, :cond_18

    .line 216
    new-instance v0, Landroid/support/v7/widget/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/c;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;B)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->D:Landroid/support/v7/widget/c;

    .line 218
    :cond_18
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->D:Landroid/support/v7/widget/c;

    invoke-virtual {p2, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->setPopupCallback(Landroid/support/v7/internal/view/menu/c;)V

    .line 219
    return-void
.end method

.method public final a(Landroid/support/v7/widget/ActionMenuView;)V
    .registers 3

    .prologue
    .line 573
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    .line 574
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    .line 7629
    iput-object v0, p1, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    .line 575
    return-void
.end method

.method public final a(Z)V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 228
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 232
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/menu/d;->a(Z)V

    .line 234
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 236
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_35

    .line 237
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    .line 3165
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->i()V

    .line 3166
    iget-object v4, v0, Landroid/support/v7/internal/view/menu/i;->h:Ljava/util/ArrayList;

    .line 238
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 239
    :goto_23
    if-ge v3, v5, :cond_35

    .line 240
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 3651
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    .line 241
    if-eqz v0, :cond_31

    .line 4226
    iput-object p0, v0, Landroid/support/v4/view/n;->a:Landroid/support/v4/view/o;

    .line 239
    :cond_31
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_23

    .line 247
    :cond_35
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_92

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 251
    :goto_3f
    iget-boolean v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    if-eqz v3, :cond_59

    if-eqz v0, :cond_59

    .line 252
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 253
    if-ne v3, v1, :cond_96

    .line 254
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_94

    move v0, v1

    :goto_58
    move v2, v0

    .line 260
    :cond_59
    :goto_59
    if-eqz v2, :cond_9c

    .line 261
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-nez v0, :cond_68

    .line 262
    new-instance v0, Landroid/support/v7/widget/e;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/e;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    .line 264
    :cond_68
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 265
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    if-eq v0, v1, :cond_88

    .line 266
    if-eqz v0, :cond_7b

    .line 267
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 269
    :cond_7b
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    .line 270
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->a()Landroid/support/v7/widget/m;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 276
    :cond_88
    :goto_88
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iget-boolean v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->setOverflowReserved(Z)V

    .line 277
    return-void

    .line 247
    :cond_92
    const/4 v0, 0x0

    goto :goto_3f

    :cond_94
    move v0, v2

    .line 254
    goto :goto_58

    .line 256
    :cond_96
    if-lez v3, :cond_9a

    :goto_98
    move v2, v1

    goto :goto_59

    :cond_9a
    move v1, v2

    goto :goto_98

    .line 272
    :cond_9c
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-eqz v0, :cond_88

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    if-ne v0, v1, :cond_88

    .line 273
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_88
.end method

.method public final a()Z
    .registers 20

    .prologue
    .line 404
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/i;->h()Ljava/util/ArrayList;

    move-result-object v13

    .line 405
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 406
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/ActionMenuPresenter;->m:I

    .line 407
    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/v7/widget/ActionMenuPresenter;->x:I

    .line 408
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 409
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v2, Landroid/view/ViewGroup;

    .line 411
    const/4 v6, 0x0

    .line 412
    const/4 v5, 0x0

    .line 413
    const/4 v8, 0x0

    .line 414
    const/4 v4, 0x0

    .line 415
    const/4 v3, 0x0

    move v10, v3

    :goto_26
    if-ge v10, v14, :cond_53

    .line 416
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/internal/view/menu/m;

    .line 417
    invoke-virtual {v3}, Landroid/support/v7/internal/view/menu/m;->h()Z

    move-result v11

    if-eqz v11, :cond_48

    .line 418
    add-int/lit8 v6, v6, 0x1

    .line 424
    :goto_36
    move-object/from16 v0, p0

    iget-boolean v11, v0, Landroid/support/v7/widget/ActionMenuPresenter;->o:Z

    if-eqz v11, :cond_1b1

    invoke-virtual {v3}, Landroid/support/v7/internal/view/menu/m;->isActionViewExpanded()Z

    move-result v3

    if-eqz v3, :cond_1b1

    .line 427
    const/4 v3, 0x0

    .line 415
    :goto_43
    add-int/lit8 v7, v10, 0x1

    move v10, v7

    move v7, v3

    goto :goto_26

    .line 419
    :cond_48
    invoke-virtual {v3}, Landroid/support/v7/internal/view/menu/m;->g()Z

    move-result v11

    if-eqz v11, :cond_51

    .line 420
    add-int/lit8 v5, v5, 0x1

    goto :goto_36

    .line 422
    :cond_51
    const/4 v4, 0x1

    goto :goto_36

    .line 432
    :cond_53
    move-object/from16 v0, p0

    iget-boolean v3, v0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    if-eqz v3, :cond_61

    if-nez v4, :cond_5f

    add-int v3, v6, v5

    if-le v3, v7, :cond_61

    .line 434
    :cond_5f
    add-int/lit8 v7, v7, -0x1

    .line 436
    :cond_61
    sub-int v10, v7, v6

    .line 438
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuPresenter;->B:Landroid/util/SparseBooleanArray;

    move-object/from16 v16, v0

    .line 439
    invoke-virtual/range {v16 .. v16}, Landroid/util/SparseBooleanArray;->clear()V

    .line 441
    const/4 v3, 0x0

    .line 442
    const/4 v4, 0x0

    .line 443
    move-object/from16 v0, p0

    iget-boolean v5, v0, Landroid/support/v7/widget/ActionMenuPresenter;->y:Z

    if-eqz v5, :cond_1ad

    .line 444
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ActionMenuPresenter;->A:I

    div-int v4, v9, v3

    .line 445
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ActionMenuPresenter;->A:I

    rem-int v3, v9, v3

    .line 446
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/v7/widget/ActionMenuPresenter;->A:I

    div-int/2addr v3, v4

    add-int/2addr v3, v5

    move v5, v3

    move v3, v4

    .line 450
    :goto_88
    const/4 v4, 0x0

    move v7, v8

    move v11, v4

    move v6, v3

    :goto_8c
    if-ge v11, v14, :cond_1a1

    .line 451
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Landroid/support/v7/internal/view/menu/m;

    .line 453
    invoke-virtual {v4}, Landroid/support/v7/internal/view/menu/m;->h()Z

    move-result v3

    if-eqz v3, :cond_e3

    .line 454
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ActionMenuPresenter;->C:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3, v2}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 455
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/ActionMenuPresenter;->C:Landroid/view/View;

    if-nez v3, :cond_af

    .line 456
    move-object/from16 v0, p0

    iput-object v8, v0, Landroid/support/v7/widget/ActionMenuPresenter;->C:Landroid/view/View;

    .line 458
    :cond_af
    move-object/from16 v0, p0

    iget-boolean v3, v0, Landroid/support/v7/widget/ActionMenuPresenter;->y:Z

    if-eqz v3, :cond_de

    .line 459
    const/4 v3, 0x0

    invoke-static {v8, v5, v6, v15, v3}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v3

    sub-int v3, v6, v3

    .line 464
    :goto_bc
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 465
    sub-int v8, v9, v6

    .line 466
    if-nez v7, :cond_1aa

    .line 469
    :goto_c4
    invoke-virtual {v4}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v7

    .line 470
    if-eqz v7, :cond_d0

    .line 471
    const/4 v9, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v9}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 473
    :cond_d0
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/support/v7/internal/view/menu/m;->d(Z)V

    move v4, v8

    move v7, v10

    .line 450
    :goto_d6
    add-int/lit8 v8, v11, 0x1

    move v11, v8

    move v9, v4

    move v10, v7

    move v7, v6

    move v6, v3

    goto :goto_8c

    .line 462
    :cond_de
    invoke-virtual {v8, v15, v15}, Landroid/view/View;->measure(II)V

    move v3, v6

    goto :goto_bc

    .line 474
    :cond_e3
    invoke-virtual {v4}, Landroid/support/v7/internal/view/menu/m;->g()Z

    move-result v3

    if-eqz v3, :cond_197

    .line 477
    invoke-virtual {v4}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v17

    .line 478
    invoke-virtual/range {v16 .. v17}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v18

    .line 479
    if-gtz v10, :cond_f5

    if-eqz v18, :cond_152

    :cond_f5
    if-lez v9, :cond_152

    move-object/from16 v0, p0

    iget-boolean v3, v0, Landroid/support/v7/widget/ActionMenuPresenter;->y:Z

    if-eqz v3, :cond_ff

    if-lez v6, :cond_152

    :cond_ff
    const/4 v3, 0x1

    .line 482
    :goto_100
    if-eqz v3, :cond_1a7

    .line 483
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/ActionMenuPresenter;->C:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8, v2}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 484
    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/v7/widget/ActionMenuPresenter;->C:Landroid/view/View;

    if-nez v12, :cond_116

    .line 485
    move-object/from16 v0, p0

    iput-object v8, v0, Landroid/support/v7/widget/ActionMenuPresenter;->C:Landroid/view/View;

    .line 487
    :cond_116
    move-object/from16 v0, p0

    iget-boolean v12, v0, Landroid/support/v7/widget/ActionMenuPresenter;->y:Z

    if-eqz v12, :cond_154

    .line 488
    const/4 v12, 0x0

    invoke-static {v8, v5, v6, v15, v12}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v12

    .line 490
    sub-int/2addr v6, v12

    .line 491
    if-nez v12, :cond_125

    .line 492
    const/4 v3, 0x0

    .line 497
    :cond_125
    :goto_125
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 498
    sub-int/2addr v9, v8

    .line 499
    if-nez v7, :cond_12d

    move v7, v8

    .line 503
    :cond_12d
    move-object/from16 v0, p0

    iget-boolean v8, v0, Landroid/support/v7/widget/ActionMenuPresenter;->y:Z

    if-eqz v8, :cond_15a

    .line 504
    if-ltz v9, :cond_158

    const/4 v8, 0x1

    :goto_136
    and-int/2addr v3, v8

    move v12, v3

    move v8, v6

    .line 511
    :goto_139
    if-eqz v12, :cond_165

    if-eqz v17, :cond_165

    .line 512
    const/4 v3, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    move v3, v10

    .line 526
    :goto_146
    if-eqz v12, :cond_14a

    add-int/lit8 v3, v3, -0x1

    .line 528
    :cond_14a
    invoke-virtual {v4, v12}, Landroid/support/v7/internal/view/menu/m;->d(Z)V

    move v6, v7

    move v4, v9

    move v7, v3

    move v3, v8

    .line 529
    goto :goto_d6

    .line 479
    :cond_152
    const/4 v3, 0x0

    goto :goto_100

    .line 495
    :cond_154
    invoke-virtual {v8, v15, v15}, Landroid/view/View;->measure(II)V

    goto :goto_125

    .line 504
    :cond_158
    const/4 v8, 0x0

    goto :goto_136

    .line 507
    :cond_15a
    add-int v8, v9, v7

    if-lez v8, :cond_163

    const/4 v8, 0x1

    :goto_15f
    and-int/2addr v3, v8

    move v12, v3

    move v8, v6

    goto :goto_139

    :cond_163
    const/4 v8, 0x0

    goto :goto_15f

    .line 513
    :cond_165
    if-eqz v18, :cond_1a5

    .line 515
    const/4 v3, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 516
    const/4 v3, 0x0

    move v6, v10

    move v10, v3

    :goto_172
    if-ge v10, v11, :cond_1a3

    .line 517
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/internal/view/menu/m;

    .line 518
    invoke-virtual {v3}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v18

    move/from16 v0, v18

    move/from16 v1, v17

    if-ne v0, v1, :cond_193

    .line 520
    invoke-virtual {v3}, Landroid/support/v7/internal/view/menu/m;->f()Z

    move-result v18

    if-eqz v18, :cond_18c

    add-int/lit8 v6, v6, 0x1

    .line 521
    :cond_18c
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/view/menu/m;->d(Z)V

    .line 516
    :cond_193
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    goto :goto_172

    .line 531
    :cond_197
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/support/v7/internal/view/menu/m;->d(Z)V

    move v3, v6

    move v4, v9

    move v6, v7

    move v7, v10

    goto/16 :goto_d6

    .line 534
    :cond_1a1
    const/4 v2, 0x1

    return v2

    :cond_1a3
    move v3, v6

    goto :goto_146

    :cond_1a5
    move v3, v10

    goto :goto_146

    :cond_1a7
    move v12, v3

    move v8, v6

    goto :goto_139

    :cond_1aa
    move v6, v7

    goto/16 :goto_c4

    :cond_1ad
    move v5, v3

    move v3, v4

    goto/16 :goto_88

    :cond_1b1
    move v3, v7

    goto/16 :goto_43
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)Z
    .registers 9

    .prologue
    const/4 v3, 0x0

    .line 286
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/ad;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v3

    .line 303
    :goto_8
    return v0

    :cond_9
    move-object v0, p1

    .line 5065
    :goto_a
    iget-object v1, v0, Landroid/support/v7/internal/view/menu/ad;->p:Landroid/support/v7/internal/view/menu/i;

    .line 289
    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    if-eq v1, v2, :cond_15

    .line 6065
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/ad;->p:Landroid/support/v7/internal/view/menu/i;

    .line 290
    check-cast v0, Landroid/support/v7/internal/view/menu/ad;

    goto :goto_a

    .line 292
    :cond_15
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ad;->getItem()Landroid/view/MenuItem;

    move-result-object v5

    .line 6307
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/view/ViewGroup;

    .line 6308
    if-eqz v0, :cond_44

    .line 6310
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    move v4, v3

    .line 6311
    :goto_24
    if-ge v4, v6, :cond_44

    .line 6312
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 6313
    instance-of v1, v2, Landroid/support/v7/internal/view/menu/aa;

    if-eqz v1, :cond_40

    move-object v1, v2

    check-cast v1, Landroid/support/v7/internal/view/menu/aa;

    invoke-interface {v1}, Landroid/support/v7/internal/view/menu/aa;->getItemData()Landroid/support/v7/internal/view/menu/m;

    move-result-object v1

    if-ne v1, v5, :cond_40

    move-object v0, v2

    .line 293
    :goto_38
    if-nez v0, :cond_48

    .line 294
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-nez v0, :cond_46

    move v0, v3

    goto :goto_8

    .line 6311
    :cond_40
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_24

    .line 6318
    :cond_44
    const/4 v0, 0x0

    goto :goto_38

    .line 295
    :cond_46
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    .line 298
    :cond_48
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/ad;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->t:I

    .line 299
    new-instance v1, Landroid/support/v7/widget/b;

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/content/Context;

    invoke-direct {v1, p0, v2, p1}, Landroid/support/v7/widget/b;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;Landroid/support/v7/internal/view/menu/ad;)V

    iput-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->q:Landroid/support/v7/widget/b;

    .line 300
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->q:Landroid/support/v7/widget/b;

    .line 7113
    iput-object v0, v1, Landroid/support/v7/internal/view/menu/v;->b:Landroid/view/View;

    .line 301
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->q:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->d()V

    .line 302
    invoke-super {p0, p1}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/ad;)Z

    .line 303
    const/4 v0, 0x1

    goto :goto_8
.end method

.method public final a(Landroid/view/ViewGroup;I)Z
    .registers 5

    .prologue
    .line 281
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-ne v0, v1, :cond_a

    const/4 v0, 0x0

    .line 282
    :goto_9
    return v0

    :cond_a
    invoke-super {p0, p1, p2}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/view/ViewGroup;I)Z

    move-result v0

    goto :goto_9
.end method

.method public final b(Z)V
    .registers 4

    .prologue
    .line 564
    if-eqz p1, :cond_7

    .line 566
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/ad;)Z

    .line 570
    :goto_6
    return-void

    .line 568
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    goto :goto_6
.end method

.method public final c()Landroid/os/Parcelable;
    .registers 3

    .prologue
    .line 545
    new-instance v0, Landroid/support/v7/widget/ActionMenuPresenter$SavedState;

    invoke-direct {v0}, Landroid/support/v7/widget/ActionMenuPresenter$SavedState;-><init>()V

    .line 546
    iget v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->t:I

    iput v1, v0, Landroid/support/v7/widget/ActionMenuPresenter$SavedState;->a:I

    .line 547
    return-object v0
.end method

.method public final c(Landroid/support/v7/internal/view/menu/m;)Z
    .registers 3

    .prologue
    .line 223
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->f()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    .line 153
    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    .line 154
    iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->v:Z

    .line 155
    return-void
.end method

.method public final e()Z
    .registers 5

    .prologue
    .line 326
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    if-eqz v0, :cond_43

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuPresenter;->i()Z

    move-result v0

    if-nez v0, :cond_43

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_43

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    if-eqz v0, :cond_43

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    if-nez v0, :cond_43

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_43

    .line 328
    new-instance v0, Landroid/support/v7/widget/g;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    iget-object v3, p0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-direct {v0, p0, v1, v2, v3}, Landroid/support/v7/widget/g;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;)V

    .line 329
    new-instance v1, Landroid/support/v7/widget/d;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/widget/d;-><init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/support/v7/widget/g;)V

    iput-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    .line 331
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 335
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/support/v7/internal/view/menu/d;->a(Landroid/support/v7/internal/view/menu/ad;)Z

    .line 337
    const/4 v0, 0x1

    .line 339
    :goto_42
    return v0

    :cond_43
    const/4 v0, 0x0

    goto :goto_42
.end method

.method public final f()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 348
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    if-eqz v0, :cond_17

    .line 349
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->g:Landroid/support/v7/internal/view/menu/z;

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    move v0, v1

    .line 359
    :goto_16
    return v0

    .line 354
    :cond_17
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->p:Landroid/support/v7/widget/g;

    .line 355
    if-eqz v0, :cond_20

    .line 356
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/v;->f()V

    move v0, v1

    .line 357
    goto :goto_16

    .line 359
    :cond_20
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public final g()Z
    .registers 3

    .prologue
    .line 367
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuPresenter;->f()Z

    move-result v0

    .line 368
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuPresenter;->h()Z

    move-result v1

    or-int/2addr v0, v1

    .line 369
    return v0
.end method

.method public final h()Z
    .registers 2

    .prologue
    .line 378
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->q:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_b

    .line 379
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->q:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->f()V

    .line 380
    const/4 v0, 0x1

    .line 382
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final i()Z
    .registers 2

    .prologue
    .line 389
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->p:Landroid/support/v7/widget/g;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->p:Landroid/support/v7/widget/g;

    invoke-virtual {v0}, Landroid/support/v7/widget/g;->g()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final j()Z
    .registers 2

    .prologue
    .line 393
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuPresenter;->r:Landroid/support/v7/widget/d;

    if-nez v0, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuPresenter;->i()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method
