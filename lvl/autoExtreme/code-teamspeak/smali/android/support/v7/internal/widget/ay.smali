.class public final Landroid/support/v7/internal/widget/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/widget/ad;


# static fields
.field private static final e:Ljava/lang/String; = "ToolbarWidgetWrapper"

.field private static final f:I = 0x3

.field private static final g:J = 0xc8L


# instance fields
.field a:Landroid/support/v7/widget/Toolbar;

.field b:Ljava/lang/CharSequence;

.field c:Landroid/view/Window$Callback;

.field d:Z

.field private h:I

.field private i:Landroid/view/View;

.field private j:Landroid/widget/Spinner;

.field private k:Landroid/view/View;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Z

.field private p:Ljava/lang/CharSequence;

.field private q:Ljava/lang/CharSequence;

.field private r:Landroid/support/v7/widget/ActionMenuPresenter;

.field private s:I

.field private final t:Landroid/support/v7/internal/widget/av;

.field private u:I

.field private v:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Z)V
    .registers 5

    .prologue
    .line 93
    sget v0, Landroid/support/v7/a/l;->abc_action_bar_up_description:I

    sget v1, Landroid/support/v7/a/h;->abc_ic_ab_back_mtrl_am_alpha:I

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v7/internal/widget/ay;-><init>(Landroid/support/v7/widget/Toolbar;ZII)V

    .line 95
    return-void
.end method

.method private constructor <init>(Landroid/support/v7/widget/Toolbar;ZII)V
    .registers 11

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput v1, p0, Landroid/support/v7/internal/widget/ay;->s:I

    .line 89
    iput v1, p0, Landroid/support/v7/internal/widget/ay;->u:I

    .line 99
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    .line 100
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->b:Ljava/lang/CharSequence;

    .line 101
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getSubtitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->p:Ljava/lang/CharSequence;

    .line 102
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_15c

    const/4 v0, 0x1

    :goto_1c
    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ay;->o:Z

    .line 103
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->n:Landroid/graphics/drawable/Drawable;

    .line 105
    if-eqz p2, :cond_15f

    .line 106
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Landroid/support/v7/a/n;->ActionBar:[I

    sget v4, Landroid/support/v7/a/d;->actionBarStyle:I

    invoke-static {v0, v2, v3, v4}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v0

    .line 109
    sget v2, Landroid/support/v7/a/n;->ActionBar_title:I

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ax;->c(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 110
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_42

    .line 111
    invoke-virtual {p0, v2}, Landroid/support/v7/internal/widget/ay;->b(Ljava/lang/CharSequence;)V

    .line 114
    :cond_42
    sget v2, Landroid/support/v7/a/n;->ActionBar_subtitle:I

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ax;->c(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 115
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_51

    .line 116
    invoke-virtual {p0, v2}, Landroid/support/v7/internal/widget/ay;->c(Ljava/lang/CharSequence;)V

    .line 119
    :cond_51
    sget v2, Landroid/support/v7/a/n;->ActionBar_logo:I

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 120
    if-eqz v2, :cond_5c

    .line 121
    invoke-virtual {p0, v2}, Landroid/support/v7/internal/widget/ay;->b(Landroid/graphics/drawable/Drawable;)V

    .line 124
    :cond_5c
    sget v2, Landroid/support/v7/a/n;->ActionBar_icon:I

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 125
    iget-object v3, p0, Landroid/support/v7/internal/widget/ay;->n:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_6b

    if-eqz v2, :cond_6b

    .line 126
    invoke-virtual {p0, v2}, Landroid/support/v7/internal/widget/ay;->a(Landroid/graphics/drawable/Drawable;)V

    .line 129
    :cond_6b
    sget v2, Landroid/support/v7/a/n;->ActionBar_homeAsUpIndicator:I

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 130
    if-eqz v2, :cond_76

    .line 131
    invoke-virtual {p0, v2}, Landroid/support/v7/internal/widget/ay;->c(Landroid/graphics/drawable/Drawable;)V

    .line 134
    :cond_76
    sget v2, Landroid/support/v7/a/n;->ActionBar_displayOptions:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/internal/widget/ax;->a(II)I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/support/v7/internal/widget/ay;->c(I)V

    .line 136
    sget v2, Landroid/support/v7/a/n;->ActionBar_customNavigationLayout:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v2

    .line 138
    if-eqz v2, :cond_a1

    .line 139
    iget-object v3, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3, v2, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/support/v7/internal/widget/ay;->a(Landroid/view/View;)V

    .line 141
    iget v2, p0, Landroid/support/v7/internal/widget/ay;->h:I

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {p0, v2}, Landroid/support/v7/internal/widget/ay;->c(I)V

    .line 144
    :cond_a1
    sget v2, Landroid/support/v7/a/n;->ActionBar_height:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/internal/widget/ax;->d(II)I

    move-result v2

    .line 145
    if-lez v2, :cond_b6

    .line 146
    iget-object v3, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 147
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 148
    iget-object v2, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/Toolbar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 151
    :cond_b6
    sget v2, Landroid/support/v7/a/n;->ActionBar_contentInsetStart:I

    invoke-virtual {v0, v2, v5}, Landroid/support/v7/internal/widget/ax;->b(II)I

    move-result v2

    .line 153
    sget v3, Landroid/support/v7/a/n;->ActionBar_contentInsetEnd:I

    invoke-virtual {v0, v3, v5}, Landroid/support/v7/internal/widget/ax;->b(II)I

    move-result v3

    .line 155
    if-gez v2, :cond_c6

    if-ltz v3, :cond_d5

    .line 156
    :cond_c6
    iget-object v4, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1943
    iget-object v4, v4, Landroid/support/v7/widget/Toolbar;->i:Landroid/support/v7/internal/widget/ak;

    invoke-virtual {v4, v2, v3}, Landroid/support/v7/internal/widget/ak;->a(II)V

    .line 160
    :cond_d5
    sget v2, Landroid/support/v7/a/n;->ActionBar_titleTextStyle:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v2

    .line 161
    if-eqz v2, :cond_f0

    .line 162
    iget-object v3, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2689
    iput v2, v3, Landroid/support/v7/widget/Toolbar;->g:I

    .line 2690
    iget-object v5, v3, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/TextView;

    if-eqz v5, :cond_f0

    .line 2691
    iget-object v3, v3, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 165
    :cond_f0
    sget v2, Landroid/support/v7/a/n;->ActionBar_subtitleTextStyle:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v2

    .line 167
    if-eqz v2, :cond_10b

    .line 168
    iget-object v3, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2700
    iput v2, v3, Landroid/support/v7/widget/Toolbar;->h:I

    .line 2701
    iget-object v5, v3, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;

    if-eqz v5, :cond_10b

    .line 2702
    iget-object v3, v3, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 171
    :cond_10b
    sget v2, Landroid/support/v7/a/n;->ActionBar_popupTheme:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/internal/widget/ax;->e(II)I

    move-result v2

    .line 172
    if-eqz v2, :cond_118

    .line 173
    iget-object v3, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/Toolbar;->setPopupTheme(I)V

    .line 3183
    :cond_118
    iget-object v2, v0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 178
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ax;->a()Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->t:Landroid/support/v7/internal/widget/av;

    .line 4213
    :goto_123
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->u:I

    if-eq p3, v0, :cond_13a

    .line 4216
    iput p3, p0, Landroid/support/v7/internal/widget/ay;->u:I

    .line 4217
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13a

    .line 4218
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->u:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ay;->h(I)V

    .line 186
    :cond_13a
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->q:Ljava/lang/CharSequence;

    .line 188
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->t:Landroid/support/v7/internal/widget/av;

    .line 5167
    invoke-virtual {v0, p4, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 5224
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->v:Landroid/graphics/drawable/Drawable;

    if-eq v1, v0, :cond_151

    .line 5225
    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->v:Landroid/graphics/drawable/Drawable;

    .line 5226
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->G()V

    .line 190
    :cond_151
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Landroid/support/v7/internal/widget/az;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/az;-><init>(Landroid/support/v7/internal/widget/ay;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    return-void

    :cond_15c
    move v0, v1

    .line 102
    goto/16 :goto_1c

    .line 3231
    :cond_15f
    const/16 v0, 0xb

    .line 3233
    iget-object v2, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_16b

    .line 3234
    const/16 v0, 0xf

    .line 180
    :cond_16b
    iput v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    .line 182
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->t:Landroid/support/v7/internal/widget/av;

    goto :goto_123
.end method

.method private C()I
    .registers 3

    .prologue
    .line 231
    const/16 v0, 0xb

    .line 233
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 234
    const/16 v0, 0xf

    .line 236
    :cond_c
    return v0
.end method

.method private D()V
    .registers 3

    .prologue
    .line 346
    const/4 v0, 0x0

    .line 347
    iget v1, p0, Landroid/support/v7/internal/widget/ay;->h:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_13

    .line 348
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1c

    .line 349
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_19

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->m:Landroid/graphics/drawable/Drawable;

    .line 354
    :cond_13
    :goto_13
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setLogo(Landroid/graphics/drawable/Drawable;)V

    .line 355
    return-void

    .line 349
    :cond_19
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->l:Landroid/graphics/drawable/Drawable;

    goto :goto_13

    .line 351
    :cond_1c
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->l:Landroid/graphics/drawable/Drawable;

    goto :goto_13
.end method

.method private E()V
    .registers 5

    .prologue
    .line 529
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    if-nez v0, :cond_22

    .line 530
    new-instance v0, Landroid/support/v7/widget/aa;

    .line 13246
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 530
    const/4 v2, 0x0

    sget v3, Landroid/support/v7/a/d;->actionDropDownStyle:I

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    .line 531
    new-instance v0, Landroid/support/v7/widget/ck;

    const/4 v1, -0x2

    const v2, 0x800013

    invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/ck;-><init>(II)V

    .line 533
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 535
    :cond_22
    return-void
.end method

.method private F()V
    .registers 3

    .prologue
    .line 648
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_15

    .line 649
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->q:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 650
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget v1, p0, Landroid/support/v7/internal/widget/ay;->u:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(I)V

    .line 655
    :cond_15
    :goto_15
    return-void

    .line 652
    :cond_16
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->q:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_15
.end method

.method private G()V
    .registers 3

    .prologue
    .line 658
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_11

    .line 659
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->n:Landroid/graphics/drawable/Drawable;

    :goto_e
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 661
    :cond_11
    return-void

    .line 659
    :cond_12
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->v:Landroid/graphics/drawable/Drawable;

    goto :goto_e
.end method

.method private static synthetic a(Landroid/support/v7/internal/widget/ay;)Landroid/support/v7/widget/Toolbar;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method private static synthetic b(Landroid/support/v7/internal/widget/ay;)Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private static synthetic c(Landroid/support/v7/internal/widget/ay;)Landroid/view/Window$Callback;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->c:Landroid/view/Window$Callback;

    return-object v0
.end method

.method private static synthetic d(Landroid/support/v7/internal/widget/ay;)Z
    .registers 2

    .prologue
    .line 58
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ay;->d:Z

    return v0
.end method

.method private e(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 284
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->b:Ljava/lang/CharSequence;

    .line 285
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_d

    .line 286
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 288
    :cond_d
    return-void
.end method


# virtual methods
.method public final A()I
    .registers 2

    .prologue
    .line 691
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getVisibility()I

    move-result v0

    return v0
.end method

.method public final B()Landroid/view/Menu;
    .registers 2

    .prologue
    .line 702
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method

.method public final a(IJ)Landroid/support/v4/view/fk;
    .registers 6

    .prologue
    .line 591
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1b

    .line 592
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 593
    invoke-virtual {v0, p2, p3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    .line 594
    new-instance v1, Landroid/support/v7/internal/widget/ba;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/ba;-><init>(Landroid/support/v7/internal/widget/ay;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 620
    :goto_1a
    return-object v0

    .line 609
    :cond_1b
    if-nez p1, :cond_35

    .line 610
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 611
    invoke-virtual {v0, p2, p3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    .line 612
    new-instance v1, Landroid/support/v7/internal/widget/bb;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/widget/bb;-><init>(Landroid/support/v7/internal/widget/ay;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    goto :goto_1a

    .line 620
    :cond_35
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public final a()Landroid/view/ViewGroup;
    .registers 2

    .prologue
    .line 241
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public final a(I)V
    .registers 4

    .prologue
    .line 325
    if-eqz p1, :cond_d

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->t:Landroid/support/v7/internal/widget/av;

    .line 6167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 325
    :goto_9
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ay;->a(Landroid/graphics/drawable/Drawable;)V

    .line 326
    return-void

    .line 325
    :cond_d
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 330
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->l:Landroid/graphics/drawable/Drawable;

    .line 331
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->D()V

    .line 332
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;Landroid/support/v7/internal/view/menu/j;)V
    .registers 4

    .prologue
    .line 697
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    .line 14833
    iput-object p1, v0, Landroid/support/v7/widget/Toolbar;->m:Landroid/support/v7/internal/view/menu/y;

    .line 14834
    iput-object p2, v0, Landroid/support/v7/widget/Toolbar;->n:Landroid/support/v7/internal/view/menu/j;

    .line 698
    return-void
.end method

.method public final a(Landroid/support/v7/internal/widget/al;)V
    .registers 6

    .prologue
    const/4 v3, -0x2

    .line 448
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_16

    .line 449
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 451
    :cond_16
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    .line 452
    if-eqz p1, :cond_3c

    iget v0, p0, Landroid/support/v7/internal/widget/ay;->s:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3c

    .line 453
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 454
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ck;

    .line 455
    iput v3, v0, Landroid/support/v7/widget/ck;->width:I

    .line 456
    iput v3, v0, Landroid/support/v7/widget/ck;->height:I

    .line 457
    const v1, 0x800053

    iput v1, v0, Landroid/support/v7/widget/ck;->a:I

    .line 458
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/widget/al;->setAllowCollapse(Z)V

    .line 460
    :cond_3c
    return-void
.end method

.method public final a(Landroid/util/SparseArray;)V
    .registers 3

    .prologue
    .line 665
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 666
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V
    .registers 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 389
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->r:Landroid/support/v7/widget/ActionMenuPresenter;

    if-nez v0, :cond_19

    .line 390
    new-instance v0, Landroid/support/v7/widget/ActionMenuPresenter;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/ActionMenuPresenter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ay;->r:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 391
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->r:Landroid/support/v7/widget/ActionMenuPresenter;

    sget v1, Landroid/support/v7/a/i;->action_menu_presenter:I

    .line 10235
    iput v1, v0, Landroid/support/v7/internal/view/menu/d;->h:I

    .line 393
    :cond_19
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->r:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 11148
    iput-object p2, v0, Landroid/support/v7/internal/view/menu/d;->f:Landroid/support/v7/internal/view/menu/y;

    .line 394
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    check-cast p1, Landroid/support/v7/internal/view/menu/i;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->r:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 11400
    if-nez p1, :cond_29

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v2, :cond_68

    .line 11404
    :cond_29
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->d()V

    .line 11405
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    .line 11670
    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    .line 11406
    if-eq v2, p1, :cond_68

    .line 11410
    if-eqz v2, :cond_3e

    .line 11411
    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->k:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v2, v3}, Landroid/support/v7/internal/view/menu/i;->b(Landroid/support/v7/internal/view/menu/x;)V

    .line 11412
    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->l:Landroid/support/v7/widget/cj;

    invoke-virtual {v2, v3}, Landroid/support/v7/internal/view/menu/i;->b(Landroid/support/v7/internal/view/menu/x;)V

    .line 11415
    :cond_3e
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Landroid/support/v7/widget/cj;

    if-nez v2, :cond_4a

    .line 11416
    new-instance v2, Landroid/support/v7/widget/cj;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Landroid/support/v7/widget/cj;-><init>(Landroid/support/v7/widget/Toolbar;B)V

    iput-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Landroid/support/v7/widget/cj;

    .line 12163
    :cond_4a
    iput-boolean v4, v1, Landroid/support/v7/widget/ActionMenuPresenter;->o:Z

    .line 11420
    if-eqz p1, :cond_69

    .line 11421
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {p1, v1, v2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V

    .line 11422
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Landroid/support/v7/widget/cj;

    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {p1, v2, v3}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V

    .line 11429
    :goto_5a
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget v3, v0, Landroid/support/v7/widget/Toolbar;->f:I

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ActionMenuView;->setPopupTheme(I)V

    .line 11430
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ActionMenuView;->setPresenter(Landroid/support/v7/widget/ActionMenuPresenter;)V

    .line 11431
    iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->k:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 395
    :cond_68
    return-void

    .line 11424
    :cond_69
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {v1, v2, v5}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V

    .line 11425
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Landroid/support/v7/widget/cj;

    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {v2, v3, v5}, Landroid/support/v7/widget/cj;->a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V

    .line 11426
    invoke-virtual {v1, v4}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Z)V

    .line 11427
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Landroid/support/v7/widget/cj;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/cj;->a(Z)V

    goto :goto_5a
.end method

.method public final a(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 566
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->k:Landroid/view/View;

    if-eqz v0, :cond_11

    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_11

    .line 567
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 569
    :cond_11
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->k:Landroid/view/View;

    .line 570
    if-eqz p1, :cond_22

    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_22

    .line 571
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 573
    :cond_22
    return-void
.end method

.method public final a(Landroid/view/Window$Callback;)V
    .registers 2

    .prologue
    .line 261
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->c:Landroid/view/Window$Callback;

    .line 262
    return-void
.end method

.method public final a(Landroid/widget/SpinnerAdapter;Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .registers 4

    .prologue
    .line 540
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->E()V

    .line 541
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 542
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 543
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 267
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ay;->o:Z

    if-nez v0, :cond_7

    .line 268
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/ay;->e(Ljava/lang/CharSequence;)V

    .line 270
    :cond_7
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 474
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setCollapsible(Z)V

    .line 475
    return-void
.end method

.method public final b()Landroid/content/Context;
    .registers 2

    .prologue
    .line 246
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)V
    .registers 4

    .prologue
    .line 336
    if-eqz p1, :cond_d

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->t:Landroid/support/v7/internal/widget/av;

    .line 7167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 336
    :goto_9
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ay;->b(Landroid/graphics/drawable/Drawable;)V

    .line 337
    return-void

    .line 336
    :cond_d
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 341
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->m:Landroid/graphics/drawable/Drawable;

    .line 342
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->D()V

    .line 343
    return-void
.end method

.method public final b(Landroid/util/SparseArray;)V
    .registers 3

    .prologue
    .line 670
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 671
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 279
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ay;->o:Z

    .line 280
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/ay;->e(Ljava/lang/CharSequence;)V

    .line 281
    return-void
.end method

.method public final c(I)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 409
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    .line 410
    xor-int/2addr v0, p1

    .line 411
    iput p1, p0, Landroid/support/v7/internal/widget/ay;->h:I

    .line 412
    if-eqz v0, :cond_46

    .line 413
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_16

    .line 414
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_47

    .line 415
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->G()V

    .line 416
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->F()V

    .line 422
    :cond_16
    :goto_16
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_1d

    .line 423
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->D()V

    .line 426
    :cond_1d
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_33

    .line 427
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_4d

    .line 428
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ay;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 429
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/internal/widget/ay;->p:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 436
    :cond_33
    :goto_33
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_46

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->k:Landroid/view/View;

    if-eqz v0, :cond_46

    .line 437
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_58

    .line 438
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 444
    :cond_46
    :goto_46
    return-void

    .line 418
    :cond_47
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_16

    .line 431
    :cond_4d
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 432
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_33

    .line 440
    :cond_58
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_46
.end method

.method public final c(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 625
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->n:Landroid/graphics/drawable/Drawable;

    .line 626
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->G()V

    .line 627
    return-void
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 297
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->p:Ljava/lang/CharSequence;

    .line 298
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_d

    .line 299
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 301
    :cond_d
    return-void
.end method

.method public final c()Z
    .registers 3

    .prologue
    .line 251
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    .line 5553
    iget-object v1, v0, Landroid/support/v7/widget/Toolbar;->l:Landroid/support/v7/widget/cj;

    if-eqz v1, :cond_e

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->l:Landroid/support/v7/widget/cj;

    iget-object v0, v0, Landroid/support/v7/widget/cj;->b:Landroid/support/v7/internal/view/menu/m;

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    .line 251
    goto :goto_d
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 256
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->c()V

    .line 257
    return-void
.end method

.method public final d(I)V
    .registers 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 489
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->s:I

    .line 490
    if-eq p1, v0, :cond_59

    .line 491
    packed-switch v0, :pswitch_data_78

    .line 504
    :cond_9
    :goto_9
    iput p1, p0, Landroid/support/v7/internal/widget/ay;->s:I

    .line 506
    packed-switch p1, :pswitch_data_80

    .line 523
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid navigation mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 493
    :pswitch_23
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_9

    .line 494
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_9

    .line 498
    :pswitch_39
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_9

    .line 499
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_9

    .line 510
    :pswitch_4f
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->E()V

    .line 511
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 526
    :cond_59
    :goto_59
    :pswitch_59
    return-void

    .line 514
    :pswitch_5a
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    if-eqz v0, :cond_59

    .line 515
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 516
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ck;

    .line 517
    iput v2, v0, Landroid/support/v7/widget/ck;->width:I

    .line 518
    iput v2, v0, Landroid/support/v7/widget/ck;->height:I

    .line 519
    const v1, 0x800053

    iput v1, v0, Landroid/support/v7/widget/ck;->a:I

    goto :goto_59

    .line 491
    nop

    :pswitch_data_78
    .packed-switch 0x1
        :pswitch_23
        :pswitch_39
    .end packed-switch

    .line 506
    :pswitch_data_80
    .packed-switch 0x0
        :pswitch_59
        :pswitch_4f
        :pswitch_5a
    .end packed-switch
.end method

.method public final d(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 224
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->v:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_9

    .line 225
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->v:Landroid/graphics/drawable/Drawable;

    .line 226
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->G()V

    .line 228
    :cond_9
    return-void
.end method

.method public final d(Ljava/lang/CharSequence;)V
    .registers 2

    .prologue
    .line 638
    iput-object p1, p0, Landroid/support/v7/internal/widget/ay;->q:Ljava/lang/CharSequence;

    .line 639
    invoke-direct {p0}, Landroid/support/v7/internal/widget/ay;->F()V

    .line 640
    return-void
.end method

.method public final e()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 274
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)V
    .registers 4

    .prologue
    .line 547
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    if-nez v0, :cond_c

    .line 548
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t set dropdown selected position without an adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 551
    :cond_c
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 552
    return-void
.end method

.method public final e(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 676
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 677
    return-void
.end method

.method public final f()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 292
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getSubtitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)V
    .registers 4

    .prologue
    .line 582
    const-wide/16 v0, 0xc8

    invoke-virtual {p0, p1, v0, v1}, Landroid/support/v7/internal/widget/ay;->a(IJ)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 584
    if-eqz v0, :cond_b

    .line 585
    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    .line 587
    :cond_b
    return-void
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 305
    const-string v0, "ToolbarWidgetWrapper"

    const-string v1, "Progress display unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    return-void
.end method

.method public final g(I)V
    .registers 4

    .prologue
    .line 631
    if-eqz p1, :cond_d

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->t:Landroid/support/v7/internal/widget/av;

    .line 14167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 631
    :goto_9
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ay;->c(Landroid/graphics/drawable/Drawable;)V

    .line 634
    return-void

    .line 631
    :cond_d
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final h()V
    .registers 3

    .prologue
    .line 310
    const-string v0, "ToolbarWidgetWrapper"

    const-string v1, "Progress display unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    return-void
.end method

.method public final h(I)V
    .registers 3

    .prologue
    .line 644
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ay;->d(Ljava/lang/CharSequence;)V

    .line 645
    return-void

    .line 14246
    :cond_7
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 644
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public final i(I)V
    .registers 3

    .prologue
    .line 213
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->u:I

    if-ne p1, v0, :cond_5

    .line 220
    :cond_4
    :goto_4
    return-void

    .line 216
    :cond_5
    iput p1, p0, Landroid/support/v7/internal/widget/ay;->u:I

    .line 217
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 218
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->u:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ay;->h(I)V

    goto :goto_4
.end method

.method public final i()Z
    .registers 2

    .prologue
    .line 315
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final j(I)V
    .registers 3

    .prologue
    .line 686
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    .line 687
    return-void
.end method

.method public final j()Z
    .registers 2

    .prologue
    .line 320
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final k()Z
    .registers 3

    .prologue
    .line 359
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    .line 7362
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getVisibility()I

    move-result v1

    if-nez v1, :cond_14

    iget-object v1, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v1, :cond_14

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    .line 7570
    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView;->d:Z

    .line 7362
    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    .line 359
    goto :goto_13
.end method

.method public final l()Z
    .registers 2

    .prologue
    .line 364
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->a()Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 369
    iget-object v2, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    .line 8377
    iget-object v3, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v3, :cond_1c

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    .line 8703
    iget-object v3, v2, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v3, :cond_1a

    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v2}, Landroid/support/v7/widget/ActionMenuPresenter;->j()Z

    move-result v2

    if-eqz v2, :cond_1a

    move v2, v0

    .line 8377
    :goto_17
    if-eqz v2, :cond_1c

    :goto_19
    return v0

    :cond_1a
    move v2, v1

    .line 8703
    goto :goto_17

    :cond_1c
    move v0, v1

    .line 369
    goto :goto_19
.end method

.method public final n()Z
    .registers 2

    .prologue
    .line 374
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->b()Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 379
    iget-object v2, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    .line 9395
    iget-object v3, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v3, :cond_1c

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    .line 9688
    iget-object v3, v2, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v3, :cond_1a

    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v2}, Landroid/support/v7/widget/ActionMenuPresenter;->f()Z

    move-result v2

    if-eqz v2, :cond_1a

    move v2, v0

    .line 9395
    :goto_17
    if-eqz v2, :cond_1c

    :goto_19
    return v0

    :cond_1a
    move v2, v1

    .line 9688
    goto :goto_17

    :cond_1c
    move v0, v1

    .line 379
    goto :goto_19
.end method

.method public final p()V
    .registers 2

    .prologue
    .line 384
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ay;->d:Z

    .line 385
    return-void
.end method

.method public final q()V
    .registers 3

    .prologue
    .line 399
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    .line 12438
    iget-object v1, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v1, :cond_b

    .line 12439
    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->b()V

    .line 400
    :cond_b
    return-void
.end method

.method public final r()I
    .registers 2

    .prologue
    .line 404
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->h:I

    return v0
.end method

.method public final s()Z
    .registers 2

    .prologue
    .line 464
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->i:Landroid/view/View;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final t()Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 469
    iget-object v1, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    .line 12445
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/TextView;

    if-eqz v2, :cond_1d

    .line 12449
    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 12450
    if-eqz v2, :cond_1d

    .line 12454
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v3

    move v1, v0

    .line 12455
    :goto_14
    if-ge v1, v3, :cond_1d

    .line 12456
    invoke-virtual {v2, v1}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v4

    if-lez v4, :cond_1e

    .line 12457
    const/4 v0, 0x1

    :cond_1d
    return v0

    .line 12455
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_14
.end method

.method public final u()V
    .registers 1

    .prologue
    .line 480
    return-void
.end method

.method public final v()I
    .registers 2

    .prologue
    .line 484
    iget v0, p0, Landroid/support/v7/internal/widget/ay;->s:I

    return v0
.end method

.method public final w()I
    .registers 2

    .prologue
    .line 556
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final x()I
    .registers 2

    .prologue
    .line 561
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->j:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final y()Landroid/view/View;
    .registers 2

    .prologue
    .line 577
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->k:Landroid/view/View;

    return-object v0
.end method

.method public final z()I
    .registers 2

    .prologue
    .line 681
    iget-object v0, p0, Landroid/support/v7/internal/widget/ay;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getHeight()I

    move-result v0

    return v0
.end method
