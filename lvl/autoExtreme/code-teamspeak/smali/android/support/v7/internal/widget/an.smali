.class public final Landroid/support/v7/internal/widget/an;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/internal/widget/al;


# direct methods
.method private constructor <init>(Landroid/support/v7/internal/widget/al;)V
    .registers 2

    .prologue
    .line 547
    iput-object p1, p0, Landroid/support/v7/internal/widget/an;->a:Landroid/support/v7/internal/widget/al;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/internal/widget/al;B)V
    .registers 3

    .prologue
    .line 547
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/an;-><init>(Landroid/support/v7/internal/widget/al;)V

    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 550
    iget-object v0, p0, Landroid/support/v7/internal/widget/an;->a:Landroid/support/v7/internal/widget/al;

    invoke-static {v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/internal/widget/al;)Landroid/support/v7/widget/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/aj;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 555
    iget-object v0, p0, Landroid/support/v7/internal/widget/an;->a:Landroid/support/v7/internal/widget/al;

    invoke-static {v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/internal/widget/al;)Landroid/support/v7/widget/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ap;

    .line 1543
    iget-object v0, v0, Landroid/support/v7/internal/widget/ap;->a:Landroid/support/v7/app/g;

    .line 555
    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 560
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6

    .prologue
    .line 565
    if-nez p2, :cond_f

    .line 566
    iget-object v1, p0, Landroid/support/v7/internal/widget/an;->a:Landroid/support/v7/internal/widget/al;

    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/an;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/g;

    invoke-static {v1, v0}, Landroid/support/v7/internal/widget/al;->a(Landroid/support/v7/internal/widget/al;Landroid/support/v7/app/g;)Landroid/support/v7/internal/widget/ap;

    move-result-object p2

    .line 570
    :goto_e
    return-object p2

    :cond_f
    move-object v0, p2

    .line 568
    check-cast v0, Landroid/support/v7/internal/widget/ap;

    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/an;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/g;

    .line 2408
    iput-object v1, v0, Landroid/support/v7/internal/widget/ap;->a:Landroid/support/v7/app/g;

    .line 2409
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ap;->a()V

    goto :goto_e
.end method
