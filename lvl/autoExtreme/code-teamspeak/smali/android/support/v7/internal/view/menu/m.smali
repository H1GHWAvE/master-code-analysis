.class public final Landroid/support/v7/internal/view/menu/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/g/a/b;


# static fields
.field private static final F:I = 0x1

.field private static final G:I = 0x2

.field private static final H:I = 0x4

.field private static final I:I = 0x8

.field private static final J:I = 0x10

.field private static final K:I = 0x20

.field static final j:I = 0x0

.field static l:Ljava/lang/String; = null

.field static m:Ljava/lang/String; = null

.field static n:Ljava/lang/String; = null

.field static o:Ljava/lang/String; = null

.field private static final p:Ljava/lang/String; = "MenuItemImpl"

.field private static final q:I = 0x3


# instance fields
.field private A:I

.field private B:Landroid/support/v7/internal/view/menu/ad;

.field private C:Ljava/lang/Runnable;

.field private D:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private E:I

.field private L:Landroid/view/View;

.field private M:Landroid/support/v4/view/bf;

.field private N:Z

.field final f:I

.field g:Landroid/support/v7/internal/view/menu/i;

.field h:I

.field public i:Landroid/support/v4/view/n;

.field k:Landroid/view/ContextMenu$ContextMenuInfo;

.field private final r:I

.field private final s:I

.field private final t:I

.field private u:Ljava/lang/CharSequence;

.field private v:Ljava/lang/CharSequence;

.field private w:Landroid/content/Intent;

.field private x:C

.field private y:C

.field private z:Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>(Landroid/support/v7/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput v1, p0, Landroid/support/v7/internal/view/menu/m;->A:I

    .line 77
    const/16 v0, 0x10

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 85
    iput v1, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    .line 90
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/m;->N:Z

    .line 134
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    .line 135
    iput p3, p0, Landroid/support/v7/internal/view/menu/m;->r:I

    .line 136
    iput p2, p0, Landroid/support/v7/internal/view/menu/m;->s:I

    .line 137
    iput p4, p0, Landroid/support/v7/internal/view/menu/m;->t:I

    .line 138
    iput p5, p0, Landroid/support/v7/internal/view/menu/m;->f:I

    .line 139
    iput-object p6, p0, Landroid/support/v7/internal/view/menu/m;->u:Ljava/lang/CharSequence;

    .line 140
    iput p7, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    .line 141
    return-void
.end method

.method private a(I)Landroid/support/v4/g/a/b;
    .registers 5

    .prologue
    .line 619
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    .line 5807
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 620
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 621
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/view/View;)Landroid/support/v4/g/a/b;

    .line 622
    return-object p0
.end method

.method private a(Landroid/view/View;)Landroid/support/v4/g/a/b;
    .registers 4

    .prologue
    .line 608
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    .line 609
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    .line 610
    if-eqz p1, :cond_17

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_17

    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->r:I

    if-lez v0, :cond_17

    .line 611
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->r:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 613
    :cond_17
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->g()V

    .line 614
    return-object p0
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/v7/internal/view/menu/i;
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method private a(Ljava/lang/Runnable;)Landroid/view/MenuItem;
    .registers 2

    .prologue
    .line 232
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->C:Ljava/lang/Runnable;

    .line 233
    return-object p0
.end method

.method private a(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 2

    .prologue
    .line 545
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->k:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 546
    return-void
.end method

.method private b(I)Landroid/support/v4/g/a/b;
    .registers 2

    .prologue
    .line 675
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/m;->setShowAsAction(I)V

    .line 676
    return-object p0
.end method

.method private j()I
    .registers 2

    .prologue
    .line 213
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->f:I

    return v0
.end method

.method private k()Ljava/lang/Runnable;
    .registers 2

    .prologue
    .line 228
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->C:Ljava/lang/Runnable;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .registers 4

    .prologue
    .line 296
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->c()C

    move-result v0

    .line 297
    if-nez v0, :cond_9

    .line 298
    const-string v0, ""

    .line 321
    :goto_8
    return-object v0

    .line 301
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Landroid/support/v7/internal/view/menu/m;->l:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 302
    sparse-switch v0, :sswitch_data_2e

    .line 317
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 321
    :goto_16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 305
    :sswitch_1b
    sget-object v0, Landroid/support/v7/internal/view/menu/m;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_16

    .line 309
    :sswitch_21
    sget-object v0, Landroid/support/v7/internal/view/menu/m;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_16

    .line 313
    :sswitch_27
    sget-object v0, Landroid/support/v7/internal/view/menu/m;->o:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_16

    .line 302
    nop

    :sswitch_data_2e
    .sparse-switch
        0x8 -> :sswitch_21
        0xa -> :sswitch_1b
        0x20 -> :sswitch_27
    .end sparse-switch
.end method

.method private m()V
    .registers 2

    .prologue
    .line 554
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->g()V

    .line 555
    return-void
.end method

.method private n()Z
    .registers 2

    .prologue
    .line 561
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    .line 5310
    iget-boolean v0, v0, Landroid/support/v7/internal/view/menu/i;->m:Z

    .line 561
    return v0
.end method

.method private o()Z
    .registers 3

    .prologue
    .line 585
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method


# virtual methods
.method public final a(Landroid/support/v4/view/bf;)Landroid/support/v4/g/a/b;
    .registers 2

    .prologue
    .line 714
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->M:Landroid/support/v4/view/bf;

    .line 715
    return-object p0
.end method

.method public final a(Landroid/support/v4/view/n;)Landroid/support/v4/g/a/b;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 656
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    if-eqz v0, :cond_b

    .line 657
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    .line 6248
    iput-object v1, v0, Landroid/support/v4/view/n;->b:Landroid/support/v4/view/p;

    .line 6249
    iput-object v1, v0, Landroid/support/v4/view/n;->a:Landroid/support/v4/view/o;

    .line 659
    :cond_b
    iput-object v1, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    .line 660
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    .line 661
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 662
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    if-eqz v0, :cond_23

    .line 663
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    new-instance v1, Landroid/support/v7/internal/view/menu/n;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/view/menu/n;-><init>(Landroid/support/v7/internal/view/menu/m;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/n;->a(Landroid/support/v4/view/p;)V

    .line 670
    :cond_23
    return-object p0
.end method

.method public final a()Landroid/support/v4/view/n;
    .registers 2

    .prologue
    .line 651
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    return-object v0
.end method

.method final a(Landroid/support/v7/internal/view/menu/aa;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 363
    if-eqz p1, :cond_d

    invoke-interface {p1}, Landroid/support/v7/internal/view/menu/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_c
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)V
    .registers 3

    .prologue
    .line 345
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->B:Landroid/support/v7/internal/view/menu/ad;

    .line 347
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/view/menu/ad;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 348
    return-void
.end method

.method public final a(Z)V
    .registers 4

    .prologue
    .line 468
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v1, v0, -0x5

    if-eqz p1, :cond_b

    const/4 v0, 0x4

    :goto_7
    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 469
    return-void

    .line 468
    :cond_b
    const/4 v0, 0x0

    goto :goto_7
.end method

.method final b(Z)V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 494
    iget v2, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 495
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v3, v0, -0x3

    if-eqz p1, :cond_17

    const/4 v0, 0x2

    :goto_a
    or-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 496
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    if-eq v2, v0, :cond_16

    .line 497
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 499
    :cond_16
    return-void

    :cond_17
    move v0, v1

    .line 495
    goto :goto_a
.end method

.method public final b()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 149
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->D:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v1, :cond_e

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->D:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v1, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 175
    :cond_d
    :goto_d
    return v0

    .line 153
    :cond_e
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/i;->k()Landroid/support/v7/internal/view/menu/i;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 157
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->C:Ljava/lang/Runnable;

    if-eqz v1, :cond_26

    .line 158
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->C:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_d

    .line 162
    :cond_26
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->w:Landroid/content/Intent;

    if-eqz v1, :cond_3c

    .line 164
    :try_start_2a
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    .line 1807
    iget-object v1, v1, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 164
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/m;->w:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_33
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2a .. :try_end_33} :catch_34

    goto :goto_d

    .line 166
    :catch_34
    move-exception v1

    .line 167
    const-string v2, "MenuItemImpl"

    const-string v3, "Can\'t find activity to handle intent; ignoring"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 171
    :cond_3c
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    if-eqz v1, :cond_48

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    invoke-virtual {v1}, Landroid/support/v4/view/n;->e()Z

    move-result v1

    if-nez v1, :cond_d

    .line 175
    :cond_48
    const/4 v0, 0x0

    goto :goto_d
.end method

.method final c()C
    .registers 2

    .prologue
    .line 286
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->y:C

    :goto_a
    return v0

    :cond_b
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->x:C

    goto :goto_a
.end method

.method final c(Z)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 518
    iget v2, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 519
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v3, v0, -0x9

    if-eqz p1, :cond_13

    move v0, v1

    :goto_a
    or-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 520
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    if-eq v2, v0, :cond_12

    const/4 v1, 0x1

    :cond_12
    return v1

    .line 519
    :cond_13
    const/16 v0, 0x8

    goto :goto_a
.end method

.method public final collapseActionView()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 695
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    and-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_8

    .line 708
    :cond_7
    :goto_7
    return v0

    .line 698
    :cond_8
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    if-nez v1, :cond_e

    .line 700
    const/4 v0, 0x1

    goto :goto_7

    .line 703
    :cond_e
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->M:Landroid/support/v4/view/bf;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->M:Landroid/support/v4/view/bf;

    invoke-interface {v1, p0}, Landroid/support/v4/view/bf;->b(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 705
    :cond_1a
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->b(Landroid/support/v7/internal/view/menu/m;)Z

    move-result v0

    goto :goto_7
.end method

.method public final d(Z)V
    .registers 3

    .prologue
    .line 577
    if-eqz p1, :cond_9

    .line 578
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 582
    :goto_8
    return-void

    .line 580
    :cond_9
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    goto :goto_8
.end method

.method final d()Z
    .registers 2

    .prologue
    .line 331
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->c()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->c()C

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final e(Z)V
    .registers 4

    .prologue
    .line 729
    iput-boolean p1, p0, Landroid/support/v7/internal/view/menu/m;->N:Z

    .line 730
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 731
    return-void
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 472
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final expandActionView()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 681
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->i()Z

    move-result v1

    if-nez v1, :cond_8

    .line 690
    :cond_7
    :goto_7
    return v0

    .line 685
    :cond_8
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->M:Landroid/support/v4/view/bf;

    if-eqz v1, :cond_14

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->M:Landroid/support/v4/view/bf;

    invoke-interface {v1, p0}, Landroid/support/v4/view/bf;->a(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 687
    :cond_14
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/m;)Z

    move-result v0

    goto :goto_7
.end method

.method public final f()Z
    .registers 3

    .prologue
    .line 565
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final g()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 569
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
    .registers 3

    .prologue
    .line 645
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.getActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getActionView()Landroid/view/View;
    .registers 2

    .prologue
    .line 627
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 628
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    .line 633
    :goto_6
    return-object v0

    .line 629
    :cond_7
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    if-eqz v0, :cond_16

    .line 630
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/n;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    .line 631
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    goto :goto_6

    .line 633
    :cond_16
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final getAlphabeticShortcut()C
    .registers 2

    .prologue
    .line 238
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->y:C

    return v0
.end method

.method public final getGroupId()I
    .registers 2

    .prologue
    .line 198
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->s:I

    return v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 417
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->z:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_7

    .line 418
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->z:Landroid/graphics/drawable/Drawable;

    .line 428
    :goto_6
    return-object v0

    .line 421
    :cond_7
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->A:I

    if-eqz v0, :cond_1b

    .line 422
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    .line 3807
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 422
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->A:I

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 423
    const/4 v1, 0x0

    iput v1, p0, Landroid/support/v7/internal/view/menu/m;->A:I

    .line 424
    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->z:Landroid/graphics/drawable/Drawable;

    goto :goto_6

    .line 428
    :cond_1b
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final getIntent()Landroid/content/Intent;
    .registers 2

    .prologue
    .line 218
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->w:Landroid/content/Intent;

    return-object v0
.end method

.method public final getItemId()I
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 204
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->r:I

    return v0
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 2

    .prologue
    .line 550
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->k:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public final getNumericShortcut()C
    .registers 2

    .prologue
    .line 256
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->x:C

    return v0
.end method

.method public final getOrder()I
    .registers 2

    .prologue
    .line 209
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->t:I

    return v0
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
    .registers 2

    .prologue
    .line 336
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->B:Landroid/support/v7/internal/view/menu/ad;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .registers 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 353
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->u:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
    .registers 4

    .prologue
    .line 388
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->v:Ljava/lang/CharSequence;

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->v:Ljava/lang/CharSequence;

    .line 390
    :goto_6
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_16

    if-eqz v0, :cond_16

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_16

    .line 394
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 397
    :cond_16
    return-object v0

    .line 388
    :cond_17
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->u:Ljava/lang/CharSequence;

    goto :goto_6
.end method

.method public final h()Z
    .registers 3

    .prologue
    .line 573
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasSubMenu()Z
    .registers 2

    .prologue
    .line 341
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->B:Landroid/support/v7/internal/view/menu/ad;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final i()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 719
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1c

    .line 720
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    if-nez v1, :cond_17

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    if-eqz v1, :cond_17

    .line 721
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    invoke-virtual {v1, p0}, Landroid/support/v4/view/n;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    .line 723
    :cond_17
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->L:Landroid/view/View;

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    .line 725
    :cond_1c
    return v0
.end method

.method public final isActionViewExpanded()Z
    .registers 2

    .prologue
    .line 735
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/m;->N:Z

    return v0
.end method

.method public final isCheckable()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 453
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isChecked()Z
    .registers 3

    .prologue
    .line 477
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isEnabled()Z
    .registers 2

    .prologue
    .line 180
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isVisible()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 503
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    if-eqz v2, :cond_1f

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    invoke-virtual {v2}, Landroid/support/v4/view/n;->b()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 504
    iget v2, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_1d

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v4/view/n;

    invoke-virtual {v2}, Landroid/support/v4/view/n;->c()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 506
    :cond_1c
    :goto_1c
    return v0

    :cond_1d
    move v0, v1

    .line 504
    goto :goto_1c

    .line 506
    :cond_1f
    iget v2, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_1c

    move v0, v1

    goto :goto_1c
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 639
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic setActionView(I)Landroid/view/MenuItem;
    .registers 5

    .prologue
    .line 41
    .line 6619
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    .line 6807
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 6620
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 6621
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/view/View;)Landroid/support/v4/g/a/b;

    .line 41
    return-object p0
.end method

.method public final synthetic setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/view/View;)Landroid/support/v4/g/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 243
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->y:C

    if-ne v0, p1, :cond_5

    .line 251
    :goto_4
    return-object p0

    .line 247
    :cond_5
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/internal/view/menu/m;->y:C

    .line 249
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    goto :goto_4
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 458
    iget v2, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 459
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v3, v0, -0x2

    if-eqz p1, :cond_17

    const/4 v0, 0x1

    :goto_a
    or-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 460
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    if-eq v2, v0, :cond_16

    .line 461
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 464
    :cond_16
    return-object p0

    :cond_17
    move v0, v1

    .line 459
    goto :goto_a
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 482
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3c

    .line 485
    iget-object v4, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    .line 4593
    invoke-interface {p0}, Landroid/view/MenuItem;->getGroupId()I

    move-result v5

    .line 4595
    iget-object v0, v4, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v2

    .line 4596
    :goto_14
    if-ge v3, v6, :cond_3f

    .line 4597
    iget-object v0, v4, Landroid/support/v7/internal/view/menu/i;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 4598
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v1

    if-ne v1, v5, :cond_36

    .line 4599
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->e()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 4600
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isCheckable()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 4603
    if-ne v0, p0, :cond_3a

    const/4 v1, 0x1

    :goto_33
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/m;->b(Z)V

    .line 4596
    :cond_36
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_14

    :cond_3a
    move v1, v2

    .line 4603
    goto :goto_33

    .line 487
    :cond_3c
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/m;->b(Z)V

    .line 490
    :cond_3f
    return-object p0
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 185
    if-eqz p1, :cond_f

    .line 186
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    .line 191
    :goto_8
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 193
    return-object p0

    .line 188
    :cond_f
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->E:I

    goto :goto_8
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 442
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->z:Landroid/graphics/drawable/Drawable;

    .line 443
    iput p1, p0, Landroid/support/v7/internal/view/menu/m;->A:I

    .line 446
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 448
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 433
    iput v1, p0, Landroid/support/v7/internal/view/menu/m;->A:I

    .line 434
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->z:Landroid/graphics/drawable/Drawable;

    .line 435
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 437
    return-object p0
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .registers 2

    .prologue
    .line 223
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->w:Landroid/content/Intent;

    .line 224
    return-object p0
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 261
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->x:C

    if-ne v0, p1, :cond_5

    .line 269
    :goto_4
    return-object p0

    .line 265
    :cond_5
    iput-char p1, p0, Landroid/support/v7/internal/view/menu/m;->x:C

    .line 267
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    goto :goto_4
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 740
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setOnActionExpandListener()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .registers 2

    .prologue
    .line 535
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->D:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 536
    return-object p0
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
    .registers 5

    .prologue
    .line 274
    iput-char p1, p0, Landroid/support/v7/internal/view/menu/m;->x:C

    .line 275
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/internal/view/menu/m;->y:C

    .line 277
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 279
    return-object p0
.end method

.method public final setShowAsAction(I)V
    .registers 4

    .prologue
    .line 590
    and-int/lit8 v0, p1, 0x3

    packed-switch v0, :pswitch_data_16

    .line 599
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 602
    :pswitch_d
    iput p1, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    .line 603
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->g()V

    .line 604
    return-void

    .line 590
    nop

    :pswitch_data_16
    .packed-switch 0x0
        :pswitch_d
        :pswitch_d
        :pswitch_d
    .end packed-switch
.end method

.method public final synthetic setShowAsActionFlags(I)Landroid/view/MenuItem;
    .registers 2

    .prologue
    .line 41
    .line 7675
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/m;->setShowAsAction(I)V

    .line 41
    return-object p0
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 383
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    .line 2807
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 383
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/m;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 370
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->u:Ljava/lang/CharSequence;

    .line 372
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 374
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->B:Landroid/support/v7/internal/view/menu/ad;

    if-eqz v0, :cond_11

    .line 375
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->B:Landroid/support/v7/internal/view/menu/ad;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/view/menu/ad;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 378
    :cond_11
    return-object p0
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 403
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->v:Ljava/lang/CharSequence;

    .line 410
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 412
    return-object p0
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 528
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/m;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->f()V

    .line 530
    :cond_b
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 541
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->u:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
