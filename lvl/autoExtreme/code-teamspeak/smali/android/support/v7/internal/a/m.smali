.class final Landroid/support/v7/internal/a/m;
.super Landroid/support/v4/view/ge;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/internal/a/l;


# direct methods
.method constructor <init>(Landroid/support/v7/internal/a/l;)V
    .registers 2

    .prologue
    .line 137
    iput-object p1, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-direct {p0}, Landroid/support/v4/view/ge;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/View;)V
    .registers 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->a(Landroid/support/v7/internal/a/l;)Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->b(Landroid/support/v7/internal/a/l;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 141
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->b(Landroid/support/v7/internal/a/l;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 142
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContainer;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 144
    :cond_24
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContainer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->c(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContainer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTransitioning(Z)V

    .line 146
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->d(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/view/i;

    .line 147
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    .line 1312
    iget-object v1, v0, Landroid/support/v7/internal/a/l;->l:Landroid/support/v7/c/b;

    if-eqz v1, :cond_4f

    .line 1313
    iget-object v1, v0, Landroid/support/v7/internal/a/l;->l:Landroid/support/v7/c/b;

    iget-object v2, v0, Landroid/support/v7/internal/a/l;->k:Landroid/support/v7/c/a;

    invoke-interface {v1, v2}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;)V

    .line 1314
    iput-object v3, v0, Landroid/support/v7/internal/a/l;->k:Landroid/support/v7/c/a;

    .line 1315
    iput-object v3, v0, Landroid/support/v7/internal/a/l;->l:Landroid/support/v7/c/b;

    .line 148
    :cond_4f
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->e(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    move-result-object v0

    if-eqz v0, :cond_60

    .line 149
    iget-object v0, p0, Landroid/support/v7/internal/a/m;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->e(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/cx;->t(Landroid/view/View;)V

    .line 151
    :cond_60
    return-void
.end method
