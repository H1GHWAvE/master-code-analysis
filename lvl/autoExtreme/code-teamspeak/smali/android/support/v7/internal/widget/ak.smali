.class public final Landroid/support/v7/internal/widget/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = -0x80000000


# instance fields
.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->b:I

    .line 31
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    .line 32
    iput v1, p0, Landroid/support/v7/internal/widget/ak;->d:I

    .line 33
    iput v1, p0, Landroid/support/v7/internal/widget/ak;->e:I

    .line 34
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->f:I

    .line 35
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->g:I

    .line 37
    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->h:Z

    .line 38
    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->i:Z

    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 41
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->b:I

    return v0
.end method

.method private a(Z)V
    .registers 4

    .prologue
    const/high16 v1, -0x80000000

    .line 76
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->h:Z

    if-ne p1, v0, :cond_7

    .line 92
    :goto_6
    return-void

    .line 79
    :cond_7
    iput-boolean p1, p0, Landroid/support/v7/internal/widget/ak;->h:Z

    .line 80
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->i:Z

    if-eqz v0, :cond_3d

    .line 81
    if-eqz p1, :cond_26

    .line 82
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->e:I

    if-eq v0, v1, :cond_20

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->e:I

    :goto_15
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->b:I

    .line 83
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->d:I

    if-eq v0, v1, :cond_23

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->d:I

    :goto_1d
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    goto :goto_6

    .line 82
    :cond_20
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->f:I

    goto :goto_15

    .line 83
    :cond_23
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->g:I

    goto :goto_1d

    .line 85
    :cond_26
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->d:I

    if-eq v0, v1, :cond_37

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->d:I

    :goto_2c
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->b:I

    .line 86
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->e:I

    if-eq v0, v1, :cond_3a

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->e:I

    :goto_34
    iput v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    goto :goto_6

    .line 85
    :cond_37
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->f:I

    goto :goto_2c

    .line 86
    :cond_3a
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->g:I

    goto :goto_34

    .line 89
    :cond_3d
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->f:I

    iput v0, p0, Landroid/support/v7/internal/widget/ak;->b:I

    .line 90
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->g:I

    iput v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    goto :goto_6
.end method

.method private b()I
    .registers 2

    .prologue
    .line 45
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    return v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 49
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->h:Z

    if-eqz v0, :cond_7

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    :goto_6
    return v0

    :cond_7
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->b:I

    goto :goto_6
.end method

.method private d()I
    .registers 2

    .prologue
    .line 53
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->h:Z

    if-eqz v0, :cond_7

    iget v0, p0, Landroid/support/v7/internal/widget/ak;->b:I

    :goto_6
    return v0

    :cond_7
    iget v0, p0, Landroid/support/v7/internal/widget/ak;->c:I

    goto :goto_6
.end method


# virtual methods
.method public final a(II)V
    .registers 5

    .prologue
    const/high16 v1, -0x80000000

    .line 57
    iput p1, p0, Landroid/support/v7/internal/widget/ak;->d:I

    .line 58
    iput p2, p0, Landroid/support/v7/internal/widget/ak;->e:I

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->i:Z

    .line 60
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->h:Z

    if-eqz v0, :cond_16

    .line 61
    if-eq p2, v1, :cond_11

    iput p2, p0, Landroid/support/v7/internal/widget/ak;->b:I

    .line 62
    :cond_11
    if-eq p1, v1, :cond_15

    iput p1, p0, Landroid/support/v7/internal/widget/ak;->c:I

    .line 67
    :cond_15
    :goto_15
    return-void

    .line 64
    :cond_16
    if-eq p1, v1, :cond_1a

    iput p1, p0, Landroid/support/v7/internal/widget/ak;->b:I

    .line 65
    :cond_1a
    if-eq p2, v1, :cond_15

    iput p2, p0, Landroid/support/v7/internal/widget/ak;->c:I

    goto :goto_15
.end method

.method public final b(II)V
    .registers 5

    .prologue
    const/high16 v1, -0x80000000

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ak;->i:Z

    .line 71
    if-eq p1, v1, :cond_b

    iput p1, p0, Landroid/support/v7/internal/widget/ak;->f:I

    iput p1, p0, Landroid/support/v7/internal/widget/ak;->b:I

    .line 72
    :cond_b
    if-eq p2, v1, :cond_11

    iput p2, p0, Landroid/support/v7/internal/widget/ak;->g:I

    iput p2, p0, Landroid/support/v7/internal/widget/ak;->c:I

    .line 73
    :cond_11
    return-void
.end method
