.class public final Landroid/support/v7/internal/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:[Ljava/lang/Class;

.field private static final b:Ljava/lang/String; = "AppCompatViewInflater"

.field private static final c:Ljava/util/Map;


# instance fields
.field private final d:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/util/AttributeSet;

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/v7/internal/a/a;->a:[Ljava/lang/Class;

    .line 62
    new-instance v0, Landroid/support/v4/n/a;

    invoke-direct {v0}, Landroid/support/v4/n/a;-><init>()V

    sput-object v0, Landroid/support/v7/internal/a/a;->c:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;Z)Landroid/content/Context;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 168
    sget-object v0, Landroid/support/v7/a/n;->View:[I

    invoke-virtual {p0, p1, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 170
    if-eqz p2, :cond_38

    .line 172
    sget v0, Landroid/support/v7/a/n;->View_android_theme:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 174
    :goto_f
    if-nez v0, :cond_20

    .line 176
    sget v0, Landroid/support/v7/a/n;->View_theme:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 178
    if-eqz v0, :cond_20

    .line 179
    const-string v1, "AppCompatViewInflater"

    const-string v3, "app:theme is now deprecated. Please move to using android:theme instead."

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    move v1, v0

    .line 183
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 185
    if-eqz v1, :cond_37

    instance-of v0, p0, Landroid/support/v7/internal/view/b;

    if-eqz v0, :cond_31

    move-object v0, p0

    check-cast v0, Landroid/support/v7/internal/view/b;

    .line 3056
    iget v0, v0, Landroid/support/v7/internal/view/b;->a:I

    .line 185
    if-eq v0, v1, :cond_37

    .line 189
    :cond_31
    new-instance v0, Landroid/support/v7/internal/view/b;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    move-object p0, v0

    .line 191
    :cond_37
    return-object p0

    :cond_38
    move v0, v1

    goto :goto_f
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .registers 6

    .prologue
    .line 143
    sget-object v0, Landroid/support/v7/internal/a/a;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Constructor;

    .line 146
    if-nez v0, :cond_36

    .line 148
    :try_start_a
    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    if-eqz p3, :cond_43

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_21
    invoke-virtual {v1, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 151
    sget-object v1, Landroid/support/v7/internal/a/a;->a:[Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 152
    sget-object v1, Landroid/support/v7/internal/a/a;->c:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    :cond_36
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 155
    iget-object v1, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_42} :catch_45

    .line 159
    :goto_42
    return-object v0

    :cond_43
    move-object v0, p2

    .line 148
    goto :goto_21

    .line 159
    :catch_45
    move-exception v0

    const/4 v0, 0x0

    goto :goto_42
.end method

.method private a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZ)Landroid/view/View;
    .registers 13
    .param p3    # Landroid/content/Context;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p4    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 70
    .line 74
    if-eqz p5, :cond_ff

    if-eqz p1, :cond_ff

    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1168
    :goto_9
    sget-object v0, Landroid/support/v7/a/n;->View:[I

    invoke-virtual {v1, p4, v0, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 1170
    if-eqz p6, :cond_fc

    .line 1172
    sget v0, Landroid/support/v7/a/n;->View_android_theme:I

    invoke-virtual {v4, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 1174
    :goto_17
    if-nez v0, :cond_28

    .line 1176
    sget v0, Landroid/support/v7/a/n;->View_theme:I

    invoke-virtual {v4, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 1178
    if-eqz v0, :cond_28

    .line 1179
    const-string v2, "AppCompatViewInflater"

    const-string v5, "app:theme is now deprecated. Please move to using android:theme instead."

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_28
    move v2, v0

    .line 1183
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 1185
    if-eqz v2, :cond_3f

    instance-of v0, v1, Landroid/support/v7/internal/view/b;

    if-eqz v0, :cond_39

    move-object v0, v1

    check-cast v0, Landroid/support/v7/internal/view/b;

    .line 2056
    iget v0, v0, Landroid/support/v7/internal/view/b;->a:I

    .line 1185
    if-eq v0, v2, :cond_3f

    .line 1189
    :cond_39
    new-instance v0, Landroid/support/v7/internal/view/b;

    invoke-direct {v0, v1, v2}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    move-object v1, v0

    .line 83
    :cond_3f
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_102

    :cond_47
    :goto_47
    packed-switch v0, :pswitch_data_12c

    .line 106
    if-eq p3, v1, :cond_f9

    .line 109
    invoke-virtual {p0, v1, p2, p4}, Landroid/support/v7/internal/a/a;->a(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 112
    :goto_50
    return-object v0

    .line 83
    :sswitch_51
    const-string v2, "EditText"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    move v0, v3

    goto :goto_47

    :sswitch_5b
    const-string v2, "Spinner"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/4 v0, 0x1

    goto :goto_47

    :sswitch_65
    const-string v2, "CheckBox"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/4 v0, 0x2

    goto :goto_47

    :sswitch_6f
    const-string v2, "RadioButton"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/4 v0, 0x3

    goto :goto_47

    :sswitch_79
    const-string v2, "CheckedTextView"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/4 v0, 0x4

    goto :goto_47

    :sswitch_83
    const-string v2, "AutoCompleteTextView"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/4 v0, 0x5

    goto :goto_47

    :sswitch_8d
    const-string v2, "MultiAutoCompleteTextView"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/4 v0, 0x6

    goto :goto_47

    :sswitch_97
    const-string v2, "RatingBar"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/4 v0, 0x7

    goto :goto_47

    :sswitch_a1
    const-string v2, "Button"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/16 v0, 0x8

    goto :goto_47

    :sswitch_ac
    const-string v2, "TextView"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    const/16 v0, 0x9

    goto :goto_47

    .line 85
    :pswitch_b7
    new-instance v0, Landroid/support/v7/widget/w;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/w;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_50

    .line 87
    :pswitch_bd
    new-instance v0, Landroid/support/v7/widget/aa;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_50

    .line 89
    :pswitch_c3
    new-instance v0, Landroid/support/v7/widget/s;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/s;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_50

    .line 91
    :pswitch_c9
    new-instance v0, Landroid/support/v7/widget/y;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_50

    .line 93
    :pswitch_cf
    new-instance v0, Landroid/support/v7/widget/t;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_50

    .line 95
    :pswitch_d6
    new-instance v0, Landroid/support/v7/widget/p;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/p;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_50

    .line 97
    :pswitch_dd
    new-instance v0, Landroid/support/v7/widget/x;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/x;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_50

    .line 99
    :pswitch_e4
    new-instance v0, Landroid/support/v7/widget/z;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_50

    .line 101
    :pswitch_eb
    new-instance v0, Landroid/support/v7/widget/r;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/r;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_50

    .line 103
    :pswitch_f2
    new-instance v0, Landroid/support/v7/widget/ai;

    invoke-direct {v0, v1, p4}, Landroid/support/v7/widget/ai;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_50

    .line 112
    :cond_f9
    const/4 v0, 0x0

    goto/16 :goto_50

    :cond_fc
    move v0, v3

    goto/16 :goto_17

    :cond_ff
    move-object v1, p3

    goto/16 :goto_9

    .line 83
    :sswitch_data_102
    .sparse-switch
        -0x7404ceea -> :sswitch_97
        -0x56c015e7 -> :sswitch_79
        -0x503aa7ad -> :sswitch_8d
        -0x37f7066e -> :sswitch_ac
        -0x1440b607 -> :sswitch_5b
        0x2e46a6ed -> :sswitch_6f
        0x5445f9ba -> :sswitch_83
        0x5f7507c3 -> :sswitch_65
        0x63577677 -> :sswitch_51
        0x77471352 -> :sswitch_a1
    .end sparse-switch

    :pswitch_data_12c
    .packed-switch 0x0
        :pswitch_b7
        :pswitch_bd
        :pswitch_c3
        :pswitch_c9
        :pswitch_cf
        :pswitch_d6
        :pswitch_dd
        :pswitch_e4
        :pswitch_eb
        :pswitch_f2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 116
    const-string v0, "view"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 117
    const-string v0, "class"

    invoke-interface {p3, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 121
    :cond_11
    :try_start_11
    iget-object v0, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    .line 122
    iget-object v0, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    const/4 v2, 0x1

    aput-object p3, v0, v2

    .line 124
    const/4 v0, -0x1

    const/16 v2, 0x2e

    invoke-virtual {p2, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ne v0, v2, :cond_33

    .line 126
    const-string v0, "android.widget."

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_29} :catch_41
    .catchall {:try_start_11 .. :try_end_29} :catchall_4c

    move-result-object v0

    .line 136
    iget-object v2, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    aput-object v1, v2, v3

    .line 137
    iget-object v2, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    aput-object v1, v2, v4

    :goto_32
    return-object v0

    .line 128
    :cond_33
    const/4 v0, 0x0

    :try_start_34
    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_37} :catch_41
    .catchall {:try_start_34 .. :try_end_37} :catchall_4c

    move-result-object v0

    .line 136
    iget-object v2, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    aput-object v1, v2, v3

    .line 137
    iget-object v2, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    aput-object v1, v2, v4

    goto :goto_32

    .line 136
    :catch_41
    move-exception v0

    iget-object v0, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    aput-object v1, v0, v3

    .line 137
    iget-object v0, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    aput-object v1, v0, v4

    move-object v0, v1

    goto :goto_32

    .line 136
    :catchall_4c
    move-exception v0

    iget-object v2, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    aput-object v1, v2, v3

    .line 137
    iget-object v2, p0, Landroid/support/v7/internal/a/a;->d:[Ljava/lang/Object;

    aput-object v1, v2, v4

    throw v0
.end method
