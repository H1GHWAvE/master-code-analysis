.class public final Landroid/support/v7/internal/widget/av;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Z

.field private static final b:Ljava/lang/String; = "TintManager"

.field private static final c:Z

.field private static final d:Landroid/graphics/PorterDuff$Mode;

.field private static final e:Ljava/util/WeakHashMap;

.field private static final f:Landroid/support/v7/internal/widget/aw;

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final j:[I

.field private static final k:[I

.field private static final l:[I


# instance fields
.field private final m:Ljava/lang/ref/WeakReference;

.field private n:Landroid/util/SparseArray;

.field private o:Landroid/content/res/ColorStateList;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v0, v3, :cond_d7

    move v0, v1

    :goto_c
    sput-boolean v0, Landroid/support/v7/internal/widget/av;->a:Z

    .line 52
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sput-object v0, Landroid/support/v7/internal/widget/av;->d:Landroid/graphics/PorterDuff$Mode;

    .line 54
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Landroid/support/v7/internal/widget/av;->e:Ljava/util/WeakHashMap;

    .line 55
    new-instance v0, Landroid/support/v7/internal/widget/aw;

    invoke-direct {v0}, Landroid/support/v7/internal/widget/aw;-><init>()V

    sput-object v0, Landroid/support/v7/internal/widget/av;->f:Landroid/support/v7/internal/widget/aw;

    .line 61
    new-array v0, v6, [I

    sget v3, Landroid/support/v7/a/h;->abc_textfield_search_default_mtrl_alpha:I

    aput v3, v0, v2

    sget v3, Landroid/support/v7/a/h;->abc_textfield_default_mtrl_alpha:I

    aput v3, v0, v1

    sget v3, Landroid/support/v7/a/h;->abc_ab_share_pack_mtrl_alpha:I

    aput v3, v0, v5

    sput-object v0, Landroid/support/v7/internal/widget/av;->g:[I

    .line 71
    const/16 v0, 0xc

    new-array v0, v0, [I

    sget v3, Landroid/support/v7/a/h;->abc_ic_ab_back_mtrl_am_alpha:I

    aput v3, v0, v2

    sget v3, Landroid/support/v7/a/h;->abc_ic_go_search_api_mtrl_alpha:I

    aput v3, v0, v1

    sget v3, Landroid/support/v7/a/h;->abc_ic_search_api_mtrl_alpha:I

    aput v3, v0, v5

    sget v3, Landroid/support/v7/a/h;->abc_ic_commit_search_api_mtrl_alpha:I

    aput v3, v0, v6

    sget v3, Landroid/support/v7/a/h;->abc_ic_clear_mtrl_alpha:I

    aput v3, v0, v7

    const/4 v3, 0x5

    sget v4, Landroid/support/v7/a/h;->abc_ic_menu_share_mtrl_alpha:I

    aput v4, v0, v3

    const/4 v3, 0x6

    sget v4, Landroid/support/v7/a/h;->abc_ic_menu_copy_mtrl_am_alpha:I

    aput v4, v0, v3

    const/4 v3, 0x7

    sget v4, Landroid/support/v7/a/h;->abc_ic_menu_cut_mtrl_alpha:I

    aput v4, v0, v3

    const/16 v3, 0x8

    sget v4, Landroid/support/v7/a/h;->abc_ic_menu_selectall_mtrl_alpha:I

    aput v4, v0, v3

    const/16 v3, 0x9

    sget v4, Landroid/support/v7/a/h;->abc_ic_menu_paste_mtrl_am_alpha:I

    aput v4, v0, v3

    const/16 v3, 0xa

    sget v4, Landroid/support/v7/a/h;->abc_ic_menu_moreoverflow_mtrl_alpha:I

    aput v4, v0, v3

    const/16 v3, 0xb

    sget v4, Landroid/support/v7/a/h;->abc_ic_voice_search_api_mtrl_alpha:I

    aput v4, v0, v3

    sput-object v0, Landroid/support/v7/internal/widget/av;->h:[I

    .line 90
    new-array v0, v7, [I

    sget v3, Landroid/support/v7/a/h;->abc_textfield_activated_mtrl_alpha:I

    aput v3, v0, v2

    sget v3, Landroid/support/v7/a/h;->abc_textfield_search_activated_mtrl_alpha:I

    aput v3, v0, v1

    sget v3, Landroid/support/v7/a/h;->abc_cab_background_top_mtrl_alpha:I

    aput v3, v0, v5

    sget v3, Landroid/support/v7/a/h;->abc_text_cursor_material:I

    aput v3, v0, v6

    sput-object v0, Landroid/support/v7/internal/widget/av;->i:[I

    .line 101
    new-array v0, v6, [I

    sget v3, Landroid/support/v7/a/h;->abc_popup_background_mtrl_mult:I

    aput v3, v0, v2

    sget v3, Landroid/support/v7/a/h;->abc_cab_background_internal_bg:I

    aput v3, v0, v1

    sget v3, Landroid/support/v7/a/h;->abc_menu_hardkey_panel_mtrl_mult:I

    aput v3, v0, v5

    sput-object v0, Landroid/support/v7/internal/widget/av;->j:[I

    .line 111
    const/16 v0, 0xa

    new-array v0, v0, [I

    sget v3, Landroid/support/v7/a/h;->abc_edit_text_material:I

    aput v3, v0, v2

    sget v3, Landroid/support/v7/a/h;->abc_tab_indicator_material:I

    aput v3, v0, v1

    sget v3, Landroid/support/v7/a/h;->abc_textfield_search_material:I

    aput v3, v0, v5

    sget v3, Landroid/support/v7/a/h;->abc_spinner_mtrl_am_alpha:I

    aput v3, v0, v6

    sget v3, Landroid/support/v7/a/h;->abc_spinner_textfield_background_material:I

    aput v3, v0, v7

    const/4 v3, 0x5

    sget v4, Landroid/support/v7/a/h;->abc_ratingbar_full_material:I

    aput v4, v0, v3

    const/4 v3, 0x6

    sget v4, Landroid/support/v7/a/h;->abc_switch_track_mtrl_alpha:I

    aput v4, v0, v3

    const/4 v3, 0x7

    sget v4, Landroid/support/v7/a/h;->abc_switch_thumb_material:I

    aput v4, v0, v3

    const/16 v3, 0x8

    sget v4, Landroid/support/v7/a/h;->abc_btn_default_mtrl_shape:I

    aput v4, v0, v3

    const/16 v3, 0x9

    sget v4, Landroid/support/v7/a/h;->abc_btn_borderless_material:I

    aput v4, v0, v3

    sput-object v0, Landroid/support/v7/internal/widget/av;->k:[I

    .line 129
    new-array v0, v5, [I

    sget v3, Landroid/support/v7/a/h;->abc_btn_check_material:I

    aput v3, v0, v2

    sget v2, Landroid/support/v7/a/h;->abc_btn_radio_material:I

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/internal/widget/av;->l:[I

    return-void

    :cond_d7
    move v0, v2

    .line 48
    goto/16 :goto_c
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/av;->m:Ljava/lang/ref/WeakReference;

    .line 164
    return-void
.end method

.method private static a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
    .registers 5

    .prologue
    .line 595
    sget-object v0, Landroid/support/v7/internal/widget/av;->f:Landroid/support/v7/internal/widget/aw;

    .line 7551
    invoke-static {p0, p1}, Landroid/support/v7/internal/widget/aw;->a(ILandroid/graphics/PorterDuff$Mode;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/aw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    .line 597
    if-nez v0, :cond_24

    .line 599
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    invoke-direct {v0, p0, p1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 600
    sget-object v1, Landroid/support/v7/internal/widget/av;->f:Landroid/support/v7/internal/widget/aw;

    .line 7555
    invoke-static {p0, p1}, Landroid/support/v7/internal/widget/aw;->a(ILandroid/graphics/PorterDuff$Mode;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/internal/widget/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 603
    :cond_24
    return-object v0
.end method

.method private static a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;[I)Landroid/graphics/PorterDuffColorFilter;
    .registers 4

    .prologue
    .line 586
    if-eqz p0, :cond_4

    if-nez p1, :cond_6

    .line 587
    :cond_4
    const/4 v0, 0x0

    .line 590
    :goto_5
    return-object v0

    .line 589
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 590
    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    goto :goto_5
.end method

.method public static a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 143
    .line 1262
    sget-object v0, Landroid/support/v7/internal/widget/av;->h:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_35

    sget-object v0, Landroid/support/v7/internal/widget/av;->g:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_35

    sget-object v0, Landroid/support/v7/internal/widget/av;->i:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_35

    sget-object v0, Landroid/support/v7/internal/widget/av;->k:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_35

    sget-object v0, Landroid/support/v7/internal/widget/av;->j:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_35

    sget-object v0, Landroid/support/v7/internal/widget/av;->l:[I

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_35

    sget v0, Landroid/support/v7/a/h;->abc_cab_background_top_material:I

    if-ne p1, v0, :cond_41

    :cond_35
    const/4 v0, 0x1

    .line 143
    :goto_36
    if-eqz v0, :cond_43

    .line 144
    invoke-static {p0}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v0

    .line 2167
    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 146
    :goto_40
    return-object v0

    :cond_41
    move v0, v1

    .line 1262
    goto :goto_36

    .line 146
    :cond_43
    invoke-static {p0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_40
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;
    .registers 3

    .prologue
    .line 154
    sget-object v0, Landroid/support/v7/internal/widget/av;->e:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/av;

    .line 155
    if-nez v0, :cond_14

    .line 156
    new-instance v0, Landroid/support/v7/internal/widget/av;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/av;-><init>(Landroid/content/Context;)V

    .line 157
    sget-object v1, Landroid/support/v7/internal/widget/av;->e:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    :cond_14
    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/support/v7/internal/widget/au;)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 567
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 568
    iget-boolean v0, p1, Landroid/support/v7/internal/widget/au;->d:Z

    if-nez v0, :cond_d

    iget-boolean v0, p1, Landroid/support/v7/internal/widget/au;->c:Z

    if-eqz v0, :cond_3d

    .line 569
    :cond_d
    iget-boolean v0, p1, Landroid/support/v7/internal/widget/au;->d:Z

    if-eqz v0, :cond_2e

    iget-object v0, p1, Landroid/support/v7/internal/widget/au;->a:Landroid/content/res/ColorStateList;

    :goto_13
    iget-boolean v2, p1, Landroid/support/v7/internal/widget/au;->c:Z

    if-eqz v2, :cond_30

    iget-object v2, p1, Landroid/support/v7/internal/widget/au;->b:Landroid/graphics/PorterDuff$Mode;

    :goto_19
    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    move-result-object v4

    .line 6586
    if-eqz v0, :cond_21

    if-nez v2, :cond_33

    .line 569
    :cond_21
    :goto_21
    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 577
    :goto_24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-gt v0, v1, :cond_2d

    .line 580
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 582
    :cond_2d
    return-void

    :cond_2e
    move-object v0, v1

    .line 569
    goto :goto_13

    :cond_30
    sget-object v2, Landroid/support/v7/internal/widget/av;->d:Landroid/graphics/PorterDuff$Mode;

    goto :goto_19

    .line 6589
    :cond_33
    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 6590
    invoke-static {v0, v2}, Landroid/support/v7/internal/widget/av;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v1

    goto :goto_21

    .line 574
    :cond_3d
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_24
.end method

.method private static a([II)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 253
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_a

    aget v3, p0, v1

    .line 254
    if-ne v3, p1, :cond_b

    .line 255
    const/4 v0, 0x1

    .line 258
    :cond_a
    return v0

    .line 253
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private b(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .registers 12

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 325
    iget-object v0, p0, Landroid/support/v7/internal/widget/av;->o:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_56

    .line 331
    sget v0, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {p1, v0}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v0

    .line 332
    sget v1, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {p1, v1}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v1

    .line 335
    const/4 v2, 0x7

    new-array v2, v2, [[I

    .line 336
    const/4 v3, 0x7

    new-array v3, v3, [I

    .line 340
    sget-object v4, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v4, v2, v5

    .line 341
    sget v4, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {p1, v4}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v4

    aput v4, v3, v5

    .line 344
    sget-object v4, Landroid/support/v7/internal/widget/ar;->b:[I

    aput-object v4, v2, v6

    .line 345
    aput v1, v3, v6

    .line 348
    sget-object v4, Landroid/support/v7/internal/widget/ar;->c:[I

    aput-object v4, v2, v7

    .line 349
    aput v1, v3, v7

    .line 352
    sget-object v4, Landroid/support/v7/internal/widget/ar;->d:[I

    aput-object v4, v2, v8

    .line 353
    aput v1, v3, v8

    .line 356
    sget-object v4, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v4, v2, v9

    .line 357
    aput v1, v3, v9

    .line 360
    const/4 v4, 0x5

    sget-object v5, Landroid/support/v7/internal/widget/ar;->f:[I

    aput-object v5, v2, v4

    .line 361
    const/4 v4, 0x5

    aput v1, v3, v4

    .line 365
    const/4 v1, 0x6

    sget-object v4, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v4, v2, v1

    .line 366
    const/4 v1, 0x6

    aput v0, v3, v1

    .line 369
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/av;->o:Landroid/content/res/ColorStateList;

    .line 371
    :cond_56
    iget-object v0, p0, Landroid/support/v7/internal/widget/av;->o:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method private static b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .registers 11

    .prologue
    const/4 v1, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 495
    new-array v0, v1, [[I

    .line 496
    new-array v1, v1, [I

    .line 499
    invoke-static {p0, p1}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    .line 500
    sget v3, Landroid/support/v7/a/d;->colorControlHighlight:I

    invoke-static {p0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v3

    .line 503
    sget-object v4, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v4, v0, v5

    .line 504
    sget v4, Landroid/support/v7/a/d;->colorButtonNormal:I

    invoke-static {p0, v4}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v4

    aput v4, v1, v5

    .line 507
    sget-object v4, Landroid/support/v7/internal/widget/ar;->d:[I

    aput-object v4, v0, v6

    .line 508
    invoke-static {v3, v2}, Landroid/support/v4/e/j;->a(II)I

    move-result v4

    aput v4, v1, v6

    .line 511
    sget-object v4, Landroid/support/v7/internal/widget/ar;->b:[I

    aput-object v4, v0, v7

    .line 512
    invoke-static {v3, v2}, Landroid/support/v4/e/j;->a(II)I

    move-result v3

    aput v3, v1, v7

    .line 516
    sget-object v3, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v3, v0, v8

    .line 517
    aput v2, v1, v8

    .line 520
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method

.method private b(I)Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .registers 7

    .prologue
    const/4 v1, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 375
    new-array v0, v1, [[I

    .line 376
    new-array v1, v1, [I

    .line 380
    sget-object v2, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v2, v0, v3

    .line 381
    sget v2, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v3

    .line 384
    sget-object v2, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v2, v0, v4

    .line 385
    sget v2, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v4

    .line 389
    sget-object v2, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v2, v0, v5

    .line 390
    sget v2, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    .line 393
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method

.method private static c(I)Z
    .registers 2

    .prologue
    .line 262
    sget-object v0, Landroid/support/v7/internal/widget/av;->h:[I

    invoke-static {v0, p0}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_34

    sget-object v0, Landroid/support/v7/internal/widget/av;->g:[I

    invoke-static {v0, p0}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_34

    sget-object v0, Landroid/support/v7/internal/widget/av;->i:[I

    invoke-static {v0, p0}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_34

    sget-object v0, Landroid/support/v7/internal/widget/av;->k:[I

    invoke-static {v0, p0}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_34

    sget-object v0, Landroid/support/v7/internal/widget/av;->j:[I

    invoke-static {v0, p0}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_34

    sget-object v0, Landroid/support/v7/internal/widget/av;->l:[I

    invoke-static {v0, p0}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v0

    if-nez v0, :cond_34

    sget v0, Landroid/support/v7/a/h;->abc_cab_background_top_material:I

    if-ne p0, v0, :cond_36

    :cond_34
    const/4 v0, 0x1

    :goto_35
    return v0

    :cond_36
    const/4 v0, 0x0

    goto :goto_35
.end method

.method private static d(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .registers 9

    .prologue
    const/4 v1, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x3e99999a    # 0.3f

    .line 397
    new-array v0, v1, [[I

    .line 398
    new-array v1, v1, [I

    .line 402
    sget-object v2, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v2, v0, v5

    .line 403
    const v2, 0x1010030

    const v3, 0x3dcccccd    # 0.1f

    invoke-static {p0, v2, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;IF)I

    move-result v2

    aput v2, v1, v5

    .line 406
    sget-object v2, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v2, v0, v6

    .line 407
    sget v2, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {p0, v2, v4}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;IF)I

    move-result v2

    aput v2, v1, v6

    .line 411
    sget-object v2, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v2, v0, v7

    .line 412
    const v2, 0x1010030

    invoke-static {p0, v2, v4}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;IF)I

    move-result v2

    aput v2, v1, v7

    .line 415
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method

.method private static d(I)Landroid/graphics/PorterDuff$Mode;
    .registers 3

    .prologue
    .line 272
    const/4 v0, 0x0

    .line 274
    sget v1, Landroid/support/v7/a/h;->abc_switch_thumb_material:I

    if-ne p0, v1, :cond_7

    .line 275
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    .line 278
    :cond_7
    return-object v0
.end method

.method private static e(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .registers 8

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 419
    new-array v0, v1, [[I

    .line 420
    new-array v1, v1, [I

    .line 423
    sget v2, Landroid/support/v7/a/d;->colorSwitchThumbNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 426
    if-eqz v2, :cond_3e

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_3e

    .line 431
    sget-object v3, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v3, v0, v4

    .line 432
    aget-object v3, v0, v4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    aput v3, v1, v4

    .line 435
    sget-object v3, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v3, v0, v5

    .line 436
    sget v3, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {p0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v3

    aput v3, v1, v5

    .line 440
    sget-object v3, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v3, v0, v6

    .line 441
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    aput v2, v1, v6

    .line 461
    :goto_38
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2

    .line 447
    :cond_3e
    sget-object v2, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v2, v0, v4

    .line 448
    sget v2, Landroid/support/v7/a/d;->colorSwitchThumbNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v4

    .line 451
    sget-object v2, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v2, v0, v5

    .line 452
    sget v2, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    .line 456
    sget-object v2, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v2, v0, v6

    .line 457
    sget v2, Landroid/support/v7/a/d;->colorSwitchThumbNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v6

    goto :goto_38
.end method

.method private static f(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .registers 7

    .prologue
    const/4 v1, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 465
    new-array v0, v1, [[I

    .line 466
    new-array v1, v1, [I

    .line 470
    sget-object v2, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v2, v0, v3

    .line 471
    sget v2, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v3

    .line 474
    sget-object v2, Landroid/support/v7/internal/widget/ar;->g:[I

    aput-object v2, v0, v4

    .line 475
    sget v2, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v4

    .line 479
    sget-object v2, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v2, v0, v5

    .line 480
    sget v2, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    .line 483
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method

.method private static g(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 487
    sget v0, Landroid/support/v7/a/d;->colorButtonNormal:I

    invoke-static {p0, v0}, Landroid/support/v7/internal/widget/av;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private static h(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 491
    sget v0, Landroid/support/v7/a/d;->colorAccent:I

    invoke-static {p0, v0}, Landroid/support/v7/internal/widget/av;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private static i(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .registers 7

    .prologue
    const/4 v1, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 524
    new-array v0, v1, [[I

    .line 525
    new-array v1, v1, [I

    .line 529
    sget-object v2, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v2, v0, v3

    .line 530
    sget v2, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v3

    .line 533
    sget-object v2, Landroid/support/v7/internal/widget/ar;->g:[I

    aput-object v2, v0, v4

    .line 534
    sget v2, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v4

    .line 537
    sget-object v2, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v2, v0, v5

    .line 538
    sget v2, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {p0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    .line 541
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method


# virtual methods
.method public final a(I)Landroid/content/res/ColorStateList;
    .registers 12

    .prologue
    const v5, 0x3e99999a    # 0.3f

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 282
    iget-object v0, p0, Landroid/support/v7/internal/widget/av;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 283
    if-nez v0, :cond_13

    const/4 v0, 0x0

    .line 321
    :cond_12
    :goto_12
    return-object v0

    .line 286
    :cond_13
    iget-object v1, p0, Landroid/support/v7/internal/widget/av;->n:Landroid/util/SparseArray;

    if-eqz v1, :cond_65

    iget-object v1, p0, Landroid/support/v7/internal/widget/av;->n:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/ColorStateList;

    .line 288
    :goto_1f
    if-nez v1, :cond_1fe

    .line 290
    sget v2, Landroid/support/v7/a/h;->abc_edit_text_material:I

    if-ne p1, v2, :cond_67

    .line 4465
    new-array v1, v9, [[I

    .line 4466
    new-array v2, v9, [I

    .line 4470
    sget-object v3, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v3, v1, v6

    .line 4471
    sget v3, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v6

    .line 4474
    sget-object v3, Landroid/support/v7/internal/widget/ar;->g:[I

    aput-object v3, v1, v7

    .line 4475
    sget v3, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v7

    .line 4479
    sget-object v3, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v3, v1, v8

    .line 4480
    sget v3, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v0

    aput v0, v2, v8

    .line 4483
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 312
    :goto_52
    if-eqz v0, :cond_12

    .line 313
    iget-object v1, p0, Landroid/support/v7/internal/widget/av;->n:Landroid/util/SparseArray;

    if-nez v1, :cond_5f

    .line 315
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Landroid/support/v7/internal/widget/av;->n:Landroid/util/SparseArray;

    .line 318
    :cond_5f
    iget-object v1, p0, Landroid/support/v7/internal/widget/av;->n:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_12

    .line 286
    :cond_65
    const/4 v1, 0x0

    goto :goto_1f

    .line 292
    :cond_67
    sget v2, Landroid/support/v7/a/h;->abc_switch_track_mtrl_alpha:I

    if-ne p1, v2, :cond_9e

    .line 5397
    new-array v1, v9, [[I

    .line 5398
    new-array v2, v9, [I

    .line 5402
    sget-object v3, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v3, v1, v6

    .line 5403
    const v3, 0x1010030

    const v4, 0x3dcccccd    # 0.1f

    invoke-static {v0, v3, v4}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;IF)I

    move-result v3

    aput v3, v2, v6

    .line 5406
    sget-object v3, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v3, v1, v7

    .line 5407
    sget v3, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {v0, v3, v5}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;IF)I

    move-result v3

    aput v3, v2, v7

    .line 5411
    sget-object v3, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v3, v1, v8

    .line 5412
    const v3, 0x1010030

    invoke-static {v0, v3, v5}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;IF)I

    move-result v0

    aput v0, v2, v8

    .line 5415
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    goto :goto_52

    .line 294
    :cond_9e
    sget v2, Landroid/support/v7/a/h;->abc_switch_thumb_material:I

    if-ne p1, v2, :cond_102

    .line 5419
    new-array v1, v9, [[I

    .line 5420
    new-array v2, v9, [I

    .line 5423
    sget v3, Landroid/support/v7/a/d;->colorSwitchThumbNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 5426
    if-eqz v3, :cond_dd

    invoke-virtual {v3}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v4

    if-eqz v4, :cond_dd

    .line 5431
    sget-object v4, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v4, v1, v6

    .line 5432
    aget-object v4, v1, v6

    invoke-virtual {v3, v4, v6}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v4

    aput v4, v2, v6

    .line 5435
    sget-object v4, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v4, v1, v7

    .line 5436
    sget v4, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {v0, v4}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v0

    aput v0, v2, v7

    .line 5440
    sget-object v0, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v0, v1, v8

    .line 5441
    invoke-virtual {v3}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    aput v0, v2, v8

    .line 5461
    :goto_d6
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    goto/16 :goto_52

    .line 5447
    :cond_dd
    sget-object v3, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v3, v1, v6

    .line 5448
    sget v3, Landroid/support/v7/a/d;->colorSwitchThumbNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v6

    .line 5451
    sget-object v3, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v3, v1, v7

    .line 5452
    sget v3, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v7

    .line 5456
    sget-object v3, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v3, v1, v8

    .line 5457
    sget v3, Landroid/support/v7/a/d;->colorSwitchThumbNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v0

    aput v0, v2, v8

    goto :goto_d6

    .line 296
    :cond_102
    sget v2, Landroid/support/v7/a/h;->abc_btn_default_mtrl_shape:I

    if-eq p1, v2, :cond_10a

    sget v2, Landroid/support/v7/a/h;->abc_btn_borderless_material:I

    if-ne p1, v2, :cond_112

    .line 5487
    :cond_10a
    sget v1, Landroid/support/v7/a/d;->colorButtonNormal:I

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/av;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto/16 :goto_52

    .line 299
    :cond_112
    sget v2, Landroid/support/v7/a/h;->abc_btn_colored_material:I

    if-ne p1, v2, :cond_11e

    .line 5491
    sget v1, Landroid/support/v7/a/d;->colorAccent:I

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/av;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto/16 :goto_52

    .line 301
    :cond_11e
    sget v2, Landroid/support/v7/a/h;->abc_spinner_mtrl_am_alpha:I

    if-eq p1, v2, :cond_126

    sget v2, Landroid/support/v7/a/h;->abc_spinner_textfield_background_material:I

    if-ne p1, v2, :cond_155

    .line 5524
    :cond_126
    new-array v1, v9, [[I

    .line 5525
    new-array v2, v9, [I

    .line 5529
    sget-object v3, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v3, v1, v6

    .line 5530
    sget v3, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v6

    .line 5533
    sget-object v3, Landroid/support/v7/internal/widget/ar;->g:[I

    aput-object v3, v1, v7

    .line 5534
    sget v3, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v7

    .line 5537
    sget-object v3, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v3, v1, v8

    .line 5538
    sget v3, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v0

    aput v0, v2, v8

    .line 5541
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    goto/16 :goto_52

    .line 304
    :cond_155
    sget-object v2, Landroid/support/v7/internal/widget/av;->h:[I

    invoke-static {v2, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v2

    if-eqz v2, :cond_165

    .line 305
    sget v1, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/ar;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto/16 :goto_52

    .line 306
    :cond_165
    sget-object v2, Landroid/support/v7/internal/widget/av;->k:[I

    invoke-static {v2, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v2

    if-eqz v2, :cond_1c4

    .line 6325
    iget-object v1, p0, Landroid/support/v7/internal/widget/av;->o:Landroid/content/res/ColorStateList;

    if-nez v1, :cond_1c0

    .line 6331
    sget v1, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v1

    .line 6332
    sget v2, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {v0, v2}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v2

    .line 6335
    const/4 v3, 0x7

    new-array v3, v3, [[I

    .line 6336
    const/4 v4, 0x7

    new-array v4, v4, [I

    .line 6340
    sget-object v5, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v5, v3, v6

    .line 6341
    sget v5, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v5}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v0

    aput v0, v4, v6

    .line 6344
    sget-object v0, Landroid/support/v7/internal/widget/ar;->b:[I

    aput-object v0, v3, v7

    .line 6345
    aput v2, v4, v7

    .line 6348
    sget-object v0, Landroid/support/v7/internal/widget/ar;->c:[I

    aput-object v0, v3, v8

    .line 6349
    aput v2, v4, v8

    .line 6352
    sget-object v0, Landroid/support/v7/internal/widget/ar;->d:[I

    aput-object v0, v3, v9

    .line 6353
    aput v2, v4, v9

    .line 6356
    const/4 v0, 0x4

    sget-object v5, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v5, v3, v0

    .line 6357
    const/4 v0, 0x4

    aput v2, v4, v0

    .line 6360
    const/4 v0, 0x5

    sget-object v5, Landroid/support/v7/internal/widget/ar;->f:[I

    aput-object v5, v3, v0

    .line 6361
    const/4 v0, 0x5

    aput v2, v4, v0

    .line 6365
    const/4 v0, 0x6

    sget-object v2, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v2, v3, v0

    .line 6366
    const/4 v0, 0x6

    aput v1, v4, v0

    .line 6369
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v3, v4}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/av;->o:Landroid/content/res/ColorStateList;

    .line 6371
    :cond_1c0
    iget-object v0, p0, Landroid/support/v7/internal/widget/av;->o:Landroid/content/res/ColorStateList;

    goto/16 :goto_52

    .line 308
    :cond_1c4
    sget-object v2, Landroid/support/v7/internal/widget/av;->l:[I

    invoke-static {v2, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v2

    if-eqz v2, :cond_1fb

    .line 6375
    new-array v1, v9, [[I

    .line 6376
    new-array v2, v9, [I

    .line 6380
    sget-object v3, Landroid/support/v7/internal/widget/ar;->a:[I

    aput-object v3, v1, v6

    .line 6381
    sget v3, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->c(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v6

    .line 6384
    sget-object v3, Landroid/support/v7/internal/widget/ar;->e:[I

    aput-object v3, v1, v7

    .line 6385
    sget v3, Landroid/support/v7/a/d;->colorControlActivated:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v7

    .line 6389
    sget-object v3, Landroid/support/v7/internal/widget/ar;->h:[I

    aput-object v3, v1, v8

    .line 6390
    sget v3, Landroid/support/v7/a/d;->colorControlNormal:I

    invoke-static {v0, v3}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v0

    aput v0, v2, v8

    .line 6393
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    goto/16 :goto_52

    :cond_1fb
    move-object v0, v1

    goto/16 :goto_52

    :cond_1fe
    move-object v0, v1

    goto/16 :goto_12
.end method

.method public final a(IZ)Landroid/graphics/drawable/Drawable;
    .registers 8

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 171
    iget-object v0, p0, Landroid/support/v7/internal/widget/av;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 172
    if-nez v0, :cond_d

    .line 207
    :goto_c
    return-object v1

    .line 174
    :cond_d
    invoke-static {v0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_35

    .line 177
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v2, v3, :cond_1d

    .line 179
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 182
    :cond_1d
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/av;->a(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 183
    if-eqz v2, :cond_37

    .line 185
    invoke-static {v0}, Landroid/support/v4/e/a/a;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 186
    invoke-static {v0, v2}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 2274
    sget v2, Landroid/support/v7/a/h;->abc_switch_thumb_material:I

    if-ne p1, v2, :cond_30

    .line 2275
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    .line 190
    :cond_30
    if-eqz v1, :cond_35

    .line 191
    invoke-static {v0, v1}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    :cond_35
    :goto_35
    move-object v1, v0

    .line 207
    goto :goto_c

    .line 193
    :cond_37
    sget v2, Landroid/support/v7/a/h;->abc_cab_background_top_material:I

    if-ne p1, v2, :cond_55

    .line 194
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    sget v2, Landroid/support/v7/a/h;->abc_cab_background_internal_bg:I

    .line 3167
    invoke-virtual {p0, v2, v4}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 194
    aput-object v2, v0, v4

    const/4 v2, 0x1

    sget v3, Landroid/support/v7/a/h;->abc_cab_background_top_mtrl_alpha:I

    .line 4167
    invoke-virtual {p0, v3, v4}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 194
    aput-object v3, v0, v2

    invoke-direct {v1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    goto :goto_c

    .line 199
    :cond_55
    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/widget/av;->a(ILandroid/graphics/drawable/Drawable;)Z

    move-result v2

    .line 200
    if-nez v2, :cond_35

    if-eqz p2, :cond_35

    move-object v0, v1

    .line 203
    goto :goto_35
.end method

.method public final a(ILandroid/graphics/drawable/Drawable;)Z
    .registers 11

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 211
    iget-object v0, p0, Landroid/support/v7/internal/widget/av;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 212
    if-nez v0, :cond_f

    move v0, v1

    .line 249
    :goto_e
    return v0

    .line 214
    :cond_f
    sget-object v6, Landroid/support/v7/internal/widget/av;->d:Landroid/graphics/PorterDuff$Mode;

    .line 219
    sget-object v3, Landroid/support/v7/internal/widget/av;->g:[I

    invoke-static {v3, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 220
    sget v3, Landroid/support/v7/a/d;->colorControlNormal:I

    move v5, v3

    move-object v7, v6

    move v6, v2

    move v3, v4

    .line 235
    :goto_1f
    if-eqz v6, :cond_65

    .line 236
    invoke-static {v0, v5}, Landroid/support/v7/internal/widget/ar;->a(Landroid/content/Context;I)I

    move-result v0

    .line 237
    invoke-static {v0, v7}, Landroid/support/v7/internal/widget/av;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 239
    if-eq v3, v4, :cond_31

    .line 240
    invoke-virtual {p2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_31
    move v0, v2

    .line 247
    goto :goto_e

    .line 222
    :cond_33
    sget-object v3, Landroid/support/v7/internal/widget/av;->i:[I

    invoke-static {v3, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v3

    if-eqz v3, :cond_42

    .line 223
    sget v3, Landroid/support/v7/a/d;->colorControlActivated:I

    move v5, v3

    move-object v7, v6

    move v6, v2

    move v3, v4

    .line 224
    goto :goto_1f

    .line 225
    :cond_42
    sget-object v3, Landroid/support/v7/internal/widget/av;->j:[I

    invoke-static {v3, p1}, Landroid/support/v7/internal/widget/av;->a([II)Z

    move-result v3

    if-eqz v3, :cond_54

    .line 226
    const v3, 0x1010031

    .line 228
    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    move v6, v2

    move-object v7, v5

    move v5, v3

    move v3, v4

    goto :goto_1f

    .line 229
    :cond_54
    sget v3, Landroid/support/v7/a/h;->abc_list_divider_mtrl_alpha:I

    if-ne p1, v3, :cond_67

    .line 230
    const v5, 0x1010030

    .line 232
    const v3, 0x42233333    # 40.8f

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object v7, v6

    move v6, v2

    goto :goto_1f

    :cond_65
    move v0, v1

    .line 249
    goto :goto_e

    :cond_67
    move v3, v4

    move v5, v1

    move-object v7, v6

    move v6, v1

    goto :goto_1f
.end method
