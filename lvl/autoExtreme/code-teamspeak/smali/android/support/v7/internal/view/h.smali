.class final Landroid/support/v7/internal/view/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final A:I = 0x0

.field private static final B:I = 0x0

.field private static final C:I = 0x0

.field private static final D:I = 0x0

.field private static final E:I = 0x0

.field private static final F:Z = false

.field private static final G:Z = true

.field private static final H:Z = true


# instance fields
.field a:Landroid/view/Menu;

.field b:I

.field c:I

.field d:I

.field e:I

.field f:Z

.field g:Z

.field h:Z

.field i:I

.field j:I

.field k:Ljava/lang/CharSequence;

.field l:Ljava/lang/CharSequence;

.field m:I

.field n:C

.field o:C

.field p:I

.field q:Z

.field r:Z

.field s:Z

.field t:I

.field u:I

.field v:Ljava/lang/String;

.field w:Ljava/lang/String;

.field x:Ljava/lang/String;

.field y:Landroid/support/v4/view/n;

.field final synthetic z:Landroid/support/v7/internal/view/f;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/view/f;Landroid/view/Menu;)V
    .registers 3

    .prologue
    .line 329
    iput-object p1, p0, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    iput-object p2, p0, Landroid/support/v7/internal/view/h;->a:Landroid/view/Menu;

    .line 332
    invoke-virtual {p0}, Landroid/support/v7/internal/view/h;->a()V

    .line 333
    return-void
.end method

.method static a(Ljava/lang/String;)C
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 417
    if-nez p0, :cond_4

    .line 420
    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_3
.end method

.method private static synthetic a(Landroid/support/v7/internal/view/h;)Landroid/support/v4/view/n;
    .registers 2

    .prologue
    .line 270
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    return-object v0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .registers 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 348
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    invoke-static {v0}, Landroid/support/v7/internal/view/f;->a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/a/n;->MenuGroup:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 350
    sget v1, Landroid/support/v7/a/n;->MenuGroup_android_id:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/view/h;->b:I

    .line 351
    sget v1, Landroid/support/v7/a/n;->MenuGroup_android_menuCategory:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/view/h;->c:I

    .line 353
    sget v1, Landroid/support/v7/a/n;->MenuGroup_android_orderInCategory:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/view/h;->d:I

    .line 354
    sget v1, Landroid/support/v7/a/n;->MenuGroup_android_checkableBehavior:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/internal/view/h;->e:I

    .line 356
    sget v1, Landroid/support/v7/a/n;->MenuGroup_android_visible:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/internal/view/h;->f:Z

    .line 357
    sget v1, Landroid/support/v7/a/n;->MenuGroup_android_enabled:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/internal/view/h;->g:Z

    .line 359
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 360
    return-void
.end method

.method private b(Landroid/util/AttributeSet;)V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 366
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    invoke-static {v0}, Landroid/support/v7/internal/view/f;->a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;

    move-result-object v0

    sget-object v3, Landroid/support/v7/a/n;->MenuItem:[I

    invoke-virtual {v0, p1, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 369
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_id:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/h;->i:I

    .line 370
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_menuCategory:I

    iget v4, p0, Landroid/support/v7/internal/view/h;->c:I

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 371
    sget v4, Landroid/support/v7/a/n;->MenuItem_android_orderInCategory:I

    iget v5, p0, Landroid/support/v7/internal/view/h;->d:I

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 372
    const/high16 v5, -0x10000

    and-int/2addr v0, v5

    const v5, 0xffff

    and-int/2addr v4, v5

    or-int/2addr v0, v4

    iput v0, p0, Landroid/support/v7/internal/view/h;->j:I

    .line 374
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_title:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/h;->k:Ljava/lang/CharSequence;

    .line 375
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_titleCondensed:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/h;->l:Ljava/lang/CharSequence;

    .line 376
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_icon:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/h;->m:I

    .line 377
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_alphabeticShortcut:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/view/h;->a(Ljava/lang/String;)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/internal/view/h;->n:C

    .line 379
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_numericShortcut:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/view/h;->a(Ljava/lang/String;)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/internal/view/h;->o:C

    .line 381
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_checkable:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_e2

    .line 383
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_checkable:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_e0

    move v0, v1

    :goto_71
    iput v0, p0, Landroid/support/v7/internal/view/h;->p:I

    .line 389
    :goto_73
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_checked:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/h;->q:Z

    .line 390
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_visible:I

    iget-boolean v4, p0, Landroid/support/v7/internal/view/h;->f:Z

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/h;->r:Z

    .line 391
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_enabled:I

    iget-boolean v4, p0, Landroid/support/v7/internal/view/h;->g:Z

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/internal/view/h;->s:Z

    .line 392
    sget v0, Landroid/support/v7/a/n;->MenuItem_showAsAction:I

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/h;->t:I

    .line 393
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_onClick:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/h;->x:Ljava/lang/String;

    .line 394
    sget v0, Landroid/support/v7/a/n;->MenuItem_actionLayout:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/h;->u:I

    .line 395
    sget v0, Landroid/support/v7/a/n;->MenuItem_actionViewClass:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/h;->v:Ljava/lang/String;

    .line 396
    sget v0, Landroid/support/v7/a/n;->MenuItem_actionProviderClass:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/h;->w:Ljava/lang/String;

    .line 398
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->w:Ljava/lang/String;

    if-eqz v0, :cond_e7

    .line 399
    :goto_bc
    if-eqz v1, :cond_e9

    iget v0, p0, Landroid/support/v7/internal/view/h;->u:I

    if-nez v0, :cond_e9

    iget-object v0, p0, Landroid/support/v7/internal/view/h;->v:Ljava/lang/String;

    if-nez v0, :cond_e9

    .line 400
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->w:Ljava/lang/String;

    invoke-static {}, Landroid/support/v7/internal/view/f;->a()[Ljava/lang/Class;

    move-result-object v1

    iget-object v4, p0, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    invoke-static {v4}, Landroid/support/v7/internal/view/f;->b(Landroid/support/v7/internal/view/f;)[Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v0, v1, v4}, Landroid/support/v7/internal/view/h;->a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/n;

    iput-object v0, p0, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    .line 411
    :goto_da
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 413
    iput-boolean v2, p0, Landroid/support/v7/internal/view/h;->h:Z

    .line 414
    return-void

    :cond_e0
    move v0, v2

    .line 383
    goto :goto_71

    .line 387
    :cond_e2
    iget v0, p0, Landroid/support/v7/internal/view/h;->e:I

    iput v0, p0, Landroid/support/v7/internal/view/h;->p:I

    goto :goto_73

    :cond_e7
    move v1, v2

    .line 398
    goto :goto_bc

    .line 404
    :cond_e9
    if-eqz v1, :cond_f2

    .line 405
    const-string v0, "SupportMenuInflater"

    const-string v1, "Ignoring attribute \'actionProviderClass\'. Action view already specified."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_f2
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    goto :goto_da
.end method

.method private c()V
    .registers 6

    .prologue
    .line 478
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/h;->h:Z

    .line 479
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->a:Landroid/view/Menu;

    iget v1, p0, Landroid/support/v7/internal/view/h;->b:I

    iget v2, p0, Landroid/support/v7/internal/view/h;->i:I

    iget v3, p0, Landroid/support/v7/internal/view/h;->j:I

    iget-object v4, p0, Landroid/support/v7/internal/view/h;->k:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/h;->a(Landroid/view/MenuItem;)V

    .line 480
    return-void
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 490
    iget-boolean v0, p0, Landroid/support/v7/internal/view/h;->h:Z

    return v0
.end method


# virtual methods
.method final a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 8

    .prologue
    .line 497
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    invoke-static {v0}, Landroid/support/v7/internal/view/f;->a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 498
    invoke-virtual {v0, p2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 499
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 500
    invoke-virtual {v0, p3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_19} :catch_1b

    move-result-object v0

    .line 504
    :goto_1a
    return-object v0

    .line 501
    :catch_1b
    move-exception v0

    .line 502
    const-string v1, "SupportMenuInflater"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot instantiate class: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 504
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public final a()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 336
    iput v0, p0, Landroid/support/v7/internal/view/h;->b:I

    .line 337
    iput v0, p0, Landroid/support/v7/internal/view/h;->c:I

    .line 338
    iput v0, p0, Landroid/support/v7/internal/view/h;->d:I

    .line 339
    iput v0, p0, Landroid/support/v7/internal/view/h;->e:I

    .line 340
    iput-boolean v1, p0, Landroid/support/v7/internal/view/h;->f:Z

    .line 341
    iput-boolean v1, p0, Landroid/support/v7/internal/view/h;->g:Z

    .line 342
    return-void
.end method

.method final a(Landroid/view/MenuItem;)V
    .registers 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 425
    iget-boolean v0, p0, Landroid/support/v7/internal/view/h;->q:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/internal/view/h;->r:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/internal/view/h;->s:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v1

    iget v0, p0, Landroid/support/v7/internal/view/h;->p:I

    if-lez v0, :cond_55

    move v0, v2

    :goto_19
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/view/h;->l:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/view/h;->m:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v1, p0, Landroid/support/v7/internal/view/h;->n:C

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v1, p0, Landroid/support/v7/internal/view/h;->o:C

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setNumericShortcut(C)Landroid/view/MenuItem;

    .line 434
    iget v0, p0, Landroid/support/v7/internal/view/h;->t:I

    if-ltz v0, :cond_3d

    .line 435
    iget v0, p0, Landroid/support/v7/internal/view/h;->t:I

    invoke-static {p1, v0}, Landroid/support/v4/view/az;->a(Landroid/view/MenuItem;I)V

    .line 438
    :cond_3d
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->x:Ljava/lang/String;

    if-eqz v0, :cond_67

    .line 439
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    invoke-static {v0}, Landroid/support/v7/internal/view/f;->a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->isRestricted()Z

    move-result v0

    if-eqz v0, :cond_57

    .line 440
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The android:onClick attribute cannot be used within a restricted context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_55
    move v0, v3

    .line 425
    goto :goto_19

    .line 443
    :cond_57
    new-instance v0, Landroid/support/v7/internal/view/g;

    iget-object v1, p0, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    invoke-static {v1}, Landroid/support/v7/internal/view/f;->c(Landroid/support/v7/internal/view/f;)Ljava/lang/Object;

    move-result-object v1

    iget-object v4, p0, Landroid/support/v7/internal/view/h;->x:Ljava/lang/String;

    invoke-direct {v0, v1, v4}, Landroid/support/v7/internal/view/g;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 448
    :cond_67
    iget v0, p0, Landroid/support/v7/internal/view/h;->p:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_76

    .line 449
    instance-of v0, p1, Landroid/support/v7/internal/view/menu/m;

    if-eqz v0, :cond_a4

    move-object v0, p1

    .line 450
    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/view/menu/m;->a(Z)V

    .line 457
    :cond_76
    :goto_76
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->v:Ljava/lang/String;

    if-eqz v0, :cond_eb

    .line 458
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->v:Ljava/lang/String;

    invoke-static {}, Landroid/support/v7/internal/view/f;->b()[Ljava/lang/Class;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    invoke-static {v3}, Landroid/support/v7/internal/view/f;->d(Landroid/support/v7/internal/view/f;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v3}, Landroid/support/v7/internal/view/h;->a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 460
    invoke-static {p1, v0}, Landroid/support/v4/view/az;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 463
    :goto_8f
    iget v0, p0, Landroid/support/v7/internal/view/h;->u:I

    if-lez v0, :cond_9a

    .line 464
    if-nez v2, :cond_e3

    .line 465
    iget v0, p0, Landroid/support/v7/internal/view/h;->u:I

    invoke-static {p1, v0}, Landroid/support/v4/view/az;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    .line 472
    :cond_9a
    :goto_9a
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    if-eqz v0, :cond_a3

    .line 473
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    invoke-static {p1, v0}, Landroid/support/v4/view/az;->a(Landroid/view/MenuItem;Landroid/support/v4/view/n;)Landroid/view/MenuItem;

    .line 475
    :cond_a3
    return-void

    .line 451
    :cond_a4
    instance-of v0, p1, Landroid/support/v7/internal/view/menu/o;

    if-eqz v0, :cond_76

    move-object v0, p1

    .line 452
    check-cast v0, Landroid/support/v7/internal/view/menu/o;

    .line 1298
    :try_start_ab
    iget-object v1, v0, Landroid/support/v7/internal/view/menu/o;->f:Ljava/lang/reflect/Method;

    if-nez v1, :cond_c7

    .line 1299
    iget-object v1, v0, Landroid/support/v7/internal/view/menu/o;->d:Ljava/lang/Object;

    check-cast v1, Landroid/support/v4/g/a/b;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v4, "setExclusiveCheckable"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/internal/view/menu/o;->f:Ljava/lang/reflect/Method;

    .line 1302
    :cond_c7
    iget-object v1, v0, Landroid/support/v7/internal/view/menu/o;->f:Ljava/lang/reflect/Method;

    iget-object v0, v0, Landroid/support/v7/internal/view/menu/o;->d:Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_d9
    .catch Ljava/lang/Exception; {:try_start_ab .. :try_end_d9} :catch_da

    goto :goto_76

    .line 1303
    :catch_da
    move-exception v0

    .line 1304
    const-string v1, "MenuItemWrapper"

    const-string v4, "Error while calling setExclusiveCheckable"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_76

    .line 468
    :cond_e3
    const-string v0, "SupportMenuInflater"

    const-string v1, "Ignoring attribute \'itemActionViewLayout\'. Action view already specified."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9a

    :cond_eb
    move v2, v3

    goto :goto_8f
.end method

.method public final b()Landroid/view/SubMenu;
    .registers 6

    .prologue
    .line 483
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/view/h;->h:Z

    .line 484
    iget-object v0, p0, Landroid/support/v7/internal/view/h;->a:Landroid/view/Menu;

    iget v1, p0, Landroid/support/v7/internal/view/h;->b:I

    iget v2, p0, Landroid/support/v7/internal/view/h;->i:I

    iget v3, p0, Landroid/support/v7/internal/view/h;->j:I

    iget-object v4, p0, Landroid/support/v7/internal/view/h;->k:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    .line 485
    invoke-interface {v0}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/view/h;->a(Landroid/view/MenuItem;)V

    .line 486
    return-object v0
.end method
