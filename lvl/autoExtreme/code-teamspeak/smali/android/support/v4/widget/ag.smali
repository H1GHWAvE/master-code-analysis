.class final Landroid/support/v4/widget/ag;
.super Landroid/support/v4/widget/ej;
.source "SourceFile"


# instance fields
.field final a:I

.field b:Landroid/support/v4/widget/eg;

.field final synthetic c:Landroid/support/v4/widget/DrawerLayout;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/support/v4/widget/DrawerLayout;I)V
    .registers 4

    .prologue
    .line 1811
    iput-object p1, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-direct {p0}, Landroid/support/v4/widget/ej;-><init>()V

    .line 1805
    new-instance v0, Landroid/support/v4/widget/ah;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/ah;-><init>(Landroid/support/v4/widget/ag;)V

    iput-object v0, p0, Landroid/support/v4/widget/ag;->d:Ljava/lang/Runnable;

    .line 1812
    iput p2, p0, Landroid/support/v4/widget/ag;->a:I

    .line 1813
    return-void
.end method

.method private static synthetic a(Landroid/support/v4/widget/ag;)V
    .registers 11

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1801
    .line 5896
    iget-object v0, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    .line 6448
    iget v2, v0, Landroid/support/v4/widget/eg;->t:I

    .line 5897
    iget v0, p0, Landroid/support/v4/widget/ag;->a:I

    if-ne v0, v4, :cond_72

    move v3, v8

    .line 5898
    :goto_d
    if-eqz v3, :cond_76

    .line 5899
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v1

    .line 5900
    if-eqz v1, :cond_74

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    :goto_1c
    add-int/2addr v0, v2

    move-object v2, v1

    move v1, v0

    .line 5906
    :goto_1f
    if-eqz v2, :cond_8c

    if-eqz v3, :cond_29

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    if-lt v0, v1, :cond_31

    :cond_29
    if-nez v3, :cond_8c

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    if-le v0, v1, :cond_8c

    :cond_31
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_8c

    .line 5909
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 5910
    iget-object v3, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v3, v2, v1, v6}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;II)Z

    .line 5911
    iput-boolean v8, v0, Landroid/support/v4/widget/ad;->c:Z

    .line 5912
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 5914
    invoke-virtual {p0}, Landroid/support/v4/widget/ag;->b()V

    .line 5916
    iget-object v9, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    .line 6662
    iget-boolean v0, v9, Landroid/support/v4/widget/DrawerLayout;->j:Z

    if-nez v0, :cond_8c

    .line 6663
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    move-wide v2, v0

    move v6, v5

    .line 6664
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 6666
    invoke-virtual {v9}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v1

    .line 6667
    :goto_66
    if-ge v7, v1, :cond_87

    .line 6668
    invoke-virtual {v9, v7}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 6667
    add-int/lit8 v7, v7, 0x1

    goto :goto_66

    :cond_72
    move v3, v7

    .line 5897
    goto :goto_d

    :cond_74
    move v0, v7

    .line 5900
    goto :goto_1c

    .line 5902
    :cond_76
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v1

    .line 5903
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v0

    sub-int/2addr v0, v2

    move-object v2, v1

    move v1, v0

    goto :goto_1f

    .line 6670
    :cond_87
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 6671
    iput-boolean v8, v9, Landroid/support/v4/widget/DrawerLayout;->j:Z

    .line 1801
    :cond_8c
    return-void
.end method

.method private a(Landroid/support/v4/widget/eg;)V
    .registers 2

    .prologue
    .line 1816
    iput-object p1, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    .line 1817
    return-void
.end method

.method private e()V
    .registers 11

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1896
    iget-object v0, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    .line 5448
    iget v2, v0, Landroid/support/v4/widget/eg;->t:I

    .line 1897
    iget v0, p0, Landroid/support/v4/widget/ag;->a:I

    if-ne v0, v4, :cond_72

    move v3, v8

    .line 1898
    :goto_d
    if-eqz v3, :cond_76

    .line 1899
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1900
    if-eqz v1, :cond_74

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    :goto_1c
    add-int/2addr v0, v2

    move-object v2, v1

    move v1, v0

    .line 1906
    :goto_1f
    if-eqz v2, :cond_8c

    if-eqz v3, :cond_29

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    if-lt v0, v1, :cond_31

    :cond_29
    if-nez v3, :cond_8c

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    if-le v0, v1, :cond_8c

    :cond_31
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_8c

    .line 1909
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 1910
    iget-object v3, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v3, v2, v1, v6}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;II)Z

    .line 1911
    iput-boolean v8, v0, Landroid/support/v4/widget/ad;->c:Z

    .line 1912
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1914
    invoke-virtual {p0}, Landroid/support/v4/widget/ag;->b()V

    .line 1916
    iget-object v9, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    .line 5662
    iget-boolean v0, v9, Landroid/support/v4/widget/DrawerLayout;->j:Z

    if-nez v0, :cond_8c

    .line 5663
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    move-wide v2, v0

    move v6, v5

    .line 5664
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 5666
    invoke-virtual {v9}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v1

    .line 5667
    :goto_66
    if-ge v7, v1, :cond_87

    .line 5668
    invoke-virtual {v9, v7}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 5667
    add-int/lit8 v7, v7, 0x1

    goto :goto_66

    :cond_72
    move v3, v7

    .line 1897
    goto :goto_d

    :cond_74
    move v0, v7

    .line 1900
    goto :goto_1c

    .line 1902
    :cond_76
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1903
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v0

    sub-int/2addr v0, v2

    move-object v2, v1

    move v1, v0

    goto :goto_1f

    .line 5670
    :cond_87
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 5671
    iput-boolean v8, v9, Landroid/support/v4/widget/DrawerLayout;->j:Z

    .line 1918
    :cond_8c
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)I
    .registers 5

    .prologue
    .line 1953
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1954
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    const/4 v1, 0x0

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1957
    :goto_17
    return v0

    .line 1956
    :cond_18
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v0

    .line 1957
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v0, v1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_17
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 1820
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Landroid/support/v4/widget/ag;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1821
    return-void
.end method

.method public final a(I)V
    .registers 11

    .prologue
    const/16 v8, 0x20

    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1833
    iget-object v4, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    .line 2475
    iget-object v5, v1, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    .line 2683
    iget-object v1, v4, Landroid/support/v4/widget/DrawerLayout;->g:Landroid/support/v4/widget/eg;

    .line 3421
    iget v1, v1, Landroid/support/v4/widget/eg;->m:I

    .line 2684
    iget-object v6, v4, Landroid/support/v4/widget/DrawerLayout;->h:Landroid/support/v4/widget/eg;

    .line 4421
    iget v6, v6, Landroid/support/v4/widget/eg;->m:I

    .line 2687
    if-eq v1, v3, :cond_17

    if-ne v6, v3, :cond_57

    :cond_17
    move v1, v3

    .line 2695
    :goto_18
    if-eqz v5, :cond_50

    if-nez p1, :cond_50

    .line 2696
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 2697
    iget v6, v0, Landroid/support/v4/widget/ad;->b:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_5f

    .line 4714
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 4715
    iget-boolean v3, v0, Landroid/support/v4/widget/ad;->d:Z

    if-eqz v3, :cond_50

    .line 4716
    iput-boolean v2, v0, Landroid/support/v4/widget/ad;->d:Z

    .line 4717
    iget-object v0, v4, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    if-eqz v0, :cond_3e

    .line 4718
    iget-object v0, v4, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    invoke-interface {v0}, Landroid/support/v4/widget/ac;->b()V

    .line 4721
    :cond_3e
    invoke-virtual {v4, v5, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;Z)V

    .line 4726
    invoke-virtual {v4}, Landroid/support/v4/widget/DrawerLayout;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 4727
    invoke-virtual {v4}, Landroid/support/v4/widget/DrawerLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 4728
    if-eqz v0, :cond_50

    .line 4729
    invoke-virtual {v0, v8}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 2704
    :cond_50
    :goto_50
    iget v0, v4, Landroid/support/v4/widget/DrawerLayout;->i:I

    if-eq v1, v0, :cond_56

    .line 2705
    iput v1, v4, Landroid/support/v4/widget/DrawerLayout;->i:I

    .line 1834
    :cond_56
    return-void

    .line 2689
    :cond_57
    if-eq v1, v0, :cond_5b

    if-ne v6, v0, :cond_5d

    :cond_5b
    move v1, v0

    .line 2690
    goto :goto_18

    :cond_5d
    move v1, v2

    .line 2692
    goto :goto_18

    .line 2699
    :cond_5f
    iget v0, v0, Landroid/support/v4/widget/ad;->b:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_50

    .line 4736
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 4737
    iget-boolean v2, v0, Landroid/support/v4/widget/ad;->d:Z

    if-nez v2, :cond_50

    .line 4738
    iput-boolean v3, v0, Landroid/support/v4/widget/ad;->d:Z

    .line 4739
    iget-object v0, v4, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    if-eqz v0, :cond_7c

    .line 4740
    iget-object v0, v4, Landroid/support/v4/widget/DrawerLayout;->k:Landroid/support/v4/widget/ac;

    invoke-interface {v0}, Landroid/support/v4/widget/ac;->a()V

    .line 4743
    :cond_7c
    invoke-virtual {v4, v5, v3}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;Z)V

    .line 4746
    invoke-virtual {v4}, Landroid/support/v4/widget/DrawerLayout;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_88

    .line 4747
    invoke-virtual {v4, v8}, Landroid/support/v4/widget/DrawerLayout;->sendAccessibilityEvent(I)V

    .line 4750
    :cond_88
    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    goto :goto_50
.end method

.method public final a(II)V
    .registers 5

    .prologue
    .line 1935
    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1c

    .line 1936
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1941
    :goto_c
    if-eqz v0, :cond_1b

    iget-object v1, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_1b

    .line 1942
    iget-object v1, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    invoke-virtual {v1, v0, p2}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;I)V

    .line 1944
    :cond_1b
    return-void

    .line 1938
    :cond_1c
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    goto :goto_c
.end method

.method public final a(Landroid/view/View;F)V
    .registers 9

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 1873
    invoke-static {p1}, Landroid/support/v4/widget/DrawerLayout;->b(Landroid/view/View;)F

    move-result v1

    .line 1874
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 1877
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v3, 0x3

    invoke-virtual {v0, p1, v3}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 1878
    cmpl-float v0, p2, v4

    if-gtz v0, :cond_20

    cmpl-float v0, p2, v4

    if-nez v0, :cond_30

    cmpl-float v0, v1, v5

    if-lez v0, :cond_30

    :cond_20
    const/4 v0, 0x0

    .line 1884
    :cond_21
    :goto_21
    iget-object v1, p0, Landroid/support/v4/widget/ag;->b:Landroid/support/v4/widget/eg;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/widget/eg;->a(II)Z

    .line 1885
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1886
    return-void

    .line 1878
    :cond_30
    neg-int v0, v2

    goto :goto_21

    .line 1880
    :cond_32
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v0

    .line 1881
    cmpg-float v3, p2, v4

    if-ltz v3, :cond_44

    cmpl-float v3, p2, v4

    if-nez v3, :cond_21

    cmpl-float v1, v1, v5

    if-lez v1, :cond_21

    :cond_44
    sub-int/2addr v0, v2

    goto :goto_21
.end method

.method public final a(Landroid/view/View;)Z
    .registers 4

    .prologue
    .line 1827
    invoke-static {p1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    iget v1, p0, Landroid/support/v4/widget/ag;->a:I

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public final b(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1948
    invoke-static {p1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method final b()V
    .registers 3

    .prologue
    const/4 v0, 0x3

    .line 1862
    iget v1, p0, Landroid/support/v4/widget/ag;->a:I

    if-ne v1, v0, :cond_6

    const/4 v0, 0x5

    .line 1863
    :cond_6
    iget-object v1, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1864
    if-eqz v0, :cond_13

    .line 1865
    iget-object v1, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->e(Landroid/view/View;)V

    .line 1867
    :cond_13
    return-void
.end method

.method public final b(Landroid/view/View;I)V
    .registers 6

    .prologue
    .line 1839
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 1842
    iget-object v1, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v2, 0x3

    invoke-virtual {v1, p1, v2}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 1843
    add-int v1, v0, p2

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 1848
    :goto_13
    iget-object v1, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/widget/DrawerLayout;->a(Landroid/view/View;F)V

    .line 1849
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_33

    const/4 v0, 0x4

    :goto_1e
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1850
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 1851
    return-void

    .line 1845
    :cond_27
    iget-object v1, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayout;->getWidth()I

    move-result v1

    .line 1846
    sub-int/2addr v1, p2

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_13

    .line 1849
    :cond_33
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public final c(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1963
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method

.method public final c()V
    .registers 5

    .prologue
    .line 1890
    iget-object v0, p0, Landroid/support/v4/widget/ag;->c:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Landroid/support/v4/widget/ag;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0xa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/widget/DrawerLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1891
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 1855
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/ad;

    .line 1856
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/widget/ad;->c:Z

    .line 1858
    invoke-virtual {p0}, Landroid/support/v4/widget/ag;->b()V

    .line 1859
    return-void
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 1929
    const/4 v0, 0x0

    return v0
.end method
