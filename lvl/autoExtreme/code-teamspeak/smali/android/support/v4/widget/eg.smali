.class public final Landroid/support/v4/widget/eg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final L:Landroid/view/animation/Interpolator;

.field public static final a:I = -0x1

.field public static final b:I = 0x0

.field public static final c:I = 0x1

.field public static final d:I = 0x2

.field public static final e:I = 0x1

.field public static final f:I = 0x2

.field public static final g:I = 0x4

.field public static final h:I = 0x8

.field public static final i:I = 0xf

.field public static final j:I = 0x1

.field public static final k:I = 0x2

.field public static final l:I = 0x3

.field private static final w:Ljava/lang/String; = "ViewDragHelper"

.field private static final x:I = 0x14

.field private static final y:I = 0x100

.field private static final z:I = 0x258


# instance fields
.field private A:I

.field private B:[I

.field private C:[I

.field private D:[I

.field private E:I

.field private F:Landroid/view/VelocityTracker;

.field private G:F

.field private H:Landroid/support/v4/widget/ca;

.field private final I:Landroid/support/v4/widget/ej;

.field private J:Z

.field private final K:Landroid/view/ViewGroup;

.field private final M:Ljava/lang/Runnable;

.field public m:I

.field n:I

.field o:[F

.field p:[F

.field q:[F

.field r:[F

.field s:F

.field t:I

.field u:I

.field v:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 326
    new-instance v0, Landroid/support/v4/widget/eh;

    invoke-direct {v0}, Landroid/support/v4/widget/eh;-><init>()V

    sput-object v0, Landroid/support/v4/widget/eg;->L:Landroid/view/animation/Interpolator;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)V
    .registers 7

    .prologue
    .line 373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/eg;->A:I

    .line 333
    new-instance v0, Landroid/support/v4/widget/ei;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/ei;-><init>(Landroid/support/v4/widget/eg;)V

    iput-object v0, p0, Landroid/support/v4/widget/eg;->M:Ljava/lang/Runnable;

    .line 374
    if-nez p2, :cond_17

    .line 375
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parent view may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 377
    :cond_17
    if-nez p3, :cond_21

    .line 378
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Callback may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_21
    iput-object p2, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    .line 382
    iput-object p3, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    .line 384
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 385
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 386
    const/high16 v2, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/widget/eg;->t:I

    .line 388
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/eg;->n:I

    .line 389
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v4/widget/eg;->G:F

    .line 390
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Landroid/support/v4/widget/eg;->s:F

    .line 391
    sget-object v0, Landroid/support/v4/widget/eg;->L:Landroid/view/animation/Interpolator;

    invoke-static {p1, v0}, Landroid/support/v4/widget/ca;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    .line 392
    return-void
.end method

.method private static a(FFF)F
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 674
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 675
    cmpg-float v2, v1, p1

    if-gez v2, :cond_b

    move p2, v0

    .line 677
    :cond_a
    :goto_a
    return p2

    .line 676
    :cond_b
    cmpl-float v1, v1, p2

    if-lez v1, :cond_15

    cmpl-float v0, p0, v0

    if-gtz v0, :cond_a

    neg-float p2, p2

    goto :goto_a

    :cond_15
    move p2, p0

    .line 677
    goto :goto_a
.end method

.method private a(III)I
    .registers 13

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 625
    if-nez p1, :cond_6

    .line 626
    const/4 v0, 0x0

    .line 643
    :goto_5
    return v0

    .line 629
    :cond_6
    iget-object v0, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    .line 630
    div-int/lit8 v1, v0, 0x2

    .line 631
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-static {v8, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 632
    int-to-float v2, v1

    int-to-float v1, v1

    .line 3681
    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v0, v3

    .line 3682
    float-to-double v4, v0

    const-wide v6, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v4, v6

    double-to-float v0, v4

    .line 3683
    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 632
    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    .line 636
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 637
    if-lez v1, :cond_4b

    .line 638
    const/high16 v2, 0x447a0000    # 1000.0f

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 643
    :goto_44
    const/16 v1, 0x258

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_5

    .line 640
    :cond_4b
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 641
    add-float/2addr v0, v8

    const/high16 v1, 0x43800000    # 256.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_44
.end method

.method private a(Landroid/view/View;IIII)I
    .registers 14

    .prologue
    .line 604
    iget v0, p0, Landroid/support/v4/widget/eg;->s:F

    float-to-int v0, v0

    iget v1, p0, Landroid/support/v4/widget/eg;->G:F

    float-to-int v1, v1

    invoke-static {p4, v0, v1}, Landroid/support/v4/widget/eg;->b(III)I

    move-result v2

    .line 605
    iget v0, p0, Landroid/support/v4/widget/eg;->s:F

    float-to-int v0, v0

    iget v1, p0, Landroid/support/v4/widget/eg;->G:F

    float-to-int v1, v1

    invoke-static {p5, v0, v1}, Landroid/support/v4/widget/eg;->b(III)I

    move-result v3

    .line 606
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 607
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 608
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 609
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 610
    add-int v6, v1, v5

    .line 611
    add-int v7, v0, v4

    .line 613
    if-eqz v2, :cond_49

    int-to-float v0, v1

    int-to-float v1, v6

    div-float/2addr v0, v1

    move v1, v0

    .line 615
    :goto_2e
    if-eqz v3, :cond_4e

    int-to-float v0, v5

    int-to-float v4, v6

    div-float/2addr v0, v4

    .line 618
    :goto_33
    iget-object v4, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v4, p1}, Landroid/support/v4/widget/ej;->b(Landroid/view/View;)I

    move-result v4

    invoke-direct {p0, p2, v2, v4}, Landroid/support/v4/widget/eg;->a(III)I

    move-result v2

    .line 619
    const/4 v4, 0x0

    invoke-direct {p0, p3, v3, v4}, Landroid/support/v4/widget/eg;->a(III)I

    move-result v3

    .line 621
    int-to-float v2, v2

    mul-float/2addr v1, v2

    int-to-float v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    .line 613
    :cond_49
    int-to-float v0, v0

    int-to-float v1, v7

    div-float/2addr v0, v1

    move v1, v0

    goto :goto_2e

    .line 615
    :cond_4e
    int-to-float v0, v4

    int-to-float v4, v7

    div-float/2addr v0, v4

    goto :goto_33
.end method

.method public static a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
    .registers 6

    .prologue
    .line 360
    invoke-static {p0, p2}, Landroid/support/v4/widget/eg;->a(Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;

    move-result-object v0

    .line 361
    iget v1, v0, Landroid/support/v4/widget/eg;->n:I

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    div-float/2addr v2, p1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/support/v4/widget/eg;->n:I

    .line 362
    return-object v0
.end method

.method public static a(Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
    .registers 4

    .prologue
    .line 347
    new-instance v0, Landroid/support/v4/widget/eg;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Landroid/support/v4/widget/eg;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)V

    return-object v0
.end method

.method private a(F)V
    .registers 2

    .prologue
    .line 401
    iput p1, p0, Landroid/support/v4/widget/eg;->s:F

    .line 402
    return-void
.end method

.method private a(FFI)V
    .registers 15

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 835
    .line 4805
    iget-object v2, p0, Landroid/support/v4/widget/eg;->o:[F

    if-eqz v2, :cond_b

    iget-object v2, p0, Landroid/support/v4/widget/eg;->o:[F

    array-length v2, v2

    if-gt v2, p3, :cond_71

    .line 4806
    :cond_b
    add-int/lit8 v2, p3, 0x1

    new-array v2, v2, [F

    .line 4807
    add-int/lit8 v3, p3, 0x1

    new-array v3, v3, [F

    .line 4808
    add-int/lit8 v4, p3, 0x1

    new-array v4, v4, [F

    .line 4809
    add-int/lit8 v5, p3, 0x1

    new-array v5, v5, [F

    .line 4810
    add-int/lit8 v6, p3, 0x1

    new-array v6, v6, [I

    .line 4811
    add-int/lit8 v7, p3, 0x1

    new-array v7, v7, [I

    .line 4812
    add-int/lit8 v8, p3, 0x1

    new-array v8, v8, [I

    .line 4814
    iget-object v9, p0, Landroid/support/v4/widget/eg;->o:[F

    if-eqz v9, :cond_63

    .line 4815
    iget-object v9, p0, Landroid/support/v4/widget/eg;->o:[F

    iget-object v10, p0, Landroid/support/v4/widget/eg;->o:[F

    array-length v10, v10

    invoke-static {v9, v0, v2, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4816
    iget-object v9, p0, Landroid/support/v4/widget/eg;->p:[F

    iget-object v10, p0, Landroid/support/v4/widget/eg;->p:[F

    array-length v10, v10

    invoke-static {v9, v0, v3, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4817
    iget-object v9, p0, Landroid/support/v4/widget/eg;->q:[F

    iget-object v10, p0, Landroid/support/v4/widget/eg;->q:[F

    array-length v10, v10

    invoke-static {v9, v0, v4, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4818
    iget-object v9, p0, Landroid/support/v4/widget/eg;->r:[F

    iget-object v10, p0, Landroid/support/v4/widget/eg;->r:[F

    array-length v10, v10

    invoke-static {v9, v0, v5, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4819
    iget-object v9, p0, Landroid/support/v4/widget/eg;->B:[I

    iget-object v10, p0, Landroid/support/v4/widget/eg;->B:[I

    array-length v10, v10

    invoke-static {v9, v0, v6, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4820
    iget-object v9, p0, Landroid/support/v4/widget/eg;->C:[I

    iget-object v10, p0, Landroid/support/v4/widget/eg;->C:[I

    array-length v10, v10

    invoke-static {v9, v0, v7, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4821
    iget-object v9, p0, Landroid/support/v4/widget/eg;->D:[I

    iget-object v10, p0, Landroid/support/v4/widget/eg;->D:[I

    array-length v10, v10

    invoke-static {v9, v0, v8, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4824
    :cond_63
    iput-object v2, p0, Landroid/support/v4/widget/eg;->o:[F

    .line 4825
    iput-object v3, p0, Landroid/support/v4/widget/eg;->p:[F

    .line 4826
    iput-object v4, p0, Landroid/support/v4/widget/eg;->q:[F

    .line 4827
    iput-object v5, p0, Landroid/support/v4/widget/eg;->r:[F

    .line 4828
    iput-object v6, p0, Landroid/support/v4/widget/eg;->B:[I

    .line 4829
    iput-object v7, p0, Landroid/support/v4/widget/eg;->C:[I

    .line 4830
    iput-object v8, p0, Landroid/support/v4/widget/eg;->D:[I

    .line 836
    :cond_71
    iget-object v2, p0, Landroid/support/v4/widget/eg;->o:[F

    iget-object v3, p0, Landroid/support/v4/widget/eg;->q:[F

    aput p1, v3, p3

    aput p1, v2, p3

    .line 837
    iget-object v2, p0, Landroid/support/v4/widget/eg;->p:[F

    iget-object v3, p0, Landroid/support/v4/widget/eg;->r:[F

    aput p2, v3, p3

    aput p2, v2, p3

    .line 838
    iget-object v2, p0, Landroid/support/v4/widget/eg;->B:[I

    float-to-int v3, p1

    float-to-int v4, p2

    .line 5475
    iget-object v5, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getLeft()I

    move-result v5

    iget v6, p0, Landroid/support/v4/widget/eg;->t:I

    add-int/2addr v5, v6

    if-ge v3, v5, :cond_91

    move v0, v1

    .line 5476
    :cond_91
    iget-object v5, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getTop()I

    move-result v5

    iget v6, p0, Landroid/support/v4/widget/eg;->t:I

    add-int/2addr v5, v6

    if-ge v4, v5, :cond_9e

    or-int/lit8 v0, v0, 0x4

    .line 5477
    :cond_9e
    iget-object v5, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getRight()I

    move-result v5

    iget v6, p0, Landroid/support/v4/widget/eg;->t:I

    sub-int/2addr v5, v6

    if-le v3, v5, :cond_ab

    or-int/lit8 v0, v0, 0x2

    .line 5478
    :cond_ab
    iget-object v3, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    iget v5, p0, Landroid/support/v4/widget/eg;->t:I

    sub-int/2addr v3, v5

    if-le v4, v3, :cond_b8

    or-int/lit8 v0, v0, 0x8

    .line 838
    :cond_b8
    aput v0, v2, p3

    .line 839
    iget v0, p0, Landroid/support/v4/widget/eg;->E:I

    shl-int/2addr v1, p3

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/eg;->E:I

    .line 840
    return-void
.end method

.method private a(FFII)Z
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 1251
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1252
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 1254
    iget-object v3, p0, Landroid/support/v4/widget/eg;->B:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-ne v3, p4, :cond_31

    iget v3, p0, Landroid/support/v4/widget/eg;->u:I

    and-int/2addr v3, p4

    if-eqz v3, :cond_31

    iget-object v3, p0, Landroid/support/v4/widget/eg;->D:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-eq v3, p4, :cond_31

    iget-object v3, p0, Landroid/support/v4/widget/eg;->C:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-eq v3, p4, :cond_31

    iget v3, p0, Landroid/support/v4/widget/eg;->n:I

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_32

    iget v3, p0, Landroid/support/v4/widget/eg;->n:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_32

    .line 1264
    :cond_31
    :goto_31
    return v0

    :cond_32
    iget-object v2, p0, Landroid/support/v4/widget/eg;->C:[I

    aget v2, v2, p3

    and-int/2addr v2, p4

    if-nez v2, :cond_31

    iget v2, p0, Landroid/support/v4/widget/eg;->n:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_31

    const/4 v0, 0x1

    goto :goto_31
.end method

.method private a(IIII)Z
    .registers 19

    .prologue
    .line 584
    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 585
    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    .line 586
    sub-int v5, p1, v3

    .line 587
    sub-int v6, p2, v4

    .line 589
    if-nez v5, :cond_1f

    if-nez v6, :cond_1f

    .line 591
    iget-object v1, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->g()V

    .line 592
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/eg;->b(I)V

    .line 593
    const/4 v1, 0x0

    .line 600
    :goto_1e
    return v1

    .line 596
    :cond_1f
    iget-object v7, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    .line 2604
    iget v1, p0, Landroid/support/v4/widget/eg;->s:F

    float-to-int v1, v1

    iget v2, p0, Landroid/support/v4/widget/eg;->G:F

    float-to-int v2, v2

    move/from16 v0, p3

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/eg;->b(III)I

    move-result v8

    .line 2605
    iget v1, p0, Landroid/support/v4/widget/eg;->s:F

    float-to-int v1, v1

    iget v2, p0, Landroid/support/v4/widget/eg;->G:F

    float-to-int v2, v2

    move/from16 v0, p4

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/eg;->b(III)I

    move-result v9

    .line 2606
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2607
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v10

    .line 2608
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 2609
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v11

    .line 2610
    add-int v12, v2, v11

    .line 2611
    add-int v13, v1, v10

    .line 2613
    if-eqz v8, :cond_7c

    int-to-float v1, v2

    int-to-float v2, v12

    div-float/2addr v1, v2

    move v2, v1

    .line 2615
    :goto_53
    if-eqz v9, :cond_81

    int-to-float v1, v11

    int-to-float v10, v12

    div-float/2addr v1, v10

    .line 2618
    :goto_58
    iget-object v10, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v10, v7}, Landroid/support/v4/widget/ej;->b(Landroid/view/View;)I

    move-result v7

    invoke-direct {p0, v5, v8, v7}, Landroid/support/v4/widget/eg;->a(III)I

    move-result v7

    .line 2619
    const/4 v8, 0x0

    invoke-direct {p0, v6, v9, v8}, Landroid/support/v4/widget/eg;->a(III)I

    move-result v8

    .line 2621
    int-to-float v7, v7

    mul-float/2addr v2, v7

    int-to-float v7, v8

    mul-float/2addr v1, v7

    add-float/2addr v1, v2

    float-to-int v7, v1

    .line 597
    iget-object v2, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    .line 3369
    iget-object v1, v2, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v2, v2, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface/range {v1 .. v7}, Landroid/support/v4/widget/cb;->a(Ljava/lang/Object;IIIII)V

    .line 599
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/eg;->b(I)V

    .line 600
    const/4 v1, 0x1

    goto :goto_1e

    .line 2613
    :cond_7c
    int-to-float v1, v1

    int-to-float v2, v13

    div-float/2addr v1, v2

    move v2, v1

    goto :goto_53

    .line 2615
    :cond_81
    int-to-float v1, v10

    int-to-float v10, v13

    div-float/2addr v1, v10

    goto :goto_58
.end method

.method private a(Landroid/view/View;F)Z
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1278
    if-nez p1, :cond_5

    .line 1291
    :cond_4
    :goto_4
    return v0

    .line 1281
    :cond_5
    iget-object v2, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v2, p1}, Landroid/support/v4/widget/ej;->b(Landroid/view/View;)I

    move-result v2

    if-lez v2, :cond_1d

    move v2, v1

    .line 1286
    :goto_e
    if-eqz v2, :cond_4

    .line 1287
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Landroid/support/v4/widget/eg;->n:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    move v0, v1

    goto :goto_4

    :cond_1d
    move v2, v0

    .line 1281
    goto :goto_e
.end method

.method private static b(F)F
    .registers 5

    .prologue
    .line 681
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v0, p0, v0

    .line 682
    float-to-double v0, v0

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 683
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private static b(III)I
    .registers 4

    .prologue
    .line 657
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 658
    if-ge v0, p1, :cond_8

    const/4 p2, 0x0

    .line 660
    :cond_7
    :goto_7
    return p2

    .line 659
    :cond_8
    if-le v0, p2, :cond_e

    if-gtz p0, :cond_7

    neg-int p2, p2

    goto :goto_7

    :cond_e
    move p2, p0

    .line 660
    goto :goto_7
.end method

.method private b(FFI)V
    .registers 7

    .prologue
    const/4 v0, 0x1

    .line 1230
    const/4 v1, 0x0

    .line 1231
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/widget/eg;->a(FFII)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 1234
    :goto_8
    const/4 v1, 0x4

    invoke-direct {p0, p2, p1, p3, v1}, Landroid/support/v4/widget/eg;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1235
    or-int/lit8 v0, v0, 0x4

    .line 1237
    :cond_11
    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, p3, v1}, Landroid/support/v4/widget/eg;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 1238
    or-int/lit8 v0, v0, 0x2

    .line 1240
    :cond_1a
    const/16 v1, 0x8

    invoke-direct {p0, p2, p1, p3, v1}, Landroid/support/v4/widget/eg;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 1241
    or-int/lit8 v0, v0, 0x8

    .line 1244
    :cond_24
    if-eqz v0, :cond_32

    .line 1245
    iget-object v1, p0, Landroid/support/v4/widget/eg;->C:[I

    aget v2, v1, p3

    or-int/2addr v2, v0

    aput v2, v1, p3

    .line 1246
    iget-object v1, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v1, v0, p3}, Landroid/support/v4/widget/ej;->a(II)V

    .line 1248
    :cond_32
    return-void

    :cond_33
    move v0, v1

    goto :goto_8
.end method

.method private b(IIII)V
    .registers 14

    .prologue
    .line 697
    iget-boolean v0, p0, Landroid/support/v4/widget/eg;->J:Z

    if-nez v0, :cond_c

    .line 698
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot flingCapturedView outside of a call to Callback#onViewReleased"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 702
    :cond_c
    iget-object v0, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    iget v4, p0, Landroid/support/v4/widget/eg;->A:I

    invoke-static {v3, v4}, Landroid/support/v4/view/cs;->a(Landroid/view/VelocityTracker;I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    iget v5, p0, Landroid/support/v4/widget/eg;->A:I

    invoke-static {v4, v5}, Landroid/support/v4/view/cs;->b(Landroid/view/VelocityTracker;I)F

    move-result v4

    float-to-int v4, v4

    move v5, p1

    move v6, p3

    move v7, p2

    move v8, p4

    invoke-virtual/range {v0 .. v8}, Landroid/support/v4/widget/ca;->a(IIIIIIII)V

    .line 707
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/eg;->b(I)V

    .line 708
    return-void
.end method

.method private b(Landroid/view/View;I)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 891
    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    if-ne p1, v1, :cond_a

    iget v1, p0, Landroid/support/v4/widget/eg;->A:I

    if-ne v1, p2, :cond_a

    .line 900
    :goto_9
    return v0

    .line 895
    :cond_a
    if-eqz p1, :cond_1a

    iget-object v1, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v1, p1}, Landroid/support/v4/widget/ej;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 896
    iput p2, p0, Landroid/support/v4/widget/eg;->A:I

    .line 897
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;I)V

    goto :goto_9

    .line 900
    :cond_1a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public static b(Landroid/view/View;II)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1443
    if-nez p0, :cond_4

    .line 1446
    :cond_3
    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    if-lt p1, v1, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v1

    if-ge p1, v1, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    if-lt p2, v1, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-ge p2, v1, :cond_3

    const/4 v0, 0x1

    goto :goto_3
.end method

.method private b(Landroid/view/View;IIII)Z
    .registers 16

    .prologue
    .line 916
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_5b

    move-object v6, p1

    .line 917
    check-cast v6, Landroid/view/ViewGroup;

    .line 918
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v8

    .line 919
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v9

    .line 920
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 922
    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_16
    if-ltz v7, :cond_5b

    .line 925
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 926
    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt v0, v2, :cond_57

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge v0, v2, :cond_57

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt v0, v2, :cond_57

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge v0, v2, :cond_57

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int v4, v0, v2

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int v5, v0, v2

    move-object v0, p0

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;IIII)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 930
    const/4 v0, 0x1

    .line 935
    :goto_56
    return v0

    .line 922
    :cond_57
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_16

    .line 935
    :cond_5b
    neg-int v0, p2

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_69

    neg-int v0, p3

    invoke-static {p1, v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_6b

    :cond_69
    const/4 v0, 0x1

    goto :goto_56

    :cond_6b
    const/4 v0, 0x0

    goto :goto_56
.end method

.method private c(F)V
    .registers 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 766
    iput-boolean v3, p0, Landroid/support/v4/widget/eg;->J:Z

    .line 767
    iget-object v0, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/widget/ej;->a(Landroid/view/View;F)V

    .line 768
    iput-boolean v2, p0, Landroid/support/v4/widget/eg;->J:Z

    .line 770
    iget v0, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v0, v3, :cond_14

    .line 772
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/eg;->b(I)V

    .line 774
    :cond_14
    return-void
.end method

.method private c(I)V
    .registers 2

    .prologue
    .line 437
    iput p1, p0, Landroid/support/v4/widget/eg;->u:I

    .line 438
    return-void
.end method

.method private c(III)V
    .registers 8

    .prologue
    .line 1399
    .line 1401
    iget-object v0, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1402
    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 1403
    if-eqz p2, :cond_1d

    .line 1404
    iget-object v2, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v3, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v2, v3, p1}, Landroid/support/v4/widget/ej;->a(Landroid/view/View;I)I

    move-result p1

    .line 1405
    iget-object v2, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    sub-int v0, p1, v0

    invoke-virtual {v2, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1407
    :cond_1d
    if-eqz p3, :cond_2d

    .line 1408
    iget-object v0, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v2, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/ej;->c(Landroid/view/View;)I

    move-result v0

    .line 1409
    iget-object v2, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    sub-int/2addr v0, v1

    invoke-virtual {v2, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1412
    :cond_2d
    if-nez p2, :cond_31

    if-eqz p3, :cond_38

    .line 1415
    :cond_31
    iget-object v0, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/widget/ej;->b(Landroid/view/View;I)V

    .line 1418
    :cond_38
    return-void
.end method

.method private c(Landroid/view/MotionEvent;)V
    .registers 8

    .prologue
    .line 843
    invoke-static {p1}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;)I

    move-result v1

    .line 844
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_1e

    .line 845
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 846
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 847
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 848
    iget-object v5, p0, Landroid/support/v4/widget/eg;->q:[F

    aput v3, v5, v2

    .line 849
    iget-object v3, p0, Landroid/support/v4/widget/eg;->r:[F

    aput v4, v3, v2

    .line 844
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 851
    :cond_1e
    return-void
.end method

.method private c(II)Z
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1334
    invoke-virtual {p0, p2}, Landroid/support/v4/widget/eg;->a(I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1351
    :cond_8
    :goto_8
    return v2

    .line 1338
    :cond_9
    and-int/lit8 v0, p1, 0x1

    if-ne v0, v1, :cond_3b

    move v3, v1

    .line 1339
    :goto_e
    and-int/lit8 v0, p1, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_3d

    move v0, v1

    .line 1341
    :goto_14
    iget-object v4, p0, Landroid/support/v4/widget/eg;->q:[F

    aget v4, v4, p2

    iget-object v5, p0, Landroid/support/v4/widget/eg;->o:[F

    aget v5, v5, p2

    sub-float/2addr v4, v5

    .line 1342
    iget-object v5, p0, Landroid/support/v4/widget/eg;->r:[F

    aget v5, v5, p2

    iget-object v6, p0, Landroid/support/v4/widget/eg;->p:[F

    aget v6, v6, p2

    sub-float/2addr v5, v6

    .line 1344
    if-eqz v3, :cond_3f

    if-eqz v0, :cond_3f

    .line 1345
    mul-float v0, v4, v4

    mul-float v3, v5, v5

    add-float/2addr v0, v3

    iget v3, p0, Landroid/support/v4/widget/eg;->n:I

    iget v4, p0, Landroid/support/v4/widget/eg;->n:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    move v2, v1

    goto :goto_8

    :cond_3b
    move v3, v2

    .line 1338
    goto :goto_e

    :cond_3d
    move v0, v2

    .line 1339
    goto :goto_14

    .line 1346
    :cond_3f
    if-eqz v3, :cond_4e

    .line 1347
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, Landroid/support/v4/widget/eg;->n:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    move v2, v1

    goto :goto_8

    .line 1348
    :cond_4e
    if-eqz v0, :cond_8

    .line 1349
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, Landroid/support/v4/widget/eg;->n:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    move v2, v1

    goto :goto_8
.end method

.method private d()F
    .registers 2

    .prologue
    .line 412
    iget v0, p0, Landroid/support/v4/widget/eg;->s:F

    return v0
.end method

.method private d(I)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 791
    iget-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    if-nez v0, :cond_7

    .line 802
    :goto_6
    return-void

    .line 794
    :cond_7
    iget-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    aput v1, v0, p1

    .line 795
    iget-object v0, p0, Landroid/support/v4/widget/eg;->p:[F

    aput v1, v0, p1

    .line 796
    iget-object v0, p0, Landroid/support/v4/widget/eg;->q:[F

    aput v1, v0, p1

    .line 797
    iget-object v0, p0, Landroid/support/v4/widget/eg;->r:[F

    aput v1, v0, p1

    .line 798
    iget-object v0, p0, Landroid/support/v4/widget/eg;->B:[I

    aput v2, v0, p1

    .line 799
    iget-object v0, p0, Landroid/support/v4/widget/eg;->C:[I

    aput v2, v0, p1

    .line 800
    iget-object v0, p0, Landroid/support/v4/widget/eg;->D:[I

    aput v2, v0, p1

    .line 801
    iget v0, p0, Landroid/support/v4/widget/eg;->E:I

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/eg;->E:I

    goto :goto_6
.end method

.method private d(II)Z
    .registers 4

    .prologue
    .line 1384
    invoke-virtual {p0, p2}, Landroid/support/v4/widget/eg;->a(I)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Landroid/support/v4/widget/eg;->B:[I

    aget v0, v0, p2

    and-int/2addr v0, p1

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private e()I
    .registers 2

    .prologue
    .line 421
    iget v0, p0, Landroid/support/v4/widget/eg;->m:I

    return v0
.end method

.method private e(I)V
    .registers 12

    .prologue
    const/4 v9, 0x0

    .line 805
    iget-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    array-length v0, v0

    if-gt v0, p1, :cond_70

    .line 806
    :cond_a
    add-int/lit8 v0, p1, 0x1

    new-array v0, v0, [F

    .line 807
    add-int/lit8 v1, p1, 0x1

    new-array v1, v1, [F

    .line 808
    add-int/lit8 v2, p1, 0x1

    new-array v2, v2, [F

    .line 809
    add-int/lit8 v3, p1, 0x1

    new-array v3, v3, [F

    .line 810
    add-int/lit8 v4, p1, 0x1

    new-array v4, v4, [I

    .line 811
    add-int/lit8 v5, p1, 0x1

    new-array v5, v5, [I

    .line 812
    add-int/lit8 v6, p1, 0x1

    new-array v6, v6, [I

    .line 814
    iget-object v7, p0, Landroid/support/v4/widget/eg;->o:[F

    if-eqz v7, :cond_62

    .line 815
    iget-object v7, p0, Landroid/support/v4/widget/eg;->o:[F

    iget-object v8, p0, Landroid/support/v4/widget/eg;->o:[F

    array-length v8, v8

    invoke-static {v7, v9, v0, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 816
    iget-object v7, p0, Landroid/support/v4/widget/eg;->p:[F

    iget-object v8, p0, Landroid/support/v4/widget/eg;->p:[F

    array-length v8, v8

    invoke-static {v7, v9, v1, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817
    iget-object v7, p0, Landroid/support/v4/widget/eg;->q:[F

    iget-object v8, p0, Landroid/support/v4/widget/eg;->q:[F

    array-length v8, v8

    invoke-static {v7, v9, v2, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 818
    iget-object v7, p0, Landroid/support/v4/widget/eg;->r:[F

    iget-object v8, p0, Landroid/support/v4/widget/eg;->r:[F

    array-length v8, v8

    invoke-static {v7, v9, v3, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 819
    iget-object v7, p0, Landroid/support/v4/widget/eg;->B:[I

    iget-object v8, p0, Landroid/support/v4/widget/eg;->B:[I

    array-length v8, v8

    invoke-static {v7, v9, v4, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 820
    iget-object v7, p0, Landroid/support/v4/widget/eg;->C:[I

    iget-object v8, p0, Landroid/support/v4/widget/eg;->C:[I

    array-length v8, v8

    invoke-static {v7, v9, v5, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 821
    iget-object v7, p0, Landroid/support/v4/widget/eg;->D:[I

    iget-object v8, p0, Landroid/support/v4/widget/eg;->D:[I

    array-length v8, v8

    invoke-static {v7, v9, v6, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 824
    :cond_62
    iput-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    .line 825
    iput-object v1, p0, Landroid/support/v4/widget/eg;->p:[F

    .line 826
    iput-object v2, p0, Landroid/support/v4/widget/eg;->q:[F

    .line 827
    iput-object v3, p0, Landroid/support/v4/widget/eg;->r:[F

    .line 828
    iput-object v4, p0, Landroid/support/v4/widget/eg;->B:[I

    .line 829
    iput-object v5, p0, Landroid/support/v4/widget/eg;->C:[I

    .line 830
    iput-object v6, p0, Landroid/support/v4/widget/eg;->D:[I

    .line 832
    :cond_70
    return-void
.end method

.method private e(II)Z
    .registers 4

    .prologue
    .line 1430
    iget-object v0, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-static {v0, p1, p2}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;II)Z

    move-result v0

    return v0
.end method

.method private f()I
    .registers 2

    .prologue
    .line 448
    iget v0, p0, Landroid/support/v4/widget/eg;->t:I

    return v0
.end method

.method private f(II)I
    .registers 6

    .prologue
    .line 1473
    const/4 v0, 0x0

    .line 1475
    iget-object v1, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLeft()I

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/eg;->t:I

    add-int/2addr v1, v2

    if-ge p1, v1, :cond_d

    const/4 v0, 0x1

    .line 1476
    :cond_d
    iget-object v1, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getTop()I

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/eg;->t:I

    add-int/2addr v1, v2

    if-ge p2, v1, :cond_1a

    or-int/lit8 v0, v0, 0x4

    .line 1477
    :cond_1a
    iget-object v1, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getRight()I

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/eg;->t:I

    sub-int/2addr v1, v2

    if-le p1, v1, :cond_27

    or-int/lit8 v0, v0, 0x2

    .line 1478
    :cond_27
    iget-object v1, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getBottom()I

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/eg;->t:I

    sub-int/2addr v1, v2

    if-le p2, v1, :cond_34

    or-int/lit8 v0, v0, 0x8

    .line 1480
    :cond_34
    return v0
.end method

.method private f(I)Z
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1364
    iget-object v2, p0, Landroid/support/v4/widget/eg;->B:[I

    array-length v4, v2

    move v3, v1

    .line 1365
    :goto_6
    if-ge v3, v4, :cond_1f

    .line 8384
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/eg;->a(I)Z

    move-result v2

    if-eqz v2, :cond_19

    iget-object v2, p0, Landroid/support/v4/widget/eg;->B:[I

    aget v2, v2, v3

    and-int/2addr v2, p1

    if-eqz v2, :cond_19

    move v2, v0

    .line 1366
    :goto_16
    if-eqz v2, :cond_1b

    .line 1370
    :goto_18
    return v0

    :cond_19
    move v2, v1

    .line 8384
    goto :goto_16

    .line 1365
    :cond_1b
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    :cond_1f
    move v0, v1

    .line 1370
    goto :goto_18
.end method

.method private g()Landroid/view/View;
    .registers 2

    .prologue
    .line 475
    iget-object v0, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    return-object v0
.end method

.method private h()I
    .registers 2

    .prologue
    .line 483
    iget v0, p0, Landroid/support/v4/widget/eg;->A:I

    return v0
.end method

.method private i()I
    .registers 2

    .prologue
    .line 490
    iget v0, p0, Landroid/support/v4/widget/eg;->n:I

    return v0
.end method

.method private j()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 777
    iget-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    if-nez v0, :cond_7

    .line 788
    :goto_6
    return-void

    .line 780
    :cond_7
    iget-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 781
    iget-object v0, p0, Landroid/support/v4/widget/eg;->p:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 782
    iget-object v0, p0, Landroid/support/v4/widget/eg;->q:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 783
    iget-object v0, p0, Landroid/support/v4/widget/eg;->r:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 784
    iget-object v0, p0, Landroid/support/v4/widget/eg;->B:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 785
    iget-object v0, p0, Landroid/support/v4/widget/eg;->C:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 786
    iget-object v0, p0, Landroid/support/v4/widget/eg;->D:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 787
    iput v2, p0, Landroid/support/v4/widget/eg;->E:I

    goto :goto_6
.end method

.method private k()Z
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1309
    iget-object v2, p0, Landroid/support/v4/widget/eg;->o:[F

    array-length v4, v2

    move v3, v1

    .line 1310
    :goto_6
    if-ge v3, v4, :cond_39

    .line 8334
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/eg;->a(I)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 8341
    iget-object v2, p0, Landroid/support/v4/widget/eg;->q:[F

    aget v2, v2, v3

    iget-object v5, p0, Landroid/support/v4/widget/eg;->o:[F

    aget v5, v5, v3

    sub-float/2addr v2, v5

    .line 8342
    iget-object v5, p0, Landroid/support/v4/widget/eg;->r:[F

    aget v5, v5, v3

    iget-object v6, p0, Landroid/support/v4/widget/eg;->p:[F

    aget v6, v6, v3

    sub-float/2addr v5, v6

    .line 8345
    mul-float/2addr v2, v2

    mul-float/2addr v5, v5

    add-float/2addr v2, v5

    iget v5, p0, Landroid/support/v4/widget/eg;->n:I

    iget v6, p0, Landroid/support/v4/widget/eg;->n:I

    mul-int/2addr v5, v6

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-lez v2, :cond_31

    move v2, v0

    .line 1311
    :goto_2e
    if-eqz v2, :cond_35

    .line 1315
    :goto_30
    return v0

    :cond_31
    move v2, v1

    .line 8345
    goto :goto_2e

    :cond_33
    move v2, v1

    .line 8351
    goto :goto_2e

    .line 1310
    :cond_35
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    :cond_39
    move v0, v1

    .line 1315
    goto :goto_30
.end method

.method private l()V
    .registers 5

    .prologue
    .line 1388
    iget-object v0, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Landroid/support/v4/widget/eg;->G:F

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1389
    iget-object v0, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    iget v1, p0, Landroid/support/v4/widget/eg;->A:I

    invoke-static {v0, v1}, Landroid/support/v4/view/cs;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    iget v1, p0, Landroid/support/v4/widget/eg;->s:F

    iget v2, p0, Landroid/support/v4/widget/eg;->G:F

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/eg;->a(FFF)F

    move-result v0

    .line 1392
    iget-object v1, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    iget v2, p0, Landroid/support/v4/widget/eg;->A:I

    invoke-static {v1, v2}, Landroid/support/v4/view/cs;->b(Landroid/view/VelocityTracker;I)F

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/eg;->s:F

    iget v3, p0, Landroid/support/v4/widget/eg;->G:F

    invoke-static {v1, v2, v3}, Landroid/support/v4/widget/eg;->a(FFF)F

    .line 1395
    invoke-direct {p0, v0}, Landroid/support/v4/widget/eg;->c(F)V

    .line 1396
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 498
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/eg;->A:I

    .line 1777
    iget-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    if-eqz v0, :cond_2e

    .line 1780
    iget-object v0, p0, Landroid/support/v4/widget/eg;->o:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 1781
    iget-object v0, p0, Landroid/support/v4/widget/eg;->p:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 1782
    iget-object v0, p0, Landroid/support/v4/widget/eg;->q:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 1783
    iget-object v0, p0, Landroid/support/v4/widget/eg;->r:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 1784
    iget-object v0, p0, Landroid/support/v4/widget/eg;->B:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 1785
    iget-object v0, p0, Landroid/support/v4/widget/eg;->C:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 1786
    iget-object v0, p0, Landroid/support/v4/widget/eg;->D:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 1787
    iput v2, p0, Landroid/support/v4/widget/eg;->E:I

    .line 501
    :cond_2e
    iget-object v0, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_3a

    .line 502
    iget-object v0, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 503
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    .line 505
    :cond_3a
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .registers 6

    .prologue
    .line 460
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_25

    .line 461
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "captureChildView: parameter must be a descendant of the ViewDragHelper\'s tracked parent view ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 465
    :cond_25
    iput-object p1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    .line 466
    iput p2, p0, Landroid/support/v4/widget/eg;->A:I

    .line 467
    iget-object v0, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ej;->d(Landroid/view/View;)V

    .line 468
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/eg;->b(I)V

    .line 469
    return-void
.end method

.method public final a(I)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 867
    iget v1, p0, Landroid/support/v4/widget/eg;->E:I

    shl-int v2, v0, p1

    and-int/2addr v1, v2

    if-eqz v1, :cond_9

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final a(II)Z
    .registers 6

    .prologue
    .line 564
    iget-boolean v0, p0, Landroid/support/v4/widget/eg;->J:Z

    if-nez v0, :cond_c

    .line 565
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_c
    iget-object v0, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    iget v1, p0, Landroid/support/v4/widget/eg;->A:I

    invoke-static {v0, v1}, Landroid/support/v4/view/cs;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    iget v2, p0, Landroid/support/v4/widget/eg;->A:I

    invoke-static {v1, v2}, Landroid/support/v4/view/cs;->b(Landroid/view/VelocityTracker;I)F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v4/widget/eg;->a(IIII)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .registers 14

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 947
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 948
    invoke-static {p1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;)I

    move-result v3

    .line 950
    if-nez v2, :cond_10

    .line 953
    invoke-virtual {p0}, Landroid/support/v4/widget/eg;->a()V

    .line 956
    :cond_10
    iget-object v4, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    if-nez v4, :cond_1a

    .line 957
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    .line 959
    :cond_1a
    iget-object v4, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 961
    packed-switch v2, :pswitch_data_10e

    .line 1069
    :cond_22
    :goto_22
    :pswitch_22
    iget v2, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v2, v0, :cond_10a

    :goto_26
    return v0

    .line 963
    :pswitch_27
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 964
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 965
    invoke-static {p1, v1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 966
    invoke-direct {p0, v2, v3, v4}, Landroid/support/v4/widget/eg;->a(FFI)V

    .line 968
    float-to-int v2, v2

    float-to-int v3, v3

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v2

    .line 971
    iget-object v3, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    if-ne v2, v3, :cond_47

    iget v3, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v3, v6, :cond_47

    .line 972
    invoke-direct {p0, v2, v4}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;I)Z

    .line 975
    :cond_47
    iget-object v2, p0, Landroid/support/v4/widget/eg;->B:[I

    aget v2, v2, v4

    .line 976
    iget v3, p0, Landroid/support/v4/widget/eg;->u:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_22

    .line 977
    iget-object v2, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v2}, Landroid/support/v4/widget/ej;->c()V

    goto :goto_22

    .line 983
    :pswitch_56
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 984
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 985
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 987
    invoke-direct {p0, v4, v3, v2}, Landroid/support/v4/widget/eg;->a(FFI)V

    .line 990
    iget v5, p0, Landroid/support/v4/widget/eg;->m:I

    if-nez v5, :cond_78

    .line 991
    iget-object v3, p0, Landroid/support/v4/widget/eg;->B:[I

    aget v2, v3, v2

    .line 992
    iget v3, p0, Landroid/support/v4/widget/eg;->u:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_22

    .line 993
    iget-object v2, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v2}, Landroid/support/v4/widget/ej;->c()V

    goto :goto_22

    .line 995
    :cond_78
    iget v5, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v5, v6, :cond_22

    .line 997
    float-to-int v4, v4

    float-to-int v3, v3

    invoke-virtual {p0, v4, v3}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v3

    .line 998
    iget-object v4, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    if-ne v3, v4, :cond_22

    .line 999
    invoke-direct {p0, v3, v2}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;I)Z

    goto :goto_22

    .line 1006
    :pswitch_8a
    iget-object v2, p0, Landroid/support/v4/widget/eg;->o:[F

    if-eqz v2, :cond_22

    iget-object v2, p0, Landroid/support/v4/widget/eg;->p:[F

    if-eqz v2, :cond_22

    .line 1009
    invoke-static {p1}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;)I

    move-result v4

    move v3, v1

    .line 1010
    :goto_97
    if-ge v3, v4, :cond_e2

    .line 1011
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 1012
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 1013
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v6

    .line 1014
    iget-object v7, p0, Landroid/support/v4/widget/eg;->o:[F

    aget v7, v7, v5

    sub-float v7, v2, v7

    .line 1015
    iget-object v8, p0, Landroid/support/v4/widget/eg;->p:[F

    aget v8, v8, v5

    sub-float v8, v6, v8

    .line 1017
    float-to-int v2, v2

    float-to-int v6, v6

    invoke-virtual {p0, v2, v6}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v6

    .line 1018
    if-eqz v6, :cond_e7

    invoke-direct {p0, v6, v7}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;F)Z

    move-result v2

    if-eqz v2, :cond_e7

    move v2, v0

    .line 1019
    :goto_c0
    if-eqz v2, :cond_e9

    .line 1025
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 1026
    float-to-int v10, v7

    add-int/2addr v10, v9

    .line 1027
    iget-object v11, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v11, v6, v10}, Landroid/support/v4/widget/ej;->a(Landroid/view/View;I)I

    move-result v10

    .line 1029
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    .line 1031
    iget-object v11, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v11, v6}, Landroid/support/v4/widget/ej;->c(Landroid/view/View;)I

    .line 1033
    iget-object v11, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v11, v6}, Landroid/support/v4/widget/ej;->b(Landroid/view/View;)I

    move-result v11

    .line 1036
    if-eqz v11, :cond_e2

    if-lez v11, :cond_e9

    if-ne v10, v9, :cond_e9

    .line 1052
    :cond_e2
    invoke-direct {p0, p1}, Landroid/support/v4/widget/eg;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_22

    :cond_e7
    move v2, v1

    .line 1018
    goto :goto_c0

    .line 1042
    :cond_e9
    invoke-direct {p0, v7, v8, v5}, Landroid/support/v4/widget/eg;->b(FFI)V

    .line 1043
    iget v7, p0, Landroid/support/v4/widget/eg;->m:I

    if-eq v7, v0, :cond_e2

    .line 1048
    if-eqz v2, :cond_f8

    invoke-direct {p0, v6, v5}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_e2

    .line 1010
    :cond_f8
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_97

    .line 1057
    :pswitch_fc
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 1058
    invoke-direct {p0, v2}, Landroid/support/v4/widget/eg;->d(I)V

    goto/16 :goto_22

    .line 1064
    :pswitch_105
    invoke-virtual {p0}, Landroid/support/v4/widget/eg;->a()V

    goto/16 :goto_22

    :cond_10a
    move v0, v1

    .line 1069
    goto/16 :goto_26

    .line 961
    nop

    :pswitch_data_10e
    .packed-switch 0x0
        :pswitch_27
        :pswitch_105
        :pswitch_8a
        :pswitch_105
        :pswitch_22
        :pswitch_56
        :pswitch_fc
    .end packed-switch
.end method

.method public final a(Landroid/view/View;II)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 539
    iput-object p1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    .line 540
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/eg;->A:I

    .line 542
    invoke-direct {p0, p2, p3, v1, v1}, Landroid/support/v4/widget/eg;->a(IIII)Z

    move-result v0

    .line 543
    if-nez v0, :cond_17

    iget v1, p0, Landroid/support/v4/widget/eg;->m:I

    if-nez v1, :cond_17

    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    if-eqz v1, :cond_17

    .line 546
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    .line 549
    :cond_17
    return v0
.end method

.method public final b(II)Landroid/view/View;
    .registers 6

    .prologue
    .line 1461
    iget-object v0, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 1462
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_2e

    .line 1463
    iget-object v0, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1464
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt p1, v2, :cond_2a

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge p1, v2, :cond_2a

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt p2, v2, :cond_2a

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge p2, v2, :cond_2a

    .line 1469
    :goto_29
    return-object v0

    .line 1462
    :cond_2a
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1469
    :cond_2e
    const/4 v0, 0x0

    goto :goto_29
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 512
    invoke-virtual {p0}, Landroid/support/v4/widget/eg;->a()V

    .line 513
    iget v0, p0, Landroid/support/v4/widget/eg;->m:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_29

    .line 514
    iget-object v0, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->b()I

    .line 515
    iget-object v0, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->c()I

    .line 516
    iget-object v0, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->g()V

    .line 517
    iget-object v0, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->b()I

    move-result v0

    .line 518
    iget-object v1, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->c()I

    .line 519
    iget-object v1, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v2, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/widget/ej;->b(Landroid/view/View;I)V

    .line 521
    :cond_29
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/eg;->b(I)V

    .line 522
    return-void
.end method

.method final b(I)V
    .registers 4

    .prologue
    .line 871
    iget-object v0, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v4/widget/eg;->M:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 872
    iget v0, p0, Landroid/support/v4/widget/eg;->m:I

    if-eq v0, p1, :cond_19

    .line 873
    iput p1, p0, Landroid/support/v4/widget/eg;->m:I

    .line 874
    iget-object v0, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ej;->a(I)V

    .line 875
    iget v0, p0, Landroid/support/v4/widget/eg;->m:I

    if-nez v0, :cond_19

    .line 876
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    .line 879
    :cond_19
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .registers 10

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 1079
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 1080
    invoke-static {p1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;)I

    move-result v3

    .line 1082
    if-nez v2, :cond_10

    .line 1085
    invoke-virtual {p0}, Landroid/support/v4/widget/eg;->a()V

    .line 1088
    :cond_10
    iget-object v4, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    if-nez v4, :cond_1a

    .line 1089
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    .line 1091
    :cond_1a
    iget-object v4, p0, Landroid/support/v4/widget/eg;->F:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1093
    packed-switch v2, :pswitch_data_18e

    .line 1227
    :cond_22
    :goto_22
    :pswitch_22
    return-void

    .line 1095
    :pswitch_23
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 1096
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1097
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1098
    float-to-int v3, v1

    float-to-int v4, v2

    invoke-virtual {p0, v3, v4}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v3

    .line 1100
    invoke-direct {p0, v1, v2, v0}, Landroid/support/v4/widget/eg;->a(FFI)V

    .line 1105
    invoke-direct {p0, v3, v0}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;I)Z

    .line 1107
    iget-object v1, p0, Landroid/support/v4/widget/eg;->B:[I

    aget v0, v1, v0

    .line 1108
    iget v1, p0, Landroid/support/v4/widget/eg;->u:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_22

    .line 1109
    iget-object v0, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v0}, Landroid/support/v4/widget/ej;->c()V

    goto :goto_22

    .line 1115
    :pswitch_4a
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1116
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 1117
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 1119
    invoke-direct {p0, v1, v2, v0}, Landroid/support/v4/widget/eg;->a(FFI)V

    .line 1122
    iget v3, p0, Landroid/support/v4/widget/eg;->m:I

    if-nez v3, :cond_75

    .line 1125
    float-to-int v1, v1

    float-to-int v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v1

    .line 1126
    invoke-direct {p0, v1, v0}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;I)Z

    .line 1128
    iget-object v1, p0, Landroid/support/v4/widget/eg;->B:[I

    aget v0, v1, v0

    .line 1129
    iget v1, p0, Landroid/support/v4/widget/eg;->u:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_22

    .line 1130
    iget-object v0, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    invoke-virtual {v0}, Landroid/support/v4/widget/ej;->c()V

    goto :goto_22

    .line 1132
    :cond_75
    float-to-int v1, v1

    float-to-int v2, v2

    .line 6430
    iget-object v3, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-static {v3, v1, v2}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;II)Z

    move-result v1

    .line 1132
    if-eqz v1, :cond_22

    .line 1137
    iget-object v1, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;I)Z

    goto :goto_22

    .line 1143
    :pswitch_85
    iget v1, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v1, v7, :cond_f0

    .line 1144
    iget v0, p0, Landroid/support/v4/widget/eg;->A:I

    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1145
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 1146
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 1147
    iget-object v2, p0, Landroid/support/v4/widget/eg;->q:[F

    iget v3, p0, Landroid/support/v4/widget/eg;->A:I

    aget v2, v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 1148
    iget-object v2, p0, Landroid/support/v4/widget/eg;->r:[F

    iget v3, p0, Landroid/support/v4/widget/eg;->A:I

    aget v2, v2, v3

    sub-float/2addr v0, v2

    float-to-int v2, v0

    .line 1150
    iget-object v0, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int/2addr v0, v1

    iget-object v3, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    .line 7401
    iget-object v3, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 7402
    iget-object v4, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    .line 7403
    if-eqz v1, :cond_d0

    .line 7404
    iget-object v5, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v6, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v5, v6, v0}, Landroid/support/v4/widget/ej;->a(Landroid/view/View;I)I

    move-result v0

    .line 7405
    iget-object v5, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    sub-int v3, v0, v3

    invoke-virtual {v5, v3}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 7407
    :cond_d0
    if-eqz v2, :cond_e0

    .line 7408
    iget-object v3, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v5, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/support/v4/widget/ej;->c(Landroid/view/View;)I

    move-result v3

    .line 7409
    iget-object v5, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    sub-int/2addr v3, v4

    invoke-virtual {v5, v3}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 7412
    :cond_e0
    if-nez v1, :cond_e4

    if-eqz v2, :cond_eb

    .line 7415
    :cond_e4
    iget-object v1, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v2, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/widget/ej;->b(Landroid/view/View;I)V

    .line 1152
    :cond_eb
    invoke-direct {p0, p1}, Landroid/support/v4/widget/eg;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_22

    .line 1155
    :cond_f0
    invoke-static {p1}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;)I

    move-result v1

    .line 1156
    :goto_f4
    if-ge v0, v1, :cond_12a

    .line 1157
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 1158
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 1159
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 1160
    iget-object v5, p0, Landroid/support/v4/widget/eg;->o:[F

    aget v5, v5, v2

    sub-float v5, v3, v5

    .line 1161
    iget-object v6, p0, Landroid/support/v4/widget/eg;->p:[F

    aget v6, v6, v2

    sub-float v6, v4, v6

    .line 1163
    invoke-direct {p0, v5, v6, v2}, Landroid/support/v4/widget/eg;->b(FFI)V

    .line 1164
    iget v6, p0, Landroid/support/v4/widget/eg;->m:I

    if-eq v6, v7, :cond_12a

    .line 1169
    float-to-int v3, v3

    float-to-int v4, v4

    invoke-virtual {p0, v3, v4}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v3

    .line 1170
    invoke-direct {p0, v3, v5}, Landroid/support/v4/widget/eg;->a(Landroid/view/View;F)Z

    move-result v4

    if-eqz v4, :cond_127

    invoke-direct {p0, v3, v2}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_12a

    .line 1156
    :cond_127
    add-int/lit8 v0, v0, 0x1

    goto :goto_f4

    .line 1175
    :cond_12a
    invoke-direct {p0, p1}, Landroid/support/v4/widget/eg;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_22

    .line 1181
    :pswitch_12f
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 1182
    iget v3, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v3, v7, :cond_16a

    iget v3, p0, Landroid/support/v4/widget/eg;->A:I

    if-ne v2, v3, :cond_16a

    .line 1185
    invoke-static {p1}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;)I

    move-result v3

    .line 1186
    :goto_13f
    if-ge v0, v3, :cond_18b

    .line 1187
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 1188
    iget v5, p0, Landroid/support/v4/widget/eg;->A:I

    if-eq v4, v5, :cond_16f

    .line 1193
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 1194
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v6

    .line 1195
    float-to-int v5, v5

    float-to-int v6, v6

    invoke-virtual {p0, v5, v6}, Landroid/support/v4/widget/eg;->b(II)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    if-ne v5, v6, :cond_16f

    iget-object v5, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-direct {p0, v5, v4}, Landroid/support/v4/widget/eg;->b(Landroid/view/View;I)Z

    move-result v4

    if-eqz v4, :cond_16f

    .line 1197
    iget v0, p0, Landroid/support/v4/widget/eg;->A:I

    .line 1202
    :goto_165
    if-ne v0, v1, :cond_16a

    .line 1204
    invoke-direct {p0}, Landroid/support/v4/widget/eg;->l()V

    .line 1207
    :cond_16a
    invoke-direct {p0, v2}, Landroid/support/v4/widget/eg;->d(I)V

    goto/16 :goto_22

    .line 1186
    :cond_16f
    add-int/lit8 v0, v0, 0x1

    goto :goto_13f

    .line 1212
    :pswitch_172
    iget v0, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v0, v7, :cond_179

    .line 1213
    invoke-direct {p0}, Landroid/support/v4/widget/eg;->l()V

    .line 1215
    :cond_179
    invoke-virtual {p0}, Landroid/support/v4/widget/eg;->a()V

    goto/16 :goto_22

    .line 1220
    :pswitch_17e
    iget v0, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v0, v7, :cond_186

    .line 1221
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/widget/eg;->c(F)V

    .line 1223
    :cond_186
    invoke-virtual {p0}, Landroid/support/v4/widget/eg;->a()V

    goto/16 :goto_22

    :cond_18b
    move v0, v1

    goto :goto_165

    .line 1093
    nop

    :pswitch_data_18e
    .packed-switch 0x0
        :pswitch_23
        :pswitch_172
        :pswitch_85
        :pswitch_17e
        :pswitch_22
        :pswitch_4a
        :pswitch_12f
    .end packed-switch
.end method

.method public final c()Z
    .registers 9

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 722
    iget v1, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v1, v7, :cond_66

    .line 723
    iget-object v1, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->f()Z

    move-result v1

    .line 724
    iget-object v2, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v2}, Landroid/support/v4/widget/ca;->b()I

    move-result v2

    .line 725
    iget-object v3, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v3}, Landroid/support/v4/widget/ca;->c()I

    move-result v3

    .line 726
    iget-object v4, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int v4, v2, v4

    .line 727
    iget-object v5, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int v5, v3, v5

    .line 729
    if-eqz v4, :cond_2f

    .line 730
    iget-object v6, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v6, v4}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 732
    :cond_2f
    if-eqz v5, :cond_36

    .line 733
    iget-object v6, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 736
    :cond_36
    if-nez v4, :cond_3a

    if-eqz v5, :cond_41

    .line 737
    :cond_3a
    iget-object v4, p0, Landroid/support/v4/widget/eg;->I:Landroid/support/v4/widget/ej;

    iget-object v5, p0, Landroid/support/v4/widget/eg;->v:Landroid/view/View;

    invoke-virtual {v4, v5, v2}, Landroid/support/v4/widget/ej;->b(Landroid/view/View;I)V

    .line 740
    :cond_41
    if-eqz v1, :cond_5d

    iget-object v4, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    .line 4304
    iget-object v5, v4, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v4, v4, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v5, v4}, Landroid/support/v4/widget/cb;->h(Ljava/lang/Object;)I

    move-result v4

    .line 740
    if-ne v2, v4, :cond_5d

    iget-object v2, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v2}, Landroid/support/v4/widget/ca;->d()I

    move-result v2

    if-ne v3, v2, :cond_5d

    .line 743
    iget-object v1, p0, Landroid/support/v4/widget/eg;->H:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->g()V

    move v1, v0

    .line 747
    :cond_5d
    if-nez v1, :cond_66

    .line 749
    iget-object v1, p0, Landroid/support/v4/widget/eg;->K:Landroid/view/ViewGroup;

    iget-object v2, p0, Landroid/support/v4/widget/eg;->M:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 756
    :cond_66
    iget v1, p0, Landroid/support/v4/widget/eg;->m:I

    if-ne v1, v7, :cond_6b

    const/4 v0, 0x1

    :cond_6b
    return v0
.end method
