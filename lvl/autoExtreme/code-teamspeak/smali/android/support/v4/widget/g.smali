.class public final Landroid/support/v4/widget/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/support/v4/widget/j;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 40
    const/16 v1, 0x17

    if-lt v0, v1, :cond_e

    .line 41
    new-instance v0, Landroid/support/v4/widget/h;

    invoke-direct {v0}, Landroid/support/v4/widget/h;-><init>()V

    sput-object v0, Landroid/support/v4/widget/g;->a:Landroid/support/v4/widget/j;

    .line 47
    :goto_d
    return-void

    .line 42
    :cond_e
    const/16 v1, 0x15

    if-lt v0, v1, :cond_1a

    .line 43
    new-instance v0, Landroid/support/v4/widget/k;

    invoke-direct {v0}, Landroid/support/v4/widget/k;-><init>()V

    sput-object v0, Landroid/support/v4/widget/g;->a:Landroid/support/v4/widget/j;

    goto :goto_d

    .line 45
    :cond_1a
    new-instance v0, Landroid/support/v4/widget/i;

    invoke-direct {v0}, Landroid/support/v4/widget/i;-><init>()V

    sput-object v0, Landroid/support/v4/widget/g;->a:Landroid/support/v4/widget/j;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .param p0    # Landroid/widget/CompoundButton;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 174
    sget-object v0, Landroid/support/v4/widget/g;->a:Landroid/support/v4/widget/j;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/j;->a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p0    # Landroid/widget/CompoundButton;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 128
    sget-object v0, Landroid/support/v4/widget/g;->a:Landroid/support/v4/widget/j;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/j;->a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V

    .line 129
    return-void
.end method

.method public static a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
    .registers 3
    .param p0    # Landroid/widget/CompoundButton;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 154
    sget-object v0, Landroid/support/v4/widget/g;->a:Landroid/support/v4/widget/j;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/j;->a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V

    .line 155
    return-void
.end method

.method private static b(Landroid/widget/CompoundButton;)Landroid/content/res/ColorStateList;
    .registers 2
    .param p0    # Landroid/widget/CompoundButton;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 138
    sget-object v0, Landroid/support/v4/widget/g;->a:Landroid/support/v4/widget/j;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/j;->b(Landroid/widget/CompoundButton;)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/widget/CompoundButton;)Landroid/graphics/PorterDuff$Mode;
    .registers 2
    .param p0    # Landroid/widget/CompoundButton;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 164
    sget-object v0, Landroid/support/v4/widget/g;->a:Landroid/support/v4/widget/j;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/j;->c(Landroid/widget/CompoundButton;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method
