.class public abstract Landroid/support/v4/widget/as;
.super Landroid/support/v4/view/a;
.source "SourceFile"


# static fields
.field public static final a:I = -0x80000000

.field public static final c:I = -0x1

.field private static final d:Ljava/lang/String;


# instance fields
.field private final e:Landroid/graphics/Rect;

.field private final f:Landroid/graphics/Rect;

.field private final g:Landroid/graphics/Rect;

.field private final h:[I

.field private final i:Landroid/view/accessibility/AccessibilityManager;

.field private final j:Landroid/view/View;

.field private k:Landroid/support/v4/widget/au;

.field private l:I

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 64
    const-class v0, Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v4/widget/as;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/view/View;)V
    .registers 4

    .prologue
    const/high16 v1, -0x80000000

    .line 92
    invoke-direct {p0}, Landroid/support/v4/view/a;-><init>()V

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    .line 68
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    .line 69
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/as;->g:Landroid/graphics/Rect;

    .line 70
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v4/widget/as;->h:[I

    .line 82
    iput v1, p0, Landroid/support/v4/widget/as;->l:I

    .line 85
    iput v1, p0, Landroid/support/v4/widget/as;->m:I

    .line 93
    if-nez p1, :cond_2d

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "View may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_2d
    iput-object p1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    .line 98
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 99
    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    .line 100
    return-void
.end method

.method static synthetic a(Landroid/support/v4/widget/as;I)Landroid/support/v4/view/a/q;
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 12323
    packed-switch p1, :pswitch_data_e8

    .line 13384
    invoke-static {}, Landroid/support/v4/view/a/q;->a()Landroid/support/v4/view/a/q;

    move-result-object v0

    .line 13387
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->h(Z)V

    .line 13388
    sget-object v1, Landroid/support/v4/widget/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 13394
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->m()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_55

    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->n()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_55

    .line 13395
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must add text or a content description in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 12338
    :pswitch_25
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Landroid/view/View;)Landroid/support/v4/view/a/q;

    move-result-object v1

    .line 12339
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 12345
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 12348
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_39
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 12349
    iget-object v3, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 12437
    sget-object v4, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v5, v1, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v4, v5, v3, v0}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Landroid/view/View;I)V

    goto :goto_39

    :cond_53
    move-object v0, v1

    .line 12325
    :goto_54
    return-object v0

    .line 13399
    :cond_55
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(Landroid/graphics/Rect;)V

    .line 13400
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6a

    .line 13401
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must set parent bounds in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13405
    :cond_6a
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->b()I

    move-result v1

    .line 13406
    and-int/lit8 v2, v1, 0x40

    if-eqz v2, :cond_7a

    .line 13407
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13410
    :cond_7a
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_86

    .line 13411
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13416
    :cond_86
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(Ljava/lang/CharSequence;)V

    .line 13417
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    .line 14336
    sget-object v2, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, v0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v2, v3, v1, p1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Landroid/view/View;I)V

    .line 13418
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->d(Landroid/view/View;)V

    .line 13421
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    if-ne v1, p1, :cond_df

    .line 13422
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 13423
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(I)V

    .line 13430
    :goto_ad
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v1}, Landroid/support/v4/widget/as;->a(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_bd

    .line 13431
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->c(Z)V

    .line 13432
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->b(Landroid/graphics/Rect;)V

    .line 13436
    :cond_bd
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    iget-object v2, p0, Landroid/support/v4/widget/as;->h:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 13437
    iget-object v1, p0, Landroid/support/v4/widget/as;->h:[I

    aget v1, v1, v5

    .line 13438
    iget-object v2, p0, Landroid/support/v4/widget/as;->h:[I

    aget v2, v2, v4

    .line 13439
    iget-object v3, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 13440
    iget-object v3, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 13441
    iget-object v1, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->d(Landroid/graphics/Rect;)V

    goto/16 :goto_54

    .line 13425
    :cond_df
    invoke-virtual {v0, v5}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 13426
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(I)V

    goto :goto_ad

    .line 12323
    :pswitch_data_e8
    .packed-switch -0x1
        :pswitch_25
    .end packed-switch
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 233
    iget v0, p0, Landroid/support/v4/widget/as;->m:I

    if-ne v0, p1, :cond_5

    .line 245
    :goto_4
    return-void

    .line 237
    :cond_5
    iget v0, p0, Landroid/support/v4/widget/as;->m:I

    .line 238
    iput p1, p0, Landroid/support/v4/widget/as;->m:I

    .line 242
    const/16 v1, 0x80

    invoke-direct {p0, p1, v1}, Landroid/support/v4/widget/as;->a(II)Z

    .line 243
    const/16 v1, 0x100

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_4
.end method

.method private a(II)Z
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 176
    const/high16 v1, -0x80000000

    if-eq p1, v1, :cond_d

    iget-object v1, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_e

    .line 186
    :cond_d
    :goto_d
    return v0

    .line 180
    :cond_e
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 181
    if-eqz v1, :cond_d

    .line 3258
    packed-switch p1, :pswitch_data_6a

    .line 3290
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 3291
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 3292
    sget-object v2, Landroid/support/v4/widget/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 3298
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4e

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_4e

    .line 3299
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must add text or a content description in populateEventForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3274
    :pswitch_3e
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 3275
    iget-object v2, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v2, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 186
    :goto_47
    iget-object v2, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v1, v2, v0}, Landroid/support/v4/view/fb;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_d

    .line 3304
    :cond_4e
    iget-object v2, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 3306
    invoke-static {v0}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;

    move-result-object v2

    .line 3307
    iget-object v3, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    .line 3589
    sget-object v4, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v2, v2, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v4, v2, v3, p1}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;Landroid/view/View;I)V

    goto :goto_47

    .line 3258
    nop

    :pswitch_data_6a
    .packed-switch -0x1
        :pswitch_3e
    .end packed-switch
.end method

.method private a(IILandroid/os/Bundle;)Z
    .registers 9

    .prologue
    const/high16 v4, 0x10000

    const/4 v0, 0x1

    const/high16 v3, -0x80000000

    const/4 v1, 0x0

    .line 447
    packed-switch p1, :pswitch_data_62

    .line 9460
    sparse-switch p2, :sswitch_data_68

    .line 9465
    invoke-virtual {p0}, Landroid/support/v4/widget/as;->e()Z

    move-result v0

    .line 9582
    :goto_10
    return v0

    .line 9456
    :pswitch_11
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0, p2, p3}, Landroid/support/v4/view/cx;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_10

    .line 9470
    :sswitch_18
    sparse-switch p2, :sswitch_data_72

    move v0, v1

    .line 9463
    goto :goto_10

    .line 9545
    :sswitch_1d
    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2d

    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v2}, Landroid/support/v4/view/a/h;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v2

    if-nez v2, :cond_2f

    :cond_2d
    move v0, v1

    .line 9547
    goto :goto_10

    .line 9550
    :cond_2f
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v2

    if-nez v2, :cond_4c

    .line 9552
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    if-eq v1, v3, :cond_3e

    .line 9553
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    invoke-direct {p0, v1, v4}, Landroid/support/v4/widget/as;->a(II)Z

    .line 9558
    :cond_3e
    iput p1, p0, Landroid/support/v4/widget/as;->l:I

    .line 9561
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 9562
    const v1, 0x8000

    invoke-direct {p0, p1, v1}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_10

    :cond_4c
    move v0, v1

    .line 9472
    goto :goto_10

    .line 9577
    :sswitch_4e
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 9578
    iput v3, p0, Landroid/support/v4/widget/as;->l:I

    .line 9579
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 9580
    invoke-direct {p0, p1, v4}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_10

    :cond_5f
    move v0, v1

    .line 9474
    goto :goto_10

    .line 447
    nop

    :pswitch_data_62
    .packed-switch -0x1
        :pswitch_11
    .end packed-switch

    .line 9460
    :sswitch_data_68
    .sparse-switch
        0x40 -> :sswitch_18
        0x80 -> :sswitch_18
    .end sparse-switch

    .line 9470
    :sswitch_data_72
    .sparse-switch
        0x40 -> :sswitch_1d
        0x80 -> :sswitch_4e
    .end sparse-switch
.end method

.method private a(ILandroid/os/Bundle;)Z
    .registers 4

    .prologue
    .line 456
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0, p1, p2}, Landroid/support/v4/view/cx;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method private a(Landroid/graphics/Rect;)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 490
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_9
    move v0, v1

    .line 520
    :goto_a
    return v0

    .line 495
    :cond_b
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    if-eqz v0, :cond_15

    move v0, v1

    .line 496
    goto :goto_a

    .line 500
    :cond_15
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 501
    :goto_1b
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_37

    .line 502
    check-cast v0, Landroid/view/View;

    .line 503
    invoke-static {v0}, Landroid/support/v4/view/cx;->d(Landroid/view/View;)F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-lez v2, :cond_30

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_32

    :cond_30
    move v0, v1

    .line 504
    goto :goto_a

    .line 506
    :cond_32
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_1b

    .line 510
    :cond_37
    if-nez v0, :cond_3b

    move v0, v1

    .line 511
    goto :goto_a

    .line 515
    :cond_3b
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    iget-object v2, p0, Landroid/support/v4/widget/as;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_47

    move v0, v1

    .line 516
    goto :goto_a

    .line 520
    :cond_47
    iget-object v0, p0, Landroid/support/v4/widget/as;->g:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v0

    goto :goto_a
.end method

.method static synthetic a(Landroid/support/v4/widget/as;IILandroid/os/Bundle;)Z
    .registers 9

    .prologue
    const/high16 v4, 0x10000

    const/4 v0, 0x1

    const/high16 v3, -0x80000000

    const/4 v1, 0x0

    .line 14447
    packed-switch p1, :pswitch_data_62

    .line 14460
    sparse-switch p2, :sswitch_data_68

    .line 14465
    invoke-virtual {p0}, Landroid/support/v4/widget/as;->e()Z

    move-result v0

    .line 14582
    :goto_10
    return v0

    .line 14456
    :pswitch_11
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0, p2, p3}, Landroid/support/v4/view/cx;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_10

    .line 14470
    :sswitch_18
    sparse-switch p2, :sswitch_data_72

    move v0, v1

    .line 14463
    goto :goto_10

    .line 14545
    :sswitch_1d
    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2d

    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v2}, Landroid/support/v4/view/a/h;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v2

    if-nez v2, :cond_2f

    :cond_2d
    move v0, v1

    .line 14547
    goto :goto_10

    .line 14550
    :cond_2f
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v2

    if-nez v2, :cond_4c

    .line 14552
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    if-eq v1, v3, :cond_3e

    .line 14553
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    invoke-direct {p0, v1, v4}, Landroid/support/v4/widget/as;->a(II)Z

    .line 14558
    :cond_3e
    iput p1, p0, Landroid/support/v4/widget/as;->l:I

    .line 14561
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 14562
    const v1, 0x8000

    invoke-direct {p0, p1, v1}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_10

    :cond_4c
    move v0, v1

    .line 14472
    goto :goto_10

    .line 14577
    :sswitch_4e
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 14578
    iput v3, p0, Landroid/support/v4/widget/as;->l:I

    .line 14579
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 14580
    invoke-direct {p0, p1, v4}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_10

    :cond_5f
    move v0, v1

    .line 14474
    goto :goto_10

    .line 14447
    nop

    :pswitch_data_62
    .packed-switch -0x1
        :pswitch_11
    .end packed-switch

    .line 14460
    :sswitch_data_68
    .sparse-switch
        0x40 -> :sswitch_18
        0x80 -> :sswitch_18
    .end sparse-switch

    .line 14470
    :sswitch_data_72
    .sparse-switch
        0x40 -> :sswitch_1d
        0x80 -> :sswitch_4e
    .end sparse-switch
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/high16 v3, -0x80000000

    const/4 v1, 0x0

    .line 136
    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_14

    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v2}, Landroid/support/v4/view/a/h;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 154
    :cond_15
    :goto_15
    return v0

    .line 141
    :cond_16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_3a

    :pswitch_1d
    move v0, v1

    .line 154
    goto :goto_15

    .line 144
    :pswitch_1f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    invoke-virtual {p0}, Landroid/support/v4/widget/as;->a()I

    move-result v2

    .line 145
    invoke-direct {p0, v2}, Landroid/support/v4/widget/as;->a(I)V

    .line 146
    if-ne v2, v3, :cond_15

    move v0, v1

    goto :goto_15

    .line 148
    :pswitch_30
    iget v2, p0, Landroid/support/v4/widget/as;->l:I

    if-eq v2, v3, :cond_38

    .line 149
    invoke-direct {p0, v3}, Landroid/support/v4/widget/as;->a(I)V

    goto :goto_15

    :cond_38
    move v0, v1

    .line 152
    goto :goto_15

    .line 141
    :pswitch_data_3a
    .packed-switch 0x7
        :pswitch_1f
        :pswitch_1d
        :pswitch_1f
        :pswitch_30
    .end packed-switch
.end method

.method private b(I)Landroid/view/accessibility/AccessibilityEvent;
    .registers 4

    .prologue
    .line 274
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 275
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 276
    return-object v0
.end method

.method private b(II)Landroid/view/accessibility/AccessibilityEvent;
    .registers 7

    .prologue
    .line 258
    packed-switch p1, :pswitch_data_4e

    .line 4290
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 4291
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 4292
    sget-object v1, Landroid/support/v4/widget/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 4298
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_32

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_32

    .line 4299
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must add text or a content description in populateEventForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4274
    :pswitch_28
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 4275
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 262
    :goto_31
    return-object v0

    .line 4304
    :cond_32
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 4306
    invoke-static {v0}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;

    move-result-object v1

    .line 4307
    iget-object v2, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    .line 4589
    sget-object v3, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, v1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v3, v1, v2, p1}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;Landroid/view/View;I)V

    goto :goto_31

    .line 258
    nop

    :pswitch_data_4e
    .packed-switch -0x1
        :pswitch_28
    .end packed-switch
.end method

.method private c(I)Landroid/support/v4/view/a/q;
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 323
    packed-switch p1, :pswitch_data_e8

    .line 7384
    invoke-static {}, Landroid/support/v4/view/a/q;->a()Landroid/support/v4/view/a/q;

    move-result-object v0

    .line 7387
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->h(Z)V

    .line 7388
    sget-object v1, Landroid/support/v4/widget/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 7394
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->m()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_55

    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->n()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_55

    .line 7395
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must add text or a content description in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6338
    :pswitch_25
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Landroid/view/View;)Landroid/support/v4/view/a/q;

    move-result-object v1

    .line 6339
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 6345
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 6348
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_39
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 6349
    iget-object v3, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 6437
    sget-object v4, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v5, v1, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v4, v5, v3, v0}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Landroid/view/View;I)V

    goto :goto_39

    :cond_53
    move-object v0, v1

    .line 327
    :goto_54
    return-object v0

    .line 7399
    :cond_55
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(Landroid/graphics/Rect;)V

    .line 7400
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6a

    .line 7401
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must set parent bounds in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7405
    :cond_6a
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->b()I

    move-result v1

    .line 7406
    and-int/lit8 v2, v1, 0x40

    if-eqz v2, :cond_7a

    .line 7407
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7410
    :cond_7a
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_86

    .line 7411
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7416
    :cond_86
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(Ljava/lang/CharSequence;)V

    .line 7417
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    .line 8336
    sget-object v2, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, v0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v2, v3, v1, p1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Landroid/view/View;I)V

    .line 7418
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->d(Landroid/view/View;)V

    .line 7421
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    if-ne v1, p1, :cond_df

    .line 7422
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 7423
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(I)V

    .line 7430
    :goto_ad
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v1}, Landroid/support/v4/widget/as;->a(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_bd

    .line 7431
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->c(Z)V

    .line 7432
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->b(Landroid/graphics/Rect;)V

    .line 7436
    :cond_bd
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    iget-object v2, p0, Landroid/support/v4/widget/as;->h:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 7437
    iget-object v1, p0, Landroid/support/v4/widget/as;->h:[I

    aget v1, v1, v5

    .line 7438
    iget-object v2, p0, Landroid/support/v4/widget/as;->h:[I

    aget v2, v2, v4

    .line 7439
    iget-object v3, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 7440
    iget-object v3, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 7441
    iget-object v1, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->d(Landroid/graphics/Rect;)V

    goto/16 :goto_54

    .line 7425
    :cond_df
    invoke-virtual {v0, v5}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 7426
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(I)V

    goto :goto_ad

    .line 323
    :pswitch_data_e8
    .packed-switch -0x1
        :pswitch_25
    .end packed-switch
.end method

.method private c(II)Landroid/view/accessibility/AccessibilityEvent;
    .registers 7

    .prologue
    .line 290
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 291
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 292
    sget-object v1, Landroid/support/v4/widget/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 298
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_25

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_25

    .line 299
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must add text or a content description in populateEventForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_25
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 306
    invoke-static {v0}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;

    move-result-object v1

    .line 307
    iget-object v2, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    .line 5589
    sget-object v3, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, v1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v3, v1, v2, p1}, Landroid/support/v4/view/a/bg;->a(Ljava/lang/Object;Landroid/view/View;I)V

    .line 309
    return-object v0
.end method

.method private d(I)Landroid/support/v4/view/a/q;
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 384
    invoke-static {}, Landroid/support/v4/view/a/q;->a()Landroid/support/v4/view/a/q;

    move-result-object v0

    .line 387
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->h(Z)V

    .line 388
    sget-object v1, Landroid/support/v4/widget/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 394
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->m()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_22

    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->n()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_22

    .line 395
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must add text or a content description in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 399
    :cond_22
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(Landroid/graphics/Rect;)V

    .line 400
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 401
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must set parent bounds in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :cond_37
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->b()I

    move-result v1

    .line 406
    and-int/lit8 v2, v1, 0x40

    if-eqz v2, :cond_47

    .line 407
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_47
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_53

    .line 411
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 416
    :cond_53
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    .line 9336
    sget-object v2, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, v0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v2, v3, v1, p1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Landroid/view/View;I)V

    .line 418
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->d(Landroid/view/View;)V

    .line 421
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    if-ne v1, p1, :cond_ab

    .line 422
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 423
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(I)V

    .line 430
    :goto_7a
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v1}, Landroid/support/v4/widget/as;->a(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_8a

    .line 431
    invoke-virtual {v0, v4}, Landroid/support/v4/view/a/q;->c(Z)V

    .line 432
    iget-object v1, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->b(Landroid/graphics/Rect;)V

    .line 436
    :cond_8a
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    iget-object v2, p0, Landroid/support/v4/widget/as;->h:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 437
    iget-object v1, p0, Landroid/support/v4/widget/as;->h:[I

    aget v1, v1, v5

    .line 438
    iget-object v2, p0, Landroid/support/v4/widget/as;->h:[I

    aget v2, v2, v4

    .line 439
    iget-object v3, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/v4/widget/as;->f:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 440
    iget-object v3, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 441
    iget-object v1, p0, Landroid/support/v4/widget/as;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->d(Landroid/graphics/Rect;)V

    .line 443
    return-object v0

    .line 425
    :cond_ab
    invoke-virtual {v0, v5}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 426
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(I)V

    goto :goto_7a
.end method

.method private d(II)Z
    .registers 8

    .prologue
    const/high16 v4, 0x10000

    const/4 v0, 0x1

    const/high16 v3, -0x80000000

    const/4 v1, 0x0

    .line 460
    sparse-switch p2, :sswitch_data_58

    .line 465
    invoke-virtual {p0}, Landroid/support/v4/widget/as;->e()Z

    move-result v0

    .line 10582
    :goto_d
    return v0

    .line 10470
    :sswitch_e
    sparse-switch p2, :sswitch_data_62

    move v0, v1

    .line 463
    goto :goto_d

    .line 10545
    :sswitch_13
    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_23

    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v2}, Landroid/support/v4/view/a/h;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    .line 10547
    goto :goto_d

    .line 10550
    :cond_25
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v2

    if-nez v2, :cond_42

    .line 10552
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    if-eq v1, v3, :cond_34

    .line 10553
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    invoke-direct {p0, v1, v4}, Landroid/support/v4/widget/as;->a(II)Z

    .line 10558
    :cond_34
    iput p1, p0, Landroid/support/v4/widget/as;->l:I

    .line 10561
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 10562
    const v1, 0x8000

    invoke-direct {p0, p1, v1}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_d

    :cond_42
    move v0, v1

    .line 10472
    goto :goto_d

    .line 10577
    :sswitch_44
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v2

    if-eqz v2, :cond_55

    .line 10578
    iput v3, p0, Landroid/support/v4/widget/as;->l:I

    .line 10579
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 10580
    invoke-direct {p0, p1, v4}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_d

    :cond_55
    move v0, v1

    .line 10474
    goto :goto_d

    .line 460
    nop

    :sswitch_data_58
    .sparse-switch
        0x40 -> :sswitch_e
        0x80 -> :sswitch_e
    .end sparse-switch

    .line 10470
    :sswitch_data_62
    .sparse-switch
        0x40 -> :sswitch_13
        0x80 -> :sswitch_44
    .end sparse-switch
.end method

.method private e(I)Z
    .registers 3

    .prologue
    .line 529
    iget v0, p0, Landroid/support/v4/widget/as;->l:I

    if-ne v0, p1, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private e(II)Z
    .registers 8

    .prologue
    const/high16 v4, 0x10000

    const/4 v0, 0x1

    const/high16 v3, -0x80000000

    const/4 v1, 0x0

    .line 470
    sparse-switch p2, :sswitch_data_50

    move v0, v1

    .line 11582
    :goto_a
    return v0

    .line 11545
    :sswitch_b
    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1b

    iget-object v2, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v2}, Landroid/support/v4/view/a/h;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v2

    if-nez v2, :cond_1d

    :cond_1b
    move v0, v1

    .line 11547
    goto :goto_a

    .line 11550
    :cond_1d
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v2

    if-nez v2, :cond_3a

    .line 11552
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    if-eq v1, v3, :cond_2c

    .line 11553
    iget v1, p0, Landroid/support/v4/widget/as;->l:I

    invoke-direct {p0, v1, v4}, Landroid/support/v4/widget/as;->a(II)Z

    .line 11558
    :cond_2c
    iput p1, p0, Landroid/support/v4/widget/as;->l:I

    .line 11561
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 11562
    const v1, 0x8000

    invoke-direct {p0, p1, v1}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_a

    :cond_3a
    move v0, v1

    .line 472
    goto :goto_a

    .line 11577
    :sswitch_3c
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v2

    if-eqz v2, :cond_4d

    .line 11578
    iput v3, p0, Landroid/support/v4/widget/as;->l:I

    .line 11579
    iget-object v1, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 11580
    invoke-direct {p0, p1, v4}, Landroid/support/v4/widget/as;->a(II)Z

    goto :goto_a

    :cond_4d
    move v0, v1

    .line 474
    goto :goto_a

    .line 470
    nop

    :sswitch_data_50
    .sparse-switch
        0x40 -> :sswitch_b
        0x80 -> :sswitch_3c
    .end sparse-switch
.end method

.method private f()V
    .registers 3

    .prologue
    .line 197
    .line 4210
    const/4 v0, -0x1

    const/16 v1, 0x800

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/as;->a(II)Z

    .line 198
    return-void
.end method

.method private f(I)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 545
    iget-object v1, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_11

    iget-object v1, p0, Landroid/support/v4/widget/as;->i:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v1}, Landroid/support/v4/view/a/h;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 566
    :cond_11
    :goto_11
    return v0

    .line 550
    :cond_12
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v1

    if-nez v1, :cond_11

    .line 552
    iget v0, p0, Landroid/support/v4/widget/as;->l:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_25

    .line 553
    iget v0, p0, Landroid/support/v4/widget/as;->l:I

    const/high16 v1, 0x10000

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/as;->a(II)Z

    .line 558
    :cond_25
    iput p1, p0, Landroid/support/v4/widget/as;->l:I

    .line 561
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 562
    const v0, 0x8000

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/as;->a(II)Z

    .line 564
    const/4 v0, 0x1

    goto :goto_11
.end method

.method private g()V
    .registers 3

    .prologue
    .line 210
    const/4 v0, -0x1

    const/16 v1, 0x800

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/as;->a(II)Z

    .line 212
    return-void
.end method

.method private g(I)Z
    .registers 3

    .prologue
    .line 577
    invoke-direct {p0, p1}, Landroid/support/v4/widget/as;->e(I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 578
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v4/widget/as;->l:I

    .line 579
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 580
    const/high16 v0, 0x10000

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/as;->a(II)Z

    .line 582
    const/4 v0, 0x1

    .line 584
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private h()I
    .registers 2

    .prologue
    .line 221
    iget v0, p0, Landroid/support/v4/widget/as;->l:I

    return v0
.end method

.method private i()Landroid/support/v4/view/a/q;
    .registers 7

    .prologue
    .line 338
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Landroid/view/View;)Landroid/support/v4/view/a/q;

    move-result-object v1

    .line 339
    iget-object v0, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 345
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 348
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 349
    iget-object v3, p0, Landroid/support/v4/widget/as;->j:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 8437
    sget-object v4, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v5, v1, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v4, v5, v3, v0}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Landroid/view/View;I)V

    goto :goto_14

    .line 352
    :cond_2e
    return-object v1
.end method

.method private static j()V
    .registers 0

    .prologue
    .line 708
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public final a(Landroid/view/View;)Landroid/support/v4/view/a/aq;
    .registers 4

    .prologue
    .line 110
    iget-object v0, p0, Landroid/support/v4/widget/as;->k:Landroid/support/v4/widget/au;

    if-nez v0, :cond_c

    .line 111
    new-instance v0, Landroid/support/v4/widget/au;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v4/widget/au;-><init>(Landroid/support/v4/widget/as;B)V

    iput-object v0, p0, Landroid/support/v4/widget/as;->k:Landroid/support/v4/widget/au;

    .line 113
    :cond_c
    iget-object v0, p0, Landroid/support/v4/widget/as;->k:Landroid/support/v4/widget/au;

    return-object v0
.end method

.method protected abstract b()V
.end method

.method protected abstract c()V
.end method

.method protected abstract d()V
.end method

.method protected abstract e()Z
.end method
