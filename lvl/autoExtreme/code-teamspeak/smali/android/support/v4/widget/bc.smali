.class final Landroid/support/v4/widget/bc;
.super Landroid/view/animation/Animation;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v4/widget/bg;

.field final synthetic b:Landroid/support/v4/widget/bb;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/bb;Landroid/support/v4/widget/bg;)V
    .registers 3

    .prologue
    .line 364
    iput-object p1, p0, Landroid/support/v4/widget/bc;->b:Landroid/support/v4/widget/bb;

    iput-object p2, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 12

    .prologue
    const v8, 0x3f4ccccd    # 0.8f

    const/high16 v7, 0x3f000000    # 0.5f

    .line 367
    iget-object v0, p0, Landroid/support/v4/widget/bc;->b:Landroid/support/v4/widget/bb;

    iget-boolean v0, v0, Landroid/support/v4/widget/bb;->d:Z

    if-eqz v0, :cond_11

    .line 368
    iget-object v0, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    invoke-static {p1, v0}, Landroid/support/v4/widget/bb;->a(FLandroid/support/v4/widget/bg;)V

    .line 414
    :goto_10
    return-void

    .line 372
    :cond_11
    iget-object v0, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    invoke-static {v0}, Landroid/support/v4/widget/bb;->a(Landroid/support/v4/widget/bg;)F

    move-result v0

    .line 373
    iget-object v1, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    .line 1682
    iget v1, v1, Landroid/support/v4/widget/bg;->l:F

    .line 374
    iget-object v2, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    .line 2678
    iget v2, v2, Landroid/support/v4/widget/bg;->k:F

    .line 375
    iget-object v3, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    .line 2763
    iget v3, v3, Landroid/support/v4/widget/bg;->m:F

    .line 377
    iget-object v4, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    invoke-static {p1, v4}, Landroid/support/v4/widget/bb;->b(FLandroid/support/v4/widget/bg;)V

    .line 381
    cmpg-float v4, p1, v7

    if-gtz v4, :cond_3f

    .line 385
    div-float v4, p1, v7

    .line 387
    sub-float v5, v8, v0

    invoke-static {}, Landroid/support/v4/widget/bb;->a()Landroid/view/animation/Interpolator;

    move-result-object v6

    invoke-interface {v6, v4}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v4

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    .line 390
    iget-object v4, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    invoke-virtual {v4, v2}, Landroid/support/v4/widget/bg;->a(F)V

    .line 395
    :cond_3f
    cmpl-float v2, p1, v7

    if-lez v2, :cond_57

    .line 399
    sub-float v0, v8, v0

    .line 400
    sub-float v2, p1, v7

    div-float/2addr v2, v7

    .line 402
    invoke-static {}, Landroid/support/v4/widget/bb;->a()Landroid/view/animation/Interpolator;

    move-result-object v4

    invoke-interface {v4, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 404
    iget-object v1, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/bg;->b(F)V

    .line 407
    :cond_57
    const/high16 v0, 0x3e800000    # 0.25f

    mul-float/2addr v0, p1

    add-float/2addr v0, v3

    .line 408
    iget-object v1, p0, Landroid/support/v4/widget/bc;->a:Landroid/support/v4/widget/bg;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/bg;->c(F)V

    .line 410
    const/high16 v0, 0x43580000    # 216.0f

    mul-float/2addr v0, p1

    const/high16 v1, 0x44870000    # 1080.0f

    iget-object v2, p0, Landroid/support/v4/widget/bc;->b:Landroid/support/v4/widget/bb;

    invoke-static {v2}, Landroid/support/v4/widget/bb;->a(Landroid/support/v4/widget/bb;)F

    move-result v2

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 412
    iget-object v1, p0, Landroid/support/v4/widget/bc;->b:Landroid/support/v4/widget/bb;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/bb;->c(F)V

    goto :goto_10
.end method
