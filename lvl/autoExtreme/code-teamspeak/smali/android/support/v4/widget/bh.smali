.class final Landroid/support/v4/widget/bh;
.super Landroid/support/v4/view/a;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1794
    invoke-direct {p0}, Landroid/support/v4/view/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
    .registers 5

    .prologue
    .line 1832
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 1833
    check-cast p1, Landroid/support/v4/widget/NestedScrollView;

    .line 1834
    const-class v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 1835
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 1836
    invoke-static {p1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/support/v4/widget/NestedScrollView;)I

    move-result v0

    .line 1837
    if-lez v0, :cond_34

    .line 1838
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->i(Z)V

    .line 1839
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    if-lez v1, :cond_29

    .line 1840
    const/16 v1, 0x2000

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->a(I)V

    .line 1842
    :cond_29
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    if-ge v1, v0, :cond_34

    .line 1843
    const/16 v0, 0x1000

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->a(I)V

    .line 1847
    :cond_34
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 7

    .prologue
    .line 1851
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1852
    check-cast p1, Landroid/support/v4/widget/NestedScrollView;

    .line 1853
    const-class v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1854
    invoke-static {p2}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;

    move-result-object v1

    .line 1855
    invoke-static {p1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/support/v4/widget/NestedScrollView;)I

    move-result v0

    if-lez v0, :cond_49

    const/4 v0, 0x1

    .line 1856
    :goto_19
    invoke-virtual {v1, v0}, Landroid/support/v4/view/a/bd;->a(Z)V

    .line 1857
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    .line 2818
    sget-object v2, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v3, v1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v2, v3, v0}, Landroid/support/v4/view/a/bg;->f(Ljava/lang/Object;I)V

    .line 1858
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    .line 2836
    sget-object v2, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v3, v1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v2, v3, v0}, Landroid/support/v4/view/a/bg;->g(Ljava/lang/Object;I)V

    .line 1859
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    .line 2853
    sget-object v2, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v3, v1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v2, v3, v0}, Landroid/support/v4/view/a/bg;->i(Ljava/lang/Object;I)V

    .line 1860
    invoke-static {p1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/support/v4/widget/NestedScrollView;)I

    move-result v0

    .line 2871
    sget-object v2, Landroid/support/v4/view/a/bd;->a:Landroid/support/v4/view/a/bg;

    iget-object v1, v1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    invoke-interface {v2, v1, v0}, Landroid/support/v4/view/a/bg;->j(Ljava/lang/Object;I)V

    .line 1861
    return-void

    .line 1855
    :cond_49
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1797
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/a;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1827
    :goto_8
    return v0

    .line 1800
    :cond_9
    check-cast p1, Landroid/support/v4/widget/NestedScrollView;

    .line 1801
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 1802
    goto :goto_8

    .line 1804
    :cond_13
    sparse-switch p2, :sswitch_data_64

    move v0, v1

    .line 1827
    goto :goto_8

    .line 1806
    :sswitch_18
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1808
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {p1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/support/v4/widget/NestedScrollView;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1810
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    if-eq v2, v3, :cond_3d

    .line 1811
    invoke-virtual {p1, v2}, Landroid/support/v4/widget/NestedScrollView;->a(I)V

    goto :goto_8

    :cond_3d
    move v0, v1

    .line 1815
    goto :goto_8

    .line 1817
    :sswitch_3f
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1819
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    sub-int v2, v3, v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1820
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    if-eq v2, v3, :cond_61

    .line 1821
    invoke-virtual {p1, v2}, Landroid/support/v4/widget/NestedScrollView;->a(I)V

    goto :goto_8

    :cond_61
    move v0, v1

    .line 1825
    goto :goto_8

    .line 1804
    nop

    :sswitch_data_64
    .sparse-switch
        0x1000 -> :sswitch_18
        0x2000 -> :sswitch_3f
    .end sparse-switch
.end method
