.class Landroid/support/v4/widget/ck;
.super Landroid/support/v4/widget/cp;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 135
    invoke-direct {p0}, Landroid/support/v4/widget/cp;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/view/View;
    .registers 3

    .prologue
    .line 1042
    new-instance v0, Landroid/widget/SearchView;

    invoke-direct {v0, p1}, Landroid/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 139
    return-object v0
.end method

.method public final a(Landroid/view/View;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 185
    .line 1084
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    .line 185
    return-object v0
.end method

.method public final a(Landroid/support/v4/widget/ci;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 169
    new-instance v0, Landroid/support/v4/widget/cm;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/widget/cm;-><init>(Landroid/support/v4/widget/ck;Landroid/support/v4/widget/ci;)V

    .line 1071
    new-instance v1, Landroid/support/v4/widget/cs;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/cs;-><init>(Landroid/support/v4/widget/ct;)V

    .line 169
    return-object v1
.end method

.method public final a(Landroid/support/v4/widget/cj;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 149
    new-instance v0, Landroid/support/v4/widget/cl;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/widget/cl;-><init>(Landroid/support/v4/widget/ck;Landroid/support/v4/widget/cj;)V

    .line 1053
    new-instance v1, Landroid/support/v4/widget/cr;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/cr;-><init>(Landroid/support/v4/widget/cu;)V

    .line 149
    return-object v1
.end method

.method public final a(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 230
    .line 1120
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1, p2}, Landroid/widget/SearchView;->setMaxWidth(I)V

    .line 231
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/content/ComponentName;)V
    .registers 5

    .prologue
    .line 144
    .line 1046
    check-cast p1, Landroid/widget/SearchView;

    .line 1047
    invoke-virtual {p1}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 1049
    invoke-virtual {v0, p2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 145
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 195
    .line 1092
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1, p2}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 196
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/CharSequence;Z)V
    .registers 4

    .prologue
    .line 190
    .line 1088
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1, p2, p3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 191
    return-void
.end method

.method public final a(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 200
    .line 1096
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1, p2}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 201
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 164
    .line 1067
    check-cast p1, Landroid/widget/SearchView;

    check-cast p2, Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual {p1, p2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 165
    return-void
.end method

.method public final b(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 210
    .line 1104
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1, p2}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    .line 211
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 180
    .line 1080
    check-cast p1, Landroid/widget/SearchView;

    check-cast p2, Landroid/widget/SearchView$OnCloseListener;

    invoke-virtual {p1, p2}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    .line 181
    return-void
.end method

.method public final b(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 205
    .line 1100
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1}, Landroid/widget/SearchView;->isIconified()Z

    move-result v0

    .line 205
    return v0
.end method

.method public final c(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 220
    .line 1112
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1, p2}, Landroid/widget/SearchView;->setQueryRefinementEnabled(Z)V

    .line 221
    return-void
.end method

.method public final c(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 215
    .line 1108
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1}, Landroid/widget/SearchView;->isSubmitButtonEnabled()Z

    move-result v0

    .line 215
    return v0
.end method

.method public final d(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 225
    .line 1116
    check-cast p1, Landroid/widget/SearchView;

    invoke-virtual {p1}, Landroid/widget/SearchView;->isQueryRefinementEnabled()Z

    move-result v0

    .line 225
    return v0
.end method
