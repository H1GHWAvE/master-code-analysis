.class public final Landroid/support/v4/widget/ch;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/support/v4/widget/co;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 255
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_e

    .line 256
    new-instance v0, Landroid/support/v4/widget/cn;

    invoke-direct {v0}, Landroid/support/v4/widget/cn;-><init>()V

    sput-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    .line 262
    :goto_d
    return-void

    .line 257
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1c

    .line 258
    new-instance v0, Landroid/support/v4/widget/ck;

    invoke-direct {v0}, Landroid/support/v4/widget/ck;-><init>()V

    sput-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    goto :goto_d

    .line 260
    :cond_1c
    new-instance v0, Landroid/support/v4/widget/cp;

    invoke-direct {v0}, Landroid/support/v4/widget/cp;-><init>()V

    sput-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    return-void
.end method

.method static synthetic a()Landroid/support/v4/widget/co;
    .registers 1

    .prologue
    .line 30
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    return-object v0
.end method

.method private static a(Landroid/content/Context;)Landroid/view/View;
    .registers 2

    .prologue
    .line 276
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/co;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/View;)Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 408
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/co;->a(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 303
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/co;->b(Landroid/view/View;I)V

    .line 304
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/content/ComponentName;)V
    .registers 3

    .prologue
    .line 290
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/co;->a(Landroid/view/View;Landroid/content/ComponentName;)V

    .line 291
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/support/v4/widget/ci;)V
    .registers 4

    .prologue
    .line 376
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    iget-object v1, p1, Landroid/support/v4/widget/ci;->a:Ljava/lang/Object;

    invoke-interface {v0, p0, v1}, Landroid/support/v4/widget/co;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 377
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/support/v4/widget/cj;)V
    .registers 4

    .prologue
    .line 327
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    iget-object v1, p1, Landroid/support/v4/widget/cj;->a:Ljava/lang/Object;

    invoke-interface {v0, p0, v1}, Landroid/support/v4/widget/co;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 328
    return-void
.end method

.method private static a(Landroid/view/View;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 432
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/co;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 433
    return-void
.end method

.method private static a(Landroid/view/View;Ljava/lang/CharSequence;Z)V
    .registers 4

    .prologue
    .line 421
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/widget/co;->a(Landroid/view/View;Ljava/lang/CharSequence;Z)V

    .line 422
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 447
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/co;->a(Landroid/view/View;Z)V

    .line 448
    return-void
.end method

.method private static b(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 316
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/co;->c(Landroid/view/View;I)V

    .line 317
    return-void
.end method

.method private static b(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 471
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/co;->b(Landroid/view/View;Z)V

    .line 472
    return-void
.end method

.method private static b(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 458
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/co;->b(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 517
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/co;->a(Landroid/view/View;I)V

    .line 518
    return-void
.end method

.method private static c(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 500
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/co;->c(Landroid/view/View;Z)V

    .line 501
    return-void
.end method

.method private static c(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 481
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/co;->c(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private static d(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 509
    sget-object v0, Landroid/support/v4/widget/ch;->a:Landroid/support/v4/widget/co;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/co;->d(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
