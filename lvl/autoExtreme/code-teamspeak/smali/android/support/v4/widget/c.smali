.class final Landroid/support/v4/widget/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:I

.field c:F

.field d:F

.field e:J

.field f:J

.field g:I

.field h:I

.field i:J

.field j:F

.field k:I


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 744
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Landroid/support/v4/widget/c;->e:J

    .line 745
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v4/widget/c;->i:J

    .line 746
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/support/v4/widget/c;->f:J

    .line 747
    iput v2, p0, Landroid/support/v4/widget/c;->g:I

    .line 748
    iput v2, p0, Landroid/support/v4/widget/c;->h:I

    .line 749
    return-void
.end method

.method private static a(F)F
    .registers 3

    .prologue
    .line 807
    const/high16 v0, -0x3f800000    # -4.0f

    mul-float/2addr v0, p0

    mul-float/2addr v0, p0

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, p0

    add-float/2addr v0, v1

    return v0
.end method

.method private a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 763
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/widget/c;->e:J

    .line 764
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v4/widget/c;->i:J

    .line 765
    iget-wide v0, p0, Landroid/support/v4/widget/c;->e:J

    iput-wide v0, p0, Landroid/support/v4/widget/c;->f:J

    .line 766
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/v4/widget/c;->j:F

    .line 767
    iput v2, p0, Landroid/support/v4/widget/c;->g:I

    .line 768
    iput v2, p0, Landroid/support/v4/widget/c;->h:I

    .line 769
    return-void
.end method

.method private a(FF)V
    .registers 3

    .prologue
    .line 839
    iput p1, p0, Landroid/support/v4/widget/c;->c:F

    .line 840
    iput p2, p0, Landroid/support/v4/widget/c;->d:F

    .line 841
    return-void
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 752
    iput p1, p0, Landroid/support/v4/widget/c;->a:I

    .line 753
    return-void
.end method

.method private b()V
    .registers 5

    .prologue
    .line 775
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 776
    iget-wide v2, p0, Landroid/support/v4/widget/c;->e:J

    sub-long v2, v0, v2

    long-to-int v2, v2

    iget v3, p0, Landroid/support/v4/widget/c;->b:I

    invoke-static {v2, v3}, Landroid/support/v4/widget/a;->a(II)I

    move-result v2

    iput v2, p0, Landroid/support/v4/widget/c;->k:I

    .line 777
    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/c;->a(J)F

    move-result v2

    iput v2, p0, Landroid/support/v4/widget/c;->j:F

    .line 778
    iput-wide v0, p0, Landroid/support/v4/widget/c;->i:J

    .line 779
    return-void
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 756
    iput p1, p0, Landroid/support/v4/widget/c;->b:I

    .line 757
    return-void
.end method

.method private c()Z
    .registers 7

    .prologue
    .line 782
    iget-wide v0, p0, Landroid/support/v4/widget/c;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_18

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/v4/widget/c;->i:J

    iget v4, p0, Landroid/support/v4/widget/c;->k:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private d()V
    .registers 7

    .prologue
    .line 818
    iget-wide v0, p0, Landroid/support/v4/widget/c;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_10

    .line 819
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot compute scroll delta before calling start()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 822
    :cond_10
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 823
    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/c;->a(J)F

    move-result v2

    .line 1807
    const/high16 v3, -0x3f800000    # -4.0f

    mul-float/2addr v3, v2

    mul-float/2addr v3, v2

    const/high16 v4, 0x40800000    # 4.0f

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    .line 825
    iget-wide v4, p0, Landroid/support/v4/widget/c;->f:J

    sub-long v4, v0, v4

    .line 827
    iput-wide v0, p0, Landroid/support/v4/widget/c;->f:J

    .line 828
    long-to-float v0, v4

    mul-float/2addr v0, v2

    iget v1, p0, Landroid/support/v4/widget/c;->c:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/c;->g:I

    .line 829
    long-to-float v0, v4

    mul-float/2addr v0, v2

    iget v1, p0, Landroid/support/v4/widget/c;->d:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/c;->h:I

    .line 830
    return-void
.end method

.method private e()I
    .registers 3

    .prologue
    .line 844
    iget v0, p0, Landroid/support/v4/widget/c;->c:F

    iget v1, p0, Landroid/support/v4/widget/c;->c:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private f()I
    .registers 3

    .prologue
    .line 848
    iget v0, p0, Landroid/support/v4/widget/c;->d:F

    iget v1, p0, Landroid/support/v4/widget/c;->d:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private g()I
    .registers 2

    .prologue
    .line 856
    iget v0, p0, Landroid/support/v4/widget/c;->g:I

    return v0
.end method

.method private h()I
    .registers 2

    .prologue
    .line 864
    iget v0, p0, Landroid/support/v4/widget/c;->h:I

    return v0
.end method


# virtual methods
.method final a(J)F
    .registers 8

    .prologue
    .line 787
    iget-wide v0, p0, Landroid/support/v4/widget/c;->e:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_8

    .line 788
    const/4 v0, 0x0

    .line 794
    :goto_7
    return v0

    .line 789
    :cond_8
    iget-wide v0, p0, Landroid/support/v4/widget/c;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_16

    iget-wide v0, p0, Landroid/support/v4/widget/c;->i:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_27

    .line 790
    :cond_16
    iget-wide v0, p0, Landroid/support/v4/widget/c;->e:J

    sub-long v0, p1, v0

    .line 791
    const/high16 v2, 0x3f000000    # 0.5f

    long-to-float v0, v0

    iget v1, p0, Landroid/support/v4/widget/c;->a:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/support/v4/widget/a;->a(F)F

    move-result v0

    mul-float/2addr v0, v2

    goto :goto_7

    .line 793
    :cond_27
    iget-wide v0, p0, Landroid/support/v4/widget/c;->i:J

    sub-long v0, p1, v0

    .line 794
    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, Landroid/support/v4/widget/c;->j:F

    sub-float/2addr v2, v3

    iget v3, p0, Landroid/support/v4/widget/c;->j:F

    long-to-float v0, v0

    iget v1, p0, Landroid/support/v4/widget/c;->k:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/support/v4/widget/a;->a(F)F

    move-result v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    goto :goto_7
.end method
