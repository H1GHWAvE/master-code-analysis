.class Landroid/support/v4/widget/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/j;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 80
    invoke-static {p1}, Landroid/support/v4/widget/m;->a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
    .registers 4

    .prologue
    .line 60
    .line 1035
    instance-of v0, p1, Landroid/support/v4/widget/ef;

    if-eqz v0, :cond_9

    .line 1036
    check-cast p1, Landroid/support/v4/widget/ef;

    invoke-interface {p1, p2}, Landroid/support/v4/widget/ef;->setSupportButtonTintList(Landroid/content/res/ColorStateList;)V

    .line 61
    :cond_9
    return-void
.end method

.method public a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
    .registers 4

    .prologue
    .line 70
    .line 1048
    instance-of v0, p1, Landroid/support/v4/widget/ef;

    if-eqz v0, :cond_9

    .line 1049
    check-cast p1, Landroid/support/v4/widget/ef;

    invoke-interface {p1, p2}, Landroid/support/v4/widget/ef;->setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 71
    :cond_9
    return-void
.end method

.method public b(Landroid/widget/CompoundButton;)Landroid/content/res/ColorStateList;
    .registers 3

    .prologue
    .line 65
    .line 1041
    instance-of v0, p1, Landroid/support/v4/widget/ef;

    if-eqz v0, :cond_b

    .line 1042
    check-cast p1, Landroid/support/v4/widget/ef;

    invoke-interface {p1}, Landroid/support/v4/widget/ef;->getSupportButtonTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_a
    return-object v0

    .line 1044
    :cond_b
    const/4 v0, 0x0

    .line 65
    goto :goto_a
.end method

.method public c(Landroid/widget/CompoundButton;)Landroid/graphics/PorterDuff$Mode;
    .registers 3

    .prologue
    .line 75
    .line 1054
    instance-of v0, p1, Landroid/support/v4/widget/ef;

    if-eqz v0, :cond_b

    .line 1055
    check-cast p1, Landroid/support/v4/widget/ef;

    invoke-interface {p1}, Landroid/support/v4/widget/ef;->getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :goto_a
    return-object v0

    .line 1057
    :cond_b
    const/4 v0, 0x0

    .line 75
    goto :goto_a
.end method
