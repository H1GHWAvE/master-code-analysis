.class public final Landroid/support/v4/app/ga;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "android.support.v4.app.EXTRA_CALLING_PACKAGE"

.field public static final b:Ljava/lang/String; = "android.support.v4.app.EXTRA_CALLING_ACTIVITY"

.field private static c:Landroid/support/v4/app/gd;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 152
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_e

    .line 153
    new-instance v0, Landroid/support/v4/app/gg;

    invoke-direct {v0}, Landroid/support/v4/app/gg;-><init>()V

    sput-object v0, Landroid/support/v4/app/ga;->c:Landroid/support/v4/app/gd;

    .line 159
    :goto_d
    return-void

    .line 154
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1c

    .line 155
    new-instance v0, Landroid/support/v4/app/gf;

    invoke-direct {v0}, Landroid/support/v4/app/gf;-><init>()V

    sput-object v0, Landroid/support/v4/app/ga;->c:Landroid/support/v4/app/gd;

    goto :goto_d

    .line 157
    :cond_1c
    new-instance v0, Landroid/support/v4/app/ge;

    invoke-direct {v0}, Landroid/support/v4/app/ge;-><init>()V

    sput-object v0, Landroid/support/v4/app/ga;->c:Landroid/support/v4/app/gd;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 655
    return-void
.end method

.method static synthetic a()Landroid/support/v4/app/gd;
    .registers 1

    .prologue
    .line 59
    sget-object v0, Landroid/support/v4/app/ga;->c:Landroid/support/v4/app/gd;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 174
    invoke-virtual {p0}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 175
    if-nez v0, :cond_10

    .line 176
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.support.v4.app.EXTRA_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    :cond_10
    return-object v0
.end method

.method private static a(Landroid/view/Menu;ILandroid/support/v4/app/gb;)V
    .registers 6

    .prologue
    .line 245
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 246
    if-nez v0, :cond_21

    .line 247
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find menu item with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in the supplied menu"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1233
    :cond_21
    sget-object v1, Landroid/support/v4/app/ga;->c:Landroid/support/v4/app/gd;

    invoke-interface {v1, v0, p2}, Landroid/support/v4/app/gd;->a(Landroid/view/MenuItem;Landroid/support/v4/app/gb;)V

    .line 251
    return-void
.end method

.method private static a(Landroid/view/MenuItem;Landroid/support/v4/app/gb;)V
    .registers 3

    .prologue
    .line 233
    sget-object v0, Landroid/support/v4/app/ga;->c:Landroid/support/v4/app/gd;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/app/gd;->a(Landroid/view/MenuItem;Landroid/support/v4/app/gb;)V

    .line 234
    return-void
.end method

.method public static b(Landroid/app/Activity;)Landroid/content/ComponentName;
    .registers 3

    .prologue
    .line 194
    invoke-virtual {p0}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 195
    if-nez v0, :cond_12

    .line 196
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.support.v4.app.EXTRA_CALLING_ACTIVITY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 198
    :cond_12
    return-object v0
.end method
