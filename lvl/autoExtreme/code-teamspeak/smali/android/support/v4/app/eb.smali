.class Landroid/support/v4/app/eb;
.super Landroid/support/v4/app/dx;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 589
    invoke-direct {p0}, Landroid/support/v4/app/dx;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification;
    .registers 27

    .prologue
    .line 592
    new-instance v2, Landroid/support/v4/app/eu;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/support/v4/app/dm;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v9, v0, Landroid/support/v4/app/dm;->i:I

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v4/app/dm;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v13, v0, Landroid/support/v4/app/dm;->o:I

    move-object/from16 v0, p1

    iget v14, v0, Landroid/support/v4/app/dm;->p:I

    move-object/from16 v0, p1

    iget-boolean v15, v0, Landroid/support/v4/app/dm;->q:Z

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->l:Z

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/support/v4/app/dm;->j:I

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->v:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->r:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->s:Z

    move/from16 v22, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->t:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-direct/range {v2 .. v23}, Landroid/support/v4/app/eu;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZILjava/lang/CharSequence;ZLandroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 598
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Landroid/support/v4/app/dd;->a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V

    .line 599
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    invoke-static {v2, v3}, Landroid/support/v4/app/dd;->a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V

    .line 600
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/dn;->a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dc;)Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method public a(Landroid/app/Notification;)Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 605
    invoke-static {p1}, Landroid/support/v4/app/et;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Notification;I)Landroid/support/v4/app/df;
    .registers 5

    .prologue
    .line 615
    sget-object v0, Landroid/support/v4/app/df;->e:Landroid/support/v4/app/el;

    sget-object v1, Landroid/support/v4/app/fn;->c:Landroid/support/v4/app/fx;

    invoke-static {p1, p2, v0, v1}, Landroid/support/v4/app/et;->a(Landroid/app/Notification;ILandroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/df;

    return-object v0
.end method

.method public a([Landroid/support/v4/app/df;)Ljava/util/ArrayList;
    .registers 3

    .prologue
    .line 629
    invoke-static {p1}, Landroid/support/v4/app/et;->a([Landroid/support/v4/app/ek;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/ArrayList;)[Landroid/support/v4/app/df;
    .registers 4

    .prologue
    .line 622
    sget-object v0, Landroid/support/v4/app/df;->e:Landroid/support/v4/app/el;

    sget-object v1, Landroid/support/v4/app/fn;->c:Landroid/support/v4/app/fx;

    invoke-static {p1, v0, v1}, Landroid/support/v4/app/et;->a(Ljava/util/ArrayList;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/ek;

    move-result-object v0

    check-cast v0, [Landroid/support/v4/app/df;

    check-cast v0, [Landroid/support/v4/app/df;

    return-object v0
.end method

.method public b(Landroid/app/Notification;)I
    .registers 3

    .prologue
    .line 610
    invoke-static {p1}, Landroid/support/v4/app/et;->b(Landroid/app/Notification;)I

    move-result v0

    return v0
.end method

.method public d(Landroid/app/Notification;)Z
    .registers 3

    .prologue
    .line 634
    invoke-static {p1}, Landroid/support/v4/app/et;->c(Landroid/app/Notification;)Z

    move-result v0

    return v0
.end method

.method public e(Landroid/app/Notification;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 639
    invoke-static {p1}, Landroid/support/v4/app/et;->d(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(Landroid/app/Notification;)Z
    .registers 3

    .prologue
    .line 644
    invoke-static {p1}, Landroid/support/v4/app/et;->e(Landroid/app/Notification;)Z

    move-result v0

    return v0
.end method

.method public g(Landroid/app/Notification;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 649
    invoke-static {p1}, Landroid/support/v4/app/et;->f(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
