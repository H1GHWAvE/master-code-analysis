.class public Landroid/support/v4/app/dd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Ljava/lang/String; = "android.bigText"

.field public static final B:Ljava/lang/String; = "android.icon"

.field public static final C:Ljava/lang/String; = "android.largeIcon"

.field public static final D:Ljava/lang/String; = "android.largeIcon.big"

.field public static final E:Ljava/lang/String; = "android.progress"

.field public static final F:Ljava/lang/String; = "android.progressMax"

.field public static final G:Ljava/lang/String; = "android.progressIndeterminate"

.field public static final H:Ljava/lang/String; = "android.showChronometer"

.field public static final I:Ljava/lang/String; = "android.showWhen"

.field public static final J:Ljava/lang/String; = "android.picture"

.field public static final K:Ljava/lang/String; = "android.textLines"

.field public static final L:Ljava/lang/String; = "android.template"

.field public static final M:Ljava/lang/String; = "android.people"

.field public static final N:Ljava/lang/String; = "android.backgroundImageUri"

.field public static final O:Ljava/lang/String; = "android.mediaSession"

.field public static final P:Ljava/lang/String; = "android.compactActions"

.field public static final Q:I = 0x0
    .annotation build Landroid/support/a/j;
    .end annotation
.end field

.field public static final R:I = 0x1

.field public static final S:I = 0x0

.field public static final T:I = -0x1

.field public static final U:Ljava/lang/String; = "call"

.field public static final V:Ljava/lang/String; = "msg"

.field public static final W:Ljava/lang/String; = "email"

.field public static final X:Ljava/lang/String; = "event"

.field public static final Y:Ljava/lang/String; = "promo"

.field public static final Z:Ljava/lang/String; = "alarm"

.field public static final a:I = -0x1

.field public static final aa:Ljava/lang/String; = "progress"

.field public static final ab:Ljava/lang/String; = "social"

.field public static final ac:Ljava/lang/String; = "err"

.field public static final ad:Ljava/lang/String; = "transport"

.field public static final ae:Ljava/lang/String; = "sys"

.field public static final af:Ljava/lang/String; = "service"

.field public static final ag:Ljava/lang/String; = "recommendation"

.field public static final ah:Ljava/lang/String; = "status"

.field private static final ai:Landroid/support/v4/app/du;

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x4

.field public static final e:I = -0x1

.field public static final f:I = 0x1

.field public static final g:I = 0x2

.field public static final h:I = 0x4

.field public static final i:I = 0x8

.field public static final j:I = 0x10

.field public static final k:I = 0x20

.field public static final l:I = 0x40

.field public static final m:I = 0x80

.field public static final n:I = 0x100

.field public static final o:I = 0x200

.field public static final p:I = 0x0

.field public static final q:I = -0x1

.field public static final r:I = -0x2

.field public static final s:I = 0x1

.field public static final t:I = 0x2

.field public static final u:Ljava/lang/String; = "android.title"

.field public static final v:Ljava/lang/String; = "android.title.big"

.field public static final w:Ljava/lang/String; = "android.text"

.field public static final x:Ljava/lang/String; = "android.subText"

.field public static final y:Ljava/lang/String; = "android.infoText"

.field public static final z:Ljava/lang/String; = "android.summaryText"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 830
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    .line 831
    new-instance v0, Landroid/support/v4/app/dw;

    invoke-direct {v0}, Landroid/support/v4/app/dw;-><init>()V

    sput-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    .line 847
    :goto_d
    return-void

    .line 832
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1c

    .line 833
    new-instance v0, Landroid/support/v4/app/dv;

    invoke-direct {v0}, Landroid/support/v4/app/dv;-><init>()V

    sput-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    goto :goto_d

    .line 834
    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2a

    .line 835
    new-instance v0, Landroid/support/v4/app/ec;

    invoke-direct {v0}, Landroid/support/v4/app/ec;-><init>()V

    sput-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    goto :goto_d

    .line 836
    :cond_2a
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_38

    .line 837
    new-instance v0, Landroid/support/v4/app/eb;

    invoke-direct {v0}, Landroid/support/v4/app/eb;-><init>()V

    sput-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    goto :goto_d

    .line 838
    :cond_38
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_46

    .line 839
    new-instance v0, Landroid/support/v4/app/ea;

    invoke-direct {v0}, Landroid/support/v4/app/ea;-><init>()V

    sput-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    goto :goto_d

    .line 840
    :cond_46
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_54

    .line 841
    new-instance v0, Landroid/support/v4/app/dz;

    invoke-direct {v0}, Landroid/support/v4/app/dz;-><init>()V

    sput-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    goto :goto_d

    .line 842
    :cond_54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_62

    .line 843
    new-instance v0, Landroid/support/v4/app/dy;

    invoke-direct {v0}, Landroid/support/v4/app/dy;-><init>()V

    sput-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    goto :goto_d

    .line 845
    :cond_62
    new-instance v0, Landroid/support/v4/app/dx;

    invoke-direct {v0}, Landroid/support/v4/app/dx;-><init>()V

    sput-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2940
    return-void
.end method

.method public static a(Landroid/app/Notification;)Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 3290
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    invoke-interface {v0, p0}, Landroid/support/v4/app/du;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/Notification;I)Landroid/support/v4/app/df;
    .registers 3

    .prologue
    .line 3308
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/app/du;->a(Landroid/app/Notification;I)Landroid/support/v4/app/df;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Landroid/support/v4/app/du;
    .registers 1

    .prologue
    .line 42
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V
    .registers 4

    .prologue
    .line 3794
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/df;

    .line 3795
    invoke-interface {p0, v0}, Landroid/support/v4/app/db;->a(Landroid/support/v4/app/ek;)V

    goto :goto_4

    .line 42
    :cond_14
    return-void
.end method

.method static synthetic a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
    .registers 9

    .prologue
    .line 3801
    if-eqz p1, :cond_13

    .line 3802
    instance-of v0, p1, Landroid/support/v4/app/dl;

    if-eqz v0, :cond_14

    .line 3803
    check-cast p1, Landroid/support/v4/app/dl;

    .line 3804
    iget-object v0, p1, Landroid/support/v4/app/dl;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Landroid/support/v4/app/dl;->g:Z

    iget-object v2, p1, Landroid/support/v4/app/dl;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Landroid/support/v4/app/dl;->a:Ljava/lang/CharSequence;

    invoke-static {p0, v0, v1, v2, v3}, Landroid/support/v4/app/et;->a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 3816
    :cond_13
    :goto_13
    return-void

    .line 3809
    :cond_14
    instance-of v0, p1, Landroid/support/v4/app/dt;

    if-eqz v0, :cond_26

    .line 3810
    check-cast p1, Landroid/support/v4/app/dt;

    .line 3811
    iget-object v0, p1, Landroid/support/v4/app/dt;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Landroid/support/v4/app/dt;->g:Z

    iget-object v2, p1, Landroid/support/v4/app/dt;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Landroid/support/v4/app/dt;->a:Ljava/util/ArrayList;

    invoke-static {p0, v0, v1, v2, v3}, Landroid/support/v4/app/et;->a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_13

    .line 3816
    :cond_26
    instance-of v0, p1, Landroid/support/v4/app/dk;

    if-eqz v0, :cond_13

    .line 3817
    check-cast p1, Landroid/support/v4/app/dk;

    .line 3818
    iget-object v1, p1, Landroid/support/v4/app/dk;->e:Ljava/lang/CharSequence;

    iget-boolean v2, p1, Landroid/support/v4/app/dk;->g:Z

    iget-object v3, p1, Landroid/support/v4/app/dk;->f:Ljava/lang/CharSequence;

    iget-object v4, p1, Landroid/support/v4/app/dk;->a:Landroid/graphics/Bitmap;

    iget-object v5, p1, Landroid/support/v4/app/dk;->b:Landroid/graphics/Bitmap;

    iget-boolean v6, p1, Landroid/support/v4/app/dk;->c:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v4/app/et;->a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    goto :goto_13
.end method

.method static synthetic a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/app/Notification;
    .registers 6

    .prologue
    .line 42
    .line 4272
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 4273
    instance-of v1, v0, [Landroid/app/Notification;

    if-nez v1, :cond_a

    if-nez v0, :cond_f

    .line 4274
    :cond_a
    check-cast v0, [Landroid/app/Notification;

    check-cast v0, [Landroid/app/Notification;

    :goto_e
    return-object v0

    .line 4276
    :cond_f
    array-length v1, v0

    new-array v3, v1, [Landroid/app/Notification;

    .line 4277
    const/4 v1, 0x0

    move v2, v1

    :goto_14
    array-length v1, v0

    if-ge v2, v1, :cond_21

    .line 4278
    aget-object v1, v0, v2

    check-cast v1, Landroid/app/Notification;

    aput-object v1, v3, v2

    .line 4277
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_14

    .line 4280
    :cond_21
    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    move-object v0, v3

    .line 42
    goto :goto_e
.end method

.method private static b(Landroid/app/Notification;)I
    .registers 2

    .prologue
    .line 3298
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    invoke-interface {v0, p0}, Landroid/support/v4/app/du;->b(Landroid/app/Notification;)I

    move-result v0

    return v0
.end method

.method private static b(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V
    .registers 4

    .prologue
    .line 794
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/df;

    .line 795
    invoke-interface {p0, v0}, Landroid/support/v4/app/db;->a(Landroid/support/v4/app/ek;)V

    goto :goto_4

    .line 797
    :cond_14
    return-void
.end method

.method private static b(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
    .registers 9

    .prologue
    .line 801
    if-eqz p1, :cond_13

    .line 802
    instance-of v0, p1, Landroid/support/v4/app/dl;

    if-eqz v0, :cond_14

    .line 803
    check-cast p1, Landroid/support/v4/app/dl;

    .line 804
    iget-object v0, p1, Landroid/support/v4/app/dl;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Landroid/support/v4/app/dl;->g:Z

    iget-object v2, p1, Landroid/support/v4/app/dl;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Landroid/support/v4/app/dl;->a:Ljava/lang/CharSequence;

    invoke-static {p0, v0, v1, v2, v3}, Landroid/support/v4/app/et;->a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 827
    :cond_13
    :goto_13
    return-void

    .line 809
    :cond_14
    instance-of v0, p1, Landroid/support/v4/app/dt;

    if-eqz v0, :cond_26

    .line 810
    check-cast p1, Landroid/support/v4/app/dt;

    .line 811
    iget-object v0, p1, Landroid/support/v4/app/dt;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Landroid/support/v4/app/dt;->g:Z

    iget-object v2, p1, Landroid/support/v4/app/dt;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Landroid/support/v4/app/dt;->a:Ljava/util/ArrayList;

    invoke-static {p0, v0, v1, v2, v3}, Landroid/support/v4/app/et;->a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_13

    .line 816
    :cond_26
    instance-of v0, p1, Landroid/support/v4/app/dk;

    if-eqz v0, :cond_13

    .line 817
    check-cast p1, Landroid/support/v4/app/dk;

    .line 818
    iget-object v1, p1, Landroid/support/v4/app/dk;->e:Ljava/lang/CharSequence;

    iget-boolean v2, p1, Landroid/support/v4/app/dk;->g:Z

    iget-object v3, p1, Landroid/support/v4/app/dk;->f:Ljava/lang/CharSequence;

    iget-object v4, p1, Landroid/support/v4/app/dk;->a:Landroid/graphics/Bitmap;

    iget-object v5, p1, Landroid/support/v4/app/dk;->b:Landroid/graphics/Bitmap;

    iget-boolean v6, p1, Landroid/support/v4/app/dk;->c:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v4/app/et;->a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    goto :goto_13
.end method

.method private static b(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/app/Notification;
    .registers 6

    .prologue
    .line 3272
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 3273
    instance-of v1, v0, [Landroid/app/Notification;

    if-nez v1, :cond_a

    if-nez v0, :cond_f

    .line 3274
    :cond_a
    check-cast v0, [Landroid/app/Notification;

    check-cast v0, [Landroid/app/Notification;

    .line 3281
    :goto_e
    return-object v0

    .line 3276
    :cond_f
    array-length v1, v0

    new-array v3, v1, [Landroid/app/Notification;

    .line 3277
    const/4 v1, 0x0

    move v2, v1

    :goto_14
    array-length v1, v0

    if-ge v2, v1, :cond_21

    .line 3278
    aget-object v1, v0, v2

    check-cast v1, Landroid/app/Notification;

    aput-object v1, v3, v2

    .line 3277
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_14

    .line 3280
    :cond_21
    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    move-object v0, v3

    .line 3281
    goto :goto_e
.end method

.method private static c(Landroid/app/Notification;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 3317
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    invoke-interface {v0, p0}, Landroid/support/v4/app/du;->c(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/app/Notification;)Z
    .registers 2

    .prologue
    .line 3327
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    invoke-interface {v0, p0}, Landroid/support/v4/app/du;->d(Landroid/app/Notification;)Z

    move-result v0

    return v0
.end method

.method private static e(Landroid/app/Notification;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 3335
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    invoke-interface {v0, p0}, Landroid/support/v4/app/du;->e(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static f(Landroid/app/Notification;)Z
    .registers 2

    .prologue
    .line 3345
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    invoke-interface {v0, p0}, Landroid/support/v4/app/du;->f(Landroid/app/Notification;)Z

    move-result v0

    return v0
.end method

.method private static g(Landroid/app/Notification;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 3361
    sget-object v0, Landroid/support/v4/app/dd;->ai:Landroid/support/v4/app/du;

    invoke-interface {v0, p0}, Landroid/support/v4/app/du;->g(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
