.class public final Landroid/support/v4/app/fa;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "android.support.useSideChannel"

.field public static final b:Ljava/lang/String; = "android.support.BIND_NOTIFICATION_SIDE_CHANNEL"

.field static final c:I = 0x13

.field private static final d:Ljava/lang/String; = "NotifManCompat"

.field private static final e:I = 0x3e8

.field private static final f:I = 0x6

.field private static final g:Ljava/lang/String; = "enabled_notification_listeners"

.field private static final h:I

.field private static final i:Ljava/lang/Object;

.field private static j:Ljava/lang/String;

.field private static k:Ljava/util/Set;

.field private static final n:Ljava/lang/Object;

.field private static o:Landroid/support/v4/app/fi;

.field private static final p:Landroid/support/v4/app/fc;


# instance fields
.field private final l:Landroid/content/Context;

.field private final m:Landroid/app/NotificationManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 88
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/app/fa;->i:Ljava/lang/Object;

    .line 92
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Landroid/support/v4/app/fa;->k:Ljava/util/Set;

    .line 97
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/app/fa;->n:Ljava/lang/Object;

    .line 165
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2b

    .line 166
    new-instance v0, Landroid/support/v4/app/ff;

    invoke-direct {v0}, Landroid/support/v4/app/ff;-><init>()V

    sput-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    .line 172
    :goto_22
    sget-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    invoke-interface {v0}, Landroid/support/v4/app/fc;->a()I

    move-result v0

    sput v0, Landroid/support/v4/app/fa;->h:I

    .line 173
    return-void

    .line 167
    :cond_2b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_38

    .line 168
    new-instance v0, Landroid/support/v4/app/fe;

    invoke-direct {v0}, Landroid/support/v4/app/fe;-><init>()V

    sput-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    goto :goto_22

    .line 170
    :cond_38
    new-instance v0, Landroid/support/v4/app/fd;

    invoke-direct {v0}, Landroid/support/v4/app/fd;-><init>()V

    sput-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    goto :goto_22
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Landroid/support/v4/app/fa;->l:Landroid/content/Context;

    .line 108
    iget-object v0, p0, Landroid/support/v4/app/fa;->l:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Landroid/support/v4/app/fa;->m:Landroid/app/NotificationManager;

    .line 110
    return-void
.end method

.method static synthetic a()I
    .registers 1

    .prologue
    .line 54
    sget v0, Landroid/support/v4/app/fa;->h:I

    return v0
.end method

.method public static a(Landroid/content/Context;)Ljava/util/Set;
    .registers 7

    .prologue
    .line 233
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_notification_listeners"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 237
    if-eqz v1, :cond_3e

    sget-object v0, Landroid/support/v4/app/fa;->j:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3e

    .line 239
    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 240
    new-instance v3, Ljava/util/HashSet;

    array-length v0, v2

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 241
    array-length v4, v2

    const/4 v0, 0x0

    :goto_22
    if-ge v0, v4, :cond_36

    aget-object v5, v2, v0

    .line 242
    invoke-static {v5}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v5

    .line 243
    if-eqz v5, :cond_33

    .line 244
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_33
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 247
    :cond_36
    sget-object v2, Landroid/support/v4/app/fa;->i:Ljava/lang/Object;

    monitor-enter v2

    .line 248
    :try_start_39
    sput-object v3, Landroid/support/v4/app/fa;->k:Ljava/util/Set;

    .line 249
    sput-object v1, Landroid/support/v4/app/fa;->j:Ljava/lang/String;

    .line 250
    monitor-exit v2
    :try_end_3e
    .catchall {:try_start_39 .. :try_end_3e} :catchall_41

    .line 252
    :cond_3e
    sget-object v0, Landroid/support/v4/app/fa;->k:Ljava/util/Set;

    return-object v0

    .line 250
    :catchall_41
    move-exception v0

    :try_start_42
    monitor-exit v2
    :try_end_43
    .catchall {:try_start_42 .. :try_end_43} :catchall_41

    throw v0
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 180
    .line 1189
    sget-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    iget-object v1, p0, Landroid/support/v4/app/fa;->m:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/app/fc;->a(Landroid/app/NotificationManager;I)V

    .line 1190
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_1b

    .line 1191
    new-instance v0, Landroid/support/v4/app/fb;

    iget-object v1, p0, Landroid/support/v4/app/fa;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/app/fb;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/fa;->a(Landroid/support/v4/app/fk;)V

    .line 181
    :cond_1b
    return-void
.end method

.method private a(ILandroid/app/Notification;)V
    .registers 5

    .prologue
    .line 209
    .line 1259
    invoke-static {p2}, Landroid/support/v4/app/dd;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    .line 1260
    if-eqz v0, :cond_27

    const-string v1, "android.support.useSideChannel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    .line 1219
    :goto_f
    if-eqz v0, :cond_29

    .line 1220
    new-instance v0, Landroid/support/v4/app/fg;

    iget-object v1, p0, Landroid/support/v4/app/fa;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Landroid/support/v4/app/fg;-><init>(Ljava/lang/String;ILandroid/app/Notification;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/fa;->a(Landroid/support/v4/app/fk;)V

    .line 1223
    sget-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    iget-object v1, p0, Landroid/support/v4/app/fa;->m:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/app/fc;->a(Landroid/app/NotificationManager;I)V

    :goto_26
    return-void

    .line 1260
    :cond_27
    const/4 v0, 0x0

    goto :goto_f

    .line 1225
    :cond_29
    sget-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    iget-object v1, p0, Landroid/support/v4/app/fa;->m:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/app/fc;->a(Landroid/app/NotificationManager;ILandroid/app/Notification;)V

    goto :goto_26
.end method

.method private a(Landroid/support/v4/app/fk;)V
    .registers 5

    .prologue
    .line 267
    sget-object v1, Landroid/support/v4/app/fa;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 268
    :try_start_3
    sget-object v0, Landroid/support/v4/app/fa;->o:Landroid/support/v4/app/fi;

    if-nez v0, :cond_14

    .line 269
    new-instance v0, Landroid/support/v4/app/fi;

    iget-object v2, p0, Landroid/support/v4/app/fa;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v4/app/fi;-><init>(Landroid/content/Context;)V

    sput-object v0, Landroid/support/v4/app/fa;->o:Landroid/support/v4/app/fi;

    .line 271
    :cond_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_22

    .line 272
    sget-object v0, Landroid/support/v4/app/fa;->o:Landroid/support/v4/app/fi;

    .line 2306
    iget-object v0, v0, Landroid/support/v4/app/fi;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 273
    return-void

    .line 271
    :catchall_22
    move-exception v0

    :try_start_23
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_22

    throw v0
.end method

.method private static a(Landroid/app/Notification;)Z
    .registers 3

    .prologue
    .line 259
    invoke-static {p0}, Landroid/support/v4/app/dd;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_10

    const-string v1, "android.support.useSideChannel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private static b(Landroid/content/Context;)Landroid/support/v4/app/fa;
    .registers 2

    .prologue
    .line 103
    new-instance v0, Landroid/support/v4/app/fa;

    invoke-direct {v0, p0}, Landroid/support/v4/app/fa;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 197
    iget-object v0, p0, Landroid/support/v4/app/fa;->m:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 198
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_19

    .line 199
    new-instance v0, Landroid/support/v4/app/fb;

    iget-object v1, p0, Landroid/support/v4/app/fa;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/fb;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/fa;->a(Landroid/support/v4/app/fk;)V

    .line 201
    :cond_19
    return-void
.end method

.method private b(I)V
    .registers 4

    .prologue
    .line 189
    sget-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    iget-object v1, p0, Landroid/support/v4/app/fa;->m:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/app/fc;->a(Landroid/app/NotificationManager;I)V

    .line 190
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_1b

    .line 191
    new-instance v0, Landroid/support/v4/app/fb;

    iget-object v1, p0, Landroid/support/v4/app/fa;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/app/fb;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/fa;->a(Landroid/support/v4/app/fk;)V

    .line 193
    :cond_1b
    return-void
.end method

.method private b(ILandroid/app/Notification;)V
    .registers 5

    .prologue
    .line 219
    .line 2259
    invoke-static {p2}, Landroid/support/v4/app/dd;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    .line 2260
    if-eqz v0, :cond_27

    const-string v1, "android.support.useSideChannel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    .line 219
    :goto_f
    if-eqz v0, :cond_29

    .line 220
    new-instance v0, Landroid/support/v4/app/fg;

    iget-object v1, p0, Landroid/support/v4/app/fa;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Landroid/support/v4/app/fg;-><init>(Ljava/lang/String;ILandroid/app/Notification;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/fa;->a(Landroid/support/v4/app/fk;)V

    .line 223
    sget-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    iget-object v1, p0, Landroid/support/v4/app/fa;->m:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/app/fc;->a(Landroid/app/NotificationManager;I)V

    .line 227
    :goto_26
    return-void

    .line 2260
    :cond_27
    const/4 v0, 0x0

    goto :goto_f

    .line 225
    :cond_29
    sget-object v0, Landroid/support/v4/app/fa;->p:Landroid/support/v4/app/fc;

    iget-object v1, p0, Landroid/support/v4/app/fa;->m:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/app/fc;->a(Landroid/app/NotificationManager;ILandroid/app/Notification;)V

    goto :goto_26
.end method
