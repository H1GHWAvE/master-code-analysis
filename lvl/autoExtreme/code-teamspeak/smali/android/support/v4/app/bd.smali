.class final Landroid/support/v4/app/bd;
.super Landroid/support/v4/app/bh;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v4/app/bb;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/bb;)V
    .registers 2

    .prologue
    .line 840
    iput-object p1, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    .line 841
    invoke-direct {p0, p1}, Landroid/support/v4/app/bh;-><init>(Landroid/support/v4/app/bb;)V

    .line 842
    return-void
.end method

.method private j()Landroid/support/v4/app/bb;
    .registers 2

    .prologue
    .line 861
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .registers 3
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 906
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .registers 5

    .prologue
    .line 871
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/bb;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 872
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
    .registers 5
    .param p1    # Landroid/support/v4/app/Fragment;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p2    # [Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 877
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-static {v0, p1, p2, p3}, Landroid/support/v4/app/bb;->a(Landroid/support/v4/app/bb;Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V

    .line 879
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 846
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2, p3}, Landroid/support/v4/app/bb;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 847
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 911
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 912
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final a(Ljava/lang/String;)Z
    .registers 5
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 883
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    .line 1362
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_d

    .line 2037
    invoke-virtual {v0, p1}, Landroid/app/Activity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v0

    .line 1363
    :goto_c
    return v0

    .line 1365
    :cond_d
    const/4 v0, 0x0

    .line 883
    goto :goto_c
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 851
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final c()Landroid/view/LayoutInflater;
    .registers 3

    .prologue
    .line 856
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 866
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->b()V

    .line 867
    return-void
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 889
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 894
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 895
    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return v0

    :cond_a
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    goto :goto_9
.end method

.method public final g()V
    .registers 1

    .prologue
    .line 901
    return-void
.end method

.method public final bridge synthetic h()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 839
    .line 2861
    iget-object v0, p0, Landroid/support/v4/app/bd;->a:Landroid/support/v4/app/bb;

    .line 839
    return-object v0
.end method
