.class final Landroid/support/v4/app/ak;
.super Landroid/support/v4/app/cd;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/bj;
.implements Ljava/lang/Runnable;


# static fields
.field static final a:Ljava/lang/String; = "FragmentManager"

.field static final b:Z

.field static final d:I = 0x0

.field static final e:I = 0x1

.field static final f:I = 0x2

.field static final g:I = 0x3

.field static final h:I = 0x4

.field static final i:I = 0x5

.field static final j:I = 0x6

.field static final k:I = 0x7


# instance fields
.field A:Ljava/lang/CharSequence;

.field B:I

.field C:Ljava/lang/CharSequence;

.field D:Ljava/util/ArrayList;

.field E:Ljava/util/ArrayList;

.field final c:Landroid/support/v4/app/bl;

.field l:Landroid/support/v4/app/ao;

.field m:Landroid/support/v4/app/ao;

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:I

.field t:I

.field u:Z

.field v:Z

.field w:Ljava/lang/String;

.field x:Z

.field y:I

.field z:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 191
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_7
    sput-boolean v0, Landroid/support/v4/app/ak;->b:Z

    return-void

    :cond_a
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public constructor <init>(Landroid/support/v4/app/bl;)V
    .registers 3

    .prologue
    .line 353
    invoke-direct {p0}, Landroid/support/v4/app/cd;-><init>()V

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ak;->v:Z

    .line 229
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/ak;->y:I

    .line 354
    iput-object p1, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    .line 355
    return-void
.end method

.method private a(Z)I
    .registers 5

    .prologue
    .line 621
    iget-boolean v0, p0, Landroid/support/v4/app/ak;->x:Z

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "commit already called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 622
    :cond_c
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_35

    .line 623
    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Commit: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    new-instance v0, Landroid/support/v4/n/h;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Landroid/support/v4/n/h;-><init>(Ljava/lang/String;)V

    .line 625
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 626
    const-string v0, "  "

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/ak;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 628
    :cond_35
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ak;->x:Z

    .line 629
    iget-boolean v0, p0, Landroid/support/v4/app/ak;->u:Z

    if-eqz v0, :cond_4c

    .line 630
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p0}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/ak;)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ak;->y:I

    .line 634
    :goto_44
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/bl;->a(Ljava/lang/Runnable;Z)V

    .line 635
    iget v0, p0, Landroid/support/v4/app/ak;->y:I

    return v0

    .line 632
    :cond_4c
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/ak;->y:I

    goto :goto_44
.end method

.method private a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/ap;
    .registers 14

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 1019
    new-instance v2, Landroid/support/v4/app/ap;

    invoke-direct {v2, p0}, Landroid/support/v4/app/ap;-><init>(Landroid/support/v4/app/ak;)V

    .line 1024
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v1, v1, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 6166
    iget-object v1, v1, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 1024
    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, v2, Landroid/support/v4/app/ap;->d:Landroid/view/View;

    move v6, v7

    move v8, v7

    .line 1028
    :goto_16
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v6, v0, :cond_30

    .line 1029
    invoke-virtual {p1, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    move-object v0, p0

    move v3, p3

    move-object v4, p1

    move-object v5, p2

    .line 1030
    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/ak;->a(ILandroid/support/v4/app/ap;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z

    move-result v0

    if-eqz v0, :cond_52

    move v1, v9

    .line 1028
    :goto_2b
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v8, v1

    goto :goto_16

    .line 1037
    :cond_30
    :goto_30
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v7, v0, :cond_4e

    .line 1038
    invoke-virtual {p2, v7}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 1039
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_4b

    move-object v0, p0

    move v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/ak;->a(ILandroid/support/v4/app/ap;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z

    move-result v0

    if-eqz v0, :cond_4b

    move v8, v9

    .line 1037
    :cond_4b
    add-int/lit8 v7, v7, 0x1

    goto :goto_30

    .line 1046
    :cond_4e
    if-nez v8, :cond_51

    .line 1047
    const/4 v2, 0x0

    .line 1050
    :cond_51
    return-object v2

    :cond_52
    move v1, v8

    goto :goto_2b
.end method

.method static synthetic a(Landroid/support/v4/app/ak;Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;)Landroid/support/v4/n/a;
    .registers 8

    .prologue
    const/4 v3, 0x1

    .line 188
    .line 34341
    new-instance v0, Landroid/support/v4/n/a;

    invoke-direct {v0}, Landroid/support/v4/n/a;-><init>()V

    .line 35237
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 34343
    if-eqz v1, :cond_1b

    .line 34344
    iget-object v2, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    if-eqz v2, :cond_1b

    .line 34345
    invoke-static {v0, v1}, Landroid/support/v4/app/ce;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 34346
    if-eqz p2, :cond_27

    .line 34347
    iget-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/ak;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;

    move-result-object v0

    .line 34290
    :cond_1b
    :goto_1b
    if-eqz p2, :cond_2d

    .line 34291
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->as:Landroid/support/v4/app/gj;

    if-eqz v1, :cond_23

    .line 34292
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->as:Landroid/support/v4/app/gj;

    .line 34295
    :cond_23
    invoke-direct {p0, p1, v0, v3}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V

    .line 188
    :goto_26
    return-object v0

    .line 34350
    :cond_27
    iget-object v1, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    .line 36161
    invoke-static {v0, v1}, Landroid/support/v4/n/k;->c(Ljava/util/Map;Ljava/util/Collection;)Z

    goto :goto_1b

    .line 34297
    :cond_2d
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    if-eqz v1, :cond_33

    .line 34298
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    .line 34301
    :cond_33
    invoke-static {p1, v0, v3}, Landroid/support/v4/app/ak;->b(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V

    goto :goto_26
.end method

.method private a(Landroid/support/v4/app/ap;Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/n/a;
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 1090
    new-instance v0, Landroid/support/v4/n/a;

    invoke-direct {v0}, Landroid/support/v4/n/a;-><init>()V

    .line 1091
    iget-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    if-eqz v1, :cond_16

    .line 13237
    iget-object v1, p2, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 1092
    invoke-static {v0, v1}, Landroid/support/v4/app/ce;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 1093
    if-eqz p3, :cond_22

    .line 1094
    iget-object v1, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    .line 14161
    invoke-static {v0, v1}, Landroid/support/v4/n/k;->c(Ljava/util/Map;Ljava/util/Collection;)Z

    .line 1101
    :cond_16
    :goto_16
    if-eqz p3, :cond_2b

    .line 1102
    iget-object v1, p2, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    if-eqz v1, :cond_1e

    .line 1103
    iget-object v1, p2, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    .line 1106
    :cond_1e
    invoke-direct {p0, p1, v0, v3}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V

    .line 1115
    :goto_21
    return-object v0

    .line 1096
    :cond_22
    iget-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/ak;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;

    move-result-object v0

    goto :goto_16

    .line 1108
    :cond_2b
    iget-object v1, p2, Landroid/support/v4/app/Fragment;->as:Landroid/support/v4/app/gj;

    if-eqz v1, :cond_31

    .line 1109
    iget-object v1, p2, Landroid/support/v4/app/Fragment;->as:Landroid/support/v4/app/gj;

    .line 1112
    :cond_31
    invoke-static {p1, v0, v3}, Landroid/support/v4/app/ak;->b(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V

    goto :goto_21
.end method

.method private a(Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;)Landroid/support/v4/n/a;
    .registers 8

    .prologue
    const/4 v3, 0x1

    .line 1286
    .line 29341
    new-instance v0, Landroid/support/v4/n/a;

    invoke-direct {v0}, Landroid/support/v4/n/a;-><init>()V

    .line 30237
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 29343
    if-eqz v1, :cond_1b

    .line 29344
    iget-object v2, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    if-eqz v2, :cond_1b

    .line 29345
    invoke-static {v0, v1}, Landroid/support/v4/app/ce;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 29346
    if-eqz p2, :cond_27

    .line 29347
    iget-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/ak;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;

    move-result-object v0

    .line 1290
    :cond_1b
    :goto_1b
    if-eqz p2, :cond_2d

    .line 1291
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->as:Landroid/support/v4/app/gj;

    if-eqz v1, :cond_23

    .line 1292
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->as:Landroid/support/v4/app/gj;

    .line 1295
    :cond_23
    invoke-direct {p0, p1, v0, v3}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V

    .line 1303
    :goto_26
    return-object v0

    .line 29350
    :cond_27
    iget-object v1, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    .line 31161
    invoke-static {v0, v1}, Landroid/support/v4/n/k;->c(Ljava/util/Map;Ljava/util/Collection;)Z

    goto :goto_1b

    .line 1297
    :cond_2d
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    if-eqz v1, :cond_33

    .line 1298
    iget-object v1, p3, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    .line 1301
    :cond_33
    invoke-static {p1, v0, v3}, Landroid/support/v4/app/ak;->b(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V

    goto :goto_26
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;
    .registers 8

    .prologue
    .line 1316
    invoke-virtual {p2}, Landroid/support/v4/n/a;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1327
    :goto_6
    return-object p2

    .line 1319
    :cond_7
    new-instance v1, Landroid/support/v4/n/a;

    invoke-direct {v1}, Landroid/support/v4/n/a;-><init>()V

    .line 1320
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1321
    const/4 v0, 0x0

    move v2, v0

    :goto_12
    if-ge v2, v3, :cond_2b

    .line 1322
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1323
    if-eqz v0, :cond_27

    .line 1324
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321
    :cond_27
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_12

    :cond_2b
    move-object p2, v1

    .line 1327
    goto :goto_6
.end method

.method private static a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1071
    if-eqz p0, :cond_5

    if-nez p1, :cond_7

    :cond_5
    move-object v0, v1

    .line 12088
    :goto_6
    return-object v0

    .line 1074
    :cond_7
    if-eqz p2, :cond_15

    .line 9767
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ao:Ljava/lang/Object;

    sget-object v2, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-eq v0, v2, :cond_16

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ao:Ljava/lang/Object;

    .line 12083
    :goto_11
    if-nez v0, :cond_19

    move-object v0, v1

    .line 12084
    goto :goto_6

    :cond_15
    move-object p1, p0

    .line 11735
    :cond_16
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->an:Ljava/lang/Object;

    goto :goto_11

    .line 12086
    :cond_19
    check-cast v0, Landroid/transition/Transition;

    .line 12087
    if-nez v0, :cond_1f

    move-object v0, v1

    .line 12088
    goto :goto_6

    .line 12090
    :cond_1f
    new-instance v1, Landroid/transition/TransitionSet;

    invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V

    .line 12091
    invoke-virtual {v1, v0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-object v0, v1

    .line 1074
    goto :goto_6
.end method

.method private static a(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1054
    if-nez p0, :cond_4

    .line 1055
    const/4 v0, 0x0

    .line 1057
    :goto_3
    return-object v0

    :cond_4
    if-eqz p1, :cond_16

    .line 6708
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->am:Ljava/lang/Object;

    sget-object v1, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-ne v0, v1, :cond_13

    .line 7675
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->al:Ljava/lang/Object;

    .line 1057
    :goto_e
    invoke-static {v0}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 6708
    :cond_13
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->am:Ljava/lang/Object;

    goto :goto_e

    .line 8607
    :cond_16
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->aj:Ljava/lang/Object;

    goto :goto_e
.end method

.method private static a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;Ljava/util/ArrayList;Landroid/support/v4/n/a;Landroid/view/View;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 1081
    if-eqz p0, :cond_19

    .line 12237
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 13045
    if-eqz p0, :cond_19

    .line 13046
    invoke-static {p2, v0}, Landroid/support/v4/app/ce;->a(Ljava/util/ArrayList;Landroid/view/View;)V

    .line 13047
    if-eqz p3, :cond_12

    .line 13048
    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 13050
    :cond_12
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 13051
    const/4 p0, 0x0

    .line 1085
    :cond_19
    :goto_19
    return-object p0

    .line 13053
    :cond_1a
    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    .line 13054
    check-cast v0, Landroid/transition/Transition;

    invoke-static {v0, p2}, Landroid/support/v4/app/ce;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    goto :goto_19
.end method

.method private a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
    .registers 8

    .prologue
    .line 414
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iput-object v0, p2, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 416
    if-eqz p3, :cond_3f

    .line 417
    iget-object v0, p2, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    if-eqz v0, :cond_3d

    iget-object v0, p2, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3d

    .line 418
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t change tag of fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_3d
    iput-object p3, p2, Landroid/support/v4/app/Fragment;->S:Ljava/lang/String;

    .line 425
    :cond_3f
    if-eqz p1, :cond_78

    .line 426
    iget v0, p2, Landroid/support/v4/app/Fragment;->Q:I

    if-eqz v0, :cond_74

    iget v0, p2, Landroid/support/v4/app/Fragment;->Q:I

    if-eq v0, p1, :cond_74

    .line 427
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t change container ID of fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/support/v4/app/Fragment;->Q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 431
    :cond_74
    iput p1, p2, Landroid/support/v4/app/Fragment;->Q:I

    iput p1, p2, Landroid/support/v4/app/Fragment;->R:I

    .line 434
    :cond_78
    new-instance v0, Landroid/support/v4/app/ao;

    invoke-direct {v0}, Landroid/support/v4/app/ao;-><init>()V

    .line 435
    iput p4, v0, Landroid/support/v4/app/ao;->c:I

    .line 436
    iput-object p2, v0, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 437
    invoke-virtual {p0, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ao;)V

    .line 438
    return-void
.end method

.method static synthetic a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLandroid/support/v4/n/a;)V
    .registers 6

    .prologue
    .line 188
    .line 37262
    if-eqz p2, :cond_19

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    .line 37265
    :goto_4
    if-eqz v0, :cond_18

    .line 37266
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p3}, Landroid/support/v4/n/a;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 37267
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p3}, Landroid/support/v4/n/a;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 188
    :cond_18
    return-void

    .line 37262
    :cond_19
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    goto :goto_4
.end method

.method static synthetic a(Landroid/support/v4/app/ak;Landroid/support/v4/app/ap;ILjava/lang/Object;)V
    .registers 4

    .prologue
    .line 188
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ap;ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v4/app/ak;Landroid/support/v4/n/a;Landroid/support/v4/app/ap;)V
    .registers 5

    .prologue
    .line 188
    .line 36273
    iget-object v0, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    if-eqz v0, :cond_1d

    invoke-virtual {p1}, Landroid/support/v4/n/a;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 36275
    iget-object v0, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 36277
    if-eqz v0, :cond_1d

    .line 36278
    iget-object v1, p2, Landroid/support/v4/app/ap;->c:Landroid/support/v4/app/cj;

    iput-object v0, v1, Landroid/support/v4/app/cj;->a:Landroid/view/View;

    .line 188
    :cond_1d
    return-void
.end method

.method private a(Landroid/support/v4/app/ap;ILjava/lang/Object;)V
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 1370
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_54

    move v1, v2

    .line 1371
    :goto_8
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_54

    .line 1372
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1373
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    if-eqz v3, :cond_43

    iget-object v3, v0, Landroid/support/v4/app/Fragment;->ab:Landroid/view/ViewGroup;

    if-eqz v3, :cond_43

    iget v3, v0, Landroid/support/v4/app/Fragment;->R:I

    if-ne v3, p2, :cond_43

    .line 1375
    iget-boolean v3, v0, Landroid/support/v4/app/Fragment;->T:Z

    if-eqz v3, :cond_47

    .line 1376
    iget-object v3, p1, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;

    iget-object v4, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_43

    .line 1377
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    const/4 v4, 0x1

    invoke-static {p3, v3, v4}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 1379
    iget-object v3, p1, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;

    iget-object v0, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1371
    :cond_43
    :goto_43
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 1382
    :cond_47
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-static {p3, v3, v2}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 1384
    iget-object v3, p1, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;

    iget-object v0, v0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_43

    .line 1389
    :cond_54
    return-void
.end method

.method private a(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 1417
    iget-object v1, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    if-nez v1, :cond_30

    move v2, v0

    :goto_6
    move v3, v0

    .line 1418
    :goto_7
    if-ge v3, v2, :cond_3e

    .line 1419
    iget-object v0, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1420
    iget-object v1, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1421
    invoke-virtual {p2, v1}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1422
    if-eqz v1, :cond_2c

    .line 33033
    invoke-virtual {v1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v1

    .line 1424
    if-eqz p3, :cond_38

    .line 1425
    iget-object v4, p1, Landroid/support/v4/app/ap;->a:Landroid/support/v4/n/a;

    invoke-static {v4, v0, v1}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    :cond_2c
    :goto_2c
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    .line 1417
    :cond_30
    iget-object v1, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    goto :goto_6

    .line 1427
    :cond_38
    iget-object v4, p1, Landroid/support/v4/app/ap;->a:Landroid/support/v4/n/a;

    invoke-static {v4, v1, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2c

    .line 1431
    :cond_3e
    return-void
.end method

.method private a(Landroid/support/v4/app/ap;Landroid/view/View;Ljava/lang/Object;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLjava/util/ArrayList;)V
    .registers 18

    .prologue
    .line 1233
    invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v9

    new-instance v0, Landroid/support/v4/app/am;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p7

    move-object v5, p1

    move/from16 v6, p6

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Landroid/support/v4/app/am;-><init>(Landroid/support/v4/app/ak;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v9, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1258
    return-void
.end method

.method private static a(Landroid/support/v4/app/ap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 7

    .prologue
    .line 1406
    if-eqz p1, :cond_1f

    .line 1407
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1f

    .line 1408
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1409
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1410
    iget-object v3, p0, Landroid/support/v4/app/ap;->a:Landroid/support/v4/n/a;

    invoke-static {v3, v0, v1}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V

    .line 1407
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1413
    :cond_1f
    return-void
.end method

.method private a(Landroid/support/v4/n/a;Landroid/support/v4/app/ap;)V
    .registers 5

    .prologue
    .line 1273
    iget-object v0, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    if-eqz v0, :cond_1d

    invoke-virtual {p1}, Landroid/support/v4/n/a;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 1275
    iget-object v0, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1277
    if-eqz v0, :cond_1d

    .line 1278
    iget-object v1, p2, Landroid/support/v4/app/ap;->c:Landroid/support/v4/app/cj;

    iput-object v0, v1, Landroid/support/v4/app/cj;->a:Landroid/view/View;

    .line 1281
    :cond_1d
    return-void
.end method

.method private static a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 1393
    if-eqz p1, :cond_18

    if-eqz p2, :cond_18

    .line 1394
    const/4 v0, 0x0

    :goto_5
    invoke-virtual {p0}, Landroid/support/v4/n/a;->size()I

    move-result v1

    if-ge v0, v1, :cond_1c

    .line 1395
    invoke-virtual {p0, v0}, Landroid/support/v4/n/a;->c(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1396
    invoke-virtual {p0, v0, p2}, Landroid/support/v4/n/a;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1402
    :cond_18
    :goto_18
    return-void

    .line 1394
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1400
    :cond_1c
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_18
.end method

.method private static a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
    .registers 4

    .prologue
    .line 746
    if-eqz p1, :cond_1d

    .line 747
    iget v0, p1, Landroid/support/v4/app/Fragment;->R:I

    .line 748
    if-eqz v0, :cond_1d

    .line 4781
    iget-boolean v1, p1, Landroid/support/v4/app/Fragment;->T:Z

    .line 748
    if-nez v1, :cond_1d

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->k()Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 5237
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 748
    if-eqz v1, :cond_1d

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1d

    .line 750
    invoke-virtual {p0, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 753
    :cond_1d
    return-void
.end method

.method private a(Landroid/view/View;Landroid/support/v4/app/ap;ILjava/lang/Object;)V
    .registers 12

    .prologue
    .line 1359
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v6

    new-instance v0, Landroid/support/v4/app/an;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/an;-><init>(Landroid/support/v4/app/ak;Landroid/view/View;Landroid/support/v4/app/ap;ILjava/lang/Object;)V

    invoke-virtual {v6, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1367
    return-void
.end method

.method private a(ILandroid/support/v4/app/ap;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
    .registers 34

    .prologue
    .line 1132
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v3, v3, Landroid/support/v4/app/bl;->w:Landroid/support/v4/app/bf;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/support/v4/app/bf;->a(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 1133
    if-nez v5, :cond_12

    .line 1134
    const/4 v3, 0x0

    .line 1226
    :goto_11
    return v3

    .line 1136
    :cond_12
    move-object/from16 v0, p5

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/support/v4/app/Fragment;

    .line 1137
    move-object/from16 v0, p4

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/support/v4/app/Fragment;

    .line 15054
    if-nez v10, :cond_59

    .line 15055
    const/16 v16, 0x0

    .line 18071
    :goto_2a
    if-eqz v10, :cond_2e

    if-nez v11, :cond_6e

    .line 18072
    :cond_2e
    const/4 v6, 0x0

    .line 22062
    :goto_2f
    if-nez v11, :cond_91

    .line 22063
    const/4 v3, 0x0

    move-object v12, v3

    .line 1143
    :goto_33
    const/4 v3, 0x0

    .line 1144
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1145
    if-eqz v6, :cond_249

    .line 1146
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v11, v2}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ap;Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/n/a;

    move-result-object v13

    .line 1147
    invoke-virtual {v13}, Landroid/support/v4/n/a;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_a7

    .line 1148
    const/4 v6, 0x0

    .line 1149
    const/4 v3, 0x0

    move-object/from16 v18, v3

    move-object/from16 v17, v6

    .line 1164
    :goto_51
    if-nez v16, :cond_da

    if-nez v17, :cond_da

    if-nez v12, :cond_da

    .line 1166
    const/4 v3, 0x0

    goto :goto_11

    .line 15057
    :cond_59
    if-eqz p3, :cond_6b

    .line 15708
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->am:Ljava/lang/Object;

    sget-object v4, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-ne v3, v4, :cond_68

    .line 16675
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->al:Ljava/lang/Object;

    .line 15057
    :goto_63
    invoke-static {v3}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    goto :goto_2a

    .line 15708
    :cond_68
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->am:Ljava/lang/Object;

    goto :goto_63

    .line 17607
    :cond_6b
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->aj:Ljava/lang/Object;

    goto :goto_63

    .line 18074
    :cond_6e
    if-eqz p3, :cond_7f

    .line 18767
    iget-object v3, v11, Landroid/support/v4/app/Fragment;->ao:Ljava/lang/Object;

    sget-object v4, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-ne v3, v4, :cond_7c

    .line 19735
    iget-object v3, v11, Landroid/support/v4/app/Fragment;->an:Ljava/lang/Object;

    .line 21083
    :goto_78
    if-nez v3, :cond_82

    .line 21084
    const/4 v6, 0x0

    goto :goto_2f

    .line 18767
    :cond_7c
    iget-object v3, v11, Landroid/support/v4/app/Fragment;->ao:Ljava/lang/Object;

    goto :goto_78

    .line 20735
    :cond_7f
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->an:Ljava/lang/Object;

    goto :goto_78

    .line 21086
    :cond_82
    check-cast v3, Landroid/transition/Transition;

    .line 21087
    if-nez v3, :cond_88

    .line 21088
    const/4 v6, 0x0

    goto :goto_2f

    .line 21090
    :cond_88
    new-instance v6, Landroid/transition/TransitionSet;

    invoke-direct {v6}, Landroid/transition/TransitionSet;-><init>()V

    .line 21091
    invoke-virtual {v6, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    goto :goto_2f

    .line 22065
    :cond_91
    if-eqz p3, :cond_a4

    .line 22641
    iget-object v3, v11, Landroid/support/v4/app/Fragment;->ak:Ljava/lang/Object;

    sget-object v4, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-ne v3, v4, :cond_a1

    .line 23607
    iget-object v3, v11, Landroid/support/v4/app/Fragment;->aj:Ljava/lang/Object;

    .line 22065
    :goto_9b
    invoke-static {v3}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    goto :goto_33

    .line 22641
    :cond_a1
    iget-object v3, v11, Landroid/support/v4/app/Fragment;->ak:Ljava/lang/Object;

    goto :goto_9b

    .line 23675
    :cond_a4
    iget-object v3, v11, Landroid/support/v4/app/Fragment;->al:Ljava/lang/Object;

    goto :goto_9b

    .line 1152
    :cond_a7
    if-eqz p3, :cond_d7

    iget-object v3, v11, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    .line 1155
    :goto_ab
    if-eqz v3, :cond_bf

    .line 1156
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v13}, Landroid/support/v4/n/a;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1157
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v13}, Landroid/support/v4/n/a;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 24233
    :cond_bf
    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v14

    new-instance v3, Landroid/support/v4/app/am;

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    move/from16 v9, p3

    invoke-direct/range {v3 .. v11}, Landroid/support/v4/app/am;-><init>(Landroid/support/v4/app/ak;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v14, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    move-object/from16 v18, v13

    move-object/from16 v17, v6

    goto/16 :goto_51

    .line 1152
    :cond_d7
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    goto :goto_ab

    .line 1169
    :cond_da
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 1170
    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/support/v4/app/ap;->d:Landroid/view/View;

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-static {v12, v11, v0, v1, v3}, Landroid/support/v4/app/ak;->a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;Ljava/util/ArrayList;Landroid/support/v4/n/a;Landroid/view/View;)Ljava/lang/Object;

    move-result-object v6

    .line 1174
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    if-eqz v3, :cond_112

    if-eqz v18, :cond_112

    .line 1175
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 1176
    if-eqz v3, :cond_112

    .line 1177
    if-eqz v6, :cond_10b

    .line 1178
    invoke-static {v6, v3}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 1180
    :cond_10b
    if-eqz v17, :cond_112

    .line 1181
    move-object/from16 v0, v17

    invoke-static {v0, v3}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 1187
    :cond_112
    new-instance v12, Landroid/support/v4/app/al;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v10}, Landroid/support/v4/app/al;-><init>(Landroid/support/v4/app/ak;Landroid/support/v4/app/Fragment;)V

    .line 1195
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1196
    new-instance v14, Landroid/support/v4/n/a;

    invoke-direct {v14}, Landroid/support/v4/n/a;-><init>()V

    .line 1198
    const/4 v3, 0x1

    .line 1199
    if-eqz v10, :cond_246

    .line 1200
    if-eqz p3, :cond_1fe

    .line 24816
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->ap:Ljava/lang/Boolean;

    if-nez v3, :cond_1f6

    const/4 v3, 0x1

    :goto_12d
    move v9, v3

    .line 26162
    :goto_12e
    const/4 v10, 0x1

    move-object/from16 v3, v16

    .line 26163
    check-cast v3, Landroid/transition/Transition;

    move-object v4, v6

    .line 26164
    check-cast v4, Landroid/transition/Transition;

    move-object/from16 v8, v17

    .line 26165
    check-cast v8, Landroid/transition/Transition;

    .line 26167
    if-eqz v3, :cond_243

    if-eqz v4, :cond_243

    .line 26176
    :goto_13e
    if-eqz v9, :cond_20d

    .line 26178
    new-instance v9, Landroid/transition/TransitionSet;

    invoke-direct {v9}, Landroid/transition/TransitionSet;-><init>()V

    .line 26179
    if-eqz v3, :cond_14a

    .line 26180
    invoke-virtual {v9, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 26182
    :cond_14a
    if-eqz v4, :cond_14f

    .line 26183
    invoke-virtual {v9, v4}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 26185
    :cond_14f
    if-eqz v8, :cond_154

    .line 26186
    invoke-virtual {v9, v8}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    :cond_154
    move-object v4, v9

    .line 1206
    :goto_155
    if-eqz v4, :cond_1f1

    .line 1207
    move-object/from16 v0, p2

    iget-object v11, v0, Landroid/support/v4/app/ap;->d:Landroid/view/View;

    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/support/v4/app/ap;->c:Landroid/support/v4/app/cj;

    move-object/from16 v0, p2

    iget-object v13, v0, Landroid/support/v4/app/ap;->a:Landroid/support/v4/n/a;

    .line 27112
    if-nez v16, :cond_167

    if-eqz v17, :cond_192

    :cond_167
    move-object/from16 v10, v16

    .line 27113
    check-cast v10, Landroid/transition/Transition;

    .line 27114
    if-eqz v10, :cond_170

    .line 27115
    invoke-virtual {v10, v11}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 27117
    :cond_170
    if-eqz v17, :cond_179

    .line 27118
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v11, v1, v7}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V

    .line 27123
    :cond_179
    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v18

    new-instance v8, Landroid/support/v4/app/cg;

    move-object v9, v5

    invoke-direct/range {v8 .. v15}, Landroid/support/v4/app/cg;-><init>(Landroid/view/View;Landroid/transition/Transition;Landroid/view/View;Landroid/support/v4/app/ck;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 27281
    if-eqz v10, :cond_192

    .line 27282
    new-instance v8, Landroid/support/v4/app/ch;

    invoke-direct {v8, v3}, Landroid/support/v4/app/ch;-><init>(Landroid/support/v4/app/cj;)V

    invoke-virtual {v10, v8}, Landroid/transition/Transition;->setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V

    .line 27359
    :cond_192
    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v8, Landroid/support/v4/app/an;

    move-object/from16 v9, p0

    move-object v10, v5

    move-object/from16 v11, p2

    move/from16 v12, p1

    move-object v13, v4

    invoke-direct/range {v8 .. v13}, Landroid/support/v4/app/an;-><init>(Landroid/support/v4/app/ak;Landroid/view/View;Landroid/support/v4/app/ap;ILjava/lang/Object;)V

    invoke-virtual {v3, v8}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1215
    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/support/v4/app/ap;->d:Landroid/view/View;

    const/4 v8, 0x1

    invoke-static {v4, v3, v8}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 1217
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ap;ILjava/lang/Object;)V

    move-object v3, v4

    .line 28066
    check-cast v3, Landroid/transition/Transition;

    .line 28067
    invoke-static {v5, v3}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V

    .line 1221
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/v4/app/ap;->d:Landroid/view/View;

    move-object/from16 v27, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v18, v16

    .line 28347
    check-cast v18, Landroid/transition/Transition;

    move-object/from16 v20, v6

    .line 28348
    check-cast v20, Landroid/transition/Transition;

    move-object/from16 v22, v17

    .line 28349
    check-cast v22, Landroid/transition/Transition;

    move-object/from16 v26, v4

    .line 28350
    check-cast v26, Landroid/transition/Transition;

    .line 28351
    if-eqz v26, :cond_1f1

    .line 28352
    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v16, Landroid/support/v4/app/ci;

    move-object/from16 v17, v5

    move-object/from16 v19, v15

    move-object/from16 v23, v7

    move-object/from16 v24, v14

    invoke-direct/range {v16 .. v27}, Landroid/support/v4/app/ci;-><init>(Landroid/view/View;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;Landroid/transition/Transition;Landroid/view/View;)V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1226
    :cond_1f1
    if-eqz v4, :cond_240

    const/4 v3, 0x1

    goto/16 :goto_11

    .line 24816
    :cond_1f6
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->ap:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    goto/16 :goto_12d

    .line 25792
    :cond_1fe
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->aq:Ljava/lang/Boolean;

    if-nez v3, :cond_205

    const/4 v3, 0x1

    goto/16 :goto_12d

    :cond_205
    iget-object v3, v10, Landroid/support/v4/app/Fragment;->aq:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    goto/16 :goto_12d

    .line 26192
    :cond_20d
    const/4 v9, 0x0

    .line 26193
    if-eqz v4, :cond_235

    if-eqz v3, :cond_235

    .line 26194
    new-instance v9, Landroid/transition/TransitionSet;

    invoke-direct {v9}, Landroid/transition/TransitionSet;-><init>()V

    invoke-virtual {v9, v4}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    move-result-object v3

    .line 26203
    :cond_224
    :goto_224
    if-eqz v8, :cond_23d

    .line 26204
    new-instance v4, Landroid/transition/TransitionSet;

    invoke-direct {v4}, Landroid/transition/TransitionSet;-><init>()V

    .line 26205
    if-eqz v3, :cond_230

    .line 26206
    invoke-virtual {v4, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 26208
    :cond_230
    invoke-virtual {v4, v8}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    goto/16 :goto_155

    .line 26198
    :cond_235
    if-eqz v4, :cond_239

    move-object v3, v4

    .line 26199
    goto :goto_224

    .line 26200
    :cond_239
    if-nez v3, :cond_224

    move-object v3, v9

    goto :goto_224

    :cond_23d
    move-object v4, v3

    .line 26211
    goto/16 :goto_155

    .line 1226
    :cond_240
    const/4 v3, 0x0

    goto/16 :goto_11

    :cond_243
    move v9, v10

    goto/16 :goto_13e

    :cond_246
    move v9, v3

    goto/16 :goto_12e

    :cond_249
    move-object/from16 v18, v3

    move-object/from16 v17, v6

    goto/16 :goto_51
.end method

.method private static b(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1062
    if-nez p0, :cond_4

    .line 1063
    const/4 v0, 0x0

    .line 1065
    :goto_3
    return-object v0

    :cond_4
    if-eqz p1, :cond_16

    .line 8641
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ak:Ljava/lang/Object;

    sget-object v1, Landroid/support/v4/app/Fragment;->n:Ljava/lang/Object;

    if-ne v0, v1, :cond_13

    .line 9607
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->aj:Ljava/lang/Object;

    .line 1065
    :goto_e
    invoke-static {v0}, Landroid/support/v4/app/ce;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 8641
    :cond_13
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ak:Ljava/lang/Object;

    goto :goto_e

    .line 9675
    :cond_16
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->al:Ljava/lang/Object;

    goto :goto_e
.end method

.method private static b(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLandroid/support/v4/n/a;)V
    .registers 6

    .prologue
    .line 1262
    if-eqz p2, :cond_19

    iget-object v0, p1, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    .line 1265
    :goto_4
    if-eqz v0, :cond_18

    .line 1266
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p3}, Landroid/support/v4/n/a;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1267
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p3}, Landroid/support/v4/n/a;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1270
    :cond_18
    return-void

    .line 1262
    :cond_19
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ar:Landroid/support/v4/app/gj;

    goto :goto_4
.end method

.method private static b(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
    .registers 8

    .prologue
    .line 1435
    invoke-virtual {p1}, Landroid/support/v4/n/a;->size()I

    move-result v3

    .line 1436
    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v3, :cond_29

    .line 1437
    invoke-virtual {p1, v2}, Landroid/support/v4/n/a;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1438
    invoke-virtual {p1, v2}, Landroid/support/v4/n/a;->c(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 34033
    invoke-virtual {v1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v1

    .line 1439
    if-eqz p2, :cond_23

    .line 1440
    iget-object v4, p0, Landroid/support/v4/app/ap;->a:Landroid/support/v4/n/a;

    invoke-static {v4, v0, v1}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    :goto_1f
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 1442
    :cond_23
    iget-object v4, p0, Landroid/support/v4/app/ap;->a:Landroid/support/v4/n/a;

    invoke-static {v4, v1, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1f

    .line 1445
    :cond_29
    return-void
.end method

.method private static b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
    .registers 3

    .prologue
    .line 756
    if-eqz p1, :cond_9

    .line 757
    iget v0, p1, Landroid/support/v4/app/Fragment;->R:I

    .line 758
    if-eqz v0, :cond_9

    .line 759
    invoke-virtual {p0, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 762
    :cond_9
    return-void
.end method

.method private b(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
    .registers 9

    .prologue
    .line 775
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->w:Landroid/support/v4/app/bf;

    invoke-virtual {v0}, Landroid/support/v4/app/bf;->a()Z

    move-result v0

    if-nez v0, :cond_b

    .line 820
    :cond_a
    return-void

    .line 778
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/ak;->l:Landroid/support/v4/app/ao;

    move-object v3, v0

    .line 779
    :goto_e
    if-eqz v3, :cond_a

    .line 780
    iget v0, v3, Landroid/support/v4/app/ao;->c:I

    packed-switch v0, :pswitch_data_74

    .line 818
    :goto_15
    iget-object v0, v3, Landroid/support/v4/app/ao;->a:Landroid/support/v4/app/ao;

    move-object v3, v0

    goto :goto_e

    .line 782
    :pswitch_19
    iget-object v0, v3, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 785
    :pswitch_1f
    iget-object v1, v3, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 786
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_51

    .line 787
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_2a
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_52

    .line 788
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 789
    if-eqz v2, :cond_46

    iget v4, v0, Landroid/support/v4/app/Fragment;->R:I

    iget v5, v2, Landroid/support/v4/app/Fragment;->R:I

    if-ne v4, v5, :cond_49

    .line 790
    :cond_46
    if-ne v0, v2, :cond_4d

    .line 791
    const/4 v2, 0x0

    .line 787
    :cond_49
    :goto_49
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2a

    .line 793
    :cond_4d
    invoke-static {p1, v0}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_49

    :cond_51
    move-object v2, v1

    .line 798
    :cond_52
    invoke-static {p2, v2}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 802
    :pswitch_56
    iget-object v0, v3, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 805
    :pswitch_5c
    iget-object v0, v3, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 808
    :pswitch_62
    iget-object v0, v3, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 811
    :pswitch_68
    iget-object v0, v3, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 814
    :pswitch_6e
    iget-object v0, v3, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 780
    :pswitch_data_74
    .packed-switch 0x1
        :pswitch_19
        :pswitch_1f
        :pswitch_56
        :pswitch_5c
        :pswitch_62
        :pswitch_68
        :pswitch_6e
    .end packed-switch
.end method

.method private c(Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/n/a;
    .registers 6

    .prologue
    .line 1341
    new-instance v0, Landroid/support/v4/n/a;

    invoke-direct {v0}, Landroid/support/v4/n/a;-><init>()V

    .line 31237
    iget-object v1, p1, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 1343
    if-eqz v1, :cond_1a

    .line 1344
    iget-object v2, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    if-eqz v2, :cond_1a

    .line 1345
    invoke-static {v0, v1}, Landroid/support/v4/app/ce;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 1346
    if-eqz p2, :cond_1b

    .line 1347
    iget-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/ak;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;

    move-result-object v0

    .line 1354
    :cond_1a
    :goto_1a
    return-object v0

    .line 1350
    :cond_1b
    iget-object v1, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    .line 32161
    invoke-static {v0, v1}, Landroid/support/v4/n/k;->c(Ljava/util/Map;Ljava/util/Collection;)Z

    goto :goto_1a
.end method

.method private m()I
    .registers 2

    .prologue
    .line 975
    iget v0, p0, Landroid/support/v4/app/ak;->s:I

    return v0
.end method

.method private n()I
    .registers 2

    .prologue
    .line 979
    iget v0, p0, Landroid/support/v4/app/ak;->t:I

    return v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 358
    iget v0, p0, Landroid/support/v4/app/ak;->y:I

    return v0
.end method

.method public final a(ZLandroid/support/v4/app/ap;Landroid/util/SparseArray;Landroid/util/SparseArray;)Landroid/support/v4/app/ap;
    .registers 16

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x1

    const/4 v9, -0x1

    const/4 v2, 0x0

    .line 873
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_2d

    .line 874
    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "popFromBackStack: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    new-instance v0, Landroid/support/v4/n/h;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Landroid/support/v4/n/h;-><init>(Ljava/lang/String;)V

    .line 876
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 877
    const-string v0, "  "

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/ak;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 880
    :cond_2d
    sget-boolean v0, Landroid/support/v4/app/ak;->b:Z

    if-eqz v0, :cond_43

    .line 881
    if-nez p2, :cond_73

    .line 882
    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_3f

    invoke-virtual {p4}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-eqz v0, :cond_43

    .line 883
    :cond_3f
    invoke-direct {p0, p3, p4, v10}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/ap;

    move-result-object p2

    .line 890
    :cond_43
    :goto_43
    invoke-virtual {p0, v9}, Landroid/support/v4/app/ak;->e(I)V

    .line 892
    if-eqz p2, :cond_7d

    move v7, v2

    .line 893
    :goto_49
    if-eqz p2, :cond_81

    move v1, v2

    .line 894
    :goto_4c
    iget-object v0, p0, Landroid/support/v4/app/ak;->m:Landroid/support/v4/app/ao;

    move-object v6, v0

    .line 895
    :goto_4f
    if-eqz v6, :cond_10e

    .line 896
    if-eqz p2, :cond_85

    move v5, v2

    .line 897
    :goto_54
    if-eqz p2, :cond_89

    move v0, v2

    .line 898
    :goto_57
    iget v3, v6, Landroid/support/v4/app/ao;->c:I

    packed-switch v3, :pswitch_data_160

    .line 950
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown cmd: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v6, Landroid/support/v4/app/ao;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_73
    if-nez p1, :cond_43

    .line 886
    iget-object v0, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    invoke-static {p2, v0, v1}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_43

    .line 892
    :cond_7d
    iget v0, p0, Landroid/support/v4/app/ak;->t:I

    move v7, v0

    goto :goto_49

    .line 893
    :cond_81
    iget v0, p0, Landroid/support/v4/app/ak;->s:I

    move v1, v0

    goto :goto_4c

    .line 896
    :cond_85
    iget v0, v6, Landroid/support/v4/app/ao;->g:I

    move v5, v0

    goto :goto_54

    .line 897
    :cond_89
    iget v0, v6, Landroid/support/v4/app/ao;->h:I

    goto :goto_57

    .line 900
    :pswitch_8c
    iget-object v3, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 901
    iput v0, v3, Landroid/support/v4/app/Fragment;->aa:I

    .line 902
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-static {v1}, Landroid/support/v4/app/bl;->d(I)I

    move-result v5

    invoke-virtual {v0, v3, v5, v7}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;II)V

    .line 954
    :cond_99
    :goto_99
    iget-object v0, v6, Landroid/support/v4/app/ao;->b:Landroid/support/v4/app/ao;

    move-object v6, v0

    .line 955
    goto :goto_4f

    .line 906
    :pswitch_9d
    iget-object v3, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 907
    if-eqz v3, :cond_ac

    .line 908
    iput v0, v3, Landroid/support/v4/app/Fragment;->aa:I

    .line 909
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-static {v1}, Landroid/support/v4/app/bl;->d(I)I

    move-result v8

    invoke-virtual {v0, v3, v8, v7}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;II)V

    .line 912
    :cond_ac
    iget-object v0, v6, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_99

    move v3, v2

    .line 913
    :goto_b1
    iget-object v0, v6, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_99

    .line 914
    iget-object v0, v6, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 915
    iput v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 916
    iget-object v8, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v8, v0, v2}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 913
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b1

    .line 921
    :pswitch_cc
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 922
    iput v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 923
    iget-object v3, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v3, v0, v2}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;Z)V

    goto :goto_99

    .line 926
    :pswitch_d6
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 927
    iput v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 928
    iget-object v3, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-static {v1}, Landroid/support/v4/app/bl;->d(I)I

    move-result v5

    invoke-virtual {v3, v0, v5, v7}, Landroid/support/v4/app/bl;->c(Landroid/support/v4/app/Fragment;II)V

    goto :goto_99

    .line 932
    :pswitch_e4
    iget-object v3, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 933
    iput v0, v3, Landroid/support/v4/app/Fragment;->aa:I

    .line 934
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-static {v1}, Landroid/support/v4/app/bl;->d(I)I

    move-result v5

    invoke-virtual {v0, v3, v5, v7}, Landroid/support/v4/app/bl;->b(Landroid/support/v4/app/Fragment;II)V

    goto :goto_99

    .line 938
    :pswitch_f2
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 939
    iput v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 940
    iget-object v3, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-static {v1}, Landroid/support/v4/app/bl;->d(I)I

    move-result v5

    invoke-virtual {v3, v0, v5, v7}, Landroid/support/v4/app/bl;->e(Landroid/support/v4/app/Fragment;II)V

    goto :goto_99

    .line 944
    :pswitch_100
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 945
    iput v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 946
    iget-object v3, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-static {v1}, Landroid/support/v4/app/bl;->d(I)I

    move-result v5

    invoke-virtual {v3, v0, v5, v7}, Landroid/support/v4/app/bl;->d(Landroid/support/v4/app/Fragment;II)V

    goto :goto_99

    .line 957
    :cond_10e
    if-eqz p1, :cond_11e

    .line 958
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v2, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget v2, v2, Landroid/support/v4/app/bl;->t:I

    invoke-static {v1}, Landroid/support/v4/app/bl;->d(I)I

    move-result v1

    invoke-virtual {v0, v2, v1, v7, v10}, Landroid/support/v4/app/bl;->a(IIIZ)V

    move-object p2, v4

    .line 963
    :cond_11e
    iget v0, p0, Landroid/support/v4/app/ak;->y:I

    if-ltz v0, :cond_15c

    .line 964
    iget-object v1, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget v0, p0, Landroid/support/v4/app/ak;->y:I

    .line 5529
    monitor-enter v1

    .line 5530
    :try_start_127
    iget-object v2, v1, Landroid/support/v4/app/bl;->q:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 5531
    iget-object v2, v1, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    if-nez v2, :cond_138

    .line 5532
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    .line 5534
    :cond_138
    sget-boolean v2, Landroid/support/v4/app/bl;->b:Z

    if-eqz v2, :cond_150

    const-string v2, "FragmentManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Freeing back stack index "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5535
    :cond_150
    iget-object v2, v1, Landroid/support/v4/app/bl;->r:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5536
    monitor-exit v1
    :try_end_15a
    .catchall {:try_start_127 .. :try_end_15a} :catchall_15d

    .line 965
    iput v9, p0, Landroid/support/v4/app/ak;->y:I

    .line 967
    :cond_15c
    return-object p2

    .line 5536
    :catchall_15d
    move-exception v0

    :try_start_15e
    monitor-exit v1
    :try_end_15f
    .catchall {:try_start_15e .. :try_end_15f} :catchall_15d

    throw v0

    .line 898
    :pswitch_data_160
    .packed-switch 0x1
        :pswitch_8c
        :pswitch_9d
        :pswitch_cc
        :pswitch_d6
        :pswitch_e4
        :pswitch_f2
        :pswitch_100
    .end packed-switch
.end method

.method public final a(I)Landroid/support/v4/app/cd;
    .registers 2

    .prologue
    .line 512
    iput p1, p0, Landroid/support/v4/app/ak;->s:I

    .line 513
    return-object p0
.end method

.method public final a(II)Landroid/support/v4/app/cd;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 499
    .line 3504
    iput p1, p0, Landroid/support/v4/app/ak;->o:I

    .line 3505
    iput p2, p0, Landroid/support/v4/app/ak;->p:I

    .line 3506
    iput v0, p0, Landroid/support/v4/app/ak;->q:I

    .line 3507
    iput v0, p0, Landroid/support/v4/app/ak;->r:I

    .line 499
    return-object p0
.end method

.method public final a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    .registers 5

    .prologue
    .line 404
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v4/app/ak;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 405
    return-object p0
.end method

.method public final a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
    .registers 5

    .prologue
    .line 409
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/ak;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 410
    return-object p0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    .registers 3

    .prologue
    .line 441
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/app/ak;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
    .registers 5

    .prologue
    .line 399
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Landroid/support/v4/app/ak;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 400
    return-object p0
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/cd;
    .registers 5

    .prologue
    .line 518
    sget-boolean v0, Landroid/support/v4/app/ak;->b:Z

    if-eqz v0, :cond_2e

    .line 4033
    invoke-virtual {p1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v0

    .line 520
    if-nez v0, :cond_12

    .line 521
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unique transitionNames are required for all sharedElements"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 524
    :cond_12
    iget-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    if-nez v1, :cond_24

    .line 525
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    .line 526
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    .line 529
    :cond_24
    iget-object v1, p0, Landroid/support/v4/app/ak;->D:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 530
    iget-object v0, p0, Landroid/support/v4/app/ak;->E:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 532
    :cond_2e
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/cd;
    .registers 3

    .prologue
    .line 570
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/ak;->z:I

    .line 571
    iput-object p1, p0, Landroid/support/v4/app/ak;->A:Ljava/lang/CharSequence;

    .line 572
    return-object p0
.end method

.method final a(Landroid/support/v4/app/ao;)V
    .registers 3

    .prologue
    .line 384
    iget-object v0, p0, Landroid/support/v4/app/ak;->l:Landroid/support/v4/app/ao;

    if-nez v0, :cond_1f

    .line 385
    iput-object p1, p0, Landroid/support/v4/app/ak;->m:Landroid/support/v4/app/ao;

    iput-object p1, p0, Landroid/support/v4/app/ak;->l:Landroid/support/v4/app/ao;

    .line 391
    :goto_8
    iget v0, p0, Landroid/support/v4/app/ak;->o:I

    iput v0, p1, Landroid/support/v4/app/ao;->e:I

    .line 392
    iget v0, p0, Landroid/support/v4/app/ak;->p:I

    iput v0, p1, Landroid/support/v4/app/ao;->f:I

    .line 393
    iget v0, p0, Landroid/support/v4/app/ak;->q:I

    iput v0, p1, Landroid/support/v4/app/ao;->g:I

    .line 394
    iget v0, p0, Landroid/support/v4/app/ak;->r:I

    iput v0, p1, Landroid/support/v4/app/ao;->h:I

    .line 395
    iget v0, p0, Landroid/support/v4/app/ak;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/app/ak;->n:I

    .line 396
    return-void

    .line 387
    :cond_1f
    iget-object v0, p0, Landroid/support/v4/app/ak;->m:Landroid/support/v4/app/ao;

    iput-object v0, p1, Landroid/support/v4/app/ao;->b:Landroid/support/v4/app/ao;

    .line 388
    iget-object v0, p0, Landroid/support/v4/app/ak;->m:Landroid/support/v4/app/ao;

    iput-object p1, v0, Landroid/support/v4/app/ao;->a:Landroid/support/v4/app/ao;

    .line 389
    iput-object p1, p0, Landroid/support/v4/app/ak;->m:Landroid/support/v4/app/ao;

    goto :goto_8
.end method

.method public final a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
    .registers 6

    .prologue
    .line 833
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->w:Landroid/support/v4/app/bf;

    invoke-virtual {v0}, Landroid/support/v4/app/bf;->a()Z

    move-result v0

    if-nez v0, :cond_b

    .line 869
    :cond_a
    return-void

    .line 836
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/ak;->l:Landroid/support/v4/app/ao;

    move-object v2, v0

    .line 837
    :goto_e
    if-eqz v2, :cond_a

    .line 838
    iget v0, v2, Landroid/support/v4/app/ao;->c:I

    packed-switch v0, :pswitch_data_62

    .line 867
    :goto_15
    iget-object v0, v2, Landroid/support/v4/app/ao;->a:Landroid/support/v4/app/ao;

    move-object v2, v0

    goto :goto_e

    .line 840
    :pswitch_19
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 843
    :pswitch_1f
    iget-object v0, v2, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_3d

    .line 844
    iget-object v0, v2, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2c
    if-ltz v1, :cond_3d

    .line 845
    iget-object v0, v2, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    .line 844
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2c

    .line 848
    :cond_3d
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 851
    :pswitch_43
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 854
    :pswitch_49
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 857
    :pswitch_4f
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 860
    :pswitch_55
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p2, v0}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 863
    :pswitch_5b
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-static {p1, v0}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V

    goto :goto_15

    .line 838
    nop

    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_19
        :pswitch_1f
        :pswitch_43
        :pswitch_49
        :pswitch_4f
        :pswitch_55
        :pswitch_5b
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .registers 4

    .prologue
    .line 257
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/app/ak;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 258
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .registers 11

    .prologue
    const/4 v1, 0x0

    .line 261
    if-eqz p3, :cond_db

    .line 262
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mName="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/ak;->w:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 263
    const-string v0, " mIndex="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/ak;->y:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 264
    const-string v0, " mCommitted="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/ak;->x:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 265
    iget v0, p0, Landroid/support/v4/app/ak;->s:I

    if-eqz v0, :cond_47

    .line 266
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTransition=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 267
    iget v0, p0, Landroid/support/v4/app/ak;->s:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 268
    const-string v0, " mTransitionStyle=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 269
    iget v0, p0, Landroid/support/v4/app/ak;->t:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 271
    :cond_47
    iget v0, p0, Landroid/support/v4/app/ak;->o:I

    if-nez v0, :cond_4f

    iget v0, p0, Landroid/support/v4/app/ak;->p:I

    if-eqz v0, :cond_6e

    .line 272
    :cond_4f
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 273
    iget v0, p0, Landroid/support/v4/app/ak;->o:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 274
    const-string v0, " mExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 275
    iget v0, p0, Landroid/support/v4/app/ak;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 277
    :cond_6e
    iget v0, p0, Landroid/support/v4/app/ak;->q:I

    if-nez v0, :cond_76

    iget v0, p0, Landroid/support/v4/app/ak;->r:I

    if-eqz v0, :cond_95

    .line 278
    :cond_76
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mPopEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 279
    iget v0, p0, Landroid/support/v4/app/ak;->q:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 280
    const-string v0, " mPopExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 281
    iget v0, p0, Landroid/support/v4/app/ak;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 283
    :cond_95
    iget v0, p0, Landroid/support/v4/app/ak;->z:I

    if-nez v0, :cond_9d

    iget-object v0, p0, Landroid/support/v4/app/ak;->A:Ljava/lang/CharSequence;

    if-eqz v0, :cond_b8

    .line 284
    :cond_9d
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 285
    iget v0, p0, Landroid/support/v4/app/ak;->z:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 286
    const-string v0, " mBreadCrumbTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Landroid/support/v4/app/ak;->A:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 289
    :cond_b8
    iget v0, p0, Landroid/support/v4/app/ak;->B:I

    if-nez v0, :cond_c0

    iget-object v0, p0, Landroid/support/v4/app/ak;->C:Ljava/lang/CharSequence;

    if-eqz v0, :cond_db

    .line 290
    :cond_c0
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbShortTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 291
    iget v0, p0, Landroid/support/v4/app/ak;->B:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 292
    const-string v0, " mBreadCrumbShortTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Landroid/support/v4/app/ak;->C:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 297
    :cond_db
    iget-object v0, p0, Landroid/support/v4/app/ak;->l:Landroid/support/v4/app/ao;

    if-eqz v0, :cond_1f4

    .line 298
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Operations:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 300
    iget-object v0, p0, Landroid/support/v4/app/ak;->l:Landroid/support/v4/app/ao;

    move v2, v1

    move-object v3, v0

    .line 302
    :goto_fe
    if-eqz v3, :cond_1f4

    .line 304
    iget v0, v3, Landroid/support/v4/app/ao;->c:I

    packed-switch v0, :pswitch_data_1f6

    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "cmd="

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v3, Landroid/support/v4/app/ao;->c:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    :goto_116
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  Op #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 316
    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 317
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, v3, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 318
    if-eqz p3, :cond_183

    .line 319
    iget v0, v3, Landroid/support/v4/app/ao;->e:I

    if-nez v0, :cond_13d

    iget v0, v3, Landroid/support/v4/app/ao;->f:I

    if-eqz v0, :cond_15c

    .line 320
    :cond_13d
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "enterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 321
    iget v0, v3, Landroid/support/v4/app/ao;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 322
    const-string v0, " exitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 323
    iget v0, v3, Landroid/support/v4/app/ao;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 325
    :cond_15c
    iget v0, v3, Landroid/support/v4/app/ao;->g:I

    if-nez v0, :cond_164

    iget v0, v3, Landroid/support/v4/app/ao;->h:I

    if-eqz v0, :cond_183

    .line 326
    :cond_164
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "popEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 327
    iget v0, v3, Landroid/support/v4/app/ao;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 328
    const-string v0, " popExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 329
    iget v0, v3, Landroid/support/v4/app/ao;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 332
    :cond_183
    iget-object v0, v3, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_1ed

    iget-object v0, v3, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1ed

    move v0, v1

    .line 333
    :goto_190
    iget-object v5, v3, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_1ed

    .line 334
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 335
    iget-object v5, v3, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1d5

    .line 336
    const-string v5, "Removed: "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 344
    :goto_1a9
    iget-object v5, v3, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 333
    add-int/lit8 v0, v0, 0x1

    goto :goto_190

    .line 305
    :pswitch_1b5
    const-string v0, "NULL"

    goto/16 :goto_116

    .line 306
    :pswitch_1b9
    const-string v0, "ADD"

    goto/16 :goto_116

    .line 307
    :pswitch_1bd
    const-string v0, "REPLACE"

    goto/16 :goto_116

    .line 308
    :pswitch_1c1
    const-string v0, "REMOVE"

    goto/16 :goto_116

    .line 309
    :pswitch_1c5
    const-string v0, "HIDE"

    goto/16 :goto_116

    .line 310
    :pswitch_1c9
    const-string v0, "SHOW"

    goto/16 :goto_116

    .line 311
    :pswitch_1cd
    const-string v0, "DETACH"

    goto/16 :goto_116

    .line 312
    :pswitch_1d1
    const-string v0, "ATTACH"

    goto/16 :goto_116

    .line 338
    :cond_1d5
    if-nez v0, :cond_1dc

    .line 339
    const-string v5, "Removed:"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 341
    :cond_1dc
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 342
    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_1a9

    .line 347
    :cond_1ed
    iget-object v3, v3, Landroid/support/v4/app/ao;->a:Landroid/support/v4/app/ao;

    .line 348
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 349
    goto/16 :goto_fe

    .line 351
    :cond_1f4
    return-void

    .line 304
    nop

    :pswitch_data_1f6
    .packed-switch 0x0
        :pswitch_1b5
        :pswitch_1b9
        :pswitch_1bd
        :pswitch_1c1
        :pswitch_1c5
        :pswitch_1c9
        :pswitch_1cd
        :pswitch_1d1
    .end packed-switch
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 362
    iget v0, p0, Landroid/support/v4/app/ak;->z:I

    return v0
.end method

.method public final b(I)Landroid/support/v4/app/cd;
    .registers 2

    .prologue
    .line 536
    iput p1, p0, Landroid/support/v4/app/ak;->t:I

    .line 537
    return-object p0
.end method

.method public final b(II)Landroid/support/v4/app/cd;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 504
    iput p1, p0, Landroid/support/v4/app/ak;->o:I

    .line 505
    iput p2, p0, Landroid/support/v4/app/ak;->p:I

    .line 506
    iput v0, p0, Landroid/support/v4/app/ak;->q:I

    .line 507
    iput v0, p0, Landroid/support/v4/app/ak;->r:I

    .line 508
    return-object p0
.end method

.method public final b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    .registers 4

    .prologue
    .line 454
    new-instance v0, Landroid/support/v4/app/ao;

    invoke-direct {v0}, Landroid/support/v4/app/ao;-><init>()V

    .line 455
    const/4 v1, 0x3

    iput v1, v0, Landroid/support/v4/app/ao;->c:I

    .line 456
    iput-object p1, v0, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 457
    invoke-virtual {p0, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ao;)V

    .line 459
    return-object p0
.end method

.method public final b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
    .registers 5

    .prologue
    .line 449
    const v0, 0x7f0c01c7

    const/4 v1, 0x2

    invoke-direct {p0, v0, p1, p2, v1}, Landroid/support/v4/app/ak;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    .line 450
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/cd;
    .registers 3

    .prologue
    .line 582
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/ak;->B:I

    .line 583
    iput-object p1, p0, Landroid/support/v4/app/ak;->C:Ljava/lang/CharSequence;

    .line 584
    return-object p0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 366
    iget v0, p0, Landroid/support/v4/app/ak;->B:I

    return v0
.end method

.method public final c(I)Landroid/support/v4/app/cd;
    .registers 3

    .prologue
    .line 564
    iput p1, p0, Landroid/support/v4/app/ak;->z:I

    .line 565
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/ak;->A:Ljava/lang/CharSequence;

    .line 566
    return-object p0
.end method

.method public final c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    .registers 4

    .prologue
    .line 463
    new-instance v0, Landroid/support/v4/app/ao;

    invoke-direct {v0}, Landroid/support/v4/app/ao;-><init>()V

    .line 464
    const/4 v1, 0x4

    iput v1, v0, Landroid/support/v4/app/ao;->c:I

    .line 465
    iput-object p1, v0, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 466
    invoke-virtual {p0, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ao;)V

    .line 468
    return-object p0
.end method

.method public final d(I)Landroid/support/v4/app/cd;
    .registers 3

    .prologue
    .line 576
    iput p1, p0, Landroid/support/v4/app/ak;->B:I

    .line 577
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/ak;->C:Ljava/lang/CharSequence;

    .line 578
    return-object p0
.end method

.method public final d(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    .registers 4

    .prologue
    .line 472
    new-instance v0, Landroid/support/v4/app/ao;

    invoke-direct {v0}, Landroid/support/v4/app/ao;-><init>()V

    .line 473
    const/4 v1, 0x5

    iput v1, v0, Landroid/support/v4/app/ao;->c:I

    .line 474
    iput-object p1, v0, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 475
    invoke-virtual {p0, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ao;)V

    .line 477
    return-object p0
.end method

.method public final d()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 370
    iget v0, p0, Landroid/support/v4/app/ak;->z:I

    if-eqz v0, :cond_11

    .line 371
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 2166
    iget-object v0, v0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 371
    iget v1, p0, Landroid/support/v4/app/ak;->z:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 373
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, Landroid/support/v4/app/ak;->A:Ljava/lang/CharSequence;

    goto :goto_10
.end method

.method public final e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    .registers 4

    .prologue
    .line 481
    new-instance v0, Landroid/support/v4/app/ao;

    invoke-direct {v0}, Landroid/support/v4/app/ao;-><init>()V

    .line 482
    const/4 v1, 0x6

    iput v1, v0, Landroid/support/v4/app/ao;->c:I

    .line 483
    iput-object p1, v0, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 484
    invoke-virtual {p0, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ao;)V

    .line 486
    return-object p0
.end method

.method public final e()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 377
    iget v0, p0, Landroid/support/v4/app/ak;->B:I

    if-eqz v0, :cond_11

    .line 378
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->u:Landroid/support/v4/app/bh;

    .line 3166
    iget-object v0, v0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 378
    iget v1, p0, Landroid/support/v4/app/ak;->B:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 380
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, Landroid/support/v4/app/ak;->C:Ljava/lang/CharSequence;

    goto :goto_10
.end method

.method final e(I)V
    .registers 8

    .prologue
    .line 588
    iget-boolean v0, p0, Landroid/support/v4/app/ak;->u:Z

    if-nez v0, :cond_5

    .line 610
    :cond_4
    return-void

    .line 591
    :cond_5
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_27

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bump nesting in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    :cond_27
    iget-object v0, p0, Landroid/support/v4/app/ak;->l:Landroid/support/v4/app/ao;

    move-object v2, v0

    .line 594
    :goto_2a
    if-eqz v2, :cond_4

    .line 595
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_5f

    .line 596
    iget-object v0, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    iget v1, v0, Landroid/support/v4/app/Fragment;->L:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/support/v4/app/Fragment;->L:I

    .line 597
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_5f

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Bump nesting of "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v2, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    iget v3, v3, Landroid/support/v4/app/Fragment;->L:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :cond_5f
    iget-object v0, v2, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_a3

    .line 601
    iget-object v0, v2, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_6c
    if-ltz v1, :cond_a3

    .line 602
    iget-object v0, v2, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 603
    iget v3, v0, Landroid/support/v4/app/Fragment;->L:I

    add-int/2addr v3, p1

    iput v3, v0, Landroid/support/v4/app/Fragment;->L:I

    .line 604
    sget-boolean v3, Landroid/support/v4/app/bl;->b:Z

    if-eqz v3, :cond_9f

    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bump nesting of "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, v0, Landroid/support/v4/app/Fragment;->L:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    :cond_9f
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_6c

    .line 608
    :cond_a3
    iget-object v0, v2, Landroid/support/v4/app/ao;->a:Landroid/support/v4/app/ao;

    move-object v2, v0

    goto :goto_2a
.end method

.method public final f()Landroid/support/v4/app/cd;
    .registers 3

    .prologue
    .line 541
    iget-boolean v0, p0, Landroid/support/v4/app/ak;->v:Z

    if-nez v0, :cond_c

    .line 542
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This FragmentTransaction is not allowed to be added to the back stack."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 545
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ak;->u:Z

    .line 546
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/ak;->w:Ljava/lang/String;

    .line 547
    return-object p0
.end method

.method public final f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    .registers 4

    .prologue
    .line 490
    new-instance v0, Landroid/support/v4/app/ao;

    invoke-direct {v0}, Landroid/support/v4/app/ao;-><init>()V

    .line 491
    const/4 v1, 0x7

    iput v1, v0, Landroid/support/v4/app/ao;->c:I

    .line 492
    iput-object p1, v0, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 493
    invoke-virtual {p0, v0}, Landroid/support/v4/app/ak;->a(Landroid/support/v4/app/ao;)V

    .line 495
    return-object p0
.end method

.method public final g()Z
    .registers 2

    .prologue
    .line 551
    iget-boolean v0, p0, Landroid/support/v4/app/ak;->v:Z

    return v0
.end method

.method public final h()Landroid/support/v4/app/cd;
    .registers 3

    .prologue
    .line 555
    iget-boolean v0, p0, Landroid/support/v4/app/ak;->u:Z

    if-eqz v0, :cond_c

    .line 556
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This transaction is already being added to the back stack"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 559
    :cond_c
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/ak;->v:Z

    .line 560
    return-object p0
.end method

.method public final i()I
    .registers 2

    .prologue
    .line 613
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/app/ak;->a(Z)I

    move-result v0

    return v0
.end method

.method public final j()I
    .registers 2

    .prologue
    .line 617
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/app/ak;->a(Z)I

    move-result v0

    return v0
.end method

.method public final k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 971
    iget-object v0, p0, Landroid/support/v4/app/ak;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .registers 2

    .prologue
    .line 983
    iget v0, p0, Landroid/support/v4/app/ak;->n:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final run()V
    .registers 14

    .prologue
    .line 639
    sget-boolean v0, Landroid/support/v4/app/bl;->b:Z

    if-eqz v0, :cond_18

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Run: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    :cond_18
    iget-boolean v0, p0, Landroid/support/v4/app/ak;->u:Z

    if-eqz v0, :cond_28

    .line 642
    iget v0, p0, Landroid/support/v4/app/ak;->y:I

    if-gez v0, :cond_28

    .line 643
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "addToBackStack() called after commit()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647
    :cond_28
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/ak;->e(I)V

    .line 649
    const/4 v0, 0x0

    .line 652
    sget-boolean v1, Landroid/support/v4/app/ak;->b:Z

    if-eqz v1, :cond_191

    .line 653
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 654
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 656
    invoke-direct {p0, v0, v1}, Landroid/support/v4/app/ak;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 658
    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/app/ak;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/ap;

    move-result-object v0

    move-object v8, v0

    .line 661
    :goto_44
    if-eqz v8, :cond_75

    const/4 v0, 0x0

    move v7, v0

    .line 662
    :goto_48
    if-eqz v8, :cond_79

    const/4 v0, 0x0

    move v1, v0

    .line 663
    :goto_4c
    iget-object v0, p0, Landroid/support/v4/app/ak;->l:Landroid/support/v4/app/ao;

    move-object v6, v0

    .line 664
    :goto_4f
    if-eqz v6, :cond_16d

    .line 665
    if-eqz v8, :cond_7d

    const/4 v0, 0x0

    move v5, v0

    .line 666
    :goto_55
    if-eqz v8, :cond_81

    const/4 v0, 0x0

    move v2, v0

    .line 667
    :goto_59
    iget v0, v6, Landroid/support/v4/app/ao;->c:I

    packed-switch v0, :pswitch_data_194

    .line 731
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown cmd: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v6, Landroid/support/v4/app/ao;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 661
    :cond_75
    iget v0, p0, Landroid/support/v4/app/ak;->t:I

    move v7, v0

    goto :goto_48

    .line 662
    :cond_79
    iget v0, p0, Landroid/support/v4/app/ak;->s:I

    move v1, v0

    goto :goto_4c

    .line 665
    :cond_7d
    iget v0, v6, Landroid/support/v4/app/ao;->e:I

    move v5, v0

    goto :goto_55

    .line 666
    :cond_81
    iget v0, v6, Landroid/support/v4/app/ao;->f:I

    move v2, v0

    goto :goto_59

    .line 669
    :pswitch_85
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 670
    iput v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 671
    iget-object v2, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 735
    :cond_8f
    :goto_8f
    iget-object v0, v6, Landroid/support/v4/app/ao;->a:Landroid/support/v4/app/ao;

    move-object v6, v0

    .line 736
    goto :goto_4f

    .line 674
    :pswitch_93
    iget-object v3, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 675
    iget v9, v3, Landroid/support/v4/app/Fragment;->R:I

    .line 676
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_129

    .line 677
    const/4 v0, 0x0

    move-object v4, v3

    move v3, v0

    :goto_a0
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_12a

    .line 678
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 679
    sget-boolean v10, Landroid/support/v4/app/bl;->b:Z

    if-eqz v10, :cond_d6

    const-string v10, "FragmentManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "OP_REPLACE: adding="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " old="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :cond_d6
    iget v10, v0, Landroid/support/v4/app/Fragment;->R:I

    if-ne v10, v9, :cond_df

    .line 682
    if-ne v0, v4, :cond_e3

    .line 683
    const/4 v4, 0x0

    iput-object v4, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 677
    :cond_df
    :goto_df
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a0

    .line 685
    :cond_e3
    iget-object v10, v6, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    if-nez v10, :cond_ee

    .line 686
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, v6, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    .line 688
    :cond_ee
    iget-object v10, v6, Landroid/support/v4/app/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 689
    iput v2, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 690
    iget-boolean v10, p0, Landroid/support/v4/app/ak;->u:Z

    if-eqz v10, :cond_123

    .line 691
    iget v10, v0, Landroid/support/v4/app/Fragment;->L:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v0, Landroid/support/v4/app/Fragment;->L:I

    .line 692
    sget-boolean v10, Landroid/support/v4/app/bl;->b:Z

    if-eqz v10, :cond_123

    const-string v10, "FragmentManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Bump nesting of "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " to "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/support/v4/app/Fragment;->L:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    :cond_123
    iget-object v10, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v10, v0, v1, v7}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;II)V

    goto :goto_df

    :cond_129
    move-object v4, v3

    .line 700
    :cond_12a
    if-eqz v4, :cond_8f

    .line 701
    iput v5, v4, Landroid/support/v4/app/Fragment;->aa:I

    .line 702
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;Z)V

    goto/16 :goto_8f

    .line 706
    :pswitch_136
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 707
    iput v2, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 708
    iget-object v2, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_8f

    .line 711
    :pswitch_141
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 712
    iput v2, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 713
    iget-object v2, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/bl;->b(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_8f

    .line 716
    :pswitch_14c
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 717
    iput v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 718
    iget-object v2, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/bl;->c(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_8f

    .line 721
    :pswitch_157
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 722
    iput v2, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 723
    iget-object v2, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/bl;->d(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_8f

    .line 726
    :pswitch_162
    iget-object v0, v6, Landroid/support/v4/app/ao;->d:Landroid/support/v4/app/Fragment;

    .line 727
    iput v5, v0, Landroid/support/v4/app/Fragment;->aa:I

    .line 728
    iget-object v2, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/bl;->e(Landroid/support/v4/app/Fragment;II)V

    goto/16 :goto_8f

    .line 738
    :cond_16d
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget-object v2, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    iget v2, v2, Landroid/support/v4/app/bl;->t:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v7, v3}, Landroid/support/v4/app/bl;->a(IIIZ)V

    .line 740
    iget-boolean v0, p0, Landroid/support/v4/app/ak;->u:Z

    if-eqz v0, :cond_190

    .line 741
    iget-object v0, p0, Landroid/support/v4/app/ak;->c:Landroid/support/v4/app/bl;

    .line 4604
    iget-object v1, v0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    if-nez v1, :cond_188

    .line 4605
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    .line 4607
    :cond_188
    iget-object v1, v0, Landroid/support/v4/app/bl;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4608
    invoke-virtual {v0}, Landroid/support/v4/app/bl;->l()V

    .line 743
    :cond_190
    return-void

    :cond_191
    move-object v8, v0

    goto/16 :goto_44

    .line 667
    :pswitch_data_194
    .packed-switch 0x1
        :pswitch_85
        :pswitch_93
        :pswitch_136
        :pswitch_141
        :pswitch_14c
        :pswitch_157
        :pswitch_162
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 242
    const-string v1, "BackStackEntry{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    iget v1, p0, Landroid/support/v4/app/ak;->y:I

    if-ltz v1, :cond_25

    .line 245
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    iget v1, p0, Landroid/support/v4/app/ak;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 248
    :cond_25
    iget-object v1, p0, Landroid/support/v4/app/ak;->w:Ljava/lang/String;

    if-eqz v1, :cond_33

    .line 249
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    iget-object v1, p0, Landroid/support/v4/app/ak;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_33
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
