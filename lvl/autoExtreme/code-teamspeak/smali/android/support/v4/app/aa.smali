.class public Landroid/support/v4/app/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    return-void
.end method

.method private static a(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/aa;
    .registers 6

    .prologue
    .line 133
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_15

    .line 134
    new-instance v0, Landroid/support/v4/app/ab;

    .line 2031
    new-instance v1, Landroid/support/v4/app/ad;

    invoke-static {p0, p1, p2}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/app/ActivityOptions;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/ad;-><init>(Landroid/app/ActivityOptions;)V

    .line 134
    invoke-direct {v0, v1}, Landroid/support/v4/app/ab;-><init>(Landroid/support/v4/app/ad;)V

    .line 138
    :goto_14
    return-object v0

    :cond_15
    new-instance v0, Landroid/support/v4/app/aa;

    invoke-direct {v0}, Landroid/support/v4/app/aa;-><init>()V

    goto :goto_14
.end method

.method private static varargs a(Landroid/app/Activity;[Landroid/support/v4/n/q;)Landroid/support/v4/app/aa;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 160
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_35

    .line 163
    if-eqz p1, :cond_3b

    .line 164
    array-length v0, p1

    new-array v3, v0, [Landroid/view/View;

    .line 165
    array-length v0, p1

    new-array v2, v0, [Ljava/lang/String;

    .line 166
    const/4 v0, 0x0

    move v1, v0

    :goto_11
    array-length v0, p1

    if-ge v1, v0, :cond_28

    .line 167
    aget-object v0, p1, v1

    iget-object v0, v0, Landroid/support/v4/n/q;->a:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    aput-object v0, v3, v1

    .line 168
    aget-object v0, p1, v1

    iget-object v0, v0, Landroid/support/v4/n/q;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v1

    .line 166
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    :cond_28
    move-object v0, v2

    move-object v1, v3

    .line 171
    :goto_2a
    new-instance v2, Landroid/support/v4/app/ab;

    invoke-static {p0, v1, v0}, Landroid/support/v4/app/ad;->a(Landroid/app/Activity;[Landroid/view/View;[Ljava/lang/String;)Landroid/support/v4/app/ad;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/support/v4/app/ab;-><init>(Landroid/support/v4/app/ad;)V

    move-object v0, v2

    .line 174
    :goto_34
    return-object v0

    :cond_35
    new-instance v0, Landroid/support/v4/app/aa;

    invoke-direct {v0}, Landroid/support/v4/app/aa;-><init>()V

    goto :goto_34

    :cond_3b
    move-object v1, v0

    goto :goto_2a
.end method

.method private static a(Landroid/content/Context;II)Landroid/support/v4/app/aa;
    .registers 6

    .prologue
    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_15

    .line 48
    new-instance v0, Landroid/support/v4/app/ac;

    .line 1029
    new-instance v1, Landroid/support/v4/app/ae;

    invoke-static {p0, p1, p2}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/ae;-><init>(Landroid/app/ActivityOptions;)V

    .line 48
    invoke-direct {v0, v1}, Landroid/support/v4/app/ac;-><init>(Landroid/support/v4/app/ae;)V

    .line 51
    :goto_14
    return-object v0

    :cond_15
    new-instance v0, Landroid/support/v4/app/aa;

    invoke-direct {v0}, Landroid/support/v4/app/aa;-><init>()V

    goto :goto_14
.end method

.method private static a(Landroid/view/View;IIII)Landroid/support/v4/app/aa;
    .registers 8

    .prologue
    .line 76
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_15

    .line 77
    new-instance v0, Landroid/support/v4/app/ac;

    .line 1035
    new-instance v1, Landroid/support/v4/app/ae;

    invoke-static {p0, p1, p2, p3, p4}, Landroid/app/ActivityOptions;->makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/ae;-><init>(Landroid/app/ActivityOptions;)V

    .line 77
    invoke-direct {v0, v1}, Landroid/support/v4/app/ac;-><init>(Landroid/support/v4/app/ae;)V

    .line 81
    :goto_14
    return-object v0

    :cond_15
    new-instance v0, Landroid/support/v4/app/aa;

    invoke-direct {v0}, Landroid/support/v4/app/aa;-><init>()V

    goto :goto_14
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/support/v4/app/aa;
    .registers 7

    .prologue
    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_15

    .line 106
    new-instance v0, Landroid/support/v4/app/ac;

    .line 1041
    new-instance v1, Landroid/support/v4/app/ae;

    invoke-static {p0, p1, p2, p3}, Landroid/app/ActivityOptions;->makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/app/ActivityOptions;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/ae;-><init>(Landroid/app/ActivityOptions;)V

    .line 106
    invoke-direct {v0, v1}, Landroid/support/v4/app/ac;-><init>(Landroid/support/v4/app/ae;)V

    .line 110
    :goto_14
    return-object v0

    :cond_15
    new-instance v0, Landroid/support/v4/app/aa;

    invoke-direct {v0}, Landroid/support/v4/app/aa;-><init>()V

    goto :goto_14
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 231
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/support/v4/app/aa;)V
    .registers 2

    .prologue
    .line 241
    return-void
.end method
