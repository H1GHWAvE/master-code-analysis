.class public final Landroid/support/v4/app/gc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "IntentReader"


# instance fields
.field private b:Landroid/app/Activity;

.field private c:Landroid/content/Intent;

.field private d:Ljava/lang/String;

.field private e:Landroid/content/ComponentName;

.field private f:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .registers 3

    .prologue
    .line 676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677
    iput-object p1, p0, Landroid/support/v4/app/gc;->b:Landroid/app/Activity;

    .line 678
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    .line 679
    invoke-static {p1}, Landroid/support/v4/app/ga;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/gc;->d:Ljava/lang/String;

    .line 680
    invoke-static {p1}, Landroid/support/v4/app/ga;->b(Landroid/app/Activity;)Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/gc;->e:Landroid/content/ComponentName;

    .line 681
    return-void
.end method

.method private a(I)Landroid/net/Uri;
    .registers 6

    .prologue
    .line 787
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_14

    invoke-direct {p0}, Landroid/support/v4/app/gc;->c()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 788
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    .line 790
    :cond_14
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_21

    .line 791
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 794
    :goto_20
    return-object v0

    .line 793
    :cond_21
    if-nez p1, :cond_2e

    .line 794
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    goto :goto_20

    .line 796
    :cond_2e
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Stream items available: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1808
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_4b

    invoke-direct {p0}, Landroid/support/v4/app/gc;->c()Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 1809
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    .line 1811
    :cond_4b
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_6b

    .line 1812
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 796
    :goto_55
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " index requested: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1814
    :cond_6b
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_77

    const/4 v0, 0x1

    goto :goto_55

    :cond_77
    const/4 v0, 0x0

    goto :goto_55
.end method

.method private static a(Landroid/app/Activity;)Landroid/support/v4/app/gc;
    .registers 2

    .prologue
    .line 673
    new-instance v0, Landroid/support/v4/app/gc;

    invoke-direct {v0, p0}, Landroid/support/v4/app/gc;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method private a()Z
    .registers 3

    .prologue
    .line 692
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 693
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private b()Z
    .registers 3

    .prologue
    .line 705
    const-string v0, "android.intent.action.SEND"

    iget-object v1, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c()Z
    .registers 3

    .prologue
    .line 716
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    iget-object v1, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 726
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 736
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .registers 4

    .prologue
    .line 750
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.HTML_TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 751
    if-nez v1, :cond_28

    .line 1736
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 753
    instance-of v2, v0, Landroid/text/Spanned;

    if-eqz v2, :cond_1d

    .line 754
    check-cast v0, Landroid/text/Spanned;

    invoke-static {v0}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v0

    .line 759
    :goto_1c
    return-object v0

    .line 755
    :cond_1d
    if-eqz v0, :cond_28

    .line 756
    invoke-static {}, Landroid/support/v4/app/ga;->a()Landroid/support/v4/app/gd;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/support/v4/app/gd;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1c

    :cond_28
    move-object v0, v1

    goto :goto_1c
.end method

.method private g()Landroid/net/Uri;
    .registers 3

    .prologue
    .line 774
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method private h()I
    .registers 3

    .prologue
    .line 808
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_14

    invoke-direct {p0}, Landroid/support/v4/app/gc;->c()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 809
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    .line 811
    :cond_14
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1f

    .line 812
    iget-object v0, p0, Landroid/support/v4/app/gc;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 814
    :goto_1e
    return v0

    :cond_1f
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    const/4 v0, 0x1

    goto :goto_1e

    :cond_2b
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method private i()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 824
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.EMAIL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private j()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 834
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.CC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private k()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 844
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.BCC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .registers 3

    .prologue
    .line 854
    iget-object v0, p0, Landroid/support/v4/app/gc;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 871
    iget-object v0, p0, Landroid/support/v4/app/gc;->d:Ljava/lang/String;

    return-object v0
.end method

.method private n()Landroid/content/ComponentName;
    .registers 2

    .prologue
    .line 888
    iget-object v0, p0, Landroid/support/v4/app/gc;->e:Landroid/content/ComponentName;

    return-object v0
.end method

.method private o()Landroid/graphics/drawable/Drawable;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 902
    iget-object v1, p0, Landroid/support/v4/app/gc;->e:Landroid/content/ComponentName;

    if-nez v1, :cond_6

    .line 910
    :goto_5
    return-object v0

    .line 904
    :cond_6
    iget-object v1, p0, Landroid/support/v4/app/gc;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 906
    :try_start_c
    iget-object v2, p0, Landroid/support/v4/app/gc;->e:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
    :try_end_11
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c .. :try_end_11} :catch_13

    move-result-object v0

    goto :goto_5

    .line 907
    :catch_13
    move-exception v1

    .line 908
    const-string v2, "IntentReader"

    const-string v3, "Could not retrieve icon for calling activity"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5
.end method

.method private p()Landroid/graphics/drawable/Drawable;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 924
    iget-object v1, p0, Landroid/support/v4/app/gc;->d:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 932
    :goto_5
    return-object v0

    .line 926
    :cond_6
    iget-object v1, p0, Landroid/support/v4/app/gc;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 928
    :try_start_c
    iget-object v2, p0, Landroid/support/v4/app/gc;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_11
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c .. :try_end_11} :catch_13

    move-result-object v0

    goto :goto_5

    .line 929
    :catch_13
    move-exception v1

    .line 930
    const-string v2, "IntentReader"

    const-string v3, "Could not retrieve icon for calling application"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5
.end method

.method private q()Ljava/lang/CharSequence;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 946
    iget-object v1, p0, Landroid/support/v4/app/gc;->d:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 954
    :goto_5
    return-object v0

    .line 948
    :cond_6
    iget-object v1, p0, Landroid/support/v4/app/gc;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 950
    :try_start_c
    iget-object v2, p0, Landroid/support/v4/app/gc;->d:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_16
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c .. :try_end_16} :catch_18

    move-result-object v0

    goto :goto_5

    .line 951
    :catch_18
    move-exception v1

    .line 952
    const-string v2, "IntentReader"

    const-string v3, "Could not retrieve label for calling application"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5
.end method
