.class public final Landroid/support/v4/app/gb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/app/Activity;

.field private b:Landroid/content/Intent;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/util/ArrayList;

.field private e:Ljava/util/ArrayList;

.field private f:Ljava/util/ArrayList;

.field private g:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .registers 5

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    iput-object p1, p0, Landroid/support/v4/app/gb;->a:Landroid/app/Activity;

    .line 281
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    .line 282
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.support.v4.app.EXTRA_CALLING_PACKAGE"

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.support.v4.app.EXTRA_CALLING_ACTIVITY"

    invoke-virtual {p1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 284
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 285
    return-void
.end method

.method private a(I)Landroid/support/v4/app/gb;
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 408
    iget-object v0, p0, Landroid/support/v4/app/gb;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1397
    iput-object v0, p0, Landroid/support/v4/app/gb;->c:Ljava/lang/CharSequence;

    .line 408
    return-object p0
.end method

.method private static a(Landroid/app/Activity;)Landroid/support/v4/app/gb;
    .registers 2

    .prologue
    .line 276
    new-instance v0, Landroid/support/v4/app/gb;

    invoke-direct {v0, p0}, Landroid/support/v4/app/gb;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method private a(Landroid/net/Uri;)Landroid/support/v4/app/gb;
    .registers 4

    .prologue
    .line 467
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 468
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 470
    :cond_15
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    .line 471
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 472
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/app/gb;
    .registers 2

    .prologue
    .line 397
    iput-object p1, p0, Landroid/support/v4/app/gb;->c:Ljava/lang/CharSequence;

    .line 398
    return-object p0
.end method

.method private a(Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 3

    .prologue
    .line 419
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 420
    return-object p0
.end method

.method private a([Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 4

    .prologue
    .line 511
    iget-object v0, p0, Landroid/support/v4/app/gb;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 512
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/gb;->d:Ljava/util/ArrayList;

    .line 514
    :cond_7
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.EMAIL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 515
    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 345
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 346
    if-eqz v2, :cond_23

    array-length v0, v2

    .line 347
    :goto_a
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v3, v0

    new-array v3, v3, [Ljava/lang/String;

    .line 348
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 349
    if-eqz v2, :cond_1d

    .line 350
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v2, v1, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 352
    :cond_1d
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    invoke-virtual {v0, p1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    return-void

    :cond_23
    move v0, v1

    .line 346
    goto :goto_a
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 357
    invoke-virtual {p0}, Landroid/support/v4/app/gb;->a()Landroid/content/Intent;

    move-result-object v2

    .line 358
    invoke-virtual {v2, p1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 359
    if-eqz v3, :cond_1d

    array-length v0, v3

    .line 360
    :goto_c
    array-length v4, p2

    add-int/2addr v4, v0

    new-array v4, v4, [Ljava/lang/String;

    .line 361
    if-eqz v3, :cond_15

    invoke-static {v3, v1, v4, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 362
    :cond_15
    array-length v3, p2

    invoke-static {p2, v1, v4, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 363
    invoke-virtual {v2, p1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    return-void

    :cond_1d
    move v0, v1

    .line 359
    goto :goto_c
.end method

.method private b(Landroid/net/Uri;)Landroid/support/v4/app/gb;
    .registers 5

    .prologue
    .line 487
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 488
    if-nez v0, :cond_2c

    .line 1467
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1468
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1470
    :cond_21
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    .line 1471
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 499
    :goto_2b
    return-object p0

    .line 491
    :cond_2c
    iget-object v1, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    if-nez v1, :cond_37

    .line 492
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    .line 494
    :cond_37
    if-eqz v0, :cond_45

    .line 495
    iget-object v1, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 496
    iget-object v1, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 498
    :cond_45
    iget-object v0, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2b
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/v4/app/gb;
    .registers 4

    .prologue
    .line 432
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 433
    return-object p0
.end method

.method private b(Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 5

    .prologue
    .line 448
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.HTML_TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 451
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 1432
    iget-object v1, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 453
    :cond_1c
    return-object p0
.end method

.method private b([Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 3

    .prologue
    .line 541
    const-string v0, "android.intent.extra.EMAIL"

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/gb;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 542
    return-object p0
.end method

.method private c()Landroid/app/Activity;
    .registers 2

    .prologue
    .line 341
    iget-object v0, p0, Landroid/support/v4/app/gb;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 3

    .prologue
    .line 526
    iget-object v0, p0, Landroid/support/v4/app/gb;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    .line 527
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/gb;->d:Ljava/util/ArrayList;

    .line 529
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 530
    return-object p0
.end method

.method private c([Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 4

    .prologue
    .line 554
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.CC"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 555
    return-object p0
.end method

.method private d(Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 3

    .prologue
    .line 566
    iget-object v0, p0, Landroid/support/v4/app/gb;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    .line 567
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/gb;->e:Ljava/util/ArrayList;

    .line 569
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/gb;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    return-object p0
.end method

.method private d([Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 3

    .prologue
    .line 581
    const-string v0, "android.intent.extra.CC"

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/gb;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 582
    return-object p0
.end method

.method private d()V
    .registers 3

    .prologue
    .line 387
    iget-object v0, p0, Landroid/support/v4/app/gb;->a:Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/support/v4/app/gb;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 388
    return-void
.end method

.method private e(Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 3

    .prologue
    .line 606
    iget-object v0, p0, Landroid/support/v4/app/gb;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_b

    .line 607
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/gb;->f:Ljava/util/ArrayList;

    .line 609
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/gb;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    return-object p0
.end method

.method private e([Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 4

    .prologue
    .line 594
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.BCC"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 595
    return-object p0
.end method

.method private f(Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 4

    .prologue
    .line 633
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 634
    return-object p0
.end method

.method private f([Ljava/lang/String;)Landroid/support/v4/app/gb;
    .registers 3

    .prologue
    .line 621
    const-string v0, "android.intent.extra.BCC"

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/gb;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 622
    return-object p0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 297
    iget-object v1, p0, Landroid/support/v4/app/gb;->d:Ljava/util/ArrayList;

    if-eqz v1, :cond_10

    .line 298
    const-string v1, "android.intent.extra.EMAIL"

    iget-object v3, p0, Landroid/support/v4/app/gb;->d:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v3}, Landroid/support/v4/app/gb;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 299
    iput-object v6, p0, Landroid/support/v4/app/gb;->d:Ljava/util/ArrayList;

    .line 301
    :cond_10
    iget-object v1, p0, Landroid/support/v4/app/gb;->e:Ljava/util/ArrayList;

    if-eqz v1, :cond_1d

    .line 302
    const-string v1, "android.intent.extra.CC"

    iget-object v3, p0, Landroid/support/v4/app/gb;->e:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v3}, Landroid/support/v4/app/gb;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 303
    iput-object v6, p0, Landroid/support/v4/app/gb;->e:Ljava/util/ArrayList;

    .line 305
    :cond_1d
    iget-object v1, p0, Landroid/support/v4/app/gb;->f:Ljava/util/ArrayList;

    if-eqz v1, :cond_2a

    .line 306
    const-string v1, "android.intent.extra.BCC"

    iget-object v3, p0, Landroid/support/v4/app/gb;->f:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v3}, Landroid/support/v4/app/gb;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 307
    iput-object v6, p0, Landroid/support/v4/app/gb;->f:Ljava/util/ArrayList;

    .line 311
    :cond_2a
    iget-object v1, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    if-eqz v1, :cond_8e

    iget-object v1, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v0, :cond_8e

    move v1, v0

    .line 312
    :goto_37
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 314
    if-nez v1, :cond_6b

    if-eqz v3, :cond_6b

    .line 317
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v4, "android.intent.action.SEND"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    iget-object v0, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_90

    iget-object v0, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_90

    .line 319
    iget-object v4, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v5, "android.intent.extra.STREAM"

    iget-object v0, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 323
    :goto_69
    iput-object v6, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    .line 326
    :cond_6b
    if-eqz v1, :cond_8b

    if-nez v3, :cond_8b

    .line 329
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    iget-object v0, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_98

    iget-object v0, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_98

    .line 331
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    iget-object v2, p0, Landroid/support/v4/app/gb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 337
    :cond_8b
    :goto_8b
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    return-object v0

    :cond_8e
    move v1, v2

    .line 311
    goto :goto_37

    .line 321
    :cond_90
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_69

    .line 333
    :cond_98
    iget-object v0, p0, Landroid/support/v4/app/gb;->b:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_8b
.end method

.method public final b()Landroid/content/Intent;
    .registers 3

    .prologue
    .line 374
    invoke-virtual {p0}, Landroid/support/v4/app/gb;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/gb;->c:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
