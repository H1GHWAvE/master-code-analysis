.class final Landroid/support/v4/b/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/support/v4/b/f;


# direct methods
.method constructor <init>(Landroid/support/v4/b/f;)V
    .registers 2

    .prologue
    .line 54
    iput-object p1, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 57
    iget-object v0, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    .line 1108
    iget-object v0, v0, Landroid/support/v4/b/f;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingTime()J

    move-result-wide v2

    .line 57
    iget-object v0, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    .line 2038
    iget-wide v4, v0, Landroid/support/v4/b/f;->d:J

    .line 57
    sub-long/2addr v2, v4

    .line 58
    long-to-float v0, v2

    mul-float/2addr v0, v1

    iget-object v2, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    .line 3038
    iget-wide v2, v2, Landroid/support/v4/b/f;->e:J

    .line 58
    long-to-float v2, v2

    div-float/2addr v0, v2

    .line 59
    cmpl-float v2, v0, v1

    if-gtz v2, :cond_25

    iget-object v2, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    iget-object v2, v2, Landroid/support/v4/b/f;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_26

    :cond_25
    move v0, v1

    .line 62
    :cond_26
    iget-object v2, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    .line 4038
    iput v0, v2, Landroid/support/v4/b/f;->f:F

    .line 63
    iget-object v2, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    .line 5073
    iget-object v0, v2, Landroid/support/v4/b/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_34
    if-ltz v0, :cond_3e

    .line 5074
    iget-object v3, v2, Landroid/support/v4/b/f;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 5073
    add-int/lit8 v0, v0, -0x1

    goto :goto_34

    .line 64
    :cond_3e
    iget-object v0, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    .line 6038
    iget v0, v0, Landroid/support/v4/b/f;->f:F

    .line 64
    cmpl-float v0, v0, v1

    if-ltz v0, :cond_4c

    .line 65
    iget-object v0, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    .line 7038
    invoke-virtual {v0}, Landroid/support/v4/b/f;->b()V

    .line 69
    :goto_4b
    return-void

    .line 67
    :cond_4c
    iget-object v0, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    iget-object v0, v0, Landroid/support/v4/b/f;->c:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/b/g;->a:Landroid/support/v4/b/f;

    .line 8038
    iget-object v1, v1, Landroid/support/v4/b/f;->g:Ljava/lang/Runnable;

    .line 67
    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_4b
.end method
