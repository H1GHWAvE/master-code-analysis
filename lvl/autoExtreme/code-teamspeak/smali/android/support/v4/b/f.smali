.class final Landroid/support/v4/b/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/b/l;


# instance fields
.field a:Ljava/util/List;

.field b:Ljava/util/List;

.field c:Landroid/view/View;

.field d:J

.field e:J

.field f:F

.field g:Ljava/lang/Runnable;

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/b/f;->b:Ljava/util/List;

    .line 45
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Landroid/support/v4/b/f;->e:J

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/b/f;->f:F

    .line 48
    iput-boolean v2, p0, Landroid/support/v4/b/f;->h:Z

    .line 49
    iput-boolean v2, p0, Landroid/support/v4/b/f;->i:Z

    .line 54
    new-instance v0, Landroid/support/v4/b/g;

    invoke-direct {v0, p0}, Landroid/support/v4/b/g;-><init>(Landroid/support/v4/b/f;)V

    iput-object v0, p0, Landroid/support/v4/b/f;->g:Ljava/lang/Runnable;

    .line 52
    return-void
.end method

.method private static synthetic a(Landroid/support/v4/b/f;F)F
    .registers 2

    .prologue
    .line 38
    iput p1, p0, Landroid/support/v4/b/f;->f:F

    return p1
.end method

.method private static synthetic a(Landroid/support/v4/b/f;)J
    .registers 3

    .prologue
    .line 38
    .line 3108
    iget-object v0, p0, Landroid/support/v4/b/f;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingTime()J

    move-result-wide v0

    .line 38
    return-wide v0
.end method

.method private static synthetic b(Landroid/support/v4/b/f;)J
    .registers 3

    .prologue
    .line 38
    iget-wide v0, p0, Landroid/support/v4/b/f;->d:J

    return-wide v0
.end method

.method private static synthetic c(Landroid/support/v4/b/f;)J
    .registers 3

    .prologue
    .line 38
    iget-wide v0, p0, Landroid/support/v4/b/f;->e:J

    return-wide v0
.end method

.method private static synthetic d(Landroid/support/v4/b/f;)V
    .registers 3

    .prologue
    .line 38
    .line 4073
    iget-object v0, p0, Landroid/support/v4/b/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-ltz v0, :cond_12

    .line 4074
    iget-object v1, p0, Landroid/support/v4/b/f;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4073
    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .line 38
    :cond_12
    return-void
.end method

.method private static synthetic e(Landroid/support/v4/b/f;)F
    .registers 2

    .prologue
    .line 38
    iget v0, p0, Landroid/support/v4/b/f;->f:F

    return v0
.end method

.method private e()V
    .registers 3

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v4/b/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-ltz v0, :cond_12

    .line 74
    iget-object v1, p0, Landroid/support/v4/b/f;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 73
    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .line 76
    :cond_12
    return-void
.end method

.method private f()J
    .registers 3

    .prologue
    .line 108
    iget-object v0, p0, Landroid/support/v4/b/f;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingTime()J

    move-result-wide v0

    return-wide v0
.end method

.method private static synthetic f(Landroid/support/v4/b/f;)V
    .registers 1

    .prologue
    .line 38
    invoke-virtual {p0}, Landroid/support/v4/b/f;->b()V

    return-void
.end method

.method private static synthetic g(Landroid/support/v4/b/f;)Ljava/lang/Runnable;
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Landroid/support/v4/b/f;->g:Ljava/lang/Runnable;

    return-object v0
.end method

.method private g()V
    .registers 3

    .prologue
    .line 112
    iget-object v0, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-ltz v0, :cond_12

    .line 113
    iget-object v1, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 112
    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .line 115
    :cond_12
    return-void
.end method

.method private h()V
    .registers 3

    .prologue
    .line 124
    iget-object v0, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-ltz v0, :cond_12

    .line 125
    iget-object v1, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 124
    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .line 127
    :cond_12
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 5

    .prologue
    .line 97
    iget-boolean v0, p0, Landroid/support/v4/b/f;->h:Z

    if-eqz v0, :cond_5

    .line 105
    :goto_4
    return-void

    .line 100
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/b/f;->h:Z

    .line 1112
    iget-object v0, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_10
    if-ltz v0, :cond_1a

    .line 1113
    iget-object v1, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 1112
    add-int/lit8 v0, v0, -0x1

    goto :goto_10

    .line 102
    :cond_1a
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/b/f;->f:F

    .line 2108
    iget-object v0, p0, Landroid/support/v4/b/f;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingTime()J

    move-result-wide v0

    .line 103
    iput-wide v0, p0, Landroid/support/v4/b/f;->d:J

    .line 104
    iget-object v0, p0, Landroid/support/v4/b/f;->c:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/b/f;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_4
.end method

.method public final a(J)V
    .registers 4

    .prologue
    .line 90
    iget-boolean v0, p0, Landroid/support/v4/b/f;->h:Z

    if-nez v0, :cond_6

    .line 91
    iput-wide p1, p0, Landroid/support/v4/b/f;->e:J

    .line 93
    :cond_6
    return-void
.end method

.method public final a(Landroid/support/v4/b/b;)V
    .registers 3

    .prologue
    .line 85
    iget-object v0, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public final a(Landroid/support/v4/b/d;)V
    .registers 3

    .prologue
    .line 143
    iget-object v0, p0, Landroid/support/v4/b/f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 80
    iput-object p1, p0, Landroid/support/v4/b/f;->c:Landroid/view/View;

    .line 81
    return-void
.end method

.method final b()V
    .registers 3

    .prologue
    .line 118
    iget-object v0, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-ltz v0, :cond_12

    .line 119
    iget-object v1, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 118
    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .line 121
    :cond_12
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 131
    iget-boolean v0, p0, Landroid/support/v4/b/f;->i:Z

    if-eqz v0, :cond_5

    .line 139
    :goto_4
    return-void

    .line 134
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/b/f;->i:Z

    .line 135
    iget-boolean v0, p0, Landroid/support/v4/b/f;->h:Z

    if-eqz v0, :cond_1e

    .line 2124
    iget-object v0, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_14
    if-ltz v0, :cond_1e

    .line 2125
    iget-object v1, p0, Landroid/support/v4/b/f;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 2124
    add-int/lit8 v0, v0, -0x1

    goto :goto_14

    .line 138
    :cond_1e
    invoke-virtual {p0}, Landroid/support/v4/b/f;->b()V

    goto :goto_4
.end method

.method public final d()F
    .registers 2

    .prologue
    .line 148
    iget v0, p0, Landroid/support/v4/b/f;->f:F

    return v0
.end method
