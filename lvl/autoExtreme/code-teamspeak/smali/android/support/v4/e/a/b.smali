.class Landroid/support/v4/e/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/e/a/c;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 52
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;FF)V
    .registers 4

    .prologue
    .line 65
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;I)V
    .registers 3

    .prologue
    .line 73
    invoke-static {p1, p2}, Landroid/support/v4/e/a/l;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 74
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;IIII)V
    .registers 6

    .prologue
    .line 69
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
    .registers 3

    .prologue
    .line 78
    invoke-static {p1, p2}, Landroid/support/v4/e/a/l;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 79
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    .prologue
    .line 83
    invoke-static {p1, p2}, Landroid/support/v4/e/a/l;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 84
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Z)V
    .registers 3

    .prologue
    .line 56
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;I)V
    .registers 3

    .prologue
    .line 94
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)Z
    .registers 3

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 88
    .line 1047
    instance-of v0, p1, Landroid/support/v4/e/a/r;

    if-nez v0, :cond_a

    .line 1048
    new-instance v0, Landroid/support/v4/e/a/r;

    invoke-direct {v0, p1}, Landroid/support/v4/e/a/r;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object p1, v0

    :cond_a
    return-object p1
.end method

.method public d(Landroid/graphics/drawable/Drawable;)I
    .registers 3

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method
