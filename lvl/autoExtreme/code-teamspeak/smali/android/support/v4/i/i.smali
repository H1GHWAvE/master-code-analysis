.class public final Landroid/support/v4/i/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method private static a(Landroid/support/v4/i/k;)Landroid/os/Parcelable$Creator;
    .registers 3

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_c

    .line 1024
    new-instance v0, Landroid/support/v4/i/l;

    invoke-direct {v0, p0}, Landroid/support/v4/i/l;-><init>(Landroid/support/v4/i/k;)V

    .line 39
    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Landroid/support/v4/i/j;

    invoke-direct {v0, p0}, Landroid/support/v4/i/j;-><init>(Landroid/support/v4/i/k;)V

    goto :goto_b
.end method
