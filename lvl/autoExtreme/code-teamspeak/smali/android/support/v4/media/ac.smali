.class public abstract Landroid/support/v4/media/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:I = 0x1

.field static final b:I = 0x2

.field static final c:I = 0x3

.field static final d:I = -0x1

.field static final e:I = -0x2

.field static final f:I = -0x3


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(I)Z
    .registers 2

    .prologue
    const/4 v0, 0x1

    .line 127
    sparse-switch p0, :sswitch_data_6

    .line 145
    :sswitch_4
    return v0

    .line 127
    nop

    :sswitch_data_6
    .sparse-switch
        0x56 -> :sswitch_4
        0x7e -> :sswitch_4
        0x7f -> :sswitch_4
    .end sparse-switch
.end method

.method private static b(I)V
    .registers 10

    .prologue
    .line 188
    const/4 v0, 0x0

    .line 189
    packed-switch p0, :pswitch_data_22

    .line 196
    :goto_4
    if-eqz v0, :cond_1e

    .line 197
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 198
    new-instance v1, Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/16 v7, 0x7f

    const/4 v8, 0x0

    move-wide v4, v2

    invoke-direct/range {v1 .. v8}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 199
    new-instance v1, Landroid/view/KeyEvent;

    const/4 v6, 0x1

    const/16 v7, 0x7f

    const/4 v8, 0x0

    move-wide v4, v2

    invoke-direct/range {v1 .. v8}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 201
    :cond_1e
    return-void

    .line 193
    :pswitch_1f
    const/16 v0, 0x7f

    goto :goto_4

    .line 189
    :pswitch_data_22
    .packed-switch -0x1
        :pswitch_1f
    .end packed-switch
.end method

.method private static h()I
    .registers 1

    .prologue
    .line 76
    const/16 v0, 0x64

    return v0
.end method

.method private static i()I
    .registers 1

    .prologue
    .line 98
    const/16 v0, 0x3c

    return v0
.end method

.method private static j()Z
    .registers 1

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public abstract d()J
.end method

.method public abstract e()J
.end method

.method public abstract f()V
.end method

.method public abstract g()Z
.end method
