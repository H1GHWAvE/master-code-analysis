.class final Landroid/support/v4/media/session/am;
.super Landroid/support/v4/media/session/e;
.source "SourceFile"


# instance fields
.field final synthetic G:Landroid/support/v4/media/session/ai;


# direct methods
.method constructor <init>(Landroid/support/v4/media/session/ai;)V
    .registers 2

    .prologue
    .line 1512
    iput-object p1, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    invoke-direct {p0}, Landroid/support/v4/media/session/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/String;)V
    .registers 5

    .prologue
    .line 1602
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    invoke-static {v0, p1, p2}, Landroid/support/v4/media/session/ai;->a(Landroid/support/v4/media/session/ai;II)V

    .line 1603
    return-void
.end method

.method public final a(J)V
    .registers 6

    .prologue
    .line 1632
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 24963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1632
    const/4 v1, 0x4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1633
    return-void
.end method

.method public final a(Landroid/net/Uri;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 1627
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 23963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1627
    const/16 v1, 0x12

    invoke-virtual {v0, v1, p1, p2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;Landroid/os/Bundle;)V

    .line 1628
    return-void
.end method

.method public final a(Landroid/support/v4/media/RatingCompat;)V
    .registers 4

    .prologue
    .line 1672
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 32963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1672
    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1673
    return-void
.end method

.method public final a(Landroid/support/v4/media/session/a;)V
    .registers 3

    .prologue
    .line 1533
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 4963
    iget-boolean v0, v0, Landroid/support/v4/media/session/ai;->g:Z

    .line 1533
    if-eqz v0, :cond_a

    .line 1535
    :try_start_6
    invoke-interface {p1}, Landroid/support/v4/media/session/a;->a()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_9} :catch_12

    .line 1542
    :goto_9
    return-void

    .line 1541
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 5963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    .line 1541
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    goto :goto_9

    .line 1539
    :catch_12
    move-exception v0

    goto :goto_9
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 1617
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 21963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1617
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1, p2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;Landroid/os/Bundle;)V

    .line 1618
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;)V
    .registers 8

    .prologue
    .line 1515
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 1963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1515
    const/16 v1, 0xf

    new-instance v2, Landroid/support/v4/media/session/al;

    invoke-static {p3}, Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;->a(Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;)Landroid/os/ResultReceiver;

    move-result-object v3

    invoke-direct {v2, p1, p2, v3}, Landroid/support/v4/media/session/al;-><init>(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1517
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 1718
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 42963
    iget v0, v0, Landroid/support/v4/media/session/ai;->i:I

    .line 1718
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .registers 5

    .prologue
    .line 1521
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 2963
    iget v0, v0, Landroid/support/v4/media/session/ai;->i:I

    .line 1521
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    .line 1523
    :goto_9
    if-eqz v0, :cond_14

    .line 1524
    iget-object v1, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 3963
    iget-object v1, v1, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1524
    const/16 v2, 0xe

    invoke-virtual {v1, v2, p1}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1526
    :cond_14
    return v0

    .line 1521
    :cond_15
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1552
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 7963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->b:Ljava/lang/String;

    .line 1552
    return-object v0
.end method

.method public final b(IILjava/lang/String;)V
    .registers 5

    .prologue
    .line 1607
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    invoke-static {v0, p1, p2}, Landroid/support/v4/media/session/ai;->b(Landroid/support/v4/media/session/ai;II)V

    .line 1608
    return-void
.end method

.method public final b(J)V
    .registers 6

    .prologue
    .line 1667
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 31963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1667
    const/16 v1, 0xb

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1668
    return-void
.end method

.method public final b(Landroid/support/v4/media/session/a;)V
    .registers 3

    .prologue
    .line 1546
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 6963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    .line 1546
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 1547
    return-void
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 1622
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 22963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1622
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, p2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;Landroid/os/Bundle;)V

    .line 1623
    return-void
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1558
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 8963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->c:Ljava/lang/String;

    .line 1558
    return-object v0
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 1678
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 33963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1678
    const/16 v1, 0xd

    invoke-virtual {v0, v1, p1, p2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;Landroid/os/Bundle;)V

    .line 1679
    return-void
.end method

.method public final d()Landroid/app/PendingIntent;
    .registers 3

    .prologue
    .line 1563
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 9963
    iget-object v1, v0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    .line 1563
    monitor-enter v1

    .line 1564
    :try_start_5
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 10963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->l:Landroid/app/PendingIntent;

    .line 1564
    monitor-exit v1

    return-object v0

    .line 1565
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public final e()J
    .registers 5

    .prologue
    .line 1571
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 11963
    iget-object v1, v0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    .line 1571
    monitor-enter v1

    .line 1572
    :try_start_5
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 12963
    iget v0, v0, Landroid/support/v4/media/session/ai;->i:I

    .line 1572
    int-to-long v2, v0

    monitor-exit v1

    return-wide v2

    .line 1573
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public final f()Landroid/support/v4/media/session/ParcelableVolumeInfo;
    .registers 8

    .prologue
    const/4 v3, 0x2

    .line 1583
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 13963
    iget-object v6, v0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    .line 1583
    monitor-enter v6

    .line 1584
    :try_start_6
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 14963
    iget v1, v0, Landroid/support/v4/media/session/ai;->q:I

    .line 1585
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 15963
    iget v2, v0, Landroid/support/v4/media/session/ai;->r:I

    .line 1586
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 16963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    .line 1587
    if-ne v1, v3, :cond_21

    .line 17099
    iget v3, v0, Landroid/support/v4/media/ae;->d:I

    .line 17108
    iget v4, v0, Landroid/support/v4/media/ae;->e:I

    .line 18089
    iget v5, v0, Landroid/support/v4/media/ae;->f:I

    .line 1596
    :goto_1a
    monitor-exit v6
    :try_end_1b
    .catchall {:try_start_6 .. :try_end_1b} :catchall_32

    .line 1597
    new-instance v0, Landroid/support/v4/media/session/ParcelableVolumeInfo;

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/media/session/ParcelableVolumeInfo;-><init>(IIIII)V

    return-object v0

    .line 1593
    :cond_21
    :try_start_21
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 18963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    .line 1593
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v4

    .line 1594
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 19963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    .line 1594
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v5

    goto :goto_1a

    .line 1596
    :catchall_32
    move-exception v0

    monitor-exit v6
    :try_end_34
    .catchall {:try_start_21 .. :try_end_34} :catchall_32

    throw v0
.end method

.method public final g()V
    .registers 4

    .prologue
    .line 1612
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 20963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 21774
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1613
    return-void
.end method

.method public final h()V
    .registers 4

    .prologue
    .line 1637
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 25963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 26774
    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1638
    return-void
.end method

.method public final i()V
    .registers 4

    .prologue
    .line 1642
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 26963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 27774
    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1643
    return-void
.end method

.method public final j()V
    .registers 4

    .prologue
    .line 1647
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 27963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 28774
    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1648
    return-void
.end method

.method public final k()V
    .registers 4

    .prologue
    .line 1652
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 28963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 29774
    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1653
    return-void
.end method

.method public final l()V
    .registers 4

    .prologue
    .line 1657
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 29963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 30774
    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1658
    return-void
.end method

.method public final m()V
    .registers 4

    .prologue
    .line 1662
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 30963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 31774
    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/session/an;->a(ILjava/lang/Object;)V

    .line 1663
    return-void
.end method

.method public final n()Landroid/support/v4/media/MediaMetadataCompat;
    .registers 2

    .prologue
    .line 1683
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 34963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->j:Landroid/support/v4/media/MediaMetadataCompat;

    .line 1683
    return-object v0
.end method

.method public final o()Landroid/support/v4/media/session/PlaybackStateCompat;
    .registers 2

    .prologue
    .line 1688
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 35963
    invoke-virtual {v0}, Landroid/support/v4/media/session/ai;->f()Landroid/support/v4/media/session/PlaybackStateCompat;

    move-result-object v0

    .line 1688
    return-object v0
.end method

.method public final p()Ljava/util/List;
    .registers 3

    .prologue
    .line 1693
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 36963
    iget-object v1, v0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    .line 1693
    monitor-enter v1

    .line 1694
    :try_start_5
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 37963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->m:Ljava/util/List;

    .line 1694
    monitor-exit v1

    return-object v0

    .line 1695
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public final q()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1700
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 38963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->n:Ljava/lang/CharSequence;

    .line 1700
    return-object v0
.end method

.method public final r()Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 1705
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 39963
    iget-object v1, v0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    .line 1705
    monitor-enter v1

    .line 1706
    :try_start_5
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 40963
    iget-object v0, v0, Landroid/support/v4/media/session/ai;->p:Landroid/os/Bundle;

    .line 1706
    monitor-exit v1

    return-object v0

    .line 1707
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public final s()I
    .registers 2

    .prologue
    .line 1713
    iget-object v0, p0, Landroid/support/v4/media/session/am;->G:Landroid/support/v4/media/session/ai;

    .line 41963
    iget v0, v0, Landroid/support/v4/media/session/ai;->o:I

    .line 1713
    return v0
.end method
