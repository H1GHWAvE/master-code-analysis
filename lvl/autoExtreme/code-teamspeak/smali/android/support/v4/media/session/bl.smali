.class public final Landroid/support/v4/media/session/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field b:I

.field c:J

.field d:J

.field e:F

.field f:J

.field g:Ljava/lang/CharSequence;

.field h:J

.field i:J

.field j:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 760
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/media/session/bl;->a:Ljava/util/List;

    .line 769
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v4/media/session/bl;->i:J

    .line 776
    return-void
.end method

.method public constructor <init>(Landroid/support/v4/media/session/PlaybackStateCompat;)V
    .registers 4

    .prologue
    .line 784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 760
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/media/session/bl;->a:Ljava/util/List;

    .line 769
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v4/media/session/bl;->i:J

    .line 785
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->a(Landroid/support/v4/media/session/PlaybackStateCompat;)I

    move-result v0

    iput v0, p0, Landroid/support/v4/media/session/bl;->b:I

    .line 786
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->b(Landroid/support/v4/media/session/PlaybackStateCompat;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/media/session/bl;->c:J

    .line 787
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->c(Landroid/support/v4/media/session/PlaybackStateCompat;)F

    move-result v0

    iput v0, p0, Landroid/support/v4/media/session/bl;->e:F

    .line 788
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->d(Landroid/support/v4/media/session/PlaybackStateCompat;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/media/session/bl;->h:J

    .line 789
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->e(Landroid/support/v4/media/session/PlaybackStateCompat;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/media/session/bl;->d:J

    .line 790
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->f(Landroid/support/v4/media/session/PlaybackStateCompat;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/media/session/bl;->f:J

    .line 791
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->g(Landroid/support/v4/media/session/PlaybackStateCompat;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/session/bl;->g:Ljava/lang/CharSequence;

    .line 792
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->h(Landroid/support/v4/media/session/PlaybackStateCompat;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_47

    .line 793
    iget-object v0, p0, Landroid/support/v4/media/session/bl;->a:Ljava/util/List;

    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->h(Landroid/support/v4/media/session/PlaybackStateCompat;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 795
    :cond_47
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->i(Landroid/support/v4/media/session/PlaybackStateCompat;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/media/session/bl;->i:J

    .line 796
    invoke-static {p1}, Landroid/support/v4/media/session/PlaybackStateCompat;->j(Landroid/support/v4/media/session/PlaybackStateCompat;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/session/bl;->j:Landroid/os/Bundle;

    .line 797
    return-void
.end method

.method private a()Landroid/support/v4/media/session/PlaybackStateCompat;
    .registers 20

    .prologue
    .line 998
    new-instance v2, Landroid/support/v4/media/session/PlaybackStateCompat;

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/media/session/bl;->b:I

    move-object/from16 v0, p0

    iget-wide v4, v0, Landroid/support/v4/media/session/bl;->c:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Landroid/support/v4/media/session/bl;->d:J

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v4/media/session/bl;->e:F

    move-object/from16 v0, p0

    iget-wide v9, v0, Landroid/support/v4/media/session/bl;->f:J

    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/v4/media/session/bl;->g:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-wide v12, v0, Landroid/support/v4/media/session/bl;->h:J

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v4/media/session/bl;->a:Ljava/util/List;

    move-object/from16 v0, p0

    iget-wide v15, v0, Landroid/support/v4/media/session/bl;->i:J

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/media/session/bl;->j:Landroid/os/Bundle;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-direct/range {v2 .. v18}, Landroid/support/v4/media/session/PlaybackStateCompat;-><init>(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;B)V

    return-object v2
.end method

.method private a(IJF)Landroid/support/v4/media/session/bl;
    .registers 13

    .prologue
    .line 831
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move v4, p4

    invoke-virtual/range {v0 .. v6}, Landroid/support/v4/media/session/bl;->a(IJFJ)Landroid/support/v4/media/session/bl;

    move-result-object v0

    return-object v0
.end method

.method private a(J)Landroid/support/v4/media/session/bl;
    .registers 4

    .prologue
    .line 885
    iput-wide p1, p0, Landroid/support/v4/media/session/bl;->d:J

    .line 886
    return-object p0
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/media/session/bl;
    .registers 2

    .prologue
    .line 990
    iput-object p1, p0, Landroid/support/v4/media/session/bl;->j:Landroid/os/Bundle;

    .line 991
    return-object p0
.end method

.method private a(Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;)Landroid/support/v4/media/session/bl;
    .registers 3

    .prologue
    .line 956
    iget-object v0, p0, Landroid/support/v4/media/session/bl;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 957
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/media/session/bl;
    .registers 2

    .prologue
    .line 979
    iput-object p1, p0, Landroid/support/v4/media/session/bl;->g:Ljava/lang/CharSequence;

    .line 980
    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v4/media/session/bl;
    .registers 10

    .prologue
    .line 937
    new-instance v0, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;ILandroid/os/Bundle;B)V

    .line 1956
    iget-object v1, p0, Landroid/support/v4/media/session/bl;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 937
    return-object p0
.end method

.method private b(J)Landroid/support/v4/media/session/bl;
    .registers 4

    .prologue
    .line 912
    iput-wide p1, p0, Landroid/support/v4/media/session/bl;->f:J

    .line 913
    return-object p0
.end method

.method private c(J)Landroid/support/v4/media/session/bl;
    .registers 4

    .prologue
    .line 968
    iput-wide p1, p0, Landroid/support/v4/media/session/bl;->i:J

    .line 969
    return-object p0
.end method


# virtual methods
.method public final a(IJFJ)Landroid/support/v4/media/session/bl;
    .registers 7

    .prologue
    .line 870
    iput p1, p0, Landroid/support/v4/media/session/bl;->b:I

    .line 871
    iput-wide p2, p0, Landroid/support/v4/media/session/bl;->c:J

    .line 872
    iput-wide p5, p0, Landroid/support/v4/media/session/bl;->h:J

    .line 873
    iput p4, p0, Landroid/support/v4/media/session/bl;->e:F

    .line 874
    return-object p0
.end method
