.class Landroid/support/v4/view/cz;
.super Landroid/support/v4/view/cy;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 968
    invoke-direct {p0}, Landroid/support/v4/view/cy;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    .line 1037
    sget-object v0, Landroid/support/v4/view/dk;->b:Ljava/lang/reflect/Method;

    if-nez v0, :cond_1c

    .line 1039
    :try_start_5
    const-class v0, Landroid/view/ViewGroup;

    const-string v1, "setChildrenDrawingOrderEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Landroid/support/v4/view/dk;->b:Ljava/lang/reflect/Method;
    :try_end_17
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_17} :catch_2d

    .line 1044
    :goto_17
    sget-object v0, Landroid/support/v4/view/dk;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1047
    :cond_1c
    :try_start_1c
    sget-object v0, Landroid/support/v4/view/dk;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2c
    .catch Ljava/lang/IllegalAccessException; {:try_start_1c .. :try_end_2c} :catch_36
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1c .. :try_end_2c} :catch_3f
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1c .. :try_end_2c} :catch_48

    .line 1054
    :goto_2c
    return-void

    .line 1041
    :catch_2d
    move-exception v0

    .line 1042
    const-string v1, "ViewCompat"

    const-string v2, "Unable to find childrenDrawingOrderEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_17

    .line 1048
    :catch_36
    move-exception v0

    .line 1049
    const-string v1, "ViewCompat"

    const-string v2, "Unable to invoke childrenDrawingOrderEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2c

    .line 1050
    :catch_3f
    move-exception v0

    .line 1051
    const-string v1, "ViewCompat"

    const-string v2, "Unable to invoke childrenDrawingOrderEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2c

    .line 1052
    :catch_48
    move-exception v0

    .line 1053
    const-string v1, "ViewCompat"

    const-string v2, "Unable to invoke childrenDrawingOrderEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2c
.end method

.method public final m(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 971
    .line 1033
    invoke-virtual {p1}, Landroid/view/View;->isOpaque()Z

    move-result v0

    .line 971
    return v0
.end method
