.class public final Landroid/support/v4/view/fk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final b:I = 0x7e000000

.field public static final c:Landroid/support/v4/view/fu;

.field private static final d:Ljava/lang/String; = "ViewAnimatorCompat"


# instance fields
.field public a:Ljava/lang/ref/WeakReference;

.field private e:Ljava/lang/Runnable;

.field private f:Ljava/lang/Runnable;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 645
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 646
    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    .line 647
    new-instance v0, Landroid/support/v4/view/ft;

    invoke-direct {v0}, Landroid/support/v4/view/ft;-><init>()V

    sput-object v0, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    .line 659
    :goto_d
    return-void

    .line 648
    :cond_e
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1a

    .line 649
    new-instance v0, Landroid/support/v4/view/fs;

    invoke-direct {v0}, Landroid/support/v4/view/fs;-><init>()V

    sput-object v0, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    goto :goto_d

    .line 650
    :cond_1a
    const/16 v1, 0x12

    if-lt v0, v1, :cond_26

    .line 651
    new-instance v0, Landroid/support/v4/view/fq;

    invoke-direct {v0}, Landroid/support/v4/view/fq;-><init>()V

    sput-object v0, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    goto :goto_d

    .line 652
    :cond_26
    const/16 v1, 0x10

    if-lt v0, v1, :cond_32

    .line 653
    new-instance v0, Landroid/support/v4/view/fr;

    invoke-direct {v0}, Landroid/support/v4/view/fr;-><init>()V

    sput-object v0, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    goto :goto_d

    .line 654
    :cond_32
    const/16 v1, 0xe

    if-lt v0, v1, :cond_3e

    .line 655
    new-instance v0, Landroid/support/v4/view/fo;

    invoke-direct {v0}, Landroid/support/v4/view/fo;-><init>()V

    sput-object v0, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    goto :goto_d

    .line 657
    :cond_3e
    new-instance v0, Landroid/support/v4/view/fm;

    invoke-direct {v0}, Landroid/support/v4/view/fm;-><init>()V

    sput-object v0, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    goto :goto_d
.end method

.method constructor <init>(Landroid/view/View;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Landroid/support/v4/view/fk;->e:Ljava/lang/Runnable;

    .line 28
    iput-object v0, p0, Landroid/support/v4/view/fk;->f:Ljava/lang/Runnable;

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/view/fk;->g:I

    .line 36
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    .line 37
    return-void
.end method

.method static synthetic a(Landroid/support/v4/view/fk;I)I
    .registers 2

    .prologue
    .line 24
    iput p1, p0, Landroid/support/v4/view/fk;->g:I

    return p1
.end method

.method private a(Ljava/lang/Runnable;)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 777
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 778
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->a(Landroid/support/v4/view/fk;Landroid/view/View;Ljava/lang/Runnable;)V

    .line 780
    :cond_f
    return-object p0
.end method

.method static synthetic a(Landroid/support/v4/view/fk;)Ljava/lang/Runnable;
    .registers 2

    .prologue
    .line 24
    iget-object v0, p0, Landroid/support/v4/view/fk;->e:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v4/view/fk;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .registers 2

    .prologue
    .line 24
    iput-object p1, p0, Landroid/support/v4/view/fk;->f:Ljava/lang/Runnable;

    return-object p1
.end method

.method private b(Ljava/lang/Runnable;)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1296
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1297
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->b(Landroid/support/v4/view/fk;Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1299
    :cond_f
    return-object p0
.end method

.method static synthetic b(Landroid/support/v4/view/fk;)Ljava/lang/Runnable;
    .registers 2

    .prologue
    .line 24
    iget-object v0, p0, Landroid/support/v4/view/fk;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v4/view/fk;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .registers 2

    .prologue
    .line 24
    iput-object p1, p0, Landroid/support/v4/view/fk;->e:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic c(Landroid/support/v4/view/fk;)I
    .registers 2

    .prologue
    .line 24
    iget v0, p0, Landroid/support/v4/view/fk;->g:I

    return v0
.end method

.method private c()J
    .registers 3

    .prologue
    .line 795
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_11

    .line 796
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0}, Landroid/support/v4/view/fu;->a(Landroid/view/View;)J

    move-result-wide v0

    .line 798
    :goto_10
    return-wide v0

    :cond_11
    const-wide/16 v0, 0x0

    goto :goto_10
.end method

.method private c(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 708
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 709
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->d(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 711
    :cond_f
    return-object p0
.end method

.method private d(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 725
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 726
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->b(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 728
    :cond_f
    return-object p0
.end method

.method private d()Landroid/view/animation/Interpolator;
    .registers 3

    .prologue
    .line 829
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_11

    .line 830
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0}, Landroid/support/v4/view/fu;->b(Landroid/view/View;)Landroid/view/animation/Interpolator;

    move-result-object v0

    .line 832
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private e()J
    .registers 3

    .prologue
    .line 866
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_11

    .line 867
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0}, Landroid/support/v4/view/fu;->c(Landroid/view/View;)J

    move-result-wide v0

    .line 869
    :goto_10
    return-wide v0

    :cond_11
    const-wide/16 v0, 0x0

    goto :goto_10
.end method

.method private e(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 884
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 885
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->e(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 887
    :cond_f
    return-object p0
.end method

.method private f()Landroid/support/v4/view/fk;
    .registers 3

    .prologue
    .line 986
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 987
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0}, Landroid/support/v4/view/fu;->a(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 989
    :cond_f
    return-object p0
.end method

.method private f(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 901
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 902
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->f(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 904
    :cond_f
    return-object p0
.end method

.method private g()Landroid/support/v4/view/fk;
    .registers 3

    .prologue
    .line 1020
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1021
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0}, Landroid/support/v4/view/fu;->b(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 1023
    :cond_f
    return-object p0
.end method

.method private g(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 918
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 919
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->g(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 921
    :cond_f
    return-object p0
.end method

.method private h()Landroid/support/v4/view/fk;
    .registers 3

    .prologue
    .line 1271
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1272
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0}, Landroid/support/v4/view/fu;->e(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 1274
    :cond_f
    return-object p0
.end method

.method private h(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 935
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 936
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->h(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 938
    :cond_f
    return-object p0
.end method

.method private i(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 952
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 953
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->i(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 955
    :cond_f
    return-object p0
.end method

.method private j(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 969
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 970
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->j(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 972
    :cond_f
    return-object p0
.end method

.method private k(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1003
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1004
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->k(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 1006
    :cond_f
    return-object p0
.end method

.method private l(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1037
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1038
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->l(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 1040
    :cond_f
    return-object p0
.end method

.method private m(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1064
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1065
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->m(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 1067
    :cond_f
    return-object p0
.end method

.method private n(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1081
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1082
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->n(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 1084
    :cond_f
    return-object p0
.end method

.method private o(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1098
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1099
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->o(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 1101
    :cond_f
    return-object p0
.end method

.method private p(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1115
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1116
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->p(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 1118
    :cond_f
    return-object p0
.end method

.method private q(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1132
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1133
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->q(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 1135
    :cond_f
    return-object p0
.end method

.method private r(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1149
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1150
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->r(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 1152
    :cond_f
    return-object p0
.end method

.method private s(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1166
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1167
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0, p1}, Landroid/support/v4/view/fu;->d(Landroid/view/View;F)V

    .line 1169
    :cond_f
    return-object p0
.end method

.method private t(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1183
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1184
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0, p1}, Landroid/support/v4/view/fu;->c(Landroid/view/View;F)V

    .line 1186
    :cond_f
    return-object p0
.end method

.method private u(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1200
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1201
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0, p1}, Landroid/support/v4/view/fu;->a(Landroid/view/View;F)V

    .line 1203
    :cond_f
    return-object p0
.end method

.method private v(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1217
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1218
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0, p1}, Landroid/support/v4/view/fu;->b(Landroid/view/View;F)V

    .line 1220
    :cond_f
    return-object p0
.end method


# virtual methods
.method public final a(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 691
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 692
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->a(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 694
    :cond_f
    return-object p0
.end method

.method public final a(J)Landroid/support/v4/view/fk;
    .registers 6

    .prologue
    .line 674
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 675
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0, p1, p2}, Landroid/support/v4/view/fu;->a(Landroid/view/View;J)V

    .line 677
    :cond_f
    return-object p0
.end method

.method public final a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1314
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1315
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->a(Landroid/support/v4/view/fk;Landroid/view/View;Landroid/support/v4/view/gd;)V

    .line 1317
    :cond_f
    return-object p0
.end method

.method public final a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 1333
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1334
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0, p1}, Landroid/support/v4/view/fu;->a(Landroid/view/View;Landroid/support/v4/view/gf;)V

    .line 1336
    :cond_f
    return-object p0
.end method

.method public final a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 814
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 815
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0, p1}, Landroid/support/v4/view/fu;->a(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 817
    :cond_f
    return-object p0
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 1048
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1049
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0}, Landroid/support/v4/view/fu;->c(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 1051
    :cond_f
    return-void
.end method

.method public final b(F)Landroid/support/v4/view/fk;
    .registers 4

    .prologue
    .line 742
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 743
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/view/fu;->c(Landroid/support/v4/view/fk;Landroid/view/View;F)V

    .line 745
    :cond_f
    return-object p0
.end method

.method public final b(J)Landroid/support/v4/view/fk;
    .registers 6

    .prologue
    .line 848
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 849
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, v0, p1, p2}, Landroid/support/v4/view/fu;->b(Landroid/view/View;J)V

    .line 851
    :cond_f
    return-object p0
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 1234
    iget-object v0, p0, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_f

    .line 1235
    sget-object v1, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v1, p0, v0}, Landroid/support/v4/view/fu;->d(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 1237
    :cond_f
    return-void
.end method
