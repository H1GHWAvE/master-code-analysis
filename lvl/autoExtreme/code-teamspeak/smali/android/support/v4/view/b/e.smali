.class public final Landroid/support/v4/view/b/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method private static a(FF)Landroid/view/animation/Interpolator;
    .registers 4

    .prologue
    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_c

    .line 2037
    new-instance v0, Landroid/view/animation/PathInterpolator;

    invoke-direct {v0, p0, p1}, Landroid/view/animation/PathInterpolator;-><init>(FF)V

    .line 65
    :goto_b
    return-object v0

    .line 3036
    :cond_c
    new-instance v0, Landroid/support/v4/view/b/h;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/view/b/h;-><init>(FF)V

    goto :goto_b
.end method

.method private static a(FFFF)Landroid/view/animation/Interpolator;
    .registers 6

    .prologue
    .line 80
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_c

    .line 3042
    new-instance v0, Landroid/view/animation/PathInterpolator;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    .line 83
    :goto_b
    return-object v0

    .line 4041
    :cond_c
    new-instance v0, Landroid/support/v4/view/b/h;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/support/v4/view/b/h;-><init>(FFFF)V

    goto :goto_b
.end method

.method private static a(Landroid/graphics/Path;)Landroid/view/animation/Interpolator;
    .registers 3

    .prologue
    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_c

    .line 1033
    new-instance v0, Landroid/view/animation/PathInterpolator;

    invoke-direct {v0, p0}, Landroid/view/animation/PathInterpolator;-><init>(Landroid/graphics/Path;)V

    .line 50
    :goto_b
    return-object v0

    .line 2032
    :cond_c
    new-instance v0, Landroid/support/v4/view/b/h;

    invoke-direct {v0, p0}, Landroid/support/v4/view/b/h;-><init>(Landroid/graphics/Path;)V

    goto :goto_b
.end method
