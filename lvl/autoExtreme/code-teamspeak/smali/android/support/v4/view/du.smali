.class public final Landroid/support/v4/view/du;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/view/dz;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 86
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_e

    .line 87
    new-instance v0, Landroid/support/v4/view/dy;

    invoke-direct {v0}, Landroid/support/v4/view/dy;-><init>()V

    sput-object v0, Landroid/support/v4/view/du;->a:Landroid/support/v4/view/dz;

    .line 95
    :goto_d
    return-void

    .line 88
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1c

    .line 89
    new-instance v0, Landroid/support/v4/view/dx;

    invoke-direct {v0}, Landroid/support/v4/view/dx;-><init>()V

    sput-object v0, Landroid/support/v4/view/du;->a:Landroid/support/v4/view/dz;

    goto :goto_d

    .line 90
    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_2a

    .line 91
    new-instance v0, Landroid/support/v4/view/dw;

    invoke-direct {v0}, Landroid/support/v4/view/dw;-><init>()V

    sput-object v0, Landroid/support/v4/view/du;->a:Landroid/support/v4/view/dz;

    goto :goto_d

    .line 93
    :cond_2a
    new-instance v0, Landroid/support/v4/view/dv;

    invoke-direct {v0}, Landroid/support/v4/view/dv;-><init>()V

    sput-object v0, Landroid/support/v4/view/du;->a:Landroid/support/v4/view/dz;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    return-void
.end method

.method public static a(Landroid/view/ViewConfiguration;)I
    .registers 2

    .prologue
    .line 105
    sget-object v0, Landroid/support/v4/view/du;->a:Landroid/support/v4/view/dz;

    invoke-interface {v0, p0}, Landroid/support/v4/view/dz;->a(Landroid/view/ViewConfiguration;)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/ViewConfiguration;)Z
    .registers 2

    .prologue
    .line 113
    sget-object v0, Landroid/support/v4/view/du;->a:Landroid/support/v4/view/dz;

    invoke-interface {v0, p0}, Landroid/support/v4/view/dz;->b(Landroid/view/ViewConfiguration;)Z

    move-result v0

    return v0
.end method
