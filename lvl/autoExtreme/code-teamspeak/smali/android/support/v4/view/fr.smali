.class Landroid/support/v4/view/fr;
.super Landroid/support/v4/view/fo;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 583
    invoke-direct {p0}, Landroid/support/v4/view/fo;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/view/fk;Landroid/view/View;Landroid/support/v4/view/gd;)V
    .registers 6

    .prologue
    .line 587
    .line 1038
    if-eqz p3, :cond_f

    .line 1039
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/support/v4/view/fy;

    invoke-direct {v1, p3, p2}, Landroid/support/v4/view/fy;-><init>(Landroid/support/v4/view/gd;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :goto_e
    return-void

    .line 1056
    :cond_f
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_e
.end method

.method public final a(Landroid/support/v4/view/fk;Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 5

    .prologue
    .line 597
    .line 2029
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 598
    return-void
.end method

.method public final b(Landroid/support/v4/view/fk;Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 5

    .prologue
    .line 592
    .line 2025
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 593
    return-void
.end method

.method public final e(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 4

    .prologue
    .line 602
    .line 2033
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 603
    return-void
.end method
