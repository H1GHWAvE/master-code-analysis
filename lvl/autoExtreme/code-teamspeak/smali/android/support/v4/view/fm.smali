.class Landroid/support/v4/view/fm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/fu;


# instance fields
.field a:Ljava/util/WeakHashMap;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    .line 316
    return-void
.end method

.method private static synthetic a(Landroid/support/v4/view/fm;Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 81
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/view/fm;->f(Landroid/support/v4/view/fk;Landroid/view/View;)V

    return-void
.end method

.method private d(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 336
    iget-object v0, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_11

    .line 337
    iget-object v0, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 338
    if-eqz v0, :cond_11

    .line 339
    invoke-virtual {p1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 342
    :cond_11
    return-void
.end method

.method private g(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 5

    .prologue
    .line 345
    const/4 v0, 0x0

    .line 346
    iget-object v1, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    if-eqz v1, :cond_d

    .line 347
    iget-object v0, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 349
    :cond_d
    if-nez v0, :cond_25

    .line 350
    new-instance v0, Landroid/support/v4/view/fn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Landroid/support/v4/view/fn;-><init>(Landroid/support/v4/view/fm;Landroid/support/v4/view/fk;Landroid/view/View;B)V

    .line 351
    iget-object v1, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    if-nez v1, :cond_20

    .line 352
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    .line 354
    :cond_20
    iget-object v1, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    :cond_25
    invoke-virtual {p2, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 357
    invoke-virtual {p2, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 358
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)J
    .registers 4

    .prologue
    .line 115
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public a(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 184
    return-void
.end method

.method public a(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 93
    return-void
.end method

.method public a(Landroid/support/v4/view/fk;Landroid/view/View;Landroid/support/v4/view/gd;)V
    .registers 5

    .prologue
    .line 285
    const/high16 v0, 0x7e000000

    invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 286
    return-void
.end method

.method public a(Landroid/support/v4/view/fk;Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 4

    .prologue
    .line 109
    invoke-static {p1, p3}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/fk;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 110
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 111
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 237
    return-void
.end method

.method public a(Landroid/view/View;J)V
    .registers 4

    .prologue
    .line 87
    return-void
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/gf;)V
    .registers 3

    .prologue
    .line 291
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/animation/Interpolator;)V
    .registers 3

    .prologue
    .line 121
    return-void
.end method

.method public b(Landroid/view/View;)Landroid/view/animation/Interpolator;
    .registers 3

    .prologue
    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 195
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 196
    return-void
.end method

.method public b(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 99
    return-void
.end method

.method public b(Landroid/support/v4/view/fk;Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 4

    .prologue
    .line 279
    invoke-static {p1, p3}, Landroid/support/v4/view/fk;->b(Landroid/support/v4/view/fk;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 280
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 281
    return-void
.end method

.method public b(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 242
    return-void
.end method

.method public b(Landroid/view/View;J)V
    .registers 4

    .prologue
    .line 131
    return-void
.end method

.method public c(Landroid/view/View;)J
    .registers 4

    .prologue
    .line 135
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public c(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 207
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 208
    return-void
.end method

.method public c(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 105
    return-void
.end method

.method public c(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 259
    return-void
.end method

.method public d(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 4

    .prologue
    .line 268
    .line 1336
    iget-object v0, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_11

    .line 1337
    iget-object v0, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 1338
    if-eqz v0, :cond_11

    .line 1339
    invoke-virtual {p2, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 269
    :cond_11
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/view/fm;->f(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 270
    return-void
.end method

.method public d(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 142
    return-void
.end method

.method public d(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 264
    return-void
.end method

.method public e(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 275
    return-void
.end method

.method public e(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 148
    return-void
.end method

.method final f(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 6

    .prologue
    .line 294
    const/high16 v0, 0x7e000000

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 295
    const/4 v1, 0x0

    .line 296
    instance-of v2, v0, Landroid/support/v4/view/gd;

    if-eqz v2, :cond_31

    .line 297
    check-cast v0, Landroid/support/v4/view/gd;

    .line 299
    :goto_d
    invoke-static {p1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/fk;)Ljava/lang/Runnable;

    move-result-object v1

    .line 300
    invoke-static {p1}, Landroid/support/v4/view/fk;->b(Landroid/support/v4/view/fk;)Ljava/lang/Runnable;

    move-result-object v2

    .line 301
    if-eqz v1, :cond_1a

    .line 302
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 304
    :cond_1a
    if-eqz v0, :cond_22

    .line 305
    invoke-interface {v0, p2}, Landroid/support/v4/view/gd;->a(Landroid/view/View;)V

    .line 306
    invoke-interface {v0, p2}, Landroid/support/v4/view/gd;->b(Landroid/view/View;)V

    .line 308
    :cond_22
    if-eqz v2, :cond_27

    .line 309
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 311
    :cond_27
    iget-object v0, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_30

    .line 312
    iget-object v0, p0, Landroid/support/v4/view/fm;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    :cond_30
    return-void

    :cond_31
    move-object v0, v1

    goto :goto_d
.end method

.method public f(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 154
    return-void
.end method

.method public g(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 160
    return-void
.end method

.method public h(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 166
    return-void
.end method

.method public i(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 171
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 172
    return-void
.end method

.method public j(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 178
    return-void
.end method

.method public k(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 189
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 190
    return-void
.end method

.method public l(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 201
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 202
    return-void
.end method

.method public m(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 213
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 214
    return-void
.end method

.method public n(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 220
    return-void
.end method

.method public o(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 225
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 226
    return-void
.end method

.method public p(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 231
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 232
    return-void
.end method

.method public q(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 247
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 248
    return-void
.end method

.method public r(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 4

    .prologue
    .line 253
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/fm;->g(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 254
    return-void
.end method
