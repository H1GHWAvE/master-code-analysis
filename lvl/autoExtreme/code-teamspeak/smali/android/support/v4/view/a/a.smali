.class public final Landroid/support/v4/view/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x80

.field public static final b:I = 0x100

.field public static final c:I = 0x200

.field public static final d:I = 0x400

.field public static final e:I = 0x800

.field public static final f:I = 0x1000

.field public static final g:I = 0x2000

.field public static final h:I = 0x4000

.field public static final i:I = 0x8000

.field public static final j:I = 0x10000

.field public static final k:I = 0x20000

.field public static final l:I = 0x40000

.field public static final m:I = 0x80000

.field public static final n:I = 0x100000

.field public static final o:I = 0x200000

.field public static final p:I = 0x0

.field public static final q:I = 0x1

.field public static final r:I = 0x2

.field public static final s:I = 0x4

.field public static final t:I = -0x1

.field private static final u:Landroid/support/v4/view/a/e;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_e

    .line 99
    new-instance v0, Landroid/support/v4/view/a/c;

    invoke-direct {v0}, Landroid/support/v4/view/a/c;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/a;->u:Landroid/support/v4/view/a/e;

    .line 105
    :goto_d
    return-void

    .line 100
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1c

    .line 101
    new-instance v0, Landroid/support/v4/view/a/b;

    invoke-direct {v0}, Landroid/support/v4/view/a/b;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/a;->u:Landroid/support/v4/view/a/e;

    goto :goto_d

    .line 103
    :cond_1c
    new-instance v0, Landroid/support/v4/view/a/d;

    invoke-direct {v0}, Landroid/support/v4/view/a/d;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/a;->u:Landroid/support/v4/view/a/e;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    return-void
.end method

.method public static a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;
    .registers 2

    .prologue
    .line 284
    new-instance v0, Landroid/support/v4/view/a/bd;

    invoke-direct {v0, p0}, Landroid/support/v4/view/a/bd;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Landroid/view/accessibility/AccessibilityEvent;I)Landroid/support/v4/view/a/bd;
    .registers 4

    .prologue
    .line 267
    new-instance v0, Landroid/support/v4/view/a/bd;

    sget-object v1, Landroid/support/v4/view/a/a;->u:Landroid/support/v4/view/a/e;

    invoke-interface {v1, p0, p1}, Landroid/support/v4/view/a/e;->a(Landroid/view/accessibility/AccessibilityEvent;I)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/a/bd;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/a/bd;)V
    .registers 4

    .prologue
    .line 257
    sget-object v0, Landroid/support/v4/view/a/a;->u:Landroid/support/v4/view/a/e;

    .line 1539
    iget-object v1, p1, Landroid/support/v4/view/a/bd;->b:Ljava/lang/Object;

    .line 257
    invoke-interface {v0, p0, v1}, Landroid/support/v4/view/a/e;->a(Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/Object;)V

    .line 258
    return-void
.end method

.method private static b(Landroid/view/accessibility/AccessibilityEvent;)I
    .registers 2

    .prologue
    .line 245
    sget-object v0, Landroid/support/v4/view/a/a;->u:Landroid/support/v4/view/a/e;

    invoke-interface {v0, p0}, Landroid/support/v4/view/a/e;->a(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v0

    return v0
.end method

.method private static b(Landroid/view/accessibility/AccessibilityEvent;I)V
    .registers 3

    .prologue
    .line 296
    sget-object v0, Landroid/support/v4/view/a/a;->u:Landroid/support/v4/view/a/e;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/a/e;->b(Landroid/view/accessibility/AccessibilityEvent;I)V

    .line 297
    return-void
.end method

.method private static c(Landroid/view/accessibility/AccessibilityEvent;)I
    .registers 2

    .prologue
    .line 313
    sget-object v0, Landroid/support/v4/view/a/a;->u:Landroid/support/v4/view/a/e;

    invoke-interface {v0, p0}, Landroid/support/v4/view/a/e;->b(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v0

    return v0
.end method
