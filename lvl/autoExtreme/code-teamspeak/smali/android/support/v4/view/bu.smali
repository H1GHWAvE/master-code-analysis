.class public final Landroid/support/v4/view/bu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field private final b:Landroid/view/View;

.field private c:Landroid/view/ViewParent;

.field private d:[I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    .line 48
    return-void
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 76
    iget-boolean v0, p0, Landroid/support/v4/view/bu;->a:Z

    return v0
.end method

.method private d()V
    .registers 2

    .prologue
    .line 267
    iget-object v0, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->A(Landroid/view/View;)V

    .line 268
    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 280
    iget-object v0, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->A(Landroid/view/View;)V

    .line 281
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .registers 3

    .prologue
    .line 60
    iget-boolean v0, p0, Landroid/support/v4/view/bu;->a:Z

    if-eqz v0, :cond_9

    .line 61
    iget-object v0, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->A(Landroid/view/View;)V

    .line 63
    :cond_9
    iput-boolean p1, p0, Landroid/support/v4/view/bu;->a:Z

    .line 64
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 90
    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final a(FF)Z
    .registers 5

    .prologue
    .line 251
    .line 5076
    iget-boolean v0, p0, Landroid/support/v4/view/bu;->a:Z

    .line 251
    if-eqz v0, :cond_11

    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    if-eqz v0, :cond_11

    .line 252
    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    iget-object v1, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v0, v1, p1, p2}, Landroid/support/v4/view/fb;->a(Landroid/view/ViewParent;Landroid/view/View;FF)Z

    move-result v0

    .line 255
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final a(FFZ)Z
    .registers 6

    .prologue
    .line 234
    .line 4076
    iget-boolean v0, p0, Landroid/support/v4/view/bu;->a:Z

    .line 234
    if-eqz v0, :cond_11

    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    if-eqz v0, :cond_11

    .line 235
    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    iget-object v1, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v0, v1, p1, p2, p3}, Landroid/support/v4/view/fb;->a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z

    move-result v0

    .line 238
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final a(I)Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    .line 105
    invoke-virtual {p0}, Landroid/support/v4/view/bu;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    .line 124
    :goto_8
    return v0

    .line 1076
    :cond_9
    iget-boolean v0, p0, Landroid/support/v4/view/bu;->a:Z

    .line 109
    if-eqz v0, :cond_34

    .line 110
    iget-object v0, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 111
    iget-object v0, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    .line 112
    :goto_15
    if-eqz v1, :cond_34

    .line 113
    iget-object v3, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v1, v0, v3, p1}, Landroid/support/v4/view/fb;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 114
    iput-object v1, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    .line 115
    iget-object v3, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v1, v0, v3, p1}, Landroid/support/v4/view/fb;->b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V

    move v0, v2

    .line 116
    goto :goto_8

    .line 118
    :cond_28
    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_2f

    move-object v0, v1

    .line 119
    check-cast v0, Landroid/view/View;

    .line 121
    :cond_2f
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_15

    .line 124
    :cond_34
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final a(IIII[I)Z
    .registers 16

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 152
    .line 2076
    iget-boolean v0, p0, Landroid/support/v4/view/bu;->a:Z

    .line 152
    if-eqz v0, :cond_3c

    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    if-eqz v0, :cond_3c

    .line 153
    if-nez p1, :cond_12

    if-nez p2, :cond_12

    if-nez p3, :cond_12

    if-eqz p4, :cond_3d

    .line 156
    :cond_12
    if-eqz p5, :cond_44

    .line 157
    iget-object v0, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-virtual {v0, p5}, Landroid/view/View;->getLocationInWindow([I)V

    .line 158
    aget v1, p5, v7

    .line 159
    aget v0, p5, v9

    move v6, v0

    move v8, v1

    .line 162
    :goto_1f
    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    iget-object v1, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/fb;->a(Landroid/view/ViewParent;Landroid/view/View;IIII)V

    .line 165
    if-eqz p5, :cond_3b

    .line 166
    iget-object v0, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-virtual {v0, p5}, Landroid/view/View;->getLocationInWindow([I)V

    .line 167
    aget v0, p5, v7

    sub-int/2addr v0, v8

    aput v0, p5, v7

    .line 168
    aget v0, p5, v9

    sub-int/2addr v0, v6

    aput v0, p5, v9

    :cond_3b
    move v7, v9

    .line 177
    :cond_3c
    :goto_3c
    return v7

    .line 171
    :cond_3d
    if-eqz p5, :cond_3c

    .line 173
    aput v7, p5, v7

    .line 174
    aput v7, p5, v9

    goto :goto_3c

    :cond_44
    move v6, v7

    move v8, v7

    goto :goto_1f
.end method

.method public final a(II[I[I)Z
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 190
    .line 3076
    iget-boolean v2, p0, Landroid/support/v4/view/bu;->a:Z

    .line 190
    if-eqz v2, :cond_4d

    iget-object v2, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    if-eqz v2, :cond_4d

    .line 191
    if-nez p1, :cond_e

    if-eqz p2, :cond_4e

    .line 194
    :cond_e
    if-eqz p4, :cond_55

    .line 195
    iget-object v2, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-virtual {v2, p4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 196
    aget v3, p4, v0

    .line 197
    aget v2, p4, v1

    .line 200
    :goto_19
    if-nez p3, :cond_26

    .line 201
    iget-object v4, p0, Landroid/support/v4/view/bu;->d:[I

    if-nez v4, :cond_24

    .line 202
    const/4 v4, 0x2

    new-array v4, v4, [I

    iput-object v4, p0, Landroid/support/v4/view/bu;->d:[I

    .line 204
    :cond_24
    iget-object p3, p0, Landroid/support/v4/view/bu;->d:[I

    .line 206
    :cond_26
    aput v0, p3, v0

    .line 207
    aput v0, p3, v1

    .line 208
    iget-object v4, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    iget-object v5, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v4, v5, p1, p2, p3}, Landroid/support/v4/view/fb;->a(Landroid/view/ViewParent;Landroid/view/View;II[I)V

    .line 210
    if-eqz p4, :cond_44

    .line 211
    iget-object v4, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-virtual {v4, p4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 212
    aget v4, p4, v0

    sub-int v3, v4, v3

    aput v3, p4, v0

    .line 213
    aget v3, p4, v1

    sub-int v2, v3, v2

    aput v2, p4, v1

    .line 215
    :cond_44
    aget v2, p3, v0

    if-nez v2, :cond_4c

    aget v2, p3, v1

    if-eqz v2, :cond_4d

    :cond_4c
    move v0, v1

    .line 221
    :cond_4d
    :goto_4d
    return v0

    .line 216
    :cond_4e
    if-eqz p4, :cond_4d

    .line 217
    aput v0, p4, v0

    .line 218
    aput v0, p4, v1

    goto :goto_4d

    :cond_55
    move v2, v0

    move v3, v0

    goto :goto_19
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 135
    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    if-eqz v0, :cond_e

    .line 136
    iget-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    iget-object v1, p0, Landroid/support/v4/view/bu;->b:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v4/view/fb;->a(Landroid/view/ViewParent;Landroid/view/View;)V

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/bu;->c:Landroid/view/ViewParent;

    .line 139
    :cond_e
    return-void
.end method
