.class public final Landroid/support/v4/view/a/bm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x3

.field public static final d:I = 0x4

.field private static final e:Landroid/support/v4/view/a/bp;

.field private static final g:I = -0x1


# instance fields
.field private f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 189
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_f

    .line 190
    new-instance v0, Landroid/support/v4/view/a/bo;

    invoke-direct {v0, v2}, Landroid/support/v4/view/a/bo;-><init>(B)V

    sput-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    .line 194
    :goto_e
    return-void

    .line 192
    :cond_f
    new-instance v0, Landroid/support/v4/view/a/bq;

    invoke-direct {v0, v2}, Landroid/support/v4/view/a/bq;-><init>(B)V

    sput-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    goto :goto_e
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput-object p1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    .line 247
    return-void
.end method

.method private a()I
    .registers 3

    .prologue
    .line 260
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private a(I)Landroid/support/v4/view/a/bm;
    .registers 4

    .prologue
    .line 354
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bp;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/bm;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/support/v4/view/a/bm;)Landroid/support/v4/view/a/bm;
    .registers 3

    .prologue
    .line 376
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/bm;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;
    .registers 2

    .prologue
    .line 239
    if-eqz p0, :cond_8

    .line 240
    new-instance v0, Landroid/support/v4/view/a/bm;

    invoke-direct {v0, p0}, Landroid/support/v4/view/a/bm;-><init>(Ljava/lang/Object;)V

    .line 242
    :goto_7
    return-object v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private a(Landroid/graphics/Rect;)V
    .registers 4

    .prologue
    .line 306
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/bp;->a(Ljava/lang/Object;Landroid/graphics/Rect;)V

    .line 307
    return-void
.end method

.method private b()I
    .registers 3

    .prologue
    .line 270
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->c(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static b(I)Ljava/lang/String;
    .registers 2

    .prologue
    .line 437
    packed-switch p0, :pswitch_data_12

    .line 451
    const-string v0, "<UNKNOWN>"

    :goto_5
    return-object v0

    .line 439
    :pswitch_6
    const-string v0, "TYPE_APPLICATION"

    goto :goto_5

    .line 442
    :pswitch_9
    const-string v0, "TYPE_INPUT_METHOD"

    goto :goto_5

    .line 445
    :pswitch_c
    const-string v0, "TYPE_SYSTEM"

    goto :goto_5

    .line 448
    :pswitch_f
    const-string v0, "TYPE_ACCESSIBILITY_OVERLAY"

    goto :goto_5

    .line 437
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method

.method private c()Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 279
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private d()Landroid/support/v4/view/a/bm;
    .registers 3

    .prologue
    .line 288
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/bm;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;

    move-result-object v0

    return-object v0
.end method

.method private e()I
    .registers 3

    .prologue
    .line 297
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->f(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private f()Z
    .registers 3

    .prologue
    .line 317
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private g()Z
    .registers 3

    .prologue
    .line 326
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->h(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private h()Z
    .registers 3

    .prologue
    .line 335
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->i(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private i()I
    .registers 3

    .prologue
    .line 344
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->j(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static j()Landroid/support/v4/view/a/bm;
    .registers 1

    .prologue
    .line 364
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    invoke-interface {v0}, Landroid/support/v4/view/a/bp;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/bm;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .registers 3

    .prologue
    .line 388
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v1, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/bp;->k(Ljava/lang/Object;)V

    .line 389
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 398
    if-ne p0, p1, :cond_5

    .line 415
    :cond_4
    :goto_4
    return v0

    .line 401
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 402
    goto :goto_4

    .line 404
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 405
    goto :goto_4

    .line 407
    :cond_15
    check-cast p1, Landroid/support/v4/view/a/bm;

    .line 408
    iget-object v2, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    if-nez v2, :cond_21

    .line 409
    iget-object v2, p1, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    if-eqz v2, :cond_4

    move v0, v1

    .line 410
    goto :goto_4

    .line 412
    :cond_21
    iget-object v2, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    iget-object v3, p1, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 413
    goto :goto_4
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 393
    iget-object v0, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_5
.end method

.method public final toString()Ljava/lang/String;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 420
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 421
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1306
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v5, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v5, v4}, Landroid/support/v4/view/a/bp;->a(Ljava/lang/Object;Landroid/graphics/Rect;)V

    .line 423
    const-string v0, "AccessibilityWindowInfo["

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    const-string v0, "id="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2297
    sget-object v5, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v6, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v5, v6}, Landroid/support/v4/view/a/bp;->f(Ljava/lang/Object;)I

    move-result v5

    .line 424
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 425
    const-string v0, ", type="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 3260
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v6, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v6}, Landroid/support/v4/view/a/bp;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3437
    packed-switch v0, :pswitch_data_c0

    .line 3451
    const-string v0, "<UNKNOWN>"

    .line 425
    :goto_3c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    const-string v0, ", layer="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 4270
    sget-object v5, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v6, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v5, v6}, Landroid/support/v4/view/a/bp;->c(Ljava/lang/Object;)I

    move-result v5

    .line 426
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 427
    const-string v0, ", bounds="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 428
    const-string v0, ", focused="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 4326
    sget-object v4, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v5, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v4, v5}, Landroid/support/v4/view/a/bp;->h(Ljava/lang/Object;)Z

    move-result v4

    .line 428
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 429
    const-string v0, ", active="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 5317
    sget-object v4, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v5, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v4, v5}, Landroid/support/v4/view/a/bp;->g(Ljava/lang/Object;)Z

    move-result v4

    .line 429
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 430
    const-string v0, ", hasParent="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 6288
    sget-object v0, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v5, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v0, v5}, Landroid/support/v4/view/a/bp;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/bm;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;

    move-result-object v0

    .line 430
    if-eqz v0, :cond_bc

    move v0, v1

    :goto_90
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 431
    const-string v0, ", hasChildren="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 6344
    sget-object v4, Landroid/support/v4/view/a/bm;->e:Landroid/support/v4/view/a/bp;

    iget-object v5, p0, Landroid/support/v4/view/a/bm;->f:Ljava/lang/Object;

    invoke-interface {v4, v5}, Landroid/support/v4/view/a/bp;->j(Ljava/lang/Object;)I

    move-result v4

    .line 431
    if-lez v4, :cond_be

    :goto_a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 432
    const/16 v0, 0x5d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 433
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 3439
    :pswitch_b0
    const-string v0, "TYPE_APPLICATION"

    goto :goto_3c

    .line 3442
    :pswitch_b3
    const-string v0, "TYPE_INPUT_METHOD"

    goto :goto_3c

    .line 3445
    :pswitch_b6
    const-string v0, "TYPE_SYSTEM"

    goto :goto_3c

    .line 3448
    :pswitch_b9
    const-string v0, "TYPE_ACCESSIBILITY_OVERLAY"

    goto :goto_3c

    :cond_bc
    move v0, v2

    .line 430
    goto :goto_90

    :cond_be
    move v1, v2

    .line 431
    goto :goto_a3

    .line 3437
    :pswitch_data_c0
    .packed-switch 0x1
        :pswitch_b0
        :pswitch_b3
        :pswitch_b6
        :pswitch_b9
    .end packed-switch
.end method
