.class Landroid/support/v4/view/dg;
.super Landroid/support/v4/view/df;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1373
    invoke-direct {p0}, Landroid/support/v4/view/df;-><init>()V

    return-void
.end method


# virtual methods
.method public final Z(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1391
    .line 3034
    invoke-virtual {p1}, Landroid/view/View;->isLaidOut()Z

    move-result v0

    .line 1391
    return v0
.end method

.method public final ab(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1396
    .line 3038
    invoke-virtual {p1}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    .line 1396
    return v0
.end method

.method public final d(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 1386
    .line 2058
    invoke-virtual {p1, p2}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1387
    return-void
.end method

.method public final g(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 1381
    .line 2030
    invoke-virtual {p1, p2}, Landroid/view/View;->setAccessibilityLiveRegion(I)V

    .line 1382
    return-void
.end method

.method public final q(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1376
    .line 2026
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityLiveRegion()I

    move-result v0

    .line 1376
    return v0
.end method
