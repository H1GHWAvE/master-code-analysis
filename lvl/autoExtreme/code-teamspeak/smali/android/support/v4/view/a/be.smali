.class Landroid/support/v4/view/a/be;
.super Landroid/support/v4/view/a/bi;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 268
    invoke-direct {p0}, Landroid/support/v4/view/a/bi;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1031
    invoke-static {}, Landroid/view/accessibility/AccessibilityRecord;->obtain()Landroid/view/accessibility/AccessibilityRecord;

    move-result-object v0

    .line 271
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 276
    .line 1035
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-static {p1}, Landroid/view/accessibility/AccessibilityRecord;->obtain(Landroid/view/accessibility/AccessibilityRecord;)Landroid/view/accessibility/AccessibilityRecord;

    move-result-object v0

    .line 276
    return-object v0
.end method

.method public final a(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 387
    .line 1123
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setAddedCount(I)V

    .line 388
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/os/Parcelable;)V
    .registers 3

    .prologue
    .line 437
    .line 1163
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setParcelableData(Landroid/os/Parcelable;)V

    .line 438
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 467
    .line 1187
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setSource(Landroid/view/View;)V

    .line 468
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 392
    .line 1127
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setBeforeText(Ljava/lang/CharSequence;)V

    .line 393
    return-void
.end method

.method public final a(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 397
    .line 1131
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setChecked(Z)V

    .line 398
    return-void
.end method

.method public final b(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 281
    .line 1039
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getAddedCount()I

    move-result v0

    .line 281
    return v0
.end method

.method public final b(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 412
    .line 1143
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setCurrentItemIndex(I)V

    .line 413
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 402
    .line 1135
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setClassName(Ljava/lang/CharSequence;)V

    .line 403
    return-void
.end method

.method public final b(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 417
    .line 1147
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setEnabled(Z)V

    .line 418
    return-void
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 286
    .line 1043
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 286
    return-object v0
.end method

.method public final c(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 422
    .line 1151
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setFromIndex(I)V

    .line 423
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 407
    .line 1139
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 408
    return-void
.end method

.method public final c(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 427
    .line 1155
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setFullScreen(Z)V

    .line 428
    return-void
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 291
    .line 1047
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getClassName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 291
    return-object v0
.end method

.method public final d(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 432
    .line 1159
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setItemCount(I)V

    .line 433
    return-void
.end method

.method public final d(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 442
    .line 1167
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setPassword(Z)V

    .line 443
    return-void
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 296
    .line 1051
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 296
    return-object v0
.end method

.method public final e(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 447
    .line 1171
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setRemovedCount(I)V

    .line 448
    return-void
.end method

.method public final e(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 462
    .line 1183
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setScrollable(Z)V

    .line 463
    return-void
.end method

.method public final f(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 301
    .line 1055
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getCurrentItemIndex()I

    move-result v0

    .line 301
    return v0
.end method

.method public final f(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 452
    .line 1175
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setScrollX(I)V

    .line 453
    return-void
.end method

.method public final g(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 306
    .line 1059
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getFromIndex()I

    move-result v0

    .line 306
    return v0
.end method

.method public final g(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 457
    .line 1179
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setScrollY(I)V

    .line 458
    return-void
.end method

.method public final h(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 311
    .line 1063
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getItemCount()I

    move-result v0

    .line 311
    return v0
.end method

.method public final h(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 472
    .line 1191
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setToIndex(I)V

    .line 473
    return-void
.end method

.method public final i(Ljava/lang/Object;)Landroid/os/Parcelable;
    .registers 3

    .prologue
    .line 316
    .line 1067
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v0

    .line 316
    return-object v0
.end method

.method public final j(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 321
    .line 1071
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getRemovedCount()I

    move-result v0

    .line 321
    return v0
.end method

.method public final k(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 326
    .line 1075
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getScrollX()I

    move-result v0

    .line 326
    return v0
.end method

.method public final l(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 331
    .line 1079
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getScrollY()I

    move-result v0

    .line 331
    return v0
.end method

.method public final m(Ljava/lang/Object;)Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 336
    .line 1083
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getSource()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 336
    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 342
    .line 1087
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getText()Ljava/util/List;

    move-result-object v0

    .line 342
    return-object v0
.end method

.method public final o(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 347
    .line 1091
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getToIndex()I

    move-result v0

    .line 347
    return v0
.end method

.method public final p(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 352
    .line 1095
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->getWindowId()I

    move-result v0

    .line 352
    return v0
.end method

.method public final q(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 357
    .line 1099
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->isChecked()Z

    move-result v0

    .line 357
    return v0
.end method

.method public final r(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 362
    .line 1103
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->isEnabled()Z

    move-result v0

    .line 362
    return v0
.end method

.method public final s(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 367
    .line 1107
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->isFullScreen()Z

    move-result v0

    .line 367
    return v0
.end method

.method public final t(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 372
    .line 1111
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->isPassword()Z

    move-result v0

    .line 372
    return v0
.end method

.method public final u(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 377
    .line 1115
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->isScrollable()Z

    move-result v0

    .line 377
    return v0
.end method

.method public final v(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 382
    .line 1119
    check-cast p1, Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityRecord;->recycle()V

    .line 383
    return-void
.end method
