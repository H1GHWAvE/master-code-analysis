.class public final Landroid/support/v4/view/cx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final A:J = 0xaL

.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x0

.field public static final e:I = 0x1

.field public static final f:I = 0x2

.field public static final g:I = 0x4

.field public static final h:I = 0x0

.field public static final i:I = 0x1

.field public static final j:I = 0x2

.field public static final k:I = 0x0

.field public static final l:I = 0x1

.field public static final m:I = 0x2

.field public static final n:I = 0x0

.field public static final o:I = 0x1

.field public static final p:I = 0x2

.field public static final q:I = 0x3

.field public static final r:I = 0xffffff

.field public static final s:I = -0x1000000

.field public static final t:I = 0x10

.field public static final u:I = 0x1000000

.field public static final v:I = 0x0

.field public static final w:I = 0x1

.field public static final x:I = 0x2

.field static final y:Landroid/support/v4/view/di;

.field private static final z:Ljava/lang/String; = "ViewCompat"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1534
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1535
    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    .line 1536
    new-instance v0, Landroid/support/v4/view/dh;

    invoke-direct {v0}, Landroid/support/v4/view/dh;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    .line 1554
    :goto_d
    return-void

    .line 1537
    :cond_e
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1a

    .line 1538
    new-instance v0, Landroid/support/v4/view/dg;

    invoke-direct {v0}, Landroid/support/v4/view/dg;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    goto :goto_d

    .line 1539
    :cond_1a
    const/16 v1, 0x11

    if-lt v0, v1, :cond_26

    .line 1540
    new-instance v0, Landroid/support/v4/view/de;

    invoke-direct {v0}, Landroid/support/v4/view/de;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    goto :goto_d

    .line 1541
    :cond_26
    const/16 v1, 0x10

    if-lt v0, v1, :cond_32

    .line 1542
    new-instance v0, Landroid/support/v4/view/dd;

    invoke-direct {v0}, Landroid/support/v4/view/dd;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    goto :goto_d

    .line 1543
    :cond_32
    const/16 v1, 0xe

    if-lt v0, v1, :cond_3e

    .line 1544
    new-instance v0, Landroid/support/v4/view/dc;

    invoke-direct {v0}, Landroid/support/v4/view/dc;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    goto :goto_d

    .line 1545
    :cond_3e
    const/16 v1, 0xb

    if-lt v0, v1, :cond_4a

    .line 1546
    new-instance v0, Landroid/support/v4/view/db;

    invoke-direct {v0}, Landroid/support/v4/view/db;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    goto :goto_d

    .line 1547
    :cond_4a
    const/16 v1, 0x9

    if-lt v0, v1, :cond_56

    .line 1548
    new-instance v0, Landroid/support/v4/view/da;

    invoke-direct {v0}, Landroid/support/v4/view/da;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    goto :goto_d

    .line 1549
    :cond_56
    const/4 v1, 0x7

    if-lt v0, v1, :cond_61

    .line 1550
    new-instance v0, Landroid/support/v4/view/cz;

    invoke-direct {v0}, Landroid/support/v4/view/cz;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    goto :goto_d

    .line 1552
    :cond_61
    new-instance v0, Landroid/support/v4/view/cy;

    invoke-direct {v0}, Landroid/support/v4/view/cy;-><init>()V

    sput-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1400
    return-void
.end method

.method public static A(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 2884
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->X(Landroid/view/View;)V

    .line 2885
    return-void
.end method

.method public static B(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 3014
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->Z(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static C(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 3025
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->aa(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public static D(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 3086
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->ab(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private static E(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 1736
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->b(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private static F(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 1748
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->c(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private static G(Landroid/view/View;)Landroid/support/v4/view/a/aq;
    .registers 2

    .prologue
    .line 1908
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->g(Landroid/view/View;)Landroid/support/v4/view/a/aq;

    move-result-object v0

    return-object v0
.end method

.method private static H(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 1994
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->j(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static I(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2144
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->o(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static J(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2180
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->q(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static K(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 2260
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->t(Landroid/view/View;)V

    .line 2261
    return-void
.end method

.method private static L(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 2267
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->u(Landroid/view/View;)V

    .line 2268
    return-void
.end method

.method private static M(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2304
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->F(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static N(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2474
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->I(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static O(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2501
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->J(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static P(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2520
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->A(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static Q(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2524
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->B(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static R(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2528
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->C(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static S(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2536
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->E(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static T(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2540
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->y(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static U(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2544
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->z(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static V(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2576
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->O(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static W(Landroid/view/View;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 2601
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->K(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static X(Landroid/view/View;)Landroid/content/res/ColorStateList;
    .registers 2

    .prologue
    .line 2762
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->V(Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private static Y(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
    .registers 2

    .prologue
    .line 2784
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->W(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method private static Z(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 2896
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->Y(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static a(II)I
    .registers 3

    .prologue
    .line 2167
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(II)I

    move-result v0

    return v0
.end method

.method public static a(III)I
    .registers 4

    .prologue
    .line 2116
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/di;->a(III)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 1589
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->a(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 3

    .prologue
    .line 2683
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2341
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->b(Landroid/view/View;F)V

    .line 2342
    return-void
.end method

.method public static a(Landroid/view/View;IIII)V
    .registers 11

    .prologue
    .line 1790
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/view/di;->a(Landroid/view/View;IIII)V

    .line 1791
    return-void
.end method

.method public static a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .registers 4

    .prologue
    .line 1962
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/di;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1963
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
    .registers 3

    .prologue
    .line 2773
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 2774
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Paint;)V
    .registers 3

    .prologue
    .line 2039
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/graphics/Paint;)V

    .line 2040
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    .prologue
    .line 2797
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V

    .line 2798
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 3067
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 3068
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
    .registers 3

    .prologue
    .line 1711
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 1712
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/support/v4/view/a;)V
    .registers 3

    .prologue
    .line 1726
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 1727
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/support/v4/view/bx;)V
    .registers 3

    .prologue
    .line 2667
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/support/v4/view/bx;)V

    .line 2668
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3

    .prologue
    .line 1675
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1676
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 3

    .prologue
    .line 1804
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1805
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .registers 6

    .prologue
    .line 1821
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/view/di;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 1822
    return-void
.end method

.method private static a(Landroid/view/View;Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 2587
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 2588
    return-void
.end method

.method public static a(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 2647
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->b(Landroid/view/View;Z)V

    .line 2648
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)V
    .registers 2

    .prologue
    .line 2629
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->a(Landroid/view/ViewGroup;)V

    .line 2630
    return-void
.end method

.method private static a(Landroid/view/View;FF)Z
    .registers 4

    .prologue
    .line 3006
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/di;->a(Landroid/view/View;FF)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;FFZ)Z
    .registers 5

    .prologue
    .line 2972
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/view/di;->a(Landroid/view/View;FFZ)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;I)Z
    .registers 3

    .prologue
    .line 1564
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;IIII[I)Z
    .registers 13

    .prologue
    .line 2924
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/view/di;->a(Landroid/view/View;IIII[I)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;II[I[I)Z
    .registers 11

    .prologue
    .line 2949
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/view/di;->a(Landroid/view/View;II[I[I)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .registers 4

    .prologue
    .line 1881
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/di;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method private static aa(Landroid/view/View;)Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 3079
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->P(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 3

    .prologue
    .line 2700
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1772
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->d(Landroid/view/View;)V

    .line 1773
    return-void
.end method

.method public static b(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2357
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->c(Landroid/view/View;F)V

    .line 2358
    return-void
.end method

.method public static b(Landroid/view/View;IIII)V
    .registers 11

    .prologue
    .line 2253
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/view/di;->b(Landroid/view/View;IIII)V

    .line 2254
    return-void
.end method

.method private static b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3

    .prologue
    .line 1642
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1643
    return-void
.end method

.method public static b(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 2724
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->c(Landroid/view/View;Z)V

    .line 2725
    return-void
.end method

.method public static b(Landroid/view/View;I)Z
    .registers 3

    .prologue
    .line 1575
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->b(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 1839
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->e(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/View;F)V
    .registers 3
    .param p1    # F
        .annotation build Landroid/support/a/n;
            a = 0.0
            b = 1.0
        .end annotation
    .end param

    .prologue
    .line 2373
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->d(Landroid/view/View;F)V

    .line 2374
    return-void
.end method

.method public static c(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 1863
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->d(Landroid/view/View;I)V

    .line 1864
    return-void
.end method

.method private static c(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 1759
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;Z)V

    .line 1760
    return-void
.end method

.method public static d(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 1919
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->h(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public static d(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2451
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->g(Landroid/view/View;F)V

    .line 2452
    return-void
.end method

.method public static d(Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 3034
    invoke-virtual {p0, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 3036
    if-eqz p1, :cond_e

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_e

    .line 3038
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 3040
    :cond_e
    return-void
.end method

.method private static d(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 2815
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->d(Landroid/view/View;Z)V

    .line 2816
    return-void
.end method

.method public static e(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 1983
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->i(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static e(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2463
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->h(Landroid/view/View;F)V

    .line 2464
    return-void
.end method

.method public static e(Landroid/view/View;I)V
    .registers 4

    .prologue
    .line 3047
    invoke-virtual {p0, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 3049
    if-eqz p1, :cond_e

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_e

    .line 3051
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 3053
    :cond_e
    return-void
.end method

.method public static f(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2054
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->k(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static f(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2551
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->m(Landroid/view/View;F)V

    .line 2552
    return-void
.end method

.method private static f(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 1605
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->c(Landroid/view/View;I)V

    .line 1606
    return-void
.end method

.method public static g(Landroid/view/View;)Landroid/view/ViewParent;
    .registers 2

    .prologue
    .line 2086
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->l(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method private static g(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2387
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->i(Landroid/view/View;F)V

    .line 2388
    return-void
.end method

.method private static g(Landroid/view/View;I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/p;
        .end annotation
    .end param

    .prologue
    .line 2005
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->e(Landroid/view/View;I)V

    .line 2006
    return-void
.end method

.method private static h(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2401
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->j(Landroid/view/View;F)V

    .line 2402
    return-void
.end method

.method private static h(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 2074
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->f(Landroid/view/View;I)V

    .line 2075
    return-void
.end method

.method public static h(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 2099
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->m(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static i(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2130
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->n(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static i(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2413
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->a(Landroid/view/View;F)V

    .line 2414
    return-void
.end method

.method private static i(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 2212
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->g(Landroid/view/View;I)V

    .line 2213
    return-void
.end method

.method public static j(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2155
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->p(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static j(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2426
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->e(Landroid/view/View;F)V

    .line 2427
    return-void
.end method

.method private static j(Landroid/view/View;I)Z
    .registers 3

    .prologue
    .line 2873
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->h(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static k(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2224
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->r(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static k(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2439
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->f(Landroid/view/View;F)V

    .line 2440
    return-void
.end method

.method public static l(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2236
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->s(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static l(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2489
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->k(Landroid/view/View;F)V

    .line 2490
    return-void
.end method

.method public static m(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2280
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->w(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static m(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2516
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->k(Landroid/view/View;F)V

    .line 2517
    return-void
.end method

.method public static n(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2293
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->x(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private static n(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 2567
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/di;->n(Landroid/view/View;F)V

    .line 2568
    return-void
.end method

.method public static o(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2315
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->G(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static p(Landroid/view/View;)Landroid/support/v4/view/fk;
    .registers 2

    .prologue
    .line 2327
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->H(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    return-object v0
.end method

.method public static q(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2532
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->D(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public static r(Landroid/view/View;)F
    .registers 2

    .prologue
    .line 2560
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->N(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public static s(Landroid/view/View;)I
    .registers 2

    .prologue
    .line 2608
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->L(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static t(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 2616
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->M(Landroid/view/View;)V

    .line 2617
    return-void
.end method

.method public static u(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 2637
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->Q(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static v(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 2658
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->R(Landroid/view/View;)V

    .line 2659
    return-void
.end method

.method public static w(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 2711
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->S(Landroid/view/View;)V

    .line 2712
    return-void
.end method

.method public static x(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 2742
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->v(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static y(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 2752
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->T(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static z(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 2831
    sget-object v0, Landroid/support/v4/view/cx;->y:Landroid/support/v4/view/di;

    invoke-interface {v0, p0}, Landroid/support/v4/view/di;->U(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
