.class public final Landroid/support/v4/view/a/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Ljava/lang/String; = "ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN"

.field public static final B:Ljava/lang/String; = "ACTION_ARGUMENT_SELECTION_START_INT"

.field public static final C:Ljava/lang/String; = "ACTION_ARGUMENT_SELECTION_END_INT"

.field public static final D:Ljava/lang/String; = "ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE"

.field public static final E:I = 0x1

.field public static final F:I = 0x2

.field public static final G:I = 0x1

.field public static final H:I = 0x2

.field public static final I:I = 0x4

.field public static final J:I = 0x8

.field public static final K:I = 0x10

.field public static final a:Landroid/support/v4/view/a/w;

.field public static final c:I = 0x1

.field public static final d:I = 0x2

.field public static final e:I = 0x4

.field public static final f:I = 0x8

.field public static final g:I = 0x10

.field public static final h:I = 0x20

.field public static final i:I = 0x40

.field public static final j:I = 0x80

.field public static final k:I = 0x100

.field public static final l:I = 0x200

.field public static final m:I = 0x400

.field public static final n:I = 0x800

.field public static final o:I = 0x1000

.field public static final p:I = 0x2000

.field public static final q:I = 0x4000

.field public static final r:I = 0x8000

.field public static final s:I = 0x10000

.field public static final t:I = 0x20000

.field public static final u:I = 0x40000

.field public static final v:I = 0x80000

.field public static final w:I = 0x100000

.field public static final x:I = 0x200000

.field public static final y:Ljava/lang/String; = "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

.field public static final z:Ljava/lang/String; = "ACTION_ARGUMENT_HTML_ELEMENT_STRING"


# instance fields
.field public final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1877
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_e

    .line 1878
    new-instance v0, Landroid/support/v4/view/a/u;

    invoke-direct {v0}, Landroid/support/v4/view/a/u;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    .line 1894
    :goto_d
    return-void

    .line 1879
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1c

    .line 1880
    new-instance v0, Landroid/support/v4/view/a/t;

    invoke-direct {v0}, Landroid/support/v4/view/a/t;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    goto :goto_d

    .line 1881
    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2a

    .line 1882
    new-instance v0, Landroid/support/v4/view/a/aa;

    invoke-direct {v0}, Landroid/support/v4/view/a/aa;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    goto :goto_d

    .line 1883
    :cond_2a
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_38

    .line 1884
    new-instance v0, Landroid/support/v4/view/a/z;

    invoke-direct {v0}, Landroid/support/v4/view/a/z;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    goto :goto_d

    .line 1885
    :cond_38
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_46

    .line 1886
    new-instance v0, Landroid/support/v4/view/a/y;

    invoke-direct {v0}, Landroid/support/v4/view/a/y;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    goto :goto_d

    .line 1887
    :cond_46
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_54

    .line 1888
    new-instance v0, Landroid/support/v4/view/a/x;

    invoke-direct {v0}, Landroid/support/v4/view/a/x;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    goto :goto_d

    .line 1889
    :cond_54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_62

    .line 1890
    new-instance v0, Landroid/support/v4/view/a/v;

    invoke-direct {v0}, Landroid/support/v4/view/a/v;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    goto :goto_d

    .line 1892
    :cond_62
    new-instance v0, Landroid/support/v4/view/a/ab;

    invoke-direct {v0}, Landroid/support/v4/view/a/ab;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    goto :goto_d
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 2250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2251
    iput-object p1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    .line 2252
    return-void
.end method

.method private A()I
    .registers 3

    .prologue
    .line 3162
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->H(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private B()Landroid/support/v4/view/a/ac;
    .registers 4

    .prologue
    .line 3188
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->I(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3189
    if-nez v1, :cond_c

    const/4 v0, 0x0

    .line 3190
    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Landroid/support/v4/view/a/ac;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v4/view/a/ac;-><init>(Ljava/lang/Object;B)V

    goto :goto_b
.end method

.method private C()Landroid/support/v4/view/a/ad;
    .registers 4

    .prologue
    .line 3208
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->J(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3209
    if-nez v1, :cond_c

    const/4 v0, 0x0

    .line 3210
    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Landroid/support/v4/view/a/ad;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v4/view/a/ad;-><init>(Ljava/lang/Object;B)V

    goto :goto_b
.end method

.method private D()Landroid/support/v4/view/a/ae;
    .registers 4

    .prologue
    .line 3219
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->K(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3220
    if-nez v1, :cond_c

    const/4 v0, 0x0

    .line 3221
    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Landroid/support/v4/view/a/ae;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v4/view/a/ae;-><init>(Ljava/lang/Object;B)V

    goto :goto_b
.end method

.method private E()Ljava/util/List;
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 3244
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 3245
    if-eqz v3, :cond_26

    .line 3246
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3247
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    .line 3248
    :goto_15
    if-ge v1, v4, :cond_2a

    .line 3249
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    .line 3250
    new-instance v6, Landroid/support/v4/view/a/s;

    invoke-direct {v6, v5, v2}, Landroid/support/v4/view/a/s;-><init>(Ljava/lang/Object;B)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3248
    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    .line 3254
    :cond_26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_2a
    return-object v0
.end method

.method private F()V
    .registers 3

    .prologue
    .line 3270
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->T(Ljava/lang/Object;)V

    .line 3271
    return-void
.end method

.method private G()Z
    .registers 3

    .prologue
    .line 3280
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->U(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private H()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 3305
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private I()Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 3348
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->V(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private J()Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 3396
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->W(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private K()Z
    .registers 3

    .prologue
    .line 3405
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->X(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private L()Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 3471
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->Y(Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private M()I
    .registers 3

    .prologue
    .line 3480
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->Z(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private N()I
    .registers 3

    .prologue
    .line 3526
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->f(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private O()I
    .registers 3

    .prologue
    .line 3552
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->aa(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private P()I
    .registers 3

    .prologue
    .line 3561
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->ab(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private Q()Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 3574
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private R()Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 3631
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->i(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private S()Landroid/support/v4/view/a/bm;
    .registers 3

    .prologue
    .line 3684
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/bm;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;

    move-result-object v0

    return-object v0
.end method

.method private T()Z
    .registers 3

    .prologue
    .line 3693
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->ac(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private U()Z
    .registers 3

    .prologue
    .line 3716
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->ad(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private V()Z
    .registers 3

    .prologue
    .line 3741
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->ae(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private W()Z
    .registers 3

    .prologue
    .line 3768
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->af(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a()Landroid/support/v4/view/a/q;
    .registers 1

    .prologue
    .line 2293
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    invoke-interface {v0}, Landroid/support/v4/view/a/w;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/support/v4/view/a/q;)Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 2304
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;)Landroid/support/v4/view/a/q;
    .registers 2

    .prologue
    .line 2269
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    invoke-interface {v0, p0}, Landroid/support/v4/view/a/w;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/View;I)Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 2283
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/a/w;->a(Landroid/view/View;I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;
    .registers 2

    .prologue
    .line 2238
    if-eqz p0, :cond_8

    .line 2239
    new-instance v0, Landroid/support/v4/view/a/q;

    invoke-direct {v0, p0}, Landroid/support/v4/view/a/q;-><init>(Ljava/lang/Object;)V

    .line 2241
    :goto_7
    return-object v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .registers 8

    .prologue
    .line 2605
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2606
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v2, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v2, p1}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 2607
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 2608
    const/4 v0, 0x0

    :goto_12
    if-ge v0, v3, :cond_23

    .line 2609
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 2610
    new-instance v5, Landroid/support/v4/view/a/q;

    invoke-direct {v5, v4}, Landroid/support/v4/view/a/q;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2608
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 2612
    :cond_23
    return-object v1
.end method

.method private a(II)V
    .registers 5

    .prologue
    .line 3543
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;II)V

    .line 3544
    return-void
.end method

.method private a(Landroid/support/v4/view/a/ae;)V
    .registers 5

    .prologue
    .line 3235
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    .line 4456
    iget-object v2, p1, Landroid/support/v4/view/a/ae;->d:Ljava/lang/Object;

    .line 3235
    invoke-interface {v0, v1, v2}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3236
    return-void
.end method

.method private a(ILandroid/os/Bundle;)Z
    .registers 5

    .prologue
    .line 2563
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method private b(I)Landroid/support/v4/view/a/q;
    .registers 4

    .prologue
    .line 2351
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/support/v4/view/a/s;)V
    .registers 5

    .prologue
    .line 2512
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-static {p1}, Landroid/support/v4/view/a/s;->a(Landroid/support/v4/view/a/s;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2513
    return-void
.end method

.method private b(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 2336
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Landroid/view/View;I)V

    .line 2337
    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 3194
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    check-cast p1, Landroid/support/v4/view/a/ac;

    iget-object v2, p1, Landroid/support/v4/view/a/ac;->d:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Landroid/support/v4/view/a/w;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3195
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 3124
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3125
    return-void
.end method

.method private c(I)Landroid/support/v4/view/a/q;
    .registers 4

    .prologue
    .line 2369
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->f(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)Ljava/util/List;
    .registers 6

    .prologue
    .line 3445
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 3446
    if-eqz v1, :cond_26

    .line 3447
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3448
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 3449
    new-instance v3, Landroid/support/v4/view/a/q;

    invoke-direct {v3, v2}, Landroid/support/v4/view/a/q;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 3453
    :cond_26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_2a
    return-object v0
.end method

.method private c(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 2437
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Landroid/view/View;I)V

    .line 2438
    return-void
.end method

.method private c(Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 3198
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    check-cast p1, Landroid/support/v4/view/a/ad;

    .line 4412
    iget-object v2, p1, Landroid/support/v4/view/a/ad;->a:Ljava/lang/Object;

    .line 3198
    invoke-interface {v0, v1, v2}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3199
    return-void
.end method

.method private d(I)Landroid/support/v4/view/a/q;
    .registers 4

    .prologue
    .line 2404
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 3074
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 3075
    return-void
.end method

.method private d(Landroid/view/View;I)Z
    .registers 5

    .prologue
    .line 2469
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method private e(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 2664
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->f(Ljava/lang/Object;Landroid/view/View;I)V

    .line 2665
    return-void
.end method

.method private e(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 3296
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 3297
    return-void
.end method

.method private e(I)Z
    .registers 4

    .prologue
    .line 2546
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method private e(Landroid/view/View;)Z
    .registers 4

    .prologue
    .line 2455
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private f(I)V
    .registers 4

    .prologue
    .line 2579
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->g(Ljava/lang/Object;I)V

    .line 2580
    return-void
.end method

.method private f(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 3315
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->g(Ljava/lang/Object;Landroid/view/View;)V

    .line 3316
    return-void
.end method

.method private f(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 3333
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->g(Ljava/lang/Object;Landroid/view/View;I)V

    .line 3334
    return-void
.end method

.method private g(I)V
    .registers 4

    .prologue
    .line 3178
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->h(Ljava/lang/Object;I)V

    .line 3179
    return-void
.end method

.method private g(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 3358
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->h(Ljava/lang/Object;Landroid/view/View;)V

    .line 3359
    return-void
.end method

.method private g(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 3381
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->h(Ljava/lang/Object;Landroid/view/View;I)V

    .line 3382
    return-void
.end method

.method private h(I)V
    .registers 4

    .prologue
    .line 3497
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->i(Ljava/lang/Object;I)V

    .line 3498
    return-void
.end method

.method private h(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 3592
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->b(Ljava/lang/Object;Landroid/view/View;)V

    .line 3593
    return-void
.end method

.method private h(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 3617
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->b(Ljava/lang/Object;Landroid/view/View;I)V

    .line 3618
    return-void
.end method

.method private i(I)V
    .registers 4

    .prologue
    .line 3516
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;I)V

    .line 3517
    return-void
.end method

.method private i(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 3649
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->c(Ljava/lang/Object;Landroid/view/View;)V

    .line 3650
    return-void
.end method

.method private i(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 3673
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/view/a/w;->c(Ljava/lang/Object;Landroid/view/View;I)V

    .line 3674
    return-void
.end method

.method private static j(I)Ljava/lang/String;
    .registers 2

    .prologue
    .line 3843
    sparse-switch p0, :sswitch_data_3c

    .line 3881
    const-string v0, "ACTION_UNKNOWN"

    :goto_5
    return-object v0

    .line 3845
    :sswitch_6
    const-string v0, "ACTION_FOCUS"

    goto :goto_5

    .line 3847
    :sswitch_9
    const-string v0, "ACTION_CLEAR_FOCUS"

    goto :goto_5

    .line 3849
    :sswitch_c
    const-string v0, "ACTION_SELECT"

    goto :goto_5

    .line 3851
    :sswitch_f
    const-string v0, "ACTION_CLEAR_SELECTION"

    goto :goto_5

    .line 3853
    :sswitch_12
    const-string v0, "ACTION_CLICK"

    goto :goto_5

    .line 3855
    :sswitch_15
    const-string v0, "ACTION_LONG_CLICK"

    goto :goto_5

    .line 3857
    :sswitch_18
    const-string v0, "ACTION_ACCESSIBILITY_FOCUS"

    goto :goto_5

    .line 3859
    :sswitch_1b
    const-string v0, "ACTION_CLEAR_ACCESSIBILITY_FOCUS"

    goto :goto_5

    .line 3861
    :sswitch_1e
    const-string v0, "ACTION_NEXT_AT_MOVEMENT_GRANULARITY"

    goto :goto_5

    .line 3863
    :sswitch_21
    const-string v0, "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY"

    goto :goto_5

    .line 3865
    :sswitch_24
    const-string v0, "ACTION_NEXT_HTML_ELEMENT"

    goto :goto_5

    .line 3867
    :sswitch_27
    const-string v0, "ACTION_PREVIOUS_HTML_ELEMENT"

    goto :goto_5

    .line 3869
    :sswitch_2a
    const-string v0, "ACTION_SCROLL_FORWARD"

    goto :goto_5

    .line 3871
    :sswitch_2d
    const-string v0, "ACTION_SCROLL_BACKWARD"

    goto :goto_5

    .line 3873
    :sswitch_30
    const-string v0, "ACTION_CUT"

    goto :goto_5

    .line 3875
    :sswitch_33
    const-string v0, "ACTION_COPY"

    goto :goto_5

    .line 3877
    :sswitch_36
    const-string v0, "ACTION_PASTE"

    goto :goto_5

    .line 3879
    :sswitch_39
    const-string v0, "ACTION_SET_SELECTION"

    goto :goto_5

    .line 3843
    :sswitch_data_3c
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_9
        0x4 -> :sswitch_c
        0x8 -> :sswitch_f
        0x10 -> :sswitch_12
        0x20 -> :sswitch_15
        0x40 -> :sswitch_18
        0x80 -> :sswitch_1b
        0x100 -> :sswitch_1e
        0x200 -> :sswitch_21
        0x400 -> :sswitch_24
        0x800 -> :sswitch_27
        0x1000 -> :sswitch_2a
        0x2000 -> :sswitch_2d
        0x4000 -> :sswitch_33
        0x8000 -> :sswitch_36
        0x10000 -> :sswitch_30
        0x20000 -> :sswitch_39
    .end sparse-switch
.end method

.method private j(Z)V
    .registers 4

    .prologue
    .line 2736
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;Z)V

    .line 2737
    return-void
.end method

.method private k(Z)V
    .registers 4

    .prologue
    .line 2760
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->b(Ljava/lang/Object;Z)V

    .line 2761
    return-void
.end method

.method private l(Z)V
    .registers 4

    .prologue
    .line 2978
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->h(Ljava/lang/Object;Z)V

    .line 2979
    return-void
.end method

.method private m(Z)V
    .registers 4

    .prologue
    .line 3419
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->m(Ljava/lang/Object;Z)V

    .line 3420
    return-void
.end method

.method private n(Z)V
    .registers 4

    .prologue
    .line 3707
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->n(Ljava/lang/Object;Z)V

    .line 3708
    return-void
.end method

.method private o(Z)V
    .registers 4

    .prologue
    .line 3732
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->o(Ljava/lang/Object;Z)V

    .line 3733
    return-void
.end method

.method static synthetic p()Landroid/support/v4/view/a/w;
    .registers 1

    .prologue
    .line 35
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    return-object v0
.end method

.method private p(Z)V
    .registers 4

    .prologue
    .line 3755
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->p(Ljava/lang/Object;Z)V

    .line 3756
    return-void
.end method

.method private q()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2258
    iget-object v0, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    return-object v0
.end method

.method private r()I
    .registers 3

    .prologue
    .line 2378
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->r(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private s()I
    .registers 3

    .prologue
    .line 2387
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->l(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private t()I
    .registers 3

    .prologue
    .line 2588
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->D(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private u()Landroid/support/v4/view/a/q;
    .registers 3

    .prologue
    .line 2626
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->p(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;

    move-result-object v0

    return-object v0
.end method

.method private v()Z
    .registers 3

    .prologue
    .line 2721
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->s(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private w()Z
    .registers 3

    .prologue
    .line 2745
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->t(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private x()Z
    .registers 3

    .prologue
    .line 2963
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->z(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private y()Z
    .registers 3

    .prologue
    .line 2987
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->A(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private z()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3140
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->G(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .registers 4

    .prologue
    .line 2497
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->b(Ljava/lang/Object;I)V

    .line 2498
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .registers 4

    .prologue
    .line 2673
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->a(Ljava/lang/Object;Landroid/graphics/Rect;)V

    .line 2674
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 3026
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 3027
    return-void
.end method

.method public final a(Z)V
    .registers 4

    .prologue
    .line 2784
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Z)V

    .line 2785
    return-void
.end method

.method public final a(Landroid/support/v4/view/a/s;)Z
    .registers 5

    .prologue
    .line 2530
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-static {p1}, Landroid/support/v4/view/a/s;->a(Landroid/support/v4/view/a/s;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/support/v4/view/a/w;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .registers 3

    .prologue
    .line 2482
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->k(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final b(Landroid/graphics/Rect;)V
    .registers 4

    .prologue
    .line 2688
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->c(Ljava/lang/Object;Landroid/graphics/Rect;)V

    .line 2689
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 2313
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->f(Ljava/lang/Object;Landroid/view/View;)V

    .line 2314
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 3050
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->b(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 3051
    return-void
.end method

.method public final b(Z)V
    .registers 4

    .prologue
    .line 2808
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->f(Ljava/lang/Object;Z)V

    .line 2809
    return-void
.end method

.method public final c(Landroid/graphics/Rect;)V
    .registers 4

    .prologue
    .line 2697
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->b(Ljava/lang/Object;Landroid/graphics/Rect;)V

    .line 2698
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 2419
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Landroid/view/View;)V

    .line 2420
    return-void
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 3098
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->c(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 3099
    return-void
.end method

.method public final c(Z)V
    .registers 4

    .prologue
    .line 2833
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->k(Ljava/lang/Object;Z)V

    .line 2834
    return-void
.end method

.method public final c()Z
    .registers 3

    .prologue
    .line 2769
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->w(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d(Landroid/graphics/Rect;)V
    .registers 4

    .prologue
    .line 2712
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Landroid/graphics/Rect;)V

    .line 2713
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 2641
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->e(Ljava/lang/Object;Landroid/view/View;)V

    .line 2642
    return-void
.end method

.method public final d(Z)V
    .registers 4

    .prologue
    .line 2858
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->l(Ljava/lang/Object;Z)V

    .line 2859
    return-void
.end method

.method public final d()Z
    .registers 3

    .prologue
    .line 2793
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->x(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e(Z)V
    .registers 4

    .prologue
    .line 2882
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->j(Ljava/lang/Object;Z)V

    .line 2883
    return-void
.end method

.method public final e()Z
    .registers 3

    .prologue
    .line 2817
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->E(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3778
    if-ne p0, p1, :cond_5

    .line 3795
    :cond_4
    :goto_4
    return v0

    .line 3781
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 3782
    goto :goto_4

    .line 3784
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 3785
    goto :goto_4

    .line 3787
    :cond_15
    check-cast p1, Landroid/support/v4/view/a/q;

    .line 3788
    iget-object v2, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    if-nez v2, :cond_21

    .line 3789
    iget-object v2, p1, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3790
    goto :goto_4

    .line 3792
    :cond_21
    iget-object v2, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    iget-object v3, p1, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3793
    goto :goto_4
.end method

.method public final f(Z)V
    .registers 4

    .prologue
    .line 2906
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->c(Ljava/lang/Object;Z)V

    .line 2907
    return-void
.end method

.method public final f()Z
    .registers 3

    .prologue
    .line 2842
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->F(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g(Z)V
    .registers 4

    .prologue
    .line 2930
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->g(Ljava/lang/Object;Z)V

    .line 2931
    return-void
.end method

.method public final g()Z
    .registers 3

    .prologue
    .line 2867
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->B(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final h(Z)V
    .registers 4

    .prologue
    .line 2954
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->d(Ljava/lang/Object;Z)V

    .line 2955
    return-void
.end method

.method public final h()Z
    .registers 3

    .prologue
    .line 2891
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->u(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 3773
    iget-object v0, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_5
.end method

.method public final i(Z)V
    .registers 4

    .prologue
    .line 3002
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/view/a/w;->i(Ljava/lang/Object;Z)V

    .line 3003
    return-void
.end method

.method public final i()Z
    .registers 3

    .prologue
    .line 2915
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->y(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .registers 3

    .prologue
    .line 2939
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->v(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 3011
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->o(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 3035
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->m(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 3059
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->q(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final n()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 3083
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->n(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final o()V
    .registers 3

    .prologue
    .line 3109
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->C(Ljava/lang/Object;)V

    .line 3110
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 3800
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3801
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3803
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 3805
    invoke-virtual {p0, v0}, Landroid/support/v4/view/a/q;->a(Landroid/graphics/Rect;)V

    .line 3806
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "; boundsInParent: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3808
    invoke-virtual {p0, v0}, Landroid/support/v4/view/a/q;->c(Landroid/graphics/Rect;)V

    .line 3809
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "; boundsInScreen: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3811
    const-string v0, "; packageName: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->k()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 3812
    const-string v0, "; className: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->l()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 3813
    const-string v0, "; text: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->m()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 3814
    const-string v0, "; contentDescription: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->n()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 3815
    const-string v0, "; viewId: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 5140
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v3}, Landroid/support/v4/view/a/w;->G(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3815
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3817
    const-string v0, "; checkable: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 5721
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v3}, Landroid/support/v4/view/a/w;->s(Ljava/lang/Object;)Z

    move-result v1

    .line 3817
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3818
    const-string v0, "; checked: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 5745
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v3}, Landroid/support/v4/view/a/w;->t(Ljava/lang/Object;)Z

    move-result v1

    .line 3818
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3819
    const-string v0, "; focusable: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3820
    const-string v0, "; focused: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3821
    const-string v0, "; selected: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->g()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3822
    const-string v0, "; clickable: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3823
    const-string v0, "; longClickable: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->i()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3824
    const-string v0, "; enabled: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->j()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3825
    const-string v0, "; password: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 5963
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v3}, Landroid/support/v4/view/a/w;->z(Ljava/lang/Object;)Z

    move-result v1

    .line 3825
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3826
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "; scrollable: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5987
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, p0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v3}, Landroid/support/v4/view/a/w;->A(Ljava/lang/Object;)Z

    move-result v1

    .line 3826
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3828
    const-string v0, "; ["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3829
    invoke-virtual {p0}, Landroid/support/v4/view/a/q;->b()I

    move-result v0

    :goto_124
    if-eqz v0, :cond_177

    .line 3830
    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v3

    shl-int v3, v1, v3

    .line 3831
    xor-int/lit8 v1, v3, -0x1

    and-int/2addr v1, v0

    .line 6843
    sparse-switch v3, :sswitch_data_182

    .line 6881
    const-string v0, "ACTION_UNKNOWN"

    .line 3832
    :goto_135
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3833
    if-eqz v1, :cond_13f

    .line 3834
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_13f
    move v0, v1

    .line 3836
    goto :goto_124

    .line 6845
    :sswitch_141
    const-string v0, "ACTION_FOCUS"

    goto :goto_135

    .line 6847
    :sswitch_144
    const-string v0, "ACTION_CLEAR_FOCUS"

    goto :goto_135

    .line 6849
    :sswitch_147
    const-string v0, "ACTION_SELECT"

    goto :goto_135

    .line 6851
    :sswitch_14a
    const-string v0, "ACTION_CLEAR_SELECTION"

    goto :goto_135

    .line 6853
    :sswitch_14d
    const-string v0, "ACTION_CLICK"

    goto :goto_135

    .line 6855
    :sswitch_150
    const-string v0, "ACTION_LONG_CLICK"

    goto :goto_135

    .line 6857
    :sswitch_153
    const-string v0, "ACTION_ACCESSIBILITY_FOCUS"

    goto :goto_135

    .line 6859
    :sswitch_156
    const-string v0, "ACTION_CLEAR_ACCESSIBILITY_FOCUS"

    goto :goto_135

    .line 6861
    :sswitch_159
    const-string v0, "ACTION_NEXT_AT_MOVEMENT_GRANULARITY"

    goto :goto_135

    .line 6863
    :sswitch_15c
    const-string v0, "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY"

    goto :goto_135

    .line 6865
    :sswitch_15f
    const-string v0, "ACTION_NEXT_HTML_ELEMENT"

    goto :goto_135

    .line 6867
    :sswitch_162
    const-string v0, "ACTION_PREVIOUS_HTML_ELEMENT"

    goto :goto_135

    .line 6869
    :sswitch_165
    const-string v0, "ACTION_SCROLL_FORWARD"

    goto :goto_135

    .line 6871
    :sswitch_168
    const-string v0, "ACTION_SCROLL_BACKWARD"

    goto :goto_135

    .line 6873
    :sswitch_16b
    const-string v0, "ACTION_CUT"

    goto :goto_135

    .line 6875
    :sswitch_16e
    const-string v0, "ACTION_COPY"

    goto :goto_135

    .line 6877
    :sswitch_171
    const-string v0, "ACTION_PASTE"

    goto :goto_135

    .line 6879
    :sswitch_174
    const-string v0, "ACTION_SET_SELECTION"

    goto :goto_135

    .line 3837
    :cond_177
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3839
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 6843
    nop

    :sswitch_data_182
    .sparse-switch
        0x1 -> :sswitch_141
        0x2 -> :sswitch_144
        0x4 -> :sswitch_147
        0x8 -> :sswitch_14a
        0x10 -> :sswitch_14d
        0x20 -> :sswitch_150
        0x40 -> :sswitch_153
        0x80 -> :sswitch_156
        0x100 -> :sswitch_159
        0x200 -> :sswitch_15c
        0x400 -> :sswitch_15f
        0x800 -> :sswitch_162
        0x1000 -> :sswitch_165
        0x2000 -> :sswitch_168
        0x4000 -> :sswitch_16e
        0x8000 -> :sswitch_171
        0x10000 -> :sswitch_16b
        0x20000 -> :sswitch_174
    .end sparse-switch
.end method
