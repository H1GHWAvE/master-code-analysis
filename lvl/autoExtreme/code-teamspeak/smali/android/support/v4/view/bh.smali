.class final Landroid/support/v4/view/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public static a(Landroid/view/MenuItem;Landroid/support/v4/view/bj;)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 37
    new-instance v0, Landroid/support/v4/view/bi;

    invoke-direct {v0, p1}, Landroid/support/v4/view/bi;-><init>(Landroid/support/v4/view/bj;)V

    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/MenuItem;)Z
    .registers 2

    .prologue
    .line 24
    invoke-interface {p0}, Landroid/view/MenuItem;->expandActionView()Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/view/MenuItem;)Z
    .registers 2

    .prologue
    .line 28
    invoke-interface {p0}, Landroid/view/MenuItem;->collapseActionView()Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/view/MenuItem;)Z
    .registers 2

    .prologue
    .line 32
    invoke-interface {p0}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    return v0
.end method
