.class Landroid/support/v4/view/cy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/di;


# instance fields
.field a:Ljava/util/WeakHashMap;

.field private b:Ljava/lang/reflect/Method;

.field private c:Ljava/lang/reflect/Method;

.field private d:Z


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/cy;->a:Ljava/util/WeakHashMap;

    return-void
.end method

.method private static a(Landroid/support/v4/view/cq;I)Z
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 864
    invoke-interface {p0}, Landroid/support/v4/view/cq;->b()I

    move-result v2

    .line 865
    invoke-interface {p0}, Landroid/support/v4/view/cq;->a()I

    move-result v3

    invoke-interface {p0}, Landroid/support/v4/view/cq;->c()I

    move-result v4

    sub-int/2addr v3, v4

    .line 867
    if-nez v3, :cond_12

    .line 871
    :cond_11
    :goto_11
    return v0

    .line 868
    :cond_12
    if-gez p1, :cond_18

    .line 869
    if-lez v2, :cond_11

    move v0, v1

    goto :goto_11

    .line 871
    :cond_18
    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_11

    move v0, v1

    goto :goto_11
.end method

.method private b()V
    .registers 4

    .prologue
    .line 593
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "dispatchStartTemporaryDetach"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/cy;->b:Ljava/lang/reflect/Method;

    .line 595
    const-class v0, Landroid/view/View;

    const-string v1, "dispatchFinishTemporaryDetach"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/view/cy;->c:Ljava/lang/reflect/Method;
    :try_end_1a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_1a} :catch_1e

    .line 600
    :goto_1a
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/cy;->d:Z

    .line 601
    return-void

    .line 597
    :catch_1e
    move-exception v0

    .line 598
    const-string v1, "ViewCompat"

    const-string v2, "Couldn\'t find method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1a
.end method

.method private static b(Landroid/support/v4/view/cq;I)Z
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 876
    invoke-interface {p0}, Landroid/support/v4/view/cq;->e()I

    move-result v2

    .line 877
    invoke-interface {p0}, Landroid/support/v4/view/cq;->d()I

    move-result v3

    invoke-interface {p0}, Landroid/support/v4/view/cq;->f()I

    move-result v4

    sub-int/2addr v3, v4

    .line 879
    if-nez v3, :cond_12

    .line 883
    :cond_11
    :goto_11
    return v0

    .line 880
    :cond_12
    if-gez p1, :cond_18

    .line 881
    if-lez v2, :cond_11

    move v0, v1

    goto :goto_11

    .line 883
    :cond_18
    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_11

    move v0, v1

    goto :goto_11
.end method


# virtual methods
.method public A(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 625
    const/4 v0, 0x0

    return v0
.end method

.method public B(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 630
    const/4 v0, 0x0

    return v0
.end method

.method public C(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 635
    const/4 v0, 0x0

    return v0
.end method

.method public D(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 640
    const/4 v0, 0x0

    return v0
.end method

.method public E(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 645
    const/4 v0, 0x0

    return v0
.end method

.method public F(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 650
    invoke-static {p1}, Landroid/support/v4/view/dj;->a(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public G(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 655
    invoke-static {p1}, Landroid/support/v4/view/dj;->b(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public H(Landroid/view/View;)Landroid/support/v4/view/fk;
    .registers 3

    .prologue
    .line 660
    new-instance v0, Landroid/support/v4/view/fk;

    invoke-direct {v0, p1}, Landroid/support/v4/view/fk;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public I(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 725
    const/4 v0, 0x0

    return v0
.end method

.method public J(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 730
    const/4 v0, 0x0

    return v0
.end method

.method public K(Landroid/view/View;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 739
    const/4 v0, 0x0

    return-object v0
.end method

.method public L(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 744
    const/4 v0, 0x0

    return v0
.end method

.method public M(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 749
    return-void
.end method

.method public N(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 757
    const/4 v0, 0x0

    return v0
.end method

.method public O(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 766
    const/4 v0, 0x0

    return v0
.end method

.method public P(Landroid/view/View;)Landroid/graphics/Rect;
    .registers 3

    .prologue
    .line 775
    const/4 v0, 0x0

    return-object v0
.end method

.method public Q(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 785
    const/4 v0, 0x0

    return v0
.end method

.method public R(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 796
    return-void
.end method

.method public S(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 817
    return-void
.end method

.method public T(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 826
    const/4 v0, 0x0

    return v0
.end method

.method public U(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 837
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_b

    .line 838
    check-cast p1, Landroid/support/v4/view/bt;

    invoke-interface {p1}, Landroid/support/v4/view/bt;->isNestedScrollingEnabled()Z

    move-result v0

    .line 840
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public V(Landroid/view/View;)Landroid/content/res/ColorStateList;
    .registers 3

    .prologue
    .line 845
    .line 2035
    instance-of v0, p1, Landroid/support/v4/view/cr;

    if-eqz v0, :cond_b

    check-cast p1, Landroid/support/v4/view/cr;

    invoke-interface {p1}, Landroid/support/v4/view/cr;->getSupportBackgroundTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    .line 845
    goto :goto_a
.end method

.method public W(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
    .registers 3

    .prologue
    .line 860
    .line 3047
    instance-of v0, p1, Landroid/support/v4/view/cr;

    if-eqz v0, :cond_b

    check-cast p1, Landroid/support/v4/view/cr;

    invoke-interface {p1}, Landroid/support/v4/view/cr;->getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    .line 860
    goto :goto_a
.end method

.method public X(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 896
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_9

    .line 897
    check-cast p1, Landroid/support/v4/view/bt;

    invoke-interface {p1}, Landroid/support/v4/view/bt;->stopNestedScroll()V

    .line 899
    :cond_9
    return-void
.end method

.method public Y(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 903
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_b

    .line 904
    check-cast p1, Landroid/support/v4/view/bt;

    invoke-interface {p1}, Landroid/support/v4/view/bt;->hasNestedScrollingParent()Z

    move-result v0

    .line 906
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public Z(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 949
    .line 3059
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-lez v0, :cond_e

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    .line 949
    goto :goto_d
.end method

.method public a(II)I
    .registers 4

    .prologue
    .line 954
    or-int v0, p1, p2

    return v0
.end method

.method public a(III)I
    .registers 5

    .prologue
    .line 509
    invoke-static {p1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 405
    const/4 v0, 0x2

    return v0
.end method

.method a()J
    .registers 3

    .prologue
    .line 448
    const-wide/16 v0, 0xa

    return-wide v0
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 3

    .prologue
    .line 806
    return-object p2
.end method

.method public a(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 666
    return-void
.end method

.method public a(Landroid/view/View;IIII)V
    .registers 6

    .prologue
    .line 439
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->invalidate(IIII)V

    .line 440
    return-void
.end method

.method public a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .registers 4

    .prologue
    .line 470
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
    .registers 4

    .prologue
    .line 850
    .line 2041
    instance-of v0, p1, Landroid/support/v4/view/cr;

    if-eqz v0, :cond_9

    .line 2042
    check-cast p1, Landroid/support/v4/view/cr;

    invoke-interface {p1, p2}, Landroid/support/v4/view/cr;->setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 851
    :cond_9
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
    .registers 3

    .prologue
    .line 482
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
    .registers 4

    .prologue
    .line 855
    .line 2053
    instance-of v0, p1, Landroid/support/v4/view/cr;

    if-eqz v0, :cond_9

    .line 2054
    check-cast p1, Landroid/support/v4/view/cr;

    invoke-interface {p1, p2}, Landroid/support/v4/view/cr;->setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 856
    :cond_9
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 771
    return-void
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
    .registers 3

    .prologue
    .line 427
    return-void
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/a;)V
    .registers 3

    .prologue
    .line 412
    return-void
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/bx;)V
    .registers 3

    .prologue
    .line 802
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3

    .prologue
    .line 421
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 5

    .prologue
    .line 442
    invoke-virtual {p0}, Landroid/support/v4/view/cy;->a()J

    move-result-wide v0

    invoke-virtual {p1, p2, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 443
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .registers 8

    .prologue
    .line 445
    invoke-virtual {p0}, Landroid/support/v4/view/cy;->a()J

    move-result-wide v0

    add-long/2addr v0, p3

    invoke-virtual {p1, p2, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 446
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 735
    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 434
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .registers 2

    .prologue
    .line 781
    return-void
.end method

.method public a(Landroid/view/View;FF)Z
    .registers 5

    .prologue
    .line 941
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_b

    .line 942
    check-cast p1, Landroid/support/v4/view/bt;

    invoke-interface {p1, p2, p3}, Landroid/support/v4/view/bt;->dispatchNestedPreFling(FF)Z

    move-result v0

    .line 944
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public a(Landroid/view/View;FFZ)Z
    .registers 6

    .prologue
    .line 932
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_b

    .line 933
    check-cast p1, Landroid/support/v4/view/bt;

    invoke-interface {p1, p2, p3, p4}, Landroid/support/v4/view/bt;->dispatchNestedFling(FFZ)Z

    move-result v0

    .line 936
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public a(Landroid/view/View;I)Z
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 397
    instance-of v2, p1, Landroid/support/v4/view/cq;

    if-eqz v2, :cond_29

    check-cast p1, Landroid/support/v4/view/cq;

    .line 1864
    invoke-interface {p1}, Landroid/support/v4/view/cq;->b()I

    move-result v2

    .line 1865
    invoke-interface {p1}, Landroid/support/v4/view/cq;->a()I

    move-result v3

    invoke-interface {p1}, Landroid/support/v4/view/cq;->c()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1867
    if-eqz v3, :cond_27

    .line 1868
    if-gez p2, :cond_21

    .line 1869
    if-lez v2, :cond_1f

    move v2, v0

    .line 397
    :goto_1c
    if-eqz v2, :cond_29

    :goto_1e
    return v0

    :cond_1f
    move v2, v1

    .line 1869
    goto :goto_1c

    .line 1871
    :cond_21
    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_27

    move v2, v0

    goto :goto_1c

    :cond_27
    move v2, v1

    goto :goto_1c

    :cond_29
    move v0, v1

    .line 397
    goto :goto_1e
.end method

.method public a(Landroid/view/View;IIII[I)Z
    .registers 13

    .prologue
    .line 912
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_11

    move-object v0, p1

    .line 913
    check-cast v0, Landroid/support/v4/view/bt;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/view/bt;->dispatchNestedScroll(IIII[I)Z

    move-result v0

    .line 916
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public a(Landroid/view/View;II[I[I)Z
    .registers 7

    .prologue
    .line 922
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_b

    .line 923
    check-cast p1, Landroid/support/v4/view/bt;

    invoke-interface {p1, p2, p3, p4, p5}, Landroid/support/v4/view/bt;->dispatchNestedPreScroll(II[I[I)Z

    move-result v0

    .line 926
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .registers 5

    .prologue
    .line 460
    const/4 v0, 0x0

    return v0
.end method

.method public aa(Landroid/view/View;)F
    .registers 4

    .prologue
    .line 959
    invoke-virtual {p0, p1}, Landroid/support/v4/view/cy;->O(Landroid/view/View;)F

    move-result v0

    invoke-virtual {p0, p1}, Landroid/support/v4/view/cy;->N(Landroid/view/View;)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method public ab(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 964
    .line 3109
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    .line 964
    goto :goto_7
.end method

.method public b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
    .registers 3

    .prologue
    .line 811
    return-object p2
.end method

.method public b(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 671
    return-void
.end method

.method public b(Landroid/view/View;IIII)V
    .registers 6

    .prologue
    .line 549
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->setPadding(IIII)V

    .line 550
    return-void
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3

    .prologue
    .line 424
    return-void
.end method

.method public b(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 791
    return-void
.end method

.method public b(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 416
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/view/View;I)Z
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 401
    instance-of v2, p1, Landroid/support/v4/view/cq;

    if-eqz v2, :cond_29

    check-cast p1, Landroid/support/v4/view/cq;

    .line 1876
    invoke-interface {p1}, Landroid/support/v4/view/cq;->e()I

    move-result v2

    .line 1877
    invoke-interface {p1}, Landroid/support/v4/view/cq;->d()I

    move-result v3

    invoke-interface {p1}, Landroid/support/v4/view/cq;->f()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1879
    if-eqz v3, :cond_27

    .line 1880
    if-gez p2, :cond_21

    .line 1881
    if-lez v2, :cond_1f

    move v2, v0

    .line 401
    :goto_1c
    if-eqz v2, :cond_29

    :goto_1e
    return v0

    :cond_1f
    move v2, v1

    .line 1881
    goto :goto_1c

    .line 1883
    :cond_21
    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_27

    move v2, v0

    goto :goto_1c

    :cond_27
    move v2, v1

    goto :goto_1c

    :cond_29
    move v0, v1

    .line 401
    goto :goto_1e
.end method

.method public c(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 676
    return-void
.end method

.method public c(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 409
    return-void
.end method

.method public c(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 822
    return-void
.end method

.method public c(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 430
    const/4 v0, 0x0

    return v0
.end method

.method public d(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 436
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 437
    return-void
.end method

.method public d(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 681
    return-void
.end method

.method public d(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 455
    return-void
.end method

.method public d(Landroid/view/View;Z)V
    .registers 4

    .prologue
    .line 830
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_9

    .line 831
    check-cast p1, Landroid/support/v4/view/bt;

    invoke-interface {p1, p2}, Landroid/support/v4/view/bt;->setNestedScrollingEnabled(Z)V

    .line 833
    :cond_9
    return-void
.end method

.method public e(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 451
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 686
    return-void
.end method

.method public e(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 479
    return-void
.end method

.method public f(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 691
    return-void
.end method

.method public f(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 492
    return-void
.end method

.method public f(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 457
    const/4 v0, 0x1

    return v0
.end method

.method public g(Landroid/view/View;)Landroid/support/v4/view/a/aq;
    .registers 3

    .prologue
    .line 463
    const/4 v0, 0x0

    return-object v0
.end method

.method public g(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 696
    return-void
.end method

.method public g(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 535
    return-void
.end method

.method public h(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 466
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public h(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 701
    return-void
.end method

.method public h(Landroid/view/View;I)Z
    .registers 4

    .prologue
    .line 888
    instance-of v0, p1, Landroid/support/v4/view/bt;

    if-eqz v0, :cond_b

    .line 889
    check-cast p1, Landroid/support/v4/view/bt;

    invoke-interface {p1, p2}, Landroid/support/v4/view/bt;->startNestedScroll(I)Z

    move-result v0

    .line 891
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public i(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 472
    const/4 v0, 0x0

    return v0
.end method

.method public i(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 706
    return-void
.end method

.method public j(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 475
    const/4 v0, 0x0

    return v0
.end method

.method public j(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 711
    return-void
.end method

.method public k(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 486
    const/4 v0, 0x0

    return v0
.end method

.method public k(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 716
    return-void
.end method

.method public l(Landroid/view/View;)Landroid/view/ViewParent;
    .registers 3

    .prologue
    .line 496
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method public l(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 721
    return-void
.end method

.method public m(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 753
    return-void
.end method

.method public m(Landroid/view/View;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 501
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 502
    if-eqz v1, :cond_f

    .line 503
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_f

    const/4 v0, 0x1

    .line 505
    :cond_f
    return v0
.end method

.method public n(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 514
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public n(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 762
    return-void
.end method

.method public o(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 519
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public p(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 524
    const/4 v0, 0x0

    return v0
.end method

.method public q(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 529
    const/4 v0, 0x0

    return v0
.end method

.method public r(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 539
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    return v0
.end method

.method public s(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 544
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    return v0
.end method

.method public final t(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 554
    iget-boolean v0, p0, Landroid/support/v4/view/cy;->d:Z

    if-nez v0, :cond_7

    .line 555
    invoke-direct {p0}, Landroid/support/v4/view/cy;->b()V

    .line 557
    :cond_7
    iget-object v0, p0, Landroid/support/v4/view/cy;->b:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1d

    .line 559
    :try_start_b
    iget-object v0, p0, Landroid/support/v4/view/cy;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_13} :catch_14

    .line 567
    :goto_13
    return-void

    .line 560
    :catch_14
    move-exception v0

    .line 561
    const-string v1, "ViewCompat"

    const-string v2, "Error calling dispatchStartTemporaryDetach"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_13

    .line 565
    :cond_1d
    invoke-virtual {p1}, Landroid/view/View;->onStartTemporaryDetach()V

    goto :goto_13
.end method

.method public final u(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 571
    iget-boolean v0, p0, Landroid/support/v4/view/cy;->d:Z

    if-nez v0, :cond_7

    .line 572
    invoke-direct {p0}, Landroid/support/v4/view/cy;->b()V

    .line 574
    :cond_7
    iget-object v0, p0, Landroid/support/v4/view/cy;->c:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1d

    .line 576
    :try_start_b
    iget-object v0, p0, Landroid/support/v4/view/cy;->c:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_13} :catch_14

    .line 584
    :goto_13
    return-void

    .line 577
    :catch_14
    move-exception v0

    .line 578
    const-string v1, "ViewCompat"

    const-string v2, "Error calling dispatchFinishTemporaryDetach"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_13

    .line 582
    :cond_1d
    invoke-virtual {p1}, Landroid/view/View;->onFinishTemporaryDetach()V

    goto :goto_13
.end method

.method public v(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 588
    const/4 v0, 0x1

    return v0
.end method

.method public w(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 605
    const/4 v0, 0x0

    return v0
.end method

.method public x(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 610
    const/4 v0, 0x0

    return v0
.end method

.method public y(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 615
    const/4 v0, 0x0

    return v0
.end method

.method public z(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 620
    const/4 v0, 0x0

    return v0
.end method
