.class public final Landroid/support/v4/view/at;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/view/au;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 127
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 128
    const/16 v1, 0x11

    if-lt v0, v1, :cond_e

    .line 129
    new-instance v0, Landroid/support/v4/view/aw;

    invoke-direct {v0}, Landroid/support/v4/view/aw;-><init>()V

    sput-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    .line 133
    :goto_d
    return-void

    .line 131
    :cond_e
    new-instance v0, Landroid/support/v4/view/av;

    invoke-direct {v0}, Landroid/support/v4/view/av;-><init>()V

    sput-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    return-void
.end method

.method public static a(Landroid/view/ViewGroup$MarginLayoutParams;)I
    .registers 2

    .prologue
    .line 147
    sget-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    invoke-interface {v0, p0}, Landroid/support/v4/view/au;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/ViewGroup$MarginLayoutParams;I)V
    .registers 3

    .prologue
    .line 177
    sget-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/au;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 178
    return-void
.end method

.method public static b(Landroid/view/ViewGroup$MarginLayoutParams;)I
    .registers 2

    .prologue
    .line 162
    sget-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    invoke-interface {v0, p0}, Landroid/support/v4/view/au;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    return v0
.end method

.method private static b(Landroid/view/ViewGroup$MarginLayoutParams;I)V
    .registers 3

    .prologue
    .line 192
    sget-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/au;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 193
    return-void
.end method

.method private static c(Landroid/view/ViewGroup$MarginLayoutParams;I)V
    .registers 3

    .prologue
    .line 222
    sget-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/au;->c(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 223
    return-void
.end method

.method private static c(Landroid/view/ViewGroup$MarginLayoutParams;)Z
    .registers 2

    .prologue
    .line 201
    sget-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    invoke-interface {v0, p0}, Landroid/support/v4/view/au;->c(Landroid/view/ViewGroup$MarginLayoutParams;)Z

    move-result v0

    return v0
.end method

.method private static d(Landroid/view/ViewGroup$MarginLayoutParams;)I
    .registers 2

    .prologue
    .line 211
    sget-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    invoke-interface {v0, p0}, Landroid/support/v4/view/au;->d(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    return v0
.end method

.method private static d(Landroid/view/ViewGroup$MarginLayoutParams;I)V
    .registers 3

    .prologue
    .line 231
    sget-object v0, Landroid/support/v4/view/at;->a:Landroid/support/v4/view/au;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/au;->d(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 232
    return-void
.end method
