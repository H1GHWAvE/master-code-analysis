.class public final Landroid/support/v4/view/az;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x4

.field public static final e:I = 0x8

.field static final f:Landroid/support/v4/view/be;

.field private static final g:Ljava/lang/String; = "MenuItemCompat"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 247
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 248
    const/16 v1, 0xe

    if-lt v0, v1, :cond_e

    .line 249
    new-instance v0, Landroid/support/v4/view/bc;

    invoke-direct {v0}, Landroid/support/v4/view/bc;-><init>()V

    sput-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    .line 255
    :goto_d
    return-void

    .line 250
    :cond_e
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1a

    .line 251
    new-instance v0, Landroid/support/v4/view/bb;

    invoke-direct {v0}, Landroid/support/v4/view/bb;-><init>()V

    sput-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    goto :goto_d

    .line 253
    :cond_1a
    new-instance v0, Landroid/support/v4/view/ba;

    invoke-direct {v0}, Landroid/support/v4/view/ba;-><init>()V

    sput-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    return-void
.end method

.method private static a(Landroid/view/MenuItem;Landroid/support/v4/view/bf;)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 435
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 436
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0, p1}, Landroid/support/v4/g/a/b;->a(Landroid/support/v4/view/bf;)Landroid/support/v4/g/a/b;

    move-result-object v0

    .line 438
    :goto_a
    return-object v0

    :cond_b
    sget-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/be;->a(Landroid/view/MenuItem;Landroid/support/v4/view/bf;)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_a
.end method

.method public static a(Landroid/view/MenuItem;Landroid/support/v4/view/n;)Landroid/view/MenuItem;
    .registers 4

    .prologue
    .line 344
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 345
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0, p1}, Landroid/support/v4/g/a/b;->a(Landroid/support/v4/view/n;)Landroid/support/v4/g/a/b;

    move-result-object p0

    .line 349
    :goto_a
    return-object p0

    .line 348
    :cond_b
    const-string v0, "MenuItemCompat"

    const-string v1, "setActionProvider: item does not implement SupportMenuItem; ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a
.end method

.method public static a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 287
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 288
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0, p1}, Landroid/support/v4/g/a/b;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v0

    .line 290
    :goto_a
    return-object v0

    :cond_b
    sget-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/be;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_a
.end method

.method public static a(Landroid/view/MenuItem;)Landroid/view/View;
    .registers 2

    .prologue
    .line 322
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 323
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0}, Landroid/support/v4/g/a/b;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 325
    :goto_a
    return-object v0

    :cond_b
    sget-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    invoke-interface {v0, p0}, Landroid/support/v4/view/be;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    goto :goto_a
.end method

.method public static a(Landroid/view/MenuItem;I)V
    .registers 3

    .prologue
    .line 268
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_a

    .line 269
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0, p1}, Landroid/support/v4/g/a/b;->setShowAsAction(I)V

    .line 273
    :goto_9
    return-void

    .line 271
    :cond_a
    sget-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/be;->a(Landroid/view/MenuItem;I)V

    goto :goto_9
.end method

.method public static b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;
    .registers 3

    .prologue
    .line 309
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 310
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0, p1}, Landroid/support/v4/g/a/b;->setActionView(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 312
    :goto_a
    return-object v0

    :cond_b
    sget-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/be;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_a
.end method

.method public static b(Landroid/view/MenuItem;)Z
    .registers 2

    .prologue
    .line 383
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 384
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0}, Landroid/support/v4/g/a/b;->expandActionView()Z

    move-result v0

    .line 386
    :goto_a
    return v0

    :cond_b
    sget-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    invoke-interface {v0, p0}, Landroid/support/v4/view/be;->b(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_a
.end method

.method public static c(Landroid/view/MenuItem;)Z
    .registers 2

    .prologue
    .line 418
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 419
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0}, Landroid/support/v4/g/a/b;->isActionViewExpanded()Z

    move-result v0

    .line 421
    :goto_a
    return v0

    :cond_b
    sget-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    invoke-interface {v0, p0}, Landroid/support/v4/view/be;->d(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_a
.end method

.method private static d(Landroid/view/MenuItem;)Landroid/support/v4/view/n;
    .registers 3

    .prologue
    .line 361
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 362
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0}, Landroid/support/v4/g/a/b;->a()Landroid/support/v4/view/n;

    move-result-object v0

    .line 367
    :goto_a
    return-object v0

    .line 366
    :cond_b
    const-string v0, "MenuItemCompat"

    const-string v1, "getActionProvider: item does not implement SupportMenuItem; returning null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private static e(Landroid/view/MenuItem;)Z
    .registers 2

    .prologue
    .line 402
    instance-of v0, p0, Landroid/support/v4/g/a/b;

    if-eqz v0, :cond_b

    .line 403
    check-cast p0, Landroid/support/v4/g/a/b;

    invoke-interface {p0}, Landroid/support/v4/g/a/b;->collapseActionView()Z

    move-result v0

    .line 405
    :goto_a
    return v0

    :cond_b
    sget-object v0, Landroid/support/v4/view/az;->f:Landroid/support/v4/view/be;

    invoke-interface {v0, p0}, Landroid/support/v4/view/be;->c(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_a
.end method
