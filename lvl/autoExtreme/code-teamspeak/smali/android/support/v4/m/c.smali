.class public final Landroid/support/v4/m/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:Landroid/support/v4/m/l;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/m/a;->a(Ljava/util/Locale;)Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v4/m/c;->a(Z)V

    .line 139
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .registers 3

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    invoke-static {p1}, Landroid/support/v4/m/a;->a(Ljava/util/Locale;)Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v4/m/c;->a(Z)V

    .line 157
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 2

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    invoke-direct {p0, p1}, Landroid/support/v4/m/c;->a(Z)V

    .line 148
    return-void
.end method

.method private a(Landroid/support/v4/m/l;)Landroid/support/v4/m/c;
    .registers 2

    .prologue
    .line 191
    iput-object p1, p0, Landroid/support/v4/m/c;->c:Landroid/support/v4/m/l;

    .line 192
    return-object p0
.end method

.method private a(Z)V
    .registers 3

    .prologue
    .line 165
    iput-boolean p1, p0, Landroid/support/v4/m/c;->a:Z

    .line 166
    invoke-static {}, Landroid/support/v4/m/a;->a()Landroid/support/v4/m/l;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/m/c;->c:Landroid/support/v4/m/l;

    .line 167
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v4/m/c;->b:I

    .line 168
    return-void
.end method

.method private b(Z)Landroid/support/v4/m/c;
    .registers 3

    .prologue
    .line 175
    if-eqz p1, :cond_9

    .line 176
    iget v0, p0, Landroid/support/v4/m/c;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/support/v4/m/c;->b:I

    .line 180
    :goto_8
    return-object p0

    .line 178
    :cond_9
    iget v0, p0, Landroid/support/v4/m/c;->b:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Landroid/support/v4/m/c;->b:I

    goto :goto_8
.end method

.method private static c(Z)Landroid/support/v4/m/a;
    .registers 2

    .prologue
    .line 196
    if-eqz p0, :cond_7

    invoke-static {}, Landroid/support/v4/m/a;->b()Landroid/support/v4/m/a;

    move-result-object v0

    :goto_6
    return-object v0

    :cond_7
    invoke-static {}, Landroid/support/v4/m/a;->c()Landroid/support/v4/m/a;

    move-result-object v0

    goto :goto_6
.end method


# virtual methods
.method public final a()Landroid/support/v4/m/a;
    .registers 6

    .prologue
    .line 203
    iget v0, p0, Landroid/support/v4/m/c;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1b

    iget-object v0, p0, Landroid/support/v4/m/c;->c:Landroid/support/v4/m/l;

    invoke-static {}, Landroid/support/v4/m/a;->a()Landroid/support/v4/m/l;

    move-result-object v1

    if-ne v0, v1, :cond_1b

    .line 205
    iget-boolean v0, p0, Landroid/support/v4/m/c;->a:Z

    .line 1196
    if-eqz v0, :cond_16

    invoke-static {}, Landroid/support/v4/m/a;->b()Landroid/support/v4/m/a;

    move-result-object v0

    :goto_15
    return-object v0

    :cond_16
    invoke-static {}, Landroid/support/v4/m/a;->c()Landroid/support/v4/m/a;

    move-result-object v0

    goto :goto_15

    .line 207
    :cond_1b
    new-instance v0, Landroid/support/v4/m/a;

    iget-boolean v1, p0, Landroid/support/v4/m/c;->a:Z

    iget v2, p0, Landroid/support/v4/m/c;->b:I

    iget-object v3, p0, Landroid/support/v4/m/c;->c:Landroid/support/v4/m/l;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/support/v4/m/a;-><init>(ZILandroid/support/v4/m/l;B)V

    goto :goto_15
.end method
