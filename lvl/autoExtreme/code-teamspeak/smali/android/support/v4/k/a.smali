.class public abstract Landroid/support/v4/k/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String; = "DocumentFile"


# instance fields
.field private final b:Landroid/support/v4/k/a;


# direct methods
.method constructor <init>(Landroid/support/v4/k/a;)V
    .registers 2

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Landroid/support/v4/k/a;->b:Landroid/support/v4/k/a;

    .line 85
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/support/v4/k/a;
    .registers 4

    .prologue
    .line 110
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 111
    const/16 v1, 0x13

    if-lt v0, v1, :cond_c

    .line 112
    new-instance v0, Landroid/support/v4/k/e;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/k/e;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 114
    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private static a(Ljava/io/File;)Landroid/support/v4/k/a;
    .registers 3

    .prologue
    .line 96
    new-instance v0, Landroid/support/v4/k/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Landroid/support/v4/k/d;-><init>(Landroid/support/v4/k/a;Ljava/io/File;)V

    return-object v0
.end method

.method private static b(Landroid/content/Context;Landroid/net/Uri;)Landroid/support/v4/k/a;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 128
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 129
    const/16 v2, 0x15

    if-lt v0, v2, :cond_15

    .line 130
    new-instance v0, Landroid/support/v4/k/f;

    .line 1042
    invoke-static {p1}, Landroid/provider/DocumentsContract;->getTreeDocumentId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/provider/DocumentsContract;->buildDocumentUriUsingTree(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 130
    invoke-direct {v0, v1, p0, v2}, Landroid/support/v4/k/f;-><init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V

    .line 133
    :goto_14
    return-object v0

    :cond_15
    move-object v0, v1

    goto :goto_14
.end method

.method private c(Ljava/lang/String;)Landroid/support/v4/k/a;
    .registers 7

    .prologue
    .line 316
    invoke-virtual {p0}, Landroid/support/v4/k/a;->l()[Landroid/support/v4/k/a;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_1a

    aget-object v0, v2, v1

    .line 317
    invoke-virtual {v0}, Landroid/support/v4/k/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 321
    :goto_15
    return-object v0

    .line 316
    :cond_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 321
    :cond_1a
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private static c(Landroid/content/Context;Landroid/net/Uri;)Z
    .registers 4

    .prologue
    .line 142
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 143
    const/16 v1, 0x13

    if-lt v0, v1, :cond_b

    .line 2033
    invoke-static {p0, p1}, Landroid/provider/DocumentsContract;->isDocumentUri(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    .line 146
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private m()Landroid/support/v4/k/a;
    .registers 2

    .prologue
    .line 217
    iget-object v0, p0, Landroid/support/v4/k/a;->b:Landroid/support/v4/k/a;

    return-object v0
.end method


# virtual methods
.method public abstract a()Landroid/net/Uri;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/support/v4/k/a;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/k/a;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract b(Ljava/lang/String;)Z
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()J
.end method

.method public abstract g()J
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public abstract j()Z
.end method

.method public abstract k()Z
.end method

.method public abstract l()[Landroid/support/v4/k/a;
.end method
