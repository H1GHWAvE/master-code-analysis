.class final Landroid/support/v4/k/e;
.super Landroid/support/v4/k/a;
.source "SourceFile"


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/net/Uri;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 4

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/k/a;-><init>(Landroid/support/v4/k/a;)V

    .line 29
    iput-object p1, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v4/k/a;
    .registers 3

    .prologue
    .line 40
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/k/a;
    .registers 4

    .prologue
    .line 35
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .registers 4

    .prologue
    .line 50
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    .line 1037
    const-string v2, "_display_name"

    invoke-static {v0, v1, v2}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method public final b(Ljava/lang/String;)Z
    .registers 3

    .prologue
    .line 105
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .registers 3

    .prologue
    .line 60
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .registers 3

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->c(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final f()J
    .registers 4

    .prologue
    .line 70
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    .line 1067
    const-string v2, "last_modified"

    invoke-static {v0, v1, v2}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    .line 70
    return-wide v0
.end method

.method public final g()J
    .registers 4

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    .line 1071
    const-string v2, "_size"

    invoke-static {v0, v1, v2}, Landroid/support/v4/k/b;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    .line 75
    return-wide v0
.end method

.method public final h()Z
    .registers 3

    .prologue
    .line 80
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->d(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .registers 3

    .prologue
    .line 85
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->e(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .registers 3

    .prologue
    .line 90
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->f(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .registers 3

    .prologue
    .line 95
    iget-object v0, p0, Landroid/support/v4/k/e;->b:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/k/e;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v4/k/b;->g(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final l()[Landroid/support/v4/k/a;
    .registers 2

    .prologue
    .line 100
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
