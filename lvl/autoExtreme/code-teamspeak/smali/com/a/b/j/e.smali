.class final Lcom/a/b/j/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/c;
    a = "com.google.common.math.DoubleUtils"
.end annotation


# instance fields
.field private a:J

.field private b:D


# direct methods
.method private constructor <init>()V
    .registers 3

    .prologue
    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/a/b/j/e;->a:J

    .line 390
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/a/b/j/e;->b:D

    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/a/b/j/e;-><init>()V

    return-void
.end method


# virtual methods
.method final a()D
    .registers 5

    .prologue
    .line 400
    iget-wide v0, p0, Lcom/a/b/j/e;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_11

    const/4 v0, 0x1

    :goto_9
    const-string v1, "Cannot take mean of 0 values"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 401
    iget-wide v0, p0, Lcom/a/b/j/e;->b:D

    return-wide v0

    .line 400
    :cond_11
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final a(D)V
    .registers 10

    .prologue
    .line 393
    invoke-static {p1, p2}, Lcom/a/b/j/f;->b(D)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 394
    iget-wide v0, p0, Lcom/a/b/j/e;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/j/e;->a:J

    .line 396
    iget-wide v0, p0, Lcom/a/b/j/e;->b:D

    iget-wide v2, p0, Lcom/a/b/j/e;->b:D

    sub-double v2, p1, v2

    iget-wide v4, p0, Lcom/a/b/j/e;->a:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/j/e;->b:D

    .line 397
    return-void
.end method
