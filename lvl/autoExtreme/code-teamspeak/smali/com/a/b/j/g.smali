.class public final Lcom/a/b/j/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field static final a:I = -0x4afb0ccd
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field static final b:[B
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field static final c:[I
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field static final d:[I
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field static final e:I = 0xb504
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field static f:[I
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field private static final g:[I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/16 v1, 0xa

    .line 169
    const/16 v0, 0x21

    new-array v0, v0, [B

    fill-array-data v0, :array_2c

    sput-object v0, Lcom/a/b/j/g;->b:[B

    .line 172
    new-array v0, v1, [I

    fill-array-data v0, :array_42

    sput-object v0, Lcom/a/b/j/g;->c:[I

    .line 176
    new-array v0, v1, [I

    fill-array-data v0, :array_5a

    sput-object v0, Lcom/a/b/j/g;->d:[I

    .line 502
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_72

    sput-object v0, Lcom/a/b/j/g;->g:[I

    .line 550
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_90

    sput-object v0, Lcom/a/b/j/g;->f:[I

    return-void

    .line 169
    :array_2c
    .array-data 1
        0x9t
        0x9t
        0x9t
        0x8t
        0x8t
        0x8t
        0x7t
        0x7t
        0x7t
        0x6t
        0x6t
        0x6t
        0x6t
        0x5t
        0x5t
        0x5t
        0x4t
        0x4t
        0x4t
        0x3t
        0x3t
        0x3t
        0x3t
        0x2t
        0x2t
        0x2t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 172
    nop

    :array_42
    .array-data 4
        0x1
        0xa
        0x64
        0x3e8
        0x2710
        0x186a0
        0xf4240
        0x989680
        0x5f5e100
        0x3b9aca00
    .end array-data

    .line 176
    :array_5a
    .array-data 4
        0x3
        0x1f
        0x13c
        0xc5a
        0x7b86
        0x4d343
        0x3040a5
        0x1e28678
        0x12d940b6
        0x7fffffff
    .end array-data

    .line 502
    :array_72
    .array-data 4
        0x1
        0x1
        0x2
        0x6
        0x18
        0x78
        0x2d0
        0x13b0
        0x9d80
        0x58980
        0x375f00
        0x2611500
        0x1c8cfc00
    .end array-data

    .line 550
    :array_90
    .array-data 4
        0x7fffffff
        0x7fffffff
        0x10000
        0x929
        0x1dd
        0xc1
        0x6e
        0x4b
        0x3a
        0x31
        0x2b
        0x27
        0x25
        0x23
        0x22
        0x22
        0x21
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .registers 2

    .prologue
    .line 498
    const-string v0, "n"

    invoke-static {v0, p0}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 499
    sget-object v0, Lcom/a/b/j/g;->g:[I

    array-length v0, v0

    if-ge p0, v0, :cond_f

    sget-object v0, Lcom/a/b/j/g;->g:[I

    aget v0, v0, p0

    :goto_e
    return v0

    :cond_f
    const v0, 0x7fffffff

    goto :goto_e
.end method

.method public static a(II)I
    .registers 6

    .prologue
    .line 415
    int-to-long v0, p0

    int-to-long v2, p1

    add-long/2addr v2, v0

    .line 416
    long-to-int v0, v2

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_a
    invoke-static {v0}, Lcom/a/b/j/k;->c(Z)V

    .line 417
    long-to-int v0, v2

    return v0

    .line 416
    :cond_f
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public static a(IILjava/math/RoundingMode;)I
    .registers 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 279
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    if-nez p1, :cond_f

    .line 281
    new-instance v0, Ljava/lang/ArithmeticException;

    const-string v1, "/ by zero"

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_f
    div-int v2, p0, p1

    .line 284
    mul-int v3, p1, v2

    sub-int v3, p0, v3

    .line 286
    if-nez v3, :cond_19

    move v0, v2

    .line 331
    :goto_18
    return v0

    .line 297
    :cond_19
    xor-int v4, p0, p1

    shr-int/lit8 v4, v4, 0x1f

    or-int/lit8 v5, v4, 0x1

    .line 299
    sget-object v4, Lcom/a/b/j/h;->a:[I

    invoke-virtual {p2}, Ljava/math/RoundingMode;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_70

    .line 329
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 301
    :pswitch_30
    if-nez v3, :cond_3b

    :goto_32
    invoke-static {v0}, Lcom/a/b/j/k;->a(Z)V

    :pswitch_35
    move v0, v1

    .line 331
    :cond_36
    :goto_36
    :pswitch_36
    if-eqz v0, :cond_6d

    add-int v0, v2, v5

    goto :goto_18

    :cond_3b
    move v0, v1

    .line 301
    goto :goto_32

    .line 310
    :pswitch_3d
    if-gtz v5, :cond_36

    move v0, v1

    goto :goto_36

    .line 313
    :pswitch_41
    if-ltz v5, :cond_36

    move v0, v1

    goto :goto_36

    .line 318
    :pswitch_45
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 319
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    sub-int/2addr v4, v3

    sub-int/2addr v3, v4

    .line 322
    if-nez v3, :cond_69

    .line 323
    sget-object v3, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    if-eq p2, v3, :cond_62

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    if-ne p2, v3, :cond_65

    move v4, v0

    :goto_5a
    and-int/lit8 v3, v2, 0x1

    if-eqz v3, :cond_67

    move v3, v0

    :goto_5f
    and-int/2addr v3, v4

    if-eqz v3, :cond_63

    :cond_62
    move v1, v0

    :cond_63
    move v0, v1

    goto :goto_36

    :cond_65
    move v4, v1

    goto :goto_5a

    :cond_67
    move v3, v1

    goto :goto_5f

    .line 325
    :cond_69
    if-gtz v3, :cond_36

    move v0, v1

    goto :goto_36

    :cond_6d
    move v0, v2

    .line 331
    goto :goto_18

    .line 299
    nop

    :pswitch_data_70
    .packed-switch 0x1
        :pswitch_30
        :pswitch_35
        :pswitch_41
        :pswitch_36
        :pswitch_3d
        :pswitch_45
        :pswitch_45
        :pswitch_45
    .end packed-switch
.end method

.method public static a(ILjava/math/RoundingMode;)I
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    const-string v2, "x"

    invoke-static {v2, p0}, Lcom/a/b/j/k;->a(Ljava/lang/String;I)I

    .line 89
    sget-object v2, Lcom/a/b/j/h;->a:[I

    invoke-virtual {p1}, Ljava/math/RoundingMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_48

    .line 112
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1063
    :pswitch_18
    if-lez p0, :cond_2b

    move v2, v0

    :goto_1b
    add-int/lit8 v3, p0, -0x1

    and-int/2addr v3, p0

    if-nez v3, :cond_2d

    :goto_20
    and-int/2addr v0, v2

    .line 91
    invoke-static {v0}, Lcom/a/b/j/k;->a(Z)V

    .line 95
    :pswitch_24
    invoke-static {p0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x1f

    .line 109
    :goto_2a
    return v0

    :cond_2b
    move v2, v1

    .line 1063
    goto :goto_1b

    :cond_2d
    move v0, v1

    goto :goto_20

    .line 99
    :pswitch_2f
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x20

    goto :goto_2a

    .line 105
    :pswitch_38
    invoke-static {p0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    .line 106
    const v1, -0x4afb0ccd

    ushr-int/2addr v1, v0

    .line 108
    rsub-int/lit8 v0, v0, 0x1f

    .line 109
    invoke-static {v1, p0}, Lcom/a/b/j/g;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2a

    .line 89
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_18
        :pswitch_24
        :pswitch_24
        :pswitch_2f
        :pswitch_2f
        :pswitch_38
        :pswitch_38
        :pswitch_38
    .end packed-switch
.end method

.method public static b(II)I
    .registers 6

    .prologue
    .line 437
    int-to-long v0, p0

    int-to-long v2, p1

    mul-long/2addr v2, v0

    .line 438
    long-to-int v0, v2

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_a
    invoke-static {v0}, Lcom/a/b/j/k;->c(Z)V

    .line 439
    long-to-int v0, v2

    return v0

    .line 438
    :cond_f
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public static b(ILjava/math/RoundingMode;)I
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "need BigIntegerMath to adequately test"
    .end annotation

    .prologue
    .line 232
    const-string v0, "x"

    invoke-static {v0, p0}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 1267
    int-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v1, v0

    .line 234
    sget-object v0, Lcom/a/b/j/h;->a:[I

    invoke-virtual {p1}, Ljava/math/RoundingMode;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_3a

    .line 260
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 236
    :pswitch_1c
    mul-int v0, v1, v1

    if-ne v0, p0, :cond_26

    const/4 v0, 0x1

    :goto_21
    invoke-static {v0}, Lcom/a/b/j/k;->a(Z)V

    :pswitch_24
    move v0, v1

    .line 258
    :goto_25
    return v0

    .line 236
    :cond_26
    const/4 v0, 0x0

    goto :goto_21

    .line 242
    :pswitch_28
    mul-int v0, v1, v1

    invoke-static {v0, p0}, Lcom/a/b/j/g;->c(II)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_25

    .line 246
    :pswitch_30
    mul-int v0, v1, v1

    add-int/2addr v0, v1

    .line 258
    invoke-static {v0, p0}, Lcom/a/b/j/g;->c(II)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_25

    .line 234
    nop

    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_24
        :pswitch_24
        :pswitch_28
        :pswitch_28
        :pswitch_30
        :pswitch_30
        :pswitch_30
    .end packed-switch
.end method

.method private static b(I)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63
    if-lez p0, :cond_c

    move v2, v0

    :goto_5
    add-int/lit8 v3, p0, -0x1

    and-int/2addr v3, p0

    if-nez v3, :cond_e

    :goto_a
    and-int/2addr v0, v2

    return v0

    :cond_c
    move v2, v1

    goto :goto_5

    :cond_e
    move v0, v1

    goto :goto_a
.end method

.method private static c(I)I
    .registers 3

    .prologue
    .line 160
    sget-object v0, Lcom/a/b/j/g;->b:[B

    invoke-static {p0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v1

    aget-byte v0, v0, v1

    .line 165
    sget-object v1, Lcom/a/b/j/g;->c:[I

    aget v1, v1, v0

    invoke-static {p0, v1}, Lcom/a/b/j/g;->c(II)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private static c(II)I
    .registers 3
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 75
    sub-int v0, p0, p1

    xor-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    ushr-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method private static c(ILjava/math/RoundingMode;)I
    .registers 6
    .annotation build Lcom/a/b/a/c;
        a = "need BigIntegerMath to adequately test"
    .end annotation

    .prologue
    .line 129
    const-string v0, "x"

    invoke-static {v0, p0}, Lcom/a/b/j/k;->a(Ljava/lang/String;I)I

    .line 1160
    sget-object v0, Lcom/a/b/j/g;->b:[B

    invoke-static {p0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v1

    aget-byte v0, v0, v1

    .line 1165
    sget-object v1, Lcom/a/b/j/g;->c:[I

    aget v1, v1, v0

    invoke-static {p0, v1}, Lcom/a/b/j/g;->c(II)I

    move-result v1

    sub-int v1, v0, v1

    .line 131
    sget-object v0, Lcom/a/b/j/g;->c:[I

    aget v0, v0, v1

    .line 132
    sget-object v2, Lcom/a/b/j/h;->a:[I

    invoke-virtual {p1}, Ljava/math/RoundingMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_46

    .line 148
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 134
    :pswitch_2c
    if-ne p0, v0, :cond_34

    const/4 v0, 0x1

    :goto_2f
    invoke-static {v0}, Lcom/a/b/j/k;->a(Z)V

    :pswitch_32
    move v0, v1

    .line 146
    :goto_33
    return v0

    .line 134
    :cond_34
    const/4 v0, 0x0

    goto :goto_2f

    .line 141
    :pswitch_36
    invoke-static {v0, p0}, Lcom/a/b/j/g;->c(II)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_33

    .line 146
    :pswitch_3c
    sget-object v0, Lcom/a/b/j/g;->d:[I

    aget v0, v0, v1

    invoke-static {v0, p0}, Lcom/a/b/j/g;->c(II)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_33

    .line 132
    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_2c
        :pswitch_32
        :pswitch_32
        :pswitch_36
        :pswitch_36
        :pswitch_3c
        :pswitch_3c
        :pswitch_3c
    .end packed-switch
.end method

.method private static d(I)I
    .registers 3

    .prologue
    .line 267
    int-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method private static d(II)I
    .registers 6
    .annotation build Lcom/a/b/a/c;
        a = "failing tests"
    .end annotation

    .prologue
    const/16 v3, 0x20

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 190
    const-string v2, "exponent"

    invoke-static {v2, p1}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 191
    packed-switch p0, :pswitch_data_42

    move v3, v1

    move v2, p0

    .line 210
    :goto_e
    packed-switch p1, :pswitch_data_50

    .line 216
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_3f

    move v0, v1

    :goto_16
    mul-int/2addr v3, v0

    .line 217
    mul-int/2addr v2, v2

    .line 209
    shr-int/lit8 p1, p1, 0x1

    goto :goto_e

    .line 193
    :pswitch_1b
    if-nez p1, :cond_1e

    .line 214
    :cond_1d
    :goto_1d
    :pswitch_1d
    return v1

    :cond_1e
    move v1, v0

    .line 193
    goto :goto_1d

    .line 197
    :pswitch_20
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1d

    const/4 v1, -0x1

    goto :goto_1d

    .line 199
    :pswitch_26
    if-ge p1, v3, :cond_2a

    shl-int/2addr v1, p1

    goto :goto_1d

    :cond_2a
    move v1, v0

    goto :goto_1d

    .line 201
    :pswitch_2c
    if-ge p1, v3, :cond_38

    .line 202
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_34

    shl-int/2addr v1, p1

    goto :goto_1d

    :cond_34
    shl-int v0, v1, p1

    neg-int v1, v0

    goto :goto_1d

    :cond_38
    move v1, v0

    .line 204
    goto :goto_1d

    :pswitch_3a
    move v1, v3

    .line 212
    goto :goto_1d

    .line 214
    :pswitch_3c
    mul-int v1, v2, v3

    goto :goto_1d

    :cond_3f
    move v0, v2

    .line 216
    goto :goto_16

    .line 191
    nop

    :pswitch_data_42
    .packed-switch -0x2
        :pswitch_2c
        :pswitch_20
        :pswitch_1b
        :pswitch_1d
        :pswitch_26
    .end packed-switch

    .line 210
    :pswitch_data_50
    .packed-switch 0x0
        :pswitch_3a
        :pswitch_3c
    .end packed-switch
.end method

.method private static e(II)I
    .registers 5

    .prologue
    .line 351
    if-gtz p1, :cond_23

    .line 352
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Modulus "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be > 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_23
    rem-int v0, p0, p1

    .line 355
    if-ltz v0, :cond_28

    :goto_27
    return v0

    :cond_28
    add-int/2addr v0, p1

    goto :goto_27
.end method

.method private static f(II)I
    .registers 7

    .prologue
    .line 370
    const-string v0, "a"

    invoke-static {v0, p0}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 371
    const-string v0, "b"

    invoke-static {v0, p1}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 372
    if-nez p0, :cond_d

    .line 406
    :goto_c
    return p1

    .line 376
    :cond_d
    if-nez p1, :cond_11

    move p1, p0

    .line 377
    goto :goto_c

    .line 383
    :cond_11
    invoke-static {p0}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v2

    .line 384
    shr-int v1, p0, v2

    .line 385
    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v3

    .line 386
    shr-int v0, p1, v3

    .line 387
    :goto_1d
    if-eq v1, v0, :cond_2c

    .line 395
    sub-int/2addr v1, v0

    .line 397
    shr-int/lit8 v4, v1, 0x1f

    and-int/2addr v4, v1

    .line 400
    sub-int/2addr v1, v4

    sub-int/2addr v1, v4

    .line 403
    add-int/2addr v0, v4

    .line 404
    invoke-static {v1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v4

    shr-int/2addr v1, v4

    .line 405
    goto :goto_1d

    .line 406
    :cond_2c
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    shl-int p1, v1, v0

    goto :goto_c
.end method

.method private static g(II)I
    .registers 6

    .prologue
    .line 426
    int-to-long v0, p0

    int-to-long v2, p1

    sub-long v2, v0, v2

    .line 427
    long-to-int v0, v2

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_b
    invoke-static {v0}, Lcom/a/b/j/k;->c(Z)V

    .line 428
    long-to-int v0, v2

    return v0

    .line 427
    :cond_10
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private static h(II)I
    .registers 7

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 451
    const-string v3, "exponent"

    invoke-static {v3, p1}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 452
    packed-switch p0, :pswitch_data_64

    move v2, v1

    .line 470
    :goto_c
    packed-switch p1, :pswitch_data_72

    .line 476
    and-int/lit8 v3, p1, 0x1

    if-eqz v3, :cond_62

    .line 477
    invoke-static {v2, p0}, Lcom/a/b/j/g;->b(II)I

    move-result v2

    move v4, v2

    .line 479
    :goto_18
    shr-int/lit8 p1, p1, 0x1

    .line 480
    if-lez p1, :cond_60

    .line 481
    const v2, -0xb504

    if-gt v2, p0, :cond_5c

    move v3, v1

    :goto_22
    const v2, 0xb504

    if-gt p0, v2, :cond_5e

    move v2, v1

    :goto_28
    and-int/2addr v2, v3

    invoke-static {v2}, Lcom/a/b/j/k;->c(Z)V

    .line 482
    mul-int/2addr p0, p0

    move v2, v4

    goto :goto_c

    .line 454
    :pswitch_2f
    if-nez p1, :cond_32

    .line 474
    :cond_31
    :goto_31
    :pswitch_31
    return v1

    :cond_32
    move v1, v0

    .line 454
    goto :goto_31

    .line 458
    :pswitch_34
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_31

    move v1, v2

    goto :goto_31

    .line 460
    :pswitch_3a
    const/16 v2, 0x1f

    if-ge p1, v2, :cond_3f

    move v0, v1

    :cond_3f
    invoke-static {v0}, Lcom/a/b/j/k;->c(Z)V

    .line 461
    shl-int/2addr v1, p1

    goto :goto_31

    .line 463
    :pswitch_44
    const/16 v3, 0x20

    if-ge p1, v3, :cond_49

    move v0, v1

    :cond_49
    invoke-static {v0}, Lcom/a/b/j/k;->c(Z)V

    .line 464
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_52

    shl-int/2addr v1, p1

    goto :goto_31

    :cond_52
    shl-int v1, v2, p1

    goto :goto_31

    :pswitch_55
    move v1, v2

    .line 472
    goto :goto_31

    .line 474
    :pswitch_57
    invoke-static {v2, p0}, Lcom/a/b/j/g;->b(II)I

    move-result v1

    goto :goto_31

    :cond_5c
    move v3, v0

    .line 481
    goto :goto_22

    :cond_5e
    move v2, v0

    goto :goto_28

    :cond_60
    move v2, v4

    goto :goto_c

    :cond_62
    move v4, v2

    goto :goto_18

    .line 452
    :pswitch_data_64
    .packed-switch -0x2
        :pswitch_44
        :pswitch_34
        :pswitch_2f
        :pswitch_31
        :pswitch_3a
    .end packed-switch

    .line 470
    :pswitch_data_72
    .packed-switch 0x0
        :pswitch_55
        :pswitch_57
    .end packed-switch
.end method

.method private static i(II)I
    .registers 8
    .annotation build Lcom/a/b/a/c;
        a = "need BigIntegerMath to adequately test"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 525
    const-string v0, "n"

    invoke-static {v0, p0}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 526
    const-string v0, "k"

    invoke-static {v0, p1}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 527
    if-gt p1, p0, :cond_38

    move v0, v1

    :goto_f
    const-string v3, "k (%s) > n (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 528
    shr-int/lit8 v0, p0, 0x1

    if-le p1, v0, :cond_29

    .line 529
    sub-int p1, p0, p1

    .line 531
    :cond_29
    sget-object v0, Lcom/a/b/j/g;->f:[I

    array-length v0, v0

    if-ge p1, v0, :cond_34

    sget-object v0, Lcom/a/b/j/g;->f:[I

    aget v0, v0, p1

    if-le p0, v0, :cond_3a

    .line 532
    :cond_34
    const v1, 0x7fffffff

    .line 545
    :goto_37
    :pswitch_37
    return v1

    :cond_38
    move v0, v2

    .line 527
    goto :goto_f

    .line 534
    :cond_3a
    packed-switch p1, :pswitch_data_50

    .line 540
    const-wide/16 v0, 0x1

    .line 541
    :goto_3f
    if-ge v2, p1, :cond_4e

    .line 542
    sub-int v3, p0, v2

    int-to-long v4, v3

    mul-long/2addr v0, v4

    .line 543
    add-int/lit8 v3, v2, 0x1

    int-to-long v4, v3

    div-long/2addr v0, v4

    .line 541
    add-int/lit8 v2, v2, 0x1

    goto :goto_3f

    :pswitch_4c
    move v1, p0

    .line 538
    goto :goto_37

    .line 545
    :cond_4e
    long-to-int v1, v0

    goto :goto_37

    .line 534
    :pswitch_data_50
    .packed-switch 0x0
        :pswitch_37
        :pswitch_4c
    .end packed-switch
.end method

.method private static j(II)I
    .registers 4

    .prologue
    .line 580
    and-int v0, p0, p1

    xor-int v1, p0, p1

    shr-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method
