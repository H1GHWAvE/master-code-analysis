.class public final Lcom/a/b/i/av;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private a:J

.field private b:J


# direct methods
.method private constructor <init>(Ljava/io/InputStream;)V
    .registers 4
    .param p1    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/a/b/i/av;->b:J

    .line 46
    return-void
.end method

.method private a()J
    .registers 3

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/a/b/i/av;->a:J

    return-wide v0
.end method


# virtual methods
.method public final declared-synchronized mark(I)V
    .registers 4

    .prologue
    .line 76
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/b/i/av;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 77
    iget-wide v0, p0, Lcom/a/b/i/av;->a:J

    iput-wide v0, p0, Lcom/a/b/i/av;->b:J
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 79
    monitor-exit p0

    return-void

    .line 76
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final read()I
    .registers 7

    .prologue
    .line 54
    iget-object v0, p0, Lcom/a/b/i/av;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 55
    const/4 v1, -0x1

    if-eq v0, v1, :cond_10

    .line 56
    iget-wide v2, p0, Lcom/a/b/i/av;->a:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/i/av;->a:J

    .line 58
    :cond_10
    return v0
.end method

.method public final read([BII)I
    .registers 10

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/b/i/av;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 63
    const/4 v1, -0x1

    if-eq v0, v1, :cond_f

    .line 64
    iget-wide v2, p0, Lcom/a/b/i/av;->a:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/i/av;->a:J

    .line 66
    :cond_f
    return v0
.end method

.method public final declared-synchronized reset()V
    .registers 5

    .prologue
    .line 82
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/b/i/av;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-nez v0, :cond_14

    .line 83
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Mark not supported"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_11

    .line 82
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85
    :cond_14
    :try_start_14
    iget-wide v0, p0, Lcom/a/b/i/av;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_24

    .line 86
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Mark not set"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_24
    iget-object v0, p0, Lcom/a/b/i/av;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 90
    iget-wide v0, p0, Lcom/a/b/i/av;->b:J

    iput-wide v0, p0, Lcom/a/b/i/av;->a:J
    :try_end_2d
    .catchall {:try_start_14 .. :try_end_2d} :catchall_11

    .line 91
    monitor-exit p0

    return-void
.end method

.method public final skip(J)J
    .registers 8

    .prologue
    .line 70
    iget-object v0, p0, Lcom/a/b/i/av;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 71
    iget-wide v2, p0, Lcom/a/b/i/av;->a:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/a/b/i/av;->a:J

    .line 72
    return-wide v0
.end method
