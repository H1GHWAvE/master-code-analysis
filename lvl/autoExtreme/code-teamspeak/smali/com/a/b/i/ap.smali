.class final Lcom/a/b/i/ap;
.super Ljava/io/Writer;
.source "SourceFile"


# static fields
.field private static final a:Lcom/a/b/i/ap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 189
    new-instance v0, Lcom/a/b/i/ap;

    invoke-direct {v0}, Lcom/a/b/i/ap;-><init>()V

    sput-object v0, Lcom/a/b/i/ap;->a:Lcom/a/b/i/ap;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    return-void
.end method

.method static synthetic a()Lcom/a/b/i/ap;
    .registers 1

    .prologue
    .line 187
    sget-object v0, Lcom/a/b/i/ap;->a:Lcom/a/b/i/ap;

    return-object v0
.end method


# virtual methods
.method public final append(C)Ljava/io/Writer;
    .registers 2

    .prologue
    .line 229
    return-object p0
.end method

.method public final append(Ljava/lang/CharSequence;)Ljava/io/Writer;
    .registers 2

    .prologue
    .line 217
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    return-object p0
.end method

.method public final append(Ljava/lang/CharSequence;II)Ljava/io/Writer;
    .registers 5

    .prologue
    .line 223
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-static {p2, p3, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 224
    return-object p0
.end method

.method public final bridge synthetic append(C)Ljava/lang/Appendable;
    .registers 3

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/a/b/i/ap;->append(C)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .registers 3

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/a/b/i/ap;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .registers 5

    .prologue
    .line 187
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/i/ap;->append(Ljava/lang/CharSequence;II)Ljava/io/Writer;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .registers 1

    .prologue
    .line 238
    return-void
.end method

.method public final flush()V
    .registers 1

    .prologue
    .line 234
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 242
    const-string v0, "CharStreams.nullWriter()"

    return-object v0
.end method

.method public final write(I)V
    .registers 2

    .prologue
    .line 193
    return-void
.end method

.method public final write(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 207
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    return-void
.end method

.method public final write(Ljava/lang/String;II)V
    .registers 6

    .prologue
    .line 212
    add-int v0, p2, p3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/a/b/b/cn;->a(III)V

    .line 213
    return-void
.end method

.method public final write([C)V
    .registers 2

    .prologue
    .line 197
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    return-void
.end method

.method public final write([CII)V
    .registers 6

    .prologue
    .line 202
    add-int v0, p2, p3

    array-length v1, p1

    invoke-static {p2, v0, v1}, Lcom/a/b/b/cn;->a(III)V

    .line 203
    return-void
.end method
