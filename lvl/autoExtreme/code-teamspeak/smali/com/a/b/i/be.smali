.class final Lcom/a/b/i/be;
.super Lcom/a/b/d/aga;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 807
    invoke-direct {p0}, Lcom/a/b/d/aga;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/File;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 811
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 812
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 813
    if-eqz v0, :cond_15

    .line 814
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 818
    :goto_14
    return-object v0

    :cond_15
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_14
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 807
    check-cast p1, Ljava/io/File;

    .line 1811
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1812
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 1813
    if-eqz v0, :cond_17

    .line 1814
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_16
    return-object v0

    .line 1818
    :cond_17
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_16
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 823
    const-string v0, "Files.fileTreeTraverser()"

    return-object v0
.end method
