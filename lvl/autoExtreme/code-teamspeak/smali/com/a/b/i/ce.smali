.class final Lcom/a/b/i/ce;
.super Ljava/io/Reader;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Iterator;

.field private b:Ljava/io/Reader;


# direct methods
.method constructor <init>(Ljava/util/Iterator;)V
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/io/Reader;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/a/b/i/ce;->a:Ljava/util/Iterator;

    .line 39
    invoke-direct {p0}, Lcom/a/b/i/ce;->a()V

    .line 40
    return-void
.end method

.method private a()V
    .registers 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/i/ce;->close()V

    .line 47
    iget-object v0, p0, Lcom/a/b/i/ce;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 48
    iget-object v0, p0, Lcom/a/b/i/ce;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/i/ah;

    invoke-virtual {v0}, Lcom/a/b/i/ah;->a()Ljava/io/Reader;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    .line 50
    :cond_19
    return-void
.end method


# virtual methods
.method public final close()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 83
    iget-object v0, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    if-eqz v0, :cond_c

    .line 85
    :try_start_5
    iget-object v0, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_d

    .line 87
    iput-object v1, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    .line 90
    :cond_c
    return-void

    .line 87
    :catchall_d
    move-exception v0

    iput-object v1, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    throw v0
.end method

.method public final read([CII)I
    .registers 6
    .param p1    # [C
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 53
    :goto_1
    iget-object v1, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    if-nez v1, :cond_6

    .line 61
    :goto_5
    return v0

    .line 56
    :cond_6
    iget-object v1, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/Reader;->read([CII)I

    move-result v1

    .line 57
    if-ne v1, v0, :cond_12

    .line 58
    invoke-direct {p0}, Lcom/a/b/i/ce;->a()V

    goto :goto_1

    :cond_12
    move v0, v1

    .line 61
    goto :goto_5
.end method

.method public final ready()Z
    .registers 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->ready()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final skip(J)J
    .registers 8

    .prologue
    const-wide/16 v2, 0x0

    .line 65
    cmp-long v0, p1, v2

    if-ltz v0, :cond_1f

    const/4 v0, 0x1

    :goto_7
    const-string v1, "n is negative"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 66
    cmp-long v0, p1, v2

    if-lez v0, :cond_25

    .line 67
    :goto_10
    iget-object v0, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    if-eqz v0, :cond_25

    .line 68
    iget-object v0, p0, Lcom/a/b/i/ce;->b:Ljava/io/Reader;

    invoke-virtual {v0, p1, p2}, Ljava/io/Reader;->skip(J)J

    move-result-wide v0

    .line 69
    cmp-long v4, v0, v2

    if-lez v4, :cond_21

    .line 75
    :goto_1e
    return-wide v0

    .line 65
    :cond_1f
    const/4 v0, 0x0

    goto :goto_7

    .line 72
    :cond_21
    invoke-direct {p0}, Lcom/a/b/i/ce;->a()V

    goto :goto_10

    :cond_25
    move-wide v0, v2

    .line 75
    goto :goto_1e
.end method
