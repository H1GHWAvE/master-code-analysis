.class public final Lcom/a/b/i/ch;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/net/URL;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;
    .registers 3

    .prologue
    .line 88
    invoke-static {p0}, Lcom/a/b/i/ch;->a(Ljava/net/URL;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/i/s;->a(Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/net/URL;)Lcom/a/b/i/s;
    .registers 3

    .prologue
    .line 56
    new-instance v0, Lcom/a/b/i/cj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/i/cj;-><init>(Ljava/net/URL;B)V

    return-object v0
.end method

.method private static a(Ljava/net/URL;Ljava/nio/charset/Charset;Lcom/a/b/i/by;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 129
    invoke-static {p0, p1}, Lcom/a/b/i/ch;->a(Ljava/net/URL;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/a/b/i/ah;->a(Lcom/a/b/i/by;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;)Ljava/net/URL;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 208
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 209
    if-eqz v3, :cond_1a

    move v0, v1

    :goto_9
    const-string v4, "resource %s relative to %s not found."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v2

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v0, v4, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 211
    return-object v3

    :cond_1a
    move v0, v2

    .line 209
    goto :goto_9
.end method

.method private static a(Ljava/lang/String;)Ljava/net/URL;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 193
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-class v3, Lcom/a/b/i/ch;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ClassLoader;

    .line 196
    invoke-virtual {v0, p0}, Ljava/lang/ClassLoader;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 197
    if-eqz v3, :cond_27

    move v0, v1

    :goto_1d
    const-string v4, "resource %s not found."

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v4, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 198
    return-object v3

    :cond_27
    move v0, v2

    .line 197
    goto :goto_1d
.end method

.method private static a(Ljava/net/URL;Ljava/io/OutputStream;)V
    .registers 3

    .prologue
    .line 175
    invoke-static {p0}, Lcom/a/b/i/ch;->a(Ljava/net/URL;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/i/s;->a(Ljava/io/OutputStream;)J

    .line 176
    return-void
.end method

.method private static b(Ljava/net/URL;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 113
    invoke-static {p0, p1}, Lcom/a/b/i/ch;->a(Ljava/net/URL;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/i/ah;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/net/URL;)[B
    .registers 2

    .prologue
    .line 99
    invoke-static {p0}, Lcom/a/b/i/ch;->a(Ljava/net/URL;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/i/s;->e()[B

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/net/URL;Ljava/nio/charset/Charset;)Ljava/util/List;
    .registers 4

    .prologue
    .line 151
    new-instance v0, Lcom/a/b/i/ci;

    invoke-direct {v0}, Lcom/a/b/i/ci;-><init>()V

    .line 1129
    invoke-static {p0, p1}, Lcom/a/b/i/ch;->a(Ljava/net/URL;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/a/b/i/ah;->a(Lcom/a/b/i/by;)Ljava/lang/Object;

    move-result-object v0

    .line 151
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
