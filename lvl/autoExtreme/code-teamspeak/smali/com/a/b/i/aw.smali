.class public final Lcom/a/b/i/aw;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private a:J


# direct methods
.method private constructor <init>(Ljava/io/OutputStream;)V
    .registers 2
    .param p1    # Ljava/io/OutputStream;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 44
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 45
    return-void
.end method

.method private a()J
    .registers 3

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/a/b/i/aw;->a:J

    return-wide v0
.end method


# virtual methods
.method public final close()V
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/a/b/i/aw;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 67
    return-void
.end method

.method public final write(I)V
    .registers 6

    .prologue
    .line 58
    iget-object v0, p0, Lcom/a/b/i/aw;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 59
    iget-wide v0, p0, Lcom/a/b/i/aw;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/i/aw;->a:J

    .line 60
    return-void
.end method

.method public final write([BII)V
    .registers 8

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/b/i/aw;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 54
    iget-wide v0, p0, Lcom/a/b/i/aw;->a:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/i/aw;->a:J

    .line 55
    return-void
.end method
