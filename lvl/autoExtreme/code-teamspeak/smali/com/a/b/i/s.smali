.class public abstract Lcom/a/b/i/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I = 0x1000

.field private static final b:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 196
    const/16 v0, 0x1000

    new-array v0, v0, [B

    sput-object v0, Lcom/a/b/i/s;->b:[B

    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/InputStream;)J
    .registers 8

    .prologue
    const-wide/16 v2, 0x0

    .line 176
    move-wide v0, v2

    .line 180
    :goto_3
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v4

    const v5, 0x7fffffff

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {p0, v4, v5}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v4

    .line 181
    cmp-long v6, v4, v2

    if-gtz v6, :cond_33

    .line 182
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1f

    .line 183
    return-wide v0

    .line 184
    :cond_1f
    cmp-long v4, v0, v2

    if-nez v4, :cond_2f

    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v4

    if-nez v4, :cond_2f

    .line 187
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 189
    :cond_2f
    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    goto :goto_3

    .line 191
    :cond_33
    add-long/2addr v0, v4

    .line 193
    goto :goto_3
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/i/s;
    .registers 2

    .prologue
    .line 346
    new-instance v0, Lcom/a/b/i/w;

    invoke-direct {v0, p0}, Lcom/a/b/i/w;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/i/s;
    .registers 2

    .prologue
    .line 368
    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/util/Iterator;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/i/s;->a(Ljava/lang/Iterable;)Lcom/a/b/i/s;

    move-result-object v0

    return-object v0
.end method

.method private static a([B)Lcom/a/b/i/s;
    .registers 2

    .prologue
    .line 394
    new-instance v0, Lcom/a/b/i/v;

    invoke-direct {v0, p0}, Lcom/a/b/i/v;-><init>([B)V

    return-object v0
.end method

.method private static varargs a([Lcom/a/b/i/s;)Lcom/a/b/i/s;
    .registers 2

    .prologue
    .line 384
    invoke-static {p0}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/i/s;->a(Ljava/lang/Iterable;)Lcom/a/b/i/s;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/io/InputStream;)J
    .registers 7

    .prologue
    .line 199
    const-wide/16 v0, 0x0

    .line 201
    :goto_2
    sget-object v2, Lcom/a/b/i/s;->b:[B

    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_11

    .line 202
    add-long/2addr v0, v2

    goto :goto_2

    .line 204
    :cond_11
    return-wide v0
.end method

.method private static f()Lcom/a/b/i/s;
    .registers 1

    .prologue
    .line 403
    invoke-static {}, Lcom/a/b/i/x;->f()Lcom/a/b/i/x;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/i/p;)J
    .registers 5

    .prologue
    .line 235
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v2

    .line 239
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 240
    invoke-virtual {p1}, Lcom/a/b/i/p;->a()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v1

    check-cast v1, Ljava/io/OutputStream;

    .line 241
    invoke-static {v0, v1}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_1e} :catch_23
    .catchall {:try_start_7 .. :try_end_1e} :catchall_29

    move-result-wide v0

    .line 245
    invoke-virtual {v2}, Lcom/a/b/i/ar;->close()V

    return-wide v0

    .line 242
    :catch_23
    move-exception v0

    .line 243
    :try_start_24
    invoke-virtual {v2, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_29
    .catchall {:try_start_24 .. :try_end_29} :catchall_29

    .line 245
    :catchall_29
    move-exception v0

    invoke-virtual {v2}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public a(Ljava/io/OutputStream;)J
    .registers 6

    .prologue
    .line 215
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 219
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 220
    invoke-static {v0, p1}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_14} :catch_19
    .catchall {:try_start_7 .. :try_end_14} :catchall_1f

    move-result-wide v2

    .line 224
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-wide v2

    .line 221
    :catch_19
    move-exception v0

    .line 222
    :try_start_1a
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1f
    .catchall {:try_start_1a .. :try_end_1f} :catchall_1f

    .line 224
    :catchall_1f
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public a(Lcom/a/b/g/ak;)Lcom/a/b/g/ag;
    .registers 4

    .prologue
    .line 296
    invoke-interface {p1}, Lcom/a/b/g/ak;->a()Lcom/a/b/g/al;

    move-result-object v0

    .line 1227
    new-instance v1, Lcom/a/b/g/ac;

    invoke-direct {v1, v0}, Lcom/a/b/g/ac;-><init>(Lcom/a/b/g/bn;)V

    .line 297
    invoke-virtual {p0, v1}, Lcom/a/b/i/s;->a(Ljava/io/OutputStream;)J

    .line 298
    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;
    .registers 4

    .prologue
    .line 73
    new-instance v0, Lcom/a/b/i/u;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/i/u;-><init>(Lcom/a/b/i/s;Ljava/nio/charset/Charset;B)V

    return-object v0
.end method

.method public a(JJ)Lcom/a/b/i/s;
    .registers 12

    .prologue
    .line 112
    new-instance v0, Lcom/a/b/i/y;

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/a/b/i/y;-><init>(Lcom/a/b/i/s;JJB)V

    return-object v0
.end method

.method public abstract a()Ljava/io/InputStream;
.end method

.method public a(Lcom/a/b/i/o;)Ljava/lang/Object;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 277
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 281
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 282
    invoke-static {v0, p1}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;Lcom/a/b/i/o;)Ljava/lang/Object;
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_14} :catch_19
    .catchall {:try_start_7 .. :try_end_14} :catchall_1f

    move-result-object v0

    .line 286
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-object v0

    .line 283
    :catch_19
    move-exception v0

    .line 284
    :try_start_1a
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1f
    .catchall {:try_start_1a .. :try_end_1f} :catchall_1f

    .line 286
    :catchall_1f
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public final a(Lcom/a/b/i/s;)Z
    .registers 12

    .prologue
    const/4 v2, 0x0

    const/16 v9, 0x1000

    .line 309
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    new-array v3, v9, [B

    .line 312
    new-array v4, v9, [B

    .line 314
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v5

    .line 316
    :try_start_e
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 317
    invoke-virtual {p1}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v1

    check-cast v1, Ljava/io/InputStream;

    .line 319
    :cond_22
    const/4 v6, 0x0

    const/16 v7, 0x1000

    invoke-static {v0, v3, v6, v7}, Lcom/a/b/i/z;->b(Ljava/io/InputStream;[BII)I

    move-result v6

    .line 320
    const/4 v7, 0x0

    const/16 v8, 0x1000

    invoke-static {v1, v4, v7, v8}, Lcom/a/b/i/z;->b(Ljava/io/InputStream;[BII)I

    move-result v7

    .line 321
    if-ne v6, v7, :cond_38

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_35} :catch_44
    .catchall {:try_start_e .. :try_end_35} :catchall_4a

    move-result v7

    if-nez v7, :cond_3d

    .line 330
    :cond_38
    invoke-virtual {v5}, Lcom/a/b/i/ar;->close()V

    move v0, v2

    :goto_3c
    return v0

    .line 323
    :cond_3d
    if-eq v6, v9, :cond_22

    .line 330
    invoke-virtual {v5}, Lcom/a/b/i/ar;->close()V

    const/4 v0, 0x1

    goto :goto_3c

    .line 327
    :catch_44
    move-exception v0

    .line 328
    :try_start_45
    invoke-virtual {v5, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_4a
    .catchall {:try_start_45 .. :try_end_4a} :catchall_4a

    .line 330
    :catchall_4a
    move-exception v0

    invoke-virtual {v5}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public b()Ljava/io/InputStream;
    .registers 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 100
    instance-of v1, v0, Ljava/io/BufferedInputStream;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/io/BufferedInputStream;

    :goto_a
    return-object v0

    :cond_b
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v0, v1

    goto :goto_a
.end method

.method public c()Z
    .registers 4

    .prologue
    .line 123
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 125
    :try_start_4
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 126
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_11} :catch_1c
    .catchall {:try_start_4 .. :try_end_11} :catchall_22

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1a

    const/4 v0, 0x1

    .line 130
    :goto_16
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return v0

    .line 126
    :cond_1a
    const/4 v0, 0x0

    goto :goto_16

    .line 127
    :catch_1c
    move-exception v0

    .line 128
    :try_start_1d
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_22

    .line 130
    :catchall_22
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public d()J
    .registers 10

    .prologue
    const-wide/16 v4, 0x0

    .line 150
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 152
    :try_start_6
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    move-wide v2, v4

    .line 1180
    :goto_11
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v6

    const v7, 0x7fffffff

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v0, v6, v7}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v6

    .line 1181
    cmp-long v8, v6, v4

    if-gtz v8, :cond_5f

    .line 1182
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_28} :catch_41
    .catchall {:try_start_6 .. :try_end_28} :catchall_61

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_31

    .line 157
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    move-wide v0, v2

    .line 167
    :goto_30
    return-wide v0

    .line 1184
    :cond_31
    cmp-long v6, v2, v4

    if-nez v6, :cond_5b

    :try_start_35
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v6

    if-nez v6, :cond_5b

    .line 1187
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_41} :catch_41
    .catchall {:try_start_35 .. :try_end_41} :catchall_61

    .line 157
    :catch_41
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    .line 160
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v2

    .line 162
    :try_start_49
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 163
    invoke-static {v0}, Lcom/a/b/i/s;->b(Ljava/io/InputStream;)J
    :try_end_56
    .catch Ljava/lang/Throwable; {:try_start_49 .. :try_end_56} :catch_66
    .catchall {:try_start_49 .. :try_end_56} :catchall_6c

    move-result-wide v0

    .line 167
    invoke-virtual {v2}, Lcom/a/b/i/ar;->close()V

    goto :goto_30

    .line 1189
    :cond_5b
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    goto :goto_11

    .line 1191
    :cond_5f
    add-long/2addr v2, v6

    .line 1193
    goto :goto_11

    .line 157
    :catchall_61
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0

    .line 164
    :catch_66
    move-exception v0

    .line 165
    :try_start_67
    invoke-virtual {v2, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_6c
    .catchall {:try_start_67 .. :try_end_6c} :catchall_6c

    .line 167
    :catchall_6c
    move-exception v0

    invoke-virtual {v2}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method public e()[B
    .registers 3

    .prologue
    .line 255
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 257
    :try_start_4
    invoke-virtual {p0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 258
    invoke-static {v0}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;)[B
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_11} :catch_16
    .catchall {:try_start_4 .. :try_end_11} :catchall_1c

    move-result-object v0

    .line 262
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-object v0

    .line 259
    :catch_16
    move-exception v0

    .line 260
    :try_start_17
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1c
    .catchall {:try_start_17 .. :try_end_1c} :catchall_1c

    .line 262
    :catchall_1c
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method
