.class public abstract Lcom/a/b/i/p;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/io/InputStream;)J
    .registers 6

    .prologue
    .line 119
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 123
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/p;->a()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 124
    invoke-static {p1, v0}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    move-result-wide v2

    .line 125
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_18
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_18} :catch_1c
    .catchall {:try_start_7 .. :try_end_18} :catchall_22

    .line 130
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-wide v2

    .line 127
    :catch_1c
    move-exception v0

    .line 128
    :try_start_1d
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_22

    .line 130
    :catchall_22
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method private a(Ljava/nio/charset/Charset;)Lcom/a/b/i/ag;
    .registers 4

    .prologue
    .line 59
    new-instance v0, Lcom/a/b/i/r;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/i/r;-><init>(Lcom/a/b/i/p;Ljava/nio/charset/Charset;B)V

    return-object v0
.end method

.method private a([B)V
    .registers 4

    .prologue
    .line 97
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 101
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/i/p;->a()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 102
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 103
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_17} :catch_1b
    .catchall {:try_start_7 .. :try_end_17} :catchall_21

    .line 107
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    .line 108
    return-void

    .line 104
    :catch_1b
    move-exception v0

    .line 105
    :try_start_1c
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_21

    .line 107
    :catchall_21
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method private b()Ljava/io/OutputStream;
    .registers 3

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/a/b/i/p;->a()Ljava/io/OutputStream;

    move-result-object v0

    .line 86
    instance-of v1, v0, Ljava/io/BufferedOutputStream;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/io/BufferedOutputStream;

    :goto_a
    return-object v0

    :cond_b
    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v0, v1

    goto :goto_a
.end method


# virtual methods
.method public abstract a()Ljava/io/OutputStream;
.end method
