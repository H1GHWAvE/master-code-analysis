.class public final Lcom/a/b/d/afb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/yq;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation


# static fields
.field private static final b:Lcom/a/b/d/yq;


# instance fields
.field private final a:Ljava/util/NavigableMap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 262
    new-instance v0, Lcom/a/b/d/afc;

    invoke-direct {v0}, Lcom/a/b/d/afc;-><init>()V

    sput-object v0, Lcom/a/b/d/afb;->b:Lcom/a/b/d/yq;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Lcom/a/b/d/sz;->e()Ljava/util/TreeMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    return-object v0
.end method

.method private a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 158
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    new-instance v1, Lcom/a/b/d/aff;

    invoke-direct {v1, p1, p2, p3}, Lcom/a/b/d/aff;-><init>(Lcom/a/b/d/dw;Lcom/a/b/d/dw;Ljava/lang/Object;)V

    invoke-interface {v0, p1, v1}, Ljava/util/NavigableMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    return-void
.end method

.method public static c()Lcom/a/b/d/afb;
    .registers 1

    .prologue
    .line 61
    new-instance v0, Lcom/a/b/d/afb;

    invoke-direct {v0}, Lcom/a/b/d/afb;-><init>()V

    return-object v0
.end method

.method static synthetic e()Lcom/a/b/d/yq;
    .registers 1

    .prologue
    .line 7259
    sget-object v0, Lcom/a/b/d/afb;->b:Lcom/a/b/d/yq;

    .line 56
    return-object v0
.end method

.method private static f()Lcom/a/b/d/yq;
    .registers 1

    .prologue
    .line 259
    sget-object v0, Lcom/a/b/d/afb;->b:Lcom/a/b/d/yq;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    invoke-interface {v1}, Ljava/util/NavigableMap;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v1

    .line 149
    if-nez v0, :cond_14

    .line 150
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 152
    :cond_14
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 2084
    iget-object v0, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    .line 152
    iget-object v2, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 3084
    iget-object v0, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    .line 152
    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v2, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Comparable;)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/a/b/d/afb;->b(Ljava/lang/Comparable;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 109
    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_7
.end method

.method public final a(Lcom/a/b/d/yl;)V
    .registers 7

    .prologue
    .line 163
    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 204
    :goto_6
    return-void

    .line 171
    :cond_7
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 173
    if-eqz v2, :cond_55

    .line 175
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 3101
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 176
    iget-object v3, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1, v3}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-lez v1, :cond_55

    .line 4101
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 178
    iget-object v3, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1, v3}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v1

    if-lez v1, :cond_42

    .line 181
    iget-object v3, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 5101
    iget-object v1, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v4, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 181
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/aff;

    invoke-virtual {v1}, Lcom/a/b/d/aff;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v3, v4, v1}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;Ljava/lang/Object;)V

    .line 6097
    :cond_42
    iget-object v0, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v1, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    .line 185
    iget-object v3, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    invoke-virtual {v0}, Lcom/a/b/d/aff;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v1, v3, v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;Ljava/lang/Object;)V

    .line 190
    :cond_55
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 192
    if-eqz v1, :cond_8b

    .line 194
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 6101
    iget-object v2, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v2, v2, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 195
    iget-object v3, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v2, v3}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v2

    if-lez v2, :cond_8b

    .line 198
    iget-object v2, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 7101
    iget-object v0, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    iget-object v3, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    .line 198
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    invoke-virtual {v0}, Lcom/a/b/d/aff;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v2, v3, v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;Ljava/lang/Object;)V

    .line 200
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    :cond_8b
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v2, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    goto/16 :goto_6
.end method

.method public final a(Lcom/a/b/d/yl;Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-nez v0, :cond_18

    .line 127
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-virtual {p0, p1}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;)V

    .line 129
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    new-instance v2, Lcom/a/b/d/aff;

    invoke-direct {v2, p1, p2}, Lcom/a/b/d/aff;-><init>(Lcom/a/b/d/yl;Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :cond_18
    return-void
.end method

.method public final a(Lcom/a/b/d/yq;)V
    .registers 5

    .prologue
    .line 135
    invoke-interface {p1}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 136
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/yl;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;Ljava/lang/Object;)V

    goto :goto_c

    .line 138
    :cond_26
    return-void
.end method

.method public final b(Ljava/lang/Comparable;)Ljava/util/Map$Entry;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    invoke-static {p1}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 117
    if-eqz v1, :cond_21

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 1093
    iget-object v0, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    .line 117
    if-eqz v0, :cond_21

    .line 118
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 120
    :goto_20
    return-object v0

    :cond_21
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->clear()V

    .line 143
    return-void
.end method

.method public final c(Lcom/a/b/d/yl;)Lcom/a/b/d/yq;
    .registers 3

    .prologue
    .line 250
    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 253
    :goto_a
    return-object p0

    :cond_b
    new-instance v0, Lcom/a/b/d/afg;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/afg;-><init>(Lcom/a/b/d/afb;Lcom/a/b/d/yl;)V

    move-object p0, v0

    goto :goto_a
.end method

.method public final d()Ljava/util/Map;
    .registers 3

    .prologue
    .line 208
    new-instance v0, Lcom/a/b/d/afd;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/afd;-><init>(Lcom/a/b/d/afb;B)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591
    instance-of v0, p1, Lcom/a/b/d/yq;

    if-eqz v0, :cond_13

    .line 592
    check-cast p1, Lcom/a/b/d/yq;

    .line 593
    invoke-virtual {p0}, Lcom/a/b/d/afb;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 595
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/a/b/d/afb;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/a/b/d/afb;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
