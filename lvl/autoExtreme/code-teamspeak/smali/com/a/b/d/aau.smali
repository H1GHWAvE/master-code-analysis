.class final Lcom/a/b/d/aau;
.super Lcom/a/b/d/it;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# instance fields
.field final transient a:Ljava/lang/Object;

.field final transient b:Ljava/lang/Object;

.field transient c:Lcom/a/b/d/it;


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/a/b/d/it;-><init>()V

    .line 39
    invoke-static {p1, p2}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 40
    iput-object p1, p0, Lcom/a/b/d/aau;->a:Ljava/lang/Object;

    .line 41
    iput-object p2, p0, Lcom/a/b/d/aau;->b:Ljava/lang/Object;

    .line 42
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/it;)V
    .registers 4

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/a/b/d/it;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/a/b/d/aau;->a:Ljava/lang/Object;

    .line 47
    iput-object p2, p0, Lcom/a/b/d/aau;->b:Ljava/lang/Object;

    .line 48
    iput-object p3, p0, Lcom/a/b/d/aau;->c:Lcom/a/b/d/it;

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/util/Map$Entry;)V
    .registers 4

    .prologue
    .line 52
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/aau;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 53
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/d/it;
    .registers 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/a/b/d/aau;->c:Lcom/a/b/d/it;

    .line 92
    if-nez v0, :cond_f

    .line 93
    new-instance v0, Lcom/a/b/d/aau;

    iget-object v1, p0, Lcom/a/b/d/aau;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/b/d/aau;->a:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p0}, Lcom/a/b/d/aau;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/it;)V

    iput-object v0, p0, Lcom/a/b/d/aau;->c:Lcom/a/b/d/it;

    .line 96
    :cond_f
    return-object v0
.end method

.method public final synthetic b()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/a/b/d/aau;->a()Lcom/a/b/d/it;

    move-result-object v0

    return-object v0
.end method

.method final c()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/a/b/d/aau;->a:Ljava/lang/Object;

    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/d/aau;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/b/d/aau;->b:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final d()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/a/b/d/aau;->a:Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/b/d/aau;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 56
    iget-object v0, p0, Lcom/a/b/d/aau;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/a/b/d/aau;->b:Ljava/lang/Object;

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method final i_()Z
    .registers 2

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method
