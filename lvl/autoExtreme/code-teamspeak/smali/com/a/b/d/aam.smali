.class Lcom/a/b/d/aam;
.super Lcom/a/b/d/aal;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# direct methods
.method constructor <init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
    .registers 3

    .prologue
    .line 833
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/aal;-><init>(Ljava/util/Set;Lcom/a/b/b/co;)V

    .line 834
    return-void
.end method


# virtual methods
.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 838
    iget-object v0, p0, Lcom/a/b/d/aam;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 859
    invoke-virtual {p0}, Lcom/a/b/d/aam;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 849
    new-instance v1, Lcom/a/b/d/aam;

    iget-object v0, p0, Lcom/a/b/d/aam;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/aam;->b:Lcom/a/b/b/co;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    return-object v1
.end method

.method public last()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 864
    iget-object v0, p0, Lcom/a/b/d/aam;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    .line 866
    :goto_4
    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v1

    .line 867
    iget-object v2, p0, Lcom/a/b/d/aam;->b:Lcom/a/b/b/co;

    invoke-interface {v2, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 868
    return-object v1

    .line 870
    :cond_11
    invoke-interface {v0, v1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    goto :goto_4
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 6

    .prologue
    .line 843
    new-instance v1, Lcom/a/b/d/aam;

    iget-object v0, p0, Lcom/a/b/d/aam;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/aam;->b:Lcom/a/b/b/co;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    return-object v1
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 854
    new-instance v1, Lcom/a/b/d/aam;

    iget-object v0, p0, Lcom/a/b/d/aam;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/aam;->b:Lcom/a/b/b/co;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    return-object v1
.end method
