.class abstract Lcom/a/b/d/wm;
.super Ljava/util/AbstractCollection;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1639
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lcom/a/b/d/vi;
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 1664
    invoke-virtual {p0}, Lcom/a/b/d/wm;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->g()V

    .line 1665
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1648
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_17

    .line 1649
    check-cast p1, Ljava/util/Map$Entry;

    .line 1650
    invoke-virtual {p0}, Lcom/a/b/d/wm;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/a/b/d/vi;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 1652
    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1656
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_17

    .line 1657
    check-cast p1, Ljava/util/Map$Entry;

    .line 1658
    invoke-virtual {p0}, Lcom/a/b/d/wm;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 1660
    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public size()I
    .registers 2

    .prologue
    .line 1644
    invoke-virtual {p0}, Lcom/a/b/d/wm;->a()Lcom/a/b/d/vi;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/vi;->f()I

    move-result v0

    return v0
.end method
