.class final Lcom/a/b/d/acu;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/d/aac;)Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 729
    instance-of v0, p0, Lcom/a/b/d/adr;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/lr;

    if-eqz v0, :cond_9

    .line 733
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/adr;

    invoke-direct {v0, p0}, Lcom/a/b/d/adr;-><init>(Lcom/a/b/d/aac;)V

    move-object p0, v0

    goto :goto_8
.end method

.method private static a(Lcom/a/b/d/abs;)Lcom/a/b/d/abs;
    .registers 2

    .prologue
    .line 776
    instance-of v0, p0, Lcom/a/b/d/adu;

    if-eqz v0, :cond_5

    .line 779
    :goto_4
    return-object p0

    :cond_5
    new-instance v0, Lcom/a/b/d/adu;

    invoke-direct {v0, p0}, Lcom/a/b/d/adu;-><init>(Lcom/a/b/d/abs;)V

    move-object p0, v0

    goto :goto_4
.end method

.method private static a(Lcom/a/b/d/bw;)Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 1104
    instance-of v0, p0, Lcom/a/b/d/adc;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/it;

    if-eqz v0, :cond_9

    .line 1108
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/adc;

    invoke-direct {v0, p0}, Lcom/a/b/d/adc;-><init>(Lcom/a/b/d/bw;)V

    move-object p0, v0

    goto :goto_8
.end method

.method private static a(Lcom/a/b/d/ou;)Lcom/a/b/d/ou;
    .registers 2

    .prologue
    .line 692
    instance-of v0, p0, Lcom/a/b/d/adh;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/jr;

    if-eqz v0, :cond_9

    .line 696
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/adh;

    invoke-direct {v0, p0}, Lcom/a/b/d/adh;-><init>(Lcom/a/b/d/ou;)V

    move-object p0, v0

    goto :goto_8
.end method

.method private static a(Lcom/a/b/d/vi;)Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 507
    instance-of v0, p0, Lcom/a/b/d/adj;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/kk;

    if-eqz v0, :cond_9

    .line 511
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/adj;

    invoke-direct {v0, p0}, Lcom/a/b/d/adj;-><init>(Lcom/a/b/d/vi;)V

    move-object p0, v0

    goto :goto_8
.end method

.method private static a(Lcom/a/b/d/xc;Ljava/lang/Object;)Lcom/a/b/d/xc;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 412
    instance-of v0, p0, Lcom/a/b/d/adk;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/ku;

    if-eqz v0, :cond_9

    .line 416
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/adk;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adk;-><init>(Lcom/a/b/d/xc;Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_8
.end method

.method static a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 102
    new-instance v0, Lcom/a/b/d/add;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/d/add;-><init>(Ljava/util/Collection;Ljava/lang/Object;B)V

    return-object v0
.end method

.method private static a(Ljava/util/Deque;)Ljava/util/Deque;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "Deque"
    .end annotation

    .prologue
    .line 1625
    new-instance v0, Lcom/a/b/d/ade;

    invoke-direct {v0, p0}, Lcom/a/b/d/ade;-><init>(Ljava/util/Deque;)V

    return-object v0
.end method

.method static a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 303
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/a/b/d/adp;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adp;-><init>(Ljava/util/List;Ljava/lang/Object;)V

    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Lcom/a/b/d/adg;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adg;-><init>(Ljava/util/List;Ljava/lang/Object;)V

    goto :goto_9
.end method

.method static synthetic a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 60
    .line 3516
    if-nez p0, :cond_4

    .line 3517
    const/4 v0, 0x0

    :goto_3
    return-object v0

    .line 3519
    :cond_4
    new-instance v0, Lcom/a/b/d/adf;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adf;-><init>(Ljava/util/Map$Entry;Ljava/lang/Object;)V

    goto :goto_3
.end method

.method private static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/util/Map;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 919
    new-instance v0, Lcom/a/b/d/adi;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adi;-><init>(Ljava/util/Map;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Ljava/util/NavigableMap;)Ljava/util/NavigableMap;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 1341
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 1347
    new-instance v0, Lcom/a/b/d/adl;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adl;-><init>(Ljava/util/NavigableMap;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 1335
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 1330
    new-instance v0, Lcom/a/b/d/adm;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adm;-><init>(Ljava/util/NavigableSet;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Ljava/util/Queue;)Ljava/util/Queue;
    .registers 2

    .prologue
    .line 1569
    instance-of v0, p0, Lcom/a/b/d/ado;

    if-eqz v0, :cond_5

    :goto_4
    return-object p0

    :cond_5
    new-instance v0, Lcom/a/b/d/ado;

    invoke-direct {v0, p0}, Lcom/a/b/d/ado;-><init>(Ljava/util/Queue;)V

    move-object p0, v0

    goto :goto_4
.end method

.method static a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 210
    new-instance v0, Lcom/a/b/d/adq;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adq;-><init>(Ljava/util/Set;Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Ljava/util/SortedMap;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1050
    new-instance v0, Lcom/a/b/d/ads;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ads;-><init>(Ljava/util/SortedMap;Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 244
    new-instance v0, Lcom/a/b/d/adt;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adt;-><init>(Ljava/util/SortedSet;Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 60
    .line 2818
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 2819
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 2825
    :goto_a
    return-object v0

    .line 2821
    :cond_b
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_16

    .line 2822
    check-cast p0, Ljava/util/Set;

    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a

    .line 2824
    :cond_16
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_21

    .line 2825
    check-cast p0, Ljava/util/List;

    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_a

    .line 2827
    :cond_21
    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_a
.end method

.method private static synthetic b(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 60
    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3
    .param p0    # Ljava/util/Map$Entry;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/c;
        a = "works but is needed only for NavigableMap"
    .end annotation

    .prologue
    .line 1516
    if-nez p0, :cond_4

    .line 1517
    const/4 v0, 0x0

    .line 1519
    :goto_3
    return-object v0

    :cond_4
    new-instance v0, Lcom/a/b/d/adf;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/adf;-><init>(Ljava/util/Map$Entry;Ljava/lang/Object;)V

    goto :goto_3
.end method

.method static synthetic b(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 60
    .line 1832
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 1833
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    :goto_a
    return-object v0

    .line 1835
    :cond_b
    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a
.end method

.method private static synthetic b(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 60
    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 818
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 819
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 827
    :goto_a
    return-object v0

    .line 821
    :cond_b
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_16

    .line 822
    check-cast p0, Ljava/util/Set;

    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a

    .line 824
    :cond_16
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_21

    .line 825
    check-cast p0, Ljava/util/List;

    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_a

    .line 827
    :cond_21
    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_a
.end method

.method private static c(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 832
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 833
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 835
    :goto_a
    return-object v0

    :cond_b
    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a
.end method

.method private static synthetic d(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 60
    invoke-static {p0, p1}, Lcom/a/b/d/acu;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
