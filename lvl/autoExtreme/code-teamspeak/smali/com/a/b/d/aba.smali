.class final Lcom/a/b/d/aba;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I
    .registers 11
    .param p2    # Ljava/lang/Comparable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 205
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I
    .registers 7
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228
    invoke-static {p0, p1}, Lcom/a/b/d/ov;->a(Ljava/util/List;Lcom/a/b/b/bj;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p2, p3, p4, p5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/List;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I
    .registers 6

    .prologue
    .line 191
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-static {p0, v0, v1, p2, p3}, Lcom/a/b/d/aba;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I
    .registers 9
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 258
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    invoke-static {p4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-nez v0, :cond_14

    .line 263
    invoke-static {p0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p0

    .line 267
    :cond_14
    const/4 v1, 0x0

    .line 268
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 270
    :goto_1b
    if-gt v1, v0, :cond_40

    .line 271
    add-int v2, v1, v0

    ushr-int/lit8 v2, v2, 0x1

    .line 272
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p2, p1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 273
    if-gez v3, :cond_2e

    .line 274
    add-int/lit8 v0, v2, -0x1

    goto :goto_1b

    .line 275
    :cond_2e
    if-lez v3, :cond_33

    .line 276
    add-int/lit8 v1, v2, 0x1

    goto :goto_1b

    .line 278
    :cond_33
    add-int/lit8 v0, v0, 0x1

    invoke-interface {p0, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    sub-int/2addr v2, v1

    invoke-virtual {p3, p2, p1, v0, v2}, Lcom/a/b/d/abg;->a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I

    move-result v0

    add-int/2addr v0, v1

    .line 282
    :goto_3f
    return v0

    :cond_40
    invoke-virtual {p4, v1}, Lcom/a/b/d/abc;->a(I)I

    move-result v0

    goto :goto_3f
.end method
