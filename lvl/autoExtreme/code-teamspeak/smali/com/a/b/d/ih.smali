.class final Lcom/a/b/d/ih;
.super Lcom/a/b/d/am;
.source "SourceFile"


# instance fields
.field a:Lcom/a/b/d/ia;

.field final synthetic b:Lcom/a/b/d/ig;


# direct methods
.method constructor <init>(Lcom/a/b/d/ig;Lcom/a/b/d/ia;)V
    .registers 3

    .prologue
    .line 594
    iput-object p1, p0, Lcom/a/b/d/ih;->b:Lcom/a/b/d/ig;

    invoke-direct {p0}, Lcom/a/b/d/am;-><init>()V

    .line 595
    iput-object p2, p0, Lcom/a/b/d/ih;->a:Lcom/a/b/d/ia;

    .line 596
    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 600
    iget-object v0, p0, Lcom/a/b/d/ih;->a:Lcom/a/b/d/ia;

    iget-object v0, v0, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/a/b/d/ih;->a:Lcom/a/b/d/ia;

    iget-object v0, v0, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 610
    iget-object v0, p0, Lcom/a/b/d/ih;->a:Lcom/a/b/d/ia;

    iget-object v3, v0, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    .line 611
    invoke-static {p1}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;)I

    move-result v4

    .line 612
    iget-object v0, p0, Lcom/a/b/d/ih;->a:Lcom/a/b/d/ia;

    iget v0, v0, Lcom/a/b/d/ia;->a:I

    if-ne v4, v0, :cond_17

    invoke-static {p1, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 623
    :goto_16
    return-object p1

    .line 615
    :cond_17
    iget-object v0, p0, Lcom/a/b/d/ih;->b:Lcom/a/b/d/ig;

    iget-object v0, v0, Lcom/a/b/d/ig;->a:Lcom/a/b/d/if;

    iget-object v0, v0, Lcom/a/b/d/if;->a:Lcom/a/b/d/ie;

    iget-object v0, v0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    invoke-static {v0, p1, v4}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    if-nez v0, :cond_66

    move v0, v1

    :goto_26
    const-string v5, "value already present: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v5, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 616
    iget-object v0, p0, Lcom/a/b/d/ih;->b:Lcom/a/b/d/ig;

    iget-object v0, v0, Lcom/a/b/d/ig;->a:Lcom/a/b/d/if;

    iget-object v0, v0, Lcom/a/b/d/if;->a:Lcom/a/b/d/ie;

    iget-object v0, v0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    iget-object v1, p0, Lcom/a/b/d/ih;->a:Lcom/a/b/d/ia;

    invoke-static {v0, v1}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V

    .line 617
    new-instance v0, Lcom/a/b/d/ia;

    iget-object v1, p0, Lcom/a/b/d/ih;->a:Lcom/a/b/d/ia;

    iget-object v1, v1, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/b/d/ih;->a:Lcom/a/b/d/ia;

    iget v2, v2, Lcom/a/b/d/ia;->b:I

    invoke-direct {v0, p1, v4, v1, v2}, Lcom/a/b/d/ia;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 619
    iget-object v1, p0, Lcom/a/b/d/ih;->b:Lcom/a/b/d/ig;

    iget-object v1, v1, Lcom/a/b/d/ig;->a:Lcom/a/b/d/if;

    iget-object v1, v1, Lcom/a/b/d/if;->a:Lcom/a/b/d/ie;

    iget-object v1, v1, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    invoke-static {v1, v0}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V

    .line 620
    iget-object v0, p0, Lcom/a/b/d/ih;->b:Lcom/a/b/d/ig;

    iget-object v1, p0, Lcom/a/b/d/ih;->b:Lcom/a/b/d/ig;

    iget-object v1, v1, Lcom/a/b/d/ig;->a:Lcom/a/b/d/if;

    iget-object v1, v1, Lcom/a/b/d/if;->a:Lcom/a/b/d/ie;

    iget-object v1, v1, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    invoke-static {v1}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;)I

    move-result v1

    iput v1, v0, Lcom/a/b/d/ig;->e:I

    move-object p1, v3

    .line 623
    goto :goto_16

    :cond_66
    move v0, v2

    .line 615
    goto :goto_26
.end method
