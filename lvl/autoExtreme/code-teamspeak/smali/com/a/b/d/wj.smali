.class final Lcom/a/b/d/wj;
.super Lcom/a/b/d/n;
.source "SourceFile"


# static fields
.field private static final b:J
    .annotation build Lcom/a/b/a/c;
        a = "java serialization not supported"
    .end annotation
.end field


# instance fields
.field transient a:Lcom/a/b/b/dz;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/b/dz;)V
    .registers 4

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/a/b/d/n;-><init>(Ljava/util/Map;)V

    .line 122
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/d/wj;->a:Lcom/a/b/b/dz;

    .line 123
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 144
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 145
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/d/wj;->a:Lcom/a/b/b/dz;

    .line 146
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 147
    invoke-virtual {p0, v0}, Lcom/a/b/d/wj;->a(Ljava/util/Map;)V

    .line 148
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 136
    iget-object v0, p0, Lcom/a/b/d/wj;->a:Lcom/a/b/b/dz;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 137
    invoke-virtual {p0}, Lcom/a/b/d/wj;->e()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 138
    return-void
.end method


# virtual methods
.method protected final c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/a/b/d/wj;->a:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method
