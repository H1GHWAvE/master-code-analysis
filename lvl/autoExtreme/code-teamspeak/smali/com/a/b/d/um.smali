.class final Lcom/a/b/d/um;
.super Lcom/a/b/d/av;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation


# instance fields
.field private final a:Ljava/util/NavigableSet;

.field private final b:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 900
    invoke-direct {p0}, Lcom/a/b/d/av;-><init>()V

    .line 901
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    iput-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    .line 902
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/d/um;->b:Lcom/a/b/b/bj;

    .line 903
    return-void
.end method


# virtual methods
.method final a()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 945
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    iget-object v1, p0, Lcom/a/b/d/um;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Set;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method final b()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 950
    invoke-virtual {p0}, Lcom/a/b/d/um;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 940
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->clear()V

    .line 941
    return-void
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 923
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final descendingMap()Ljava/util/NavigableMap;
    .registers 3

    .prologue
    .line 965
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/um;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 929
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 932
    iget-object v0, p0, Lcom/a/b/d/um;->b:Lcom/a/b/b/bj;

    invoke-interface {v0, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 934
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 913
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/um;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final navigableKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 955
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 960
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->size()I

    move-result v0

    return v0
.end method

.method public final subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 908
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableSet;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/um;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 918
    iget-object v0, p0, Lcom/a/b/d/um;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/um;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method
