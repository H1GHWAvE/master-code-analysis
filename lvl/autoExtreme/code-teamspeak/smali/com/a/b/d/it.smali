.class public abstract Lcom/a/b/d/it;
.super Lcom/a/b/d/jt;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/bw;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final a:[Ljava/util/Map$Entry;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 214
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/util/Map$Entry;

    sput-object v0, Lcom/a/b/d/it;->a:[Ljava/util/Map$Entry;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/a/b/d/jt;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;
    .registers 8

    .prologue
    .line 66
    new-instance v0, Lcom/a/b/d/yx;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/a/b/d/kb;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/a/b/d/yx;-><init>([Lcom/a/b/d/kb;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;
    .registers 10

    .prologue
    .line 76
    new-instance v0, Lcom/a/b/d/yx;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/a/b/d/kb;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/a/b/d/yx;-><init>([Lcom/a/b/d/kb;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;
    .registers 12

    .prologue
    .line 86
    new-instance v0, Lcom/a/b/d/yx;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/a/b/d/kb;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p6, p7}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/a/b/d/yx;-><init>([Lcom/a/b/d/kb;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;
    .registers 14

    .prologue
    .line 97
    new-instance v0, Lcom/a/b/d/yx;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/a/b/d/kb;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p6, p7}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {p8, p9}, Lcom/a/b/d/it;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/a/b/d/yx;-><init>([Lcom/a/b/d/kb;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;
    .registers 3

    .prologue
    .line 57
    new-instance v0, Lcom/a/b/d/aau;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/aau;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static b(Ljava/util/Map;)Lcom/a/b/d/it;
    .registers 3

    .prologue
    .line 192
    instance-of v0, p0, Lcom/a/b/d/it;

    if-eqz v0, :cond_e

    move-object v0, p0

    .line 194
    check-cast v0, Lcom/a/b/d/it;

    .line 197
    invoke-virtual {v0}, Lcom/a/b/d/it;->i_()Z

    move-result v1

    if-nez v1, :cond_e

    .line 210
    :goto_d
    return-object v0

    .line 201
    :cond_e
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/a/b/d/it;->a:[Ljava/util/Map$Entry;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/Map$Entry;

    .line 202
    array-length v1, v0

    packed-switch v1, :pswitch_data_38

    .line 210
    new-instance v1, Lcom/a/b/d/yx;

    invoke-direct {v1, v0}, Lcom/a/b/d/yx;-><init>([Ljava/util/Map$Entry;)V

    move-object v0, v1

    goto :goto_d

    .line 1050
    :pswitch_25
    sget-object v0, Lcom/a/b/d/ew;->a:Lcom/a/b/d/ew;

    goto :goto_d

    .line 207
    :pswitch_28
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 208
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/a/b/d/it;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/it;

    move-result-object v0

    goto :goto_d

    .line 202
    :pswitch_data_38
    .packed-switch 0x0
        :pswitch_25
        :pswitch_28
    .end packed-switch
.end method

.method public static i()Lcom/a/b/d/it;
    .registers 1

    .prologue
    .line 50
    sget-object v0, Lcom/a/b/d/ew;->a:Lcom/a/b/d/ew;

    return-object v0
.end method

.method private static m()Lcom/a/b/d/iu;
    .registers 1

    .prologue
    .line 108
    new-instance v0, Lcom/a/b/d/iu;

    invoke-direct {v0}, Lcom/a/b/d/iu;-><init>()V

    return-object v0
.end method

.method private n()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/a/b/d/it;->a()Lcom/a/b/d/it;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/it;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a()Lcom/a/b/d/it;
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 244
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic b()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/a/b/d/it;->a()Lcom/a/b/d/it;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 40
    .line 1232
    invoke-virtual {p0}, Lcom/a/b/d/it;->a()Lcom/a/b/d/it;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/it;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method j()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 268
    new-instance v0, Lcom/a/b/d/iv;

    invoke-direct {v0, p0}, Lcom/a/b/d/iv;-><init>(Lcom/a/b/d/it;)V

    return-object v0
.end method

.method public final synthetic j_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 40
    .line 3232
    invoke-virtual {p0}, Lcom/a/b/d/it;->a()Lcom/a/b/d/it;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/it;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 40
    .line 2232
    invoke-virtual {p0}, Lcom/a/b/d/it;->a()Lcom/a/b/d/it;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/it;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 40
    return-object v0
.end method
