.class final Lcom/a/b/d/ns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Ljava/util/Iterator;

.field b:Ljava/util/Iterator;

.field final synthetic c:Ljava/lang/Iterable;


# direct methods
.method constructor <init>(Ljava/lang/Iterable;)V
    .registers 3

    .prologue
    .line 394
    iput-object p1, p0, Lcom/a/b/d/ns;->c:Ljava/lang/Iterable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ns;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/a/b/d/ns;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 401
    iget-object v0, p0, Lcom/a/b/d/ns;->c:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ns;->a:Ljava/util/Iterator;

    .line 403
    :cond_10
    iget-object v0, p0, Lcom/a/b/d/ns;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/a/b/d/ns;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 408
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 410
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/ns;->a:Ljava/util/Iterator;

    iput-object v0, p0, Lcom/a/b/d/ns;->b:Ljava/util/Iterator;

    .line 411
    iget-object v0, p0, Lcom/a/b/d/ns;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 415
    iget-object v0, p0, Lcom/a/b/d/ns;->b:Ljava/util/Iterator;

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    .line 1049
    :goto_5
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 416
    iget-object v0, p0, Lcom/a/b/d/ns;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 417
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/ns;->b:Ljava/util/Iterator;

    .line 418
    return-void

    .line 415
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method
