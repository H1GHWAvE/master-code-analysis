.class final Lcom/a/b/d/eb;
.super Lcom/a/b/d/dw;
.source "SourceFile"


# static fields
.field private static final b:J


# direct methods
.method constructor <init>(Ljava/lang/Comparable;)V
    .registers 3

    .prologue
    .line 240
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    invoke-direct {p0, v0}, Lcom/a/b/d/dw;-><init>(Ljava/lang/Comparable;)V

    .line 241
    return-void
.end method


# virtual methods
.method final a()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 247
    sget-object v0, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    return-object v0
.end method

.method final a(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 5

    .prologue
    .line 253
    sget-object v0, Lcom/a/b/d/dx;->a:[I

    invoke-virtual {p1}, Lcom/a/b/d/ce;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_24

    .line 260
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 257
    :pswitch_11
    iget-object v0, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    invoke-virtual {p2, v0}, Lcom/a/b/d/ep;->b(Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    .line 258
    if-nez v0, :cond_1e

    .line 1108
    invoke-static {}, Lcom/a/b/d/ea;->f()Lcom/a/b/d/ea;

    move-result-object p0

    .line 258
    :goto_1d
    :pswitch_1d
    return-object p0

    :cond_1e
    new-instance p0, Lcom/a/b/d/dz;

    invoke-direct {p0, v0}, Lcom/a/b/d/dz;-><init>(Ljava/lang/Comparable;)V

    goto :goto_1d

    .line 253
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_11
    .end packed-switch
.end method

.method final a(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 281
    iget-object v0, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    return-object v0
.end method

.method final a(Ljava/lang/StringBuilder;)V
    .registers 4

    .prologue
    .line 275
    const/16 v0, 0x5b

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 276
    return-void
.end method

.method final a(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 244
    iget-object v0, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    invoke-static {v0, p1}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-gtz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final b()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 250
    sget-object v0, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    return-object v0
.end method

.method final b(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 5

    .prologue
    .line 264
    sget-object v0, Lcom/a/b/d/dx;->a:[I

    invoke-virtual {p1}, Lcom/a/b/d/ce;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_24

    .line 271
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 266
    :pswitch_11
    iget-object v0, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    invoke-virtual {p2, v0}, Lcom/a/b/d/ep;->b(Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    .line 267
    if-nez v0, :cond_1e

    .line 1179
    invoke-static {}, Lcom/a/b/d/dy;->f()Lcom/a/b/d/dy;

    move-result-object p0

    .line 269
    :goto_1d
    :pswitch_1d
    return-object p0

    .line 267
    :cond_1e
    new-instance p0, Lcom/a/b/d/dz;

    invoke-direct {p0, v0}, Lcom/a/b/d/dz;-><init>(Ljava/lang/Comparable;)V

    goto :goto_1d

    .line 264
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_11
        :pswitch_1d
    .end packed-switch
.end method

.method final b(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 284
    iget-object v0, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    invoke-virtual {p1, v0}, Lcom/a/b/d/ep;->b(Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/StringBuilder;)V
    .registers 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 279
    return-void
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 238
    check-cast p1, Lcom/a/b/d/dw;

    invoke-super {p0, p1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 290
    iget-object v0, p0, Lcom/a/b/d/eb;->a:Ljava/lang/Comparable;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "\\"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
