.class public final Lcom/a/b/d/bl;
.super Lcom/a/b/d/bf;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final h:J


# instance fields
.field private final a:Lcom/a/b/d/jl;

.field private final b:Lcom/a/b/d/jl;

.field private final c:Lcom/a/b/d/jt;

.field private final d:Lcom/a/b/d/jt;

.field private final e:[[Ljava/lang/Object;

.field private transient f:Lcom/a/b/d/bt;

.field private transient g:Lcom/a/b/d/bv;


# direct methods
.method private constructor <init>(Lcom/a/b/d/adv;)V
    .registers 4

    .prologue
    .line 176
    invoke-interface {p1}, Lcom/a/b/d/adv;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/adv;->b()Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/bl;-><init>(Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    .line 1495
    invoke-super {p0, p1}, Lcom/a/b/d/bf;->a(Lcom/a/b/d/adv;)V

    .line 178
    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/bl;)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 180
    invoke-direct {p0}, Lcom/a/b/d/bf;-><init>()V

    .line 181
    iget-object v0, p1, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    iput-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    .line 182
    iget-object v0, p1, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    iput-object v0, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    .line 183
    iget-object v0, p1, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    iput-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    .line 184
    iget-object v0, p1, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    iput-object v0, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    .line 186
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/Object;

    check-cast v0, [[Ljava/lang/Object;

    .line 187
    iput-object v0, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    .line 189
    invoke-direct {p0}, Lcom/a/b/d/bl;->p()V

    move v1, v2

    .line 190
    :goto_34
    iget-object v3, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v3}, Lcom/a/b/d/jl;->size()I

    move-result v3

    if-ge v1, v3, :cond_4d

    .line 191
    iget-object v3, p1, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    aget-object v3, v3, v1

    aget-object v4, v0, v1

    iget-object v5, p1, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    aget-object v5, v5, v1

    array-length v5, v5

    invoke-static {v3, v2, v4, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_34

    .line 193
    :cond_4d
    return-void
.end method

.method private constructor <init>(Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 145
    invoke-direct {p0}, Lcom/a/b/d/bf;-><init>()V

    .line 146
    invoke-static {p1}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    .line 147
    invoke-static {p2}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    .line 148
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_58

    move v0, v1

    :goto_1a
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 149
    iget-object v0, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5a

    :goto_25
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Z)V

    .line 156
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/d/bl;->a(Ljava/util/List;)Lcom/a/b/d/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    .line 157
    iget-object v0, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/d/bl;->a(Ljava/util/List;)Lcom/a/b/d/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    .line 160
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/Object;

    check-cast v0, [[Ljava/lang/Object;

    .line 162
    iput-object v0, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    .line 164
    invoke-direct {p0}, Lcom/a/b/d/bl;->p()V

    .line 165
    return-void

    :cond_58
    move v0, v2

    .line 148
    goto :goto_1a

    :cond_5a
    move v1, v2

    .line 149
    goto :goto_25
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Lcom/a/b/d/bl;
    .registers 3

    .prologue
    .line 99
    new-instance v0, Lcom/a/b/d/bl;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/bl;-><init>(Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/bl;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    return-object v0
.end method

.method private static a(Ljava/util/List;)Lcom/a/b/d/jt;
    .registers 5

    .prologue
    .line 168
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v1

    .line 169
    const/4 v0, 0x0

    :goto_5
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_19

    .line 170
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 172
    :cond_19
    invoke-virtual {v1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Class;)[[Ljava/lang/Object;
    .registers 8
    .annotation build Lcom/a/b/a/c;
        a = "reflection"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 375
    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    aput v1, v0, v2

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v3}, Lcom/a/b/d/jl;->size()I

    move-result v3

    aput v3, v0, v1

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/Object;

    check-cast v0, [[Ljava/lang/Object;

    move v1, v2

    .line 377
    :goto_1e
    iget-object v3, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v3}, Lcom/a/b/d/jl;->size()I

    move-result v3

    if-ge v1, v3, :cond_37

    .line 378
    iget-object v3, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    aget-object v3, v3, v1

    aget-object v4, v0, v1

    iget-object v5, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    aget-object v5, v5, v1

    array-length v5, v5

    invoke-static {v3, v2, v4, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 377
    add-int/lit8 v1, v1, 0x1

    goto :goto_1e

    .line 380
    :cond_37
    return-object v0
.end method

.method private static b(Lcom/a/b/d/adv;)Lcom/a/b/d/bl;
    .registers 2

    .prologue
    .line 131
    instance-of v0, p0, Lcom/a/b/d/bl;

    if-eqz v0, :cond_c

    new-instance v0, Lcom/a/b/d/bl;

    check-cast p0, Lcom/a/b/d/bl;

    invoke-direct {v0, p0}, Lcom/a/b/d/bl;-><init>(Lcom/a/b/d/bl;)V

    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Lcom/a/b/d/bl;

    invoke-direct {v0, p0}, Lcom/a/b/d/bl;-><init>(Lcom/a/b/d/adv;)V

    goto :goto_b
.end method

.method static synthetic b(Lcom/a/b/d/bl;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    return-object v0
.end method

.method static synthetic c(Lcom/a/b/d/bl;)Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    return-object v0
.end method

.method static synthetic d(Lcom/a/b/d/bl;)Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    return-object v0
.end method

.method private d(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 523
    iget-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 524
    iget-object v1, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    invoke-virtual {v1, p2}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 525
    if-eqz v0, :cond_15

    if-nez v1, :cond_17

    :cond_15
    move-object v0, v2

    .line 528
    :goto_16
    return-object v0

    :cond_17
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/a/b/d/bl;->a(IILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_16
.end method

.method private n()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    return-object v0
.end method

.method private o()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    return-object v0
.end method

.method private p()V
    .registers 6

    .prologue
    .line 399
    iget-object v1, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_f

    aget-object v3, v1, v0

    .line 400
    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 399
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 402
    :cond_f
    return-void
.end method

.method private q()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private r()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 721
    iget-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(II)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 332
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 333
    iget-object v0, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    invoke-static {p2, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 334
    iget-object v0, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    aget-object v0, v0, p1

    aget-object v0, v0, p2

    return-object v0
.end method

.method public final a(IILjava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 354
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 355
    iget-object v0, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    invoke-static {p2, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 356
    iget-object v0, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    aget-object v0, v0, p1

    aget-object v0, v0, p2

    .line 357
    iget-object v1, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    aget-object v1, v1, p1

    aput-object p3, v1, p2

    .line 358
    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 12
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 467
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    iget-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 470
    if-eqz v0, :cond_46

    move v1, v2

    :goto_14
    const-string v4, "Row %s not in %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v3

    iget-object v6, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    aput-object v6, v5, v2

    invoke-static {v1, v4, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 471
    iget-object v1, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    invoke-virtual {v1, p2}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 472
    if-eqz v1, :cond_48

    move v4, v2

    :goto_2c
    const-string v5, "Column %s not in %s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p2, v6, v3

    iget-object v3, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    aput-object v3, v6, v2

    invoke-static {v4, v5, v6}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 474
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1, p3}, Lcom/a/b/d/bl;->a(IILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_46
    move v1, v3

    .line 470
    goto :goto_14

    :cond_48
    move v4, v3

    .line 472
    goto :goto_2c
.end method

.method public final synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 84
    .line 1721
    iget-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 84
    return-object v0
.end method

.method public final a(Lcom/a/b/d/adv;)V
    .registers 2

    .prologue
    .line 495
    invoke-super {p0, p1}, Lcom/a/b/d/bf;->a(Lcom/a/b/d/adv;)V

    .line 496
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 428
    iget-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 410
    invoke-virtual {p0, p1}, Lcom/a/b/d/bl;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, p2}, Lcom/a/b/d/bl;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 445
    iget-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 446
    iget-object v1, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    invoke-virtual {v1, p2}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 447
    if-eqz v0, :cond_14

    if-nez v1, :cond_16

    :cond_14
    const/4 v0, 0x0

    :goto_15
    return-object v0

    :cond_16
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/bl;->a(II)Ljava/lang/Object;

    move-result-object v0

    goto :goto_15
.end method

.method public final synthetic b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 84
    .line 1632
    iget-object v0, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 84
    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 419
    iget-object v0, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 506
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 456
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Ljava/lang/Object;)Z
    .registers 10
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 433
    iget-object v3, p0, Lcom/a/b/d/bl;->e:[[Ljava/lang/Object;

    array-length v4, v3

    move v2, v0

    :goto_5
    if-ge v2, v4, :cond_16

    aget-object v5, v3, v2

    .line 434
    array-length v6, v5

    move v1, v0

    :goto_b
    if-ge v1, v6, :cond_1a

    aget-object v7, v5, v1

    .line 435
    invoke-static {p1, v7}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_17

    .line 436
    const/4 v0, 0x1

    .line 440
    :cond_16
    return v0

    .line 434
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 433
    :cond_1a
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5
.end method

.method public final d(Ljava/lang/Object;)Ljava/util/Map;
    .registers 4

    .prologue
    .line 594
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    iget-object v0, p0, Lcom/a/b/d/bl;->d:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 596
    if-nez v0, :cond_12

    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v0

    :goto_11
    return-object v0

    :cond_12
    new-instance v1, Lcom/a/b/d/bs;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/bs;-><init>(Lcom/a/b/d/bl;I)V

    move-object v0, v1

    goto :goto_11
.end method

.method public final d()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 391
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e(Ljava/lang/Object;)Ljava/util/Map;
    .registers 4

    .prologue
    .line 684
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 685
    iget-object v0, p0, Lcom/a/b/d/bl;->c:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 686
    if-nez v0, :cond_12

    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v0

    :goto_11
    return-object v0

    :cond_12
    new-instance v1, Lcom/a/b/d/bu;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/bu;-><init>(Lcom/a/b/d/bl;I)V

    move-object v0, v1

    goto :goto_11
.end method

.method public final e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 553
    invoke-super {p0}, Lcom/a/b/d/bf;->e()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/a/b/d/bf;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final g()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 558
    new-instance v0, Lcom/a/b/d/bm;

    invoke-virtual {p0}, Lcom/a/b/d/bl;->k()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/bm;-><init>(Lcom/a/b/d/bl;I)V

    return-object v0
.end method

.method public final h()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 769
    invoke-super {p0}, Lcom/a/b/d/bf;->h()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 84
    invoke-super {p0}, Lcom/a/b/d/bf;->hashCode()I

    move-result v0

    return v0
.end method

.method public final k()I
    .registers 3

    .prologue
    .line 535
    iget-object v0, p0, Lcom/a/b/d/bl;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/bl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method public final l()Ljava/util/Map;
    .registers 3

    .prologue
    .line 639
    iget-object v0, p0, Lcom/a/b/d/bl;->f:Lcom/a/b/d/bt;

    .line 640
    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/bt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/bt;-><init>(Lcom/a/b/d/bl;B)V

    iput-object v0, p0, Lcom/a/b/d/bl;->f:Lcom/a/b/d/bt;

    :cond_c
    return-object v0
.end method

.method public final m()Ljava/util/Map;
    .registers 3

    .prologue
    .line 728
    iget-object v0, p0, Lcom/a/b/d/bl;->g:Lcom/a/b/d/bv;

    .line 729
    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/bv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/bv;-><init>(Lcom/a/b/d/bl;B)V

    iput-object v0, p0, Lcom/a/b/d/bl;->g:Lcom/a/b/d/bv;

    :cond_c
    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 84
    invoke-super {p0}, Lcom/a/b/d/bf;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
