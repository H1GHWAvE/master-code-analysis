.class public abstract Lcom/a/b/d/gg;
.super Lcom/a/b/d/gj;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/BlockingDeque;


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/a/b/d/gj;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/util/Queue;
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    return-object v0
.end method

.method protected abstract c()Ljava/util/concurrent/BlockingDeque;
.end method

.method protected final synthetic d()Ljava/util/Deque;
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    return-object v0
.end method

.method public drainTo(Ljava/util/Collection;)I
    .registers 3

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingDeque;->drainTo(Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method public drainTo(Ljava/util/Collection;I)I
    .registers 4

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/BlockingDeque;->drainTo(Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    return-object v0
.end method

.method public offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .registers 7

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/BlockingDeque;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public offerFirst(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .registers 7

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/BlockingDeque;->offerFirst(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public offerLast(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .registers 7

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/BlockingDeque;->offerLast(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/BlockingDeque;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollFirst(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/BlockingDeque;->pollFirst(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollLast(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/BlockingDeque;->pollLast(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingDeque;->put(Ljava/lang/Object;)V

    .line 97
    return-void
.end method

.method public putFirst(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingDeque;->putFirst(Ljava/lang/Object;)V

    .line 57
    return-void
.end method

.method public putLast(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingDeque;->putLast(Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method public remainingCapacity()I
    .registers 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->remainingCapacity()I

    move-result v0

    return v0
.end method

.method public take()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->take()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public takeFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->takeFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public takeLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/a/b/d/gg;->c()Ljava/util/concurrent/BlockingDeque;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->takeLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
