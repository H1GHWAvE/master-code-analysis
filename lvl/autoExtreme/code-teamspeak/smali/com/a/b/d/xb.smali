.class final Lcom/a/b/d/xb;
.super Lcom/a/b/d/xa;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/abs;


# static fields
.field private static final g:J


# direct methods
.method constructor <init>(Lcom/a/b/d/abs;)V
    .registers 2

    .prologue
    .line 643
    invoke-direct {p0, p1}, Lcom/a/b/d/xa;-><init>(Lcom/a/b/d/aac;)V

    .line 644
    return-void
.end method

.method private d()Lcom/a/b/d/abs;
    .registers 2

    .prologue
    .line 646
    invoke-super {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 640
    .line 3646
    invoke-super {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 640
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 640
    invoke-virtual {p0, p1}, Lcom/a/b/d/xb;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 640
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/xb;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 640
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/xb;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 640
    invoke-virtual {p0, p1}, Lcom/a/b/d/xb;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 640
    .line 4646
    invoke-super {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 640
    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 640
    invoke-virtual {p0, p1}, Lcom/a/b/d/xb;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 640
    invoke-virtual {p0, p1}, Lcom/a/b/d/xb;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 656
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d_()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 660
    .line 2646
    invoke-super {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 660
    invoke-interface {v0}, Lcom/a/b/d/abs;->d_()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 649
    .line 1646
    invoke-super {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 649
    invoke-interface {v0, p1}, Lcom/a/b/d/abs;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 652
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 640
    .line 5646
    invoke-super {p0}, Lcom/a/b/d/xa;->a()Lcom/a/b/d/aac;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abs;

    .line 640
    return-object v0
.end method
