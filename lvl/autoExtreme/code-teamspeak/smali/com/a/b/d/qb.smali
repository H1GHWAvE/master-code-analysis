.class Lcom/a/b/d/qb;
.super Lcom/a/b/d/gs;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field final b:Lcom/a/b/d/pn;

.field private transient c:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/d/pn;)V
    .registers 4

    .prologue
    .line 297
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    .line 298
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/a/b/d/qb;->a:Ljava/util/Map;

    .line 299
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/pn;

    iput-object v0, p0, Lcom/a/b/d/qb;->b:Lcom/a/b/d/pn;

    .line 300
    return-void
.end method


# virtual methods
.method protected a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/a/b/d/qb;->a:Ljava/util/Map;

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .registers 3

    .prologue
    .line 305
    iget-object v0, p0, Lcom/a/b/d/qb;->c:Ljava/util/Set;

    .line 306
    if-nez v0, :cond_12

    .line 307
    iget-object v0, p0, Lcom/a/b/d/qb;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/qb;->b:Lcom/a/b/d/pn;

    .line 1046
    invoke-static {v0, v1}, Lcom/a/b/d/po;->a(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;

    move-result-object v0

    .line 307
    iput-object v0, p0, Lcom/a/b/d/qb;->c:Ljava/util/Set;

    .line 310
    :cond_12
    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/a/b/d/qb;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 313
    iget-object v0, p0, Lcom/a/b/d/qb;->b:Lcom/a/b/d/pn;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/pn;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 314
    iget-object v0, p0, Lcom/a/b/d/qb;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .registers 8

    .prologue
    .line 317
    iget-object v1, p0, Lcom/a/b/d/qb;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/a/b/d/qb;->b:Lcom/a/b/d/pn;

    .line 2778
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3, p1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 2779
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_11
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2780
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v5, v0}, Lcom/a/b/d/pn;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_11

    .line 317
    :cond_29
    invoke-interface {v1, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 318
    return-void
.end method
