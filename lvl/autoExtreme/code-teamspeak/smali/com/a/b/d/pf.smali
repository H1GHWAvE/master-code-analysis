.class Lcom/a/b/d/pf;
.super Ljava/util/AbstractList;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .registers 3

    .prologue
    .line 801
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 802
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    .line 803
    return-void
.end method

.method private a(I)I
    .registers 3

    .prologue
    .line 810
    invoke-virtual {p0}, Lcom/a/b/d/pf;->size()I

    move-result v0

    .line 811
    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 812
    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method static synthetic a(Lcom/a/b/d/pf;I)I
    .registers 3

    .prologue
    .line 798
    invoke-direct {p0, p1}, Lcom/a/b/d/pf;->b(I)I

    move-result v0

    return v0
.end method

.method private a()Ljava/util/List;
    .registers 2

    .prologue
    .line 806
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    return-object v0
.end method

.method private b(I)I
    .registers 3

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/a/b/d/pf;->size()I

    move-result v0

    .line 817
    invoke-static {p1, v0}, Lcom/a/b/b/cn;->b(II)I

    .line 818
    sub-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .registers 5
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/a/b/d/pf;->b(I)I

    move-result v1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 823
    return-void
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 826
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 827
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 842
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/a/b/d/pf;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 856
    invoke-virtual {p0}, Lcom/a/b/d/pf;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .registers 4

    .prologue
    .line 860
    invoke-direct {p0, p1}, Lcom/a/b/d/pf;->b(I)I

    move-result v0

    .line 861
    iget-object v1, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 862
    new-instance v1, Lcom/a/b/d/pg;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/pg;-><init>(Lcom/a/b/d/pf;Ljava/util/ListIterator;)V

    return-object v1
.end method

.method public remove(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 830
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/a/b/d/pf;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected removeRange(II)V
    .registers 4

    .prologue
    .line 834
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/pf;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 835
    return-void
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 838
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/a/b/d/pf;->a(I)I

    move-result v1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 846
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .registers 6

    .prologue
    .line 850
    invoke-virtual {p0}, Lcom/a/b/d/pf;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 851
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/a/b/d/pf;->b(I)I

    move-result v1

    invoke-direct {p0, p1}, Lcom/a/b/d/pf;->b(I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 1787
    instance-of v1, v0, Lcom/a/b/d/jl;

    if-eqz v1, :cond_20

    .line 1788
    check-cast v0, Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->e()Lcom/a/b/d/jl;

    move-result-object v0

    .line 1792
    :goto_1f
    return-object v0

    .line 1789
    :cond_20
    instance-of v1, v0, Lcom/a/b/d/pf;

    if-eqz v1, :cond_29

    .line 1790
    check-cast v0, Lcom/a/b/d/pf;

    .line 1806
    iget-object v0, v0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    goto :goto_1f

    .line 1791
    :cond_29
    instance-of v1, v0, Ljava/util/RandomAccess;

    if-eqz v1, :cond_34

    .line 1792
    new-instance v1, Lcom/a/b/d/pe;

    invoke-direct {v1, v0}, Lcom/a/b/d/pe;-><init>(Ljava/util/List;)V

    move-object v0, v1

    goto :goto_1f

    .line 1794
    :cond_34
    new-instance v1, Lcom/a/b/d/pf;

    invoke-direct {v1, v0}, Lcom/a/b/d/pf;-><init>(Ljava/util/List;)V

    move-object v0, v1

    .line 851
    goto :goto_1f
.end method
