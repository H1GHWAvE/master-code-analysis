.class final Lcom/a/b/d/pk;
.super Ljava/util/AbstractSequentialList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field final a:Ljava/util/List;

.field final b:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 559
    invoke-direct {p0}, Ljava/util/AbstractSequentialList;-><init>()V

    .line 560
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/a/b/d/pk;->a:Ljava/util/List;

    .line 561
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/d/pk;->b:Lcom/a/b/b/bj;

    .line 562
    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/a/b/d/pk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 570
    return-void
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
    .registers 4

    .prologue
    .line 575
    new-instance v0, Lcom/a/b/d/pl;

    iget-object v1, p0, Lcom/a/b/d/pk;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/pl;-><init>(Lcom/a/b/d/pk;Ljava/util/ListIterator;)V

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/a/b/d/pk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
