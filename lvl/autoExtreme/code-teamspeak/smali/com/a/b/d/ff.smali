.class public final Lcom/a/b/d/ff;
.super Lcom/a/b/d/ai;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final c:J
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in emulated source"
    .end annotation
.end field


# instance fields
.field private transient b:Ljava/lang/Class;


# direct methods
.method private constructor <init>(Ljava/lang/Class;)V
    .registers 3

    .prologue
    .line 79
    new-instance v0, Ljava/util/EnumMap;

    invoke-direct {v0, p1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/a/b/d/agm;->a(Ljava/util/Map;)Lcom/a/b/d/agm;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ai;-><init>(Ljava/util/Map;)V

    .line 80
    iput-object p1, p0, Lcom/a/b/d/ff;->b:Ljava/lang/Class;

    .line 81
    return-void
.end method

.method private static a(Ljava/lang/Class;)Lcom/a/b/d/ff;
    .registers 2

    .prologue
    .line 42
    new-instance v0, Lcom/a/b/d/ff;

    invoke-direct {v0, p0}, Lcom/a/b/d/ff;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/ff;
    .registers 4

    .prologue
    .line 55
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, "EnumMultiset constructor passed empty Iterable"

    invoke-static {v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 57
    new-instance v1, Lcom/a/b/d/ff;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/b/d/ff;-><init>(Ljava/lang/Class;)V

    .line 58
    invoke-static {v1, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 59
    return-object v1
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Class;)Lcom/a/b/d/ff;
    .registers 3

    .prologue
    .line 1042
    new-instance v0, Lcom/a/b/d/ff;

    invoke-direct {v0, p1}, Lcom/a/b/d/ff;-><init>(Ljava/lang/Class;)V

    .line 71
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 72
    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 99
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 100
    iput-object v0, p0, Lcom/a/b/d/ff;->b:Ljava/lang/Class;

    .line 101
    new-instance v0, Ljava/util/EnumMap;

    iget-object v1, p0, Lcom/a/b/d/ff;->b:Ljava/lang/Class;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/a/b/d/agm;->a(Ljava/util/Map;)Lcom/a/b/d/agm;

    move-result-object v0

    .line 1068
    iput-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    .line 102
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/xc;Ljava/io/ObjectInputStream;)V

    .line 103
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 86
    iget-object v0, p0, Lcom/a/b/d/ff;->b:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 87
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/xc;Ljava/io/ObjectOutputStream;)V

    .line 88
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 38
    invoke-super {p0}, Lcom/a/b/d/ai;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ai;->b(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic clear()V
    .registers 1

    .prologue
    .line 38
    invoke-super {p0}, Lcom/a/b/d/ai;->clear()V

    return-void
.end method

.method public final bridge synthetic contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 38
    invoke-super {p0}, Lcom/a/b/d/ai;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic isEmpty()Z
    .registers 2

    .prologue
    .line 38
    invoke-super {p0}, Lcom/a/b/d/ai;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 38
    invoke-super {p0}, Lcom/a/b/d/ai;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 38
    invoke-super {p0}, Lcom/a/b/d/ai;->n_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic size()I
    .registers 2

    .prologue
    .line 38
    invoke-super {p0}, Lcom/a/b/d/ai;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 38
    invoke-super {p0}, Lcom/a/b/d/ai;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
