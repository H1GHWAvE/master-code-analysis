.class final Lcom/a/b/d/xz;
.super Lcom/a/b/d/yd;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field static final a:Lcom/a/b/d/xz;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 30
    new-instance v0, Lcom/a/b/d/xz;

    invoke-direct {v0}, Lcom/a/b/d/xz;-><init>()V

    sput-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/a/b/d/yd;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .registers 3

    .prologue
    .line 33
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static f()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 44
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 39
    sget-object v0, Lcom/a/b/d/zw;->a:Lcom/a/b/d/zw;

    return-object v0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    .line 1033
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1034
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1035
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 26
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 48
    const-string v0, "Ordering.natural()"

    return-object v0
.end method
