.class final Lcom/a/b/d/c;
.super Lcom/a/b/d/hi;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Set;

.field final synthetic b:Lcom/a/b/d/a;


# direct methods
.method private constructor <init>(Lcom/a/b/d/a;)V
    .registers 3

    .prologue
    .line 257
    iput-object p1, p0, Lcom/a/b/d/c;->b:Lcom/a/b/d/a;

    invoke-direct {p0}, Lcom/a/b/d/hi;-><init>()V

    .line 258
    iget-object v0, p0, Lcom/a/b/d/c;->b:Lcom/a/b/d/a;

    invoke-static {v0}, Lcom/a/b/d/a;->a(Lcom/a/b/d/a;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/c;->a:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/a;B)V
    .registers 3

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/a/b/d/c;-><init>(Lcom/a/b/d/a;)V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/a/b/d/c;->a:Ljava/util/Set;

    return-object v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 257
    .line 2261
    iget-object v0, p0, Lcom/a/b/d/c;->a:Ljava/util/Set;

    .line 257
    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/a/b/d/c;->b:Lcom/a/b/d/a;

    invoke-virtual {v0}, Lcom/a/b/d/a;->clear()V

    .line 266
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 339
    .line 1261
    iget-object v0, p0, Lcom/a/b/d/c;->a:Ljava/util/Set;

    .line 339
    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 342
    .line 2141
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    .line 342
    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/a/b/d/c;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 287
    new-instance v1, Lcom/a/b/d/d;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/d;-><init>(Lcom/a/b/d/c;Ljava/util/Iterator;)V

    return-object v1
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 257
    .line 3261
    iget-object v0, p0, Lcom/a/b/d/c;->a:Ljava/util/Set;

    .line 257
    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 269
    iget-object v0, p0, Lcom/a/b/d/c;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 270
    const/4 v0, 0x0

    .line 282
    :goto_9
    return v0

    .line 274
    :cond_a
    check-cast p1, Ljava/util/Map$Entry;

    .line 275
    iget-object v0, p0, Lcom/a/b/d/c;->b:Lcom/a/b/d/a;

    iget-object v0, v0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    invoke-static {v0}, Lcom/a/b/d/a;->a(Lcom/a/b/d/a;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    iget-object v0, p0, Lcom/a/b/d/c;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 282
    const/4 v0, 0x1

    goto :goto_9
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 345
    invoke-virtual {p0, p1}, Lcom/a/b/d/c;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 348
    invoke-virtual {p0, p1}, Lcom/a/b/d/c;->c(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/a/b/d/c;->p()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 336
    .line 1253
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 336
    return-object v0
.end method
