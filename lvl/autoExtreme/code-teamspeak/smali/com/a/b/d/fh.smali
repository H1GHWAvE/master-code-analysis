.class final Lcom/a/b/d/fh;
.super Lcom/a/b/d/yd;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field private static final b:J


# instance fields
.field final a:Lcom/a/b/d/jt;


# direct methods
.method private constructor <init>(Lcom/a/b/d/jt;)V
    .registers 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/a/b/d/yd;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/a/b/d/fh;->a:Lcom/a/b/d/jt;

    .line 37
    return-void
.end method

.method constructor <init>(Ljava/util/List;)V
    .registers 3

    .prologue
    .line 32
    invoke-static {p1}, Lcom/a/b/d/fh;->a(Ljava/util/List;)Lcom/a/b/d/jt;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/fh;-><init>(Lcom/a/b/d/jt;)V

    .line 33
    return-void
.end method

.method private a(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/a/b/d/fh;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 45
    if-nez v0, :cond_10

    .line 46
    new-instance v0, Lcom/a/b/d/yh;

    invoke-direct {v0, p1}, Lcom/a/b/d/yh;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 48
    :cond_10
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/List;)Lcom/a/b/d/jt;
    .registers 6

    .prologue
    .line 53
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v2

    .line 54
    const/4 v0, 0x0

    .line 55
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 56
    add-int/lit8 v1, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move v0, v1

    .line 57
    goto :goto_9

    .line 58
    :cond_1e
    invoke-virtual {v2}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/a/b/d/fh;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/a/b/d/fh;->a(Ljava/lang/Object;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62
    instance-of v0, p1, Lcom/a/b/d/fh;

    if-eqz v0, :cond_f

    .line 63
    check-cast p1, Lcom/a/b/d/fh;

    .line 64
    iget-object v0, p0, Lcom/a/b/d/fh;->a:Lcom/a/b/d/jt;

    iget-object v1, p1, Lcom/a/b/d/fh;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, v1}, Lcom/a/b/d/jt;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 66
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/a/b/d/fh;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 74
    iget-object v0, p0, Lcom/a/b/d/fh;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Ordering.explicit("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
