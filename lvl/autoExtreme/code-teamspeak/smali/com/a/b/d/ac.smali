.class Lcom/a/b/d/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field final b:Ljava/util/Collection;

.field final synthetic c:Lcom/a/b/d/ab;


# direct methods
.method constructor <init>(Lcom/a/b/d/ab;)V
    .registers 3

    .prologue
    .line 458
    iput-object p1, p0, Lcom/a/b/d/ac;->c:Lcom/a/b/d/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    iget-object v0, p0, Lcom/a/b/d/ac;->c:Lcom/a/b/d/ab;

    iget-object v0, v0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    iput-object v0, p0, Lcom/a/b/d/ac;->b:Ljava/util/Collection;

    .line 459
    iget-object v0, p1, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    invoke-static {v0}, Lcom/a/b/d/n;->b(Ljava/util/Collection;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ac;->a:Ljava/util/Iterator;

    .line 460
    return-void
.end method

.method constructor <init>(Lcom/a/b/d/ab;Ljava/util/Iterator;)V
    .registers 4

    .prologue
    .line 462
    iput-object p1, p0, Lcom/a/b/d/ac;->c:Lcom/a/b/d/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    iget-object v0, p0, Lcom/a/b/d/ac;->c:Lcom/a/b/d/ab;

    iget-object v0, v0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    iput-object v0, p0, Lcom/a/b/d/ac;->b:Ljava/util/Collection;

    .line 463
    iput-object p2, p0, Lcom/a/b/d/ac;->a:Ljava/util/Iterator;

    .line 464
    return-void
.end method

.method private b()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/a/b/d/ac;->a()V

    .line 498
    iget-object v0, p0, Lcom/a/b/d/ac;->a:Ljava/util/Iterator;

    return-object v0
.end method


# virtual methods
.method final a()V
    .registers 3

    .prologue
    .line 471
    iget-object v0, p0, Lcom/a/b/d/ac;->c:Lcom/a/b/d/ab;

    invoke-virtual {v0}, Lcom/a/b/d/ab;->a()V

    .line 472
    iget-object v0, p0, Lcom/a/b/d/ac;->c:Lcom/a/b/d/ab;

    iget-object v0, v0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    iget-object v1, p0, Lcom/a/b/d/ac;->b:Ljava/util/Collection;

    if-eq v0, v1, :cond_13

    .line 473
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 475
    :cond_13
    return-void
.end method

.method public hasNext()Z
    .registers 2

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/a/b/d/ac;->a()V

    .line 480
    iget-object v0, p0, Lcom/a/b/d/ac;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 485
    invoke-virtual {p0}, Lcom/a/b/d/ac;->a()V

    .line 486
    iget-object v0, p0, Lcom/a/b/d/ac;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .registers 2

    .prologue
    .line 491
    iget-object v0, p0, Lcom/a/b/d/ac;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 492
    iget-object v0, p0, Lcom/a/b/d/ac;->c:Lcom/a/b/d/ab;

    iget-object v0, v0, Lcom/a/b/d/ab;->f:Lcom/a/b/d/n;

    invoke-static {v0}, Lcom/a/b/d/n;->b(Lcom/a/b/d/n;)I

    .line 493
    iget-object v0, p0, Lcom/a/b/d/ac;->c:Lcom/a/b/d/ab;

    invoke-virtual {v0}, Lcom/a/b/d/ab;->b()V

    .line 494
    return-void
.end method
