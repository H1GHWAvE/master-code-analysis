.class public final Lcom/a/b/d/io;
.super Lcom/a/b/d/ba;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final b:I = 0x2

.field private static final c:J
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in emulated source"
    .end annotation
.end field


# instance fields
.field transient a:I
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/a/b/d/ba;-><init>(Ljava/util/Map;)V

    .line 53
    const/4 v0, 0x2

    iput v0, p0, Lcom/a/b/d/io;->a:I

    .line 92
    return-void
.end method

.method private constructor <init>(II)V
    .registers 4

    .prologue
    .line 95
    invoke-static {p1}, Lcom/a/b/d/sz;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ba;-><init>(Ljava/util/Map;)V

    .line 53
    const/4 v0, 0x2

    iput v0, p0, Lcom/a/b/d/io;->a:I

    .line 96
    if-ltz p2, :cond_13

    const/4 v0, 0x1

    :goto_d
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 97
    iput p2, p0, Lcom/a/b/d/io;->a:I

    .line 98
    return-void

    .line 96
    :cond_13
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private constructor <init>(Lcom/a/b/d/vi;)V
    .registers 3

    .prologue
    .line 101
    invoke-interface {p1}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ba;-><init>(Ljava/util/Map;)V

    .line 53
    const/4 v0, 0x2

    iput v0, p0, Lcom/a/b/d/io;->a:I

    .line 103
    invoke-virtual {p0, p1}, Lcom/a/b/d/io;->a(Lcom/a/b/d/vi;)Z

    .line 104
    return-void
.end method

.method private static a(II)Lcom/a/b/d/io;
    .registers 3

    .prologue
    .line 75
    new-instance v0, Lcom/a/b/d/io;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/io;-><init>(II)V

    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 132
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 133
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/a/b/d/io;->a:I

    .line 1050
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 135
    invoke-static {v0}, Lcom/a/b/d/sz;->a(I)Ljava/util/HashMap;

    move-result-object v1

    .line 136
    invoke-virtual {p0, v1}, Lcom/a/b/d/io;->a(Ljava/util/Map;)V

    .line 137
    invoke-static {p0, p1, v0}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/vi;Ljava/io/ObjectInputStream;I)V

    .line 138
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 124
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 125
    iget v0, p0, Lcom/a/b/d/io;->a:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 126
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V

    .line 127
    return-void
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/io;
    .registers 2

    .prologue
    .line 87
    new-instance v0, Lcom/a/b/d/io;

    invoke-direct {v0, p0}, Lcom/a/b/d/io;-><init>(Lcom/a/b/d/vi;)V

    return-object v0
.end method

.method public static v()Lcom/a/b/d/io;
    .registers 1

    .prologue
    .line 61
    new-instance v0, Lcom/a/b/d/io;

    invoke-direct {v0}, Lcom/a/b/d/io;-><init>()V

    return-object v0
.end method


# virtual methods
.method final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 114
    iget v0, p0, Lcom/a/b/d/io;->a:I

    invoke-static {v0}, Lcom/a/b/d/aad;->a(I)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->a(Lcom/a/b/d/vi;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 49
    .line 1114
    iget v0, p0, Lcom/a/b/d/io;->a:I

    invoke-static {v0}, Lcom/a/b/d/aad;->a(I)Ljava/util/HashSet;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()I
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->f()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic g()V
    .registers 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->g()V

    return-void
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n()Z
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->n()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->q()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/a/b/d/ba;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
