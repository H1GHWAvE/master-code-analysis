.class final Lcom/a/b/d/ji;
.super Lcom/a/b/d/lo;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# instance fields
.field private final transient a:Ljava/util/EnumSet;

.field private transient c:I


# direct methods
.method private constructor <init>(Ljava/util/EnumSet;)V
    .registers 2

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/a/b/d/lo;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    .line 57
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/EnumSet;B)V
    .registers 3

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/a/b/d/ji;-><init>(Ljava/util/EnumSet;)V

    return-void
.end method

.method static a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 35
    invoke-virtual {p0}, Ljava/util/EnumSet;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_1a

    .line 41
    new-instance v0, Lcom/a/b/d/ji;

    invoke-direct {v0, p0}, Lcom/a/b/d/ji;-><init>(Ljava/util/EnumSet;)V

    :goto_c
    return-object v0

    .line 1084
    :pswitch_d
    sget-object v0, Lcom/a/b/d/ey;->a:Lcom/a/b/d/ey;

    goto :goto_c

    .line 39
    :pswitch_10
    invoke-static {p0}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_c

    .line 35
    nop

    :pswitch_data_1a
    .packed-switch 0x0
        :pswitch_d
        :pswitch_10
    .end packed-switch
.end method


# virtual methods
.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 85
    if-eq p1, p0, :cond_a

    iget-object v0, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method final g()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 101
    new-instance v0, Lcom/a/b/d/jk;

    iget-object v1, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-direct {v0, v1}, Lcom/a/b/d/jk;-><init>(Ljava/util/EnumSet;)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 91
    iget v0, p0, Lcom/a/b/d/ji;->c:I

    .line 92
    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/a/b/d/ji;->c:I

    :cond_c
    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/a/b/d/ji;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/a/b/d/ji;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
