.class final Lcom/a/b/d/to;
.super Lcom/a/b/b/ak;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J


# instance fields
.field private final a:Lcom/a/b/d/bw;


# direct methods
.method constructor <init>(Lcom/a/b/d/bw;)V
    .registers 3

    .prologue
    .line 1321
    invoke-direct {p0}, Lcom/a/b/b/ak;-><init>()V

    .line 1322
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/bw;

    iput-object v0, p0, Lcom/a/b/d/to;->a:Lcom/a/b/d/bw;

    .line 1323
    return-void
.end method

.method private static a(Lcom/a/b/d/bw;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1336
    invoke-interface {p0, p1}, Lcom/a/b/d/bw;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 1337
    if-eqz v3, :cond_13

    move v0, v1

    :goto_9
    const-string v4, "No non-null mapping present for input: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v4, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1338
    return-object v3

    :cond_13
    move v0, v2

    .line 1337
    goto :goto_9
.end method


# virtual methods
.method protected final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/a/b/d/to;->a:Lcom/a/b/d/bw;

    invoke-interface {v0}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/to;->a(Lcom/a/b/d/bw;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1327
    iget-object v0, p0, Lcom/a/b/d/to;->a:Lcom/a/b/d/bw;

    invoke-static {v0, p1}, Lcom/a/b/d/to;->a(Lcom/a/b/d/bw;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1343
    instance-of v0, p1, Lcom/a/b/d/to;

    if-eqz v0, :cond_f

    .line 1344
    check-cast p1, Lcom/a/b/d/to;

    .line 1345
    iget-object v0, p0, Lcom/a/b/d/to;->a:Lcom/a/b/d/bw;

    iget-object v1, p1, Lcom/a/b/d/to;->a:Lcom/a/b/d/bw;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1347
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/a/b/d/to;->a:Lcom/a/b/d/bw;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/a/b/d/to;->a:Lcom/a/b/d/bw;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Maps.asConverter("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
