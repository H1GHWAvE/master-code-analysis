.class final Lcom/a/b/d/ie;
.super Ljava/util/AbstractMap;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/bw;
.implements Ljava/io/Serializable;


# instance fields
.field final synthetic a:Lcom/a/b/d/hy;


# direct methods
.method private constructor <init>(Lcom/a/b/d/hy;)V
    .registers 2

    .prologue
    .line 486
    iput-object p1, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 543
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/hy;B)V
    .registers 3

    .prologue
    .line 486
    invoke-direct {p0, p1}, Lcom/a/b/d/ie;-><init>(Lcom/a/b/d/hy;)V

    return-void
.end method

.method private a()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 488
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    return-object v0
.end method

.method private d()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 632
    new-instance v0, Lcom/a/b/d/ik;

    iget-object v1, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    invoke-direct {v0, v1}, Lcom/a/b/d/ik;-><init>(Lcom/a/b/d/hy;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 519
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    const/4 v1, 0x1

    invoke-static {v0, p1, p2, v1}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 535
    .line 7488
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    .line 535
    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 498
    .line 1488
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    .line 498
    invoke-interface {v0}, Lcom/a/b/d/bw;->clear()V

    .line 499
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 503
    .line 2488
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    .line 503
    invoke-interface {v0, p1}, Lcom/a/b/d/bw;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 576
    new-instance v0, Lcom/a/b/d/if;

    invoke-direct {v0, p0}, Lcom/a/b/d/if;-><init>(Lcom/a/b/d/ie;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 508
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    .line 3052
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v1

    .line 4052
    invoke-virtual {v0, p1, v1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    .line 509
    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return-object v0

    :cond_e
    iget-object v0, v0, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    goto :goto_d
.end method

.method public final j_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 571
    .line 8488
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    .line 571
    invoke-interface {v0}, Lcom/a/b/d/bw;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 540
    new-instance v0, Lcom/a/b/d/ii;

    invoke-direct {v0, p0}, Lcom/a/b/d/ii;-><init>(Lcom/a/b/d/ie;)V

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 514
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 524
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    .line 5052
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v1

    .line 6052
    invoke-virtual {v0, p1, v1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    .line 525
    if-nez v0, :cond_e

    .line 526
    const/4 v0, 0x0

    .line 529
    :goto_d
    return-object v0

    .line 528
    :cond_e
    iget-object v1, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    .line 7052
    invoke-virtual {v1, v0}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    .line 529
    iget-object v0, v0, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    goto :goto_d
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/a/b/d/ie;->a:Lcom/a/b/d/hy;

    .line 1052
    iget v0, v0, Lcom/a/b/d/hy;->a:I

    .line 493
    return v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/a/b/d/ie;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
