.class final Lcom/a/b/d/lu;
.super Lcom/a/b/d/lo;
.source "SourceFile"


# instance fields
.field private final transient a:Lcom/a/b/d/lr;


# direct methods
.method constructor <init>(Lcom/a/b/d/lr;)V
    .registers 2

    .prologue
    .line 428
    invoke-direct {p0}, Lcom/a/b/d/lo;-><init>()V

    .line 429
    iput-object p1, p0, Lcom/a/b/d/lu;->a:Lcom/a/b/d/lr;

    .line 430
    return-void
.end method


# virtual methods
.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 448
    iget-object v0, p0, Lcom/a/b/d/lu;->a:Lcom/a/b/d/lr;

    invoke-virtual {v0}, Lcom/a/b/d/lr;->w()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 434
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_15

    .line 435
    check-cast p1, Ljava/util/Map$Entry;

    .line 436
    iget-object v0, p0, Lcom/a/b/d/lu;->a:Lcom/a/b/d/lr;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/lr;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 438
    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 453
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 425
    .line 1448
    iget-object v0, p0, Lcom/a/b/d/lu;->a:Lcom/a/b/d/lr;

    invoke-virtual {v0}, Lcom/a/b/d/lr;->w()Lcom/a/b/d/agi;

    move-result-object v0

    .line 425
    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/a/b/d/lu;->a:Lcom/a/b/d/lr;

    invoke-virtual {v0}, Lcom/a/b/d/lr;->f()I

    move-result v0

    return v0
.end method
