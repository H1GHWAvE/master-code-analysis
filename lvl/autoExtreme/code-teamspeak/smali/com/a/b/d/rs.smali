.class final Lcom/a/b/d/rs;
.super Ljava/util/AbstractQueue;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/rz;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 3239
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3240
    new-instance v0, Lcom/a/b/d/rt;

    invoke-direct {v0, p0}, Lcom/a/b/d/rt;-><init>(Lcom/a/b/d/rs;)V

    iput-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    return-void
.end method

.method private a()Lcom/a/b/d/rz;
    .registers 3

    .prologue
    .line 3291
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    .line 3292
    iget-object v1, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    if-ne v0, v1, :cond_b

    const/4 v0, 0x0

    :cond_b
    return-object v0
.end method

.method private a(Lcom/a/b/d/rz;)Z
    .registers 4

    .prologue
    .line 3280
    invoke-interface {p1}, Lcom/a/b/d/rz;->g()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 3283
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->g()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 3284
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-static {p1, v0}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 3286
    const/4 v0, 0x1

    return v0
.end method

.method private b()Lcom/a/b/d/rz;
    .registers 3

    .prologue
    .line 3297
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    .line 3298
    iget-object v1, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    if-ne v0, v1, :cond_c

    .line 3299
    const/4 v0, 0x0

    .line 3303
    :goto_b
    return-object v0

    .line 3302
    :cond_c
    invoke-virtual {p0, v0}, Lcom/a/b/d/rs;->remove(Ljava/lang/Object;)Z

    goto :goto_b
.end method


# virtual methods
.method public final clear()V
    .registers 3

    .prologue
    .line 3341
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    .line 3342
    :goto_6
    iget-object v1, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    if-eq v0, v1, :cond_13

    .line 3343
    invoke-interface {v0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v1

    .line 3344
    invoke-static {v0}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;)V

    move-object v0, v1

    .line 3346
    goto :goto_6

    .line 3348
    :cond_13
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    iget-object v1, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0, v1}, Lcom/a/b/d/rz;->a(Lcom/a/b/d/rz;)V

    .line 3349
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    iget-object v1, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0, v1}, Lcom/a/b/d/rz;->b(Lcom/a/b/d/rz;)V

    .line 3350
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3321
    check-cast p1, Lcom/a/b/d/rz;

    .line 3322
    invoke-interface {p1}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    sget-object v1, Lcom/a/b/d/ry;->a:Lcom/a/b/d/ry;

    if-eq v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final isEmpty()Z
    .registers 3

    .prologue
    .line 3327
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3354
    new-instance v0, Lcom/a/b/d/ru;

    invoke-direct {p0}, Lcom/a/b/d/rs;->a()Lcom/a/b/d/rz;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ru;-><init>(Lcom/a/b/d/rs;Lcom/a/b/d/rz;)V

    return-object v0
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3239
    check-cast p1, Lcom/a/b/d/rz;

    .line 5280
    invoke-interface {p1}, Lcom/a/b/d/rz;->g()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 5283
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->g()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 5284
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-static {p1, v0}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 5286
    const/4 v0, 0x1

    .line 3239
    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3239
    invoke-direct {p0}, Lcom/a/b/d/rs;->a()Lcom/a/b/d/rz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3239
    .line 4297
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    .line 4298
    iget-object v1, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    if-ne v0, v1, :cond_c

    .line 4299
    const/4 v0, 0x0

    :goto_b
    return-object v0

    .line 4302
    :cond_c
    invoke-virtual {p0, v0}, Lcom/a/b/d/rs;->remove(Ljava/lang/Object;)Z

    goto :goto_b
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3309
    check-cast p1, Lcom/a/b/d/rz;

    .line 3310
    invoke-interface {p1}, Lcom/a/b/d/rz;->g()Lcom/a/b/d/rz;

    move-result-object v0

    .line 3311
    invoke-interface {p1}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v1

    .line 3312
    invoke-static {v0, v1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 3313
    invoke-static {p1}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;)V

    .line 3315
    sget-object v0, Lcom/a/b/d/ry;->a:Lcom/a/b/d/ry;

    if-eq v1, v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final size()I
    .registers 4

    .prologue
    .line 3332
    const/4 v1, 0x0

    .line 3333
    iget-object v0, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    :goto_7
    iget-object v2, p0, Lcom/a/b/d/rs;->a:Lcom/a/b/d/rz;

    if-eq v0, v2, :cond_12

    .line 3334
    add-int/lit8 v1, v1, 0x1

    .line 3333
    invoke-interface {v0}, Lcom/a/b/d/rz;->f()Lcom/a/b/d/rz;

    move-result-object v0

    goto :goto_7

    .line 3336
    :cond_12
    return v1
.end method
