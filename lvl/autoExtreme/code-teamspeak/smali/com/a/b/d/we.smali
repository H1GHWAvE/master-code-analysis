.class public final Lcom/a/b/d/we;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/d/aac;)Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 679
    .line 2729
    instance-of v0, p0, Lcom/a/b/d/adr;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/lr;

    if-eqz v0, :cond_9

    .line 2731
    :cond_8
    :goto_8
    return-object p0

    .line 2733
    :cond_9
    new-instance v0, Lcom/a/b/d/adr;

    invoke-direct {v0, p0}, Lcom/a/b/d/adr;-><init>(Lcom/a/b/d/aac;)V

    move-object p0, v0

    .line 679
    goto :goto_8
.end method

.method public static a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
    .registers 5

    .prologue
    .line 1821
    instance-of v0, p0, Lcom/a/b/d/fy;

    if-eqz v0, :cond_17

    .line 1822
    check-cast p0, Lcom/a/b/d/fy;

    .line 1823
    new-instance v1, Lcom/a/b/d/fy;

    .line 5042
    iget-object v0, p0, Lcom/a/b/d/fy;->a:Lcom/a/b/d/vi;

    check-cast v0, Lcom/a/b/d/aac;

    .line 1823
    iget-object v2, p0, Lcom/a/b/d/fy;->b:Lcom/a/b/b/co;

    invoke-static {v2, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/fy;-><init>(Lcom/a/b/d/aac;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 1829
    :goto_16
    return-object v0

    .line 1825
    :cond_17
    instance-of v0, p0, Lcom/a/b/d/gc;

    if-eqz v0, :cond_26

    .line 1826
    check-cast p0, Lcom/a/b/d/gc;

    .line 1827
    invoke-static {p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/we;->a(Lcom/a/b/d/gc;Lcom/a/b/b/co;)Lcom/a/b/d/aac;

    move-result-object v0

    goto :goto_16

    .line 1829
    :cond_26
    new-instance v0, Lcom/a/b/d/fy;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/fy;-><init>(Lcom/a/b/d/aac;Lcom/a/b/b/co;)V

    goto :goto_16
.end method

.method private static a(Lcom/a/b/d/gc;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
    .registers 5

    .prologue
    .line 2042
    invoke-interface {p0}, Lcom/a/b/d/gc;->c()Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2044
    new-instance v1, Lcom/a/b/d/fs;

    invoke-interface {p0}, Lcom/a/b/d/gc;->d()Lcom/a/b/d/aac;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/fs;-><init>(Lcom/a/b/d/aac;Lcom/a/b/b/co;)V

    return-object v1
.end method

.method private static a(Lcom/a/b/d/lr;)Lcom/a/b/d/aac;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 717
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aac;

    return-object v0
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 923
    new-instance v0, Lcom/a/b/d/wr;

    invoke-direct {v0, p0}, Lcom/a/b/d/wr;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/abs;)Lcom/a/b/d/abs;
    .registers 2

    .prologue
    .line 734
    .line 2776
    instance-of v0, p0, Lcom/a/b/d/adu;

    if-eqz v0, :cond_5

    .line 2777
    :goto_4
    return-object p0

    .line 2779
    :cond_5
    new-instance v0, Lcom/a/b/d/adu;

    invoke-direct {v0, p0}, Lcom/a/b/d/adu;-><init>(Lcom/a/b/d/abs;)V

    move-object p0, v0

    .line 734
    goto :goto_4
.end method

.method private static a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Lcom/a/b/d/jr;
    .registers 6

    .prologue
    .line 1455
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 4503
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4504
    invoke-static {}, Lcom/a/b/d/jr;->c()Lcom/a/b/d/js;

    move-result-object v1

    .line 4506
    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 4507
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 4508
    invoke-static {v2, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4509
    invoke-interface {p1, v2}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_b

    .line 4511
    :cond_20
    invoke-virtual {v1}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 1455
    return-object v0
.end method

.method private static a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Lcom/a/b/d/jr;
    .registers 5

    .prologue
    .line 1503
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1504
    invoke-static {}, Lcom/a/b/d/jr;->c()Lcom/a/b/d/js;

    move-result-object v0

    .line 1506
    :goto_7
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 1507
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1508
    invoke-static {v1, p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1509
    invoke-interface {p1, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_7

    .line 1511
    :cond_1c
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/a/b/d/jr;)Lcom/a/b/d/ou;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 812
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    return-object v0
.end method

.method private static a(Lcom/a/b/d/ou;)Lcom/a/b/d/ou;
    .registers 2

    .prologue
    .line 774
    .line 3692
    instance-of v0, p0, Lcom/a/b/d/adh;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/jr;

    if-eqz v0, :cond_9

    .line 3694
    :cond_8
    :goto_8
    return-object p0

    .line 3696
    :cond_9
    new-instance v0, Lcom/a/b/d/adh;

    invoke-direct {v0, p0}, Lcom/a/b/d/adh;-><init>(Lcom/a/b/d/ou;)V

    move-object p0, v0

    .line 774
    goto :goto_8
.end method

.method private static a(Lcom/a/b/d/ou;Lcom/a/b/b/bj;)Lcom/a/b/d/ou;
    .registers 4

    .prologue
    .line 1320
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1321
    invoke-static {p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/tv;

    move-result-object v0

    .line 4380
    new-instance v1, Lcom/a/b/d/wu;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/wu;-><init>(Lcom/a/b/d/ou;Lcom/a/b/d/tv;)V

    .line 1322
    return-object v1
.end method

.method private static a(Lcom/a/b/d/ou;Lcom/a/b/b/co;)Lcom/a/b/d/ou;
    .registers 5

    .prologue
    .line 1865
    instance-of v0, p0, Lcom/a/b/d/ft;

    if-eqz v0, :cond_16

    .line 1866
    check-cast p0, Lcom/a/b/d/ft;

    .line 1867
    new-instance v0, Lcom/a/b/d/ft;

    invoke-virtual {p0}, Lcom/a/b/d/ft;->d()Lcom/a/b/d/ou;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/ft;->b:Lcom/a/b/b/co;

    invoke-static {v2, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ft;-><init>(Lcom/a/b/d/ou;Lcom/a/b/b/co;)V

    .line 1870
    :goto_15
    return-object v0

    :cond_16
    new-instance v0, Lcom/a/b/d/ft;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ft;-><init>(Lcom/a/b/d/ou;Lcom/a/b/b/co;)V

    goto :goto_15
.end method

.method private static a(Lcom/a/b/d/ou;Lcom/a/b/d/tv;)Lcom/a/b/d/ou;
    .registers 3

    .prologue
    .line 1380
    new-instance v0, Lcom/a/b/d/wu;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/wu;-><init>(Lcom/a/b/d/ou;Lcom/a/b/d/tv;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;Lcom/a/b/b/dz;)Lcom/a/b/d/ou;
    .registers 3

    .prologue
    .line 194
    new-instance v0, Lcom/a/b/d/wi;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/wi;-><init>(Ljava/util/Map;Lcom/a/b/b/dz;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/ga;Lcom/a/b/b/co;)Lcom/a/b/d/vi;
    .registers 5

    .prologue
    .line 2028
    invoke-interface {p0}, Lcom/a/b/d/ga;->c()Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2030
    new-instance v1, Lcom/a/b/d/fi;

    invoke-interface {p0}, Lcom/a/b/d/ga;->a()Lcom/a/b/d/vi;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/fi;-><init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V

    return-object v1
.end method

.method private static a(Lcom/a/b/d/kk;)Lcom/a/b/d/vi;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 488
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    return-object v0
.end method

.method private static a(Lcom/a/b/d/vi;)Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 451
    .line 2507
    instance-of v0, p0, Lcom/a/b/d/adj;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/kk;

    if-eqz v0, :cond_9

    .line 2509
    :cond_8
    :goto_8
    return-object p0

    .line 2511
    :cond_9
    new-instance v0, Lcom/a/b/d/adj;

    invoke-direct {v0, p0}, Lcom/a/b/d/adj;-><init>(Lcom/a/b/d/vi;)V

    move-object p0, v0

    .line 451
    goto :goto_8
.end method

.method private static a(Lcom/a/b/d/vi;Lcom/a/b/b/bj;)Lcom/a/b/d/vi;
    .registers 4

    .prologue
    .line 1109
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110
    invoke-static {p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/tv;

    move-result-object v0

    .line 4172
    new-instance v1, Lcom/a/b/d/wv;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/wv;-><init>(Lcom/a/b/d/vi;Lcom/a/b/d/tv;)V

    .line 1111
    return-object v1
.end method

.method private static a(Lcom/a/b/d/vi;Lcom/a/b/b/co;)Lcom/a/b/d/vi;
    .registers 5

    .prologue
    .line 1773
    instance-of v0, p0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_b

    .line 1774
    check-cast p0, Lcom/a/b/d/aac;

    invoke-static {p0, p1}, Lcom/a/b/d/we;->a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;

    move-result-object v0

    .line 4867
    :goto_a
    return-object v0

    .line 1775
    :cond_b
    instance-of v0, p0, Lcom/a/b/d/ou;

    if-eqz v0, :cond_2d

    .line 1776
    check-cast p0, Lcom/a/b/d/ou;

    .line 4865
    instance-of v0, p0, Lcom/a/b/d/ft;

    if-eqz v0, :cond_27

    .line 4866
    check-cast p0, Lcom/a/b/d/ft;

    .line 4867
    new-instance v0, Lcom/a/b/d/ft;

    invoke-virtual {p0}, Lcom/a/b/d/ft;->d()Lcom/a/b/d/ou;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/ft;->b:Lcom/a/b/b/co;

    invoke-static {v2, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ft;-><init>(Lcom/a/b/d/ou;Lcom/a/b/b/co;)V

    goto :goto_a

    .line 4870
    :cond_27
    new-instance v0, Lcom/a/b/d/ft;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ft;-><init>(Lcom/a/b/d/ou;Lcom/a/b/b/co;)V

    goto :goto_a

    .line 1777
    :cond_2d
    instance-of v0, p0, Lcom/a/b/d/fu;

    if-eqz v0, :cond_41

    .line 1778
    check-cast p0, Lcom/a/b/d/fu;

    .line 1779
    new-instance v0, Lcom/a/b/d/fu;

    iget-object v1, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    iget-object v2, p0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-static {v2, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/fu;-><init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V

    goto :goto_a

    .line 1781
    :cond_41
    instance-of v0, p0, Lcom/a/b/d/ga;

    if-eqz v0, :cond_50

    .line 1782
    check-cast p0, Lcom/a/b/d/ga;

    .line 1783
    invoke-static {p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/we;->a(Lcom/a/b/d/ga;Lcom/a/b/b/co;)Lcom/a/b/d/vi;

    move-result-object v0

    goto :goto_a

    .line 1785
    :cond_50
    new-instance v0, Lcom/a/b/d/fu;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/fu;-><init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V

    goto :goto_a
.end method

.method private static a(Lcom/a/b/d/vi;Lcom/a/b/d/tv;)Lcom/a/b/d/vi;
    .registers 3

    .prologue
    .line 1172
    new-instance v0, Lcom/a/b/d/wv;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/wv;-><init>(Lcom/a/b/d/vi;Lcom/a/b/d/tv;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/vi;Lcom/a/b/d/vi;)Lcom/a/b/d/vi;
    .registers 5

    .prologue
    .line 409
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    invoke-interface {p0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 411
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_b

    .line 413
    :cond_23
    return-object p1
.end method

.method static synthetic a(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 2

    .prologue
    .line 65
    .line 6825
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 6826
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    .line 6830
    :goto_a
    return-object v0

    .line 6827
    :cond_b
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_16

    .line 6828
    check-cast p0, Ljava/util/Set;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a

    .line 6829
    :cond_16
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_21

    .line 6830
    check-cast p0, Ljava/util/List;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_a

    .line 6832
    :cond_21
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_a
.end method

.method private static a(Lcom/a/b/d/vi;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048
    if-ne p1, p0, :cond_4

    .line 2049
    const/4 v0, 0x1

    .line 2055
    :goto_3
    return v0

    .line 2051
    :cond_4
    instance-of v0, p1, Lcom/a/b/d/vi;

    if-eqz v0, :cond_17

    .line 2052
    check-cast p1, Lcom/a/b/d/vi;

    .line 2053
    invoke-interface {p0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 2055
    :cond_17
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static b(Lcom/a/b/d/aac;)Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 702
    instance-of v0, p0, Lcom/a/b/d/xa;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/lr;

    if-eqz v0, :cond_9

    .line 706
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/xa;

    invoke-direct {v0, p0}, Lcom/a/b/d/xa;-><init>(Lcom/a/b/d/aac;)V

    move-object p0, v0

    goto :goto_8
.end method

.method private static b(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
    .registers 3

    .prologue
    .line 1941
    invoke-static {p1}, Lcom/a/b/d/sz;->b(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/we;->c(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Map;Lcom/a/b/b/dz;)Lcom/a/b/d/aac;
    .registers 3

    .prologue
    .line 272
    new-instance v0, Lcom/a/b/d/wk;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/wk;-><init>(Ljava/util/Map;Lcom/a/b/b/dz;)V

    return-object v0
.end method

.method private static b(Lcom/a/b/d/abs;)Lcom/a/b/d/abs;
    .registers 2

    .prologue
    .line 757
    instance-of v0, p0, Lcom/a/b/d/xb;

    if-eqz v0, :cond_5

    .line 760
    :goto_4
    return-object p0

    :cond_5
    new-instance v0, Lcom/a/b/d/xb;

    invoke-direct {v0, p0}, Lcom/a/b/d/xb;-><init>(Lcom/a/b/d/abs;)V

    move-object p0, v0

    goto :goto_4
.end method

.method private static b(Lcom/a/b/d/ou;)Lcom/a/b/d/ou;
    .registers 2

    .prologue
    .line 797
    instance-of v0, p0, Lcom/a/b/d/wx;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/jr;

    if-eqz v0, :cond_9

    .line 801
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/wx;

    invoke-direct {v0, p0}, Lcom/a/b/d/wx;-><init>(Lcom/a/b/d/ou;)V

    move-object p0, v0

    goto :goto_8
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 473
    instance-of v0, p0, Lcom/a/b/d/wy;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/kk;

    if-eqz v0, :cond_9

    .line 477
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v0, Lcom/a/b/d/wy;

    invoke-direct {v0, p0}, Lcom/a/b/d/wy;-><init>(Lcom/a/b/d/vi;)V

    move-object p0, v0

    goto :goto_8
.end method

.method private static b(Lcom/a/b/d/vi;Lcom/a/b/b/co;)Lcom/a/b/d/vi;
    .registers 5

    .prologue
    .line 1906
    invoke-static {p1}, Lcom/a/b/d/sz;->b(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 5974
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5975
    instance-of v0, p0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_12

    .line 5976
    check-cast p0, Lcom/a/b/d/aac;

    invoke-static {p0, v2}, Lcom/a/b/d/we;->c(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;

    move-result-object v0

    .line 5978
    :goto_11
    return-object v0

    :cond_12
    instance-of v0, p0, Lcom/a/b/d/ga;

    if-eqz v0, :cond_1d

    check-cast p0, Lcom/a/b/d/ga;

    invoke-static {p0, v2}, Lcom/a/b/d/we;->a(Lcom/a/b/d/ga;Lcom/a/b/b/co;)Lcom/a/b/d/vi;

    move-result-object v0

    goto :goto_11

    :cond_1d
    new-instance v1, Lcom/a/b/d/fi;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/fi;-><init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 1906
    goto :goto_11
.end method

.method private static b(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 2

    .prologue
    .line 825
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 826
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    .line 832
    :goto_a
    return-object v0

    .line 827
    :cond_b
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_16

    .line 828
    check-cast p0, Ljava/util/Set;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a

    .line 829
    :cond_16
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_21

    .line 830
    check-cast p0, Ljava/util/List;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_a

    .line 832
    :cond_21
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_a
.end method

.method private static c(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
    .registers 4

    .prologue
    .line 2013
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2014
    instance-of v0, p0, Lcom/a/b/d/gc;

    if-eqz v0, :cond_e

    check-cast p0, Lcom/a/b/d/gc;

    invoke-static {p0, p1}, Lcom/a/b/d/we;->a(Lcom/a/b/d/gc;Lcom/a/b/b/co;)Lcom/a/b/d/aac;

    move-result-object v0

    :goto_d
    return-object v0

    :cond_e
    new-instance v1, Lcom/a/b/d/fs;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aac;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/fs;-><init>(Lcom/a/b/d/aac;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_d
.end method

.method private static c(Lcom/a/b/d/vi;Lcom/a/b/b/co;)Lcom/a/b/d/vi;
    .registers 4

    .prologue
    .line 1974
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1975
    instance-of v0, p0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_e

    .line 1976
    check-cast p0, Lcom/a/b/d/aac;

    invoke-static {p0, p1}, Lcom/a/b/d/we;->c(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;

    move-result-object v0

    .line 1978
    :goto_d
    return-object v0

    :cond_e
    instance-of v0, p0, Lcom/a/b/d/ga;

    if-eqz v0, :cond_19

    check-cast p0, Lcom/a/b/d/ga;

    invoke-static {p0, p1}, Lcom/a/b/d/we;->a(Lcom/a/b/d/ga;Lcom/a/b/b/co;)Lcom/a/b/d/vi;

    move-result-object v0

    goto :goto_d

    :cond_19
    new-instance v1, Lcom/a/b/d/fi;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/fi;-><init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_d
.end method

.method private static c(Ljava/util/Map;Lcom/a/b/b/dz;)Lcom/a/b/d/vi;
    .registers 3

    .prologue
    .line 113
    new-instance v0, Lcom/a/b/d/wj;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/wj;-><init>(Ljava/util/Map;Lcom/a/b/b/dz;)V

    return-object v0
.end method

.method private static c(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 846
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_b

    .line 847
    check-cast p0, Ljava/util/Set;

    invoke-static {p0}, Lcom/a/b/d/sz;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 849
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/uw;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/uw;-><init>(Ljava/util/Collection;)V

    goto :goto_a
.end method

.method private static c(Lcom/a/b/d/aac;)Ljava/util/Map;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 876
    invoke-interface {p0}, Lcom/a/b/d/aac;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/a/b/d/abs;)Ljava/util/Map;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 891
    invoke-interface {p0}, Lcom/a/b/d/abs;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/a/b/d/ou;)Ljava/util/Map;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 863
    invoke-interface {p0}, Lcom/a/b/d/ou;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/a/b/d/vi;)Ljava/util/Map;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 902
    invoke-interface {p0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/util/Map;Lcom/a/b/b/dz;)Lcom/a/b/d/abs;
    .registers 3

    .prologue
    .line 350
    new-instance v0, Lcom/a/b/d/wl;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/wl;-><init>(Ljava/util/Map;Lcom/a/b/b/dz;)V

    return-object v0
.end method

.method private static synthetic d(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 65
    .line 6846
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_b

    .line 6847
    check-cast p0, Ljava/util/Set;

    invoke-static {p0}, Lcom/a/b/d/sz;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    :goto_a
    return-object v0

    .line 6849
    :cond_b
    new-instance v0, Lcom/a/b/d/uw;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/uw;-><init>(Ljava/util/Collection;)V

    goto :goto_a
.end method
