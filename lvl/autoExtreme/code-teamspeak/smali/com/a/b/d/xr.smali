.class abstract Lcom/a/b/d/xr;
.super Lcom/a/b/d/aan;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 964
    invoke-direct {p0}, Lcom/a/b/d/aan;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lcom/a/b/d/xc;
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 1002
    invoke-virtual {p0}, Lcom/a/b/d/xr;->a()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/xc;->clear()V

    .line 1003
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 968
    instance-of v1, p1, Lcom/a/b/d/xd;

    if-eqz v1, :cond_d

    .line 973
    check-cast p1, Lcom/a/b/d/xd;

    .line 974
    invoke-interface {p1}, Lcom/a/b/d/xd;->b()I

    move-result v1

    if-gtz v1, :cond_e

    .line 981
    :cond_d
    :goto_d
    return v0

    .line 977
    :cond_e
    invoke-virtual {p0}, Lcom/a/b/d/xr;->a()Lcom/a/b/d/xc;

    move-result-object v1

    invoke-interface {p1}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v1

    .line 978
    invoke-interface {p1}, Lcom/a/b/d/xd;->b()I

    move-result v2

    if-ne v1, v2, :cond_d

    const/4 v0, 0x1

    goto :goto_d
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 987
    instance-of v1, p1, Lcom/a/b/d/xd;

    if-eqz v1, :cond_19

    .line 988
    check-cast p1, Lcom/a/b/d/xd;

    .line 989
    invoke-interface {p1}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 990
    invoke-interface {p1}, Lcom/a/b/d/xd;->b()I

    move-result v2

    .line 991
    if-eqz v2, :cond_19

    .line 994
    invoke-virtual {p0}, Lcom/a/b/d/xr;->a()Lcom/a/b/d/xc;

    move-result-object v3

    .line 995
    invoke-interface {v3, v1, v2, v0}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;II)Z

    move-result v0

    .line 998
    :cond_19
    return v0
.end method
