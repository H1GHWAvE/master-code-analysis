.class final Lcom/a/b/d/ya;
.super Lcom/a/b/d/yd;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field private static final b:J


# instance fields
.field final a:Lcom/a/b/d/yd;


# direct methods
.method constructor <init>(Lcom/a/b/d/yd;)V
    .registers 2

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/a/b/d/yd;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/a/b/d/ya;->a:Lcom/a/b/d/yd;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/b/d/ya;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->c()Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/a/b/d/yd;
    .registers 1

    .prologue
    .line 54
    return-object p0
.end method

.method public final c()Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/a/b/d/ya;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0}, Lcom/a/b/d/yd;->c()Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 35
    if-ne p1, p2, :cond_4

    .line 36
    const/4 v0, 0x0

    .line 44
    :goto_3
    return v0

    .line 38
    :cond_4
    if-nez p1, :cond_8

    .line 39
    const/4 v0, -0x1

    goto :goto_3

    .line 41
    :cond_8
    if-nez p2, :cond_c

    .line 42
    const/4 v0, 0x1

    goto :goto_3

    .line 44
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/ya;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_3
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62
    if-ne p1, p0, :cond_4

    .line 63
    const/4 v0, 0x1

    .line 69
    :goto_3
    return v0

    .line 65
    :cond_4
    instance-of v0, p1, Lcom/a/b/d/ya;

    if-eqz v0, :cond_13

    .line 66
    check-cast p1, Lcom/a/b/d/ya;

    .line 67
    iget-object v0, p0, Lcom/a/b/d/ya;->a:Lcom/a/b/d/yd;

    iget-object v1, p1, Lcom/a/b/d/ya;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 69
    :cond_13
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/a/b/d/ya;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0x39153a74

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/a/b/d/ya;->a:Lcom/a/b/d/yd;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nullsFirst()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
