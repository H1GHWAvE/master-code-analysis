.class final Lcom/a/b/d/aao;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/jt;


# direct methods
.method constructor <init>(Ljava/util/Set;)V
    .registers 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1279
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 1280
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v4

    .line 1282
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v0, v1

    :goto_14
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 1283
    add-int/lit8 v2, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move v0, v2

    .line 1284
    goto :goto_14

    .line 1285
    :cond_29
    invoke-virtual {v4}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    .line 1286
    iget-object v0, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    const/16 v2, 0x1e

    if-gt v0, v2, :cond_4e

    move v0, v3

    :goto_3a
    const-string v2, "Too many elements to create power set: %s > 30"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    invoke-virtual {v4}, Lcom/a/b/d/jt;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1288
    return-void

    :cond_4e
    move v0, v1

    .line 1286
    goto :goto_3a
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1307
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_11

    .line 1308
    check-cast p1, Ljava/util/Set;

    .line 1309
    iget-object v0, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/lo;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    .line 1311
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1315
    instance-of v0, p1, Lcom/a/b/d/aao;

    if-eqz v0, :cond_f

    .line 1316
    check-cast p1, Lcom/a/b/d/aao;

    .line 1317
    iget-object v0, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    iget-object v1, p1, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, v1}, Lcom/a/b/d/jt;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1319
    :goto_e
    return v0

    :cond_f
    invoke-super {p0, p1}, Ljava/util/AbstractSet;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    shl-int/2addr v0, v1

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 1295
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 1299
    new-instance v0, Lcom/a/b/d/aap;

    invoke-virtual {p0}, Lcom/a/b/d/aao;->size()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/aap;-><init>(Lcom/a/b/d/aao;I)V

    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 1291
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->size()I

    move-result v1

    shl-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/a/b/d/aao;->a:Lcom/a/b/d/jt;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "powerSet("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
