.class public final Lcom/a/b/d/vc;
.super Ljava/util/AbstractQueue;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final g:I = 0x55555555

.field private static final h:I = -0x55555556

.field private static final i:I = 0xb


# instance fields
.field final a:I
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field b:[Ljava/lang/Object;

.field private final c:Lcom/a/b/d/vf;

.field private final d:Lcom/a/b/d/vf;

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>(Lcom/a/b/d/ve;I)V
    .registers 5

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3216
    iget-object v0, p1, Lcom/a/b/d/ve;->a:Ljava/util/Comparator;

    invoke-static {v0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    .line 229
    new-instance v1, Lcom/a/b/d/vf;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/vf;-><init>(Lcom/a/b/d/vc;Lcom/a/b/d/yd;)V

    iput-object v1, p0, Lcom/a/b/d/vc;->c:Lcom/a/b/d/vf;

    .line 230
    new-instance v1, Lcom/a/b/d/vf;

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/vf;-><init>(Lcom/a/b/d/vc;Lcom/a/b/d/yd;)V

    iput-object v1, p0, Lcom/a/b/d/vc;->d:Lcom/a/b/d/vf;

    .line 231
    iget-object v0, p0, Lcom/a/b/d/vc;->c:Lcom/a/b/d/vf;

    iget-object v1, p0, Lcom/a/b/d/vc;->d:Lcom/a/b/d/vf;

    iput-object v1, v0, Lcom/a/b/d/vf;->b:Lcom/a/b/d/vf;

    .line 232
    iget-object v0, p0, Lcom/a/b/d/vc;->d:Lcom/a/b/d/vf;

    iget-object v1, p0, Lcom/a/b/d/vc;->c:Lcom/a/b/d/vf;

    iput-object v1, v0, Lcom/a/b/d/vf;->b:Lcom/a/b/d/vf;

    .line 4155
    iget v0, p1, Lcom/a/b/d/ve;->c:I

    .line 234
    iput v0, p0, Lcom/a/b/d/vc;->a:I

    .line 236
    new-array v0, p2, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 237
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/ve;IB)V
    .registers 4

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/vc;-><init>(Lcom/a/b/d/ve;I)V

    return-void
.end method

.method private static a(II)I
    .registers 3

    .prologue
    .line 937
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static a(IILjava/lang/Iterable;)I
    .registers 4
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 903
    const/4 v0, -0x1

    if-ne p0, v0, :cond_5

    const/16 p0, 0xb

    .line 908
    :cond_5
    instance-of v0, p2, Ljava/util/Collection;

    if-eqz v0, :cond_13

    .line 909
    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    .line 910
    invoke-static {p0, v0}, Ljava/lang/Math;->max(II)I

    move-result p0

    .line 914
    :cond_13
    invoke-static {p0, p1}, Lcom/a/b/d/vc;->a(II)I

    move-result v0

    return v0
.end method

.method private static a()Lcom/a/b/d/vc;
    .registers 3

    .prologue
    .line 98
    new-instance v0, Lcom/a/b/d/ve;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ve;-><init>(Ljava/util/Comparator;B)V

    .line 1197
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/ve;->a(Ljava/lang/Iterable;)Lcom/a/b/d/vc;

    move-result-object v0

    .line 98
    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/vc;
    .registers 4

    .prologue
    .line 107
    new-instance v0, Lcom/a/b/d/ve;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ve;-><init>(Ljava/util/Comparator;B)V

    invoke-virtual {v0, p0}, Lcom/a/b/d/ve;->a(Ljava/lang/Iterable;)Lcom/a/b/d/vc;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Comparator;)Lcom/a/b/d/ve;
    .registers 3

    .prologue
    .line 116
    new-instance v0, Lcom/a/b/d/ve;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ve;-><init>(Ljava/util/Comparator;B)V

    return-object v0
.end method

.method private a(ILjava/lang/Object;)Lcom/a/b/d/vg;
    .registers 9

    .prologue
    const/4 v1, 0x0

    .line 411
    invoke-direct {p0, p1}, Lcom/a/b/d/vc;->f(I)Lcom/a/b/d/vf;

    move-result-object v4

    move v3, p1

    .line 28726
    :goto_6
    mul-int/lit8 v0, v3, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 28599
    if-gez v0, :cond_1d

    .line 28600
    const/4 v0, -0x1

    .line 27698
    :goto_d
    if-lez v0, :cond_27

    .line 27699
    iget-object v2, v4, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 30091
    iget-object v2, v2, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 27699
    iget-object v5, v4, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 30290
    iget-object v5, v5, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v5, v5, v0

    .line 27699
    aput-object v5, v2, v3

    move v3, v0

    .line 27700
    goto :goto_6

    .line 29726
    :cond_1d
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 28602
    const/4 v2, 0x4

    invoke-virtual {v4, v0, v2}, Lcom/a/b/d/vf;->b(II)I

    move-result v0

    goto :goto_d

    .line 421
    :cond_27
    invoke-virtual {v4, v3, p2}, Lcom/a/b/d/vf;->a(ILjava/lang/Object;)I

    move-result v0

    .line 422
    if-ne v0, v3, :cond_85

    .line 31726
    mul-int/lit8 v0, v3, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 31591
    const/4 v2, 0x2

    invoke-virtual {v4, v0, v2}, Lcom/a/b/d/vf;->b(II)I

    move-result v0

    .line 30679
    if-lez v0, :cond_72

    iget-object v2, v4, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    iget-object v5, v4, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 32290
    iget-object v5, v5, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v5, v5, v0

    .line 30679
    invoke-virtual {v2, v5, p2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_72

    .line 30681
    iget-object v2, v4, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 33091
    iget-object v2, v2, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 30681
    iget-object v5, v4, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 33290
    iget-object v5, v5, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v5, v5, v0

    .line 30681
    aput-object v5, v2, v3

    .line 30682
    iget-object v2, v4, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 34091
    iget-object v2, v2, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 30682
    aput-object p2, v2, v0

    move v2, v0

    .line 30508
    :goto_59
    if-eq v2, v3, :cond_83

    .line 30517
    if-ge v2, p1, :cond_78

    .line 30520
    iget-object v0, v4, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 34290
    iget-object v0, v0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v0, p1

    .line 30525
    :goto_63
    iget-object v3, v4, Lcom/a/b/d/vf;->b:Lcom/a/b/d/vf;

    invoke-virtual {v3, v2, p2}, Lcom/a/b/d/vf;->a(ILjava/lang/Object;)I

    move-result v2

    if-ge v2, p1, :cond_83

    .line 30527
    new-instance v1, Lcom/a/b/d/vg;

    invoke-direct {v1, p2, v0}, Lcom/a/b/d/vg;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    :goto_71
    return-object v0

    .line 30685
    :cond_72
    invoke-virtual {v4, v3, p2}, Lcom/a/b/d/vf;->b(ILjava/lang/Object;)I

    move-result v0

    move v2, v0

    goto :goto_59

    .line 30522
    :cond_78
    iget-object v0, v4, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 34734
    add-int/lit8 v3, p1, -0x1

    div-int/lit8 v3, v3, 0x2

    .line 35290
    iget-object v0, v0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v0, v3

    goto :goto_63

    :cond_83
    move-object v0, v1

    .line 426
    goto :goto_71

    .line 428
    :cond_85
    if-ge v0, p1, :cond_91

    new-instance v0, Lcom/a/b/d/vg;

    .line 36290
    iget-object v1, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v1, p1

    .line 428
    invoke-direct {v0, p2, v1}, Lcom/a/b/d/vg;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_71

    :cond_91
    move-object v0, v1

    goto :goto_71
.end method

.method static synthetic a(Lcom/a/b/d/vc;)[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    return-object v0
.end method

.method private b()I
    .registers 4

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 301
    iget v2, p0, Lcom/a/b/d/vc;->e:I

    packed-switch v2, :pswitch_data_14

    .line 309
    iget-object v2, p0, Lcom/a/b/d/vc;->d:Lcom/a/b/d/vf;

    invoke-virtual {v2, v0, v1}, Lcom/a/b/d/vf;->a(II)I

    move-result v2

    if-gtz v2, :cond_12

    :goto_f
    :pswitch_f
    return v0

    .line 303
    :pswitch_10
    const/4 v0, 0x0

    goto :goto_f

    :cond_12
    move v0, v1

    .line 309
    goto :goto_f

    .line 301
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
    .end packed-switch
.end method

.method static synthetic b(Lcom/a/b/d/vc;)I
    .registers 2

    .prologue
    .line 91
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    return v0
.end method

.method private static b(I)Lcom/a/b/d/ve;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 125
    new-instance v1, Lcom/a/b/d/ve;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/ve;-><init>(Ljava/util/Comparator;B)V

    .line 2175
    if-ltz p0, :cond_d

    const/4 v0, 0x1

    :cond_d
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 2176
    iput p0, v1, Lcom/a/b/d/ve;->b:I

    .line 125
    return-object v1
.end method

.method static synthetic c(Lcom/a/b/d/vc;)I
    .registers 2

    .prologue
    .line 91
    iget v0, p0, Lcom/a/b/d/vc;->f:I

    return v0
.end method

.method private static c(I)Lcom/a/b/d/ve;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 137
    new-instance v1, Lcom/a/b/d/ve;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/ve;-><init>(Ljava/util/Comparator;B)V

    .line 2187
    if-lez p0, :cond_d

    const/4 v0, 0x1

    :cond_d
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 2188
    iput p0, v1, Lcom/a/b/d/ve;->c:I

    .line 137
    return-object v1
.end method

.method private c()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/a/b/d/vc;->poll()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/a/b/d/vc;->remove()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private d(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 290
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/a/b/d/vc;->peek()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private e(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 449
    .line 37290
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v0, p1

    .line 450
    invoke-virtual {p0, p1}, Lcom/a/b/d/vc;->a(I)Lcom/a/b/d/vg;

    .line 451
    return-object v0
.end method

.method private f(I)Lcom/a/b/d/vf;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 455
    .line 37462
    add-int/lit8 v3, p1, 0x1

    .line 37463
    if-lez v3, :cond_1b

    move v0, v1

    :goto_7
    const-string v4, "negative index"

    invoke-static {v0, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 37464
    const v0, 0x55555555

    and-int/2addr v0, v3

    const v4, -0x55555556

    and-int/2addr v3, v4

    if-le v0, v3, :cond_1d

    .line 455
    :goto_16
    if-eqz v1, :cond_1f

    iget-object v0, p0, Lcom/a/b/d/vc;->c:Lcom/a/b/d/vf;

    :goto_1a
    return-object v0

    :cond_1b
    move v0, v2

    .line 37463
    goto :goto_7

    :cond_1d
    move v1, v2

    .line 37464
    goto :goto_16

    .line 455
    :cond_1f
    iget-object v0, p0, Lcom/a/b/d/vc;->d:Lcom/a/b/d/vf;

    goto :goto_1a
.end method

.method private f()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/a/b/d/vc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-direct {p0}, Lcom/a/b/d/vc;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/a/b/d/vc;->e(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_7
.end method

.method private g()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/a/b/d/vc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 353
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 355
    :cond_c
    invoke-direct {p0}, Lcom/a/b/d/vc;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/a/b/d/vc;->e(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static g(I)Z
    .registers 6
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 462
    add-int/lit8 v3, p0, 0x1

    .line 463
    if-lez v3, :cond_17

    move v0, v1

    :goto_7
    const-string v4, "negative index"

    invoke-static {v0, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 464
    const v0, 0x55555555

    and-int/2addr v0, v3

    const v4, -0x55555556

    and-int/2addr v3, v4

    if-le v0, v3, :cond_19

    :goto_16
    return v1

    :cond_17
    move v0, v2

    .line 463
    goto :goto_7

    :cond_19
    move v1, v2

    .line 464
    goto :goto_16
.end method

.method private h()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/a/b/d/vc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-direct {p0}, Lcom/a/b/d/vc;->b()I

    move-result v0

    .line 8290
    iget-object v1, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v1, v0

    goto :goto_7
.end method

.method private i()Z
    .registers 7
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 474
    move v0, v1

    :goto_3
    iget v3, p0, Lcom/a/b/d/vc;->e:I

    if-ge v0, v3, :cond_23

    .line 475
    invoke-direct {p0, v0}, Lcom/a/b/d/vc;->f(I)Lcom/a/b/d/vf;

    move-result-object v3

    .line 37726
    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x1

    .line 37706
    iget-object v5, v3, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 38091
    iget v5, v5, Lcom/a/b/d/vc;->e:I

    .line 37706
    if-ge v4, v5, :cond_24

    .line 38726
    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x1

    .line 37706
    invoke-virtual {v3, v0, v4}, Lcom/a/b/d/vf;->a(II)I

    move-result v4

    if-lez v4, :cond_24

    move v3, v2

    .line 475
    :goto_20
    if-nez v3, :cond_5d

    move v1, v2

    .line 479
    :cond_23
    return v1

    .line 38730
    :cond_24
    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x2

    .line 37710
    iget-object v5, v3, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 39091
    iget v5, v5, Lcom/a/b/d/vc;->e:I

    .line 37710
    if-ge v4, v5, :cond_3a

    .line 39730
    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x2

    .line 37710
    invoke-virtual {v3, v0, v4}, Lcom/a/b/d/vf;->a(II)I

    move-result v4

    if-lez v4, :cond_3a

    move v3, v2

    .line 37712
    goto :goto_20

    .line 37714
    :cond_3a
    if-lez v0, :cond_48

    .line 39734
    add-int/lit8 v4, v0, -0x1

    div-int/lit8 v4, v4, 0x2

    .line 37714
    invoke-virtual {v3, v0, v4}, Lcom/a/b/d/vf;->a(II)I

    move-result v4

    if-lez v4, :cond_48

    move v3, v2

    .line 37715
    goto :goto_20

    .line 37717
    :cond_48
    const/4 v4, 0x2

    if-le v0, v4, :cond_5b

    .line 40734
    add-int/lit8 v4, v0, -0x1

    div-int/lit8 v4, v4, 0x2

    .line 41734
    add-int/lit8 v4, v4, -0x1

    div-int/lit8 v4, v4, 0x2

    .line 37717
    invoke-virtual {v3, v4, v0}, Lcom/a/b/d/vf;->a(II)I

    move-result v3

    if-lez v3, :cond_5b

    move v3, v2

    .line 37718
    goto :goto_20

    :cond_5b
    move v3, v1

    .line 37720
    goto :goto_20

    .line 474
    :cond_5d
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private j()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 889
    iget-object v0, p0, Lcom/a/b/d/vc;->c:Lcom/a/b/d/vf;

    iget-object v0, v0, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    return-object v0
.end method

.method private k()I
    .registers 2
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 893
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method

.method private l()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 918
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    iget-object v1, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    array-length v1, v1

    if-le v0, v1, :cond_25

    .line 41928
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    array-length v0, v0

    .line 41929
    const/16 v1, 0x40

    if-ge v0, v1, :cond_26

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x2

    .line 41932
    :goto_13
    iget v1, p0, Lcom/a/b/d/vc;->a:I

    invoke-static {v0, v1}, Lcom/a/b/d/vc;->a(II)I

    move-result v0

    .line 920
    new-array v0, v0, [Ljava/lang/Object;

    .line 921
    iget-object v1, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 922
    iput-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 924
    :cond_25
    return-void

    .line 41929
    :cond_26
    div-int/lit8 v0, v0, 0x2

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/a/b/j/g;->b(II)I

    move-result v0

    goto :goto_13
.end method

.method private m()I
    .registers 3

    .prologue
    .line 928
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    array-length v0, v0

    .line 929
    const/16 v1, 0x40

    if-ge v0, v1, :cond_12

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x2

    .line 932
    :goto_b
    iget v1, p0, Lcom/a/b/d/vc;->a:I

    invoke-static {v0, v1}, Lcom/a/b/d/vc;->a(II)I

    move-result v0

    return v0

    .line 929
    :cond_12
    div-int/lit8 v0, v0, 0x2

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/a/b/j/g;->b(II)I

    move-result v0

    goto :goto_b
.end method


# virtual methods
.method final a(I)Lcom/a/b/d/vg;
    .registers 11
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 382
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->b(II)I

    .line 383
    iget v0, p0, Lcom/a/b/d/vc;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/vc;->f:I

    .line 384
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/vc;->e:I

    .line 385
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    if-ne v0, p1, :cond_1e

    .line 386
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/a/b/d/vc;->e:I

    aput-object v2, v0, v1

    move-object v0, v2

    .line 407
    :goto_1d
    return-object v0

    .line 389
    :cond_1e
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    .line 9290
    iget-object v1, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v5, v1, v0

    .line 390
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    invoke-direct {p0, v0}, Lcom/a/b/d/vc;->f(I)Lcom/a/b/d/vf;

    move-result-object v1

    .line 9652
    iget-object v0, v1, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 10091
    iget v0, v0, Lcom/a/b/d/vc;->e:I

    .line 10734
    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v3, v0, 0x2

    .line 9653
    if-eqz v3, :cond_8e

    .line 11734
    add-int/lit8 v0, v3, -0x1

    div-int/lit8 v0, v0, 0x2

    .line 12730
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x2

    .line 9656
    if-eq v0, v3, :cond_8e

    .line 13726
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    .line 9656
    iget-object v4, v1, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 14091
    iget v4, v4, Lcom/a/b/d/vc;->e:I

    .line 9656
    if-lt v3, v4, :cond_8e

    .line 9658
    iget-object v3, v1, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 14290
    iget-object v3, v3, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v3, v3, v0

    .line 9659
    iget-object v4, v1, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    invoke-virtual {v4, v3, v5}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_8e

    .line 9660
    iget-object v4, v1, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 15091
    iget-object v4, v4, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 9660
    aput-object v5, v4, v0

    .line 9661
    iget-object v4, v1, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 16091
    iget-object v4, v4, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 9661
    iget-object v1, v1, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 17091
    iget v1, v1, Lcom/a/b/d/vc;->e:I

    .line 9661
    aput-object v3, v4, v1

    .line 392
    :goto_66
    iget v1, p0, Lcom/a/b/d/vc;->e:I

    .line 18290
    iget-object v3, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v6, v3, v1

    .line 393
    iget-object v1, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    iget v3, p0, Lcom/a/b/d/vc;->e:I

    aput-object v2, v1, v3

    .line 18411
    invoke-direct {p0, p1}, Lcom/a/b/d/vc;->f(I)Lcom/a/b/d/vf;

    move-result-object v7

    move v4, p1

    .line 19726
    :goto_77
    mul-int/lit8 v1, v4, 0x2

    add-int/lit8 v1, v1, 0x1

    .line 19599
    if-gez v1, :cond_93

    .line 19600
    const/4 v1, -0x1

    .line 18698
    :goto_7e
    if-lez v1, :cond_9d

    .line 18699
    iget-object v3, v7, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 21091
    iget-object v3, v3, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 18699
    iget-object v8, v7, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 21290
    iget-object v8, v8, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v8, v8, v1

    .line 18699
    aput-object v8, v3, v4

    move v4, v1

    .line 18700
    goto :goto_77

    .line 9666
    :cond_8e
    iget-object v0, v1, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 18091
    iget v0, v0, Lcom/a/b/d/vc;->e:I

    goto :goto_66

    .line 20726
    :cond_93
    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    .line 19602
    const/4 v3, 0x4

    invoke-virtual {v7, v1, v3}, Lcom/a/b/d/vf;->b(II)I

    move-result v1

    goto :goto_7e

    .line 18421
    :cond_9d
    invoke-virtual {v7, v4, v6}, Lcom/a/b/d/vf;->a(ILjava/lang/Object;)I

    move-result v1

    .line 18422
    if-ne v1, v4, :cond_105

    .line 22726
    mul-int/lit8 v1, v4, 0x2

    add-int/lit8 v1, v1, 0x1

    .line 22591
    const/4 v3, 0x2

    invoke-virtual {v7, v1, v3}, Lcom/a/b/d/vf;->b(II)I

    move-result v1

    .line 21679
    if-lez v1, :cond_f2

    iget-object v3, v7, Lcom/a/b/d/vf;->a:Lcom/a/b/d/yd;

    iget-object v8, v7, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 23290
    iget-object v8, v8, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v8, v8, v1

    .line 21679
    invoke-virtual {v3, v8, v6}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_f2

    .line 21681
    iget-object v3, v7, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 24091
    iget-object v3, v3, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 21681
    iget-object v8, v7, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 24290
    iget-object v8, v8, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v8, v8, v1

    .line 21681
    aput-object v8, v3, v4

    .line 21682
    iget-object v3, v7, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 25091
    iget-object v3, v3, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 21682
    aput-object v6, v3, v1

    move v3, v1

    .line 21508
    :goto_cf
    if-eq v3, v4, :cond_103

    .line 21517
    if-ge v3, p1, :cond_f8

    .line 21520
    iget-object v1, v7, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 25290
    iget-object v1, v1, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v1, p1

    .line 21525
    :goto_d9
    iget-object v4, v7, Lcom/a/b/d/vf;->b:Lcom/a/b/d/vf;

    invoke-virtual {v4, v3, v6}, Lcom/a/b/d/vf;->a(ILjava/lang/Object;)I

    move-result v3

    if-ge v3, p1, :cond_103

    .line 21527
    new-instance v2, Lcom/a/b/d/vg;

    invoke-direct {v2, v6, v1}, Lcom/a/b/d/vg;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v1, v2

    .line 395
    :goto_e7
    if-ge v0, p1, :cond_11d

    .line 397
    if-nez v1, :cond_114

    .line 399
    new-instance v0, Lcom/a/b/d/vg;

    invoke-direct {v0, v5, v6}, Lcom/a/b/d/vg;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1d

    .line 21685
    :cond_f2
    invoke-virtual {v7, v4, v6}, Lcom/a/b/d/vf;->b(ILjava/lang/Object;)I

    move-result v1

    move v3, v1

    goto :goto_cf

    .line 21522
    :cond_f8
    iget-object v1, v7, Lcom/a/b/d/vf;->c:Lcom/a/b/d/vc;

    .line 25734
    add-int/lit8 v4, p1, -0x1

    div-int/lit8 v4, v4, 0x2

    .line 26290
    iget-object v1, v1, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v1, v4

    goto :goto_d9

    :cond_103
    move-object v1, v2

    .line 18426
    goto :goto_e7

    .line 18428
    :cond_105
    if-ge v1, p1, :cond_112

    new-instance v2, Lcom/a/b/d/vg;

    .line 27290
    iget-object v1, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v1, p1

    .line 18428
    invoke-direct {v2, v6, v1}, Lcom/a/b/d/vg;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v1, v2

    goto :goto_e7

    :cond_112
    move-object v1, v2

    goto :goto_e7

    .line 403
    :cond_114
    new-instance v0, Lcom/a/b/d/vg;

    iget-object v1, v1, Lcom/a/b/d/vg;->b:Ljava/lang/Object;

    invoke-direct {v0, v5, v1}, Lcom/a/b/d/vg;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1d

    :cond_11d
    move-object v0, v1

    .line 407
    goto/16 :goto_1d
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 252
    invoke-virtual {p0, p1}, Lcom/a/b/d/vc;->offer(Ljava/lang/Object;)Z

    .line 253
    const/4 v0, 0x1

    return v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 5

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 258
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 259
    invoke-virtual {p0, v0}, Lcom/a/b/d/vc;->offer(Ljava/lang/Object;)Z

    .line 260
    const/4 v0, 0x1

    .line 261
    goto :goto_5

    .line 262
    :cond_14
    return v0
.end method

.method public final clear()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 871
    move v0, v1

    :goto_2
    iget v2, p0, Lcom/a/b/d/vc;->e:I

    if-ge v0, v2, :cond_e

    .line 872
    iget-object v2, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 871
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 874
    :cond_e
    iput v1, p0, Lcom/a/b/d/vc;->e:I

    .line 875
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 867
    new-instance v0, Lcom/a/b/d/vh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/vh;-><init>(Lcom/a/b/d/vc;B)V

    return-object v0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 272
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    iget v0, p0, Lcom/a/b/d/vc;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/vc;->f:I

    .line 274
    iget v1, p0, Lcom/a/b/d/vc;->e:I

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Lcom/a/b/d/vc;->e:I

    .line 4918
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    iget-object v2, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    array-length v2, v2

    if-le v0, v2, :cond_34

    .line 4928
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    array-length v0, v0

    .line 4929
    const/16 v2, 0x40

    if-ge v0, v2, :cond_52

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x2

    .line 4932
    :goto_22
    iget v2, p0, Lcom/a/b/d/vc;->a:I

    invoke-static {v0, v2}, Lcom/a/b/d/vc;->a(II)I

    move-result v0

    .line 4920
    new-array v0, v0, [Ljava/lang/Object;

    .line 4921
    iget-object v2, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v2, v3, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4922
    iput-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    .line 280
    :cond_34
    invoke-direct {p0, v1}, Lcom/a/b/d/vc;->f(I)Lcom/a/b/d/vf;

    move-result-object v0

    .line 5537
    invoke-virtual {v0, v1, p1}, Lcom/a/b/d/vf;->b(ILjava/lang/Object;)I

    move-result v2

    .line 5540
    if-ne v2, v1, :cond_5a

    .line 5546
    :goto_3e
    invoke-virtual {v0, v1, p1}, Lcom/a/b/d/vf;->a(ILjava/lang/Object;)I

    .line 281
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    iget v1, p0, Lcom/a/b/d/vc;->a:I

    if-le v0, v1, :cond_50

    .line 6343
    invoke-virtual {p0}, Lcom/a/b/d/vc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5e

    const/4 v0, 0x0

    .line 281
    :goto_4e
    if-eq v0, p1, :cond_67

    :cond_50
    const/4 v0, 0x1

    :goto_51
    return v0

    .line 4929
    :cond_52
    div-int/lit8 v0, v0, 0x2

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/a/b/j/g;->b(II)I

    move-result v0

    goto :goto_22

    .line 5544
    :cond_5a
    iget-object v0, v0, Lcom/a/b/d/vf;->b:Lcom/a/b/d/vf;

    move v1, v2

    goto :goto_3e

    .line 6343
    :cond_5e
    invoke-direct {p0}, Lcom/a/b/d/vc;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/a/b/d/vc;->e(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_4e

    :cond_67
    move v0, v3

    .line 281
    goto :goto_51
.end method

.method public final peek()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/a/b/d/vc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    .line 7290
    :cond_8
    iget-object v0, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_7
.end method

.method public final poll()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/a/b/d/vc;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/d/vc;->e(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_7
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 240
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 878
    iget v0, p0, Lcom/a/b/d/vc;->e:I

    new-array v0, v0, [Ljava/lang/Object;

    .line 879
    iget-object v1, p0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    iget v2, p0, Lcom/a/b/d/vc;->e:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 880
    return-object v0
.end method
