.class final Lcom/a/b/d/zx;
.super Lcom/a/b/d/yd;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field private static final b:J


# instance fields
.field final a:Lcom/a/b/d/yd;


# direct methods
.method constructor <init>(Lcom/a/b/d/yd;)V
    .registers 3

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/a/b/d/yd;-><init>()V

    .line 34
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yd;

    iput-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    .line 35
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yd;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final varargs b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yd;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yd;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p2, p1}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yd;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85
    if-ne p1, p0, :cond_4

    .line 86
    const/4 v0, 0x1

    .line 92
    :goto_3
    return v0

    .line 88
    :cond_4
    instance-of v0, p1, Lcom/a/b/d/zx;

    if-eqz v0, :cond_13

    .line 89
    check-cast p1, Lcom/a/b/d/zx;

    .line 90
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    iget-object v1, p1, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 92
    :cond_13
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 96
    iget-object v0, p0, Lcom/a/b/d/zx;->a:Lcom/a/b/d/yd;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".reverse()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
