.class final Lcom/a/b/d/zl;
.super Lcom/a/b/d/lw;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field final transient a:Lcom/a/b/d/zq;

.field private final transient b:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>(Lcom/a/b/d/zq;Lcom/a/b/d/jl;)V
    .registers 3

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/a/b/d/lw;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    .line 37
    iput-object p2, p0, Lcom/a/b/d/zl;->b:Lcom/a/b/d/jl;

    .line 38
    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/zq;Lcom/a/b/d/jl;Lcom/a/b/d/lw;)V
    .registers 4

    .prologue
    .line 44
    invoke-direct {p0, p3}, Lcom/a/b/d/lw;-><init>(Lcom/a/b/d/lw;)V

    .line 45
    iput-object p1, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    .line 46
    iput-object p2, p0, Lcom/a/b/d/zl;->b:Lcom/a/b/d/jl;

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/zl;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/a/b/d/zl;->b:Lcom/a/b/d/jl;

    return-object v0
.end method

.method private a(II)Lcom/a/b/d/lw;
    .registers 5

    .prologue
    .line 101
    if-nez p1, :cond_9

    invoke-virtual {p0}, Lcom/a/b/d/zl;->size()I

    move-result v0

    if-ne p2, v0, :cond_9

    .line 106
    :goto_8
    return-object p0

    .line 103
    :cond_9
    if-ne p1, p2, :cond_14

    .line 104
    invoke-virtual {p0}, Lcom/a/b/d/zl;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/zl;->a(Ljava/util/Comparator;)Lcom/a/b/d/lw;

    move-result-object p0

    goto :goto_8

    .line 106
    :cond_14
    iget-object v0, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/zq;->a(II)Lcom/a/b/d/me;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/zl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v1, p1, p2}, Lcom/a/b/d/jl;->a(II)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/zl;->a(Lcom/a/b/d/me;Lcom/a/b/d/jl;)Lcom/a/b/d/lw;

    move-result-object p0

    goto :goto_8
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;
    .registers 6

    .prologue
    .line 114
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/a/b/d/zq;->e(Ljava/lang/Object;Z)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/zl;->a(II)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;
    .registers 5

    .prologue
    .line 119
    iget-object v0, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/a/b/d/zq;->f(Ljava/lang/Object;Z)I

    move-result v0

    invoke-virtual {p0}, Lcom/a/b/d/zl;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/zl;->a(II)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 51
    new-instance v0, Lcom/a/b/d/zn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/zn;-><init>(Lcom/a/b/d/zl;B)V

    return-object v0
.end method

.method public final bridge synthetic g()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 29
    .line 3086
    iget-object v0, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    .line 29
    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 96
    iget-object v0, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    invoke-virtual {v0, p1}, Lcom/a/b/d/zq;->c(Ljava/lang/Object;)I

    move-result v0

    .line 97
    const/4 v1, -0x1

    if-ne v0, v1, :cond_b

    const/4 v0, 0x0

    :goto_a
    return-object v0

    :cond_b
    iget-object v1, p0, Lcom/a/b/d/zl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_a
.end method

.method public final h()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/a/b/d/zl;->b:Lcom/a/b/d/jl;

    return-object v0
.end method

.method public final synthetic headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/zl;->a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method final i()Lcom/a/b/d/lw;
    .registers 4

    .prologue
    .line 124
    new-instance v1, Lcom/a/b/d/zl;

    iget-object v0, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    invoke-virtual {v0}, Lcom/a/b/d/zq;->b()Lcom/a/b/d/me;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/zq;

    iget-object v2, p0, Lcom/a/b/d/zl;->b:Lcom/a/b/d/jl;

    invoke-virtual {v2}, Lcom/a/b/d/jl;->e()Lcom/a/b/d/jl;

    move-result-object v2

    invoke-direct {v1, v0, v2, p0}, Lcom/a/b/d/zl;-><init>(Lcom/a/b/d/zq;Lcom/a/b/d/jl;Lcom/a/b/d/lw;)V

    return-object v1
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 29
    .line 2086
    iget-object v0, p0, Lcom/a/b/d/zl;->a:Lcom/a/b/d/zq;

    .line 29
    return-object v0
.end method

.method public final synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/zl;->b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 29
    .line 1091
    iget-object v0, p0, Lcom/a/b/d/zl;->b:Lcom/a/b/d/jl;

    .line 29
    return-object v0
.end method
