.class final Lcom/a/b/d/zf;
.super Lcom/a/b/d/jt;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final d:D = 1.2

.field private static final e:J


# instance fields
.field private final transient a:[Lcom/a/b/d/ka;

.field private final transient b:[Lcom/a/b/d/ka;

.field private final transient c:I


# direct methods
.method constructor <init>(I[Lcom/a/b/d/kb;)V
    .registers 9

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/a/b/d/jt;-><init>()V

    .line 1148
    new-array v0, p1, [Lcom/a/b/d/ka;

    .line 54
    iput-object v0, p0, Lcom/a/b/d/zf;->a:[Lcom/a/b/d/ka;

    .line 55
    const-wide v0, 0x3ff3333333333333L    # 1.2

    invoke-static {p1, v0, v1}, Lcom/a/b/d/iq;->a(ID)I

    move-result v0

    .line 2148
    new-array v1, v0, [Lcom/a/b/d/ka;

    .line 56
    iput-object v1, p0, Lcom/a/b/d/zf;->b:[Lcom/a/b/d/ka;

    .line 57
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/zf;->c:I

    .line 58
    const/4 v0, 0x0

    move v2, v0

    :goto_1a
    if-ge v2, p1, :cond_49

    .line 60
    aget-object v0, p2, v2

    .line 61
    invoke-virtual {v0}, Lcom/a/b/d/kb;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 62
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Lcom/a/b/d/iq;->a(I)I

    move-result v1

    iget v4, p0, Lcom/a/b/d/zf;->c:I

    and-int/2addr v4, v1

    .line 63
    iget-object v1, p0, Lcom/a/b/d/zf;->b:[Lcom/a/b/d/ka;

    aget-object v5, v1, v4

    .line 65
    if-nez v5, :cond_42

    .line 68
    :goto_33
    iget-object v1, p0, Lcom/a/b/d/zf;->b:[Lcom/a/b/d/ka;

    aput-object v0, v1, v4

    .line 69
    iget-object v1, p0, Lcom/a/b/d/zf;->a:[Lcom/a/b/d/ka;

    aput-object v0, v1, v2

    .line 70
    invoke-static {v3, v0, v5}, Lcom/a/b/d/zf;->a(Ljava/lang/Object;Lcom/a/b/d/ka;Lcom/a/b/d/ka;)V

    .line 58
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1a

    .line 65
    :cond_42
    new-instance v1, Lcom/a/b/d/zi;

    invoke-direct {v1, v0, v5}, Lcom/a/b/d/zi;-><init>(Lcom/a/b/d/ka;Lcom/a/b/d/ka;)V

    move-object v0, v1

    goto :goto_33

    .line 72
    :cond_49
    return-void
.end method

.method varargs constructor <init>([Lcom/a/b/d/kb;)V
    .registers 3

    .prologue
    .line 44
    array-length v0, p1

    invoke-direct {p0, v0, p1}, Lcom/a/b/d/zf;-><init>(I[Lcom/a/b/d/kb;)V

    .line 45
    return-void
.end method

.method constructor <init>([Ljava/util/Map$Entry;)V
    .registers 9

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/a/b/d/jt;-><init>()V

    .line 78
    array-length v2, p1

    .line 3148
    new-array v0, v2, [Lcom/a/b/d/ka;

    .line 79
    iput-object v0, p0, Lcom/a/b/d/zf;->a:[Lcom/a/b/d/ka;

    .line 80
    const-wide v0, 0x3ff3333333333333L    # 1.2

    invoke-static {v2, v0, v1}, Lcom/a/b/d/iq;->a(ID)I

    move-result v0

    .line 4148
    new-array v1, v0, [Lcom/a/b/d/ka;

    .line 81
    iput-object v1, p0, Lcom/a/b/d/zf;->b:[Lcom/a/b/d/ka;

    .line 82
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/zf;->c:I

    .line 83
    const/4 v0, 0x0

    move v1, v0

    :goto_1b
    if-ge v1, v2, :cond_55

    .line 85
    aget-object v0, p1, v1

    .line 86
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 87
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 88
    invoke-static {v3, v4}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 89
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/iq;->a(I)I

    move-result v0

    iget v5, p0, Lcom/a/b/d/zf;->c:I

    and-int/2addr v5, v0

    .line 90
    iget-object v0, p0, Lcom/a/b/d/zf;->b:[Lcom/a/b/d/ka;

    aget-object v6, v0, v5

    .line 92
    if-nez v6, :cond_4f

    new-instance v0, Lcom/a/b/d/kb;

    invoke-direct {v0, v3, v4}, Lcom/a/b/d/kb;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 95
    :goto_40
    iget-object v4, p0, Lcom/a/b/d/zf;->b:[Lcom/a/b/d/ka;

    aput-object v0, v4, v5

    .line 96
    iget-object v4, p0, Lcom/a/b/d/zf;->a:[Lcom/a/b/d/ka;

    aput-object v0, v4, v1

    .line 97
    invoke-static {v3, v0, v6}, Lcom/a/b/d/zf;->a(Ljava/lang/Object;Lcom/a/b/d/ka;Lcom/a/b/d/ka;)V

    .line 83
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1b

    .line 92
    :cond_4f
    new-instance v0, Lcom/a/b/d/zi;

    invoke-direct {v0, v3, v4, v6}, Lcom/a/b/d/zi;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/ka;)V

    goto :goto_40

    .line 99
    :cond_55
    return-void
.end method

.method private static a(Ljava/lang/Object;Lcom/a/b/d/ka;Lcom/a/b/d/ka;)V
    .registers 5

    .prologue
    .line 103
    :goto_0
    if-eqz p2, :cond_19

    .line 104
    invoke-virtual {p2}, Lcom/a/b/d/ka;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_d
    const-string v1, "key"

    invoke-static {v0, v1, p1, p2}, Lcom/a/b/d/zf;->a(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 103
    invoke-virtual {p2}, Lcom/a/b/d/ka;->a()Lcom/a/b/d/ka;

    move-result-object p2

    goto :goto_0

    .line 104
    :cond_17
    const/4 v0, 0x0

    goto :goto_d

    .line 106
    :cond_19
    return-void
.end method

.method private static a(I)[Lcom/a/b/d/ka;
    .registers 2

    .prologue
    .line 148
    new-array v0, p0, [Lcom/a/b/d/ka;

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/zf;)[Lcom/a/b/d/ka;
    .registers 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/a/b/d/zf;->a:[Lcom/a/b/d/ka;

    return-object v0
.end method


# virtual methods
.method final d()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 185
    new-instance v0, Lcom/a/b/d/zh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/zh;-><init>(Lcom/a/b/d/zf;B)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 152
    if-nez p1, :cond_4

    .line 171
    :cond_3
    :goto_3
    return-object v0

    .line 155
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Lcom/a/b/d/iq;->a(I)I

    move-result v1

    iget v2, p0, Lcom/a/b/d/zf;->c:I

    and-int/2addr v1, v2

    .line 156
    iget-object v2, p0, Lcom/a/b/d/zf;->b:[Lcom/a/b/d/ka;

    aget-object v1, v2, v1

    .line 157
    :goto_13
    if-eqz v1, :cond_3

    .line 159
    invoke-virtual {v1}, Lcom/a/b/d/ka;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 167
    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 168
    invoke-virtual {v1}, Lcom/a/b/d/ka;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 158
    :cond_24
    invoke-virtual {v1}, Lcom/a/b/d/ka;->a()Lcom/a/b/d/ka;

    move-result-object v1

    goto :goto_13
.end method

.method final i_()Z
    .registers 2

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/a/b/d/zf;->a:[Lcom/a/b/d/ka;

    array-length v0, v0

    return v0
.end method
