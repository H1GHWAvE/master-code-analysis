.class final Lcom/a/b/d/adc;
.super Lcom/a/b/d/adi;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/bw;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# static fields
.field private static final f:J


# instance fields
.field private transient a:Ljava/util/Set;

.field private transient b:Lcom/a/b/d/bw;


# direct methods
.method synthetic constructor <init>(Lcom/a/b/d/bw;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 1111
    invoke-direct {p0, p1, v0, v0}, Lcom/a/b/d/adc;-><init>(Lcom/a/b/d/bw;Ljava/lang/Object;Lcom/a/b/d/bw;)V

    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/bw;Ljava/lang/Object;Lcom/a/b/d/bw;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/a/b/d/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1118
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/adi;-><init>(Ljava/util/Map;Ljava/lang/Object;)V

    .line 1119
    iput-object p3, p0, Lcom/a/b/d/adc;->b:Lcom/a/b/d/bw;

    .line 1120
    return-void
.end method

.method private e()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 1123
    invoke-super {p0}, Lcom/a/b/d/adi;->a()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/bw;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 1137
    iget-object v1, p0, Lcom/a/b/d/adc;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 3123
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adi;->a()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/bw;

    .line 1138
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/bw;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1139
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method final bridge synthetic a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 1111
    .line 5123
    invoke-super {p0}, Lcom/a/b/d/adi;->a()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/bw;

    .line 1111
    return-object v0
.end method

.method public final b()Lcom/a/b/d/bw;
    .registers 5

    .prologue
    .line 1144
    iget-object v1, p0, Lcom/a/b/d/adc;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1145
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adc;->b:Lcom/a/b/d/bw;

    if-nez v0, :cond_1a

    .line 1146
    new-instance v2, Lcom/a/b/d/adc;

    .line 4123
    invoke-super {p0}, Lcom/a/b/d/adi;->a()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/bw;

    .line 1146
    invoke-interface {v0}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v0

    iget-object v3, p0, Lcom/a/b/d/adc;->h:Ljava/lang/Object;

    invoke-direct {v2, v0, v3, p0}, Lcom/a/b/d/adc;-><init>(Lcom/a/b/d/bw;Ljava/lang/Object;Lcom/a/b/d/bw;)V

    iput-object v2, p0, Lcom/a/b/d/adc;->b:Lcom/a/b/d/bw;

    .line 1149
    :cond_1a
    iget-object v0, p0, Lcom/a/b/d/adc;->b:Lcom/a/b/d/bw;

    monitor-exit v1

    return-object v0

    .line 1150
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0
.end method

.method final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1111
    .line 6123
    invoke-super {p0}, Lcom/a/b/d/adi;->a()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/bw;

    .line 1111
    return-object v0
.end method

.method public final j_()Ljava/util/Set;
    .registers 4

    .prologue
    .line 1127
    iget-object v1, p0, Lcom/a/b/d/adc;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1128
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adc;->a:Ljava/util/Set;

    if-nez v0, :cond_19

    .line 2123
    invoke-super {p0}, Lcom/a/b/d/adi;->a()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/bw;

    .line 1129
    invoke-interface {v0}, Lcom/a/b/d/bw;->j_()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adc;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adc;->a:Ljava/util/Set;

    .line 1131
    :cond_19
    iget-object v0, p0, Lcom/a/b/d/adc;->a:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 1132
    :catchall_1d
    move-exception v0

    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    throw v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1111
    invoke-virtual {p0}, Lcom/a/b/d/adc;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
