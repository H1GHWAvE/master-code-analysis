.class final Lcom/a/b/d/id;
.super Lcom/a/b/d/am;
.source "SourceFile"


# instance fields
.field a:Lcom/a/b/d/ia;

.field final synthetic b:Lcom/a/b/d/ic;


# direct methods
.method constructor <init>(Lcom/a/b/d/ic;Lcom/a/b/d/ia;)V
    .registers 3

    .prologue
    .line 443
    iput-object p1, p0, Lcom/a/b/d/id;->b:Lcom/a/b/d/ic;

    invoke-direct {p0}, Lcom/a/b/d/am;-><init>()V

    .line 444
    iput-object p2, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    .line 445
    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 448
    iget-object v0, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    iget-object v0, v0, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    iget-object v0, v0, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 456
    iget-object v0, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    iget-object v3, v0, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    .line 457
    invoke-static {p1}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;)I

    move-result v4

    .line 458
    iget-object v0, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    iget v0, v0, Lcom/a/b/d/ia;->b:I

    if-ne v4, v0, :cond_17

    invoke-static {p1, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 472
    :goto_16
    return-object p1

    .line 461
    :cond_17
    iget-object v0, p0, Lcom/a/b/d/id;->b:Lcom/a/b/d/ic;

    iget-object v0, v0, Lcom/a/b/d/ic;->a:Lcom/a/b/d/ib;

    iget-object v0, v0, Lcom/a/b/d/ib;->a:Lcom/a/b/d/hy;

    invoke-static {v0, p1, v4}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/hy;Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    if-nez v0, :cond_6c

    move v0, v1

    :goto_24
    const-string v5, "value already present: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v5, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 463
    iget-object v0, p0, Lcom/a/b/d/id;->b:Lcom/a/b/d/ic;

    iget-object v0, v0, Lcom/a/b/d/ic;->a:Lcom/a/b/d/ib;

    iget-object v0, v0, Lcom/a/b/d/ib;->a:Lcom/a/b/d/hy;

    iget-object v1, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    invoke-static {v0, v1}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V

    .line 464
    new-instance v0, Lcom/a/b/d/ia;

    iget-object v1, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    iget-object v1, v1, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    iget v2, v2, Lcom/a/b/d/ia;->a:I

    invoke-direct {v0, v1, v2, p1, v4}, Lcom/a/b/d/ia;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 466
    iget-object v1, p0, Lcom/a/b/d/id;->b:Lcom/a/b/d/ic;

    iget-object v1, v1, Lcom/a/b/d/ic;->a:Lcom/a/b/d/ib;

    iget-object v1, v1, Lcom/a/b/d/ib;->a:Lcom/a/b/d/hy;

    invoke-static {v1, v0}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V

    .line 467
    iget-object v1, p0, Lcom/a/b/d/id;->b:Lcom/a/b/d/ic;

    iget-object v2, p0, Lcom/a/b/d/id;->b:Lcom/a/b/d/ic;

    iget-object v2, v2, Lcom/a/b/d/ic;->a:Lcom/a/b/d/ib;

    iget-object v2, v2, Lcom/a/b/d/ib;->a:Lcom/a/b/d/hy;

    invoke-static {v2}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;)I

    move-result v2

    iput v2, v1, Lcom/a/b/d/ic;->e:I

    .line 468
    iget-object v1, p0, Lcom/a/b/d/id;->b:Lcom/a/b/d/ic;

    iget-object v1, v1, Lcom/a/b/d/ic;->d:Lcom/a/b/d/ia;

    iget-object v2, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    if-ne v1, v2, :cond_68

    .line 469
    iget-object v1, p0, Lcom/a/b/d/id;->b:Lcom/a/b/d/ic;

    iput-object v0, v1, Lcom/a/b/d/ic;->d:Lcom/a/b/d/ia;

    .line 471
    :cond_68
    iput-object v0, p0, Lcom/a/b/d/id;->a:Lcom/a/b/d/ia;

    move-object p1, v3

    .line 472
    goto :goto_16

    :cond_6c
    move v0, v2

    .line 461
    goto :goto_24
.end method
