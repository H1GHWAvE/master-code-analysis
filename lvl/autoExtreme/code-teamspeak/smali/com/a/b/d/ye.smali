.class final Lcom/a/b/d/ye;
.super Lcom/a/b/d/yd;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# instance fields
.field private a:Ljava/util/Map;


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/a/b/d/yd;-><init>()V

    .line 265
    new-instance v0, Lcom/a/b/d/ql;

    invoke-direct {v0}, Lcom/a/b/d/ql;-><init>()V

    .line 1265
    sget-object v1, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {v0, v1}, Lcom/a/b/d/ql;->a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 265
    new-instance v1, Lcom/a/b/d/yf;

    invoke-direct {v1, p0}, Lcom/a/b/d/yf;-><init>(Lcom/a/b/d/ye;)V

    invoke-virtual {v0, v1}, Lcom/a/b/d/ql;->a(Lcom/a/b/b/bj;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ye;->a:Ljava/util/Map;

    return-void
.end method

.method private static a(Ljava/lang/Object;)I
    .registers 2

    .prologue
    .line 311
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 277
    if-ne p1, p2, :cond_6

    .line 278
    const/4 v0, 0x0

    .line 295
    :cond_5
    :goto_5
    return v0

    .line 279
    :cond_6
    if-eqz p1, :cond_5

    .line 281
    if-nez p2, :cond_c

    move v0, v1

    .line 282
    goto :goto_5

    .line 1311
    :cond_c
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 2311
    invoke-static {p2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    .line 286
    if-eq v2, v3, :cond_1a

    .line 287
    if-lt v2, v3, :cond_5

    move v0, v1

    goto :goto_5

    .line 291
    :cond_1a
    iget-object v0, p0, Lcom/a/b/d/ye;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/a/b/d/ye;->a:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    .line 292
    if-nez v0, :cond_5

    .line 293
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 299
    const-string v0, "Ordering.arbitrary()"

    return-object v0
.end method
