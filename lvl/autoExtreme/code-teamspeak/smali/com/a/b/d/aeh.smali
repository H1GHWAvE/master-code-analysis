.class final Lcom/a/b/d/aeh;
.super Lcom/a/b/d/aei;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/zy;


# static fields
.field private static final b:J


# direct methods
.method public constructor <init>(Lcom/a/b/d/zy;)V
    .registers 2

    .prologue
    .line 564
    invoke-direct {p0, p1}, Lcom/a/b/d/aei;-><init>(Lcom/a/b/d/adv;)V

    .line 565
    return-void
.end method

.method private n()Lcom/a/b/d/zy;
    .registers 2

    .prologue
    .line 569
    invoke-super {p0}, Lcom/a/b/d/aei;->f()Lcom/a/b/d/adv;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/zy;

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 560
    invoke-virtual {p0}, Lcom/a/b/d/aeh;->l_()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic f()Lcom/a/b/d/adv;
    .registers 2

    .prologue
    .line 560
    .line 3569
    invoke-super {p0}, Lcom/a/b/d/aei;->f()Lcom/a/b/d/adv;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/zy;

    .line 560
    return-object v0
.end method

.method public final j()Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 574
    invoke-static {}, Lcom/a/b/d/adx;->a()Lcom/a/b/b/bj;

    move-result-object v1

    .line 1569
    invoke-super {p0}, Lcom/a/b/d/aei;->f()Lcom/a/b/d/adv;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/zy;

    .line 575
    invoke-interface {v0}, Lcom/a/b/d/zy;->j()Ljava/util/SortedMap;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedMap;Lcom/a/b/b/bj;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 560
    .line 4569
    invoke-super {p0}, Lcom/a/b/d/aei;->f()Lcom/a/b/d/adv;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/zy;

    .line 560
    return-object v0
.end method

.method public final l_()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 580
    .line 2569
    invoke-super {p0}, Lcom/a/b/d/aei;->f()Lcom/a/b/d/adv;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/zy;

    .line 580
    invoke-interface {v0}, Lcom/a/b/d/zy;->l_()Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 560
    invoke-virtual {p0}, Lcom/a/b/d/aeh;->j()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method
