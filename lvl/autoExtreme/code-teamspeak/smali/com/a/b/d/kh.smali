.class final Lcom/a/b/d/kh;
.super Lcom/a/b/d/iz;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field private final a:Lcom/a/b/d/jt;


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;)V
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/a/b/d/iz;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/a/b/d/kh;->a:Lcom/a/b/d/jt;

    .line 39
    return-void
.end method


# virtual methods
.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/a/b/d/kh;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Lcom/a/b/d/agi;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 53
    if-eqz p1, :cond_e

    invoke-virtual {p0}, Lcom/a/b/d/kh;->c()Lcom/a/b/d/agi;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method final g()Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "serialization"
    .end annotation

    .prologue
    .line 79
    new-instance v0, Lcom/a/b/d/kj;

    iget-object v1, p0, Lcom/a/b/d/kh;->a:Lcom/a/b/d/jt;

    invoke-direct {v0, v1}, Lcom/a/b/d/kj;-><init>(Lcom/a/b/d/jt;)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/a/b/d/kh;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method final l()Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/a/b/d/kh;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/a/b/d/ki;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/ki;-><init>(Lcom/a/b/d/kh;Lcom/a/b/d/jl;)V

    return-object v1
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/a/b/d/kh;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    return v0
.end method
