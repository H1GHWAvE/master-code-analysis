.class Lcom/a/b/d/wy;
.super Lcom/a/b/d/gx;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final g:J


# instance fields
.field final a:Lcom/a/b/d/vi;

.field transient b:Ljava/util/Collection;

.field transient c:Lcom/a/b/d/xc;

.field transient d:Ljava/util/Set;

.field transient e:Ljava/util/Collection;

.field transient f:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/a/b/d/vi;)V
    .registers 3

    .prologue
    .line 500
    invoke-direct {p0}, Lcom/a/b/d/gx;-><init>()V

    .line 501
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    iput-object v0, p0, Lcom/a/b/d/wy;->a:Lcom/a/b/d/vi;

    .line 502
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 564
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 555
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 577
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Ljava/util/Map;
    .registers 3

    .prologue
    .line 513
    iget-object v0, p0, Lcom/a/b/d/wy;->f:Ljava/util/Map;

    .line 514
    if-nez v0, :cond_19

    .line 515
    iget-object v0, p0, Lcom/a/b/d/wy;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/wz;

    invoke-direct {v1, p0}, Lcom/a/b/d/wz;-><init>(Lcom/a/b/d/wy;)V

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/wy;->f:Ljava/util/Map;

    .line 523
    :cond_19
    return-object v0
.end method

.method protected c()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 505
    iget-object v0, p0, Lcom/a/b/d/wy;->a:Lcom/a/b/d/vi;

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 535
    iget-object v0, p0, Lcom/a/b/d/wy;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/we;->a(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 559
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 568
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 572
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 509
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 581
    iget-object v0, p0, Lcom/a/b/d/wy;->e:Ljava/util/Collection;

    .line 582
    if-nez v0, :cond_10

    .line 583
    iget-object v0, p0, Lcom/a/b/d/wy;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/wy;->e:Ljava/util/Collection;

    .line 585
    :cond_10
    return-object v0
.end method

.method public k()Ljava/util/Collection;
    .registers 3

    .prologue
    .line 527
    iget-object v0, p0, Lcom/a/b/d/wy;->b:Ljava/util/Collection;

    .line 528
    if-nez v0, :cond_16

    .line 529
    iget-object v0, p0, Lcom/a/b/d/wy;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    .line 1846
    instance-of v1, v0, Ljava/util/Set;

    if-eqz v1, :cond_17

    .line 1847
    check-cast v0, Ljava/util/Set;

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 529
    :goto_14
    iput-object v0, p0, Lcom/a/b/d/wy;->b:Ljava/util/Collection;

    .line 531
    :cond_16
    return-object v0

    .line 1849
    :cond_17
    new-instance v1, Lcom/a/b/d/uw;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/b/d/uw;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    goto :goto_14
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/a/b/d/wy;->c()Lcom/a/b/d/vi;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/a/b/d/wy;->d:Ljava/util/Set;

    .line 548
    if-nez v0, :cond_10

    .line 549
    iget-object v0, p0, Lcom/a/b/d/wy;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/wy;->d:Ljava/util/Set;

    .line 551
    :cond_10
    return-object v0
.end method

.method public final q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 539
    iget-object v0, p0, Lcom/a/b/d/wy;->c:Lcom/a/b/d/xc;

    .line 540
    if-nez v0, :cond_10

    .line 541
    iget-object v0, p0, Lcom/a/b/d/wy;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->q()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/xc;)Lcom/a/b/d/xc;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/wy;->c:Lcom/a/b/d/xc;

    .line 543
    :cond_10
    return-object v0
.end method
