.class final Lcom/a/b/d/bj;
.super Lcom/a/b/d/yd;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field static final a:Lcom/a/b/d/bj;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    new-instance v0, Lcom/a/b/d/bj;

    invoke-direct {v0}, Lcom/a/b/d/bj;-><init>()V

    sput-object v0, Lcom/a/b/d/bj;->a:Lcom/a/b/d/bj;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/a/b/d/yd;-><init>()V

    return-void
.end method

.method private static f()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 57
    sget-object v0, Lcom/a/b/d/bj;->a:Lcom/a/b/d/bj;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/yd;
    .registers 1

    .prologue
    .line 53
    return-object p0
.end method

.method public final a(Ljava/lang/Iterable;)Ljava/util/List;
    .registers 3

    .prologue
    .line 42
    invoke-static {p1}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 47
    invoke-static {p1}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    const-string v0, "Ordering.allEqual()"

    return-object v0
.end method
