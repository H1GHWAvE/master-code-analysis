.class final Lcom/a/b/d/abw;
.super Lcom/a/b/d/acq;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# instance fields
.field final synthetic a:Lcom/a/b/d/abu;


# direct methods
.method private constructor <init>(Lcom/a/b/d/abu;)V
    .registers 2

    .prologue
    .line 91
    iput-object p1, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    invoke-direct {p0, p1}, Lcom/a/b/d/acq;-><init>(Lcom/a/b/d/abx;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/abu;B)V
    .registers 3

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/a/b/d/abw;-><init>(Lcom/a/b/d/abu;)V

    return-void
.end method

.method private c()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 94
    invoke-super {p0}, Lcom/a/b/d/acq;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method private d()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 99
    new-instance v0, Lcom/a/b/d/up;

    invoke-direct {v0, p0}, Lcom/a/b/d/up;-><init>(Ljava/util/SortedMap;)V

    return-object v0
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    invoke-static {v0}, Lcom/a/b/d/abu;->a(Lcom/a/b/d/abu;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method final synthetic e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 91
    .line 1099
    new-instance v0, Lcom/a/b/d/up;

    invoke-direct {v0, p0}, Lcom/a/b/d/up;-><init>(Ljava/util/SortedMap;)V

    .line 91
    return-object v0
.end method

.method public final firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    invoke-static {v0}, Lcom/a/b/d/abu;->a(Lcom/a/b/d/abu;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 119
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    new-instance v0, Lcom/a/b/d/abu;

    iget-object v1, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    invoke-static {v1}, Lcom/a/b/d/abu;->a(Lcom/a/b/d/abu;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    iget-object v2, v2, Lcom/a/b/d/abu;->b:Lcom/a/b/b/dz;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/abu;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/dz;)V

    invoke-virtual {v0}, Lcom/a/b/d/abu;->j()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 91
    .line 2094
    invoke-super {p0}, Lcom/a/b/d/acq;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 91
    return-object v0
.end method

.method public final lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    invoke-static {v0}, Lcom/a/b/d/abu;->a(Lcom/a/b/d/abu;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 6

    .prologue
    .line 126
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v0, Lcom/a/b/d/abu;

    iget-object v1, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    invoke-static {v1}, Lcom/a/b/d/abu;->a(Lcom/a/b/d/abu;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    iget-object v2, v2, Lcom/a/b/d/abu;->b:Lcom/a/b/b/dz;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/abu;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/dz;)V

    invoke-virtual {v0}, Lcom/a/b/d/abu;->j()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 134
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    new-instance v0, Lcom/a/b/d/abu;

    iget-object v1, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    invoke-static {v1}, Lcom/a/b/d/abu;->a(Lcom/a/b/d/abu;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/abw;->a:Lcom/a/b/d/abu;

    iget-object v2, v2, Lcom/a/b/d/abu;->b:Lcom/a/b/b/dz;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/abu;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/dz;)V

    invoke-virtual {v0}, Lcom/a/b/d/abu;->j()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method
