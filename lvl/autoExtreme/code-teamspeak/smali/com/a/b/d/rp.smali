.class final Lcom/a/b/d/rp;
.super Ljava/util/AbstractQueue;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/rz;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 3111
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3112
    new-instance v0, Lcom/a/b/d/rq;

    invoke-direct {v0, p0}, Lcom/a/b/d/rq;-><init>(Lcom/a/b/d/rp;)V

    iput-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    return-void
.end method

.method private a()Lcom/a/b/d/rz;
    .registers 3

    .prologue
    .line 3155
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    .line 3156
    iget-object v1, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    if-ne v0, v1, :cond_b

    const/4 v0, 0x0

    :cond_b
    return-object v0
.end method

.method private a(Lcom/a/b/d/rz;)Z
    .registers 4

    .prologue
    .line 3144
    invoke-interface {p1}, Lcom/a/b/d/rz;->i()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 3147
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->i()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 3148
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-static {p1, v0}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 3150
    const/4 v0, 0x1

    return v0
.end method

.method private b()Lcom/a/b/d/rz;
    .registers 3

    .prologue
    .line 3161
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    .line 3162
    iget-object v1, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    if-ne v0, v1, :cond_c

    .line 3163
    const/4 v0, 0x0

    .line 3167
    :goto_b
    return-object v0

    .line 3166
    :cond_c
    invoke-virtual {p0, v0}, Lcom/a/b/d/rp;->remove(Ljava/lang/Object;)Z

    goto :goto_b
.end method


# virtual methods
.method public final clear()V
    .registers 3

    .prologue
    .line 3205
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    .line 3206
    :goto_6
    iget-object v1, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    if-eq v0, v1, :cond_13

    .line 3207
    invoke-interface {v0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v1

    .line 3208
    invoke-static {v0}, Lcom/a/b/d/qy;->c(Lcom/a/b/d/rz;)V

    move-object v0, v1

    .line 3210
    goto :goto_6

    .line 3212
    :cond_13
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    iget-object v1, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0, v1}, Lcom/a/b/d/rz;->c(Lcom/a/b/d/rz;)V

    .line 3213
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    iget-object v1, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0, v1}, Lcom/a/b/d/rz;->d(Lcom/a/b/d/rz;)V

    .line 3214
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3185
    check-cast p1, Lcom/a/b/d/rz;

    .line 3186
    invoke-interface {p1}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    sget-object v1, Lcom/a/b/d/ry;->a:Lcom/a/b/d/ry;

    if-eq v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final isEmpty()Z
    .registers 3

    .prologue
    .line 3191
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3218
    new-instance v0, Lcom/a/b/d/rr;

    invoke-direct {p0}, Lcom/a/b/d/rp;->a()Lcom/a/b/d/rz;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/rr;-><init>(Lcom/a/b/d/rp;Lcom/a/b/d/rz;)V

    return-object v0
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3111
    check-cast p1, Lcom/a/b/d/rz;

    .line 5144
    invoke-interface {p1}, Lcom/a/b/d/rz;->i()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 5147
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->i()Lcom/a/b/d/rz;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 5148
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-static {p1, v0}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 5150
    const/4 v0, 0x1

    .line 3111
    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3111
    invoke-direct {p0}, Lcom/a/b/d/rp;->a()Lcom/a/b/d/rz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3111
    .line 4161
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    .line 4162
    iget-object v1, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    if-ne v0, v1, :cond_c

    .line 4163
    const/4 v0, 0x0

    :goto_b
    return-object v0

    .line 4166
    :cond_c
    invoke-virtual {p0, v0}, Lcom/a/b/d/rp;->remove(Ljava/lang/Object;)Z

    goto :goto_b
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3173
    check-cast p1, Lcom/a/b/d/rz;

    .line 3174
    invoke-interface {p1}, Lcom/a/b/d/rz;->i()Lcom/a/b/d/rz;

    move-result-object v0

    .line 3175
    invoke-interface {p1}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v1

    .line 3176
    invoke-static {v0, v1}, Lcom/a/b/d/qy;->b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V

    .line 3177
    invoke-static {p1}, Lcom/a/b/d/qy;->c(Lcom/a/b/d/rz;)V

    .line 3179
    sget-object v0, Lcom/a/b/d/ry;->a:Lcom/a/b/d/ry;

    if-eq v1, v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final size()I
    .registers 4

    .prologue
    .line 3196
    const/4 v1, 0x0

    .line 3197
    iget-object v0, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    :goto_7
    iget-object v2, p0, Lcom/a/b/d/rp;->a:Lcom/a/b/d/rz;

    if-eq v0, v2, :cond_12

    .line 3198
    add-int/lit8 v1, v1, 0x1

    .line 3197
    invoke-interface {v0}, Lcom/a/b/d/rz;->h()Lcom/a/b/d/rz;

    move-result-object v0

    goto :goto_7

    .line 3200
    :cond_12
    return v1
.end method
