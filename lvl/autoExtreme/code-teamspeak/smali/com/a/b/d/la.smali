.class final Lcom/a/b/d/la;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field final a:[Ljava/lang/Object;

.field final b:[I


# direct methods
.method constructor <init>(Lcom/a/b/d/xc;)V
    .registers 7

    .prologue
    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 432
    invoke-interface {p1}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 433
    new-array v1, v0, [Ljava/lang/Object;

    iput-object v1, p0, Lcom/a/b/d/la;->a:[Ljava/lang/Object;

    .line 434
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/a/b/d/la;->b:[I

    .line 435
    const/4 v0, 0x0

    .line 436
    invoke-interface {p1}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 437
    iget-object v3, p0, Lcom/a/b/d/la;->a:[Ljava/lang/Object;

    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 438
    iget-object v3, p0, Lcom/a/b/d/la;->b:[I

    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    aput v0, v3, v1

    .line 439
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 440
    goto :goto_1d

    .line 441
    :cond_3d
    return-void
.end method

.method private a()Ljava/lang/Object;
    .registers 5

    .prologue
    .line 444
    iget-object v0, p0, Lcom/a/b/d/la;->a:[Ljava/lang/Object;

    array-length v0, v0

    invoke-static {v0}, Lcom/a/b/d/oi;->a(I)Lcom/a/b/d/oi;

    move-result-object v1

    .line 446
    const/4 v0, 0x0

    :goto_8
    iget-object v2, p0, Lcom/a/b/d/la;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_1b

    .line 447
    iget-object v2, p0, Lcom/a/b/d/la;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/a/b/d/la;->b:[I

    aget v3, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/a/b/d/oi;->a(Ljava/lang/Object;I)I

    .line 446
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 449
    :cond_1b
    invoke-static {v1}, Lcom/a/b/d/ku;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method
