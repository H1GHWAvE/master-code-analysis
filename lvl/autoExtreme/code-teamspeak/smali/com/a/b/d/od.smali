.class final Lcom/a/b/d/od;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Lcom/a/b/d/oe;

.field b:Lcom/a/b/d/oe;

.field final synthetic c:Lcom/a/b/d/oc;


# direct methods
.method constructor <init>(Lcom/a/b/d/oc;)V
    .registers 3

    .prologue
    .line 494
    iput-object p1, p0, Lcom/a/b/d/od;->c:Lcom/a/b/d/oc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495
    iget-object v0, p0, Lcom/a/b/d/od;->c:Lcom/a/b/d/oc;

    invoke-static {v0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oc;)Lcom/a/b/d/oe;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/d/oe;->h:Lcom/a/b/d/oe;

    iput-object v0, p0, Lcom/a/b/d/od;->a:Lcom/a/b/d/oe;

    return-void
.end method

.method private a()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/a/b/d/od;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 506
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 508
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/od;->a:Lcom/a/b/d/oe;

    .line 509
    iput-object v0, p0, Lcom/a/b/d/od;->b:Lcom/a/b/d/oe;

    .line 510
    iget-object v1, p0, Lcom/a/b/d/od;->a:Lcom/a/b/d/oe;

    iget-object v1, v1, Lcom/a/b/d/oe;->h:Lcom/a/b/d/oe;

    iput-object v1, p0, Lcom/a/b/d/od;->a:Lcom/a/b/d/oe;

    .line 511
    return-object v0
.end method


# virtual methods
.method public final hasNext()Z
    .registers 3

    .prologue
    .line 500
    iget-object v0, p0, Lcom/a/b/d/od;->a:Lcom/a/b/d/oe;

    iget-object v1, p0, Lcom/a/b/d/od;->c:Lcom/a/b/d/oc;

    invoke-static {v1}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oc;)Lcom/a/b/d/oe;

    move-result-object v1

    if-eq v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 494
    .line 1505
    invoke-virtual {p0}, Lcom/a/b/d/od;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1506
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1508
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/od;->a:Lcom/a/b/d/oe;

    .line 1509
    iput-object v0, p0, Lcom/a/b/d/od;->b:Lcom/a/b/d/oe;

    .line 1510
    iget-object v1, p0, Lcom/a/b/d/od;->a:Lcom/a/b/d/oe;

    iget-object v1, v1, Lcom/a/b/d/oe;->h:Lcom/a/b/d/oe;

    iput-object v1, p0, Lcom/a/b/d/od;->a:Lcom/a/b/d/oe;

    .line 494
    return-object v0
.end method

.method public final remove()V
    .registers 4

    .prologue
    .line 516
    iget-object v0, p0, Lcom/a/b/d/od;->b:Lcom/a/b/d/oe;

    if-eqz v0, :cond_1f

    const/4 v0, 0x1

    .line 1049
    :goto_5
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 517
    iget-object v0, p0, Lcom/a/b/d/od;->c:Lcom/a/b/d/oc;

    iget-object v1, p0, Lcom/a/b/d/od;->b:Lcom/a/b/d/oe;

    invoke-virtual {v1}, Lcom/a/b/d/oe;->getKey()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/od;->b:Lcom/a/b/d/oe;

    invoke-virtual {v2}, Lcom/a/b/d/oe;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/oc;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 518
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/od;->b:Lcom/a/b/d/oe;

    .line 519
    return-void

    .line 516
    :cond_1f
    const/4 v0, 0x0

    goto :goto_5
.end method
