.class public abstract Lcom/a/b/d/gh;
.super Lcom/a/b/d/hg;
.source "SourceFile"

# interfaces
.implements Ljava/util/Collection;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/a/b/d/hg;-><init>()V

    return-void
.end method

.method private a([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 253
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/a/b/d/gh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private d(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 152
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    move-result v0

    return v0
.end method

.method public add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public abstract b()Ljava/util/Collection;
.end method

.method protected b(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/a/b/d/gh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected b(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/a/b/d/gh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method protected c(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/a/b/d/gh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 165
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 166
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 167
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 168
    const/4 v0, 0x1

    .line 171
    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method protected c(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/a/b/d/gh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 110
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected l()V
    .registers 2

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/a/b/d/gh;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->i(Ljava/util/Iterator;)V

    .line 208
    return-void
.end method

.method protected o()Ljava/lang/String;
    .registers 2

    .prologue
    .line 230
    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/a/b/d/gh;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    .line 242
    invoke-virtual {p0, v0}, Lcom/a/b/d/gh;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/a/b/d/gh;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
