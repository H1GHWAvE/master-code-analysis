.class final Lcom/a/b/d/es;
.super Lcom/a/b/d/ep;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Lcom/a/b/d/es;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 106
    new-instance v0, Lcom/a/b/d/es;

    invoke-direct {v0}, Lcom/a/b/d/es;-><init>()V

    sput-object v0, Lcom/a/b/d/es;->a:Lcom/a/b/d/es;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/a/b/d/ep;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Long;Ljava/lang/Long;)J
    .registers 10

    .prologue
    const-wide/16 v6, 0x0

    .line 119
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 120
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_21

    cmp-long v2, v0, v6

    if-gez v2, :cond_21

    .line 121
    const-wide v0, 0x7fffffffffffffffL

    .line 126
    :cond_20
    :goto_20
    return-wide v0

    .line 123
    :cond_21
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_20

    cmp-long v2, v0, v6

    if-lez v2, :cond_20

    .line 124
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_20
.end method

.method private static a(Ljava/lang/Long;)Ljava/lang/Long;
    .registers 5

    .prologue
    .line 109
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 110
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v0, v2

    if-nez v2, :cond_f

    const/4 v0, 0x0

    :goto_e
    return-object v0

    :cond_f
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_e
.end method

.method private static b(Ljava/lang/Long;)Ljava/lang/Long;
    .registers 5

    .prologue
    .line 114
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 115
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-nez v2, :cond_c

    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_b
.end method

.method static synthetic c()Lcom/a/b/d/es;
    .registers 1

    .prologue
    .line 104
    sget-object v0, Lcom/a/b/d/es;->a:Lcom/a/b/d/es;

    return-object v0
.end method

.method private static d()Ljava/lang/Long;
    .registers 2

    .prologue
    .line 130
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private static e()Ljava/lang/Long;
    .registers 2

    .prologue
    .line 134
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private static f()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 138
    sget-object v0, Lcom/a/b/d/es;->a:Lcom/a/b/d/es;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Comparable;Ljava/lang/Comparable;)J
    .registers 11

    .prologue
    const-wide/16 v6, 0x0

    .line 104
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    .line 3119
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 3120
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_25

    cmp-long v2, v0, v6

    if-gez v2, :cond_25

    .line 3121
    const-wide v0, 0x7fffffffffffffffL

    .line 3124
    :cond_24
    :goto_24
    return-wide v0

    .line 3123
    :cond_25
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_24

    cmp-long v2, v0, v6

    if-lez v2, :cond_24

    .line 3124
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_24
.end method

.method public final synthetic a()Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 2130
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 104
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 6

    .prologue
    .line 104
    check-cast p1, Ljava/lang/Long;

    .line 5109
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 5110
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v0, v2

    if-nez v2, :cond_11

    const/4 v0, 0x0

    :goto_10
    return-object v0

    :cond_11
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_10
.end method

.method public final synthetic b()Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 1134
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 104
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 6

    .prologue
    .line 104
    check-cast p1, Ljava/lang/Long;

    .line 4114
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 4115
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-nez v2, :cond_e

    const/4 v0, 0x0

    :goto_d
    return-object v0

    :cond_e
    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_d
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 143
    const-string v0, "DiscreteDomain.longs()"

    return-object v0
.end method
