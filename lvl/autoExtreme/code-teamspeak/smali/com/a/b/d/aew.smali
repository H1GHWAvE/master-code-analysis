.class abstract enum Lcom/a/b/d/aew;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/d/aew;

.field public static final enum b:Lcom/a/b/d/aew;

.field private static final synthetic c:[Lcom/a/b/d/aew;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 134
    new-instance v0, Lcom/a/b/d/aex;

    const-string v1, "SIZE"

    invoke-direct {v0, v1}, Lcom/a/b/d/aex;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/aew;->a:Lcom/a/b/d/aew;

    .line 145
    new-instance v0, Lcom/a/b/d/aey;

    const-string v1, "DISTINCT"

    invoke-direct {v0, v1}, Lcom/a/b/d/aey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/aew;->b:Lcom/a/b/d/aew;

    .line 133
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/d/aew;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/d/aew;->a:Lcom/a/b/d/aew;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/d/aew;->b:Lcom/a/b/d/aew;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/d/aew;->c:[Lcom/a/b/d/aew;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/aew;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/d/aew;
    .registers 2

    .prologue
    .line 133
    const-class v0, Lcom/a/b/d/aew;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aew;

    return-object v0
.end method

.method public static values()[Lcom/a/b/d/aew;
    .registers 1

    .prologue
    .line 133
    sget-object v0, Lcom/a/b/d/aew;->c:[Lcom/a/b/d/aew;

    invoke-virtual {v0}, [Lcom/a/b/d/aew;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/d/aew;

    return-object v0
.end method


# virtual methods
.method abstract a(Lcom/a/b/d/aez;)I
.end method

.method abstract b(Lcom/a/b/d/aez;)J
    .param p1    # Lcom/a/b/d/aez;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method
