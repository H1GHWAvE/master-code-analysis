.class abstract Lcom/a/b/d/rv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field b:I

.field c:I

.field d:Lcom/a/b/d/sa;

.field e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field f:Lcom/a/b/d/rz;

.field g:Lcom/a/b/d/sy;

.field h:Lcom/a/b/d/sy;

.field final synthetic i:Lcom/a/b/d/qy;


# direct methods
.method constructor <init>(Lcom/a/b/d/qy;)V
    .registers 3

    .prologue
    .line 3600
    iput-object p1, p0, Lcom/a/b/d/rv;->i:Lcom/a/b/d/qy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3601
    iget-object v0, p1, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/rv;->b:I

    .line 3602
    const/4 v0, -0x1

    iput v0, p0, Lcom/a/b/d/rv;->c:I

    .line 3603
    invoke-direct {p0}, Lcom/a/b/d/rv;->b()V

    .line 3604
    return-void
.end method

.method private a(Lcom/a/b/d/rz;)Z
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 3666
    :try_start_1
    invoke-interface {p1}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v2

    .line 3667
    iget-object v3, p0, Lcom/a/b/d/rv;->i:Lcom/a/b/d/qy;

    .line 3896
    invoke-interface {p1}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1f

    .line 3668
    :cond_d
    :goto_d
    if-eqz v0, :cond_37

    .line 3669
    new-instance v1, Lcom/a/b/d/sy;

    iget-object v3, p0, Lcom/a/b/d/rv;->i:Lcom/a/b/d/qy;

    invoke-direct {v1, v3, v2, v0}, Lcom/a/b/d/sy;-><init>(Lcom/a/b/d/qy;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/a/b/d/rv;->g:Lcom/a/b/d/sy;
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_3e

    .line 3676
    iget-object v0, p0, Lcom/a/b/d/rv;->d:Lcom/a/b/d/sa;

    invoke-virtual {v0}, Lcom/a/b/d/sa;->a()V

    const/4 v0, 0x1

    :goto_1e
    return v0

    .line 3899
    :cond_1f
    :try_start_1f
    invoke-interface {p1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v1

    .line 3900
    if-eqz v1, :cond_d

    .line 3904
    invoke-virtual {v3}, Lcom/a/b/d/qy;->c()Z

    move-result v4

    if-eqz v4, :cond_35

    invoke-virtual {v3, p1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;)Z
    :try_end_32
    .catchall {:try_start_1f .. :try_end_32} :catchall_3e

    move-result v3

    if-nez v3, :cond_d

    :cond_35
    move-object v0, v1

    .line 3907
    goto :goto_d

    .line 3676
    :cond_37
    iget-object v0, p0, Lcom/a/b/d/rv;->d:Lcom/a/b/d/sa;

    invoke-virtual {v0}, Lcom/a/b/d/sa;->a()V

    const/4 v0, 0x0

    goto :goto_1e

    :catchall_3e
    move-exception v0

    iget-object v1, p0, Lcom/a/b/d/rv;->d:Lcom/a/b/d/sa;

    invoke-virtual {v1}, Lcom/a/b/d/sa;->a()V

    throw v0
.end method

.method private b()V
    .registers 4

    .prologue
    .line 3610
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/rv;->g:Lcom/a/b/d/sy;

    .line 3612
    invoke-direct {p0}, Lcom/a/b/d/rv;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 3630
    :cond_9
    :goto_9
    return-void

    .line 3616
    :cond_a
    invoke-direct {p0}, Lcom/a/b/d/rv;->d()Z

    move-result v0

    if-nez v0, :cond_9

    .line 3620
    :cond_10
    iget v0, p0, Lcom/a/b/d/rv;->b:I

    if-ltz v0, :cond_9

    .line 3621
    iget-object v0, p0, Lcom/a/b/d/rv;->i:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    iget v1, p0, Lcom/a/b/d/rv;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/a/b/d/rv;->b:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/a/b/d/rv;->d:Lcom/a/b/d/sa;

    .line 3622
    iget-object v0, p0, Lcom/a/b/d/rv;->d:Lcom/a/b/d/sa;

    iget v0, v0, Lcom/a/b/d/sa;->b:I

    if-eqz v0, :cond_10

    .line 3623
    iget-object v0, p0, Lcom/a/b/d/rv;->d:Lcom/a/b/d/sa;

    iget-object v0, v0, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lcom/a/b/d/rv;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3624
    iget-object v0, p0, Lcom/a/b/d/rv;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/rv;->c:I

    .line 3625
    invoke-direct {p0}, Lcom/a/b/d/rv;->d()Z

    move-result v0

    if-eqz v0, :cond_10

    goto :goto_9
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 3636
    iget-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    if-eqz v0, :cond_23

    .line 3637
    iget-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    :goto_c
    iget-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    if-eqz v0, :cond_23

    .line 3638
    iget-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    invoke-direct {p0, v0}, Lcom/a/b/d/rv;->a(Lcom/a/b/d/rz;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 3639
    const/4 v0, 0x1

    .line 3643
    :goto_19
    return v0

    .line 3637
    :cond_1a
    iget-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    goto :goto_c

    .line 3643
    :cond_23
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private d()Z
    .registers 4

    .prologue
    .line 3650
    :cond_0
    iget v0, p0, Lcom/a/b/d/rv;->c:I

    if-ltz v0, :cond_26

    .line 3651
    iget-object v0, p0, Lcom/a/b/d/rv;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lcom/a/b/d/rv;->c:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/a/b/d/rv;->c:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    iput-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    if-eqz v0, :cond_0

    .line 3652
    iget-object v0, p0, Lcom/a/b/d/rv;->f:Lcom/a/b/d/rz;

    invoke-direct {p0, v0}, Lcom/a/b/d/rv;->a(Lcom/a/b/d/rz;)Z

    move-result v0

    if-nez v0, :cond_24

    invoke-direct {p0}, Lcom/a/b/d/rv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3653
    :cond_24
    const/4 v0, 0x1

    .line 3657
    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method


# virtual methods
.method final a()Lcom/a/b/d/sy;
    .registers 2

    .prologue
    .line 3686
    iget-object v0, p0, Lcom/a/b/d/rv;->g:Lcom/a/b/d/sy;

    if-nez v0, :cond_a

    .line 3687
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 3689
    :cond_a
    iget-object v0, p0, Lcom/a/b/d/rv;->g:Lcom/a/b/d/sy;

    iput-object v0, p0, Lcom/a/b/d/rv;->h:Lcom/a/b/d/sy;

    .line 3690
    invoke-direct {p0}, Lcom/a/b/d/rv;->b()V

    .line 3691
    iget-object v0, p0, Lcom/a/b/d/rv;->h:Lcom/a/b/d/sy;

    return-object v0
.end method

.method public hasNext()Z
    .registers 2

    .prologue
    .line 3682
    iget-object v0, p0, Lcom/a/b/d/rv;->g:Lcom/a/b/d/sy;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public abstract next()Ljava/lang/Object;
.end method

.method public remove()V
    .registers 3

    .prologue
    .line 3696
    iget-object v0, p0, Lcom/a/b/d/rv;->h:Lcom/a/b/d/sy;

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    .line 4049
    :goto_5
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 3697
    iget-object v0, p0, Lcom/a/b/d/rv;->i:Lcom/a/b/d/qy;

    iget-object v1, p0, Lcom/a/b/d/rv;->h:Lcom/a/b/d/sy;

    invoke-virtual {v1}, Lcom/a/b/d/sy;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/qy;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3698
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/rv;->h:Lcom/a/b/d/sy;

    .line 3699
    return-void

    .line 3696
    :cond_19
    const/4 v0, 0x0

    goto :goto_5
.end method
