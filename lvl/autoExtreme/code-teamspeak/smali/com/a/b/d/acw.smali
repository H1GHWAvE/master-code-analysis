.class final Lcom/a/b/d/acw;
.super Lcom/a/b/d/adi;
.source "SourceFile"


# static fields
.field private static final f:J


# instance fields
.field transient a:Ljava/util/Set;

.field transient b:Ljava/util/Collection;


# direct methods
.method constructor <init>(Ljava/util/Map;Ljava/lang/Object;)V
    .registers 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1162
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/adi;-><init>(Ljava/util/Map;Ljava/lang/Object;)V

    .line 1163
    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 5

    .prologue
    .line 1166
    iget-object v1, p0, Lcom/a/b/d/acw;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1167
    :try_start_3
    invoke-super {p0, p1}, Lcom/a/b/d/adi;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1168
    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_c
    monitor-exit v1

    return-object v0

    :cond_e
    iget-object v2, p0, Lcom/a/b/d/acw;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_c

    .line 1170
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method


# virtual methods
.method public final containsValue(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1195
    invoke-virtual {p0}, Lcom/a/b/d/acw;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 5

    .prologue
    .line 1174
    iget-object v1, p0, Lcom/a/b/d/acw;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1175
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/acw;->a:Ljava/util/Set;

    if-nez v0, :cond_18

    .line 1176
    new-instance v0, Lcom/a/b/d/acx;

    invoke-virtual {p0}, Lcom/a/b/d/acw;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/d/acw;->h:Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/a/b/d/acx;-><init>(Ljava/util/Set;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/a/b/d/acw;->a:Ljava/util/Set;

    .line 1179
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/acw;->a:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 1180
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    throw v0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1156
    invoke-direct {p0, p1}, Lcom/a/b/d/acw;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final values()Ljava/util/Collection;
    .registers 5

    .prologue
    .line 1184
    iget-object v1, p0, Lcom/a/b/d/acw;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1185
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/acw;->b:Ljava/util/Collection;

    if-nez v0, :cond_18

    .line 1186
    new-instance v0, Lcom/a/b/d/ada;

    invoke-virtual {p0}, Lcom/a/b/d/acw;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/d/acw;->h:Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/a/b/d/ada;-><init>(Ljava/util/Collection;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/a/b/d/acw;->b:Ljava/util/Collection;

    .line 1189
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/acw;->b:Ljava/util/Collection;

    monitor-exit v1

    return-object v0

    .line 1190
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    throw v0
.end method
