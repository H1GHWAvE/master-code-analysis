.class final Lcom/a/b/d/ach;
.super Lcom/a/b/d/act;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/abx;


# direct methods
.method private constructor <init>(Lcom/a/b/d/abx;)V
    .registers 3

    .prologue
    .line 582
    iput-object p1, p0, Lcom/a/b/d/ach;->a:Lcom/a/b/d/abx;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/act;-><init>(Lcom/a/b/d/abx;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/abx;B)V
    .registers 3

    .prologue
    .line 582
    invoke-direct {p0, p1}, Lcom/a/b/d/ach;-><init>(Lcom/a/b/d/abx;)V

    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 644
    iget-object v0, p0, Lcom/a/b/d/ach;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 584
    iget-object v0, p0, Lcom/a/b/d/ach;->a:Lcom/a/b/d/abx;

    invoke-virtual {v0}, Lcom/a/b/d/abx;->o()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 592
    if-nez p1, :cond_4

    .line 606
    :goto_3
    return v0

    .line 596
    :cond_4
    iget-object v1, p0, Lcom/a/b/d/ach;->a:Lcom/a/b/d/abx;

    iget-object v1, v1, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 597
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 598
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 599
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 600
    const/4 v1, 0x1

    .line 601
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 602
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    :cond_31
    move v0, v1

    move v1, v0

    .line 605
    goto :goto_11

    :cond_34
    move v0, v1

    .line 606
    goto :goto_3
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 6

    .prologue
    .line 610
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    const/4 v0, 0x0

    .line 612
    iget-object v1, p0, Lcom/a/b/d/ach;->a:Lcom/a/b/d/abx;

    iget-object v1, v1, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 613
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 614
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 617
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 618
    const/4 v1, 0x1

    .line 619
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 620
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    :cond_35
    move v0, v1

    move v1, v0

    .line 623
    goto :goto_11

    .line 624
    :cond_38
    return v1
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 6

    .prologue
    .line 628
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    const/4 v0, 0x0

    .line 630
    iget-object v1, p0, Lcom/a/b/d/ach;->a:Lcom/a/b/d/abx;

    iget-object v1, v1, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 631
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 632
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 633
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 634
    const/4 v1, 0x1

    .line 635
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 636
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    :cond_31
    move v0, v1

    move v1, v0

    .line 639
    goto :goto_11

    .line 640
    :cond_34
    return v1
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 588
    invoke-virtual {p0}, Lcom/a/b/d/ach;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method
