.class final Lcom/a/b/d/jg;
.super Lcom/a/b/d/agi;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/jf;

.field private final b:Ljava/util/Iterator;


# direct methods
.method constructor <init>(Lcom/a/b/d/jf;)V
    .registers 3

    .prologue
    .line 109
    iput-object p1, p0, Lcom/a/b/d/jg;->a:Lcom/a/b/d/jf;

    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 110
    iget-object v0, p0, Lcom/a/b/d/jg;->a:Lcom/a/b/d/jf;

    iget-object v0, v0, Lcom/a/b/d/jf;->a:Lcom/a/b/d/jd;

    invoke-static {v0}, Lcom/a/b/d/jd;->a(Lcom/a/b/d/jd;)Ljava/util/EnumMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/jg;->b:Ljava/util/Iterator;

    return-void
.end method

.method private a()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 119
    iget-object v0, p0, Lcom/a/b/d/jg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 120
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/a/b/d/jg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 109
    .line 1119
    iget-object v0, p0, Lcom/a/b/d/jg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1120
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 109
    return-object v0
.end method
