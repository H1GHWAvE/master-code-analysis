.class final Lcom/a/b/d/ps;
.super Lcom/a/b/d/hi;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/d/pn;

.field private final b:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Set;Lcom/a/b/d/pn;)V
    .registers 3

    .prologue
    .line 645
    invoke-direct {p0}, Lcom/a/b/d/hi;-><init>()V

    .line 646
    iput-object p1, p0, Lcom/a/b/d/ps;->b:Ljava/util/Set;

    .line 647
    iput-object p2, p0, Lcom/a/b/d/ps;->a:Lcom/a/b/d/pn;

    .line 648
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/ps;)Lcom/a/b/d/pn;
    .registers 2

    .prologue
    .line 639
    iget-object v0, p0, Lcom/a/b/d/ps;->a:Lcom/a/b/d/pn;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/a/b/d/ps;->b:Ljava/util/Set;

    return-object v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 639
    .line 4651
    iget-object v0, p0, Lcom/a/b/d/ps;->b:Ljava/util/Set;

    .line 639
    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 677
    .line 1651
    iget-object v0, p0, Lcom/a/b/d/ps;->b:Ljava/util/Set;

    .line 677
    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 681
    .line 2141
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    .line 681
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 685
    .line 3087
    invoke-static {p0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    .line 685
    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 689
    .line 3098
    invoke-static {p0}, Lcom/a/b/d/aad;->a(Ljava/util/Set;)I

    move-result v0

    .line 689
    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 655
    iget-object v0, p0, Lcom/a/b/d/ps;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 656
    new-instance v1, Lcom/a/b/d/pt;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/pt;-><init>(Lcom/a/b/d/ps;Ljava/util/Iterator;)V

    return-object v1
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 639
    .line 5651
    iget-object v0, p0, Lcom/a/b/d/ps;->b:Ljava/util/Set;

    .line 639
    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 693
    .line 3651
    iget-object v0, p0, Lcom/a/b/d/ps;->b:Ljava/util/Set;

    .line 693
    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 697
    invoke-virtual {p0, p1}, Lcom/a/b/d/ps;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 701
    invoke-virtual {p0, p1}, Lcom/a/b/d/ps;->c(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 669
    invoke-virtual {p0}, Lcom/a/b/d/ps;->p()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 673
    .line 1253
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 673
    return-object v0
.end method
