.class final Lcom/a/b/d/aet;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Lcom/a/b/d/aez;

.field b:Lcom/a/b/d/xd;

.field final synthetic c:Lcom/a/b/d/aer;


# direct methods
.method constructor <init>(Lcom/a/b/d/aer;)V
    .registers 3

    .prologue
    .line 402
    iput-object p1, p0, Lcom/a/b/d/aet;->c:Lcom/a/b/d/aer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    iget-object v0, p0, Lcom/a/b/d/aet;->c:Lcom/a/b/d/aer;

    invoke-static {v0}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    return-void
.end method

.method private a()Lcom/a/b/d/xd;
    .registers 4

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/a/b/d/aet;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 421
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 423
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/aet;->c:Lcom/a/b/d/aer;

    iget-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    invoke-static {v0, v1}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)Lcom/a/b/d/xd;

    move-result-object v0

    .line 424
    iput-object v0, p0, Lcom/a/b/d/aet;->b:Lcom/a/b/d/xd;

    .line 425
    iget-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    .line 2519
    iget-object v1, v1, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    .line 425
    iget-object v2, p0, Lcom/a/b/d/aet;->c:Lcom/a/b/d/aer;

    invoke-static {v2}, Lcom/a/b/d/aer;->c(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;

    move-result-object v2

    if-ne v1, v2, :cond_26

    .line 426
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    .line 430
    :goto_25
    return-object v0

    .line 428
    :cond_26
    iget-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    .line 3519
    iget-object v1, v1, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    .line 428
    iput-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    goto :goto_25
.end method


# virtual methods
.method public final hasNext()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 408
    iget-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    if-nez v1, :cond_6

    .line 414
    :goto_5
    return v0

    .line 410
    :cond_6
    iget-object v1, p0, Lcom/a/b/d/aet;->c:Lcom/a/b/d/aer;

    invoke-static {v1}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aer;)Lcom/a/b/d/hs;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    .line 1923
    iget-object v2, v2, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 410
    invoke-virtual {v1, v2}, Lcom/a/b/d/hs;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 411
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    goto :goto_5

    .line 414
    :cond_1a
    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 402
    .line 4420
    invoke-virtual {p0}, Lcom/a/b/d/aet;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 4421
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 4423
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/aet;->c:Lcom/a/b/d/aer;

    iget-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    invoke-static {v0, v1}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)Lcom/a/b/d/xd;

    move-result-object v0

    .line 4424
    iput-object v0, p0, Lcom/a/b/d/aet;->b:Lcom/a/b/d/xd;

    .line 4425
    iget-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    .line 4519
    iget-object v1, v1, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    .line 4425
    iget-object v2, p0, Lcom/a/b/d/aet;->c:Lcom/a/b/d/aer;

    invoke-static {v2}, Lcom/a/b/d/aer;->c(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;

    move-result-object v2

    if-ne v1, v2, :cond_26

    .line 4426
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    .line 402
    :goto_25
    return-object v0

    .line 4428
    :cond_26
    iget-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    .line 5519
    iget-object v1, v1, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    .line 4428
    iput-object v1, p0, Lcom/a/b/d/aet;->a:Lcom/a/b/d/aez;

    goto :goto_25
.end method

.method public final remove()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 435
    iget-object v0, p0, Lcom/a/b/d/aet;->b:Lcom/a/b/d/xd;

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    .line 4049
    :goto_6
    const-string v2, "no calls to next() since the last call to remove()"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 436
    iget-object v0, p0, Lcom/a/b/d/aet;->c:Lcom/a/b/d/aer;

    iget-object v2, p0, Lcom/a/b/d/aet;->b:Lcom/a/b/d/xd;

    invoke-interface {v2}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/a/b/d/aer;->c(Ljava/lang/Object;I)I

    .line 437
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/aet;->b:Lcom/a/b/d/xd;

    .line 438
    return-void

    :cond_1a
    move v0, v1

    .line 435
    goto :goto_6
.end method
