.class final Lcom/a/b/d/ug;
.super Lcom/a/b/d/uc;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field final synthetic b:Lcom/a/b/d/uf;


# direct methods
.method constructor <init>(Lcom/a/b/d/uf;)V
    .registers 2

    .prologue
    .line 2802
    iput-object p1, p0, Lcom/a/b/d/ug;->b:Lcom/a/b/d/uf;

    invoke-direct {p0, p1}, Lcom/a/b/d/uc;-><init>(Lcom/a/b/d/ty;)V

    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 2805
    iget-object v0, p0, Lcom/a/b/d/ug;->b:Lcom/a/b/d/uf;

    .line 3790
    iget-object v0, v0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 2805
    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2825
    iget-object v0, p0, Lcom/a/b/d/ug;->b:Lcom/a/b/d/uf;

    invoke-virtual {v0}, Lcom/a/b/d/uf;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 2815
    iget-object v0, p0, Lcom/a/b/d/ug;->b:Lcom/a/b/d/uf;

    invoke-virtual {v0, p1}, Lcom/a/b/d/uf;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public final last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2830
    iget-object v0, p0, Lcom/a/b/d/ug;->b:Lcom/a/b/d/uf;

    invoke-virtual {v0}, Lcom/a/b/d/uf;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 2810
    iget-object v0, p0, Lcom/a/b/d/ug;->b:Lcom/a/b/d/uf;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/uf;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 2820
    iget-object v0, p0, Lcom/a/b/d/ug;->b:Lcom/a/b/d/uf;

    invoke-virtual {v0, p1}, Lcom/a/b/d/uf;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method
