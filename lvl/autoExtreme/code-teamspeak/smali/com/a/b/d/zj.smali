.class final Lcom/a/b/d/zj;
.super Lcom/a/b/d/ku;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# instance fields
.field private final transient a:Lcom/a/b/d/jt;

.field private final transient b:I


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;I)V
    .registers 3

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/a/b/d/ku;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/a/b/d/zj;->a:Lcom/a/b/d/jt;

    .line 40
    iput p2, p0, Lcom/a/b/d/zj;->b:I

    .line 41
    return-void
.end method

.method private b()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/a/b/d/zj;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 50
    iget-object v0, p0, Lcom/a/b/d/zj;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 51
    if-nez v0, :cond_c

    const/4 v0, 0x0

    :goto_b
    return v0

    :cond_c
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_b
.end method

.method final a(I)Lcom/a/b/d/xd;
    .registers 4

    .prologue
    .line 71
    iget-object v0, p0, Lcom/a/b/d/zj;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 72
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 61
    iget-object v0, p0, Lcom/a/b/d/zj;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/a/b/d/zj;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->i_()Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/a/b/d/zj;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->hashCode()I

    move-result v0

    return v0
.end method

.method public final synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 31
    .line 1066
    iget-object v0, p0, Lcom/a/b/d/zj;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 31
    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 56
    iget v0, p0, Lcom/a/b/d/zj;->b:I

    return v0
.end method
