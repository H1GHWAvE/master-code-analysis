.class final Lcom/a/b/d/dn;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 280
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 281
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0, p1}, Lcom/a/b/d/dn;->a(Ljava/util/SortedSet;Lcom/a/b/d/dm;)Ljava/util/SortedSet;

    move-result-object v0

    .line 287
    :goto_a
    return-object v0

    .line 282
    :cond_b
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_17

    .line 283
    check-cast p0, Ljava/util/Set;

    .line 1093
    new-instance v0, Lcom/a/b/d/ds;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ds;-><init>(Ljava/util/Set;Lcom/a/b/d/dm;)V

    goto :goto_a

    .line 284
    :cond_17
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_22

    .line 285
    check-cast p0, Ljava/util/List;

    invoke-static {p0, p1}, Lcom/a/b/d/dn;->a(Ljava/util/List;Lcom/a/b/d/dm;)Ljava/util/List;

    move-result-object v0

    goto :goto_a

    .line 2054
    :cond_22
    new-instance v0, Lcom/a/b/d/do;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/do;-><init>(Ljava/util/Collection;Lcom/a/b/d/dm;)V

    goto :goto_a
.end method

.method public static a(Ljava/util/List;Lcom/a/b/d/dm;)Ljava/util/List;
    .registers 3

    .prologue
    .line 181
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/a/b/d/dr;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/dr;-><init>(Ljava/util/List;Lcom/a/b/d/dm;)V

    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Lcom/a/b/d/dp;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/dp;-><init>(Ljava/util/List;Lcom/a/b/d/dm;)V

    goto :goto_9
.end method

.method static synthetic a(Ljava/util/ListIterator;Lcom/a/b/d/dm;)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 3251
    new-instance v0, Lcom/a/b/d/dq;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/dq;-><init>(Ljava/util/ListIterator;Lcom/a/b/d/dm;)V

    .line 37
    return-object v0
.end method

.method private static a(Ljava/util/Set;Lcom/a/b/d/dm;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 93
    new-instance v0, Lcom/a/b/d/ds;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ds;-><init>(Ljava/util/Set;Lcom/a/b/d/dm;)V

    return-object v0
.end method

.method public static a(Ljava/util/SortedSet;Lcom/a/b/d/dm;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 131
    new-instance v0, Lcom/a/b/d/dt;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/dt;-><init>(Ljava/util/SortedSet;Lcom/a/b/d/dm;)V

    return-object v0
.end method

.method static synthetic b(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
    .registers 5

    .prologue
    .line 37
    .line 2298
    invoke-static {p0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2299
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2300
    invoke-interface {p1, v2}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 37
    :cond_16
    return-object v0
.end method

.method private static b(Ljava/util/ListIterator;Lcom/a/b/d/dm;)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 251
    new-instance v0, Lcom/a/b/d/dq;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/dq;-><init>(Ljava/util/ListIterator;Lcom/a/b/d/dm;)V

    return-object v0
.end method

.method private static c(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 54
    new-instance v0, Lcom/a/b/d/do;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/do;-><init>(Ljava/util/Collection;Lcom/a/b/d/dm;)V

    return-object v0
.end method

.method private static d(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
    .registers 5

    .prologue
    .line 298
    invoke-static {p0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 299
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 300
    invoke-interface {p1, v2}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 302
    :cond_16
    return-object v0
.end method
