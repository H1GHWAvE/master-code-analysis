.class abstract Lcom/a/b/d/tp;
.super Lcom/a/b/d/gs;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableMap;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation


# instance fields
.field private transient a:Ljava/util/Comparator;

.field private transient b:Ljava/util/Set;

.field private transient c:Ljava/util/NavigableSet;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 3794
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 3822
    invoke-static {p0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3911
    new-instance v0, Lcom/a/b/d/tq;

    invoke-direct {v0, p0}, Lcom/a/b/d/tq;-><init>(Lcom/a/b/d/tp;)V

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 3801
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method abstract b()Ljava/util/NavigableMap;
.end method

.method abstract c()Ljava/util/Iterator;
.end method

.method public ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 3857
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3862
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 3809
    iget-object v0, p0, Lcom/a/b/d/tp;->a:Ljava/util/Comparator;

    .line 3810
    if-nez v0, :cond_1c

    .line 3811
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 3812
    if-nez v0, :cond_12

    .line 3813
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 4822
    :cond_12
    invoke-static {v0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    .line 3815
    iput-object v0, p0, Lcom/a/b/d/tp;->a:Ljava/util/Comparator;

    .line 3817
    :cond_1c
    return-object v0
.end method

.method public descendingKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 3939
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public descendingMap()Ljava/util/NavigableMap;
    .registers 2

    .prologue
    .line 3897
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3904
    iget-object v0, p0, Lcom/a/b/d/tp;->b:Ljava/util/Set;

    .line 3905
    if-nez v0, :cond_b

    .line 4911
    new-instance v0, Lcom/a/b/d/tq;

    invoke-direct {v0, p0}, Lcom/a/b/d/tq;-><init>(Lcom/a/b/d/tp;)V

    .line 3905
    iput-object v0, p0, Lcom/a/b/d/tp;->b:Ljava/util/Set;

    :cond_b
    return-object v0
.end method

.method public firstEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3827
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 3847
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public floorKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3852
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 3951
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 3966
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/tp;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 3867
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public higherKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3872
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3793
    .line 5801
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    .line 3793
    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3926
    invoke-virtual {p0}, Lcom/a/b/d/tp;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public lastEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 3882
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3832
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 3837
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public lowerKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3842
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public navigableKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 3933
    iget-object v0, p0, Lcom/a/b/d/tp;->c:Ljava/util/NavigableSet;

    .line 3934
    if-nez v0, :cond_b

    new-instance v0, Lcom/a/b/d/un;

    invoke-direct {v0, p0}, Lcom/a/b/d/un;-><init>(Ljava/util/NavigableMap;)V

    iput-object v0, p0, Lcom/a/b/d/tp;->c:Ljava/util/NavigableSet;

    :cond_b
    return-object v0
.end method

.method public pollFirstEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 3887
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->pollLastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public pollLastEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 3892
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 6

    .prologue
    .line 3946
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p3, p4, p1, p2}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 3961
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/tp;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 3956
    invoke-virtual {p0}, Lcom/a/b/d/tp;->b()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 3971
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/tp;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 3981
    .line 5307
    invoke-static {p0}, Lcom/a/b/d/sz;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 3981
    return-object v0
.end method

.method public values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 3976
    new-instance v0, Lcom/a/b/d/vb;

    invoke-direct {v0, p0}, Lcom/a/b/d/vb;-><init>(Ljava/util/Map;)V

    return-object v0
.end method
