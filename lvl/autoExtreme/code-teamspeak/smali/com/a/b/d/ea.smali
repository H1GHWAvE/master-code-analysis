.class final Lcom/a/b/d/ea;
.super Lcom/a/b/d/dw;
.source "SourceFile"


# static fields
.field private static final b:Lcom/a/b/d/ea;

.field private static final c:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 114
    new-instance v0, Lcom/a/b/d/ea;

    invoke-direct {v0}, Lcom/a/b/d/ea;-><init>()V

    sput-object v0, Lcom/a/b/d/ea;->b:Lcom/a/b/d/ea;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/d/dw;-><init>(Ljava/lang/Comparable;)V

    .line 118
    return-void
.end method

.method static synthetic f()Lcom/a/b/d/ea;
    .registers 1

    .prologue
    .line 113
    sget-object v0, Lcom/a/b/d/ea;->b:Lcom/a/b/d/ea;

    return-object v0
.end method

.method private static g()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 168
    sget-object v0, Lcom/a/b/d/ea;->b:Lcom/a/b/d/ea;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/d/dw;)I
    .registers 3

    .prologue
    .line 162
    if-ne p1, p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    const/4 v0, -0x1

    goto :goto_3
.end method

.method final a()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 126
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method final a(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 4

    .prologue
    .line 133
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method final a(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 147
    invoke-virtual {p1}, Lcom/a/b/d/ep;->a()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/StringBuilder;)V
    .registers 3

    .prologue
    .line 140
    const-string v0, "(-\u221e"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    return-void
.end method

.method final a(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 123
    const/4 v0, 0x1

    return v0
.end method

.method final b()Lcom/a/b/d/ce;
    .registers 3

    .prologue
    .line 129
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method final b(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 5

    .prologue
    .line 137
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method final b(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 151
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method final b(Ljava/lang/StringBuilder;)V
    .registers 3

    .prologue
    .line 143
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method final c(Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 3

    .prologue
    .line 156
    :try_start_0
    invoke-virtual {p1}, Lcom/a/b/d/ep;->a()Ljava/lang/Comparable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;
    :try_end_7
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object p0

    .line 158
    :goto_8
    return-object p0

    :catch_9
    move-exception v0

    goto :goto_8
.end method

.method final c()Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 120
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "range unbounded on this side"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 113
    check-cast p1, Lcom/a/b/d/dw;

    invoke-virtual {p0, p1}, Lcom/a/b/d/ea;->a(Lcom/a/b/d/dw;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 165
    const-string v0, "-\u221e"

    return-object v0
.end method
