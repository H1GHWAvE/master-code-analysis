.class final Lcom/a/b/d/dh;
.super Lcom/a/b/d/hi;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Set;

.field final synthetic b:Lcom/a/b/d/dg;


# direct methods
.method constructor <init>(Lcom/a/b/d/dg;Ljava/util/Set;)V
    .registers 3

    .prologue
    .line 450
    iput-object p1, p0, Lcom/a/b/d/dh;->b:Lcom/a/b/d/dg;

    iput-object p2, p0, Lcom/a/b/d/dh;->a:Ljava/util/Set;

    invoke-direct {p0}, Lcom/a/b/d/hi;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/a/b/d/dh;->a:Ljava/util/Set;

    return-object v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 450
    .line 1452
    iget-object v0, p0, Lcom/a/b/d/dh;->a:Ljava/util/Set;

    .line 450
    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 457
    if-eqz p1, :cond_c

    iget-object v0, p0, Lcom/a/b/d/dh;->a:Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 462
    .line 1141
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    .line 462
    return v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 450
    .line 2452
    iget-object v0, p0, Lcom/a/b/d/dh;->a:Ljava/util/Set;

    .line 450
    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 466
    if-eqz p1, :cond_c

    iget-object v0, p0, Lcom/a/b/d/dh;->a:Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->b(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 470
    invoke-virtual {p0, p1}, Lcom/a/b/d/dh;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
