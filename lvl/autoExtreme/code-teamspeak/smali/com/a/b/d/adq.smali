.class Lcom/a/b/d/adq;
.super Lcom/a/b/d/add;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# static fields
.field private static final a:J


# direct methods
.method constructor <init>(Ljava/util/Set;Ljava/lang/Object;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/add;-><init>(Ljava/util/Collection;Ljava/lang/Object;B)V

    .line 218
    return-void
.end method


# virtual methods
.method synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/a/b/d/adq;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method c()Ljava/util/Set;
    .registers 2

    .prologue
    .line 221
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/a/b/d/adq;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 225
    if-ne p1, p0, :cond_4

    .line 226
    const/4 v0, 0x1

    .line 229
    :goto_3
    return v0

    .line 228
    :cond_4
    iget-object v1, p0, Lcom/a/b/d/adq;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 229
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/d/adq;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_3

    .line 230
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_7 .. :try_end_13} :catchall_11

    throw v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 234
    iget-object v1, p0, Lcom/a/b/d/adq;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 235
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adq;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    monitor-exit v1

    return v0

    .line 236
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method
