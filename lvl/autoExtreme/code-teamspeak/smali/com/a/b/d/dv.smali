.class final Lcom/a/b/d/dv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field a:I


# direct methods
.method constructor <init>(I)V
    .registers 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lcom/a/b/d/dv;->a:I

    .line 34
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 37
    iget v0, p0, Lcom/a/b/d/dv;->a:I

    return v0
.end method

.method private b()V
    .registers 2

    .prologue
    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/dv;->a:I

    .line 52
    return-void
.end method

.method private c(I)I
    .registers 4

    .prologue
    .line 41
    iget v0, p0, Lcom/a/b/d/dv;->a:I

    .line 42
    add-int v1, v0, p1

    iput v1, p0, Lcom/a/b/d/dv;->a:I

    .line 43
    return v0
.end method


# virtual methods
.method public final a(I)I
    .registers 3

    .prologue
    .line 47
    iget v0, p0, Lcom/a/b/d/dv;->a:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/a/b/d/dv;->a:I

    return v0
.end method

.method public final b(I)I
    .registers 3

    .prologue
    .line 55
    iget v0, p0, Lcom/a/b/d/dv;->a:I

    .line 56
    iput p1, p0, Lcom/a/b/d/dv;->a:I

    .line 57
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 67
    instance-of v0, p1, Lcom/a/b/d/dv;

    if-eqz v0, :cond_e

    check-cast p1, Lcom/a/b/d/dv;

    iget v0, p1, Lcom/a/b/d/dv;->a:I

    iget v1, p0, Lcom/a/b/d/dv;->a:I

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 62
    iget v0, p0, Lcom/a/b/d/dv;->a:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 72
    iget v0, p0, Lcom/a/b/d/dv;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
