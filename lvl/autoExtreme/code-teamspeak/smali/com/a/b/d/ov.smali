.class public final Lcom/a/b/d/ov;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/d/jl;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 698
    new-instance v1, Lcom/a/b/d/ph;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/a/b/d/ph;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method private static a()Ljava/util/ArrayList;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public static a(I)Ljava/util/ArrayList;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 185
    const-string v0, "initialArraySize"

    invoke-static {p0, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    .registers 3
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 142
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_11

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_10
    return-object v0

    :cond_11
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_10
.end method

.method public static a(Ljava/util/Iterator;)Ljava/util/ArrayList;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 1088
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 160
    invoke-static {v0, p0}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 161
    return-object v0
.end method

.method public static varargs a([Ljava/lang/Object;)Ljava/util/ArrayList;
    .registers 3
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 110
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    array-length v0, p0

    invoke-static {v0}, Lcom/a/b/d/ov;->c(I)I

    move-result v0

    .line 113
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 114
    invoke-static {v1, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 115
    return-object v1
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/util/List;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 753
    new-instance v1, Lcom/a/b/d/oz;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {v1, v0}, Lcom/a/b/d/oz;-><init>(Ljava/lang/CharSequence;)V

    return-object v1
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;
    .registers 4
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 355
    new-instance v0, Lcom/a/b/d/pm;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/d/pm;-><init>(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 312
    new-instance v0, Lcom/a/b/d/pa;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pa;-><init>(Ljava/lang/Object;[Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .registers 2

    .prologue
    .line 445
    invoke-static {p0}, Lcom/a/b/d/ci;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;I)Ljava/util/List;
    .registers 3

    .prologue
    .line 652
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 653
    if-lez p1, :cond_13

    const/4 v0, 0x1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 654
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_15

    new-instance v0, Lcom/a/b/d/pd;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pd;-><init>(Ljava/util/List;I)V

    :goto_12
    return-object v0

    .line 653
    :cond_13
    const/4 v0, 0x0

    goto :goto_6

    .line 654
    :cond_15
    new-instance v0, Lcom/a/b/d/pb;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pb;-><init>(Ljava/util/List;I)V

    goto :goto_12
.end method

.method private static a(Ljava/util/List;II)Ljava/util/List;
    .registers 4

    .prologue
    .line 1010
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_e

    .line 1011
    new-instance v0, Lcom/a/b/d/ow;

    invoke-direct {v0, p0}, Lcom/a/b/d/ow;-><init>(Ljava/util/List;)V

    .line 1027
    :goto_9
    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 1019
    :cond_e
    new-instance v0, Lcom/a/b/d/ox;

    invoke-direct {v0, p0}, Lcom/a/b/d/ox;-><init>(Ljava/util/List;)V

    goto :goto_9
.end method

.method public static a(Ljava/util/List;Lcom/a/b/b/bj;)Ljava/util/List;
    .registers 3

    .prologue
    .line 543
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/a/b/d/pi;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pi;-><init>(Ljava/util/List;Lcom/a/b/b/bj;)V

    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Lcom/a/b/d/pk;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pk;-><init>(Ljava/util/List;Lcom/a/b/b/bj;)V

    goto :goto_9
.end method

.method private static varargs a([Ljava/util/List;)Ljava/util/List;
    .registers 2

    .prologue
    .line 505
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1445
    invoke-static {v0}, Lcom/a/b/d/ci;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 505
    return-object v0
.end method

.method private static a(Ljava/util/List;ILjava/lang/Iterable;)Z
    .registers 7

    .prologue
    .line 962
    const/4 v0, 0x0

    .line 963
    invoke-interface {p0, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    .line 964
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_18

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 965
    invoke-interface {v1, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 966
    const/4 v0, 0x1

    .line 967
    goto :goto_9

    .line 968
    :cond_18
    return v0
.end method

.method static a(Ljava/util/List;Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 944
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_9

    .line 953
    :cond_8
    :goto_8
    return v0

    .line 947
    :cond_9
    instance-of v2, p1, Ljava/util/List;

    if-nez v2, :cond_f

    move v0, v1

    .line 948
    goto :goto_8

    .line 951
    :cond_f
    check-cast p1, Ljava/util/List;

    .line 953
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_29

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/util/Iterator;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_29
    move v0, v1

    goto :goto_8
.end method

.method static b(Ljava/util/List;Ljava/lang/Object;)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 975
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 976
    :cond_4
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 977
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 978
    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    .line 981
    :goto_18
    return v0

    :cond_19
    const/4 v0, -0x1

    goto :goto_18
.end method

.method public static b(I)Ljava/util/ArrayList;
    .registers 3
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0}, Lcom/a/b/d/ov;->c(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method private static b()Ljava/util/LinkedList;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 232
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/LinkedList;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 1232
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 259
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 260
    return-object v0
.end method

.method private static b(Ljava/util/List;)Ljava/util/List;
    .registers 2

    .prologue
    .line 787
    instance-of v0, p0, Lcom/a/b/d/jl;

    if-eqz v0, :cond_b

    .line 788
    check-cast p0, Lcom/a/b/d/jl;

    invoke-virtual {p0}, Lcom/a/b/d/jl;->e()Lcom/a/b/d/jl;

    move-result-object v0

    .line 794
    :goto_a
    return-object v0

    .line 789
    :cond_b
    instance-of v0, p0, Lcom/a/b/d/pf;

    if-eqz v0, :cond_14

    .line 790
    check-cast p0, Lcom/a/b/d/pf;

    .line 1806
    iget-object v0, p0, Lcom/a/b/d/pf;->a:Ljava/util/List;

    goto :goto_a

    .line 791
    :cond_14
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_1e

    .line 792
    new-instance v0, Lcom/a/b/d/pe;

    invoke-direct {v0, p0}, Lcom/a/b/d/pe;-><init>(Ljava/util/List;)V

    goto :goto_a

    .line 794
    :cond_1e
    new-instance v0, Lcom/a/b/d/pf;

    invoke-direct {v0, p0}, Lcom/a/b/d/pf;-><init>(Ljava/util/List;)V

    goto :goto_a
.end method

.method private static b(Ljava/util/List;I)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 1001
    new-instance v0, Lcom/a/b/d/oy;

    invoke-direct {v0, p0}, Lcom/a/b/d/oy;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, p1}, Lcom/a/b/d/oy;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method private static c(I)I
    .registers 5
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 119
    const-string v0, "arraySize"

    invoke-static {p0, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 122
    const-wide/16 v0, 0x5

    int-to-long v2, p0

    add-long/2addr v0, v2

    div-int/lit8 v2, p0, 0xa

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method

.method private static c(Ljava/util/List;)I
    .registers 5

    .prologue
    .line 930
    const/4 v0, 0x1

    .line 931
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 932
    mul-int/lit8 v3, v0, 0x1f

    if-nez v2, :cond_1a

    const/4 v0, 0x0

    :goto_14
    add-int/2addr v0, v3

    .line 934
    xor-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    .line 936
    goto :goto_5

    .line 932
    :cond_1a
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_14

    .line 937
    :cond_1f
    return v0
.end method

.method static c(Ljava/util/List;Ljava/lang/Object;)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 988
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 989
    :cond_8
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 990
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 991
    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    .line 994
    :goto_1c
    return v0

    :cond_1d
    const/4 v0, -0x1

    goto :goto_1c
.end method

.method private static c()Ljava/util/concurrent/CopyOnWriteArrayList;
    .registers 1
    .annotation build Lcom/a/b/a/c;
        a = "CopyOnWriteArrayList"
    .end annotation

    .prologue
    .line 274
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    return-object v0
.end method

.method private static c(Ljava/lang/Iterable;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "CopyOnWriteArrayList"
    .end annotation

    .prologue
    .line 289
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_e

    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    .line 292
    :goto_8
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1

    .line 289
    :cond_e
    invoke-static {p0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_8
.end method

.method private static d(Ljava/lang/Iterable;)Ljava/util/List;
    .registers 1

    .prologue
    .line 1077
    check-cast p0, Ljava/util/List;

    return-object p0
.end method
