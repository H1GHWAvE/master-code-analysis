.class final Lcom/a/b/d/mt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Z

.field final synthetic b:Ljava/util/Iterator;

.field final synthetic c:Lcom/a/b/d/ms;


# direct methods
.method constructor <init>(Lcom/a/b/d/ms;Ljava/util/Iterator;)V
    .registers 4

    .prologue
    .line 872
    iput-object p1, p0, Lcom/a/b/d/mt;->c:Lcom/a/b/d/ms;

    iput-object p2, p0, Lcom/a/b/d/mt;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 873
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/b/d/mt;->a:Z

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 877
    iget-object v0, p0, Lcom/a/b/d/mt;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 882
    iget-object v0, p0, Lcom/a/b/d/mt;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 883
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/a/b/d/mt;->a:Z

    .line 884
    return-object v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 889
    iget-boolean v0, p0, Lcom/a/b/d/mt;->a:Z

    if-nez v0, :cond_10

    const/4 v0, 0x1

    .line 1049
    :goto_5
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 890
    iget-object v0, p0, Lcom/a/b/d/mt;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 891
    return-void

    .line 889
    :cond_10
    const/4 v0, 0x0

    goto :goto_5
.end method
