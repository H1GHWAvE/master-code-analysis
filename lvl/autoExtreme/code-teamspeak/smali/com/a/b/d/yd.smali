.class public abstract Lcom/a/b/d/yd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field static final c:I = 0x1

.field static final d:I = -0x1


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/Object;)I
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 901
    invoke-static {p1, p2, p0}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    return v0
.end method

.method private a([Ljava/lang/Object;III)I
    .registers 8

    .prologue
    .line 742
    aget-object v1, p1, p4

    .line 744
    aget-object v0, p1, p3

    aput-object v0, p1, p4

    .line 745
    aput-object v1, p1, p3

    move v0, p2

    .line 748
    :goto_9
    if-ge p2, p3, :cond_1b

    .line 749
    aget-object v2, p1, p2

    invoke-virtual {p0, v2, v1}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_18

    .line 750
    invoke-static {p1, v0, p2}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;II)V

    .line 751
    add-int/lit8 v0, v0, 0x1

    .line 748
    :cond_18
    add-int/lit8 p2, p2, 0x1

    goto :goto_9

    .line 754
    :cond_1b
    invoke-static {p1, p3, v0}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;II)V

    .line 755
    return v0
.end method

.method private static a(Lcom/a/b/d/yd;)Lcom/a/b/d/yd;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 136
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yd;

    return-object v0
.end method

.method private static varargs a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/yd;
    .registers 4
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 1312
    new-instance v0, Lcom/a/b/d/pa;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/pa;-><init>(Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 2162
    new-instance v1, Lcom/a/b/d/fh;

    invoke-direct {v1, v0}, Lcom/a/b/d/fh;-><init>(Ljava/util/List;)V

    .line 191
    return-object v1
.end method

.method public static a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 124
    instance-of v0, p0, Lcom/a/b/d/yd;

    if-eqz v0, :cond_7

    check-cast p0, Lcom/a/b/d/yd;

    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Lcom/a/b/d/cu;

    invoke-direct {v0, p0}, Lcom/a/b/d/cu;-><init>(Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_6
.end method

.method private static a(Ljava/util/List;)Lcom/a/b/d/yd;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 162
    new-instance v0, Lcom/a/b/d/fh;

    invoke-direct {v0, p0}, Lcom/a/b/d/fh;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method private a(Ljava/lang/Iterable;I)Ljava/util/List;
    .registers 11

    .prologue
    .line 615
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_2d

    move-object v0, p1

    .line 616
    check-cast v0, Ljava/util/Collection;

    .line 617
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    int-to-long v2, v1

    const-wide/16 v4, 0x2

    int-to-long v6, p2

    mul-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gtz v1, :cond_2d

    .line 623
    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 624
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 625
    array-length v1, v0

    if-le v1, p2, :cond_24

    .line 626
    invoke-static {v0, p2}, Lcom/a/b/d/yc;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 628
    :cond_24
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 631
    :goto_2c
    return-object v0

    :cond_2d
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/a/b/d/yd;->a(Ljava/util/Iterator;I)Ljava/util/List;

    move-result-object v0

    goto :goto_2c
.end method

.method private a(Ljava/util/Iterator;I)Ljava/util/List;
    .registers 13

    .prologue
    const/4 v5, 0x0

    .line 649
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 650
    const-string v0, "k"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 652
    if-eqz p2, :cond_11

    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_16

    .line 653
    :cond_11
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    .line 735
    :goto_15
    return-object v0

    .line 654
    :cond_16
    const v0, 0x3fffffff    # 1.9999999f

    if-lt p2, v0, :cond_3b

    .line 656
    invoke-static {p1}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 657
    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 658
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p2, :cond_33

    .line 659
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 661
    :cond_33
    invoke-virtual {v0}, Ljava/util/ArrayList;->trimToSize()V

    .line 662
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_15

    .line 679
    :cond_3b
    mul-int/lit8 v7, p2, 0x2

    .line 681
    new-array v0, v7, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 682
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 683
    aput-object v2, v0, v5

    .line 684
    const/4 v1, 0x1

    .line 688
    :goto_48
    if-ge v1, p2, :cond_5f

    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5f

    .line 689
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 690
    add-int/lit8 v3, v1, 0x1

    aput-object v4, v0, v1

    .line 691
    invoke-virtual {p0, v2, v4}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move v1, v3

    .line 692
    goto :goto_48

    :cond_5e
    move v1, p2

    .line 694
    :cond_5f
    :goto_5f
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c0

    .line 695
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 696
    invoke-virtual {p0, v4, v2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_5f

    .line 700
    add-int/lit8 v3, v1, 0x1

    aput-object v4, v0, v1

    .line 701
    if-ne v3, v7, :cond_d5

    .line 705
    add-int/lit8 v1, v7, -0x1

    move v4, v5

    move v6, v1

    move v2, v5

    .line 711
    :goto_7a
    if-ge v2, v6, :cond_b1

    .line 712
    add-int v1, v2, v6

    add-int/lit8 v1, v1, 0x1

    ushr-int/lit8 v1, v1, 0x1

    .line 2742
    aget-object v8, v0, v1

    .line 2744
    aget-object v3, v0, v6

    aput-object v3, v0, v1

    .line 2745
    aput-object v8, v0, v6

    move v1, v2

    move v3, v2

    .line 2748
    :goto_8c
    if-ge v3, v6, :cond_9e

    .line 2749
    aget-object v9, v0, v3

    invoke-virtual {p0, v9, v8}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v9

    if-gez v9, :cond_9b

    .line 2750
    invoke-static {v0, v1, v3}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;II)V

    .line 2751
    add-int/lit8 v1, v1, 0x1

    .line 2748
    :cond_9b
    add-int/lit8 v3, v3, 0x1

    goto :goto_8c

    .line 2754
    :cond_9e
    invoke-static {v0, v6, v1}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;II)V

    .line 714
    if-le v1, p2, :cond_a7

    .line 715
    add-int/lit8 v1, v1, -0x1

    move v6, v1

    goto :goto_7a

    .line 716
    :cond_a7
    if-ge v1, p2, :cond_b1

    .line 717
    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v4, v1

    .line 722
    goto :goto_7a

    .line 725
    :cond_b1
    aget-object v2, v0, v4

    .line 726
    add-int/lit8 v1, v4, 0x1

    :goto_b5
    if-ge v1, p2, :cond_5e

    .line 727
    aget-object v3, v0, v1

    invoke-virtual {p0, v2, v3}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 726
    add-int/lit8 v1, v1, 0x1

    goto :goto_b5

    .line 732
    :cond_c0
    invoke-static {v0, v5, v1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 734
    invoke-static {v1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 735
    invoke-static {v0, v1}, Lcom/a/b/d/yc;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_15

    :cond_d5
    move v1, v3

    goto :goto_5f
.end method

.method private b(Ljava/util/Comparator;)Lcom/a/b/d/yd;
    .registers 4
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 390
    new-instance v1, Lcom/a/b/d/cy;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/cy;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    return-object v1
.end method

.method private b(Ljava/lang/Iterable;I)Ljava/util/List;
    .registers 11

    .prologue
    .line 775
    invoke-virtual {p0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    .line 3615
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_31

    move-object v0, p1

    .line 3616
    check-cast v0, Ljava/util/Collection;

    .line 3617
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x2

    int-to-long v6, p2

    mul-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gtz v2, :cond_31

    .line 3623
    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 3624
    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 3625
    array-length v1, v0

    if-le v1, p2, :cond_28

    .line 3626
    invoke-static {v0, p2}, Lcom/a/b/d/yc;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 3628
    :cond_28
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_30
    return-object v0

    .line 3631
    :cond_31
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {v1, v0, p2}, Lcom/a/b/d/yd;->a(Ljava/util/Iterator;I)Ljava/util/List;

    move-result-object v0

    goto :goto_30
.end method

.method private b(Ljava/util/Iterator;I)Ljava/util/List;
    .registers 4

    .prologue
    .line 793
    invoke-virtual {p0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-direct {v0, p1, p2}, Lcom/a/b/d/yd;->a(Ljava/util/Iterator;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/a/b/d/yd;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 106
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    return-object v0
.end method

.method public static e()Lcom/a/b/d/yd;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 237
    sget-object v0, Lcom/a/b/d/agl;->a:Lcom/a/b/d/agl;

    return-object v0
.end method

.method private static e(Ljava/lang/Iterable;)Lcom/a/b/d/yd;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 411
    new-instance v0, Lcom/a/b/d/cy;

    invoke-direct {v0, p0}, Lcom/a/b/d/cy;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method private static f()Lcom/a/b/d/yd;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 225
    sget-object v0, Lcom/a/b/d/bj;->a:Lcom/a/b/d/bj;

    return-object v0
.end method

.method private f(Ljava/lang/Iterable;)Z
    .registers 5

    .prologue
    .line 857
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 858
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 859
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 860
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 861
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 862
    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_20

    .line 863
    const/4 v0, 0x0

    .line 868
    :goto_1f
    return v0

    :cond_20
    move-object v0, v1

    .line 866
    goto :goto_e

    .line 868
    :cond_22
    const/4 v0, 0x1

    goto :goto_1f
.end method

.method private static g()Lcom/a/b/d/yd;
    .registers 1

    .prologue
    .line 257
    sget-object v0, Lcom/a/b/d/yg;->a:Lcom/a/b/d/yd;

    return-object v0
.end method

.method private g(Ljava/lang/Iterable;)Z
    .registers 5

    .prologue
    .line 878
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 879
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 880
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 881
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 882
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 883
    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_20

    .line 884
    const/4 v0, 0x0

    .line 889
    :goto_1f
    return v0

    :cond_20
    move-object v0, v1

    .line 887
    goto :goto_e

    .line 889
    :cond_22
    const/4 v0, 0x1

    goto :goto_1f
.end method

.method private h()Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 373
    invoke-static {}, Lcom/a/b/d/sz;->a()Lcom/a/b/b/bj;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/yd;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method

.method private i()Lcom/a/b/d/yd;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 440
    new-instance v0, Lcom/a/b/d/ob;

    invoke-direct {v0, p0}, Lcom/a/b/d/ob;-><init>(Lcom/a/b/d/yd;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/a/b/d/yd;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 333
    new-instance v0, Lcom/a/b/d/zx;

    invoke-direct {v0, p0}, Lcom/a/b/d/zx;-><init>(Lcom/a/b/d/yd;)V

    return-object v0
.end method

.method public final a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;
    .registers 3
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 369
    new-instance v0, Lcom/a/b/d/ch;

    invoke-direct {v0, p1, p0}, Lcom/a/b/d/ch;-><init>(Lcom/a/b/b/bj;Lcom/a/b/d/yd;)V

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 499
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_7

    :goto_6
    return-object p1

    :cond_7
    move-object p1, p2

    goto :goto_6
.end method

.method public varargs a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 9
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 515
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 517
    array-length v2, p4

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_15

    aget-object v3, p4, v0

    .line 518
    invoke-virtual {p0, v1, v3}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 517
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 521
    :cond_15
    return-object v1
.end method

.method public a(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 463
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 465
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 466
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_4

    .line 469
    :cond_13
    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Ljava/util/List;
    .registers 3

    .prologue
    .line 816
    invoke-static {p1}, Lcom/a/b/d/mq;->c(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 817
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 818
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
    .registers 6

    .prologue
    .line 842
    invoke-static {p1}, Lcom/a/b/d/mq;->c(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 843
    array-length v2, v0

    const/4 v1, 0x0

    :goto_8
    if-ge v1, v2, :cond_12

    aget-object v3, v0, v1

    .line 844
    invoke-static {v3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 846
    :cond_12
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 847
    invoke-static {v0}, Lcom/a/b/d/jl;->b([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/a/b/d/yd;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 344
    new-instance v0, Lcom/a/b/d/ya;

    invoke-direct {v0, p0}, Lcom/a/b/d/ya;-><init>(Lcom/a/b/d/yd;)V

    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 575
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_7

    :goto_6
    return-object p1

    :cond_7
    move-object p1, p2

    goto :goto_6
.end method

.method public varargs b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 9
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 593
    array-length v2, p4

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_15

    aget-object v3, p4, v0

    .line 594
    invoke-virtual {p0, v1, v3}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 593
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 597
    :cond_15
    return-object v1
.end method

.method public b(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 539
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 541
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 542
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_4

    .line 545
    :cond_13
    return-object v0
.end method

.method public c()Lcom/a/b/d/yd;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 355
    new-instance v0, Lcom/a/b/d/yb;

    invoke-direct {v0, p0}, Lcom/a/b/d/yb;-><init>(Lcom/a/b/d/yd;)V

    return-object v0
.end method

.method public c(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 482
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/yd;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public d(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 558
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/yd;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
