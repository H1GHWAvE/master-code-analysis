.class public Lcom/a/b/d/jr;
.super Lcom/a/b/d/kk;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ou;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final d:J
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in emulated source"
    .end annotation
.end field


# instance fields
.field private transient a:Lcom/a/b/d/jr;


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;I)V
    .registers 3

    .prologue
    .line 272
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/kk;-><init>(Lcom/a/b/d/jt;I)V

    .line 273
    return-void
.end method

.method private static A()Lcom/a/b/d/jl;
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 324
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private static B()Lcom/a/b/d/jl;
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 335
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static a()Lcom/a/b/d/jr;
    .registers 1

    .prologue
    .line 64
    sget-object v0, Lcom/a/b/d/ex;->a:Lcom/a/b/d/ex;

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
    .registers 5

    .prologue
    .line 2137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 83
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 84
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 85
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
    .registers 7

    .prologue
    .line 3137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 95
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 96
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 97
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 98
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
    .registers 9

    .prologue
    .line 4137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 108
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 109
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 110
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 111
    invoke-virtual {v0, p6, p7}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 112
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
    .registers 11

    .prologue
    .line 5137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 122
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 123
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 124
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 125
    invoke-virtual {v0, p6, p7}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 126
    invoke-virtual {v0, p8, p9}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 127
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 12
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 351
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 352
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v4

    .line 353
    if-gez v4, :cond_25

    .line 354
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid key count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 356
    :cond_25
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v5

    move v2, v1

    move v3, v1

    .line 360
    :goto_2b
    if-ge v2, v4, :cond_6c

    .line 361
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v6

    .line 362
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v7

    .line 363
    if-gtz v7, :cond_52

    .line 364
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid value count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_52
    new-array v8, v7, [Ljava/lang/Object;

    move v0, v1

    .line 368
    :goto_55
    if-ge v0, v7, :cond_60

    .line 369
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v0

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_55

    .line 371
    :cond_60
    invoke-static {v8}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 372
    add-int/2addr v3, v7

    .line 360
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2b

    .line 377
    :cond_6c
    :try_start_6c
    invoke-virtual {v5}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;
    :try_end_6f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6c .. :try_end_6f} :catch_7b

    move-result-object v0

    .line 383
    sget-object v1, Lcom/a/b/d/kq;->a:Lcom/a/b/d/aab;

    invoke-virtual {v1, p0, v0}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 384
    sget-object v0, Lcom/a/b/d/kq;->b:Lcom/a/b/d/aab;

    invoke-virtual {v0, p0, v3}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;I)V

    .line 385
    return-void

    .line 378
    :catch_7b
    move-exception v0

    .line 379
    new-instance v1, Ljava/io/InvalidObjectException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/InvalidObjectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/InvalidObjectException;

    throw v0
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 344
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 345
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V

    .line 346
    return-void
.end method

.method private static c(Lcom/a/b/d/vi;)Lcom/a/b/d/jr;
    .registers 7

    .prologue
    .line 242
    invoke-interface {p0}, Lcom/a/b/d/vi;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 6064
    sget-object v0, Lcom/a/b/d/ex;->a:Lcom/a/b/d/ex;

    .line 268
    :cond_8
    :goto_8
    return-object v0

    .line 247
    :cond_9
    instance-of v0, p0, Lcom/a/b/d/jr;

    if-eqz v0, :cond_18

    move-object v0, p0

    .line 249
    check-cast v0, Lcom/a/b/d/jr;

    .line 6438
    iget-object v1, v0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->i_()Z

    move-result v1

    .line 251
    if-eqz v1, :cond_8

    .line 256
    :cond_18
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v3

    .line 257
    const/4 v0, 0x0

    .line 260
    invoke-interface {p0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_2a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_54

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 261
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v1

    .line 262
    invoke-virtual {v1}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5e

    .line 263
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 264
    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    add-int/2addr v0, v2

    :goto_52
    move v2, v0

    .line 266
    goto :goto_2a

    .line 268
    :cond_54
    new-instance v0, Lcom/a/b/d/jr;

    invoke-virtual {v3}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/jr;-><init>(Lcom/a/b/d/jt;I)V

    goto :goto_8

    :cond_5e
    move v0, v2

    goto :goto_52
.end method

.method public static c()Lcom/a/b/d/js;
    .registers 1

    .prologue
    .line 137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    return-object v0
.end method

.method public static d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;
    .registers 3

    .prologue
    .line 1137
    new-instance v0, Lcom/a/b/d/js;

    invoke-direct {v0}, Lcom/a/b/d/js;-><init>()V

    .line 73
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 74
    invoke-virtual {v0}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    return-object v0
.end method

.method private u()Lcom/a/b/d/jr;
    .registers 5

    .prologue
    .line 303
    iget-object v0, p0, Lcom/a/b/d/jr;->a:Lcom/a/b/d/jr;

    .line 304
    if-nez v0, :cond_31

    .line 8137
    new-instance v1, Lcom/a/b/d/js;

    invoke-direct {v1}, Lcom/a/b/d/js;-><init>()V

    .line 7309
    invoke-virtual {p0}, Lcom/a/b/d/jr;->v()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 7310
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_11

    .line 7312
    :cond_29
    invoke-virtual {v1}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 7313
    iput-object p0, v0, Lcom/a/b/d/jr;->a:Lcom/a/b/d/jr;

    .line 304
    iput-object v0, p0, Lcom/a/b/d/jr;->a:Lcom/a/b/d/jr;

    :cond_31
    return-object v0
.end method

.method private z()Lcom/a/b/d/jr;
    .registers 5

    .prologue
    .line 9137
    new-instance v1, Lcom/a/b/d/js;

    invoke-direct {v1}, Lcom/a/b/d/js;-><init>()V

    .line 309
    invoke-virtual {p0}, Lcom/a/b/d/jr;->v()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 310
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_d

    .line 312
    :cond_25
    invoke-virtual {v1}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 313
    iput-object p0, v0, Lcom/a/b/d/jr;->a:Lcom/a/b/d/jr;

    .line 314
    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/a/b/d/jr;->e(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 4

    .prologue
    .line 12335
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 11335
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 13324
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/a/b/d/jr;->e(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/a/b/d/kk;
    .registers 5

    .prologue
    .line 55
    .line 9303
    iget-object v0, p0, Lcom/a/b/d/jr;->a:Lcom/a/b/d/jr;

    .line 9304
    if-nez v0, :cond_31

    .line 10137
    new-instance v1, Lcom/a/b/d/js;

    invoke-direct {v1}, Lcom/a/b/d/js;-><init>()V

    .line 9309
    invoke-virtual {p0}, Lcom/a/b/d/jr;->v()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 9310
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_11

    .line 9312
    :cond_29
    invoke-virtual {v1}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 9313
    iput-object p0, v0, Lcom/a/b/d/jr;->a:Lcom/a/b/d/jr;

    .line 9304
    iput-object v0, p0, Lcom/a/b/d/jr;->a:Lcom/a/b/d/jr;

    :cond_31
    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 12324
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic e()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 10335
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e(Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 285
    iget-object v0, p0, Lcom/a/b/d/jr;->b:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jl;

    .line 286
    if-nez v0, :cond_e

    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    :cond_e
    return-object v0
.end method

.method public final synthetic h(Ljava/lang/Object;)Lcom/a/b/d/iz;
    .registers 3

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/a/b/d/jr;->e(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic t()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 11324
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
