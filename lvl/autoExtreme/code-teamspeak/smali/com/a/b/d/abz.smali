.class final Lcom/a/b/d/abz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field b:Ljava/util/Map$Entry;

.field c:Ljava/util/Iterator;

.field final synthetic d:Lcom/a/b/d/abx;


# direct methods
.method private constructor <init>(Lcom/a/b/d/abx;)V
    .registers 3

    .prologue
    .line 225
    iput-object p1, p0, Lcom/a/b/d/abz;->d:Lcom/a/b/d/abx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iget-object v0, p0, Lcom/a/b/d/abz;->d:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/abz;->a:Ljava/util/Iterator;

    .line 229
    invoke-static {}, Lcom/a/b/d/nj;->b()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/abx;B)V
    .registers 3

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lcom/a/b/d/abz;-><init>(Lcom/a/b/d/abx;)V

    return-void
.end method

.method private a()Lcom/a/b/d/adw;
    .registers 4

    .prologue
    .line 237
    iget-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_24

    .line 238
    iget-object v0, p0, Lcom/a/b/d/abz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/a/b/d/abz;->b:Ljava/util/Map$Entry;

    .line 239
    iget-object v0, p0, Lcom/a/b/d/abz;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    .line 241
    :cond_24
    iget-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 242
    iget-object v1, p0, Lcom/a/b/d/abz;->b:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/a/b/d/adx;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/a/b/d/abz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 225
    .line 1237
    iget-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_24

    .line 1238
    iget-object v0, p0, Lcom/a/b/d/abz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lcom/a/b/d/abz;->b:Ljava/util/Map$Entry;

    .line 1239
    iget-object v0, p0, Lcom/a/b/d/abz;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    .line 1241
    :cond_24
    iget-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1242
    iget-object v1, p0, Lcom/a/b/d/abz;->b:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/a/b/d/adx;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    .line 225
    return-object v0
.end method

.method public final remove()V
    .registers 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/a/b/d/abz;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 248
    iget-object v0, p0, Lcom/a/b/d/abz;->b:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 249
    iget-object v0, p0, Lcom/a/b/d/abz;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 251
    :cond_18
    return-void
.end method
