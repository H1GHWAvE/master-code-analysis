.class Lcom/a/b/d/up;
.super Lcom/a/b/d/uk;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# direct methods
.method constructor <init>(Ljava/util/SortedMap;)V
    .registers 2

    .prologue
    .line 3533
    invoke-direct {p0, p1}, Lcom/a/b/d/uk;-><init>(Ljava/util/Map;)V

    .line 3534
    return-void
.end method


# virtual methods
.method a()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 3538
    invoke-super {p0}, Lcom/a/b/d/uk;->b()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 3531
    invoke-virtual {p0}, Lcom/a/b/d/up;->a()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 3543
    invoke-virtual {p0}, Lcom/a/b/d/up;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3563
    invoke-virtual {p0}, Lcom/a/b/d/up;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 3553
    new-instance v0, Lcom/a/b/d/up;

    invoke-virtual {p0}, Lcom/a/b/d/up;->a()Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/up;-><init>(Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3568
    invoke-virtual {p0}, Lcom/a/b/d/up;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 3548
    new-instance v0, Lcom/a/b/d/up;

    invoke-virtual {p0}, Lcom/a/b/d/up;->a()Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/up;-><init>(Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 3558
    new-instance v0, Lcom/a/b/d/up;

    invoke-virtual {p0}, Lcom/a/b/d/up;->a()Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/up;-><init>(Ljava/util/SortedMap;)V

    return-object v0
.end method
