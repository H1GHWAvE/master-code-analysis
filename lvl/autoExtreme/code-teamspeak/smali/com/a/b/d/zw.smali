.class final Lcom/a/b/d/zw;
.super Lcom/a/b/d/yd;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field static final a:Lcom/a/b/d/zw;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 31
    new-instance v0, Lcom/a/b/d/zw;

    invoke-direct {v0}, Lcom/a/b/d/zw;-><init>()V

    sput-object v0, Lcom/a/b/d/zw;->a:Lcom/a/b/d/zw;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/a/b/d/yd;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .registers 3

    .prologue
    .line 34
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    if-ne p0, p1, :cond_7

    .line 36
    const/4 v0, 0x0

    .line 39
    :goto_6
    return v0

    :cond_7
    invoke-interface {p1, p0}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_6
.end method

.method private static varargs a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;[Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 5

    .prologue
    .line 53
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/a/b/d/xz;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method private static b(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 49
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/xz;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method private static varargs b(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;[Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 5

    .prologue
    .line 69
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/a/b/d/xz;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method private static c(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 65
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/xz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method private static c(Ljava/util/Iterator;)Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 57
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p0}, Lcom/a/b/d/xz;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method private static d(Ljava/util/Iterator;)Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 73
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p0}, Lcom/a/b/d/xz;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method private static e(Ljava/lang/Iterable;)Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 61
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p0}, Lcom/a/b/d/xz;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method private static f(Ljava/lang/Iterable;)Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 77
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p0}, Lcom/a/b/d/xz;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method private static f()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 82
    sget-object v0, Lcom/a/b/d/zw;->a:Lcom/a/b/d/zw;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 1106
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    .line 43
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    .line 6049
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/xz;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 27
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    check-cast p3, Ljava/lang/Comparable;

    check-cast p4, [Ljava/lang/Comparable;

    .line 5053
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/a/b/d/xz;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 27
    return-object v0
.end method

.method public final synthetic a(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 7057
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p1}, Lcom/a/b/d/xz;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 27
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    .line 3065
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/xz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 27
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    check-cast p3, Ljava/lang/Comparable;

    check-cast p4, [Ljava/lang/Comparable;

    .line 2069
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/a/b/d/xz;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 27
    return-object v0
.end method

.method public final synthetic b(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 4073
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p1}, Lcom/a/b/d/xz;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 27
    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 6061
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p1}, Lcom/a/b/d/xz;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 27
    return-object v0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    .line 8034
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8035
    if-ne p1, p2, :cond_b

    .line 8036
    const/4 v0, 0x0

    :goto_a
    return v0

    .line 8039
    :cond_b
    invoke-interface {p2, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_a
.end method

.method public final synthetic d(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3077
    sget-object v0, Lcom/a/b/d/xz;->a:Lcom/a/b/d/xz;

    invoke-virtual {v0, p1}, Lcom/a/b/d/xz;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 27
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 86
    const-string v0, "Ordering.natural().reverse()"

    return-object v0
.end method
