.class public final Lcom/a/b/d/mq;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 1037
    new-instance v0, Lcom/a/b/d/my;

    invoke-direct {v0}, Lcom/a/b/d/my;-><init>()V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/iz;)Ljava/lang/Iterable;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 82
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;I)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 539
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    if-lez p1, :cond_f

    const/4 v0, 0x1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 541
    new-instance v0, Lcom/a/b/d/nb;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/nb;-><init>(Ljava/lang/Iterable;I)V

    return-object v0

    .line 540
    :cond_f
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public static a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 708
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 710
    new-instance v0, Lcom/a/b/d/nf;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/nf;-><init>(Ljava/lang/Iterable;Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 435
    invoke-static {p0, p1}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 4

    .prologue
    .line 449
    invoke-static {p0, p1, p2}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 5

    .prologue
    .line 465
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/lang/Iterable;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1020
    const-string v0, "iterables"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1021
    const-string v0, "comparator"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1022
    new-instance v0, Lcom/a/b/d/mx;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/mx;-><init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V

    .line 1030
    new-instance v1, Lcom/a/b/d/ni;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/ni;-><init>(Ljava/lang/Iterable;B)V

    return-object v1
.end method

.method private static varargs a([Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 479
    invoke-static {p0}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a([Ljava/lang/Object;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 422
    invoke-static {p0}, Lcom/a/b/d/ov;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->d(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 748
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 749
    invoke-static {p1}, Lcom/a/b/d/nj;->a(I)V

    .line 750
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_17

    .line 2077
    check-cast p0, Ljava/util/List;

    .line 752
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_16

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    .line 756
    :cond_16
    :goto_16
    return-object p2

    .line 754
    :cond_17
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 755
    invoke-static {v0, p1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;I)I

    .line 756
    invoke-static {v0, p2}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_16
.end method

.method private static a(Ljava/lang/Iterable;Lcom/a/b/b/co;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 660
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/co;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/List;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 820
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 278
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->c(Ljava/util/Iterator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Lcom/a/b/b/co;II)V
    .registers 6

    .prologue
    .line 222
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-le v0, p3, :cond_18

    .line 223
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 224
    invoke-interface {p0, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 222
    :cond_15
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 228
    :cond_18
    add-int/lit8 v0, p3, -0x1

    :goto_1a
    if-lt v0, p2, :cond_22

    .line 229
    invoke-interface {p0, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 228
    add-int/lit8 v0, v0, -0x1

    goto :goto_1a

    .line 231
    :cond_22
    return-void
.end method

.method public static a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
    .registers 3

    .prologue
    .line 176
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_15

    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_15

    .line 177
    check-cast p0, Ljava/util/List;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    invoke-static {p0, v0}, Lcom/a/b/d/mq;->a(Ljava/util/List;Lcom/a/b/b/co;)Z

    move-result v0

    .line 180
    :goto_14
    return v0

    :cond_15
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/co;)Z

    move-result v0

    goto :goto_14
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 118
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_b

    .line 119
    check-cast p0, Ljava/util/Collection;

    .line 120
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    .line 122
    :goto_a
    return v0

    :cond_b
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_a
.end method

.method private static a(Ljava/lang/Iterable;Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 138
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_11

    check-cast p0, Ljava/util/Collection;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p0, v0}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    :goto_10
    return v0

    :cond_11
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    goto :goto_10
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
    .registers 3

    .prologue
    .line 350
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_d

    .line 351
    invoke-static {p1}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    .line 352
    invoke-interface {p0, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v0

    .line 354
    :goto_c
    return v0

    :cond_d
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    move-result v0

    goto :goto_c
.end method

.method private static a(Ljava/util/List;Lcom/a/b/b/co;)Z
    .registers 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 187
    move v0, v1

    move v2, v1

    .line 190
    :goto_4
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_43

    .line 191
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 192
    invoke-interface {p1, v4}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1b

    .line 193
    if-le v2, v0, :cond_19

    .line 195
    :try_start_16
    invoke-interface {p0, v0, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_19
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_16 .. :try_end_19} :catch_1e

    .line 201
    :cond_19
    add-int/lit8 v0, v0, 0x1

    .line 190
    :cond_1b
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 197
    :catch_1e
    move-exception v1

    .line 1222
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_25
    if-le v1, v2, :cond_37

    .line 1223
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_34

    .line 1224
    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1222
    :cond_34
    add-int/lit8 v1, v1, -0x1

    goto :goto_25

    .line 1228
    :cond_37
    add-int/lit8 v1, v2, -0x1

    :goto_39
    if-lt v1, v0, :cond_41

    .line 1229
    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1228
    add-int/lit8 v1, v1, -0x1

    goto :goto_39

    :cond_41
    move v1, v3

    .line 207
    :cond_42
    :goto_42
    return v1

    .line 206
    :cond_43
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p0, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 207
    if-eq v2, v0, :cond_42

    move v1, v3

    goto :goto_42
.end method

.method public static a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "Array.newInstance(Class, int)"
    .end annotation

    .prologue
    .line 315
    invoke-static {p0}, Lcom/a/b/d/mq;->i(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    .line 316
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {p1, v1}, Lcom/a/b/d/yc;->a(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v1

    .line 317
    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic b()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 4037
    new-instance v0, Lcom/a/b/d/my;

    invoke-direct {v0}, Lcom/a/b/d/my;-><init>()V

    .line 60
    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;I)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 568
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    if-lez p1, :cond_f

    const/4 v0, 0x1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 570
    new-instance v0, Lcom/a/b/d/nc;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/nc;-><init>(Ljava/lang/Iterable;I)V

    return-object v0

    .line 569
    :cond_f
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private static b(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "Class.isInstance"
    .end annotation

    .prologue
    .line 608
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 609
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    new-instance v0, Lcom/a/b/d/ne;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ne;-><init>(Ljava/lang/Iterable;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 289
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;
    .registers 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 238
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 240
    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 241
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 242
    invoke-interface {p1, v0}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 243
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 247
    :goto_1a
    return-object v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method private static b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 302
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 259
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_1a

    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_1a

    move-object v0, p0

    .line 260
    check-cast v0, Ljava/util/Collection;

    move-object v1, p1

    .line 261
    check-cast v1, Ljava/util/Collection;

    .line 262
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-eq v0, v1, :cond_1a

    .line 263
    const/4 v0, 0x0

    .line 266
    :goto_19
    return v0

    :cond_1a
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/util/Iterator;)Z

    move-result v0

    goto :goto_19
.end method

.method private static b(Ljava/lang/Iterable;Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 156
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_11

    check-cast p0, Ljava/util/Collection;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p0, v0}, Ljava/util/Collection;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    :goto_10
    return v0

    :cond_11
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    goto :goto_10
.end method

.method private static c(Ljava/lang/Iterable;Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 365
    instance-of v0, p0, Lcom/a/b/d/xc;

    if-eqz v0, :cond_b

    .line 366
    check-cast p0, Lcom/a/b/d/xc;

    invoke-interface {p0, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v0

    .line 370
    :goto_a
    return v0

    .line 367
    :cond_b
    instance-of v0, p0, Ljava/util/Set;

    if-eqz v0, :cond_1b

    .line 368
    check-cast p0, Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    goto :goto_a

    :cond_19
    const/4 v0, 0x0

    goto :goto_a

    .line 370
    :cond_1b
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->c(Ljava/util/Iterator;Ljava/lang/Object;)I

    move-result v0

    goto :goto_a
.end method

.method public static c(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 584
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 586
    new-instance v0, Lcom/a/b/d/nd;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/nd;-><init>(Ljava/lang/Iterable;Lcom/a/b/b/co;)V

    return-object v0
.end method

.method private static c(Ljava/lang/Iterable;I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 727
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 728
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_e

    check-cast p0, Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_d
    return-object v0

    :cond_e
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->c(Ljava/util/Iterator;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_d
.end method

.method static c(Ljava/lang/Iterable;)[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 328
    invoke-static {p0}, Lcom/a/b/d/mq;->i(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 391
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    new-instance v0, Lcom/a/b/d/mr;

    invoke-direct {v0, p0}, Lcom/a/b/d/mr;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method private static d(Ljava/lang/Iterable;I)Ljava/lang/Iterable;
    .registers 4

    .prologue
    .line 845
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    if-ltz p1, :cond_17

    const/4 v0, 0x1

    :goto_6
    const-string v1, "number to skip cannot be negative"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 848
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_19

    .line 849
    check-cast p0, Ljava/util/List;

    .line 850
    new-instance v0, Lcom/a/b/d/ng;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ng;-><init>(Ljava/util/List;I)V

    .line 860
    :goto_16
    return-object v0

    .line 846
    :cond_17
    const/4 v0, 0x0

    goto :goto_6

    .line 860
    :cond_19
    new-instance v0, Lcom/a/b/d/ms;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ms;-><init>(Ljava/lang/Iterable;I)V

    goto :goto_16
.end method

.method private static d(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 807
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_1a

    .line 808
    invoke-static {p0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    .line 809
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 816
    :goto_e
    return-object p1

    .line 811
    :cond_f
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_1a

    .line 3077
    check-cast p0, Ljava/util/List;

    .line 812
    invoke-static {p0}, Lcom/a/b/d/mq;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_e

    .line 816
    :cond_1a
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->e(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_e
.end method

.method public static d(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
    .registers 3

    .prologue
    .line 623
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->c(Ljava/util/Iterator;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public static e(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 494
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    new-instance v0, Lcom/a/b/d/mz;

    invoke-direct {v0, p0}, Lcom/a/b/d/mz;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method private static e(Ljava/lang/Iterable;I)Ljava/lang/Iterable;
    .registers 4

    .prologue
    .line 911
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 912
    if-ltz p1, :cond_11

    const/4 v0, 0x1

    :goto_6
    const-string v1, "limit is negative"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 913
    new-instance v0, Lcom/a/b/d/mu;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/mu;-><init>(Ljava/lang/Iterable;I)V

    return-object v0

    .line 912
    :cond_11
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public static e(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
    .registers 3

    .prologue
    .line 632
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public static f(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 775
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static f(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 646
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->e(Ljava/util/Iterator;Lcom/a/b/b/co;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static g(Ljava/lang/Iterable;Lcom/a/b/b/co;)Lcom/a/b/b/ci;
    .registers 3

    .prologue
    .line 675
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->f(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/b/ci;

    move-result-object v0

    return-object v0
.end method

.method private static g(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 66
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    instance-of v0, p0, Lcom/a/b/d/ni;

    if-nez v0, :cond_b

    instance-of v0, p0, Lcom/a/b/d/iz;

    if-eqz v0, :cond_c

    .line 71
    :cond_b
    :goto_b
    return-object p0

    :cond_c
    new-instance v0, Lcom/a/b/d/ni;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ni;-><init>(Ljava/lang/Iterable;B)V

    move-object p0, v0

    goto :goto_b
.end method

.method private static h(Ljava/lang/Iterable;)I
    .registers 2

    .prologue
    .line 108
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    goto :goto_a
.end method

.method private static h(Ljava/lang/Iterable;Lcom/a/b/b/co;)I
    .registers 3

    .prologue
    .line 691
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->g(Ljava/util/Iterator;Lcom/a/b/b/co;)I

    move-result v0

    return v0
.end method

.method private static i(Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 2

    .prologue
    .line 337
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_7

    check-cast p0, Ljava/util/Collection;

    :goto_6
    return-object p0

    :cond_7
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object p0

    goto :goto_6
.end method

.method private static j(Ljava/lang/Iterable;)Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 508
    new-instance v0, Lcom/a/b/d/na;

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/na;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method private static k(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 786
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_17

    .line 787
    check-cast p0, Ljava/util/List;

    .line 788
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 789
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 791
    :cond_12
    invoke-static {p0}, Lcom/a/b/d/mq;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    .line 794
    :goto_16
    return-object v0

    :cond_17
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->f(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_16
.end method

.method private static l(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 941
    instance-of v0, p0, Ljava/util/Queue;

    if-eqz v0, :cond_a

    .line 942
    new-instance v0, Lcom/a/b/d/mv;

    invoke-direct {v0, p0}, Lcom/a/b/d/mv;-><init>(Ljava/lang/Iterable;)V

    .line 957
    :goto_9
    return-object v0

    .line 955
    :cond_a
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 957
    new-instance v0, Lcom/a/b/d/mw;

    invoke-direct {v0, p0}, Lcom/a/b/d/mw;-><init>(Ljava/lang/Iterable;)V

    goto :goto_9
.end method

.method private static m(Ljava/lang/Iterable;)Z
    .registers 2

    .prologue
    .line 998
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_b

    .line 999
    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    .line 1001
    :goto_a
    return v0

    :cond_b
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_17

    const/4 v0, 0x1

    goto :goto_a

    :cond_17
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private static synthetic n(Ljava/lang/Iterable;)Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3508
    new-instance v0, Lcom/a/b/d/na;

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/na;-><init>(Ljava/util/Iterator;)V

    .line 60
    return-object v0
.end method
