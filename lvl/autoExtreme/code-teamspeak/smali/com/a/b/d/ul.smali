.class Lcom/a/b/d/ul;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/qj;


# instance fields
.field final a:Ljava/util/Map;

.field final b:Ljava/util/Map;

.field final c:Ljava/util/Map;

.field final d:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .registers 6

    .prologue
    .line 461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462
    invoke-static {p1}, Lcom/a/b/d/sz;->b(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ul;->a:Ljava/util/Map;

    .line 463
    invoke-static {p2}, Lcom/a/b/d/sz;->b(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ul;->b:Ljava/util/Map;

    .line 464
    invoke-static {p3}, Lcom/a/b/d/sz;->b(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ul;->c:Ljava/util/Map;

    .line 465
    invoke-static {p4}, Lcom/a/b/d/sz;->b(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ul;->d:Ljava/util/Map;

    .line 466
    return-void
.end method


# virtual methods
.method public final a()Z
    .registers 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/a/b/d/ul;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/a/b/d/ul;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/a/b/d/ul;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 475
    iget-object v0, p0, Lcom/a/b/d/ul;->a:Ljava/util/Map;

    return-object v0
.end method

.method public c()Ljava/util/Map;
    .registers 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/a/b/d/ul;->b:Ljava/util/Map;

    return-object v0
.end method

.method public d()Ljava/util/Map;
    .registers 2

    .prologue
    .line 485
    iget-object v0, p0, Lcom/a/b/d/ul;->c:Ljava/util/Map;

    return-object v0
.end method

.method public e()Ljava/util/Map;
    .registers 2

    .prologue
    .line 490
    iget-object v0, p0, Lcom/a/b/d/ul;->d:Ljava/util/Map;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 494
    if-ne p1, p0, :cond_5

    .line 504
    :cond_4
    :goto_4
    return v0

    .line 497
    :cond_5
    instance-of v2, p1, Lcom/a/b/d/qj;

    if-eqz v2, :cond_45

    .line 498
    check-cast p1, Lcom/a/b/d/qj;

    .line 499
    invoke-virtual {p0}, Lcom/a/b/d/ul;->b()Ljava/util/Map;

    move-result-object v2

    invoke-interface {p1}, Lcom/a/b/d/qj;->b()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_43

    invoke-virtual {p0}, Lcom/a/b/d/ul;->c()Ljava/util/Map;

    move-result-object v2

    invoke-interface {p1}, Lcom/a/b/d/qj;->c()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_43

    invoke-virtual {p0}, Lcom/a/b/d/ul;->d()Ljava/util/Map;

    move-result-object v2

    invoke-interface {p1}, Lcom/a/b/d/qj;->d()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_43

    invoke-virtual {p0}, Lcom/a/b/d/ul;->e()Ljava/util/Map;

    move-result-object v2

    invoke-interface {p1}, Lcom/a/b/d/qj;->e()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_43
    move v0, v1

    goto :goto_4

    :cond_45
    move v0, v1

    .line 504
    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 508
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/a/b/d/ul;->b()Ljava/util/Map;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/a/b/d/ul;->c()Ljava/util/Map;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/a/b/d/ul;->d()Ljava/util/Map;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/a/b/d/ul;->e()Ljava/util/Map;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 508
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 513
    .line 1470
    iget-object v0, p0, Lcom/a/b/d/ul;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/a/b/d/ul;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/a/b/d/ul;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    .line 513
    :goto_19
    if-eqz v0, :cond_20

    .line 514
    const-string v0, "equal"

    .line 527
    :goto_1d
    return-object v0

    .line 1470
    :cond_1e
    const/4 v0, 0x0

    goto :goto_19

    .line 517
    :cond_20
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "not equal"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 518
    iget-object v1, p0, Lcom/a/b/d/ul;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3a

    .line 519
    const-string v1, ": only on left="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/ul;->a:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 521
    :cond_3a
    iget-object v1, p0, Lcom/a/b/d/ul;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4d

    .line 522
    const-string v1, ": only on right="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/ul;->b:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 524
    :cond_4d
    iget-object v1, p0, Lcom/a/b/d/ul;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_60

    .line 525
    const-string v1, ": value differences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/ul;->d:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 527
    :cond_60
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1d
.end method
