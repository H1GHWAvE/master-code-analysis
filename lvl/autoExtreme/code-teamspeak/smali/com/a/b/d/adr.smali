.class Lcom/a/b/d/adr;
.super Lcom/a/b/d/adj;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/aac;


# static fields
.field private static final i:J


# instance fields
.field transient f:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/a/b/d/aac;)V
    .registers 2

    .prologue
    .line 742
    invoke-direct {p0, p1}, Lcom/a/b/d/adj;-><init>(Lcom/a/b/d/vi;)V

    .line 743
    return-void
.end method


# virtual methods
.method synthetic a()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 736
    invoke-virtual {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 5

    .prologue
    .line 748
    iget-object v1, p0, Lcom/a/b/d/adr;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 749
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/aac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adr;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 750
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 5

    .prologue
    .line 759
    iget-object v1, p0, Lcom/a/b/d/adr;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 760
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/aac;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 761
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 736
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/adr;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 753
    iget-object v1, p0, Lcom/a/b/d/adr;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 754
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/aac;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 755
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method c()Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 745
    invoke-super {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aac;

    return-object v0
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 736
    invoke-virtual {p0, p1}, Lcom/a/b/d/adr;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 736
    invoke-virtual {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 736
    invoke-virtual {p0, p1}, Lcom/a/b/d/adr;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 736
    invoke-virtual {p0}, Lcom/a/b/d/adr;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/util/Set;
    .registers 4

    .prologue
    .line 764
    iget-object v1, p0, Lcom/a/b/d/adr;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 765
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adr;->f:Ljava/util/Set;

    if-nez v0, :cond_17

    .line 766
    invoke-virtual {p0}, Lcom/a/b/d/adr;->c()Lcom/a/b/d/aac;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/aac;->u()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adr;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adr;->f:Ljava/util/Set;

    .line 768
    :cond_17
    iget-object v0, p0, Lcom/a/b/d/adr;->f:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 769
    :catchall_1b
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    throw v0
.end method
