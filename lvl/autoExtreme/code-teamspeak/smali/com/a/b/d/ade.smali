.class final Lcom/a/b/d/ade;
.super Lcom/a/b/d/ado;
.source "SourceFile"

# interfaces
.implements Ljava/util/Deque;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "Deque"
.end annotation


# static fields
.field private static final a:J


# direct methods
.method constructor <init>(Ljava/util/Deque;)V
    .registers 2

    .prologue
    .line 1633
    invoke-direct {p0, p1}, Lcom/a/b/d/ado;-><init>(Ljava/util/Queue;)V

    .line 1634
    return-void
.end method

.method private c()Ljava/util/Deque;
    .registers 2

    .prologue
    .line 1637
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a()Ljava/util/Queue;
    .registers 2

    .prologue
    .line 1628
    .line 19637
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1628
    return-object v0
.end method

.method public final addFirst(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 1642
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1643
    invoke-interface {v0, p1}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 1644
    monitor-exit v1

    return-void

    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public final addLast(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 1649
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 3637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1650
    invoke-interface {v0, p1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 1651
    monitor-exit v1

    return-void

    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1628
    .line 20637
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1628
    return-object v0
.end method

.method final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1628
    .line 21637
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1628
    return-object v0
.end method

.method public final descendingIterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 1754
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 18637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1755
    invoke-interface {v0}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1756
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final getFirst()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1698
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 10637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1699
    invoke-interface {v0}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1700
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final getLast()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1705
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 11637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1706
    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1707
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final offerFirst(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1656
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 4637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1657
    invoke-interface {v0, p1}, Ljava/util/Deque;->offerFirst(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1658
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final offerLast(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1663
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 5637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1664
    invoke-interface {v0, p1}, Ljava/util/Deque;->offerLast(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1665
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final peekFirst()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1712
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 12637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1713
    invoke-interface {v0}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1714
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final peekLast()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1719
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 13637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1720
    invoke-interface {v0}, Ljava/util/Deque;->peekLast()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1721
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final pollFirst()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1684
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 8637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1685
    invoke-interface {v0}, Ljava/util/Deque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1686
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final pollLast()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1691
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 9637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1692
    invoke-interface {v0}, Ljava/util/Deque;->pollLast()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1693
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final pop()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1747
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 17637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1748
    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1749
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final push(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 1740
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 16637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1741
    invoke-interface {v0, p1}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 1742
    monitor-exit v1

    return-void

    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public final removeFirst()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1670
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 6637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1671
    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1672
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final removeFirstOccurrence(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1726
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 14637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1727
    invoke-interface {v0, p1}, Ljava/util/Deque;->removeFirstOccurrence(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1728
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final removeLast()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1677
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 7637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1678
    invoke-interface {v0}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1679
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final removeLastOccurrence(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1733
    iget-object v1, p0, Lcom/a/b/d/ade;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 15637
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 1734
    invoke-interface {v0, p1}, Ljava/util/Deque;->removeLastOccurrence(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1735
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method
