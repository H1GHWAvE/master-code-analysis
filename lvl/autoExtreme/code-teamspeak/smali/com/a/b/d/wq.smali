.class final Lcom/a/b/d/wq;
.super Lcom/a/b/d/xr;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/wn;


# direct methods
.method constructor <init>(Lcom/a/b/d/wn;)V
    .registers 2

    .prologue
    .line 1550
    iput-object p1, p0, Lcom/a/b/d/wq;->a:Lcom/a/b/d/wn;

    invoke-direct {p0}, Lcom/a/b/d/xr;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 1552
    iget-object v0, p0, Lcom/a/b/d/wq;->a:Lcom/a/b/d/wn;

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1568
    instance-of v0, p1, Lcom/a/b/d/xd;

    if-eqz v0, :cond_29

    .line 1569
    check-cast p1, Lcom/a/b/d/xd;

    .line 1570
    iget-object v0, p0, Lcom/a/b/d/wq;->a:Lcom/a/b/d/wn;

    iget-object v0, v0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1571
    if-eqz v0, :cond_27

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/a/b/d/xd;->b()I

    move-result v2

    if-ne v0, v2, :cond_27

    const/4 v0, 0x1

    .line 1573
    :goto_26
    return v0

    :cond_27
    move v0, v1

    .line 1571
    goto :goto_26

    :cond_29
    move v0, v1

    .line 1573
    goto :goto_26
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 1564
    iget-object v0, p0, Lcom/a/b/d/wq;->a:Lcom/a/b/d/wn;

    iget-object v0, v0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->n()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1556
    iget-object v0, p0, Lcom/a/b/d/wq;->a:Lcom/a/b/d/wn;

    invoke-virtual {v0}, Lcom/a/b/d/wn;->b()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1577
    instance-of v0, p1, Lcom/a/b/d/xd;

    if-eqz v0, :cond_29

    .line 1578
    check-cast p1, Lcom/a/b/d/xd;

    .line 1579
    iget-object v0, p0, Lcom/a/b/d/wq;->a:Lcom/a/b/d/wn;

    iget-object v0, v0, Lcom/a/b/d/wn;->b:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1580
    if-eqz v0, :cond_29

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-interface {p1}, Lcom/a/b/d/xd;->b()I

    move-result v2

    if-ne v1, v2, :cond_29

    .line 1581
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1582
    const/4 v0, 0x1

    .line 1585
    :goto_28
    return v0

    :cond_29
    const/4 v0, 0x0

    goto :goto_28
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 1560
    iget-object v0, p0, Lcom/a/b/d/wq;->a:Lcom/a/b/d/wn;

    invoke-virtual {v0}, Lcom/a/b/d/wn;->c()I

    move-result v0

    return v0
.end method
