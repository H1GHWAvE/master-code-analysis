.class public abstract enum Lcom/a/b/d/sh;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/d/sh;

.field public static final enum b:Lcom/a/b/d/sh;

.field public static final enum c:Lcom/a/b/d/sh;

.field private static final synthetic d:[Lcom/a/b/d/sh;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 295
    new-instance v0, Lcom/a/b/d/si;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lcom/a/b/d/si;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    .line 308
    new-instance v0, Lcom/a/b/d/sj;

    const-string v1, "SOFT"

    invoke-direct {v0, v1}, Lcom/a/b/d/sj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/sh;->b:Lcom/a/b/d/sh;

    .line 321
    new-instance v0, Lcom/a/b/d/sk;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lcom/a/b/d/sk;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    .line 289
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/a/b/d/sh;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/d/sh;->b:Lcom/a/b/d/sh;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/d/sh;->d:[Lcom/a/b/d/sh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 289
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 289
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/sh;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/d/sh;
    .registers 2

    .prologue
    .line 289
    const-class v0, Lcom/a/b/d/sh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/sh;

    return-object v0
.end method

.method public static values()[Lcom/a/b/d/sh;
    .registers 1

    .prologue
    .line 289
    sget-object v0, Lcom/a/b/d/sh;->d:[Lcom/a/b/d/sh;

    invoke-virtual {v0}, [Lcom/a/b/d/sh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/d/sh;

    return-object v0
.end method


# virtual methods
.method abstract a()Lcom/a/b/b/au;
.end method

.method abstract a(Lcom/a/b/d/sa;Lcom/a/b/d/rz;Ljava/lang/Object;)Lcom/a/b/d/sr;
.end method
