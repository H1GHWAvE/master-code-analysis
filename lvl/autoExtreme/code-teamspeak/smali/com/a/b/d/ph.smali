.class final Lcom/a/b/d/ph;
.super Lcom/a/b/d/jl;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 707
    invoke-direct {p0}, Lcom/a/b/d/jl;-><init>()V

    .line 708
    iput-object p1, p0, Lcom/a/b/d/ph;->a:Ljava/lang/String;

    .line 709
    return-void
.end method

.method private b(I)Ljava/lang/Character;
    .registers 3

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/a/b/d/ph;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 733
    iget-object v0, p0, Lcom/a/b/d/ph;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(II)Lcom/a/b/d/jl;
    .registers 5

    .prologue
    .line 723
    invoke-virtual {p0}, Lcom/a/b/d/ph;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 724
    iget-object v0, p0, Lcom/a/b/d/ph;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1698
    new-instance v1, Lcom/a/b/d/ph;

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/a/b/d/ph;-><init>(Ljava/lang/String;)V

    .line 724
    return-object v1
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 701
    .line 1732
    invoke-virtual {p0}, Lcom/a/b/d/ph;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 1733
    iget-object v0, p0, Lcom/a/b/d/ph;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 701
    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 728
    const/4 v0, 0x0

    return v0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 712
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/a/b/d/ph;->a:Ljava/lang/String;

    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    :goto_10
    return v0

    :cond_11
    const/4 v0, -0x1

    goto :goto_10
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 717
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/a/b/d/ph;->a:Ljava/lang/String;

    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    :goto_10
    return v0

    :cond_11
    const/4 v0, -0x1

    goto :goto_10
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 737
    iget-object v0, p0, Lcom/a/b/d/ph;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public final synthetic subList(II)Ljava/util/List;
    .registers 4

    .prologue
    .line 701
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ph;->a(II)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method
