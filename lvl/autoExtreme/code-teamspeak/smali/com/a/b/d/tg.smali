.class final Lcom/a/b/d/tg;
.super Lcom/a/b/d/he;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/NavigableSet;


# direct methods
.method constructor <init>(Ljava/util/NavigableSet;)V
    .registers 2

    .prologue
    .line 1024
    iput-object p1, p0, Lcom/a/b/d/tg;->a:Ljava/util/NavigableSet;

    invoke-direct {p0}, Lcom/a/b/d/he;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1024
    .line 3027
    iget-object v0, p0, Lcom/a/b/d/tg;->a:Ljava/util/NavigableSet;

    .line 1024
    return-object v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1032
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 1037
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1024
    .line 4027
    iget-object v0, p0, Lcom/a/b/d/tg;->a:Ljava/util/NavigableSet;

    .line 1024
    return-object v0
.end method

.method protected final bridge synthetic c()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 1024
    .line 2027
    iget-object v0, p0, Lcom/a/b/d/tg;->a:Ljava/util/NavigableSet;

    .line 1024
    return-object v0
.end method

.method protected final d()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/a/b/d/tg;->a:Ljava/util/NavigableSet;

    return-object v0
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 1075
    invoke-super {p0}, Lcom/a/b/d/he;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 1058
    invoke-super {p0, p1, p2}, Lcom/a/b/d/he;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1042
    invoke-super {p0, p1}, Lcom/a/b/d/he;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1024
    .line 5027
    iget-object v0, p0, Lcom/a/b/d/tg;->a:Ljava/util/NavigableSet;

    .line 1024
    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 1069
    invoke-super {p0, p1, p2, p3, p4}, Lcom/a/b/d/he;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 1047
    invoke-super {p0, p1, p2}, Lcom/a/b/d/he;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 1063
    invoke-super {p0, p1, p2}, Lcom/a/b/d/he;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1053
    invoke-super {p0, p1}, Lcom/a/b/d/he;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method
