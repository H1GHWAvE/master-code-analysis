.class final Lcom/a/b/d/aae;
.super Lcom/a/b/d/aaq;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Set;

.field final synthetic b:Ljava/util/Set;

.field final synthetic c:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .registers 5

    .prologue
    .line 586
    iput-object p1, p0, Lcom/a/b/d/aae;->a:Ljava/util/Set;

    iput-object p2, p0, Lcom/a/b/d/aae;->b:Ljava/util/Set;

    iput-object p3, p0, Lcom/a/b/d/aae;->c:Ljava/util/Set;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/d/aaq;-><init>(B)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 606
    new-instance v0, Lcom/a/b/d/lp;

    invoke-direct {v0}, Lcom/a/b/d/lp;-><init>()V

    iget-object v1, p0, Lcom/a/b/d/aae;->a:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/a/b/d/lp;->b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aae;->c:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/a/b/d/lp;->b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Set;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 601
    iget-object v0, p0, Lcom/a/b/d/aae;->a:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 602
    iget-object v0, p0, Lcom/a/b/d/aae;->c:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 603
    return-object p1
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 598
    iget-object v0, p0, Lcom/a/b/d/aae;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/a/b/d/aae;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 591
    iget-object v0, p0, Lcom/a/b/d/aae;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/d/aae;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 594
    iget-object v0, p0, Lcom/a/b/d/aae;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aae;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 588
    iget-object v0, p0, Lcom/a/b/d/aae;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/aae;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
