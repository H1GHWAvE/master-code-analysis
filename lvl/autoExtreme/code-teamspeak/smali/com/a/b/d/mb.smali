.class public final Lcom/a/b/d/mb;
.super Lcom/a/b/d/kw;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .registers 3

    .prologue
    .line 444
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v0}, Lcom/a/b/d/aer;->a(Ljava/util/Comparator;)Lcom/a/b/d/aer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/kw;-><init>(Lcom/a/b/d/xc;)V

    .line 445
    return-void
.end method

.method private c(Ljava/lang/Iterable;)Lcom/a/b/d/mb;
    .registers 2

    .prologue
    .line 515
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b(Ljava/lang/Iterable;)Lcom/a/b/d/kw;

    .line 516
    return-object p0
.end method

.method private c(Ljava/lang/Object;)Lcom/a/b/d/mb;
    .registers 2

    .prologue
    .line 456
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    .line 457
    return-object p0
.end method

.method private varargs c([Ljava/lang/Object;)Lcom/a/b/d/mb;
    .registers 2

    .prologue
    .line 502
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b([Ljava/lang/Object;)Lcom/a/b/d/kw;

    .line 503
    return-object p0
.end method

.method private d(Ljava/lang/Object;I)Lcom/a/b/d/mb;
    .registers 3

    .prologue
    .line 489
    invoke-super {p0, p1, p2}, Lcom/a/b/d/kw;->b(Ljava/lang/Object;I)Lcom/a/b/d/kw;

    .line 490
    return-object p0
.end method


# virtual methods
.method public final synthetic a()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 438
    invoke-virtual {p0}, Lcom/a/b/d/mb;->c()Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;
    .registers 2

    .prologue
    .line 438
    .line 6515
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b(Ljava/lang/Iterable;)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final synthetic a(Ljava/util/Iterator;)Lcom/a/b/d/jb;
    .registers 2

    .prologue
    .line 438
    .line 5528
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b(Ljava/util/Iterator;)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final synthetic a([Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 2

    .prologue
    .line 438
    .line 7502
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b([Ljava/lang/Object;)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/a/b/d/kw;
    .registers 2

    .prologue
    .line 438
    .line 5456
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final synthetic a(Ljava/lang/Object;I)Lcom/a/b/d/kw;
    .registers 4

    .prologue
    .line 438
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/mb;->c(Ljava/lang/Object;I)Lcom/a/b/d/mb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 2

    .prologue
    .line 438
    .line 8456
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final synthetic b()Lcom/a/b/d/ku;
    .registers 2

    .prologue
    .line 438
    invoke-virtual {p0}, Lcom/a/b/d/mb;->c()Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Iterable;)Lcom/a/b/d/kw;
    .registers 2

    .prologue
    .line 438
    .line 2515
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b(Ljava/lang/Iterable;)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;I)Lcom/a/b/d/kw;
    .registers 3

    .prologue
    .line 438
    .line 4489
    invoke-super {p0, p1, p2}, Lcom/a/b/d/kw;->b(Ljava/lang/Object;I)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final bridge synthetic b(Ljava/util/Iterator;)Lcom/a/b/d/kw;
    .registers 2

    .prologue
    .line 438
    .line 1528
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b(Ljava/util/Iterator;)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final bridge synthetic b([Ljava/lang/Object;)Lcom/a/b/d/kw;
    .registers 2

    .prologue
    .line 438
    .line 3502
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b([Ljava/lang/Object;)Lcom/a/b/d/kw;

    .line 438
    return-object p0
.end method

.method public final c()Lcom/a/b/d/ma;
    .registers 2

    .prologue
    .line 538
    iget-object v0, p0, Lcom/a/b/d/mb;->a:Lcom/a/b/d/xc;

    check-cast v0, Lcom/a/b/d/abn;

    invoke-static {v0}, Lcom/a/b/d/ma;->a(Lcom/a/b/d/abn;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;I)Lcom/a/b/d/mb;
    .registers 3

    .prologue
    .line 473
    invoke-super {p0, p1, p2}, Lcom/a/b/d/kw;->a(Ljava/lang/Object;I)Lcom/a/b/d/kw;

    .line 474
    return-object p0
.end method

.method public final c(Ljava/util/Iterator;)Lcom/a/b/d/mb;
    .registers 2

    .prologue
    .line 528
    invoke-super {p0, p1}, Lcom/a/b/d/kw;->b(Ljava/util/Iterator;)Lcom/a/b/d/kw;

    .line 529
    return-object p0
.end method
