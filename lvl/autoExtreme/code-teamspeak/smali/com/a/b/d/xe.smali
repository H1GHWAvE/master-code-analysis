.class public final Lcom/a/b/d/xe;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:Lcom/a/b/d/yd;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1080
    new-instance v0, Lcom/a/b/d/xn;

    invoke-direct {v0}, Lcom/a/b/d/xn;-><init>()V

    sput-object v0, Lcom/a/b/d/xe;->a:Lcom/a/b/d/yd;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/a/b/d/xc;Ljava/lang/Object;I)I
    .registers 5

    .prologue
    .line 891
    const-string v0, "count"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 893
    invoke-interface {p0, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v0

    .line 895
    sub-int v1, p2, v0

    .line 896
    if-lez v1, :cond_11

    .line 897
    invoke-interface {p0, p1, v1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;I)I

    .line 902
    :cond_10
    :goto_10
    return v0

    .line 898
    :cond_11
    if-gez v1, :cond_10

    .line 899
    neg-int v1, v1

    invoke-interface {p0, p1, v1}, Lcom/a/b/d/xc;->b(Ljava/lang/Object;I)I

    goto :goto_10
.end method

.method static a(Ljava/lang/Iterable;)I
    .registers 2

    .prologue
    .line 358
    instance-of v0, p0, Lcom/a/b/d/xc;

    if-eqz v0, :cond_f

    .line 359
    check-cast p0, Lcom/a/b/d/xc;

    invoke-interface {p0}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 361
    :goto_e
    return v0

    :cond_f
    const/16 v0, 0xb

    goto :goto_e
.end method

.method public static a(Lcom/a/b/d/abn;)Lcom/a/b/d/abn;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 200
    new-instance v1, Lcom/a/b/d/agk;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abn;

    invoke-direct {v1, v0}, Lcom/a/b/d/agk;-><init>(Lcom/a/b/d/abn;)V

    return-object v1
.end method

.method private static a(Lcom/a/b/d/ku;)Lcom/a/b/d/xc;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 92
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    return-object v0
.end method

.method public static a(Lcom/a/b/d/xc;)Lcom/a/b/d/xc;
    .registers 3

    .prologue
    .line 74
    instance-of v0, p0, Lcom/a/b/d/xw;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/ku;

    if-eqz v0, :cond_9

    .line 81
    :cond_8
    :goto_8
    return-object p0

    :cond_9
    new-instance v1, Lcom/a/b/d/xw;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    invoke-direct {v1, v0}, Lcom/a/b/d/xw;-><init>(Lcom/a/b/d/xc;)V

    move-object p0, v1

    goto :goto_8
.end method

.method public static a(Lcom/a/b/d/xc;Lcom/a/b/b/co;)Lcom/a/b/d/xc;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 267
    instance-of v0, p0, Lcom/a/b/d/xs;

    if-eqz v0, :cond_14

    .line 270
    check-cast p0, Lcom/a/b/d/xs;

    .line 271
    iget-object v0, p0, Lcom/a/b/d/xs;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    .line 273
    new-instance v0, Lcom/a/b/d/xs;

    iget-object v2, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    invoke-direct {v0, v2, v1}, Lcom/a/b/d/xs;-><init>(Lcom/a/b/d/xc;Lcom/a/b/b/co;)V

    .line 275
    :goto_13
    return-object v0

    :cond_14
    new-instance v0, Lcom/a/b/d/xs;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/xs;-><init>(Lcom/a/b/d/xc;Lcom/a/b/b/co;)V

    goto :goto_13
.end method

.method private static a(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)Lcom/a/b/d/xc;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 382
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    new-instance v0, Lcom/a/b/d/xf;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/xf;-><init>(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;I)Lcom/a/b/d/xd;
    .registers 3
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 212
    new-instance v0, Lcom/a/b/d/xu;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/xu;-><init>(Ljava/lang/Object;I)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/xc;Ljava/lang/Iterable;)Z
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 731
    instance-of v1, p1, Lcom/a/b/d/xc;

    if-eqz v1, :cond_42

    .line 732
    check-cast p1, Lcom/a/b/d/xc;

    .line 1756
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1757
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1760
    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    .line 1761
    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 1762
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 1763
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v4

    .line 1764
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v5

    if-lt v4, v5, :cond_36

    .line 1765
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move v1, v2

    .line 1766
    goto :goto_17

    .line 1767
    :cond_36
    if-lez v4, :cond_5e

    .line 1768
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0, v4}, Lcom/a/b/d/xc;->b(Ljava/lang/Object;I)I

    move v0, v2

    :goto_40
    move v1, v0

    .line 1771
    goto :goto_17

    .line 2741
    :cond_42
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2742
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2744
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2745
    invoke-interface {p0, v2}, Lcom/a/b/d/xc;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 2746
    goto :goto_4c

    :cond_5c
    move v1, v0

    .line 735
    :cond_5d
    return v1

    :cond_5e
    move v0, v1

    goto :goto_40
.end method

.method static a(Lcom/a/b/d/xc;Ljava/lang/Object;)Z
    .registers 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 820
    if-ne p1, p0, :cond_6

    move v0, v1

    .line 842
    :goto_5
    return v0

    .line 823
    :cond_6
    instance-of v0, p1, Lcom/a/b/d/xc;

    if-eqz v0, :cond_50

    .line 824
    check-cast p1, Lcom/a/b/d/xc;

    .line 831
    invoke-interface {p0}, Lcom/a/b/d/xc;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/a/b/d/xc;->size()I

    move-result v3

    if-ne v0, v3, :cond_28

    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v0, v3, :cond_2a

    :cond_28
    move v0, v2

    .line 833
    goto :goto_5

    .line 835
    :cond_2a
    invoke-interface {p1}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_32
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 836
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v4}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    if-eq v4, v0, :cond_32

    move v0, v2

    .line 837
    goto :goto_5

    :cond_4e
    move v0, v1

    .line 840
    goto :goto_5

    :cond_50
    move v0, v2

    .line 842
    goto :goto_5
.end method

.method static a(Lcom/a/b/d/xc;Ljava/lang/Object;II)Z
    .registers 5

    .prologue
    .line 910
    const-string v0, "oldCount"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 911
    const-string v0, "newCount"

    invoke-static {p3, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 913
    invoke-interface {p0, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, p2, :cond_15

    .line 914
    invoke-interface {p0, p1, p3}, Lcom/a/b/d/xc;->c(Ljava/lang/Object;I)I

    .line 915
    const/4 v0, 0x1

    .line 917
    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method static a(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
    .registers 5

    .prologue
    .line 850
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 851
    const/4 v0, 0x0

    .line 861
    :goto_7
    return v0

    .line 853
    :cond_8
    instance-of v0, p1, Lcom/a/b/d/xc;

    if-eqz v0, :cond_2e

    .line 3077
    check-cast p1, Lcom/a/b/d/xc;

    .line 855
    invoke-interface {p1}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 856
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    invoke-interface {p0, v2, v0}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;I)I

    goto :goto_16

    .line 859
    :cond_2e
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 861
    :cond_35
    const/4 v0, 0x1

    goto :goto_7
.end method

.method private static b(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)Lcom/a/b/d/xc;
    .registers 3

    .prologue
    .line 457
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    new-instance v0, Lcom/a/b/d/xh;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/xh;-><init>(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)V

    return-object v0
.end method

.method static b(Ljava/lang/Iterable;)Lcom/a/b/d/xc;
    .registers 1

    .prologue
    .line 1077
    check-cast p0, Lcom/a/b/d/xc;

    return-object p0
.end method

.method static b(Lcom/a/b/d/xc;)Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 1010
    new-instance v0, Lcom/a/b/d/xv;

    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/xv;-><init>(Lcom/a/b/d/xc;Ljava/util/Iterator;)V

    return-object v0
.end method

.method private static b(Lcom/a/b/d/xc;Ljava/lang/Iterable;)Z
    .registers 5

    .prologue
    .line 741
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 743
    const/4 v0, 0x0

    .line 744
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 745
    invoke-interface {p0, v2}, Lcom/a/b/d/xc;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 746
    goto :goto_b

    .line 747
    :cond_1b
    return v0
.end method

.method static b(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 869
    instance-of v0, p1, Lcom/a/b/d/xc;

    if-eqz v0, :cond_a

    check-cast p1, Lcom/a/b/d/xc;

    invoke-interface {p1}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object p1

    .line 872
    :cond_a
    invoke-interface {p0}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method static c(Lcom/a/b/d/xc;)I
    .registers 6

    .prologue
    .line 1066
    const-wide/16 v0, 0x0

    .line 1067
    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 1068
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 1069
    goto :goto_b

    .line 1070
    :cond_1f
    invoke-static {v2, v3}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method

.method private static c(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)Lcom/a/b/d/xc;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 518
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    new-instance v0, Lcom/a/b/d/xj;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/xj;-><init>(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)V

    return-object v0
.end method

.method static c(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 880
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 881
    instance-of v0, p1, Lcom/a/b/d/xc;

    if-eqz v0, :cond_d

    check-cast p1, Lcom/a/b/d/xc;

    invoke-interface {p1}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object p1

    .line 884
    :cond_d
    invoke-interface {p0}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method private static d(Lcom/a/b/d/xc;)Lcom/a/b/d/ku;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1095
    sget-object v0, Lcom/a/b/d/xe;->a:Lcom/a/b/d/yd;

    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yd;->b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 1097
    invoke-static {v0}, Lcom/a/b/d/ku;->a(Ljava/util/Collection;)Lcom/a/b/d/ku;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)Lcom/a/b/d/xc;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 600
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    new-instance v0, Lcom/a/b/d/xl;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/xl;-><init>(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)V

    return-object v0
.end method

.method private static e(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)Z
    .registers 5

    .prologue
    .line 646
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 648
    invoke-interface {p1}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 649
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0, v2}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v2

    .line 650
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 651
    const/4 v0, 0x0

    .line 654
    :goto_29
    return v0

    :cond_2a
    const/4 v0, 0x1

    goto :goto_29
.end method

.method private static f(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)Z
    .registers 8

    .prologue
    const/4 v2, 0x1

    .line 678
    .line 1686
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1687
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1689
    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1690
    const/4 v0, 0x0

    move v1, v0

    .line 1691
    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 1692
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 1693
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v4

    .line 1694
    if-nez v4, :cond_2c

    .line 1695
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move v1, v2

    .line 1696
    goto :goto_11

    .line 1697
    :cond_2c
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v5

    if-ge v4, v5, :cond_3d

    .line 1698
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0, v4}, Lcom/a/b/d/xc;->c(Ljava/lang/Object;I)I

    move v0, v2

    :goto_3a
    move v1, v0

    .line 1701
    goto :goto_11

    .line 678
    :cond_3c
    return v1

    :cond_3d
    move v0, v1

    goto :goto_3a
.end method

.method private static g(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)Z
    .registers 8

    .prologue
    const/4 v2, 0x1

    .line 686
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 687
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 689
    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 690
    const/4 v0, 0x0

    move v1, v0

    .line 691
    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 692
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 693
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v4

    .line 694
    if-nez v4, :cond_2c

    .line 695
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move v1, v2

    .line 696
    goto :goto_11

    .line 697
    :cond_2c
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v5

    if-ge v4, v5, :cond_3d

    .line 698
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0, v4}, Lcom/a/b/d/xc;->c(Ljava/lang/Object;I)I

    move v0, v2

    :goto_3a
    move v1, v0

    .line 701
    goto :goto_11

    .line 702
    :cond_3c
    return v1

    :cond_3d
    move v0, v1

    goto :goto_3a
.end method

.method private static h(Lcom/a/b/d/xc;Lcom/a/b/d/xc;)Z
    .registers 8

    .prologue
    const/4 v2, 0x1

    .line 756
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 757
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 759
    const/4 v0, 0x0

    .line 760
    invoke-interface {p0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    .line 761
    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 762
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 763
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v4

    .line 764
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v5

    if-lt v4, v5, :cond_30

    .line 765
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    move v1, v2

    .line 766
    goto :goto_11

    .line 767
    :cond_30
    if-lez v4, :cond_3d

    .line 768
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0, v4}, Lcom/a/b/d/xc;->b(Ljava/lang/Object;I)I

    move v0, v2

    :goto_3a
    move v1, v0

    .line 771
    goto :goto_11

    .line 772
    :cond_3c
    return v1

    :cond_3d
    move v0, v1

    goto :goto_3a
.end method
