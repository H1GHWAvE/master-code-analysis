.class public Lcom/a/b/d/ju;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[Lcom/a/b/d/kb;

.field b:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/d/ju;-><init>(B)V

    .line 185
    return-void
.end method

.method private constructor <init>(B)V
    .registers 3

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/a/b/d/kb;

    iput-object v0, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    .line 190
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/ju;->b:I

    .line 191
    return-void
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 194
    iget-object v0, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    array-length v0, v0

    if-le p1, v0, :cond_16

    .line 195
    iget-object v0, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    iget-object v1, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    array-length v1, v1

    invoke-static {v1, p1}, Lcom/a/b/d/jb;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/a/b/d/yc;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/d/kb;

    iput-object v0, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    .line 198
    :cond_16
    return-void
.end method


# virtual methods
.method public a()Lcom/a/b/d/jt;
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 248
    iget v0, p0, Lcom/a/b/d/ju;->b:I

    packed-switch v0, :pswitch_data_2a

    .line 254
    new-instance v0, Lcom/a/b/d/zf;

    iget v1, p0, Lcom/a/b/d/ju;->b:I

    iget-object v2, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zf;-><init>(I[Lcom/a/b/d/kb;)V

    :goto_f
    return-object v0

    .line 250
    :pswitch_10
    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v0

    goto :goto_f

    .line 252
    :pswitch_15
    iget-object v0, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/a/b/d/kb;->getKey()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/a/b/d/kb;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/jt;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v0

    goto :goto_f

    .line 248
    :pswitch_data_2a
    .packed-switch 0x0
        :pswitch_10
        :pswitch_15
    .end packed-switch
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
    .registers 7

    .prologue
    .line 205
    iget v0, p0, Lcom/a/b/d/ju;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/b/d/ju;->a(I)V

    .line 206
    invoke-static {p1, p2}, Lcom/a/b/d/jt;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/a/b/d/ju;->a:[Lcom/a/b/d/kb;

    iget v2, p0, Lcom/a/b/d/ju;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/a/b/d/ju;->b:I

    aput-object v0, v1, v2

    .line 209
    return-object p0
.end method

.method public a(Ljava/util/Map$Entry;)Lcom/a/b/d/ju;
    .registers 4

    .prologue
    .line 220
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)Lcom/a/b/d/ju;
    .registers 4

    .prologue
    .line 230
    iget v0, p0, Lcom/a/b/d/ju;->b:I

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/a/b/d/ju;->a(I)V

    .line 231
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 232
    invoke-virtual {p0, v0}, Lcom/a/b/d/ju;->a(Ljava/util/Map$Entry;)Lcom/a/b/d/ju;

    goto :goto_12

    .line 234
    :cond_22
    return-object p0
.end method
