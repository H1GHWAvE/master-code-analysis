.class final Lcom/a/b/d/pi;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# static fields
.field private static final c:J


# instance fields
.field final a:Ljava/util/List;

.field final b:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 600
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 601
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/a/b/d/pi;->a:Ljava/util/List;

    .line 602
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/d/pi;->b:Lcom/a/b/b/bj;

    .line 603
    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/a/b/d/pi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 606
    return-void
.end method

.method public final get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 608
    iget-object v0, p0, Lcom/a/b/d/pi;->b:Lcom/a/b/b/bj;

    iget-object v1, p0, Lcom/a/b/d/pi;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 622
    iget-object v0, p0, Lcom/a/b/d/pi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/a/b/d/pi;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
    .registers 4

    .prologue
    .line 614
    new-instance v0, Lcom/a/b/d/pj;

    iget-object v1, p0, Lcom/a/b/d/pi;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/pj;-><init>(Lcom/a/b/d/pi;Ljava/util/ListIterator;)V

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 625
    iget-object v0, p0, Lcom/a/b/d/pi;->b:Lcom/a/b/b/bj;

    iget-object v1, p0, Lcom/a/b/d/pi;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 628
    iget-object v0, p0, Lcom/a/b/d/pi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
