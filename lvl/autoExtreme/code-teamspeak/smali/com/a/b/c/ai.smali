.class public final Lcom/a/b/c/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field final a:J

.field final b:J

.field final c:J

.field final d:J

.field final e:J

.field final f:J


# direct methods
.method public constructor <init>(JJJJJJ)V
    .registers 16

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_4c

    const/4 v0, 0x1

    :goto_a
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 81
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-ltz v0, :cond_4e

    const/4 v0, 0x1

    :goto_14
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 82
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-ltz v0, :cond_50

    const/4 v0, 0x1

    :goto_1e
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 83
    const-wide/16 v0, 0x0

    cmp-long v0, p7, v0

    if-ltz v0, :cond_52

    const/4 v0, 0x1

    :goto_28
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 84
    const-wide/16 v0, 0x0

    cmp-long v0, p9, v0

    if-ltz v0, :cond_54

    const/4 v0, 0x1

    :goto_32
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 85
    const-wide/16 v0, 0x0

    cmp-long v0, p11, v0

    if-ltz v0, :cond_56

    const/4 v0, 0x1

    :goto_3c
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 87
    iput-wide p1, p0, Lcom/a/b/c/ai;->a:J

    .line 88
    iput-wide p3, p0, Lcom/a/b/c/ai;->b:J

    .line 89
    iput-wide p5, p0, Lcom/a/b/c/ai;->c:J

    .line 90
    iput-wide p7, p0, Lcom/a/b/c/ai;->d:J

    .line 91
    iput-wide p9, p0, Lcom/a/b/c/ai;->e:J

    .line 92
    iput-wide p11, p0, Lcom/a/b/c/ai;->f:J

    .line 93
    return-void

    .line 80
    :cond_4c
    const/4 v0, 0x0

    goto :goto_a

    .line 81
    :cond_4e
    const/4 v0, 0x0

    goto :goto_14

    .line 82
    :cond_50
    const/4 v0, 0x0

    goto :goto_1e

    .line 83
    :cond_52
    const/4 v0, 0x0

    goto :goto_28

    .line 84
    :cond_54
    const/4 v0, 0x0

    goto :goto_32

    .line 85
    :cond_56
    const/4 v0, 0x0

    goto :goto_3c
.end method

.method private a()J
    .registers 5

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/a/b/c/ai;->a:J

    iget-wide v2, p0, Lcom/a/b/c/ai;->b:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private a(Lcom/a/b/c/ai;)Lcom/a/b/c/ai;
    .registers 22

    .prologue
    .line 221
    new-instance v3, Lcom/a/b/c/ai;

    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/a/b/c/ai;->a:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/a/b/c/ai;->a:J

    sub-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/a/b/c/ai;->b:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/a/b/c/ai;->b:J

    sub-long/2addr v8, v10

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/a/b/c/ai;->c:J

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/a/b/c/ai;->c:J

    sub-long/2addr v10, v12

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/a/b/c/ai;->d:J

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/a/b/c/ai;->d:J

    sub-long/2addr v12, v14

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/a/b/c/ai;->e:J

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/a/b/c/ai;->e:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v12

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/a/b/c/ai;->f:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/a/b/c/ai;->f:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v14

    invoke-direct/range {v3 .. v15}, Lcom/a/b/c/ai;-><init>(JJJJJJ)V

    return-object v3
.end method

.method private b()J
    .registers 3

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/a/b/c/ai;->a:J

    return-wide v0
.end method

.method private b(Lcom/a/b/c/ai;)Lcom/a/b/c/ai;
    .registers 20

    .prologue
    .line 237
    new-instance v3, Lcom/a/b/c/ai;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/a/b/c/ai;->a:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/a/b/c/ai;->a:J

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/a/b/c/ai;->b:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/a/b/c/ai;->b:J

    add-long/2addr v6, v8

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/a/b/c/ai;->c:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/a/b/c/ai;->c:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/a/b/c/ai;->d:J

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/a/b/c/ai;->d:J

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/a/b/c/ai;->e:J

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/a/b/c/ai;->e:J

    add-long/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/a/b/c/ai;->f:J

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/a/b/c/ai;->f:J

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    invoke-direct/range {v3 .. v15}, Lcom/a/b/c/ai;-><init>(JJJJJJ)V

    return-object v3
.end method

.method private c()D
    .registers 5

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/a/b/c/ai;->a()J

    move-result-wide v0

    .line 117
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_d

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_c
    return-wide v0

    :cond_d
    iget-wide v2, p0, Lcom/a/b/c/ai;->a:J

    long-to-double v2, v2

    long-to-double v0, v0

    div-double v0, v2, v0

    goto :goto_c
.end method

.method private d()J
    .registers 3

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/a/b/c/ai;->b:J

    return-wide v0
.end method

.method private e()D
    .registers 5

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/a/b/c/ai;->a()J

    move-result-wide v0

    .line 141
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_d

    const-wide/16 v0, 0x0

    :goto_c
    return-wide v0

    :cond_d
    iget-wide v2, p0, Lcom/a/b/c/ai;->b:J

    long-to-double v2, v2

    long-to-double v0, v0

    div-double v0, v2, v0

    goto :goto_c
.end method

.method private f()J
    .registers 5

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/a/b/c/ai;->c:J

    iget-wide v2, p0, Lcom/a/b/c/ai;->d:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private g()J
    .registers 3

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/a/b/c/ai;->c:J

    return-wide v0
.end method

.method private h()J
    .registers 3

    .prologue
    .line 172
    iget-wide v0, p0, Lcom/a/b/c/ai;->d:J

    return-wide v0
.end method

.method private i()D
    .registers 5

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/a/b/c/ai;->c:J

    iget-wide v2, p0, Lcom/a/b/c/ai;->d:J

    add-long/2addr v0, v2

    .line 182
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_e

    const-wide/16 v0, 0x0

    :goto_d
    return-wide v0

    :cond_e
    iget-wide v2, p0, Lcom/a/b/c/ai;->d:J

    long-to-double v2, v2

    long-to-double v0, v0

    div-double v0, v2, v0

    goto :goto_d
.end method

.method private j()J
    .registers 3

    .prologue
    .line 193
    iget-wide v0, p0, Lcom/a/b/c/ai;->e:J

    return-wide v0
.end method

.method private k()D
    .registers 5

    .prologue
    .line 201
    iget-wide v0, p0, Lcom/a/b/c/ai;->c:J

    iget-wide v2, p0, Lcom/a/b/c/ai;->d:J

    add-long/2addr v0, v2

    .line 202
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_e

    const-wide/16 v0, 0x0

    :goto_d
    return-wide v0

    :cond_e
    iget-wide v2, p0, Lcom/a/b/c/ai;->e:J

    long-to-double v2, v2

    long-to-double v0, v0

    div-double v0, v2, v0

    goto :goto_d
.end method

.method private l()J
    .registers 3

    .prologue
    .line 212
    iget-wide v0, p0, Lcom/a/b/c/ai;->f:J

    return-wide v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 254
    instance-of v1, p1, Lcom/a/b/c/ai;

    if-eqz v1, :cond_38

    .line 255
    check-cast p1, Lcom/a/b/c/ai;

    .line 256
    iget-wide v2, p0, Lcom/a/b/c/ai;->a:J

    iget-wide v4, p1, Lcom/a/b/c/ai;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_38

    iget-wide v2, p0, Lcom/a/b/c/ai;->b:J

    iget-wide v4, p1, Lcom/a/b/c/ai;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_38

    iget-wide v2, p0, Lcom/a/b/c/ai;->c:J

    iget-wide v4, p1, Lcom/a/b/c/ai;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_38

    iget-wide v2, p0, Lcom/a/b/c/ai;->d:J

    iget-wide v4, p1, Lcom/a/b/c/ai;->d:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_38

    iget-wide v2, p0, Lcom/a/b/c/ai;->e:J

    iget-wide v4, p1, Lcom/a/b/c/ai;->e:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_38

    iget-wide v2, p0, Lcom/a/b/c/ai;->f:J

    iget-wide v4, p1, Lcom/a/b/c/ai;->f:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_38

    const/4 v0, 0x1

    .line 263
    :cond_38
    return v0
.end method

.method public final hashCode()I
    .registers 5

    .prologue
    .line 248
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/a/b/c/ai;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/a/b/c/ai;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/a/b/c/ai;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/a/b/c/ai;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/a/b/c/ai;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/a/b/c/ai;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 248
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 268
    invoke-static {p0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    const-string v1, "hitCount"

    iget-wide v2, p0, Lcom/a/b/c/ai;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/b/cc;->a(Ljava/lang/String;J)Lcom/a/b/b/cc;

    move-result-object v0

    const-string v1, "missCount"

    iget-wide v2, p0, Lcom/a/b/c/ai;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/b/cc;->a(Ljava/lang/String;J)Lcom/a/b/b/cc;

    move-result-object v0

    const-string v1, "loadSuccessCount"

    iget-wide v2, p0, Lcom/a/b/c/ai;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/b/cc;->a(Ljava/lang/String;J)Lcom/a/b/b/cc;

    move-result-object v0

    const-string v1, "loadExceptionCount"

    iget-wide v2, p0, Lcom/a/b/c/ai;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/b/cc;->a(Ljava/lang/String;J)Lcom/a/b/b/cc;

    move-result-object v0

    const-string v1, "totalLoadTime"

    iget-wide v2, p0, Lcom/a/b/c/ai;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/b/cc;->a(Ljava/lang/String;J)Lcom/a/b/b/cc;

    move-result-object v0

    const-string v1, "evictionCount"

    iget-wide v2, p0, Lcom/a/b/c/ai;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/b/cc;->a(Ljava/lang/String;J)Lcom/a/b/b/cc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/b/cc;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
