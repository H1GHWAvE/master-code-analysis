.class Lcom/a/b/c/bq;
.super Lcom/a/b/c/aj;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:J = 0x1L


# instance fields
.field final b:Lcom/a/b/c/bw;

.field final c:Lcom/a/b/c/bw;

.field final d:Lcom/a/b/b/au;

.field final e:Lcom/a/b/b/au;

.field final f:J

.field final g:J

.field final h:J

.field final i:Lcom/a/b/c/do;

.field final j:I

.field final k:Lcom/a/b/c/dg;

.field final l:Lcom/a/b/b/ej;

.field final m:Lcom/a/b/c/ab;

.field transient n:Lcom/a/b/c/e;


# direct methods
.method constructor <init>(Lcom/a/b/c/ao;)V
    .registers 21

    .prologue
    .line 4579
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/a/b/c/ao;->m:Lcom/a/b/c/bw;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/a/b/c/ao;->n:Lcom/a/b/c/bw;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/a/b/c/ao;->k:Lcom/a/b/b/au;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/a/b/c/ao;->l:Lcom/a/b/b/au;

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/a/b/c/ao;->r:J

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/a/b/c/ao;->q:J

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/a/b/c/ao;->o:J

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/a/b/c/ao;->p:Lcom/a/b/c/do;

    move-object/from16 v0, p1

    iget v15, v0, Lcom/a/b/c/ao;->j:I

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/a/b/c/ao;->u:Lcom/a/b/c/dg;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/a/b/c/ao;->v:Lcom/a/b/b/ej;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/a/b/c/ao;->y:Lcom/a/b/c/ab;

    move-object/from16 v18, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v18}, Lcom/a/b/c/bq;-><init>(Lcom/a/b/c/bw;Lcom/a/b/c/bw;Lcom/a/b/b/au;Lcom/a/b/b/au;JJJLcom/a/b/c/do;ILcom/a/b/c/dg;Lcom/a/b/b/ej;Lcom/a/b/c/ab;)V

    .line 4592
    return-void
.end method

.method private constructor <init>(Lcom/a/b/c/bw;Lcom/a/b/c/bw;Lcom/a/b/b/au;Lcom/a/b/b/au;JJJLcom/a/b/c/do;ILcom/a/b/c/dg;Lcom/a/b/b/ej;Lcom/a/b/c/ab;)V
    .registers 19

    .prologue
    .line 4600
    invoke-direct {p0}, Lcom/a/b/c/aj;-><init>()V

    .line 4601
    iput-object p1, p0, Lcom/a/b/c/bq;->b:Lcom/a/b/c/bw;

    .line 4602
    iput-object p2, p0, Lcom/a/b/c/bq;->c:Lcom/a/b/c/bw;

    .line 4603
    iput-object p3, p0, Lcom/a/b/c/bq;->d:Lcom/a/b/b/au;

    .line 4604
    iput-object p4, p0, Lcom/a/b/c/bq;->e:Lcom/a/b/b/au;

    .line 4605
    iput-wide p5, p0, Lcom/a/b/c/bq;->f:J

    .line 4606
    iput-wide p7, p0, Lcom/a/b/c/bq;->g:J

    .line 4607
    iput-wide p9, p0, Lcom/a/b/c/bq;->h:J

    .line 4608
    iput-object p11, p0, Lcom/a/b/c/bq;->i:Lcom/a/b/c/do;

    .line 4609
    iput p12, p0, Lcom/a/b/c/bq;->j:I

    .line 4610
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/a/b/c/bq;->k:Lcom/a/b/c/dg;

    .line 4611
    invoke-static {}, Lcom/a/b/b/ej;->b()Lcom/a/b/b/ej;

    move-result-object v1

    move-object/from16 v0, p14

    if-eq v0, v1, :cond_27

    sget-object v1, Lcom/a/b/c/f;->d:Lcom/a/b/b/ej;

    move-object/from16 v0, p14

    if-ne v0, v1, :cond_29

    :cond_27
    const/16 p14, 0x0

    :cond_29
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/a/b/c/bq;->l:Lcom/a/b/b/ej;

    .line 4613
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/a/b/c/bq;->m:Lcom/a/b/c/ab;

    .line 4614
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 8

    .prologue
    .line 4648
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 4649
    invoke-virtual {p0}, Lcom/a/b/c/bq;->h()Lcom/a/b/c/f;

    move-result-object v1

    .line 6805
    invoke-virtual {v1}, Lcom/a/b/c/f;->d()V

    .line 6811
    iget-wide v2, v1, Lcom/a/b/c/f;->p:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-nez v0, :cond_20

    const/4 v0, 0x1

    :goto_13
    const-string v2, "refreshAfterWrite requires a LoadingCache"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 6807
    new-instance v0, Lcom/a/b/c/bo;

    invoke-direct {v0, v1}, Lcom/a/b/c/bo;-><init>(Lcom/a/b/c/f;)V

    .line 4650
    iput-object v0, p0, Lcom/a/b/c/bq;->n:Lcom/a/b/c/e;

    .line 4651
    return-void

    .line 6811
    :cond_20
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private i()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 4654
    iget-object v0, p0, Lcom/a/b/c/bq;->n:Lcom/a/b/c/e;

    return-object v0
.end method


# virtual methods
.method protected final f()Lcom/a/b/c/e;
    .registers 2

    .prologue
    .line 4659
    iget-object v0, p0, Lcom/a/b/c/bq;->n:Lcom/a/b/c/e;

    return-object v0
.end method

.method final h()Lcom/a/b/c/f;
    .registers 13

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v10, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4617
    invoke-static {}, Lcom/a/b/c/f;->a()Lcom/a/b/c/f;

    move-result-object v0

    iget-object v3, p0, Lcom/a/b/c/bq;->b:Lcom/a/b/c/bw;

    invoke-virtual {v0, v3}, Lcom/a/b/c/f;->a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    move-result-object v0

    iget-object v3, p0, Lcom/a/b/c/bq;->c:Lcom/a/b/c/bw;

    invoke-virtual {v0, v3}, Lcom/a/b/c/f;->b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    move-result-object v3

    iget-object v4, p0, Lcom/a/b/c/bq;->d:Lcom/a/b/b/au;

    .line 5293
    iget-object v0, v3, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    if-nez v0, :cond_d1

    move v0, v1

    :goto_1d
    const-string v5, "key equivalence was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, v3, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 5294
    invoke-static {v4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, v3, Lcom/a/b/c/f;->q:Lcom/a/b/b/au;

    .line 4617
    iget-object v4, p0, Lcom/a/b/c/bq;->e:Lcom/a/b/b/au;

    .line 5311
    iget-object v0, v3, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    if-nez v0, :cond_d4

    move v0, v1

    :goto_37
    const-string v5, "value equivalence was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, v3, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 5313
    invoke-static {v4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, v3, Lcom/a/b/c/f;->r:Lcom/a/b/b/au;

    .line 4617
    iget v0, p0, Lcom/a/b/c/bq;->j:I

    invoke-virtual {v3, v0}, Lcom/a/b/c/f;->a(I)Lcom/a/b/c/f;

    move-result-object v3

    iget-object v4, p0, Lcom/a/b/c/bq;->k:Lcom/a/b/c/dg;

    .line 5737
    iget-object v0, v3, Lcom/a/b/c/f;->s:Lcom/a/b/c/dg;

    if-nez v0, :cond_d7

    move v0, v1

    :goto_57
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 5742
    invoke-static {v4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/dg;

    iput-object v0, v3, Lcom/a/b/c/f;->s:Lcom/a/b/c/dg;

    .line 4624
    iput-boolean v2, v3, Lcom/a/b/c/f;->f:Z

    .line 4625
    iget-wide v4, p0, Lcom/a/b/c/bq;->f:J

    cmp-long v0, v4, v8

    if-lez v0, :cond_71

    .line 4626
    iget-wide v4, p0, Lcom/a/b/c/bq;->f:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v0}, Lcom/a/b/c/f;->a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;

    .line 4628
    :cond_71
    iget-wide v4, p0, Lcom/a/b/c/bq;->g:J

    cmp-long v0, v4, v8

    if-lez v0, :cond_7e

    .line 4629
    iget-wide v4, p0, Lcom/a/b/c/bq;->g:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v0}, Lcom/a/b/c/f;->b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;

    .line 4631
    :cond_7e
    iget-object v0, p0, Lcom/a/b/c/bq;->i:Lcom/a/b/c/do;

    sget-object v4, Lcom/a/b/c/k;->a:Lcom/a/b/c/k;

    if-eq v0, v4, :cond_de

    .line 4632
    iget-object v4, p0, Lcom/a/b/c/bq;->i:Lcom/a/b/c/do;

    .line 6477
    iget-object v0, v3, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    if-nez v0, :cond_da

    move v0, v1

    :goto_8b
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 6478
    iget-boolean v0, v3, Lcom/a/b/c/f;->f:Z

    if-eqz v0, :cond_a8

    .line 6479
    iget-wide v6, v3, Lcom/a/b/c/f;->i:J

    cmp-long v0, v6, v10

    if-nez v0, :cond_dc

    move v0, v1

    :goto_99
    const-string v5, "weigher can not be combined with maximum size"

    new-array v6, v1, [Ljava/lang/Object;

    iget-wide v8, v3, Lcom/a/b/c/f;->i:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 6486
    :cond_a8
    invoke-static {v4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/do;

    iput-object v0, v3, Lcom/a/b/c/f;->k:Lcom/a/b/c/do;

    .line 4633
    iget-wide v4, p0, Lcom/a/b/c/bq;->h:J

    cmp-long v0, v4, v10

    if-eqz v0, :cond_bb

    .line 4634
    iget-wide v4, p0, Lcom/a/b/c/bq;->h:J

    invoke-virtual {v3, v4, v5}, Lcom/a/b/c/f;->b(J)Lcom/a/b/c/f;

    .line 4641
    :cond_bb
    :goto_bb
    iget-object v0, p0, Lcom/a/b/c/bq;->l:Lcom/a/b/b/ej;

    if-eqz v0, :cond_d0

    .line 4642
    iget-object v0, p0, Lcom/a/b/c/bq;->l:Lcom/a/b/b/ej;

    .line 6701
    iget-object v4, v3, Lcom/a/b/c/f;->t:Lcom/a/b/b/ej;

    if-nez v4, :cond_ea

    :goto_c5
    invoke-static {v1}, Lcom/a/b/b/cn;->b(Z)V

    .line 6702
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/ej;

    iput-object v0, v3, Lcom/a/b/c/f;->t:Lcom/a/b/b/ej;

    .line 4644
    :cond_d0
    return-object v3

    :cond_d1
    move v0, v2

    .line 5293
    goto/16 :goto_1d

    :cond_d4
    move v0, v2

    .line 5311
    goto/16 :goto_37

    :cond_d7
    move v0, v2

    .line 5737
    goto/16 :goto_57

    :cond_da
    move v0, v2

    .line 6477
    goto :goto_8b

    :cond_dc
    move v0, v2

    .line 6479
    goto :goto_99

    .line 4637
    :cond_de
    iget-wide v4, p0, Lcom/a/b/c/bq;->h:J

    cmp-long v0, v4, v10

    if-eqz v0, :cond_bb

    .line 4638
    iget-wide v4, p0, Lcom/a/b/c/bq;->h:J

    invoke-virtual {v3, v4, v5}, Lcom/a/b/c/f;->a(J)Lcom/a/b/c/f;

    goto :goto_bb

    :cond_ea
    move v1, v2

    .line 6701
    goto :goto_c5
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 4559
    .line 7659
    iget-object v0, p0, Lcom/a/b/c/bq;->n:Lcom/a/b/c/e;

    .line 4559
    return-object v0
.end method
