.class final Lcom/a/b/c/bn;
.super Lcom/a/b/c/bo;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/an;


# static fields
.field private static final b:J = 0x1L


# direct methods
.method constructor <init>(Lcom/a/b/c/f;Lcom/a/b/c/ab;)V
    .registers 5

    .prologue
    .line 4817
    new-instance v1, Lcom/a/b/c/ao;

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/ab;

    invoke-direct {v1, p1, v0}, Lcom/a/b/c/ao;-><init>(Lcom/a/b/c/f;Lcom/a/b/c/ab;)V

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/a/b/c/bo;-><init>(Lcom/a/b/c/ao;B)V

    .line 4818
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 4830
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/a/b/c/bn;->f(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    .line 4831
    :catch_5
    move-exception v0

    .line 4832
    new-instance v1, Lcom/a/b/n/a/gq;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/b/n/a/gq;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/lang/Iterable;)Lcom/a/b/d/jt;
    .registers 3

    .prologue
    .line 4838
    iget-object v0, p0, Lcom/a/b/c/bn;->a:Lcom/a/b/c/ao;

    invoke-virtual {v0, p1}, Lcom/a/b/c/ao;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 4843
    iget-object v0, p0, Lcom/a/b/c/bn;->a:Lcom/a/b/c/ao;

    .line 5089
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v1

    .line 5090
    invoke-virtual {v0, v1}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v2

    iget-object v0, v0, Lcom/a/b/c/ao;->y:Lcom/a/b/c/ab;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v1, v0, v3}, Lcom/a/b/c/bt;->a(Ljava/lang/Object;ILcom/a/b/c/ab;Z)Ljava/lang/Object;

    .line 4844
    return-void
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 4848
    invoke-virtual {p0, p1}, Lcom/a/b/c/bn;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final f()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 4857
    new-instance v0, Lcom/a/b/c/bk;

    iget-object v1, p0, Lcom/a/b/c/bn;->a:Lcom/a/b/c/ao;

    invoke-direct {v0, v1}, Lcom/a/b/c/bk;-><init>(Lcom/a/b/c/ao;)V

    return-object v0
.end method

.method public final f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 4824
    iget-object v0, p0, Lcom/a/b/c/bn;->a:Lcom/a/b/c/ao;

    .line 4941
    iget-object v1, v0, Lcom/a/b/c/ao;->y:Lcom/a/b/c/ab;

    invoke-virtual {v0, p1, v1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;Lcom/a/b/c/ab;)Ljava/lang/Object;

    move-result-object v0

    .line 4824
    return-object v0
.end method
