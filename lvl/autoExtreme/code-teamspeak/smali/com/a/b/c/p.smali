.class abstract Lcom/a/b/c/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/y;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/a/b/c/l;JLjava/util/concurrent/TimeUnit;)V
.end method

.method public final a(Lcom/a/b/c/l;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 417
    if-eqz p3, :cond_4a

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4a

    move v0, v1

    :goto_c
    const-string v3, "value of key %s omitted"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p2, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 419
    :try_start_15
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 421
    sparse-switch v0, :sswitch_data_6a

    .line 435
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "key %s invalid format.  was %s, must end with one of [dDhHmMsS]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_37
    .catch Ljava/lang/NumberFormatException; {:try_start_15 .. :try_end_37} :catch_37

    .line 443
    :catch_37
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "key %s value set to %s, must be integer"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p2, v4, v2

    aput-object p3, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4a
    move v0, v2

    .line 417
    goto :goto_c

    .line 423
    :sswitch_4c
    :try_start_4c
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 440
    :goto_4e
    const/4 v3, 0x0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p3, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 441
    invoke-virtual {p0, p1, v4, v5, v0}, Lcom/a/b/c/p;->a(Lcom/a/b/c/l;JLjava/util/concurrent/TimeUnit;)V

    .line 445
    return-void

    .line 426
    :sswitch_61
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    goto :goto_4e

    .line 429
    :sswitch_64
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    goto :goto_4e

    .line 432
    :sswitch_67
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;
    :try_end_69
    .catch Ljava/lang/NumberFormatException; {:try_start_4c .. :try_end_69} :catch_37

    goto :goto_4e

    .line 421
    :sswitch_data_6a
    .sparse-switch
        0x64 -> :sswitch_4c
        0x68 -> :sswitch_61
        0x6d -> :sswitch_64
        0x73 -> :sswitch_67
    .end sparse-switch
.end method
