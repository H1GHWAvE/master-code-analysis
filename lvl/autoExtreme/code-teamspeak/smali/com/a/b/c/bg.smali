.class final Lcom/a/b/c/bg;
.super Lcom/a/b/c/ar;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/a/b/c/ao;


# direct methods
.method constructor <init>(Lcom/a/b/c/ao;Ljava/util/concurrent/ConcurrentMap;)V
    .registers 3

    .prologue
    .line 4514
    iput-object p1, p0, Lcom/a/b/c/bg;->c:Lcom/a/b/c/ao;

    .line 4515
    invoke-direct {p0, p1, p2}, Lcom/a/b/c/ar;-><init>(Lcom/a/b/c/ao;Ljava/util/concurrent/ConcurrentMap;)V

    .line 4516
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 4525
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_6

    .line 4535
    :cond_5
    :goto_5
    return v0

    .line 4528
    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    .line 4529
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 4530
    if-eqz v1, :cond_5

    .line 4533
    iget-object v2, p0, Lcom/a/b/c/bg;->c:Lcom/a/b/c/ao;

    invoke-virtual {v2, v1}, Lcom/a/b/c/ao;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 4535
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/a/b/c/bg;->c:Lcom/a/b/c/ao;

    iget-object v2, v2, Lcom/a/b/c/ao;->l:Lcom/a/b/b/au;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 4520
    new-instance v0, Lcom/a/b/c/bf;

    iget-object v1, p0, Lcom/a/b/c/bg;->c:Lcom/a/b/c/ao;

    invoke-direct {v0, v1}, Lcom/a/b/c/bf;-><init>(Lcom/a/b/c/ao;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 4540
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_6

    .line 4545
    :cond_5
    :goto_5
    return v0

    .line 4543
    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    .line 4544
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 4545
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/a/b/c/bg;->c:Lcom/a/b/c/ao;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/a/b/c/ao;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method
