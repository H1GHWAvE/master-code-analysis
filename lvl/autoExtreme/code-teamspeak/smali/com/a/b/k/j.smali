.class final Lcom/a/b/k/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field b:I


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/k/j;->b:I

    .line 638
    iput-object p1, p0, Lcom/a/b/k/j;->a:Ljava/lang/String;

    .line 639
    return-void
.end method

.method private c(Lcom/a/b/b/m;)C
    .registers 4

    .prologue
    .line 656
    invoke-virtual {p0}, Lcom/a/b/k/j;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 657
    invoke-virtual {p0}, Lcom/a/b/k/j;->a()C

    move-result v0

    .line 658
    invoke-virtual {p1, v0}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    invoke-static {v1}, Lcom/a/b/b/cn;->b(Z)V

    .line 659
    iget v1, p0, Lcom/a/b/k/j;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/b/k/j;->b:I

    .line 660
    return v0
.end method


# virtual methods
.method final a()C
    .registers 3

    .prologue
    .line 671
    invoke-virtual {p0}, Lcom/a/b/k/j;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 672
    iget-object v0, p0, Lcom/a/b/k/j;->a:Ljava/lang/String;

    iget v1, p0, Lcom/a/b/k/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    return v0
.end method

.method final a(C)C
    .registers 3

    .prologue
    .line 664
    invoke-virtual {p0}, Lcom/a/b/k/j;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 665
    invoke-virtual {p0}, Lcom/a/b/k/j;->a()C

    move-result v0

    if-ne v0, p1, :cond_18

    const/4 v0, 0x1

    :goto_e
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 666
    iget v0, p0, Lcom/a/b/k/j;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/k/j;->b:I

    .line 667
    return p1

    .line 665
    :cond_18
    const/4 v0, 0x0

    goto :goto_e
.end method

.method final a(Lcom/a/b/b/m;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 642
    invoke-virtual {p0}, Lcom/a/b/k/j;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 643
    iget v0, p0, Lcom/a/b/k/j;->b:I

    .line 644
    invoke-virtual {p1}, Lcom/a/b/b/m;->a()Lcom/a/b/b/m;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/k/j;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;I)I

    move-result v1

    iput v1, p0, Lcom/a/b/k/j;->b:I

    .line 645
    invoke-virtual {p0}, Lcom/a/b/k/j;->b()Z

    move-result v1

    if-eqz v1, :cond_24

    iget-object v1, p0, Lcom/a/b/k/j;->a:Ljava/lang/String;

    iget v2, p0, Lcom/a/b/k/j;->b:I

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_23
    return-object v0

    :cond_24
    iget-object v1, p0, Lcom/a/b/k/j;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_23
.end method

.method final b(Lcom/a/b/b/m;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 649
    iget v0, p0, Lcom/a/b/k/j;->b:I

    .line 650
    invoke-virtual {p0, p1}, Lcom/a/b/k/j;->a(Lcom/a/b/b/m;)Ljava/lang/String;

    move-result-object v1

    .line 651
    iget v2, p0, Lcom/a/b/k/j;->b:I

    if-eq v2, v0, :cond_f

    const/4 v0, 0x1

    :goto_b
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 652
    return-object v1

    .line 651
    :cond_f
    const/4 v0, 0x0

    goto :goto_b
.end method

.method final b()Z
    .registers 3

    .prologue
    .line 676
    iget v0, p0, Lcom/a/b/k/j;->b:I

    if-ltz v0, :cond_10

    iget v0, p0, Lcom/a/b/k/j;->b:I

    iget-object v1, p0, Lcom/a/b/k/j;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method
