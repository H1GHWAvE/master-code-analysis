.class public final Lcom/a/b/k/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:I = 0x4

.field private static final b:I = 0x8

.field private static final c:Ljava/net/Inet4Address;

.field private static final d:Ljava/net/Inet4Address;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 117
    const-string v0, "127.0.0.1"

    invoke-static {v0}, Lcom/a/b/k/d;->a(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;

    sput-object v0, Lcom/a/b/k/d;->c:Ljava/net/Inet4Address;

    .line 118
    const-string v0, "0.0.0.0"

    invoke-static {v0}, Lcom/a/b/k/d;->a(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;

    sput-object v0, Lcom/a/b/k/d;->d:Ljava/net/Inet4Address;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/net/InetAddress;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 463
    instance-of v0, p0, Ljava/net/Inet6Address;

    if-eqz v0, :cond_30

    .line 464
    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 466
    :goto_2f
    return-object v0

    :cond_30
    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2f
.end method

.method static synthetic a()Ljava/net/Inet4Address;
    .registers 1

    .prologue
    .line 114
    sget-object v0, Lcom/a/b/k/d;->d:Ljava/net/Inet4Address;

    return-object v0
.end method

.method private static a(I)Ljava/net/Inet4Address;
    .registers 2

    .prologue
    .line 949
    invoke-static {p0}, Lcom/a/b/l/q;->b(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    return-object v0
.end method

.method private static a([B)Ljava/net/Inet4Address;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    array-length v0, p0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1c

    move v0, v1

    :goto_7
    const-string v3, "Byte array has invalid length for an IPv4 address: %s != 4."

    new-array v1, v1, [Ljava/lang/Object;

    array-length v4, p0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 135
    invoke-static {p0}, Lcom/a/b/k/d;->b([B)Ljava/net/InetAddress;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;

    return-object v0

    :cond_1c
    move v0, v2

    .line 130
    goto :goto_7
.end method

.method public static a(Ljava/lang/String;)Ljava/net/InetAddress;
    .registers 5

    .prologue
    .line 149
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/lang/String;)[B

    move-result-object v0

    .line 152
    if-nez v0, :cond_18

    .line 153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'%s\' is not an IP string literal."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_18
    invoke-static {v0}, Lcom/a/b/k/d;->b([B)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method private static a([I)V
    .registers 7

    .prologue
    const/4 v5, -0x1

    .line 381
    .line 384
    const/4 v4, 0x0

    move v2, v5

    move v1, v5

    move v3, v5

    :goto_5
    const/16 v0, 0x9

    if-ge v4, v0, :cond_22

    .line 385
    const/16 v0, 0x8

    if-ge v4, v0, :cond_17

    aget v0, p0, v4

    if-nez v0, :cond_17

    .line 386
    if-gez v2, :cond_14

    move v2, v4

    .line 384
    :cond_14
    :goto_14
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 389
    :cond_17
    if-ltz v2, :cond_14

    .line 390
    sub-int v0, v4, v2

    .line 391
    if-le v0, v1, :cond_2b

    move v1, v2

    :goto_1e
    move v2, v5

    move v3, v1

    move v1, v0

    .line 395
    goto :goto_14

    .line 398
    :cond_22
    const/4 v0, 0x2

    if-lt v1, v0, :cond_2a

    .line 399
    add-int v0, v3, v1

    invoke-static {p0, v3, v0, v5}, Ljava/util/Arrays;->fill([IIII)V

    .line 401
    :cond_2a
    return-void

    :cond_2b
    move v0, v1

    move v1, v3

    goto :goto_1e
.end method

.method private static a(Ljava/net/Inet6Address;)Z
    .registers 6

    .prologue
    const/16 v4, 0xf

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 549
    invoke-virtual {p0}, Ljava/net/Inet6Address;->isIPv4CompatibleAddress()Z

    move-result v2

    if-nez v2, :cond_b

    .line 559
    :cond_a
    :goto_a
    return v0

    .line 553
    :cond_b
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v2

    .line 554
    const/16 v3, 0xc

    aget-byte v3, v2, v3

    if-nez v3, :cond_29

    const/16 v3, 0xd

    aget-byte v3, v2, v3

    if-nez v3, :cond_29

    const/16 v3, 0xe

    aget-byte v3, v2, v3

    if-nez v3, :cond_29

    aget-byte v3, v2, v4

    if-eqz v3, :cond_a

    aget-byte v2, v2, v4

    if-eq v2, v1, :cond_a

    :cond_29
    move v0, v1

    .line 559
    goto :goto_a
.end method

.method private static b(Ljava/net/InetAddress;)Ljava/lang/String;
    .registers 10

    .prologue
    const/16 v8, 0x8

    const/4 v5, -0x1

    const/4 v6, 0x0

    .line 355
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    instance-of v0, p0, Ljava/net/Inet4Address;

    if-eqz v0, :cond_10

    .line 358
    invoke-virtual {p0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 368
    :goto_f
    return-object v0

    .line 360
    :cond_10
    instance-of v0, p0, Ljava/net/Inet6Address;

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 361
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    .line 362
    new-array v7, v8, [I

    move v0, v6

    .line 363
    :goto_1c
    if-ge v0, v8, :cond_31

    .line 364
    mul-int/lit8 v2, v0, 0x2

    aget-byte v2, v1, v2

    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    aget-byte v3, v1, v3

    invoke-static {v6, v6, v2, v3}, Lcom/a/b/l/q;->a(BBBB)I

    move-result v2

    aput v2, v7, v0

    .line 363
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    :cond_31
    move v4, v6

    move v2, v5

    move v1, v5

    move v3, v5

    .line 1384
    :goto_35
    const/16 v0, 0x9

    if-ge v4, v0, :cond_50

    .line 1385
    if-ge v4, v8, :cond_45

    aget v0, v7, v4

    if-nez v0, :cond_45

    .line 1386
    if-gez v2, :cond_42

    move v2, v4

    .line 1384
    :cond_42
    :goto_42
    add-int/lit8 v4, v4, 0x1

    goto :goto_35

    .line 1389
    :cond_45
    if-ltz v2, :cond_42

    .line 1390
    sub-int v0, v4, v2

    .line 1391
    if-le v0, v1, :cond_8f

    move v1, v2

    :goto_4c
    move v2, v5

    move v3, v1

    move v1, v0

    .line 1395
    goto :goto_42

    .line 1398
    :cond_50
    const/4 v0, 0x2

    if-lt v1, v0, :cond_58

    .line 1399
    add-int v0, v3, v1

    invoke-static {v7, v3, v0, v5}, Ljava/util/Arrays;->fill([IIII)V

    .line 1418
    :cond_58
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v0, 0x27

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    move v1, v6

    move v2, v6

    .line 1420
    :goto_61
    if-ge v1, v8, :cond_8a

    .line 1421
    aget v0, v7, v1

    if-ltz v0, :cond_7e

    const/4 v0, 0x1

    .line 1422
    :goto_68
    if-eqz v0, :cond_80

    .line 1423
    if-eqz v2, :cond_71

    .line 1424
    const/16 v2, 0x3a

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1426
    :cond_71
    aget v2, v7, v1

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1420
    :cond_7a
    :goto_7a
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_61

    :cond_7e
    move v0, v6

    .line 1421
    goto :goto_68

    .line 1428
    :cond_80
    if-eqz v1, :cond_84

    if-eqz v2, :cond_7a

    .line 1429
    :cond_84
    const-string v2, "::"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7a

    .line 1434
    :cond_8a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_f

    :cond_8f
    move v0, v1

    move v1, v3

    goto :goto_4c
.end method

.method private static b([I)Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 418
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x27

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    move v2, v1

    move v3, v1

    .line 420
    :goto_a
    const/16 v0, 0x8

    if-ge v2, v0, :cond_35

    .line 421
    aget v0, p0, v2

    if-ltz v0, :cond_29

    const/4 v0, 0x1

    .line 422
    :goto_13
    if-eqz v0, :cond_2b

    .line 423
    if-eqz v3, :cond_1c

    .line 424
    const/16 v3, 0x3a

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 426
    :cond_1c
    aget v3, p0, v2

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    :cond_25
    :goto_25
    add-int/lit8 v2, v2, 0x1

    move v3, v0

    goto :goto_a

    :cond_29
    move v0, v1

    .line 421
    goto :goto_13

    .line 428
    :cond_2b
    if-eqz v2, :cond_2f

    if-eqz v3, :cond_25

    .line 429
    :cond_2f
    const-string v3, "::"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_25

    .line 434
    :cond_35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/net/Inet6Address;)Ljava/net/Inet4Address;
    .registers 6

    .prologue
    .line 570
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v0

    const-string v1, "Address \'%s\' is not IPv4-compatible."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 573
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    const/16 v1, 0xc

    const/16 v2, 0x10

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    return-object v0
.end method

.method private static b([B)Ljava/net/InetAddress;
    .registers 3

    .prologue
    .line 331
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    .line 332
    :catch_5
    move-exception v0

    .line 333
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static b(Ljava/lang/String;)Z
    .registers 2

    .prologue
    .line 168
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static c(Ljava/net/InetAddress;)Ljava/net/Inet4Address;
    .registers 9

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0xf

    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 872
    instance-of v1, p0, Ljava/net/Inet4Address;

    if-eqz v1, :cond_f

    .line 873
    check-cast p0, Ljava/net/Inet4Address;

    .line 913
    :goto_e
    return-object p0

    .line 877
    :cond_f
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v3

    move v1, v0

    .line 879
    :goto_14
    if-ge v1, v6, :cond_137

    .line 880
    aget-byte v4, v3, v1

    if-eqz v4, :cond_24

    move v1, v0

    .line 885
    :goto_1b
    if-eqz v1, :cond_27

    aget-byte v4, v3, v6

    if-ne v4, v2, :cond_27

    .line 886
    sget-object p0, Lcom/a/b/k/d;->c:Ljava/net/Inet4Address;

    goto :goto_e

    .line 879
    :cond_24
    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 887
    :cond_27
    if-eqz v1, :cond_30

    aget-byte v1, v3, v6

    if-nez v1, :cond_30

    .line 888
    sget-object p0, Lcom/a/b/k/d;->d:Ljava/net/Inet4Address;

    goto :goto_e

    .line 891
    :cond_30
    check-cast p0, Ljava/net/Inet6Address;

    .line 3778
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v1

    if-nez v1, :cond_44

    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v1

    if-nez v1, :cond_44

    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_8d

    :cond_44
    move v1, v2

    .line 893
    :goto_45
    if-eqz v1, :cond_129

    .line 3795
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_8f

    .line 4570
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v3, "Address \'%s\' is not IPv4-compatible."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v1, v3, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 4573
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v7, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    .line 894
    :goto_6c
    invoke-virtual {v0}, Ljava/net/Inet4Address;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 902
    :goto_71
    invoke-static {}, Lcom/a/b/g/am;->a()Lcom/a/b/g/ak;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/a/b/g/ak;->a(J)Lcom/a/b/g/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/g/ag;->b()I

    move-result v0

    .line 905
    const/high16 v1, -0x20000000

    or-int/2addr v0, v1

    .line 909
    const/4 v1, -0x1

    if-ne v0, v1, :cond_84

    .line 910
    const/4 v0, -0x2

    .line 913
    :cond_84
    invoke-static {v0}, Lcom/a/b/l/q;->b(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object p0

    goto :goto_e

    :cond_8d
    move v1, v0

    .line 3778
    goto :goto_45

    .line 3799
    :cond_8f
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_b5

    .line 4603
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v3, "Address \'%s\' is not a 6to4 address."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v1, v3, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 4606
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    goto :goto_6c

    .line 3803
    :cond_b5
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_115

    .line 4694
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v3, "Address \'%s\' is not a Teredo address."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v1, v3, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 4697
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v1

    .line 4698
    const/4 v2, 0x4

    invoke-static {v1, v2, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v2

    .line 4700
    invoke-static {v1, v5}, Lcom/a/b/i/z;->a([BI)Lcom/a/b/i/m;

    move-result-object v3

    invoke-interface {v3}, Lcom/a/b/i/m;->readShort()S

    move-result v3

    const v4, 0xffff

    and-int/2addr v3, v4

    .line 4703
    const/16 v4, 0xa

    invoke-static {v1, v4}, Lcom/a/b/i/z;->a([BI)Lcom/a/b/i/m;

    move-result-object v4

    invoke-interface {v4}, Lcom/a/b/i/m;->readShort()S

    move-result v4

    xor-int/lit8 v4, v4, -0x1

    const v5, 0xffff

    and-int/2addr v4, v5

    .line 4705
    const/16 v5, 0x10

    invoke-static {v1, v7, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    .line 4706
    :goto_fb
    array-length v5, v1

    if-ge v0, v5, :cond_108

    .line 4708
    aget-byte v5, v1, v0

    xor-int/lit8 v5, v5, -0x1

    int-to-byte v5, v5

    aput-byte v5, v1, v0

    .line 4706
    add-int/lit8 v0, v0, 0x1

    goto :goto_fb

    .line 4710
    :cond_108
    invoke-static {v1}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    .line 4712
    new-instance v1, Lcom/a/b/k/e;

    invoke-direct {v1, v2, v0, v4, v3}, Lcom/a/b/k/e;-><init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V

    .line 5660
    iget-object v0, v1, Lcom/a/b/k/e;->a:Ljava/net/Inet4Address;

    goto/16 :goto_6c

    .line 3807
    :cond_115
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "\'%s\' has no embedded IPv4 address."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 898
    :cond_129
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v1

    invoke-static {v1, v0, v5}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    goto/16 :goto_71

    :cond_137
    move v1, v2

    goto/16 :goto_1b
.end method

.method private static c([B)Ljava/net/InetAddress;
    .registers 4

    .prologue
    .line 964
    array-length v0, p0

    new-array v1, v0, [B

    .line 965
    const/4 v0, 0x0

    :goto_4
    array-length v2, p0

    if-ge v0, v2, :cond_12

    .line 966
    array-length v2, p0

    sub-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-byte v2, p0, v2

    aput-byte v2, v1, v0

    .line 965
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 968
    :cond_12
    invoke-static {v1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/net/Inet6Address;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 591
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v2

    .line 592
    aget-byte v3, v2, v1

    const/16 v4, 0x20

    if-ne v3, v4, :cond_12

    aget-byte v2, v2, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_12

    :goto_11
    return v0

    :cond_12
    move v0, v1

    goto :goto_11
.end method

.method private static c(Ljava/lang/String;)[B
    .registers 10

    .prologue
    const/16 v8, 0x3a

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 173
    move v0, v1

    move v2, v1

    move v3, v1

    .line 175
    :goto_8
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v0, v6, :cond_2d

    .line 176
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 177
    const/16 v7, 0x2e

    if-ne v6, v7, :cond_1a

    move v2, v4

    .line 175
    :cond_17
    :goto_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 179
    :cond_1a
    if-ne v6, v8, :cond_22

    .line 180
    if-eqz v2, :cond_20

    move-object v0, v5

    .line 201
    :goto_1f
    return-object v0

    :cond_20
    move v3, v4

    .line 183
    goto :goto_17

    .line 184
    :cond_22
    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Character;->digit(CI)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_17

    move-object v0, v5

    .line 185
    goto :goto_1f

    .line 190
    :cond_2d
    if-eqz v3, :cond_b6

    .line 191
    if-eqz v2, :cond_b0

    .line 1286
    invoke-virtual {p0, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 1287
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1288
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1289
    invoke-static {v0}, Lcom/a/b/k/d;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 1290
    if-nez v0, :cond_4c

    move-object p0, v5

    .line 193
    :goto_48
    if-nez p0, :cond_b0

    move-object v0, v5

    .line 194
    goto :goto_1f

    .line 1293
    :cond_4c
    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    aget-byte v3, v0, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 1294
    const/4 v3, 0x2

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    const/4 v4, 0x3

    aget-byte v0, v0, v4

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 1295
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v4, v6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v4, v6

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_48

    .line 197
    :cond_b0
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/lang/String;)[B

    move-result-object v0

    goto/16 :goto_1f

    .line 198
    :cond_b6
    if-eqz v2, :cond_be

    .line 199
    invoke-static {p0}, Lcom/a/b/k/d;->d(Ljava/lang/String;)[B

    move-result-object v0

    goto/16 :goto_1f

    :cond_be
    move-object v0, v5

    .line 201
    goto/16 :goto_1f
.end method

.method private static d(Ljava/net/InetAddress;)I
    .registers 9

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0xf

    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 938
    .line 5872
    instance-of v1, p0, Ljava/net/Inet4Address;

    if-eqz v1, :cond_1b

    .line 5873
    check-cast p0, Ljava/net/Inet4Address;

    .line 938
    :goto_e
    invoke-virtual {p0}, Ljava/net/Inet4Address;->getAddress()[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/i/z;->a([B)Lcom/a/b/i/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/i/m;->readInt()I

    move-result v0

    return v0

    .line 5877
    :cond_1b
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v3

    move v1, v0

    .line 5879
    :goto_20
    if-ge v1, v6, :cond_144

    .line 5880
    aget-byte v4, v3, v1

    if-eqz v4, :cond_30

    move v1, v0

    .line 5885
    :goto_27
    if-eqz v1, :cond_33

    aget-byte v4, v3, v6

    if-ne v4, v2, :cond_33

    .line 5886
    sget-object p0, Lcom/a/b/k/d;->c:Ljava/net/Inet4Address;

    goto :goto_e

    .line 5879
    :cond_30
    add-int/lit8 v1, v1, 0x1

    goto :goto_20

    .line 5887
    :cond_33
    if-eqz v1, :cond_3c

    aget-byte v1, v3, v6

    if-nez v1, :cond_3c

    .line 5888
    sget-object p0, Lcom/a/b/k/d;->d:Ljava/net/Inet4Address;

    goto :goto_e

    .line 5891
    :cond_3c
    check-cast p0, Ljava/net/Inet6Address;

    .line 6778
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v1

    if-nez v1, :cond_50

    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v1

    if-nez v1, :cond_50

    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_9a

    :cond_50
    move v1, v2

    .line 5893
    :goto_51
    if-eqz v1, :cond_136

    .line 6795
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_9c

    .line 7570
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v3, "Address \'%s\' is not IPv4-compatible."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v1, v3, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 7573
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v0, v7, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    .line 5894
    :goto_78
    invoke-virtual {v0}, Ljava/net/Inet4Address;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 5902
    :goto_7d
    invoke-static {}, Lcom/a/b/g/am;->a()Lcom/a/b/g/ak;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/a/b/g/ak;->a(J)Lcom/a/b/g/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/g/ag;->b()I

    move-result v0

    .line 5905
    const/high16 v1, -0x20000000

    or-int/2addr v0, v1

    .line 5909
    const/4 v1, -0x1

    if-ne v0, v1, :cond_90

    .line 5910
    const/4 v0, -0x2

    .line 5913
    :cond_90
    invoke-static {v0}, Lcom/a/b/l/q;->b(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object p0

    goto/16 :goto_e

    :cond_9a
    move v1, v0

    .line 6778
    goto :goto_51

    .line 6799
    :cond_9c
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_c2

    .line 7603
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v3, "Address \'%s\' is not a 6to4 address."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v1, v3, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 7606
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    goto :goto_78

    .line 6803
    :cond_c2
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_122

    .line 7694
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v3, "Address \'%s\' is not a Teredo address."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v1, v3, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 7697
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v1

    .line 7698
    const/4 v2, 0x4

    invoke-static {v1, v2, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v2

    .line 7700
    invoke-static {v1, v5}, Lcom/a/b/i/z;->a([BI)Lcom/a/b/i/m;

    move-result-object v3

    invoke-interface {v3}, Lcom/a/b/i/m;->readShort()S

    move-result v3

    const v4, 0xffff

    and-int/2addr v3, v4

    .line 7703
    const/16 v4, 0xa

    invoke-static {v1, v4}, Lcom/a/b/i/z;->a([BI)Lcom/a/b/i/m;

    move-result-object v4

    invoke-interface {v4}, Lcom/a/b/i/m;->readShort()S

    move-result v4

    xor-int/lit8 v4, v4, -0x1

    const v5, 0xffff

    and-int/2addr v4, v5

    .line 7705
    const/16 v5, 0x10

    invoke-static {v1, v7, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    .line 7706
    :goto_108
    array-length v5, v1

    if-ge v0, v5, :cond_115

    .line 7708
    aget-byte v5, v1, v0

    xor-int/lit8 v5, v5, -0x1

    int-to-byte v5, v5

    aput-byte v5, v1, v0

    .line 7706
    add-int/lit8 v0, v0, 0x1

    goto :goto_108

    .line 7710
    :cond_115
    invoke-static {v1}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    .line 7712
    new-instance v1, Lcom/a/b/k/e;

    invoke-direct {v1, v2, v0, v4, v3}, Lcom/a/b/k/e;-><init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V

    .line 8660
    iget-object v0, v1, Lcom/a/b/k/e;->a:Ljava/net/Inet4Address;

    goto/16 :goto_78

    .line 6807
    :cond_122
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "\'%s\' has no embedded IPv4 address."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 5898
    :cond_136
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v1

    invoke-static {v1, v0, v5}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    goto/16 :goto_7d

    :cond_144
    move v1, v2

    goto/16 :goto_27
.end method

.method private static d(Ljava/net/Inet6Address;)Ljava/net/Inet4Address;
    .registers 6

    .prologue
    .line 603
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v0

    const-string v1, "Address \'%s\' is not a 6to4 address."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 606
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/String;)[B
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x4

    .line 205
    const-string v1, "\\."

    const/4 v2, 0x5

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 206
    array-length v1, v3

    if-eq v1, v7, :cond_d

    .line 219
    :goto_c
    return-object v0

    .line 210
    :cond_d
    new-array v1, v7, [B

    .line 212
    const/4 v2, 0x0

    :goto_10
    if-ge v2, v7, :cond_39

    .line 213
    :try_start_12
    aget-object v4, v3, v2

    .line 1300
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1303
    const/16 v6, 0xff

    if-gt v5, v6, :cond_2b

    const-string v6, "0"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_33

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v6, 0x1

    if-le v4, v6, :cond_33

    .line 1304
    :cond_2b
    new-instance v1, Ljava/lang/NumberFormatException;

    invoke-direct {v1}, Ljava/lang/NumberFormatException;-><init>()V

    throw v1

    .line 216
    :catch_31
    move-exception v1

    goto :goto_c

    .line 1306
    :cond_33
    int-to-byte v4, v5

    .line 213
    aput-byte v4, v1, v2
    :try_end_36
    .catch Ljava/lang/NumberFormatException; {:try_start_12 .. :try_end_36} :catch_31

    .line 212
    add-int/lit8 v2, v2, 0x1

    goto :goto_10

    :cond_39
    move-object v0, v1

    .line 219
    goto :goto_c
.end method

.method private static e(Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 981
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v4

    .line 982
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    .line 983
    :goto_a
    if-ltz v3, :cond_17

    aget-byte v0, v4, v3

    if-nez v0, :cond_17

    .line 984
    const/4 v0, -0x1

    aput-byte v0, v4, v3

    .line 985
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_a

    .line 988
    :cond_17
    if-ltz v3, :cond_2f

    move v0, v1

    :goto_1a
    const-string v5, "Decrementing %s would wrap."

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v5, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 990
    aget-byte v0, v4, v3

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    aput-byte v0, v4, v3

    .line 991
    invoke-static {v4}, Lcom/a/b/k/d;->b([B)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0

    :cond_2f
    move v0, v2

    .line 988
    goto :goto_1a
.end method

.method private static e(Ljava/net/Inet6Address;)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 681
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v2

    .line 682
    aget-byte v3, v2, v1

    const/16 v4, 0x20

    if-ne v3, v4, :cond_1b

    aget-byte v3, v2, v0

    if-ne v3, v0, :cond_1b

    const/4 v3, 0x2

    aget-byte v3, v2, v3

    if-nez v3, :cond_1b

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    if-nez v2, :cond_1b

    :goto_1a
    return v0

    :cond_1b
    move v0, v1

    goto :goto_1a
.end method

.method private static e(Ljava/lang/String;)[B
    .registers 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 224
    const-string v0, ":"

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    .line 225
    array-length v0, v5

    const/4 v1, 0x3

    if-lt v0, v1, :cond_13

    array-length v0, v5

    const/16 v1, 0x9

    if-le v0, v1, :cond_15

    :cond_13
    move-object v0, v2

    .line 282
    :goto_14
    return-object v0

    .line 231
    :cond_15
    const/4 v1, -0x1

    .line 232
    const/4 v0, 0x1

    :goto_17
    array-length v4, v5

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_2c

    .line 233
    aget-object v4, v5, v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_29

    .line 234
    if-ltz v1, :cond_28

    move-object v0, v2

    .line 235
    goto :goto_14

    :cond_28
    move v1, v0

    .line 232
    :cond_29
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .line 243
    :cond_2c
    if-ltz v1, :cond_52

    .line 246
    array-length v0, v5

    sub-int/2addr v0, v1

    add-int/lit8 v4, v0, -0x1

    .line 247
    aget-object v0, v5, v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_40

    add-int/lit8 v0, v1, -0x1

    if-eqz v0, :cond_41

    move-object v0, v2

    .line 248
    goto :goto_14

    :cond_40
    move v0, v1

    .line 250
    :cond_41
    array-length v6, v5

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v5, v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_99

    add-int/lit8 v4, v4, -0x1

    if-eqz v4, :cond_99

    move-object v0, v2

    .line 251
    goto :goto_14

    .line 256
    :cond_52
    array-length v0, v5

    move v4, v0

    move v0, v3

    .line 262
    :goto_55
    add-int v6, v4, v0

    rsub-int/lit8 v6, v6, 0x8

    .line 263
    if-ltz v1, :cond_72

    if-lez v6, :cond_74

    .line 268
    :cond_5d
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    move v1, v3

    .line 270
    :goto_64
    if-ge v1, v4, :cond_76

    .line 271
    :try_start_66
    aget-object v8, v5, v1

    invoke-static {v8}, Lcom/a/b/k/d;->h(Ljava/lang/String;)S

    move-result v8

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 270
    add-int/lit8 v1, v1, 0x1

    goto :goto_64

    .line 263
    :cond_72
    if-eqz v6, :cond_5d

    :cond_74
    move-object v0, v2

    .line 264
    goto :goto_14

    :cond_76
    move v1, v3

    .line 273
    :goto_77
    if-ge v1, v6, :cond_80

    .line 274
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 273
    add-int/lit8 v1, v1, 0x1

    goto :goto_77

    .line 276
    :cond_80
    :goto_80
    if-lez v0, :cond_93

    .line 277
    array-length v1, v5

    sub-int/2addr v1, v0

    aget-object v1, v5, v1

    invoke-static {v1}, Lcom/a/b/k/d;->h(Ljava/lang/String;)S

    move-result v1

    invoke-virtual {v7, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
    :try_end_8d
    .catch Ljava/lang/NumberFormatException; {:try_start_66 .. :try_end_8d} :catch_90

    .line 276
    add-int/lit8 v0, v0, -0x1

    goto :goto_80

    .line 280
    :catch_90
    move-exception v0

    move-object v0, v2

    goto :goto_14

    .line 282
    :cond_93
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    goto/16 :goto_14

    :cond_99
    move v9, v4

    move v4, v0

    move v0, v9

    goto :goto_55
.end method

.method private static f(Ljava/net/Inet6Address;)Lcom/a/b/k/e;
    .registers 8

    .prologue
    const v6, 0xffff

    const/16 v5, 0x8

    const/4 v0, 0x0

    .line 694
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v2, "Address \'%s\' is not a Teredo address."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 697
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v1

    .line 698
    const/4 v2, 0x4

    invoke-static {v1, v2, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v2

    .line 700
    invoke-static {v1, v5}, Lcom/a/b/i/z;->a([BI)Lcom/a/b/i/m;

    move-result-object v3

    invoke-interface {v3}, Lcom/a/b/i/m;->readShort()S

    move-result v3

    and-int/2addr v3, v6

    .line 703
    const/16 v4, 0xa

    invoke-static {v1, v4}, Lcom/a/b/i/z;->a([BI)Lcom/a/b/i/m;

    move-result-object v4

    invoke-interface {v4}, Lcom/a/b/i/m;->readShort()S

    move-result v4

    xor-int/lit8 v4, v4, -0x1

    and-int/2addr v4, v6

    .line 705
    const/16 v5, 0xc

    const/16 v6, 0x10

    invoke-static {v1, v5, v6}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    .line 706
    :goto_43
    array-length v5, v1

    if-ge v0, v5, :cond_50

    .line 708
    aget-byte v5, v1, v0

    xor-int/lit8 v5, v5, -0x1

    int-to-byte v5, v5

    aput-byte v5, v1, v0

    .line 706
    add-int/lit8 v0, v0, 0x1

    goto :goto_43

    .line 710
    :cond_50
    invoke-static {v1}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    .line 712
    new-instance v1, Lcom/a/b/k/e;

    invoke-direct {v1, v2, v0, v4, v3}, Lcom/a/b/k/e;-><init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V

    return-object v1
.end method

.method private static f(Ljava/lang/String;)Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 286
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 287
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 288
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 289
    invoke-static {v0}, Lcom/a/b/k/d;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 290
    if-nez v0, :cond_1b

    .line 291
    const/4 v0, 0x0

    .line 295
    :goto_1a
    return-object v0

    .line 293
    :cond_1b
    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x1

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 294
    const/4 v3, 0x2

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    const/4 v4, 0x3

    aget-byte v0, v0, v4

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 295
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1a
.end method

.method private static f(Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1004
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v4

    .line 1005
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    .line 1006
    :goto_a
    if-ltz v3, :cond_17

    aget-byte v0, v4, v3

    const/4 v5, -0x1

    if-ne v0, v5, :cond_17

    .line 1007
    aput-byte v2, v4, v3

    .line 1008
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_a

    .line 1011
    :cond_17
    if-ltz v3, :cond_2f

    move v0, v1

    :goto_1a
    const-string v5, "Incrementing %s would wrap."

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v5, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1013
    aget-byte v0, v4, v3

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    aput-byte v0, v4, v3

    .line 1014
    invoke-static {v4}, Lcom/a/b/k/d;->b([B)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0

    :cond_2f
    move v0, v2

    .line 1011
    goto :goto_1a
.end method

.method private static g(Ljava/lang/String;)B
    .registers 4

    .prologue
    .line 300
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 303
    const/16 v1, 0xff

    if-gt v0, v1, :cond_17

    const-string v1, "0"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1d

    .line 304
    :cond_17
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0}, Ljava/lang/NumberFormatException;-><init>()V

    throw v0

    .line 306
    :cond_1d
    int-to-byte v0, v0

    return v0
.end method

.method private static g(Ljava/net/Inet6Address;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 734
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 747
    :cond_7
    :goto_7
    return v0

    .line 738
    :cond_8
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v1

    .line 740
    const/16 v2, 0x8

    aget-byte v2, v1, v2

    or-int/lit8 v2, v2, 0x3

    const/4 v3, 0x3

    if-ne v2, v3, :cond_7

    .line 747
    const/16 v2, 0x9

    aget-byte v2, v1, v2

    if-nez v2, :cond_7

    const/16 v2, 0xa

    aget-byte v2, v1, v2

    const/16 v3, 0x5e

    if-ne v2, v3, :cond_7

    const/16 v2, 0xb

    aget-byte v1, v1, v2

    const/4 v2, -0x2

    if-ne v1, v2, :cond_7

    const/4 v0, 0x1

    goto :goto_7
.end method

.method private static g(Ljava/net/InetAddress;)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1026
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v2

    move v0, v1

    .line 1027
    :goto_6
    array-length v3, v2

    if-ge v0, v3, :cond_12

    .line 1028
    aget-byte v3, v2, v0

    const/4 v4, -0x1

    if-eq v3, v4, :cond_f

    .line 1032
    :goto_e
    return v1

    .line 1027
    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1032
    :cond_12
    const/4 v1, 0x1

    goto :goto_e
.end method

.method private static h(Ljava/net/Inet6Address;)Ljava/net/Inet4Address;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 759
    .line 1734
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 1738
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    .line 1740
    const/16 v3, 0x8

    aget-byte v3, v0, v3

    or-int/lit8 v3, v3, 0x3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_49

    .line 1747
    const/16 v3, 0x9

    aget-byte v3, v0, v3

    if-nez v3, :cond_49

    const/16 v3, 0xa

    aget-byte v3, v0, v3

    const/16 v4, 0x5e

    if-ne v3, v4, :cond_49

    const/16 v3, 0xb

    aget-byte v0, v0, v3

    const/4 v3, -0x2

    if-ne v0, v3, :cond_49

    move v0, v1

    .line 759
    :goto_2b
    const-string v3, "Address \'%s\' is not an ISATAP address."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 762
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    const/16 v1, 0xc

    const/16 v2, 0x10

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    return-object v0

    :cond_49
    move v0, v2

    .line 1747
    goto :goto_2b
.end method

.method private static h(Ljava/lang/String;)S
    .registers 3

    .prologue
    .line 311
    const/16 v0, 0x10

    invoke-static {p0, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 312
    const v1, 0xffff

    if-le v0, v1, :cond_11

    .line 313
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0}, Ljava/lang/NumberFormatException;-><init>()V

    throw v0

    .line 315
    :cond_11
    int-to-short v0, v0

    return v0
.end method

.method private static i(Ljava/lang/String;)Ljava/net/InetAddress;
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 485
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 491
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 492
    const/16 v0, 0x10

    .line 499
    :goto_20
    invoke-static {v1}, Lcom/a/b/k/d;->c(Ljava/lang/String;)[B

    move-result-object v1

    .line 500
    if-eqz v1, :cond_29

    array-length v2, v1

    if-eq v2, v0, :cond_3d

    .line 501
    :cond_29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not a valid URI IP literal: \'%s\'"

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495
    :cond_3a
    const/4 v0, 0x4

    move-object v1, p0

    goto :goto_20

    .line 505
    :cond_3d
    invoke-static {v1}, Lcom/a/b/k/d;->b([B)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method private static i(Ljava/net/Inet6Address;)Z
    .registers 2

    .prologue
    .line 778
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private static j(Ljava/net/Inet6Address;)Ljava/net/Inet4Address;
    .registers 9

    .prologue
    const/16 v7, 0x10

    const/16 v6, 0xc

    const/16 v5, 0x8

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 795
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 2570
    invoke-static {p0}, Lcom/a/b/k/d;->a(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v2, "Address \'%s\' is not IPv4-compatible."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2573
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    invoke-static {v0, v6, v7}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    .line 804
    :goto_2b
    return-object v0

    .line 799
    :cond_2c
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 2603
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v2, "Address \'%s\' is not a 6to4 address."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2606
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    goto :goto_2b

    .line 803
    :cond_52
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    if-eqz v1, :cond_b0

    .line 2694
    invoke-static {p0}, Lcom/a/b/k/d;->e(Ljava/net/Inet6Address;)Z

    move-result v1

    const-string v2, "Address \'%s\' is not a Teredo address."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2697
    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v1

    .line 2698
    const/4 v2, 0x4

    invoke-static {v1, v2, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v2

    .line 2700
    invoke-static {v1, v5}, Lcom/a/b/i/z;->a([BI)Lcom/a/b/i/m;

    move-result-object v3

    invoke-interface {v3}, Lcom/a/b/i/m;->readShort()S

    move-result v3

    const v4, 0xffff

    and-int/2addr v3, v4

    .line 2703
    const/16 v4, 0xa

    invoke-static {v1, v4}, Lcom/a/b/i/z;->a([BI)Lcom/a/b/i/m;

    move-result-object v4

    invoke-interface {v4}, Lcom/a/b/i/m;->readShort()S

    move-result v4

    xor-int/lit8 v4, v4, -0x1

    const v5, 0xffff

    and-int/2addr v4, v5

    .line 2705
    invoke-static {v1, v6, v7}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    .line 2706
    :goto_96
    array-length v5, v1

    if-ge v0, v5, :cond_a3

    .line 2708
    aget-byte v5, v1, v0

    xor-int/lit8 v5, v5, -0x1

    int-to-byte v5, v5

    aput-byte v5, v1, v0

    .line 2706
    add-int/lit8 v0, v0, 0x1

    goto :goto_96

    .line 2710
    :cond_a3
    invoke-static {v1}, Lcom/a/b/k/d;->a([B)Ljava/net/Inet4Address;

    move-result-object v0

    .line 2712
    new-instance v1, Lcom/a/b/k/e;

    invoke-direct {v1, v2, v0, v4, v3}, Lcom/a/b/k/e;-><init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V

    .line 3660
    iget-object v0, v1, Lcom/a/b/k/e;->a:Ljava/net/Inet4Address;

    goto/16 :goto_2b

    .line 807
    :cond_b0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'%s\' has no embedded IPv4 address."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/a/b/k/d;->b(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static j(Ljava/lang/String;)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 517
    .line 1485
    :try_start_2
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1490
    const-string v2, "["

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_40

    const-string v2, "]"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_40

    .line 1491
    const/4 v2, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1492
    const/16 v2, 0x10

    .line 1499
    :goto_22
    invoke-static {v3}, Lcom/a/b/k/d;->c(Ljava/lang/String;)[B

    move-result-object v3

    .line 1500
    if-eqz v3, :cond_2b

    array-length v4, v3

    if-eq v4, v2, :cond_43

    .line 1501
    :cond_2b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Not a valid URI IP literal: \'%s\'"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :catch_3d
    move-exception v0

    move v0, v1

    :goto_3f
    return v0

    .line 1495
    :cond_40
    const/4 v2, 0x4

    move-object v3, p0

    goto :goto_22

    .line 1505
    :cond_43
    invoke-static {v3}, Lcom/a/b/k/d;->b([B)Ljava/net/InetAddress;
    :try_end_46
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_46} :catch_3d

    goto :goto_3f
.end method

.method private static k(Ljava/lang/String;)Z
    .registers 6

    .prologue
    const/16 v1, 0xa

    const/4 v0, 0x0

    .line 834
    invoke-static {p0}, Lcom/a/b/k/d;->c(Ljava/lang/String;)[B

    move-result-object v3

    .line 835
    if-eqz v3, :cond_15

    array-length v2, v3

    const/16 v4, 0x10

    if-ne v2, v4, :cond_15

    move v2, v0

    .line 836
    :goto_f
    if-ge v2, v1, :cond_1b

    .line 837
    aget-byte v4, v3, v2

    if-eqz v4, :cond_16

    .line 848
    :cond_15
    :goto_15
    return v0

    .line 836
    :cond_16
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    .line 841
    :cond_19
    add-int/lit8 v1, v1, 0x1

    :cond_1b
    const/16 v2, 0xc

    if-ge v1, v2, :cond_25

    .line 842
    aget-byte v2, v3, v1

    const/4 v4, -0x1

    if-eq v2, v4, :cond_19

    goto :goto_15

    .line 846
    :cond_25
    const/4 v0, 0x1

    goto :goto_15
.end method
