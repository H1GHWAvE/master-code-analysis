.class abstract enum Lcom/a/b/m/at;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;


# static fields
.field public static final enum a:Lcom/a/b/m/at;

.field public static final enum b:Lcom/a/b/m/at;

.field private static final synthetic c:[Lcom/a/b/m/at;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 682
    new-instance v0, Lcom/a/b/m/au;

    const-string v1, "IGNORE_TYPE_VARIABLE_OR_WILDCARD"

    invoke-direct {v0, v1}, Lcom/a/b/m/au;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/m/at;->a:Lcom/a/b/m/at;

    .line 688
    new-instance v0, Lcom/a/b/m/av;

    const-string v1, "INTERFACE_ONLY"

    invoke-direct {v0, v1}, Lcom/a/b/m/av;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/m/at;->b:Lcom/a/b/m/at;

    .line 680
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/m/at;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/m/at;->a:Lcom/a/b/m/at;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/m/at;->b:Lcom/a/b/m/at;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/m/at;->c:[Lcom/a/b/m/at;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 680
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 680
    invoke-direct {p0, p1, p2}, Lcom/a/b/m/at;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/m/at;
    .registers 2

    .prologue
    .line 680
    const-class v0, Lcom/a/b/m/at;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/m/at;

    return-object v0
.end method

.method public static values()[Lcom/a/b/m/at;
    .registers 1

    .prologue
    .line 680
    sget-object v0, Lcom/a/b/m/at;->c:[Lcom/a/b/m/at;

    invoke-virtual {v0}, [Lcom/a/b/m/at;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/m/at;

    return-object v0
.end method
