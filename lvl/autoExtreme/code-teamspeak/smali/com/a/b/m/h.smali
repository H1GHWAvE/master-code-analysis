.class public final Lcom/a/b/m/h;
.super Lcom/a/b/d/gs;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/m/ad;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Lcom/a/b/d/jt;


# direct methods
.method private constructor <init>(Lcom/a/b/d/jt;)V
    .registers 2

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/a/b/m/h;->a:Lcom/a/b/d/jt;

    .line 102
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/jt;B)V
    .registers 3

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/a/b/m/h;-><init>(Lcom/a/b/d/jt;)V

    return-void
.end method

.method private static b()Lcom/a/b/m/h;
    .registers 2

    .prologue
    .line 38
    new-instance v0, Lcom/a/b/m/h;

    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/m/h;-><init>(Lcom/a/b/d/jt;)V

    return-object v0
.end method

.method private b(Lcom/a/b/m/ae;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/a/b/m/h;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static c()Lcom/a/b/m/j;
    .registers 2

    .prologue
    .line 43
    new-instance v0, Lcom/a/b/m/j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/m/j;-><init>(B)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/m/ae;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 105
    invoke-virtual {p1}, Lcom/a/b/m/ae;->c()Lcom/a/b/m/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/m/h;->b(Lcom/a/b/m/ae;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 114
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 118
    invoke-static {p1}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/m/h;->b(Lcom/a/b/m/ae;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 127
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/a/b/m/h;->a:Lcom/a/b/d/jt;

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 32
    .line 1131
    iget-object v0, p0, Lcom/a/b/m/h;->a:Lcom/a/b/d/jt;

    .line 32
    return-object v0
.end method
