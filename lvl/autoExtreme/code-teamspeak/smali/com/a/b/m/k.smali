.class public abstract Lcom/a/b/m/k;
.super Lcom/a/b/m/g;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/GenericDeclaration;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/reflect/AccessibleObject;)V
    .registers 2

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/a/b/m/g;-><init>(Ljava/lang/reflect/AccessibleObject;)V

    .line 64
    return-void
.end method

.method private a(Lcom/a/b/m/ae;)Lcom/a/b/m/k;
    .registers 8

    .prologue
    .line 153
    .line 4109
    invoke-virtual {p0}, Lcom/a/b/m/k;->g()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 153
    invoke-virtual {p1, v0}, Lcom/a/b/m/ae;->a(Lcom/a/b/m/ae;)Z

    move-result v0

    if-nez v0, :cond_54

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 5109
    invoke-virtual {p0}, Lcom/a/b/m/k;->g()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v1

    .line 154
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x23

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invokable is known to return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", not "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_54
    return-object p0
.end method

.method private a(Ljava/lang/Class;)Lcom/a/b/m/k;
    .registers 8

    .prologue
    .line 148
    invoke-static {p1}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 2109
    invoke-virtual {p0}, Lcom/a/b/m/k;->g()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v1

    .line 1153
    invoke-virtual {v0, v1}, Lcom/a/b/m/ae;->a(Lcom/a/b/m/ae;)Z

    move-result v1

    if-nez v1, :cond_58

    .line 1154
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 3109
    invoke-virtual {p0}, Lcom/a/b/m/k;->g()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v2

    .line 1154
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x23

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invokable is known to return "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", not "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 148
    :cond_58
    return-object p0
.end method

.method private static a(Ljava/lang/reflect/Constructor;)Lcom/a/b/m/k;
    .registers 2

    .prologue
    .line 73
    new-instance v0, Lcom/a/b/m/l;

    invoke-direct {v0, p0}, Lcom/a/b/m/l;-><init>(Ljava/lang/reflect/Constructor;)V

    return-object v0
.end method

.method private static a(Ljava/lang/reflect/Method;)Lcom/a/b/m/k;
    .registers 2

    .prologue
    .line 68
    new-instance v0, Lcom/a/b/m/m;

    invoke-direct {v0, p0}, Lcom/a/b/m/m;-><init>(Ljava/lang/reflect/Method;)V

    return-object v0
.end method

.method private varargs b(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 102
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, Lcom/a/b/m/k;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/a/b/m/ae;
    .registers 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/a/b/m/k;->g()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    return-object v0
.end method

.method private i()Lcom/a/b/d/jl;
    .registers 8

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/a/b/m/k;->d()[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 119
    invoke-virtual {p0}, Lcom/a/b/m/k;->f()[[Ljava/lang/annotation/Annotation;

    move-result-object v2

    .line 120
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v3

    .line 121
    const/4 v0, 0x0

    :goto_d
    array-length v4, v1

    if-ge v0, v4, :cond_23

    .line 122
    new-instance v4, Lcom/a/b/m/s;

    aget-object v5, v1, v0

    invoke-static {v5}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v5

    aget-object v6, v2, v0

    invoke-direct {v4, p0, v0, v5, v6}, Lcom/a/b/m/s;-><init>(Lcom/a/b/m/k;ILcom/a/b/m/ae;[Ljava/lang/annotation/Annotation;)V

    invoke-virtual {v3, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 125
    :cond_23
    invoke-virtual {v3}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method private j()Lcom/a/b/d/jl;
    .registers 6

    .prologue
    .line 130
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v1

    .line 131
    invoke-virtual {p0}, Lcom/a/b/m/k;->e()[Ljava/lang/reflect/Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v3, :cond_18

    aget-object v4, v2, v0

    .line 134
    invoke-static {v4}, Lcom/a/b/m/ae;->a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v4

    .line 136
    invoke-virtual {v1, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 138
    :cond_18
    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/a/b/m/ae;
    .registers 2

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/a/b/m/k;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract b()Z
.end method

.method public abstract c()Z
.end method

.method abstract d()[Ljava/lang/reflect/Type;
.end method

.method abstract e()[Ljava/lang/reflect/Type;
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/a/b/m/g;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method abstract f()[[Ljava/lang/annotation/Annotation;
.end method

.method abstract g()Ljava/lang/reflect/Type;
.end method

.method public final getDeclaringClass()Ljava/lang/Class;
    .registers 2

    .prologue
    .line 164
    invoke-super {p0}, Lcom/a/b/m/g;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/a/b/m/g;->hashCode()I

    move-result v0

    return v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/a/b/m/g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
