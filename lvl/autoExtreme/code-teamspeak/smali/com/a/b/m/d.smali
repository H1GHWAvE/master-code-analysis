.class public final Lcom/a/b/m/d;
.super Lcom/a/b/m/e;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field final a:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .registers 4

    .prologue
    .line 211
    invoke-direct {p0, p1, p2}, Lcom/a/b/m/e;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 212
    invoke-static {p1}, Lcom/a/b/m/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    .line 213
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/b/m/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/m/d;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 232
    iget-object v0, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 233
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1a

    .line 234
    iget-object v1, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 237
    sget-object v1, Lcom/a/b/b/m;->c:Lcom/a/b/b/m;

    invoke-virtual {v1, v0}, Lcom/a/b/b/m;->j(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 245
    :goto_19
    return-object v0

    .line 1222
    :cond_1a
    iget-object v0, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/b/m/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_29

    .line 241
    iget-object v0, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    goto :goto_19

    .line 245
    :cond_29
    iget-object v1, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_19
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    return-object v0
.end method

.method private d()Ljava/lang/Class;
    .registers 3

    .prologue
    .line 266
    :try_start_0
    iget-object v0, p0, Lcom/a/b/m/d;->b:Ljava/lang/ClassLoader;

    iget-object v1, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    return-object v0

    .line 267
    :catch_9
    move-exception v0

    .line 269
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    return-object v0
.end method
