.class public Lcom/a/b/m/aw;
.super Lcom/a/b/d/hi;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field private transient a:Lcom/a/b/d/lo;

.field final synthetic b:Lcom/a/b/m/ae;


# direct methods
.method constructor <init>(Lcom/a/b/m/ae;)V
    .registers 2

    .prologue
    .line 551
    iput-object p1, p0, Lcom/a/b/m/aw;->b:Lcom/a/b/m/ae;

    invoke-direct {p0}, Lcom/a/b/d/hi;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Ljava/util/Set;
    .registers 3

    .prologue
    .line 564
    iget-object v0, p0, Lcom/a/b/m/aw;->a:Lcom/a/b/d/lo;

    .line 565
    if-nez v0, :cond_1e

    .line 568
    sget-object v0, Lcom/a/b/m/an;->a:Lcom/a/b/m/an;

    iget-object v1, p0, Lcom/a/b/m/aw;->b:Lcom/a/b/m/ae;

    invoke-virtual {v0, v1}, Lcom/a/b/m/an;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 570
    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    sget-object v1, Lcom/a/b/m/at;->a:Lcom/a/b/m/at;

    invoke-virtual {v0, v1}, Lcom/a/b/d/gd;->a(Lcom/a/b/b/co;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 1396
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 570
    iput-object v0, p0, Lcom/a/b/m/aw;->a:Lcom/a/b/d/lo;

    .line 574
    :cond_1e
    return-object v0
.end method

.method protected synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/a/b/m/aw;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/a/b/m/aw;
    .registers 4

    .prologue
    .line 560
    new-instance v0, Lcom/a/b/m/aj;

    iget-object v1, p0, Lcom/a/b/m/aw;->b:Lcom/a/b/m/ae;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/m/aj;-><init>(Lcom/a/b/m/ae;B)V

    return-object v0
.end method

.method public d()Ljava/util/Set;
    .registers 3

    .prologue
    .line 582
    sget-object v0, Lcom/a/b/m/an;->b:Lcom/a/b/m/an;

    iget-object v1, p0, Lcom/a/b/m/aw;->b:Lcom/a/b/m/ae;

    .line 2190
    iget-object v1, v1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/a/b/m/ae;->f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;

    move-result-object v1

    .line 582
    invoke-virtual {v0, v1}, Lcom/a/b/m/an;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 584
    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/a/b/m/aw;
    .registers 3

    .prologue
    .line 555
    new-instance v0, Lcom/a/b/m/ak;

    iget-object v1, p0, Lcom/a/b/m/aw;->b:Lcom/a/b/m/ae;

    invoke-direct {v0, v1, p0}, Lcom/a/b/m/ak;-><init>(Lcom/a/b/m/ae;Lcom/a/b/m/aw;)V

    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/a/b/m/aw;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
