.class final Lcom/a/b/m/aj;
.super Lcom/a/b/m/aw;
.source "SourceFile"


# static fields
.field private static final d:J


# instance fields
.field final synthetic a:Lcom/a/b/m/ae;

.field private transient c:Lcom/a/b/d/lo;


# direct methods
.method private constructor <init>(Lcom/a/b/m/ae;)V
    .registers 2

    .prologue
    .line 639
    iput-object p1, p0, Lcom/a/b/m/aj;->a:Lcom/a/b/m/ae;

    invoke-direct {p0, p1}, Lcom/a/b/m/aw;-><init>(Lcom/a/b/m/ae;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/m/ae;B)V
    .registers 3

    .prologue
    .line 639
    invoke-direct {p0, p1}, Lcom/a/b/m/aj;-><init>(Lcom/a/b/m/ae;)V

    return-void
.end method

.method private f()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 674
    iget-object v0, p0, Lcom/a/b/m/aj;->a:Lcom/a/b/m/ae;

    invoke-virtual {v0}, Lcom/a/b/m/ae;->b()Lcom/a/b/m/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/m/aw;->c()Lcom/a/b/m/aw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 3

    .prologue
    .line 644
    iget-object v0, p0, Lcom/a/b/m/aj;->c:Lcom/a/b/d/lo;

    .line 645
    if-nez v0, :cond_22

    .line 647
    sget-object v0, Lcom/a/b/m/an;->a:Lcom/a/b/m/an;

    invoke-virtual {v0}, Lcom/a/b/m/an;->a()Lcom/a/b/m/an;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/m/aj;->a:Lcom/a/b/m/ae;

    invoke-virtual {v0, v1}, Lcom/a/b/m/an;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 649
    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    sget-object v1, Lcom/a/b/m/at;->a:Lcom/a/b/m/at;

    invoke-virtual {v0, v1}, Lcom/a/b/d/gd;->a(Lcom/a/b/b/co;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 1396
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 649
    iput-object v0, p0, Lcom/a/b/m/aj;->c:Lcom/a/b/d/lo;

    .line 653
    :cond_22
    return-object v0
.end method

.method protected final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/a/b/m/aj;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/a/b/m/aw;
    .registers 1

    .prologue
    .line 658
    return-object p0
.end method

.method public final d()Ljava/util/Set;
    .registers 3

    .prologue
    .line 664
    sget-object v0, Lcom/a/b/m/an;->b:Lcom/a/b/m/an;

    invoke-virtual {v0}, Lcom/a/b/m/an;->a()Lcom/a/b/m/an;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/m/aj;->a:Lcom/a/b/m/ae;

    .line 2190
    iget-object v1, v1, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/a/b/m/ae;->f(Ljava/lang/reflect/Type;)Lcom/a/b/d/lo;

    move-result-object v1

    .line 664
    invoke-virtual {v0, v1}, Lcom/a/b/m/an;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 666
    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/a/b/m/aw;
    .registers 3

    .prologue
    .line 670
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "classes().interfaces() not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/a/b/m/aj;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
