.class final Lcom/a/b/m/bn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/reflect/ParameterizedType;


# static fields
.field private static final d:J


# instance fields
.field private final a:Ljava/lang/reflect/Type;

.field private final b:Lcom/a/b/d/jl;

.field private final c:Ljava/lang/Class;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)V
    .registers 6
    .param p1    # Ljava/lang/reflect/Type;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    array-length v0, p3

    invoke-virtual {p2}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_24

    const/4 v0, 0x1

    :goto_f
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 271
    const-string v0, "type parameter"

    invoke-static {p3, v0}, Lcom/a/b/m/ay;->a([Ljava/lang/reflect/Type;Ljava/lang/String;)V

    .line 272
    iput-object p1, p0, Lcom/a/b/m/bn;->a:Ljava/lang/reflect/Type;

    .line 273
    iput-object p2, p0, Lcom/a/b/m/bn;->c:Ljava/lang/Class;

    .line 274
    sget-object v0, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    invoke-virtual {v0, p3}, Lcom/a/b/m/bh;->a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/bn;->b:Lcom/a/b/d/jl;

    .line 275
    return-void

    .line 270
    :cond_24
    const/4 v0, 0x0

    goto :goto_f
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 307
    instance-of v1, p1, Ljava/lang/reflect/ParameterizedType;

    if-nez v1, :cond_6

    .line 311
    :cond_5
    :goto_5
    return v0

    .line 310
    :cond_6
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    .line 311
    invoke-virtual {p0}, Lcom/a/b/m/bn;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/a/b/m/bn;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/a/b/m/bn;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final getActualTypeArguments()[Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/a/b/m/bn;->b:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method public final getOwnerType()Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/a/b/m/bn;->a:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public final getRawType()Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/a/b/m/bn;->c:Ljava/lang/Class;

    return-object v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 302
    iget-object v0, p0, Lcom/a/b/m/bn;->a:Ljava/lang/reflect/Type;

    if-nez v0, :cond_14

    const/4 v0, 0x0

    :goto_5
    iget-object v1, p0, Lcom/a/b/m/bn;->b:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lcom/a/b/m/bn;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0

    :cond_14
    iget-object v0, p0, Lcom/a/b/m/bn;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_5
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 290
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 291
    iget-object v1, p0, Lcom/a/b/m/bn;->a:Ljava/lang/reflect/Type;

    if-eqz v1, :cond_1a

    .line 292
    sget-object v1, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    iget-object v2, p0, Lcom/a/b/m/bn;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v1, v2}, Lcom/a/b/m/bh;->c(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 294
    :cond_1a
    iget-object v1, p0, Lcom/a/b/m/bn;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/a/b/m/ay;->b()Lcom/a/b/b/bv;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/m/bn;->b:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/m/ay;->a()Lcom/a/b/b/bj;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/a/b/b/bv;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 298
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
