.class final Lcom/a/b/b/dh;
.super Lcom/a/b/b/ae;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/c;
    a = "no precomputation is done in GWT"
.end annotation


# static fields
.field static final s:I = 0x3ff

.field private static final w:I = -0x3361d2af

.field private static final x:I = 0x1b873593

.field private static final y:D = 0.5


# instance fields
.field private final t:[C

.field private final u:Z

.field private final v:J


# direct methods
.method constructor <init>([CJZLjava/lang/String;)V
    .registers 6

    .prologue
    .line 40
    invoke-direct {p0, p5}, Lcom/a/b/b/ae;-><init>(Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lcom/a/b/b/dh;->t:[C

    .line 42
    iput-wide p2, p0, Lcom/a/b/b/dh;->v:J

    .line 43
    iput-boolean p4, p0, Lcom/a/b/b/dh;->u:Z

    .line 44
    return-void
.end method

.method static a(I)I
    .registers 4

    .prologue
    .line 58
    const v0, 0x1b873593

    const v1, -0x3361d2af    # -8.2930312E7f

    mul-int/2addr v1, p0

    const/16 v2, 0xf

    invoke-static {v1, v2}, Ljava/lang/Integer;->rotateLeft(II)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method private static a(Ljava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;
    .registers 13

    .prologue
    const/4 v10, 0x0

    .line 92
    const-wide/16 v2, 0x0

    .line 93
    invoke-virtual {p0}, Ljava/util/BitSet;->cardinality()I

    move-result v1

    .line 94
    invoke-virtual {p0, v10}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    .line 1078
    const/4 v0, 0x1

    if-ne v1, v0, :cond_34

    .line 1079
    const/4 v0, 0x2

    .line 96
    :cond_f
    new-array v1, v0, [C

    .line 97
    array-length v0, v1

    add-int/lit8 v6, v0, -0x1

    .line 98
    invoke-virtual {p0, v10}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v5, v0

    :goto_19
    const/4 v0, -0x1

    if-eq v5, v0, :cond_4c

    .line 100
    const-wide/16 v8, 0x1

    shl-long/2addr v8, v5

    or-long/2addr v2, v8

    .line 101
    invoke-static {v5}, Lcom/a/b/b/dh;->a(I)I

    move-result v0

    and-int/2addr v0, v6

    .line 104
    :goto_25
    aget-char v7, v1, v0

    if-nez v7, :cond_48

    .line 105
    int-to-char v7, v5

    aput-char v7, v1, v0

    .line 98
    add-int/lit8 v0, v5, 0x1

    invoke-virtual {p0, v0}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v5, v0

    goto :goto_19

    .line 1083
    :cond_34
    add-int/lit8 v0, v1, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 1084
    :goto_3c
    int-to-double v6, v0

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v6, v8

    int-to-double v8, v1

    cmpg-double v5, v6, v8

    if-gez v5, :cond_f

    .line 1085
    shl-int/lit8 v0, v0, 0x1

    goto :goto_3c

    .line 109
    :cond_48
    add-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v6

    goto :goto_25

    .line 112
    :cond_4c
    new-instance v0, Lcom/a/b/b/dh;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/a/b/b/dh;-><init>([CJZLjava/lang/String;)V

    return-object v0
.end method

.method private b(I)Z
    .registers 6

    .prologue
    const-wide/16 v2, 0x1

    .line 62
    iget-wide v0, p0, Lcom/a/b/b/dh;->v:J

    shr-long/2addr v0, p1

    and-long/2addr v0, v2

    cmp-long v0, v2, v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private static c(I)I
    .registers 7
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 78
    const/4 v0, 0x1

    if-ne p0, v0, :cond_5

    .line 79
    const/4 v0, 0x2

    .line 87
    :cond_4
    return v0

    .line 83
    :cond_5
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 84
    :goto_d
    int-to-double v2, v0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    int-to-double v4, p0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_4

    .line 85
    shl-int/lit8 v0, v0, 0x1

    goto :goto_d
.end method


# virtual methods
.method final a(Ljava/util/BitSet;)V
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 144
    iget-boolean v1, p0, Lcom/a/b/b/dh;->u:Z

    if-eqz v1, :cond_8

    .line 145
    invoke-virtual {p1, v0}, Ljava/util/BitSet;->set(I)V

    .line 147
    :cond_8
    iget-object v1, p0, Lcom/a/b/b/dh;->t:[C

    array-length v2, v1

    :goto_b
    if-ge v0, v2, :cond_17

    aget-char v3, v1, v0

    .line 148
    if-eqz v3, :cond_14

    .line 149
    invoke-virtual {p1, v3}, Ljava/util/BitSet;->set(I)V

    .line 147
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 152
    :cond_17
    return-void
.end method

.method public final c(C)Z
    .registers 8

    .prologue
    const-wide/16 v4, 0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 117
    if-nez p1, :cond_9

    .line 118
    iget-boolean v0, p0, Lcom/a/b/b/dh;->u:Z

    .line 139
    :goto_8
    return v0

    .line 2062
    :cond_9
    iget-wide v0, p0, Lcom/a/b/b/dh;->v:J

    shr-long/2addr v0, p1

    and-long/2addr v0, v4

    cmp-long v0, v4, v0

    if-nez v0, :cond_16

    move v0, v3

    .line 120
    :goto_12
    if-nez v0, :cond_18

    move v0, v2

    .line 121
    goto :goto_8

    :cond_16
    move v0, v2

    .line 2062
    goto :goto_12

    .line 123
    :cond_18
    iget-object v0, p0, Lcom/a/b/b/dh;->t:[C

    array-length v0, v0

    add-int/lit8 v4, v0, -0x1

    .line 124
    invoke-static {p1}, Lcom/a/b/b/dh;->a(I)I

    move-result v0

    and-int v1, v0, v4

    move v0, v1

    .line 128
    :cond_24
    iget-object v5, p0, Lcom/a/b/b/dh;->t:[C

    aget-char v5, v5, v0

    if-nez v5, :cond_2c

    move v0, v2

    .line 129
    goto :goto_8

    .line 131
    :cond_2c
    iget-object v5, p0, Lcom/a/b/b/dh;->t:[C

    aget-char v5, v5, v0

    if-ne v5, p1, :cond_34

    move v0, v3

    .line 132
    goto :goto_8

    .line 135
    :cond_34
    add-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v4

    .line 138
    if-ne v0, v1, :cond_24

    move v0, v2

    .line 139
    goto :goto_8
.end method
