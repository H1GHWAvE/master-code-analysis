.class public final Lcom/a/b/b/cc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/a/b/b/cd;

.field private c:Lcom/a/b/b/cd;

.field private d:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v0, Lcom/a/b/b/cd;

    invoke-direct {v0, v1}, Lcom/a/b/b/cd;-><init>(B)V

    iput-object v0, p0, Lcom/a/b/b/cc;->b:Lcom/a/b/b/cd;

    .line 156
    iget-object v0, p0, Lcom/a/b/b/cc;->b:Lcom/a/b/b/cd;

    iput-object v0, p0, Lcom/a/b/b/cc;->c:Lcom/a/b/b/cd;

    .line 157
    iput-boolean v1, p0, Lcom/a/b/b/cc;->d:Z

    .line 163
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/b/b/cc;->a:Ljava/lang/String;

    .line 164
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;B)V
    .registers 3

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/a/b/b/cc;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a()Lcom/a/b/b/cc;
    .registers 2

    .prologue
    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/b/b/cc;->d:Z

    .line 175
    return-object p0
.end method

.method private a(C)Lcom/a/b/b/cc;
    .registers 3

    .prologue
    .line 283
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(D)Lcom/a/b/b/cc;
    .registers 4

    .prologue
    .line 295
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(F)Lcom/a/b/b/cc;
    .registers 3

    .prologue
    .line 307
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lcom/a/b/b/cc;
    .registers 3

    .prologue
    .line 319
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(J)Lcom/a/b/b/cc;
    .registers 4

    .prologue
    .line 331
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;C)Lcom/a/b/b/cc;
    .registers 4

    .prologue
    .line 205
    invoke-static {p2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;D)Lcom/a/b/b/cc;
    .registers 6

    .prologue
    .line 215
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;F)Lcom/a/b/b/cc;
    .registers 4

    .prologue
    .line 225
    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)Lcom/a/b/b/cc;
    .registers 4

    .prologue
    .line 195
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Lcom/a/b/b/cc;
    .registers 3

    .prologue
    .line 271
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Lcom/a/b/b/cc;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 257
    invoke-virtual {p0, p1}, Lcom/a/b/b/cc;->a(Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 185
    invoke-virtual {p0, p1, p2}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method private b()Lcom/a/b/b/cd;
    .registers 3

    .prologue
    .line 366
    new-instance v0, Lcom/a/b/b/cd;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/b/cd;-><init>(B)V

    .line 367
    iget-object v1, p0, Lcom/a/b/b/cc;->c:Lcom/a/b/b/cd;

    iput-object v0, v1, Lcom/a/b/b/cd;->c:Lcom/a/b/b/cd;

    iput-object v0, p0, Lcom/a/b/b/cc;->c:Lcom/a/b/b/cd;

    .line 368
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/a/b/b/cc;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 372
    invoke-direct {p0}, Lcom/a/b/b/cc;->b()Lcom/a/b/b/cd;

    move-result-object v0

    .line 373
    iput-object p1, v0, Lcom/a/b/b/cd;->b:Ljava/lang/Object;

    .line 374
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)Lcom/a/b/b/cc;
    .registers 4

    .prologue
    .line 235
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;J)Lcom/a/b/b/cc;
    .registers 6

    .prologue
    .line 245
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cc;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;
    .registers 5
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/a/b/b/cc;->b()Lcom/a/b/b/cd;

    move-result-object v1

    .line 379
    iput-object p2, v1, Lcom/a/b/b/cd;->b:Ljava/lang/Object;

    .line 380
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/a/b/b/cd;->a:Ljava/lang/String;

    .line 381
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .registers 8

    .prologue
    .line 346
    iget-boolean v2, p0, Lcom/a/b/b/cc;->d:Z

    .line 347
    const-string v1, ""

    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, Lcom/a/b/b/cc;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 350
    iget-object v0, p0, Lcom/a/b/b/cc;->b:Lcom/a/b/b/cd;

    iget-object v0, v0, Lcom/a/b/b/cd;->c:Lcom/a/b/b/cd;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_1e
    if-eqz v1, :cond_42

    .line 352
    if-eqz v2, :cond_26

    iget-object v4, v1, Lcom/a/b/b/cd;->b:Ljava/lang/Object;

    if-eqz v4, :cond_3f

    .line 353
    :cond_26
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    const-string v0, ", "

    .line 356
    iget-object v4, v1, Lcom/a/b/b/cd;->a:Ljava/lang/String;

    if-eqz v4, :cond_3a

    .line 357
    iget-object v4, v1, Lcom/a/b/b/cd;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 359
    :cond_3a
    iget-object v4, v1, Lcom/a/b/b/cd;->b:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 351
    :cond_3f
    iget-object v1, v1, Lcom/a/b/b/cd;->c:Lcom/a/b/b/cd;

    goto :goto_1e

    .line 362
    :cond_42
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
