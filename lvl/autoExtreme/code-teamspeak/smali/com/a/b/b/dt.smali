.class abstract Lcom/a/b/b/dt;
.super Lcom/a/b/b/b;
.source "SourceFile"


# instance fields
.field final c:Ljava/lang/CharSequence;

.field final d:Lcom/a/b/b/m;

.field final e:Z

.field f:I

.field g:I


# direct methods
.method protected constructor <init>(Lcom/a/b/b/di;Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 543
    invoke-direct {p0}, Lcom/a/b/b/b;-><init>()V

    .line 540
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/b/dt;->f:I

    .line 1103
    iget-object v0, p1, Lcom/a/b/b/di;->a:Lcom/a/b/b/m;

    .line 544
    iput-object v0, p0, Lcom/a/b/b/dt;->d:Lcom/a/b/b/m;

    .line 2103
    iget-boolean v0, p1, Lcom/a/b/b/di;->b:Z

    .line 545
    iput-boolean v0, p0, Lcom/a/b/b/dt;->e:Z

    .line 3103
    iget v0, p1, Lcom/a/b/b/di;->c:I

    .line 546
    iput v0, p0, Lcom/a/b/b/dt;->g:I

    .line 547
    iput-object p2, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    .line 548
    return-void
.end method

.method private c()Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v5, -0x1

    .line 557
    iget v0, p0, Lcom/a/b/b/dt;->f:I

    .line 558
    :cond_3
    :goto_3
    iget v1, p0, Lcom/a/b/b/dt;->f:I

    if-eq v1, v5, :cond_9b

    .line 562
    iget v1, p0, Lcom/a/b/b/dt;->f:I

    invoke-virtual {p0, v1}, Lcom/a/b/b/dt;->a(I)I

    move-result v1

    .line 563
    if-ne v1, v5, :cond_2e

    .line 564
    iget-object v1, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 565
    iput v5, p0, Lcom/a/b/b/dt;->f:I

    .line 570
    :goto_17
    iget v2, p0, Lcom/a/b/b/dt;->f:I

    if-ne v2, v0, :cond_a2

    .line 578
    iget v1, p0, Lcom/a/b/b/dt;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/b/b/dt;->f:I

    .line 579
    iget v1, p0, Lcom/a/b/b/dt;->f:I

    iget-object v2, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 580
    iput v5, p0, Lcom/a/b/b/dt;->f:I

    goto :goto_3

    .line 568
    :cond_2e
    invoke-virtual {p0, v1}, Lcom/a/b/b/dt;->b(I)I

    move-result v2

    iput v2, p0, Lcom/a/b/b/dt;->f:I

    goto :goto_17

    .line 585
    :goto_35
    if-ge v2, v1, :cond_a0

    iget-object v0, p0, Lcom/a/b/b/dt;->d:Lcom/a/b/b/m;

    iget-object v3, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Lcom/a/b/b/m;->c(C)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 586
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_35

    .line 588
    :goto_49
    if-le v0, v2, :cond_5f

    iget-object v1, p0, Lcom/a/b/b/dt;->d:Lcom/a/b/b/m;

    iget-object v3, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-eqz v1, :cond_5f

    .line 589
    add-int/lit8 v1, v0, -0x1

    move v0, v1

    goto :goto_49

    .line 592
    :cond_5f
    iget-boolean v1, p0, Lcom/a/b/b/dt;->e:Z

    if-eqz v1, :cond_68

    if-ne v2, v0, :cond_68

    .line 594
    iget v0, p0, Lcom/a/b/b/dt;->f:I

    goto :goto_3

    .line 598
    :cond_68
    iget v1, p0, Lcom/a/b/b/dt;->g:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_8a

    .line 602
    iget-object v0, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 603
    iput v5, p0, Lcom/a/b/b/dt;->f:I

    .line 605
    :goto_75
    if-le v0, v2, :cond_90

    iget-object v1, p0, Lcom/a/b/b/dt;->d:Lcom/a/b/b/m;

    iget-object v3, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-eqz v1, :cond_90

    .line 606
    add-int/lit8 v0, v0, -0x1

    goto :goto_75

    .line 609
    :cond_8a
    iget v1, p0, Lcom/a/b/b/dt;->g:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/a/b/b/dt;->g:I

    .line 612
    :cond_90
    iget-object v1, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v1, v2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 614
    :goto_9a
    return-object v0

    :cond_9b
    invoke-virtual {p0}, Lcom/a/b/b/dt;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    goto :goto_9a

    :cond_a0
    move v0, v1

    goto :goto_49

    :cond_a2
    move v2, v0

    goto :goto_35
.end method


# virtual methods
.method abstract a(I)I
.end method

.method protected final synthetic a()Ljava/lang/Object;
    .registers 7

    .prologue
    const/4 v5, -0x1

    .line 522
    .line 3557
    iget v0, p0, Lcom/a/b/b/dt;->f:I

    .line 3558
    :cond_3
    :goto_3
    iget v1, p0, Lcom/a/b/b/dt;->f:I

    if-eq v1, v5, :cond_9b

    .line 3562
    iget v1, p0, Lcom/a/b/b/dt;->f:I

    invoke-virtual {p0, v1}, Lcom/a/b/b/dt;->a(I)I

    move-result v1

    .line 3563
    if-ne v1, v5, :cond_2e

    .line 3564
    iget-object v1, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 3565
    iput v5, p0, Lcom/a/b/b/dt;->f:I

    .line 3570
    :goto_17
    iget v2, p0, Lcom/a/b/b/dt;->f:I

    if-ne v2, v0, :cond_a2

    .line 3578
    iget v1, p0, Lcom/a/b/b/dt;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/b/b/dt;->f:I

    .line 3579
    iget v1, p0, Lcom/a/b/b/dt;->f:I

    iget-object v2, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 3580
    iput v5, p0, Lcom/a/b/b/dt;->f:I

    goto :goto_3

    .line 3568
    :cond_2e
    invoke-virtual {p0, v1}, Lcom/a/b/b/dt;->b(I)I

    move-result v2

    iput v2, p0, Lcom/a/b/b/dt;->f:I

    goto :goto_17

    .line 3585
    :goto_35
    if-ge v2, v1, :cond_a0

    iget-object v0, p0, Lcom/a/b/b/dt;->d:Lcom/a/b/b/m;

    iget-object v3, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Lcom/a/b/b/m;->c(C)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 3586
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_35

    .line 3588
    :goto_49
    if-le v0, v2, :cond_5f

    iget-object v1, p0, Lcom/a/b/b/dt;->d:Lcom/a/b/b/m;

    iget-object v3, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-eqz v1, :cond_5f

    .line 3589
    add-int/lit8 v1, v0, -0x1

    move v0, v1

    goto :goto_49

    .line 3592
    :cond_5f
    iget-boolean v1, p0, Lcom/a/b/b/dt;->e:Z

    if-eqz v1, :cond_68

    if-ne v2, v0, :cond_68

    .line 3594
    iget v0, p0, Lcom/a/b/b/dt;->f:I

    goto :goto_3

    .line 3598
    :cond_68
    iget v1, p0, Lcom/a/b/b/dt;->g:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_8a

    .line 3602
    iget-object v0, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 3603
    iput v5, p0, Lcom/a/b/b/dt;->f:I

    .line 3605
    :goto_75
    if-le v0, v2, :cond_90

    iget-object v1, p0, Lcom/a/b/b/dt;->d:Lcom/a/b/b/m;

    iget-object v3, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-eqz v1, :cond_90

    .line 3606
    add-int/lit8 v0, v0, -0x1

    goto :goto_75

    .line 3609
    :cond_8a
    iget v1, p0, Lcom/a/b/b/dt;->g:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/a/b/b/dt;->g:I

    .line 3612
    :cond_90
    iget-object v1, p0, Lcom/a/b/b/dt;->c:Ljava/lang/CharSequence;

    invoke-interface {v1, v2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_9a
    return-object v0

    .line 3614
    :cond_9b
    invoke-virtual {p0}, Lcom/a/b/b/dt;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 522
    goto :goto_9a

    :cond_a0
    move v0, v1

    goto :goto_49

    :cond_a2
    move v2, v0

    goto :goto_35
.end method

.method abstract b(I)I
.end method
