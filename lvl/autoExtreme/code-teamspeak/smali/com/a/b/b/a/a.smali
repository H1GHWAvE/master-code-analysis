.class public Lcom/a/b/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static final b:Ljava/lang/String; = "com.google.common.base.FinalizableReference"

.field private static final f:Ljava/lang/reflect/Field;


# instance fields
.field private final c:Ljava/lang/ref/WeakReference;

.field private final d:Ljava/lang/ref/PhantomReference;

.field private final e:Ljava/lang/ref/ReferenceQueue;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 51
    const-class v0, Lcom/a/b/b/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/a/b/b/a/a;->a:Ljava/util/logging/Logger;

    .line 106
    invoke-static {}, Lcom/a/b/b/a/a;->b()Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/a/b/b/a/a;->f:Ljava/lang/reflect/Field;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;Ljava/lang/ref/ReferenceQueue;Ljava/lang/ref/PhantomReference;)V
    .registers 5

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p2, p0, Lcom/a/b/b/a/a;->e:Ljava/lang/ref/ReferenceQueue;

    .line 116
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/a/b/b/a/a;->c:Ljava/lang/ref/WeakReference;

    .line 120
    iput-object p3, p0, Lcom/a/b/b/a/a;->d:Ljava/lang/ref/PhantomReference;

    .line 121
    return-void
.end method

.method private a()Ljava/lang/reflect/Method;
    .registers 4

    .prologue
    .line 181
    iget-object v0, p0, Lcom/a/b/b/a/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 183
    if-nez v0, :cond_c

    .line 192
    const/4 v0, 0x0

    .line 195
    :goto_b
    return-object v0

    :cond_c
    :try_start_c
    const-string v1, "finalizeReferent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_14
    .catch Ljava/lang/NoSuchMethodException; {:try_start_c .. :try_end_14} :catch_16

    move-result-object v0

    goto :goto_b

    .line 196
    :catch_16
    move-exception v0

    .line 197
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/ref/ReferenceQueue;Ljava/lang/ref/PhantomReference;)V
    .registers 8

    .prologue
    .line 80
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.common.base.FinalizableReference"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected com.google.common.base.FinalizableReference."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_14
    new-instance v0, Lcom/a/b/b/a/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/b/a/a;-><init>(Ljava/lang/Class;Ljava/lang/ref/ReferenceQueue;Ljava/lang/ref/PhantomReference;)V

    .line 86
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 87
    const-class v0, Lcom/a/b/b/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 88
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 91
    :try_start_2b
    sget-object v0, Lcom/a/b/b/a/a;->f:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_35

    .line 92
    sget-object v0, Lcom/a/b/b/a/a;->f:Ljava/lang/reflect/Field;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_35} :catch_39

    .line 99
    :cond_35
    :goto_35
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 100
    return-void

    .line 94
    :catch_39
    move-exception v0

    .line 95
    sget-object v2, Lcom/a/b/b/a/a;->a:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v4, "Failed to clear thread local values inherited by reference finalizer thread."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_35
.end method

.method private a(Ljava/lang/ref/Reference;)Z
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 144
    invoke-direct {p0}, Lcom/a/b/b/a/a;->a()Ljava/lang/reflect/Method;

    move-result-object v2

    .line 145
    if-nez v2, :cond_8

    .line 174
    :cond_7
    :goto_7
    return v0

    .line 153
    :cond_8
    invoke-virtual {p1}, Ljava/lang/ref/Reference;->clear()V

    .line 155
    iget-object v1, p0, Lcom/a/b/b/a/a;->d:Ljava/lang/ref/PhantomReference;

    if-eq p1, v1, :cond_7

    .line 164
    const/4 v1, 0x0

    :try_start_10
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_15} :catch_1f

    .line 173
    :goto_15
    iget-object v1, p0, Lcom/a/b/b/a/a;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v1}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object p1

    if-nez p1, :cond_8

    .line 174
    const/4 v0, 0x1

    goto :goto_7

    .line 165
    :catch_1f
    move-exception v1

    .line 166
    sget-object v3, Lcom/a/b/b/a/a;->a:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "Error cleaning up after reference."

    invoke-virtual {v3, v4, v5, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_15
.end method

.method private static b()Ljava/lang/reflect/Field;
    .registers 3

    .prologue
    .line 203
    :try_start_0
    const-class v0, Ljava/lang/Thread;

    const-string v1, "inheritableThreadLocals"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 205
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_c} :catch_d

    .line 211
    :goto_c
    return-object v0

    .line 208
    :catch_d
    move-exception v0

    sget-object v0, Lcom/a/b/b/a/a;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Couldn\'t access Thread.inheritableThreadLocals. Reference finalizer threads will inherit thread local values."

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 211
    const/4 v0, 0x0

    goto :goto_c
.end method


# virtual methods
.method public run()V
    .registers 2

    .prologue
    .line 131
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/a/b/b/a/a;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->remove()Ljava/lang/ref/Reference;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/a/a;->a(Ljava/lang/ref/Reference;)Z
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_9} :catch_d

    move-result v0

    if-nez v0, :cond_0

    .line 132
    return-void

    .line 134
    :catch_d
    move-exception v0

    goto :goto_0
.end method
