.class final Lcom/a/b/b/ao;
.super Lcom/a/b/b/ak;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Lcom/a/b/b/bj;

.field private final b:Lcom/a/b/b/bj;


# direct methods
.method private constructor <init>(Lcom/a/b/b/bj;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 415
    invoke-direct {p0}, Lcom/a/b/b/ak;-><init>()V

    .line 416
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/b/ao;->a:Lcom/a/b/b/bj;

    .line 417
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/b/ao;->b:Lcom/a/b/b/bj;

    .line 418
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/b/bj;Lcom/a/b/b/bj;B)V
    .registers 4

    .prologue
    .line 408
    invoke-direct {p0, p1, p2}, Lcom/a/b/b/ao;-><init>(Lcom/a/b/b/bj;Lcom/a/b/b/bj;)V

    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 427
    iget-object v0, p0, Lcom/a/b/b/ao;->b:Lcom/a/b/b/bj;

    invoke-interface {v0, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 422
    iget-object v0, p0, Lcom/a/b/b/ao;->a:Lcom/a/b/b/bj;

    invoke-interface {v0, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 432
    instance-of v1, p1, Lcom/a/b/b/ao;

    if-eqz v1, :cond_1c

    .line 433
    check-cast p1, Lcom/a/b/b/ao;

    .line 434
    iget-object v1, p0, Lcom/a/b/b/ao;->a:Lcom/a/b/b/bj;

    iget-object v2, p1, Lcom/a/b/b/ao;->a:Lcom/a/b/b/bj;

    invoke-interface {v1, v2}, Lcom/a/b/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/a/b/b/ao;->b:Lcom/a/b/b/bj;

    iget-object v2, p1, Lcom/a/b/b/ao;->b:Lcom/a/b/b/bj;

    invoke-interface {v1, v2}, Lcom/a/b/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    .line 437
    :cond_1c
    return v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 442
    iget-object v0, p0, Lcom/a/b/b/ao;->a:Lcom/a/b/b/bj;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/a/b/b/ao;->b:Lcom/a/b/b/bj;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 447
    iget-object v0, p0, Lcom/a/b/b/ao;->a:Lcom/a/b/b/bj;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/b/ao;->b:Lcom/a/b/b/bj;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Converter.from("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
