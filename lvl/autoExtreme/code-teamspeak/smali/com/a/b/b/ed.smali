.class final Lcom/a/b/b/ed;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/dz;
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field final a:Lcom/a/b/b/bj;

.field final b:Lcom/a/b/b/dz;


# direct methods
.method constructor <init>(Lcom/a/b/b/bj;Lcom/a/b/b/dz;)V
    .registers 3

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/a/b/b/ed;->a:Lcom/a/b/b/bj;

    .line 63
    iput-object p2, p0, Lcom/a/b/b/ed;->b:Lcom/a/b/b/dz;

    .line 64
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/a/b/b/ed;->a:Lcom/a/b/b/bj;

    iget-object v1, p0, Lcom/a/b/b/ed;->b:Lcom/a/b/b/dz;

    invoke-interface {v1}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 71
    instance-of v1, p1, Lcom/a/b/b/ed;

    if-eqz v1, :cond_1c

    .line 72
    check-cast p1, Lcom/a/b/b/ed;

    .line 73
    iget-object v1, p0, Lcom/a/b/b/ed;->a:Lcom/a/b/b/bj;

    iget-object v2, p1, Lcom/a/b/b/ed;->a:Lcom/a/b/b/bj;

    invoke-interface {v1, v2}, Lcom/a/b/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/a/b/b/ed;->b:Lcom/a/b/b/dz;

    iget-object v2, p1, Lcom/a/b/b/ed;->b:Lcom/a/b/b/dz;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    .line 75
    :cond_1c
    return v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 79
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/b/b/ed;->a:Lcom/a/b/b/bj;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/a/b/b/ed;->b:Lcom/a/b/b/dz;

    aput-object v2, v0, v1

    .line 1084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 79
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 83
    iget-object v0, p0, Lcom/a/b/b/ed;->a:Lcom/a/b/b/bj;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/b/ed;->b:Lcom/a/b/b/dz;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Suppliers.compose("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
