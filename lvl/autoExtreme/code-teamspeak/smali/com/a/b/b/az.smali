.class public final Lcom/a/b/b/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field private final a:Lcom/a/b/b/au;

.field private final b:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/a/b/b/au;Ljava/lang/Object;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, p0, Lcom/a/b/b/az;->a:Lcom/a/b/b/au;

    .line 178
    iput-object p2, p0, Lcom/a/b/b/az;->b:Ljava/lang/Object;

    .line 179
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/b/au;Ljava/lang/Object;B)V
    .registers 4

    .prologue
    .line 172
    invoke-direct {p0, p1, p2}, Lcom/a/b/b/az;-><init>(Lcom/a/b/b/au;Ljava/lang/Object;)V

    return-void
.end method

.method private a()Ljava/lang/Object;
    .registers 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/a/b/b/az;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 192
    if-ne p1, p0, :cond_4

    .line 193
    const/4 v0, 0x1

    .line 208
    :goto_3
    return v0

    .line 195
    :cond_4
    instance-of v0, p1, Lcom/a/b/b/az;

    if-eqz v0, :cond_1f

    .line 196
    check-cast p1, Lcom/a/b/b/az;

    .line 198
    iget-object v0, p0, Lcom/a/b/b/az;->a:Lcom/a/b/b/au;

    iget-object v1, p1, Lcom/a/b/b/az;->a:Lcom/a/b/b/au;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 204
    iget-object v0, p0, Lcom/a/b/b/az;->a:Lcom/a/b/b/au;

    .line 205
    iget-object v1, p0, Lcom/a/b/b/az;->b:Ljava/lang/Object;

    iget-object v2, p1, Lcom/a/b/b/az;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 208
    :cond_1f
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 215
    iget-object v0, p0, Lcom/a/b/b/az;->a:Lcom/a/b/b/au;

    iget-object v1, p0, Lcom/a/b/b/az;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/a/b/b/au;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 223
    iget-object v0, p0, Lcom/a/b/b/az;->a:Lcom/a/b/b/au;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/b/az;->b:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".wrap("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
