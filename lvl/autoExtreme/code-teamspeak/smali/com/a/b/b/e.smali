.class public final Lcom/a/b/b/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field public static final A:B = 0x17t

.field public static final B:B = 0x18t

.field public static final C:B = 0x19t

.field public static final D:B = 0x1at

.field public static final E:B = 0x1bt

.field public static final F:B = 0x1ct

.field public static final G:B = 0x1dt

.field public static final H:B = 0x1et

.field public static final I:B = 0x1ft

.field public static final J:B = 0x20t

.field public static final K:B = 0x20t

.field public static final L:B = 0x7ft

.field public static final M:C = '\u0000'

.field public static final N:C = '\u007f'

.field public static final a:B = 0x0t

.field public static final b:B = 0x1t

.field public static final c:B = 0x2t

.field public static final d:B = 0x3t

.field public static final e:B = 0x4t

.field public static final f:B = 0x5t

.field public static final g:B = 0x6t

.field public static final h:B = 0x7t

.field public static final i:B = 0x8t

.field public static final j:B = 0x9t

.field public static final k:B = 0xat

.field public static final l:B = 0xat

.field public static final m:B = 0xbt

.field public static final n:B = 0xct

.field public static final o:B = 0xdt

.field public static final p:B = 0xet

.field public static final q:B = 0xft

.field public static final r:B = 0x10t

.field public static final s:B = 0x11t

.field public static final t:B = 0x11t

.field public static final u:B = 0x12t

.field public static final v:B = 0x13t

.field public static final w:B = 0x13t

.field public static final x:B = 0x14t

.field public static final y:B = 0x15t

.field public static final z:B = 0x16t


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(C)C
    .registers 2

    .prologue
    .line 478
    invoke-static {p0}, Lcom/a/b/b/e;->d(C)Z

    move-result v0

    if-eqz v0, :cond_9

    xor-int/lit8 v0, p0, 0x20

    int-to-char p0, v0

    :cond_9
    return p0
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 462
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 463
    check-cast p0, Ljava/lang/String;

    invoke-static {p0}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 470
    :goto_a
    return-object v0

    .line 465
    :cond_b
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 466
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 467
    const/4 v0, 0x0

    :goto_15
    if-ge v0, v1, :cond_25

    .line 468
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/a/b/b/e;->a(C)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 467
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 470
    :cond_25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method

.method public static a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v7, 0x1e

    .line 585
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 588
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    rsub-int/lit8 v3, v0, 0x1e

    .line 592
    if-ltz v3, :cond_39

    move v0, v1

    :goto_10
    const-string v4, "maxLength (%s) must be >= length of the truncation indicator (%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v0, v4, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 596
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gt v0, v7, :cond_3b

    .line 597
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    .line 598
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v7, :cond_3b

    .line 605
    :goto_38
    return-object p0

    :cond_39
    move v0, v2

    .line 592
    goto :goto_10

    .line 605
    :cond_3b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0, v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_38
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    .prologue
    .line 438
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 439
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_2b

    .line 440
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/a/b/b/e;->d(C)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 441
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 442
    :goto_15
    if-ge v0, v1, :cond_27

    .line 443
    aget-char v3, v2, v0

    .line 444
    invoke-static {v3}, Lcom/a/b/b/e;->d(C)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 445
    xor-int/lit8 v3, v3, 0x20

    int-to-char v3, v3

    aput-char v3, v2, v0

    .line 442
    :cond_24
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 448
    :cond_27
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object p0

    .line 451
    :cond_2b
    return-object p0

    .line 439
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .registers 9
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 634
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 635
    if-ne p0, p1, :cond_9

    .line 655
    :cond_8
    :goto_8
    return v0

    .line 638
    :cond_9
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-eq v3, v2, :cond_11

    move v0, v1

    .line 639
    goto :goto_8

    :cond_11
    move v2, v1

    .line 641
    :goto_12
    if-ge v2, v3, :cond_8

    .line 642
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 643
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 644
    if-eq v4, v5, :cond_2e

    .line 647
    invoke-static {v4}, Lcom/a/b/b/e;->e(C)I

    move-result v4

    .line 650
    const/16 v6, 0x1a

    if-ge v4, v6, :cond_2c

    invoke-static {v5}, Lcom/a/b/b/e;->e(C)I

    move-result v5

    if-eq v4, v5, :cond_2e

    :cond_2c
    move v0, v1

    .line 653
    goto :goto_8

    .line 641
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    goto :goto_12
.end method

.method public static b(C)C
    .registers 2

    .prologue
    .line 527
    invoke-static {p0}, Lcom/a/b/b/e;->c(C)Z

    move-result v0

    if-eqz v0, :cond_9

    and-int/lit8 v0, p0, 0x5f

    int-to-char p0, v0

    :cond_9
    return p0
.end method

.method private static b(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 511
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 512
    check-cast p0, Ljava/lang/String;

    invoke-static {p0}, Lcom/a/b/b/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 519
    :goto_a
    return-object v0

    .line 514
    :cond_b
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 515
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 516
    const/4 v0, 0x0

    :goto_15
    if-ge v0, v1, :cond_25

    .line 517
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/a/b/b/e;->b(C)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 516
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 519
    :cond_25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    .prologue
    .line 487
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 488
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_2b

    .line 489
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/a/b/b/e;->c(C)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 490
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 491
    :goto_15
    if-ge v0, v1, :cond_27

    .line 492
    aget-char v3, v2, v0

    .line 493
    invoke-static {v3}, Lcom/a/b/b/e;->c(C)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 494
    and-int/lit8 v3, v3, 0x5f

    int-to-char v3, v3

    aput-char v3, v2, v0

    .line 491
    :cond_24
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 497
    :cond_27
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object p0

    .line 500
    :cond_2b
    return-object p0

    .line 488
    :cond_2c
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public static c(C)Z
    .registers 2

    .prologue
    .line 538
    const/16 v0, 0x61

    if-lt p0, v0, :cond_a

    const/16 v0, 0x7a

    if-gt p0, v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public static d(C)Z
    .registers 2

    .prologue
    .line 547
    const/16 v0, 0x41

    if-lt p0, v0, :cond_a

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private static e(C)I
    .registers 2

    .prologue
    .line 665
    or-int/lit8 v0, p0, 0x20

    add-int/lit8 v0, v0, -0x61

    int-to-char v0, v0

    return v0
.end method
