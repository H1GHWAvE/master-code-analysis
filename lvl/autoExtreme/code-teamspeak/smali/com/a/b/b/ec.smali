.class final Lcom/a/b/b/ec;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/dz;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final a:Lcom/a/b/b/dz;

.field volatile transient b:Z

.field transient c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/a/b/b/dz;)V
    .registers 2

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/a/b/b/ec;->a:Lcom/a/b/b/dz;

    .line 118
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/a/b/b/ec;->b:Z

    if-nez v0, :cond_17

    .line 123
    monitor-enter p0

    .line 124
    :try_start_5
    iget-boolean v0, p0, Lcom/a/b/b/ec;->b:Z

    if-nez v0, :cond_16

    .line 125
    iget-object v0, p0, Lcom/a/b/b/ec;->a:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    .line 126
    iput-object v0, p0, Lcom/a/b/b/ec;->c:Ljava/lang/Object;

    .line 127
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/a/b/b/ec;->b:Z

    .line 128
    monitor-exit p0

    .line 132
    :goto_15
    return-object v0

    .line 130
    :cond_16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_1a

    .line 132
    :cond_17
    iget-object v0, p0, Lcom/a/b/b/ec;->c:Ljava/lang/Object;

    goto :goto_15

    .line 130
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 136
    iget-object v0, p0, Lcom/a/b/b/ec;->a:Lcom/a/b/b/dz;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Suppliers.memoize("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
