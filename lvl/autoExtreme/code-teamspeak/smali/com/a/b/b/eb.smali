.class final Lcom/a/b/b/eb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/dz;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# static fields
.field private static final e:J


# instance fields
.field final a:Lcom/a/b/b/dz;

.field final b:J

.field volatile transient c:Ljava/lang/Object;

.field volatile transient d:J


# direct methods
.method constructor <init>(Lcom/a/b/b/dz;JLjava/util/concurrent/TimeUnit;)V
    .registers 7

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/b/eb;->a:Lcom/a/b/b/dz;

    .line 176
    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/b/eb;->b:J

    .line 177
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_1c

    const/4 v0, 0x1

    :goto_18
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 178
    return-void

    .line 177
    :cond_1c
    const/4 v0, 0x0

    goto :goto_18
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 9

    .prologue
    const-wide/16 v6, 0x0

    .line 187
    iget-wide v0, p0, Lcom/a/b/b/eb;->d:J

    .line 1034
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 189
    cmp-long v4, v0, v6

    if-eqz v4, :cond_12

    sub-long v4, v2, v0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2f

    .line 190
    :cond_12
    monitor-enter p0

    .line 191
    :try_start_13
    iget-wide v4, p0, Lcom/a/b/b/eb;->d:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_2e

    .line 192
    iget-object v0, p0, Lcom/a/b/b/eb;->a:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    .line 193
    iput-object v0, p0, Lcom/a/b/b/eb;->c:Ljava/lang/Object;

    .line 194
    iget-wide v4, p0, Lcom/a/b/b/eb;->b:J

    add-long/2addr v2, v4

    .line 197
    cmp-long v1, v2, v6

    if-nez v1, :cond_2a

    const-wide/16 v2, 0x1

    :cond_2a
    iput-wide v2, p0, Lcom/a/b/b/eb;->d:J

    .line 198
    monitor-exit p0

    .line 202
    :goto_2d
    return-object v0

    .line 200
    :cond_2e
    monitor-exit p0
    :try_end_2f
    .catchall {:try_start_13 .. :try_end_2f} :catchall_32

    .line 202
    :cond_2f
    iget-object v0, p0, Lcom/a/b/b/eb;->c:Ljava/lang/Object;

    goto :goto_2d

    .line 200
    :catchall_32
    move-exception v0

    :try_start_33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 208
    iget-object v0, p0, Lcom/a/b/b/eb;->a:Lcom/a/b/b/dz;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/a/b/b/eb;->b:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3e

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Suppliers.memoizeWithExpiration("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", NANOS)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
