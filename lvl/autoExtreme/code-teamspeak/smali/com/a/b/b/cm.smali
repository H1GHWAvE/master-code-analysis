.class final Lcom/a/b/b/cm;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()J
    .registers 2

    .prologue
    .line 34
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/b/ci;
    .registers 3

    .prologue
    .line 42
    invoke-static {p0}, Lcom/a/b/b/as;->a(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 43
    if-nez v0, :cond_11

    invoke-static {}, Lcom/a/b/b/ci;->f()Lcom/a/b/b/ci;

    move-result-object v0

    :goto_10
    return-object v0

    :cond_11
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ci;->b(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_10
.end method

.method private static a(Lcom/a/b/b/m;)Lcom/a/b/b/m;
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/high16 v3, 0x10000

    .line 1792
    new-instance v2, Ljava/util/BitSet;

    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    .line 1793
    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->a(Ljava/util/BitSet;)V

    .line 1794
    invoke-virtual {v2}, Ljava/util/BitSet;->cardinality()I

    move-result v0

    .line 1795
    mul-int/lit8 v1, v0, 0x2

    if-gt v1, v3, :cond_1a

    .line 1796
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/a/b/b/m;->a(ILjava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    :goto_19
    return-object v0

    .line 1799
    :cond_1a
    invoke-virtual {v2, v5, v3}, Ljava/util/BitSet;->flip(II)V

    .line 1800
    sub-int/2addr v3, v0

    .line 1801
    const-string v0, ".negate()"

    .line 1802
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_49

    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1805
    :goto_3a
    new-instance v1, Lcom/a/b/b/af;

    invoke-virtual {p0}, Lcom/a/b/b/m;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v0}, Lcom/a/b/b/m;->a(ILjava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lcom/a/b/b/af;-><init>(Ljava/lang/String;Lcom/a/b/b/m;)V

    move-object v0, v1

    .line 38
    goto :goto_19

    .line 1802
    :cond_49
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5e

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3a

    :cond_5e
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3a
.end method
