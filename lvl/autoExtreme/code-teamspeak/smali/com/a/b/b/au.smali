.class public abstract Lcom/a/b/b/au;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/a/b/b/au;
    .registers 2
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 244
    new-instance v0, Lcom/a/b/b/cl;

    invoke-direct {v0, p0}, Lcom/a/b/b/cl;-><init>(Lcom/a/b/b/au;)V

    return-object v0
.end method

.method private a(Lcom/a/b/b/bj;)Lcom/a/b/b/au;
    .registers 3

    .prologue
    .line 140
    new-instance v0, Lcom/a/b/b/bk;

    invoke-direct {v0, p1, p0}, Lcom/a/b/b/bk;-><init>(Lcom/a/b/b/bj;Lcom/a/b/b/au;)V

    return-object v0
.end method

.method private static b()Lcom/a/b/b/au;
    .registers 1

    .prologue
    .line 306
    sget-object v0, Lcom/a/b/b/aw;->a:Lcom/a/b/b/aw;

    return-object v0
.end method

.method private static c()Lcom/a/b/b/au;
    .registers 1

    .prologue
    .line 318
    sget-object v0, Lcom/a/b/b/ay;->a:Lcom/a/b/b/ay;

    return-object v0
.end method

.method private c(Ljava/lang/Object;)Lcom/a/b/b/az;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 151
    new-instance v0, Lcom/a/b/b/az;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/b/az;-><init>(Lcom/a/b/b/au;Ljava/lang/Object;B)V

    return-object v0
.end method

.method private d(Ljava/lang/Object;)Lcom/a/b/b/co;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 255
    new-instance v0, Lcom/a/b/b/ax;

    invoke-direct {v0, p0, p1}, Lcom/a/b/b/ax;-><init>(Lcom/a/b/b/au;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 101
    if-nez p1, :cond_4

    .line 102
    const/4 v0, 0x0

    .line 104
    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/b/au;->b(Ljava/lang/Object;)I

    move-result v0

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 65
    if-ne p1, p2, :cond_4

    .line 66
    const/4 v0, 0x1

    .line 71
    :goto_3
    return v0

    .line 68
    :cond_4
    if-eqz p1, :cond_8

    if-nez p2, :cond_a

    .line 69
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 71
    :cond_a
    invoke-virtual {p0, p1, p2}, Lcom/a/b/b/au;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3
.end method

.method protected abstract b(Ljava/lang/Object;)I
.end method

.method protected abstract b(Ljava/lang/Object;Ljava/lang/Object;)Z
.end method
