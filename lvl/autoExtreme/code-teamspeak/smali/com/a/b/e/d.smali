.class public abstract Lcom/a/b/e/d;
.super Lcom/a/b/e/g;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:I = 0x2


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/a/b/e/g;-><init>()V

    return-void
.end method

.method private static a([CII)[C
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 163
    new-array v0, p2, [C

    .line 164
    if-lez p1, :cond_8

    .line 165
    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 167
    :cond_8
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 59
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 62
    const/4 v0, 0x0

    :goto_8
    if-ge v0, v1, :cond_18

    .line 63
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/b/e/d;->a(C)[C

    move-result-object v2

    if-eqz v2, :cond_19

    .line 64
    invoke-virtual {p0, p1, v0}, Lcom/a/b/e/d;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    .line 67
    :cond_18
    return-object p1

    .line 62
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_8
.end method

.method protected final a(Ljava/lang/String;I)Ljava/lang/String;
    .registers 14

    .prologue
    const/4 v5, 0x0

    .line 82
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    .line 85
    invoke-static {}, Lcom/a/b/e/n;->a()[C

    move-result-object v3

    .line 86
    array-length v2, v3

    move v0, v5

    move v1, v5

    .line 92
    :goto_c
    if-ge p2, v6, :cond_3e

    .line 95
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {p0, v4}, Lcom/a/b/e/d;->a(C)[C

    move-result-object v4

    .line 98
    if-eqz v4, :cond_3b

    .line 100
    array-length v7, v4

    .line 101
    sub-int v8, p2, v0

    .line 106
    add-int v9, v1, v8

    add-int/2addr v9, v7

    .line 107
    if-ge v2, v9, :cond_29

    .line 108
    sub-int v2, v6, p2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v9

    .line 109
    invoke-static {v3, v1, v2}, Lcom/a/b/e/d;->a([CII)[C

    move-result-object v3

    .line 113
    :cond_29
    if-lez v8, :cond_53

    .line 114
    invoke-virtual {p1, v0, p2, v3, v1}, Ljava/lang/String;->getChars(II[CI)V

    .line 115
    add-int v0, v1, v8

    .line 119
    :goto_30
    if-lez v7, :cond_36

    .line 120
    invoke-static {v4, v5, v3, v0, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 121
    add-int/2addr v0, v7

    .line 123
    :cond_36
    add-int/lit8 v1, p2, 0x1

    move v10, v1

    move v1, v0

    move v0, v10

    .line 92
    :cond_3b
    add-int/lit8 p2, p2, 0x1

    goto :goto_c

    .line 127
    :cond_3e
    sub-int v4, v6, v0

    .line 128
    if-lez v4, :cond_4d

    .line 129
    add-int/2addr v4, v1

    .line 130
    if-ge v2, v4, :cond_49

    .line 133
    invoke-static {v3, v1, v4}, Lcom/a/b/e/d;->a([CII)[C

    move-result-object v3

    .line 135
    :cond_49
    invoke-virtual {p1, v0, v6, v3, v1}, Ljava/lang/String;->getChars(II[CI)V

    move v1, v4

    .line 138
    :cond_4d
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3, v5, v1}, Ljava/lang/String;-><init>([CII)V

    return-object v0

    :cond_53
    move v0, v1

    goto :goto_30
.end method

.method protected abstract a(C)[C
.end method
