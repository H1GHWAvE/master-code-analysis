.class final Lcom/a/b/n/a/dc;
.super Lcom/a/b/n/a/df;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/CancellationException;


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/n/a/df;-><init>(B)V

    .line 199
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "Immediate cancelled future."

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/a/b/n/a/dc;->a:Ljava/util/concurrent/CancellationException;

    .line 200
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 209
    const-string v0, "Task was cancelled."

    iget-object v1, p0, Lcom/a/b/n/a/dc;->a:Ljava/util/concurrent/CancellationException;

    invoke-static {v0, v1}, Lcom/a/b/n/a/g;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    throw v0
.end method

.method public final isCancelled()Z
    .registers 2

    .prologue
    .line 204
    const/4 v0, 0x1

    return v0
.end method
