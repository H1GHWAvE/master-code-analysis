.class public final Lcom/a/b/n/a/dy;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/n/a/du;Ljava/util/concurrent/Callable;Ljava/util/concurrent/BlockingQueue;)Lcom/a/b/n/a/dp;
    .registers 6

    .prologue
    .line 748
    invoke-interface {p0, p1}, Lcom/a/b/n/a/du;->a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    .line 749
    new-instance v1, Lcom/a/b/n/a/dz;

    invoke-direct {v1, p2, v0}, Lcom/a/b/n/a/dz;-><init>(Ljava/util/concurrent/BlockingQueue;Lcom/a/b/n/a/dp;)V

    .line 2450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 749
    invoke-interface {v0, v1, v2}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 754
    return-object v0
.end method

.method private static a(Ljava/util/concurrent/ExecutorService;)Lcom/a/b/n/a/du;
    .registers 2

    .prologue
    .line 481
    instance-of v0, p0, Lcom/a/b/n/a/du;

    if-eqz v0, :cond_7

    check-cast p0, Lcom/a/b/n/a/du;

    :goto_6
    return-object p0

    :cond_7
    instance-of v0, p0, Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_14

    new-instance v0, Lcom/a/b/n/a/ei;

    check-cast p0, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/ei;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    move-object p0, v0

    goto :goto_6

    :cond_14
    new-instance v0, Lcom/a/b/n/a/eh;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/eh;-><init>(Ljava/util/concurrent/ExecutorService;)V

    move-object p0, v0

    goto :goto_6
.end method

.method private static a(Ljava/util/concurrent/ScheduledExecutorService;)Lcom/a/b/n/a/dv;
    .registers 2

    .prologue
    .line 509
    instance-of v0, p0, Lcom/a/b/n/a/dv;

    if-eqz v0, :cond_7

    check-cast p0, Lcom/a/b/n/a/dv;

    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Lcom/a/b/n/a/ei;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/ei;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    move-object p0, v0

    goto :goto_6
.end method

.method private static a(Lcom/a/b/n/a/du;Ljava/util/Collection;ZJ)Ljava/lang/Object;
    .registers 24

    .prologue
    .line 675
    invoke-static/range {p0 .. p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v3

    .line 677
    if-lez v3, :cond_76

    const/4 v2, 0x1

    :goto_a
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Z)V

    .line 678
    invoke-static {v3}, Lcom/a/b/d/ov;->a(I)Ljava/util/ArrayList;

    move-result-object v12

    .line 2146
    new-instance v13, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v13}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 690
    const/4 v4, 0x0

    .line 691
    if-eqz p2, :cond_78

    :try_start_19
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 692
    :goto_1d
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 694
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Callable;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v13}, Lcom/a/b/n/a/dy;->a(Lcom/a/b/n/a/du;Ljava/util/concurrent/Callable;Ljava/util/concurrent/BlockingQueue;)Lcom/a/b/n/a/dp;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 695
    add-int/lit8 v5, v3, -0x1

    .line 696
    const/4 v2, 0x1

    move v3, v5

    move-wide/from16 v10, p3

    move v5, v2

    .line 699
    :goto_37
    invoke-interface {v13}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 700
    if-nez v2, :cond_e3

    .line 701
    if-lez v3, :cond_7b

    .line 702
    add-int/lit8 v8, v3, -0x1

    .line 703
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Callable;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v13}, Lcom/a/b/n/a/dy;->a(Lcom/a/b/n/a/du;Ljava/util/concurrent/Callable;Ljava/util/concurrent/BlockingQueue;)Lcom/a/b/n/a/dp;

    move-result-object v3

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_52
    .catchall {:try_start_19 .. :try_end_52} :catchall_8f

    .line 704
    add-int/lit8 v3, v5, 0x1

    move v5, v8

    move-wide v8, v10

    move-object v15, v2

    move v2, v3

    move-object v3, v15

    .line 719
    :goto_59
    if-eqz v3, :cond_e1

    .line 720
    add-int/lit8 v2, v2, -0x1

    .line 722
    :try_start_5d
    invoke-interface {v3}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_60
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_5d .. :try_end_60} :catch_c4
    .catch Ljava/lang/RuntimeException; {:try_start_5d .. :try_end_60} :catch_ca
    .catchall {:try_start_5d .. :try_end_60} :catchall_8f

    move-result-object v3

    .line 736
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_65
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 737
    const/4 v5, 0x1

    invoke-interface {v2, v5}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_65

    .line 677
    :cond_76
    const/4 v2, 0x0

    goto :goto_a

    .line 691
    :cond_78
    const-wide/16 v6, 0x0

    goto :goto_1d

    .line 705
    :cond_7b
    if-eqz v5, :cond_d6

    .line 707
    if-eqz p2, :cond_b8

    .line 708
    :try_start_7f
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v13, v10, v11, v2}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 709
    if-nez v2, :cond_a6

    .line 710
    new-instance v2, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v2}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v2
    :try_end_8f
    .catchall {:try_start_7f .. :try_end_8f} :catchall_8f

    .line 736
    :catchall_8f
    move-exception v2

    move-object v3, v2

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_95
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_df

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 737
    const/4 v5, 0x1

    invoke-interface {v2, v5}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_95

    .line 712
    :cond_a6
    :try_start_a6
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 713
    sub-long v6, v8, v6

    sub-long v6, v10, v6

    move-object v15, v2

    move v2, v5

    move v5, v3

    move-object v3, v15

    move-wide/from16 v16, v8

    move-wide v8, v6

    move-wide/from16 v6, v16

    .line 715
    goto :goto_59

    .line 716
    :cond_b8
    invoke-interface {v13}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    move-wide v8, v10

    move v15, v5

    move v5, v3

    move-object v3, v2

    move v2, v15

    goto :goto_59

    .line 724
    :catch_c4
    move-exception v4

    move v3, v5

    move-wide v10, v8

    move v5, v2

    .line 727
    goto/16 :goto_37

    .line 725
    :catch_ca
    move-exception v4

    .line 726
    new-instance v3, Ljava/util/concurrent/ExecutionException;

    invoke-direct {v3, v4}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    :goto_d0
    move-object v4, v3

    move-wide v10, v8

    move v3, v5

    move v5, v2

    .line 729
    goto/16 :goto_37

    .line 731
    :cond_d6
    if-nez v4, :cond_de

    .line 732
    new-instance v4, Ljava/util/concurrent/ExecutionException;

    const/4 v2, 0x0

    invoke-direct {v4, v2}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    .line 734
    :cond_de
    throw v4
    :try_end_df
    .catchall {:try_start_a6 .. :try_end_df} :catchall_8f

    .line 738
    :cond_df
    throw v3

    :cond_e0
    return-object v3

    :cond_e1
    move-object v3, v4

    goto :goto_d0

    :cond_e3
    move-wide v8, v10

    move v15, v5

    move v5, v3

    move-object v3, v2

    move v2, v15

    goto/16 :goto_59
.end method

.method static a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;
    .registers 4

    .prologue
    .line 814
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 816
    invoke-static {}, Lcom/a/b/n/a/dy;->e()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/ThreadFactory;->newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v0

    .line 818
    :try_start_e
    invoke-virtual {v0, p0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_11} :catch_12

    .line 822
    :goto_11
    return-object v0

    :catch_12
    move-exception v1

    goto :goto_11
.end method

.method static a(Ljava/util/concurrent/Executor;Lcom/a/b/b/dz;)Ljava/util/concurrent/Executor;
    .registers 3

    .prologue
    .line 841
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 842
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    invoke-static {}, Lcom/a/b/n/a/dy;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 847
    :goto_c
    return-object p0

    :cond_d
    new-instance v0, Lcom/a/b/n/a/ea;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/ea;-><init>(Ljava/util/concurrent/Executor;Lcom/a/b/b/dz;)V

    move-object p0, v0

    goto :goto_c
.end method

.method private static a(Ljava/util/concurrent/ExecutorService;Lcom/a/b/b/dz;)Ljava/util/concurrent/ExecutorService;
    .registers 3

    .prologue
    .line 868
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    invoke-static {}, Lcom/a/b/n/a/dy;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 874
    :goto_c
    return-object p0

    :cond_d
    new-instance v0, Lcom/a/b/n/a/eb;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/eb;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/a/b/b/dz;)V

    move-object p0, v0

    goto :goto_c
.end method

.method private static a(Ljava/util/concurrent/ThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ExecutorService;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lcom/a/b/n/a/ed;

    invoke-direct {v0}, Lcom/a/b/n/a/ed;-><init>()V

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/a/b/n/a/ed;->a(Ljava/util/concurrent/ThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/concurrent/ScheduledExecutorService;Lcom/a/b/b/dz;)Ljava/util/concurrent/ScheduledExecutorService;
    .registers 3

    .prologue
    .line 898
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 899
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 900
    invoke-static {}, Lcom/a/b/n/a/dy;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 904
    :goto_c
    return-object p0

    :cond_d
    new-instance v0, Lcom/a/b/n/a/ec;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/ec;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/a/b/b/dz;)V

    move-object p0, v0

    goto :goto_c
.end method

.method private static a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;)Ljava/util/concurrent/ScheduledExecutorService;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 169
    new-instance v0, Lcom/a/b/n/a/ed;

    invoke-direct {v0}, Lcom/a/b/n/a/ed;-><init>()V

    .line 1219
    const-wide/16 v2, 0x78

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, v2, v3, v1}, Lcom/a/b/n/a/ed;->a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    .line 169
    return-object v0
.end method

.method private static a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledExecutorService;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcom/a/b/n/a/ed;

    invoke-direct {v0}, Lcom/a/b/n/a/ed;-><init>()V

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/a/b/n/a/ed;->a(Ljava/util/concurrent/ScheduledThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)V
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 127
    new-instance v0, Lcom/a/b/n/a/ed;

    invoke-direct {v0}, Lcom/a/b/n/a/ed;-><init>()V

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/a/b/n/a/ed;->a(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)V

    .line 129
    return-void
.end method

.method static synthetic a(Ljava/util/concurrent/ThreadPoolExecutor;)V
    .registers 3

    .prologue
    .line 65
    .line 3228
    new-instance v0, Lcom/a/b/n/a/gl;

    invoke-direct {v0}, Lcom/a/b/n/a/gl;-><init>()V

    invoke-virtual {v0}, Lcom/a/b/n/a/gl;->a()Lcom/a/b/n/a/gl;

    move-result-object v1

    invoke-virtual {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->getThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    .line 4133
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadFactory;

    iput-object v0, v1, Lcom/a/b/n/a/gl;->b:Ljava/util/concurrent/ThreadFactory;

    .line 3228
    invoke-virtual {v1}, Lcom/a/b/n/a/gl;->b()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->setThreadFactory(Ljava/util/concurrent/ThreadFactory;)V

    .line 65
    return-void
.end method

.method static a()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 786
    const-string v1, "com.google.appengine.runtime.environment"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    .line 805
    :cond_9
    :goto_9
    return v0

    .line 791
    :cond_a
    :try_start_a
    const-string v1, "com.google.apphosting.api.ApiProxy"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "getCurrentEnvironment"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_20
    .catch Ljava/lang/ClassNotFoundException; {:try_start_a .. :try_end_20} :catch_2b
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_20} :catch_29
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_20} :catch_27
    .catch Ljava/lang/NoSuchMethodException; {:try_start_a .. :try_end_20} :catch_25

    move-result-object v1

    if-eqz v1, :cond_9

    const/4 v0, 0x1

    goto :goto_9

    .line 805
    :catch_25
    move-exception v1

    goto :goto_9

    .line 802
    :catch_27
    move-exception v1

    goto :goto_9

    .line 799
    :catch_29
    move-exception v1

    goto :goto_9

    .line 796
    :catch_2b
    move-exception v1

    goto :goto_9
.end method

.method private static b()Lcom/a/b/n/a/du;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 270
    new-instance v0, Lcom/a/b/n/a/eg;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/n/a/eg;-><init>(B)V

    return-object v0
.end method

.method private static b(Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ExecutorService;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 148
    new-instance v0, Lcom/a/b/n/a/ed;

    invoke-direct {v0}, Lcom/a/b/n/a/ed;-><init>()V

    .line 1214
    const-wide/16 v2, 0x78

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, v2, v3, v1}, Lcom/a/b/n/a/ed;->a(Ljava/util/concurrent/ThreadPoolExecutor;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 148
    return-object v0
.end method

.method private static b(Ljava/util/concurrent/ExecutorService;JLjava/util/concurrent/TimeUnit;)Z
    .registers 9
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 942
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    invoke-interface {p0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 946
    :try_start_6
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    .line 948
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v0, v1, v2}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    if-nez v2, :cond_1f

    .line 950
    invoke-interface {p0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 952
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v0, v1, v2}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1f
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_1f} :catch_24

    .line 960
    :cond_1f
    :goto_1f
    invoke-interface {p0}, Ljava/util/concurrent/ExecutorService;->isTerminated()Z

    move-result v0

    return v0

    .line 956
    :catch_24
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 958
    invoke-interface {p0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    goto :goto_1f
.end method

.method private static c()Lcom/a/b/n/a/du;
    .registers 2

    .prologue
    .line 430
    new-instance v0, Lcom/a/b/n/a/eg;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/n/a/eg;-><init>(B)V

    return-object v0
.end method

.method private static c(Ljava/util/concurrent/ThreadPoolExecutor;)V
    .registers 3

    .prologue
    .line 228
    new-instance v0, Lcom/a/b/n/a/gl;

    invoke-direct {v0}, Lcom/a/b/n/a/gl;-><init>()V

    invoke-virtual {v0}, Lcom/a/b/n/a/gl;->a()Lcom/a/b/n/a/gl;

    move-result-object v1

    invoke-virtual {p0}, Ljava/util/concurrent/ThreadPoolExecutor;->getThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    .line 2133
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadFactory;

    iput-object v0, v1, Lcom/a/b/n/a/gl;->b:Ljava/util/concurrent/ThreadFactory;

    .line 228
    invoke-virtual {v1}, Lcom/a/b/n/a/gl;->b()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->setThreadFactory(Ljava/util/concurrent/ThreadFactory;)V

    .line 232
    return-void
.end method

.method private static d()Ljava/util/concurrent/Executor;
    .registers 1

    .prologue
    .line 450
    sget-object v0, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    return-object v0
.end method

.method private static e()Ljava/util/concurrent/ThreadFactory;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 767
    invoke-static {}, Lcom/a/b/n/a/dy;->a()Z

    move-result v0

    if-nez v0, :cond_b

    .line 768
    invoke-static {}, Ljava/util/concurrent/Executors;->defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    .line 771
    :goto_a
    return-object v0

    :cond_b
    :try_start_b
    const-string v0, "com.google.appengine.api.ThreadManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "currentRequestThreadFactory"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadFactory;
    :try_end_24
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_24} :catch_25
    .catch Ljava/lang/ClassNotFoundException; {:try_start_b .. :try_end_24} :catch_2e
    .catch Ljava/lang/NoSuchMethodException; {:try_start_b .. :try_end_24} :catch_37
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_24} :catch_40

    goto :goto_a

    .line 774
    :catch_25
    move-exception v0

    .line 775
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t invoke ThreadManager.currentRequestThreadFactory"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 776
    :catch_2e
    move-exception v0

    .line 777
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t invoke ThreadManager.currentRequestThreadFactory"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 778
    :catch_37
    move-exception v0

    .line 779
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t invoke ThreadManager.currentRequestThreadFactory"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 781
    :catch_40
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
