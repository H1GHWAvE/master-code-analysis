.class public abstract Lcom/a/b/n/a/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/et;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:Lcom/a/b/n/a/dt;

.field private static final b:Lcom/a/b/n/a/dt;

.field private static final c:Lcom/a/b/n/a/dt;

.field private static final d:Lcom/a/b/n/a/dt;

.field private static final e:Lcom/a/b/n/a/dt;

.field private static final f:Lcom/a/b/n/a/dt;

.field private static final g:Lcom/a/b/n/a/dt;


# instance fields
.field private final h:Lcom/a/b/n/a/dw;

.field private final i:Lcom/a/b/n/a/dx;

.field private final j:Lcom/a/b/n/a/dx;

.field private final k:Lcom/a/b/n/a/dx;

.field private final l:Lcom/a/b/n/a/dx;

.field private final m:Ljava/util/List;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor"
    .end annotation
.end field

.field private volatile n:Lcom/a/b/n/a/ao;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 57
    new-instance v0, Lcom/a/b/n/a/ae;

    const-string v1, "starting()"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ae;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/ad;->a:Lcom/a/b/n/a/dt;

    .line 63
    new-instance v0, Lcom/a/b/n/a/ag;

    const-string v1, "running()"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ag;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/ad;->b:Lcom/a/b/n/a/dt;

    .line 69
    sget-object v0, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    invoke-static {v0}, Lcom/a/b/n/a/ad;->b(Lcom/a/b/n/a/ew;)Lcom/a/b/n/a/dt;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/ad;->c:Lcom/a/b/n/a/dt;

    .line 71
    sget-object v0, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    invoke-static {v0}, Lcom/a/b/n/a/ad;->b(Lcom/a/b/n/a/ew;)Lcom/a/b/n/a/dt;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/ad;->d:Lcom/a/b/n/a/dt;

    .line 74
    sget-object v0, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    invoke-static {v0}, Lcom/a/b/n/a/ad;->a(Lcom/a/b/n/a/ew;)Lcom/a/b/n/a/dt;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/ad;->e:Lcom/a/b/n/a/dt;

    .line 76
    sget-object v0, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    invoke-static {v0}, Lcom/a/b/n/a/ad;->a(Lcom/a/b/n/a/ew;)Lcom/a/b/n/a/dt;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/ad;->f:Lcom/a/b/n/a/dt;

    .line 78
    sget-object v0, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    invoke-static {v0}, Lcom/a/b/n/a/ad;->a(Lcom/a/b/n/a/ew;)Lcom/a/b/n/a/dt;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/ad;->g:Lcom/a/b/n/a/dt;

    return-void
.end method

.method protected constructor <init>()V
    .registers 3

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Lcom/a/b/n/a/dw;

    invoke-direct {v0}, Lcom/a/b/n/a/dw;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    .line 99
    new-instance v0, Lcom/a/b/n/a/aj;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-direct {v0, p0, v1}, Lcom/a/b/n/a/aj;-><init>(Lcom/a/b/n/a/ad;Lcom/a/b/n/a/dw;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->i:Lcom/a/b/n/a/dx;

    .line 105
    new-instance v0, Lcom/a/b/n/a/ak;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-direct {v0, p0, v1}, Lcom/a/b/n/a/ak;-><init>(Lcom/a/b/n/a/ad;Lcom/a/b/n/a/dw;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->j:Lcom/a/b/n/a/dx;

    .line 111
    new-instance v0, Lcom/a/b/n/a/al;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-direct {v0, p0, v1}, Lcom/a/b/n/a/al;-><init>(Lcom/a/b/n/a/ad;Lcom/a/b/n/a/dw;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->k:Lcom/a/b/n/a/dx;

    .line 117
    new-instance v0, Lcom/a/b/n/a/am;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-direct {v0, p0, v1}, Lcom/a/b/n/a/am;-><init>(Lcom/a/b/n/a/ad;Lcom/a/b/n/a/dw;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->l:Lcom/a/b/n/a/dx;

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    .line 139
    new-instance v0, Lcom/a/b/n/a/ao;

    sget-object v1, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 143
    return-void
.end method

.method private static a(Lcom/a/b/n/a/ew;)Lcom/a/b/n/a/dt;
    .registers 5

    .prologue
    .line 82
    new-instance v0, Lcom/a/b/n/a/ah;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "terminated({from = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "})"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/a/b/n/a/ah;-><init>(Ljava/lang/String;Lcom/a/b/n/a/ew;)V

    return-object v0
.end method

.method private a(Lcom/a/b/n/a/ew;Ljava/lang/Throwable;)V
    .registers 9
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor"
    .end annotation

    .prologue
    .line 482
    new-instance v0, Lcom/a/b/n/a/an;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "failed({from = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", cause = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "})"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/a/b/n/a/an;-><init>(Lcom/a/b/n/a/ad;Ljava/lang/String;Lcom/a/b/n/a/ew;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/an;->a(Ljava/lang/Iterable;)V

    .line 487
    return-void
.end method

.method private static b(Lcom/a/b/n/a/ew;)Lcom/a/b/n/a/dt;
    .registers 5

    .prologue
    .line 90
    new-instance v0, Lcom/a/b/n/a/ai;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "stopping({from = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "})"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/a/b/n/a/ai;-><init>(Ljava/lang/String;Lcom/a/b/n/a/ew;)V

    return-object v0
.end method

.method private c(Lcom/a/b/n/a/ew;)V
    .registers 8
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor"
    .end annotation

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->f()Lcom/a/b/n/a/ew;

    move-result-object v0

    .line 282
    if-eq v0, p1, :cond_79

    .line 283
    sget-object v1, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    if-ne v0, v1, :cond_3b

    .line 285
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Expected the service to be "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", but the service has FAILED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->g()Ljava/lang/Throwable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 288
    :cond_3b
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x25

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Expected the service to be "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 291
    :cond_79
    return-void
.end method

.method private d(Lcom/a/b/n/a/ew;)V
    .registers 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor"
    .end annotation

    .prologue
    .line 450
    sget-object v0, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    if-ne p1, v0, :cond_c

    .line 451
    sget-object v0, Lcom/a/b/n/a/ad;->c:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V

    .line 453
    :goto_b
    return-void

    .line 452
    :cond_c
    sget-object v0, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    if-ne p1, v0, :cond_18

    .line 453
    sget-object v0, Lcom/a/b/n/a/ad;->d:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V

    goto :goto_b

    .line 455
    :cond_18
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private e(Lcom/a/b/n/a/ew;)V
    .registers 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor"
    .end annotation

    .prologue
    .line 461
    sget-object v0, Lcom/a/b/n/a/af;->a:[I

    invoke-virtual {p1}, Lcom/a/b/n/a/ew;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2a

    .line 475
    :pswitch_b
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 463
    :pswitch_11
    sget-object v0, Lcom/a/b/n/a/ad;->e:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V

    .line 470
    :goto_18
    return-void

    .line 466
    :pswitch_19
    sget-object v0, Lcom/a/b/n/a/ad;->f:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V

    goto :goto_18

    .line 469
    :pswitch_21
    sget-object v0, Lcom/a/b/n/a/ad;->g:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V

    goto :goto_18

    .line 461
    nop

    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_11
        :pswitch_b
        :pswitch_19
        :pswitch_21
    .end packed-switch
.end method

.method private l()V
    .registers 3

    .prologue
    .line 430
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    .line 5796
    iget-object v0, v0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    .line 430
    if-nez v0, :cond_23

    .line 432
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    iget-object v0, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_23

    .line 433
    iget-object v0, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/ds;

    invoke-virtual {v0}, Lcom/a/b/n/a/ds;->a()V

    .line 432
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 436
    :cond_23
    return-void
.end method

.method private m()V
    .registers 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor"
    .end annotation

    .prologue
    .line 440
    sget-object v0, Lcom/a/b/n/a/ad;->a:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V

    .line 441
    return-void
.end method

.method private n()V
    .registers 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "monitor"
    .end annotation

    .prologue
    .line 445
    sget-object v0, Lcom/a/b/n/a/ad;->b:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V

    .line 446
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)V
    .registers 11

    .prologue
    .line 236
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->k:Lcom/a/b/n/a/dx;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 238
    :try_start_a
    sget-object v0, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    invoke-direct {p0, v0}, Lcom/a/b/n/a/ad;->c(Lcom/a/b/n/a/ew;)V
    :try_end_f
    .catchall {:try_start_a .. :try_end_f} :catchall_15

    .line 240
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 241
    return-void

    .line 240
    :catchall_15
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    throw v0

    .line 247
    :cond_1c
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->f()Lcom/a/b/n/a/ew;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x42

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Timed out waiting for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to reach the RUNNING state. Current state: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/a/b/n/a/ev;Ljava/util/concurrent/Executor;)V
    .registers 5

    .prologue
    .line 409
    const-string v0, "listener"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    const-string v0, "executor"

    invoke-static {p2, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    .line 5357
    iget-object v0, v0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 413
    :try_start_11
    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->f()Lcom/a/b/n/a/ew;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/n/a/ew;->a()Z

    move-result v0

    if-nez v0, :cond_25

    .line 414
    iget-object v0, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    new-instance v1, Lcom/a/b/n/a/ds;

    invoke-direct {v1, p1, p2}, Lcom/a/b/n/a/ds;-><init>(Ljava/lang/Object;Ljava/util/concurrent/Executor;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_25
    .catchall {:try_start_11 .. :try_end_25} :catchall_2b

    .line 417
    :cond_25
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 418
    return-void

    .line 417
    :catchall_2b
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    throw v0
.end method

.method protected final a(Ljava/lang/Throwable;)V
    .registers 9

    .prologue
    .line 359
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    .line 4357
    iget-object v0, v0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 363
    :try_start_a
    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->f()Lcom/a/b/n/a/ew;

    move-result-object v0

    .line 364
    sget-object v1, Lcom/a/b/n/a/af;->a:[I

    invoke-virtual {v0}, Lcom/a/b/n/a/ew;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_cc

    .line 378
    new-instance v1, Ljava/lang/AssertionError;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_40
    .catchall {:try_start_a .. :try_end_40} :catchall_40

    .line 381
    :catchall_40
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    .line 382
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    throw v0

    .line 367
    :pswitch_4a
    :try_start_4a
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed while in state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 371
    :pswitch_71
    new-instance v1, Lcom/a/b/n/a/ao;

    sget-object v2, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, p1}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;ZLjava/lang/Throwable;)V

    iput-object v1, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 4482
    new-instance v1, Lcom/a/b/n/a/an;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1b

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "failed({from = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", cause = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "})"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0, p1}, Lcom/a/b/n/a/an;-><init>(Lcom/a/b/n/a/ad;Ljava/lang/String;Lcom/a/b/n/a/ew;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/a/b/n/a/an;->a(Ljava/lang/Iterable;)V
    :try_end_c3
    .catchall {:try_start_4a .. :try_end_c3} :catchall_40

    .line 381
    :pswitch_c3
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 382
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    .line 383
    return-void

    .line 364
    :pswitch_data_cc
    .packed-switch 0x1
        :pswitch_4a
        :pswitch_71
        :pswitch_71
        :pswitch_71
        :pswitch_4a
        :pswitch_c3
    .end packed-switch
.end method

.method protected abstract b()V
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)V
    .registers 11

    .prologue
    .line 262
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->l:Lcom/a/b/n/a/dx;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 264
    :try_start_a
    sget-object v0, Lcom/a/b/n/a/ew;->e:Lcom/a/b/n/a/ew;

    invoke-direct {p0, v0}, Lcom/a/b/n/a/ad;->c(Lcom/a/b/n/a/ew;)V
    :try_end_f
    .catchall {:try_start_a .. :try_end_f} :catchall_15

    .line 266
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 267
    return-void

    .line 266
    :catchall_15
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    throw v0

    .line 273
    :cond_1c
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->f()Lcom/a/b/n/a/ew;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x41

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Timed out waiting for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to reach a terminal state. Current state: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final c()V
    .registers 5

    .prologue
    .line 300
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    .line 2357
    iget-object v0, v0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 304
    :try_start_7
    iget-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    iget-object v0, v0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    sget-object v1, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    if-eq v0, v1, :cond_47

    .line 305
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    iget-object v1, v1, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot notifyStarted() when the service is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/ad;->a(Ljava/lang/Throwable;)V

    .line 308
    throw v0
    :try_end_3d
    .catchall {:try_start_7 .. :try_end_3d} :catchall_3d

    .line 321
    :catchall_3d
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    .line 322
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    throw v0

    .line 311
    :cond_47
    :try_start_47
    iget-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    iget-boolean v0, v0, Lcom/a/b/n/a/ao;->b:Z

    if-eqz v0, :cond_62

    .line 312
    new-instance v0, Lcom/a/b/n/a/ao;

    sget-object v1, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 315
    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->b()V
    :try_end_59
    .catchall {:try_start_47 .. :try_end_59} :catchall_3d

    .line 321
    :goto_59
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 322
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    .line 323
    return-void

    .line 317
    :cond_62
    :try_start_62
    new-instance v0, Lcom/a/b/n/a/ao;

    sget-object v1, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 2445
    sget-object v0, Lcom/a/b/n/a/ad;->b:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V
    :try_end_72
    .catchall {:try_start_62 .. :try_end_72} :catchall_3d

    goto :goto_59
.end method

.method protected final d()V
    .registers 5

    .prologue
    .line 334
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    .line 3357
    iget-object v0, v0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 338
    :try_start_7
    iget-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    iget-object v0, v0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    .line 339
    sget-object v1, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    if-eq v0, v1, :cond_47

    sget-object v1, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    if-eq v0, v1, :cond_47

    .line 340
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot notifyStopped() when the service is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 342
    invoke-virtual {p0, v1}, Lcom/a/b/n/a/ad;->a(Ljava/lang/Throwable;)V

    .line 343
    throw v1
    :try_end_3d
    .catchall {:try_start_7 .. :try_end_3d} :catchall_3d

    .line 348
    :catchall_3d
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    .line 349
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    throw v0

    .line 345
    :cond_47
    :try_start_47
    new-instance v1, Lcom/a/b/n/a/ao;

    sget-object v2, Lcom/a/b/n/a/ew;->e:Lcom/a/b/n/a/ew;

    invoke-direct {v1, v2}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;)V

    iput-object v1, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 346
    invoke-direct {p0, v0}, Lcom/a/b/n/a/ad;->e(Lcom/a/b/n/a/ew;)V
    :try_end_53
    .catchall {:try_start_47 .. :try_end_53} :catchall_3d

    .line 348
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 349
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    .line 350
    return-void
.end method

.method public final e()Z
    .registers 3

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->f()Lcom/a/b/n/a/ew;

    move-result-object v0

    sget-object v1, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final f()Lcom/a/b/n/a/ew;
    .registers 4

    .prologue
    .line 393
    iget-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 4534
    iget-boolean v1, v0, Lcom/a/b/n/a/ao;->b:Z

    if-eqz v1, :cond_f

    iget-object v1, v0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    sget-object v2, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    if-ne v1, v2, :cond_f

    .line 4535
    sget-object v0, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    :goto_e
    return-object v0

    .line 4537
    :cond_f
    iget-object v0, v0, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    goto :goto_e
.end method

.method public final g()Ljava/lang/Throwable;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 401
    iget-object v3, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 4543
    iget-object v0, v3, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    sget-object v4, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    if-ne v0, v4, :cond_19

    move v0, v1

    :goto_b
    const-string v4, "failureCause() is only valid if the service has failed, service is %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, v3, Lcom/a/b/n/a/ao;->a:Lcom/a/b/n/a/ew;

    aput-object v5, v1, v2

    invoke-static {v0, v4, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 4545
    iget-object v0, v3, Lcom/a/b/n/a/ao;->c:Ljava/lang/Throwable;

    .line 401
    return-object v0

    :cond_19
    move v0, v2

    .line 4543
    goto :goto_b
.end method

.method public final h()Lcom/a/b/n/a/et;
    .registers 5

    .prologue
    .line 170
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->i:Lcom/a/b/n/a/dx;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dw;->b(Lcom/a/b/n/a/dx;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 172
    :try_start_a
    new-instance v0, Lcom/a/b/n/a/ao;

    sget-object v1, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 1440
    sget-object v0, Lcom/a/b/n/a/ad;->a:Lcom/a/b/n/a/dt;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dt;->a(Ljava/lang/Iterable;)V

    .line 174
    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->a()V
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_1d} :catch_26
    .catchall {:try_start_a .. :try_end_1d} :catchall_33

    .line 179
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 180
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    .line 185
    :goto_25
    return-object p0

    .line 176
    :catch_26
    move-exception v0

    .line 177
    :try_start_27
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/ad;->a(Ljava/lang/Throwable;)V
    :try_end_2a
    .catchall {:try_start_27 .. :try_end_2a} :catchall_33

    .line 179
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 180
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    goto :goto_25

    .line 179
    :catchall_33
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    .line 180
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    throw v0

    .line 183
    :cond_3d
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Service "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has already been started"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final i()Lcom/a/b/n/a/et;
    .registers 5

    .prologue
    .line 189
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->j:Lcom/a/b/n/a/dx;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dw;->b(Lcom/a/b/n/a/dx;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 191
    :try_start_a
    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->f()Lcom/a/b/n/a/ew;

    move-result-object v0

    .line 192
    sget-object v1, Lcom/a/b/n/a/af;->a:[I

    invoke-virtual {v0}, Lcom/a/b/n/a/ew;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_b8

    .line 212
    new-instance v1, Ljava/lang/AssertionError;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_40
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_40} :catch_40
    .catchall {:try_start_a .. :try_end_40} :catchall_75

    .line 216
    :catch_40
    move-exception v0

    .line 217
    :try_start_41
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/ad;->a(Ljava/lang/Throwable;)V
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_75

    .line 219
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 220
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    .line 223
    :cond_4c
    :goto_4c
    return-object p0

    .line 194
    :pswitch_4d
    :try_start_4d
    new-instance v0, Lcom/a/b/n/a/ao;

    sget-object v1, Lcom/a/b/n/a/ew;->e:Lcom/a/b/n/a/ew;

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 195
    sget-object v0, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    invoke-direct {p0, v0}, Lcom/a/b/n/a/ad;->e(Lcom/a/b/n/a/ew;)V
    :try_end_5b
    .catch Ljava/lang/Throwable; {:try_start_4d .. :try_end_5b} :catch_40
    .catchall {:try_start_4d .. :try_end_5b} :catchall_75

    .line 219
    :goto_5b
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 220
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    goto :goto_4c

    .line 198
    :pswitch_64
    :try_start_64
    new-instance v0, Lcom/a/b/n/a/ao;

    sget-object v1, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;ZLjava/lang/Throwable;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 199
    sget-object v0, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    invoke-direct {p0, v0}, Lcom/a/b/n/a/ad;->d(Lcom/a/b/n/a/ew;)V
    :try_end_74
    .catch Ljava/lang/Throwable; {:try_start_64 .. :try_end_74} :catch_40
    .catchall {:try_start_64 .. :try_end_74} :catchall_75

    goto :goto_5b

    .line 219
    :catchall_75
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    .line 220
    invoke-direct {p0}, Lcom/a/b/n/a/ad;->l()V

    throw v0

    .line 202
    :pswitch_7f
    :try_start_7f
    new-instance v0, Lcom/a/b/n/a/ao;

    sget-object v1, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ao;-><init>(Lcom/a/b/n/a/ew;)V

    iput-object v0, p0, Lcom/a/b/n/a/ad;->n:Lcom/a/b/n/a/ao;

    .line 203
    sget-object v0, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    invoke-direct {p0, v0}, Lcom/a/b/n/a/ad;->d(Lcom/a/b/n/a/ew;)V

    .line 204
    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->b()V

    goto :goto_5b

    .line 210
    :pswitch_91
    new-instance v1, Ljava/lang/AssertionError;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "isStoppable is incorrectly implemented, saw: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_b8
    .catch Ljava/lang/Throwable; {:try_start_7f .. :try_end_b8} :catch_40
    .catchall {:try_start_7f .. :try_end_b8} :catchall_75

    .line 192
    :pswitch_data_b8
    .packed-switch 0x1
        :pswitch_4d
        :pswitch_64
        :pswitch_7f
        :pswitch_91
        :pswitch_91
        :pswitch_91
    .end packed-switch
.end method

.method public final j()V
    .registers 3

    .prologue
    .line 227
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->k:Lcom/a/b/n/a/dx;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;)V

    .line 229
    :try_start_7
    sget-object v0, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    invoke-direct {p0, v0}, Lcom/a/b/n/a/ad;->c(Lcom/a/b/n/a/ew;)V
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_12

    .line 231
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 232
    return-void

    .line 231
    :catchall_12
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    throw v0
.end method

.method public final k()V
    .registers 3

    .prologue
    .line 253
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    iget-object v1, p0, Lcom/a/b/n/a/ad;->l:Lcom/a/b/n/a/dx;

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;)V

    .line 255
    :try_start_7
    sget-object v0, Lcom/a/b/n/a/ew;->e:Lcom/a/b/n/a/ew;

    invoke-direct {p0, v0}, Lcom/a/b/n/a/ad;->c(Lcom/a/b/n/a/ew;)V
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_12

    .line 257
    iget-object v0, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v0}, Lcom/a/b/n/a/dw;->a()V

    .line 258
    return-void

    .line 257
    :catchall_12
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/ad;->h:Lcom/a/b/n/a/dw;

    invoke-virtual {v1}, Lcom/a/b/n/a/dw;->a()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 422
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/a/b/n/a/ad;->f()Lcom/a/b/n/a/ew;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
