.class public abstract Lcom/a/b/n/a/a;
.super Lcom/a/b/n/a/ce;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/bc;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method protected constructor <init>(Lcom/a/b/n/a/dp;)V
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/a/b/n/a/ce;-><init>(Lcom/a/b/n/a/dp;)V

    .line 42
    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Exception;)Ljava/lang/Exception;
.end method

.method public final a()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 78
    :try_start_0
    invoke-virtual {p0}, Lcom/a/b/n/a/a;->get()Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_3} :catch_5
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_3} :catch_12
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_3} :catch_18

    move-result-object v0

    return-object v0

    .line 79
    :catch_5
    move-exception v0

    .line 80
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 81
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/a;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 82
    :catch_12
    move-exception v0

    .line 83
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/a;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 84
    :catch_18
    move-exception v0

    .line 85
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/a;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 7

    .prologue
    .line 107
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/n/a/a;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_3} :catch_5
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_3} :catch_12
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_3} :catch_18

    move-result-object v0

    return-object v0

    .line 108
    :catch_5
    move-exception v0

    .line 109
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 110
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/a;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 111
    :catch_12
    move-exception v0

    .line 112
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/a;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 113
    :catch_18
    move-exception v0

    .line 114
    invoke-virtual {p0, v0}, Lcom/a/b/n/a/a;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method
