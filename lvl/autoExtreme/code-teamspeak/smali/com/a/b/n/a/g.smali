.class public abstract Lcom/a/b/n/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/dp;


# instance fields
.field final a:Lcom/a/b/n/a/h;

.field private final b:Lcom/a/b/n/a/bu;


# direct methods
.method protected constructor <init>()V
    .registers 2

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Lcom/a/b/n/a/h;

    invoke-direct {v0}, Lcom/a/b/n/a/h;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    .line 71
    new-instance v0, Lcom/a/b/n/a/bu;

    invoke-direct {v0}, Lcom/a/b/n/a/bu;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/g;->b:Lcom/a/b/n/a/bu;

    .line 76
    return-void
.end method

.method static final a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;
    .registers 3
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Throwable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 392
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0, p0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    .line 393
    invoke-virtual {v0, p1}, Ljava/util/concurrent/CancellationException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 394
    return-object v0
.end method

.method private static a()V
    .registers 0

    .prologue
    .line 151
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    invoke-virtual {v0}, Lcom/a/b/n/a/h;->d()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .registers 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/a/b/n/a/g;->b:Lcom/a/b/n/a/bu;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/n/a/bu;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 171
    return-void
.end method

.method protected a(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 183
    iget-object v0, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    .line 2341
    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v1, v2}, Lcom/a/b/n/a/h;->a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v0

    .line 184
    if-eqz v0, :cond_f

    .line 185
    iget-object v1, p0, Lcom/a/b/n/a/g;->b:Lcom/a/b/n/a/bu;

    invoke-virtual {v1}, Lcom/a/b/n/a/bu;->a()V

    .line 187
    :cond_f
    return v0
.end method

.method protected a(Ljava/lang/Throwable;)Z
    .registers 6

    .prologue
    .line 200
    iget-object v1, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 2348
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Lcom/a/b/n/a/h;->a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v0

    .line 201
    if-eqz v0, :cond_15

    .line 202
    iget-object v1, p0, Lcom/a/b/n/a/g;->b:Lcom/a/b/n/a/bu;

    invoke-virtual {v1}, Lcom/a/b/n/a/bu;->a()V

    .line 204
    :cond_15
    return v0
.end method

.method public cancel(Z)Z
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 131
    iget-object v1, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    .line 1355
    if-eqz p1, :cond_f

    const/16 v0, 0x8

    :goto_7
    invoke-virtual {v1, v2, v2, v0}, Lcom/a/b/n/a/h;->a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v0

    .line 131
    if-nez v0, :cond_11

    .line 132
    const/4 v0, 0x0

    .line 138
    :goto_e
    return v0

    .line 1355
    :cond_f
    const/4 v0, 0x4

    goto :goto_7

    .line 134
    :cond_11
    iget-object v0, p0, Lcom/a/b/n/a/g;->b:Lcom/a/b/n/a/bu;

    invoke-virtual {v0}, Lcom/a/b/n/a/bu;->a()V

    .line 138
    const/4 v0, 0x1

    goto :goto_e
.end method

.method public get()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    .line 1285
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/a/b/n/a/h;->acquireSharedInterruptibly(I)V

    .line 1286
    invoke-virtual {v0}, Lcom/a/b/n/a/h;->a()Ljava/lang/Object;

    move-result-object v0

    .line 116
    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 9

    .prologue
    .line 96
    iget-object v0, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 1268
    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/b/n/a/h;->tryAcquireSharedNanos(IJ)Z

    move-result v1

    if-nez v1, :cond_15

    .line 1269
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "Timeout waiting for task."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1272
    :cond_15
    invoke-virtual {v0}, Lcom/a/b/n/a/h;->a()Ljava/lang/Object;

    move-result-object v0

    .line 96
    return-object v0
.end method

.method public isCancelled()Z
    .registers 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    invoke-virtual {v0}, Lcom/a/b/n/a/h;->c()Z

    move-result v0

    return v0
.end method

.method public isDone()Z
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/a/b/n/a/g;->a:Lcom/a/b/n/a/h;

    invoke-virtual {v0}, Lcom/a/b/n/a/h;->b()Z

    move-result v0

    return v0
.end method
