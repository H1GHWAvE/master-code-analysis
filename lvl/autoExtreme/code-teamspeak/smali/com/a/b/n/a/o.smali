.class public abstract Lcom/a/b/n/a/o;
.super Ljava/util/concurrent/AbstractExecutorService;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/du;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    return-void
.end method

.method private static b(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;
    .registers 3

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/a/b/n/a/dq;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;
    .registers 2

    .prologue
    .line 46
    invoke-static {p0}, Lcom/a/b/n/a/dq;->a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)Lcom/a/b/n/a/dp;
    .registers 3

    .prologue
    .line 50
    invoke-super {p0, p1}, Ljava/util/concurrent/AbstractExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dp;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/AbstractExecutorService;->submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    return-object v0
.end method

.method public final a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp;
    .registers 3

    .prologue
    .line 58
    invoke-super {p0, p1}, Ljava/util/concurrent/AbstractExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    return-object v0
.end method

.method protected synthetic newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
    .registers 4

    .prologue
    .line 37
    .line 2042
    invoke-static {p1, p2}, Lcom/a/b/n/a/dq;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;

    move-result-object v0

    .line 37
    return-object v0
.end method

.method protected synthetic newTaskFor(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/RunnableFuture;
    .registers 3

    .prologue
    .line 37
    .line 1046
    invoke-static {p1}, Lcom/a/b/n/a/dq;->a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;

    move-result-object v0

    .line 37
    return-object v0
.end method

.method public synthetic submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .registers 3

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/a/b/n/a/o;->a(Ljava/lang/Runnable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .registers 4

    .prologue
    .line 37
    invoke-virtual {p0, p1, p2}, Lcom/a/b/n/a/o;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .registers 3

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/a/b/n/a/o;->a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method
