.class public final Lcom/a/b/n/a/fr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/gn;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 80
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/n/a/fr;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 81
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .registers 3

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lcom/a/b/n/a/fr;->a:Ljava/util/concurrent/ExecutorService;

    .line 68
    return-void
.end method

.method private static synthetic a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .registers 2

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/a/b/n/a/fr;->a(Ljava/lang/Exception;Z)Ljava/lang/Exception;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/Exception;Z)Ljava/lang/Exception;
    .registers 6

    .prologue
    .line 149
    invoke-virtual {p0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 150
    if-nez v1, :cond_7

    .line 151
    throw p0

    .line 153
    :cond_7
    if-eqz p1, :cond_1c

    .line 154
    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const-class v3, Ljava/lang/StackTraceElement;

    invoke-static {v0, v2, v3}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/StackTraceElement;

    .line 156
    invoke-virtual {v1, v0}, Ljava/lang/Throwable;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 158
    :cond_1c
    instance-of v0, v1, Ljava/lang/Exception;

    if-eqz v0, :cond_24

    move-object v0, v1

    .line 159
    check-cast v0, Ljava/lang/Exception;

    throw v0

    .line 161
    :cond_24
    instance-of v0, v1, Ljava/lang/Error;

    if-eqz v0, :cond_2b

    .line 162
    check-cast v1, Ljava/lang/Error;

    throw v1

    .line 165
    :cond_2b
    throw p0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 191
    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1, p1}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    .line 193
    invoke-virtual {p0, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;)Ljava/util/Set;
    .registers 12

    .prologue
    const/4 v1, 0x0

    .line 3164
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 170
    invoke-virtual {p0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_c
    if-ge v2, v5, :cond_2d

    aget-object v6, v4, v2

    .line 3179
    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getExceptionTypes()[Ljava/lang/Class;

    move-result-object v7

    array-length v8, v7

    move v0, v1

    :goto_16
    if-ge v0, v8, :cond_2b

    aget-object v9, v7, v0

    .line 3181
    const-class v10, Ljava/lang/InterruptedException;

    if-ne v9, v10, :cond_28

    .line 3182
    const/4 v0, 0x1

    .line 171
    :goto_1f
    if-eqz v0, :cond_24

    .line 172
    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 170
    :cond_24
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c

    .line 3179
    :cond_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    :cond_2b
    move v0, v1

    .line 3185
    goto :goto_1f

    .line 175
    :cond_2d
    return-object v3
.end method

.method private static a(Ljava/lang/reflect/Method;)Z
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getExceptionTypes()[Ljava/lang/Class;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 181
    const-class v5, Ljava/lang/InterruptedException;

    if-ne v4, v5, :cond_11

    .line 182
    const/4 v0, 0x1

    .line 185
    :cond_10
    return v0

    .line 179
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_7
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Class;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 17

    .prologue
    .line 86
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    invoke-static/range {p5 .. p5}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_50

    const/4 v0, 0x1

    :goto_10
    const-string v1, "bad timeout: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 90
    invoke-virtual {p2}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    const-string v1, "interfaceType must be an interface type"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 2164
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 1170
    invoke-virtual {p2}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_34
    if-ge v1, v3, :cond_57

    aget-object v4, v2, v1

    .line 2179
    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getExceptionTypes()[Ljava/lang/Class;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    :goto_3e
    if-ge v0, v6, :cond_55

    aget-object v8, v5, v0

    .line 2181
    const-class v9, Ljava/lang/InterruptedException;

    if-ne v8, v9, :cond_52

    .line 2182
    const/4 v0, 0x1

    .line 1171
    :goto_47
    if-eqz v0, :cond_4c

    .line 1172
    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1170
    :cond_4c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_34

    .line 89
    :cond_50
    const/4 v0, 0x0

    goto :goto_10

    .line 2179
    :cond_52
    add-int/lit8 v0, v0, 0x1

    goto :goto_3e

    .line 2185
    :cond_55
    const/4 v0, 0x0

    goto :goto_47

    .line 96
    :cond_57
    new-instance v1, Lcom/a/b/n/a/fs;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p3

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/a/b/n/a/fs;-><init>(Lcom/a/b/n/a/fr;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;Ljava/util/Set;)V

    .line 2191
    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v2, v1}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    .line 2193
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 115
    return-object v0
.end method

.method public final a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;Z)Ljava/lang/Object;
    .registers 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 122
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    invoke-static {p4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-lez v0, :cond_29

    move v0, v1

    :goto_f
    const-string v3, "timeout must be positive: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 126
    iget-object v0, p0, Lcom/a/b/n/a/fr;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v2

    .line 128
    if-eqz p5, :cond_37

    .line 130
    :try_start_24
    invoke-interface {v2, p2, p3, p4}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_27
    .catch Ljava/lang/InterruptedException; {:try_start_24 .. :try_end_27} :catch_2b
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_24 .. :try_end_27} :catch_31
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_24 .. :try_end_27} :catch_3c

    move-result-object v0

    .line 136
    :goto_28
    return-object v0

    :cond_29
    move v0, v2

    .line 124
    goto :goto_f

    .line 131
    :catch_2b
    move-exception v0

    .line 132
    const/4 v3, 0x1

    :try_start_2d
    invoke-interface {v2, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 133
    throw v0
    :try_end_31
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2d .. :try_end_31} :catch_31
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2d .. :try_end_31} :catch_3c

    .line 140
    :catch_31
    move-exception v0

    invoke-static {v0, v1}, Lcom/a/b/n/a/fr;->a(Ljava/lang/Exception;Z)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 136
    :cond_37
    :try_start_37
    invoke-static {v2, p2, p3, p4}, Lcom/a/b/n/a/gs;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_3a
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_37 .. :try_end_3a} :catch_31
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_37 .. :try_end_3a} :catch_3c

    move-result-object v0

    goto :goto_28

    .line 141
    :catch_3c
    move-exception v0

    .line 142
    invoke-interface {v2, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 143
    new-instance v1, Lcom/a/b/n/a/gr;

    invoke-direct {v1, v0}, Lcom/a/b/n/a/gr;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
