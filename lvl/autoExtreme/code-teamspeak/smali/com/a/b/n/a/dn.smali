.class final Lcom/a/b/n/a/dn;
.super Lcom/a/b/n/a/cb;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/dp;


# static fields
.field private static final a:Ljava/util/concurrent/ThreadFactory;

.field private static final b:Ljava/util/concurrent/Executor;


# instance fields
.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Lcom/a/b/n/a/bu;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final f:Ljava/util/concurrent/Future;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x0

    .line 112
    new-instance v0, Lcom/a/b/n/a/gl;

    invoke-direct {v0}, Lcom/a/b/n/a/gl;-><init>()V

    invoke-virtual {v0}, Lcom/a/b/n/a/gl;->a()Lcom/a/b/n/a/gl;

    move-result-object v0

    const-string v1, "ListenableFutureAdapter-thread-%d"

    .line 2071
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 2072
    iput-object v1, v0, Lcom/a/b/n/a/gl;->a:Ljava/lang/String;

    .line 112
    invoke-virtual {v0}, Lcom/a/b/n/a/gl;->b()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    .line 117
    sput-object v0, Lcom/a/b/n/a/dn;->a:Ljava/util/concurrent/ThreadFactory;

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/dn;->b:Ljava/util/concurrent/Executor;

    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/Future;)V
    .registers 3

    .prologue
    .line 133
    sget-object v0, Lcom/a/b/n/a/dn;->b:Ljava/util/concurrent/Executor;

    invoke-direct {p0, p1, v0}, Lcom/a/b/n/a/dn;-><init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Executor;)V

    .line 134
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Executor;)V
    .registers 5

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/a/b/n/a/cb;-><init>()V

    .line 123
    new-instance v0, Lcom/a/b/n/a/bu;

    invoke-direct {v0}, Lcom/a/b/n/a/bu;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/dn;->d:Lcom/a/b/n/a/bu;

    .line 127
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/a/b/n/a/dn;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 137
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    iput-object v0, p0, Lcom/a/b/n/a/dn;->f:Ljava/util/concurrent/Future;

    .line 138
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/a/b/n/a/dn;->c:Ljava/util/concurrent/Executor;

    .line 139
    return-void
.end method

.method static synthetic a(Lcom/a/b/n/a/dn;)Ljava/util/concurrent/Future;
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/a/b/n/a/dn;->f:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/n/a/dn;)Lcom/a/b/n/a/bu;
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/a/b/n/a/dn;->d:Lcom/a/b/n/a/bu;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .registers 6

    .prologue
    .line 148
    iget-object v0, p0, Lcom/a/b/n/a/dn;->d:Lcom/a/b/n/a/bu;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/n/a/bu;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 152
    iget-object v0, p0, Lcom/a/b/n/a/dn;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 153
    iget-object v0, p0, Lcom/a/b/n/a/dn;->f:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 156
    iget-object v0, p0, Lcom/a/b/n/a/dn;->d:Lcom/a/b/n/a/bu;

    invoke-virtual {v0}, Lcom/a/b/n/a/bu;->a()V

    .line 181
    :cond_1c
    :goto_1c
    return-void

    .line 160
    :cond_1d
    iget-object v0, p0, Lcom/a/b/n/a/dn;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/a/b/n/a/do;

    invoke-direct {v1, p0}, Lcom/a/b/n/a/do;-><init>(Lcom/a/b/n/a/dn;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1c
.end method

.method protected final b()Ljava/util/concurrent/Future;
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/a/b/n/a/dn;->f:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 109
    .line 1143
    iget-object v0, p0, Lcom/a/b/n/a/dn;->f:Ljava/util/concurrent/Future;

    .line 109
    return-object v0
.end method
