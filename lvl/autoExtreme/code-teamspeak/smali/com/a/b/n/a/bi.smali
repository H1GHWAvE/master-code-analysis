.class final Lcom/a/b/n/a/bi;
.super Ljava/util/concurrent/locks/ReentrantReadWriteLock;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/bf;


# instance fields
.field final synthetic a:Lcom/a/b/n/a/bd;

.field private final b:Lcom/a/b/n/a/bh;

.field private final c:Lcom/a/b/n/a/bj;

.field private final d:Lcom/a/b/n/a/bl;


# direct methods
.method private constructor <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;)V
    .registers 4

    .prologue
    .line 879
    iput-object p1, p0, Lcom/a/b/n/a/bi;->a:Lcom/a/b/n/a/bd;

    .line 880
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    .line 881
    new-instance v0, Lcom/a/b/n/a/bh;

    invoke-direct {v0, p1, p0}, Lcom/a/b/n/a/bh;-><init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bi;)V

    iput-object v0, p0, Lcom/a/b/n/a/bi;->b:Lcom/a/b/n/a/bh;

    .line 882
    new-instance v0, Lcom/a/b/n/a/bj;

    invoke-direct {v0, p1, p0}, Lcom/a/b/n/a/bj;-><init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bi;)V

    iput-object v0, p0, Lcom/a/b/n/a/bi;->c:Lcom/a/b/n/a/bj;

    .line 883
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bl;

    iput-object v0, p0, Lcom/a/b/n/a/bi;->d:Lcom/a/b/n/a/bl;

    .line 884
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;B)V
    .registers 4

    .prologue
    .line 866
    invoke-direct {p0, p1, p2}, Lcom/a/b/n/a/bi;-><init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/n/a/bl;
    .registers 2

    .prologue
    .line 902
    iget-object v0, p0, Lcom/a/b/n/a/bi;->d:Lcom/a/b/n/a/bl;

    return-object v0
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 907
    invoke-virtual {p0}, Lcom/a/b/n/a/bi;->isWriteLockedByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/n/a/bi;->getReadHoldCount()I

    move-result v0

    if-lez v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final bridge synthetic readLock()Ljava/util/concurrent/locks/Lock;
    .registers 2

    .prologue
    .line 866
    invoke-virtual {p0}, Lcom/a/b/n/a/bi;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    return-object v0
.end method

.method public final readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
    .registers 2

    .prologue
    .line 890
    iget-object v0, p0, Lcom/a/b/n/a/bi;->b:Lcom/a/b/n/a/bh;

    return-object v0
.end method

.method public final bridge synthetic writeLock()Ljava/util/concurrent/locks/Lock;
    .registers 2

    .prologue
    .line 866
    invoke-virtual {p0}, Lcom/a/b/n/a/bi;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    return-object v0
.end method

.method public final writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;
    .registers 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/a/b/n/a/bi;->c:Lcom/a/b/n/a/bj;

    return-object v0
.end method
