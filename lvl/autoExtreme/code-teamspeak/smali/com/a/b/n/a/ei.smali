.class final Lcom/a/b/n/a/ei;
.super Lcom/a/b/n/a/eh;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/dv;


# instance fields
.field final a:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;)V
    .registers 3

    .prologue
    .line 560
    invoke-direct {p0, p1}, Lcom/a/b/n/a/eh;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 561
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 562
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Lcom/a/b/n/a/dr;
    .registers 15

    .prologue
    .line 584
    new-instance v1, Lcom/a/b/n/a/ek;

    invoke-direct {v1, p1}, Lcom/a/b/n/a/ek;-><init>(Ljava/lang/Runnable;)V

    .line 586
    iget-object v0, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 588
    new-instance v2, Lcom/a/b/n/a/ej;

    invoke-direct {v2, v1, v0}, Lcom/a/b/n/a/ej;-><init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V

    return-object v2
.end method

.method public final a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lcom/a/b/n/a/dr;
    .registers 9

    .prologue
    .line 567
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/a/b/n/a/dq;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;

    move-result-object v0

    .line 569
    iget-object v1, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1, v0, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    .line 570
    new-instance v2, Lcom/a/b/n/a/ej;

    invoke-direct {v2, v0, v1}, Lcom/a/b/n/a/ej;-><init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V

    return-object v2
.end method

.method public final a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Lcom/a/b/n/a/dr;
    .registers 9

    .prologue
    .line 576
    invoke-static {p1}, Lcom/a/b/n/a/dq;->a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;

    move-result-object v0

    .line 577
    iget-object v1, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1, v0, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    .line 578
    new-instance v2, Lcom/a/b/n/a/ej;

    invoke-direct {v2, v0, v1}, Lcom/a/b/n/a/ej;-><init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V

    return-object v2
.end method

.method public final b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Lcom/a/b/n/a/dr;
    .registers 15

    .prologue
    .line 594
    new-instance v1, Lcom/a/b/n/a/ek;

    invoke-direct {v1, p1}, Lcom/a/b/n/a/ek;-><init>(Ljava/lang/Runnable;)V

    .line 596
    iget-object v0, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 598
    new-instance v2, Lcom/a/b/n/a/ej;

    invoke-direct {v2, v1, v0}, Lcom/a/b/n/a/ej;-><init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V

    return-object v2
.end method

.method public final synthetic schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .registers 9

    .prologue
    .line 554
    .line 4567
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/a/b/n/a/dq;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;

    move-result-object v0

    .line 4569
    iget-object v1, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1, v0, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    .line 4570
    new-instance v2, Lcom/a/b/n/a/ej;

    invoke-direct {v2, v0, v1}, Lcom/a/b/n/a/ej;-><init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V

    .line 554
    return-object v2
.end method

.method public final synthetic schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .registers 9

    .prologue
    .line 554
    .line 3576
    invoke-static {p1}, Lcom/a/b/n/a/dq;->a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;

    move-result-object v0

    .line 3577
    iget-object v1, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1, v0, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    .line 3578
    new-instance v2, Lcom/a/b/n/a/ej;

    invoke-direct {v2, v0, v1}, Lcom/a/b/n/a/ej;-><init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V

    .line 554
    return-object v2
.end method

.method public final synthetic scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .registers 15

    .prologue
    .line 554
    .line 2584
    new-instance v1, Lcom/a/b/n/a/ek;

    invoke-direct {v1, p1}, Lcom/a/b/n/a/ek;-><init>(Ljava/lang/Runnable;)V

    .line 2586
    iget-object v0, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 2588
    new-instance v2, Lcom/a/b/n/a/ej;

    invoke-direct {v2, v1, v0}, Lcom/a/b/n/a/ej;-><init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V

    .line 554
    return-object v2
.end method

.method public final synthetic scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .registers 15

    .prologue
    .line 554
    .line 1594
    new-instance v1, Lcom/a/b/n/a/ek;

    invoke-direct {v1, p1}, Lcom/a/b/n/a/ek;-><init>(Ljava/lang/Runnable;)V

    .line 1596
    iget-object v0, p0, Lcom/a/b/n/a/ei;->a:Ljava/util/concurrent/ScheduledExecutorService;

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 1598
    new-instance v2, Lcom/a/b/n/a/ej;

    invoke-direct {v2, v1, v0}, Lcom/a/b/n/a/ej;-><init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V

    .line 554
    return-object v2
.end method
