.class final Lcom/a/b/n/a/y;
.super Lcom/a/b/n/a/cb;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:Lcom/a/b/n/a/x;

.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/util/concurrent/ScheduledExecutorService;

.field private final d:Lcom/a/b/n/a/ad;

.field private final e:Ljava/util/concurrent/locks/ReentrantLock;

.field private f:Ljava/util/concurrent/Future;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/a/b/n/a/x;Lcom/a/b/n/a/ad;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;)V
    .registers 6

    .prologue
    .line 429
    iput-object p1, p0, Lcom/a/b/n/a/y;->a:Lcom/a/b/n/a/x;

    invoke-direct {p0}, Lcom/a/b/n/a/cb;-><init>()V

    .line 422
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/y;->e:Ljava/util/concurrent/locks/ReentrantLock;

    .line 430
    iput-object p4, p0, Lcom/a/b/n/a/y;->b:Ljava/lang/Runnable;

    .line 431
    iput-object p3, p0, Lcom/a/b/n/a/y;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 432
    iput-object p2, p0, Lcom/a/b/n/a/y;->d:Lcom/a/b/n/a/ad;

    .line 433
    return-void
.end method

.method private c()Ljava/lang/Void;
    .registers 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/a/b/n/a/y;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 438
    invoke-virtual {p0}, Lcom/a/b/n/a/y;->a()V

    .line 439
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 5

    .prologue
    .line 450
    iget-object v0, p0, Lcom/a/b/n/a/y;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 452
    :try_start_5
    iget-object v0, p0, Lcom/a/b/n/a/y;->f:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/a/b/n/a/y;->f:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_23

    .line 453
    :cond_11
    iget-object v0, p0, Lcom/a/b/n/a/y;->a:Lcom/a/b/n/a/x;

    invoke-virtual {v0}, Lcom/a/b/n/a/x;->a()Lcom/a/b/n/a/z;

    move-result-object v0

    .line 454
    iget-object v1, p0, Lcom/a/b/n/a/y;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1502
    iget-wide v2, v0, Lcom/a/b/n/a/z;->a:J

    .line 2502
    iget-object v0, v0, Lcom/a/b/n/a/z;->b:Ljava/util/concurrent/TimeUnit;

    .line 454
    invoke-interface {v1, p0, v2, v3, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/y;->f:Ljava/util/concurrent/Future;
    :try_end_23
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_23} :catch_29
    .catchall {:try_start_5 .. :try_end_23} :catchall_35

    .line 464
    :cond_23
    iget-object v0, p0, Lcom/a/b/n/a/y;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 465
    :goto_28
    return-void

    .line 456
    :catch_29
    move-exception v0

    .line 462
    :try_start_2a
    iget-object v1, p0, Lcom/a/b/n/a/y;->d:Lcom/a/b/n/a/ad;

    invoke-virtual {v1, v0}, Lcom/a/b/n/a/ad;->a(Ljava/lang/Throwable;)V
    :try_end_2f
    .catchall {:try_start_2a .. :try_end_2f} :catchall_35

    .line 464
    iget-object v0, p0, Lcom/a/b/n/a/y;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_28

    :catchall_35
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/y;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method protected final b()Ljava/util/concurrent/Future;
    .registers 3

    .prologue
    .line 483
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only cancel is supported by this future"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic call()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 403
    invoke-direct {p0}, Lcom/a/b/n/a/y;->c()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final cancel(Z)Z
    .registers 4

    .prologue
    .line 473
    iget-object v0, p0, Lcom/a/b/n/a/y;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 475
    :try_start_5
    iget-object v0, p0, Lcom/a/b/n/a/y;->f:Ljava/util/concurrent/Future;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_11

    move-result v0

    .line 477
    iget-object v1, p0, Lcom/a/b/n/a/y;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_11
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/y;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/a/b/n/a/y;->b()Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method
