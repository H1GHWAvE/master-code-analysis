.class Lcom/a/b/n/a/cu;
.super Lcom/a/b/n/a/g;
.source "SourceFile"


# static fields
.field private static final i:Ljava/util/logging/Logger;


# instance fields
.field b:Lcom/a/b/d/iz;

.field final c:Z

.field final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field e:Lcom/a/b/n/a/db;

.field f:Ljava/util/List;

.field final g:Ljava/lang/Object;

.field h:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1609
    const-class v0, Lcom/a/b/n/a/cu;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/cu;->i:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>(Lcom/a/b/d/iz;ZLjava/util/concurrent/Executor;Lcom/a/b/n/a/db;)V
    .registers 7

    .prologue
    .line 1623
    invoke-direct {p0}, Lcom/a/b/n/a/g;-><init>()V

    .line 1617
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/cu;->g:Ljava/lang/Object;

    .line 1624
    iput-object p1, p0, Lcom/a/b/n/a/cu;->b:Lcom/a/b/d/iz;

    .line 1625
    iput-boolean p2, p0, Lcom/a/b/n/a/cu;->c:Z

    .line 1626
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Lcom/a/b/d/iz;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1627
    iput-object p4, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 1628
    invoke-virtual {p1}, Lcom/a/b/d/iz;->size()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/cu;->f:Ljava/util/List;

    .line 1629
    invoke-direct {p0, p3}, Lcom/a/b/n/a/cu;->a(Ljava/util/concurrent/Executor;)V

    .line 1630
    return-void
.end method

.method private a(ILjava/util/concurrent/Future;)V
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1724
    iget-object v3, p0, Lcom/a/b/n/a/cu;->f:Ljava/util/List;

    .line 1732
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    if-nez v0, :cond_c

    if-nez v3, :cond_1c

    .line 1737
    :cond_c
    iget-boolean v0, p0, Lcom/a/b/n/a/cu;->c:Z

    if-nez v0, :cond_16

    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_4f

    :cond_16
    move v0, v2

    :goto_17
    const-string v4, "Future was done before all dependencies completed"

    invoke-static {v0, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1742
    :cond_1c
    :try_start_1c
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    const-string v4, "Tried to set value from future which is not done"

    invoke-static {v0, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1744
    invoke-static {p2}, Lcom/a/b/n/a/gs;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    .line 1745
    if-eqz v3, :cond_32

    .line 1746
    invoke-static {v0}, Lcom/a/b/b/ci;->c(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    invoke-interface {v3, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_32
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1c .. :try_end_32} :catch_5b
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1c .. :try_end_32} :catch_8b
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_32} :catch_ba
    .catchall {:try_start_1c .. :try_end_32} :catchall_e7

    .line 1759
    :cond_32
    iget-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 1760
    if-ltz v0, :cond_51

    :goto_3a
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1761
    if-nez v0, :cond_4e

    .line 1762
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 1763
    if-eqz v0, :cond_53

    if-eqz v3, :cond_53

    .line 1764
    invoke-interface {v0, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    .line 1769
    :cond_4e
    :goto_4e
    return-void

    :cond_4f
    move v0, v1

    .line 1737
    goto :goto_17

    :cond_51
    move v2, v1

    .line 1760
    goto :goto_3a

    .line 1766
    :cond_53
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    goto :goto_4e

    .line 1749
    :catch_5b
    move-exception v0

    :try_start_5c
    iget-boolean v0, p0, Lcom/a/b/n/a/cu;->c:Z

    if-eqz v0, :cond_64

    .line 1752
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->cancel(Z)Z
    :try_end_64
    .catchall {:try_start_5c .. :try_end_64} :catchall_e7

    .line 1759
    :cond_64
    iget-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 1760
    if-ltz v0, :cond_81

    :goto_6c
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1761
    if-nez v0, :cond_4e

    .line 1762
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 1763
    if-eqz v0, :cond_83

    if-eqz v3, :cond_83

    .line 1764
    invoke-interface {v0, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    goto :goto_4e

    :cond_81
    move v2, v1

    .line 1760
    goto :goto_6c

    .line 1766
    :cond_83
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    goto :goto_4e

    .line 1754
    :catch_8b
    move-exception v0

    .line 1755
    :try_start_8c
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/n/a/cu;->b(Ljava/lang/Throwable;)V
    :try_end_93
    .catchall {:try_start_8c .. :try_end_93} :catchall_e7

    .line 1759
    iget-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 1760
    if-ltz v0, :cond_b0

    :goto_9b
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1761
    if-nez v0, :cond_4e

    .line 1762
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 1763
    if-eqz v0, :cond_b2

    if-eqz v3, :cond_b2

    .line 1764
    invoke-interface {v0, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    goto :goto_4e

    :cond_b0
    move v2, v1

    .line 1760
    goto :goto_9b

    .line 1766
    :cond_b2
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    goto :goto_4e

    .line 1756
    :catch_ba
    move-exception v0

    .line 1757
    :try_start_bb
    invoke-direct {p0, v0}, Lcom/a/b/n/a/cu;->b(Ljava/lang/Throwable;)V
    :try_end_be
    .catchall {:try_start_bb .. :try_end_be} :catchall_e7

    .line 1759
    iget-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 1760
    if-ltz v0, :cond_dc

    :goto_c6
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1761
    if-nez v0, :cond_4e

    .line 1762
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 1763
    if-eqz v0, :cond_de

    if-eqz v3, :cond_de

    .line 1764
    invoke-interface {v0, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    goto/16 :goto_4e

    :cond_dc
    move v2, v1

    .line 1760
    goto :goto_c6

    .line 1766
    :cond_de
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    goto/16 :goto_4e

    .line 1759
    :catchall_e7
    move-exception v0

    iget-object v4, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v4

    .line 1760
    if-ltz v4, :cond_105

    :goto_f0
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 1761
    if-nez v4, :cond_104

    .line 1762
    iget-object v1, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 1763
    if-eqz v1, :cond_107

    if-eqz v3, :cond_107

    .line 1764
    invoke-interface {v1, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    .line 1769
    :cond_104
    :goto_104
    throw v0

    :cond_105
    move v2, v1

    .line 1760
    goto :goto_f0

    .line 1766
    :cond_107
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v1

    invoke-static {v1}, Lcom/a/b/b/cn;->b(Z)V

    goto :goto_104
.end method

.method static synthetic a(Lcom/a/b/n/a/cu;ILjava/util/concurrent/Future;)V
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1608
    .line 3724
    iget-object v3, p0, Lcom/a/b/n/a/cu;->f:Ljava/util/List;

    .line 3732
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    if-nez v0, :cond_c

    if-nez v3, :cond_1c

    .line 3737
    :cond_c
    iget-boolean v0, p0, Lcom/a/b/n/a/cu;->c:Z

    if-nez v0, :cond_16

    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_4f

    :cond_16
    move v0, v2

    :goto_17
    const-string v4, "Future was done before all dependencies completed"

    invoke-static {v0, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 3742
    :cond_1c
    :try_start_1c
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    const-string v4, "Tried to set value from future which is not done"

    invoke-static {v0, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 3744
    invoke-static {p2}, Lcom/a/b/n/a/gs;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    .line 3745
    if-eqz v3, :cond_32

    .line 3746
    invoke-static {v0}, Lcom/a/b/b/ci;->c(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    invoke-interface {v3, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_32
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1c .. :try_end_32} :catch_5b
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1c .. :try_end_32} :catch_8b
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_32} :catch_ba
    .catchall {:try_start_1c .. :try_end_32} :catchall_e7

    .line 3759
    :cond_32
    iget-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 3760
    if-ltz v0, :cond_51

    :goto_3a
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 3761
    if-nez v0, :cond_4e

    .line 3762
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 3763
    if-eqz v0, :cond_53

    if-eqz v3, :cond_53

    .line 3764
    invoke-interface {v0, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    .line 3769
    :cond_4e
    :goto_4e
    return-void

    :cond_4f
    move v0, v1

    .line 3737
    goto :goto_17

    :cond_51
    move v2, v1

    .line 3760
    goto :goto_3a

    .line 3766
    :cond_53
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    goto :goto_4e

    .line 3749
    :catch_5b
    move-exception v0

    :try_start_5c
    iget-boolean v0, p0, Lcom/a/b/n/a/cu;->c:Z

    if-eqz v0, :cond_64

    .line 3752
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->cancel(Z)Z
    :try_end_64
    .catchall {:try_start_5c .. :try_end_64} :catchall_e7

    .line 3759
    :cond_64
    iget-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 3760
    if-ltz v0, :cond_81

    :goto_6c
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 3761
    if-nez v0, :cond_4e

    .line 3762
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 3763
    if-eqz v0, :cond_83

    if-eqz v3, :cond_83

    .line 3764
    invoke-interface {v0, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    goto :goto_4e

    :cond_81
    move v2, v1

    .line 3760
    goto :goto_6c

    .line 3766
    :cond_83
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    goto :goto_4e

    .line 3754
    :catch_8b
    move-exception v0

    .line 3755
    :try_start_8c
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/n/a/cu;->b(Ljava/lang/Throwable;)V
    :try_end_93
    .catchall {:try_start_8c .. :try_end_93} :catchall_e7

    .line 3759
    iget-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 3760
    if-ltz v0, :cond_b0

    :goto_9b
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 3761
    if-nez v0, :cond_4e

    .line 3762
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 3763
    if-eqz v0, :cond_b2

    if-eqz v3, :cond_b2

    .line 3764
    invoke-interface {v0, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    goto :goto_4e

    :cond_b0
    move v2, v1

    .line 3760
    goto :goto_9b

    .line 3766
    :cond_b2
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    goto :goto_4e

    .line 3756
    :catch_ba
    move-exception v0

    .line 3757
    :try_start_bb
    invoke-direct {p0, v0}, Lcom/a/b/n/a/cu;->b(Ljava/lang/Throwable;)V
    :try_end_be
    .catchall {:try_start_bb .. :try_end_be} :catchall_e7

    .line 3759
    iget-object v0, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 3760
    if-ltz v0, :cond_dc

    :goto_c6
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 3761
    if-nez v0, :cond_4e

    .line 3762
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 3763
    if-eqz v0, :cond_de

    if-eqz v3, :cond_de

    .line 3764
    invoke-interface {v0, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    goto/16 :goto_4e

    :cond_dc
    move v2, v1

    .line 3760
    goto :goto_c6

    .line 3766
    :cond_de
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    goto/16 :goto_4e

    .line 3759
    :catchall_e7
    move-exception v0

    iget-object v4, p0, Lcom/a/b/n/a/cu;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v4

    .line 3760
    if-ltz v4, :cond_105

    :goto_f0
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v2, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 3761
    if-nez v4, :cond_104

    .line 3762
    iget-object v1, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    .line 3763
    if-eqz v1, :cond_107

    if-eqz v3, :cond_107

    .line 3764
    invoke-interface {v1, v3}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    .line 3769
    :cond_104
    :goto_104
    throw v0

    :cond_105
    move v2, v1

    .line 3760
    goto :goto_f0

    .line 3766
    :cond_107
    invoke-virtual {p0}, Lcom/a/b/n/a/cu;->isDone()Z

    move-result v1

    invoke-static {v1}, Lcom/a/b/b/cn;->b(Z)V

    goto :goto_104
.end method

.method private a(Ljava/util/concurrent/Executor;)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 1637
    new-instance v0, Lcom/a/b/n/a/cv;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/cv;-><init>(Lcom/a/b/n/a/cu;)V

    .line 2450
    sget-object v2, Lcom/a/b/n/a/ef;->a:Lcom/a/b/n/a/ef;

    .line 1637
    invoke-virtual {p0, v0, v2}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1662
    iget-object v0, p0, Lcom/a/b/n/a/cu;->b:Lcom/a/b/d/iz;

    invoke-virtual {v0}, Lcom/a/b/d/iz;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1663
    iget-object v0, p0, Lcom/a/b/n/a/cu;->e:Lcom/a/b/n/a/db;

    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/n/a/db;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/cu;->a(Ljava/lang/Object;)Z

    .line 1690
    :cond_20
    return-void

    :cond_21
    move v0, v1

    .line 1668
    :goto_22
    iget-object v2, p0, Lcom/a/b/n/a/cu;->b:Lcom/a/b/d/iz;

    invoke-virtual {v2}, Lcom/a/b/d/iz;->size()I

    move-result v2

    if-ge v0, v2, :cond_33

    .line 1669
    iget-object v2, p0, Lcom/a/b/n/a/cu;->f:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 1681
    :cond_33
    iget-object v0, p0, Lcom/a/b/n/a/cu;->b:Lcom/a/b/d/iz;

    invoke-virtual {v0}, Lcom/a/b/d/iz;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_39
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    .line 1682
    add-int/lit8 v2, v1, 0x1

    .line 1683
    new-instance v4, Lcom/a/b/n/a/cw;

    invoke-direct {v4, p0, v1, v0}, Lcom/a/b/n/a/cw;-><init>(Lcom/a/b/n/a/cu;ILcom/a/b/n/a/dp;)V

    invoke-interface {v0, v4, p1}, Lcom/a/b/n/a/dp;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    move v1, v2

    .line 1689
    goto :goto_39
.end method

.method private b(Ljava/lang/Throwable;)V
    .registers 5

    .prologue
    .line 1699
    const/4 v1, 0x0

    .line 1700
    const/4 v0, 0x1

    .line 1701
    iget-boolean v2, p0, Lcom/a/b/n/a/cu;->c:Z

    if-eqz v2, :cond_1f

    .line 1704
    invoke-super {p0, p1}, Lcom/a/b/n/a/g;->a(Ljava/lang/Throwable;)Z

    move-result v1

    .line 1706
    iget-object v2, p0, Lcom/a/b/n/a/cu;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 1707
    :try_start_d
    iget-object v0, p0, Lcom/a/b/n/a/cu;->h:Ljava/util/Set;

    if-nez v0, :cond_18

    .line 3164
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1708
    iput-object v0, p0, Lcom/a/b/n/a/cu;->h:Ljava/util/Set;

    .line 1710
    :cond_18
    iget-object v0, p0, Lcom/a/b/n/a/cu;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 1711
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_d .. :try_end_1f} :catchall_35

    .line 1714
    :cond_1f
    instance-of v2, p1, Ljava/lang/Error;

    if-nez v2, :cond_2b

    iget-boolean v2, p0, Lcom/a/b/n/a/cu;->c:Z

    if-eqz v2, :cond_34

    if-nez v1, :cond_34

    if-eqz v0, :cond_34

    .line 1716
    :cond_2b
    sget-object v0, Lcom/a/b/n/a/cu;->i:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "input future failed."

    invoke-virtual {v0, v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718
    :cond_34
    return-void

    .line 1711
    :catchall_35
    move-exception v0

    :try_start_36
    monitor-exit v2
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    throw v0
.end method
