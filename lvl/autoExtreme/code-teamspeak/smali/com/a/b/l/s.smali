.class final Lcom/a/b/l/s;
.super Lcom/a/b/b/ak;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Lcom/a/b/l/s;

.field private static final b:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 340
    new-instance v0, Lcom/a/b/l/s;

    invoke-direct {v0}, Lcom/a/b/l/s;-><init>()V

    sput-object v0, Lcom/a/b/l/s;->a:Lcom/a/b/l/s;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/a/b/b/ak;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Integer;
    .registers 2

    .prologue
    .line 344
    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Integer;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 349
    invoke-virtual {p0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 358
    sget-object v0, Lcom/a/b/l/s;->a:Lcom/a/b/l/s;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 338
    check-cast p1, Ljava/lang/Integer;

    .line 1349
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 338
    return-object v0
.end method

.method protected final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 338
    check-cast p1, Ljava/lang/String;

    .line 2344
    invoke-static {p1}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 338
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 354
    const-string v0, "Ints.stringConverter()"

    return-object v0
.end method
