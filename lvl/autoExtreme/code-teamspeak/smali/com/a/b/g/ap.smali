.class abstract enum Lcom/a/b/g/ap;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/dz;


# static fields
.field public static final enum a:Lcom/a/b/g/ap;

.field public static final enum b:Lcom/a/b/g/ap;

.field private static final synthetic d:[Lcom/a/b/g/ap;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 283
    new-instance v0, Lcom/a/b/g/aq;

    const-string v1, "CRC_32"

    invoke-direct {v0, v1}, Lcom/a/b/g/aq;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/g/ap;->a:Lcom/a/b/g/ap;

    .line 289
    new-instance v0, Lcom/a/b/g/ar;

    const-string v1, "ADLER_32"

    invoke-direct {v0, v1}, Lcom/a/b/g/ar;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/g/ap;->b:Lcom/a/b/g/ap;

    .line 282
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/g/ap;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/g/ap;->a:Lcom/a/b/g/ap;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/g/ap;->b:Lcom/a/b/g/ap;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/g/ap;->d:[Lcom/a/b/g/ap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 4

    .prologue
    .line 298
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 299
    const/16 v0, 0x20

    iput v0, p0, Lcom/a/b/g/ap;->c:I

    .line 300
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 282
    invoke-direct {p0, p1, p2}, Lcom/a/b/g/ap;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/a/b/g/ap;)I
    .registers 2

    .prologue
    .line 282
    iget v0, p0, Lcom/a/b/g/ap;->c:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/g/ap;
    .registers 2

    .prologue
    .line 282
    const-class v0, Lcom/a/b/g/ap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/ap;

    return-object v0
.end method

.method public static values()[Lcom/a/b/g/ap;
    .registers 1

    .prologue
    .line 282
    sget-object v0, Lcom/a/b/g/ap;->d:[Lcom/a/b/g/ap;

    invoke-virtual {v0}, [Lcom/a/b/g/ap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/g/ap;

    return-object v0
.end method


# virtual methods
.method public synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/a/b/g/ap;->b()Ljava/util/zip/Checksum;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()Ljava/util/zip/Checksum;
.end method
