.class public final Lcom/a/b/g/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final e:Lcom/a/b/g/m;


# instance fields
.field private final a:Lcom/a/b/g/q;

.field private final b:I

.field private final c:Lcom/a/b/g/w;

.field private final d:Lcom/a/b/g/m;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 262
    sget-object v0, Lcom/a/b/g/n;->b:Lcom/a/b/g/n;

    sput-object v0, Lcom/a/b/g/j;->e:Lcom/a/b/g/m;

    return-void
.end method

.method private constructor <init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    if-lez p2, :cond_42

    move v0, v1

    :goto_8
    const-string v3, "numHashFunctions (%s) must be > 0"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 115
    const/16 v0, 0xff

    if-gt p2, v0, :cond_44

    move v0, v1

    :goto_1a
    const-string v3, "numHashFunctions (%s) must be <= 255"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 117
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/q;

    iput-object v0, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    .line 118
    iput p2, p0, Lcom/a/b/g/j;->b:I

    .line 119
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/w;

    iput-object v0, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    .line 120
    invoke-static {p4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/m;

    iput-object v0, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    .line 121
    return-void

    :cond_42
    move v0, v2

    .line 113
    goto :goto_8

    :cond_44
    move v0, v2

    .line 115
    goto :goto_1a
.end method

.method synthetic constructor <init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;B)V
    .registers 6

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/b/g/j;-><init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V

    return-void
.end method

.method private static a(JJ)I
    .registers 10
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 366
    const/4 v0, 0x1

    long-to-double v2, p2

    long-to-double v4, p0

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private static a(JD)J
    .registers 10
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 380
    const-wide/16 v0, 0x0

    cmpl-double v0, p2, v0

    if-nez v0, :cond_a

    .line 381
    const-wide/16 p2, 0x1

    .line 383
    :cond_a
    neg-long v0, p0

    long-to-double v0, v0

    invoke-static {p2, p3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    double-to-long v0, v0

    return-wide v0
.end method

.method private a()Lcom/a/b/g/j;
    .registers 6

    .prologue
    .line 130
    new-instance v0, Lcom/a/b/g/j;

    iget-object v1, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v1}, Lcom/a/b/g/q;->b()Lcom/a/b/g/q;

    move-result-object v1

    iget v2, p0, Lcom/a/b/g/j;->b:I

    iget-object v3, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    iget-object v4, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/a/b/g/j;-><init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/g/w;I)Lcom/a/b/g/j;
    .registers 3

    .prologue
    .line 288
    sget-object v0, Lcom/a/b/g/j;->e:Lcom/a/b/g/m;

    invoke-static {p0, p1, v0}, Lcom/a/b/g/j;->a(Lcom/a/b/g/w;ILcom/a/b/g/m;)Lcom/a/b/g/j;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/a/b/g/w;ILcom/a/b/g/m;)Lcom/a/b/g/j;
    .registers 13
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const-wide v6, 0x3f9eb851eb851eb8L    # 0.03

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 294
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    if-ltz p1, :cond_6d

    move v0, v1

    :goto_f
    const-string v3, "Expected insertions (%s) must be >= 0"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 297
    const-string v0, "False positive probability (%s) must be > 0.0"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v0, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 298
    const-string v0, "False positive probability (%s) must be < 1.0"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v1, v0, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 299
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    if-nez p1, :cond_3c

    move p1, v1

    .line 310
    :cond_3c
    int-to-long v2, p1

    .line 8383
    neg-long v2, v2

    long-to-double v2, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    double-to-long v2, v2

    .line 311
    int-to-long v4, p1

    .line 9366
    long-to-double v6, v2

    long-to-double v4, v4

    div-double v4, v6, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 313
    :try_start_62
    new-instance v1, Lcom/a/b/g/j;

    new-instance v4, Lcom/a/b/g/q;

    invoke-direct {v4, v2, v3}, Lcom/a/b/g/q;-><init>(J)V

    invoke-direct {v1, v4, v0, p0, p2}, Lcom/a/b/g/j;-><init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V
    :try_end_6c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_62 .. :try_end_6c} :catch_6f

    return-object v1

    :cond_6d
    move v0, v2

    .line 295
    goto :goto_f

    .line 314
    :catch_6f
    move-exception v0

    .line 315
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x39

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Could not create BloomFilter of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bits"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/io/InputStream;Lcom/a/b/g/w;)Lcom/a/b/g/j;
    .registers 12

    .prologue
    const/4 v1, -0x1

    .line 445
    const-string v0, "InputStream"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    const-string v0, "Funnel"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    :try_start_b
    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 455
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readByte()B
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_13} :catch_3d

    move-result v3

    .line 456
    :try_start_14
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readByte()B
    :try_end_17
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_17} :catch_7e

    move-result v0

    .line 12075
    and-int/lit16 v2, v0, 0xff

    .line 457
    :try_start_1a
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 459
    invoke-static {}, Lcom/a/b/g/n;->values()[Lcom/a/b/g/n;

    move-result-object v0

    aget-object v5, v0, v3

    .line 460
    new-array v6, v1, [J

    .line 461
    const/4 v0, 0x0

    :goto_27
    if-ge v0, v1, :cond_32

    .line 462
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v8

    aput-wide v8, v6, v0

    .line 461
    add-int/lit8 v0, v0, 0x1

    goto :goto_27

    .line 464
    :cond_32
    new-instance v0, Lcom/a/b/g/j;

    new-instance v4, Lcom/a/b/g/q;

    invoke-direct {v4, v6}, Lcom/a/b/g/q;-><init>([J)V

    invoke-direct {v0, v4, v2, p1, v5}, Lcom/a/b/g/j;-><init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V
    :try_end_3c
    .catch Ljava/lang/RuntimeException; {:try_start_1a .. :try_end_3c} :catch_81

    return-object v0

    .line 465
    :catch_3d
    move-exception v0

    move v2, v1

    move v3, v1

    .line 466
    :goto_40
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Unable to deserialize BloomFilter from InputStream. strategyOrdinal: "

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x41

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " numHashFunctions: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dataLength: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 471
    invoke-virtual {v4, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 472
    throw v4

    .line 465
    :catch_7e
    move-exception v0

    move v2, v1

    goto :goto_40

    :catch_81
    move-exception v0

    goto :goto_40
.end method

.method static synthetic a(Lcom/a/b/g/j;)Lcom/a/b/g/q;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    return-object v0
.end method

.method private a(Ljava/io/OutputStream;)V
    .registers 11

    .prologue
    const/16 v8, 0x22

    .line 423
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 424
    iget-object v0, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    invoke-interface {v0}, Lcom/a/b/g/m;->ordinal()I

    move-result v0

    int-to-long v2, v0

    .line 11061
    long-to-int v0, v2

    int-to-byte v0, v0

    .line 11062
    int-to-long v4, v0

    cmp-long v4, v4, v2

    if-eqz v4, :cond_2e

    .line 11064
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Out of range: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 424
    :cond_2e
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 425
    iget v0, p0, Lcom/a/b/g/j;->b:I

    int-to-long v2, v0

    .line 11089
    const/16 v0, 0x8

    shr-long v4, v2, v0

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_57

    .line 11091
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Out of range: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 11093
    :cond_57
    long-to-int v0, v2

    int-to-byte v0, v0

    .line 425
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 426
    iget-object v0, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    iget-object v0, v0, Lcom/a/b/g/q;->a:[J

    array-length v0, v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 427
    iget-object v0, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    iget-object v2, v0, Lcom/a/b/g/q;->a:[J

    array-length v3, v2

    const/4 v0, 0x0

    :goto_6a
    if-ge v0, v3, :cond_74

    aget-wide v4, v2, v0

    .line 428
    invoke-virtual {v1, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 427
    add-int/lit8 v0, v0, 0x1

    goto :goto_6a

    .line 430
    :cond_74
    return-void
.end method

.method private b()D
    .registers 5

    .prologue
    .line 180
    iget-object v0, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    .line 1180
    iget-wide v0, v0, Lcom/a/b/g/q;->b:J

    .line 180
    long-to-double v0, v0

    .line 1187
    iget-object v2, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v2}, Lcom/a/b/g/q;->a()J

    move-result-wide v2

    .line 180
    long-to-double v2, v2

    div-double/2addr v0, v2

    iget v2, p0, Lcom/a/b/g/j;->b:I

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic b(Lcom/a/b/g/j;)I
    .registers 2

    .prologue
    .line 62
    iget v0, p0, Lcom/a/b/g/j;->b:I

    return v0
.end method

.method private static b(Lcom/a/b/g/w;I)Lcom/a/b/g/j;
    .registers 3

    .prologue
    .line 337
    .line 10288
    sget-object v0, Lcom/a/b/g/j;->e:Lcom/a/b/g/m;

    invoke-static {p0, p1, v0}, Lcom/a/b/g/j;->a(Lcom/a/b/g/w;ILcom/a/b/g/m;)Lcom/a/b/g/j;

    move-result-object v0

    .line 337
    return-object v0
.end method

.method private b(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 138
    iget-object v0, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    iget-object v1, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    iget v2, p0, Lcom/a/b/g/j;->b:I

    iget-object v3, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/a/b/g/m;->b(Ljava/lang/Object;Lcom/a/b/g/w;ILcom/a/b/g/q;)Z

    move-result v0

    return v0
.end method

.method private c()J
    .registers 3
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v0}, Lcom/a/b/g/q;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic c(Lcom/a/b/g/j;)Lcom/a/b/g/w;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    return-object v0
.end method

.method private c(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 164
    iget-object v0, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    iget-object v1, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    iget v2, p0, Lcom/a/b/g/j;->b:I

    iget-object v3, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/a/b/g/m;->a(Ljava/lang/Object;Lcom/a/b/g/w;ILcom/a/b/g/q;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/a/b/g/j;)Lcom/a/b/g/m;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    return-object v0
.end method

.method private d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 387
    new-instance v0, Lcom/a/b/g/l;

    invoke-direct {v0, p0}, Lcom/a/b/g/l;-><init>(Lcom/a/b/g/j;)V

    return-object v0
.end method

.method private e(Lcom/a/b/g/j;)Z
    .registers 6

    .prologue
    .line 206
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    if-eq p0, p1, :cond_31

    iget v0, p0, Lcom/a/b/g/j;->b:I

    iget v1, p1, Lcom/a/b/g/j;->b:I

    if-ne v0, v1, :cond_31

    .line 2187
    iget-object v0, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v0}, Lcom/a/b/g/q;->a()J

    move-result-wide v0

    .line 3187
    iget-object v2, p1, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v2}, Lcom/a/b/g/q;->a()J

    move-result-wide v2

    .line 207
    cmp-long v0, v0, v2

    if-nez v0, :cond_31

    iget-object v0, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    iget-object v1, p1, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    iget-object v1, p1, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    const/4 v0, 0x1

    :goto_30
    return v0

    :cond_31
    const/4 v0, 0x0

    goto :goto_30
.end method

.method private f(Lcom/a/b/g/j;)V
    .registers 12

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 225
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    if-eq p0, p1, :cond_d5

    move v0, v1

    :goto_9
    const-string v3, "Cannot combine a BloomFilter with itself."

    invoke-static {v0, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 227
    iget v0, p0, Lcom/a/b/g/j;->b:I

    iget v3, p1, Lcom/a/b/g/j;->b:I

    if-ne v0, v3, :cond_d8

    move v0, v1

    :goto_15
    const-string v3, "BloomFilters must have the same number of hash functions (%s != %s)"

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, p0, Lcom/a/b/g/j;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget v5, p1, Lcom/a/b/g/j;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 4187
    iget-object v0, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v0}, Lcom/a/b/g/q;->a()J

    move-result-wide v4

    .line 5187
    iget-object v0, p1, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v0}, Lcom/a/b/g/q;->a()J

    move-result-wide v6

    .line 230
    cmp-long v0, v4, v6

    if-nez v0, :cond_db

    move v0, v1

    :goto_3d
    const-string v3, "BloomFilters must have the same size underlying bit arrays (%s != %s)"

    new-array v4, v8, [Ljava/lang/Object;

    .line 6187
    iget-object v5, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v5}, Lcom/a/b/g/q;->a()J

    move-result-wide v6

    .line 230
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    .line 7187
    iget-object v5, p1, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v5}, Lcom/a/b/g/q;->a()J

    move-result-wide v6

    .line 230
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 233
    iget-object v0, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    iget-object v3, p1, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "BloomFilters must have equal strategies (%s != %s)"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    aput-object v5, v4, v2

    iget-object v5, p1, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 236
    iget-object v0, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    iget-object v3, p1, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "BloomFilters must have equal funnels (%s != %s)"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    aput-object v5, v4, v2

    iget-object v5, p1, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 239
    iget-object v3, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    iget-object v4, p1, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    .line 7189
    iget-object v0, v3, Lcom/a/b/g/q;->a:[J

    array-length v0, v0

    iget-object v5, v4, Lcom/a/b/g/q;->a:[J

    array-length v5, v5

    if-ne v0, v5, :cond_de

    move v0, v1

    :goto_97
    const-string v5, "BitArrays must be of equal length (%s != %s)"

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, v3, Lcom/a/b/g/q;->a:[J

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    iget-object v7, v4, Lcom/a/b/g/q;->a:[J

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v0, v5, v6}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 7191
    const-wide/16 v0, 0x0

    iput-wide v0, v3, Lcom/a/b/g/q;->b:J

    .line 7192
    :goto_b4
    iget-object v0, v3, Lcom/a/b/g/q;->a:[J

    array-length v0, v0

    if-ge v2, v0, :cond_e0

    .line 7193
    iget-object v0, v3, Lcom/a/b/g/q;->a:[J

    aget-wide v6, v0, v2

    iget-object v1, v4, Lcom/a/b/g/q;->a:[J

    aget-wide v8, v1, v2

    or-long/2addr v6, v8

    aput-wide v6, v0, v2

    .line 7194
    iget-wide v0, v3, Lcom/a/b/g/q;->b:J

    iget-object v5, v3, Lcom/a/b/g/q;->a:[J

    aget-wide v6, v5, v2

    invoke-static {v6, v7}, Ljava/lang/Long;->bitCount(J)I

    move-result v5

    int-to-long v6, v5

    add-long/2addr v0, v6

    iput-wide v0, v3, Lcom/a/b/g/q;->b:J

    .line 7192
    add-int/lit8 v2, v2, 0x1

    goto :goto_b4

    :cond_d5
    move v0, v2

    .line 226
    goto/16 :goto_9

    :cond_d8
    move v0, v2

    .line 227
    goto/16 :goto_15

    :cond_db
    move v0, v2

    .line 230
    goto/16 :goto_3d

    :cond_de
    move v0, v2

    .line 7189
    goto :goto_97

    .line 240
    :cond_e0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .registers 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 148
    .line 1138
    iget-object v0, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    iget-object v1, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    iget v2, p0, Lcom/a/b/g/j;->b:I

    iget-object v3, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/a/b/g/m;->b(Ljava/lang/Object;Lcom/a/b/g/w;ILcom/a/b/g/q;)Z

    move-result v0

    .line 148
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 244
    if-ne p1, p0, :cond_5

    .line 254
    :cond_4
    :goto_4
    return v0

    .line 247
    :cond_5
    instance-of v2, p1, Lcom/a/b/g/j;

    if-eqz v2, :cond_31

    .line 248
    check-cast p1, Lcom/a/b/g/j;

    .line 249
    iget v2, p0, Lcom/a/b/g/j;->b:I

    iget v3, p1, Lcom/a/b/g/j;->b:I

    if-ne v2, v3, :cond_2f

    iget-object v2, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    iget-object v3, p1, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    iget-object v3, p1, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    invoke-virtual {v2, v3}, Lcom/a/b/g/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    iget-object v3, p1, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_2f
    move v0, v1

    goto :goto_4

    :cond_31
    move v0, v1

    .line 254
    goto :goto_4
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 259
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/a/b/g/j;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/a/b/g/j;->c:Lcom/a/b/g/w;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/a/b/g/j;->d:Lcom/a/b/g/m;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/a/b/g/j;->a:Lcom/a/b/g/q;

    aput-object v2, v0, v1

    .line 8084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 259
    return v0
.end method
