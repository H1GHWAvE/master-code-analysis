.class final Lcom/a/b/g/as;
.super Lcom/a/b/g/b;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# instance fields
.field private final b:I


# direct methods
.method varargs constructor <init>([Lcom/a/b/g/ak;)V
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 418
    invoke-direct {p0, p1}, Lcom/a/b/g/b;-><init>([Lcom/a/b/g/ak;)V

    .line 420
    array-length v2, p1

    move v1, v0

    :goto_6
    if-ge v0, v2, :cond_12

    aget-object v3, p1, v0

    .line 421
    invoke-interface {v3}, Lcom/a/b/g/ak;->b()I

    move-result v3

    add-int/2addr v1, v3

    .line 420
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 423
    :cond_12
    iput v1, p0, Lcom/a/b/g/as;->b:I

    .line 424
    return-void
.end method


# virtual methods
.method final a([Lcom/a/b/g/al;)Lcom/a/b/g/ag;
    .registers 11

    .prologue
    const/4 v1, 0x0

    .line 428
    iget v0, p0, Lcom/a/b/g/as;->b:I

    div-int/lit8 v0, v0, 0x8

    new-array v3, v0, [B

    .line 430
    array-length v4, p1

    move v0, v1

    move v2, v1

    :goto_a
    if-ge v0, v4, :cond_37

    aget-object v5, p1, v0

    .line 431
    invoke-interface {v5}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v5

    .line 432
    invoke-virtual {v5}, Lcom/a/b/g/ag;->a()I

    move-result v6

    div-int/lit8 v6, v6, 0x8

    .line 1090
    const/4 v7, 0x2

    new-array v7, v7, [I

    aput v6, v7, v1

    const/4 v6, 0x1

    invoke-virtual {v5}, Lcom/a/b/g/ag;->a()I

    move-result v8

    div-int/lit8 v8, v8, 0x8

    aput v8, v7, v6

    invoke-static {v7}, Lcom/a/b/l/q;->a([I)I

    move-result v6

    .line 1091
    add-int v7, v2, v6

    array-length v8, v3

    invoke-static {v2, v7, v8}, Lcom/a/b/b/cn;->a(III)V

    .line 1092
    invoke-virtual {v5, v3, v2, v6}, Lcom/a/b/g/ag;->a([BII)V

    .line 432
    add-int/2addr v2, v6

    .line 430
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 434
    :cond_37
    invoke-static {v3}, Lcom/a/b/g/ag;->a([B)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 439
    iget v0, p0, Lcom/a/b/g/as;->b:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 444
    instance-of v0, p1, Lcom/a/b/g/as;

    if-eqz v0, :cond_15

    .line 445
    check-cast p1, Lcom/a/b/g/as;

    .line 446
    iget v0, p0, Lcom/a/b/g/as;->b:I

    iget v2, p1, Lcom/a/b/g/as;->b:I

    if-ne v0, v2, :cond_15

    iget-object v0, p0, Lcom/a/b/g/as;->a:[Lcom/a/b/g/ak;

    array-length v0, v0

    iget-object v2, p1, Lcom/a/b/g/as;->a:[Lcom/a/b/g/ak;

    array-length v2, v2

    if-eq v0, v2, :cond_16

    .line 456
    :cond_15
    :goto_15
    return v1

    :cond_16
    move v0, v1

    .line 449
    :goto_17
    iget-object v2, p0, Lcom/a/b/g/as;->a:[Lcom/a/b/g/ak;

    array-length v2, v2

    if-ge v0, v2, :cond_2d

    .line 450
    iget-object v2, p0, Lcom/a/b/g/as;->a:[Lcom/a/b/g/ak;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/a/b/g/as;->a:[Lcom/a/b/g/ak;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 449
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .line 454
    :cond_2d
    const/4 v1, 0x1

    goto :goto_15
.end method

.method public final hashCode()I
    .registers 6

    .prologue
    .line 461
    iget v1, p0, Lcom/a/b/g/as;->b:I

    .line 462
    iget-object v2, p0, Lcom/a/b/g/as;->a:[Lcom/a/b/g/ak;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v3, :cond_12

    aget-object v4, v2, v0

    .line 463
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    xor-int/2addr v1, v4

    .line 462
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 465
    :cond_12
    return v1
.end method
