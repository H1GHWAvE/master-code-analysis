.class final Lcom/a/b/g/f;
.super Lcom/a/b/g/d;
.source "SourceFile"


# static fields
.field static final b:I = 0xff


# instance fields
.field final a:Lcom/a/b/g/g;

.field final synthetic c:Lcom/a/b/g/e;


# direct methods
.method constructor <init>(Lcom/a/b/g/e;I)V
    .registers 4

    .prologue
    .line 80
    iput-object p1, p0, Lcom/a/b/g/f;->c:Lcom/a/b/g/e;

    invoke-direct {p0}, Lcom/a/b/g/d;-><init>()V

    .line 81
    new-instance v0, Lcom/a/b/g/g;

    invoke-direct {v0, p2}, Lcom/a/b/g/g;-><init>(I)V

    iput-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    .line 82
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/g/ag;
    .registers 4

    .prologue
    .line 145
    iget-object v0, p0, Lcom/a/b/g/f;->c:Lcom/a/b/g/e;

    iget-object v1, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    invoke-virtual {v1}, Lcom/a/b/g/g;->a()[B

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    invoke-virtual {v2}, Lcom/a/b/g/g;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/a/b/g/e;->a([BI)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a(C)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 132
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    and-int/lit16 v1, p1, 0xff

    invoke-virtual {v0, v1}, Lcom/a/b/g/g;->write(I)V

    .line 133
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    ushr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Lcom/a/b/g/g;->write(I)V

    .line 134
    return-object p0
.end method

.method public final a(I)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 115
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    and-int/lit16 v1, p1, 0xff

    invoke-virtual {v0, v1}, Lcom/a/b/g/g;->write(I)V

    .line 116
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    ushr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Lcom/a/b/g/g;->write(I)V

    .line 117
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    ushr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Lcom/a/b/g/g;->write(I)V

    .line 118
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    ushr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Lcom/a/b/g/g;->write(I)V

    .line 119
    return-object p0
.end method

.method public final a(J)Lcom/a/b/g/al;
    .registers 10

    .prologue
    .line 124
    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x40

    if-ge v0, v1, :cond_14

    .line 125
    iget-object v1, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    ushr-long v2, p1, v0

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/a/b/g/g;->write(I)V

    .line 124
    add-int/lit8 v0, v0, 0x8

    goto :goto_1

    .line 127
    :cond_14
    return-object p0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 139
    invoke-interface {p2, p1, p0}, Lcom/a/b/g/w;->a(Ljava/lang/Object;Lcom/a/b/g/bn;)V

    .line 140
    return-object p0
.end method

.method public final a(S)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 108
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    and-int/lit16 v1, p1, 0xff

    invoke-virtual {v0, v1}, Lcom/a/b/g/g;->write(I)V

    .line 109
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    ushr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Lcom/a/b/g/g;->write(I)V

    .line 110
    return-object p0
.end method

.method public final b(B)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    invoke-virtual {v0, p1}, Lcom/a/b/g/g;->write(I)V

    .line 87
    return-object p0
.end method

.method public final b([B)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    invoke-virtual {v0, p1}, Lcom/a/b/g/g;->write([B)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_6

    .line 97
    return-object p0

    .line 94
    :catch_6
    move-exception v0

    .line 95
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b([BII)Lcom/a/b/g/al;
    .registers 5

    .prologue
    .line 102
    iget-object v0, p0, Lcom/a/b/g/f;->a:Lcom/a/b/g/g;

    invoke-virtual {v0, p1, p2, p3}, Lcom/a/b/g/g;->write([BII)V

    .line 103
    return-object p0
.end method

.method public final synthetic b(C)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/a/b/g/f;->a(C)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(I)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/a/b/g/f;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(J)Lcom/a/b/g/bn;
    .registers 4

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/f;->a(J)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(S)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/a/b/g/f;->a(S)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(B)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/a/b/g/f;->b(B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([B)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/a/b/g/f;->b([B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([BII)Lcom/a/b/g/bn;
    .registers 5

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/g/f;->b([BII)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method
