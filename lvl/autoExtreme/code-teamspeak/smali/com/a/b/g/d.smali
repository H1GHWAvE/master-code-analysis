.class abstract Lcom/a/b/g/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/g/al;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(D)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 32
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/a/b/g/d;->a(J)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 36
    invoke-static {p1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/g/d;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;
    .registers 5

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    :goto_5
    if-ge v0, v1, :cond_11

    .line 41
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/b/g/d;->a(C)Lcom/a/b/g/al;

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 43
    :cond_11
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 47
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/g/d;->b([B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 28
    if-eqz p1, :cond_8

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p0, v0}, Lcom/a/b/g/d;->b(B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final synthetic b(D)Lcom/a/b/g/bn;
    .registers 4

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/d;->a(D)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(F)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/a/b/g/d;->a(F)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/CharSequence;)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/a/b/g/d;->a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/bn;
    .registers 4

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/d;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Z)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/a/b/g/d;->a(Z)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method
