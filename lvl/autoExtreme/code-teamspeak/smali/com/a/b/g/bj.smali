.class final Lcom/a/b/g/bj;
.super Lcom/a/b/g/h;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(I)V
    .registers 2

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/a/b/g/h;-><init>()V

    .line 48
    iput p1, p0, Lcom/a/b/g/bj;->a:I

    .line 49
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 56
    new-instance v0, Lcom/a/b/g/bk;

    iget v1, p0, Lcom/a/b/g/bj;->a:I

    invoke-direct {v0, v1}, Lcom/a/b/g/bk;-><init>(I)V

    return-object v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 52
    const/16 v0, 0x80

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 66
    instance-of v1, p1, Lcom/a/b/g/bj;

    if-eqz v1, :cond_e

    .line 67
    check-cast p1, Lcom/a/b/g/bj;

    .line 68
    iget v1, p0, Lcom/a/b/g/bj;->a:I

    iget v2, p1, Lcom/a/b/g/bj;->a:I

    if-ne v1, v2, :cond_e

    const/4 v0, 0x1

    .line 70
    :cond_e
    return v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 75
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/a/b/g/bj;->a:I

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 61
    iget v0, p0, Lcom/a/b/g/bj;->a:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Hashing.murmur3_128("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
