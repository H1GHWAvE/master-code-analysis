.class final enum Lcom/a/b/g/o;
.super Lcom/a/b/g/n;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1, v0, v0}, Lcom/a/b/g/n;-><init>(Ljava/lang/String;IB)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;ILcom/a/b/g/q;)Z
    .registers 15

    .prologue
    .line 47
    invoke-virtual {p4}, Lcom/a/b/g/q;->a()J

    move-result-wide v4

    .line 48
    invoke-static {}, Lcom/a/b/g/am;->b()Lcom/a/b/g/ak;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/g/ak;->a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/g/ag;->c()J

    move-result-wide v0

    .line 49
    long-to-int v3, v0

    .line 50
    const/16 v2, 0x20

    ushr-long/2addr v0, v2

    long-to-int v6, v0

    .line 52
    const/4 v1, 0x0

    .line 53
    const/4 v0, 0x1

    move v2, v1

    move v1, v0

    :goto_19
    if-gt v1, p3, :cond_2d

    .line 54
    mul-int v0, v1, v6

    add-int/2addr v0, v3

    .line 56
    if-gez v0, :cond_22

    .line 57
    xor-int/lit8 v0, v0, -0x1

    .line 59
    :cond_22
    int-to-long v8, v0

    rem-long/2addr v8, v4

    invoke-virtual {p4, v8, v9}, Lcom/a/b/g/q;->a(J)Z

    move-result v0

    or-int/2addr v2, v0

    .line 53
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_19

    .line 61
    :cond_2d
    return v2
.end method

.method public final b(Ljava/lang/Object;Lcom/a/b/g/w;ILcom/a/b/g/q;)Z
    .registers 15

    .prologue
    const/4 v1, 0x1

    .line 66
    invoke-virtual {p4}, Lcom/a/b/g/q;->a()J

    move-result-wide v4

    .line 67
    invoke-static {}, Lcom/a/b/g/am;->b()Lcom/a/b/g/ak;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/g/ak;->a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/g/ag;->c()J

    move-result-wide v2

    .line 68
    long-to-int v6, v2

    .line 69
    const/16 v0, 0x20

    ushr-long/2addr v2, v0

    long-to-int v3, v2

    move v2, v1

    .line 71
    :goto_17
    if-gt v2, p3, :cond_2e

    .line 72
    mul-int v0, v2, v3

    add-int/2addr v0, v6

    .line 74
    if-gez v0, :cond_20

    .line 75
    xor-int/lit8 v0, v0, -0x1

    .line 77
    :cond_20
    int-to-long v8, v0

    rem-long/2addr v8, v4

    invoke-virtual {p4, v8, v9}, Lcom/a/b/g/q;->b(J)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 78
    const/4 v0, 0x0

    .line 81
    :goto_29
    return v0

    .line 71
    :cond_2a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_17

    :cond_2e
    move v0, v1

    .line 81
    goto :goto_29
.end method
