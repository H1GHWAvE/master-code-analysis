.class final Lcom/a/b/g/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[J

.field b:J


# direct methods
.method constructor <init>(J)V
    .registers 6

    .prologue
    .line 145
    sget-object v0, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {p1, p2, v0}, Lcom/a/b/j/i;->d(JLjava/math/RoundingMode;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/l/q;->a(J)I

    move-result v0

    new-array v0, v0, [J

    invoke-direct {p0, v0}, Lcom/a/b/g/q;-><init>([J)V

    .line 146
    return-void
.end method

.method constructor <init>([J)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    array-length v0, p1

    if-lez v0, :cond_1f

    const/4 v0, 0x1

    :goto_8
    const-string v2, "data length is zero!"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 151
    iput-object p1, p0, Lcom/a/b/g/q;->a:[J

    .line 152
    const-wide/16 v2, 0x0

    .line 153
    array-length v0, p1

    :goto_12
    if-ge v1, v0, :cond_21

    aget-wide v4, p1, v1

    .line 154
    invoke-static {v4, v5}, Ljava/lang/Long;->bitCount(J)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    :cond_1f
    move v0, v1

    .line 150
    goto :goto_8

    .line 156
    :cond_21
    iput-wide v2, p0, Lcom/a/b/g/q;->b:J

    .line 157
    return-void
.end method

.method private a(Lcom/a/b/g/q;)V
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    array-length v0, v0

    iget-object v3, p1, Lcom/a/b/g/q;->a:[J

    array-length v3, v3

    if-ne v0, v3, :cond_4a

    move v0, v1

    :goto_b
    const-string v3, "BitArrays must be of equal length (%s != %s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/b/g/q;->a:[J

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p1, Lcom/a/b/g/q;->a:[J

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 191
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/a/b/g/q;->b:J

    .line 192
    :goto_29
    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    array-length v0, v0

    if-ge v2, v0, :cond_4c

    .line 193
    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    aget-wide v4, v0, v2

    iget-object v1, p1, Lcom/a/b/g/q;->a:[J

    aget-wide v6, v1, v2

    or-long/2addr v4, v6

    aput-wide v4, v0, v2

    .line 194
    iget-wide v0, p0, Lcom/a/b/g/q;->b:J

    iget-object v3, p0, Lcom/a/b/g/q;->a:[J

    aget-wide v4, v3, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->bitCount(J)I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/a/b/g/q;->b:J

    .line 192
    add-int/lit8 v2, v2, 0x1

    goto :goto_29

    :cond_4a
    move v0, v2

    .line 189
    goto :goto_b

    .line 196
    :cond_4c
    return-void
.end method

.method private c()J
    .registers 3

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/a/b/g/q;->b:J

    return-wide v0
.end method


# virtual methods
.method final a()J
    .registers 5

    .prologue
    .line 175
    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    array-length v0, v0

    int-to-long v0, v0

    const-wide/16 v2, 0x40

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method final a(J)Z
    .registers 12

    .prologue
    const-wide/16 v6, 0x1

    .line 161
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/q;->b(J)Z

    move-result v0

    if-nez v0, :cond_1d

    .line 162
    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    const/4 v1, 0x6

    ushr-long v2, p1, v1

    long-to-int v1, v2

    aget-wide v2, v0, v1

    long-to-int v4, p1

    shl-long v4, v6, v4

    or-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 163
    iget-wide v0, p0, Lcom/a/b/g/q;->b:J

    add-long/2addr v0, v6

    iput-wide v0, p0, Lcom/a/b/g/q;->b:J

    .line 164
    const/4 v0, 0x1

    .line 166
    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method final b()Lcom/a/b/g/q;
    .registers 3

    .prologue
    .line 184
    new-instance v1, Lcom/a/b/g/q;

    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    invoke-virtual {v0}, [J->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    invoke-direct {v1, v0}, Lcom/a/b/g/q;-><init>([J)V

    return-object v1
.end method

.method final b(J)Z
    .registers 8

    .prologue
    .line 170
    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    const/4 v1, 0x6

    ushr-long v2, p1, v1

    long-to-int v1, v2

    aget-wide v0, v0, v1

    const-wide/16 v2, 0x1

    long-to-int v4, p1

    shl-long/2addr v2, v4

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 199
    instance-of v0, p1, Lcom/a/b/g/q;

    if-eqz v0, :cond_f

    .line 200
    check-cast p1, Lcom/a/b/g/q;

    .line 201
    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    iget-object v1, p1, Lcom/a/b/g/q;->a:[J

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    .line 203
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/a/b/g/q;->a:[J

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([J)I

    move-result v0

    return v0
.end method
