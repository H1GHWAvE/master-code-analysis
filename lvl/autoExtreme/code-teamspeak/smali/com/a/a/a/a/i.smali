.class public abstract Lcom/a/a/a/a/i;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/a/a/a/a/h;


# static fields
.field static final a:I = 0x1

.field private static final b:Ljava/lang/String; = "com.android.vending.licensing.ILicensingService"


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 21
    const-string v0, "com.android.vending.licensing.ILicensingService"

    invoke-virtual {p0, p0, v0}, Lcom/a/a/a/a/i;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/a/a/a/a/h;
    .registers 3

    .prologue
    .line 29
    if-nez p0, :cond_4

    .line 30
    const/4 v0, 0x0

    .line 36
    :goto_3
    return-object v0

    .line 32
    :cond_4
    const-string v0, "com.android.vending.licensing.ILicensingService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_13

    instance-of v1, v0, Lcom/a/a/a/a/h;

    if-eqz v1, :cond_13

    .line 34
    check-cast v0, Lcom/a/a/a/a/h;

    goto :goto_3

    .line 36
    :cond_13
    new-instance v0, Lcom/a/a/a/a/j;

    invoke-direct {v0, p0}, Lcom/a/a/a/a/j;-><init>(Landroid/os/IBinder;)V

    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12

    .prologue
    const/4 v1, 0x1

    .line 44
    sparse-switch p1, :sswitch_data_3e

    .line 64
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_8
    return v0

    .line 48
    :sswitch_9
    const-string v0, "com.android.vending.licensing.ILicensingService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 49
    goto :goto_8

    .line 53
    :sswitch_10
    const-string v0, "com.android.vending.licensing.ILicensingService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 59
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    .line 1029
    if-nez v5, :cond_29

    .line 1030
    const/4 v0, 0x0

    .line 60
    :goto_24
    invoke-virtual {p0, v2, v3, v4, v0}, Lcom/a/a/a/a/i;->a(JLjava/lang/String;Lcom/a/a/a/a/e;)V

    move v0, v1

    .line 61
    goto :goto_8

    .line 1032
    :cond_29
    const-string v0, "com.android.vending.licensing.ILicenseResultListener"

    invoke-interface {v5, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 1033
    if-eqz v0, :cond_38

    instance-of v6, v0, Lcom/a/a/a/a/e;

    if-eqz v6, :cond_38

    .line 1034
    check-cast v0, Lcom/a/a/a/a/e;

    goto :goto_24

    .line 1036
    :cond_38
    new-instance v0, Lcom/a/a/a/a/g;

    invoke-direct {v0, v5}, Lcom/a/a/a/a/g;-><init>(Landroid/os/IBinder;)V

    goto :goto_24

    .line 44
    :sswitch_data_3e
    .sparse-switch
        0x1 -> :sswitch_10
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
