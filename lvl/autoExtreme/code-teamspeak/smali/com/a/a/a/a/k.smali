.class public final Lcom/a/a/a/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final b:Ljava/lang/String; = "LicenseChecker"

.field private static final c:Ljava/lang/String; = "RSA"

.field private static final d:I = 0x2710

.field private static final e:Ljava/security/SecureRandom;

.field private static final f:Z


# instance fields
.field public a:I

.field private g:Lcom/a/a/a/a/h;

.field private h:Ljava/security/PublicKey;

.field private final i:Landroid/content/Context;

.field private final j:Lcom/a/a/a/a/s;

.field private k:Landroid/os/Handler;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/util/Set;

.field private final o:Ljava/util/Queue;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 65
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/a/a/a/a/k;->e:Ljava/security/SecureRandom;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/a/a/a/a/s;Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/k;->n:Ljava/util/Set;

    .line 81
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/k;->o:Ljava/util/Queue;

    .line 93
    iput-object p1, p0, Lcom/a/a/a/a/k;->i:Landroid/content/Context;

    .line 94
    iput-object p2, p0, Lcom/a/a/a/a/k;->j:Lcom/a/a/a/a/s;

    .line 95
    invoke-static {p3}, Lcom/a/a/a/a/k;->a(Ljava/lang/String;)Ljava/security/PublicKey;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/k;->h:Ljava/security/PublicKey;

    .line 96
    iget-object v0, p0, Lcom/a/a/a/a/k;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/k;->l:Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lcom/a/a/a/a/k;->l:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/a/a/a/a/k;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/k;->m:Ljava/lang/String;

    .line 98
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "background thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 99
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 100
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/a/a/a/a/k;->k:Landroid/os/Handler;

    .line 101
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 356
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_e} :catch_10

    move-result-object v0

    .line 360
    :goto_f
    return-object v0

    .line 359
    :catch_10
    move-exception v0

    const-string v0, "LicenseChecker"

    const-string v1, "Package not found. could not get version code."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const-string v0, ""

    goto :goto_f
.end method

.method private static a(Ljava/lang/String;)Ljava/security/PublicKey;
    .registers 4

    .prologue
    .line 112
    :try_start_0
    invoke-static {p0}, Lcom/a/a/a/a/a/a;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 113
    const-string v1, "RSA"

    invoke-static {v1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 115
    new-instance v2, Ljava/security/spec/X509EncodedKeySpec;

    invoke-direct {v2, v0}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    :try_end_12
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_12} :catch_14
    .catch Lcom/a/a/a/a/a/b; {:try_start_0 .. :try_end_12} :catch_1b
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_12} :catch_29

    move-result-object v0

    return-object v0

    .line 116
    :catch_14
    move-exception v0

    .line 118
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 119
    :catch_1b
    move-exception v0

    .line 120
    const-string v1, "LicenseChecker"

    const-string v2, "Could not decode from Base64."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 122
    :catch_29
    move-exception v0

    .line 123
    const-string v1, "LicenseChecker"

    const-string v2, "Invalid key specification."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Lcom/a/a/a/a/k;)Ljava/util/Set;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/a/a/a/k;->n:Ljava/util/Set;

    return-object v0
.end method

.method private a()V
    .registers 7

    .prologue
    .line 183
    :goto_0
    iget-object v0, p0, Lcom/a/a/a/a/k;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/a/a/p;

    if-eqz v0, :cond_41

    .line 185
    :try_start_a
    const-string v1, "LicenseChecker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Calling checkLicense on service for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2078
    iget-object v3, v0, Lcom/a/a/a/a/p;->c:Ljava/lang/String;

    .line 185
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v1, p0, Lcom/a/a/a/a/k;->g:Lcom/a/a/a/a/h;

    .line 3074
    iget v2, v0, Lcom/a/a/a/a/p;->b:I

    .line 186
    int-to-long v2, v2

    .line 3078
    iget-object v4, v0, Lcom/a/a/a/a/p;->c:Ljava/lang/String;

    .line 186
    new-instance v5, Lcom/a/a/a/a/l;

    invoke-direct {v5, p0, v0}, Lcom/a/a/a/a/l;-><init>(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/a/a/a/a/h;->a(JLjava/lang/String;Lcom/a/a/a/a/e;)V

    .line 189
    iget-object v1, p0, Lcom/a/a/a/a/k;->n:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_34} :catch_35

    goto :goto_0

    .line 190
    :catch_35
    move-exception v1

    .line 191
    const-string v2, "LicenseChecker"

    const-string v3, "RemoteException in checkLicense call."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 192
    invoke-direct {p0, v0}, Lcom/a/a/a/a/k;->b(Lcom/a/a/a/a/p;)V

    goto :goto_0

    .line 195
    :cond_41
    return-void
.end method

.method static synthetic a(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V
    .registers 2

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/a/a/a/a/k;->b(Lcom/a/a/a/a/p;)V

    return-void
.end method

.method private declared-synchronized a(Lcom/a/a/a/a/p;)V
    .registers 3

    .prologue
    .line 204
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/a/a/a/k;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 205
    iget-object v0, p0, Lcom/a/a/a/a/k;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 206
    invoke-direct {p0}, Lcom/a/a/a/a/k;->c()V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    .line 208
    :cond_11
    monitor-exit p0

    return-void

    .line 204
    :catchall_13
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/a/a/a/a/k;)Ljava/security/PublicKey;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/a/a/a/k;->h:Ljava/security/PublicKey;

    return-object v0
.end method

.method private b()V
    .registers 2

    .prologue
    .line 199
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/a/a/a/k;->a:I

    .line 200
    iget v0, p0, Lcom/a/a/a/a/k;->a:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/a/a/a/a/k;->a:I

    .line 201
    return-void
.end method

.method static synthetic b(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V
    .registers 2

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/a/a/a/a/k;->a(Lcom/a/a/a/a/p;)V

    return-void
.end method

.method private declared-synchronized b(Lcom/a/a/a/a/p;)V
    .registers 5

    .prologue
    .line 306
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/a/a/a/k;->j:Lcom/a/a/a/a/s;

    const/16 v1, 0x123

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/a/a/a/a/s;->a(ILcom/a/a/a/a/u;)V

    .line 308
    iget-object v0, p0, Lcom/a/a/a/a/k;->j:Lcom/a/a/a/a/s;

    invoke-interface {v0}, Lcom/a/a/a/a/s;->a()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 4070
    iget-object v0, p1, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    .line 309
    invoke-interface {v0}, Lcom/a/a/a/a/o;->a()V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_20

    .line 313
    :goto_16
    monitor-exit p0

    return-void

    .line 5070
    :cond_18
    :try_start_18
    iget-object v0, p1, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    .line 311
    const/16 v1, 0x123

    invoke-interface {v0, v1}, Lcom/a/a/a/a/o;->a(I)V
    :try_end_1f
    .catchall {:try_start_18 .. :try_end_1f} :catchall_20

    goto :goto_16

    .line 306
    :catchall_20
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/a/a/a/a/k;)Landroid/os/Handler;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/a/a/a/k;->k:Landroid/os/Handler;

    return-object v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/a/a/a/a/k;->g:Lcom/a/a/a/a/h;

    if-eqz v0, :cond_c

    .line 319
    :try_start_4
    iget-object v0, p0, Lcom/a/a/a/a/k;->i:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_9} :catch_d

    .line 325
    :goto_9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/a/k;->g:Lcom/a/a/a/a/h;

    .line 327
    :cond_c
    return-void

    .line 323
    :catch_d
    move-exception v0

    const-string v0, "LicenseChecker"

    const-string v1, "Unable to unbind from licensing service (already unbound)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9
.end method

.method private declared-synchronized d()V
    .registers 2

    .prologue
    .line 338
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/a/a/a/a/k;->c()V

    .line 339
    iget-object v0, p0, Lcom/a/a/a/a/k;->k:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 340
    monitor-exit p0

    return-void

    .line 338
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static e()I
    .registers 1

    .prologue
    .line 344
    sget-object v0, Lcom/a/a/a/a/k;->e:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextInt()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/a/a/a/a/o;)V
    .registers 9

    .prologue
    .line 142
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/a/a/a/k;->j:Lcom/a/a/a/a/s;

    invoke-interface {v0}, Lcom/a/a/a/a/s;->a()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 143
    const-string v0, "LicenseChecker"

    const-string v1, "Using cached license response"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-interface {p1}, Lcom/a/a/a/a/o;->a()V

    .line 145
    const/16 v0, 0x100

    iget-object v1, p0, Lcom/a/a/a/a/k;->j:Lcom/a/a/a/a/s;

    invoke-interface {v1}, Lcom/a/a/a/a/s;->b()Lcom/a/a/a/a/w;

    move-result-object v1

    .line 1018
    iget-object v1, v1, Lcom/a/a/a/a/w;->a:Lcom/a/a/a/a/u;

    .line 145
    iget-object v2, p0, Lcom/a/a/a/a/k;->j:Lcom/a/a/a/a/s;

    invoke-interface {v2}, Lcom/a/a/a/a/s;->b()Lcom/a/a/a/a/w;

    move-result-object v2

    .line 1024
    iget-object v2, v2, Lcom/a/a/a/a/w;->b:Ljava/lang/String;

    .line 145
    iget-object v3, p0, Lcom/a/a/a/a/k;->j:Lcom/a/a/a/a/s;

    invoke-interface {v3}, Lcom/a/a/a/a/s;->b()Lcom/a/a/a/a/w;

    move-result-object v3

    .line 1030
    iget-object v3, v3, Lcom/a/a/a/a/w;->c:Ljava/lang/String;

    .line 145
    invoke-interface {p1, v0, v1, v2, v3}, Lcom/a/a/a/a/o;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_30
    .catchall {:try_start_1 .. :try_end_30} :catchall_7e

    .line 179
    :goto_30
    monitor-exit p0

    return-void

    .line 147
    :cond_32
    :try_start_32
    new-instance v0, Lcom/a/a/a/a/p;

    iget-object v1, p0, Lcom/a/a/a/a/k;->j:Lcom/a/a/a/a/s;

    new-instance v2, Lcom/a/a/a/a/q;

    invoke-direct {v2}, Lcom/a/a/a/a/q;-><init>()V

    .line 1344
    sget-object v3, Lcom/a/a/a/a/k;->e:Ljava/security/SecureRandom;

    invoke-virtual {v3}, Ljava/security/SecureRandom;->nextInt()I

    move-result v4

    .line 147
    iget-object v5, p0, Lcom/a/a/a/a/k;->l:Ljava/lang/String;

    iget-object v6, p0, Lcom/a/a/a/a/k;->m:Ljava/lang/String;

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/a/a/a/a/p;-><init>(Lcom/a/a/a/a/s;Lcom/a/a/a/a/d;Lcom/a/a/a/a/o;ILjava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/a/a/a/a/k;->g:Lcom/a/a/a/a/h;

    if-nez v1, :cond_91

    .line 151
    const-string v1, "LicenseChecker"

    const-string v2, "Binding to licensing service."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_54
    .catchall {:try_start_32 .. :try_end_54} :catchall_7e

    .line 153
    :try_start_54
    new-instance v1, Landroid/content/Intent;

    new-instance v2, Ljava/lang/String;

    const-string v3, "Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U="

    invoke-static {v3}, Lcom/a/a/a/a/a/a;->a(Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 156
    const-string v2, "com.android.vending"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    iget-object v2, p0, Lcom/a/a/a/a/k;->i:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, p0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 163
    if-eqz v1, :cond_81

    .line 164
    iget-object v1, p0, Lcom/a/a/a/a/k;->o:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z
    :try_end_77
    .catch Ljava/lang/SecurityException; {:try_start_54 .. :try_end_77} :catch_78
    .catch Lcom/a/a/a/a/a/b; {:try_start_54 .. :try_end_77} :catch_8c
    .catchall {:try_start_54 .. :try_end_77} :catchall_7e

    goto :goto_30

    .line 170
    :catch_78
    move-exception v0

    const/4 v0, 0x6

    :try_start_7a
    invoke-interface {p1, v0}, Lcom/a/a/a/a/o;->b(I)V
    :try_end_7d
    .catchall {:try_start_7a .. :try_end_7d} :catchall_7e

    goto :goto_30

    .line 142
    :catchall_7e
    move-exception v0

    monitor-exit p0

    throw v0

    .line 166
    :cond_81
    :try_start_81
    const-string v1, "LicenseChecker"

    const-string v2, "Could not bind to service."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-direct {p0, v0}, Lcom/a/a/a/a/k;->b(Lcom/a/a/a/a/p;)V
    :try_end_8b
    .catch Ljava/lang/SecurityException; {:try_start_81 .. :try_end_8b} :catch_78
    .catch Lcom/a/a/a/a/a/b; {:try_start_81 .. :try_end_8b} :catch_8c
    .catchall {:try_start_81 .. :try_end_8b} :catchall_7e

    goto :goto_30

    .line 172
    :catch_8c
    move-exception v0

    :try_start_8d
    invoke-virtual {v0}, Lcom/a/a/a/a/a/b;->printStackTrace()V

    goto :goto_30

    .line 175
    :cond_91
    iget-object v1, p0, Lcom/a/a/a/a/k;->o:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 176
    invoke-direct {p0}, Lcom/a/a/a/a/k;->a()V
    :try_end_99
    .catchall {:try_start_8d .. :try_end_99} :catchall_7e

    goto :goto_30
.end method

.method public final declared-synchronized onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 4

    .prologue
    .line 289
    monitor-enter p0

    :try_start_1
    invoke-static {p2}, Lcom/a/a/a/a/i;->a(Landroid/os/IBinder;)Lcom/a/a/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/k;->g:Lcom/a/a/a/a/h;

    .line 290
    invoke-direct {p0}, Lcom/a/a/a/a/k;->a()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 291
    monitor-exit p0

    return-void

    .line 289
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 4

    .prologue
    .line 297
    monitor-enter p0

    :try_start_1
    const-string v0, "LicenseChecker"

    const-string v1, "Service unexpectedly disconnected."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/a/a/k;->g:Lcom/a/a/a/a/h;
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    .line 299
    monitor-exit p0

    return-void

    .line 297
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method
