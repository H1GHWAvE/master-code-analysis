.class public final Lcom/a/c/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/a/c/b/s;

.field private b:Lcom/a/c/ah;

.field private c:Lcom/a/c/j;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/List;

.field private final f:Ljava/util/List;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x2

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    sget-object v0, Lcom/a/c/b/s;->a:Lcom/a/c/b/s;

    iput-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 70
    sget-object v0, Lcom/a/c/ah;->a:Lcom/a/c/ah;

    iput-object v0, p0, Lcom/a/c/r;->b:Lcom/a/c/ah;

    .line 71
    sget-object v0, Lcom/a/c/d;->a:Lcom/a/c/d;

    iput-object v0, p0, Lcom/a/c/r;->c:Lcom/a/c/j;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/c/r;->d:Ljava/util/Map;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/c/r;->e:Ljava/util/List;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/c/r;->f:Ljava/util/List;

    .line 79
    iput v1, p0, Lcom/a/c/r;->i:I

    .line 80
    iput v1, p0, Lcom/a/c/r;->j:I

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/c/r;->m:Z

    .line 94
    return-void
.end method

.method private a()Lcom/a/c/r;
    .registers 2

    .prologue
    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/c/r;->o:Z

    .line 135
    return-object p0
.end method

.method private a(D)Lcom/a/c/r;
    .registers 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 1070
    invoke-virtual {v0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v0

    .line 1071
    iput-wide p1, v0, Lcom/a/c/b/s;->b:D

    .line 104
    iput-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 105
    return-object p0
.end method

.method private a(I)Lcom/a/c/r;
    .registers 3

    .prologue
    .line 403
    iput p1, p0, Lcom/a/c/r;->i:I

    .line 404
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/r;->h:Ljava/lang/String;

    .line 405
    return-object p0
.end method

.method private a(II)Lcom/a/c/r;
    .registers 4

    .prologue
    .line 424
    iput p1, p0, Lcom/a/c/r;->i:I

    .line 425
    iput p2, p0, Lcom/a/c/r;->j:I

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/r;->h:Ljava/lang/String;

    .line 427
    return-object p0
.end method

.method private a(Lcom/a/c/ah;)Lcom/a/c/r;
    .registers 2

    .prologue
    .line 262
    iput-object p1, p0, Lcom/a/c/r;->b:Lcom/a/c/ah;

    .line 263
    return-object p0
.end method

.method private a(Lcom/a/c/ap;)Lcom/a/c/r;
    .registers 3

    .prologue
    .line 474
    iget-object v0, p0, Lcom/a/c/r;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 475
    return-object p0
.end method

.method private a(Lcom/a/c/b;)Lcom/a/c/r;
    .registers 5

    .prologue
    .line 322
    iget-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/a/c/b/s;->a(Lcom/a/c/b;ZZ)Lcom/a/c/b/s;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 323
    return-object p0
.end method

.method private a(Lcom/a/c/d;)Lcom/a/c/r;
    .registers 2

    .prologue
    .line 275
    iput-object p1, p0, Lcom/a/c/r;->c:Lcom/a/c/j;

    .line 276
    return-object p0
.end method

.method private a(Lcom/a/c/j;)Lcom/a/c/r;
    .registers 2

    .prologue
    .line 288
    iput-object p1, p0, Lcom/a/c/r;->c:Lcom/a/c/j;

    .line 289
    return-object p0
.end method

.method private a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/a/c/r;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 494
    instance-of v0, p2, Lcom/a/c/ae;

    if-nez v0, :cond_d

    instance-of v0, p2, Lcom/a/c/v;

    if-nez v0, :cond_d

    instance-of v0, p2, Lcom/a/c/an;

    if-eqz v0, :cond_32

    :cond_d
    const/4 v0, 0x1

    :goto_e
    invoke-static {v0}, Lcom/a/c/b/a;->a(Z)V

    .line 497
    instance-of v0, p2, Lcom/a/c/v;

    if-nez v0, :cond_19

    instance-of v0, p2, Lcom/a/c/ae;

    if-eqz v0, :cond_22

    .line 498
    :cond_19
    iget-object v0, p0, Lcom/a/c/r;->f:Ljava/util/List;

    .line 499
    invoke-static {p1, p2}, Lcom/a/c/ak;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/a/c/ap;

    move-result-object v2

    .line 498
    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 501
    :cond_22
    instance-of v0, p2, Lcom/a/c/an;

    if-eqz v0, :cond_31

    .line 502
    iget-object v0, p0, Lcom/a/c/r;->e:Ljava/util/List;

    check-cast p2, Lcom/a/c/an;

    invoke-static {p1, p2}, Lcom/a/c/b/a/z;->b(Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 504
    :cond_31
    return-object p0

    :cond_32
    move v0, v1

    .line 494
    goto :goto_e
.end method

.method private a(Ljava/lang/String;)Lcom/a/c/r;
    .registers 2

    .prologue
    .line 384
    iput-object p1, p0, Lcom/a/c/r;->h:Ljava/lang/String;

    .line 385
    return-object p0
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/a/c/r;
    .registers 5

    .prologue
    .line 448
    instance-of v0, p2, Lcom/a/c/ae;

    if-nez v0, :cond_10

    instance-of v0, p2, Lcom/a/c/v;

    if-nez v0, :cond_10

    instance-of v0, p2, Lcom/a/c/s;

    if-nez v0, :cond_10

    instance-of v0, p2, Lcom/a/c/an;

    if-eqz v0, :cond_49

    :cond_10
    const/4 v0, 0x1

    :goto_11
    invoke-static {v0}, Lcom/a/c/b/a;->a(Z)V

    .line 452
    instance-of v0, p2, Lcom/a/c/s;

    if-eqz v0, :cond_20

    .line 453
    iget-object v1, p0, Lcom/a/c/r;->d:Ljava/util/Map;

    move-object v0, p2

    check-cast v0, Lcom/a/c/s;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    :cond_20
    instance-of v0, p2, Lcom/a/c/ae;

    if-nez v0, :cond_28

    instance-of v0, p2, Lcom/a/c/v;

    if-eqz v0, :cond_35

    .line 456
    :cond_28
    invoke-static {p1}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v0

    .line 457
    iget-object v1, p0, Lcom/a/c/r;->e:Ljava/util/List;

    invoke-static {v0, p2}, Lcom/a/c/ak;->b(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459
    :cond_35
    instance-of v0, p2, Lcom/a/c/an;

    if-eqz v0, :cond_48

    .line 460
    iget-object v0, p0, Lcom/a/c/r;->e:Ljava/util/List;

    invoke-static {p1}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v1

    check-cast p2, Lcom/a/c/an;

    invoke-static {v1, p2}, Lcom/a/c/b/a/z;->a(Lcom/a/c/c/a;Lcom/a/c/an;)Lcom/a/c/ap;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    :cond_48
    return-object p0

    .line 448
    :cond_49
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private varargs a([I)Lcom/a/c/r;
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 120
    iget-object v1, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 1076
    invoke-virtual {v1}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v1

    .line 1077
    iput v0, v1, Lcom/a/c/b/s;->c:I

    .line 1078
    array-length v2, p1

    :goto_a
    if-ge v0, v2, :cond_16

    aget v3, p1, v0

    .line 1079
    iget v4, v1, Lcom/a/c/b/s;->c:I

    or-int/2addr v3, v4

    iput v3, v1, Lcom/a/c/b/s;->c:I

    .line 1078
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 120
    :cond_16
    iput-object v1, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 121
    return-object p0
.end method

.method private varargs a([Lcom/a/c/b;)Lcom/a/c/r;
    .registers 7

    .prologue
    const/4 v4, 0x1

    .line 303
    array-length v1, p1

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_12

    aget-object v2, p1, v0

    .line 304
    iget-object v3, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    invoke-virtual {v3, v2, v4, v4}, Lcom/a/c/b/s;->a(Lcom/a/c/b;ZZ)Lcom/a/c/b/s;

    move-result-object v2

    iput-object v2, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 306
    :cond_12
    return-object p0
.end method

.method private static a(Ljava/lang/String;IILjava/util/List;)V
    .registers 7

    .prologue
    const/4 v2, 0x2

    .line 554
    if-eqz p0, :cond_3c

    const-string v0, ""

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3c

    .line 555
    new-instance v0, Lcom/a/c/a;

    invoke-direct {v0, p0}, Lcom/a/c/a;-><init>(Ljava/lang/String;)V

    .line 562
    :goto_14
    const-class v1, Ljava/util/Date;

    invoke-static {v1}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/c/ak;->a(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 563
    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/c/ak;->a(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 564
    const-class v1, Ljava/sql/Date;

    invoke-static {v1}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/c/ak;->a(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    :cond_3b
    return-void

    .line 556
    :cond_3c
    if-eq p1, v2, :cond_3b

    if-eq p2, v2, :cond_3b

    .line 557
    new-instance v0, Lcom/a/c/a;

    invoke-direct {v0, p1, p2}, Lcom/a/c/a;-><init>(II)V

    goto :goto_14
.end method

.method private b()Lcom/a/c/r;
    .registers 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 1091
    invoke-virtual {v0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v0

    .line 1092
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/a/c/b/s;->e:Z

    .line 145
    iput-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 146
    return-object p0
.end method

.method private b(Lcom/a/c/b;)Lcom/a/c/r;
    .registers 5

    .prologue
    .line 339
    iget-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/a/c/b/s;->a(Lcom/a/c/b;ZZ)Lcom/a/c/b/s;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 340
    return-object p0
.end method

.method private c()Lcom/a/c/r;
    .registers 2

    .prologue
    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/c/r;->g:Z

    .line 158
    return-object p0
.end method

.method private d()Lcom/a/c/r;
    .registers 2

    .prologue
    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/c/r;->k:Z

    .line 239
    return-object p0
.end method

.method private e()Lcom/a/c/r;
    .registers 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 2085
    invoke-virtual {v0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v0

    .line 2086
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/a/c/b/s;->d:Z

    .line 249
    iput-object v0, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    .line 250
    return-object p0
.end method

.method private f()Lcom/a/c/r;
    .registers 2

    .prologue
    .line 350
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/c/r;->n:Z

    .line 351
    return-object p0
.end method

.method private g()Lcom/a/c/r;
    .registers 2

    .prologue
    .line 362
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/a/c/r;->m:Z

    .line 363
    return-object p0
.end method

.method private h()Lcom/a/c/r;
    .registers 2

    .prologue
    .line 528
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/c/r;->l:Z

    .line 529
    return-object p0
.end method

.method private i()Lcom/a/c/k;
    .registers 13

    .prologue
    const/4 v5, 0x2

    .line 539
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 540
    iget-object v0, p0, Lcom/a/c/r;->e:Ljava/util/List;

    invoke-interface {v11, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 541
    invoke-static {v11}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 542
    iget-object v0, p0, Lcom/a/c/r;->f:Ljava/util/List;

    invoke-interface {v11, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 543
    iget-object v1, p0, Lcom/a/c/r;->h:Ljava/lang/String;

    iget v2, p0, Lcom/a/c/r;->i:I

    iget v3, p0, Lcom/a/c/r;->j:I

    .line 2554
    if-eqz v1, :cond_6d

    const-string v0, ""

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6d

    .line 2555
    new-instance v0, Lcom/a/c/a;

    invoke-direct {v0, v1}, Lcom/a/c/a;-><init>(Ljava/lang/String;)V

    .line 2562
    :goto_2c
    const-class v1, Ljava/util/Date;

    invoke-static {v1}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/c/ak;->a(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;

    move-result-object v1

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2563
    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/c/ak;->a(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;

    move-result-object v1

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2564
    const-class v1, Ljava/sql/Date;

    invoke-static {v1}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/c/ak;->a(Lcom/a/c/c/a;Ljava/lang/Object;)Lcom/a/c/ap;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545
    :cond_53
    new-instance v0, Lcom/a/c/k;

    iget-object v1, p0, Lcom/a/c/r;->a:Lcom/a/c/b/s;

    iget-object v2, p0, Lcom/a/c/r;->c:Lcom/a/c/j;

    iget-object v3, p0, Lcom/a/c/r;->d:Ljava/util/Map;

    iget-boolean v4, p0, Lcom/a/c/r;->g:Z

    iget-boolean v5, p0, Lcom/a/c/r;->k:Z

    iget-boolean v6, p0, Lcom/a/c/r;->o:Z

    iget-boolean v7, p0, Lcom/a/c/r;->m:Z

    iget-boolean v8, p0, Lcom/a/c/r;->n:Z

    iget-boolean v9, p0, Lcom/a/c/r;->l:Z

    iget-object v10, p0, Lcom/a/c/r;->b:Lcom/a/c/ah;

    invoke-direct/range {v0 .. v11}, Lcom/a/c/k;-><init>(Lcom/a/c/b/s;Lcom/a/c/j;Ljava/util/Map;ZZZZZZLcom/a/c/ah;Ljava/util/List;)V

    return-object v0

    .line 2556
    :cond_6d
    if-eq v2, v5, :cond_53

    if-eq v3, v5, :cond_53

    .line 2557
    new-instance v0, Lcom/a/c/a;

    invoke-direct {v0, v2, v3}, Lcom/a/c/a;-><init>(II)V

    goto :goto_2c
.end method
