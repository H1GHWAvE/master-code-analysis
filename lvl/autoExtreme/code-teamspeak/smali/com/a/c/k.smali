.class public final Lcom/a/c/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Z = false

.field private static final d:Ljava/lang/String; = ")]}\'\n"


# instance fields
.field final b:Lcom/a/c/u;

.field final c:Lcom/a/c/ad;

.field private final e:Ljava/lang/ThreadLocal;

.field private final f:Ljava/util/Map;

.field private final g:Ljava/util/List;

.field private final h:Lcom/a/c/b/f;

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Z


# direct methods
.method public constructor <init>()V
    .registers 13

    .prologue
    const/4 v4, 0x0

    .line 175
    sget-object v1, Lcom/a/c/b/s;->a:Lcom/a/c/b/s;

    sget-object v2, Lcom/a/c/d;->a:Lcom/a/c/d;

    .line 176
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    const/4 v7, 0x1

    sget-object v10, Lcom/a/c/ah;->a:Lcom/a/c/ah;

    .line 178
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    move-object v0, p0

    move v5, v4

    move v6, v4

    move v8, v4

    move v9, v4

    .line 175
    invoke-direct/range {v0 .. v11}, Lcom/a/c/k;-><init>(Lcom/a/c/b/s;Lcom/a/c/j;Ljava/util/Map;ZZZZZZLcom/a/c/ah;Ljava/util/List;)V

    .line 179
    return-void
.end method

.method constructor <init>(Lcom/a/c/b/s;Lcom/a/c/j;Ljava/util/Map;ZZZZZZLcom/a/c/ah;Ljava/util/List;)V
    .registers 16

    .prologue
    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/a/c/k;->e:Ljava/lang/ThreadLocal;

    .line 113
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 114
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/k;->f:Ljava/util/Map;

    .line 124
    new-instance v0, Lcom/a/c/l;

    invoke-direct {v0, p0}, Lcom/a/c/l;-><init>(Lcom/a/c/k;)V

    iput-object v0, p0, Lcom/a/c/k;->b:Lcom/a/c/u;

    .line 131
    new-instance v0, Lcom/a/c/m;

    invoke-direct {v0, p0}, Lcom/a/c/m;-><init>(Lcom/a/c/k;)V

    iput-object v0, p0, Lcom/a/c/k;->c:Lcom/a/c/ad;

    .line 187
    new-instance v0, Lcom/a/c/b/f;

    invoke-direct {v0, p3}, Lcom/a/c/b/f;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/a/c/k;->h:Lcom/a/c/b/f;

    .line 188
    iput-boolean p4, p0, Lcom/a/c/k;->i:Z

    .line 189
    iput-boolean p6, p0, Lcom/a/c/k;->k:Z

    .line 190
    iput-boolean p7, p0, Lcom/a/c/k;->j:Z

    .line 191
    iput-boolean p8, p0, Lcom/a/c/k;->l:Z

    .line 193
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 196
    sget-object v0, Lcom/a/c/b/a/z;->Q:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/a/c/b/a/n;->a:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    invoke-interface {v1, p11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 206
    sget-object v0, Lcom/a/c/b/a/z;->x:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    sget-object v0, Lcom/a/c/b/a/z;->m:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    sget-object v0, Lcom/a/c/b/a/z;->g:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    sget-object v0, Lcom/a/c/b/a/z;->i:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    sget-object v0, Lcom/a/c/b/a/z;->k:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v3, Ljava/lang/Long;

    .line 1305
    sget-object v0, Lcom/a/c/ah;->a:Lcom/a/c/ah;

    if-ne p10, v0, :cond_12e

    .line 1306
    sget-object v0, Lcom/a/c/b/a/z;->n:Lcom/a/c/an;

    .line 211
    :goto_6a
    invoke-static {v2, v3, v0}, Lcom/a/c/b/a/z;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v3, Ljava/lang/Double;

    .line 2249
    if-eqz p9, :cond_135

    .line 2250
    sget-object v0, Lcom/a/c/b/a/z;->p:Lcom/a/c/an;

    .line 213
    :goto_79
    invoke-static {v2, v3, v0}, Lcom/a/c/b/a/z;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v3, Ljava/lang/Float;

    .line 2273
    if-eqz p9, :cond_13c

    .line 2274
    sget-object v0, Lcom/a/c/b/a/z;->o:Lcom/a/c/an;

    .line 215
    :goto_88
    invoke-static {v2, v3, v0}, Lcom/a/c/b/a/z;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    sget-object v0, Lcom/a/c/b/a/z;->r:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    sget-object v0, Lcom/a/c/b/a/z;->t:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    sget-object v0, Lcom/a/c/b/a/z;->z:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    sget-object v0, Lcom/a/c/b/a/z;->B:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    const-class v0, Ljava/math/BigDecimal;

    sget-object v2, Lcom/a/c/b/a/z;->v:Lcom/a/c/an;

    invoke-static {v0, v2}, Lcom/a/c/b/a/z;->a(Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    const-class v0, Ljava/math/BigInteger;

    sget-object v2, Lcom/a/c/b/a/z;->w:Lcom/a/c/an;

    invoke-static {v0, v2}, Lcom/a/c/b/a/z;->a(Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    sget-object v0, Lcom/a/c/b/a/z;->D:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    sget-object v0, Lcom/a/c/b/a/z;->F:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v0, Lcom/a/c/b/a/z;->J:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    sget-object v0, Lcom/a/c/b/a/z;->O:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    sget-object v0, Lcom/a/c/b/a/z;->H:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    sget-object v0, Lcom/a/c/b/a/z;->d:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    sget-object v0, Lcom/a/c/b/a/e;->a:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    sget-object v0, Lcom/a/c/b/a/z;->M:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    sget-object v0, Lcom/a/c/b/a/w;->a:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    sget-object v0, Lcom/a/c/b/a/u;->a:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    sget-object v0, Lcom/a/c/b/a/z;->K:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    sget-object v0, Lcom/a/c/b/a/a;->a:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    sget-object v0, Lcom/a/c/b/a/z;->R:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    sget-object v0, Lcom/a/c/b/a/z;->b:Lcom/a/c/ap;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    new-instance v0, Lcom/a/c/b/a/c;

    iget-object v2, p0, Lcom/a/c/k;->h:Lcom/a/c/b/f;

    invoke-direct {v0, v2}, Lcom/a/c/b/a/c;-><init>(Lcom/a/c/b/f;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    new-instance v0, Lcom/a/c/b/a/l;

    iget-object v2, p0, Lcom/a/c/k;->h:Lcom/a/c/b/f;

    invoke-direct {v0, v2, p5}, Lcom/a/c/b/a/l;-><init>(Lcom/a/c/b/f;Z)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    new-instance v0, Lcom/a/c/b/a/g;

    iget-object v2, p0, Lcom/a/c/k;->h:Lcom/a/c/b/f;

    invoke-direct {v0, v2}, Lcom/a/c/b/a/g;-><init>(Lcom/a/c/b/f;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    new-instance v0, Lcom/a/c/b/a/q;

    iget-object v2, p0, Lcom/a/c/k;->h:Lcom/a/c/b/f;

    invoke-direct {v0, v2, p2, p1}, Lcom/a/c/b/a/q;-><init>(Lcom/a/c/b/f;Lcom/a/c/j;Lcom/a/c/b/s;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/k;->g:Ljava/util/List;

    .line 246
    return-void

    .line 1308
    :cond_12e
    new-instance v0, Lcom/a/c/p;

    invoke-direct {v0, p0}, Lcom/a/c/p;-><init>(Lcom/a/c/k;)V

    goto/16 :goto_6a

    .line 2252
    :cond_135
    new-instance v0, Lcom/a/c/n;

    invoke-direct {v0, p0}, Lcom/a/c/n;-><init>(Lcom/a/c/k;)V

    goto/16 :goto_79

    .line 2276
    :cond_13c
    new-instance v0, Lcom/a/c/o;

    invoke-direct {v0, p0}, Lcom/a/c/o;-><init>(Lcom/a/c/k;)V

    goto/16 :goto_88
.end method

.method private a(Lcom/a/c/ah;)Lcom/a/c/an;
    .registers 3

    .prologue
    .line 305
    sget-object v0, Lcom/a/c/ah;->a:Lcom/a/c/ah;

    if-ne p1, v0, :cond_7

    .line 306
    sget-object v0, Lcom/a/c/b/a/z;->n:Lcom/a/c/an;

    .line 308
    :goto_6
    return-object v0

    :cond_7
    new-instance v0, Lcom/a/c/p;

    invoke-direct {v0, p0}, Lcom/a/c/p;-><init>(Lcom/a/c/k;)V

    goto :goto_6
.end method

.method private a(Z)Lcom/a/c/an;
    .registers 3

    .prologue
    .line 249
    if-eqz p1, :cond_5

    .line 250
    sget-object v0, Lcom/a/c/b/a/z;->p:Lcom/a/c/an;

    .line 252
    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/c/n;

    invoke-direct {v0, p0}, Lcom/a/c/n;-><init>(Lcom/a/c/k;)V

    goto :goto_4
.end method

.method private a(Ljava/io/Writer;)Lcom/a/c/d/e;
    .registers 5

    .prologue
    .line 640
    iget-boolean v0, p0, Lcom/a/c/k;->k:Z

    if-eqz v0, :cond_9

    .line 641
    const-string v0, ")]}\'\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 643
    :cond_9
    new-instance v0, Lcom/a/c/d/e;

    invoke-direct {v0, p1}, Lcom/a/c/d/e;-><init>(Ljava/io/Writer;)V

    .line 644
    iget-boolean v1, p0, Lcom/a/c/k;->l:Z

    if-eqz v1, :cond_21

    .line 645
    const-string v1, "  "

    .line 16213
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_26

    .line 16214
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/a/c/d/e;->a:Ljava/lang/String;

    .line 16215
    const-string v1, ":"

    iput-object v1, v0, Lcom/a/c/d/e;->b:Ljava/lang/String;

    .line 647
    :cond_21
    :goto_21
    iget-boolean v1, p0, Lcom/a/c/k;->i:Z

    .line 16269
    iput-boolean v1, v0, Lcom/a/c/d/e;->e:Z

    .line 648
    return-object v0

    .line 16217
    :cond_26
    iput-object v1, v0, Lcom/a/c/d/e;->a:Ljava/lang/String;

    .line 16218
    const-string v1, ": "

    iput-object v1, v0, Lcom/a/c/d/e;->b:Ljava/lang/String;

    goto :goto_21
.end method

.method private a(Ljava/lang/Object;)Lcom/a/c/w;
    .registers 3

    .prologue
    .line 464
    if-nez p1, :cond_5

    .line 465
    sget-object v0, Lcom/a/c/y;->a:Lcom/a/c/y;

    .line 467
    :goto_4
    return-object v0

    :cond_5
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/c/k;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/a/c/w;

    move-result-object v0

    goto :goto_4
.end method

.method private a(Lcom/a/c/w;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 843
    invoke-virtual {p0, p1, p2}, Lcom/a/c/k;->a(Lcom/a/c/w;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 844
    invoke-static {p2}, Lcom/a/c/b/ap;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 742
    new-instance v0, Lcom/a/c/d/a;

    invoke-direct {v0, p1}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 743
    invoke-virtual {p0, v0, p2}, Lcom/a/c/k;->a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    .line 744
    invoke-static {v1, v0}, Lcom/a/c/k;->a(Ljava/lang/Object;Lcom/a/c/d/a;)V

    .line 745
    invoke-static {p2}, Lcom/a/c/b/ap;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 769
    new-instance v0, Lcom/a/c/d/a;

    invoke-direct {v0, p1}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 770
    invoke-virtual {p0, v0, p2}, Lcom/a/c/k;->a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    .line 771
    invoke-static {v1, v0}, Lcom/a/c/k;->a(Ljava/lang/Object;Lcom/a/c/d/a;)V

    .line 772
    return-object v1
.end method

.method private a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 691
    .line 22715
    if-nez p1, :cond_c

    .line 22716
    const/4 v0, 0x0

    .line 692
    :goto_3
    invoke-static {p2}, Lcom/a/c/b/ap;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 22718
    :cond_c
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 22769
    new-instance v1, Lcom/a/c/d/a;

    invoke-direct {v1, v0}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 22770
    invoke-virtual {p0, v1, p2}, Lcom/a/c/k;->a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 22771
    invoke-static {v0, v1}, Lcom/a/c/k;->a(Ljava/lang/Object;Lcom/a/c/d/a;)V

    goto :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 715
    if-nez p1, :cond_4

    .line 716
    const/4 v0, 0x0

    .line 720
    :goto_3
    return-object v0

    .line 718
    :cond_4
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 23769
    new-instance v1, Lcom/a/c/d/a;

    invoke-direct {v1, v0}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 23770
    invoke-virtual {p0, v1, p2}, Lcom/a/c/k;->a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 23771
    invoke-static {v0, v1}, Lcom/a/c/k;->a(Ljava/lang/Object;Lcom/a/c/d/a;)V

    goto :goto_3
.end method

.method private a(Lcom/a/c/w;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 613
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 614
    invoke-direct {p0, p1, v0}, Lcom/a/c/k;->a(Lcom/a/c/w;Ljava/lang/Appendable;)V

    .line 615
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(D)V
    .registers 6

    .prologue
    .line 98
    .line 29297
    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {p0, p1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 29298
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_25
    return-void
.end method

.method private a(Lcom/a/c/w;Lcom/a/c/d/e;)V
    .registers 8

    .prologue
    .line 656
    .line 17242
    iget-boolean v1, p2, Lcom/a/c/d/e;->c:Z

    .line 18235
    const/4 v0, 0x1

    iput-boolean v0, p2, Lcom/a/c/d/e;->c:Z

    .line 18261
    iget-boolean v2, p2, Lcom/a/c/d/e;->d:Z

    .line 659
    iget-boolean v0, p0, Lcom/a/c/k;->j:Z

    .line 19253
    iput-boolean v0, p2, Lcom/a/c/d/e;->d:Z

    .line 19277
    iget-boolean v3, p2, Lcom/a/c/d/e;->e:Z

    .line 661
    iget-boolean v0, p0, Lcom/a/c/k;->i:Z

    .line 20269
    iput-boolean v0, p2, Lcom/a/c/d/e;->e:Z

    .line 663
    :try_start_11
    invoke-static {p1, p2}, Lcom/a/c/b/aq;->a(Lcom/a/c/w;Lcom/a/c/d/e;)V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_14} :catch_1b
    .catchall {:try_start_11 .. :try_end_14} :catchall_22

    .line 21235
    iput-boolean v1, p2, Lcom/a/c/d/e;->c:Z

    .line 21253
    iput-boolean v2, p2, Lcom/a/c/d/e;->d:Z

    .line 21269
    iput-boolean v3, p2, Lcom/a/c/d/e;->e:Z

    .line 670
    return-void

    .line 664
    :catch_1b
    move-exception v0

    .line 665
    :try_start_1c
    new-instance v4, Lcom/a/c/x;

    invoke-direct {v4, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_22
    .catchall {:try_start_1c .. :try_end_22} :catchall_22

    .line 667
    :catchall_22
    move-exception v0

    .line 22235
    iput-boolean v1, p2, Lcom/a/c/d/e;->c:Z

    .line 22253
    iput-boolean v2, p2, Lcom/a/c/d/e;->d:Z

    .line 22269
    iput-boolean v3, p2, Lcom/a/c/d/e;->e:Z

    .line 669
    throw v0
.end method

.method private a(Lcom/a/c/w;Ljava/lang/Appendable;)V
    .registers 9

    .prologue
    .line 628
    :try_start_0
    invoke-static {p2}, Lcom/a/c/b/aq;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/c/k;->a(Ljava/io/Writer;)Lcom/a/c/d/e;

    move-result-object v1

    .line 10242
    iget-boolean v2, v1, Lcom/a/c/d/e;->c:Z

    .line 11235
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/a/c/d/e;->c:Z

    .line 11261
    iget-boolean v3, v1, Lcom/a/c/d/e;->d:Z

    .line 9659
    iget-boolean v0, p0, Lcom/a/c/k;->j:Z

    .line 12253
    iput-boolean v0, v1, Lcom/a/c/d/e;->d:Z

    .line 12277
    iget-boolean v4, v1, Lcom/a/c/d/e;->e:Z

    .line 9661
    iget-boolean v0, p0, Lcom/a/c/k;->i:Z

    .line 13269
    iput-boolean v0, v1, Lcom/a/c/d/e;->e:Z
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_19} :catch_32

    .line 9663
    :try_start_19
    invoke-static {p1, v1}, Lcom/a/c/b/aq;->a(Lcom/a/c/w;Lcom/a/c/d/e;)V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_1c} :catch_23
    .catchall {:try_start_19 .. :try_end_1c} :catchall_2a

    .line 14235
    :try_start_1c
    iput-boolean v2, v1, Lcom/a/c/d/e;->c:Z

    .line 14253
    iput-boolean v3, v1, Lcom/a/c/d/e;->d:Z

    .line 14269
    iput-boolean v4, v1, Lcom/a/c/d/e;->e:Z
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_22} :catch_32

    .line 9670
    return-void

    .line 9664
    :catch_23
    move-exception v0

    .line 9665
    :try_start_24
    new-instance v5, Lcom/a/c/x;

    invoke-direct {v5, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_2a
    .catchall {:try_start_24 .. :try_end_2a} :catchall_2a

    .line 9667
    :catchall_2a
    move-exception v0

    .line 15235
    :try_start_2b
    iput-boolean v2, v1, Lcom/a/c/d/e;->c:Z

    .line 15253
    iput-boolean v3, v1, Lcom/a/c/d/e;->d:Z

    .line 15269
    iput-boolean v4, v1, Lcom/a/c/d/e;->e:Z

    .line 9669
    throw v0
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_32} :catch_32

    .line 630
    :catch_32
    move-exception v0

    .line 631
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/Object;Lcom/a/c/d/a;)V
    .registers 4

    .prologue
    .line 777
    if-eqz p0, :cond_20

    :try_start_2
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->j:Lcom/a/c/d/d;

    if-eq v0, v1, :cond_20

    .line 778
    new-instance v0, Lcom/a/c/x;

    const-string v1, "JSON document was not fully consumed."

    invoke-direct {v0, v1}, Lcom/a/c/x;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_12
    .catch Lcom/a/c/d/f; {:try_start_2 .. :try_end_12} :catch_12
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_12} :catch_19

    .line 780
    :catch_12
    move-exception v0

    .line 781
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 782
    :catch_19
    move-exception v0

    .line 783
    new-instance v1, Lcom/a/c/x;

    invoke-direct {v1, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 784
    :cond_20
    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Appendable;)V
    .registers 4

    .prologue
    .line 548
    if-eqz p1, :cond_a

    .line 549
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/a/c/k;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    .line 553
    :goto_9
    return-void

    .line 551
    :cond_a
    sget-object v0, Lcom/a/c/y;->a:Lcom/a/c/y;

    invoke-direct {p0, v0, p2}, Lcom/a/c/k;->a(Lcom/a/c/w;Ljava/lang/Appendable;)V

    goto :goto_9
.end method

.method private a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/a/c/d/e;)V
    .registers 9

    .prologue
    .line 587
    invoke-static {p2}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    .line 4242
    iget-boolean v1, p3, Lcom/a/c/d/e;->c:Z

    .line 5235
    const/4 v2, 0x1

    iput-boolean v2, p3, Lcom/a/c/d/e;->c:Z

    .line 5261
    iget-boolean v2, p3, Lcom/a/c/d/e;->d:Z

    .line 591
    iget-boolean v3, p0, Lcom/a/c/k;->j:Z

    .line 6253
    iput-boolean v3, p3, Lcom/a/c/d/e;->d:Z

    .line 6277
    iget-boolean v3, p3, Lcom/a/c/d/e;->e:Z

    .line 593
    iget-boolean v4, p0, Lcom/a/c/k;->i:Z

    .line 7269
    iput-boolean v4, p3, Lcom/a/c/d/e;->e:Z

    .line 595
    :try_start_19
    invoke-virtual {v0, p3, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_1c} :catch_23
    .catchall {:try_start_19 .. :try_end_1c} :catchall_2a

    .line 8235
    iput-boolean v1, p3, Lcom/a/c/d/e;->c:Z

    .line 8253
    iput-boolean v2, p3, Lcom/a/c/d/e;->d:Z

    .line 8269
    iput-boolean v3, p3, Lcom/a/c/d/e;->e:Z

    .line 602
    return-void

    .line 596
    :catch_23
    move-exception v0

    .line 597
    :try_start_24
    new-instance v4, Lcom/a/c/x;

    invoke-direct {v4, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_2a
    .catchall {:try_start_24 .. :try_end_2a} :catchall_2a

    .line 599
    :catchall_2a
    move-exception v0

    .line 9235
    iput-boolean v1, p3, Lcom/a/c/d/e;->c:Z

    .line 9253
    iput-boolean v2, p3, Lcom/a/c/d/e;->d:Z

    .line 9269
    iput-boolean v3, p3, Lcom/a/c/d/e;->e:Z

    .line 601
    throw v0
.end method

.method private b(Z)Lcom/a/c/an;
    .registers 3

    .prologue
    .line 273
    if-eqz p1, :cond_5

    .line 274
    sget-object v0, Lcom/a/c/b/a/z;->o:Lcom/a/c/an;

    .line 276
    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/c/o;

    invoke-direct {v0, p0}, Lcom/a/c/o;-><init>(Lcom/a/c/k;)V

    goto :goto_4
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 509
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 3528
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 3529
    invoke-virtual {p0, p1, v0, v1}, Lcom/a/c/k;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    .line 3530
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 509
    return-object v0
.end method

.method private b(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 528
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 529
    invoke-virtual {p0, p1, p2, v0}, Lcom/a/c/k;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    .line 530
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(D)V
    .registers 6

    .prologue
    .line 297
    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {p0, p1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 298
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_25
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/c/ap;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 6

    .prologue
    .line 422
    const/4 v0, 0x0

    .line 424
    iget-object v1, p0, Lcom/a/c/k;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_8
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ap;

    .line 425
    if-nez v1, :cond_1b

    .line 426
    if-ne v0, p1, :cond_8

    .line 427
    const/4 v0, 0x1

    move v1, v0

    goto :goto_8

    .line 432
    :cond_1b
    invoke-interface {v0, p0, p2}, Lcom/a/c/ap;->a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    .line 433
    if-eqz v0, :cond_8

    .line 434
    return-object v0

    .line 437
    :cond_22
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GSON cannot serialize "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 7

    .prologue
    .line 334
    iget-object v0, p0, Lcom/a/c/k;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/an;

    .line 335
    if-eqz v0, :cond_b

    .line 370
    :cond_a
    :goto_a
    return-object v0

    .line 339
    :cond_b
    iget-object v0, p0, Lcom/a/c/k;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 340
    const/4 v1, 0x0

    .line 341
    if-nez v0, :cond_88

    .line 342
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 343
    iget-object v0, p0, Lcom/a/c/k;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 344
    const/4 v0, 0x1

    move-object v2, v1

    move v1, v0

    .line 348
    :goto_23
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/q;

    .line 349
    if-nez v0, :cond_a

    .line 354
    :try_start_2b
    new-instance v3, Lcom/a/c/q;

    invoke-direct {v3}, Lcom/a/c/q;-><init>()V

    .line 355
    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    iget-object v0, p0, Lcom/a/c/k;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_39
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_73

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/ap;

    .line 358
    invoke-interface {v0, p0, p1}, Lcom/a/c/ap;->a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_39

    .line 2877
    iget-object v4, v3, Lcom/a/c/q;->a:Lcom/a/c/an;

    if-eqz v4, :cond_61

    .line 2878
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_55
    .catchall {:try_start_2b .. :try_end_55} :catchall_55

    .line 367
    :catchall_55
    move-exception v0

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    if-eqz v1, :cond_60

    .line 370
    iget-object v1, p0, Lcom/a/c/k;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    :cond_60
    throw v0

    .line 2880
    :cond_61
    :try_start_61
    iput-object v0, v3, Lcom/a/c/q;->a:Lcom/a/c/an;

    .line 361
    iget-object v3, p0, Lcom/a/c/k;->f:Ljava/util/Map;

    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_68
    .catchall {:try_start_61 .. :try_end_68} :catchall_55

    .line 367
    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    if-eqz v1, :cond_a

    .line 370
    iget-object v1, p0, Lcom/a/c/k;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_a

    .line 365
    :cond_73
    :try_start_73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GSON cannot handle "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_88
    .catchall {:try_start_73 .. :try_end_88} :catchall_55

    :cond_88
    move-object v2, v0

    goto :goto_23
.end method

.method public final a(Ljava/lang/Class;)Lcom/a/c/an;
    .registers 3

    .prologue
    .line 447
    invoke-static {p1}, Lcom/a/c/c/a;->a(Ljava/lang/Class;)Lcom/a/c/c/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/a/c/w;
    .registers 4

    .prologue
    .line 487
    new-instance v0, Lcom/a/c/b/a/j;

    invoke-direct {v0}, Lcom/a/c/b/a/j;-><init>()V

    .line 488
    invoke-direct {p0, p1, p2, v0}, Lcom/a/c/k;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/a/c/d/e;)V

    .line 489
    invoke-virtual {v0}, Lcom/a/c/b/a/j;->a()Lcom/a/c/w;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .registers 6

    .prologue
    const/4 v1, 0x1

    .line 797
    .line 24333
    iget-boolean v2, p1, Lcom/a/c/d/a;->b:Z

    .line 25326
    iput-boolean v1, p1, Lcom/a/c/d/a;->b:Z

    .line 801
    :try_start_5
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    .line 802
    const/4 v1, 0x0

    .line 803
    invoke-static {p2}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v0

    .line 804
    invoke-virtual {p0, v0}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    .line 805
    invoke-virtual {v0, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;
    :try_end_14
    .catch Ljava/io/EOFException; {:try_start_5 .. :try_end_14} :catch_18
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_14} :catch_29
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_14} :catch_30
    .catchall {:try_start_5 .. :try_end_14} :catchall_25

    move-result-object v0

    .line 26326
    iput-boolean v2, p1, Lcom/a/c/d/a;->b:Z

    .line 822
    :goto_17
    return-object v0

    .line 807
    :catch_18
    move-exception v0

    .line 812
    if-eqz v1, :cond_1f

    .line 27326
    iput-boolean v2, p1, Lcom/a/c/d/a;->b:Z

    .line 822
    const/4 v0, 0x0

    goto :goto_17

    .line 815
    :cond_1f
    :try_start_1f
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_25
    .catchall {:try_start_1f .. :try_end_25} :catchall_25

    .line 822
    :catchall_25
    move-exception v0

    .line 28326
    iput-boolean v2, p1, Lcom/a/c/d/a;->b:Z

    .line 822
    throw v0

    .line 816
    :catch_29
    move-exception v0

    .line 817
    :try_start_2a
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 818
    :catch_30
    move-exception v0

    .line 820
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_37
    .catchall {:try_start_2a .. :try_end_37} :catchall_25
.end method

.method public final a(Lcom/a/c/w;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 867
    if-nez p1, :cond_4

    .line 868
    const/4 v0, 0x0

    .line 870
    :goto_3
    return-object v0

    :cond_4
    new-instance v0, Lcom/a/c/b/a/h;

    invoke-direct {v0, p1}, Lcom/a/c/b/a/h;-><init>(Lcom/a/c/w;)V

    invoke-virtual {p0, v0, p2}, Lcom/a/c/k;->a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
    .registers 6

    .prologue
    .line 573
    :try_start_0
    invoke-static {p3}, Lcom/a/c/b/aq;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/c/k;->a(Ljava/io/Writer;)Lcom/a/c/d/e;

    move-result-object v0

    .line 574
    invoke-direct {p0, p1, p2, v0}, Lcom/a/c/k;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/a/c/d/e;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_b} :catch_c

    .line 577
    return-void

    .line 575
    :catch_c
    move-exception v0

    .line 576
    new-instance v1, Lcom/a/c/x;

    invoke-direct {v1, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 900
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{serializeNulls:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/a/c/k;->i:Z

    .line 901
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "factories:"

    .line 902
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/a/c/k;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",instanceCreators:"

    .line 903
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/a/c/k;->h:Lcom/a/c/b/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    .line 904
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 905
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
