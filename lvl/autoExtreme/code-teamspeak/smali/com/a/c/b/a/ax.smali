.class final Lcom/a/c/b/a/ax;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/lang/Boolean;)V
    .registers 3

    .prologue
    .line 157
    if-nez p1, :cond_6

    .line 158
    invoke-virtual {p0}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 162
    :goto_5
    return-void

    .line 161
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->a(Z)Lcom/a/c/d/e;

    goto :goto_5
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/lang/Boolean;
    .registers 3

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 147
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 148
    const/4 v0, 0x0

    .line 153
    :goto_c
    return-object v0

    .line 149
    :cond_d
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_22

    .line 151
    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_c

    .line 153
    :cond_22
    invoke-virtual {p0}, Lcom/a/c/d/a;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_c
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 143
    .line 1146
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 1147
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1148
    const/4 v0, 0x0

    .line 1151
    :goto_c
    return-object v0

    .line 1149
    :cond_d
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_22

    .line 1151
    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_c

    .line 1153
    :cond_22
    invoke-virtual {p1}, Lcom/a/c/d/a;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_c
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 143
    check-cast p2, Ljava/lang/Boolean;

    .line 1157
    if-nez p2, :cond_8

    .line 1158
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 1159
    :goto_7
    return-void

    .line 1161
    :cond_8
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Z)Lcom/a/c/d/e;

    goto :goto_7
.end method
