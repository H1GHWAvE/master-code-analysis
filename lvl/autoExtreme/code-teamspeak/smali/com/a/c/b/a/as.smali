.class final Lcom/a/c/b/a/as;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 642
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private a(Lcom/a/c/d/e;Lcom/a/c/w;)V
    .registers 6

    .prologue
    .line 681
    if-eqz p2, :cond_6

    .line 1074
    instance-of v0, p2, Lcom/a/c/y;

    .line 681
    if-eqz v0, :cond_a

    .line 682
    :cond_6
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 706
    :goto_9
    return-void

    .line 2064
    :cond_a
    instance-of v0, p2, Lcom/a/c/ac;

    .line 683
    if-eqz v0, :cond_36

    .line 684
    invoke-virtual {p2}, Lcom/a/c/w;->n()Lcom/a/c/ac;

    move-result-object v0

    .line 2146
    iget-object v1, v0, Lcom/a/c/ac;->a:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Number;

    .line 685
    if-eqz v1, :cond_20

    .line 686
    invoke-virtual {v0}, Lcom/a/c/ac;->a()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    goto :goto_9

    .line 3112
    :cond_20
    iget-object v1, v0, Lcom/a/c/ac;->a:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Boolean;

    .line 687
    if-eqz v1, :cond_2e

    .line 688
    invoke-virtual {v0}, Lcom/a/c/ac;->l()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->a(Z)Lcom/a/c/d/e;

    goto :goto_9

    .line 690
    :cond_2e
    invoke-virtual {v0}, Lcom/a/c/ac;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    goto :goto_9

    .line 4046
    :cond_36
    instance-of v0, p2, Lcom/a/c/t;

    .line 693
    if-eqz v0, :cond_63

    .line 694
    invoke-virtual {p1}, Lcom/a/c/d/e;->b()Lcom/a/c/d/e;

    .line 5046
    instance-of v0, p2, Lcom/a/c/t;

    .line 4103
    if-eqz v0, :cond_57

    .line 4104
    check-cast p2, Lcom/a/c/t;

    .line 695
    invoke-virtual {p2}, Lcom/a/c/t;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_47
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/w;

    .line 696
    invoke-direct {p0, p1, v0}, Lcom/a/c/b/a/as;->a(Lcom/a/c/d/e;Lcom/a/c/w;)V

    goto :goto_47

    .line 4106
    :cond_57
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This is not a JSON Array."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 698
    :cond_5f
    invoke-virtual {p1}, Lcom/a/c/d/e;->c()Lcom/a/c/d/e;

    goto :goto_9

    .line 5055
    :cond_63
    instance-of v0, p2, Lcom/a/c/z;

    .line 700
    if-eqz v0, :cond_b3

    .line 701
    invoke-virtual {p1}, Lcom/a/c/d/e;->d()Lcom/a/c/d/e;

    .line 6055
    instance-of v0, p2, Lcom/a/c/z;

    .line 5087
    if-eqz v0, :cond_99

    .line 5088
    check-cast p2, Lcom/a/c/z;

    .line 6132
    iget-object v0, p2, Lcom/a/c/z;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0}, Lcom/a/c/b/ag;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 702
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ae

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 703
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 704
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/w;

    invoke-direct {p0, p1, v0}, Lcom/a/c/b/a/as;->a(Lcom/a/c/d/e;Lcom/a/c/w;)V

    goto :goto_7a

    .line 5090
    :cond_99
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a JSON Object: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 706
    :cond_ae
    invoke-virtual {p1}, Lcom/a/c/d/e;->e()Lcom/a/c/d/e;

    goto/16 :goto_9

    .line 709
    :cond_b3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t write "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Lcom/a/c/d/a;)Lcom/a/c/w;
    .registers 5

    .prologue
    .line 644
    sget-object v0, Lcom/a/c/b/a/ba;->a:[I

    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/c/d/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7a

    .line 676
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 646
    :pswitch_15
    new-instance v0, Lcom/a/c/ac;

    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/c/ac;-><init>(Ljava/lang/String;)V

    .line 670
    :goto_1e
    return-object v0

    .line 648
    :pswitch_1f
    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    .line 649
    new-instance v0, Lcom/a/c/ac;

    new-instance v2, Lcom/a/c/b/v;

    invoke-direct {v2, v1}, Lcom/a/c/b/v;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/a/c/ac;-><init>(Ljava/lang/Number;)V

    goto :goto_1e

    .line 651
    :pswitch_2e
    new-instance v0, Lcom/a/c/ac;

    invoke-virtual {p1}, Lcom/a/c/d/a;->j()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/c/ac;-><init>(Ljava/lang/Boolean;)V

    goto :goto_1e

    .line 653
    :pswitch_3c
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 654
    sget-object v0, Lcom/a/c/y;->a:Lcom/a/c/y;

    goto :goto_1e

    .line 656
    :pswitch_42
    new-instance v0, Lcom/a/c/t;

    invoke-direct {v0}, Lcom/a/c/t;-><init>()V

    .line 657
    invoke-virtual {p1}, Lcom/a/c/d/a;->a()V

    .line 658
    :goto_4a
    invoke-virtual {p1}, Lcom/a/c/d/a;->e()Z

    move-result v1

    if-eqz v1, :cond_58

    .line 659
    invoke-direct {p0, p1}, Lcom/a/c/b/a/as;->b(Lcom/a/c/d/a;)Lcom/a/c/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/c/t;->a(Lcom/a/c/w;)V

    goto :goto_4a

    .line 661
    :cond_58
    invoke-virtual {p1}, Lcom/a/c/d/a;->b()V

    goto :goto_1e

    .line 664
    :pswitch_5c
    new-instance v0, Lcom/a/c/z;

    invoke-direct {v0}, Lcom/a/c/z;-><init>()V

    .line 665
    invoke-virtual {p1}, Lcom/a/c/d/a;->c()V

    .line 666
    :goto_64
    invoke-virtual {p1}, Lcom/a/c/d/a;->e()Z

    move-result v1

    if-eqz v1, :cond_76

    .line 667
    invoke-virtual {p1}, Lcom/a/c/d/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/a/c/b/a/as;->b(Lcom/a/c/d/a;)Lcom/a/c/w;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/c/z;->a(Ljava/lang/String;Lcom/a/c/w;)V

    goto :goto_64

    .line 669
    :cond_76
    invoke-virtual {p1}, Lcom/a/c/d/a;->d()V

    goto :goto_1e

    .line 644
    :pswitch_data_7a
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_2e
        :pswitch_15
        :pswitch_3c
        :pswitch_42
        :pswitch_5c
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 642
    invoke-direct {p0, p1}, Lcom/a/c/b/a/as;->b(Lcom/a/c/d/a;)Lcom/a/c/w;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 642
    check-cast p2, Lcom/a/c/w;

    invoke-direct {p0, p1, p2}, Lcom/a/c/b/a/as;->a(Lcom/a/c/d/e;Lcom/a/c/w;)V

    return-void
.end method
