.class public final Lcom/a/c/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .registers 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/a/c/b/f;->a:Ljava/util/Map;

    .line 49
    return-void
.end method

.method private a(Ljava/lang/Class;)Lcom/a/c/b/ao;
    .registers 4

    .prologue
    .line 95
    const/4 v0, 0x0

    :try_start_1
    new-array v0, v0, [Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 96
    invoke-virtual {v1}, Ljava/lang/reflect/Constructor;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_11

    .line 97
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 99
    :cond_11
    new-instance v0, Lcom/a/c/b/l;

    invoke-direct {v0, p0, v1}, Lcom/a/c/b/l;-><init>(Lcom/a/c/b/f;Ljava/lang/reflect/Constructor;)V
    :try_end_16
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_16} :catch_17

    .line 119
    :goto_16
    return-object v0

    :catch_17
    move-exception v0

    const/4 v0, 0x0

    goto :goto_16
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/a/c/b/ao;
    .registers 6

    .prologue
    .line 130
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 131
    const-class v0, Ljava/util/SortedSet;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 132
    new-instance v0, Lcom/a/c/b/m;

    invoke-direct {v0, p0}, Lcom/a/c/b/m;-><init>(Lcom/a/c/b/f;)V

    .line 197
    :goto_15
    return-object v0

    .line 137
    :cond_16
    const-class v0, Ljava/util/EnumSet;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 138
    new-instance v0, Lcom/a/c/b/n;

    invoke-direct {v0, p0, p1}, Lcom/a/c/b/n;-><init>(Lcom/a/c/b/f;Ljava/lang/reflect/Type;)V

    goto :goto_15

    .line 153
    :cond_24
    const-class v0, Ljava/util/Set;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 154
    new-instance v0, Lcom/a/c/b/o;

    invoke-direct {v0, p0}, Lcom/a/c/b/o;-><init>(Lcom/a/c/b/f;)V

    goto :goto_15

    .line 159
    :cond_32
    const-class v0, Ljava/util/Queue;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 160
    new-instance v0, Lcom/a/c/b/p;

    invoke-direct {v0, p0}, Lcom/a/c/b/p;-><init>(Lcom/a/c/b/f;)V

    goto :goto_15

    .line 166
    :cond_40
    new-instance v0, Lcom/a/c/b/q;

    invoke-direct {v0, p0}, Lcom/a/c/b/q;-><init>(Lcom/a/c/b/f;)V

    goto :goto_15

    .line 174
    :cond_46
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_83

    .line 175
    const-class v0, Ljava/util/SortedMap;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 176
    new-instance v0, Lcom/a/c/b/r;

    invoke-direct {v0, p0}, Lcom/a/c/b/r;-><init>(Lcom/a/c/b/f;)V

    goto :goto_15

    .line 181
    :cond_5c
    instance-of v0, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_7d

    const-class v0, Ljava/lang/String;

    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    .line 182
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v1

    .line 4094
    iget-object v1, v1, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 181
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_7d

    .line 183
    new-instance v0, Lcom/a/c/b/h;

    invoke-direct {v0, p0}, Lcom/a/c/b/h;-><init>(Lcom/a/c/b/f;)V

    goto :goto_15

    .line 189
    :cond_7d
    new-instance v0, Lcom/a/c/b/i;

    invoke-direct {v0, p0}, Lcom/a/c/b/i;-><init>(Lcom/a/c/b/f;)V

    goto :goto_15

    .line 197
    :cond_83
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private b(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/a/c/b/ao;
    .registers 4

    .prologue
    .line 202
    new-instance v0, Lcom/a/c/b/j;

    invoke-direct {v0, p0, p2, p1}, Lcom/a/c/b/j;-><init>(Lcom/a/c/b/f;Ljava/lang/Class;Ljava/lang/reflect/Type;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/c/c/a;)Lcom/a/c/b/ao;
    .registers 7

    .prologue
    .line 52
    .line 1101
    iget-object v1, p1, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 2094
    iget-object v2, p1, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 58
    iget-object v0, p0, Lcom/a/c/b/f;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/s;

    .line 59
    if-eqz v0, :cond_15

    .line 60
    new-instance v2, Lcom/a/c/b/g;

    invoke-direct {v2, p0, v0, v1}, Lcom/a/c/b/g;-><init>(Lcom/a/c/b/f;Lcom/a/c/s;Ljava/lang/reflect/Type;)V

    move-object v0, v2

    .line 90
    :cond_14
    :goto_14
    return-object v0

    .line 69
    :cond_15
    iget-object v0, p0, Lcom/a/c/b/f;->a:Ljava/util/Map;

    .line 70
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/s;

    .line 71
    if-eqz v0, :cond_26

    .line 72
    new-instance v2, Lcom/a/c/b/k;

    invoke-direct {v2, p0, v0, v1}, Lcom/a/c/b/k;-><init>(Lcom/a/c/b/f;Lcom/a/c/s;Ljava/lang/reflect/Type;)V

    move-object v0, v2

    goto :goto_14

    .line 79
    :cond_26
    invoke-direct {p0, v2}, Lcom/a/c/b/f;->a(Ljava/lang/Class;)Lcom/a/c/b/ao;

    move-result-object v0

    .line 80
    if-nez v0, :cond_14

    .line 2130
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 2131
    const-class v0, Ljava/util/SortedSet;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 2132
    new-instance v0, Lcom/a/c/b/m;

    invoke-direct {v0, p0}, Lcom/a/c/b/m;-><init>(Lcom/a/c/b/f;)V

    .line 85
    :goto_41
    if-nez v0, :cond_14

    .line 3202
    new-instance v0, Lcom/a/c/b/j;

    invoke-direct {v0, p0, v2, v1}, Lcom/a/c/b/j;-><init>(Lcom/a/c/b/f;Ljava/lang/Class;Ljava/lang/reflect/Type;)V

    goto :goto_14

    .line 2137
    :cond_49
    const-class v0, Ljava/util/EnumSet;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 2138
    new-instance v0, Lcom/a/c/b/n;

    invoke-direct {v0, p0, v1}, Lcom/a/c/b/n;-><init>(Lcom/a/c/b/f;Ljava/lang/reflect/Type;)V

    goto :goto_41

    .line 2153
    :cond_57
    const-class v0, Ljava/util/Set;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 2154
    new-instance v0, Lcom/a/c/b/o;

    invoke-direct {v0, p0}, Lcom/a/c/b/o;-><init>(Lcom/a/c/b/f;)V

    goto :goto_41

    .line 2159
    :cond_65
    const-class v0, Ljava/util/Queue;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 2160
    new-instance v0, Lcom/a/c/b/p;

    invoke-direct {v0, p0}, Lcom/a/c/b/p;-><init>(Lcom/a/c/b/f;)V

    goto :goto_41

    .line 2166
    :cond_73
    new-instance v0, Lcom/a/c/b/q;

    invoke-direct {v0, p0}, Lcom/a/c/b/q;-><init>(Lcom/a/c/b/f;)V

    goto :goto_41

    .line 2174
    :cond_79
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 2175
    const-class v0, Ljava/util/SortedMap;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 2176
    new-instance v0, Lcom/a/c/b/r;

    invoke-direct {v0, p0}, Lcom/a/c/b/r;-><init>(Lcom/a/c/b/f;)V

    goto :goto_41

    .line 2181
    :cond_8f
    instance-of v0, v1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_b1

    const-class v3, Ljava/lang/String;

    move-object v0, v1

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 2182
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v4, 0x0

    aget-object v0, v0, v4

    invoke-static {v0}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v0

    .line 3094
    iget-object v0, v0, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 2181
    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_b1

    .line 2183
    new-instance v0, Lcom/a/c/b/h;

    invoke-direct {v0, p0}, Lcom/a/c/b/h;-><init>(Lcom/a/c/b/f;)V

    goto :goto_41

    .line 2189
    :cond_b1
    new-instance v0, Lcom/a/c/b/i;

    invoke-direct {v0, p0}, Lcom/a/c/b/i;-><init>(Lcom/a/c/b/f;)V

    goto :goto_41

    .line 2197
    :cond_b7
    const/4 v0, 0x0

    goto :goto_41
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/a/c/b/f;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
