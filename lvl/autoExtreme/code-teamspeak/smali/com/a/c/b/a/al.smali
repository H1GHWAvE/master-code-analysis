.class final Lcom/a/c/b/a/al;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/util/BitSet;)V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 127
    if-nez p1, :cond_7

    .line 128
    invoke-virtual {p0}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 138
    :goto_6
    return-void

    .line 132
    :cond_7
    invoke-virtual {p0}, Lcom/a/c/d/e;->b()Lcom/a/c/d/e;

    move v0, v1

    .line 133
    :goto_b
    invoke-virtual {p1}, Ljava/util/BitSet;->length()I

    move-result v2

    if-ge v0, v2, :cond_21

    .line 134
    invoke-virtual {p1, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-eqz v2, :cond_1f

    const/4 v2, 0x1

    .line 135
    :goto_18
    int-to-long v2, v2

    invoke-virtual {p0, v2, v3}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_1f
    move v2, v1

    .line 134
    goto :goto_18

    .line 137
    :cond_21
    invoke-virtual {p0}, Lcom/a/c/d/e;->c()Lcom/a/c/d/e;

    goto :goto_6
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/util/BitSet;
    .registers 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 86
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_f

    .line 87
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 88
    const/4 v0, 0x0

    .line 123
    :goto_e
    return-object v0

    .line 91
    :cond_f
    new-instance v4, Ljava/util/BitSet;

    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    .line 92
    invoke-virtual {p0}, Lcom/a/c/d/a;->a()V

    .line 94
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    move v1, v2

    .line 95
    :goto_1c
    sget-object v5, Lcom/a/c/d/d;->b:Lcom/a/c/d/d;

    if-eq v0, v5, :cond_7e

    .line 97
    sget-object v5, Lcom/a/c/b/a/ba;->a:[I

    invoke-virtual {v0}, Lcom/a/c/d/d;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_84

    .line 114
    new-instance v1, Lcom/a/c/ag;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid bitset value type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/String;)V

    throw v1

    .line 99
    :pswitch_40
    invoke-virtual {p0}, Lcom/a/c/d/a;->n()I

    move-result v0

    if-eqz v0, :cond_53

    move v0, v3

    .line 116
    :goto_47
    if-eqz v0, :cond_4c

    .line 117
    invoke-virtual {v4, v1}, Ljava/util/BitSet;->set(I)V

    .line 119
    :cond_4c
    add-int/lit8 v1, v1, 0x1

    .line 120
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    goto :goto_1c

    :cond_53
    move v0, v2

    .line 99
    goto :goto_47

    .line 102
    :pswitch_55
    invoke-virtual {p0}, Lcom/a/c/d/a;->j()Z

    move-result v0

    goto :goto_47

    .line 105
    :pswitch_5a
    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    .line 107
    :try_start_5e
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_61
    .catch Ljava/lang/NumberFormatException; {:try_start_5e .. :try_end_61} :catch_68

    move-result v0

    if-eqz v0, :cond_66

    move v0, v3

    goto :goto_47

    :cond_66
    move v0, v2

    goto :goto_47

    .line 109
    :catch_68
    move-exception v1

    new-instance v1, Lcom/a/c/ag;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error: Expecting: bitset number value (1, 0), Found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/String;)V

    throw v1

    .line 122
    :cond_7e
    invoke-virtual {p0}, Lcom/a/c/d/a;->b()V

    move-object v0, v4

    .line 123
    goto :goto_e

    .line 97
    nop

    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_40
        :pswitch_55
        :pswitch_5a
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 84
    invoke-static {p1}, Lcom/a/c/b/a/al;->b(Lcom/a/c/d/a;)Ljava/util/BitSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 84
    check-cast p2, Ljava/util/BitSet;

    .line 1127
    if-nez p2, :cond_9

    .line 1128
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 1129
    :goto_8
    return-void

    .line 1132
    :cond_9
    invoke-virtual {p1}, Lcom/a/c/d/e;->b()Lcom/a/c/d/e;

    move v0, v1

    .line 1133
    :goto_d
    invoke-virtual {p2}, Ljava/util/BitSet;->length()I

    move-result v2

    if-ge v0, v2, :cond_23

    .line 1134
    invoke-virtual {p2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-eqz v2, :cond_21

    const/4 v2, 0x1

    .line 1135
    :goto_1a
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/a/c/d/e;->a(J)Lcom/a/c/d/e;

    .line 1133
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_21
    move v2, v1

    .line 1134
    goto :goto_1a

    .line 1137
    :cond_23
    invoke-virtual {p1}, Lcom/a/c/d/e;->c()Lcom/a/c/d/e;

    goto :goto_8
.end method
