.class final Lcom/a/c/b/a/ak;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/net/URI;)V
    .registers 3

    .prologue
    .line 474
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 475
    return-void

    .line 474
    :cond_7
    invoke-virtual {p1}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/net/URI;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 461
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v1

    sget-object v2, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v1, v2, :cond_d

    .line 462
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 467
    :cond_c
    :goto_c
    return-object v0

    .line 466
    :cond_d
    :try_start_d
    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v1

    .line 467
    const-string v2, "null"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_1e
    .catch Ljava/net/URISyntaxException; {:try_start_d .. :try_end_1e} :catch_1f

    goto :goto_c

    .line 468
    :catch_1f
    move-exception v0

    .line 469
    new-instance v1, Lcom/a/c/x;

    invoke-direct {v1, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 458
    invoke-static {p1}, Lcom/a/c/b/a/ak;->b(Lcom/a/c/d/a;)Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 458
    check-cast p2, Ljava/net/URI;

    .line 1474
    if-nez p2, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 458
    return-void

    .line 1474
    :cond_9
    invoke-virtual {p2}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method
