.class public final Lcom/a/c/b/w;
.super Ljava/util/AbstractMap;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final synthetic g:Z

.field private static final h:Ljava/util/Comparator;


# instance fields
.field a:Ljava/util/Comparator;

.field b:[Lcom/a/c/b/af;

.field final c:Lcom/a/c/b/af;

.field d:I

.field e:I

.field f:I

.field private i:Lcom/a/c/b/aa;

.field private j:Lcom/a/c/b/ac;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 40
    const-class v0, Lcom/a/c/b/w;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_13

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/a/c/b/w;->g:Z

    .line 42
    new-instance v0, Lcom/a/c/b/x;

    invoke-direct {v0}, Lcom/a/c/b/x;-><init>()V

    sput-object v0, Lcom/a/c/b/w;->h:Ljava/util/Comparator;

    return-void

    .line 40
    :cond_13
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 61
    sget-object v0, Lcom/a/c/b/w;->h:Ljava/util/Comparator;

    invoke-direct {p0, v0}, Lcom/a/c/b/w;-><init>(Ljava/util/Comparator;)V

    .line 62
    return-void
.end method

.method private constructor <init>(Ljava/util/Comparator;)V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 51
    iput v0, p0, Lcom/a/c/b/w;->d:I

    .line 52
    iput v0, p0, Lcom/a/c/b/w;->e:I

    .line 73
    if-eqz p1, :cond_27

    :goto_a
    iput-object p1, p0, Lcom/a/c/b/w;->a:Ljava/util/Comparator;

    .line 76
    new-instance v0, Lcom/a/c/b/af;

    invoke-direct {v0}, Lcom/a/c/b/af;-><init>()V

    iput-object v0, p0, Lcom/a/c/b/w;->c:Lcom/a/c/b/af;

    .line 77
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/a/c/b/af;

    iput-object v0, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    .line 78
    iget-object v0, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/c/b/w;->f:I

    .line 79
    return-void

    .line 73
    :cond_27
    sget-object p1, Lcom/a/c/b/w;->h:Ljava/util/Comparator;

    goto :goto_a
.end method

.method private static a(I)I
    .registers 3

    .prologue
    .line 235
    ushr-int/lit8 v0, p0, 0x14

    ushr-int/lit8 v1, p0, 0xc

    xor-int/2addr v0, v1

    xor-int/2addr v0, p0

    .line 236
    ushr-int/lit8 v1, v0, 0x7

    xor-int/2addr v1, v0

    ushr-int/lit8 v0, v0, 0x4

    xor-int/2addr v0, v1

    return v0
.end method

.method private a(Ljava/lang/Object;Z)Lcom/a/c/b/af;
    .registers 13

    .prologue
    const/4 v2, 0x0

    .line 132
    iget-object v7, p0, Lcom/a/c/b/w;->a:Ljava/util/Comparator;

    .line 133
    iget-object v8, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    .line 134
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 1235
    ushr-int/lit8 v1, v0, 0x14

    ushr-int/lit8 v3, v0, 0xc

    xor-int/2addr v1, v3

    xor-int/2addr v0, v1

    .line 1236
    ushr-int/lit8 v1, v0, 0x7

    xor-int/2addr v1, v0

    ushr-int/lit8 v0, v0, 0x4

    xor-int v3, v1, v0

    .line 135
    array-length v0, v8

    add-int/lit8 v0, v0, -0x1

    and-int v9, v3, v0

    .line 136
    aget-object v1, v8, v9

    .line 137
    const/4 v0, 0x0

    .line 139
    if-eqz v1, :cond_bc

    .line 142
    sget-object v0, Lcom/a/c/b/w;->h:Ljava/util/Comparator;

    if-ne v7, v0, :cond_33

    move-object v0, p1

    check-cast v0, Ljava/lang/Comparable;

    .line 147
    :goto_27
    if-eqz v0, :cond_35

    iget-object v4, v1, Lcom/a/c/b/af;->f:Ljava/lang/Object;

    .line 148
    invoke-interface {v0, v4}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v4

    .line 152
    :goto_2f
    if-nez v4, :cond_3c

    move-object v2, v1

    .line 196
    :cond_32
    :goto_32
    return-object v2

    :cond_33
    move-object v0, v2

    .line 142
    goto :goto_27

    .line 148
    :cond_35
    iget-object v4, v1, Lcom/a/c/b/af;->f:Ljava/lang/Object;

    .line 149
    invoke-interface {v7, p1, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    goto :goto_2f

    .line 157
    :cond_3c
    if-gez v4, :cond_44

    iget-object v5, v1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 158
    :goto_40
    if-eqz v5, :cond_47

    move-object v1, v5

    .line 163
    goto :goto_27

    .line 157
    :cond_44
    iget-object v5, v1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    goto :goto_40

    :cond_47
    move v6, v4

    .line 167
    :goto_48
    if-eqz p2, :cond_32

    .line 172
    iget-object v4, p0, Lcom/a/c/b/w;->c:Lcom/a/c/b/af;

    .line 174
    if-nez v1, :cond_a8

    .line 176
    sget-object v0, Lcom/a/c/b/w;->h:Ljava/util/Comparator;

    if-ne v7, v0, :cond_77

    instance-of v0, p1, Ljava/lang/Comparable;

    if-nez v0, :cond_77

    .line 177
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not Comparable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_77
    new-instance v0, Lcom/a/c/b/af;

    iget-object v5, v4, Lcom/a/c/b/af;->e:Lcom/a/c/b/af;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/a/c/b/af;-><init>(Lcom/a/c/b/af;Ljava/lang/Object;ILcom/a/c/b/af;Lcom/a/c/b/af;)V

    .line 180
    aput-object v0, v8, v9

    .line 191
    :goto_81
    iget v1, p0, Lcom/a/c/b/w;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/a/c/b/w;->d:I

    iget v2, p0, Lcom/a/c/b/w;->f:I

    if-le v1, v2, :cond_a0

    .line 1558
    iget-object v1, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    invoke-static {v1}, Lcom/a/c/b/w;->a([Lcom/a/c/b/af;)[Lcom/a/c/b/af;

    move-result-object v1

    iput-object v1, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    .line 1559
    iget-object v1, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    array-length v2, v2

    div-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    iput v1, p0, Lcom/a/c/b/w;->f:I

    .line 194
    :cond_a0
    iget v1, p0, Lcom/a/c/b/w;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/c/b/w;->e:I

    move-object v2, v0

    .line 196
    goto :goto_32

    .line 182
    :cond_a8
    new-instance v0, Lcom/a/c/b/af;

    iget-object v5, v4, Lcom/a/c/b/af;->e:Lcom/a/c/b/af;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/a/c/b/af;-><init>(Lcom/a/c/b/af;Ljava/lang/Object;ILcom/a/c/b/af;Lcom/a/c/b/af;)V

    .line 183
    if-gez v6, :cond_b9

    .line 184
    iput-object v0, v1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 188
    :goto_b4
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/a/c/b/w;->b(Lcom/a/c/b/af;Z)V

    goto :goto_81

    .line 186
    :cond_b9
    iput-object v0, v1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    goto :goto_b4

    :cond_bc
    move v6, v0

    goto :goto_48
.end method

.method private a()V
    .registers 14

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 558
    iget-object v5, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    .line 3568
    array-length v6, v5

    .line 3570
    mul-int/lit8 v0, v6, 0x2

    new-array v7, v0, [Lcom/a/c/b/af;

    .line 3571
    new-instance v8, Lcom/a/c/b/z;

    invoke-direct {v8}, Lcom/a/c/b/z;-><init>()V

    .line 3572
    new-instance v9, Lcom/a/c/b/y;

    invoke-direct {v9}, Lcom/a/c/b/y;-><init>()V

    .line 3573
    new-instance v10, Lcom/a/c/b/y;

    invoke-direct {v10}, Lcom/a/c/b/y;-><init>()V

    move v4, v1

    .line 3576
    :goto_19
    if-ge v4, v6, :cond_6b

    .line 3577
    aget-object v11, v5, v4

    .line 3578
    if-eqz v11, :cond_63

    .line 3583
    invoke-virtual {v8, v11}, Lcom/a/c/b/z;->a(Lcom/a/c/b/af;)V

    move v0, v1

    move v2, v1

    .line 3586
    :goto_24
    invoke-virtual {v8}, Lcom/a/c/b/z;->a()Lcom/a/c/b/af;

    move-result-object v12

    if-eqz v12, :cond_35

    .line 3587
    iget v12, v12, Lcom/a/c/b/af;->g:I

    and-int/2addr v12, v6

    if-nez v12, :cond_32

    .line 3588
    add-int/lit8 v2, v2, 0x1

    goto :goto_24

    .line 3590
    :cond_32
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 3595
    :cond_35
    invoke-virtual {v9, v2}, Lcom/a/c/b/y;->a(I)V

    .line 3596
    invoke-virtual {v10, v0}, Lcom/a/c/b/y;->a(I)V

    .line 3597
    invoke-virtual {v8, v11}, Lcom/a/c/b/z;->a(Lcom/a/c/b/af;)V

    .line 3598
    :goto_3e
    invoke-virtual {v8}, Lcom/a/c/b/z;->a()Lcom/a/c/b/af;

    move-result-object v11

    if-eqz v11, :cond_51

    .line 3599
    iget v12, v11, Lcom/a/c/b/af;->g:I

    and-int/2addr v12, v6

    if-nez v12, :cond_4d

    .line 3600
    invoke-virtual {v9, v11}, Lcom/a/c/b/y;->a(Lcom/a/c/b/af;)V

    goto :goto_3e

    .line 3602
    :cond_4d
    invoke-virtual {v10, v11}, Lcom/a/c/b/y;->a(Lcom/a/c/b/af;)V

    goto :goto_3e

    .line 3607
    :cond_51
    if-lez v2, :cond_67

    invoke-virtual {v9}, Lcom/a/c/b/y;->a()Lcom/a/c/b/af;

    move-result-object v2

    :goto_57
    aput-object v2, v7, v4

    .line 3608
    add-int v2, v4, v6

    if-lez v0, :cond_69

    invoke-virtual {v10}, Lcom/a/c/b/y;->a()Lcom/a/c/b/af;

    move-result-object v0

    :goto_61
    aput-object v0, v7, v2

    .line 3576
    :cond_63
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_19

    :cond_67
    move-object v2, v3

    .line 3607
    goto :goto_57

    :cond_69
    move-object v0, v3

    .line 3608
    goto :goto_61

    .line 558
    :cond_6b
    iput-object v7, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    .line 559
    iget-object v0, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/c/b/w;->f:I

    .line 560
    return-void
.end method

.method private a(Lcom/a/c/b/af;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 402
    iget-object v0, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 403
    iget-object v3, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 404
    iget-object v4, v3, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 405
    iget-object v5, v3, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 408
    iput-object v4, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 409
    if-eqz v4, :cond_f

    .line 410
    iput-object p1, v4, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 413
    :cond_f
    invoke-direct {p0, p1, v3}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Lcom/a/c/b/af;)V

    .line 416
    iput-object p1, v3, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 417
    iput-object v3, p1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 420
    if-eqz v0, :cond_36

    iget v0, v0, Lcom/a/c/b/af;->i:I

    move v2, v0

    :goto_1b
    if-eqz v4, :cond_38

    iget v0, v4, Lcom/a/c/b/af;->i:I

    :goto_1f
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/a/c/b/af;->i:I

    .line 422
    iget v0, p1, Lcom/a/c/b/af;->i:I

    if-eqz v5, :cond_2d

    iget v1, v5, Lcom/a/c/b/af;->i:I

    :cond_2d
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/a/c/b/af;->i:I

    .line 424
    return-void

    :cond_36
    move v2, v1

    .line 420
    goto :goto_1b

    :cond_38
    move v0, v1

    goto :goto_1f
.end method

.method private a(Lcom/a/c/b/af;Lcom/a/c/b/af;)V
    .registers 5

    .prologue
    .line 312
    iget-object v0, p1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 313
    const/4 v1, 0x0

    iput-object v1, p1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 314
    if-eqz p2, :cond_9

    .line 315
    iput-object v0, p2, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 318
    :cond_9
    if-eqz v0, :cond_23

    .line 319
    iget-object v1, v0, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    if-ne v1, p1, :cond_12

    .line 320
    iput-object p2, v0, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 329
    :goto_11
    return-void

    .line 322
    :cond_12
    sget-boolean v1, Lcom/a/c/b/w;->g:Z

    if-nez v1, :cond_20

    iget-object v1, v0, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    if-eq v1, p1, :cond_20

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 323
    :cond_20
    iput-object p2, v0, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    goto :goto_11

    .line 326
    :cond_23
    iget v0, p1, Lcom/a/c/b/af;->g:I

    iget-object v1, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    .line 327
    iget-object v1, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    aput-object p2, v1, v0

    goto :goto_11
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 224
    if-eq p0, p1, :cond_a

    if-eqz p0, :cond_c

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private static a([Lcom/a/c/b/af;)[Lcom/a/c/b/af;
    .registers 13

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 568
    array-length v5, p0

    .line 570
    mul-int/lit8 v0, v5, 0x2

    new-array v6, v0, [Lcom/a/c/b/af;

    .line 571
    new-instance v7, Lcom/a/c/b/z;

    invoke-direct {v7}, Lcom/a/c/b/z;-><init>()V

    .line 572
    new-instance v8, Lcom/a/c/b/y;

    invoke-direct {v8}, Lcom/a/c/b/y;-><init>()V

    .line 573
    new-instance v9, Lcom/a/c/b/y;

    invoke-direct {v9}, Lcom/a/c/b/y;-><init>()V

    move v4, v1

    .line 576
    :goto_17
    if-ge v4, v5, :cond_69

    .line 577
    aget-object v10, p0, v4

    .line 578
    if-eqz v10, :cond_61

    .line 583
    invoke-virtual {v7, v10}, Lcom/a/c/b/z;->a(Lcom/a/c/b/af;)V

    move v0, v1

    move v2, v1

    .line 586
    :goto_22
    invoke-virtual {v7}, Lcom/a/c/b/z;->a()Lcom/a/c/b/af;

    move-result-object v11

    if-eqz v11, :cond_33

    .line 587
    iget v11, v11, Lcom/a/c/b/af;->g:I

    and-int/2addr v11, v5

    if-nez v11, :cond_30

    .line 588
    add-int/lit8 v2, v2, 0x1

    goto :goto_22

    .line 590
    :cond_30
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 595
    :cond_33
    invoke-virtual {v8, v2}, Lcom/a/c/b/y;->a(I)V

    .line 596
    invoke-virtual {v9, v0}, Lcom/a/c/b/y;->a(I)V

    .line 597
    invoke-virtual {v7, v10}, Lcom/a/c/b/z;->a(Lcom/a/c/b/af;)V

    .line 598
    :goto_3c
    invoke-virtual {v7}, Lcom/a/c/b/z;->a()Lcom/a/c/b/af;

    move-result-object v10

    if-eqz v10, :cond_4f

    .line 599
    iget v11, v10, Lcom/a/c/b/af;->g:I

    and-int/2addr v11, v5

    if-nez v11, :cond_4b

    .line 600
    invoke-virtual {v8, v10}, Lcom/a/c/b/y;->a(Lcom/a/c/b/af;)V

    goto :goto_3c

    .line 602
    :cond_4b
    invoke-virtual {v9, v10}, Lcom/a/c/b/y;->a(Lcom/a/c/b/af;)V

    goto :goto_3c

    .line 607
    :cond_4f
    if-lez v2, :cond_65

    invoke-virtual {v8}, Lcom/a/c/b/y;->a()Lcom/a/c/b/af;

    move-result-object v2

    :goto_55
    aput-object v2, v6, v4

    .line 608
    add-int v2, v4, v5

    if-lez v0, :cond_67

    invoke-virtual {v9}, Lcom/a/c/b/y;->a()Lcom/a/c/b/af;

    move-result-object v0

    :goto_5f
    aput-object v0, v6, v2

    .line 576
    :cond_61
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_17

    :cond_65
    move-object v2, v3

    .line 607
    goto :goto_55

    :cond_67
    move-object v0, v3

    .line 608
    goto :goto_5f

    .line 610
    :cond_69
    return-object v6
.end method

.method private b(Ljava/lang/Object;)Lcom/a/c/b/af;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 202
    if-eqz p1, :cond_8

    const/4 v1, 0x0

    :try_start_4
    invoke-direct {p0, p1, v1}, Lcom/a/c/b/w;->a(Ljava/lang/Object;Z)Lcom/a/c/b/af;
    :try_end_7
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_7} :catch_9

    move-result-object v0

    .line 204
    :cond_8
    :goto_8
    return-object v0

    :catch_9
    move-exception v1

    goto :goto_8
.end method

.method private b()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 859
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private b(Lcom/a/c/b/af;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 430
    iget-object v3, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 431
    iget-object v0, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 432
    iget-object v4, v3, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 433
    iget-object v5, v3, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 436
    iput-object v5, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 437
    if-eqz v5, :cond_f

    .line 438
    iput-object p1, v5, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 441
    :cond_f
    invoke-direct {p0, p1, v3}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Lcom/a/c/b/af;)V

    .line 444
    iput-object p1, v3, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 445
    iput-object v3, p1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 448
    if-eqz v0, :cond_36

    iget v0, v0, Lcom/a/c/b/af;->i:I

    move v2, v0

    :goto_1b
    if-eqz v5, :cond_38

    iget v0, v5, Lcom/a/c/b/af;->i:I

    :goto_1f
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/a/c/b/af;->i:I

    .line 450
    iget v0, p1, Lcom/a/c/b/af;->i:I

    if-eqz v4, :cond_2d

    iget v1, v4, Lcom/a/c/b/af;->i:I

    :cond_2d
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/a/c/b/af;->i:I

    .line 452
    return-void

    :cond_36
    move v2, v1

    .line 448
    goto :goto_1b

    :cond_38
    move v0, v1

    goto :goto_1f
.end method

.method private b(Lcom/a/c/b/af;Z)V
    .registers 12

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v1, 0x0

    .line 339
    :goto_3
    if-eqz p1, :cond_6a

    .line 340
    iget-object v3, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 341
    iget-object v4, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 342
    if-eqz v3, :cond_33

    iget v0, v3, Lcom/a/c/b/af;->i:I

    move v2, v0

    .line 343
    :goto_e
    if-eqz v4, :cond_35

    iget v0, v4, Lcom/a/c/b/af;->i:I

    .line 345
    :goto_12
    sub-int v5, v2, v0

    .line 346
    const/4 v6, -0x2

    if-ne v5, v6, :cond_4e

    .line 347
    iget-object v3, v4, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 348
    iget-object v0, v4, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 349
    if-eqz v0, :cond_37

    iget v0, v0, Lcom/a/c/b/af;->i:I

    move v2, v0

    .line 350
    :goto_20
    if-eqz v3, :cond_39

    iget v0, v3, Lcom/a/c/b/af;->i:I

    .line 352
    :goto_24
    sub-int/2addr v0, v2

    .line 353
    if-eq v0, v7, :cond_2b

    if-nez v0, :cond_3b

    if-nez p2, :cond_3b

    .line 354
    :cond_2b
    invoke-direct {p0, p1}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;)V

    .line 360
    :goto_2e
    if-nez p2, :cond_6a

    .line 339
    :cond_30
    :goto_30
    iget-object p1, p1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    goto :goto_3

    :cond_33
    move v2, v1

    .line 342
    goto :goto_e

    :cond_35
    move v0, v1

    .line 343
    goto :goto_12

    :cond_37
    move v2, v1

    .line 349
    goto :goto_20

    :cond_39
    move v0, v1

    .line 350
    goto :goto_24

    .line 356
    :cond_3b
    sget-boolean v2, Lcom/a/c/b/w;->g:Z

    if-nez v2, :cond_47

    if-eq v0, v8, :cond_47

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 357
    :cond_47
    invoke-direct {p0, v4}, Lcom/a/c/b/w;->b(Lcom/a/c/b/af;)V

    .line 358
    invoke-direct {p0, p1}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;)V

    goto :goto_2e

    .line 364
    :cond_4e
    const/4 v4, 0x2

    if-ne v5, v4, :cond_82

    .line 365
    iget-object v4, v3, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 366
    iget-object v0, v3, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 367
    if-eqz v0, :cond_6b

    iget v0, v0, Lcom/a/c/b/af;->i:I

    move v2, v0

    .line 368
    :goto_5a
    if-eqz v4, :cond_6d

    iget v0, v4, Lcom/a/c/b/af;->i:I

    .line 370
    :goto_5e
    sub-int/2addr v0, v2

    .line 371
    if-eq v0, v8, :cond_65

    if-nez v0, :cond_6f

    if-nez p2, :cond_6f

    .line 372
    :cond_65
    invoke-direct {p0, p1}, Lcom/a/c/b/w;->b(Lcom/a/c/b/af;)V

    .line 378
    :goto_68
    if-eqz p2, :cond_30

    .line 396
    :cond_6a
    :goto_6a
    return-void

    :cond_6b
    move v2, v1

    .line 367
    goto :goto_5a

    :cond_6d
    move v0, v1

    .line 368
    goto :goto_5e

    .line 374
    :cond_6f
    sget-boolean v2, Lcom/a/c/b/w;->g:Z

    if-nez v2, :cond_7b

    if-eq v0, v7, :cond_7b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 375
    :cond_7b
    invoke-direct {p0, v3}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;)V

    .line 376
    invoke-direct {p0, p1}, Lcom/a/c/b/w;->b(Lcom/a/c/b/af;)V

    goto :goto_68

    .line 382
    :cond_82
    if-nez v5, :cond_8b

    .line 383
    add-int/lit8 v0, v2, 0x1

    iput v0, p1, Lcom/a/c/b/af;->i:I

    .line 384
    if-eqz p2, :cond_30

    goto :goto_6a

    .line 389
    :cond_8b
    sget-boolean v3, Lcom/a/c/b/w;->g:Z

    if-nez v3, :cond_99

    if-eq v5, v7, :cond_99

    if-eq v5, v8, :cond_99

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 390
    :cond_99
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/a/c/b/af;->i:I

    .line 391
    if-eqz p2, :cond_6a

    goto :goto_30
.end method


# virtual methods
.method final a(Ljava/lang/Object;)Lcom/a/c/b/af;
    .registers 4

    .prologue
    .line 304
    invoke-direct {p0, p1}, Lcom/a/c/b/w;->b(Ljava/lang/Object;)Lcom/a/c/b/af;

    move-result-object v0

    .line 305
    if-eqz v0, :cond_a

    .line 306
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Z)V

    .line 308
    :cond_a
    return-object v0
.end method

.method final a(Ljava/util/Map$Entry;)Lcom/a/c/b/af;
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 218
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/c/b/w;->b(Ljava/lang/Object;)Lcom/a/c/b/af;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_24

    iget-object v3, v0, Lcom/a/c/b/af;->h:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 2224
    if-eq v3, v4, :cond_1c

    if-eqz v3, :cond_22

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    :cond_1c
    move v3, v1

    .line 219
    :goto_1d
    if-eqz v3, :cond_24

    .line 220
    :goto_1f
    if-eqz v1, :cond_26

    :goto_21
    return-object v0

    :cond_22
    move v3, v2

    .line 2224
    goto :goto_1d

    :cond_24
    move v1, v2

    .line 219
    goto :goto_1f

    .line 220
    :cond_26
    const/4 v0, 0x0

    goto :goto_21
.end method

.method final a(Lcom/a/c/b/af;Z)V
    .registers 10

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 246
    if-eqz p2, :cond_14

    .line 247
    iget-object v0, p1, Lcom/a/c/b/af;->e:Lcom/a/c/b/af;

    iget-object v1, p1, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    iput-object v1, v0, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    .line 248
    iget-object v0, p1, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    iget-object v1, p1, Lcom/a/c/b/af;->e:Lcom/a/c/b/af;

    iput-object v1, v0, Lcom/a/c/b/af;->e:Lcom/a/c/b/af;

    .line 249
    iput-object v5, p1, Lcom/a/c/b/af;->e:Lcom/a/c/b/af;

    iput-object v5, p1, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    .line 252
    :cond_14
    iget-object v1, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 253
    iget-object v0, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 254
    iget-object v3, p1, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 255
    if-eqz v1, :cond_5d

    if-eqz v0, :cond_5d

    .line 266
    iget v3, v1, Lcom/a/c/b/af;->i:I

    iget v4, v0, Lcom/a/c/b/af;->i:I

    if-le v3, v4, :cond_32

    .line 2548
    iget-object v0, v1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 2549
    :goto_29
    if-eqz v1, :cond_36

    .line 2551
    iget-object v0, v1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_29

    :cond_31
    move-object v0, v1

    .line 3535
    :cond_32
    iget-object v1, v0, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 3536
    if-nez v1, :cond_31

    .line 267
    :cond_36
    invoke-virtual {p0, v0, v2}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Z)V

    .line 270
    iget-object v3, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 271
    if-eqz v3, :cond_80

    .line 272
    iget v1, v3, Lcom/a/c/b/af;->i:I

    .line 273
    iput-object v3, v0, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 274
    iput-object v0, v3, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 275
    iput-object v5, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 278
    :goto_45
    iget-object v3, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 279
    if-eqz v3, :cond_51

    .line 280
    iget v2, v3, Lcom/a/c/b/af;->i:I

    .line 281
    iput-object v3, v0, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 282
    iput-object v0, v3, Lcom/a/c/b/af;->a:Lcom/a/c/b/af;

    .line 283
    iput-object v5, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    .line 285
    :cond_51
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/a/c/b/af;->i:I

    .line 286
    invoke-direct {p0, p1, v0}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Lcom/a/c/b/af;)V

    .line 301
    :goto_5c
    return-void

    .line 288
    :cond_5d
    if-eqz v1, :cond_74

    .line 289
    invoke-direct {p0, p1, v1}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Lcom/a/c/b/af;)V

    .line 290
    iput-object v5, p1, Lcom/a/c/b/af;->b:Lcom/a/c/b/af;

    .line 298
    :goto_64
    invoke-direct {p0, v3, v2}, Lcom/a/c/b/w;->b(Lcom/a/c/b/af;Z)V

    .line 299
    iget v0, p0, Lcom/a/c/b/w;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/b/w;->d:I

    .line 300
    iget v0, p0, Lcom/a/c/b/w;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/b/w;->e:I

    goto :goto_5c

    .line 291
    :cond_74
    if-eqz v0, :cond_7c

    .line 292
    invoke-direct {p0, p1, v0}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Lcom/a/c/b/af;)V

    .line 293
    iput-object v5, p1, Lcom/a/c/b/af;->c:Lcom/a/c/b/af;

    goto :goto_64

    .line 295
    :cond_7c
    invoke-direct {p0, p1, v5}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Lcom/a/c/b/af;)V

    goto :goto_64

    :cond_80
    move v1, v2

    goto :goto_45
.end method

.method public final clear()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 105
    iget-object v0, p0, Lcom/a/c/b/w;->b:[Lcom/a/c/b/af;

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 106
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/c/b/w;->d:I

    .line 107
    iget v0, p0, Lcom/a/c/b/w;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/b/w;->e:I

    .line 110
    iget-object v2, p0, Lcom/a/c/b/w;->c:Lcom/a/c/b/af;

    .line 111
    iget-object v0, v2, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    :goto_13
    if-eq v0, v2, :cond_1d

    .line 112
    iget-object v1, v0, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    .line 113
    iput-object v3, v0, Lcom/a/c/b/af;->e:Lcom/a/c/b/af;

    iput-object v3, v0, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    move-object v0, v1

    .line 115
    goto :goto_13

    .line 117
    :cond_1d
    iput-object v2, v2, Lcom/a/c/b/af;->e:Lcom/a/c/b/af;

    iput-object v2, v2, Lcom/a/c/b/af;->d:Lcom/a/c/b/af;

    .line 118
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/a/c/b/w;->b(Ljava/lang/Object;)Lcom/a/c/b/af;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/a/c/b/w;->i:Lcom/a/c/b/aa;

    .line 459
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/c/b/aa;

    invoke-direct {v0, p0}, Lcom/a/c/b/aa;-><init>(Lcom/a/c/b/w;)V

    iput-object v0, p0, Lcom/a/c/b/w;->i:Lcom/a/c/b/aa;

    goto :goto_4
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/a/c/b/w;->b(Ljava/lang/Object;)Lcom/a/c/b/af;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_9

    iget-object v0, v0, Lcom/a/c/b/af;->h:Ljava/lang/Object;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/a/c/b/w;->j:Lcom/a/c/b/ac;

    .line 464
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/c/b/ac;

    invoke-direct {v0, p0}, Lcom/a/c/b/ac;-><init>(Lcom/a/c/b/w;)V

    iput-object v0, p0, Lcom/a/c/b/w;->j:Lcom/a/c/b/ac;

    goto :goto_4
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 95
    if-nez p1, :cond_a

    .line 96
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_a
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/a/c/b/w;->a(Ljava/lang/Object;Z)Lcom/a/c/b/af;

    move-result-object v0

    .line 99
    iget-object v1, v0, Lcom/a/c/b/af;->h:Ljava/lang/Object;

    .line 100
    iput-object p2, v0, Lcom/a/c/b/af;->h:Ljava/lang/Object;

    .line 101
    return-object v1
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lcom/a/c/b/w;->a(Ljava/lang/Object;)Lcom/a/c/b/af;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_9

    iget-object v0, v0, Lcom/a/c/b/af;->h:Ljava/lang/Object;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 82
    iget v0, p0, Lcom/a/c/b/w;->d:I

    return v0
.end method
