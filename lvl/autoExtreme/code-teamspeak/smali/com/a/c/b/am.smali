.class abstract Lcom/a/c/b/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field b:Lcom/a/c/b/an;

.field c:Lcom/a/c/b/an;

.field d:I

.field final synthetic e:Lcom/a/c/b/ag;


# direct methods
.method private constructor <init>(Lcom/a/c/b/ag;)V
    .registers 3

    .prologue
    .line 526
    iput-object p1, p0, Lcom/a/c/b/am;->e:Lcom/a/c/b/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527
    iget-object v0, p0, Lcom/a/c/b/am;->e:Lcom/a/c/b/ag;

    iget-object v0, v0, Lcom/a/c/b/ag;->e:Lcom/a/c/b/an;

    iget-object v0, v0, Lcom/a/c/b/an;->d:Lcom/a/c/b/an;

    iput-object v0, p0, Lcom/a/c/b/am;->b:Lcom/a/c/b/an;

    .line 528
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/b/am;->c:Lcom/a/c/b/an;

    .line 529
    iget-object v0, p0, Lcom/a/c/b/am;->e:Lcom/a/c/b/ag;

    iget v0, v0, Lcom/a/c/b/ag;->d:I

    iput v0, p0, Lcom/a/c/b/am;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/c/b/ag;B)V
    .registers 3

    .prologue
    .line 526
    invoke-direct {p0, p1}, Lcom/a/c/b/am;-><init>(Lcom/a/c/b/ag;)V

    return-void
.end method


# virtual methods
.method final a()Lcom/a/c/b/an;
    .registers 4

    .prologue
    .line 536
    iget-object v0, p0, Lcom/a/c/b/am;->b:Lcom/a/c/b/an;

    .line 537
    iget-object v1, p0, Lcom/a/c/b/am;->e:Lcom/a/c/b/ag;

    iget-object v1, v1, Lcom/a/c/b/ag;->e:Lcom/a/c/b/an;

    if-ne v0, v1, :cond_e

    .line 538
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 540
    :cond_e
    iget-object v1, p0, Lcom/a/c/b/am;->e:Lcom/a/c/b/ag;

    iget v1, v1, Lcom/a/c/b/ag;->d:I

    iget v2, p0, Lcom/a/c/b/am;->d:I

    if-eq v1, v2, :cond_1c

    .line 541
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 543
    :cond_1c
    iget-object v1, v0, Lcom/a/c/b/an;->d:Lcom/a/c/b/an;

    iput-object v1, p0, Lcom/a/c/b/am;->b:Lcom/a/c/b/an;

    .line 544
    iput-object v0, p0, Lcom/a/c/b/am;->c:Lcom/a/c/b/an;

    return-object v0
.end method

.method public final hasNext()Z
    .registers 3

    .prologue
    .line 532
    iget-object v0, p0, Lcom/a/c/b/am;->b:Lcom/a/c/b/an;

    iget-object v1, p0, Lcom/a/c/b/am;->e:Lcom/a/c/b/ag;

    iget-object v1, v1, Lcom/a/c/b/ag;->e:Lcom/a/c/b/an;

    if-eq v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final remove()V
    .registers 4

    .prologue
    .line 548
    iget-object v0, p0, Lcom/a/c/b/am;->c:Lcom/a/c/b/an;

    if-nez v0, :cond_a

    .line 549
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 551
    :cond_a
    iget-object v0, p0, Lcom/a/c/b/am;->e:Lcom/a/c/b/ag;

    iget-object v1, p0, Lcom/a/c/b/am;->c:Lcom/a/c/b/an;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/a/c/b/ag;->a(Lcom/a/c/b/an;Z)V

    .line 552
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/b/am;->c:Lcom/a/c/b/an;

    .line 553
    iget-object v0, p0, Lcom/a/c/b/am;->e:Lcom/a/c/b/ag;

    iget v0, v0, Lcom/a/c/b/ag;->d:I

    iput v0, p0, Lcom/a/c/b/am;->d:I

    .line 554
    return-void
.end method
