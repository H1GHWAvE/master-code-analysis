.class public final Lcom/a/c/b/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/c/ap;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final a:Lcom/a/c/b/s;

.field private static final h:D = -1.0


# instance fields
.field public b:D

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Ljava/util/List;

.field public g:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 52
    new-instance v0, Lcom/a/c/b/s;

    invoke-direct {v0}, Lcom/a/c/b/s;-><init>()V

    sput-object v0, Lcom/a/c/b/s;->a:Lcom/a/c/b/s;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/a/c/b/s;->b:D

    .line 55
    const/16 v0, 0x88

    iput v0, p0, Lcom/a/c/b/s;->c:I

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/c/b/s;->d:Z

    .line 58
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/b/s;->f:Ljava/util/List;

    .line 59
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/b/s;->g:Ljava/util/List;

    return-void
.end method

.method private a(D)Lcom/a/c/b/s;
    .registers 4

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v0

    .line 71
    iput-wide p1, v0, Lcom/a/c/b/s;->b:D

    .line 72
    return-object v0
.end method

.method private varargs a([I)Lcom/a/c/b/s;
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-virtual {p0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v1

    .line 77
    iput v0, v1, Lcom/a/c/b/s;->c:I

    .line 78
    array-length v2, p1

    :goto_8
    if-ge v0, v2, :cond_14

    aget v3, p1, v0

    .line 79
    iget v4, v1, Lcom/a/c/b/s;->c:I

    or-int/2addr v3, v4

    iput v3, v1, Lcom/a/c/b/s;->c:I

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 81
    :cond_14
    return-object v1
.end method

.method private a(Lcom/a/c/a/d;)Z
    .registers 6

    .prologue
    .line 233
    if-eqz p1, :cond_e

    .line 234
    invoke-interface {p1}, Lcom/a/c/a/d;->a()D

    move-result-wide v0

    .line 235
    iget-wide v2, p0, Lcom/a/c/b/s;->b:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_e

    .line 236
    const/4 v0, 0x0

    .line 239
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method private a(Lcom/a/c/a/e;)Z
    .registers 6

    .prologue
    .line 243
    if-eqz p1, :cond_e

    .line 244
    invoke-interface {p1}, Lcom/a/c/a/e;->a()D

    move-result-wide v0

    .line 245
    iget-wide v2, p0, Lcom/a/c/b/s;->b:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    .line 246
    const/4 v0, 0x0

    .line 249
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method public static a(Ljava/lang/Class;)Z
    .registers 2

    .prologue
    .line 216
    const-class v0, Ljava/lang/Enum;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 217
    invoke-virtual {p0}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v0

    if-nez v0, :cond_14

    invoke-virtual {p0}, Ljava/lang/Class;->isLocalClass()Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private a(Ljava/lang/reflect/Field;Z)Z
    .registers 9

    .prologue
    const/4 v2, 0x1

    .line 150
    iget v0, p0, Lcom/a/c/b/s;->c:I

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_c

    move v0, v2

    .line 188
    :goto_b
    return v0

    .line 154
    :cond_c
    iget-wide v0, p0, Lcom/a/c/b/s;->b:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_2c

    const-class v0, Lcom/a/c/a/d;

    .line 155
    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/d;

    const-class v1, Lcom/a/c/a/e;

    invoke-virtual {p1, v1}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/a/c/a/e;

    invoke-virtual {p0, v0, v1}, Lcom/a/c/b/s;->a(Lcom/a/c/a/d;Lcom/a/c/a/e;)Z

    move-result v0

    if-nez v0, :cond_2c

    move v0, v2

    .line 156
    goto :goto_b

    .line 159
    :cond_2c
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->isSynthetic()Z

    move-result v0

    if-eqz v0, :cond_34

    move v0, v2

    .line 160
    goto :goto_b

    .line 163
    :cond_34
    iget-boolean v0, p0, Lcom/a/c/b/s;->e:Z

    if-eqz v0, :cond_52

    .line 164
    const-class v0, Lcom/a/c/a/a;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/a;

    .line 165
    if-eqz v0, :cond_4a

    if-eqz p2, :cond_4c

    invoke-interface {v0}, Lcom/a/c/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_52

    :cond_4a
    move v0, v2

    .line 166
    goto :goto_b

    .line 165
    :cond_4c
    invoke-interface {v0}, Lcom/a/c/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 170
    :cond_52
    iget-boolean v0, p0, Lcom/a/c/b/s;->d:Z

    if-nez v0, :cond_62

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/c/b/s;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_62

    move v0, v2

    .line 171
    goto :goto_b

    .line 174
    :cond_62
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/c/b/s;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_6e

    move v0, v2

    .line 175
    goto :goto_b

    .line 178
    :cond_6e
    if-eqz p2, :cond_96

    iget-object v0, p0, Lcom/a/c/b/s;->f:Ljava/util/List;

    .line 179
    :goto_72
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_99

    .line 180
    new-instance v1, Lcom/a/c/c;

    invoke-direct {v1, p1}, Lcom/a/c/c;-><init>(Ljava/lang/reflect/Field;)V

    .line 181
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_81
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_99

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/b;

    .line 182
    invoke-interface {v0}, Lcom/a/c/b;->a()Z

    move-result v0

    if-eqz v0, :cond_81

    move v0, v2

    .line 183
    goto/16 :goto_b

    .line 178
    :cond_96
    iget-object v0, p0, Lcom/a/c/b/s;->g:Ljava/util/List;

    goto :goto_72

    .line 188
    :cond_99
    const/4 v0, 0x0

    goto/16 :goto_b
.end method

.method private b()Lcom/a/c/b/s;
    .registers 3

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v0

    .line 86
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/a/c/b/s;->d:Z

    .line 87
    return-object v0
.end method

.method public static b(Ljava/lang/Class;)Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 221
    invoke-virtual {p0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1225
    invoke-virtual {p0}, Ljava/lang/Class;->getModifiers()I

    move-result v2

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_14

    move v2, v0

    .line 221
    :goto_11
    if-nez v2, :cond_16

    :goto_13
    return v0

    :cond_14
    move v2, v1

    .line 1225
    goto :goto_11

    :cond_16
    move v0, v1

    .line 221
    goto :goto_13
.end method

.method private c()Lcom/a/c/b/s;
    .registers 3

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v0

    .line 92
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/a/c/b/s;->e:Z

    .line 93
    return-object v0
.end method

.method private static c(Ljava/lang/Class;)Z
    .registers 2

    .prologue
    .line 225
    invoke-virtual {p0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 9

    .prologue
    .line 112
    .line 1094
    iget-object v0, p2, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 113
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/a/c/b/s;->a(Ljava/lang/Class;Z)Z

    move-result v3

    .line 114
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/a/c/b/s;->a(Ljava/lang/Class;Z)Z

    move-result v2

    .line 116
    if-nez v3, :cond_12

    if-nez v2, :cond_12

    .line 117
    const/4 v0, 0x0

    .line 120
    :goto_11
    return-object v0

    :cond_12
    new-instance v0, Lcom/a/c/b/t;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/a/c/b/t;-><init>(Lcom/a/c/b/s;ZZLcom/a/c/k;Lcom/a/c/c/a;)V

    goto :goto_11
.end method

.method public final a()Lcom/a/c/b/s;
    .registers 2

    .prologue
    .line 63
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/b/s;
    :try_end_6
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_6} :catch_7

    return-object v0

    .line 65
    :catch_7
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final a(Lcom/a/c/b;ZZ)Lcom/a/c/b/s;
    .registers 7

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v0

    .line 99
    if-eqz p2, :cond_14

    .line 100
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/a/c/b/s;->f:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/a/c/b/s;->f:Ljava/util/List;

    .line 101
    iget-object v1, v0, Lcom/a/c/b/s;->f:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_14
    if-eqz p3, :cond_24

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/a/c/b/s;->g:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/a/c/b/s;->g:Ljava/util/List;

    .line 106
    iget-object v1, v0, Lcom/a/c/b/s;->g:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_24
    return-object v0
.end method

.method public final a(Lcom/a/c/a/d;Lcom/a/c/a/e;)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 229
    .line 1233
    if-eqz p1, :cond_21

    .line 1234
    invoke-interface {p1}, Lcom/a/c/a/d;->a()D

    move-result-wide v2

    .line 1235
    iget-wide v4, p0, Lcom/a/c/b/s;->b:D

    cmpl-double v2, v2, v4

    if-lez v2, :cond_21

    move v2, v1

    .line 229
    :goto_f
    if-eqz v2, :cond_25

    .line 1243
    if-eqz p2, :cond_23

    .line 1244
    invoke-interface {p2}, Lcom/a/c/a/e;->a()D

    move-result-wide v2

    .line 1245
    iget-wide v4, p0, Lcom/a/c/b/s;->b:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_23

    move v2, v1

    .line 229
    :goto_1e
    if-eqz v2, :cond_25

    :goto_20
    return v0

    :cond_21
    move v2, v0

    .line 1239
    goto :goto_f

    :cond_23
    move v2, v0

    .line 1249
    goto :goto_1e

    :cond_25
    move v0, v1

    .line 229
    goto :goto_20
.end method

.method public final a(Ljava/lang/Class;Z)Z
    .registers 9

    .prologue
    const/4 v2, 0x1

    .line 192
    iget-wide v0, p0, Lcom/a/c/b/s;->b:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_21

    const-class v0, Lcom/a/c/a/d;

    .line 193
    invoke-virtual {p1, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/a/c/a/d;

    const-class v1, Lcom/a/c/a/e;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/a/c/a/e;

    invoke-virtual {p0, v0, v1}, Lcom/a/c/b/s;->a(Lcom/a/c/a/d;Lcom/a/c/a/e;)Z

    move-result v0

    if-nez v0, :cond_21

    move v0, v2

    .line 212
    :goto_20
    return v0

    .line 197
    :cond_21
    iget-boolean v0, p0, Lcom/a/c/b/s;->d:Z

    if-nez v0, :cond_2d

    invoke-static {p1}, Lcom/a/c/b/s;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2d

    move v0, v2

    .line 198
    goto :goto_20

    .line 201
    :cond_2d
    invoke-static {p1}, Lcom/a/c/b/s;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_35

    move v0, v2

    .line 202
    goto :goto_20

    .line 205
    :cond_35
    if-eqz p2, :cond_51

    iget-object v0, p0, Lcom/a/c/b/s;->f:Ljava/util/List;

    .line 206
    :goto_39
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_54

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/b;

    .line 207
    invoke-interface {v0}, Lcom/a/c/b;->b()Z

    move-result v0

    if-eqz v0, :cond_3d

    move v0, v2

    .line 208
    goto :goto_20

    .line 205
    :cond_51
    iget-object v0, p0, Lcom/a/c/b/s;->g:Ljava/util/List;

    goto :goto_39

    .line 212
    :cond_54
    const/4 v0, 0x0

    goto :goto_20
.end method

.method protected final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/a/c/b/s;->a()Lcom/a/c/b/s;

    move-result-object v0

    return-object v0
.end method
