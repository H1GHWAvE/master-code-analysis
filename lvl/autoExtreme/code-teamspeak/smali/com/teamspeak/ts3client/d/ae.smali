.class public final Lcom/teamspeak/ts3client/d/ae;
.super Landroid/support/v4/app/ax;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# instance fields
.field private aA:Ljava/lang/String;

.field private aB:Ljava/lang/String;

.field private aC:I

.field private aD:I

.field private aE:J

.field private aF:Landroid/widget/Spinner;

.field private aG:Z

.field private at:Lcom/teamspeak/ts3client/Ts3Application;

.field private au:Landroid/widget/EditText;

.field private av:Landroid/widget/EditText;

.field private aw:Landroid/widget/EditText;

.field private ax:Landroid/widget/EditText;

.field private ay:Landroid/widget/Button;

.field private az:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/ae;->aG:Z

    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/ae;I)I
    .registers 2

    .prologue
    .line 28
    iput p1, p0, Lcom/teamspeak/ts3client/d/ae;->aC:I

    return p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/ae;J)J
    .registers 4

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/teamspeak/ts3client/d/ae;->aE:J

    return-wide p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/ae;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->ax:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/ae;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 28
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/ae;->az:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/ae;I)I
    .registers 2

    .prologue
    .line 28
    iput p1, p0, Lcom/teamspeak/ts3client/d/ae;->aD:I

    return p1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/ae;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->aB:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/ae;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 28
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/ae;->aA:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/ae;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->au:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/ae;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 28
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/ae;->aB:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/ae;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->at:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/ae;)J
    .registers 3

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/teamspeak/ts3client/d/ae;->aE:J

    return-wide v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/ae;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->aA:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/ae;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->av:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/ae;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->az:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/d/ae;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->aw:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/d/ae;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->aF:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/d/ae;)I
    .registers 2

    .prologue
    .line 28
    iget v0, p0, Lcom/teamspeak/ts3client/d/ae;->aD:I

    return v0
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/d/ae;)Z
    .registers 2

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/ae;->aG:Z

    return v0
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/d/ae;)Z
    .registers 2

    .prologue
    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/ae;->aG:Z

    return v0
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/d/ae;)I
    .registers 2

    .prologue
    .line 28
    iget v0, p0, Lcom/teamspeak/ts3client/d/ae;->aC:I

    return v0
.end method

.method private y()V
    .registers 3

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/ae;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/d/aj;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/aj;-><init>(Lcom/teamspeak/ts3client/d/ae;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 178
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8

    .prologue
    .line 55
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 56
    const v0, 0x7f03005a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 2207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 57
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ae;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 57
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 58
    const v1, 0x7f0c01ea

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->au:Landroid/widget/EditText;

    .line 59
    const v1, 0x7f0c01ee

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->av:Landroid/widget/EditText;

    .line 60
    const v1, 0x7f0c01f1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->aw:Landroid/widget/EditText;

    .line 61
    const v1, 0x7f0c01ec

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->ax:Landroid/widget/EditText;

    .line 62
    const v1, 0x7f0c01f2

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->ay:Landroid/widget/Button;

    .line 64
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->ay:Landroid/widget/Button;

    const-string v2, "button.save"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 65
    const-string v1, "virtualservereditdialog.name"

    const v2, 0x7f0c01e9

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 66
    const-string v1, "virtualservereditdialog.password"

    const v2, 0x7f0c01eb

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 67
    const-string v1, "virtualservereditdialog.wmessage"

    const v2, 0x7f0c01f0

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 68
    const-string v1, "virtualservereditdialog.hmessage"

    const v2, 0x7f0c01ed

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 70
    const v1, 0x7f0c01ef

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->aF:Landroid/widget/Spinner;

    .line 71
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->aF:Landroid/widget/Spinner;

    const-string v2, "virtualservereditdialog.hmessage.array"

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 72
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->ax:Landroid/widget/EditText;

    new-instance v2, Lcom/teamspeak/ts3client/d/af;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/af;-><init>(Lcom/teamspeak/ts3client/d/ae;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 82
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->ay:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/ag;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/ag;-><init>(Lcom/teamspeak/ts3client/d/ae;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4238
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 106
    invoke-virtual {v1, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 107
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ae;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 107
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ae;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 107
    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestServerVariables(J)I

    .line 108
    return-object v0
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 4

    .prologue
    .line 47
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerUpdated;

    if-eqz v0, :cond_19

    .line 48
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->at:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 48
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 2152
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/ae;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/d/aj;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/aj;-><init>(Lcom/teamspeak/ts3client/d/ae;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 51
    :cond_19
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 113
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 114
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 115
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ae;->aF:Landroid/widget/Spinner;

    new-instance v1, Lcom/teamspeak/ts3client/d/ah;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/ah;-><init>(Lcom/teamspeak/ts3client/d/ae;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 142
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 146
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 147
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 149
    return-void
.end method
