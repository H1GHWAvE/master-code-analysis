.class public final Lcom/teamspeak/ts3client/d/b/a;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private aA:Landroid/widget/Button;

.field private aB:Landroid/widget/Button;

.field private aC:Landroid/widget/Button;

.field private aD:Landroid/widget/LinearLayout;

.field private aE:Z

.field private aF:Z

.field private at:Lcom/teamspeak/ts3client/data/c;

.field private au:Landroid/widget/Button;

.field private av:Landroid/widget/Button;

.field private aw:Landroid/widget/Button;

.field private ax:Landroid/widget/Button;

.field private ay:Landroid/widget/Button;

.field private az:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/c;Z)V
    .registers 5

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aF:Z

    .line 58
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 1235
    iget v0, p1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 59
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2242
    iget v1, v1, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 59
    if-ne v0, v1, :cond_17

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aF:Z

    .line 61
    :cond_17
    iput-boolean p2, p0, Lcom/teamspeak/ts3client/d/b/a;->aE:Z

    .line 62
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/teamspeak/ts3client/Ts3Application;)Landroid/view/View;
    .registers 13

    .prologue
    const v4, 0x7f0c0138

    const v3, 0x7f0c0137

    const v8, 0x7f0c013b

    const v7, 0x7f0c013e

    const/4 v6, 0x0

    .line 169
    const v0, 0x7f03002c

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 38207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 170
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 39203
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 170
    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aA:Landroid/widget/Button;

    .line 173
    const-string v1, "dialog.client.contact.text"

    invoke-static {v1, v0, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 174
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aA:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/y;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/y;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    const v1, 0x7f0c0141

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->au:Landroid/widget/Button;

    .line 185
    const-string v1, "dialog.client.kickserver.text"

    const v2, 0x7f0c0141

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 186
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->au:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/z;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/z;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    const v1, 0x7f0c0140

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->av:Landroid/widget/Button;

    .line 197
    const-string v1, "dialog.client.kickchannel.text"

    const v2, 0x7f0c0140

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 198
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->av:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/aa;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/aa;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aw:Landroid/widget/Button;

    .line 209
    const-string v1, "dialog.client.poke.text"

    invoke-static {v1, v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 210
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aw:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/c;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/c;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    .line 220
    const-string v1, "dialog.client.priorityspeaker.text"

    invoke-static {v1, v0, v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 221
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/d;

    invoke-direct {v2, p0, p3}, Lcom/teamspeak/ts3client/d/b/d;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    const v1, 0x7f0c013d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aC:Landroid/widget/Button;

    .line 236
    const-string v1, "dialog.client.volumemodifier.text"

    const v2, 0x7f0c013d

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 237
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aC:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/e;

    invoke-direct {v2, p0, p3}, Lcom/teamspeak/ts3client/d/b/e;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    const v1, 0x7f0c013a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->ax:Landroid/widget/Button;

    .line 339
    const-string v1, "dialog.client.complain.text"

    const v2, 0x7f0c013a

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 340
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->ax:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/h;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/h;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    const v1, 0x7f0c013f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->ay:Landroid/widget/Button;

    .line 351
    const-string v1, "dialog.client.ban.text"

    const v2, 0x7f0c013f

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 352
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->ay:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/i;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/i;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->az:Landroid/widget/Button;

    .line 363
    const-string v1, "dialog.client.mute.text1"

    invoke-static {v1, v0, v8}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 40061
    iget-object v1, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 40234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 41061
    iget-object v2, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 41267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 364
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 42235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 364
    sget-object v5, Lcom/teamspeak/ts3client/jni/d;->q:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_11c

    .line 365
    const-string v1, "dialog.client.mute.text2"

    invoke-static {v1, v0, v8}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 366
    :cond_11c
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->az:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/j;

    invoke-direct {v2, p0, p3}, Lcom/teamspeak/ts3client/d/b/j;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    .line 394
    const-string v1, "dialog.client.priorityspeaker.text"

    invoke-static {v1, v0, v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 395
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/b/k;

    invoke-direct {v2, p0, p3}, Lcom/teamspeak/ts3client/d/b/k;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    const v1, 0x7f0c013c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    .line 409
    iget-boolean v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aE:Z

    if-eqz v1, :cond_16b

    .line 410
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/a;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 411
    const-string v2, "dialog.client.info.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 412
    new-instance v2, Lcom/teamspeak/ts3client/d/b/l;

    invoke-direct {v2, p0, p3}, Lcom/teamspeak/ts3client/d/b/l;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 422
    :cond_16b
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 423
    const-string v2, "dialog.client.move.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 424
    new-instance v2, Lcom/teamspeak/ts3client/d/b/m;

    invoke-direct {v2, p0, p3}, Lcom/teamspeak/ts3client/d/b/m;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 470
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 472
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 473
    const-string v2, "dialog.client.chat.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 474
    new-instance v2, Lcom/teamspeak/ts3client/d/b/o;

    invoke-direct {v2, p0, p3}, Lcom/teamspeak/ts3client/d/b/o;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 499
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 43061
    iget-object v1, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 43177
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 501
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 43235
    iget v2, v2, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 501
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_292

    .line 44061
    iget-object v1, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 502
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->do:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v2

    .line 45061
    iget-object v1, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 503
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    .line 46061
    iget-object v1, p3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 46177
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 503
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 46235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 503
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v3, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    .line 504
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 47164
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 504
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1ff

    .line 48079
    iget v3, v1, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 504
    if-lez v3, :cond_293

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 48156
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 49079
    iget v1, v1, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 504
    if-ge v3, v1, :cond_293

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 49283
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 504
    if-nez v1, :cond_293

    .line 505
    :cond_1ff
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 506
    const-string v3, "dialog.client.talkpower.text1"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 507
    new-instance v3, Lcom/teamspeak/ts3client/d/b/q;

    invoke-direct {v3, p0, p3}, Lcom/teamspeak/ts3client/d/b/q;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 515
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 516
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 532
    :cond_221
    :goto_221
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 533
    const-string v2, "dialog.client.group.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 534
    new-instance v2, Lcom/teamspeak/ts3client/d/b/s;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/s;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 543
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 544
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 545
    const-string v2, "dialog.client.cinfo.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 546
    new-instance v2, Lcom/teamspeak/ts3client/d/b/t;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/t;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 555
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 556
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 50284
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 556
    if-eqz v1, :cond_292

    .line 557
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->ax:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 558
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->ay:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 559
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aA:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 560
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->av:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 561
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 562
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->az:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 563
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aw:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 564
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 565
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->au:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 570
    :cond_292
    return-object v0

    .line 517
    :cond_293
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 50283
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 517
    if-eqz v1, :cond_221

    .line 518
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 519
    const-string v3, "dialog.client.talkpower.text2"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 520
    new-instance v3, Lcom/teamspeak/ts3client/d/b/r;

    invoke-direct {v3, p0, p3}, Lcom/teamspeak/ts3client/d/b/r;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 529
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_221
.end method

.method private a(Lcom/teamspeak/ts3client/Ts3Application;)Landroid/view/View;
    .registers 7

    .prologue
    .line 75
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/a;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 76
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 29203
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 76
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 78
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 80
    const-string v2, "dialog.client.priorityspeaker.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 81
    new-instance v2, Lcom/teamspeak/ts3client/d/b/b;

    invoke-direct {v2, p0, p1}, Lcom/teamspeak/ts3client/d/b/b;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 96
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aE:Z

    if-eqz v0, :cond_5e

    .line 97
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/a;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 98
    const-string v2, "dialog.client.info.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 99
    new-instance v2, Lcom/teamspeak/ts3client/d/b/p;

    invoke-direct {v2, p0, p1}, Lcom/teamspeak/ts3client/d/b/p;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 30061
    :cond_5e
    iget-object v0, p1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 109
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->do:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v2

    .line 31061
    iget-object v0, p1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31177
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 110
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 31235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 110
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d4

    .line 32061
    iget-object v0, p1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 111
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    .line 33061
    iget-object v0, p1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 33177
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 111
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 33235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 111
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 113
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 34164
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 113
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b4

    .line 35079
    iget v3, v0, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 113
    if-lez v3, :cond_10f

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 35156
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 36079
    iget v0, v0, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 113
    if-ge v3, v0, :cond_10f

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 36283
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 113
    if-nez v0, :cond_10f

    .line 114
    :cond_b4
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 115
    const-string v3, "dialog.client.talkpower.text1"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 116
    new-instance v3, Lcom/teamspeak/ts3client/d/b/u;

    invoke-direct {v3, p0, p1}, Lcom/teamspeak/ts3client/d/b/u;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 125
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 141
    :cond_d4
    :goto_d4
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 142
    const-string v2, "dialog.client.group.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 143
    new-instance v2, Lcom/teamspeak/ts3client/d/b/w;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/w;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 153
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 154
    const-string v2, "dialog.client.cinfo.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 155
    new-instance v2, Lcom/teamspeak/ts3client/d/b/x;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/x;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 165
    return-object v1

    .line 126
    :cond_10f
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 37283
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 126
    if-eqz v0, :cond_d4

    .line 127
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 128
    const-string v3, "dialog.client.talkpower.text2"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 129
    new-instance v3, Lcom/teamspeak/ts3client/d/b/v;

    invoke-direct {v3, p0, p1}, Lcom/teamspeak/ts3client/d/b/v;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 138
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_d4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14

    .prologue
    const v4, 0x7f0c0137

    const/4 v10, 0x1

    const v9, 0x7f0c013b

    const v8, 0x7f0c013e

    const/4 v7, 0x0

    .line 66
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 67
    iget-boolean v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aF:Z

    if-nez v1, :cond_2ce

    .line 3169
    const v1, 0x7f03002c

    invoke-virtual {p1, v1, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 4207
    iget-object v2, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 3170
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 5203
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 3170
    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 3172
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aA:Landroid/widget/Button;

    .line 3173
    const-string v2, "dialog.client.contact.text"

    invoke-static {v2, v1, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3174
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aA:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/y;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/b/y;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3184
    const v2, 0x7f0c0141

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->au:Landroid/widget/Button;

    .line 3185
    const-string v2, "dialog.client.kickserver.text"

    const v3, 0x7f0c0141

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3186
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->au:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/z;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/b/z;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3196
    const v2, 0x7f0c0140

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->av:Landroid/widget/Button;

    .line 3197
    const-string v2, "dialog.client.kickchannel.text"

    const v3, 0x7f0c0140

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3198
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->av:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/aa;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/b/aa;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3208
    const v2, 0x7f0c0138

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aw:Landroid/widget/Button;

    .line 3209
    const-string v2, "dialog.client.poke.text"

    const v3, 0x7f0c0138

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3210
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aw:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/c;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/b/c;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3219
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    .line 3220
    const-string v2, "dialog.client.priorityspeaker.text"

    invoke-static {v2, v1, v8}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3221
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/d;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/d;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3235
    const v2, 0x7f0c013d

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aC:Landroid/widget/Button;

    .line 3236
    const-string v2, "dialog.client.volumemodifier.text"

    const v3, 0x7f0c013d

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3237
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aC:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/e;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/e;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3338
    const v2, 0x7f0c013a

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->ax:Landroid/widget/Button;

    .line 3339
    const-string v2, "dialog.client.complain.text"

    const v3, 0x7f0c013a

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3340
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->ax:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/h;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/b/h;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3350
    const v2, 0x7f0c013f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->ay:Landroid/widget/Button;

    .line 3351
    const-string v2, "dialog.client.ban.text"

    const v3, 0x7f0c013f

    invoke-static {v2, v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3352
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->ay:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/i;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/b/i;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3362
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->az:Landroid/widget/Button;

    .line 3363
    const-string v2, "dialog.client.mute.text1"

    invoke-static {v2, v1, v9}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 6061
    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 7061
    iget-object v3, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 3364
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 8235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 3364
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->q:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v2

    if-ne v2, v10, :cond_12d

    .line 3365
    const-string v2, "dialog.client.mute.text2"

    invoke-static {v2, v1, v9}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3366
    :cond_12d
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->az:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/j;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/j;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3393
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    .line 3394
    const-string v2, "dialog.client.priorityspeaker.text"

    invoke-static {v2, v1, v8}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 3395
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/b/k;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/k;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3408
    const v2, 0x7f0c013c

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    .line 3409
    iget-boolean v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aE:Z

    if-eqz v2, :cond_17c

    .line 3410
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3411
    const-string v3, "dialog.client.info.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3412
    new-instance v3, Lcom/teamspeak/ts3client/d/b/l;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/l;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3420
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3422
    :cond_17c
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3423
    const-string v3, "dialog.client.move.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3424
    new-instance v3, Lcom/teamspeak/ts3client/d/b/m;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/m;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3470
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3472
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3473
    const-string v3, "dialog.client.chat.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3474
    new-instance v3, Lcom/teamspeak/ts3client/d/b/o;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/o;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3499
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 9061
    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9177
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 3501
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 9235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 3501
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2a3

    .line 10061
    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10181
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 3502
    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->do:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    .line 11061
    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3503
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    .line 12061
    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12177
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 3503
    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 12235
    iget v5, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 3503
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v4, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v2

    .line 3504
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 13164
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 3504
    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_210

    .line 14079
    iget v4, v2, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 3504
    if-lez v4, :cond_2a4

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 14156
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 15079
    iget v2, v2, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 3504
    if-ge v4, v2, :cond_2a4

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 15283
    iget-boolean v2, v2, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 3504
    if-nez v2, :cond_2a4

    .line 3505
    :cond_210
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3506
    const-string v4, "dialog.client.talkpower.text1"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3507
    new-instance v4, Lcom/teamspeak/ts3client/d/b/q;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/d/b/q;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3515
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3516
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3532
    :cond_232
    :goto_232
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3533
    const-string v2, "dialog.client.group.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3534
    new-instance v2, Lcom/teamspeak/ts3client/d/b/s;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/s;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3543
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3544
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3545
    const-string v2, "dialog.client.cinfo.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3546
    new-instance v2, Lcom/teamspeak/ts3client/d/b/t;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/b/t;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3555
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3556
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 17180
    iget v0, v0, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 3556
    if-eqz v0, :cond_2a3

    .line 3557
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->ax:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3558
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->ay:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3559
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aA:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3560
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->av:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3561
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 3562
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->az:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3563
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aw:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3564
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aB:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3565
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->au:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 70
    :cond_2a3
    :goto_2a3
    return-object v1

    .line 3517
    :cond_2a4
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 16283
    iget-boolean v2, v2, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 3517
    if-eqz v2, :cond_232

    .line 3518
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 3519
    const-string v4, "dialog.client.talkpower.text2"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3520
    new-instance v4, Lcom/teamspeak/ts3client/d/b/r;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/d/b/r;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3528
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3529
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/a;->aD:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_232

    .line 18075
    :cond_2ce
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/a;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 18207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 18076
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 19203
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 18076
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 18077
    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 18078
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 18079
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 18080
    const-string v3, "dialog.client.priorityspeaker.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 18081
    new-instance v3, Lcom/teamspeak/ts3client/d/b/b;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/b;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18094
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 18096
    iget-boolean v1, p0, Lcom/teamspeak/ts3client/d/b/a;->aE:Z

    if-eqz v1, :cond_32b

    .line 18097
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/a;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 18098
    const-string v3, "dialog.client.info.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 18099
    new-instance v3, Lcom/teamspeak/ts3client/d/b/p;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/b/p;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18107
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 20061
    :cond_32b
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 18109
    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->do:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v3}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    .line 21061
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21177
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 18110
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 21235
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 18110
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3a1

    .line 22061
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18111
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    .line 23061
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23177
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 18111
    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 23235
    iget v5, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 18111
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v4, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    .line 18113
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 24164
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 18113
    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_381

    .line 25079
    iget v4, v1, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 18113
    if-lez v4, :cond_3de

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 25156
    iget v4, v4, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 26079
    iget v1, v1, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 18113
    if-ge v4, v1, :cond_3de

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 26283
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 18113
    if-nez v1, :cond_3de

    .line 18114
    :cond_381
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 18115
    const-string v4, "dialog.client.talkpower.text1"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 18116
    new-instance v4, Lcom/teamspeak/ts3client/d/b/u;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/d/b/u;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18124
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 18125
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 18141
    :cond_3a1
    :goto_3a1
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 18142
    const-string v1, "dialog.client.group.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 18143
    new-instance v1, Lcom/teamspeak/ts3client/d/b/w;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/b/w;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18152
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 18153
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 18154
    const-string v1, "dialog.client.cinfo.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 18155
    new-instance v1, Lcom/teamspeak/ts3client/d/b/x;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/b/x;-><init>(Lcom/teamspeak/ts3client/d/b/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18164
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object v1, v2

    .line 70
    goto/16 :goto_2a3

    .line 18126
    :cond_3de
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 27283
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 18126
    if-eqz v1, :cond_3a1

    .line 18127
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 18128
    const-string v4, "dialog.client.talkpower.text2"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 18129
    new-instance v4, Lcom/teamspeak/ts3client/d/b/v;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/d/b/v;-><init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18137
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 18138
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_3a1
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 575
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cid_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/a;->at:Lcom/teamspeak/ts3client/data/c;

    .line 50285
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 576
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;Ljava/lang/String;)V

    .line 577
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 581
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 582
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 584
    return-void
.end method
