.class final Lcom/teamspeak/ts3client/d/b/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ArrayAdapter;

.field final synthetic b:Landroid/widget/Spinner;

.field final synthetic c:Landroid/app/Dialog;

.field final synthetic d:Lcom/teamspeak/ts3client/d/b/m;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/b/m;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/app/Dialog;)V
    .registers 5

    .prologue
    .line 450
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/b/n;->a:Landroid/widget/ArrayAdapter;

    iput-object p3, p0, Lcom/teamspeak/ts3client/d/b/n;->b:Landroid/widget/Spinner;

    iput-object p4, p0, Lcom/teamspeak/ts3client/d/b/n;->c:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 10

    .prologue
    .line 454
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 454
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->a:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/n;->b:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 1087
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 454
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 1167
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/a;->m:Z

    .line 454
    if-eqz v0, :cond_75

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 454
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bD:Lcom/teamspeak/ts3client/jni/g;

    .line 2331
    iget-object v1, v1, Lcom/teamspeak/ts3client/jni/g;->iy:Ljava/lang/String;

    .line 454
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_75

    .line 455
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3206
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 455
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 455
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->a:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/n;->b:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 4087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 455
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iget-object v2, v2, Lcom/teamspeak/ts3client/d/b/m;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v2

    .line 4235
    iget v2, v2, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 455
    invoke-virtual {v1, v0, v2}, Lcom/teamspeak/ts3client/t;->a(Lcom/teamspeak/ts3client/data/a;I)V

    .line 456
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 461
    :goto_74
    return-void

    .line 458
    :cond_75
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 458
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iget-object v1, v1, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v1, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 458
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/n;->d:Lcom/teamspeak/ts3client/d/b/m;

    iget-object v3, v3, Lcom/teamspeak/ts3client/d/b/m;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 7235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 458
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/n;->a:Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b/n;->b:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/teamspeak/ts3client/data/a;

    .line 8087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 458
    const-string v6, ""

    const-string v7, "Move Client"

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestClientMove(JIJLjava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/n;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_74
.end method
