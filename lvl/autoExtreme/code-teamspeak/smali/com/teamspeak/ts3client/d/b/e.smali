.class final Lcom/teamspeak/ts3client/d/b/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Lcom/teamspeak/ts3client/d/b/a;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 3

    .prologue
    .line 237
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/b/e;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 15

    .prologue
    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v9, -0x1

    const/4 v8, -0x2

    .line 241
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 242
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 243
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 245
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 246
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v9, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 247
    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 248
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setId(I)V

    .line 249
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 1203
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 249
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 251
    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 252
    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    new-instance v4, Landroid/widget/SeekBar;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 255
    invoke-virtual {v4, v10}, Landroid/widget/SeekBar;->setId(I)V

    .line 256
    const/16 v5, 0x78

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 257
    const/16 v5, 0x3c

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 258
    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 1211
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 258
    if-eqz v5, :cond_a7

    .line 259
    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 2211
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 3118
    iget v5, v5, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 259
    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_a7

    .line 260
    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v5}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 3211
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 4118
    iget v5, v5, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 260
    div-float/2addr v5, v12

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    add-int/lit8 v5, v5, 0x3c

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 264
    :cond_a7
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 265
    const/high16 v6, 0x41000000    # 8.0f

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    .line 266
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 267
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v7

    add-int/lit8 v7, v7, -0x3c

    int-to-float v7, v7

    mul-float/2addr v7, v12

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / 30"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    const/16 v6, 0xa

    invoke-virtual {v5, v11, v11, v6, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 269
    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setId(I)V

    .line 271
    new-instance v6, Lcom/teamspeak/ts3client/d/b/f;

    invoke-direct {v6, p0, v5}, Lcom/teamspeak/ts3client/d/b/f;-><init>(Lcom/teamspeak/ts3client/d/b/e;Landroid/widget/TextView;)V

    invoke-virtual {v4, v6}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 294
    iget-object v6, p0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/d/b/a;->j()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0200ac

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 296
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 297
    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-virtual {v6, v10, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 298
    invoke-virtual {v2, v4, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 300
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 301
    invoke-virtual {v4}, Landroid/widget/SeekBar;->getId()I

    move-result v6

    invoke-virtual {v3, v10, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 302
    const/16 v6, 0xb

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 303
    invoke-virtual {v2, v5, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 305
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 306
    invoke-virtual {v5}, Landroid/widget/TextView;->getId()I

    move-result v5

    invoke-virtual {v3, v10, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 307
    new-instance v5, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 308
    const-string v6, "button.save"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 309
    new-instance v6, Lcom/teamspeak/ts3client/d/b/g;

    invoke-direct {v6, p0, v4, v2, v0}, Lcom/teamspeak/ts3client/d/b/g;-><init>(Lcom/teamspeak/ts3client/d/b/e;Landroid/widget/SeekBar;Landroid/widget/RelativeLayout;Landroid/app/Dialog;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    invoke-virtual {v2, v5, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 329
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 330
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 331
    const-string v1, "dialog.client.volumemodifier.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 332
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 334
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/b/a;->b()V

    .line 335
    return-void
.end method
