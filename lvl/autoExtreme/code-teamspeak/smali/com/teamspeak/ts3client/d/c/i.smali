.class public abstract Lcom/teamspeak/ts3client/d/c/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static a(Landroid/view/Menu;ILjava/lang/String;II)V
    .registers 7

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-interface {p0, v0, p3, p4, p2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 26
    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/support/v4/view/az;->a(Landroid/view/MenuItem;I)V

    .line 27
    invoke-static {p1}, Lcom/teamspeak/ts3client/data/d/t;->a(I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 28
    return-void
.end method

.method public static a(Landroid/view/Menu;IZ)V
    .registers 6

    .prologue
    .line 31
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 32
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 33
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 34
    if-nez p2, :cond_16

    .line 35
    const v1, -0x777778

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 39
    :goto_15
    return-void

    .line 37
    :cond_16
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_15
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Landroid/view/Menu;Lcom/teamspeak/ts3client/Ts3Application;)V
.end method

.method public abstract a(Landroid/view/MenuItem;Lcom/teamspeak/ts3client/Ts3Application;)Z
.end method
