.class final Lcom/teamspeak/ts3client/d/a/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/a/m;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a/m;)V
    .registers 2

    .prologue
    .line 290
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 5

    .prologue
    .line 294
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 311
    :cond_a
    :goto_a
    return-void

    .line 296
    :cond_b
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 297
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->j(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v1

    if-eq v0, v1, :cond_a

    .line 299
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->k(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v1

    if-le v0, v1, :cond_a

    .line 300
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->k(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v0

    if-eqz v0, :cond_6e

    .line 301
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->d(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a/m;->k(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_47
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_47} :catch_48

    goto :goto_a

    .line 305
    :catch_48
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->k(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v0

    if-eqz v0, :cond_8b

    .line 306
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->d(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a/m;->k(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 303
    :cond_6e
    :try_start_6e
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->d(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a/m;->z(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_8a
    .catch Ljava/lang/NumberFormatException; {:try_start_6e .. :try_end_8a} :catch_48

    goto :goto_a

    .line 308
    :cond_8b
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->d(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/r;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a/m;->z(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .prologue
    .line 317
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .prologue
    .line 323
    return-void
.end method
