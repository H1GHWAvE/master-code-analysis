.class final Lcom/teamspeak/ts3client/d/a/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/a/m;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a/m;)V
    .registers 2

    .prologue
    .line 194
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 13

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 198
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->e(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 199
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->e(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->requestFocus()Z

    .line 200
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->e(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    .line 279
    :goto_2b
    return-void

    .line 203
    :cond_2c
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->f(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_7b

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->f(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_382

    move v0, v8

    :goto_45
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->g(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v1

    if-eq v0, v1, :cond_7b

    .line 204
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 204
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 204
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 3087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 204
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->l:Lcom/teamspeak/ts3client/jni/c;

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->f(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_385

    move v7, v8

    :goto_78
    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 208
    :cond_7b
    :try_start_7b
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->d(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 209
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->j(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v0

    if-ne v7, v0, :cond_9d

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->j(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v0

    if-nez v0, :cond_d3

    :cond_9d
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-nez v0, :cond_d3

    .line 210
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->k(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v0

    if-nez v0, :cond_388

    .line 211
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 211
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 211
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 6087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 211
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->q:Lcom/teamspeak/ts3client/jni/c;

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I
    :try_end_d3
    .catch Ljava/lang/NumberFormatException; {:try_start_7b .. :try_end_d3} :catch_3b4

    .line 218
    :cond_d3
    :goto_d3
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->l(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_128

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->l(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->m(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_128

    .line 219
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 10061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 219
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 219
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 12087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 219
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->d:Lcom/teamspeak/ts3client/jni/c;

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->l(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;Ljava/lang/String;)I

    .line 221
    :cond_128
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->e(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_17d

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->e(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->n(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17d

    .line 222
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 13061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 222
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 14061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 222
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 15087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 222
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->a:Lcom/teamspeak/ts3client/jni/c;

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->e(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;Ljava/lang/String;)I

    .line 224
    :cond_17d
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->o(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1d2

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->o(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->p(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d2

    .line 225
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 16061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 225
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 17061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 225
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 18087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 225
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->b:Lcom/teamspeak/ts3client/jni/c;

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->o(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;Ljava/lang/String;)I

    .line 227
    :cond_1d2
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->q(Lcom/teamspeak/ts3client/d/a/m;)Z

    move-result v0

    if-eqz v0, :cond_201

    .line 228
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 228
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 20061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 228
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 21087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 228
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->c:Lcom/teamspeak/ts3client/jni/c;

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->r(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;Ljava/lang/String;)I

    .line 230
    :cond_201
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->s(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v0

    if-ne v0, v8, :cond_3b7

    .line 231
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-eq v0, v10, :cond_2b5

    .line 232
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-nez v0, :cond_265

    .line 233
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 22061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 233
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 23061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 233
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 24087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 233
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->j:Lcom/teamspeak/ts3client/jni/c;

    move v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 234
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 25061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 234
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 26061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 234
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 27087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 234
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->k:Lcom/teamspeak/ts3client/jni/c;

    move v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 236
    :cond_265
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v8, :cond_2b5

    .line 237
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 28061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 237
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 29061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 29267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 237
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 30087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 237
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->j:Lcom/teamspeak/ts3client/jni/c;

    move v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 238
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 31061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 238
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 32061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 32267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 238
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 33087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 238
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->k:Lcom/teamspeak/ts3client/jni/c;

    move v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 262
    :cond_2b5
    :goto_2b5
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->u(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->getSettings()Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v0

    .line 263
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->v(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v1

    .line 50074
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 263
    if-ne v1, v2, :cond_2d3

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->w(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v1

    .line 50075
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 263
    if-eq v1, v2, :cond_319

    .line 264
    :cond_2d3
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 50076
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50077
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 264
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 50078
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50079
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 264
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    .line 50080
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 264
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->e:Lcom/teamspeak/ts3client/jni/c;

    .line 50081
    iget v7, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 264
    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 265
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 50082
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50083
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 265
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 50084
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50085
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 265
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    .line 50086
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 265
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->f:Lcom/teamspeak/ts3client/jni/c;

    .line 50087
    iget v7, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 265
    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 267
    :cond_319
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->x(Lcom/teamspeak/ts3client/d/a/m;)Z

    move-result v0

    if-eqz v0, :cond_532

    .line 268
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->y(Lcom/teamspeak/ts3client/d/a/m;)Z

    move-result v0

    if-eqz v0, :cond_50f

    .line 269
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50088
    iget-wide v0, v0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 269
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_360

    .line 270
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50089
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50090
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 270
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50091
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50092
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 270
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50093
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 270
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50094
    iget-wide v7, v0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 270
    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;J)I

    .line 271
    :cond_360
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50095
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50096
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 271
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50097
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50098
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 271
    const-wide/16 v4, 0x0

    const-string v6, "Create Channel"

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushChannelCreation(JJLjava/lang/String;)I

    .line 278
    :goto_37b
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/a/m;->b()V

    goto/16 :goto_2b

    :cond_382
    move v0, v9

    .line 203
    goto/16 :goto_45

    :cond_385
    move v7, v9

    .line 204
    goto/16 :goto_78

    .line 212
    :cond_388
    :try_start_388
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->k(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_d3

    .line 213
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 213
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 213
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 9087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 213
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->q:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I
    :try_end_3b2
    .catch Ljava/lang/NumberFormatException; {:try_start_388 .. :try_end_3b2} :catch_3b4

    goto/16 :goto_d3

    :catch_3b4
    move-exception v0

    goto/16 :goto_d3

    .line 241
    :cond_3b7
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->t(Lcom/teamspeak/ts3client/d/a/m;)I

    move-result v0

    if-ne v0, v8, :cond_46d

    .line 242
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-eq v0, v8, :cond_2b5

    .line 243
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v10, :cond_41b

    .line 244
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 34061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 244
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 35061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 35267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 244
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 36087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 244
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->j:Lcom/teamspeak/ts3client/jni/c;

    move v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 245
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 37061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 37234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 245
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 38061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 38267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 245
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 39087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 245
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->k:Lcom/teamspeak/ts3client/jni/c;

    move v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 247
    :cond_41b
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-nez v0, :cond_2b5

    .line 248
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 40061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 40234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 248
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 41061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 41267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 248
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 42087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 248
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->j:Lcom/teamspeak/ts3client/jni/c;

    move v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 249
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 43061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 43234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 249
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 44061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 249
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 45087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 249
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->k:Lcom/teamspeak/ts3client/jni/c;

    move v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    goto/16 :goto_2b5

    .line 253
    :cond_46d
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v8, :cond_4bd

    .line 254
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 46061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 46234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 254
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 47061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 47267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 254
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 48087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 254
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->j:Lcom/teamspeak/ts3client/jni/c;

    move v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 255
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 49061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 255
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50062
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 255
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50063
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 255
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->k:Lcom/teamspeak/ts3client/jni/c;

    move v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 257
    :cond_4bd
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v10, :cond_2b5

    .line 258
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50064
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50065
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 258
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50066
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50067
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 258
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50068
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 258
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->j:Lcom/teamspeak/ts3client/jni/c;

    move v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    .line 259
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50069
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50070
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 259
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50071
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50072
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 259
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50073
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 259
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->k:Lcom/teamspeak/ts3client/jni/c;

    move v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;I)I

    goto/16 :goto_2b5

    .line 273
    :cond_50f
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50099
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50100
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 273
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50101
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50102
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 273
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50103
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 273
    const-string v6, "Create Channel"

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushChannelCreation(JJLjava/lang/String;)I

    goto/16 :goto_37b

    .line 276
    :cond_532
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50104
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50105
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 276
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50106
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50107
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 276
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/q;->a:Lcom/teamspeak/ts3client/d/a/m;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/m;->i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50108
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 276
    const-string v6, "Update Channel"

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushChannelUpdates(JJLjava/lang/String;)I

    goto/16 :goto_37b
.end method
