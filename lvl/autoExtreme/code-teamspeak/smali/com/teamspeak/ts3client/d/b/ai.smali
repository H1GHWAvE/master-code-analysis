.class final Lcom/teamspeak/ts3client/d/b/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Landroid/widget/RelativeLayout;

.field final synthetic c:Lcom/teamspeak/ts3client/d/b/ah;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/b/ah;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V
    .registers 4

    .prologue
    .line 61
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/ai;->c:Lcom/teamspeak/ts3client/d/b/ah;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/b/ai;->a:Lcom/teamspeak/ts3client/Ts3Application;

    iput-object p3, p0, Lcom/teamspeak/ts3client/d/b/ai;->b:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 9

    .prologue
    .line 64
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 65
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ai;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 65
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ai;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 65
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ai;->c:Lcom/teamspeak/ts3client/d/b/ah;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ah;->a(Lcom/teamspeak/ts3client/d/b/ah;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 3235
    iget v4, v0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 65
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ai;->c:Lcom/teamspeak/ts3client/d/b/ah;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ah;->b(Lcom/teamspeak/ts3client/d/b/ah;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Poke "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/teamspeak/ts3client/d/b/ai;->c:Lcom/teamspeak/ts3client/d/b/ah;

    invoke-static {v6}, Lcom/teamspeak/ts3client/d/b/ah;->a(Lcom/teamspeak/ts3client/d/b/ah;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v6

    .line 4235
    iget v6, v6, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 65
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestClientPoke(JILjava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ai;->c:Lcom/teamspeak/ts3client/d/b/ah;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/b/ah;->b()V

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ai;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 68
    return-void
.end method
