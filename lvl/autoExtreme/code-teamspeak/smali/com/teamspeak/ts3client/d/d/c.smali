.class public final Lcom/teamspeak/ts3client/d/d/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:J

.field e:J

.field f:J

.field g:Ljava/lang/String;

.field private h:J

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V
    .registers 15

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lcom/teamspeak/ts3client/d/d/c;->h:J

    .line 21
    iput-object p3, p0, Lcom/teamspeak/ts3client/d/d/c;->a:Ljava/lang/String;

    .line 22
    iput-object p4, p0, Lcom/teamspeak/ts3client/d/d/c;->i:Ljava/lang/String;

    .line 23
    iput-object p5, p0, Lcom/teamspeak/ts3client/d/d/c;->b:Ljava/lang/String;

    .line 24
    iput-object p6, p0, Lcom/teamspeak/ts3client/d/d/c;->c:Ljava/lang/String;

    .line 25
    iput-wide p7, p0, Lcom/teamspeak/ts3client/d/d/c;->d:J

    .line 26
    iput-wide p9, p0, Lcom/teamspeak/ts3client/d/d/c;->e:J

    .line 27
    iput-wide p11, p0, Lcom/teamspeak/ts3client/d/d/c;->f:J

    .line 28
    iput-object p13, p0, Lcom/teamspeak/ts3client/d/d/c;->g:Ljava/lang/String;

    .line 29
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 9

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/teamspeak/ts3client/d/d/c;->e:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method private d()Ljava/lang/String;
    .registers 9

    .prologue
    const-wide/16 v6, 0x3e8

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/teamspeak/ts3client/d/d/c;->d:J

    mul-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/teamspeak/ts3client/d/d/c;->e:J

    mul-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method private e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method private f()J
    .registers 3

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/teamspeak/ts3client/d/d/c;->h:J

    return-wide v0
.end method

.method private g()J
    .registers 3

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/teamspeak/ts3client/d/d/c;->f:J

    return-wide v0
.end method

.method private h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method private i()J
    .registers 3

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/teamspeak/ts3client/d/d/c;->e:J

    return-wide v0
.end method

.method private j()J
    .registers 3

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/teamspeak/ts3client/d/d/c;->d:J

    return-wide v0
.end method

.method private k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/c;->i:Ljava/lang/String;

    return-object v0
.end method
