.class public final Lcom/teamspeak/ts3client/b/b;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# instance fields
.field private a:Lcom/teamspeak/ts3client/b/f;

.field private b:Lcom/teamspeak/ts3client/Ts3Application;

.field private c:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 155
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/b/b;)Lcom/teamspeak/ts3client/b/f;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->a:Lcom/teamspeak/ts3client/b/f;

    return-object v0
.end method

.method private a()V
    .registers 5

    .prologue
    .line 149
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/b/b;->c:Ljava/util/Vector;

    .line 150
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->a:Lcom/teamspeak/ts3client/b/f;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/b/f;->clear()V

    .line 151
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 151
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 152
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 152
    iget-object v1, p0, Lcom/teamspeak/ts3client/b/b;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 152
    const-string v1, "requestbannlist"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestBanList(JLjava/lang/String;)I

    .line 153
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/b/b;)Ljava/util/Vector;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->c:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/b/b;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->b:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/b/b;)V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/teamspeak/ts3client/b/b;->a()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    .line 69
    const v0, 0x7f03001c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 70
    const v0, 0x7f0c008a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 71
    const v1, 0x7f0c008b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 81
    new-instance v3, Lcom/teamspeak/ts3client/b/c;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/b/c;-><init>(Lcom/teamspeak/ts3client/b/b;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 114
    new-instance v1, Lcom/teamspeak/ts3client/b/f;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/b/b;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/teamspeak/ts3client/b/f;-><init>(Lcom/teamspeak/ts3client/b/b;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/b/b;->a:Lcom/teamspeak/ts3client/b/f;

    .line 115
    iget-object v1, p0, Lcom/teamspeak/ts3client/b/b;->a:Lcom/teamspeak/ts3client/b/f;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 116
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/b/b;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 117
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 118
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/b/b;->n()V

    .line 119
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 119
    const-string v1, "menu.banlist"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 120
    return-object v2
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 125
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 126
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 127
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 5

    .prologue
    .line 131
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;

    if-eqz v0, :cond_19

    move-object v0, p1

    .line 132
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;

    .line 133
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2077
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 133
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/b/e;

    invoke-direct {v2, p0, v0}, Lcom/teamspeak/ts3client/b/e;-><init>(Lcom/teamspeak/ts3client/b/b;Lcom/teamspeak/ts3client/jni/events/rare/BanList;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 142
    :cond_19
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    if-eqz v0, :cond_32

    .line 143
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 3041
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 143
    const-string v1, "requestbannlist"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 144
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 144
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 146
    :cond_32
    return-void
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->e()V

    .line 47
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/b/b;->c:Ljava/util/Vector;

    .line 48
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->a:Lcom/teamspeak/ts3client/b/f;

    if-eqz v0, :cond_13

    .line 49
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/b;->a:Lcom/teamspeak/ts3client/b/f;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/b/f;->clear()V

    .line 51
    :cond_13
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->f()V

    .line 64
    return-void
.end method

.method public final s()V
    .registers 1

    .prologue
    .line 56
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->s()V

    .line 57
    invoke-direct {p0}, Lcom/teamspeak/ts3client/b/b;->a()V

    .line 58
    return-void
.end method
