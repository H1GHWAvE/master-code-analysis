.class public Lcom/teamspeak/ts3client/ConnectionBackground;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;
.implements Lcom/teamspeak/ts3client/tsdns/i;


# static fields
.field private static j:Landroid/app/Notification;

.field private static k:Z

.field private static l:Landroid/support/v4/app/dm;


# instance fields
.field protected a:J

.field protected b:Z

.field protected c:Z

.field protected d:I

.field e:Lcom/teamspeak/ts3client/Ts3Application;

.field f:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field g:Z

.field h:Z

.field public i:Landroid/content/BroadcastReceiver;

.field private m:Z

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Ljava/util/ArrayList;

.field private r:Z

.field private s:Landroid/view/WindowManager;

.field private t:Landroid/widget/ImageView;

.field private u:Landroid/view/WindowManager$LayoutParams;

.field private v:Z

.field private w:Landroid/view/animation/Animation;

.field private x:Landroid/content/IntentFilter;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 140
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->a:J

    .line 110
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    .line 112
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->m:Z

    .line 113
    iput v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->n:I

    .line 115
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->o:Z

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->q:Ljava/util/ArrayList;

    .line 118
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    .line 122
    new-instance v0, Lcom/teamspeak/ts3client/l;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/l;-><init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->i:Landroid/content/BroadcastReceiver;

    .line 141
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 142
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2202
    iput-object p0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 143
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 143
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 144
    new-instance v0, Lcom/teamspeak/ts3client/s;

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1}, Lcom/teamspeak/ts3client/s;-><init>(Lcom/teamspeak/ts3client/Ts3Application;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->f:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 145
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->f:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 146
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 146
    const-string v1, "talk_notification"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/teamspeak/ts3client/ConnectionBackground;->k:Z

    .line 147
    return-void
.end method

.method private a(Ljava/util/SortedMap;[Lcom/teamspeak/ts3client/data/a;JIILjava/util/concurrent/ConcurrentHashMap;Ljava/lang/Boolean;)I
    .registers 24

    .prologue
    .line 1381
    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/SortedMap;

    .line 1382
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 1384
    invoke-interface {v2}, Ljava/util/SortedMap;->size()I

    move-result v3

    new-array v13, v3, [Lcom/teamspeak/ts3client/data/a;

    .line 1385
    const-wide/16 v4, 0x0

    .line 1386
    const/4 v3, 0x0

    move v6, v3

    :goto_1b
    invoke-interface {v2}, Ljava/util/SortedMap;->size()I

    move-result v3

    if-ge v6, v3, :cond_32

    .line 1387
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/teamspeak/ts3client/data/a;

    .line 1388
    aput-object v3, v13, v6

    .line 1389
    add-int/lit8 v6, v6, 0x1

    .line 38087
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/a;->b:J

    goto :goto_1b

    .line 1392
    :cond_32
    invoke-virtual/range {p8 .. p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_b6

    .line 1393
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1394
    :goto_3d
    array-length v14, v13

    const/4 v3, 0x0

    move-object v11, v2

    move v12, v3

    :goto_41
    if-ge v12, v14, :cond_b5

    aget-object v2, v13, v12

    .line 38163
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 39087
    iget-wide v4, v2, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1396
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_91

    .line 39099
    move/from16 v0, p6

    iput v0, v2, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 1398
    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 39155
    iput-boolean v3, v2, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 40151
    iget-boolean v3, v2, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 1399
    if-nez v3, :cond_66

    .line 1400
    aput-object v2, p2, p5

    .line 41143
    :cond_66
    iget-boolean v3, v2, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 1401
    if-nez v3, :cond_6f

    .line 1402
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 1403
    :cond_6f
    add-int/lit8 v8, p5, 0x1

    .line 42087
    iget-wide v6, v2, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1404
    add-int/lit8 v9, p6, 0x1

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v10, p7

    invoke-direct/range {v3 .. v11}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Ljava/util/SortedMap;[Lcom/teamspeak/ts3client/data/a;JIILjava/util/concurrent/ConcurrentHashMap;Ljava/lang/Boolean;)I

    move-result v2

    .line 1405
    invoke-virtual/range {p8 .. p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_8b

    .line 1406
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 1394
    :cond_8b
    :goto_8b
    add-int/lit8 v3, v12, 0x1

    move/from16 p5, v2

    move v12, v3

    goto :goto_41

    .line 42099
    :cond_91
    move/from16 v0, p6

    iput v0, v2, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 1409
    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 42155
    iput-boolean v3, v2, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 43151
    iget-boolean v3, v2, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 1410
    if-nez v3, :cond_b2

    .line 1411
    aput-object v2, p2, p5

    .line 1413
    aget-object v2, p2, p5

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/a;->j()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    if-nez v2, :cond_b2

    .line 1414
    aget-object v2, p2, p5

    .line 43163
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 1416
    :cond_b2
    add-int/lit8 v2, p5, 0x1

    goto :goto_8b

    .line 1419
    :cond_b5
    return p5

    :cond_b6
    move-object v2, v7

    goto :goto_3d
.end method

.method public static a()Landroid/app/Notification;
    .registers 1

    .prologue
    .line 177
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    return-object v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->u:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method private a(I)V
    .registers 16

    .prologue
    const-wide/16 v8, 0x1

    const-wide/16 v12, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1568
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50331
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50332
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 1568
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 1626
    :goto_16
    return-void

    .line 1570
    :cond_17
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50333
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50334
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1570
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50335
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50336
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1570
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->b:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    .line 50337
    invoke-static {v3}, Lcom/teamspeak/ts3client/data/d/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 1571
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50339
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50340
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1571
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50341
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50342
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1571
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v12

    if-nez v0, :cond_3a8

    move v0, v1

    .line 50343
    :goto_4e
    iput-boolean v0, v3, Lcom/teamspeak/ts3client/data/c;->f:Z

    .line 1572
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50345
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50346
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1572
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50347
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50348
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1572
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v12

    if-nez v0, :cond_3ab

    move v0, v1

    :goto_6b
    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/c;->a(Z)V

    .line 1573
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50349
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50350
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1573
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50351
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50352
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1573
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->j:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-nez v0, :cond_3ae

    move v0, v1

    .line 50353
    :goto_89
    iput-boolean v0, v3, Lcom/teamspeak/ts3client/data/c;->i:Z

    .line 1574
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50355
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50356
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1574
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50357
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50358
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1574
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->i:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-nez v0, :cond_3b1

    move v0, v1

    .line 50359
    :goto_a6
    iput-boolean v0, v3, Lcom/teamspeak/ts3client/data/c;->h:Z

    .line 1575
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50361
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50362
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1575
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50363
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50364
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1575
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->I:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    .line 50365
    iput-object v3, v0, Lcom/teamspeak/ts3client/data/c;->t:Ljava/lang/String;

    .line 1576
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50367
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50368
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1576
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50369
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50370
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1576
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->H:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    .line 50371
    iput-object v3, v0, Lcom/teamspeak/ts3client/data/c;->u:Ljava/lang/String;

    .line 1577
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50373
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50374
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1577
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50375
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50376
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1577
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->O:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v3

    .line 50377
    iput v3, v0, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 1578
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50379
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50380
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1578
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50381
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50382
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1578
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    if-ne v0, v2, :cond_3b4

    move v0, v2

    .line 50383
    :goto_109
    iput-boolean v0, v3, Lcom/teamspeak/ts3client/data/c;->j:Z

    .line 1579
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50385
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50386
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1579
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50387
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50388
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1579
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->N:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    .line 50389
    iput-object v3, v0, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 1580
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50391
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50392
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1580
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50393
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50394
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1580
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->af:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    if-ne v0, v2, :cond_3b7

    move v0, v2

    .line 50395
    :goto_13c
    iput-boolean v0, v3, Lcom/teamspeak/ts3client/data/c;->n:Z

    .line 1581
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50397
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50398
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1581
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50399
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50400
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1581
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->a:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/data/c;->b(Ljava/lang/String;)V

    .line 1582
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50401
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50402
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1582
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50403
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50404
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1582
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->Z:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    cmp-long v0, v4, v12

    if-nez v0, :cond_3ba

    move v0, v1

    .line 50405
    :goto_172
    iput-boolean v0, v3, Lcom/teamspeak/ts3client/data/c;->k:Z

    .line 1583
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50407
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50408
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1583
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50409
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50410
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1583
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->Q:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v3

    .line 50411
    iput v3, v0, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 1584
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50413
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50414
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1584
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50415
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50416
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1584
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->R:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    .line 50417
    iput-object v3, v0, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 1585
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50419
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50420
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1585
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50421
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50422
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1585
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->S:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    .line 50423
    iput-object v3, v0, Lcom/teamspeak/ts3client/data/c;->q:Ljava/lang/String;

    .line 1586
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50425
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50426
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1586
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50427
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50428
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1586
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->ae:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, p1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/teamspeak/ts3client/data/c;->a(J)V

    .line 1588
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 50429
    iget-boolean v4, v0, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 1589
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50430
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50431
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1589
    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50432
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50433
    iget-wide v6, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1589
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->r:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, p1, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    cmp-long v0, v6, v12

    if-nez v0, :cond_3bd

    move v3, v1

    .line 1590
    :goto_1f2
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 50434
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 1591
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50436
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1591
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50437
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50438
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 1591
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v5, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    .line 1592
    if-eqz v4, :cond_257

    if-nez v3, :cond_257

    .line 50439
    iget-wide v6, v5, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1592
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50440
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50441
    iget-wide v8, v0, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 1592
    cmp-long v0, v6, v8

    if-nez v0, :cond_257

    .line 1593
    const-string v0, "event.client.recording.stop"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v7

    .line 50442
    iget-object v7, v7, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 1593
    aput-object v7, v6, v1

    invoke-static {v0, v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 1594
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v6, Lcom/teamspeak/ts3client/jni/h;->av:Lcom/teamspeak/ts3client/jni/h;

    new-instance v7, Lcom/teamspeak/ts3client/a/o;

    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v8

    .line 50443
    iget-object v8, v8, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 50444
    iget-object v9, v5, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 1594
    iget-object v10, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50445
    iget-object v10, v10, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1594
    invoke-virtual {v10}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v8, v1, v9, v10}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6, v7}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1596
    :cond_257
    if-nez v4, :cond_29a

    if-eqz v3, :cond_29a

    .line 50446
    iget-wide v6, v5, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1596
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50447
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50448
    iget-wide v8, v0, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 1596
    cmp-long v0, v6, v8

    if-nez v0, :cond_29a

    .line 1597
    const-string v0, "event.client.recording.started"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v4

    .line 50449
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 1597
    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 1598
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->au:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v6

    .line 50450
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 50451
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 1598
    iget-object v8, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50452
    iget-object v8, v8, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1598
    invoke-virtual {v8}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v6, v1, v7, v8}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1601
    :cond_29a
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    .line 1602
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->q:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b9

    .line 1603
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 50453
    iput v2, v0, Lcom/teamspeak/ts3client/data/c;->A:I

    .line 1604
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 50455
    iput v1, v0, Lcom/teamspeak/ts3client/data/c;->d:I

    .line 1606
    :cond_2b9
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 50457
    iget-boolean v3, v0, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 1607
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v4

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50458
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50459
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1607
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50460
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50461
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1607
    sget-object v8, Lcom/teamspeak/ts3client/jni/d;->U:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, p1, v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    cmp-long v0, v6, v12

    if-nez v0, :cond_3c0

    move v0, v1

    .line 50462
    :goto_2da
    iput-boolean v0, v4, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 1608
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 50464
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 50465
    iget v4, v5, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 1610
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50466
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50467
    iget v6, v6, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 1610
    if-eq p1, v6, :cond_334

    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v6

    .line 50468
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 1610
    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_334

    .line 50469
    iget-wide v6, v5, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1610
    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50470
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50471
    iget-wide v8, v5, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 1610
    cmp-long v5, v6, v8

    if-nez v5, :cond_334

    .line 1611
    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50472
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50473
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 1611
    sget-object v6, Lcom/teamspeak/ts3client/jni/g;->do:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v5

    if-eqz v5, :cond_334

    .line 1612
    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v5

    sget-object v6, Lcom/teamspeak/ts3client/jni/h;->ax:Lcom/teamspeak/ts3client/jni/h;

    new-instance v7, Lcom/teamspeak/ts3client/a/o;

    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v8

    .line 50474
    iget-object v8, v8, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 1612
    const-string v9, ""

    iget-object v10, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50475
    iget-object v10, v10, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1612
    invoke-virtual {v10}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v8, v1, v9, v10}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v7}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1614
    :cond_334
    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50476
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50477
    iget v5, v5, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 1614
    if-ne v5, p1, :cond_362

    if-lez v4, :cond_362

    .line 1615
    if-eq v3, v0, :cond_362

    .line 1616
    if-eqz v0, :cond_3c3

    .line 1617
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->aD:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 50478
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 1617
    const-string v6, ""

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50479
    iget-object v7, v7, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1617
    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1622
    :cond_362
    :goto_362
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 50482
    iget v0, v0, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 1622
    if-ne v0, v2, :cond_388

    .line 1623
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50483
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50484
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1623
    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50485
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50486
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1623
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 50487
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1623
    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->ac:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v1

    .line 50488
    iput v1, v0, Lcom/teamspeak/ts3client/data/c;->e:I

    .line 1625
    :cond_388
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50490
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1625
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50491
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50492
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 1625
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50493
    iput-boolean v2, v0, Lcom/teamspeak/ts3client/data/a;->d:Z

    goto/16 :goto_16

    :cond_3a8
    move v0, v2

    .line 1571
    goto/16 :goto_4e

    :cond_3ab
    move v0, v2

    .line 1572
    goto/16 :goto_6b

    :cond_3ae
    move v0, v2

    .line 1573
    goto/16 :goto_89

    :cond_3b1
    move v0, v2

    .line 1574
    goto/16 :goto_a6

    :cond_3b4
    move v0, v1

    .line 1578
    goto/16 :goto_109

    :cond_3b7
    move v0, v1

    .line 1580
    goto/16 :goto_13c

    :cond_3ba
    move v0, v2

    .line 1582
    goto/16 :goto_172

    :cond_3bd
    move v3, v2

    .line 1589
    goto/16 :goto_1f2

    :cond_3c0
    move v0, v2

    .line 1607
    goto/16 :goto_2da

    .line 1619
    :cond_3c3
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->aE:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 50480
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 1619
    const-string v6, ""

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50481
    iget-object v7, v7, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1619
    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_362
.end method

.method private a(IJJ)V
    .registers 8

    .prologue
    .line 1849
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50561
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1849
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/a;->e(I)V

    .line 1850
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50562
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1850
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/a;->d(I)V

    .line 1851
    return-void
.end method

.method private a(J)V
    .registers 12

    .prologue
    const/4 v7, 0x0

    .line 1319
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1319
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->j()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_19
    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_76

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1320
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13364
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 1320
    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1321
    if-eqz v0, :cond_19

    .line 13418
    iget-boolean v2, v0, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 1322
    if-eqz v2, :cond_19

    .line 1323
    const-string v2, "event.client.recording.isrecording"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 14203
    iget-object v4, v0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 1323
    aput-object v4, v3, v7

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 1324
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->aw:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    .line 15203
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 1324
    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1324
    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    .line 16103
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 1324
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1324
    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v0, v7, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto :goto_19

    .line 1329
    :cond_76
    return-void
.end method

.method private a(Landroid/view/WindowManager$LayoutParams;)V
    .registers 5

    .prologue
    const/16 v2, 0x64

    .line 1779
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_28

    .line 1780
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50540
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1780
    const-string v1, "overlay_last_x_l"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1781
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50541
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1781
    const-string v1, "overlay_last_y_l"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1789
    :goto_27
    return-void

    .line 1782
    :cond_28
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4e

    .line 1783
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50542
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1783
    const-string v1, "overlay_last_x_p"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1784
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50543
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1784
    const-string v1, "overlay_last_y_p"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_27

    .line 1786
    :cond_4e
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50544
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1786
    const-string v1, "overlay_last_x_u"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1787
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50545
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1787
    const-string v1, "overlay_last_y_u"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_27
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/ConnectionBackground;Landroid/view/WindowManager$LayoutParams;)V
    .registers 2

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/data/a;)V
    .registers 4

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50115
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50116
    iget-wide v0, v0, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 1450
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/data/a;)V

    .line 1451
    return-void
.end method

.method public static a(Z)V
    .registers 7

    .prologue
    const v5, 0x7f02009e

    const/high16 v4, 0x42400000    # 48.0f

    const v3, 0x7f020076

    const/4 v2, 0x1

    .line 151
    if-eqz p0, :cond_6d

    .line 152
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6364
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 152
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 7061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7242
    iget v1, v1, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 152
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 8219
    iget v0, v0, Lcom/teamspeak/ts3client/data/c;->d:I

    .line 152
    if-nez v0, :cond_3c

    .line 153
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0, v3}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    .line 154
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    .line 155
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 9196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 155
    sget-object v1, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 174
    :goto_3b
    return-void

    .line 157
    :cond_3c
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    .line 158
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-static {v5, v4, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 10147
    iput-object v1, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    .line 159
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    .line 160
    const v1, -0xffff01

    iput v1, v0, Landroid/app/Notification;->ledARGB:I

    .line 161
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x3

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 162
    const/4 v1, 0x0

    iput v1, v0, Landroid/app/Notification;->ledOffMS:I

    .line 163
    iput v2, v0, Landroid/app/Notification;->ledOnMS:I

    .line 164
    sput-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    .line 165
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 10196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 165
    sget-object v1, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_3b

    .line 168
    :cond_6d
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0, v3}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    .line 169
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 11147
    iput-object v1, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    .line 170
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    .line 171
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 11196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 171
    sget-object v1, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_3b
.end method

.method private a(IJ)Z
    .registers 6

    .prologue
    .line 1845
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50559
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1845
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50560
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/a;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1845
    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/ConnectionBackground;Z)Z
    .registers 2

    .prologue
    .line 100
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->v:Z

    return p1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/widget/ImageView;
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->t:Landroid/widget/ImageView;

    return-object v0
.end method

.method private b(I)Lcom/teamspeak/ts3client/data/c;
    .registers 3

    .prologue
    .line 1854
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50563
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50564
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 1854
    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    return-object v0
.end method

.method private b(J)V
    .registers 6

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1332
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->j()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1333
    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1334
    if-nez v0, :cond_2f

    .line 1338
    :cond_2e
    return-void

    .line 18223
    :cond_2f
    const/4 v2, 0x0

    iput v2, v0, Lcom/teamspeak/ts3client/data/c;->d:I

    goto :goto_18
.end method

.method private b(Lcom/teamspeak/ts3client/data/a;)V
    .registers 12

    .prologue
    .line 1454
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50117
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50118
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1454
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50119
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50120
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50121
    iget-wide v4, p1, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1454
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getChannelClientList(JJ)[I

    move-result-object v2

    .line 1455
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_15
    if-ge v1, v3, :cond_2b0

    aget v4, v2, v1

    .line 1456
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50122
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50123
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 1456
    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/d;->a(I)Z

    move-result v0

    if-nez v0, :cond_254

    .line 1457
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50124
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50125
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1457
    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50126
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50127
    iget-wide v6, v5, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1457
    sget-object v5, Lcom/teamspeak/ts3client/jni/d;->b:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 1458
    new-instance v5, Lcom/teamspeak/ts3client/data/c;

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v5, v0, v4, v6}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V

    .line 1459
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50128
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50129
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1459
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50130
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50131
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50132
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1459
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_26f

    const/4 v0, 0x0

    .line 50133
    :goto_59
    iput-boolean v0, v5, Lcom/teamspeak/ts3client/data/c;->f:Z

    .line 1460
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50135
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50136
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1460
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50137
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50138
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50139
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1460
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_272

    const/4 v0, 0x0

    :goto_76
    invoke-virtual {v5, v0}, Lcom/teamspeak/ts3client/data/c;->a(Z)V

    .line 1461
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50140
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50141
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1461
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50142
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50143
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50144
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1461
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->j:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    const-wide/16 v8, 0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_275

    const/4 v0, 0x0

    .line 50145
    :goto_94
    iput-boolean v0, v5, Lcom/teamspeak/ts3client/data/c;->i:Z

    .line 1462
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50147
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50148
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1462
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50149
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50150
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50151
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1462
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->i:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    const-wide/16 v8, 0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_278

    const/4 v0, 0x0

    .line 50152
    :goto_b1
    iput-boolean v0, v5, Lcom/teamspeak/ts3client/data/c;->h:Z

    .line 1463
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50154
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50155
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1463
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50156
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50157
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50158
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1463
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->r:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_27b

    const/4 v0, 0x0

    .line 50159
    :goto_ce
    iput-boolean v0, v5, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 50161
    iget-wide v6, p1, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1464
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50162
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50163
    iget-wide v8, v0, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 1464
    cmp-long v0, v6, v8

    if-nez v0, :cond_283

    .line 50164
    iget v0, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1470
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50165
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50166
    iget v6, v6, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 1470
    if-eq v0, v6, :cond_f7

    .line 1473
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->q:Ljava/util/ArrayList;

    .line 50167
    iget v6, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1473
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27e

    .line 50168
    const/4 v0, 0x1

    iput v0, v5, Lcom/teamspeak/ts3client/data/c;->A:I

    .line 1492
    :cond_f7
    :goto_f7
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50180
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1492
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50182
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50183
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50184
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1492
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->I:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 50185
    iput-object v0, v5, Lcom/teamspeak/ts3client/data/c;->t:Ljava/lang/String;

    .line 1493
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50187
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50188
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1493
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50189
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50190
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50191
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1493
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->H:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 50192
    iput-object v0, v5, Lcom/teamspeak/ts3client/data/c;->u:Ljava/lang/String;

    .line 1494
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50194
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50195
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1494
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50196
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50197
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50198
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1494
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->O:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    .line 50199
    iput v0, v5, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 50201
    iget v0, v5, Lcom/teamspeak/ts3client/data/c;->v:I

    .line 1495
    const/4 v6, 0x1

    if-ne v0, v6, :cond_154

    .line 1496
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50202
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50203
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1496
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50204
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50205
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50206
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1496
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->ac:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    .line 50207
    iput v0, v5, Lcom/teamspeak/ts3client/data/c;->e:I

    .line 1498
    :cond_154
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50209
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50210
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1498
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50211
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50212
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50213
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1498
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    const/4 v6, 0x1

    if-ne v0, v6, :cond_2a5

    const/4 v0, 0x1

    .line 50214
    :goto_16c
    iput-boolean v0, v5, Lcom/teamspeak/ts3client/data/c;->j:Z

    .line 1499
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50216
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50217
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1499
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50218
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50219
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50220
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1499
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->N:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 50221
    iput-object v0, v5, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 1500
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50223
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50224
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1500
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50225
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50226
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50227
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1500
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->af:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    const/4 v6, 0x1

    if-ne v0, v6, :cond_2a8

    const/4 v0, 0x1

    .line 50228
    :goto_19c
    iput-boolean v0, v5, Lcom/teamspeak/ts3client/data/c;->n:Z

    .line 1501
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50230
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50231
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1501
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50232
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50233
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50234
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1501
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->a:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/teamspeak/ts3client/data/c;->b(Ljava/lang/String;)V

    .line 1502
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50235
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50236
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1502
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50237
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50238
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50239
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1502
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->Z:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_2ab

    const/4 v0, 0x0

    .line 50240
    :goto_1d0
    iput-boolean v0, v5, Lcom/teamspeak/ts3client/data/c;->k:Z

    .line 1503
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50242
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1503
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50244
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50245
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50246
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1503
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->Q:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    .line 50247
    iput v0, v5, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 1505
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50249
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50250
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1505
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50251
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50252
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50253
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1505
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->R:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 50254
    iput-object v0, v5, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 1506
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50256
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50257
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1506
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50258
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50259
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50260
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1506
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->S:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v0

    .line 50261
    iput-object v0, v5, Lcom/teamspeak/ts3client/data/c;->q:Ljava/lang/String;

    .line 1507
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50263
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50264
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1507
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50265
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50266
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50267
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1507
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->U:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_2ae

    const/4 v0, 0x0

    .line 50268
    :goto_22f
    iput-boolean v0, v5, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 1509
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50270
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50271
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1509
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50272
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50273
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50274
    iget v8, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1509
    sget-object v9, Lcom/teamspeak/ts3client/jni/d;->ae:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/teamspeak/ts3client/data/c;->a(J)V

    .line 1511
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50275
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50276
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 1511
    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/data/d;->a(Lcom/teamspeak/ts3client/data/c;)V

    .line 1513
    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/c;->a()V

    .line 1515
    :cond_254
    invoke-virtual {p1, v4}, Lcom/teamspeak/ts3client/data/a;->d(I)V

    .line 1516
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50277
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50278
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 1516
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 50279
    iget-wide v6, p1, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1516
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_15

    .line 1459
    :cond_26f
    const/4 v0, 0x1

    goto/16 :goto_59

    .line 1460
    :cond_272
    const/4 v0, 0x1

    goto/16 :goto_76

    .line 1461
    :cond_275
    const/4 v0, 0x1

    goto/16 :goto_94

    .line 1462
    :cond_278
    const/4 v0, 0x1

    goto/16 :goto_b1

    .line 1463
    :cond_27b
    const/4 v0, 0x1

    goto/16 :goto_ce

    .line 50170
    :cond_27e
    const/4 v0, 0x0

    iput v0, v5, Lcom/teamspeak/ts3client/data/c;->A:I

    goto/16 :goto_f7

    .line 50172
    :cond_283
    iget v0, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1482
    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50173
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50174
    iget v6, v6, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 1482
    if-eq v0, v6, :cond_f7

    .line 1483
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->q:Ljava/util/ArrayList;

    .line 50175
    iget v6, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 1483
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a0

    .line 50176
    const/4 v0, 0x1

    iput v0, v5, Lcom/teamspeak/ts3client/data/c;->A:I

    goto/16 :goto_f7

    .line 50178
    :cond_2a0
    const/4 v0, 0x0

    iput v0, v5, Lcom/teamspeak/ts3client/data/c;->A:I

    goto/16 :goto_f7

    .line 1498
    :cond_2a5
    const/4 v0, 0x0

    goto/16 :goto_16c

    .line 1500
    :cond_2a8
    const/4 v0, 0x0

    goto/16 :goto_19c

    .line 1502
    :cond_2ab
    const/4 v0, 0x1

    goto/16 :goto_1d0

    .line 1507
    :cond_2ae
    const/4 v0, 0x1

    goto :goto_22f

    .line 1518
    :cond_2b0
    return-void
.end method

.method static synthetic b(Z)Z
    .registers 1

    .prologue
    .line 100
    sput-boolean p0, Lcom/teamspeak/ts3client/ConnectionBackground;->k:Z

    return p0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager;
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->s:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/content/IntentFilter;
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->x:Landroid/content/IntentFilter;

    return-object v0
.end method

.method static synthetic e()Z
    .registers 1

    .prologue
    .line 100
    sget-boolean v0, Lcom/teamspeak/ts3client/ConnectionBackground;->k:Z

    return v0
.end method

.method private f()V
    .registers 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1341
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1341
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1342
    if-eqz v0, :cond_11

    .line 1343
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    .line 1368
    :cond_10
    :goto_10
    return-void

    .line 1346
    :cond_11
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 1346
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->cw:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    .line 1347
    if-eqz v0, :cond_99

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    if-nez v0, :cond_99

    .line 1348
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1348
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->H()V

    .line 1349
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1349
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1349
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1350
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1350
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1350
    const-string v1, "voiceactivation_level"

    const-string v4, "-50"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 1351
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1351
    invoke-virtual {v0, v8}, Lcom/teamspeak/ts3client/t;->i(Z)V

    .line 1352
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 27061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 27666
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/ai;

    invoke-direct {v2, v0}, Lcom/teamspeak/ts3client/ai;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1353
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1353
    const-string v1, "messages.forceptt.on"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.forceptt.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 1354
    iput-boolean v8, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    .line 1355
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->c()V

    goto/16 :goto_10

    .line 1356
    :cond_99
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    if-eqz v0, :cond_10

    .line 1357
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 29061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 29157
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 1357
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->d()V

    .line 1358
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1358
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1358
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1359
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1359
    const-string v1, "voiceactivation_level"

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1360
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 33234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1360
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1360
    const-string v4, "voiceactivation_level"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 1361
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 35061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 35206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1361
    invoke-virtual {v0, v7}, Lcom/teamspeak/ts3client/t;->i(Z)V

    .line 1362
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 36061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 36206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1362
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->C()V

    .line 1363
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 37061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 37206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1363
    const-string v1, "messages.forceptt.off"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.forceptt.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 1364
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->d()V

    .line 1365
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    goto/16 :goto_10
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/ConnectionBackground;)V
    .registers 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->f()V

    return-void
.end method

.method private g()V
    .registers 20

    .prologue
    .line 1423
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 44061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1423
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 45061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 45267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1423
    invoke-virtual {v2, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getChannelList(J)[J

    move-result-object v16

    .line 1424
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v2, 0x0

    move v15, v2

    :goto_1b
    move/from16 v0, v17

    if-ge v15, v0, :cond_160

    aget-wide v6, v16, v15

    .line 1425
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 46061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 46234
    iget-object v3, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1425
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 47061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 47267
    iget-wide v4, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1425
    sget-object v8, Lcom/teamspeak/ts3client/jni/c;->a:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v3 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JJLcom/teamspeak/ts3client/jni/c;)Ljava/lang/String;

    move-result-object v2

    .line 1426
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 48061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 48234
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1426
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 49061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49267
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1426
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getParentChannelOfChannel(JJ)J

    move-result-wide v12

    .line 1427
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50062
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1427
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50063
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50064
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1427
    sget-object v8, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v3 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JJLcom/teamspeak/ts3client/jni/c;)J

    move-result-wide v10

    .line 1428
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50065
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50066
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1428
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50067
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50068
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1428
    sget-object v8, Lcom/teamspeak/ts3client/jni/c;->m:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v3 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v18

    .line 1429
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50069
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50070
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1429
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50071
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50072
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1429
    sget-object v8, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v3 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JJLcom/teamspeak/ts3client/jni/c;)J

    .line 1430
    new-instance v4, Lcom/teamspeak/ts3client/data/a;

    move-object v5, v2

    move-wide v8, v12

    invoke-direct/range {v4 .. v11}, Lcom/teamspeak/ts3client/data/a;-><init>(Ljava/lang/String;JJJ)V

    .line 1431
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50073
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50074
    iget-object v9, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1431
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50075
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50076
    iget-wide v10, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1431
    sget-object v14, Lcom/teamspeak/ts3client/jni/c;->C:Lcom/teamspeak/ts3client/jni/c;

    move-wide v12, v6

    invoke-virtual/range {v9 .. v14}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v2

    .line 50077
    iput v2, v4, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 1432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50079
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50080
    iget-object v9, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50081
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50082
    iget-wide v10, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1432
    sget-object v14, Lcom/teamspeak/ts3client/jni/c;->e:Lcom/teamspeak/ts3client/jni/c;

    move-wide v12, v6

    invoke-virtual/range {v9 .. v14}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v2

    .line 50083
    iput v2, v4, Lcom/teamspeak/ts3client/data/a;->l:I

    .line 1433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50086
    iget-object v9, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50087
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50088
    iget-wide v10, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1433
    sget-object v14, Lcom/teamspeak/ts3client/jni/c;->l:Lcom/teamspeak/ts3client/jni/c;

    move-wide v12, v6

    invoke-virtual/range {v9 .. v14}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_15c

    const/4 v2, 0x1

    .line 50089
    :goto_e0
    iput-boolean v2, v4, Lcom/teamspeak/ts3client/data/a;->r:Z

    .line 50091
    iget-boolean v2, v4, Lcom/teamspeak/ts3client/data/a;->r:Z

    .line 1434
    if-eqz v2, :cond_f2

    .line 1435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50092
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1435
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    .line 50093
    iput-object v4, v2, Lcom/teamspeak/ts3client/data/b;->b:Lcom/teamspeak/ts3client/data/a;

    .line 1436
    :cond_f2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50095
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50096
    iget-object v9, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1436
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50097
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50098
    iget-wide v10, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1436
    sget-object v14, Lcom/teamspeak/ts3client/jni/c;->g:Lcom/teamspeak/ts3client/jni/c;

    move-wide v12, v6

    invoke-virtual/range {v9 .. v14}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v2

    .line 50099
    iput v2, v4, Lcom/teamspeak/ts3client/data/a;->i:I

    .line 1437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50101
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50102
    iget-object v9, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1437
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50103
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50104
    iget-wide v10, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1437
    sget-object v14, Lcom/teamspeak/ts3client/jni/c;->G:Lcom/teamspeak/ts3client/jni/c;

    move-wide v12, v6

    invoke-virtual/range {v9 .. v14}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_15e

    const/4 v2, 0x1

    .line 50105
    :goto_126
    iput-boolean v2, v4, Lcom/teamspeak/ts3client/data/a;->j:Z

    .line 1438
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50107
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50108
    iget-object v9, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1438
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50109
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50110
    iget-wide v10, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1438
    sget-object v14, Lcom/teamspeak/ts3client/jni/c;->F:Lcom/teamspeak/ts3client/jni/c;

    move-wide v12, v6

    invoke-virtual/range {v9 .. v14}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JJLcom/teamspeak/ts3client/jni/c;)J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Lcom/teamspeak/ts3client/data/a;->c(J)V

    .line 1439
    const/4 v2, 0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_14a

    .line 50111
    const/4 v2, 0x1

    iput-boolean v2, v4, Lcom/teamspeak/ts3client/data/a;->m:Z

    .line 1442
    :cond_14a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50113
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1442
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/teamspeak/ts3client/data/b;->a(Lcom/teamspeak/ts3client/data/a;)V

    .line 1424
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto/16 :goto_1b

    .line 1433
    :cond_15c
    const/4 v2, 0x0

    goto :goto_e0

    .line 1437
    :cond_15e
    const/4 v2, 0x0

    goto :goto_126

    .line 1444
    :cond_160
    return-void
.end method

.method private h()Z
    .registers 2

    .prologue
    .line 1521
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    return v0
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 1525
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->o:Z

    return v0
.end method

.method private j()V
    .registers 10

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 1529
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50280
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50281
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1529
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50282
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50283
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1529
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getClientID(J)I

    move-result v0

    .line 1530
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50284
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50285
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1530
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50286
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50287
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1530
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->G:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v2

    .line 1531
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50288
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50289
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1531
    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50290
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50291
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1531
    invoke-virtual {v1, v4, v5, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getChannelOfClient(JI)J

    move-result-wide v4

    .line 1532
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50292
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50293
    iput v0, v1, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 1533
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50295
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50296
    iput-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->z:J

    .line 1534
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50298
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50299
    iput-wide v4, v0, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 1535
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50301
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1535
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 50302
    iput-boolean v8, v0, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 1537
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    const-class v2, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1538
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1539
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1540
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-static {v1, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1542
    new-instance v1, Landroid/support/v4/app/dm;

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/dm;-><init>(Landroid/content/Context;)V

    .line 1543
    sput-object v1, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    const v2, 0x7f020076

    invoke-virtual {v1, v2}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    move-result-object v1

    const-string v2, "messages.notification.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/dm;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/dm;->a(J)Landroid/support/v4/app/dm;

    move-result-object v1

    .line 50304
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v8}, Landroid/support/v4/app/dm;->a(IZ)V

    .line 1543
    invoke-virtual {v1}, Landroid/support/v4/app/dm;->a()Landroid/support/v4/app/dm;

    move-result-object v1

    const-string v2, "messages.notification.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/dm;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50306
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50307
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1543
    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50308
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50309
    iget-wide v6, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1543
    sget-object v3, Lcom/teamspeak/ts3client/jni/i;->b:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v2, v6, v7, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/dm;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v1

    .line 50310
    iput-object v0, v1, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    .line 1544
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    .line 1545
    invoke-virtual {p0, v8, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->startForeground(ILandroid/app/Notification;)V

    .line 1546
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50312
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50313
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50314
    iput-wide v4, v0, Lcom/teamspeak/ts3client/data/ab;->q:J

    .line 1547
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50316
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50317
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->j:Ljava/util/HashMap;

    .line 1547
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ff

    .line 1548
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50318
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50319
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 1548
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50320
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50321
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->j:Ljava/util/HashMap;

    .line 1548
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50322
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->r:Ljava/lang/String;

    .line 1551
    :goto_fe
    return-void

    .line 1550
    :cond_ff
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50324
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50325
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 1550
    const-string v1, ""

    .line 50326
    iput-object v1, v0, Lcom/teamspeak/ts3client/data/ab;->r:Ljava/lang/String;

    goto :goto_fe
.end method

.method private k()V
    .registers 1

    .prologue
    .line 1554
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    .line 1555
    return-void
.end method

.method private l()V
    .registers 2

    .prologue
    .line 1558
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    .line 1559
    return-void
.end method

.method private m()V
    .registers 3

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50328
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50329
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 1562
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 1563
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50330
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1563
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->f:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1564
    return-void
.end method

.method private n()V
    .registers 5

    .prologue
    .line 1683
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50534
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1683
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    .line 50535
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 1683
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 1684
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50537
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50538
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 1687
    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/data/a;)V

    goto :goto_12

    .line 1689
    :cond_28
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    .line 1690
    return-void
.end method

.method private o()Z
    .registers 2

    .prologue
    .line 1841
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->m:Z

    return v0
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 14

    .prologue
    .line 186
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerStop;

    if-eqz v0, :cond_a7

    move-object v0, p1

    .line 187
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerStop;

    .line 189
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v1

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->b:Lcom/teamspeak/ts3client/jni/h;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 191
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    const-class v3, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 192
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 196
    new-instance v2, Landroid/support/v4/app/dm;

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/support/v4/app/dm;-><init>(Landroid/content/Context;)V

    .line 197
    const v3, 0x7f020076

    invoke-virtual {v2, v3}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    move-result-object v3

    const-string v4, "messages.servershutdown.text"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/dm;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/dm;->a(J)Landroid/support/v4/app/dm;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/dm;->a()Landroid/support/v4/app/dm;

    move-result-object v3

    const-string v4, "messages.servershutdown.text"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/dm;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v3

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/ServerStop;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/dm;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/dm;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/dm;

    .line 199
    invoke-virtual {v2}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    .line 200
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x14

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 202
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->j()Landroid/app/NotificationManager;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x66

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 203
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->d()V

    .line 204
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->E()V

    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->m:Z

    .line 207
    :cond_a7
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;

    if-eqz v0, :cond_118

    move-object v1, p1

    .line 208
    check-cast v1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;

    .line 209
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->c()I

    move-result v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->j()Lcom/teamspeak/ts3client/data/d/v;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->bK:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v2

    if-ne v0, v2, :cond_c5

    .line 1316
    :cond_c4
    :goto_c4
    :sswitch_c4
    return-void

    .line 213
    :cond_c5
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->G:Lcom/teamspeak/ts3client/jni/h;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 214
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->a()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.serverpermission.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.close"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 216
    :cond_118
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerEdited;

    if-eqz v0, :cond_186

    .line 218
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->b:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/e;->a(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->ap:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JLcom/teamspeak/ts3client/jni/i;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/ab;->a(J)V

    .line 220
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->c()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Ljava/lang/String;)V

    .line 221
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    goto/16 :goto_c4

    .line 224
    :cond_186
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    if-eqz v0, :cond_890

    .line 225
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 226
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_39b6

    .line 367
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->d()V

    .line 368
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_862

    .line 369
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.error.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 372
    :goto_1ed
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->m:Z

    .line 376
    :cond_1f0
    :goto_1f0
    :sswitch_1f0
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->a()I

    move-result v0

    if-eqz v0, :cond_c4

    .line 377
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->G:Lcom/teamspeak/ts3client/jni/h;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_c4

    .line 228
    :sswitch_204
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SubscribeAll"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22c

    .line 229
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/ab;->a(Z)V

    .line 230
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->h(Z)V

    .line 232
    :cond_22c
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UnsubscribeAll"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_254

    .line 233
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/ab;->a(Z)V

    .line 234
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->h(Z)V

    .line 236
    :cond_254
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Subscribe Channel:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_292

    .line 237
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->g()Z

    move-result v0

    if-nez v0, :cond_292

    .line 238
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/Long;)V

    .line 241
    :cond_292
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InputMute"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2ac

    .line 242
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->f(Z)V

    .line 244
    :cond_2ac
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InputUnMute"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f0

    .line 245
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->f(Z)V

    goto/16 :goto_1f0

    .line 250
    :sswitch_2c8
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Composing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Send Message to Client"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Chat closed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c4

    .line 252
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "connectioninfo_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c4

    .line 255
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.clienterror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 258
    :sswitch_326
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.clienterror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 261
    :sswitch_354
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->p:Z

    .line 262
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 263
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.disconnected.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 266
    :sswitch_392
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->p:Z

    .line 267
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 268
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "messages.clientoutdated.text2"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.clientoutdated.text1"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 271
    :sswitch_3d2
    const-string v0, "Action currently not possible due to spam protection. Please wait a few seconds and try again."

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 274
    :sswitch_3d9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->p:Z

    .line 275
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 276
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "messages.error.securitylevel"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.disconnected.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 283
    :sswitch_423
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_481

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "invalid channel password"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_481

    .line 285
    :try_start_439
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 286
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.pwerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
    :try_end_47e
    .catch Ljava/lang/Exception; {:try_start_439 .. :try_end_47e} :catch_480

    goto/16 :goto_c4

    :catch_480
    move-exception v0

    .line 293
    :cond_481
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "skip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52e

    .line 294
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 295
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v7

    .line 296
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/a;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_50d

    .line 297
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/a;->b()J

    move-result-wide v4

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/a;->b()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "join "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/a;->b()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestClientMove(JIJLjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_c4

    .line 299
    :cond_50d
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/t;->a(Lcom/teamspeak/ts3client/data/a;)V

    goto/16 :goto_c4

    .line 302
    :cond_52e
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "join"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_579

    .line 303
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 304
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/t;->a(Lcom/teamspeak/ts3client/data/a;)V

    goto/16 :goto_c4

    .line 308
    :cond_579
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "checkPW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Request Image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c4

    .line 309
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.pwerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 314
    :sswitch_5bf
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.channelerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 317
    :sswitch_5ed
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.channelerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 320
    :sswitch_61b
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.channelerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 323
    :sswitch_649
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.channelerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 326
    :sswitch_677
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.channelerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 329
    :sswitch_6a5
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.channelerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 332
    :sswitch_6d3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->p:Z

    .line 333
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 334
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.disconnected.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 337
    :sswitch_711
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->p:Z

    .line 338
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 339
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.disconnected.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 344
    :sswitch_74f
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.channelerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 353
    :sswitch_77d
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.permerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 356
    :sswitch_7ab
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.error.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 359
    :sswitch_7d9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->p:Z

    .line 360
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 361
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.ban.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 364
    :sswitch_832
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "privilegekey.error.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "privilegekey.error"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1f0

    .line 371
    :cond_862
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ServerError;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.error.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.ok"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_1ed

    .line 381
    :cond_890
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;

    if-eqz v0, :cond_ce3

    .line 382
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;

    .line 383
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_3a24

    .line 403
    :goto_89d
    sget-object v0, Lcom/teamspeak/ts3client/jni/e;->a:Lcom/teamspeak/ts3client/jni/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/e;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b()I

    move-result v1

    if-ne v0, v1, :cond_9ff

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->m:Z

    if-nez v0, :cond_9ff

    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    .line 405
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    if-eqz v0, :cond_9ff

    .line 406
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->a()I

    move-result v0

    const/16 v1, 0x701

    if-eq v0, v1, :cond_8dc

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->i()I

    move-result v0

    if-lez v0, :cond_96b

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->a()I

    move-result v0

    const/16 v1, 0x705

    if-ne v0, v1, :cond_96b

    .line 407
    :cond_8dc
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->c:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    const-string v3, ""

    const/4 v4, 0x0

    const-string v5, ""

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 408
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->d()V

    .line 409
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->E()V

    goto/16 :goto_c4

    .line 385
    :pswitch_920
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->e()Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Connect status: STATUS_DISCONNECTED"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_89d

    .line 388
    :pswitch_92f
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->e()Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Connect status: STATUS_CONNECTING"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_89d

    .line 391
    :pswitch_93e
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->e()Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Connect status: STATUS_CONNECTED"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_89d

    .line 394
    :pswitch_94d
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->e()Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Connect status: STATUS_CONNECTION_ESTABLISHING"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_89d

    .line 397
    :pswitch_95c
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->e()Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Connect status: STATUS_CONNECTION_ESTABLISHED"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_89d

    .line 412
    :cond_96b
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->a()I

    move-result v0

    if-lez v0, :cond_9c9

    .line 413
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->d()V

    .line 414
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "messages.error.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->a()I

    move-result v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/aa;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.conerror.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/teamspeak/ts3client/jni/h;->c:Lcom/teamspeak/ts3client/jni/h;

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V

    goto/16 :goto_c4

    .line 417
    :cond_9c9
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->p:Z

    if-nez v0, :cond_c4

    .line 418
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "messages.disconnected.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.disconnected.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/teamspeak/ts3client/jni/h;->b:Lcom/teamspeak/ts3client/jni/h;

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V

    goto/16 :goto_c4

    .line 424
    :cond_9ff
    sget-object v0, Lcom/teamspeak/ts3client/jni/e;->b:Lcom/teamspeak/ts3client/jni/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/e;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b()I

    move-result v1

    if-ne v0, v1, :cond_a32

    .line 425
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    if-eqz v0, :cond_a2f

    .line 426
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "connectiondialog.step0"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 427
    :cond_a2f
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    .line 430
    :cond_a32
    sget-object v0, Lcom/teamspeak/ts3client/jni/e;->c:Lcom/teamspeak/ts3client/jni/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/e;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b()I

    move-result v1

    if-ne v0, v1, :cond_afb

    .line 431
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    if-eqz v0, :cond_a62

    .line 432
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "connectiondialog.step1"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 433
    :cond_a62
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->a:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/e;->b(Ljava/lang/String;)V

    .line 434
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->b:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/e;->a(Ljava/lang/String;)V

    .line 435
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->ap:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JLcom/teamspeak/ts3client/jni/i;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/ab;->a(J)V

    .line 436
    const-string v0, "chat.connected"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 437
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/data/c/d;

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v1, v2}, Lcom/teamspeak/ts3client/data/c/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/e;->a(Lcom/teamspeak/ts3client/data/c/d;)V

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    .line 440
    :cond_afb
    sget-object v0, Lcom/teamspeak/ts3client/jni/e;->d:Lcom/teamspeak/ts3client/jni/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/e;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b()I

    move-result v1

    if-ne v0, v1, :cond_b2e

    .line 441
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    if-eqz v0, :cond_b2b

    .line 442
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "connectiondialog.step2"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 443
    :cond_b2b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    .line 446
    :cond_b2e
    sget-object v0, Lcom/teamspeak/ts3client/jni/e;->e:Lcom/teamspeak/ts3client/jni/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/e;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b()I

    move-result v1

    if-ne v0, v1, :cond_c4

    .line 447
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->g()V

    .line 448
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    if-eqz v0, :cond_b61

    .line 449
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "connectiondialog.step3"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 450
    :cond_b61
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->n()V

    .line 452
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->j()V

    .line 453
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    .line 454
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/c;->a()Lcom/teamspeak/ts3client/data/b/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/c;->c()V

    .line 455
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->a:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    const-string v3, ""

    const/4 v4, 0x0

    const-string v5, ""

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 456
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->b()Z

    move-result v0

    if-eqz v0, :cond_bb4

    .line 457
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/m;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/m;-><init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_c4

    .line 466
    :cond_bb4
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->j()V

    .line 468
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/n;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/n;-><init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 485
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/data/d/v;

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;-><init>(Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/e;->a(Lcom/teamspeak/ts3client/data/d/v;)V

    .line 486
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    if-eqz v0, :cond_bfc

    .line 487
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->d()V

    .line 488
    :cond_bfc
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    sget-object v1, Lcom/teamspeak/ts3client/jni/i;->az:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/i;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c27

    .line 489
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->a(Z)V

    .line 491
    :cond_c27
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->c()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->b()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Ljava/lang/String;)V

    .line 493
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->g()Z

    move-result v0

    if-eqz v0, :cond_cab

    .line 494
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    const-string v1, "SubscribeAll"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelSubscribeAll(JLjava/lang/String;)I

    .line 500
    :cond_c9c
    :goto_c9c
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->D()V

    goto/16 :goto_c4

    .line 496
    :cond_cab
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->e()[J

    move-result-object v0

    if-eqz v0, :cond_c9c

    .line 497
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/ab;->e()[J

    move-result-object v1

    const-string v4, "Subscribe last set"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I

    goto :goto_c9c

    .line 505
    :cond_ce3
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;

    if-eqz v0, :cond_ed5

    .line 506
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;

    .line 507
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_e1c

    .line 508
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->e()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_de1

    .line 509
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->e()J

    move-result-wide v2

    .line 510
    const-string v0, ""

    .line 511
    const-wide/32 v4, 0x15180

    div-long v4, v2, v4

    .line 512
    const-wide/32 v6, 0x15180

    mul-long/2addr v6, v4

    sub-long/2addr v2, v6

    .line 513
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_d30

    .line 514
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 515
    :cond_d30
    const-wide/16 v4, 0xe10

    div-long v4, v2, v4

    .line 516
    const-wide/16 v6, 0xe10

    mul-long/2addr v6, v4

    sub-long/2addr v2, v6

    .line 517
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_d55

    .line 518
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "h"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 519
    :cond_d55
    const-wide/16 v4, 0x3c

    div-long v4, v2, v4

    .line 520
    const-wide/16 v6, 0x3c

    mul-long/2addr v6, v4

    sub-long/2addr v2, v6

    .line 521
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_d7a

    .line 522
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "m"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 524
    :cond_d7a
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_39b2

    .line 525
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 527
    :goto_d98
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v2, "messages.ban.text"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.ban.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/teamspeak/ts3client/jni/h;->aC:Lcom/teamspeak/ts3client/jni/h;

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V

    .line 530
    :goto_ddc
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->m:Z

    goto/16 :goto_c4

    .line 529
    :cond_de1
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "messages.ban.text.permanent"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.ban.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/teamspeak/ts3client/jni/h;->aC:Lcom/teamspeak/ts3client/jni/h;

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V

    goto :goto_ddc

    .line 533
    :cond_e1c
    const-string v0, "event.client.ban"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 534
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->d()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_e8d

    .line 535
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->an:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, ""

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 536
    :cond_e8d
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->e(I)V

    .line 537
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    .line 538
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientBanFromServer;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    goto/16 :goto_c4

    .line 544
    :cond_ed5
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ChannelSubscribeFinished;

    if-eqz v0, :cond_f09

    .line 545
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ChannelSubscribeFinished;

    .line 546
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelSubscribeFinished;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_ef0

    .line 547
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->n()V

    .line 549
    :cond_ef0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->o:Z

    .line 550
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    if-eqz v0, :cond_efa

    .line 551
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    .line 553
    :cond_efa
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    goto/16 :goto_c4

    .line 556
    :cond_f09
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ChannelSubscribe;

    if-eqz v0, :cond_f2b

    .line 557
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ChannelSubscribe;

    .line 558
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelSubscribe;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->c(Z)V

    goto/16 :goto_c4

    .line 561
    :cond_f2b
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ChannelUnsubscribe;

    if-eqz v0, :cond_fa2

    .line 562
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ChannelUnsubscribe;

    .line 563
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelUnsubscribe;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->c(Z)V

    .line 564
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelUnsubscribe;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->j()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f69
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f87

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 565
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    goto :goto_f69

    .line 567
    :cond_f87
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelUnsubscribe;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->a()V

    goto/16 :goto_c4

    .line 570
    :cond_fa2
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ChannelUnsubscribeFinished;

    if-eqz v0, :cond_fd6

    .line 571
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ChannelUnsubscribeFinished;

    .line 572
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelUnsubscribeFinished;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_fbd

    .line 573
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->n()V

    .line 575
    :cond_fbd
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    if-eqz v0, :cond_c4

    .line 576
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->e()[J

    move-result-object v0

    if-nez v0, :cond_c4

    .line 580
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->h:Z

    goto/16 :goto_c4

    .line 588
    :cond_fd6
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ChannelMove;

    if-eqz v0, :cond_128e

    .line 589
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ChannelMove;

    .line 590
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->b(Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    .line 591
    if-eqz v0, :cond_101f

    .line 592
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/a;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/a;->a(J)V

    .line 594
    :cond_101f
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/a;->b(J)V

    .line 595
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1052
    :goto_1052
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10a2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 596
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->g()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->d()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_1052

    .line 597
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v8

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->b()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JJLcom/teamspeak/ts3client/jni/c;)J

    move-result-wide v0

    invoke-virtual {v8, v0, v1}, Lcom/teamspeak/ts3client/data/a;->a(J)V

    goto :goto_1052

    .line 601
    :cond_10a2
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->d()V

    .line 603
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    .line 604
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_117f

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->b()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_117f

    .line 605
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->B:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    const-string v3, ""

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 608
    :cond_1117
    :goto_1117
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 609
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->b()I

    move-result v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v2

    if-ne v1, v2, :cond_120c

    .line 610
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/a;->g()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_11da

    .line 611
    const-string v1, "event.channel.moved.self"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_c4

    .line 606
    :cond_117f
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1117

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->b()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-eq v0, v1, :cond_1117

    .line 607
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->C:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    const-string v3, ""

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_1117

    .line 613
    :cond_11da
    const-string v1, "event.channel.moved.self"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->g()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_c4

    .line 615
    :cond_120c
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/a;->g()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1255

    .line 616
    const-string v1, "event.channel.moved"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_c4

    .line 618
    :cond_1255
    const-string v1, "event.channel.moved"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelMove;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->g()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_c4

    .line 622
    :cond_128e
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;

    if-eqz v0, :cond_17b3

    .line 623
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;

    .line 624
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->a:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JJLcom/teamspeak/ts3client/jni/c;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->a(Ljava/lang/String;)V

    .line 625
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->C:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->b(I)V

    .line 626
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->e:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->c(I)V

    .line 627
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->l:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1648

    const/4 v0, 0x1

    .line 628
    :goto_135b
    if-eqz v0, :cond_1392

    .line 629
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/b;->b()Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->a(Z)V

    .line 630
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->b(Lcom/teamspeak/ts3client/data/a;)V

    .line 632
    :cond_1392
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/a;->a(Z)V

    .line 633
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->g:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->a(I)V

    .line 634
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->F:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JJLcom/teamspeak/ts3client/jni/c;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/a;->c(J)V

    .line 635
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v7

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->G:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_164b

    const/4 v0, 0x1

    :goto_1451
    invoke-virtual {v7, v0}, Lcom/teamspeak/ts3client/data/a;->b(Z)V

    .line 636
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->m:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v0

    .line 637
    const/4 v1, 0x1

    if-ne v0, v1, :cond_164e

    .line 638
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->d(Z)V

    .line 642
    :goto_148f
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->c()I

    move-result v0

    .line 644
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JJLcom/teamspeak/ts3client/jni/c;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 646
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1572

    .line 647
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;I)Ljava/lang/Long;

    move-result-object v2

    .line 648
    if-eqz v2, :cond_1530

    .line 649
    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->f()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/teamspeak/ts3client/data/a;->a(J)V

    .line 651
    :cond_1530
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;I)Ljava/lang/Long;

    move-result-object v0

    .line 652
    if-eqz v0, :cond_1555

    .line 653
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/a;->a(J)V

    .line 655
    :cond_1555
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/a;->a(J)V

    .line 657
    :cond_1572
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    .line 658
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_15b2

    .line 659
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->b()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Ljava/lang/String;)V

    .line 660
    :cond_15b2
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_166a

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->b()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_166a

    .line 661
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->w:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 668
    :cond_160d
    :goto_160d
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->b()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_1781

    .line 669
    const-string v0, "event.channel.edit.self"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_c4

    .line 627
    :cond_1648
    const/4 v0, 0x0

    goto/16 :goto_135b

    .line 635
    :cond_164b
    const/4 v0, 0x0

    goto/16 :goto_1451

    .line 640
    :cond_164e
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->d(Z)V

    goto/16 :goto_148f

    .line 662
    :cond_166a
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_16c7

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->b()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-eq v0, v1, :cond_16c7

    .line 663
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->x:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_160d

    .line 664
    :cond_16c7
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1724

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->b()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_1724

    .line 665
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->y:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_160d

    .line 666
    :cond_1724
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_160d

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->b()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-eq v0, v1, :cond_160d

    .line 667
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->z:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_160d

    .line 671
    :cond_1781
    const-string v0, "event.channel.edit"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateChannelEdited;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_c4

    .line 674
    :cond_17b3
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/DelChannel;

    if-eqz v0, :cond_1903

    .line 675
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/DelChannel;

    .line 676
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->b(Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    .line 677
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->b()I

    move-result v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v2

    if-ne v1, v2, :cond_1888

    .line 678
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v1

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->t:Lcom/teamspeak/ts3client/jni/h;

    new-instance v3, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 679
    const-string v1, "event.channel.deleted.self"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 684
    :cond_1841
    :goto_1841
    if-eqz v0, :cond_186e

    .line 685
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/a;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/a;->a(J)V

    .line 686
    :cond_186e
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->c(Ljava/lang/Long;)V

    .line 687
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    goto/16 :goto_c4

    .line 680
    :cond_1888
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->b()I

    move-result v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v2

    if-eq v1, v2, :cond_1841

    .line 681
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v1

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->u:Lcom/teamspeak/ts3client/jni/h;

    new-instance v3, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 682
    const-string v1, "event.channel.deleted"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/DelChannel;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1841

    .line 690
    :cond_1903
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;

    if-eqz v0, :cond_1ebb

    .line 691
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;

    .line 693
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1c88

    .line 694
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->e()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v1

    if-ne v0, v1, :cond_1c56

    .line 695
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v1

    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->b:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    .line 696
    new-instance v0, Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V

    .line 697
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/d;->a(Lcom/teamspeak/ts3client/data/c;)V

    .line 703
    :cond_1951
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1979

    .line 704
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/c;->b(I)V

    .line 705
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/c;->a(I)V

    .line 707
    :cond_1979
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->d()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(IJ)Z

    move-result v1

    if-eqz v1, :cond_19b9

    .line 708
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->e(I)V

    .line 709
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 712
    :cond_19b9
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1c75

    .line 713
    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->e()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    if-eq v1, v2, :cond_19d3

    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->e()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    if-ne v1, v2, :cond_1c62

    .line 714
    :cond_19d3
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->d(I)V

    .line 715
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 731
    :cond_1a0d
    :goto_1a0d
    const/4 v1, 0x0

    .line 732
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v2

    if-eqz v2, :cond_1a1c

    .line 733
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/c/a;->a()I

    move-result v1

    .line 734
    :cond_1a1c
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1d04

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v3

    if-ne v2, v3, :cond_1d04

    .line 735
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->az:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 745
    :cond_1a76
    :goto_1a76
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v2

    if-ne v1, v2, :cond_1e65

    .line 746
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    .line 747
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/ab;->b(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_1ad8

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->g()Z

    move-result v0

    if-nez v0, :cond_1ad8

    .line 748
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v4

    const/4 v1, 0x2

    new-array v1, v1, [J

    const/4 v6, 0x0

    aput-wide v2, v1, v6

    const/4 v6, 0x1

    const-wide/16 v8, 0x0

    aput-wide v8, v1, v6

    const-string v6, "Unsubscribe old Channel"

    invoke-virtual {v0, v4, v5, v1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I

    .line 749
    :cond_1ad8
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/teamspeak/ts3client/data/e;->a(J)V

    .line 750
    const-string v0, "event.client.movemoved.self"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 751
    const-string v0, "event.client.movemoved.self.info"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Ljava/lang/String;)V

    .line 752
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->b()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Ljava/lang/String;)V

    .line 753
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/data/a;)V

    .line 754
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->a()V

    .line 755
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/data/a;)V

    .line 756
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->c(Z)V

    .line 757
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->h()V

    .line 759
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/teamspeak/ts3client/data/ab;->b(J)V

    .line 760
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e54

    .line 761
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v1

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/ab;->d(Ljava/lang/String;)V

    .line 764
    :goto_1c3d
    invoke-direct {p0, v2, v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(J)V

    .line 765
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(J)V

    .line 769
    :goto_1c47
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    goto/16 :goto_c4

    .line 699
    :cond_1c56
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 700
    if-nez v0, :cond_1951

    goto/16 :goto_c4

    .line 718
    :cond_1c62
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    goto/16 :goto_1a0d

    .line 720
    :cond_1c75
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    goto/16 :goto_1a0d

    .line 722
    :cond_1c88
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v1

    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->b:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    .line 723
    new-instance v0, Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V

    .line 724
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1a0d

    .line 725
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/d;->a(Lcom/teamspeak/ts3client/data/c;)V

    .line 726
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->d(I)V

    .line 727
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1a0d

    .line 736
    :cond_1d04
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1d58

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->e()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_1d58

    .line 737
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->U:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_1a76

    .line 738
    :cond_1d58
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1dac

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->e()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_1dac

    .line 739
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->V:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_1a76

    .line 740
    :cond_1dac
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->d()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1e00

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->e()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_1e00

    .line 741
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->W:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_1a76

    .line 742
    :cond_1e00
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->d()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1a76

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->e()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_1a76

    .line 743
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->X:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_1a76

    .line 763
    :cond_1e54
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/ab;->d(Ljava/lang/String;)V

    goto/16 :goto_1c3d

    .line 767
    :cond_1e65
    const-string v1, "event.client.movemoved"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveMoved;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1c47

    .line 772
    :cond_1ebb
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ClientMove;

    if-eqz v0, :cond_26f9

    .line 773
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ClientMove;

    .line 775
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_23cc

    .line 776
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v1

    if-ne v0, v1, :cond_235f

    .line 777
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v1

    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->b:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    .line 778
    new-instance v0, Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V

    .line 779
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/d;->a(Lcom/teamspeak/ts3client/data/c;)V

    .line 791
    :cond_1f09
    :goto_1f09
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f31

    .line 792
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/c;->b(I)V

    .line 793
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/c;->a(I)V

    .line 795
    :cond_1f31
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(IJ)Z

    move-result v1

    if-eqz v1, :cond_1f71

    .line 796
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->e(I)V

    .line 797
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 799
    :cond_1f71
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_23b8

    .line 800
    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    if-eq v1, v2, :cond_1f8b

    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    if-ne v1, v2, :cond_23a4

    .line 801
    :cond_1f8b
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->d(I)V

    .line 802
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 820
    :goto_1fc6
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v2

    if-ne v0, v2, :cond_21b3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_21b3

    .line 821
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    .line 822
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/ab;->b(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_2032

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->g()Z

    move-result v0

    if-nez v0, :cond_2032

    .line 823
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v4

    const/4 v6, 0x2

    new-array v6, v6, [J

    const/4 v7, 0x0

    aput-wide v2, v6, v7

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    aput-wide v8, v6, v7

    const-string v7, "Unsubscribe old Channel"

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I

    .line 824
    :cond_2032
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/teamspeak/ts3client/data/e;->a(J)V

    .line 825
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->b()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/chat/a;->a(Ljava/lang/String;)V

    .line 826
    const-string v0, "event.client.movemoved.self.info"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Ljava/lang/String;)V

    .line 827
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/data/a;)V

    .line 828
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->a()V

    .line 829
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/data/a;)V

    .line 830
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/a;->c(Z)V

    .line 831
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->h()V

    .line 833
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/teamspeak/ts3client/data/ab;->b(J)V

    .line 834
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_245d

    .line 835
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v4

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/teamspeak/ts3client/data/ab;->d(Ljava/lang/String;)V

    .line 838
    :goto_2167
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->i()Z

    move-result v0

    if-eqz v0, :cond_21a9

    .line 839
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/a;->e(Z)V

    .line 840
    :cond_21a9
    invoke-direct {p0, v2, v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(J)V

    .line 841
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(J)V

    .line 843
    :cond_21b3
    const/4 v0, 0x0

    .line 844
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v2

    if-eqz v2, :cond_21c2

    .line 845
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/c/a;->a()I

    move-result v0

    .line 846
    :cond_21c2
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v3

    if-ne v2, v3, :cond_225a

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_225a

    .line 847
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->ay:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 848
    const-string v2, "event.client.move.self"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 850
    :cond_225a
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v3

    if-eq v2, v3, :cond_2350

    .line 851
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->e()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v3

    sget-object v6, Lcom/teamspeak/ts3client/jni/d;->O:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JILcom/teamspeak/ts3client/jni/d;)I

    move-result v2

    if-nez v2, :cond_2350

    .line 852
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_24db

    .line 853
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_22d6

    .line 854
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->O:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 855
    :cond_22d6
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_2350

    .line 857
    :try_start_22de
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_246e

    .line 858
    const-string v2, "event.client.move.a"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 859
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->I:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v1, v0, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
    :try_end_2350
    .catch Ljava/lang/Exception; {:try_start_22de .. :try_end_2350} :catch_24d8

    .line 904
    :cond_2350
    :goto_2350
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    goto/16 :goto_c4

    .line 782
    :cond_235f
    :try_start_235f
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;
    :try_end_2366
    .catch Ljava/lang/Exception; {:try_start_235f .. :try_end_2366} :catch_236b

    move-result-object v0

    .line 783
    if-nez v0, :cond_1f09

    goto/16 :goto_c4

    .line 786
    :catch_236b
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v1

    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->b:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    .line 787
    new-instance v0, Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V

    .line 788
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/d;->a(Lcom/teamspeak/ts3client/data/c;)V

    goto/16 :goto_1f09

    .line 805
    :cond_23a4
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    move-object v1, v0

    goto/16 :goto_1fc6

    .line 807
    :cond_23b8
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    move-object v1, v0

    goto/16 :goto_1fc6

    .line 809
    :cond_23cc
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v1

    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->b:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    .line 810
    new-instance v0, Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V

    .line 811
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2449

    .line 812
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/d;->a(Lcom/teamspeak/ts3client/data/c;)V

    .line 813
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->d(I)V

    .line 814
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    goto/16 :goto_1fc6

    .line 817
    :cond_2449
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    move-object v1, v0

    goto/16 :goto_1fc6

    .line 837
    :cond_245d
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    const-string v4, ""

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/ab;->d(Ljava/lang/String;)V

    goto/16 :goto_2167

    .line 861
    :cond_246e
    :try_start_246e
    const-string v2, "event.client.move.b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 862
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->N:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v1, v0, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
    :try_end_24d6
    .catch Ljava/lang/Exception; {:try_start_246e .. :try_end_24d6} :catch_24d8

    goto/16 :goto_2350

    .line 865
    :catch_24d8
    move-exception v0

    goto/16 :goto_2350

    .line 869
    :cond_24db
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_264a

    .line 871
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_257a

    .line 873
    :try_start_24f5
    const-string v2, "event.client.move.c"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V
    :try_end_2542
    .catch Ljava/lang/Exception; {:try_start_24f5 .. :try_end_2542} :catch_39ac

    .line 876
    :goto_2542
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->Q:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 878
    :cond_257a
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_2350

    .line 880
    :try_start_2582
    const-string v2, "event.client.move.d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V
    :try_end_25b2
    .catch Ljava/lang/Exception; {:try_start_2582 .. :try_end_25b2} :catch_39a9

    .line 883
    :goto_25b2
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2610

    .line 884
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->K:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 885
    const-string v0, "event.client.disconnected"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_2350

    .line 887
    :cond_2610
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->P:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v1, v0, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_2350

    .line 892
    :cond_264a
    :try_start_264a
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v0

    sget-object v2, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    if-ne v0, v2, :cond_269f

    .line 893
    const-string v0, "event.client.move.e"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 895
    :cond_269f
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v0

    sget-object v2, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    if-ne v0, v2, :cond_2350

    .line 896
    const-string v0, "event.client.move.f"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V
    :try_end_26f4
    .catch Ljava/lang/Exception; {:try_start_264a .. :try_end_26f4} :catch_26f6

    goto/16 :goto_2350

    :catch_26f6
    move-exception v0

    goto/16 :goto_2350

    .line 907
    :cond_26f9
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;

    if-eqz v0, :cond_27d5

    .line 908
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;

    .line 909
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 910
    if-eqz v1, :cond_c4

    .line 913
    :try_start_2709
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->c()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2723

    .line 914
    const-string v0, "event.client.dropped"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 915
    :cond_2723
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/a;->e(I)V

    .line 916
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 917
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v0

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    .line 918
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V
    :try_end_2773
    .catch Ljava/lang/Exception; {:try_start_2709 .. :try_end_2773} :catch_39a6

    .line 921
    :goto_2773
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->b()J

    move-result-wide v2

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_c4

    .line 922
    const/4 v0, 0x0

    .line 923
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v2

    if-eqz v2, :cond_2794

    .line 924
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/c/a;->a()I

    move-result v0

    .line 925
    :cond_2794
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->c()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_c4

    .line 926
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->M:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v1, v0, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_c4

    .line 930
    :cond_27d5
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;

    if-eqz v0, :cond_2909

    .line 931
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;

    .line 932
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    if-eqz v0, :cond_c4

    .line 934
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->b()I

    move-result v0

    if-nez v0, :cond_289a

    .line 935
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 936
    if-eqz v0, :cond_27f6

    .line 937
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/c;->a(I)V

    .line 938
    :cond_27f6
    sget-boolean v0, Lcom/teamspeak/ts3client/ConnectionBackground;->k:Z

    if-eqz v0, :cond_284e

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_284e

    .line 939
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2869

    .line 940
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    const v1, 0x7f02009e

    invoke-virtual {v0, v1}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    .line 941
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    const v1, 0x7f02009e

    const/high16 v2, 0x42400000    # 48.0f

    const/high16 v3, 0x42400000    # 48.0f

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/dm;->a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/dm;

    .line 942
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    .line 943
    const v1, -0xffff01

    iput v1, v0, Landroid/app/Notification;->ledARGB:I

    .line 944
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x3

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 945
    const/4 v1, 0x0

    iput v1, v0, Landroid/app/Notification;->ledOffMS:I

    .line 946
    const/4 v1, 0x1

    iput v1, v0, Landroid/app/Notification;->ledOnMS:I

    .line 947
    sput-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    .line 948
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->j()Landroid/app/NotificationManager;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 973
    :cond_284e
    :goto_284e
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    if-eqz v0, :cond_c4

    .line 974
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    goto/16 :goto_c4

    .line 950
    :cond_2869
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    const v1, 0x7f020076

    invoke-virtual {v0, v1}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    .line 951
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020076

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/dm;->a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/dm;

    .line 952
    sget-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->l:Landroid/support/v4/app/dm;

    invoke-virtual {v0}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    .line 953
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->j()Landroid/app/NotificationManager;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Lcom/teamspeak/ts3client/ConnectionBackground;->j:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_284e

    .line 957
    :cond_289a
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_28fb

    .line 958
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->q:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 959
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->j:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, ""

    const-string v6, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 966
    :goto_28d4
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/d;->a(I)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 968
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 969
    if-eqz v0, :cond_284e

    .line 970
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/c;->b(I)V

    goto/16 :goto_284e

    .line 961
    :cond_28fb
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->q:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_28d4

    .line 977
    :cond_2909
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;

    if-eqz v0, :cond_2b2a

    .line 978
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;

    .line 979
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->a:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JJLcom/teamspeak/ts3client/jni/c;)Ljava/lang/String;

    move-result-object v8

    .line 980
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JJLcom/teamspeak/ts3client/jni/c;)J

    move-result-wide v6

    .line 981
    new-instance v0, Lcom/teamspeak/ts3client/data/a;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->b()J

    move-result-wide v4

    move-object v1, v8

    invoke-direct/range {v0 .. v7}, Lcom/teamspeak/ts3client/data/a;-><init>(Ljava/lang/String;JJJ)V

    .line 982
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->C:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->b(I)V

    .line 983
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->e:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->c(I)V

    .line 984
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->l:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2b03

    const/4 v1, 0x1

    .line 985
    :goto_29bd
    if-eqz v1, :cond_29de

    .line 986
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/b;->b()Lcom/teamspeak/ts3client/data/a;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/a;->a(Z)V

    .line 987
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/b;->b(Lcom/teamspeak/ts3client/data/a;)V

    .line 989
    :cond_29de
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->a(Z)V

    .line 990
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->g:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->a(I)V

    .line 991
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->G:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2b06

    const/4 v1, 0x1

    :goto_2a24
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->b(Z)V

    .line 992
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->m:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;)I

    move-result v1

    .line 993
    const/4 v2, 0x1

    if-ne v1, v2, :cond_2a4c

    .line 994
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->d(Z)V

    .line 996
    :cond_2a4c
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/b;->a(Lcom/teamspeak/ts3client/data/a;)V

    .line 997
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->g()Z

    move-result v0

    if-eqz v0, :cond_2a9d

    .line 998
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 999
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Subscribe "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->a()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I

    .line 1001
    :cond_2a9d
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2ab3
    :goto_2ab3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b09

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 1002
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->g()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->b()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_2ab3

    .line 1003
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v8

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->b()J

    move-result-wide v4

    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JJLcom/teamspeak/ts3client/jni/c;)J

    move-result-wide v0

    invoke-virtual {v8, v0, v1}, Lcom/teamspeak/ts3client/data/a;->a(J)V

    goto :goto_2ab3

    .line 984
    :cond_2b03
    const/4 v1, 0x0

    goto/16 :goto_29bd

    .line 991
    :cond_2b06
    const/4 v1, 0x0

    goto/16 :goto_2a24

    .line 1007
    :cond_2b09
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/NewChannelCreated;->c()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_2b25

    .line 1008
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->r:Lcom/teamspeak/ts3client/jni/h;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1009
    :cond_2b25
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    goto/16 :goto_c4

    .line 1012
    :cond_2b2a
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/UpdateClient;

    if-eqz v0, :cond_2b6b

    .line 1013
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/UpdateClient;

    .line 1014
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateClient;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(I)V

    .line 1015
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    if-eqz v0, :cond_2b5c

    .line 1016
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/UpdateClient;->a()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_2b5c

    .line 1017
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->D()V

    .line 1018
    :cond_2b5c
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    goto/16 :goto_c4

    .line 1021
    :cond_2b6b
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;

    if-eqz v0, :cond_2fcb

    .line 1022
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;

    .line 1024
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v1

    if-eq v0, v1, :cond_2b81

    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v1

    if-ne v0, v1, :cond_2dc9

    .line 1025
    :cond_2b81
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a()I

    move-result v1

    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->b:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    .line 1026
    new-instance v0, Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;)V

    .line 1027
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/d;->a(Lcom/teamspeak/ts3client/data/c;)V

    .line 1034
    :cond_2bb7
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->e(I)V

    .line 1035
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1036
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2c3d

    .line 1037
    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    if-eq v1, v2, :cond_2c03

    sget-object v1, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    if-ne v1, v2, :cond_2dd5

    .line 1038
    :cond_2c03
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/a;->d(I)V

    .line 1039
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1043
    :cond_2c3d
    :goto_2c3d
    const/4 v1, 0x0

    .line 1044
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v2

    if-eqz v2, :cond_2c4c

    .line 1045
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/c/a;->a()I

    move-result v1

    .line 1046
    :cond_2c4c
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a()I

    move-result v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v3

    if-ne v2, v3, :cond_2e20

    .line 1047
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    .line 1048
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/ab;->b(Ljava/lang/Long;)Z

    move-result v0

    if-nez v0, :cond_2cae

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->g()Z

    move-result v0

    if-nez v0, :cond_2cae

    .line 1049
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v4

    const/4 v6, 0x2

    new-array v6, v6, [J

    const/4 v7, 0x0

    aput-wide v2, v6, v7

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    aput-wide v8, v6, v7

    const-string v7, "Unsubscribe old Channel"

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I

    .line 1050
    :cond_2cae
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/teamspeak/ts3client/data/e;->a(J)V

    .line 1052
    :try_start_2cbb
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->b()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/chat/a;->a(Ljava/lang/String;)V
    :try_end_2cec
    .catch Ljava/lang/Exception; {:try_start_2cbb .. :try_end_2cec} :catch_39a3

    .line 1055
    :goto_2cec
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/data/a;)V

    .line 1056
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->a()V

    .line 1057
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/data/a;)V

    .line 1058
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(J)V

    .line 1059
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/a;->c(Z)V

    .line 1060
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->aB:Lcom/teamspeak/ts3client/jni/h;

    new-instance v3, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v1, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1061
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2de8

    .line 1062
    const-string v0, "event.client.kicked.self"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 1085
    :goto_2dc4
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b()V

    goto/16 :goto_c4

    .line 1029
    :cond_2dc9
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1030
    if-nez v0, :cond_2bb7

    goto/16 :goto_c4

    .line 1042
    :cond_2dd5
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d;->c(I)V

    goto/16 :goto_2c3d

    .line 1064
    :cond_2de8
    const-string v0, "event.client.kicked.self.reason"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto :goto_2dc4

    .line 1066
    :cond_2e20
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2ef7

    .line 1067
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_2e72

    .line 1068
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->af:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1069
    :cond_2e72
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_2eb2

    .line 1070
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->ag:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1079
    :cond_2eb2
    :goto_2eb2
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2f8b

    .line 1080
    const-string v1, "event.client.kicked"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_2dc4

    .line 1072
    :cond_2ef7
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2eb2

    .line 1073
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_2f49

    .line 1074
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->ad:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1075
    :cond_2f49
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f()Lcom/teamspeak/ts3client/jni/j;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    if-ne v2, v3, :cond_2eb2

    .line 1076
    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v2

    sget-object v3, Lcom/teamspeak/ts3client/jni/h;->ae:Lcom/teamspeak/ts3client/jni/h;

    new-instance v4, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v6

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_2eb2

    .line 1082
    :cond_2f8b
    const-string v1, "event.client.kicked.reason"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    goto/16 :goto_2dc4

    .line 1088
    :cond_2fcb
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;

    if-eqz v0, :cond_315d

    .line 1089
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;

    .line 1090
    const/4 v0, 0x0

    .line 1091
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->a()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v9

    .line 1092
    if-eqz v9, :cond_c4

    .line 1094
    invoke-virtual {v9}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v1

    if-eqz v1, :cond_39af

    .line 1095
    invoke-virtual {v9}, Lcom/teamspeak/ts3client/data/c;->e()Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/c/a;->a()I

    move-result v0

    move v8, v0

    .line 1096
    :goto_2feb
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->a()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_30c7

    .line 1097
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.kicked.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/teamspeak/ts3client/jni/h;->aA:Lcom/teamspeak/ts3client/jni/h;

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V

    .line 1098
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->m:Z

    .line 1099
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aA:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v8, v4, v5}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1106
    :cond_3064
    :goto_3064
    const-string v0, "event.client.kick"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->a()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 1107
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    invoke-virtual {v9}, Lcom/teamspeak/ts3client/data/c;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->e(I)V

    .line 1108
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->d()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    goto/16 :goto_c4

    .line 1101
    :cond_30c7
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->d()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_3111

    .line 1102
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->al:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v9}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v8, v4, v5}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1103
    :cond_3111
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->d()J

    move-result-wide v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->f()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3064

    .line 1104
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->ak:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {v9}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ClientKickFromServer;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/a;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v8, v4, v5}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_3064

    .line 1112
    :cond_315d
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;

    if-eqz v0, :cond_3187

    .line 1113
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;

    .line 1114
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->l()Lcom/teamspeak/ts3client/data/g/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->a()Lcom/teamspeak/ts3client/data/g/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/g/b;->a(Lcom/teamspeak/ts3client/data/g/a;)V

    .line 1115
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->g()Lcom/teamspeak/ts3client/data/c/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupList;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/c/d;->a(J)Lcom/teamspeak/ts3client/data/c/c;

    goto/16 :goto_c4

    .line 1118
    :cond_3187
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ChannelGroupList;

    if-eqz v0, :cond_31b1

    .line 1119
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ChannelGroupList;

    .line 1120
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->b()Lcom/teamspeak/ts3client/data/a/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ChannelGroupList;->a()Lcom/teamspeak/ts3client/data/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a/b;->a(Lcom/teamspeak/ts3client/data/a/a;)V

    .line 1121
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->g()Lcom/teamspeak/ts3client/data/c/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ChannelGroupList;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/data/c/d;->a(J)Lcom/teamspeak/ts3client/data/c/c;

    goto/16 :goto_c4

    .line 1124
    :cond_31b1
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;

    if-eqz v0, :cond_3277

    .line 1125
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;

    .line 1127
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1128
    if-eqz v0, :cond_c4

    .line 1130
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->b()I

    move-result v4

    sget-object v5, Lcom/teamspeak/ts3client/jni/d;->H:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/c;->a(Ljava/lang/String;)V

    .line 1131
    const-string v0, "event.group.channel.assigned"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->b()Lcom/teamspeak/ts3client/data/a/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/teamspeak/ts3client/data/a/b;->b(J)Lcom/teamspeak/ts3client/data/a/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/a/a;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->b()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/c;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 1132
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->b()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_3268

    .line 1133
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->c()I

    move-result v0

    if-eqz v0, :cond_3268

    .line 1134
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aK:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->b()Lcom/teamspeak/ts3client/data/a/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->a()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/teamspeak/ts3client/data/a/b;->b(J)Lcom/teamspeak/ts3client/data/a/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/a/a;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1135
    :cond_3268
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    goto/16 :goto_c4

    .line 1138
    :cond_3277
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientPoke;

    if-eqz v0, :cond_3301

    .line 1139
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientPoke;

    .line 1140
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/c;->a()Lcom/teamspeak/ts3client/data/b/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientPoke;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b/c;->a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    .line 1141
    if-eqz v0, :cond_3291

    .line 1142
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/c/a;->b()Z

    move-result v0

    if-nez v0, :cond_c4

    .line 1144
    :cond_3291
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientPoke;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "messages.poke.info"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientPoke;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.close"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 1145
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->d:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientPoke;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, ""

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1146
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->f()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "vibrate_poke"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 1147
    invoke-static {}, Lcom/teamspeak/ts3client/data/ai;->a()Lcom/teamspeak/ts3client/data/ai;

    sget-object v0, Lcom/teamspeak/ts3client/data/aj;->a:[J

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/ai;->a([J)V

    goto/16 :goto_c4

    .line 1150
    :cond_3301
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/TextMessage;

    if-eqz v0, :cond_34fb

    .line 1151
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/TextMessage;

    .line 1152
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/c;->a()Lcom/teamspeak/ts3client/data/b/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b/c;->a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v0

    .line 1153
    if-eqz v0, :cond_3321

    .line 1154
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/c/a;->c()Z

    move-result v1

    if-nez v1, :cond_c4

    .line 1156
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/c/a;->d()Z

    move-result v0

    if-nez v0, :cond_c4

    .line 1159
    :cond_3321
    const/4 v0, 0x0

    .line 1160
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->a()I

    move-result v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v2

    if-ne v1, v2, :cond_3333

    .line 1161
    const/4 v0, 0x1

    .line 1162
    :cond_3333
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c4

    .line 1164
    new-instance v1, Lcom/teamspeak/ts3client/chat/y;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/chat/y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1165
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->e()I

    move-result v2

    packed-switch v2, :pswitch_data_3a32

    .line 1209
    :cond_335b
    :goto_335b
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/chat/y;->a()Z

    move-result v0

    if-nez v0, :cond_c4

    .line 1210
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->e(Z)V

    goto/16 :goto_c4

    .line 1167
    :pswitch_3371
    if-nez v0, :cond_335b

    .line 1168
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/chat/d;->c(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_33ef

    .line 1169
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/chat/d;->d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    .line 1170
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 1171
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->aM:Lcom/teamspeak/ts3client/jni/h;

    new-instance v3, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, ""

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 1172
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->f()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "vibrate_chat"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_33cc

    .line 1173
    invoke-static {}, Lcom/teamspeak/ts3client/data/ai;->a()Lcom/teamspeak/ts3client/data/ai;

    sget-object v0, Lcom/teamspeak/ts3client/data/aj;->c:[J

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/ai;->a([J)V

    .line 1195
    :cond_33cc
    :goto_33cc
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->aM:Lcom/teamspeak/ts3client/jni/h;

    new-instance v3, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, ""

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_335b

    .line 1176
    :cond_33ef
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->q()Lcom/teamspeak/ts3client/data/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/d;->a(I)Z

    move-result v0

    if-eqz v0, :cond_343a

    .line 1177
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1178
    new-instance v2, Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/teamspeak/ts3client/chat/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V

    .line 1179
    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 1180
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/chat/d;->a(Lcom/teamspeak/ts3client/chat/a;)V

    .line 1188
    :goto_3422
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->f()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "vibrate_chatnew"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3466

    .line 1189
    invoke-static {}, Lcom/teamspeak/ts3client/data/ai;->a()Lcom/teamspeak/ts3client/data/ai;

    sget-object v0, Lcom/teamspeak/ts3client/data/aj;->b:[J

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/ai;->a([J)V

    goto :goto_33cc

    .line 1182
    :cond_343a
    new-instance v0, Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->a()I

    move-result v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    const/4 v5, 0x1

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/data/c;-><init>(Ljava/lang/String;ILcom/teamspeak/ts3client/Ts3Application;Z)V

    .line 1183
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->g()V

    .line 1184
    new-instance v2, Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/teamspeak/ts3client/chat/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;Z)V

    .line 1185
    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 1186
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/chat/d;->a(Lcom/teamspeak/ts3client/chat/a;)V

    goto :goto_3422

    .line 1191
    :cond_3466
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->f()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "vibrate_chat"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_33cc

    .line 1192
    invoke-static {}, Lcom/teamspeak/ts3client/data/ai;->a()Lcom/teamspeak/ts3client/data/ai;

    sget-object v0, Lcom/teamspeak/ts3client/data/aj;->c:[J

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/ai;->a([J)V

    goto/16 :goto_33cc

    .line 1199
    :pswitch_347f
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->b()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 1200
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->a()I

    move-result v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v2

    if-eq v0, v2, :cond_335b

    .line 1201
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->aO:Lcom/teamspeak/ts3client/jni/h;

    new-instance v3, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, ""

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_335b

    .line 1204
    :pswitch_34bd
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/chat/d;->c()Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    .line 1205
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->a()I

    move-result v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v2

    if-eq v0, v2, :cond_335b

    .line 1206
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v2, Lcom/teamspeak/ts3client/jni/h;->aQ:Lcom/teamspeak/ts3client/jni/h;

    new-instance v3, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/TextMessage;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, ""

    iget-object v7, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v7

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_335b

    .line 1213
    :cond_34fb
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatClosed;

    if-eqz v0, :cond_353d

    .line 1214
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatClosed;

    .line 1215
    new-instance v0, Lcom/teamspeak/ts3client/chat/y;

    const-string v1, ""

    const-string v2, ""

    const-string v3, "event.chat.closed"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/chat/y;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1216
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatClosed;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/chat/d;->c(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_c4

    .line 1217
    invoke-static {}, Lcom/teamspeak/ts3client/chat/d;->a()Lcom/teamspeak/ts3client/chat/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientChatClosed;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/chat/d;->d(Ljava/lang/String;)Lcom/teamspeak/ts3client/chat/a;

    move-result-object v1

    .line 1218
    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/chat/a;->a(Lcom/teamspeak/ts3client/chat/y;)V

    goto/16 :goto_c4

    .line 1222
    :cond_353d
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;

    if-eqz v0, :cond_3582

    .line 1223
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;

    .line 1225
    :try_start_3543
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->a()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->j()Lcom/teamspeak/ts3client/data/d/v;

    move-result-object v1

    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->cw:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    if-ne v0, v1, :cond_355c

    .line 1226
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ConnectionBackground;->f()V
    :try_end_355c
    .catch Ljava/lang/Exception; {:try_start_3543 .. :try_end_355c} :catch_3573

    .line 1230
    :cond_355c
    :goto_355c
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    if-eqz v0, :cond_c4

    .line 1231
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->D()V

    goto/16 :goto_c4

    .line 1227
    :catch_3573
    move-exception v0

    .line 1228
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->e()Ljava/util/logging/Logger;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_355c

    .line 1234
    :cond_3582
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;

    if-eqz v0, :cond_362d

    .line 1235
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;

    .line 1236
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1237
    if-eqz v0, :cond_c4

    .line 1238
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->a()I

    move-result v4

    sget-object v5, Lcom/teamspeak/ts3client/jni/d;->I:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/c;->c(Ljava/lang/String;)V

    .line 1239
    const-string v0, "event.group.server.assigned"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->l()Lcom/teamspeak/ts3client/data/g/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->c()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/teamspeak/ts3client/data/g/b;->b(J)Lcom/teamspeak/ts3client/data/g/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/g/a;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 1240
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->a()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_c4

    .line 1241
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aG:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->l()Lcom/teamspeak/ts3client/data/g/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientAdded;->c()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/teamspeak/ts3client/data/g/b;->b(J)Lcom/teamspeak/ts3client/data/g/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/g/a;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_c4

    .line 1245
    :cond_362d
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;

    if-eqz v0, :cond_36d8

    .line 1246
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;

    .line 1247
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1248
    if-eqz v0, :cond_c4

    .line 1249
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->k()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;->a()I

    move-result v4

    sget-object v5, Lcom/teamspeak/ts3client/jni/d;->I:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/c;->c(Ljava/lang/String;)V

    .line 1250
    const-string v0, "event.group.server.deleted"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/e;->l()Lcom/teamspeak/ts3client/data/g/b;

    move-result-object v3

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;->c()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/teamspeak/ts3client/data/g/b;->b(J)Lcom/teamspeak/ts3client/data/g/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/data/g/a;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Ljava/lang/String;)V

    .line 1251
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;->a()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->i()I

    move-result v1

    if-ne v0, v1, :cond_c4

    .line 1252
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aI:Lcom/teamspeak/ts3client/jni/h;

    new-instance v2, Lcom/teamspeak/ts3client/a/o;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->l()Lcom/teamspeak/ts3client/data/g/b;

    move-result-object v5

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupClientDeleted;->c()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/teamspeak/ts3client/data/g/b;->b(J)Lcom/teamspeak/ts3client/data/g/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/g/a;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/teamspeak/ts3client/a/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto/16 :goto_c4

    .line 1256
    :cond_36d8
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ChannelPasswordChanged;

    if-eqz v0, :cond_3711

    .line 1257
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ChannelPasswordChanged;

    .line 1258
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->o()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelPasswordChanged;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1259
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/ChannelPasswordChanged;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/a;->e(Z)V

    goto/16 :goto_c4

    .line 1262
    :cond_3711
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/IgnoredWhisper;

    if-eqz v0, :cond_3797

    move-object v0, p1

    .line 1263
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/IgnoredWhisper;

    .line 1265
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->f()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "whisper"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_c4

    .line 1268
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/IgnoredWhisper;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/b/d/bw;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c4

    .line 1270
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/IgnoredWhisper;->b()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/IgnoredWhisper;->a()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "IgnoredWhisper: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/IgnoredWhisper;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestClientUIDfromClientID(JILjava/lang/String;)I

    .line 1271
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/IgnoredWhisper;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/b/d/bw;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3797

    .line 1272
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/IgnoredWhisper;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Lcom/a/b/d/bw;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1275
    :cond_3797
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;

    if-eqz v0, :cond_3833

    move-object v0, p1

    .line 1276
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;

    .line 1278
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->f()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "whisper"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_38bc

    .line 1279
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/b/d/bw;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3833

    .line 1280
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/b/d/bw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1281
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/b/d/bw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1282
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/a/b/d/bw;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1283
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/c;->a()Lcom/teamspeak/ts3client/data/b/c;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b/c;->a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v1

    .line 1284
    if-eqz v1, :cond_381e

    if-eqz v1, :cond_3833

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/c/a;->e()Z

    move-result v1

    if-eqz v1, :cond_3833

    .line 1285
    :cond_381e
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_allowWhispersFrom(JI)I

    .line 1299
    :cond_3833
    :goto_3833
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;

    if-eqz v0, :cond_c4

    .line 1300
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;

    .line 1301
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->a()I

    move-result v0

    if-nez v0, :cond_3966

    .line 1302
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v7

    .line 1303
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/teamspeak/ts3client/data/ab;->c(Ljava/lang/String;)V

    .line 1304
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->c()I

    move-result v0

    invoke-virtual {v7, v0}, Lcom/teamspeak/ts3client/data/ab;->a(I)V

    .line 1305
    new-instance v0, Lcom/teamspeak/ts3client/data/d/h;

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/h;-><init>(Lcom/teamspeak/ts3client/Ts3Application;Ljava/lang/String;)V

    .line 1306
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/ab;->i()I

    move-result v0

    if-lez v0, :cond_393f

    .line 1307
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->c()I

    move-result v2

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/ab;->c()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/ab;->k()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/e;->p()Lcom/teamspeak/ts3client/data/ab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/data/ab;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/ab;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/ab;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/data/e;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c4

    .line 1287
    :cond_38bc
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->f()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "whisper"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3833

    .line 1288
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/b/d/bw;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3833

    .line 1289
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/a/b/d/bw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1290
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->a()Lcom/a/b/d/bw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/a/b/d/bw;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1291
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/c;->a()Lcom/teamspeak/ts3client/data/b/c;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b/c;->a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v1

    .line 1292
    if-eqz v1, :cond_c4

    .line 1294
    if-eqz v1, :cond_3833

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/c/a;->e()Z

    move-result v1

    if-eqz v1, :cond_3833

    .line 1295
    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->h()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/events/rare/ClientUIDfromClientID;->b()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_allowWhispersFrom(JI)I

    goto/16 :goto_3833

    .line 1309
    :cond_393f
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/events/special/TSDNSResolv;->c()I

    move-result v2

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/ab;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/ab;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/ab;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/ab;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/ab;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/data/e;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c4

    .line 1311
    :cond_3966
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 1312
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->c()Lcom/teamspeak/ts3client/data/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->e()Lcom/teamspeak/ts3client/t;

    move-result-object v0

    const-string v1, "tsdns.error.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "tsdns.error"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_c4

    :catch_39a3
    move-exception v0

    goto/16 :goto_2cec

    :catch_39a6
    move-exception v0

    goto/16 :goto_2773

    :catch_39a9
    move-exception v2

    goto/16 :goto_25b2

    :catch_39ac
    move-exception v2

    goto/16 :goto_2542

    :cond_39af
    move v8, v0

    goto/16 :goto_2feb

    :cond_39b2
    move-object v1, v0

    goto/16 :goto_d98

    .line 226
    nop

    :sswitch_data_39b6
    .sparse-switch
        0x0 -> :sswitch_204
        0x200 -> :sswitch_2c8
        0x204 -> :sswitch_326
        0x207 -> :sswitch_3d9
        0x209 -> :sswitch_354
        0x20a -> :sswitch_392
        0x20c -> :sswitch_3d2
        0x300 -> :sswitch_c4
        0x302 -> :sswitch_1f0
        0x303 -> :sswitch_5bf
        0x306 -> :sswitch_5ed
        0x307 -> :sswitch_61b
        0x308 -> :sswitch_649
        0x309 -> :sswitch_677
        0x30a -> :sswitch_6a5
        0x30d -> :sswitch_423
        0x403 -> :sswitch_6d3
        0x404 -> :sswitch_711
        0x501 -> :sswitch_c4
        0x605 -> :sswitch_74f
        0x803 -> :sswitch_c4
        0x806 -> :sswitch_c4
        0xa01 -> :sswitch_7ab
        0xa03 -> :sswitch_1f0
        0xa0a -> :sswitch_77d
        0xd01 -> :sswitch_7d9
        0xf00 -> :sswitch_832
    .end sparse-switch

    .line 383
    :pswitch_data_3a24
    .packed-switch 0x0
        :pswitch_920
        :pswitch_92f
        :pswitch_93e
        :pswitch_94d
        :pswitch_95c
    .end packed-switch

    .line 1165
    :pswitch_data_3a32
    .packed-switch 0x1
        :pswitch_3371
        :pswitch_347f
        :pswitch_34bd
    .end packed-switch
.end method

.method public final a(Lcom/teamspeak/ts3client/tsdns/h;)V
    .registers 12

    .prologue
    const/4 v4, 0x1

    .line 1860
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50565
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1860
    if-nez v0, :cond_8

    .line 1886
    :goto_7
    return-void

    .line 50566
    :cond_8
    iget v0, p1, Lcom/teamspeak/ts3client/tsdns/h;->d:I

    .line 1862
    if-nez v0, :cond_8b

    .line 1863
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50567
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50568
    iget-object v7, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50569
    iget-object v0, p1, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;

    .line 50570
    iput-object v0, v7, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    .line 50572
    iget v0, p1, Lcom/teamspeak/ts3client/tsdns/h;->c:I

    .line 50573
    iput v0, v7, Lcom/teamspeak/ts3client/data/ab;->p:I

    .line 1868
    :try_start_1a
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50575
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50576
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1868
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/r;

    invoke-direct {v1, p0, p1}, Lcom/teamspeak/ts3client/r;-><init>(Lcom/teamspeak/ts3client/ConnectionBackground;Lcom/teamspeak/ts3client/tsdns/h;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_2c} :catch_67

    .line 1878
    :goto_2c
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50578
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50579
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50580
    iget v0, v0, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 1878
    if-lez v0, :cond_74

    .line 1879
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50581
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50582
    iget-object v1, p1, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;

    .line 50583
    iget v2, p1, Lcom/teamspeak/ts3client/tsdns/h;->c:I

    .line 50584
    iget-object v3, v7, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 1879
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50585
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50586
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50587
    iget-wide v8, v5, Lcom/teamspeak/ts3client/data/ab;->q:J

    .line 1879
    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50588
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50589
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50590
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/ab;->r:Ljava/lang/String;

    .line 50591
    iget-object v6, v7, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 50592
    iget-object v7, v7, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    .line 1879
    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/data/e;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 1876
    :catch_67
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50577
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 1876
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Request Error 1"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_2c

    .line 1881
    :cond_74
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50593
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50594
    iget-object v1, p1, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;

    .line 50595
    iget v2, p1, Lcom/teamspeak/ts3client/tsdns/h;->c:I

    .line 50596
    iget-object v3, v7, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 50597
    iget-object v4, v7, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 50598
    iget-object v5, v7, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 50599
    iget-object v6, v7, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 50600
    iget-object v7, v7, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    .line 1881
    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/data/e;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1883
    :cond_8b
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50601
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50602
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1883
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->a()V

    .line 1884
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50603
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50604
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1884
    const-string v1, "tsdns.error.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "tsdns.error"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "button.exit"

    invoke-static {v6}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_7
.end method

.method public final b()V
    .registers 16

    .prologue
    .line 1632
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50495
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1632
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    .line 50496
    iget-object v8, v0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 1633
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v12

    .line 1634
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v13

    .line 1636
    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_88

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 50497
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 1637
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_42

    .line 50498
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 1638
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v12, v1, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_24

    .line 50499
    :cond_42
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 1640
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v13, v1}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_64

    .line 50500
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 1641
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v13, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedMap;

    .line 50501
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 1641
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_24

    .line 50502
    :cond_64
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 1643
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    invoke-interface {v13, v1, v3}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50503
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 1644
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v13, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedMap;

    .line 50504
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 1644
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_24

    .line 1648
    :cond_88
    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v14, v0, [Lcom/teamspeak/ts3client/data/a;

    .line 1649
    new-instance v0, Lcom/teamspeak/ts3client/data/a;

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50505
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1649
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/teamspeak/ts3client/data/a;-><init>(Ljava/lang/String;JJJ)V

    .line 50506
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 1651
    const/4 v1, 0x0

    aput-object v0, v14, v1

    .line 1652
    const-wide/16 v0, 0x0

    .line 1653
    const-wide/16 v4, 0x0

    .line 1654
    const/4 v6, 0x1

    move-wide v10, v0

    :goto_af
    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ge v6, v0, :cond_10a

    .line 1655
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-nez v0, :cond_f0

    .line 1656
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 50508
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 1658
    aput-object v0, v14, v6

    .line 1659
    add-int/lit8 v6, v6, 0x1

    .line 50510
    :try_start_ce
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 1662
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v13, v1}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_df

    .line 50511
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 50512
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 1667
    :cond_df
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->j()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_ec

    .line 50514
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 50516
    :cond_ec
    iget-wide v0, v0, Lcom/teamspeak/ts3client/data/a;->b:J
    :try_end_ee
    .catch Ljava/lang/Exception; {:try_start_ce .. :try_end_ee} :catch_12b

    move-wide v10, v0

    .line 1671
    goto :goto_af

    .line 1673
    :cond_f0
    const/4 v7, 0x1

    add-int/lit8 v0, v6, -0x1

    aget-object v0, v14, v0

    .line 50517
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 1673
    if-nez v0, :cond_108

    const/4 v0, 0x1

    :goto_fa
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    move-object v1, p0

    move-object v2, v13

    move-object v3, v14

    invoke-direct/range {v1 .. v9}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Ljava/util/SortedMap;[Lcom/teamspeak/ts3client/data/a;JIILjava/util/concurrent/ConcurrentHashMap;Ljava/lang/Boolean;)I

    move-result v6

    .line 1674
    const-wide/16 v4, 0x0

    goto :goto_af

    .line 1673
    :cond_108
    const/4 v0, 0x0

    goto :goto_fa

    .line 1677
    :cond_10a
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50518
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50519
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1677
    if-eqz v0, :cond_12a

    .line 1678
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50520
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50521
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 50522
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->l()Z

    move-result v1

    if-eqz v1, :cond_12a

    .line 50523
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/v;

    invoke-direct {v2, v0, v14}, Lcom/teamspeak/ts3client/v;-><init>(Lcom/teamspeak/ts3client/t;[Lcom/teamspeak/ts3client/data/a;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1680
    :cond_12a
    return-void

    .line 1672
    :catch_12b
    move-exception v0

    goto :goto_af
.end method

.method public final c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1792
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50546
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->g:Z

    .line 1792
    if-eqz v0, :cond_8

    .line 1808
    :cond_7
    :goto_7
    return-void

    .line 1794
    :cond_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50547
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1794
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50548
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1794
    const-string v1, "android_overlay"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_24

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    if-eqz v0, :cond_40

    :cond_24
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50549
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1794
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50550
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1794
    const-string v1, "android_overlay"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    if-eqz v0, :cond_7

    .line 1795
    :cond_40
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50551
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50552
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1795
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/p;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/p;-><init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_7
.end method

.method public final d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1811
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->v:Z

    if-nez v0, :cond_6

    .line 1825
    :cond_5
    :goto_5
    return-void

    .line 1813
    :cond_6
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50553
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1813
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50554
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1813
    const-string v1, "android_overlay"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_22

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    if-eqz v0, :cond_3e

    :cond_22
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50555
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1813
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50556
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1813
    const-string v1, "android_overlay"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->r:Z

    if-eqz v0, :cond_5

    .line 1814
    :cond_3e
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50557
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50558
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1814
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/q;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/q;-><init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_5
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3

    .prologue
    .line 1695
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .registers 7

    .prologue
    const/4 v1, -0x2

    .line 1700
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 1701
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50539
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1701
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->l()V

    .line 1702
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->s:Landroid/view/WindowManager;

    .line 1704
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->t:Landroid/widget/ImageView;

    .line 1706
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->t:Landroid/widget/ImageView;

    const v2, 0x7f020076

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1707
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->x:Landroid/content/IntentFilter;

    .line 1708
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->x:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1709
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d2

    const/16 v4, 0x8

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->u:Landroid/view/WindowManager$LayoutParams;

    .line 1711
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->u:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1712
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->u:Landroid/view/WindowManager$LayoutParams;

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Landroid/view/WindowManager$LayoutParams;)V

    .line 1713
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->t:Landroid/widget/ImageView;

    new-instance v1, Lcom/teamspeak/ts3client/o;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/o;-><init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1776
    return-void
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 1830
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 1831
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->t:Landroid/widget/ImageView;

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->v:Z

    if-eqz v0, :cond_12

    .line 1832
    iget-object v0, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->s:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/teamspeak/ts3client/ConnectionBackground;->t:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1833
    :cond_12
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5

    .prologue
    .line 1837
    const/4 v0, 0x2

    return v0
.end method
