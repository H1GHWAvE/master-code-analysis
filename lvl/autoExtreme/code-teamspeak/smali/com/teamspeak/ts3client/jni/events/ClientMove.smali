.class public Lcom/teamspeak/ts3client/jni/events/ClientMove;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:I

.field private c:J

.field private d:J

.field private e:Lcom/teamspeak/ts3client/jni/j;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method private constructor <init>(JIJJILjava/lang/String;)V
    .registers 12

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a:J

    .line 20
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->b:I

    .line 21
    iput-wide p4, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c:J

    .line 22
    iput-wide p6, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d:J

    .line 23
    if-nez p8, :cond_11

    .line 24
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 25
    :cond_11
    const/4 v0, 0x1

    if-ne p8, v0, :cond_18

    .line 26
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 27
    :cond_18
    const/4 v0, 0x2

    if-ne p8, v0, :cond_1f

    .line 28
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 29
    :cond_1f
    iput-object p9, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f:Ljava/lang/String;

    .line 30
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 31
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 34
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->b:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()J
    .registers 3

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d:J

    return-wide v0
.end method

.method public final d()J
    .registers 3

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c:J

    return-wide v0
.end method

.method public final e()J
    .registers 3

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a:J

    return-wide v0
.end method

.method public final f()Lcom/teamspeak/ts3client/jni/j;
    .registers 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->e:Lcom/teamspeak/ts3client/jni/j;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientMove [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->e:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", moveMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMove;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
