.class public Lcom/teamspeak/ts3client/jni/Ts3Jni;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

.field private static b:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()I
    .registers 1

    .prologue
    .line 16
    sget v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b:I

    return v0
.end method

.method private a(JJLcom/teamspeak/ts3client/jni/i;D)I
    .registers 17

    .prologue
    .line 286
    .line 13520
    iget v6, p5, Lcom/teamspeak/ts3client/jni/i;->aD:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v7, p6

    .line 286
    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setServerVariableAsDouble(JJID)I

    move-result v0

    return v0
.end method

.method private a(JJLcom/teamspeak/ts3client/jni/i;I)I
    .registers 16

    .prologue
    .line 292
    .line 14520
    iget v6, p5, Lcom/teamspeak/ts3client/jni/i;->aD:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v7, p6

    .line 292
    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setServerVariableAsInt(JJII)I

    move-result v0

    return v0
.end method

.method private a(JJLcom/teamspeak/ts3client/jni/i;J)I
    .registers 17

    .prologue
    .line 304
    .line 16520
    iget v6, p5, Lcom/teamspeak/ts3client/jni/i;->aD:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v7, p6

    .line 304
    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setServerVariableAsUint64(JJIJ)I

    move-result v0

    return v0
.end method

.method private b(JLcom/teamspeak/ts3client/jni/f;)F
    .registers 5

    .prologue
    .line 354
    .line 17556
    iget v0, p3, Lcom/teamspeak/ts3client/jni/f;->ap:I

    .line 354
    invoke-virtual {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerConnectionVariableAsFloat(JI)F

    move-result v0

    return v0
.end method

.method public static declared-synchronized b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
    .registers 3

    .prologue
    .line 20
    const-class v1, Lcom/teamspeak/ts3client/jni/Ts3Jni;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_24

    if-nez v0, :cond_20

    .line 23
    :try_start_7
    const-string v0, "ts3client_android"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 24
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Load Lib"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_13
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_7 .. :try_end_13} :catch_27
    .catchall {:try_start_7 .. :try_end_13} :catchall_24

    .line 29
    :goto_13
    :try_start_13
    new-instance v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;-><init>()V

    .line 30
    sput-object v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_startInit()I

    move-result v0

    sput v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b:I

    .line 32
    :cond_20
    sget-object v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;
    :try_end_22
    .catchall {:try_start_13 .. :try_end_22} :catchall_24

    monitor-exit v1

    return-object v0

    .line 20
    :catchall_24
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_27
    move-exception v0

    goto :goto_13
.end method

.method private b(JLcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 124
    .line 4227
    iget v0, p3, Lcom/teamspeak/ts3client/jni/d;->ak:I

    .line 124
    invoke-virtual {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getClientSelfVariableAsString(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c()V
    .registers 1

    .prologue
    .line 36
    sget-object v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    if-eqz v0, :cond_c

    .line 38
    :try_start_4
    sget-object v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyClientLib()I
    :try_end_9
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_4 .. :try_end_9} :catch_d

    .line 42
    :goto_9
    const/4 v0, 0x0

    sput-object v0, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 44
    :cond_c
    return-void

    :catch_d
    move-exception v0

    goto :goto_9
.end method


# virtual methods
.method public final a(JILcom/teamspeak/ts3client/jni/d;)I
    .registers 6

    .prologue
    .line 130
    .line 5227
    iget v0, p4, Lcom/teamspeak/ts3client/jni/d;->ak:I

    .line 130
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getClientVariableAsInt(JII)I

    move-result v0

    return v0
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/c;)I
    .registers 13

    .prologue
    .line 92
    .line 1076
    iget v6, p5, Lcom/teamspeak/ts3client/jni/c;->I:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 92
    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getChannelVariableAsInt(JJI)I

    move-result v0

    return v0
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/c;I)I
    .registers 16

    .prologue
    .line 250
    .line 10076
    iget v6, p5, Lcom/teamspeak/ts3client/jni/c;->I:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v7, p6

    .line 250
    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setChannelVariableAsInt(JJII)I

    move-result v0

    return v0
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/c;J)I
    .registers 17

    .prologue
    .line 262
    .line 12076
    iget v6, p5, Lcom/teamspeak/ts3client/jni/c;->I:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v7, p6

    .line 262
    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setChannelVariableAsUInt64(JJIJ)I

    move-result v0

    return v0
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/c;Ljava/lang/String;)I
    .registers 16

    .prologue
    .line 256
    .line 11076
    iget v6, p5, Lcom/teamspeak/ts3client/jni/c;->I:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v7, p6

    .line 256
    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setChannelVariableAsString(JJILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/i;Ljava/lang/String;)I
    .registers 16

    .prologue
    .line 298
    .line 15520
    iget v6, p5, Lcom/teamspeak/ts3client/jni/i;->aD:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v7, p6

    .line 298
    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setServerVariableAsString(JJILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/d;)I
    .registers 5

    .prologue
    .line 118
    .line 3227
    iget v0, p3, Lcom/teamspeak/ts3client/jni/d;->ak:I

    .line 118
    invoke-virtual {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getClientSelfVariableAsInt(JI)I

    move-result v0

    return v0
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/d;I)I
    .registers 6

    .prologue
    .line 268
    .line 12227
    iget v0, p3, Lcom/teamspeak/ts3client/jni/d;->ak:I

    .line 268
    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setClientSelfVariableAsInt(JII)I

    move-result v0

    return v0
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I
    .registers 6

    .prologue
    .line 274
    .line 13227
    iget v0, p3, Lcom/teamspeak/ts3client/jni/d;->ak:I

    .line 274
    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setClientSelfVariableAsString(JILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/i;)I
    .registers 5

    .prologue
    .line 152
    .line 7520
    iget v0, p3, Lcom/teamspeak/ts3client/jni/i;->aD:I

    .line 152
    invoke-virtual {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerVariableAsInt(JI)I

    move-result v0

    return v0
.end method

.method public final a(JILcom/teamspeak/ts3client/jni/f;)J
    .registers 8

    .prologue
    .line 362
    .line 18556
    iget v0, p4, Lcom/teamspeak/ts3client/jni/f;->ap:I

    .line 362
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getConnectionVariableAsUInt64(JII)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/f;)J
    .registers 7

    .prologue
    .line 348
    .line 16556
    iget v0, p3, Lcom/teamspeak/ts3client/jni/f;->ap:I

    .line 348
    invoke-virtual {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerConnectionVariableAsUInt64(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(JILcom/teamspeak/ts3client/jni/f;)D
    .registers 8

    .prologue
    .line 368
    .line 19556
    iget v0, p4, Lcom/teamspeak/ts3client/jni/f;->ap:I

    .line 368
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getConnectionVariableAsDouble(JII)D

    move-result-wide v0

    return-wide v0
.end method

.method public final b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
    .registers 6

    .prologue
    .line 136
    .line 6227
    iget v0, p4, Lcom/teamspeak/ts3client/jni/d;->ak:I

    .line 136
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getClientVariableAsString(JII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(JJLcom/teamspeak/ts3client/jni/c;)Ljava/lang/String;
    .registers 13

    .prologue
    .line 98
    .line 2076
    iget v6, p5, Lcom/teamspeak/ts3client/jni/c;->I:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 98
    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getChannelVariableAsString(JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 158
    .line 8520
    iget v0, p3, Lcom/teamspeak/ts3client/jni/i;->aD:I

    .line 158
    invoke-virtual {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerVariableAsString(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(JILcom/teamspeak/ts3client/jni/d;)J
    .registers 8

    .prologue
    .line 142
    .line 7227
    iget v0, p4, Lcom/teamspeak/ts3client/jni/d;->ak:I

    .line 142
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getClientVariableAsUInt64(JII)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(JJLcom/teamspeak/ts3client/jni/c;)J
    .registers 13

    .prologue
    .line 104
    .line 3076
    iget v6, p5, Lcom/teamspeak/ts3client/jni/c;->I:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 104
    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getChannelVariableAsUInt64(JJI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(JLcom/teamspeak/ts3client/jni/i;)J
    .registers 7

    .prologue
    .line 164
    .line 9520
    iget v0, p3, Lcom/teamspeak/ts3client/jni/i;->aD:I

    .line 164
    invoke-virtual {p0, p1, p2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerVariableAsUInt64(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(JILcom/teamspeak/ts3client/jni/f;)Ljava/lang/String;
    .registers 6

    .prologue
    .line 374
    .line 20556
    iget v0, p4, Lcom/teamspeak/ts3client/jni/f;->ap:I

    .line 374
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getConnectionVariableAsString(JII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public native requestAudioData()[S
.end method

.method public native ts3client_activateCaptureDevice(J)I
.end method

.method public native ts3client_allowWhispersFrom(JI)I
.end method

.method public native ts3client_android_checkSignatureData(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)I
.end method

.method public native ts3client_android_getLicenseAgreementVersion()I
.end method

.method public native ts3client_android_getUpdaterURL()Ljava/lang/String;
.end method

.method public native ts3client_android_parseProtobufDataResult(Ljava/nio/ByteBuffer;I)Z
.end method

.method public native ts3client_banadd(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_banclient(JIJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_banclientUID(JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_banclientdbid(JJJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_bandel(JJLjava/lang/String;)I
.end method

.method public native ts3client_bandelall(JLjava/lang/String;)I
.end method

.method public native ts3client_bbcode_shouldPrependHTTP(Ljava/lang/String;)Z
.end method

.method public native ts3client_cleanUpConnectionInfo(JI)I
.end method

.method public native ts3client_clientChatClosed(JLjava/lang/String;ILjava/lang/String;)I
.end method

.method public native ts3client_clientChatComposing(JILjava/lang/String;)I
.end method

.method public native ts3client_closeCaptureDevice(J)I
.end method

.method public native ts3client_closePlaybackDevice(J)I
.end method

.method public native ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;
.end method

.method public native ts3client_createIdentity()Ljava/lang/String;
.end method

.method public native ts3client_destroyClientLib()I
.end method

.method public native ts3client_destroyServerConnectionHandler(J)I
.end method

.method public native ts3client_flushChannelCreation(JJLjava/lang/String;)I
.end method

.method public native ts3client_flushChannelUpdates(JJLjava/lang/String;)I
.end method

.method public native ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
.end method

.method public native ts3client_flushServerUpdates(JJLjava/lang/String;)I
.end method

.method public native ts3client_getChannelClientList(JJ)[I
.end method

.method public native ts3client_getChannelList(J)[J
.end method

.method public native ts3client_getChannelOfClient(JI)J
.end method

.method public native ts3client_getChannelVariableAsInt(JJI)I
.end method

.method public native ts3client_getChannelVariableAsString(JJI)Ljava/lang/String;
.end method

.method public native ts3client_getChannelVariableAsUInt64(JJI)J
.end method

.method public native ts3client_getClientID(J)I
.end method

.method public native ts3client_getClientLibVersion()Ljava/lang/String;
.end method

.method public native ts3client_getClientLibVersionNumber()I
.end method

.method public native ts3client_getClientList(J)[I
.end method

.method public native ts3client_getClientSelfVariableAsInt(JI)I
.end method

.method public native ts3client_getClientSelfVariableAsString(JI)Ljava/lang/String;
.end method

.method public native ts3client_getClientVariableAsInt(JII)I
.end method

.method public native ts3client_getClientVariableAsString(JII)Ljava/lang/String;
.end method

.method public native ts3client_getClientVariableAsUInt64(JII)J
.end method

.method public native ts3client_getConnectionVariableAsDouble(JII)D
.end method

.method public native ts3client_getConnectionVariableAsString(JII)Ljava/lang/String;
.end method

.method public native ts3client_getConnectionVariableAsUInt64(JII)J
.end method

.method public native ts3client_getParentChannelOfChannel(JJ)J
.end method

.method public native ts3client_getPlaybackConfigValueAsFloat(JLjava/lang/String;)F
.end method

.method public native ts3client_getPreProcessorConfigValue(JLjava/lang/String;)Ljava/lang/String;
.end method

.method public native ts3client_getPreProcessorInfoValueFloat(JLjava/lang/String;)F
.end method

.method public native ts3client_getServerConnectionVariableAsFloat(JI)F
.end method

.method public native ts3client_getServerConnectionVariableAsUInt64(JI)J
.end method

.method public native ts3client_getServerLicenseType(J)I
.end method

.method public native ts3client_getServerVariableAsInt(JI)I
.end method

.method public native ts3client_getServerVariableAsString(JI)Ljava/lang/String;
.end method

.method public native ts3client_getServerVariableAsUInt64(JI)J
.end method

.method public native ts3client_identityStringToFilename(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public native ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_openPlaybackDevice(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_prepareAudioDevice(II)I
.end method

.method public native ts3client_privilegeKeyUse(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_processCustomCaptureData(Ljava/lang/String;[SI)I
.end method

.method public native ts3client_removeFromAllowedWhispersFrom(JI)I
.end method

.method public native ts3client_requestBanList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelClientAddPerm(JJJ[J[IILjava/lang/String;)I
.end method

.method public native ts3client_requestChannelClientDelPerm(JJJ[JILjava/lang/String;)I
.end method

.method public native ts3client_requestChannelDelete(JJILjava/lang/String;)I
.end method

.method public native ts3client_requestChannelDescription(JJLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelGroupList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelMove(JJJJLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelSubscribeAll(JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelUnsubscribeAll(JLjava/lang/String;)I
.end method

.method public native ts3client_requestClientIDs(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientKickFromChannel(JILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientKickFromServer(JILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientMove(JIJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientPoke(JILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientSetIsTalker(JJILjava/lang/String;)I
.end method

.method public native ts3client_requestClientUIDfromClientID(JILjava/lang/String;)I
.end method

.method public native ts3client_requestClientVariables(JILjava/lang/String;)I
.end method

.method public native ts3client_requestComplainAdd(JJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestConnectionInfo(JILjava/lang/String;)I
.end method

.method public native ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestFileInfo(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestIsTalker(JILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestMuteClients(J[ILjava/lang/String;)I
.end method

.method public native ts3client_requestPermissionList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestSendChannelTextMsg(JLjava/lang/String;JLjava/lang/String;)I
.end method

.method public native ts3client_requestSendPrivateTextMsg(JLjava/lang/String;ILjava/lang/String;)I
.end method

.method public native ts3client_requestSendServerTextMsg(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestServerConnectionInfo(JLjava/lang/String;)I
.end method

.method public native ts3client_requestServerGroupAddClient(JJJLjava/lang/String;)I
.end method

.method public native ts3client_requestServerGroupDelClient(JJJLjava/lang/String;)I
.end method

.method public native ts3client_requestServerGroupList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestServerTemporaryPasswordAdd(JLjava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestServerTemporaryPasswordDel(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestServerTemporaryPasswordList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestServerVariables(J)I
.end method

.method public native ts3client_requestSetClientChannelGroup(J[J[J[JILjava/lang/String;)I
.end method

.method public native ts3client_requestUnmuteClients(J[ILjava/lang/String;)I
.end method

.method public native ts3client_setChannelVariableAsInt(JJII)I
.end method

.method public native ts3client_setChannelVariableAsString(JJILjava/lang/String;)I
.end method

.method public native ts3client_setChannelVariableAsUInt64(JJIJ)I
.end method

.method public native ts3client_setClientSelfVariableAsInt(JII)I
.end method

.method public native ts3client_setClientSelfVariableAsString(JILjava/lang/String;)I
.end method

.method public native ts3client_setClientVolumeModifier(JIF)I
.end method

.method public native ts3client_setLocalTestMode(JI)I
.end method

.method public native ts3client_setPlaybackConfigValue(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_setServerVariableAsDouble(JJID)I
.end method

.method public native ts3client_setServerVariableAsInt(JJII)I
.end method

.method public native ts3client_setServerVariableAsString(JJILjava/lang/String;)I
.end method

.method public native ts3client_setServerVariableAsUint64(JJIJ)I
.end method

.method public native ts3client_spawnNewServerConnectionHandler()J
.end method

.method public native ts3client_startConnection(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_startConnectionEx(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_startInit()I
.end method

.method public native ts3client_stopConnection(JLjava/lang/String;)I
.end method

.method public native ts3client_unregisterCustomDevice(Ljava/lang/String;)I
.end method

.method public native ts3client_verifyChannelPassword(JJLjava/lang/String;Ljava/lang/String;)I
.end method
