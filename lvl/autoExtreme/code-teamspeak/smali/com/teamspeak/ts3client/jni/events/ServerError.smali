.class public Lcom/teamspeak/ts3client/jni/events/ServerError;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field private d:J

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method private constructor <init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->d:J

    .line 21
    iput-object p3, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->a:Ljava/lang/String;

    .line 22
    iput p4, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    .line 23
    iput-object p5, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 24
    iput-object p6, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->e:Ljava/lang/String;

    .line 25
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 26
    return-void
.end method

.method private e()J
    .registers 3

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->d:J

    return-wide v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 29
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerError [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", returnCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extraMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ServerError;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
