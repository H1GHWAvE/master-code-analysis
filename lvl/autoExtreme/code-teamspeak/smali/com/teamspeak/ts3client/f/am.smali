.class final Lcom/teamspeak/ts3client/f/am;
.super Landroid/support/v4/app/ax;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# instance fields
.field public at:Z

.field au:Ljava/lang/Thread;

.field final synthetic av:Lcom/teamspeak/ts3client/f/ak;

.field private aw:Landroid/graphics/drawable/Drawable;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/f/ak;)V
    .registers 4

    .prologue
    .line 110
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/f/am;->at:Z

    .line 112
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/teamspeak/ts3client/f/an;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/f/an;-><init>(Lcom/teamspeak/ts3client/f/am;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/am;->au:Ljava/lang/Thread;

    return-void
.end method

.method synthetic constructor <init>(Lcom/teamspeak/ts3client/f/ak;B)V
    .registers 3

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/f/am;-><init>(Lcom/teamspeak/ts3client/f/ak;)V

    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/am;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/am;->aw:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private y()V
    .registers 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 212
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_spawnNewServerConnectionHandler()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/teamspeak/ts3client/f/ak;->a(Lcom/teamspeak/ts3client/f/ak;J)J

    .line 214
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 12093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 214
    const-string v1, "playbackStream"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 215
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 13093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 215
    const-string v2, "recordStream"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 216
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 14093
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 216
    const-string v3, "samplePlay"

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->i()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 217
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 15093
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 217
    const-string v4, "sampleRec"

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->h()I

    move-result v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 218
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_prepareAudioDevice(II)I

    .line 219
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v4

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v5

    .line 15110
    iput-object v5, v4, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 220
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v4

    invoke-virtual {v4, v3, v2, v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(IIII)Z

    .line 221
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(Ljava/lang/Boolean;)V

    .line 222
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/ak;->a(Lcom/teamspeak/ts3client/f/ak;)Z

    move-result v0

    if-eqz v0, :cond_107

    .line 223
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/teamspeak/ts3client/a/j;->a(I)V

    .line 226
    :goto_81
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_activateCaptureDevice(J)I

    .line 227
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    const-string v1, ""

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 230
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    const-string v1, "vad"

    const-string v4, "true"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    const-string v1, "voiceactivation_level"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v5

    .line 16093
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 231
    iget-object v6, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v6}, Lcom/teamspeak/ts3client/f/ak;->d(Lcom/teamspeak/ts3client/f/ak;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 232
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setLocalTestMode(JI)I

    .line 233
    invoke-static {}, Lcom/teamspeak/ts3client/jni/l;->a()Lcom/teamspeak/ts3client/jni/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 234
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->d()V

    .line 237
    return-void

    .line 225
    :cond_107
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/teamspeak/ts3client/a/j;->b(I)V

    goto/16 :goto_81
.end method

.method private z()V
    .registers 5

    .prologue
    .line 253
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 17061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 253
    if-eqz v0, :cond_3a

    .line 254
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 256
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setLocalTestMode(JI)I

    .line 257
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    .line 258
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 18061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 258
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->r()V

    .line 267
    :goto_39
    return-void

    .line 262
    :cond_3a
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->f()V

    .line 263
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->b()V

    .line 264
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 266
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    goto :goto_39
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14

    .prologue
    const/4 v10, 0x3

    const/4 v7, -0x1

    const/4 v9, 0x1

    const/4 v6, -0x2

    const/4 v8, 0x0

    .line 131
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 133
    if-eqz v1, :cond_29

    .line 2061
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 3061
    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 134
    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 4061
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 5061
    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 135
    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 6093
    :cond_29
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 138
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/ak;->d(Lcom/teamspeak/ts3client/f/ak;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 140
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 141
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    new-instance v4, Landroid/widget/SeekBar;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v4}, Lcom/teamspeak/ts3client/f/ak;->a(Lcom/teamspeak/ts3client/f/ak;Landroid/widget/SeekBar;)Landroid/widget/SeekBar;

    .line 144
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/SeekBar;->setId(I)V

    .line 145
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 146
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v3

    add-int/lit8 v1, v1, 0x32

    invoke-virtual {v3, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 147
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v1

    new-instance v3, Lcom/teamspeak/ts3client/f/ao;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/f/ao;-><init>(Lcom/teamspeak/ts3client/f/am;)V

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 169
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/am;->j()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200ac

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 170
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_a7

    .line 171
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/f/am;->aw:Landroid/graphics/drawable/Drawable;

    .line 173
    :cond_a7
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v3}, Lcom/teamspeak/ts3client/f/ak;->a(Lcom/teamspeak/ts3client/f/ak;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 174
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->e(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/TextView;

    move-result-object v1

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 175
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->e(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 176
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->e(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v4}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    add-int/lit8 v4, v4, -0x32

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / -50"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->e(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v3, 0xa

    invoke-virtual {v1, v8, v8, v3, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 178
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->e(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setId(I)V

    .line 180
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 181
    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 182
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 185
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/ak;->c(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getId()I

    move-result v3

    invoke-virtual {v1, v10, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 186
    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 187
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/ak;->e(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 190
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/ak;->e(Lcom/teamspeak/ts3client/f/ak;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-virtual {v1, v10, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 191
    new-instance v3, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 192
    const-string v4, "button.save"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 193
    new-instance v4, Lcom/teamspeak/ts3client/f/ap;

    invoke-direct {v4, p0, v0, v2}, Lcom/teamspeak/ts3client/f/ap;-><init>(Lcom/teamspeak/ts3client/f/am;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 6207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 203
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->g(Lcom/teamspeak/ts3client/f/ak;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 6212
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_spawnNewServerConnectionHandler()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lcom/teamspeak/ts3client/f/ak;->a(Lcom/teamspeak/ts3client/f/ak;J)J

    .line 6214
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 6214
    const-string v1, "playbackStream"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 6215
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 8093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 6215
    const-string v3, "recordStream"

    invoke-interface {v1, v3, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 6216
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 9093
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 6216
    const-string v4, "samplePlay"

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v5

    invoke-virtual {v5}, Lcom/teamspeak/ts3client/Ts3Application;->i()I

    move-result v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 6217
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    .line 10093
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 6217
    const-string v5, "sampleRec"

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->h()I

    move-result v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 6218
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v5

    invoke-virtual {v5, v4, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_prepareAudioDevice(II)I

    .line 6219
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v5

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v6

    .line 10110
    iput-object v6, v5, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 6220
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v5

    invoke-virtual {v5, v4, v3, v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(IIII)Z

    .line 6221
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(Ljava/lang/Boolean;)V

    .line 6222
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/ak;->a(Lcom/teamspeak/ts3client/f/ak;)Z

    move-result v0

    if-eqz v0, :cond_27f

    .line 6223
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/teamspeak/ts3client/a/j;->a(I)V

    .line 6226
    :goto_1f4
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_activateCaptureDevice(J)I

    .line 6227
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v4

    const-string v1, ""

    const-string v3, ""

    invoke-virtual {v0, v4, v5, v1, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I

    .line 6229
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v4

    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, v1, v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 6230
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v4

    const-string v1, "vad"

    const-string v3, "true"

    invoke-virtual {v0, v4, v5, v1, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 6231
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v4

    const-string v1, "voiceactivation_level"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v6

    .line 11093
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 6231
    iget-object v7, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v7}, Lcom/teamspeak/ts3client/f/ak;->d(Lcom/teamspeak/ts3client/f/ak;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v4, v5, v1, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 6232
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setLocalTestMode(JI)I

    .line 6233
    invoke-static {}, Lcom/teamspeak/ts3client/jni/l;->a()Lcom/teamspeak/ts3client/jni/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 6234
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->d()V

    .line 206
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/am;->au:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 207
    return-object v2

    .line 6225
    :cond_27f
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/teamspeak/ts3client/a/j;->b(I)V

    goto/16 :goto_1f4
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 6

    .prologue
    .line 271
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;

    if-eqz v0, :cond_27

    .line 272
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;

    .line 19036
    iget-wide v0, p1, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->a:J

    .line 273
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_27

    .line 274
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/am;->aw:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_27

    .line 19040
    iget v0, p1, Lcom/teamspeak/ts3client/jni/events/TalkStatusChange;->b:I

    .line 275
    const/4 v1, 0x1

    if-ne v0, v1, :cond_28

    .line 276
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/am;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/f/aq;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/f/aq;-><init>(Lcom/teamspeak/ts3client/f/am;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 292
    :cond_27
    :goto_27
    return-void

    .line 283
    :cond_28
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/am;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/f/ar;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/f/ar;-><init>(Lcom/teamspeak/ts3client/f/am;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_27
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 241
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 242
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 243
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/am;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/ak;->h(Lcom/teamspeak/ts3client/f/ak;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/bb;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 244
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 245
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 249
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 250
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 296
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/f/am;->at:Z

    .line 297
    invoke-static {}, Lcom/teamspeak/ts3client/jni/l;->a()Lcom/teamspeak/ts3client/jni/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 298
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/am;->au:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 300
    :try_start_f
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/am;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->h(Lcom/teamspeak/ts3client/f/ak;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_1c} :catch_58

    .line 19253
    :goto_1c
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 20061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19253
    if-eqz v0, :cond_67

    .line 19254
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 19256
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setLocalTestMode(JI)I

    .line 19257
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    .line 19258
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 21061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19258
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->r()V

    .line 305
    :goto_54
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->onDismiss(Landroid/content/DialogInterface;)V

    .line 306
    return-void

    .line 302
    :catch_58
    move-exception v0

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 19085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 302
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "java.lang.IllegalArgumentException: Receiver not registered"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1c

    .line 19262
    :cond_67
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->f()V

    .line 19263
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->b()V

    .line 19264
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 19266
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/am;->av:Lcom/teamspeak/ts3client/f/ak;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ak;->b(Lcom/teamspeak/ts3client/f/ak;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    goto :goto_54
.end method
