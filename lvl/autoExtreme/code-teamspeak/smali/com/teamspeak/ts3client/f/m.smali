.class final Lcom/teamspeak/ts3client/f/m;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field final synthetic at:Lcom/teamspeak/ts3client/f/k;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/f/k;)V
    .registers 2

    .prologue
    .line 75
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/teamspeak/ts3client/f/k;B)V
    .registers 3

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/f/m;-><init>(Lcom/teamspeak/ts3client/f/k;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14

    .prologue
    const/16 v10, 0xa

    const/4 v9, 0x3

    const/4 v8, -0x1

    const/4 v7, 0x0

    const/4 v6, -0x2

    .line 79
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 80
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/k;->a(Lcom/teamspeak/ts3client/f/k;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 82
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 83
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    new-instance v4, Landroid/widget/SeekBar;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v4}, Lcom/teamspeak/ts3client/f/k;->a(Lcom/teamspeak/ts3client/f/k;Landroid/widget/SeekBar;)Landroid/widget/SeekBar;

    .line 86
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/widget/SeekBar;->setId(I)V

    .line 87
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v3

    const/16 v4, 0x78

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 88
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v3

    add-int/lit8 v1, v1, 0x3c

    invoke-virtual {v3, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 89
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v1

    new-instance v3, Lcom/teamspeak/ts3client/f/n;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/f/n;-><init>(Lcom/teamspeak/ts3client/f/m;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 112
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/m;->j()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200ac

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v3}, Lcom/teamspeak/ts3client/f/k;->a(Lcom/teamspeak/ts3client/f/k;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 115
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->c(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/TextView;

    move-result-object v1

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 116
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->c(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 117
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->c(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v4, 0x3f000000    # 0.5f

    iget-object v5, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v5}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getProgress()I

    move-result v5

    add-int/lit8 v5, v5, -0x3c

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / 30"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->c(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v7, v7, v10, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 119
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->c(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setId(I)V

    .line 121
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 122
    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 123
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 126
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getId()I

    move-result v3

    invoke-virtual {v1, v9, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 127
    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 128
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/k;->c(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 131
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/k;->c(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-virtual {v1, v9, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 132
    new-instance v3, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 133
    const-string v4, "button.save"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 134
    new-instance v4, Lcom/teamspeak/ts3client/f/o;

    invoke-direct {v4, p0, v0, v2}, Lcom/teamspeak/ts3client/f/o;-><init>(Lcom/teamspeak/ts3client/f/m;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 144
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->e(Lcom/teamspeak/ts3client/f/k;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 145
    return-object v2
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 150
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 151
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 155
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 156
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 2

    .prologue
    .line 160
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->onDismiss(Landroid/content/DialogInterface;)V

    .line 161
    return-void
.end method
