.class public final Lcom/teamspeak/ts3client/f/ab;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/SeekBar;

.field private b:Landroid/widget/TextView;

.field private c:Ljava/lang/String;

.field private d:Lcom/teamspeak/ts3client/f/ad;

.field private e:Landroid/support/v4/app/bi;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:I

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
    .registers 13

    .prologue
    const/16 v6, 0x12

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x2

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/ab;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 38
    invoke-virtual {p0, v2, v6, v2, v6}, Lcom/teamspeak/ts3client/f/ab;->setPadding(IIII)V

    .line 39
    const v0, 0x7f0200b6

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/ab;->setBackgroundResource(I)V

    .line 40
    iput-object p5, p0, Lcom/teamspeak/ts3client/f/ab;->e:Landroid/support/v4/app/bi;

    .line 41
    iput-object p3, p0, Lcom/teamspeak/ts3client/f/ab;->c:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/teamspeak/ts3client/f/ab;->j:Ljava/lang/String;

    .line 43
    const/16 v0, -0x14

    iput v0, p0, Lcom/teamspeak/ts3client/f/ab;->g:I

    .line 44
    const/16 v0, 0x28

    iput v0, p0, Lcom/teamspeak/ts3client/f/ab;->h:I

    .line 45
    iput-object p2, p0, Lcom/teamspeak/ts3client/f/ab;->f:Ljava/lang/String;

    .line 46
    const/16 v0, 0x19

    iput v0, p0, Lcom/teamspeak/ts3client/f/ab;->i:I

    .line 47
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 50
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ab;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 52
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 53
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setId(I)V

    .line 54
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ab;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 56
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 57
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setId(I)V

    .line 59
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 60
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 61
    invoke-virtual {p0, v0, v2}, Lcom/teamspeak/ts3client/f/ab;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 64
    const/4 v3, 0x3

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 65
    invoke-virtual {p0, v1, v2}, Lcom/teamspeak/ts3client/f/ab;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    invoke-virtual {p0, v5}, Lcom/teamspeak/ts3client/f/ab;->setClickable(Z)V

    .line 68
    invoke-virtual {p0, p0}, Lcom/teamspeak/ts3client/f/ab;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ab;Landroid/widget/SeekBar;)Landroid/widget/SeekBar;
    .registers 2

    .prologue
    .line 23
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/ab;->a:Landroid/widget/SeekBar;

    return-object p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ab;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 23
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/ab;->b:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ab;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ab;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/ab;)I
    .registers 2

    .prologue
    .line 23
    iget v0, p0, Lcom/teamspeak/ts3client/f/ab;->i:I

    return v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/f/ab;)Landroid/widget/SeekBar;
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ab;->a:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/f/ab;)I
    .registers 2

    .prologue
    .line 23
    iget v0, p0, Lcom/teamspeak/ts3client/f/ab;->h:I

    return v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/f/ab;)I
    .registers 2

    .prologue
    .line 23
    iget v0, p0, Lcom/teamspeak/ts3client/f/ab;->g:I

    return v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/f/ab;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ab;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/f/ab;)Lcom/teamspeak/ts3client/f/ad;
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ab;->d:Lcom/teamspeak/ts3client/f/ad;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/f/ab;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ab;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 74
    new-instance v0, Lcom/teamspeak/ts3client/f/ad;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/teamspeak/ts3client/f/ad;-><init>(Lcom/teamspeak/ts3client/f/ab;B)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/ab;->d:Lcom/teamspeak/ts3client/f/ad;

    .line 75
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ab;->d:Lcom/teamspeak/ts3client/f/ad;

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ab;->e:Landroid/support/v4/app/bi;

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ab;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/f/ad;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 76
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 77
    return-void
.end method
