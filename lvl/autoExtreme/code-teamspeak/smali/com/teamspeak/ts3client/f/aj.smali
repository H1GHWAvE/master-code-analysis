.class final Lcom/teamspeak/ts3client/f/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Landroid/widget/RelativeLayout;

.field final synthetic c:Lcom/teamspeak/ts3client/f/ai;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/f/ai;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V
    .registers 4

    .prologue
    .line 142
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    iput-object p2, p0, Lcom/teamspeak/ts3client/f/aj;->a:Lcom/teamspeak/ts3client/Ts3Application;

    iput-object p3, p0, Lcom/teamspeak/ts3client/f/aj;->b:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 7

    .prologue
    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    iget-object v0, v0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/ag;->a(Lcom/teamspeak/ts3client/f/ag;)I

    move-result v0

    packed-switch v0, :pswitch_data_76

    .line 150
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/aj;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 150
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    iget-object v1, v1, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ag;->b(Lcom/teamspeak/ts3client/f/ag;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/ai;->a(Lcom/teamspeak/ts3client/f/ai;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 153
    :goto_2c
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 154
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    iget-object v0, v0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/ag;->c(Lcom/teamspeak/ts3client/f/ag;)Lcom/teamspeak/ts3client/f/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/f/ai;->b()V

    .line 155
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/aj;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 156
    return-void

    .line 147
    :pswitch_41
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/aj;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 147
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    iget-object v0, v0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/ag;->b(Lcom/teamspeak/ts3client/f/ag;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    iget-object v0, v0, Lcom/teamspeak/ts3client/f/ai;->au:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    iget-object v3, v3, Lcom/teamspeak/ts3client/f/ai;->at:Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/teamspeak/ts3client/f/aj;->c:Lcom/teamspeak/ts3client/f/ai;

    invoke-static {v4}, Lcom/teamspeak/ts3client/f/ai;->a(Lcom/teamspeak/ts3client/f/ai;)Landroid/widget/Spinner;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_2c

    .line 145
    nop

    :pswitch_data_76
    .packed-switch -0x1
        :pswitch_41
    .end packed-switch
.end method
