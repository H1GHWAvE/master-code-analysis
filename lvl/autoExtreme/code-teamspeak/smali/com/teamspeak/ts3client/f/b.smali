.class public final Lcom/teamspeak/ts3client/f/b;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/support/v4/app/bi;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
    .registers 12

    .prologue
    const/16 v6, 0x12

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x2

    .line 22
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 23
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/b;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 24
    invoke-virtual {p0, v2, v6, v2, v6}, Lcom/teamspeak/ts3client/f/b;->setPadding(IIII)V

    .line 25
    const v0, 0x7f0200b6

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/b;->setBackgroundResource(I)V

    .line 26
    iput-object p4, p0, Lcom/teamspeak/ts3client/f/b;->b:Landroid/support/v4/app/bi;

    .line 27
    iput-object p2, p0, Lcom/teamspeak/ts3client/f/b;->a:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/teamspeak/ts3client/f/b;->c:Ljava/lang/String;

    .line 29
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 32
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 34
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 35
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setId(I)V

    .line 36
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 38
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 39
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setId(I)V

    .line 41
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 42
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 43
    invoke-virtual {p0, v0, v2}, Lcom/teamspeak/ts3client/f/b;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 46
    const/4 v3, 0x3

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 47
    invoke-virtual {p0, v1, v2}, Lcom/teamspeak/ts3client/f/b;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 49
    invoke-virtual {p0, v5}, Lcom/teamspeak/ts3client/f/b;->setClickable(Z)V

    .line 50
    invoke-virtual {p0, p0}, Lcom/teamspeak/ts3client/f/b;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 56
    invoke-static {}, Lcom/teamspeak/ts3client/a/a;->y()Lcom/teamspeak/ts3client/a/a;

    move-result-object v0

    .line 1447
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/a/a;->at:Z

    .line 57
    if-eqz v1, :cond_9

    .line 61
    :goto_8
    return-void

    .line 59
    :cond_9
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/b;->b:Landroid/support/v4/app/bi;

    const-string v2, "AudioSettings"

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/a;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    goto :goto_8
.end method
