.class final Lcom/teamspeak/ts3client/f/h;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field final synthetic at:Lcom/teamspeak/ts3client/f/f;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/f/f;)V
    .registers 2

    .prologue
    .line 82
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/teamspeak/ts3client/f/f;B)V
    .registers 3

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/f/h;-><init>(Lcom/teamspeak/ts3client/f/f;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, -0x2

    .line 85
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 86
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/f;->a(Lcom/teamspeak/ts3client/f/f;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/f;->b(Lcom/teamspeak/ts3client/f/f;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 89
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    new-instance v4, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v4}, Lcom/teamspeak/ts3client/f/f;->a(Lcom/teamspeak/ts3client/f/f;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 92
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setId(I)V

    .line 93
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v4}, Lcom/teamspeak/ts3client/f/f;->d(Lcom/teamspeak/ts3client/f/f;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 94
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v1

    const-string v3, "Input"

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 96
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 97
    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 98
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 101
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v3}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getId()I

    move-result v3

    invoke-virtual {v1, v9, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 102
    new-instance v3, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 103
    const-string v4, "button.save"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v4, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v4}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_bb

    .line 105
    invoke-virtual {v3, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 106
    :cond_bb
    iget-object v4, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v4}, Lcom/teamspeak/ts3client/f/f;->e(Lcom/teamspeak/ts3client/f/f;)I

    move-result v4

    if-lez v4, :cond_dc

    .line 107
    iget-object v4, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v4}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Landroid/text/InputFilter;

    new-instance v6, Landroid/text/InputFilter$LengthFilter;

    iget-object v7, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v7}, Lcom/teamspeak/ts3client/f/f;->e(Lcom/teamspeak/ts3client/f/f;)I

    move-result v7

    invoke-direct {v6, v7}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v6, v5, v8

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 110
    :cond_dc
    iget-object v4, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v4}, Lcom/teamspeak/ts3client/f/f;->c(Lcom/teamspeak/ts3client/f/f;)Landroid/widget/EditText;

    move-result-object v4

    new-instance v5, Lcom/teamspeak/ts3client/f/i;

    invoke-direct {v5, p0, v3}, Lcom/teamspeak/ts3client/f/i;-><init>(Lcom/teamspeak/ts3client/f/h;Landroid/widget/Button;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 132
    new-instance v4, Lcom/teamspeak/ts3client/f/j;

    invoke-direct {v4, p0, v0, v2}, Lcom/teamspeak/ts3client/f/j;-><init>(Lcom/teamspeak/ts3client/f/h;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 144
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/h;->at:Lcom/teamspeak/ts3client/f/f;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/f;->g(Lcom/teamspeak/ts3client/f/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 146
    return-object v2
.end method
