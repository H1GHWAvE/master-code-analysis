.class final Lcom/teamspeak/ts3client/f/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Lcom/teamspeak/ts3client/f/h;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/f/h;Landroid/widget/Button;)V
    .registers 3

    .prologue
    .line 110
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/i;->b:Lcom/teamspeak/ts3client/f/h;

    iput-object p2, p0, Lcom/teamspeak/ts3client/f/i;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 4

    .prologue
    .line 114
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_d

    .line 115
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/i;->a:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 118
    :goto_c
    return-void

    .line 117
    :cond_d
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/i;->a:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_c
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .prologue
    .line 124
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .prologue
    .line 130
    return-void
.end method
