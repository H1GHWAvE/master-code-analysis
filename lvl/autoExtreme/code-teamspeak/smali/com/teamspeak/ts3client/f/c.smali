.class public final Lcom/teamspeak/ts3client/f/c;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/CheckBox;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v6, -0x2

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/c;->c:Ljava/lang/String;

    .line 26
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/f/c;->d:Z

    .line 42
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/c;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    const/16 v0, 0x12

    const/16 v1, 0x12

    invoke-virtual {p0, v2, v0, v2, v1}, Lcom/teamspeak/ts3client/f/c;->setPadding(IIII)V

    .line 44
    const v0, 0x7f0200b6

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/c;->setBackgroundResource(I)V

    .line 45
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 46
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 47
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 48
    const v3, 0x7f030045

    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/c;->e:Landroid/view/View;

    .line 49
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/c;->e:Landroid/view/View;

    const v3, 0x7f0c018f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/c;->a:Landroid/widget/CheckBox;

    .line 50
    iput-object p2, p0, Lcom/teamspeak/ts3client/f/c;->b:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v3, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 53
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v3, p2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 55
    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 57
    invoke-virtual {v1, v5, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 58
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setId(I)V

    .line 59
    invoke-virtual {v2, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    const/high16 v4, 0x41200000    # 10.0f

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 61
    invoke-virtual {v2, v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 62
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setId(I)V

    .line 63
    iget-object v4, p0, Lcom/teamspeak/ts3client/f/c;->a:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v4, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 64
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/c;->a:Landroid/widget/CheckBox;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Checkbox"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v3, p0, Lcom/teamspeak/ts3client/f/c;->a:Landroid/widget/CheckBox;

    new-instance v4, Lcom/teamspeak/ts3client/f/d;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/f/d;-><init>(Lcom/teamspeak/ts3client/f/c;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 95
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 96
    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 97
    invoke-virtual {p0, v1, v0}, Lcom/teamspeak/ts3client/f/c;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 100
    const/4 v3, 0x3

    invoke-virtual {v1}, Landroid/widget/TextView;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 101
    invoke-virtual {p0, v2, v0}, Lcom/teamspeak/ts3client/f/c;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 104
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 105
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/c;->e:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/teamspeak/ts3client/f/c;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    invoke-virtual {p0, v7}, Lcom/teamspeak/ts3client/f/c;->setClickable(Z)V

    .line 107
    invoke-virtual {p0, p0}, Lcom/teamspeak/ts3client/f/c;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 30
    invoke-direct/range {p0 .. p5}, Lcom/teamspeak/ts3client/f/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iput-object p6, p0, Lcom/teamspeak/ts3client/f/c;->c:Ljava/lang/String;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/f/c;->d:Z

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
    .registers 8

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/teamspeak/ts3client/f/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iput-object p6, p0, Lcom/teamspeak/ts3client/f/c;->c:Ljava/lang/String;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/c;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/f/c;)Z
    .registers 2

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/f/c;->d:Z

    return v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 112
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/c;->a:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/teamspeak/ts3client/f/c;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 113
    return-void

    .line 112
    :cond_f
    const/4 v0, 0x0

    goto :goto_b
.end method
