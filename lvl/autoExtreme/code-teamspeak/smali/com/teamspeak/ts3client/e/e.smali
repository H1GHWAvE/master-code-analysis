.class public final Lcom/teamspeak/ts3client/e/e;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/teamspeak/ts3client/e/e;->b:Landroid/content/Context;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    .line 32
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/e;->c:Landroid/view/LayoutInflater;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/e/e;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/teamspeak/ts3client/e/a;)V
    .registers 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_d
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/e/e;)Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Lcom/teamspeak/ts3client/e/a;)V
    .registers 3

    .prologue
    .line 112
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 113
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 114
    :cond_d
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 52
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10

    .prologue
    const v5, -0x888889

    .line 60
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/e;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 61
    const v0, 0x7f0c0188

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 62
    const v1, 0x7f0c0189

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 63
    const v2, 0x7f0c018a

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 64
    iget-object v3, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/teamspeak/ts3client/e/a;

    .line 1057
    iget-boolean v3, v3, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 64
    if-eqz v3, :cond_58

    .line 65
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v5, v3}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 95
    :goto_39
    iget-object v2, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/e/a;

    .line 2057
    iget-boolean v2, v2, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 95
    if-eqz v2, :cond_61

    .line 96
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v5, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 106
    :goto_4a
    iget-object v1, p0, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/e/a;

    .line 3041
    iget-object v1, v1, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 106
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-object v4

    .line 67
    :cond_58
    new-instance v3, Lcom/teamspeak/ts3client/e/f;

    invoke-direct {v3, p0, p1}, Lcom/teamspeak/ts3client/e/f;-><init>(Lcom/teamspeak/ts3client/e/e;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_39

    .line 98
    :cond_61
    new-instance v2, Lcom/teamspeak/ts3client/e/i;

    invoke-direct {v2, p0, p1}, Lcom/teamspeak/ts3client/e/i;-><init>(Lcom/teamspeak/ts3client/e/e;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4a
.end method
