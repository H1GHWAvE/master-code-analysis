.class final Lcom/teamspeak/ts3client/customs/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V
    .registers 2

    .prologue
    .line 89
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9

    .prologue
    .line 93
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 124
    :goto_8
    return-void

    .line 96
    :cond_9
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;Z)Z

    .line 97
    packed-switch p3, :pswitch_data_d8

    .line 122
    :goto_12
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;Z)Z

    .line 123
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->g(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V

    goto :goto_8

    .line 99
    :pswitch_1e
    sget-object v0, Lcom/teamspeak/ts3client/data/d/aa;->b:Lcom/teamspeak/ts3client/data/d/aa;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Ljava/util/BitSet;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v2}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/y;->a(Lcom/teamspeak/ts3client/data/d/aa;Ljava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->d(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/SeekBar;

    move-result-object v1

    .line 1144
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 100
    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 101
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v1

    .line 2136
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 101
    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 102
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v1

    .line 3136
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 3140
    iput v2, v1, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 103
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v1

    .line 3144
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 3148
    iput v0, v1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    goto :goto_12

    .line 106
    :pswitch_5b
    sget-object v0, Lcom/teamspeak/ts3client/data/d/aa;->c:Lcom/teamspeak/ts3client/data/d/aa;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Ljava/util/BitSet;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v2}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/y;->a(Lcom/teamspeak/ts3client/data/d/aa;Ljava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->d(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/SeekBar;

    move-result-object v1

    .line 4144
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 107
    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 108
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v1

    .line 5136
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 108
    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 109
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v1

    .line 6136
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 6140
    iput v2, v1, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 110
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v1

    .line 6144
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 6148
    iput v0, v1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    goto/16 :goto_12

    .line 113
    :pswitch_99
    sget-object v0, Lcom/teamspeak/ts3client/data/d/aa;->d:Lcom/teamspeak/ts3client/data/d/aa;

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Ljava/util/BitSet;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v2}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/y;->a(Lcom/teamspeak/ts3client/data/d/aa;Ljava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->d(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/SeekBar;

    move-result-object v1

    .line 7144
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 114
    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 115
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v1

    .line 8136
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 115
    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 116
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v1

    .line 9136
    iget v2, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 9140
    iput v2, v1, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    .line 117
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/b;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v1

    .line 9144
    iget v0, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    .line 9148
    iput v0, v1, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    goto/16 :goto_12

    .line 97
    nop

    :pswitch_data_d8
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_5b
        :pswitch_99
    .end packed-switch
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2

    .prologue
    .line 130
    return-void
.end method
