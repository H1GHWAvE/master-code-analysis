.class public final Lcom/teamspeak/ts3client/customs/h;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 6

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 24
    const v1, 0x7f030040

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 25
    const v0, 0x7f0c0184

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/h;->a:Landroid/widget/ImageView;

    .line 26
    const v0, 0x7f0c0185

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/h;->b:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f0200b6

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/customs/h;->setBackgroundResource(I)V

    .line 28
    invoke-virtual {p0, v1}, Lcom/teamspeak/ts3client/customs/h;->addView(Landroid/view/View;)V

    .line 29
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .registers 5

    .prologue
    const/high16 v1, 0x41800000    # 16.0f

    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/h;->a:Landroid/widget/ImageView;

    invoke-static {p1, v1, v1}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/h;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/h;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 33
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/h;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    return-void
.end method


# virtual methods
.method public final setEnabled(Z)V
    .registers 5

    .prologue
    const v2, -0xbbbbbc

    .line 44
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 45
    if-nez p1, :cond_14

    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/h;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 47
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/h;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v1}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 49
    :cond_14
    return-void
.end method
