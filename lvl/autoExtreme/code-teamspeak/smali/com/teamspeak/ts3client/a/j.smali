.class public final Lcom/teamspeak/ts3client/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/teamspeak/ts3client/a/j;


# instance fields
.field private b:Ljava/util/BitSet;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/j;->b:Ljava/util/BitSet;

    .line 17
    return-void
.end method

.method public static a()Lcom/teamspeak/ts3client/a/j;
    .registers 1

    .prologue
    .line 20
    sget-object v0, Lcom/teamspeak/ts3client/a/j;->a:Lcom/teamspeak/ts3client/a/j;

    if-nez v0, :cond_b

    .line 21
    new-instance v0, Lcom/teamspeak/ts3client/a/j;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/a/j;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/a/j;->a:Lcom/teamspeak/ts3client/a/j;

    .line 23
    :cond_b
    sget-object v0, Lcom/teamspeak/ts3client/a/j;->a:Lcom/teamspeak/ts3client/a/j;

    return-object v0
.end method

.method private c()Ljava/util/BitSet;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/j;->b:Ljava/util/BitSet;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .registers 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/j;->b:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->set(I)V

    .line 32
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/j;->b()V

    .line 33
    return-void
.end method

.method public final b()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 41
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/j;->b:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 42
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(Ljava/lang/Boolean;)V

    .line 47
    :goto_15
    return-void

    .line 44
    :cond_16
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 44
    const-string v2, "audio_handfree"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(Ljava/lang/Boolean;)V

    goto :goto_15
.end method

.method public final b(I)V
    .registers 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/j;->b:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->clear(I)V

    .line 37
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/a/j;->b()V

    .line 38
    return-void
.end method
