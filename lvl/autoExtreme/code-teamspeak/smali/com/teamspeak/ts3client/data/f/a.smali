.class public final Lcom/teamspeak/ts3client/data/f/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# instance fields
.field public a:Ljava/util/SortedMap;

.field public b:Lcom/teamspeak/ts3client/Ts3Application;

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 3

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/f/a;->c:Z

    .line 20
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, p1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 21
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 22
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    .line 23
    return-void
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8250
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 82
    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9250
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 10163
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/d/v;->c:Z

    .line 82
    if-nez v0, :cond_14

    .line 83
    const/4 v0, 0x0

    .line 84
    :goto_13
    return v0

    :cond_14
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/f/a;->c:Z

    goto :goto_13
.end method

.method private b(Ljava/lang/String;)I
    .registers 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4250
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 60
    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/d/v;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/data/f/a;->b(I)I

    move-result v0

    return v0
.end method

.method private c(I)Z
    .registers 4

    .prologue
    .line 49
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 6

    .prologue
    .line 65
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;

    if-eqz v0, :cond_1f

    .line 66
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/f/a;->c:Z

    if-eqz v0, :cond_b

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/f/a;->c:Z

    :cond_b
    move-object v0, p1

    .line 68
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;

    .line 69
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    .line 5023
    iget v2, v0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->a:I

    .line 69
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 5027
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions;->b:I

    .line 69
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    :cond_1f
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissionsFinished;

    if-eqz v0, :cond_3d

    .line 72
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissionsFinished;

    .line 6019
    iget-wide v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ClientNeededPermissionsFinished;->a:J

    .line 73
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 73
    cmp-long v0, v0, v2

    if-nez v0, :cond_3d

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/f/a;->c:Z

    .line 75
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 75
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    .line 78
    :cond_3d
    return-void
.end method

.method public final a(I)Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_22

    move v0, v1

    .line 29
    :goto_21
    return v0

    :cond_22
    move v0, v2

    .line 27
    goto :goto_21

    :cond_24
    move v0, v2

    .line 29
    goto :goto_21
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/g;)Z
    .registers 3

    .prologue
    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3250
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 42
    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/data/f/a;->a(I)Z
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_d} :catch_f

    move-result v0

    .line 44
    :goto_e
    return v0

    :catch_f
    move-exception v0

    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final a(Ljava/lang/String;)Z
    .registers 3

    .prologue
    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2250
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 34
    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/d/v;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/data/f/a;->a(I)Z
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_d} :catch_f

    move-result v0

    .line 36
    :goto_e
    return v0

    :catch_f
    move-exception v0

    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final b(I)I
    .registers 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 54
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 56
    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method
