.class public final Lcom/teamspeak/ts3client/data/d/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Vector;

.field private static b:Ljava/util/Vector;

.field private static c:Lcom/a/b/d/bw;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 15
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/data/d/q;->a:Ljava/util/Vector;

    .line 16
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/data/d/q;->b:Ljava/util/Vector;

    .line 17
    invoke-static {}, Lcom/a/b/d/hy;->a()Lcom/a/b/d/hy;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .registers 3

    .prologue
    .line 40
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Dialog;

    .line 41
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_6

    .line 42
    :cond_16
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 43
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_21
    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ax;

    .line 44
    invoke-virtual {v0}, Landroid/support/v4/app/ax;->k()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 45
    invoke-virtual {v0}, Landroid/support/v4/app/ax;->b()V

    goto :goto_21

    .line 47
    :cond_37
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 48
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    invoke-interface {v0}, Lcom/a/b/d/bw;->clear()V

    .line 49
    return-void
.end method

.method public static a(Landroid/app/Dialog;)V
    .registers 2

    .prologue
    .line 20
    new-instance v0, Lcom/teamspeak/ts3client/data/d/r;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/d/r;-><init>(Landroid/app/Dialog;)V

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 27
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->a:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public static a(Landroid/support/v4/app/ax;)V
    .registers 2

    .prologue
    .line 31
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->b:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public static a(Landroid/support/v4/app/ax;Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 35
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->b:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 36
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    invoke-interface {v0, p1, p0}, Lcom/a/b/d/bw;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 64
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    invoke-interface {v0, p0}, Lcom/a/b/d/bw;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 65
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    invoke-interface {v0, p0}, Lcom/a/b/d/bw;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ax;

    .line 66
    invoke-virtual {v0}, Landroid/support/v4/app/ax;->b()V

    .line 68
    :cond_13
    return-void
.end method

.method static synthetic b()Ljava/util/Vector;
    .registers 1

    .prologue
    .line 13
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->a:Ljava/util/Vector;

    return-object v0
.end method

.method public static b(Landroid/support/v4/app/ax;)V
    .registers 2

    .prologue
    .line 52
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->b:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 53
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    invoke-interface {v0}, Lcom/a/b/d/bw;->b()Lcom/a/b/d/bw;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/a/b/d/bw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method private static b(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 57
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    invoke-interface {v0, p0}, Lcom/a/b/d/bw;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 58
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->b:Ljava/util/Vector;

    sget-object v1, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    sget-object v2, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    invoke-interface {v1, v2}, Lcom/a/b/d/bw;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Lcom/teamspeak/ts3client/data/d/q;->c:Lcom/a/b/d/bw;

    invoke-interface {v0, p0}, Lcom/a/b/d/bw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :cond_1a
    return-void
.end method
