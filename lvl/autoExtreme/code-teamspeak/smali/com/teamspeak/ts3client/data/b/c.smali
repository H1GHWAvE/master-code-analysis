.class public final Lcom/teamspeak/ts3client/data/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/teamspeak/ts3client/data/b/c; = null

.field private static final d:Ljava/lang/String; = "create table contacts (contact_id integer primary key autoincrement, u_identifier text not null, customname text not null , display integer not null, status integer not null, mute integer not null, ignorepublicchat integer not null, ignoreprivatechat integer not null, ignorepokes integer not null, hideaway integer not null,hideavatar integer not null, whisperallow integer not null, volumemodifier float not null);"

.field private static final e:Ljava/lang/String; = "Create Index u_identifier_idx ON contacts(u_identifier);"

.field private static final f:Ljava/lang/String; = "contacts"

.field private static final g:Ljava/lang/String; = "Teamspeak-Contacts"

.field private static final h:I = 0x2


# instance fields
.field public b:Ljava/util/Vector;

.field public c:Lcom/teamspeak/ts3client/c/b;

.field private i:Landroid/database/sqlite/SQLiteDatabase;

.field private j:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->b:Ljava/util/Vector;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->j:Ljava/util/HashMap;

    .line 35
    new-instance v0, Lcom/teamspeak/ts3client/data/b/d;

    invoke-direct {v0, p0, p1}, Lcom/teamspeak/ts3client/data/b/d;-><init>(Lcom/teamspeak/ts3client/data/b/c;Landroid/content/Context;)V

    .line 36
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/d;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->i:Landroid/database/sqlite/SQLiteDatabase;

    .line 37
    sput-object p0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 38
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/b/c;->f()V

    .line 39
    return-void
.end method

.method private a(I)Lcom/teamspeak/ts3client/c/a;
    .registers 15

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 130
    new-instance v9, Lcom/teamspeak/ts3client/c/a;

    invoke-direct {v9}, Lcom/teamspeak/ts3client/c/a;-><init>()V

    .line 135
    :try_start_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->i:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "contacts"

    const/16 v2, 0xd

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "contact_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "u_identifier"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "customname"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "display"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "status"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "mute"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "ignorepublicchat"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "ignoreprivatechat"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "ignorepokes"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "hideaway"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "hideavatar"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "whisperallow"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "volumemodifier"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "contact_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 136
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_126

    .line 138
    const-string v0, "contact_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 15042
    iput v0, v9, Lcom/teamspeak/ts3client/c/a;->b:I

    .line 139
    const-string v0, "u_identifier"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/teamspeak/ts3client/c/a;->a(Ljava/lang/String;)V

    .line 140
    const-string v0, "customname"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 16034
    iput-object v0, v9, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 141
    const-string v0, "display"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 16050
    iput v0, v9, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 142
    const-string v0, "status"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 16058
    iput v0, v9, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 143
    const-string v0, "mute"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_12b

    move v0, v11

    .line 16106
    :goto_be
    iput-boolean v0, v9, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 144
    const-string v0, "ignorepublicchat"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_12d

    move v0, v11

    .line 17098
    :goto_cd
    iput-boolean v0, v9, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 145
    const-string v0, "ignoreprivatechat"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_12f

    move v0, v11

    .line 18090
    :goto_dc
    iput-boolean v0, v9, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 146
    const-string v0, "ignorepokes"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_131

    move v0, v11

    .line 19082
    :goto_eb
    iput-boolean v0, v9, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 147
    const-string v0, "hideaway"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_133

    move v0, v11

    .line 20074
    :goto_fa
    iput-boolean v0, v9, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 148
    const-string v0, "hideavatar"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_135

    move v0, v11

    .line 21066
    :goto_109
    iput-boolean v0, v9, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 149
    const-string v0, "whisperallow"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_137

    move v0, v11

    .line 21114
    :goto_118
    iput-boolean v0, v9, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 150
    const-string v0, "volumemodifier"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 21122
    iput v0, v9, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 153
    :cond_126
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_129
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_129} :catch_139

    move-object v0, v9

    .line 158
    :goto_12a
    return-object v0

    :cond_12b
    move v0, v12

    .line 143
    goto :goto_be

    :cond_12d
    move v0, v12

    .line 144
    goto :goto_cd

    :cond_12f
    move v0, v12

    .line 145
    goto :goto_dc

    :cond_131
    move v0, v12

    .line 146
    goto :goto_eb

    :cond_133
    move v0, v12

    .line 147
    goto :goto_fa

    :cond_135
    move v0, v12

    .line 148
    goto :goto_109

    :cond_137
    move v0, v12

    .line 149
    goto :goto_118

    .line 155
    :catch_139
    move-exception v0

    move-object v0, v10

    goto :goto_12a
.end method

.method public static a()Lcom/teamspeak/ts3client/data/b/c;
    .registers 1

    .prologue
    .line 42
    sget-object v0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    return-object v0
.end method

.method private a(Lcom/teamspeak/ts3client/c/b;)V
    .registers 2

    .prologue
    .line 263
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/b/c;->c:Lcom/teamspeak/ts3client/c/b;

    .line 264
    return-void
.end method

.method private d()V
    .registers 2

    .prologue
    .line 71
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->b:Ljava/util/Vector;

    .line 72
    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->i:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 76
    return-void
.end method

.method private f()V
    .registers 5

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/data/b/c;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 163
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/teamspeak/ts3client/data/b/c;->j:Ljava/util/HashMap;

    .line 164
    if-nez v0, :cond_e

    .line 169
    :cond_d
    return-void

    .line 166
    :cond_e
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/c/a;

    .line 167
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/b/c;->j:Ljava/util/HashMap;

    .line 22022
    iget-object v3, v0, Lcom/teamspeak/ts3client/c/a;->c:Ljava/lang/String;

    .line 167
    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_12
.end method

.method private g()V
    .registers 2

    .prologue
    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->c:Lcom/teamspeak/ts3client/c/b;

    .line 260
    return-void
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/c/a;)J
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 47
    const-string v0, "customname"

    .line 1030
    iget-object v4, p1, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 47
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v0, "u_identifier"

    .line 2022
    iget-object v4, p1, Lcom/teamspeak/ts3client/c/a;->c:Ljava/lang/String;

    .line 48
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v0, "display"

    .line 2046
    iget v4, p1, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 49
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 50
    const-string v0, "status"

    .line 2054
    iget v4, p1, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 50
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 51
    const-string v4, "mute"

    .line 2102
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 51
    if-eqz v0, :cond_a4

    move v0, v1

    :goto_32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 52
    const-string v4, "ignorepublicchat"

    .line 3094
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 52
    if-eqz v0, :cond_a6

    move v0, v1

    :goto_40
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 53
    const-string v4, "ignoreprivatechat"

    .line 4086
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 53
    if-eqz v0, :cond_a8

    move v0, v1

    :goto_4e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 54
    const-string v4, "ignorepokes"

    .line 5078
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 54
    if-eqz v0, :cond_aa

    move v0, v1

    :goto_5c
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 55
    const-string v4, "hideaway"

    .line 6070
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 55
    if-eqz v0, :cond_ac

    move v0, v1

    :goto_6a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 56
    const-string v4, "hideavatar"

    .line 7062
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 56
    if-eqz v0, :cond_ae

    move v0, v1

    :goto_78
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 57
    const-string v0, "whisperallow"

    .line 7110
    iget-boolean v4, p1, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 57
    if-eqz v4, :cond_b0

    :goto_85
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 58
    const-string v0, "volumemodifier"

    .line 7118
    iget v1, p1, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 58
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 61
    :try_start_97
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->i:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "contacts"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 62
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/b/c;->f()V
    :try_end_a3
    .catch Ljava/lang/Exception; {:try_start_97 .. :try_end_a3} :catch_b2

    .line 66
    :goto_a3
    return-wide v0

    :cond_a4
    move v0, v2

    .line 51
    goto :goto_32

    :cond_a6
    move v0, v2

    .line 52
    goto :goto_40

    :cond_a8
    move v0, v2

    .line 53
    goto :goto_4e

    :cond_aa
    move v0, v2

    .line 54
    goto :goto_5c

    :cond_ac
    move v0, v2

    .line 55
    goto :goto_6a

    :cond_ae
    move v0, v2

    .line 56
    goto :goto_78

    :cond_b0
    move v1, v2

    .line 57
    goto :goto_85

    .line 65
    :catch_b2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 66
    const-wide/16 v0, -0x1

    goto :goto_a3
.end method

.method public final a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 172
    if-nez p1, :cond_4

    .line 178
    :cond_3
    :goto_3
    return-object v0

    .line 174
    :cond_4
    invoke-static {p1}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 175
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/b/c;->j:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 176
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/c/a;

    goto :goto_3
.end method

.method public final a(J)Z
    .registers 8

    .prologue
    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->i:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "contacts"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "contact_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 81
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/b/c;->f()V

    .line 82
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/data/b/c;->c()V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1d} :catch_1f

    .line 83
    const/4 v0, 0x1

    .line 86
    :goto_1e
    return v0

    .line 85
    :catch_1f
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 86
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public final a(JLcom/teamspeak/ts3client/c/a;)Z
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 235
    const-string v0, "customname"

    .line 24030
    iget-object v4, p3, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 235
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v0, "u_identifier"

    .line 25022
    iget-object v4, p3, Lcom/teamspeak/ts3client/c/a;->c:Ljava/lang/String;

    .line 236
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v0, "display"

    .line 25046
    iget v4, p3, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 237
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 238
    const-string v0, "status"

    .line 25054
    iget v4, p3, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 238
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 239
    const-string v4, "mute"

    .line 25102
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 239
    if-eqz v0, :cond_b6

    move v0, v1

    :goto_32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 240
    const-string v4, "ignorepublicchat"

    .line 26094
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 240
    if-eqz v0, :cond_b9

    move v0, v1

    :goto_40
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 241
    const-string v4, "ignoreprivatechat"

    .line 27086
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 241
    if-eqz v0, :cond_bb

    move v0, v1

    :goto_4e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 242
    const-string v4, "ignorepokes"

    .line 28078
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 242
    if-eqz v0, :cond_bd

    move v0, v1

    :goto_5c
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 243
    const-string v4, "hideaway"

    .line 29070
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 243
    if-eqz v0, :cond_bf

    move v0, v1

    :goto_6a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 244
    const-string v4, "hideavatar"

    .line 30062
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 244
    if-eqz v0, :cond_c1

    move v0, v1

    :goto_78
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 245
    const-string v4, "whisperallow"

    .line 30110
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 245
    if-eqz v0, :cond_c3

    move v0, v1

    :goto_86
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 246
    const-string v0, "volumemodifier"

    .line 30118
    iget v4, p3, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 246
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 248
    :try_start_98
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->i:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "contacts"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "contact_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 249
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/b/c;->f()V

    .line 250
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/data/b/c;->c()V
    :try_end_b5
    .catch Ljava/lang/Exception; {:try_start_98 .. :try_end_b5} :catch_c5

    .line 254
    :goto_b5
    return v1

    :cond_b6
    move v0, v2

    .line 239
    goto/16 :goto_32

    :cond_b9
    move v0, v2

    .line 240
    goto :goto_40

    :cond_bb
    move v0, v2

    .line 241
    goto :goto_4e

    :cond_bd
    move v0, v2

    .line 242
    goto :goto_5c

    :cond_bf
    move v0, v2

    .line 243
    goto :goto_6a

    :cond_c1
    move v0, v2

    .line 244
    goto :goto_78

    :cond_c3
    move v0, v2

    .line 245
    goto :goto_86

    .line 253
    :catch_c5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v1, v2

    .line 254
    goto :goto_b5
.end method

.method public final b()Ljava/util/ArrayList;
    .registers 13

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 91
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 95
    :try_start_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->i:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "contacts"

    const/16 v2, 0xd

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "contact_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "u_identifier"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "customname"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "display"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "status"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "mute"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "ignorepublicchat"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "ignoreprivatechat"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "ignorepokes"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "hideaway"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "hideavatar"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "whisperallow"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "volumemodifier"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 97
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 98
    if-nez v0, :cond_6a

    .line 99
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v8

    .line 126
    :goto_69
    return-object v0

    .line 102
    :cond_6a
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_130

    .line 104
    :cond_70
    new-instance v2, Lcom/teamspeak/ts3client/c/a;

    invoke-direct {v2}, Lcom/teamspeak/ts3client/c/a;-><init>()V

    .line 105
    const-string v0, "contact_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 8042
    iput v0, v2, Lcom/teamspeak/ts3client/c/a;->b:I

    .line 106
    const-string v0, "u_identifier"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/c/a;->a(Ljava/lang/String;)V

    .line 107
    const-string v0, "customname"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 9034
    iput-object v0, v2, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 108
    const-string v0, "display"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 9050
    iput v0, v2, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 109
    const-string v0, "status"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 9058
    iput v0, v2, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 110
    const-string v0, "mute"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_136

    move v0, v10

    .line 9106
    :goto_bf
    iput-boolean v0, v2, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 111
    const-string v0, "ignorepublicchat"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_138

    move v0, v10

    .line 10098
    :goto_ce
    iput-boolean v0, v2, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 112
    const-string v0, "ignoreprivatechat"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_13a

    move v0, v10

    .line 11090
    :goto_dd
    iput-boolean v0, v2, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 113
    const-string v0, "ignorepokes"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_13c

    move v0, v10

    .line 12082
    :goto_ec
    iput-boolean v0, v2, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 114
    const-string v0, "hideaway"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_13e

    move v0, v10

    .line 13074
    :goto_fb
    iput-boolean v0, v2, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 115
    const-string v0, "hideavatar"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_140

    move v0, v10

    .line 14066
    :goto_10a
    iput-boolean v0, v2, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 116
    const-string v0, "whisperallow"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_142

    move v0, v10

    .line 14114
    :goto_119
    iput-boolean v0, v2, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 117
    const-string v0, "volumemodifier"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 14122
    iput v0, v2, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 118
    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_70

    .line 121
    :cond_130
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_133
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_133} :catch_144

    move-object v0, v9

    .line 126
    goto/16 :goto_69

    :cond_136
    move v0, v11

    .line 110
    goto :goto_bf

    :cond_138
    move v0, v11

    .line 111
    goto :goto_ce

    :cond_13a
    move v0, v11

    .line 112
    goto :goto_dd

    :cond_13c
    move v0, v11

    .line 113
    goto :goto_ec

    :cond_13e
    move v0, v11

    .line 114
    goto :goto_fb

    :cond_140
    move v0, v11

    .line 115
    goto :goto_10a

    :cond_142
    move v0, v11

    .line 116
    goto :goto_119

    .line 123
    :catch_144
    move-exception v0

    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    move-object v0, v8

    .line 124
    goto/16 :goto_69
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 224
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->c:Lcom/teamspeak/ts3client/c/b;

    if-eqz v0, :cond_9

    .line 225
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/c;->c:Lcom/teamspeak/ts3client/c/b;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/c/b;->a()V

    .line 226
    :cond_9
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 22061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 226
    if-eqz v0, :cond_3b

    .line 227
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 23061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23364
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 227
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/d;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_25
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 228
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->a()V

    goto :goto_25

    .line 231
    :cond_3b
    return-void
.end method
