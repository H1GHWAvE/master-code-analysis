.class public final Lcom/teamspeak/ts3client/data/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Landroid/text/Html$ImageGetter;

.field private static b:Ljava/util/regex/Pattern;

.field private static c:Ljava/util/regex/Pattern;

.field private static d:Ljava/util/regex/Pattern;

.field private static e:[Ljava/lang/String;

.field private static f:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 23
    new-instance v0, Lcom/teamspeak/ts3client/data/d/b;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/d/b;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/data/d/a;->a:Landroid/text/Html$ImageGetter;

    .line 32
    const-string v0, "<img src=\"(ts3image://.*?)\">"

    const/16 v1, 0x22

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/data/d/a;->b:Ljava/util/regex/Pattern;

    .line 33
    const-string v0, "<span style=\\\".*? font-size:([+|-]\\d+?pt);.*?\\\">"

    invoke-static {v0, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/data/d/a;->c:Ljava/util/regex/Pattern;

    .line 34
    const-string v0, "ts3image://(.*)\\?channel=(\\d*)(?:&|&amp;)path=(.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/data/d/a;->d:Ljava/util/regex/Pattern;

    .line 35
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "xx-small"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "x-small"

    aput-object v2, v0, v1

    const-string v1, "small"

    aput-object v1, v0, v3

    const-string v1, "medium"

    aput-object v1, v0, v4

    const/4 v1, 0x4

    const-string v2, "large"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "x-large"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "xx-large"

    aput-object v2, v0, v1

    sput-object v0, Lcom/teamspeak/ts3client/data/d/a;->e:[Ljava/lang/String;

    .line 36
    sput v4, Lcom/teamspeak/ts3client/data/d/a;->f:I

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/text/Spanned;
    .registers 5

    .prologue
    .line 39
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 40
    const-string v1, "<"

    const-string v2, "&lt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 41
    const-string v1, ">"

    const-string v2, "&gt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 42
    const-string v1, "\""

    const-string v2, "&quot;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 43
    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->y:Lcom/teamspeak/ts3client/jni/b;

    .line 1593
    iget v2, v2, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 43
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 44
    const-string v1, "\n"

    const-string v2, "<br>"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 45
    const-string v1, "\\n"

    const-string v2, "<br>"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 47
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;
    .registers 6

    .prologue
    .line 88
    const-string v0, "&"

    const-string v1, "&amp;"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 89
    const-string v1, "<"

    const-string v2, "&lt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 90
    const-string v1, ">"

    const-string v2, "&gt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string v1, "\""

    const-string v2, "&quot;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 92
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 3061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 92
    sget-object v2, Lcom/teamspeak/ts3client/jni/b;->y:Lcom/teamspeak/ts3client/jni/b;

    .line 3593
    iget v2, v2, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 92
    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 93
    const-string v1, "\n"

    const-string v2, "<br>"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 97
    return-object v0
.end method

.method public static a(Ljava/lang/String;JLcom/teamspeak/ts3client/data/d/c;)Ljava/lang/String;
    .registers 15
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x6

    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 52
    const-string v0, "&"

    const-string v2, "&amp;"

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 53
    const-string v2, "<"

    const-string v3, "&lt;"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 54
    const-string v2, ">"

    const-string v3, "&gt;"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 55
    const-string v2, "\""

    const-string v3, "&quot;"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 2061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 56
    sget-object v3, Lcom/teamspeak/ts3client/jni/b;->A:Lcom/teamspeak/ts3client/jni/b;

    .line 2593
    iget v3, v3, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 56
    invoke-virtual {v2, v0, v3, v9}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<body text=\"white\">"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</body>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 58
    const-string v2, "\\["

    const-string v3, "["

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 59
    const-string v2, "\\]"

    const-string v3, "]"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 60
    sget-object v2, Lcom/teamspeak/ts3client/data/d/a;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 61
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 62
    :goto_67
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 63
    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const-string v4, "pt"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "+"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 64
    sget v4, Lcom/teamspeak/ts3client/data/d/a;->f:I

    add-int/2addr v0, v4

    .line 65
    if-le v0, v1, :cond_8b

    move v0, v1

    .line 67
    :cond_8b
    if-gez v0, :cond_8e

    move v0, v6

    .line 69
    :cond_8e
    invoke-virtual {v2, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/teamspeak/ts3client/data/d/a;->e:[Ljava/lang/String;

    aget-object v0, v7, v0

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_67

    .line 71
    :cond_a6
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 72
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    .line 73
    sget-object v0, Lcom/teamspeak/ts3client/data/d/a;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    .line 74
    :goto_b3
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_ea

    .line 75
    new-instance v0, Lcom/teamspeak/ts3client/data/d/d;

    invoke-virtual {v8, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, p3

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/data/d/d;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/d/c;J)V

    .line 76
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_dd

    .line 77
    new-instance v1, Lcom/teamspeak/ts3client/data/d/e;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/data/d/e;-><init>()V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v9, [Lcom/teamspeak/ts3client/data/d/d;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/data/d/e;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_b3

    .line 79
    :cond_dd
    new-instance v1, Lcom/teamspeak/ts3client/data/d/e;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/data/d/e;-><init>()V

    new-array v2, v9, [Lcom/teamspeak/ts3client/data/d/d;

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_b3

    .line 81
    :cond_ea
    return-object v7
.end method

.method static synthetic a()Ljava/util/regex/Pattern;
    .registers 1

    .prologue
    .line 21
    sget-object v0, Lcom/teamspeak/ts3client/data/d/a;->d:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;
    .registers 7

    .prologue
    .line 101
    if-nez p1, :cond_7

    .line 102
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/a;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 103
    :goto_6
    return-object v0

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4088
    const-string v1, "&"

    const-string v2, "&amp;"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 4089
    const-string v2, "<"

    const-string v3, "&lt;"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 4090
    const-string v2, ">"

    const-string v3, "&gt;"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 4091
    const-string v2, "\""

    const-string v3, "&quot;"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 4092
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 5061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 4092
    sget-object v3, Lcom/teamspeak/ts3client/jni/b;->y:Lcom/teamspeak/ts3client/jni/b;

    .line 5593
    iget v3, v3, Lcom/teamspeak/ts3client/jni/b;->B:I

    .line 4092
    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v1

    .line 4093
    const-string v2, "\n"

    const-string v3, "<br>"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 4096
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_6
.end method
