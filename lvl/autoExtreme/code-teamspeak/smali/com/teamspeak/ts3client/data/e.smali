.class public final Lcom/teamspeak/ts3client/data/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation


# static fields
.field private static A:Ljava/util/regex/Pattern;


# instance fields
.field private B:Lcom/teamspeak/ts3client/data/b;

.field private C:Ljava/lang/String;

.field private D:I

.field private E:I

.field public a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

.field public b:Lcom/teamspeak/ts3client/a/k;

.field public c:Lcom/teamspeak/ts3client/jni/l;

.field public d:Lcom/teamspeak/ts3client/data/d;

.field public e:J

.field public f:Lcom/teamspeak/ts3client/data/g;

.field public g:J

.field public h:I

.field public i:Ljava/util/HashMap;

.field public j:Ljava/util/HashMap;

.field public k:Lcom/teamspeak/ts3client/t;

.field public l:Lcom/teamspeak/ts3client/ConnectionBackground;

.field public m:Lcom/teamspeak/ts3client/Ts3Application;

.field public n:I

.field public o:Lcom/teamspeak/ts3client/data/g/b;

.field public p:Lcom/teamspeak/ts3client/data/a/b;

.field public q:Ljava/lang/String;

.field public r:Lcom/teamspeak/ts3client/data/c/d;

.field public s:Lcom/teamspeak/ts3client/data/f/a;

.field public t:Ljava/lang/String;

.field public u:Lcom/teamspeak/ts3client/data/d/v;

.field public v:Lcom/teamspeak/ts3client/data/ab;

.field public w:Lcom/a/b/d/bw;

.field public x:I

.field public y:I

.field public z:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 39
    const-string v0, "(((\\\\/)*[^/])*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/data/e;->A:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 48
    iput v2, p0, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->j:Ljava/util/HashMap;

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 54
    iput v2, p0, Lcom/teamspeak/ts3client/data/e;->n:I

    .line 64
    invoke-static {}, Lcom/a/b/d/hy;->a()Lcom/a/b/d/hy;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->w:Lcom/a/b/d/bw;

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 73
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 74
    invoke-static {}, Lcom/teamspeak/ts3client/a/k;->a()Lcom/teamspeak/ts3client/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 75
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1110
    iput-object v1, v0, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 76
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/e;->E()V

    .line 77
    invoke-static {}, Lcom/teamspeak/ts3client/jni/l;->a()Lcom/teamspeak/ts3client/jni/l;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 78
    new-instance v0, Lcom/teamspeak/ts3client/data/b;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/b;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->B:Lcom/teamspeak/ts3client/data/b;

    .line 79
    new-instance v0, Lcom/teamspeak/ts3client/data/d;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/d;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 80
    new-instance v0, Lcom/teamspeak/ts3client/data/g/b;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/g/b;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->o:Lcom/teamspeak/ts3client/data/g/b;

    .line 81
    new-instance v0, Lcom/teamspeak/ts3client/data/a/b;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/a/b;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->p:Lcom/teamspeak/ts3client/data/a/b;

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/teamspeak/ts3client/data/ab;)V
    .registers 4

    .prologue
    .line 86
    invoke-direct {p0, p2}, Lcom/teamspeak/ts3client/data/e;-><init>(Landroid/content/Context;)V

    .line 87
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/e;->C:Ljava/lang/String;

    .line 88
    iput-object p3, p0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 89
    return-void
.end method

.method private A()Ljava/lang/String;
    .registers 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    return-object v0
.end method

.method private B()I
    .registers 5

    .prologue
    .line 295
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 295
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Loading lib"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 296
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a()I

    move-result v0

    .line 297
    if-lez v0, :cond_12

    .line 302
    :goto_11
    return v0

    .line 299
    :cond_12
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget v2, p0, Lcom/teamspeak/ts3client/data/e;->y:I

    iget v3, p0, Lcom/teamspeak/ts3client/data/e;->x:I

    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_prepareAudioDevice(II)I

    .line 300
    new-instance v1, Lcom/teamspeak/ts3client/data/f/a;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;-><init>(Lcom/teamspeak/ts3client/Ts3Application;)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 301
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_spawnNewServerConnectionHandler()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    goto :goto_11
.end method

.method private C()V
    .registers 5

    .prologue
    .line 306
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->b()V

    .line 307
    new-instance v0, Lcom/teamspeak/ts3client/data/f;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/f;-><init>(Lcom/teamspeak/ts3client/data/e;)V

    .line 313
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 314
    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 315
    return-void
.end method

.method private D()V
    .registers 4

    .prologue
    .line 348
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 35085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 348
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Stop AUDIO"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->f()V

    .line 351
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->e()V

    .line 353
    return-void
.end method

.method private E()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 373
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 35093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 373
    const-string v1, "playbackStream"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/e;->D:I

    .line 374
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 36093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 374
    const-string v1, "recordStream"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/e;->E:I

    .line 375
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 37093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 375
    const-string v1, "samplePlay"

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->i()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/e;->x:I

    .line 376
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 38093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 376
    const-string v1, "sampleRec"

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->h()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/e;->y:I

    .line 377
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget v1, p0, Lcom/teamspeak/ts3client/data/e;->y:I

    iget v2, p0, Lcom/teamspeak/ts3client/data/e;->x:I

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_prepareAudioDevice(II)I

    .line 378
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    iget v1, p0, Lcom/teamspeak/ts3client/data/e;->y:I

    iget v2, p0, Lcom/teamspeak/ts3client/data/e;->x:I

    iget v3, p0, Lcom/teamspeak/ts3client/data/e;->D:I

    iget v4, p0, Lcom/teamspeak/ts3client/data/e;->E:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/teamspeak/ts3client/a/k;->a(IIII)Z

    .line 380
    return-void
.end method

.method private F()J
    .registers 3

    .prologue
    .line 383
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/e;->z:J

    return-wide v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/data/e;)Lcom/teamspeak/ts3client/a/k;
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    return-object v0
.end method

.method private a(I)V
    .registers 10

    .prologue
    const/4 v4, 0x1

    .line 189
    iput p1, p0, Lcom/teamspeak/ts3client/data/e;->n:I

    .line 190
    iget v0, p0, Lcom/teamspeak/ts3client/data/e;->n:I

    const/16 v1, 0x207

    if-ne v0, v1, :cond_64

    .line 191
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->b()V

    .line 192
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f050028

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20185
    iget v2, v2, Lcom/teamspeak/ts3client/data/e;->n:I

    .line 192
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f050027

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iget-object v6, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f05001e

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 194
    :cond_64
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 194
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ClientLib Connect_return:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 195
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/ConnectionBackground;)V
    .registers 2

    .prologue
    .line 202
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 203
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/data/g;)V
    .registers 2

    .prologue
    .line 169
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 170
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/t;)V
    .registers 2

    .prologue
    .line 210
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 211
    return-void
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 246
    iput p1, p0, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 247
    return-void
.end method

.method private b(J)V
    .registers 4

    .prologue
    .line 387
    iput-wide p1, p0, Lcom/teamspeak/ts3client/data/e;->z:J

    .line 388
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 140
    invoke-static {}, Lcom/teamspeak/ts3client/data/d/q;->a()V

    .line 141
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v0

    .line 14092
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/data/v;->b:Z

    .line 142
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 14299
    iget-object v0, v0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    .line 15086
    iget-object v0, v0, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    .line 142
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 16063
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/ab;->l:J

    .line 142
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/data/b/a;->a(JLcom/teamspeak/ts3client/data/ab;)Z

    .line 143
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 143
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Disconnecting from Server"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v0, v2, v3, p1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_stopConnection(JLjava/lang/String;)I

    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 146
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 147
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_destroyServerConnectionHandler(J)I

    .line 148
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16188
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->k:Landroid/content/Intent;

    .line 148
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/Ts3Application;->stopService(Landroid/content/Intent;)Z

    .line 16306
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->b()V

    .line 16307
    new-instance v0, Lcom/teamspeak/ts3client/data/f;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/f;-><init>(Lcom/teamspeak/ts3client/data/e;)V

    .line 16313
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 16314
    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 150
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_unregisterCustomDevice(Ljava/lang/String;)I

    .line 151
    iput-object v4, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 152
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v0

    .line 17080
    iput-object v4, v0, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    .line 17081
    iput-object v4, v0, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    .line 17082
    iget-object v1, v0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 17083
    iget-object v1, v0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 17084
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    .line 17085
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, v0, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    .line 153
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 153
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "ClientLib closed"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 263
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/e;->t:Ljava/lang/String;

    .line 264
    return-void
.end method

.method private s()Lcom/teamspeak/ts3client/a/k;
    .registers 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    return-object v0
.end method

.method private t()Lcom/teamspeak/ts3client/data/g;
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    return-object v0
.end method

.method private u()Lcom/teamspeak/ts3client/data/f/a;
    .registers 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    return-object v0
.end method

.method private v()I
    .registers 2

    .prologue
    .line 185
    iget v0, p0, Lcom/teamspeak/ts3client/data/e;->n:I

    return v0
.end method

.method private w()Lcom/teamspeak/ts3client/ConnectionBackground;
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .registers 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->C:Ljava/lang/String;

    return-object v0
.end method

.method private y()Lcom/teamspeak/ts3client/jni/l;
    .registers 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    return-object v0
.end method

.method private z()Ljava/lang/String;
    .registers 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->t:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->w:Lcom/a/b/d/bw;

    return-object v0
.end method

.method public final a(J)V
    .registers 4

    .prologue
    .line 218
    iput-wide p1, p0, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 219
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/data/c/d;)V
    .registers 2

    .prologue
    .line 226
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/e;->r:Lcom/teamspeak/ts3client/data/c/d;

    .line 227
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/data/d/v;)V
    .registers 2

    .prologue
    .line 254
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 256
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 282
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 22195
    iput-object p1, v0, Lcom/teamspeak/ts3client/data/ab;->b:Ljava/lang/String;

    .line 283
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 23870
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/ap;

    invoke-direct {v2, v0}, Lcom/teamspeak/ts3client/ap;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 284
    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 24

    .prologue
    .line 96
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2263
    move-object/from16 v0, p1

    iput-object v0, v2, Lcom/teamspeak/ts3client/data/e;->t:Ljava/lang/String;

    .line 97
    const-string v2, ""

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 98
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->ad:Lcom/teamspeak/ts3client/jni/d;

    move-object/from16 v0, p7

    invoke-virtual {v2, v4, v5, v3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I

    .line 99
    :cond_27
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 100
    const/4 v11, 0x0

    .line 101
    const-string v3, "/"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16c

    .line 102
    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    const/4 v11, 0x1

    .line 114
    :cond_3d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    .line 115
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 117
    if-nez v14, :cond_5b

    .line 118
    const-string v14, ""

    .line 119
    :cond_5b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v15, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 119
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/teamspeak/ts3client/data/e;->C:Ljava/lang/String;

    move-object/from16 v7, p1

    move/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v12, p5

    move-object/from16 v13, p6

    invoke-virtual/range {v3 .. v14}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_startConnectionEx(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 4189
    iput v9, v15, Lcom/teamspeak/ts3client/data/e;->n:I

    .line 4190
    iget v2, v15, Lcom/teamspeak/ts3client/data/e;->n:I

    const/16 v3, 0x207

    if-ne v2, v3, :cond_e0

    .line 4191
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/b/f;->b()V

    .line 4192
    iget-object v2, v15, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5206
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 4192
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v15, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f050028

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v15, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6185
    iget v4, v4, Lcom/teamspeak/ts3client/data/e;->n:I

    .line 4192
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v15, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f050027

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-object v8, v15, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v8}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v10, 0x7f05001e

    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, Lcom/teamspeak/ts3client/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 4194
    :cond_e0
    iget-object v2, v15, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 4194
    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ClientLib Connect_return:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;

    move-result-object v2

    .line 7165
    new-instance v3, Lcom/teamspeak/ts3client/chat/a;

    const-string v4, "Server Tab"

    const-string v5, "SERVER"

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/teamspeak/ts3client/chat/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V

    iput-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    .line 7166
    iget-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    .line 8105
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/teamspeak/ts3client/chat/a;->b:Z

    .line 7167
    new-instance v3, Lcom/teamspeak/ts3client/chat/a;

    const-string v4, "Channel Tab"

    const-string v5, "CHANNEL"

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/teamspeak/ts3client/chat/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V

    iput-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    .line 7168
    iget-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    .line 9105
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/teamspeak/ts3client/chat/a;->b:Z

    .line 7169
    iget-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->c:Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/chat/d;->a(Lcom/teamspeak/ts3client/chat/a;)V

    .line 7170
    iget-object v3, v2, Lcom/teamspeak/ts3client/chat/d;->d:Lcom/teamspeak/ts3client/chat/a;

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/chat/d;->a(Lcom/teamspeak/ts3client/chat/a;)V

    .line 121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10206
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 10688
    iget-object v2, v2, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 121
    invoke-static {v2}, Lcom/teamspeak/ts3client/chat/d;->a(Landroid/support/v4/app/bi;)V

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11093
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 122
    const-string v3, "ptt_key_setting"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1ae

    .line 124
    new-instance v3, Ljava/util/BitSet;

    invoke-direct {v3}, Ljava/util/BitSet;-><init>()V

    .line 125
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 126
    array-length v5, v4

    const/4 v2, 0x0

    :goto_15e
    if-ge v2, v5, :cond_193

    aget-object v6, v4, v2

    .line 128
    :try_start_162
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/util/BitSet;->set(I)V
    :try_end_169
    .catch Ljava/lang/Exception; {:try_start_162 .. :try_end_169} :catch_1af

    .line 126
    :goto_169
    add-int/lit8 v2, v2, 0x1

    goto :goto_15e

    .line 105
    :cond_16c
    sget-object v3, Lcom/teamspeak/ts3client/data/e;->A:Ljava/util/regex/Pattern;

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 106
    :cond_174
    :goto_174
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 107
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 108
    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_174

    .line 109
    const-string v5, "\\/"

    const-string v6, "/"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 110
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_174

    .line 133
    :cond_193
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12093
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 133
    const-string v4, "ptt_key_setting_intercept"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 134
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Lcom/teamspeak/ts3client/data/v;->a(Ljava/util/BitSet;Z)V

    .line 135
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v2

    .line 13092
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/teamspeak/ts3client/data/v;->b:Z

    .line 137
    :cond_1ae
    return-void

    :catch_1af
    move-exception v6

    goto :goto_169
.end method

.method public final b()Lcom/teamspeak/ts3client/data/a/b;
    .registers 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->p:Lcom/teamspeak/ts3client/data/a/b;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 291
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 292
    return-void
.end method

.method public final declared-synchronized c()Lcom/teamspeak/ts3client/data/b;
    .registers 2

    .prologue
    .line 173
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->B:Lcom/teamspeak/ts3client/data/b;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/util/HashMap;
    .registers 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method public final e()Lcom/teamspeak/ts3client/t;
    .registers 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    return-object v0
.end method

.method public final f()J
    .registers 3

    .prologue
    .line 214
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/e;->g:J

    return-wide v0
.end method

.method public final g()Lcom/teamspeak/ts3client/data/c/d;
    .registers 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->r:Lcom/teamspeak/ts3client/data/c/d;

    return-object v0
.end method

.method public final h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
    .registers 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    return-object v0
.end method

.method public final i()I
    .registers 2

    .prologue
    .line 242
    iget v0, p0, Lcom/teamspeak/ts3client/data/e;->h:I

    return v0
.end method

.method public final j()Lcom/teamspeak/ts3client/data/d/v;
    .registers 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    return-object v0
.end method

.method public final k()J
    .registers 3

    .prologue
    .line 267
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    return-wide v0
.end method

.method public final l()Lcom/teamspeak/ts3client/data/g/b;
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->o:Lcom/teamspeak/ts3client/data/g/b;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .registers 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 21191
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->b:Ljava/lang/String;

    .line 275
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 276
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 22095
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 278
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 22191
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->b:Ljava/lang/String;

    goto :goto_10
.end method

.method public final n()V
    .registers 10

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 318
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 318
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Start AUDIO"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 319
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    .line 320
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_activateCaptureDevice(J)I

    .line 321
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    const-string v1, ""

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    const-string v1, ""

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_openPlaybackDevice(JLjava/lang/String;Ljava/lang/String;)I

    .line 323
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 323
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 27061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 323
    const-string v1, "agc"

    const-string v4, "true"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->g()V

    .line 326
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->c()V

    .line 327
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 327
    const-string v1, "voiceactivation_level"

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 328
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    const-string v4, "voiceactivation_level"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 29093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 329
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 330
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 330
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting PTT: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 331
    if-eqz v0, :cond_fc

    .line 332
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 332
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 32267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 332
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 333
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    const-string v1, "vad"

    const-string v4, "false"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 334
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    const-string v1, "voiceactivation_level"

    const-string v4, "-50"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 339
    :goto_ae
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 339
    const-string v1, "audio_bt"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 340
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 340
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting BT:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 341
    if-eqz v0, :cond_d7

    .line 342
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/a/k;->b(Z)V

    .line 344
    :cond_d7
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/e;->e:J

    const-string v1, "volume_modifier"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v5, 0x3f000000    # 0.5f

    iget-object v6, p0, Lcom/teamspeak/ts3client/data/e;->m:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34093
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 344
    const-string v7, "volume_modifier"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPlaybackConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 345
    return-void

    .line 336
    :cond_fc
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->d()V

    goto :goto_ae
.end method

.method public final o()Ljava/util/HashMap;
    .registers 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->j:Ljava/util/HashMap;

    return-object v0
.end method

.method public final p()Lcom/teamspeak/ts3client/data/ab;
    .registers 2

    .prologue
    .line 360
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    return-object v0
.end method

.method public final q()Lcom/teamspeak/ts3client/data/d;
    .registers 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    return-object v0
.end method

.method public final r()V
    .registers 1

    .prologue
    .line 368
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/e;->E()V

    .line 369
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/data/e;->n()V

    .line 370
    return-void
.end method
