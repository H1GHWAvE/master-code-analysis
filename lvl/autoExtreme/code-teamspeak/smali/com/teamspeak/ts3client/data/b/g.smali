.class final Lcom/teamspeak/ts3client/data/b/g;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/b/f;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/b/f;Landroid/content/Context;)V
    .registers 6

    .prologue
    .line 251
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/b/g;->a:Lcom/teamspeak/ts3client/data/b/f;

    .line 252
    const-string v0, "Teamspeak-Ident"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 253
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 3

    .prologue
    .line 294
    const-string v0, "Teamspeak-Ident"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 295
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 296
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 6

    .prologue
    .line 257
    const-string v0, "create table ident (ident_id integer primary key autoincrement, name text not null, identity text not null, nickname text not null, defaultid int not null);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 258
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_startInit()I

    .line 261
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3c

    .line 262
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_createIdentity()Ljava/lang/String;

    move-result-object v0

    .line 270
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 271
    const-string v2, "name"

    const-string v3, "Android Default"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v2, "identity"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v0, "nickname"

    const-string v2, "TeamSpeakAndroidUser"

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-string v0, "defaultid"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :try_start_36
    const-string v0, "ident"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_3c} :catch_3d

    .line 279
    :cond_3c
    :goto_3c
    return-void

    :catch_3d
    move-exception v0

    goto :goto_3c
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 4

    .prologue
    .line 291
    return-void
.end method
