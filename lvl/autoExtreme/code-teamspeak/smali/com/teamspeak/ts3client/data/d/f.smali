.class public final Lcom/teamspeak/ts3client/data/d/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/support/v4/n/j;

.field public b:Lcom/teamspeak/ts3client/Ts3Application;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25
    return-void
.end method

.method private a(Ljava/io/File;)V
    .registers 14

    .prologue
    const/16 v11, 0x10

    const/4 v1, 0x0

    .line 79
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_10

    .line 98
    :cond_f
    return-void

    .line 81
    :cond_10
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 82
    array-length v3, v2

    move v0, v1

    :goto_16
    if-ge v0, v3, :cond_f

    aget-object v4, v2, v0

    .line 83
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_8b

    .line 84
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8b

    .line 86
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x2e

    invoke-virtual {v6, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 87
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 88
    if-eqz v4, :cond_8b

    .line 90
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v11, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 91
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 92
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    rsub-int/lit8 v8, v8, 0x8

    int-to-float v8, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    rsub-int/lit8 v9, v9, 0x8

    int-to-float v9, v9

    const/4 v10, 0x0

    invoke-virtual {v7, v4, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 93
    invoke-virtual {v7}, Landroid/graphics/Canvas;->save()I

    .line 94
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/f;->c()I

    move-result v8

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/f;->c()I

    move-result v9

    const/4 v10, 0x1

    invoke-static {v6, v8, v9, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v4, v7, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {p0, v5, v4}, Lcom/teamspeak/ts3client/data/d/f;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 82
    :cond_8b
    add-int/lit8 v0, v0, 0x1

    goto :goto_16
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 4

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/teamspeak/ts3client/data/d/f;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_b

    .line 29
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/f;->a:Landroid/support/v4/n/j;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/n/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    :cond_b
    return-void
.end method

.method private b()V
    .registers 12

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 39
    const/high16 v2, 0x100000

    mul-int/2addr v0, v2

    div-int/lit8 v0, v0, 0xa

    .line 40
    new-instance v2, Lcom/teamspeak/ts3client/data/d/g;

    invoke-direct {v2, p0, v0}, Lcom/teamspeak/ts3client/data/d/g;-><init>(Lcom/teamspeak/ts3client/data/d/f;I)V

    iput-object v2, p0, Lcom/teamspeak/ts3client/data/d/f;->a:Landroid/support/v4/n/j;

    .line 1054
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    .line 1058
    :try_start_21
    const-string v0, "countries"

    invoke-virtual {v2, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_26} :catch_a0

    move-result-object v3

    .line 1062
    array-length v4, v3

    :goto_28
    if-ge v1, v4, :cond_a1

    aget-object v0, v3, v1

    .line 1064
    :try_start_2c
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "countries/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 1065
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1066
    const/16 v6, 0x10

    const/16 v7, 0x10

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1067
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1068
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    rsub-int/lit8 v8, v8, 0x8

    int-to-float v8, v8

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    rsub-int/lit8 v9, v9, 0x8

    int-to-float v9, v9

    const/4 v10, 0x0

    invoke-virtual {v7, v5, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1069
    invoke-virtual {v7}, Landroid/graphics/Canvas;->save()I

    .line 1070
    const/4 v5, 0x0

    const/16 v7, 0x2e

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    invoke-virtual {v0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/f;->c()I

    move-result v8

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/f;->c()I

    move-result v9

    const/4 v10, 0x1

    invoke-static {v6, v8, v9, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {p0, v0, v5}, Lcom/teamspeak/ts3client/data/d/f;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_95
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_95} :catch_99
    .catchall {:try_start_2c .. :try_end_95} :catchall_9e

    .line 1062
    :goto_95
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_28

    .line 1072
    :catch_99
    move-exception v0

    :try_start_9a
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9d
    .catchall {:try_start_9a .. :try_end_9d} :catchall_9e

    goto :goto_95

    .line 1073
    :catchall_9e
    move-exception v0

    throw v0

    .line 1060
    :catch_a0
    move-exception v0

    :cond_a1
    return-void
.end method

.method private c()I
    .registers 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 102
    const/high16 v1, 0x41800000    # 16.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/f;->a:Landroid/support/v4/n/j;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/j;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final a()V
    .registers 12

    .prologue
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    .line 58
    :try_start_7
    const-string v1, "countries"

    invoke-virtual {v2, v1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_c} :catch_87

    move-result-object v3

    .line 62
    array-length v4, v3

    move v1, v0

    :goto_f
    if-ge v1, v4, :cond_88

    aget-object v0, v3, v1

    .line 64
    :try_start_13
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "countries/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 65
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 66
    const/16 v6, 0x10

    const/16 v7, 0x10

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 67
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 68
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    rsub-int/lit8 v8, v8, 0x8

    int-to-float v8, v8

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    rsub-int/lit8 v9, v9, 0x8

    int-to-float v9, v9

    const/4 v10, 0x0

    invoke-virtual {v7, v5, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 69
    invoke-virtual {v7}, Landroid/graphics/Canvas;->save()I

    .line 70
    const/4 v5, 0x0

    const/16 v7, 0x2e

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    invoke-virtual {v0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/f;->c()I

    move-result v8

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/f;->c()I

    move-result v9

    const/4 v10, 0x1

    invoke-static {v6, v8, v9, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {p0, v0, v5}, Lcom/teamspeak/ts3client/data/d/f;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_7c
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_7c} :catch_80
    .catchall {:try_start_13 .. :try_end_7c} :catchall_85

    .line 62
    :goto_7c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 72
    :catch_80
    move-exception v0

    :try_start_81
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_84
    .catchall {:try_start_81 .. :try_end_84} :catchall_85

    goto :goto_7c

    .line 73
    :catchall_85
    move-exception v0

    throw v0

    .line 60
    :catch_87
    move-exception v0

    .line 76
    :cond_88
    return-void
.end method
