.class public final Lcom/teamspeak/ts3client/data/d/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation


# instance fields
.field a:Ljava/util/HashMap;

.field b:Ljava/util/HashMap;

.field public c:Z

.field private d:Ljava/lang/String;

.field private e:Lcom/teamspeak/ts3client/Ts3Application;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/TS3/cache/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->d:Ljava/lang/String;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/d/v;->c:Z

    .line 38
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 39
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/d/v;->a()Z

    move-result v0

    if-nez v0, :cond_49

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    .line 1061
    iget-object v0, p1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 42
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 2061
    iget-object v0, p1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 3061
    iget-object v1, p1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 43
    const-string v1, "request ServerPermissionList"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestPermissionList(JLjava/lang/String;)I

    .line 45
    :cond_49
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .registers 4

    .prologue
    .line 76
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 77
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 79
    :goto_18
    return-object v0

    :cond_19
    const-string v0, ""

    goto :goto_18
.end method

.method private a()Z
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16287
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 84
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->f:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 85
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 85
    sget-object v3, Lcom/teamspeak/ts3client/jni/i;->e:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v0, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v3

    .line 87
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2a

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 88
    :cond_2a
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 88
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Don\'t have a unique server id or version, aborting load permissions"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    move v0, v1

    .line 129
    :goto_36
    return v0

    .line 91
    :cond_37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/d/v;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20287
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 91
    const-string v5, "/"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 92
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_6c

    .line 94
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 95
    :cond_6c
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "perm.dat"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_8c

    move v0, v1

    .line 97
    goto :goto_36

    .line 100
    :cond_8c
    :try_start_8c
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 101
    new-instance v4, Ljava/io/ObjectInputStream;

    invoke-direct {v4, v5}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 102
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->g:Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->g:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_ba

    .line 105
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 105
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Found new ServerVersion, aborting load permissions"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 106
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V

    .line 107
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    move v0, v1

    .line 108
    goto/16 :goto_36

    .line 110
    :cond_ba
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    .line 111
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    .line 112
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V

    .line 113
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_d0
    .catch Ljava/io/FileNotFoundException; {:try_start_8c .. :try_end_d0} :catch_e0
    .catch Ljava/io/StreamCorruptedException; {:try_start_8c .. :try_end_d0} :catch_ef
    .catch Ljava/io/IOException; {:try_start_8c .. :try_end_d0} :catch_fe
    .catch Ljava/lang/ClassNotFoundException; {:try_start_8c .. :try_end_d0} :catch_10d

    .line 127
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 127
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "Loaded Permissions from perm.dat"

    invoke-virtual {v0, v1, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 128
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/data/d/v;->c:Z

    move v0, v2

    .line 129
    goto/16 :goto_36

    .line 115
    :catch_e0
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 115
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Failed to read new perm.dat, aborting load permissions"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    move v0, v1

    .line 116
    goto/16 :goto_36

    .line 118
    :catch_ef
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 118
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Failed to read new perm.dat, aborting load permissions"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    move v0, v1

    .line 119
    goto/16 :goto_36

    .line 121
    :catch_fe
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 121
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Failed to read new perm.dat, aborting load permissions"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    move v0, v1

    .line 122
    goto/16 :goto_36

    .line 124
    :catch_10d
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 124
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Failed to read new perm.dat, aborting load permissions"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    move v0, v1

    .line 125
    goto/16 :goto_36
.end method

.method private b()V
    .registers 5

    .prologue
    .line 133
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 27061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27287
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 133
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->f:Ljava/lang/String;

    .line 134
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 134
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 29061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 29267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 134
    sget-object v1, Lcom/teamspeak/ts3client/jni/i;->e:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->g:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2c

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 160
    :cond_2c
    :goto_2c
    return-void

    .line 137
    :cond_2d
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3d

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 138
    :cond_3d
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 138
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Don\'t have a unique server id or version, aborting load permissions"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_2c

    .line 141
    :cond_49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/v;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31287
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 141
    const-string v2, "/"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_7e

    .line 144
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 145
    :cond_7e
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "perm.dat"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 147
    :try_start_96
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 148
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 149
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/v;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 150
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 151
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 152
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 153
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_b5
    .catch Ljava/io/FileNotFoundException; {:try_start_96 .. :try_end_b5} :catch_b7
    .catch Ljava/io/IOException; {:try_start_96 .. :try_end_b5} :catch_c5

    goto/16 :goto_2c

    .line 155
    :catch_b7
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 155
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Failed to write new perm.dat, aborting save permissions"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_2c

    .line 157
    :catch_c5
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 157
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Failed to write new perm.dat, aborting save permissions"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_2c
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/d/v;->c:Z

    return v0
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/jni/g;)I
    .registers 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    .line 14331
    iget-object v1, p1, Lcom/teamspeak/ts3client/jni/g;->iy:Ljava/lang/String;

    .line 62
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 63
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    .line 15331
    iget-object v1, p1, Lcom/teamspeak/ts3client/jni/g;->iy:Ljava/lang/String;

    .line 63
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 65
    :goto_18
    return v0

    :cond_19
    const/4 v0, -0x1

    goto :goto_18
.end method

.method public final a(Ljava/lang/String;)I
    .registers 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 72
    :goto_14
    return v0

    :cond_15
    const/4 v0, -0x1

    goto :goto_14
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 6

    .prologue
    .line 49
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/PermissionList;

    if-eqz v0, :cond_21

    move-object v0, p1

    .line 50
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/PermissionList;

    .line 51
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    .line 4030
    iget v2, v0, Lcom/teamspeak/ts3client/jni/events/rare/PermissionList;->a:I

    .line 51
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 4034
    iget-object v3, v0, Lcom/teamspeak/ts3client/jni/events/rare/PermissionList;->b:Ljava/lang/String;

    .line 51
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    .line 5034
    iget-object v2, v0, Lcom/teamspeak/ts3client/jni/events/rare/PermissionList;->b:Ljava/lang/String;

    .line 6030
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/PermissionList;->a:I

    .line 52
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    :cond_21
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/PermissionListFinished;

    if-eqz v0, :cond_5d

    .line 55
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 55
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 7133
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8287
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 7133
    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->f:Ljava/lang/String;

    .line 7134
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 7134
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 7134
    sget-object v1, Lcom/teamspeak/ts3client/jni/i;->e:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->g:Ljava/lang/String;

    .line 7135
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5a

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 57
    :cond_5a
    :goto_5a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/d/v;->c:Z

    .line 59
    :cond_5d
    return-void

    .line 7137
    :cond_5e
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6e

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 7138
    :cond_6e
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 7138
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Don\'t have a unique server id or version, aborting load permissions"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_5a

    .line 7141
    :cond_7a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/v;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12287
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 7141
    const-string v2, "/"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7142
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 7143
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_af

    .line 7144
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 7145
    :cond_af
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "perm.dat"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 7147
    :try_start_c7
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 7148
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 7149
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/v;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 7150
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/v;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 7151
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/v;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 7152
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 7153
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_e6
    .catch Ljava/io/FileNotFoundException; {:try_start_c7 .. :try_end_e6} :catch_e8
    .catch Ljava/io/IOException; {:try_start_c7 .. :try_end_e6} :catch_f6

    goto/16 :goto_5a

    .line 7155
    :catch_e8
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 7155
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Failed to write new perm.dat, aborting save permissions"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_5a

    .line 7157
    :catch_f6
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/v;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 7157
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Failed to write new perm.dat, aborting save permissions"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_5a
.end method
