.class public Lcom/teamspeak/ts3client/OpenBrowserIntent;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/OpenBrowserIntent;->a:Z

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .registers 11

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 30
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 31
    const v2, 0x7fffffff

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_15
    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 33
    const-string v3, "com.teamspeak.ts3client.ConnectionBackground"

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 34
    iput-boolean v8, p0, Lcom/teamspeak/ts3client/OpenBrowserIntent;->a:Z

    goto :goto_15

    .line 37
    :cond_32
    new-instance v2, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v2}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 38
    if-eqz p1, :cond_68

    .line 39
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_69

    .line 40
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 82
    :cond_46
    :goto_46
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    const-string v1, "UrlStart"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 84
    const-string v1, "uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 85
    const-string v1, "address"

    .line 7055
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 85
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->startActivity(Landroid/content/Intent;)V

    .line 87
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->finish()V

    .line 89
    :cond_68
    return-void

    .line 44
    :cond_69
    :try_start_69
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    const-string v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 46
    const-string v4, "&"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_af

    .line 47
    const-string v4, "&"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v0, v1

    :goto_88
    if-ge v0, v5, :cond_c1

    aget-object v1, v4, v0

    .line 48
    const-string v6, "="

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 49
    array-length v6, v1

    if-le v6, v8, :cond_a1

    .line 50
    const/4 v6, 0x0

    aget-object v6, v1, v6

    const/4 v7, 0x1

    aget-object v1, v1, v7

    invoke-virtual {v3, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :goto_9e
    add-int/lit8 v0, v0, 0x1

    goto :goto_88

    .line 52
    :cond_a1
    const/4 v6, 0x0

    aget-object v1, v1, v6

    const-string v6, ""

    invoke-virtual {v3, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_a9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_69 .. :try_end_a9} :catch_aa

    goto :goto_9e

    .line 78
    :catch_aa
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_46

    .line 55
    :cond_af
    :try_start_af
    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 56
    array-length v1, v0

    if-le v1, v8, :cond_162

    .line 57
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :cond_c1
    :goto_c1
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 62
    const-string v0, "port"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f4

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5055
    iget-object v1, v2, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "port"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 64
    :cond_f4
    const-string v0, "nickname"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_106

    .line 65
    const-string v0, "nickname"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5107
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 66
    :cond_106
    const-string v0, "password"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_118

    .line 67
    const-string v0, "password"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5115
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 68
    :cond_118
    const-string v0, "channel"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12a

    .line 69
    const-string v0, "channel"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6075
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 70
    :cond_12a
    const-string v0, "channelpassword"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13c

    .line 71
    const-string v0, "channelpassword"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6083
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 72
    :cond_13c
    const-string v0, "addbookmark"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14e

    .line 73
    const-string v0, "addbookmark"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6099
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 74
    :cond_14e
    const-string v0, "token"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 75
    const-string v0, "token"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6140
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    goto/16 :goto_46

    .line 59
    :cond_162
    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, ""

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_16a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_af .. :try_end_16a} :catch_aa

    goto/16 :goto_c1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 12

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x1

    .line 22
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 24
    if-eqz v2, :cond_75

    .line 1030
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1031
    const v3, 0x7fffffff

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_22
    :goto_22
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1033
    const-string v4, "com.teamspeak.ts3client.ConnectionBackground"

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1034
    iput-boolean v9, p0, Lcom/teamspeak/ts3client/OpenBrowserIntent;->a:Z

    goto :goto_22

    .line 1037
    :cond_3f
    new-instance v3, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v3}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 1038
    if-eqz v2, :cond_75

    .line 1039
    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_76

    .line 1040
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 1082
    :cond_53
    :goto_53
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v4, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1083
    const-string v1, "UrlStart"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1084
    const-string v1, "uri"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1085
    const-string v1, "address"

    .line 4055
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 1085
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1086
    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->startActivity(Landroid/content/Intent;)V

    .line 1087
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/OpenBrowserIntent;->finish()V

    .line 26
    :cond_75
    return-void

    .line 1044
    :cond_76
    :try_start_76
    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    const-string v4, "UTF-8"

    invoke-static {v0, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1045
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1046
    const-string v5, "&"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_bc

    .line 1047
    const-string v5, "&"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v0, v1

    :goto_95
    if-ge v0, v6, :cond_ce

    aget-object v1, v5, v0

    .line 1048
    const-string v7, "="

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1049
    array-length v7, v1

    if-le v7, v9, :cond_ae

    .line 1050
    const/4 v7, 0x0

    aget-object v7, v1, v7

    const/4 v8, 0x1

    aget-object v1, v1, v8

    invoke-virtual {v4, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1047
    :goto_ab
    add-int/lit8 v0, v0, 0x1

    goto :goto_95

    .line 1052
    :cond_ae
    const/4 v7, 0x0

    aget-object v1, v1, v7

    const-string v7, ""

    invoke-virtual {v4, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_b6
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_76 .. :try_end_b6} :catch_b7

    goto :goto_ab

    .line 1078
    :catch_b7
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_53

    .line 1055
    :cond_bc
    :try_start_bc
    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1056
    array-length v1, v0

    if-le v1, v9, :cond_16f

    .line 1057
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v5, 0x1

    aget-object v0, v0, v5

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1061
    :cond_ce
    :goto_ce
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 1062
    const-string v0, "port"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_101

    .line 1063
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2055
    iget-object v1, v3, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 1063
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "port"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 1064
    :cond_101
    const-string v0, "nickname"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_113

    .line 1065
    const-string v0, "nickname"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2107
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 1066
    :cond_113
    const-string v0, "password"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_125

    .line 1067
    const-string v0, "password"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2115
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 1068
    :cond_125
    const-string v0, "channel"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_137

    .line 1069
    const-string v0, "channel"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3075
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 1070
    :cond_137
    const-string v0, "channelpassword"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_149

    .line 1071
    const-string v0, "channelpassword"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3083
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 1072
    :cond_149
    const-string v0, "addbookmark"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15b

    .line 1073
    const-string v0, "addbookmark"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3099
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 1074
    :cond_15b
    const-string v0, "token"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 1075
    const-string v0, "token"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3140
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    goto/16 :goto_53

    .line 1059
    :cond_16f
    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, ""

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_177
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_bc .. :try_end_177} :catch_b7

    goto/16 :goto_ce
.end method
