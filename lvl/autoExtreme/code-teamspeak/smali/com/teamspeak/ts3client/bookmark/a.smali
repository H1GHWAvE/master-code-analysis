.class public final Lcom/teamspeak/ts3client/bookmark/a;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private aA:Lcom/teamspeak/ts3client/data/ab;

.field private aB:Lcom/teamspeak/ts3client/Ts3Application;

.field private aC:Landroid/widget/Spinner;

.field private at:Lcom/teamspeak/ts3client/data/b/a;

.field private au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

.field private av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

.field private aw:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

.field private ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

.field private ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

.field private az:Lcom/teamspeak/ts3client/bookmark/CustomEditText;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/b/a;)V
    .registers 3

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 42
    iput-object p1, p0, Lcom/teamspeak/ts3client/bookmark/a;->at:Lcom/teamspeak/ts3client/data/b/a;

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/teamspeak/ts3client/data/b/a;Lcom/teamspeak/ts3client/data/ab;)V
    .registers 4

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 46
    iput-object p1, p0, Lcom/teamspeak/ts3client/bookmark/a;->at:Lcom/teamspeak/ts3client/data/b/a;

    .line 47
    iput-object p2, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/bookmark/a;)Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 28
    .line 7051
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 7052
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    .line 7065
    :goto_19
    return v0

    .line 7055
    :cond_1a
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    .line 7057
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 7058
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    goto :goto_19

    .line 7061
    :cond_37
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    .line 7063
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5b

    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_61

    .line 7064
    :cond_5b
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    goto :goto_19

    .line 7067
    :cond_61
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    move v0, v1

    .line 28
    goto :goto_19
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/data/ab;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->aw:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->az:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/bookmark/a;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->aC:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/data/b/a;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->at:Lcom/teamspeak/ts3client/data/b/a;

    return-object v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/bookmark/a;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/a;->aB:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method private y()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 51
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 52
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    .line 70
    :goto_19
    return v0

    .line 55
    :cond_1a
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    .line 57
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 58
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    goto :goto_19

    .line 61
    :cond_37
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    .line 63
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5b

    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_61

    .line 64
    :cond_5b
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    goto :goto_19

    .line 67
    :cond_61
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setInputerror(Z)V

    move v0, v1

    .line 70
    goto :goto_19
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11

    .prologue
    const/4 v3, 0x0

    .line 81
    const v0, 0x7f03001d

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 82
    const v1, 0x7f0c009b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 83
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/a;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->aB:Lcom/teamspeak/ts3client/Ts3Application;

    .line 84
    const-string v2, "bookmark.entry.label"

    const v4, 0x7f0c008d

    invoke-static {v2, v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 85
    const-string v2, "bookmark.entry.serveraddress"

    const v4, 0x7f0c008f

    invoke-static {v2, v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 86
    const-string v2, "bookmark.entry.serverpassword"

    const v4, 0x7f0c0091

    invoke-static {v2, v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 87
    const-string v2, "bookmark.entry.nickname"

    const v4, 0x7f0c0093

    invoke-static {v2, v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 88
    const-string v2, "bookmark.entry.defaultchannel"

    const v4, 0x7f0c0095

    invoke-static {v2, v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 89
    const-string v2, "bookmark.entry.defaultchannelpassword"

    const v4, 0x7f0c0097

    invoke-static {v2, v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 90
    const-string v2, "bookmark.entry.ident"

    const v4, 0x7f0c0099

    invoke-static {v2, v0, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 91
    const-string v2, "button.save"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 93
    const v2, 0x7f0c008e

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iput-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    .line 94
    const v2, 0x7f0c0090

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iput-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    .line 95
    const v2, 0x7f0c0092

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iput-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->aw:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    .line 96
    const v2, 0x7f0c0094

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iput-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    .line 97
    const v2, 0x7f0c0096

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iput-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    .line 98
    const v2, 0x7f0c0098

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iput-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->az:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    .line 99
    const v2, 0x7f0c009a

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->aC:Landroid/widget/Spinner;

    .line 100
    iget-object v2, p0, Lcom/teamspeak/ts3client/bookmark/a;->aB:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v4, "input_method"

    invoke-virtual {v2, v4}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 101
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v5, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInputFromWindow(Landroid/os/IBinder;II)V

    .line 103
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/b/f;->c()Ljava/util/ArrayList;

    move-result-object v5

    .line 104
    if-eqz v5, :cond_146

    .line 105
    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/bookmark/a;->i()Landroid/support/v4/app/bb;

    move-result-object v6

    const v7, 0x1090008

    invoke-direct {v4, v6, v7, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 106
    iget-object v6, p0, Lcom/teamspeak/ts3client/bookmark/a;->aC:Landroid/widget/Spinner;

    invoke-virtual {v6, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 110
    :goto_dc
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    if-eqz v4, :cond_14e

    .line 111
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/a;->au:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iget-object v6, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 1095
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 111
    invoke-virtual {v4, v6}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/a;->av:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iget-object v6, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 2055
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 112
    invoke-virtual {v4, v6}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/a;->aw:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iget-object v6, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 2111
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 113
    invoke-virtual {v4, v6}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/a;->ay:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iget-object v6, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 3103
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 114
    invoke-virtual {v4, v6}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/a;->ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iget-object v6, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 4071
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 115
    invoke-virtual {v4, v6}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v4, p0, Lcom/teamspeak/ts3client/bookmark/a;->az:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iget-object v6, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 4079
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 116
    invoke-virtual {v4, v6}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setText(Ljava/lang/CharSequence;)V

    .line 117
    if-eqz v5, :cond_132

    move v4, v3

    .line 118
    :goto_119
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v4, v3, :cond_132

    .line 119
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/teamspeak/ts3client/e/a;

    .line 5025
    iget v3, v3, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 119
    iget-object v6, p0, Lcom/teamspeak/ts3client/bookmark/a;->aA:Lcom/teamspeak/ts3client/data/ab;

    .line 5087
    iget v6, v6, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 119
    if-ne v3, v6, :cond_14a

    .line 120
    iget-object v3, p0, Lcom/teamspeak/ts3client/bookmark/a;->aC:Landroid/widget/Spinner;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 136
    :cond_132
    :goto_132
    new-instance v3, Lcom/teamspeak/ts3client/bookmark/b;

    invoke-direct {v3, p0, v2}, Lcom/teamspeak/ts3client/bookmark/b;-><init>(Lcom/teamspeak/ts3client/bookmark/a;Landroid/view/inputmethod/InputMethodManager;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 6207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 165
    const-string v2, "bookmark.entry.title"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 166
    return-object v0

    .line 108
    :cond_146
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_dc

    .line 118
    :cond_14a
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_119

    .line 127
    :cond_14e
    if-eqz v5, :cond_132

    move v4, v3

    .line 128
    :goto_151
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v4, v3, :cond_132

    .line 129
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/teamspeak/ts3client/e/a;

    .line 6057
    iget-boolean v3, v3, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 129
    if-eqz v3, :cond_167

    .line 130
    iget-object v3, p0, Lcom/teamspeak/ts3client/bookmark/a;->aC:Landroid/widget/Spinner;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_132

    .line 128
    :cond_167
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_151
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->a(Landroid/os/Bundle;)V

    .line 77
    return-void
.end method

.method public final a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 174
    invoke-virtual {p1}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 175
    invoke-virtual {v0, p0, p2}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 176
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->j()I

    .line 177
    return-void
.end method
