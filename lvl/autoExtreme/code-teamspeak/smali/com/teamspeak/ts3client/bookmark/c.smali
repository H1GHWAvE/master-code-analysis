.class public final Lcom/teamspeak/ts3client/bookmark/c;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/teamspeak/ts3client/bookmark/c;->b:Landroid/content/Context;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    .line 28
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->c:Landroid/view/LayoutInflater;

    .line 29
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/data/ab;)V
    .registers 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 33
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    :cond_d
    return-void
.end method

.method private b(Lcom/teamspeak/ts3client/data/ab;)V
    .registers 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 71
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 72
    :cond_d
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 48
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8

    .prologue
    const/high16 v3, 0x42000000    # 32.0f

    .line 54
    if-nez p2, :cond_40

    .line 55
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03001f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 56
    const v0, 0x7f0c009f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->d:Landroid/widget/ImageView;

    .line 57
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->d:Landroid/widget/ImageView;

    const v1, 0x7f02004b

    invoke-static {v1, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 58
    const v0, 0x7f0c00a0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 59
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 64
    :goto_32
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/ab;

    .line 1095
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 64
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    return-object p2

    .line 62
    :cond_40
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v1, v0

    goto :goto_32
.end method
