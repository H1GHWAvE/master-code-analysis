.class public Lcom/teamspeak/ts3client/StartGUIFragment;
.super Landroid/support/v7/app/i;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/support/v4/app/bk;
.implements Lcom/teamspeak/ts3client/d/l;
.implements Lcom/teamspeak/ts3client/data/z;


# static fields
.field public static final m:Ljava/lang/String; = "preferences"

.field private static final t:Ljava/lang/String; = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo0t0xR6Ch8m7hhrWgMBjBBdSaOA/wFTSk27DMKnqsYrw/AgxJjK07KyxMnRYojLsX34Cy3TCmPbcK3V9mKCjwlakgBYTObiVKRnnF1wu20WmsOb0wjbRbhYGDbhmAwc7Sy8sXNehiuz/O+PTSHcwWvHL20zum2cmHDmCAjREb1ZlA4myNN/tY9DYNMkQjOpMqgOCIQbyXdJLinOzwrgeX7x2IXMOJ7bbxWEmuyRXviOijp/1hQfpweo6slwfuc9iQDPBQl77M9dObMUrr+/23pxEE2i+U8WsePwTIw4uxkyEjew3ZRkVmFTtTqdp77tcOOFJ2hn4KkhLkM0hSaLLRQIDAQAB"

.field private static final u:[B

.field private static final v:Ljava/lang/String; = "Axdk3D"

.field private static final w:I = 0x1d32

.field private static x:Ljava/lang/String;


# instance fields
.field private A:Lcom/teamspeak/ts3client/data/b/c;

.field private B:Landroid/app/AlertDialog;

.field private C:Z

.field private D:I

.field private E:Lcom/teamspeak/ts3client/co;

.field private F:Landroid/os/PowerManager$WakeLock;

.field private G:Landroid/hardware/SensorManager;

.field private H:Landroid/hardware/Sensor;

.field private I:I

.field private J:Landroid/os/Handler;

.field private K:Lcom/teamspeak/ts3client/cm;

.field private L:Lcom/a/a/a/a/k;

.field private M:Landroid/view/View;

.field private N:Ljava/lang/String;

.field private O:Lcom/teamspeak/ts3client/d/c/a;

.field private P:Lcom/teamspeak/ts3client/d/t;

.field public n:Lcom/teamspeak/ts3client/Ts3Application;

.field public o:Lcom/teamspeak/ts3client/bookmark/e;

.field p:Z

.field public q:Lcom/teamspeak/ts3client/b;

.field r:Z

.field s:Z

.field private y:Landroid/os/Bundle;

.field private z:Lcom/teamspeak/ts3client/data/b/f;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 113
    const/16 v0, 0x14

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lcom/teamspeak/ts3client/StartGUIFragment;->u:[B

    .line 116
    const/4 v0, 0x0

    sput-object v0, Lcom/teamspeak/ts3client/StartGUIFragment;->x:Ljava/lang/String;

    return-void

    .line 113
    nop

    :array_e
    .array-data 1
        0x6ft
        -0x2et
        0x5ct
        0x17t
        -0x6ft
        0x5ft
        0x10t
        0x17t
        -0x69t
        0x47t
        -0x3et
        0x4et
        0x23t
        0x2ft
        -0x79t
        -0xct
        -0x6ct
        0xct
        -0x23t
        -0x2et
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 108
    invoke-direct {p0}, Landroid/support/v7/app/i;-><init>()V

    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->C:Z

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->p:Z

    .line 128
    const/4 v0, 0x2

    iput v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->D:I

    .line 141
    const-string v0, "734ea6e2d3c02045caa058dd1a7bb1b8"

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->N:Ljava/lang/String;

    .line 1246
    return-void
.end method

.method private A()V
    .registers 4

    .prologue
    .line 798
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 799
    const/4 v1, 0x3

    const-string v2, "Ts3WifiLOCK"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    .line 800
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 801
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50167
    iput-object v0, v1, Lcom/teamspeak/ts3client/Ts3Application;->n:Landroid/net/wifi/WifiManager$WifiLock;

    .line 802
    return-void
.end method

.method private B()V
    .registers 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->N:Ljava/lang/String;

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 896
    return-void
.end method

.method private C()V
    .registers 2

    .prologue
    .line 899
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->r:Z

    .line 900
    return-void
.end method

.method private D()V
    .registers 11

    .prologue
    const/4 v0, 0x0

    .line 924
    const-string v1, "com.teamspeak.ts"

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    .line 926
    :try_start_7
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/TS3/logs/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 927
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2c

    .line 928
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 930
    :cond_2c
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 931
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x240c8400

    sub-long/2addr v4, v6

    .line 932
    if-eqz v2, :cond_4d

    .line 933
    array-length v3, v2

    :goto_3b
    if-ge v0, v3, :cond_4d

    aget-object v6, v2, v0

    .line 934
    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v7, v8, v4

    if-gez v7, :cond_4a

    .line 935
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 933
    :cond_4a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3b

    .line 939
    :cond_4d
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50223
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 939
    const-string v2, "create_debuglog"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 940
    new-instance v0, Ljava/util/logging/FileHandler;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/TS3/logs/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy_MM_dd_HH_mm_ss"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".log"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/logging/FileHandler;-><init>(Ljava/lang/String;)V

    .line 941
    new-instance v2, Ljava/util/logging/SimpleFormatter;

    invoke-direct {v2}, Ljava/util/logging/SimpleFormatter;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/logging/FileHandler;->setFormatter(Ljava/util/logging/Formatter;)V

    .line 942
    invoke-virtual {v1, v0}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V
    :try_end_9b
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_9b} :catch_a5

    .line 947
    :cond_9b
    :goto_9b
    sget-object v0, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    invoke-virtual {v1, v0}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 948
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50224
    iput-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 949
    return-void

    :catch_a5
    move-exception v0

    goto :goto_9b
.end method

.method private E()V
    .registers 4

    .prologue
    .line 953
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/TS3/content/lang"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 954
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_25

    .line 955
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 956
    :cond_25
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/TS3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 957
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/Update.apk"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 958
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 959
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 960
    :cond_6c
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/TS3/cache"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 961
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_91

    .line 962
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 963
    :cond_91
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/.nomedia"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 964
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_b6

    .line 966
    :try_start_b3
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_b6
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_b6} :catch_b7

    .line 972
    :cond_b6
    :goto_b6
    return-void

    .line 969
    :catch_b7
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50226
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 969
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "Could not create .nomedia File"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_b6
.end method

.method private F()V
    .registers 4

    .prologue
    .line 976
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->f()I

    move-result v0

    if-lez v0, :cond_11

    .line 977
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->e()V

    .line 979
    :cond_11
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 980
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50227
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 980
    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 981
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v2, "Bookmark"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 982
    const/16 v1, 0x2002

    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 983
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 985
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->l()V

    .line 986
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50228
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50229
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 50230
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->B()V

    .line 50231
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50233
    iput-object v0, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 987
    return-void
.end method

.method private G()Lcom/teamspeak/ts3client/b;
    .registers 2

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->q:Lcom/teamspeak/ts3client/b;

    return-object v0
.end method

.method private H()V
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 1086
    const-string v1, "preferences"

    invoke-virtual {p0, v1, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1087
    const-string v2, "version"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1090
    :try_start_e
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v0, v3, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_e .. :try_end_1d} :catch_37
    .catch Ljava/lang/NullPointerException; {:try_start_e .. :try_end_1d} :catch_35

    .line 1094
    :goto_1d
    if-ge v2, v0, :cond_34

    .line 1095
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1096
    const-string v2, "version"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1097
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1098
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    if-nez v0, :cond_34

    .line 1099
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->q:Lcom/teamspeak/ts3client/b;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/b;->show()V

    .line 1102
    :cond_34
    return-void

    :catch_35
    move-exception v3

    goto :goto_1d

    .line 1093
    :catch_37
    move-exception v3

    goto :goto_1d
.end method

.method private I()V
    .registers 5

    .prologue
    .line 1144
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    .line 1145
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const-string v1, "Permission missing"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1146
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const-string v1, "We are missing a required permission!"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1147
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    const-string v2, "button.exit"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/cb;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/cb;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1155
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 1156
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1157
    return-void
.end method

.method private J()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 1160
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_b

    .line 1161
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->w()V

    .line 50313
    :goto_a
    return-void

    .line 50311
    :cond_b
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v0}, Landroid/support/v4/c/h;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 50312
    if-eqz v0, :cond_1e

    .line 50313
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    invoke-static {p0, v0, v3}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_a

    .line 50317
    :cond_1e
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->w()V

    .line 50318
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->L()V

    goto :goto_a
.end method

.method private K()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 1168
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v0}, Landroid/support/v4/c/h;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 1169
    if-eqz v0, :cond_14

    .line 1170
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    invoke-static {p0, v0, v3}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 1177
    :goto_13
    return-void

    .line 1174
    :cond_14
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->w()V

    .line 1175
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->L()V

    goto :goto_13
.end method

.method private L()V
    .registers 4

    .prologue
    .line 1180
    const-string v0, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {p0, v0}, Landroid/support/v4/c/h;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 1181
    if-eqz v0, :cond_15

    .line 1182
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 1188
    :goto_14
    return-void

    .line 1186
    :cond_15
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->M()V

    goto :goto_14
.end method

.method private M()V
    .registers 4

    .prologue
    .line 1191
    const-string v0, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {p0, v0}, Landroid/support/v4/c/h;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 1192
    if-eqz v0, :cond_14

    .line 1193
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_PHONE_STATE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p0, v0, v1}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 1196
    :cond_14
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/StartGUIFragment;I)I
    .registers 2

    .prologue
    .line 108
    iput p1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->D:I

    return p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/StartGUIFragment;)Landroid/app/AlertDialog;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private static declared-synchronized a(Landroid/content/Context;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 146
    const-class v1, Lcom/teamspeak/ts3client/StartGUIFragment;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/teamspeak/ts3client/StartGUIFragment;->x:Ljava/lang/String;

    if-nez v0, :cond_31

    .line 147
    const-string v0, "Axdk3D"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 148
    const-string v2, "Axdk3D"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 149
    sput-object v2, Lcom/teamspeak/ts3client/StartGUIFragment;->x:Ljava/lang/String;

    if-nez v2, :cond_31

    .line 150
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/teamspeak/ts3client/StartGUIFragment;->x:Ljava/lang/String;

    .line 151
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 152
    const-string v2, "Axdk3D"

    sget-object v3, Lcom/teamspeak/ts3client/StartGUIFragment;->x:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 153
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 156
    :cond_31
    sget-object v0, Lcom/teamspeak/ts3client/StartGUIFragment;->x:Ljava/lang/String;
    :try_end_33
    .catchall {:try_start_3 .. :try_end_33} :catchall_35

    monitor-exit v1

    return-object v0

    .line 146
    :catchall_35
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/net/Uri;)V
    .registers 10

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 194
    if-eqz p1, :cond_48

    .line 197
    :try_start_4
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 198
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 199
    const-string v3, "&"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_49

    .line 200
    const-string v3, "&"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v3, v1

    :goto_22
    if-ge v0, v3, :cond_5b

    aget-object v4, v1, v0

    .line 201
    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 202
    array-length v5, v4

    if-le v5, v7, :cond_3b

    .line 203
    const/4 v5, 0x0

    aget-object v5, v4, v5

    const/4 v6, 0x1

    aget-object v4, v4, v6

    invoke-virtual {v2, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    :goto_38
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 205
    :cond_3b
    const/4 v5, 0x0

    aget-object v4, v4, v5

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_43
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_43} :catch_44

    goto :goto_38

    .line 235
    :catch_44
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 239
    :cond_48
    :goto_48
    return-void

    .line 208
    :cond_49
    :try_start_49
    const-string v0, "="

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 209
    array-length v1, v0

    if-le v1, v7, :cond_112

    .line 210
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    :cond_5b
    :goto_5b
    new-instance v1, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 215
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 216
    const-string v0, "port"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_93

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2055
    iget-object v3, v1, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 217
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v0, "port"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 218
    :cond_93
    const-string v0, "nickname"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 219
    const-string v0, "nickname"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2107
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 220
    :cond_a5
    const-string v0, "password"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 221
    const-string v0, "password"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2115
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 222
    :cond_b7
    const-string v0, "channel"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c9

    .line 223
    const-string v0, "channel"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3075
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 224
    :cond_c9
    const-string v0, "channelpassword"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_db

    .line 225
    const-string v0, "channelpassword"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3083
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 226
    :cond_db
    const-string v0, "addbookmark"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 227
    const-string v0, "addbookmark"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3099
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 228
    :cond_ed
    const-string v0, "token"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ff

    .line 229
    const-string v0, "token"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3140
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    .line 230
    :cond_ff
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->d()Lcom/teamspeak/ts3client/e/a;

    move-result-object v0

    .line 4025
    iget v0, v0, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 4091
    iput v0, v1, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 231
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/bookmark/e;->a(Lcom/teamspeak/ts3client/data/ab;)V

    goto/16 :goto_48

    .line 212
    :cond_112
    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_11a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_49 .. :try_end_11a} :catch_44

    goto/16 :goto_5b
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;)V
    .registers 10

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 50354
    if-eqz p1, :cond_48

    .line 50357
    :try_start_4
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50358
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 50359
    const-string v3, "&"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_49

    .line 50360
    const-string v3, "&"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v3, v1

    :goto_22
    if-ge v0, v3, :cond_5b

    aget-object v4, v1, v0

    .line 50361
    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 50362
    array-length v5, v4

    if-le v5, v7, :cond_3b

    .line 50363
    const/4 v5, 0x0

    aget-object v5, v4, v5

    const/4 v6, 0x1

    aget-object v4, v4, v6

    invoke-virtual {v2, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50360
    :goto_38
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 50365
    :cond_3b
    const/4 v5, 0x0

    aget-object v4, v4, v5

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_43
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_43} :catch_44

    goto :goto_38

    .line 50395
    :catch_44
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 50396
    :cond_48
    :goto_48
    return-void

    .line 50368
    :cond_49
    :try_start_49
    const-string v0, "="

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 50369
    array-length v1, v0

    if-le v1, v7, :cond_112

    .line 50370
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50374
    :cond_5b
    :goto_5b
    new-instance v1, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 50375
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 50376
    const-string v0, "port"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_93

    .line 50377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 50400
    iget-object v3, v1, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 50377
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v0, "port"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 50378
    :cond_93
    const-string v0, "nickname"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 50379
    const-string v0, "nickname"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50401
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 50380
    :cond_a5
    const-string v0, "password"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 50381
    const-string v0, "password"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50403
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 50382
    :cond_b7
    const-string v0, "channel"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c9

    .line 50383
    const-string v0, "channel"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50405
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 50384
    :cond_c9
    const-string v0, "channelpassword"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_db

    .line 50385
    const-string v0, "channelpassword"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50407
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 50386
    :cond_db
    const-string v0, "addbookmark"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 50387
    const-string v0, "addbookmark"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50409
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 50388
    :cond_ed
    const-string v0, "token"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ff

    .line 50389
    const-string v0, "token"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50411
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    .line 50390
    :cond_ff
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->d()Lcom/teamspeak/ts3client/e/a;

    move-result-object v0

    .line 50413
    iget v0, v0, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 50414
    iput v0, v1, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 50391
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/bookmark/e;->a(Lcom/teamspeak/ts3client/data/ab;)V

    goto/16 :goto_48

    .line 50372
    :cond_112
    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_11a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_49 .. :try_end_11a} :catch_44

    goto/16 :goto_5b
.end method

.method private a(Lcom/teamspeak/ts3client/data/ab;)V
    .registers 14

    .prologue
    const/4 v11, 0x0

    const v4, 0x7f020076

    const/16 v10, 0x65

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1038
    .line 50242
    iget v0, p1, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 1038
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50243
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1038
    const-string v2, "reconnect.limit"

    const-string v3, "15"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_a0

    .line 1039
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-class v2, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1040
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1041
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1042
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-static {v1, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1044
    new-instance v1, Landroid/support/v4/app/dm;

    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/dm;-><init>(Landroid/content/Context;)V

    .line 1045
    invoke-virtual {v1, v4}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    move-result-object v2

    const-string v3, "connection.reconnect"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/dm;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/app/dm;->a(J)Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/dm;->a()Landroid/support/v4/app/dm;

    move-result-object v2

    const-string v3, "connection.reconnect"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/dm;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    const-string v3, "connection.reconnect.text"

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50244
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1045
    const-string v6, "reconnect.limit"

    const-string v7, "15"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/dm;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    .line 50245
    iput-object v0, v2, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    .line 50247
    invoke-virtual {v1}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    .line 1047
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x14

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1049
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50248
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 1049
    invoke-virtual {v1, v11, v10, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1050
    iput-boolean v8, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->p:Z

    .line 1051
    iput-boolean v9, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->s:Z

    .line 1067
    :goto_9f
    return-void

    .line 1054
    :cond_a0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-class v2, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1055
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1056
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1057
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-static {v1, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1059
    new-instance v1, Landroid/support/v4/app/dm;

    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v4/app/dm;-><init>(Landroid/content/Context;)V

    .line 1060
    invoke-virtual {v1, v4}, Landroid/support/v4/app/dm;->a(I)Landroid/support/v4/app/dm;

    move-result-object v2

    const-string v3, "connection.reconnect.retry"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/dm;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/app/dm;->a(J)Landroid/support/v4/app/dm;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/dm;->a()Landroid/support/v4/app/dm;

    move-result-object v2

    const-string v3, "connection.reconnect.retry"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/dm;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    const-string v3, "connection.reconnect.retry"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/dm;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;

    move-result-object v2

    .line 50249
    iput-object v0, v2, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    .line 50251
    invoke-virtual {v1}, Landroid/support/v4/app/dm;->c()Landroid/app/Notification;

    move-result-object v0

    .line 1062
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x14

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1064
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50252
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 1064
    invoke-virtual {v1, v11, v10, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 50253
    iget v0, p1, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 1065
    add-int/lit8 v0, v0, 0x1

    .line 50254
    iput v0, p1, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 1066
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/bookmark/e;->a(Lcom/teamspeak/ts3client/data/ab;)V

    goto :goto_9f
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/teamspeak/ts3client/cm;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->K:Lcom/teamspeak/ts3client/cm;

    return-object v0
.end method

.method private b(I)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 160
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    .line 161
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const-string v1, "critical.payment"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    const/16 v0, 0x123

    if-ne p1, v0, :cond_8c

    .line 164
    new-instance v0, Landroid/text/SpannableString;

    const-string v1, "critical.payment.network"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 167
    :goto_2f
    new-instance v1, Landroid/text/SpannableString;

    const-string v2, "\nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client"

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 168
    const/16 v2, 0xf

    invoke-static {v1, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 170
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    const-string v2, "button.exit"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/bu;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/bu;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 179
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v1, -0x3

    const-string v2, "button.retry"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/cc;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/cc;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 187
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 188
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 189
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 190
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 191
    return-void

    .line 166
    :cond_8c
    new-instance v0, Landroid/text/SpannableString;

    const-string v1, "critical.payment.other"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_2f
.end method

.method private b(Landroid/net/Uri;)V
    .registers 11

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 242
    new-instance v1, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 243
    if-eqz p1, :cond_53

    .line 246
    :try_start_9
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 247
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_66

    .line 248
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 249
    const-string v4, "&"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_54

    .line 250
    const-string v4, "&"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    :goto_2d
    if-ge v0, v4, :cond_66

    aget-object v5, v3, v0

    .line 251
    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 252
    array-length v6, v5

    if-le v6, v8, :cond_46

    .line 253
    const/4 v6, 0x0

    aget-object v6, v5, v6

    const/4 v7, 0x1

    aget-object v5, v5, v7

    invoke-virtual {v2, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :goto_43
    add-int/lit8 v0, v0, 0x1

    goto :goto_2d

    .line 255
    :cond_46
    const/4 v6, 0x0

    aget-object v5, v5, v6

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_9 .. :try_end_4e} :catch_4f

    goto :goto_43

    .line 288
    :catch_4f
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 292
    :cond_53
    :goto_53
    return-void

    .line 258
    :cond_54
    :try_start_54
    const-string v0, "="

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 259
    array-length v3, v0

    if-le v3, v8, :cond_117

    .line 260
    const/4 v3, 0x0

    aget-object v3, v0, v3

    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    :cond_66
    :goto_66
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 266
    const-string v0, "port"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_99

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5055
    iget-object v3, v1, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 267
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v0, "port"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 268
    :cond_99
    const-string v0, "nickname"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ab

    .line 269
    const-string v0, "nickname"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5107
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 270
    :cond_ab
    const-string v0, "password"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_bd

    .line 271
    const-string v0, "password"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5115
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 272
    :cond_bd
    const-string v0, "channel"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_cf

    .line 273
    const-string v0, "channel"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6075
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 274
    :cond_cf
    const-string v0, "channelpassword"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e1

    .line 275
    const-string v0, "channelpassword"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6083
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 276
    :cond_e1
    const-string v0, "addbookmark"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f3

    .line 277
    const-string v0, "addbookmark"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6099
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 278
    :cond_f3
    const-string v0, "token"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_105

    .line 279
    const-string v0, "token"

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 6140
    iput-object v0, v1, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    .line 284
    :cond_105
    new-instance v0, Lcom/teamspeak/ts3client/bookmark/a;

    .line 7042
    sget-object v2, Lcom/teamspeak/ts3client/data/b/a;->a:Lcom/teamspeak/ts3client/data/b/a;

    .line 284
    invoke-direct {v0, v2, v1}, Lcom/teamspeak/ts3client/bookmark/a;-><init>(Lcom/teamspeak/ts3client/data/b/a;Lcom/teamspeak/ts3client/data/ab;)V

    .line 285
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v2, "AddBookmark"

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/bookmark/a;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    goto/16 :goto_53

    .line 262
    :cond_117
    const/4 v3, 0x0

    aget-object v0, v0, v3

    const-string v3, ""

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_11f
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_54 .. :try_end_11f} :catch_4f

    goto/16 :goto_66
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/StartGUIFragment;I)V
    .registers 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 108
    .line 50463
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    .line 50464
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const-string v1, "critical.payment"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 50466
    const/16 v0, 0x123

    if-ne p1, v0, :cond_8c

    .line 50467
    new-instance v0, Landroid/text/SpannableString;

    const-string v1, "critical.payment.network"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 50470
    :goto_2f
    new-instance v1, Landroid/text/SpannableString;

    const-string v2, "\nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client"

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 50471
    const/16 v2, 0xf

    invoke-static {v1, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 50473
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 50474
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    const-string v2, "button.exit"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/bu;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/bu;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 50482
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v1, -0x3

    const-string v2, "button.retry"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/cc;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/cc;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 50490
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 50491
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 50492
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 50493
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 108
    return-void

    .line 50469
    :cond_8c
    new-instance v0, Landroid/text/SpannableString;

    const-string v1, "critical.payment.other"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_2f
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;)V
    .registers 2

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/StartGUIFragment;->b(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/a/a/a/a/k;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->L:Lcom/a/a/a/a/k;

    return-object v0
.end method

.method private c(Landroid/net/Uri;)V
    .registers 2

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/StartGUIFragment;->b(Landroid/net/Uri;)V

    .line 296
    return-void
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/StartGUIFragment;)V
    .registers 1

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->r()V

    return-void
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/StartGUIFragment;)Landroid/view/View;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->M:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/StartGUIFragment;)V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 50321
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_b

    .line 50322
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->w()V

    .line 50329
    :goto_a
    return-void

    .line 50327
    :cond_b
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v0}, Landroid/support/v4/c/h;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 50328
    if-eqz v0, :cond_1e

    .line 50329
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    invoke-static {p0, v0, v3}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_a

    .line 50333
    :cond_1e
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->w()V

    .line 50334
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->L()V

    goto :goto_a
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/StartGUIFragment;)V
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 108
    .line 50337
    const-string v1, "preferences"

    invoke-virtual {p0, v1, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 50338
    const-string v2, "version"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 50341
    :try_start_e
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v0, v3, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_e .. :try_end_1d} :catch_37
    .catch Ljava/lang/NullPointerException; {:try_start_e .. :try_end_1d} :catch_35

    .line 50345
    :goto_1d
    if-ge v2, v0, :cond_34

    .line 50346
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 50347
    const-string v2, "version"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 50348
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 50349
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    if-nez v0, :cond_34

    .line 50350
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->q:Lcom/teamspeak/ts3client/b;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/b;->show()V

    .line 108
    :cond_34
    return-void

    :catch_35
    move-exception v3

    goto :goto_1d

    .line 50344
    :catch_37
    move-exception v3

    goto :goto_1d
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/StartGUIFragment;)V
    .registers 5

    .prologue
    .line 108
    .line 50416
    const v0, 0x7f0c01c4

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 50417
    const v1, 0x7f04000e

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 50418
    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 50419
    new-instance v2, Lcom/teamspeak/ts3client/ci;

    invoke-direct {v2, p0, v1}, Lcom/teamspeak/ts3client/ci;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 50447
    new-instance v2, Lcom/teamspeak/ts3client/ck;

    invoke-direct {v2, p0, v0, v1}, Lcom/teamspeak/ts3client/ck;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/View;Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v2}, Lcom/teamspeak/ts3client/StartGUIFragment;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 50454
    new-instance v1, Lcom/teamspeak/ts3client/cl;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/cl;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 108
    return-void
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/StartGUIFragment;)I
    .registers 2

    .prologue
    .line 108
    iget v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->D:I

    return v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/StartGUIFragment;)Landroid/os/Handler;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->J:Landroid/os/Handler;

    return-object v0
.end method

.method private p()Lcom/teamspeak/ts3client/data/b/a;
    .registers 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    .line 7086
    iget-object v0, v0, Lcom/teamspeak/ts3client/bookmark/e;->b:Lcom/teamspeak/ts3client/data/b/a;

    .line 299
    return-object v0
.end method

.method private q()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 441
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    if-eqz v0, :cond_6

    .line 478
    :goto_5
    return-void

    .line 443
    :cond_6
    invoke-static {}, Lcom/teamspeak/ts3client/data/af;->a()Lcom/teamspeak/ts3client/data/af;

    move-result-object v0

    .line 35053
    iget v0, v0, Lcom/teamspeak/ts3client/data/af;->a:I

    .line 443
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 35093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 443
    const-string v2, "LicenseAgreement"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v0, v1, :cond_6d

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 36061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 443
    if-nez v0, :cond_6d

    .line 444
    new-instance v0, Lcom/teamspeak/ts3client/d/t;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/d/t;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    .line 445
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/d/t;->setCancelable(Z)V

    .line 446
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    invoke-static {}, Lcom/teamspeak/ts3client/data/af;->a()Lcom/teamspeak/ts3client/data/af;

    move-result-object v1

    .line 37053
    iget v1, v1, Lcom/teamspeak/ts3client/data/af;->a:I

    .line 37060
    iput v1, v0, Lcom/teamspeak/ts3client/d/t;->c:I

    .line 447
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    new-instance v1, Lcom/teamspeak/ts3client/ce;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ce;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    .line 37126
    iput-object v1, v0, Lcom/teamspeak/ts3client/d/t;->a:Lcom/teamspeak/ts3client/d/k;

    .line 456
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    new-instance v1, Lcom/teamspeak/ts3client/cf;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/cf;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    .line 37131
    iput-object v1, v0, Lcom/teamspeak/ts3client/d/t;->b:Lcom/teamspeak/ts3client/d/k;

    .line 468
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    new-instance v1, Lcom/teamspeak/ts3client/ch;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ch;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    .line 37136
    iput-object v1, v0, Lcom/teamspeak/ts3client/d/t;->d:Lcom/teamspeak/ts3client/d/k;

    .line 475
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    .line 37141
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 38093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 37141
    const-string v2, "lang_tag"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/teamspeak/ts3client/d/t;->f:Ljava/lang/String;

    .line 37142
    iput-object p0, v0, Lcom/teamspeak/ts3client/d/t;->e:Lcom/teamspeak/ts3client/d/l;

    .line 37143
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/t;->b()V

    goto :goto_5

    .line 477
    :cond_6d
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->r()V

    goto :goto_5
.end method

.method private r()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 482
    new-instance v1, Lcom/teamspeak/ts3client/cm;

    invoke-direct {v1, p0, v6}, Lcom/teamspeak/ts3client/cm;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;B)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->K:Lcom/teamspeak/ts3client/cm;

    .line 483
    new-instance v1, Lcom/a/a/a/a/k;

    new-instance v2, Lcom/a/a/a/a/v;

    new-instance v3, Lcom/a/a/a/a/a;

    sget-object v4, Lcom/teamspeak/ts3client/StartGUIFragment;->u:[B

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v0}, Lcom/a/a/a/a/a;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, p0, v3}, Lcom/a/a/a/a/v;-><init>(Landroid/content/Context;Lcom/a/a/a/a/r;)V

    const-string v0, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo0t0xR6Ch8m7hhrWgMBjBBdSaOA/wFTSk27DMKnqsYrw/AgxJjK07KyxMnRYojLsX34Cy3TCmPbcK3V9mKCjwlakgBYTObiVKRnnF1wu20WmsOb0wjbRbhYGDbhmAwc7Sy8sXNehiuz/O+PTSHcwWvHL20zum2cmHDmCAjREb1ZlA4myNN/tY9DYNMkQjOpMqgOCIQbyXdJLinOzwrgeX7x2IXMOJ7bbxWEmuyRXviOijp/1hQfpweo6slwfuc9iQDPBQl77M9dObMUrr+/23pxEE2i+U8WsePwTIw4uxkyEjew3ZRkVmFTtTqdp77tcOOFJ2hn4KkhLkM0hSaLLRQIDAQAB"

    invoke-direct {v1, p0, v2, v0}, Lcom/a/a/a/a/k;-><init>(Landroid/content/Context;Lcom/a/a/a/a/s;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->L:Lcom/a/a/a/a/k;

    .line 484
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->L:Lcom/a/a/a/a/k;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->K:Lcom/teamspeak/ts3client/cm;

    invoke-virtual {v0, v1}, Lcom/a/a/a/a/k;->a(Lcom/a/a/a/a/o;)V

    .line 485
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->L:Lcom/a/a/a/a/k;

    .line 38199
    iput v6, v0, Lcom/a/a/a/a/k;->a:I

    .line 38200
    iget v1, v0, Lcom/a/a/a/a/k;->a:I

    add-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/a/a/a/a/k;->a:I

    .line 486
    return-void
.end method

.method private s()V
    .registers 5

    .prologue
    .line 490
    const v0, 0x7f0c01c4

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 491
    const v1, 0x7f04000e

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 492
    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 493
    new-instance v2, Lcom/teamspeak/ts3client/ci;

    invoke-direct {v2, p0, v1}, Lcom/teamspeak/ts3client/ci;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 521
    new-instance v2, Lcom/teamspeak/ts3client/ck;

    invoke-direct {v2, p0, v0, v1}, Lcom/teamspeak/ts3client/ck;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/View;Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v2}, Lcom/teamspeak/ts3client/StartGUIFragment;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 528
    new-instance v1, Lcom/teamspeak/ts3client/cl;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/cl;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 536
    return-void
.end method

.method private t()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 539
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UrlStart"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 540
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 541
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 542
    const-string v2, "gui.extern.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 543
    const-string v2, "gui.extern.text"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "address"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 544
    const/4 v2, -0x1

    const-string v3, "gui.extern.button1"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/teamspeak/ts3client/bv;

    invoke-direct {v4, p0, v0, v1}, Lcom/teamspeak/ts3client/bv;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 552
    const/4 v2, -0x3

    const-string v3, "gui.extern.button2"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/teamspeak/ts3client/bw;

    invoke-direct {v4, p0, v0, v1}, Lcom/teamspeak/ts3client/bw;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 560
    const/4 v0, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/bx;

    invoke-direct {v3, p0, v1}, Lcom/teamspeak/ts3client/bx;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 568
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 569
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 571
    :cond_7e
    return-void
.end method

.method private u()Z
    .registers 4

    .prologue
    .line 575
    :try_start_0
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
    :try_end_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_3} :catch_5

    .line 582
    const/4 v0, 0x1

    :goto_4
    return v0

    .line 579
    :catch_5
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 39085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 579
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "No libs found, missing CPU support!"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 580
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private v()V
    .registers 8

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 586
    new-instance v0, Lcom/teamspeak/ts3client/data/e/a;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 39093
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 586
    const-string v3, "lang_tag"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 587
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 40093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 587
    const-string v1, "sampleRec"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const v1, 0xac44

    if-le v0, v1, :cond_53

    .line 588
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 41093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 588
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "samplePlay"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 589
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 42093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 589
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sampleRec"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 590
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 43085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 590
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Cleared sample rates higher than 44100"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 594
    :cond_53
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 43093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 594
    const-string v1, "samplePlay"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_70

    .line 595
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 44093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 595
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "samplePlay"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 596
    :cond_70
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 45093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 596
    const-string v1, "sampleRec"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_8d

    .line 597
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 46093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 597
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sampleRec"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 599
    :cond_8d
    new-instance v0, Lcom/teamspeak/ts3client/b;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->q:Lcom/teamspeak/ts3client/b;

    .line 601
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->z:Lcom/teamspeak/ts3client/data/b/f;

    .line 602
    new-instance v0, Lcom/teamspeak/ts3client/data/b/c;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/b/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->A:Lcom/teamspeak/ts3client/data/b/c;

    .line 604
    new-instance v0, Lcom/teamspeak/ts3client/bookmark/e;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/bookmark/e;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    .line 605
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/bookmark/e;->e(Landroid/os/Bundle;)V

    .line 607
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 46200
    iput-object v0, v1, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 608
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 47196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 608
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 610
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 48073
    iput-object v0, v1, Lcom/teamspeak/ts3client/Ts3Application;->h:Landroid/net/ConnectivityManager;

    .line 612
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->b()Lcom/teamspeak/ts3client/data/d/f;

    move-result-object v1

    .line 49038
    iget-object v0, v1, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 49039
    const/high16 v2, 0x100000

    mul-int/2addr v0, v2

    div-int/lit8 v0, v0, 0xa

    .line 49040
    new-instance v2, Lcom/teamspeak/ts3client/data/d/g;

    invoke-direct {v2, v1, v0}, Lcom/teamspeak/ts3client/data/d/g;-><init>(Lcom/teamspeak/ts3client/data/d/f;I)V

    iput-object v2, v1, Lcom/teamspeak/ts3client/data/d/f;->a:Landroid/support/v4/n/j;

    .line 49049
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/d/f;->a()V

    .line 615
    :try_start_f9
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;
    :try_end_fe
    .catch Ljava/lang/Exception; {:try_start_f9 .. :try_end_fe} :catch_ff

    .line 634
    :cond_fe
    :goto_fe
    return-void

    .line 617
    :catch_ff
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 49093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 617
    const-string v1, "sound_pack"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_fe

    .line 618
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 618
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sound_pack"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 619
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    .line 620
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 621
    const-string v1, "critical.tts"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 622
    const-string v1, "critical.tts.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 623
    const/4 v1, -0x3

    const-string v2, "button.ok"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/by;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/by;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 630
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 631
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_fe
.end method

.method private w()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 637
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1, v4, v4}, Lcom/teamspeak/ts3client/a/k;->a(Landroid/content/Context;II)Ljava/util/Vector;

    move-result-object v1

    .line 50094
    iput-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->j:Ljava/util/Vector;

    .line 638
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50096
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 638
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Found Audio RECSampleRate:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 640
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/teamspeak/ts3client/a/k;->a(Landroid/content/Context;I)Ljava/util/Vector;

    move-result-object v1

    .line 50097
    iput-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->i:Ljava/util/Vector;

    .line 641
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50099
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 641
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Found Audio PLAYSampleRate:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v3}, Lcom/teamspeak/ts3client/Ts3Application;->i()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 643
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->h()I

    move-result v0

    if-eqz v0, :cond_65

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->i()I

    move-result v0

    if-nez v0, :cond_a1

    .line 644
    :cond_65
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    .line 645
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const-string v1, "critical.audio"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 646
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const-string v1, "critical.audio.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 647
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    const-string v2, "button.exit"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/bz;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/bz;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 655
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 656
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 658
    :cond_a1
    return-void
.end method

.method private x()V
    .registers 7

    .prologue
    const v5, 0x7f0c01c7

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 661
    invoke-virtual {p0, v5}, Lcom/teamspeak/ts3client/StartGUIFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 662
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    .line 50100
    iput-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 663
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    const-string v2, "Bookmark"

    invoke-virtual {v0, v5, v1, v2}, Landroid/support/v4/app/cd;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 666
    :cond_24
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50102
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 666
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Finished loading GUI"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 668
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50103
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 668
    const-string v1, "screen_rotation"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 669
    packed-switch v0, :pswitch_data_4e

    .line 680
    invoke-virtual {p0, v3}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    .line 685
    :goto_3f
    return-void

    .line 671
    :pswitch_40
    invoke-virtual {p0, v3}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto :goto_3f

    .line 674
    :pswitch_44
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto :goto_3f

    .line 677
    :pswitch_49
    invoke-virtual {p0, v4}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto :goto_3f

    .line 669
    nop

    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_40
        :pswitch_44
        :pswitch_49
    .end packed-switch
.end method

.method private y()V
    .registers 4

    .prologue
    .line 772
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 774
    const/4 v1, 0x1

    const-string v2, "Ts3WakeLOCK"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 775
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 776
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50163
    iput-object v0, v1, Lcom/teamspeak/ts3client/Ts3Application;->b:Landroid/os/PowerManager$WakeLock;

    .line 777
    return-void
.end method

.method private z()V
    .registers 4

    .prologue
    .line 780
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 783
    const/16 v1, 0x20

    const-string v2, "Ts3WakeLOCKSensor"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 784
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 786
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50165
    iput-object v0, v1, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 787
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/e;->a()V

    .line 335
    return-void
.end method

.method public final h()Z
    .registers 2

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->finish()V

    .line 353
    const/4 v0, 0x1

    return v0
.end method

.method public final j()V
    .registers 2

    .prologue
    .line 790
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->G:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 791
    return-void
.end method

.method public final k()V
    .registers 4

    .prologue
    .line 794
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->G:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->H:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 795
    return-void
.end method

.method public final l()V
    .registers 4

    .prologue
    .line 990
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    .line 50235
    iput-object v1, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 991
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 992
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50237
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 992
    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 993
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50238
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50239
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 993
    const-string v2, "Connection"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 994
    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 996
    iget-boolean v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->C:Z

    if-eqz v1, :cond_30

    .line 997
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 998
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->p:Z

    .line 1004
    :goto_2f
    return-void

    .line 1000
    :cond_30
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->j()I

    .line 1001
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->p:Z

    goto :goto_2f
.end method

.method public final m()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1075
    .line 50256
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    if-nez v0, :cond_6b

    .line 50258
    invoke-static {}, Lcom/teamspeak/ts3client/data/af;->a()Lcom/teamspeak/ts3client/data/af;

    move-result-object v0

    .line 50294
    iget v0, v0, Lcom/teamspeak/ts3client/data/af;->a:I

    .line 50258
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50295
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 50258
    const-string v2, "LicenseAgreement"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v0, v1, :cond_6c

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50296
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50258
    if-nez v0, :cond_6c

    .line 50259
    new-instance v0, Lcom/teamspeak/ts3client/d/t;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/d/t;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    .line 50260
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/d/t;->setCancelable(Z)V

    .line 50261
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    invoke-static {}, Lcom/teamspeak/ts3client/data/af;->a()Lcom/teamspeak/ts3client/data/af;

    move-result-object v1

    .line 50297
    iget v1, v1, Lcom/teamspeak/ts3client/data/af;->a:I

    .line 50298
    iput v1, v0, Lcom/teamspeak/ts3client/d/t;->c:I

    .line 50262
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    new-instance v1, Lcom/teamspeak/ts3client/ce;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ce;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    .line 50300
    iput-object v1, v0, Lcom/teamspeak/ts3client/d/t;->a:Lcom/teamspeak/ts3client/d/k;

    .line 50271
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    new-instance v1, Lcom/teamspeak/ts3client/cf;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/cf;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    .line 50302
    iput-object v1, v0, Lcom/teamspeak/ts3client/d/t;->b:Lcom/teamspeak/ts3client/d/k;

    .line 50283
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    new-instance v1, Lcom/teamspeak/ts3client/ch;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ch;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    .line 50304
    iput-object v1, v0, Lcom/teamspeak/ts3client/d/t;->d:Lcom/teamspeak/ts3client/d/k;

    .line 50290
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    .line 50306
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 50310
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 50306
    const-string v2, "lang_tag"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/teamspeak/ts3client/d/t;->f:Ljava/lang/String;

    .line 50307
    iput-object p0, v0, Lcom/teamspeak/ts3client/d/t;->e:Lcom/teamspeak/ts3client/d/l;

    .line 50308
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/t;->b()V

    .line 50290
    :cond_6b
    :goto_6b
    return-void

    .line 50292
    :cond_6c
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->r()V

    goto :goto_6b
.end method

.method public final n()V
    .registers 2

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    if-eqz v0, :cond_9

    .line 1081
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->P:Lcom/teamspeak/ts3client/d/t;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/t;->show()V

    .line 1083
    :cond_9
    return-void
.end method

.method public final o()V
    .registers 1

    .prologue
    .line 1106
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->r()V

    .line 1107
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3

    .prologue
    .line 1008
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 304
    sparse-switch p1, :sswitch_data_50

    .line 323
    :cond_4
    :goto_4
    return-void

    .line 306
    :sswitch_5
    const/4 v0, -0x1

    if-ne p2, v0, :cond_4

    .line 307
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_4

    .line 312
    :sswitch_d
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_4

    .line 315
    invoke-static {p0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 316
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 316
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_overlay"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_4

    .line 318
    :cond_2c
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 318
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_overlay"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 319
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 319
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_overlay_setting"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_4

    .line 304
    nop

    :sswitch_data_50
    .sparse-switch
        0x29a -> :sswitch_5
        0x1d32 -> :sswitch_d
    .end sparse-switch
.end method

.method public onBackPressed()V
    .registers 1

    .prologue
    .line 328
    invoke-super {p0}, Landroid/support/v7/app/i;->onBackPressed()V

    .line 329
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2

    .prologue
    .line 339
    invoke-super {p0, p1}, Landroid/support/v7/app/i;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 340
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 15
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const v12, 0x7f0c01c7

    const/16 v4, 0x400

    const/4 v11, 0x1

    const/4 v10, -0x1

    const/4 v1, 0x0

    .line 364
    invoke-super {p0, p1}, Landroid/support/v7/app/i;->onCreate(Landroid/os/Bundle;)V

    .line 365
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 366
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v2, "TS3Settings"

    invoke-virtual {p0, v2, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 9097
    iput-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 367
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->O:Lcom/teamspeak/ts3client/d/c/a;

    if-nez v0, :cond_28

    .line 368
    new-instance v0, Lcom/teamspeak/ts3client/d/c/a;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/d/c/a;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->O:Lcom/teamspeak/ts3client/d/c/a;

    .line 369
    :cond_28
    const v0, 0x7f030054

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->setContentView(I)V

    .line 371
    const v0, 0x7f0c01c6

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 372
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10093
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 372
    const-string v3, "enable_fullscreen"

    invoke-interface {v2, v3, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4a

    .line 373
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 376
    :cond_4a
    if-eqz v0, :cond_53

    .line 10099
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v7/app/aj;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 379
    :cond_53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_62

    .line 380
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 381
    :cond_62
    iput-object p1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->y:Landroid/os/Bundle;

    .line 382
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11079
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/app/aj;->a()Landroid/support/v7/app/a;

    move-result-object v2

    .line 11208
    iput-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 12079
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->a()Landroid/support/v7/app/a;

    .line 384
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 384
    const-string v2, "TeamSpeak"

    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 385
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 385
    invoke-virtual {v0}, Landroid/support/v7/app/a;->n()V

    .line 13924
    const-string v0, "com.teamspeak.ts"

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v2

    .line 13926
    :try_start_8d
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/TS3/logs/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13927
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_b2

    .line 13928
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 13930
    :cond_b2
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 13931
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x240c8400

    sub-long/2addr v4, v6

    .line 13932
    if-eqz v3, :cond_d4

    .line 13933
    array-length v6, v3

    move v0, v1

    :goto_c2
    if-ge v0, v6, :cond_d4

    aget-object v7, v3, v0

    .line 13934
    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v8, v8, v4

    if-gez v8, :cond_d1

    .line 13935
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 13933
    :cond_d1
    add-int/lit8 v0, v0, 0x1

    goto :goto_c2

    .line 13939
    :cond_d4
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 13939
    const-string v3, "create_debuglog"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_122

    .line 13940
    new-instance v0, Ljava/util/logging/FileHandler;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/TS3/logs/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy_MM_dd_HH_mm_ss"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".log"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/logging/FileHandler;-><init>(Ljava/lang/String;)V

    .line 13941
    new-instance v3, Ljava/util/logging/SimpleFormatter;

    invoke-direct {v3}, Ljava/util/logging/SimpleFormatter;-><init>()V

    invoke-virtual {v0, v3}, Ljava/util/logging/FileHandler;->setFormatter(Ljava/util/logging/Formatter;)V

    .line 13942
    invoke-virtual {v2, v0}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V
    :try_end_122
    .catch Ljava/io/IOException; {:try_start_8d .. :try_end_122} :catch_488

    .line 13947
    :cond_122
    :goto_122
    sget-object v0, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    invoke-virtual {v2, v0}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 13948
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15089
    iput-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 15953
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/TS3/content/lang"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 15954
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_150

    .line 15955
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 15956
    :cond_150
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/TS3"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 15957
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/Update.apk"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 15958
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_197

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_197

    .line 15959
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 15960
    :cond_197
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/TS3/cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 15961
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1bc

    .line 15962
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 15963
    :cond_1bc
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/.nomedia"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 15964
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1e1

    .line 15966
    :try_start_1de
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_1e1
    .catch Ljava/io/IOException; {:try_start_1de .. :try_end_1e1} :catch_243

    .line 389
    :cond_1e1
    :goto_1e1
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 389
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "Start"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 391
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->u()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 392
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030053

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->M:Landroid/view/View;

    .line 393
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->M:Landroid/view/View;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v10, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v3}, Lcom/teamspeak/ts3client/StartGUIFragment;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 394
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_250

    .line 396
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    .line 397
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const-string v2, "Not supported"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const-string v2, "The device you are using seems to be an ARMv6 device, but TeamSpeak3 requires at least an ARMv7 device.\nIf you think this is an error on our side please visit our forum @ http://forum.teamspeak.com"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 399
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    const/4 v2, -0x2

    const-string v3, "EXIT"

    new-instance v4, Lcom/teamspeak/ts3client/cd;

    invoke-direct {v4, p0}, Lcom/teamspeak/ts3client/cd;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 407
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 408
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->B:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 438
    :goto_242
    return-void

    .line 15969
    :catch_243
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 15969
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Could not create .nomedia File"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1e1

    .line 411
    :cond_250
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17111
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 17586
    new-instance v0, Lcom/teamspeak/ts3client/data/e/a;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18093
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17586
    const-string v4, "lang_tag"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/teamspeak/ts3client/data/e/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 17587
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17587
    const-string v2, "sampleRec"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const v2, 0xac44

    if-le v0, v2, :cond_2a5

    .line 17588
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17588
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "samplePlay"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 17589
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17589
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "sampleRec"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 17590
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 17590
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "Cleared sample rates higher than 44100"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 17594
    :cond_2a5
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17594
    const-string v2, "samplePlay"

    invoke-interface {v0, v2, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_2c2

    .line 17595
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17595
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "samplePlay"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 17596
    :cond_2c2
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17596
    const-string v2, "sampleRec"

    invoke-interface {v0, v2, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_2df

    .line 17597
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17597
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "sampleRec"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 17599
    :cond_2df
    new-instance v0, Lcom/teamspeak/ts3client/b;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->q:Lcom/teamspeak/ts3client/b;

    .line 17601
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->z:Lcom/teamspeak/ts3client/data/b/f;

    .line 17602
    new-instance v0, Lcom/teamspeak/ts3client/data/b/c;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/b/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->A:Lcom/teamspeak/ts3client/data/b/c;

    .line 17604
    new-instance v0, Lcom/teamspeak/ts3client/bookmark/e;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/bookmark/e;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    .line 17605
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/bookmark/e;->e(Landroid/os/Bundle;)V

    .line 17607
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v3, "notification"

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 25200
    iput-object v0, v2, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 17608
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26196
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 17608
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 17610
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 27073
    iput-object v0, v2, Lcom/teamspeak/ts3client/Ts3Application;->h:Landroid/net/ConnectivityManager;

    .line 17612
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->b()Lcom/teamspeak/ts3client/data/d/f;

    move-result-object v2

    .line 28038
    iget-object v0, v2, Lcom/teamspeak/ts3client/data/d/f;->b:Lcom/teamspeak/ts3client/Ts3Application;

    const-string v3, "activity"

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/Ts3Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 28039
    const/high16 v3, 0x100000

    mul-int/2addr v0, v3

    div-int/lit8 v0, v0, 0xa

    .line 28040
    new-instance v3, Lcom/teamspeak/ts3client/data/d/g;

    invoke-direct {v3, v2, v0}, Lcom/teamspeak/ts3client/data/d/g;-><init>(Lcom/teamspeak/ts3client/data/d/f;I)V

    iput-object v3, v2, Lcom/teamspeak/ts3client/data/d/f;->a:Landroid/support/v4/n/j;

    .line 28049
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/d/f;->a()V

    .line 17615
    :try_start_34b
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;
    :try_end_350
    .catch Ljava/lang/Exception; {:try_start_34b .. :try_end_350} :catch_424

    .line 29661
    :cond_350
    :goto_350
    invoke-virtual {p0, v12}, Lcom/teamspeak/ts3client/StartGUIFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_36f

    .line 29662
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    .line 30081
    iput-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 29663
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    const-string v3, "Bookmark"

    invoke-virtual {v0, v12, v2, v3}, Landroid/support/v4/app/cd;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 29666
    :cond_36f
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 29666
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "Finished loading GUI"

    invoke-virtual {v0, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 29668
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 29668
    const-string v2, "screen_rotation"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 29669
    packed-switch v0, :pswitch_data_48c

    .line 29680
    invoke-virtual {p0, v10}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    .line 414
    :goto_38a
    invoke-static {}, Lcom/teamspeak/ts3client/data/af;->a()Lcom/teamspeak/ts3client/data/af;

    move-result-object v0

    .line 31061
    iput-object p0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 416
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->G:Landroid/hardware/SensorManager;

    .line 417
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->G:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v11}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->H:Landroid/hardware/Sensor;

    .line 419
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->J:Landroid/os/Handler;

    .line 421
    const-string v0, "preferences"

    invoke-virtual {p0, v0, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 422
    const-string v2, "version"

    invoke-interface {v0, v2, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 423
    const/16 v2, 0xc

    if-ge v0, v2, :cond_3ca

    .line 424
    const-string v2, "com.android.vending.licensing.ServerManagedPolicy"

    invoke-virtual {p0, v2, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 425
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 427
    :cond_3ca
    const/16 v1, 0x39

    if-ge v0, v1, :cond_3fb

    .line 428
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 428
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "samplePlay"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 429
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 429
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "sampleRec"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 430
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 430
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "Cleared pre 57 audio settings setting"

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 432
    :cond_3fb
    const/16 v1, 0x46

    if-ge v0, v1, :cond_410

    .line 433
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 433
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "android_overlay"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 436
    :cond_410
    new-instance v0, Lcom/teamspeak/ts3client/co;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {v0, v1}, Lcom/teamspeak/ts3client/co;-><init>(Lcom/teamspeak/ts3client/Ts3Application;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->E:Lcom/teamspeak/ts3client/co;

    .line 437
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 437
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->E:Lcom/teamspeak/ts3client/co;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    goto/16 :goto_242

    .line 17617
    :catch_424
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17617
    const-string v2, "sound_pack"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v11, :cond_350

    .line 17618
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 29093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 17618
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "sound_pack"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 17619
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    .line 17620
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 17621
    const-string v2, "critical.tts"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 17622
    const-string v2, "critical.tts.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 17623
    const/4 v2, -0x3

    const-string v3, "button.ok"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/teamspeak/ts3client/by;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/by;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 17630
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 17631
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_350

    .line 29671
    :pswitch_479
    invoke-virtual {p0, v10}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto/16 :goto_38a

    .line 29674
    :pswitch_47e
    invoke-virtual {p0, v11}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto/16 :goto_38a

    .line 29677
    :pswitch_483
    invoke-virtual {p0, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->setRequestedOrientation(I)V

    goto/16 :goto_38a

    :catch_488
    move-exception v0

    goto/16 :goto_122

    .line 29669
    nop

    :pswitch_data_48c
    .packed-switch 0x0
        :pswitch_479
        :pswitch_47e
        :pswitch_483
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4

    .prologue
    .line 344
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 346
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->O:Lcom/teamspeak/ts3client/d/c/a;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0, p1, v1}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/Menu;Lcom/teamspeak/ts3client/Ts3Application;)V

    .line 347
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 690
    invoke-super {p0}, Landroid/support/v7/app/i;->onDestroy()V

    .line 691
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50104
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->k:Landroid/content/Intent;

    .line 691
    if-eqz v0, :cond_12

    .line 692
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50105
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->k:Landroid/content/Intent;

    .line 692
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/Ts3Application;->stopService(Landroid/content/Intent;)Z

    .line 693
    :cond_12
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c()V

    .line 694
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 698
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/teamspeak/ts3client/data/v;->a(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 760
    :cond_c
    :goto_c
    return v0

    .line 700
    :cond_d
    sparse-switch p1, :sswitch_data_160

    .line 760
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_c

    .line 702
    :sswitch_15
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50106
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 702
    if-eqz v2, :cond_25

    .line 703
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50107
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50108
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 703
    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/a/k;->a(Z)V

    goto :goto_c

    :cond_25
    move v0, v1

    .line 706
    goto :goto_c

    .line 708
    :sswitch_27
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50109
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 708
    if-eqz v2, :cond_37

    .line 709
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50110
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50111
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 709
    invoke-virtual {v2, v1}, Lcom/teamspeak/ts3client/a/k;->a(Z)V

    goto :goto_c

    :cond_37
    move v0, v1

    .line 712
    goto :goto_c

    .line 714
    :sswitch_39
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50112
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 714
    if-eqz v2, :cond_159

    .line 715
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50113
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 715
    iget-object v3, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50114
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50115
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 715
    if-ne v2, v3, :cond_a3

    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50116
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50117
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 50118
    iget-boolean v2, v2, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    .line 715
    if-eqz v2, :cond_a3

    .line 716
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50119
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50120
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 50121
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/t;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 50122
    invoke-static {v3}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 50123
    const-string v4, "disconnect.info"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 50124
    const-string v4, "disconnect.text"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 50125
    const/4 v4, -0x1

    const-string v5, "disconnect.button"

    invoke-static {v5}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/teamspeak/ts3client/az;

    invoke-direct {v6, v2}, Lcom/teamspeak/ts3client/az;-><init>(Lcom/teamspeak/ts3client/t;)V

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 50136
    const/4 v4, -0x2

    const-string v5, "button.cancel"

    invoke-static {v5}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/teamspeak/ts3client/ba;

    invoke-direct {v6, v2, v3}, Lcom/teamspeak/ts3client/ba;-><init>(Lcom/teamspeak/ts3client/t;Landroid/app/AlertDialog;)V

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 50143
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 50144
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_c

    .line 719
    :cond_a3
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50146
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 719
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50147
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50148
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 719
    if-ne v1, v2, :cond_bf

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50149
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50150
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 50151
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/ConnectionBackground;->g:Z

    .line 719
    if-nez v1, :cond_bf

    .line 720
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 722
    :cond_bf
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50152
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 722
    instance-of v1, v1, Lcom/teamspeak/ts3client/bookmark/a;

    if-eqz v1, :cond_cd

    .line 723
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 725
    :cond_cd
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50153
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 725
    instance-of v1, v1, Lcom/teamspeak/ts3client/h;

    if-eqz v1, :cond_db

    .line 726
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 728
    :cond_db
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50154
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 728
    instance-of v1, v1, Lcom/teamspeak/ts3client/c;

    if-eqz v1, :cond_e9

    .line 729
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 731
    :cond_e9
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50155
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 731
    iget-object v2, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->o:Lcom/teamspeak/ts3client/bookmark/e;

    if-ne v1, v2, :cond_f7

    .line 732
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 734
    :cond_f7
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50156
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 734
    instance-of v1, v1, Lcom/teamspeak/ts3client/f/as;

    if-eqz v1, :cond_105

    .line 735
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 737
    :cond_105
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50157
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 737
    instance-of v1, v1, Lcom/teamspeak/ts3client/c/b;

    if-eqz v1, :cond_113

    .line 738
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 740
    :cond_113
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50158
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 740
    instance-of v1, v1, Lcom/teamspeak/ts3client/e/b;

    if-eqz v1, :cond_121

    .line 741
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 743
    :cond_121
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50159
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 743
    instance-of v1, v1, Lcom/teamspeak/ts3client/d/d/i;

    if-eqz v1, :cond_12f

    .line 744
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 746
    :cond_12f
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50160
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 746
    instance-of v1, v1, Lcom/teamspeak/ts3client/chat/c;

    if-eqz v1, :cond_13d

    .line 747
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 749
    :cond_13d
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50161
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 749
    instance-of v1, v1, Lcom/teamspeak/ts3client/chat/j;

    if-eqz v1, :cond_14b

    .line 750
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 752
    :cond_14b
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50162
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 752
    instance-of v1, v1, Lcom/teamspeak/ts3client/b/b;

    if-eqz v1, :cond_c

    .line 753
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 758
    :cond_159
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_c

    .line 700
    nop

    :sswitch_data_160
    .sparse-switch
        0x4 -> :sswitch_39
        0x18 -> :sswitch_15
        0x19 -> :sswitch_27
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 766
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/teamspeak/ts3client/data/v;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 767
    const/4 v0, 0x1

    .line 768
    :goto_b
    return v0

    :cond_c
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/i;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_b
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 4

    .prologue
    .line 904
    invoke-super {p0, p1}, Landroid/support/v7/app/i;->onNewIntent(Landroid/content/Intent;)V

    .line 905
    const-string v0, "bookmark"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 906
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 907
    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->b(Landroid/net/Uri;)V

    .line 909
    :cond_1a
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 358
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->O:Lcom/teamspeak/ts3client/d/c/a;

    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0, p1, v1}, Lcom/teamspeak/ts3client/d/c/a;->a(Landroid/view/MenuItem;Lcom/teamspeak/ts3client/Ts3Application;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 806
    invoke-super {p0}, Landroid/support/v7/app/i;->onPause()V

    .line 807
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50169
    iput-boolean v4, v0, Lcom/teamspeak/ts3client/Ts3Application;->g:Z

    .line 809
    :try_start_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50171
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50172
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 809
    invoke-static {}, Lcom/teamspeak/ts3client/ConnectionBackground;->a()Landroid/app/Notification;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 810
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 811
    new-instance v1, Lcom/teamspeak/ts3client/ca;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/ca;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_23
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_23} :catch_3e

    .line 822
    :cond_23
    :goto_23
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50173
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 822
    if-eqz v0, :cond_3b

    .line 823
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50174
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50175
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 823
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->A()V

    .line 824
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50176
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50177
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 824
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->c()V

    .line 833
    :cond_3b
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->C:Z

    .line 834
    return-void

    :catch_3e
    move-exception v0

    goto :goto_23
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1112
    packed-switch p1, :pswitch_data_30

    .line 1141
    :cond_4
    :goto_4
    return-void

    .line 1115
    :pswitch_5
    array-length v0, p3

    if-lez v0, :cond_27

    aget v0, p3, v1

    if-nez v0, :cond_27

    .line 1117
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->w()V

    .line 1118
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->L()V

    .line 1125
    :goto_12
    :pswitch_12
    array-length v0, p3

    if-lez v0, :cond_2b

    aget v0, p3, v1

    if-nez v0, :cond_2b

    .line 1127
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->M()V

    .line 1134
    :goto_1c
    :pswitch_1c
    array-length v0, p3

    if-lez v0, :cond_23

    aget v0, p3, v1

    if-eqz v0, :cond_4

    .line 1137
    :cond_23
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->I()V

    goto :goto_4

    .line 1120
    :cond_27
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->I()V

    goto :goto_12

    .line 1129
    :cond_2b
    invoke-direct {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->I()V

    goto :goto_1c

    .line 1112
    nop

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_5
        :pswitch_12
        :pswitch_1c
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 838
    invoke-super {p0, p1}, Landroid/support/v7/app/i;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 839
    return-void
.end method

.method protected onResume()V
    .registers 11

    .prologue
    const/16 v3, 0x2002

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 843
    invoke-super {p0}, Landroid/support/v7/app/i;->onResume()V

    .line 50178
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->N:Ljava/lang/String;

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 845
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50180
    iput-boolean v9, v0, Lcom/teamspeak/ts3client/Ts3Application;->g:Z

    .line 846
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 847
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50182
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 847
    const-string v1, "use_proximity"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 849
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->r:Z

    if-eqz v0, :cond_5d

    .line 850
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50183
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->g:Z

    .line 850
    if-eqz v0, :cond_5b

    .line 851
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->f()I

    move-result v0

    if-lez v0, :cond_39

    .line 852
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->e()V

    .line 854
    :cond_39
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 855
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50184
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 855
    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 856
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v2, "Bookmark"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 857
    invoke-virtual {v0, v3}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 858
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 861
    :cond_5b
    iput-boolean v8, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->r:Z

    .line 871
    :cond_5d
    iput-boolean v9, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->C:Z

    .line 872
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->p:Z

    if-eqz v0, :cond_a2

    .line 50185
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->f()I

    move-result v0

    if-lez v0, :cond_74

    .line 50186
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->e()V

    .line 50188
    :cond_74
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 50189
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50197
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 50189
    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 50190
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v2, "Bookmark"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 50191
    invoke-virtual {v0, v3}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 50192
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 50194
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->l()V

    .line 50195
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50198
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50199
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 50195
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->I()V

    .line 875
    :cond_a2
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->s:Z

    if-eqz v0, :cond_e3

    .line 877
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->f()I

    move-result v0

    if-lez v0, :cond_b7

    .line 878
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->e()V

    .line 880
    :cond_b7
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 881
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50200
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 881
    instance-of v1, v1, Lcom/teamspeak/ts3client/bookmark/e;

    if-nez v1, :cond_ce

    .line 882
    iget-object v1, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50201
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 882
    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 883
    :cond_ce
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v2, "Bookmark"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/cd;->f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 884
    invoke-virtual {v0, v3}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 885
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 886
    iput-boolean v8, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->s:Z

    .line 888
    :cond_e3
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50202
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 888
    if-eqz v0, :cond_f2

    .line 889
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50203
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50204
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 889
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->d()V

    .line 891
    :cond_f2
    invoke-static {}, Lcom/teamspeak/ts3client/data/af;->a()Lcom/teamspeak/ts3client/data/af;

    move-result-object v0

    .line 50205
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/data/af;->e:Z

    if-nez v1, :cond_140

    .line 50207
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 50209
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 50219
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 50209
    const-string v3, "LicenseAgreement"

    invoke-interface {v2, v3, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_126

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 50220
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 50209
    const-string v3, "la_lastcheck"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x5265c00

    sub-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gez v1, :cond_141

    .line 50210
    :cond_126
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_android_getUpdaterURL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/teamspeak/ts3client/data/af;->b:Ljava/lang/String;

    .line 50211
    iput-boolean v9, v0, Lcom/teamspeak/ts3client/data/af;->e:Z

    .line 50212
    new-instance v1, Lcom/teamspeak/ts3client/data/ah;

    invoke-direct {v1, v0, v8}, Lcom/teamspeak/ts3client/data/ah;-><init>(Lcom/teamspeak/ts3client/data/af;B)V

    new-array v2, v9, [Ljava/lang/String;

    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->b:Ljava/lang/String;

    aput-object v0, v2, v8

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/ah;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_140
    :goto_140
    return-void

    .line 50214
    :cond_141
    const-string v1, "UpdateServerData"

    const-string v2, "skipped downloading new update info"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50215
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/z;->o()V

    goto :goto_140
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 914
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1012
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v5

    .line 1013
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    .line 1014
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    .line 1015
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 1016
    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    .line 1017
    const-wide v2, 0x4066800000000000L    # 180.0

    mul-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v0, v2

    .line 1018
    const-wide v2, 0x4050400000000000L    # 65.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_40

    .line 1020
    iget v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->I:I

    if-ne v0, v4, :cond_36

    .line 1035
    :cond_35
    :goto_35
    return-void

    .line 1023
    :cond_36
    iput v4, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->I:I

    .line 1024
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50240
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 1024
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_35

    .line 1028
    :cond_40
    iget v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->I:I

    if-eqz v0, :cond_35

    .line 1031
    iput v5, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->I:I

    .line 1032
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50241
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 1032
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_35
.end method

.method public sendNewBookmark(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 917
    iget-object v0, p0, Lcom/teamspeak/ts3client/StartGUIFragment;->n:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50221
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 917
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "NewBookmark"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 918
    new-instance v0, Lcom/teamspeak/ts3client/bookmark/a;

    .line 50222
    sget-object v1, Lcom/teamspeak/ts3client/data/b/a;->a:Lcom/teamspeak/ts3client/data/b/a;

    .line 918
    invoke-direct {v0, v1}, Lcom/teamspeak/ts3client/bookmark/a;-><init>(Lcom/teamspeak/ts3client/data/b/a;)V

    .line 919
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v1

    const-string v2, "AddBookmark"

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/bookmark/a;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 921
    return-void
.end method
