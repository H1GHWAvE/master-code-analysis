.class final Lcom/teamspeak/ts3client/d;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/c;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/c;)V
    .registers 2

    .prologue
    .line 147
    iput-object p1, p0, Lcom/teamspeak/ts3client/d;->a:Lcom/teamspeak/ts3client/c;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .registers 9

    .prologue
    const/4 v5, 0x1

    .line 151
    const-string v0, "client://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 181
    :cond_9
    :goto_9
    return v5

    .line 153
    :cond_a
    const-string v0, "ts3file://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 155
    const-string v0, "ts3server://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 157
    const-string v0, "channelid://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 159
    :try_start_22
    const-string v0, "channelid://(\\d+).*"

    const-string v1, "$1"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 160
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 1061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 161
    if-eqz v2, :cond_9

    .line 162
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/b;->d(Ljava/lang/Long;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 163
    new-instance v3, Lcom/teamspeak/ts3client/d/a/a;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, v1}, Lcom/teamspeak/ts3client/d/a/a;-><init>(Lcom/teamspeak/ts3client/data/a;Z)V

    .line 1206
    iget-object v0, v2, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1688
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 164
    const-string v1, "ChannelActionDialog"

    invoke-virtual {v3, v0, v1}, Lcom/teamspeak/ts3client/d/a/a;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
    :try_end_5f
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_5f} :catch_60

    goto :goto_9

    :catch_60
    move-exception v0

    goto :goto_9

    .line 171
    :cond_62
    :try_start_62
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 171
    invoke-virtual {v0, p2}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_bbcode_shouldPrependHTTP(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 174
    :cond_7f
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 175
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 176
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 177
    const-string v0, "com.android.browser.application_id"

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_9a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_62 .. :try_end_9a} :catch_9c

    goto/16 :goto_9

    :catch_9c
    move-exception v0

    goto/16 :goto_9
.end method
