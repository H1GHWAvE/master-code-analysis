.bytecode 50.0
.class public synchronized org/xbill/DNS/EDNSOption$Code
.super java/lang/Object

.field public static final 'CLIENT_SUBNET' I = 8


.field public static final 'NSID' I = 3


.field private static 'codes' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "EDNS Option Codes"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/EDNSOption$Code/codes Lorg/xbill/DNS/Mnemonic;
aload 0
ldc_w 65535
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/EDNSOption$Code/codes Lorg/xbill/DNS/Mnemonic;
ldc "CODE"
invokevirtual org/xbill/DNS/Mnemonic/setPrefix(Ljava/lang/String;)V
getstatic org/xbill/DNS/EDNSOption$Code/codes Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/EDNSOption$Code/codes Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "NSID"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/EDNSOption$Code/codes Lorg/xbill/DNS/Mnemonic;
bipush 8
ldc "CLIENT_SUBNET"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/EDNSOption$Code/codes Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/EDNSOption$Code/codes Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
