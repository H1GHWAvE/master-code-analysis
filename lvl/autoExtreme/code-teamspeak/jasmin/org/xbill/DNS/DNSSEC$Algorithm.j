.bytecode 50.0
.class public synchronized org/xbill/DNS/DNSSEC$Algorithm
.super java/lang/Object

.field public static final 'DH' I = 2


.field public static final 'DSA' I = 3


.field public static final 'DSA_NSEC3_SHA1' I = 6


.field public static final 'ECC_GOST' I = 12


.field public static final 'ECDSAP256SHA256' I = 13


.field public static final 'ECDSAP384SHA384' I = 14


.field public static final 'INDIRECT' I = 252


.field public static final 'PRIVATEDNS' I = 253


.field public static final 'PRIVATEOID' I = 254


.field public static final 'RSAMD5' I = 1


.field public static final 'RSASHA1' I = 5


.field public static final 'RSASHA256' I = 8


.field public static final 'RSASHA512' I = 10


.field public static final 'RSA_NSEC3_SHA1' I = 7


.field private static 'algs' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "DNSSEC algorithm"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
aload 0
sipush 255
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "RSAMD5"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "DH"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "DSA"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
iconst_5
ldc "RSASHA1"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
bipush 6
ldc "DSA-NSEC3-SHA1"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
bipush 7
ldc "RSA-NSEC3-SHA1"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
bipush 8
ldc "RSASHA256"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
bipush 10
ldc "RSASHA512"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
bipush 12
ldc "ECC-GOST"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
bipush 13
ldc "ECDSAP256SHA256"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
bipush 14
ldc "ECDSAP384SHA384"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
sipush 252
ldc "INDIRECT"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
sipush 253
ldc "PRIVATEDNS"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
sipush 254
ldc "PRIVATEOID"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/DNSSEC$Algorithm/algs Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
