.bytecode 50.0
.class public synchronized org/xbill/DNS/SOARecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 1049740098229303931L


.field private 'admin' Lorg/xbill/DNS/Name;

.field private 'expire' J

.field private 'host' Lorg/xbill/DNS/Name;

.field private 'minimum' J

.field private 'refresh' J

.field private 'retry' J

.field private 'serial' J

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;JJJJJ)V
aload 0
aload 1
bipush 6
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "host"
aload 5
invokestatic org/xbill/DNS/SOARecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SOARecord/host Lorg/xbill/DNS/Name;
aload 0
ldc "admin"
aload 6
invokestatic org/xbill/DNS/SOARecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SOARecord/admin Lorg/xbill/DNS/Name;
aload 0
ldc "serial"
lload 7
invokestatic org/xbill/DNS/SOARecord/checkU32(Ljava/lang/String;J)J
putfield org/xbill/DNS/SOARecord/serial J
aload 0
ldc "refresh"
lload 9
invokestatic org/xbill/DNS/SOARecord/checkU32(Ljava/lang/String;J)J
putfield org/xbill/DNS/SOARecord/refresh J
aload 0
ldc "retry"
lload 11
invokestatic org/xbill/DNS/SOARecord/checkU32(Ljava/lang/String;J)J
putfield org/xbill/DNS/SOARecord/retry J
aload 0
ldc "expire"
lload 13
invokestatic org/xbill/DNS/SOARecord/checkU32(Ljava/lang/String;J)J
putfield org/xbill/DNS/SOARecord/expire J
aload 0
ldc "minimum"
lload 15
invokestatic org/xbill/DNS/SOARecord/checkU32(Ljava/lang/String;J)J
putfield org/xbill/DNS/SOARecord/minimum J
return
.limit locals 17
.limit stack 6
.end method

.method public getAdmin()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/SOARecord/admin Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getExpire()J
aload 0
getfield org/xbill/DNS/SOARecord/expire J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getHost()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/SOARecord/host Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getMinimum()J
aload 0
getfield org/xbill/DNS/SOARecord/minimum J
lreturn
.limit locals 1
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/SOARecord
dup
invokespecial org/xbill/DNS/SOARecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getRefresh()J
aload 0
getfield org/xbill/DNS/SOARecord/refresh J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getRetry()J
aload 0
getfield org/xbill/DNS/SOARecord/retry J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSerial()J
aload 0
getfield org/xbill/DNS/SOARecord/serial J
lreturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SOARecord/host Lorg/xbill/DNS/Name;
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SOARecord/admin Lorg/xbill/DNS/Name;
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt32()J
putfield org/xbill/DNS/SOARecord/serial J
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getTTLLike()J
putfield org/xbill/DNS/SOARecord/refresh J
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getTTLLike()J
putfield org/xbill/DNS/SOARecord/retry J
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getTTLLike()J
putfield org/xbill/DNS/SOARecord/expire J
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getTTLLike()J
putfield org/xbill/DNS/SOARecord/minimum J
return
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/SOARecord/host Lorg/xbill/DNS/Name;
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/SOARecord/admin Lorg/xbill/DNS/Name;
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/SOARecord/serial J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/SOARecord/refresh J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/SOARecord/retry J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/SOARecord/expire J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/SOARecord/minimum J
return
.limit locals 2
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/host Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/admin Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
aload 1
ldc " (\n\u0009\u0009\u0009\u0009\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/serial J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc "\u0009; serial\n\u0009\u0009\u0009\u0009\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/refresh J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc "\u0009; refresh\n\u0009\u0009\u0009\u0009\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/retry J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc "\u0009; retry\n\u0009\u0009\u0009\u0009\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/expire J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc "\u0009; expire\n\u0009\u0009\u0009\u0009\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/minimum J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc " )\u0009; minimum"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L1:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L0:
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/serial J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/refresh J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/retry J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/expire J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/minimum J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
goto L1
.limit locals 2
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/SOARecord/host Lorg/xbill/DNS/Name;
aload 1
aload 2
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/SOARecord/admin Lorg/xbill/DNS/Name;
aload 1
aload 2
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/serial J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/refresh J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/retry J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/expire J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/SOARecord/minimum J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
return
.limit locals 4
.limit stack 4
.end method
