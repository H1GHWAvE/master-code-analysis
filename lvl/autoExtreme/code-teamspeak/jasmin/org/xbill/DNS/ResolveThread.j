.bytecode 50.0
.class synchronized org/xbill/DNS/ResolveThread
.super java/lang/Thread

.field private 'id' Ljava/lang/Object;

.field private 'listener' Lorg/xbill/DNS/ResolverListener;

.field private 'query' Lorg/xbill/DNS/Message;

.field private 'res' Lorg/xbill/DNS/Resolver;

.method public <init>(Lorg/xbill/DNS/Resolver;Lorg/xbill/DNS/Message;Ljava/lang/Object;Lorg/xbill/DNS/ResolverListener;)V
aload 0
invokespecial java/lang/Thread/<init>()V
aload 0
aload 1
putfield org/xbill/DNS/ResolveThread/res Lorg/xbill/DNS/Resolver;
aload 0
aload 2
putfield org/xbill/DNS/ResolveThread/query Lorg/xbill/DNS/Message;
aload 0
aload 3
putfield org/xbill/DNS/ResolveThread/id Ljava/lang/Object;
aload 0
aload 4
putfield org/xbill/DNS/ResolveThread/listener Lorg/xbill/DNS/ResolverListener;
return
.limit locals 5
.limit stack 2
.end method

.method public run()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
getfield org/xbill/DNS/ResolveThread/res Lorg/xbill/DNS/Resolver;
aload 0
getfield org/xbill/DNS/ResolveThread/query Lorg/xbill/DNS/Message;
invokeinterface org/xbill/DNS/Resolver/send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message; 1
astore 1
aload 0
getfield org/xbill/DNS/ResolveThread/listener Lorg/xbill/DNS/ResolverListener;
aload 0
getfield org/xbill/DNS/ResolveThread/id Ljava/lang/Object;
aload 1
invokeinterface org/xbill/DNS/ResolverListener/receiveMessage(Ljava/lang/Object;Lorg/xbill/DNS/Message;)V 2
L1:
return
L2:
astore 1
aload 0
getfield org/xbill/DNS/ResolveThread/listener Lorg/xbill/DNS/ResolverListener;
aload 0
getfield org/xbill/DNS/ResolveThread/id Ljava/lang/Object;
aload 1
invokeinterface org/xbill/DNS/ResolverListener/handleException(Ljava/lang/Object;Ljava/lang/Exception;)V 2
return
.limit locals 2
.limit stack 3
.end method
