.bytecode 50.0
.class public synchronized org/xbill/DNS/Zone
.super java/lang/Object
.implements java/io/Serializable

.field public static final 'PRIMARY' I = 1


.field public static final 'SECONDARY' I = 2


.field private static final 'serialVersionUID' J = -9220510891189510942L


.field private 'NS' Lorg/xbill/DNS/RRset;

.field private 'SOA' Lorg/xbill/DNS/SOARecord;

.field private 'data' Ljava/util/Map;

.field private 'dclass' I

.field private 'hasWild' Z

.field private 'origin' Lorg/xbill/DNS/Name;

.field private 'originNode' Ljava/lang/Object;

.method public <init>(Lorg/xbill/DNS/Name;ILjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield org/xbill/DNS/Zone/dclass I
aload 1
aload 3
aconst_null
invokestatic org/xbill/DNS/ZoneTransferIn/newAXFR(Lorg/xbill/DNS/Name;Ljava/lang/String;Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
astore 1
aload 1
iload 2
invokevirtual org/xbill/DNS/ZoneTransferIn/setDClass(I)V
aload 0
aload 1
invokespecial org/xbill/DNS/Zone/fromXFR(Lorg/xbill/DNS/ZoneTransferIn;)V
return
.limit locals 4
.limit stack 3
.end method

.method public <init>(Lorg/xbill/DNS/Name;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield org/xbill/DNS/Zone/dclass I
aload 0
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
putfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "no zone name specified"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
new org/xbill/DNS/Master
dup
aload 2
aload 1
invokespecial org/xbill/DNS/Master/<init>(Ljava/lang/String;Lorg/xbill/DNS/Name;)V
astore 2
aload 0
aload 1
putfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
L1:
aload 2
invokevirtual org/xbill/DNS/Master/nextRecord()Lorg/xbill/DNS/Record;
astore 1
aload 1
ifnull L2
aload 0
aload 1
invokespecial org/xbill/DNS/Zone/maybeAddRecord(Lorg/xbill/DNS/Record;)V
goto L1
L2:
aload 0
invokespecial org/xbill/DNS/Zone/validate()V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Lorg/xbill/DNS/Name;[Lorg/xbill/DNS/Record;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield org/xbill/DNS/Zone/dclass I
aload 0
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
putfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "no zone name specified"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
iconst_0
istore 3
L1:
iload 3
aload 2
arraylength
if_icmpge L2
aload 0
aload 2
iload 3
aaload
invokespecial org/xbill/DNS/Zone/maybeAddRecord(Lorg/xbill/DNS/Record;)V
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 0
invokespecial org/xbill/DNS/Zone/validate()V
return
.limit locals 4
.limit stack 3
.end method

.method public <init>(Lorg/xbill/DNS/ZoneTransferIn;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield org/xbill/DNS/Zone/dclass I
aload 0
aload 1
invokespecial org/xbill/DNS/Zone/fromXFR(Lorg/xbill/DNS/ZoneTransferIn;)V
return
.limit locals 2
.limit stack 2
.end method

.method static access$000(Lorg/xbill/DNS/Zone;)Ljava/util/Map;
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method static access$100(Lorg/xbill/DNS/Zone;)Ljava/lang/Object;
aload 0
getfield org/xbill/DNS/Zone/originNode Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method static access$200(Lorg/xbill/DNS/Zone;Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
aload 0
aload 1
invokespecial org/xbill/DNS/Zone/allRRsets(Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
areturn
.limit locals 2
.limit stack 2
.end method

.method static access$300(Lorg/xbill/DNS/Zone;Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
aload 0
aload 1
iload 2
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
areturn
.limit locals 3
.limit stack 3
.end method

.method static access$400(Lorg/xbill/DNS/Zone;)Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method private addRRset(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/RRset;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
.catch all from L14 to L15 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Zone/hasWild Z
ifne L1
aload 1
invokevirtual org/xbill/DNS/Name/isWild()Z
ifeq L1
aload 0
iconst_1
putfield org/xbill/DNS/Zone/hasWild Z
L1:
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 5
L3:
aload 5
ifnonnull L6
L4:
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L5:
aload 0
monitorexit
return
L6:
aload 2
invokevirtual org/xbill/DNS/RRset/getType()I
istore 4
aload 5
instanceof java/util/List
ifeq L12
aload 5
checkcast java/util/List
astore 1
L7:
iconst_0
istore 3
L8:
iload 3
aload 1
invokeinterface java/util/List/size()I 0
if_icmpge L10
aload 1
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/RRset
invokevirtual org/xbill/DNS/RRset/getType()I
iload 4
if_icmpne L16
aload 1
iload 3
aload 2
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
pop
L9:
goto L5
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L16:
iload 3
iconst_1
iadd
istore 3
goto L8
L10:
aload 1
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L11:
goto L5
L12:
aload 5
checkcast org/xbill/DNS/RRset
astore 5
aload 5
invokevirtual org/xbill/DNS/RRset/getType()I
iload 4
if_icmpne L14
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L13:
goto L5
L14:
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
astore 6
aload 6
aload 5
invokevirtual java/util/LinkedList/add(Ljava/lang/Object;)Z
pop
aload 6
aload 2
invokevirtual java/util/LinkedList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
aload 6
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L15:
goto L5
.limit locals 7
.limit stack 3
.end method

.method private allRRsets(Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 1
instanceof java/util/List
ifeq L3
aload 1
checkcast java/util/List
astore 1
aload 1
aload 1
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/RRset
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/RRset;
checkcast [Lorg/xbill/DNS/RRset;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L3:
aload 1
checkcast org/xbill/DNS/RRset
astore 2
iconst_1
anewarray org/xbill/DNS/RRset
astore 1
L4:
aload 1
iconst_0
aload 2
aastore
goto L1
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 3
.end method

.method private exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method private findRRset(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/RRset;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 0
aload 1
invokespecial org/xbill/DNS/Zone/exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
astore 1
L1:
aload 1
ifnonnull L3
aconst_null
astore 1
L5:
aload 0
monitorexit
aload 1
areturn
L3:
aload 0
aload 1
iload 2
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
astore 1
L4:
goto L5
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 3
.end method

.method private fromXFR(Lorg/xbill/DNS/ZoneTransferIn;)V
aload 0
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
putfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 0
aload 1
invokevirtual org/xbill/DNS/ZoneTransferIn/getName()Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
aload 1
invokevirtual org/xbill/DNS/ZoneTransferIn/run()Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/Record
invokespecial org/xbill/DNS/Zone/maybeAddRecord(Lorg/xbill/DNS/Record;)V
goto L0
L1:
aload 1
invokevirtual org/xbill/DNS/ZoneTransferIn/isAXFR()Z
ifne L2
new java/lang/IllegalArgumentException
dup
ldc "zones can only be created from AXFRs"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
invokespecial org/xbill/DNS/Zone/validate()V
return
.limit locals 3
.limit stack 3
.end method

.method private lookup(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/SetResponse;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
.catch all from L14 to L15 using L2
.catch all from L16 to L17 using L2
.catch all from L18 to L19 using L2
.catch all from L20 to L21 using L2
.catch all from L22 to L23 using L2
.catch all from L24 to L25 using L2
.catch all from L26 to L27 using L2
.catch all from L28 to L29 using L2
.catch all from L30 to L31 using L2
.catch all from L32 to L33 using L2
.catch all from L34 to L35 using L2
.catch all from L36 to L37 using L2
.catch all from L38 to L39 using L2
.catch all from L40 to L41 using L2
iconst_0
istore 7
aload 0
monitorenter
L0:
aload 1
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/subdomain(Lorg/xbill/DNS/Name;)Z
ifne L3
iconst_1
invokestatic org/xbill/DNS/SetResponse/ofType(I)Lorg/xbill/DNS/SetResponse;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L3:
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
istore 8
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/labels()I
istore 6
L4:
iload 6
istore 3
goto L42
L43:
iload 4
ifeq L44
L5:
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
astore 9
L6:
aload 0
aload 9
invokespecial org/xbill/DNS/Zone/exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
astore 10
L7:
aload 10
ifnull L45
iload 4
ifne L46
L8:
aload 0
aload 10
iconst_2
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
astore 9
L9:
aload 9
ifnull L46
L10:
new org/xbill/DNS/SetResponse
dup
iconst_3
aload 9
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 1
L11:
goto L1
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L47:
iconst_0
istore 4
goto L48
L49:
iconst_0
istore 5
goto L43
L44:
iload 5
ifeq L12
aload 1
astore 9
goto L6
L12:
new org/xbill/DNS/Name
dup
aload 1
iload 8
iload 3
isub
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/Name;I)V
astore 9
L13:
goto L6
L46:
iload 5
ifeq L50
iload 2
sipush 255
if_icmpne L50
L14:
new org/xbill/DNS/SetResponse
dup
bipush 6
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
astore 9
aload 0
aload 10
invokespecial org/xbill/DNS/Zone/allRRsets(Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
astore 10
L15:
iload 7
istore 2
L51:
aload 9
astore 1
L16:
iload 2
aload 10
arraylength
if_icmpge L1
aload 9
aload 10
iload 2
aaload
invokevirtual org/xbill/DNS/SetResponse/addRRset(Lorg/xbill/DNS/RRset;)V
L17:
iload 2
iconst_1
iadd
istore 2
goto L51
L50:
iload 5
ifeq L26
L18:
aload 0
aload 10
iload 2
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
astore 9
L19:
aload 9
ifnull L22
L20:
new org/xbill/DNS/SetResponse
dup
bipush 6
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
astore 1
aload 1
aload 9
invokevirtual org/xbill/DNS/SetResponse/addRRset(Lorg/xbill/DNS/RRset;)V
L21:
goto L1
L22:
aload 0
aload 10
iconst_5
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
astore 9
L23:
aload 9
ifnull L52
L24:
new org/xbill/DNS/SetResponse
dup
iconst_4
aload 9
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 1
L25:
goto L1
L26:
aload 0
aload 10
bipush 39
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
astore 9
L27:
aload 9
ifnull L52
L28:
new org/xbill/DNS/SetResponse
dup
iconst_5
aload 9
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 1
L29:
goto L1
L52:
iload 5
ifeq L45
L30:
iconst_2
invokestatic org/xbill/DNS/SetResponse/ofType(I)Lorg/xbill/DNS/SetResponse;
astore 1
L31:
goto L1
L32:
aload 0
getfield org/xbill/DNS/Zone/hasWild Z
ifeq L40
L33:
iconst_0
istore 3
L53:
iload 3
iload 8
iload 6
isub
if_icmpge L40
L34:
aload 0
aload 1
iload 3
iconst_1
iadd
invokevirtual org/xbill/DNS/Name/wild(I)Lorg/xbill/DNS/Name;
invokespecial org/xbill/DNS/Zone/exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
astore 9
L35:
aload 9
ifnull L54
L36:
aload 0
aload 9
iload 2
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
astore 9
L37:
aload 9
ifnull L54
L38:
new org/xbill/DNS/SetResponse
dup
bipush 6
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
astore 1
aload 1
aload 9
invokevirtual org/xbill/DNS/SetResponse/addRRset(Lorg/xbill/DNS/RRset;)V
L39:
goto L1
L40:
iconst_1
invokestatic org/xbill/DNS/SetResponse/ofType(I)Lorg/xbill/DNS/SetResponse;
astore 1
L41:
goto L1
L42:
iload 3
iload 8
if_icmpgt L32
iload 3
iload 6
if_icmpne L47
iconst_1
istore 4
L48:
iload 3
iload 8
if_icmpne L49
iconst_1
istore 5
goto L43
L45:
iload 3
iconst_1
iadd
istore 3
goto L42
L54:
iload 3
iconst_1
iadd
istore 3
goto L53
.limit locals 11
.limit stack 5
.end method

.method private final maybeAddRecord(Lorg/xbill/DNS/Record;)V
aload 1
invokevirtual org/xbill/DNS/Record/getType()I
istore 2
aload 1
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
astore 3
iload 2
bipush 6
if_icmpne L0
aload 3
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifne L0
new java/io/IOException
dup
new java/lang/StringBuffer
dup
ldc "SOA owner "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc " does not match zone origin "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 3
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/subdomain(Lorg/xbill/DNS/Name;)Z
ifeq L1
aload 0
aload 1
invokevirtual org/xbill/DNS/Zone/addRecord(Lorg/xbill/DNS/Record;)V
L1:
return
.limit locals 4
.limit stack 5
.end method

.method private nodeToString(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
aload 0
aload 2
invokespecial org/xbill/DNS/Zone/allRRsets(Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
astore 2
iconst_0
istore 3
L0:
iload 3
aload 2
arraylength
if_icmpge L1
aload 2
iload 3
aaload
astore 4
aload 4
invokevirtual org/xbill/DNS/RRset/rrs()Ljava/util/Iterator;
astore 5
L2:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L2
L3:
aload 4
invokevirtual org/xbill/DNS/RRset/sigs()Ljava/util/Iterator;
astore 4
L4:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L4
L5:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
return
.limit locals 6
.limit stack 3
.end method

.method private oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
.catch all from L4 to L5 using L1
.catch all from L6 to L7 using L1
aload 0
monitorenter
iload 2
sipush 255
if_icmpne L2
L0:
new java/lang/IllegalArgumentException
dup
ldc "oneRRset(ANY)"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 0
monitorexit
aload 1
athrow
L2:
aload 1
instanceof java/util/List
ifeq L6
aload 1
checkcast java/util/List
astore 5
L3:
iconst_0
istore 3
L4:
iload 3
aload 5
invokeinterface java/util/List/size()I 0
if_icmpge L8
aload 5
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/RRset
astore 1
aload 1
invokevirtual org/xbill/DNS/RRset/getType()I
istore 4
L5:
iload 4
iload 2
if_icmpne L9
L10:
aload 0
monitorexit
aload 1
areturn
L9:
iload 3
iconst_1
iadd
istore 3
goto L4
L6:
aload 1
checkcast org/xbill/DNS/RRset
astore 1
aload 1
invokevirtual org/xbill/DNS/RRset/getType()I
istore 3
L7:
iload 3
iload 2
if_icmpne L8
goto L10
L8:
aconst_null
astore 1
goto L10
.limit locals 6
.limit stack 3
.end method

.method private removeRRset(Lorg/xbill/DNS/Name;I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 4
L1:
aload 4
ifnonnull L3
L9:
aload 0
monitorexit
return
L3:
aload 4
instanceof java/util/List
ifeq L7
aload 4
checkcast java/util/List
astore 4
L4:
iconst_0
istore 3
L5:
iload 3
aload 4
invokeinterface java/util/List/size()I 0
if_icmpge L9
aload 4
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/RRset
invokevirtual org/xbill/DNS/RRset/getType()I
iload 2
if_icmpne L10
aload 4
iload 3
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
aload 4
invokeinterface java/util/List/size()I 0
ifne L9
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
L6:
goto L9
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L10:
iload 3
iconst_1
iadd
istore 3
goto L5
L7:
aload 4
checkcast org/xbill/DNS/RRset
invokevirtual org/xbill/DNS/RRset/getType()I
iload 2
if_icmpne L9
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
L8:
goto L9
.limit locals 5
.limit stack 2
.end method

.method private validate()V
aload 0
aload 0
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokespecial org/xbill/DNS/Zone/exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
putfield org/xbill/DNS/Zone/originNode Ljava/lang/Object;
aload 0
getfield org/xbill/DNS/Zone/originNode Ljava/lang/Object;
ifnonnull L0
new java/io/IOException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc ": no data specified"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 0
getfield org/xbill/DNS/Zone/originNode Ljava/lang/Object;
bipush 6
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
astore 1
aload 1
ifnull L1
aload 1
invokevirtual org/xbill/DNS/RRset/size()I
iconst_1
if_icmpeq L2
L1:
new java/io/IOException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc ": exactly 1 SOA must be specified"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
aload 1
invokevirtual org/xbill/DNS/RRset/rrs()Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/SOARecord
putfield org/xbill/DNS/Zone/SOA Lorg/xbill/DNS/SOARecord;
aload 0
aload 0
aload 0
getfield org/xbill/DNS/Zone/originNode Ljava/lang/Object;
iconst_2
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
putfield org/xbill/DNS/Zone/NS Lorg/xbill/DNS/RRset;
aload 0
getfield org/xbill/DNS/Zone/NS Lorg/xbill/DNS/RRset;
ifnonnull L3
new java/io/IOException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc ": no NS set specified"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L3:
return
.limit locals 2
.limit stack 4
.end method

.method public AXFR()Ljava/util/Iterator;
new org/xbill/DNS/Zone$ZoneIterator
dup
aload 0
iconst_1
invokespecial org/xbill/DNS/Zone$ZoneIterator/<init>(Lorg/xbill/DNS/Zone;Z)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public addRRset(Lorg/xbill/DNS/RRset;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
aload 1
invokespecial org/xbill/DNS/Zone/addRRset(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/RRset;)V
return
.limit locals 2
.limit stack 3
.end method

.method public addRecord(Lorg/xbill/DNS/Record;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
aload 1
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
astore 3
aload 1
invokevirtual org/xbill/DNS/Record/getRRsetType()I
istore 2
aload 0
monitorenter
L0:
aload 0
aload 3
iload 2
invokespecial org/xbill/DNS/Zone/findRRset(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/RRset;
astore 4
L1:
aload 4
ifnonnull L6
L3:
aload 0
aload 3
new org/xbill/DNS/RRset
dup
aload 1
invokespecial org/xbill/DNS/RRset/<init>(Lorg/xbill/DNS/Record;)V
invokespecial org/xbill/DNS/Zone/addRRset(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/RRset;)V
L4:
aload 0
monitorexit
L5:
return
L6:
aload 4
aload 1
invokevirtual org/xbill/DNS/RRset/addRR(Lorg/xbill/DNS/Record;)V
L7:
goto L4
L2:
astore 1
L8:
aload 0
monitorexit
L9:
aload 1
athrow
.limit locals 5
.limit stack 5
.end method

.method public findExactMatch(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/RRset;
aload 0
aload 1
invokespecial org/xbill/DNS/Zone/exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
iload 2
invokespecial org/xbill/DNS/Zone/oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
areturn
.limit locals 3
.limit stack 3
.end method

.method public findRecords(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/SetResponse;
aload 0
aload 1
iload 2
invokespecial org/xbill/DNS/Zone/lookup(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/SetResponse;
areturn
.limit locals 3
.limit stack 3
.end method

.method public getDClass()I
aload 0
getfield org/xbill/DNS/Zone/dclass I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getNS()Lorg/xbill/DNS/RRset;
aload 0
getfield org/xbill/DNS/Zone/NS Lorg/xbill/DNS/RRset;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOrigin()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSOA()Lorg/xbill/DNS/SOARecord;
aload 0
getfield org/xbill/DNS/Zone/SOA Lorg/xbill/DNS/SOARecord;
areturn
.limit locals 1
.limit stack 1
.end method

.method public iterator()Ljava/util/Iterator;
new org/xbill/DNS/Zone$ZoneIterator
dup
aload 0
iconst_0
invokespecial org/xbill/DNS/Zone$ZoneIterator/<init>(Lorg/xbill/DNS/Zone;Z)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public removeRecord(Lorg/xbill/DNS/Record;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
aload 1
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
astore 3
aload 1
invokevirtual org/xbill/DNS/Record/getRRsetType()I
istore 2
aload 0
monitorenter
L0:
aload 0
aload 3
iload 2
invokespecial org/xbill/DNS/Zone/findRRset(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/RRset;
astore 4
L1:
aload 4
ifnonnull L5
L3:
aload 0
monitorexit
L4:
return
L5:
aload 4
invokevirtual org/xbill/DNS/RRset/size()I
iconst_1
if_icmpne L10
aload 4
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
aload 1
invokevirtual org/xbill/DNS/Record/equals(Ljava/lang/Object;)Z
ifeq L10
aload 0
aload 3
iload 2
invokespecial org/xbill/DNS/Zone/removeRRset(Lorg/xbill/DNS/Name;I)V
L6:
aload 0
monitorexit
L7:
return
L2:
astore 1
L8:
aload 0
monitorexit
L9:
aload 1
athrow
L10:
aload 4
aload 1
invokevirtual org/xbill/DNS/RRset/deleteRR(Lorg/xbill/DNS/Record;)V
L11:
goto L6
.limit locals 5
.limit stack 3
.end method

.method public toMasterFile()Ljava/lang/String;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Zone/data Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
aload 0
aload 2
aload 0
getfield org/xbill/DNS/Zone/originNode Ljava/lang/Object;
invokespecial org/xbill/DNS/Zone/nodeToString(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 0
getfield org/xbill/DNS/Zone/origin Lorg/xbill/DNS/Name;
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifne L1
aload 0
aload 2
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokespecial org/xbill/DNS/Zone/nodeToString(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
L3:
goto L1
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L4:
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
astore 1
L5:
aload 0
monitorexit
aload 1
areturn
.limit locals 4
.limit stack 3
.end method

.method public toString()Ljava/lang/String;
aload 0
invokevirtual org/xbill/DNS/Zone/toMasterFile()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
