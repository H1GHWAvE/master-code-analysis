.bytecode 50.0
.class public synchronized org/xbill/DNS/NSAPRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -1037209403185658593L


.field private 'address' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/lang/String;)V
aload 0
aload 1
bipush 22
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
aload 5
invokestatic org/xbill/DNS/NSAPRecord/checkAndConvertAddress(Ljava/lang/String;)[B
putfield org/xbill/DNS/NSAPRecord/address [B
aload 0
getfield org/xbill/DNS/NSAPRecord/address [B
ifnonnull L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "invalid NSAP address "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 6
.limit stack 6
.end method

.method private static final checkAndConvertAddress(Ljava/lang/String;)[B
iconst_2
istore 4
aload 0
iconst_0
iconst_2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
ldc "0x"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifne L0
aconst_null
areturn
L0:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 7
iconst_0
istore 6
iconst_0
istore 5
L1:
iload 4
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L2
aload 0
iload 4
invokevirtual java/lang/String/charAt(I)C
istore 1
iload 6
istore 2
iload 5
istore 3
iload 1
bipush 46
if_icmpeq L3
iload 1
bipush 16
invokestatic java/lang/Character/digit(CI)I
istore 2
iload 2
iconst_m1
if_icmpne L4
aconst_null
areturn
L4:
iload 5
ifeq L5
iload 6
iload 2
iadd
istore 2
aload 7
iload 2
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iconst_0
istore 3
L3:
iload 4
iconst_1
iadd
istore 4
iload 2
istore 6
iload 3
istore 5
goto L1
L5:
iload 2
iconst_4
ishl
istore 2
iconst_1
istore 3
goto L3
L2:
iload 5
ifeq L6
aconst_null
areturn
L6:
aload 7
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 8
.limit stack 3
.end method

.method public getAddress()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/NSAPRecord/address [B
iconst_0
invokestatic org/xbill/DNS/NSAPRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/NSAPRecord
dup
invokespecial org/xbill/DNS/NSAPRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/NSAPRecord/checkAndConvertAddress(Ljava/lang/String;)[B
putfield org/xbill/DNS/NSAPRecord/address [B
aload 0
getfield org/xbill/DNS/NSAPRecord/address [B
ifnonnull L0
aload 1
new java/lang/StringBuffer
dup
ldc "invalid NSAP address "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
return
.limit locals 3
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/NSAPRecord/address [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
ldc "0x"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/NSAPRecord/address [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/NSAPRecord/address [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 4
.limit stack 2
.end method
