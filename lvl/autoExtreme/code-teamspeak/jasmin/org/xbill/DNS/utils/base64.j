.bytecode 50.0
.class public synchronized org/xbill/DNS/utils/base64
.super java/lang/Object

.field private static final 'Base64' Ljava/lang/String; = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static formatString([BILjava/lang/String;Z)Ljava/lang/String;
aload 0
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
astore 0
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 5
iconst_0
istore 4
L0:
iload 4
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 5
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 4
iload 1
iadd
aload 0
invokevirtual java/lang/String/length()I
if_icmplt L2
aload 5
aload 0
iload 4
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 3
ifeq L3
aload 5
ldc " )"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L3:
iload 4
iload 1
iadd
istore 4
goto L0
L2:
aload 5
aload 0
iload 4
iload 4
iload 1
iadd
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 5
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L3
L1:
aload 5
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 6
.limit stack 5
.end method

.method public static fromString(Ljava/lang/String;)[B
.catch java/io/IOException from L0 to L1 using L2
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 3
aload 0
invokevirtual java/lang/String/getBytes()[B
astore 0
iconst_0
istore 1
L3:
iload 1
aload 0
arraylength
if_icmpge L4
aload 0
iload 1
baload
i2c
invokestatic java/lang/Character/isWhitespace(C)Z
ifne L5
aload 3
aload 0
iload 1
baload
invokevirtual java/io/ByteArrayOutputStream/write(I)V
L5:
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
aload 3
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 0
aload 0
arraylength
iconst_4
irem
ifeq L6
aconst_null
areturn
L6:
aload 3
invokevirtual java/io/ByteArrayOutputStream/reset()V
new java/io/DataOutputStream
dup
aload 3
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
astore 4
iconst_0
istore 1
L7:
iload 1
aload 0
arraylength
iconst_3
iadd
iconst_4
idiv
if_icmpge L8
iconst_4
newarray short
astore 5
iconst_3
newarray short
astore 6
iconst_0
istore 2
L9:
iload 2
iconst_4
if_icmpge L10
aload 5
iload 2
ldc "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
aload 0
iload 1
iconst_4
imul
iload 2
iadd
baload
invokevirtual java/lang/String/indexOf(I)I
i2s
sastore
iload 2
iconst_1
iadd
istore 2
goto L9
L10:
aload 6
iconst_0
aload 5
iconst_0
saload
iconst_2
ishl
aload 5
iconst_1
saload
iconst_4
ishr
iadd
i2s
sastore
aload 5
iconst_2
saload
bipush 64
if_icmpne L11
aload 6
iconst_2
iconst_m1
sastore
aload 6
iconst_1
iconst_m1
sastore
aload 5
iconst_1
saload
bipush 15
iand
ifeq L12
aconst_null
areturn
L11:
aload 5
iconst_3
saload
bipush 64
if_icmpne L13
aload 6
iconst_1
aload 5
iconst_1
saload
iconst_4
ishl
aload 5
iconst_2
saload
iconst_2
ishr
iadd
sipush 255
iand
i2s
sastore
aload 6
iconst_2
iconst_m1
sastore
aload 5
iconst_2
saload
iconst_3
iand
ifeq L12
aconst_null
areturn
L13:
aload 6
iconst_1
aload 5
iconst_1
saload
iconst_4
ishl
aload 5
iconst_2
saload
iconst_2
ishr
iadd
sipush 255
iand
i2s
sastore
aload 6
iconst_2
aload 5
iconst_2
saload
bipush 6
ishl
aload 5
iconst_3
saload
iadd
sipush 255
iand
i2s
sastore
L12:
iconst_0
istore 2
L14:
iload 2
iconst_3
if_icmpge L15
aload 6
iload 2
saload
iflt L1
L0:
aload 4
aload 6
iload 2
saload
invokevirtual java/io/DataOutputStream/writeByte(I)V
L1:
iload 2
iconst_1
iadd
istore 2
goto L14
L2:
astore 5
L15:
iload 1
iconst_1
iadd
istore 1
goto L7
L8:
aload 3
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 7
.limit stack 6
.end method

.method public static toString([B)Ljava/lang/String;
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 3
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
iconst_2
iadd
iconst_3
idiv
if_icmpge L1
iconst_3
newarray short
astore 4
iconst_4
newarray short
astore 5
iconst_0
istore 2
L2:
iload 2
iconst_3
if_icmpge L3
iload 1
iconst_3
imul
iload 2
iadd
aload 0
arraylength
if_icmpge L4
aload 4
iload 2
aload 0
iload 1
iconst_3
imul
iload 2
iadd
baload
sipush 255
iand
i2s
sastore
L5:
iload 2
iconst_1
iadd
istore 2
goto L2
L4:
aload 4
iload 2
iconst_m1
sastore
goto L5
L3:
aload 5
iconst_0
aload 4
iconst_0
saload
iconst_2
ishr
i2s
sastore
aload 4
iconst_1
saload
iconst_m1
if_icmpne L6
aload 5
iconst_1
aload 4
iconst_0
saload
iconst_3
iand
iconst_4
ishl
i2s
sastore
L7:
aload 4
iconst_1
saload
iconst_m1
if_icmpne L8
aload 5
iconst_3
bipush 64
sastore
aload 5
iconst_2
bipush 64
sastore
L9:
iconst_0
istore 2
L10:
iload 2
iconst_4
if_icmpge L11
aload 3
ldc "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
aload 5
iload 2
saload
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 2
iconst_1
iadd
istore 2
goto L10
L6:
aload 5
iconst_1
aload 4
iconst_0
saload
iconst_3
iand
iconst_4
ishl
aload 4
iconst_1
saload
iconst_4
ishr
iadd
i2s
sastore
goto L7
L8:
aload 4
iconst_2
saload
iconst_m1
if_icmpne L12
aload 5
iconst_2
aload 4
iconst_1
saload
bipush 15
iand
iconst_2
ishl
i2s
sastore
aload 5
iconst_3
bipush 64
sastore
goto L9
L12:
aload 5
iconst_2
aload 4
iconst_1
saload
bipush 15
iand
iconst_2
ishl
aload 4
iconst_2
saload
bipush 6
ishr
iadd
i2s
sastore
aload 5
iconst_3
aload 4
iconst_2
saload
bipush 63
iand
i2s
sastore
goto L9
L11:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
new java/lang/String
dup
aload 3
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokespecial java/lang/String/<init>([B)V
areturn
.limit locals 6
.limit stack 5
.end method
