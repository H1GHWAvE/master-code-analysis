.bytecode 50.0
.class public synchronized org/xbill/DNS/MRRecord
.super org/xbill/DNS/SingleNameBase

.field private static final 'serialVersionUID' J = -5617939094209927533L


.method <init>()V
aload 0
invokespecial org/xbill/DNS/SingleNameBase/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;)V
aload 0
aload 1
bipush 9
iload 2
lload 3
aload 5
ldc "new name"
invokespecial org/xbill/DNS/SingleNameBase/<init>(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Name;Ljava/lang/String;)V
return
.limit locals 6
.limit stack 8
.end method

.method public getNewName()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/MRRecord/getSingleName()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/MRRecord
dup
invokespecial org/xbill/DNS/MRRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method
