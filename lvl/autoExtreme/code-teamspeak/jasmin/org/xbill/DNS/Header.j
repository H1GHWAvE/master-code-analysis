.bytecode 50.0
.class public synchronized org/xbill/DNS/Header
.super java/lang/Object
.implements java/lang/Cloneable

.field public static final 'LENGTH' I = 12


.field private static 'random' Ljava/util/Random;

.field private 'counts' [I

.field private 'flags' I

.field private 'id' I

.method static <clinit>()V
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
putstatic org/xbill/DNS/Header/random Ljava/util/Random;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokespecial org/xbill/DNS/Header/init()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokespecial org/xbill/DNS/Header/init()V
aload 0
iload 1
invokevirtual org/xbill/DNS/Header/setID(I)V
return
.limit locals 2
.limit stack 2
.end method

.method <init>(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
invokespecial org/xbill/DNS/Header/<init>(I)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/Header/flags I
iconst_0
istore 2
L0:
iload 2
aload 0
getfield org/xbill/DNS/Header/counts [I
arraylength
if_icmpge L1
aload 0
getfield org/xbill/DNS/Header/counts [I
iload 2
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
iastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public <init>([B)V
aload 0
new org/xbill/DNS/DNSInput
dup
aload 1
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
invokespecial org/xbill/DNS/Header/<init>(Lorg/xbill/DNS/DNSInput;)V
return
.limit locals 2
.limit stack 4
.end method

.method private static checkFlag(I)V
iload 0
invokestatic org/xbill/DNS/Header/validFlag(I)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "invalid flag bit "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 0
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 5
.end method

.method private init()V
aload 0
iconst_4
newarray int
putfield org/xbill/DNS/Header/counts [I
aload 0
iconst_0
putfield org/xbill/DNS/Header/flags I
aload 0
iconst_m1
putfield org/xbill/DNS/Header/id I
return
.limit locals 1
.limit stack 2
.end method

.method static setFlag(IIZ)I
iload 1
invokestatic org/xbill/DNS/Header/checkFlag(I)V
iload 2
ifeq L0
iconst_1
bipush 15
iload 1
isub
ishl
iload 0
ior
ireturn
L0:
iconst_1
bipush 15
iload 1
isub
ishl
iconst_m1
ixor
iload 0
iand
ireturn
.limit locals 3
.limit stack 3
.end method

.method private static validFlag(I)Z
iload 0
iflt L0
iload 0
bipush 15
if_icmpgt L0
iload 0
invokestatic org/xbill/DNS/Flags/isFlag(I)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public clone()Ljava/lang/Object;
new org/xbill/DNS/Header
dup
invokespecial org/xbill/DNS/Header/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/Header/id I
putfield org/xbill/DNS/Header/id I
aload 1
aload 0
getfield org/xbill/DNS/Header/flags I
putfield org/xbill/DNS/Header/flags I
aload 0
getfield org/xbill/DNS/Header/counts [I
iconst_0
aload 1
getfield org/xbill/DNS/Header/counts [I
iconst_0
aload 0
getfield org/xbill/DNS/Header/counts [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method decCount(I)V
aload 0
getfield org/xbill/DNS/Header/counts [I
iload 1
iaload
ifne L0
new java/lang/IllegalStateException
dup
ldc "DNS section count cannot be decremented"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/xbill/DNS/Header/counts [I
astore 2
aload 2
iload 1
aload 2
iload 1
iaload
iconst_1
isub
iastore
return
.limit locals 3
.limit stack 4
.end method

.method public getCount(I)I
aload 0
getfield org/xbill/DNS/Header/counts [I
iload 1
iaload
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getFlag(I)Z
iload 1
invokestatic org/xbill/DNS/Header/checkFlag(I)V
aload 0
getfield org/xbill/DNS/Header/flags I
iconst_1
bipush 15
iload 1
isub
ishl
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method getFlags()[Z
bipush 16
newarray boolean
astore 2
iconst_0
istore 1
L0:
iload 1
bipush 16
if_icmpge L1
iload 1
invokestatic org/xbill/DNS/Header/validFlag(I)Z
ifeq L2
aload 2
iload 1
aload 0
iload 1
invokevirtual org/xbill/DNS/Header/getFlag(I)Z
bastore
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method getFlagsByte()I
aload 0
getfield org/xbill/DNS/Header/flags I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getID()I
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield org/xbill/DNS/Header/id I
iflt L6
aload 0
getfield org/xbill/DNS/Header/id I
ireturn
L6:
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Header/id I
ifge L1
aload 0
getstatic org/xbill/DNS/Header/random Ljava/util/Random;
ldc_w 65535
invokevirtual java/util/Random/nextInt(I)I
putfield org/xbill/DNS/Header/id I
L1:
aload 0
getfield org/xbill/DNS/Header/id I
istore 1
aload 0
monitorexit
L3:
iload 1
ireturn
L2:
astore 2
L4:
aload 0
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public getOpcode()I
aload 0
getfield org/xbill/DNS/Header/flags I
bipush 11
ishr
bipush 15
iand
ireturn
.limit locals 1
.limit stack 2
.end method

.method public getRcode()I
aload 0
getfield org/xbill/DNS/Header/flags I
bipush 15
iand
ireturn
.limit locals 1
.limit stack 2
.end method

.method incCount(I)V
aload 0
getfield org/xbill/DNS/Header/counts [I
iload 1
iaload
ldc_w 65535
if_icmpne L0
new java/lang/IllegalStateException
dup
ldc "DNS section count cannot be incremented"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/xbill/DNS/Header/counts [I
astore 2
aload 2
iload 1
aload 2
iload 1
iaload
iconst_1
iadd
iastore
return
.limit locals 3
.limit stack 4
.end method

.method public printFlags()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
iconst_0
istore 1
L0:
iload 1
bipush 16
if_icmpge L1
iload 1
invokestatic org/xbill/DNS/Header/validFlag(I)Z
ifeq L2
aload 0
iload 1
invokevirtual org/xbill/DNS/Header/getFlag(I)Z
ifeq L2
aload 2
iload 1
invokestatic org/xbill/DNS/Flags/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 2
.end method

.method setCount(II)V
iload 2
iflt L0
iload 2
ldc_w 65535
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "DNS section count "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " is out of range"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield org/xbill/DNS/Header/counts [I
iload 1
iload 2
iastore
return
.limit locals 3
.limit stack 5
.end method

.method public setFlag(I)V
iload 1
invokestatic org/xbill/DNS/Header/checkFlag(I)V
aload 0
aload 0
getfield org/xbill/DNS/Header/flags I
iload 1
iconst_1
invokestatic org/xbill/DNS/Header/setFlag(IIZ)I
putfield org/xbill/DNS/Header/flags I
return
.limit locals 2
.limit stack 4
.end method

.method public setID(I)V
iload 1
iflt L0
iload 1
ldc_w 65535
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "DNS message ID "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " is out of range"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/xbill/DNS/Header/id I
return
.limit locals 2
.limit stack 5
.end method

.method public setOpcode(I)V
iload 1
iflt L0
iload 1
bipush 15
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "DNS Opcode "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "is out of range"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 0
getfield org/xbill/DNS/Header/flags I
ldc_w 34815
iand
putfield org/xbill/DNS/Header/flags I
aload 0
aload 0
getfield org/xbill/DNS/Header/flags I
iload 1
bipush 11
ishl
ior
putfield org/xbill/DNS/Header/flags I
return
.limit locals 2
.limit stack 5
.end method

.method public setRcode(I)V
iload 1
iflt L0
iload 1
bipush 15
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "DNS Rcode "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " is out of range"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 0
getfield org/xbill/DNS/Header/flags I
bipush -16
iand
putfield org/xbill/DNS/Header/flags I
aload 0
aload 0
getfield org/xbill/DNS/Header/flags I
iload 1
ior
putfield org/xbill/DNS/Header/flags I
return
.limit locals 2
.limit stack 5
.end method

.method public toString()Ljava/lang/String;
aload 0
aload 0
invokevirtual org/xbill/DNS/Header/getRcode()I
invokevirtual org/xbill/DNS/Header/toStringWithRcode(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method toStringWithRcode(I)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
aload 2
ldc ";; ->>HEADER<<- "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
new java/lang/StringBuffer
dup
ldc "opcode: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual org/xbill/DNS/Header/getOpcode()I
invokestatic org/xbill/DNS/Opcode/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
new java/lang/StringBuffer
dup
ldc ", status: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokestatic org/xbill/DNS/Rcode/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
new java/lang/StringBuffer
dup
ldc ", id: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual org/xbill/DNS/Header/getID()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
new java/lang/StringBuffer
dup
ldc ";; flags: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual org/xbill/DNS/Header/printFlags()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
ldc "; "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iconst_0
istore 1
L0:
iload 1
iconst_4
if_icmpge L1
aload 2
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
iload 1
invokestatic org/xbill/DNS/Section/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ": "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
iload 1
invokevirtual org/xbill/DNS/Header/getCount(I)I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method toWire(Lorg/xbill/DNS/DNSOutput;)V
aload 1
aload 0
invokevirtual org/xbill/DNS/Header/getID()I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/Header/flags I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
iconst_0
istore 2
L0:
iload 2
aload 0
getfield org/xbill/DNS/Header/counts [I
arraylength
if_icmpge L1
aload 1
aload 0
getfield org/xbill/DNS/Header/counts [I
iload 2
iaload
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public toWire()[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 1
aload 0
aload 1
invokevirtual org/xbill/DNS/Header/toWire(Lorg/xbill/DNS/DNSOutput;)V
aload 1
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 2
.limit stack 2
.end method

.method public unsetFlag(I)V
iload 1
invokestatic org/xbill/DNS/Header/checkFlag(I)V
aload 0
aload 0
getfield org/xbill/DNS/Header/flags I
iload 1
iconst_0
invokestatic org/xbill/DNS/Header/setFlag(IIZ)I
putfield org/xbill/DNS/Header/flags I
return
.limit locals 2
.limit stack 4
.end method
