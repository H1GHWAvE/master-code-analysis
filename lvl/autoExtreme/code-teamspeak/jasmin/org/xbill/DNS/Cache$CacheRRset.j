.bytecode 50.0
.class synchronized org/xbill/DNS/Cache$CacheRRset
.super org/xbill/DNS/RRset
.implements org/xbill/DNS/Cache$Element

.field private static final 'serialVersionUID' J = 5971755205903597024L


.field 'credibility' I

.field 'expire' I

.method public <init>(Lorg/xbill/DNS/RRset;IJ)V
aload 0
aload 1
invokespecial org/xbill/DNS/RRset/<init>(Lorg/xbill/DNS/RRset;)V
aload 0
iload 2
putfield org/xbill/DNS/Cache$CacheRRset/credibility I
aload 0
aload 1
invokevirtual org/xbill/DNS/RRset/getTTL()J
lload 3
invokestatic org/xbill/DNS/Cache/access$000(JJ)I
putfield org/xbill/DNS/Cache$CacheRRset/expire I
return
.limit locals 5
.limit stack 5
.end method

.method public <init>(Lorg/xbill/DNS/Record;IJ)V
aload 0
invokespecial org/xbill/DNS/RRset/<init>()V
aload 0
iload 2
putfield org/xbill/DNS/Cache$CacheRRset/credibility I
aload 0
aload 1
invokevirtual org/xbill/DNS/Record/getTTL()J
lload 3
invokestatic org/xbill/DNS/Cache/access$000(JJ)I
putfield org/xbill/DNS/Cache$CacheRRset/expire I
aload 0
aload 1
invokevirtual org/xbill/DNS/Cache$CacheRRset/addRR(Lorg/xbill/DNS/Record;)V
return
.limit locals 5
.limit stack 5
.end method

.method public final compareCredibility(I)I
aload 0
getfield org/xbill/DNS/Cache$CacheRRset/credibility I
iload 1
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final expired()Z
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 1000L
ldiv
l2i
aload 0
getfield org/xbill/DNS/Cache$CacheRRset/expire I
if_icmplt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 4
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
invokespecial org/xbill/DNS/RRset/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " cl = "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/Cache$CacheRRset/credibility I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method
