.bytecode 50.0
.class public synchronized org/xbill/DNS/RTRecord
.super org/xbill/DNS/U16NameBase

.field private static final 'serialVersionUID' J = -3206215651648278098L


.method <init>()V
aload 0
invokespecial org/xbill/DNS/U16NameBase/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJILorg/xbill/DNS/Name;)V
aload 0
aload 1
bipush 21
iload 2
lload 3
iload 5
ldc "preference"
aload 6
ldc "intermediateHost"
invokespecial org/xbill/DNS/U16NameBase/<init>(Lorg/xbill/DNS/Name;IIJILjava/lang/String;Lorg/xbill/DNS/Name;Ljava/lang/String;)V
return
.limit locals 7
.limit stack 10
.end method

.method public getIntermediateHost()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/RTRecord/getNameField()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/RTRecord
dup
invokespecial org/xbill/DNS/RTRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getPreference()I
aload 0
invokevirtual org/xbill/DNS/RTRecord/getU16Field()I
ireturn
.limit locals 1
.limit stack 1
.end method
