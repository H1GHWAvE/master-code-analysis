.bytecode 50.0
.class public final synchronized org/xbill/DNS/ExtendedFlags
.super java/lang/Object

.field public static final 'DO' I = 32768


.field private static 'extflags' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "EDNS Flag"
iconst_3
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/ExtendedFlags/extflags Lorg/xbill/DNS/Mnemonic;
aload 0
ldc_w 65535
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/ExtendedFlags/extflags Lorg/xbill/DNS/Mnemonic;
ldc "FLAG"
invokevirtual org/xbill/DNS/Mnemonic/setPrefix(Ljava/lang/String;)V
getstatic org/xbill/DNS/ExtendedFlags/extflags Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/ExtendedFlags/extflags Lorg/xbill/DNS/Mnemonic;
ldc_w 32768
ldc "do"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/ExtendedFlags/extflags Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/ExtendedFlags/extflags Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
