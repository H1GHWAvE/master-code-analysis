.bytecode 50.0
.class public synchronized org/xbill/DNS/KEYRecord$Protocol
.super java/lang/Object

.field public static final 'ANY' I = 255


.field public static final 'DNSSEC' I = 3


.field public static final 'EMAIL' I = 2


.field public static final 'IPSEC' I = 4


.field public static final 'NONE' I = 0


.field public static final 'TLS' I = 1


.field private static 'protocols' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "KEY protocol"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
aload 0
sipush 255
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_0
ldc "NONE"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "TLS"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "EMAIL"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "DNSSEC"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_4
ldc "IPSEC"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
sipush 255
ldc "ANY"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/KEYRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
