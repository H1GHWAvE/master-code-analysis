.bytecode 50.0
.class public synchronized org/xbill/DNS/SimpleResolver
.super java/lang/Object
.implements org/xbill/DNS/Resolver

.field public static final 'DEFAULT_EDNS_PAYLOADSIZE' I = 1280


.field public static final 'DEFAULT_PORT' I = 53


.field private static final 'DEFAULT_UDPSIZE' S = 512


.field private static 'defaultResolver' Ljava/lang/String;

.field private static 'uniqueID' I

.field private 'address' Ljava/net/InetSocketAddress;

.field private 'ignoreTruncation' Z

.field private 'localAddress' Ljava/net/InetSocketAddress;

.field private 'queryOPT' Lorg/xbill/DNS/OPTRecord;

.field private 'timeoutValue' J

.field private 'tsig' Lorg/xbill/DNS/TSIG;

.field private 'useTCP' Z

.method static <clinit>()V
ldc "localhost"
putstatic org/xbill/DNS/SimpleResolver/defaultResolver Ljava/lang/String;
iconst_0
putstatic org/xbill/DNS/SimpleResolver/uniqueID I
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
aconst_null
invokespecial org/xbill/DNS/SimpleResolver/<init>(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w 10000L
putfield org/xbill/DNS/SimpleResolver/timeoutValue J
aload 1
astore 2
aload 1
ifnonnull L0
invokestatic org/xbill/DNS/ResolverConfig/getCurrentConfig()Lorg/xbill/DNS/ResolverConfig;
invokevirtual org/xbill/DNS/ResolverConfig/server()Ljava/lang/String;
astore 1
aload 1
astore 2
aload 1
ifnonnull L0
getstatic org/xbill/DNS/SimpleResolver/defaultResolver Ljava/lang/String;
astore 2
L0:
aload 2
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
invokestatic java/net/InetAddress/getLocalHost()Ljava/net/InetAddress;
astore 1
L2:
aload 0
new java/net/InetSocketAddress
dup
aload 1
bipush 53
invokespecial java/net/InetSocketAddress/<init>(Ljava/net/InetAddress;I)V
putfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
return
L1:
aload 2
invokestatic java/net/InetAddress/getByName(Ljava/lang/String;)Ljava/net/InetAddress;
astore 1
goto L2
.limit locals 3
.limit stack 5
.end method

.method private applyEDNS(Lorg/xbill/DNS/Message;)V
aload 0
getfield org/xbill/DNS/SimpleResolver/queryOPT Lorg/xbill/DNS/OPTRecord;
ifnull L0
aload 1
invokevirtual org/xbill/DNS/Message/getOPT()Lorg/xbill/DNS/OPTRecord;
ifnull L1
L0:
return
L1:
aload 1
aload 0
getfield org/xbill/DNS/SimpleResolver/queryOPT Lorg/xbill/DNS/OPTRecord;
iconst_3
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
return
.limit locals 2
.limit stack 3
.end method

.method private maxUDPSize(Lorg/xbill/DNS/Message;)I
aload 1
invokevirtual org/xbill/DNS/Message/getOPT()Lorg/xbill/DNS/OPTRecord;
astore 1
aload 1
ifnonnull L0
sipush 512
ireturn
L0:
aload 1
invokevirtual org/xbill/DNS/OPTRecord/getPayloadSize()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method private parseMessage([B)Lorg/xbill/DNS/Message;
.catch java/io/IOException from L0 to L1 using L2
L0:
new org/xbill/DNS/Message
dup
aload 1
invokespecial org/xbill/DNS/Message/<init>([B)V
astore 1
L1:
aload 1
areturn
L2:
astore 2
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L3
aload 2
invokevirtual java/io/IOException/printStackTrace()V
L3:
aload 2
astore 1
aload 2
instanceof org/xbill/DNS/WireParseException
ifne L4
new org/xbill/DNS/WireParseException
dup
ldc "Error parsing message"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
astore 1
L4:
aload 1
checkcast org/xbill/DNS/WireParseException
athrow
.limit locals 3
.limit stack 3
.end method

.method private sendAXFR(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message;
.catch org/xbill/DNS/ZoneTransferException from L0 to L1 using L2
aload 1
invokevirtual org/xbill/DNS/Message/getQuestion()Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
aload 0
getfield org/xbill/DNS/SimpleResolver/tsig Lorg/xbill/DNS/TSIG;
invokestatic org/xbill/DNS/ZoneTransferIn/newAXFR(Lorg/xbill/DNS/Name;Ljava/net/SocketAddress;Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/ZoneTransferIn;
astore 2
aload 2
aload 0
invokevirtual org/xbill/DNS/SimpleResolver/getTimeout()J
ldc2_w 1000L
ldiv
l2i
invokevirtual org/xbill/DNS/ZoneTransferIn/setTimeout(I)V
aload 2
aload 0
getfield org/xbill/DNS/SimpleResolver/localAddress Ljava/net/InetSocketAddress;
invokevirtual org/xbill/DNS/ZoneTransferIn/setLocalAddress(Ljava/net/SocketAddress;)V
L0:
aload 2
invokevirtual org/xbill/DNS/ZoneTransferIn/run()Ljava/util/List;
pop
L1:
aload 2
invokevirtual org/xbill/DNS/ZoneTransferIn/getAXFR()Ljava/util/List;
astore 3
new org/xbill/DNS/Message
dup
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getID()I
invokespecial org/xbill/DNS/Message/<init>(I)V
astore 2
aload 2
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_5
invokevirtual org/xbill/DNS/Header/setFlag(I)V
aload 2
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_0
invokevirtual org/xbill/DNS/Header/setFlag(I)V
aload 2
aload 1
invokevirtual org/xbill/DNS/Message/getQuestion()Lorg/xbill/DNS/Record;
iconst_0
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
aload 3
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L3:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/Record
iconst_1
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
goto L3
L2:
astore 1
new org/xbill/DNS/WireParseException
dup
aload 1
invokevirtual org/xbill/DNS/ZoneTransferException/getMessage()Ljava/lang/String;
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 2
areturn
.limit locals 4
.limit stack 5
.end method

.method public static setDefaultResolver(Ljava/lang/String;)V
aload 0
putstatic org/xbill/DNS/SimpleResolver/defaultResolver Ljava/lang/String;
return
.limit locals 1
.limit stack 1
.end method

.method private verifyTSIG(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Message;[BLorg/xbill/DNS/TSIG;)V
aload 4
ifnonnull L0
L1:
return
L0:
aload 4
aload 2
aload 3
aload 1
invokevirtual org/xbill/DNS/Message/getTSIG()Lorg/xbill/DNS/TSIGRecord;
invokevirtual org/xbill/DNS/TSIG/verify(Lorg/xbill/DNS/Message;[BLorg/xbill/DNS/TSIGRecord;)I
istore 5
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L1
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "TSIG verify: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 5
invokestatic org/xbill/DNS/Rcode/TSIGstring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 6
.limit stack 4
.end method

.method public getAddress()Ljava/net/InetSocketAddress;
aload 0
getfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
areturn
.limit locals 1
.limit stack 1
.end method

.method getTSIGKey()Lorg/xbill/DNS/TSIG;
aload 0
getfield org/xbill/DNS/SimpleResolver/tsig Lorg/xbill/DNS/TSIG;
areturn
.limit locals 1
.limit stack 1
.end method

.method getTimeout()J
aload 0
getfield org/xbill/DNS/SimpleResolver/timeoutValue J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message;
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "Sending to "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
invokevirtual java/net/InetSocketAddress/getAddress()Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ":"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
getfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
invokevirtual java/net/InetSocketAddress/getPort()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getOpcode()I
ifne L1
aload 1
invokevirtual org/xbill/DNS/Message/getQuestion()Lorg/xbill/DNS/Record;
astore 8
aload 8
ifnull L1
aload 8
invokevirtual org/xbill/DNS/Record/getType()I
sipush 252
if_icmpne L1
aload 0
aload 1
invokespecial org/xbill/DNS/SimpleResolver/sendAXFR(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message;
areturn
L1:
aload 1
invokevirtual org/xbill/DNS/Message/clone()Ljava/lang/Object;
checkcast org/xbill/DNS/Message
astore 8
aload 0
aload 8
invokespecial org/xbill/DNS/SimpleResolver/applyEDNS(Lorg/xbill/DNS/Message;)V
aload 0
getfield org/xbill/DNS/SimpleResolver/tsig Lorg/xbill/DNS/TSIG;
ifnull L2
aload 0
getfield org/xbill/DNS/SimpleResolver/tsig Lorg/xbill/DNS/TSIG;
aload 8
aconst_null
invokevirtual org/xbill/DNS/TSIG/apply(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/TSIGRecord;)V
L2:
aload 8
ldc_w 65535
invokevirtual org/xbill/DNS/Message/toWire(I)[B
astore 9
aload 0
aload 8
invokespecial org/xbill/DNS/SimpleResolver/maxUDPSize(Lorg/xbill/DNS/Message;)I
istore 3
invokestatic java/lang/System/currentTimeMillis()J
lstore 6
aload 0
getfield org/xbill/DNS/SimpleResolver/timeoutValue J
lload 6
ladd
lstore 6
iconst_0
istore 2
L3:
aload 0
getfield org/xbill/DNS/SimpleResolver/useTCP Z
ifne L4
aload 9
arraylength
iload 3
if_icmple L5
L4:
iconst_1
istore 2
L6:
iload 2
ifeq L7
aload 0
getfield org/xbill/DNS/SimpleResolver/localAddress Ljava/net/InetSocketAddress;
aload 0
getfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
aload 9
lload 6
invokestatic org/xbill/DNS/TCPClient/sendrecv(Ljava/net/SocketAddress;Ljava/net/SocketAddress;[BJ)[B
astore 1
L8:
aload 1
arraylength
bipush 12
if_icmpge L9
new org/xbill/DNS/WireParseException
dup
ldc "invalid DNS header - too short"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 0
getfield org/xbill/DNS/SimpleResolver/localAddress Ljava/net/InetSocketAddress;
aload 0
getfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
aload 9
iload 3
lload 6
invokestatic org/xbill/DNS/UDPClient/sendrecv(Ljava/net/SocketAddress;Ljava/net/SocketAddress;[BIJ)[B
astore 1
goto L8
L9:
aload 1
iconst_0
baload
sipush 255
iand
bipush 8
ishl
aload 1
iconst_1
baload
sipush 255
iand
iadd
istore 4
aload 8
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getID()I
istore 5
iload 4
iload 5
if_icmpeq L10
new java/lang/StringBuffer
dup
ldc "invalid message id: expected "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 5
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "; got id "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 4
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
astore 1
iload 2
ifeq L11
new org/xbill/DNS/WireParseException
dup
aload 1
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L11:
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L12
getstatic java/lang/System/err Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L3
L10:
aload 0
aload 1
invokespecial org/xbill/DNS/SimpleResolver/parseMessage([B)Lorg/xbill/DNS/Message;
astore 10
aload 0
aload 8
aload 10
aload 1
aload 0
getfield org/xbill/DNS/SimpleResolver/tsig Lorg/xbill/DNS/TSIG;
invokespecial org/xbill/DNS/SimpleResolver/verifyTSIG(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/Message;[BLorg/xbill/DNS/TSIG;)V
iload 2
ifne L13
aload 0
getfield org/xbill/DNS/SimpleResolver/ignoreTruncation Z
ifne L13
aload 10
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
bipush 6
invokevirtual org/xbill/DNS/Header/getFlag(I)Z
ifeq L13
iconst_1
istore 2
goto L3
L13:
aload 10
areturn
L12:
goto L3
L5:
goto L6
.limit locals 11
.limit stack 6
.end method

.method public sendAsync(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/ResolverListener;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
getstatic org/xbill/DNS/SimpleResolver/uniqueID I
istore 3
iload 3
iconst_1
iadd
putstatic org/xbill/DNS/SimpleResolver/uniqueID I
new java/lang/Integer
dup
iload 3
invokespecial java/lang/Integer/<init>(I)V
astore 5
aload 0
monitorexit
L1:
aload 1
invokevirtual org/xbill/DNS/Message/getQuestion()Lorg/xbill/DNS/Record;
astore 4
aload 4
ifnull L5
aload 4
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/toString()Ljava/lang/String;
astore 4
L6:
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc ": "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 4
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
astore 4
new org/xbill/DNS/ResolveThread
dup
aload 0
aload 1
aload 5
aload 2
invokespecial org/xbill/DNS/ResolveThread/<init>(Lorg/xbill/DNS/Resolver;Lorg/xbill/DNS/Message;Ljava/lang/Object;Lorg/xbill/DNS/ResolverListener;)V
astore 1
aload 1
aload 4
invokevirtual java/lang/Thread/setName(Ljava/lang/String;)V
aload 1
iconst_1
invokevirtual java/lang/Thread/setDaemon(Z)V
aload 1
invokevirtual java/lang/Thread/start()V
aload 5
areturn
L2:
astore 1
L3:
aload 0
monitorexit
L4:
aload 1
athrow
L5:
ldc "(none)"
astore 4
goto L6
.limit locals 6
.limit stack 6
.end method

.method public setAddress(Ljava/net/InetAddress;)V
aload 0
new java/net/InetSocketAddress
dup
aload 1
aload 0
getfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
invokevirtual java/net/InetSocketAddress/getPort()I
invokespecial java/net/InetSocketAddress/<init>(Ljava/net/InetAddress;I)V
putfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
return
.limit locals 2
.limit stack 5
.end method

.method public setAddress(Ljava/net/InetSocketAddress;)V
aload 0
aload 1
putfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
return
.limit locals 2
.limit stack 2
.end method

.method public setEDNS(I)V
aload 0
iload 1
iconst_0
iconst_0
aconst_null
invokevirtual org/xbill/DNS/SimpleResolver/setEDNS(IIILjava/util/List;)V
return
.limit locals 2
.limit stack 5
.end method

.method public setEDNS(IIILjava/util/List;)V
iload 1
ifeq L0
iload 1
iconst_m1
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "invalid EDNS level - must be 0 or -1"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 2
ifne L1
sipush 1280
istore 2
L2:
aload 0
new org/xbill/DNS/OPTRecord
dup
iload 2
iconst_0
iload 1
iload 3
aload 4
invokespecial org/xbill/DNS/OPTRecord/<init>(IIIILjava/util/List;)V
putfield org/xbill/DNS/SimpleResolver/queryOPT Lorg/xbill/DNS/OPTRecord;
return
L1:
goto L2
.limit locals 5
.limit stack 8
.end method

.method public setIgnoreTruncation(Z)V
aload 0
iload 1
putfield org/xbill/DNS/SimpleResolver/ignoreTruncation Z
return
.limit locals 2
.limit stack 2
.end method

.method public setLocalAddress(Ljava/net/InetAddress;)V
aload 0
new java/net/InetSocketAddress
dup
aload 1
iconst_0
invokespecial java/net/InetSocketAddress/<init>(Ljava/net/InetAddress;I)V
putfield org/xbill/DNS/SimpleResolver/localAddress Ljava/net/InetSocketAddress;
return
.limit locals 2
.limit stack 5
.end method

.method public setLocalAddress(Ljava/net/InetSocketAddress;)V
aload 0
aload 1
putfield org/xbill/DNS/SimpleResolver/localAddress Ljava/net/InetSocketAddress;
return
.limit locals 2
.limit stack 2
.end method

.method public setPort(I)V
aload 0
new java/net/InetSocketAddress
dup
aload 0
getfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
invokevirtual java/net/InetSocketAddress/getAddress()Ljava/net/InetAddress;
iload 1
invokespecial java/net/InetSocketAddress/<init>(Ljava/net/InetAddress;I)V
putfield org/xbill/DNS/SimpleResolver/address Ljava/net/InetSocketAddress;
return
.limit locals 2
.limit stack 5
.end method

.method public setTCP(Z)V
aload 0
iload 1
putfield org/xbill/DNS/SimpleResolver/useTCP Z
return
.limit locals 2
.limit stack 2
.end method

.method public setTSIGKey(Lorg/xbill/DNS/TSIG;)V
aload 0
aload 1
putfield org/xbill/DNS/SimpleResolver/tsig Lorg/xbill/DNS/TSIG;
return
.limit locals 2
.limit stack 2
.end method

.method public setTimeout(I)V
aload 0
iload 1
iconst_0
invokevirtual org/xbill/DNS/SimpleResolver/setTimeout(II)V
return
.limit locals 2
.limit stack 3
.end method

.method public setTimeout(II)V
aload 0
iload 1
i2l
ldc2_w 1000L
lmul
iload 2
i2l
ladd
putfield org/xbill/DNS/SimpleResolver/timeoutValue J
return
.limit locals 3
.limit stack 5
.end method
