.bytecode 50.0
.class public synchronized org/xbill/DNS/DSRecord
.super org/xbill/DNS/Record

.field public static final 'GOST3411_DIGEST_ID' I = 3


.field public static final 'SHA1_DIGEST_ID' I = 1


.field public static final 'SHA256_DIGEST_ID' I = 2


.field public static final 'SHA384_DIGEST_ID' I = 4


.field private static final 'serialVersionUID' J = -9001819329700081493L


.field private 'alg' I

.field private 'digest' [B

.field private 'digestid' I

.field private 'footprint' I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIII[B)V
aload 0
aload 1
bipush 43
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "footprint"
iload 5
invokestatic org/xbill/DNS/DSRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/DSRecord/footprint I
aload 0
ldc "alg"
iload 6
invokestatic org/xbill/DNS/DSRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/DSRecord/alg I
aload 0
ldc "digestid"
iload 7
invokestatic org/xbill/DNS/DSRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/DSRecord/digestid I
aload 0
aload 8
putfield org/xbill/DNS/DSRecord/digest [B
return
.limit locals 9
.limit stack 6
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJILorg/xbill/DNS/DNSKEYRecord;)V
aload 0
aload 1
iload 2
lload 3
aload 6
invokevirtual org/xbill/DNS/DNSKEYRecord/getFootprint()I
aload 6
invokevirtual org/xbill/DNS/DNSKEYRecord/getAlgorithm()I
iload 5
aload 6
iload 5
invokestatic org/xbill/DNS/DNSSEC/generateDSDigest(Lorg/xbill/DNS/DNSKEYRecord;I)[B
invokespecial org/xbill/DNS/DSRecord/<init>(Lorg/xbill/DNS/Name;IJIII[B)V
return
.limit locals 7
.limit stack 10
.end method

.method public getAlgorithm()I
aload 0
getfield org/xbill/DNS/DSRecord/alg I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDigest()[B
aload 0
getfield org/xbill/DNS/DSRecord/digest [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDigestID()I
aload 0
getfield org/xbill/DNS/DSRecord/digestid I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFootprint()I
aload 0
getfield org/xbill/DNS/DSRecord/footprint I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/DSRecord
dup
invokespecial org/xbill/DNS/DSRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/DSRecord/footprint I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/DSRecord/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/DSRecord/digestid I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getHex()[B
putfield org/xbill/DNS/DSRecord/digest [B
return
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/DSRecord/footprint I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/DSRecord/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/DSRecord/digestid I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/DSRecord/digest [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/DSRecord/footprint I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/DSRecord/alg I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/DSRecord/digestid I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/DSRecord/digest [B
ifnull L0
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/DSRecord/digest [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/DSRecord/footprint I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/DSRecord/alg I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/DSRecord/digestid I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
getfield org/xbill/DNS/DSRecord/digest [B
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/DSRecord/digest [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L0:
return
.limit locals 4
.limit stack 2
.end method
