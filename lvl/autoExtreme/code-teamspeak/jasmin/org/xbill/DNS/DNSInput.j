.bytecode 50.0
.class public synchronized org/xbill/DNS/DNSInput
.super java/lang/Object

.field private 'array' [B

.field private 'end' I

.field private 'pos' I

.field private 'saved_end' I

.field private 'saved_pos' I

.method public <init>([B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield org/xbill/DNS/DNSInput/array [B
aload 0
iconst_0
putfield org/xbill/DNS/DNSInput/pos I
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/array [B
arraylength
putfield org/xbill/DNS/DNSInput/end I
aload 0
iconst_m1
putfield org/xbill/DNS/DNSInput/saved_pos I
aload 0
iconst_m1
putfield org/xbill/DNS/DNSInput/saved_end I
return
.limit locals 2
.limit stack 2
.end method

.method private require(I)V
iload 1
aload 0
invokevirtual org/xbill/DNS/DNSInput/remaining()I
if_icmple L0
new org/xbill/DNS/WireParseException
dup
ldc "end of input"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public clearActive()V
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/array [B
arraylength
putfield org/xbill/DNS/DNSInput/end I
return
.limit locals 1
.limit stack 2
.end method

.method public current()I
aload 0
getfield org/xbill/DNS/DNSInput/pos I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public jump(I)V
iload 1
aload 0
getfield org/xbill/DNS/DNSInput/array [B
arraylength
if_icmplt L0
new java/lang/IllegalArgumentException
dup
ldc "cannot jump past end of input"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield org/xbill/DNS/DNSInput/pos I
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/array [B
arraylength
putfield org/xbill/DNS/DNSInput/end I
return
.limit locals 2
.limit stack 3
.end method

.method public readByteArray([BII)V
aload 0
iload 3
invokespecial org/xbill/DNS/DNSInput/require(I)V
aload 0
getfield org/xbill/DNS/DNSInput/array [B
aload 0
getfield org/xbill/DNS/DNSInput/pos I
aload 1
iload 2
iload 3
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/pos I
iload 3
iadd
putfield org/xbill/DNS/DNSInput/pos I
return
.limit locals 4
.limit stack 5
.end method

.method public readByteArray()[B
aload 0
invokevirtual org/xbill/DNS/DNSInput/remaining()I
istore 1
iload 1
newarray byte
astore 2
aload 0
getfield org/xbill/DNS/DNSInput/array [B
aload 0
getfield org/xbill/DNS/DNSInput/pos I
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
iload 1
aload 0
getfield org/xbill/DNS/DNSInput/pos I
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public readByteArray(I)[B
aload 0
iload 1
invokespecial org/xbill/DNS/DNSInput/require(I)V
iload 1
newarray byte
astore 2
aload 0
getfield org/xbill/DNS/DNSInput/array [B
aload 0
getfield org/xbill/DNS/DNSInput/pos I
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/pos I
iload 1
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public readCountedString()[B
aload 0
iconst_1
invokespecial org/xbill/DNS/DNSInput/require(I)V
aload 0
getfield org/xbill/DNS/DNSInput/array [B
astore 2
aload 0
getfield org/xbill/DNS/DNSInput/pos I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 0
aload 2
iload 1
baload
sipush 255
iand
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
areturn
.limit locals 3
.limit stack 3
.end method

.method public readU16()I
aload 0
iconst_2
invokespecial org/xbill/DNS/DNSInput/require(I)V
aload 0
getfield org/xbill/DNS/DNSInput/array [B
astore 3
aload 0
getfield org/xbill/DNS/DNSInput/pos I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 3
iload 1
baload
istore 1
aload 0
getfield org/xbill/DNS/DNSInput/array [B
astore 3
aload 0
getfield org/xbill/DNS/DNSInput/pos I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield org/xbill/DNS/DNSInput/pos I
iload 1
sipush 255
iand
bipush 8
ishl
aload 3
iload 2
baload
sipush 255
iand
iadd
ireturn
.limit locals 4
.limit stack 3
.end method

.method public readU32()J
aload 0
iconst_4
invokespecial org/xbill/DNS/DNSInput/require(I)V
aload 0
getfield org/xbill/DNS/DNSInput/array [B
astore 7
aload 0
getfield org/xbill/DNS/DNSInput/pos I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 7
iload 1
baload
istore 1
aload 0
getfield org/xbill/DNS/DNSInput/array [B
astore 7
aload 0
getfield org/xbill/DNS/DNSInput/pos I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 7
iload 2
baload
istore 2
aload 0
getfield org/xbill/DNS/DNSInput/array [B
astore 7
aload 0
getfield org/xbill/DNS/DNSInput/pos I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 7
iload 3
baload
istore 3
aload 0
getfield org/xbill/DNS/DNSInput/array [B
astore 7
aload 0
getfield org/xbill/DNS/DNSInput/pos I
istore 4
aload 0
iload 4
iconst_1
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 7
iload 4
baload
istore 4
iload 1
sipush 255
iand
i2l
lstore 5
iload 2
sipush 255
iand
bipush 16
ishl
i2l
lload 5
bipush 24
lshl
ladd
iload 3
sipush 255
iand
bipush 8
ishl
i2l
ladd
iload 4
sipush 255
iand
i2l
ladd
lreturn
.limit locals 8
.limit stack 5
.end method

.method public readU8()I
aload 0
iconst_1
invokespecial org/xbill/DNS/DNSInput/require(I)V
aload 0
getfield org/xbill/DNS/DNSInput/array [B
astore 2
aload 0
getfield org/xbill/DNS/DNSInput/pos I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield org/xbill/DNS/DNSInput/pos I
aload 2
iload 1
baload
sipush 255
iand
ireturn
.limit locals 3
.limit stack 3
.end method

.method public remaining()I
aload 0
getfield org/xbill/DNS/DNSInput/end I
aload 0
getfield org/xbill/DNS/DNSInput/pos I
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public restore()V
aload 0
getfield org/xbill/DNS/DNSInput/saved_pos I
ifge L0
new java/lang/IllegalStateException
dup
ldc "no previous state"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/saved_pos I
putfield org/xbill/DNS/DNSInput/pos I
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/saved_end I
putfield org/xbill/DNS/DNSInput/end I
aload 0
iconst_m1
putfield org/xbill/DNS/DNSInput/saved_pos I
aload 0
iconst_m1
putfield org/xbill/DNS/DNSInput/saved_end I
return
.limit locals 1
.limit stack 3
.end method

.method public restoreActive(I)V
iload 1
aload 0
getfield org/xbill/DNS/DNSInput/array [B
arraylength
if_icmple L0
new java/lang/IllegalArgumentException
dup
ldc "cannot set active region past end of input"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield org/xbill/DNS/DNSInput/end I
return
.limit locals 2
.limit stack 3
.end method

.method public save()V
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/pos I
putfield org/xbill/DNS/DNSInput/saved_pos I
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/end I
putfield org/xbill/DNS/DNSInput/saved_end I
return
.limit locals 1
.limit stack 2
.end method

.method public saveActive()I
aload 0
getfield org/xbill/DNS/DNSInput/end I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setActive(I)V
iload 1
aload 0
getfield org/xbill/DNS/DNSInput/array [B
arraylength
aload 0
getfield org/xbill/DNS/DNSInput/pos I
isub
if_icmple L0
new java/lang/IllegalArgumentException
dup
ldc "cannot set active region past end of input"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 0
getfield org/xbill/DNS/DNSInput/pos I
iload 1
iadd
putfield org/xbill/DNS/DNSInput/end I
return
.limit locals 2
.limit stack 3
.end method
