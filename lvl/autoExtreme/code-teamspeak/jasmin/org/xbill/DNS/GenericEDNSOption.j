.bytecode 50.0
.class public synchronized org/xbill/DNS/GenericEDNSOption
.super org/xbill/DNS/EDNSOption

.field private 'data' [B

.method <init>(I)V
aload 0
iload 1
invokespecial org/xbill/DNS/EDNSOption/<init>(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(I[B)V
aload 0
iload 1
invokespecial org/xbill/DNS/EDNSOption/<init>(I)V
aload 0
ldc "option data"
aload 2
ldc_w 65535
invokestatic org/xbill/DNS/Record/checkByteArrayLength(Ljava/lang/String;[BI)[B
putfield org/xbill/DNS/GenericEDNSOption/data [B
return
.limit locals 3
.limit stack 4
.end method

.method optionFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/GenericEDNSOption/data [B
return
.limit locals 2
.limit stack 2
.end method

.method optionToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
ldc "<"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/GenericEDNSOption/data [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ">"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method optionToWire(Lorg/xbill/DNS/DNSOutput;)V
aload 1
aload 0
getfield org/xbill/DNS/GenericEDNSOption/data [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 2
.limit stack 2
.end method
