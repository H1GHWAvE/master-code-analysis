.bytecode 50.0
.class public synchronized org/xbill/DNS/MXRecord
.super org/xbill/DNS/U16NameBase

.field private static final 'serialVersionUID' J = 2914841027584208546L


.method <init>()V
aload 0
invokespecial org/xbill/DNS/U16NameBase/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJILorg/xbill/DNS/Name;)V
aload 0
aload 1
bipush 15
iload 2
lload 3
iload 5
ldc "priority"
aload 6
ldc "target"
invokespecial org/xbill/DNS/U16NameBase/<init>(Lorg/xbill/DNS/Name;IIJILjava/lang/String;Lorg/xbill/DNS/Name;Ljava/lang/String;)V
return
.limit locals 7
.limit stack 10
.end method

.method public getAdditionalName()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/MXRecord/getNameField()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/MXRecord
dup
invokespecial org/xbill/DNS/MXRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getPriority()I
aload 0
invokevirtual org/xbill/DNS/MXRecord/getU16Field()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getTarget()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/MXRecord/getNameField()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/MXRecord/u16Field I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/MXRecord/nameField Lorg/xbill/DNS/Name;
aload 1
aload 2
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
return
.limit locals 4
.limit stack 4
.end method
