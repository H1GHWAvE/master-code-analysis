.bytecode 50.0
.class public synchronized org/xbill/DNS/WKSRecord$Protocol
.super java/lang/Object

.field public static final 'ARGUS' I = 13


.field public static final 'BBN_RCC_MON' I = 10


.field public static final 'BR_SAT_MON' I = 76


.field public static final 'CFTP' I = 62


.field public static final 'CHAOS' I = 16


.field public static final 'DCN_MEAS' I = 19


.field public static final 'EGP' I = 8


.field public static final 'EMCON' I = 14


.field public static final 'GGP' I = 3


.field public static final 'HMP' I = 20


.field public static final 'ICMP' I = 1


.field public static final 'IGMP' I = 2


.field public static final 'IGP' I = 9


.field public static final 'IPCV' I = 71


.field public static final 'IPPC' I = 67


.field public static final 'IRTP' I = 28


.field public static final 'ISO_TP4' I = 29


.field public static final 'LEAF_1' I = 25


.field public static final 'LEAF_2' I = 26


.field public static final 'MERIT_INP' I = 32


.field public static final 'MFE_NSP' I = 31


.field public static final 'MIT_SUBNET' I = 65


.field public static final 'MUX' I = 18


.field public static final 'NETBLT' I = 30


.field public static final 'NVP_II' I = 11


.field public static final 'PRM' I = 21


.field public static final 'PUP' I = 12


.field public static final 'RDP' I = 27


.field public static final 'RVD' I = 66


.field public static final 'SAT_EXPAK' I = 64


.field public static final 'SAT_MON' I = 69


.field public static final 'SEP' I = 33


.field public static final 'ST' I = 5


.field public static final 'TCP' I = 6


.field public static final 'TRUNK_1' I = 23


.field public static final 'TRUNK_2' I = 24


.field public static final 'UCL' I = 7


.field public static final 'UDP' I = 17


.field public static final 'WB_EXPAK' I = 79


.field public static final 'WB_MON' I = 78


.field public static final 'XNET' I = 15


.field public static final 'XNS_IDP' I = 22


.field private static 'protocols' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "IP protocol"
iconst_3
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
aload 0
sipush 255
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "icmp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "igmp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "ggp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iconst_5
ldc "st"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 6
ldc "tcp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 7
ldc "ucl"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 8
ldc "egp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 9
ldc "igp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 10
ldc "bbn-rcc-mon"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 11
ldc "nvp-ii"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 12
ldc "pup"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 13
ldc "argus"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 14
ldc "emcon"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 15
ldc "xnet"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 16
ldc "chaos"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 17
ldc "udp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 18
ldc "mux"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 19
ldc "dcn-meas"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 20
ldc "hmp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 21
ldc "prm"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 22
ldc "xns-idp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 23
ldc "trunk-1"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 24
ldc "trunk-2"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 25
ldc "leaf-1"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 26
ldc "leaf-2"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 27
ldc "rdp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 28
ldc "irtp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 29
ldc "iso-tp4"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 30
ldc "netblt"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 31
ldc "mfe-nsp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 32
ldc "merit-inp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 33
ldc "sep"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 62
ldc "cftp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 64
ldc "sat-expak"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 65
ldc "mit-subnet"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 66
ldc "rvd"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 67
ldc "ippc"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 69
ldc "sat-mon"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 71
ldc "ipcv"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 76
ldc "br-sat-mon"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 78
ldc "wb-mon"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
bipush 79
ldc "wb-expak"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/WKSRecord$Protocol/protocols Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
