.bytecode 50.0
.class public final synchronized org/xbill/DNS/Options
.super java/lang/Object

.field private static 'table' Ljava/util/Map;

.method static <clinit>()V
.catch java/lang/SecurityException from L0 to L1 using L2
L0:
invokestatic org/xbill/DNS/Options/refresh()V
L1:
return
L2:
astore 0
return
.limit locals 1
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static check(Ljava/lang/String;)Z
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
ifnonnull L0
L1:
iconst_0
ireturn
L0:
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
ifnull L1
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static clear()V
aconst_null
putstatic org/xbill/DNS/Options/table Ljava/util/Map;
return
.limit locals 0
.limit stack 1
.end method

.method public static intValue(Ljava/lang/String;)I
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
invokestatic org/xbill/DNS/Options/value(Ljava/lang/String;)Ljava/lang/String;
astore 0
aload 0
ifnull L3
L0:
aload 0
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 1
L1:
iload 1
ifle L3
iload 1
ireturn
L2:
astore 0
L3:
iconst_m1
ireturn
.limit locals 2
.limit stack 1
.end method

.method public static refresh()V
ldc "dnsjava.options"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnull L0
new java/util/StringTokenizer
dup
aload 1
ldc ","
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 1
L1:
aload 1
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L0
aload 1
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
astore 2
aload 2
bipush 61
invokevirtual java/lang/String/indexOf(I)I
istore 0
iload 0
iconst_m1
if_icmpne L2
aload 2
invokestatic org/xbill/DNS/Options/set(Ljava/lang/String;)V
goto L1
L2:
aload 2
iconst_0
iload 0
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
aload 2
iload 0
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic org/xbill/DNS/Options/set(Ljava/lang/String;Ljava/lang/String;)V
goto L1
L0:
return
.limit locals 3
.limit stack 4
.end method

.method public static set(Ljava/lang/String;)V
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
ifnonnull L0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putstatic org/xbill/DNS/Options/table Ljava/util/Map;
L0:
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "true"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 1
.limit stack 3
.end method

.method public static set(Ljava/lang/String;Ljava/lang/String;)V
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
ifnonnull L0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putstatic org/xbill/DNS/Options/table Ljava/util/Map;
L0:
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 2
.limit stack 3
.end method

.method public static unset(Ljava/lang/String;)V
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
ifnonnull L0
return
L0:
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
return
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)Ljava/lang/String;
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
ifnonnull L0
aconst_null
areturn
L0:
getstatic org/xbill/DNS/Options/table Ljava/util/Map;
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
areturn
.limit locals 1
.limit stack 2
.end method
