.bytecode 50.0
.class synchronized abstract org/xbill/DNS/U16NameBase
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -8315884183112502995L


.field protected 'nameField' Lorg/xbill/DNS/Name;

.field protected 'u16Field' I

.method protected <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected <init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
aload 1
iload 2
iload 3
lload 4
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
return
.limit locals 6
.limit stack 6
.end method

.method protected <init>(Lorg/xbill/DNS/Name;IIJILjava/lang/String;Lorg/xbill/DNS/Name;Ljava/lang/String;)V
aload 0
aload 1
iload 2
iload 3
lload 4
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
aload 7
iload 6
invokestatic org/xbill/DNS/U16NameBase/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/U16NameBase/u16Field I
aload 0
aload 9
aload 8
invokestatic org/xbill/DNS/U16NameBase/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/U16NameBase/nameField Lorg/xbill/DNS/Name;
return
.limit locals 10
.limit stack 6
.end method

.method protected getNameField()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/U16NameBase/nameField Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected getU16Field()I
aload 0
getfield org/xbill/DNS/U16NameBase/u16Field I
ireturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/U16NameBase/u16Field I
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/U16NameBase/nameField Lorg/xbill/DNS/Name;
return
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/U16NameBase/u16Field I
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/U16NameBase/nameField Lorg/xbill/DNS/Name;
return
.limit locals 2
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/U16NameBase/u16Field I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/U16NameBase/nameField Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/U16NameBase/u16Field I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/U16NameBase/nameField Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
return
.limit locals 4
.limit stack 4
.end method
