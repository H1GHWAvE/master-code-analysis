.bytecode 50.0
.class public synchronized org/xbill/DNS/NAPTRRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 5191232392044947002L


.field private 'flags' [B

.field private 'order' I

.field private 'preference' I

.field private 'regexp' [B

.field private 'replacement' Lorg/xbill/DNS/Name;

.field private 'service' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xbill/DNS/Name;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
aload 1
bipush 35
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "order"
iload 5
invokestatic org/xbill/DNS/NAPTRRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/NAPTRRecord/order I
aload 0
ldc "preference"
iload 6
invokestatic org/xbill/DNS/NAPTRRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/NAPTRRecord/preference I
L0:
aload 0
aload 7
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/NAPTRRecord/flags [B
aload 0
aload 8
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/NAPTRRecord/service [B
aload 0
aload 9
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/NAPTRRecord/regexp [B
L1:
aload 0
ldc "replacement"
aload 10
invokestatic org/xbill/DNS/NAPTRRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/NAPTRRecord/replacement Lorg/xbill/DNS/Name;
return
L2:
astore 1
new java/lang/IllegalArgumentException
dup
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 11
.limit stack 6
.end method

.method public getAdditionalName()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/NAPTRRecord/replacement Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFlags()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/NAPTRRecord/flags [B
iconst_0
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/NAPTRRecord
dup
invokespecial org/xbill/DNS/NAPTRRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getOrder()I
aload 0
getfield org/xbill/DNS/NAPTRRecord/order I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPreference()I
aload 0
getfield org/xbill/DNS/NAPTRRecord/preference I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getRegexp()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/NAPTRRecord/regexp [B
iconst_0
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getReplacement()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/NAPTRRecord/replacement Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getService()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/NAPTRRecord/service [B
iconst_0
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/NAPTRRecord/order I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/NAPTRRecord/preference I
L0:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/NAPTRRecord/flags [B
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/NAPTRRecord/service [B
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/NAPTRRecord/regexp [B
L1:
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/NAPTRRecord/replacement Lorg/xbill/DNS/Name;
return
L2:
astore 2
aload 1
aload 2
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/NAPTRRecord/order I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/NAPTRRecord/preference I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/NAPTRRecord/flags [B
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/NAPTRRecord/service [B
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/NAPTRRecord/regexp [B
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/NAPTRRecord/replacement Lorg/xbill/DNS/Name;
return
.limit locals 2
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/order I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/preference I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/flags [B
iconst_1
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/service [B
iconst_1
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/regexp [B
iconst_1
invokestatic org/xbill/DNS/NAPTRRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/replacement Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/order I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/preference I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/flags [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/service [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
aload 1
aload 0
getfield org/xbill/DNS/NAPTRRecord/regexp [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
aload 0
getfield org/xbill/DNS/NAPTRRecord/replacement Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
return
.limit locals 4
.limit stack 4
.end method
