.bytecode 50.0
.class synchronized org/xbill/DNS/ZoneTransferIn$BasicHandler
.super java/lang/Object
.implements org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler

.field private 'axfr' Ljava/util/List;

.field private 'ixfr' Ljava/util/List;

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/xbill/DNS/ZoneTransferIn$1;)V
aload 0
invokespecial org/xbill/DNS/ZoneTransferIn$BasicHandler/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method static access$300(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/axfr Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method static access$400(Lorg/xbill/DNS/ZoneTransferIn$BasicHandler;)Ljava/util/List;
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/ixfr Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public handleRecord(Lorg/xbill/DNS/Record;)V
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/ixfr Ljava/util/List;
ifnull L0
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/ixfr Ljava/util/List;
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/ixfr Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/ZoneTransferIn$Delta
astore 2
aload 2
getfield org/xbill/DNS/ZoneTransferIn$Delta/adds Ljava/util/List;
invokeinterface java/util/List/size()I 0
ifle L1
aload 2
getfield org/xbill/DNS/ZoneTransferIn$Delta/adds Ljava/util/List;
astore 2
L2:
aload 2
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
L1:
aload 2
getfield org/xbill/DNS/ZoneTransferIn$Delta/deletes Ljava/util/List;
astore 2
goto L2
L0:
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/axfr Ljava/util/List;
astore 2
goto L2
.limit locals 3
.limit stack 3
.end method

.method public startAXFR()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield org/xbill/DNS/ZoneTransferIn$BasicHandler/axfr Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method public startIXFR()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield org/xbill/DNS/ZoneTransferIn$BasicHandler/ixfr Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method public startIXFRAdds(Lorg/xbill/DNS/Record;)V
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/ixfr Ljava/util/List;
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/ixfr Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/ZoneTransferIn$Delta
astore 2
aload 2
getfield org/xbill/DNS/ZoneTransferIn$Delta/adds Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 2
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn/access$100(Lorg/xbill/DNS/Record;)J
putfield org/xbill/DNS/ZoneTransferIn$Delta/end J
return
.limit locals 3
.limit stack 3
.end method

.method public startIXFRDeletes(Lorg/xbill/DNS/Record;)V
new org/xbill/DNS/ZoneTransferIn$Delta
dup
aconst_null
invokespecial org/xbill/DNS/ZoneTransferIn$Delta/<init>(Lorg/xbill/DNS/ZoneTransferIn$1;)V
astore 2
aload 2
getfield org/xbill/DNS/ZoneTransferIn$Delta/deletes Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 2
aload 1
invokestatic org/xbill/DNS/ZoneTransferIn/access$100(Lorg/xbill/DNS/Record;)J
putfield org/xbill/DNS/ZoneTransferIn$Delta/start J
aload 0
getfield org/xbill/DNS/ZoneTransferIn$BasicHandler/ixfr Ljava/util/List;
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 3
.limit stack 3
.end method
