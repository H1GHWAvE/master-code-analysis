.bytecode 50.0
.class synchronized org/xbill/DNS/EmptyRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 3601852050646429582L


.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/EmptyRecord
dup
invokespecial org/xbill/DNS/EmptyRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
return
.limit locals 3
.limit stack 0
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
return
.limit locals 2
.limit stack 0
.end method

.method rrToString()Ljava/lang/String;
ldc ""
areturn
.limit locals 1
.limit stack 1
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
return
.limit locals 4
.limit stack 0
.end method
