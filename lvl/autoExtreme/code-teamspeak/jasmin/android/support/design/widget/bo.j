.bytecode 50.0
.class final synchronized android/support/design/widget/bo
.super android/support/v4/widget/ej

.field final synthetic 'a' Landroid/support/design/widget/SwipeDismissBehavior;

.field private 'b' I

.method <init>(Landroid/support/design/widget/SwipeDismissBehavior;)V
aload 0
aload 1
putfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
aload 0
invokespecial android/support/v4/widget/ej/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/view/View;F)Z
fload 2
fconst_0
fcmpl
ifeq L0
aload 1
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L1
iconst_1
istore 3
L2:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/c(Landroid/support/design/widget/SwipeDismissBehavior;)I
iconst_2
if_icmpne L3
L4:
iconst_1
ireturn
L1:
iconst_0
istore 3
goto L2
L3:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/c(Landroid/support/design/widget/SwipeDismissBehavior;)I
ifne L5
iload 3
ifeq L6
fload 2
fconst_0
fcmpg
iflt L4
iconst_0
ireturn
L6:
fload 2
fconst_0
fcmpl
ifgt L4
iconst_0
ireturn
L5:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/c(Landroid/support/design/widget/SwipeDismissBehavior;)I
iconst_1
if_icmpne L7
iload 3
ifeq L8
fload 2
fconst_0
fcmpl
ifgt L4
iconst_0
ireturn
L8:
fload 2
fconst_0
fcmpg
iflt L4
iconst_0
ireturn
L0:
aload 1
invokevirtual android/view/View/getLeft()I
istore 3
aload 0
getfield android/support/design/widget/bo/b I
istore 4
aload 1
invokevirtual android/view/View/getWidth()I
i2f
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/d(Landroid/support/design/widget/SwipeDismissBehavior;)F
fmul
invokestatic java/lang/Math/round(F)I
istore 5
iload 3
iload 4
isub
invokestatic java/lang/Math/abs(I)I
iload 5
if_icmpge L4
iconst_0
ireturn
L7:
iconst_0
ireturn
.limit locals 6
.limit stack 2
.end method

.method public final a(Landroid/view/View;I)I
aload 1
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
iconst_1
istore 3
L1:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/c(Landroid/support/design/widget/SwipeDismissBehavior;)I
ifne L2
iload 3
ifeq L3
aload 0
getfield android/support/design/widget/bo/b I
aload 1
invokevirtual android/view/View/getWidth()I
isub
istore 3
aload 0
getfield android/support/design/widget/bo/b I
istore 4
L4:
iload 3
iload 2
iload 4
invokestatic android/support/design/widget/SwipeDismissBehavior/a(III)I
ireturn
L0:
iconst_0
istore 3
goto L1
L3:
aload 0
getfield android/support/design/widget/bo/b I
istore 3
aload 0
getfield android/support/design/widget/bo/b I
aload 1
invokevirtual android/view/View/getWidth()I
iadd
istore 4
goto L4
L2:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/c(Landroid/support/design/widget/SwipeDismissBehavior;)I
iconst_1
if_icmpne L5
iload 3
ifeq L6
aload 0
getfield android/support/design/widget/bo/b I
istore 3
aload 0
getfield android/support/design/widget/bo/b I
aload 1
invokevirtual android/view/View/getWidth()I
iadd
istore 4
goto L4
L6:
aload 0
getfield android/support/design/widget/bo/b I
aload 1
invokevirtual android/view/View/getWidth()I
isub
istore 3
aload 0
getfield android/support/design/widget/bo/b I
istore 4
goto L4
L5:
aload 0
getfield android/support/design/widget/bo/b I
aload 1
invokevirtual android/view/View/getWidth()I
isub
istore 3
aload 0
getfield android/support/design/widget/bo/b I
aload 1
invokevirtual android/view/View/getWidth()I
iadd
istore 4
goto L4
.limit locals 5
.limit stack 3
.end method

.method public final a(I)V
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;
ifnull L0
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;
iload 1
invokeinterface android/support/design/widget/bp/a(I)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;F)V
iconst_1
istore 7
aload 1
invokevirtual android/view/View/getWidth()I
istore 4
fload 2
fconst_0
fcmpl
ifeq L0
aload 1
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L1
iconst_1
istore 3
L2:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/c(Landroid/support/design/widget/SwipeDismissBehavior;)I
iconst_2
if_icmpne L3
iconst_1
istore 3
L4:
iload 3
ifeq L5
aload 1
invokevirtual android/view/View/getLeft()I
aload 0
getfield android/support/design/widget/bo/b I
if_icmpge L6
aload 0
getfield android/support/design/widget/bo/b I
iload 4
isub
istore 3
L7:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/b(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/v4/widget/eg;
iload 3
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(II)Z
ifeq L8
aload 1
new android/support/design/widget/bq
dup
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
aload 1
iload 7
invokespecial android/support/design/widget/bq/<init>(Landroid/support/design/widget/SwipeDismissBehavior;Landroid/view/View;Z)V
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Ljava/lang/Runnable;)V
L9:
return
L1:
iconst_0
istore 3
goto L2
L3:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/c(Landroid/support/design/widget/SwipeDismissBehavior;)I
ifne L10
iload 3
ifeq L11
fload 2
fconst_0
fcmpg
ifge L12
iconst_1
istore 3
goto L4
L12:
iconst_0
istore 3
goto L4
L11:
fload 2
fconst_0
fcmpl
ifle L13
iconst_1
istore 3
goto L4
L13:
iconst_0
istore 3
goto L4
L10:
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/c(Landroid/support/design/widget/SwipeDismissBehavior;)I
iconst_1
if_icmpne L14
iload 3
ifeq L15
fload 2
fconst_0
fcmpl
ifle L16
iconst_1
istore 3
goto L4
L16:
iconst_0
istore 3
goto L4
L15:
fload 2
fconst_0
fcmpg
ifge L17
iconst_1
istore 3
goto L4
L17:
iconst_0
istore 3
goto L4
L0:
aload 1
invokevirtual android/view/View/getLeft()I
istore 3
aload 0
getfield android/support/design/widget/bo/b I
istore 5
aload 1
invokevirtual android/view/View/getWidth()I
i2f
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/d(Landroid/support/design/widget/SwipeDismissBehavior;)F
fmul
invokestatic java/lang/Math/round(F)I
istore 6
iload 3
iload 5
isub
invokestatic java/lang/Math/abs(I)I
iload 6
if_icmplt L18
iconst_1
istore 3
goto L4
L18:
iconst_0
istore 3
goto L4
L14:
iconst_0
istore 3
goto L4
L6:
aload 0
getfield android/support/design/widget/bo/b I
iload 4
iadd
istore 3
goto L7
L5:
aload 0
getfield android/support/design/widget/bo/b I
istore 3
iconst_0
istore 7
goto L7
L8:
iload 7
ifeq L9
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;
ifnull L9
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;
invokeinterface android/support/design/widget/bp/a()V 0
return
.limit locals 8
.limit stack 6
.end method

.method public final a(Landroid/view/View;)Z
aload 0
aload 1
invokevirtual android/view/View/getLeft()I
putfield android/support/design/widget/bo/b I
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getWidth()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final b(Landroid/view/View;I)V
aload 0
getfield android/support/design/widget/bo/b I
i2f
aload 1
invokevirtual android/view/View/getWidth()I
i2f
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/e(Landroid/support/design/widget/SwipeDismissBehavior;)F
fmul
fadd
fstore 3
aload 0
getfield android/support/design/widget/bo/b I
i2f
aload 1
invokevirtual android/view/View/getWidth()I
i2f
aload 0
getfield android/support/design/widget/bo/a Landroid/support/design/widget/SwipeDismissBehavior;
invokestatic android/support/design/widget/SwipeDismissBehavior/f(Landroid/support/design/widget/SwipeDismissBehavior;)F
fmul
fadd
fstore 4
iload 2
i2f
fload 3
fcmpg
ifgt L0
aload 1
fconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
return
L0:
iload 2
i2f
fload 4
fcmpl
iflt L1
aload 1
fconst_0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
return
L1:
aload 1
fconst_1
fload 3
fload 4
iload 2
i2f
invokestatic android/support/design/widget/SwipeDismissBehavior/a(FFF)F
fsub
invokestatic android/support/design/widget/SwipeDismissBehavior/b(F)F
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
return
.limit locals 5
.limit stack 5
.end method

.method public final c(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getTop()I
ireturn
.limit locals 2
.limit stack 1
.end method
