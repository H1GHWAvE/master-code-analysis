.bytecode 50.0
.class final synchronized android/support/design/widget/bw
.super android/widget/LinearLayout

.field 'a' I

.field 'b' F

.field 'c' I

.field 'd' I

.field final synthetic 'e' Landroid/support/design/widget/br;

.field private 'f' I

.field private final 'g' Landroid/graphics/Paint;

.method <init>(Landroid/support/design/widget/br;Landroid/content/Context;)V
aload 0
aload 1
putfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
aload 0
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
aload 0
iconst_m1
putfield android/support/design/widget/bw/a I
aload 0
iconst_m1
putfield android/support/design/widget/bw/c I
aload 0
iconst_m1
putfield android/support/design/widget/bw/d I
aload 0
iconst_0
invokevirtual android/support/design/widget/bw/setWillNotDraw(Z)V
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/design/widget/bw/g Landroid/graphics/Paint;
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/bw;)F
aload 0
fconst_0
putfield android/support/design/widget/bw/b F
fconst_0
freturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Landroid/support/design/widget/bw;I)I
aload 0
iload 1
putfield android/support/design/widget/bw/a I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a(IF)V
aload 0
iload 1
putfield android/support/design/widget/bw/a I
aload 0
fload 2
putfield android/support/design/widget/bw/b F
aload 0
invokevirtual android/support/design/widget/bw/a()V
return
.limit locals 3
.limit stack 2
.end method

.method private a(II)V
iload 1
aload 0
getfield android/support/design/widget/bw/c I
if_icmpne L0
iload 2
aload 0
getfield android/support/design/widget/bw/d I
if_icmpeq L1
L0:
aload 0
iload 1
putfield android/support/design/widget/bw/c I
aload 0
iload 2
putfield android/support/design/widget/bw/d I
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Landroid/support/design/widget/bw;II)V
aload 0
iload 1
iload 2
invokespecial android/support/design/widget/bw/a(II)V
return
.limit locals 3
.limit stack 3
.end method

.method private b()Z
iconst_0
istore 4
aload 0
invokevirtual android/support/design/widget/bw/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 4
istore 3
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getWidth()I
ifgt L2
iconst_1
istore 3
L1:
iload 3
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 5
.limit stack 2
.end method

.method private c()F
aload 0
getfield android/support/design/widget/bw/a I
i2f
aload 0
getfield android/support/design/widget/bw/b F
fadd
freturn
.limit locals 1
.limit stack 2
.end method

.method private c(I)V
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
iconst_1
istore 2
L1:
aload 0
iload 1
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getLeft()I
istore 4
aload 6
invokevirtual android/view/View/getRight()I
istore 5
iload 1
aload 0
getfield android/support/design/widget/bw/a I
isub
invokestatic java/lang/Math/abs(I)I
iconst_1
if_icmpgt L2
aload 0
getfield android/support/design/widget/bw/c I
istore 2
aload 0
getfield android/support/design/widget/bw/d I
istore 3
L3:
iload 2
iload 4
if_icmpne L4
iload 3
iload 5
if_icmpeq L5
L4:
aload 0
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
invokestatic android/support/design/widget/dh/a()Landroid/support/design/widget/ck;
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;Landroid/support/design/widget/ck;)Landroid/support/design/widget/ck;
astore 6
aload 6
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/design/widget/ck/a(Landroid/view/animation/Interpolator;)V
aload 6
sipush 300
invokevirtual android/support/design/widget/ck/a(I)V
aload 6
fconst_0
fconst_1
invokevirtual android/support/design/widget/ck/a(FF)V
aload 6
new android/support/design/widget/bx
dup
aload 0
iload 2
iload 4
iload 3
iload 5
invokespecial android/support/design/widget/bx/<init>(Landroid/support/design/widget/bw;IIII)V
invokevirtual android/support/design/widget/ck/a(Landroid/support/design/widget/cp;)V
new android/support/design/widget/by
dup
aload 0
iload 1
invokespecial android/support/design/widget/by/<init>(Landroid/support/design/widget/bw;I)V
astore 7
aload 6
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
new android/support/design/widget/cm
dup
aload 6
aload 7
invokespecial android/support/design/widget/cm/<init>(Landroid/support/design/widget/ck;Landroid/support/design/widget/cn;)V
invokevirtual android/support/design/widget/cr/a(Landroid/support/design/widget/cs;)V
aload 6
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/a()V
L5:
return
L0:
iconst_0
istore 2
goto L1
L2:
aload 0
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
bipush 24
invokestatic android/support/design/widget/br/b(Landroid/support/design/widget/br;I)I
istore 3
iload 1
aload 0
getfield android/support/design/widget/bw/a I
if_icmpge L6
iload 2
ifne L7
iload 5
iload 3
iadd
istore 3
iload 3
istore 2
goto L3
L6:
iload 2
ifeq L7
iload 5
iload 3
iadd
istore 3
iload 3
istore 2
goto L3
L7:
iload 4
iload 3
isub
istore 3
iload 3
istore 2
goto L3
.limit locals 8
.limit stack 8
.end method

.method final a()V
aload 0
aload 0
getfield android/support/design/widget/bw/a I
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 8
aload 8
ifnull L0
aload 8
invokevirtual android/view/View/getWidth()I
ifle L0
aload 8
invokevirtual android/view/View/getLeft()I
istore 7
aload 8
invokevirtual android/view/View/getRight()I
istore 6
iload 7
istore 4
iload 6
istore 5
aload 0
getfield android/support/design/widget/bw/b F
fconst_0
fcmpl
ifle L1
iload 7
istore 4
iload 6
istore 5
aload 0
getfield android/support/design/widget/bw/a I
aload 0
invokevirtual android/support/design/widget/bw/getChildCount()I
iconst_1
isub
if_icmpge L1
aload 0
aload 0
getfield android/support/design/widget/bw/a I
iconst_1
iadd
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 8
aload 0
getfield android/support/design/widget/bw/b F
fstore 1
aload 8
invokevirtual android/view/View/getLeft()I
i2f
fstore 2
aload 0
getfield android/support/design/widget/bw/b F
fstore 3
iload 7
i2f
fconst_1
fload 3
fsub
fmul
fload 1
fload 2
fmul
fadd
f2i
istore 4
aload 0
getfield android/support/design/widget/bw/b F
fstore 1
aload 8
invokevirtual android/view/View/getRight()I
i2f
fstore 2
aload 0
getfield android/support/design/widget/bw/b F
fstore 3
iload 6
i2f
fconst_1
fload 3
fsub
fmul
fload 2
fload 1
fmul
fadd
f2i
istore 5
L1:
aload 0
iload 4
iload 5
invokespecial android/support/design/widget/bw/a(II)V
return
L0:
iconst_m1
istore 4
iconst_m1
istore 5
goto L1
.limit locals 9
.limit stack 3
.end method

.method final a(I)V
aload 0
getfield android/support/design/widget/bw/g Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getColor()I
iload 1
if_icmpeq L0
aload 0
getfield android/support/design/widget/bw/g Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final b(I)V
aload 0
getfield android/support/design/widget/bw/f I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/bw/f I
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout/draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/widget/bw/c I
iflt L0
aload 0
getfield android/support/design/widget/bw/d I
aload 0
getfield android/support/design/widget/bw/c I
if_icmple L0
aload 1
aload 0
getfield android/support/design/widget/bw/c I
i2f
aload 0
invokevirtual android/support/design/widget/bw/getHeight()I
aload 0
getfield android/support/design/widget/bw/f I
isub
i2f
aload 0
getfield android/support/design/widget/bw/d I
i2f
aload 0
invokevirtual android/support/design/widget/bw/getHeight()I
i2f
aload 0
getfield android/support/design/widget/bw/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L0:
return
.limit locals 2
.limit stack 6
.end method

.method protected final onLayout(ZIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/LinearLayout/onLayout(ZIIII)V
aload 0
invokevirtual android/support/design/widget/bw/a()V
return
.limit locals 6
.limit stack 6
.end method

.method protected final onMeasure(II)V
aload 0
iload 1
iload 2
invokespecial android/widget/LinearLayout/onMeasure(II)V
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
ldc_w 1073741824
if_icmpeq L0
L1:
return
L0:
aload 0
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/j(Landroid/support/design/widget/br;)I
iconst_1
if_icmpne L1
aload 0
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/k(Landroid/support/design/widget/br;)I
iconst_1
if_icmpne L1
aload 0
invokevirtual android/support/design/widget/bw/getChildCount()I
istore 5
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
iconst_0
istore 4
iconst_0
istore 3
L2:
iload 4
iload 5
if_icmpge L3
aload 0
iload 4
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
astore 7
aload 7
iload 6
iload 2
invokevirtual android/view/View/measure(II)V
iload 3
aload 7
invokevirtual android/view/View/getMeasuredWidth()I
invokestatic java/lang/Math/max(II)I
istore 3
iload 4
iconst_1
iadd
istore 4
goto L2
L3:
iload 3
ifle L1
aload 0
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
bipush 16
invokestatic android/support/design/widget/br/b(Landroid/support/design/widget/br;I)I
istore 4
iload 3
iload 5
imul
aload 0
invokevirtual android/support/design/widget/bw/getMeasuredWidth()I
iload 4
iconst_2
imul
isub
if_icmpgt L4
iconst_0
istore 4
L5:
iload 4
iload 5
if_icmpge L6
aload 0
iload 4
invokevirtual android/support/design/widget/bw/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/LinearLayout$LayoutParams
astore 7
aload 7
iload 3
putfield android/widget/LinearLayout$LayoutParams/width I
aload 7
fconst_0
putfield android/widget/LinearLayout$LayoutParams/weight F
iload 4
iconst_1
iadd
istore 4
goto L5
L4:
aload 0
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/l(Landroid/support/design/widget/br;)I
pop
aload 0
getfield android/support/design/widget/bw/e Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/m(Landroid/support/design/widget/br;)V
L6:
aload 0
iload 1
iload 2
invokespecial android/widget/LinearLayout/onMeasure(II)V
return
.limit locals 8
.limit stack 4
.end method
