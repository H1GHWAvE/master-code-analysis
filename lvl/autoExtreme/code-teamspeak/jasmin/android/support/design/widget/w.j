.bytecode 50.0
.class public final synchronized android/support/design/widget/w
.super android/view/ViewGroup$MarginLayoutParams

.field 'a' Landroid/support/design/widget/t;

.field 'b' Z

.field public 'c' I

.field public 'd' I

.field public 'e' I

.field 'f' I

.field 'g' Landroid/view/View;

.field 'h' Landroid/view/View;

.field 'i' Z

.field 'j' Z

.field 'k' Z

.field final 'l' Landroid/graphics/Rect;

.field 'm' Ljava/lang/Object;

.method public <init>()V
aload 0
bipush -2
bipush -2
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(II)V
aload 0
iconst_0
putfield android/support/design/widget/w/b Z
aload 0
iconst_0
putfield android/support/design/widget/w/c I
aload 0
iconst_0
putfield android/support/design/widget/w/d I
aload 0
iconst_m1
putfield android/support/design/widget/w/e I
aload 0
iconst_m1
putfield android/support/design/widget/w/f I
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/w/l Landroid/graphics/Rect;
return
.limit locals 1
.limit stack 3
.end method

.method <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_0
putfield android/support/design/widget/w/b Z
aload 0
iconst_0
putfield android/support/design/widget/w/c I
aload 0
iconst_0
putfield android/support/design/widget/w/d I
aload 0
iconst_m1
putfield android/support/design/widget/w/e I
aload 0
iconst_m1
putfield android/support/design/widget/w/f I
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/w/l Landroid/graphics/Rect;
aload 1
aload 2
getstatic android/support/design/n/CoordinatorLayout_LayoutParams [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 3
aload 0
aload 3
getstatic android/support/design/n/CoordinatorLayout_LayoutParams_android_layout_gravity I
iconst_0
invokevirtual android/content/res/TypedArray/getInteger(II)I
putfield android/support/design/widget/w/c I
aload 0
aload 3
getstatic android/support/design/n/CoordinatorLayout_LayoutParams_layout_anchor I
iconst_m1
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/design/widget/w/f I
aload 0
aload 3
getstatic android/support/design/n/CoordinatorLayout_LayoutParams_layout_anchorGravity I
iconst_0
invokevirtual android/content/res/TypedArray/getInteger(II)I
putfield android/support/design/widget/w/d I
aload 0
aload 3
getstatic android/support/design/n/CoordinatorLayout_LayoutParams_layout_keyline I
iconst_m1
invokevirtual android/content/res/TypedArray/getInteger(II)I
putfield android/support/design/widget/w/e I
aload 0
aload 3
getstatic android/support/design/n/CoordinatorLayout_LayoutParams_layout_behavior I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
putfield android/support/design/widget/w/b Z
aload 0
getfield android/support/design/widget/w/b Z
ifeq L0
aload 0
aload 1
aload 2
aload 3
getstatic android/support/design/n/CoordinatorLayout_LayoutParams_layout_behavior I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
invokestatic android/support/design/widget/CoordinatorLayout/a(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)Landroid/support/design/widget/t;
putfield android/support/design/widget/w/a Landroid/support/design/widget/t;
L0:
aload 3
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 4
.limit stack 5
.end method

.method public <init>(Landroid/support/design/widget/w;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
iconst_0
putfield android/support/design/widget/w/b Z
aload 0
iconst_0
putfield android/support/design/widget/w/c I
aload 0
iconst_0
putfield android/support/design/widget/w/d I
aload 0
iconst_m1
putfield android/support/design/widget/w/e I
aload 0
iconst_m1
putfield android/support/design/widget/w/f I
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/w/l Landroid/graphics/Rect;
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
putfield android/support/design/widget/w/b Z
aload 0
iconst_0
putfield android/support/design/widget/w/c I
aload 0
iconst_0
putfield android/support/design/widget/w/d I
aload 0
iconst_m1
putfield android/support/design/widget/w/e I
aload 0
iconst_m1
putfield android/support/design/widget/w/f I
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/w/l Landroid/graphics/Rect;
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
iconst_0
putfield android/support/design/widget/w/b Z
aload 0
iconst_0
putfield android/support/design/widget/w/c I
aload 0
iconst_0
putfield android/support/design/widget/w/d I
aload 0
iconst_m1
putfield android/support/design/widget/w/e I
aload 0
iconst_m1
putfield android/support/design/widget/w/f I
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/w/l Landroid/graphics/Rect;
return
.limit locals 2
.limit stack 3
.end method

.method private a()I
aload 0
getfield android/support/design/widget/w/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/view/View;
aload 0
getfield android/support/design/widget/w/f I
iconst_m1
if_icmpne L0
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
aconst_null
areturn
L0:
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L1
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getId()I
aload 0
getfield android/support/design/widget/w/f I
if_icmpeq L2
iconst_0
istore 3
L3:
iload 3
ifne L4
L1:
aload 0
aload 1
aload 0
getfield android/support/design/widget/w/f I
invokevirtual android/support/design/widget/CoordinatorLayout/findViewById(I)Landroid/view/View;
putfield android/support/design/widget/w/g Landroid/view/View;
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L5
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
astore 5
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 4
L6:
aload 4
aload 1
if_acmpeq L7
aload 4
ifnull L7
aload 4
aload 2
if_acmpne L8
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/isInEditMode()Z
ifeq L9
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
L4:
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
areturn
L2:
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
astore 5
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 4
L10:
aload 4
aload 1
if_acmpeq L11
aload 4
ifnull L12
aload 4
aload 2
if_acmpne L13
L12:
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
iconst_0
istore 3
goto L3
L13:
aload 4
instanceof android/view/View
ifeq L14
aload 4
checkcast android/view/View
astore 5
L14:
aload 4
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 4
goto L10
L11:
aload 0
aload 5
putfield android/support/design/widget/w/h Landroid/view/View;
iconst_1
istore 3
goto L3
L9:
new java/lang/IllegalStateException
dup
ldc "Anchor must not be a descendant of the anchored view"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L8:
aload 4
instanceof android/view/View
ifeq L15
aload 4
checkcast android/view/View
astore 5
L15:
aload 4
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 4
goto L6
L7:
aload 0
aload 5
putfield android/support/design/widget/w/h Landroid/view/View;
goto L4
L5:
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/isInEditMode()Z
ifeq L16
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
goto L4
L16:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Could not find CoordinatorLayout descendant view with id "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getResources()Landroid/content/res/Resources;
aload 0
getfield android/support/design/widget/w/f I
invokevirtual android/content/res/Resources/getResourceName(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " to anchor view "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 6
.limit stack 5
.end method

.method private a(I)V
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
aload 0
iload 1
putfield android/support/design/widget/w/f I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/widget/w/l Landroid/graphics/Rect;
aload 1
invokevirtual android/graphics/Rect/set(Landroid/graphics/Rect;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;Landroid/support/design/widget/CoordinatorLayout;)V
aload 0
aload 2
aload 0
getfield android/support/design/widget/w/f I
invokevirtual android/support/design/widget/CoordinatorLayout/findViewById(I)Landroid/view/View;
putfield android/support/design/widget/w/g Landroid/view/View;
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
ifnull L0
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
astore 4
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 3
L1:
aload 3
aload 2
if_acmpeq L2
aload 3
ifnull L2
aload 3
aload 1
if_acmpne L3
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/isInEditMode()Z
ifeq L4
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
return
L4:
new java/lang/IllegalStateException
dup
ldc "Anchor must not be a descendant of the anchored view"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 3
instanceof android/view/View
ifeq L5
aload 3
checkcast android/view/View
astore 4
L5:
aload 3
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 3
goto L1
L2:
aload 0
aload 4
putfield android/support/design/widget/w/h Landroid/view/View;
return
L0:
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/isInEditMode()Z
ifeq L6
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
return
L6:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Could not find CoordinatorLayout descendant view with id "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/getResources()Landroid/content/res/Resources;
aload 0
getfield android/support/design/widget/w/f I
invokevirtual android/content/res/Resources/getResourceName(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " to anchor view "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 5
.end method

.method private a(Z)V
aload 0
iload 1
putfield android/support/design/widget/w/j Z
return
.limit locals 2
.limit stack 2
.end method

.method private b()Landroid/support/design/widget/t;
aload 0
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Z)V
aload 0
iload 1
putfield android/support/design/widget/w/k Z
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/view/View;Landroid/support/design/widget/CoordinatorLayout;)Z
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getId()I
aload 0
getfield android/support/design/widget/w/f I
if_icmpeq L0
iconst_0
ireturn
L0:
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
astore 4
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 3
L1:
aload 3
aload 2
if_acmpeq L2
aload 3
ifnull L3
aload 3
aload 1
if_acmpne L4
L3:
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
iconst_0
ireturn
L4:
aload 3
instanceof android/view/View
ifeq L5
aload 3
checkcast android/view/View
astore 4
L5:
aload 3
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 3
goto L1
L2:
aload 0
aload 4
putfield android/support/design/widget/w/h Landroid/view/View;
iconst_1
ireturn
.limit locals 5
.limit stack 2
.end method

.method private c()Landroid/graphics/Rect;
aload 0
getfield android/support/design/widget/w/l Landroid/graphics/Rect;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Z
aload 0
getfield android/support/design/widget/w/g Landroid/view/View;
ifnonnull L0
aload 0
getfield android/support/design/widget/w/f I
iconst_m1
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private e()Z
aload 0
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
ifnonnull L0
aload 0
iconst_0
putfield android/support/design/widget/w/i Z
L0:
aload 0
getfield android/support/design/widget/w/i Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method private f()Z
aload 0
getfield android/support/design/widget/w/i Z
ifeq L0
iconst_1
ireturn
L0:
aload 0
getfield android/support/design/widget/w/i Z
iconst_0
ior
istore 1
aload 0
iload 1
putfield android/support/design/widget/w/i Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private g()V
aload 0
iconst_0
putfield android/support/design/widget/w/i Z
return
.limit locals 1
.limit stack 2
.end method

.method private h()V
aload 0
iconst_0
putfield android/support/design/widget/w/j Z
return
.limit locals 1
.limit stack 2
.end method

.method private i()Z
aload 0
getfield android/support/design/widget/w/j Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
getfield android/support/design/widget/w/k Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()V
aload 0
iconst_0
putfield android/support/design/widget/w/k Z
return
.limit locals 1
.limit stack 2
.end method

.method private l()V
aload 0
aconst_null
putfield android/support/design/widget/w/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/design/widget/w/g Landroid/view/View;
return
.limit locals 1
.limit stack 2
.end method

.method private static m()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method public final a(Landroid/support/design/widget/t;)V
aload 0
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
aload 1
if_acmpeq L0
aload 0
aload 1
putfield android/support/design/widget/w/a Landroid/support/design/widget/t;
aload 0
aconst_null
putfield android/support/design/widget/w/m Ljava/lang/Object;
aload 0
iconst_1
putfield android/support/design/widget/w/b Z
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/view/View;)Z
aload 1
aload 0
getfield android/support/design/widget/w/h Landroid/view/View;
if_acmpeq L0
aload 0
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
ifnull L1
aload 0
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
aload 1
invokevirtual android/support/design/widget/t/b(Landroid/view/View;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method
