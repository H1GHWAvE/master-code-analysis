.bytecode 50.0
.class public final synchronized android/support/design/widget/Snackbar
.super java/lang/Object

.field public static final 'a' I = -2


.field public static final 'b' I = -1


.field public static final 'c' I = 0


.field private static final 'f' I = 250


.field private static final 'g' I = 180


.field private static final 'h' Landroid/os/Handler;

.field private static final 'i' I = 0


.field private static final 'j' I = 1


.field final 'd' Landroid/view/ViewGroup;

.field final 'e' Landroid/support/design/widget/Snackbar$SnackbarLayout;

.field private final 'k' Landroid/content/Context;

.field private 'l' I

.field private 'm' Landroid/support/design/widget/bd;

.field private final 'n' Landroid/support/design/widget/bj;

.method static <clinit>()V
new android/os/Handler
dup
invokestatic android/os/Looper/getMainLooper()Landroid/os/Looper;
new android/support/design/widget/at
dup
invokespecial android/support/design/widget/at/<init>()V
invokespecial android/os/Handler/<init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V
putstatic android/support/design/widget/Snackbar/h Landroid/os/Handler;
return
.limit locals 0
.limit stack 5
.end method

.method private <init>(Landroid/view/ViewGroup;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/support/design/widget/av
dup
aload 0
invokespecial android/support/design/widget/av/<init>(Landroid/support/design/widget/Snackbar;)V
putfield android/support/design/widget/Snackbar/n Landroid/support/design/widget/bj;
aload 0
aload 1
putfield android/support/design/widget/Snackbar/d Landroid/view/ViewGroup;
aload 0
aload 1
invokevirtual android/view/ViewGroup/getContext()Landroid/content/Context;
putfield android/support/design/widget/Snackbar/k Landroid/content/Context;
aload 0
aload 0
getfield android/support/design/widget/Snackbar/k Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_layout_snackbar I
aload 0
getfield android/support/design/widget/Snackbar/d Landroid/view/ViewGroup;
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/support/design/widget/Snackbar$SnackbarLayout
putfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
return
.limit locals 2
.limit stack 5
.end method

.method private a(I)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getActionView()Landroid/widget/Button;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
getfield android/support/design/widget/Snackbar/k Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
astore 3
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getActionView()Landroid/widget/Button;
astore 4
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
aload 2
ifnonnull L1
L0:
aload 4
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
aload 4
aconst_null
invokevirtual android/widget/TextView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
areturn
L1:
aload 4
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
aload 4
aload 3
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 4
new android/support/design/widget/au
dup
aload 0
aload 2
invokespecial android/support/design/widget/au/<init>(Landroid/support/design/widget/Snackbar;Landroid/view/View$OnClickListener;)V
invokevirtual android/widget/TextView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
areturn
.limit locals 5
.limit stack 5
.end method

.method private a(Landroid/content/res/ColorStateList;)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getActionView()Landroid/widget/Button;
aload 1
invokevirtual android/widget/TextView/setTextColor(Landroid/content/res/ColorStateList;)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/design/widget/bd;)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
aload 0
aload 1
putfield android/support/design/widget/Snackbar/m Landroid/support/design/widget/bd;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/ah;
.end annotation
aload 0
invokevirtual android/view/View/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getText(I)Ljava/lang/CharSequence;
astore 3
new android/support/design/widget/Snackbar
dup
aload 0
invokestatic android/support/design/widget/Snackbar/a(Landroid/view/View;)Landroid/view/ViewGroup;
invokespecial android/support/design/widget/Snackbar/<init>(Landroid/view/ViewGroup;)V
astore 0
aload 0
aload 3
invokespecial android/support/design/widget/Snackbar/a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;
pop
aload 0
iload 2
putfield android/support/design/widget/Snackbar/l I
aload 0
areturn
.limit locals 4
.limit stack 3
.end method

.method private static a(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
new android/support/design/widget/Snackbar
dup
aload 0
invokestatic android/support/design/widget/Snackbar/a(Landroid/view/View;)Landroid/view/ViewGroup;
invokespecial android/support/design/widget/Snackbar/<init>(Landroid/view/ViewGroup;)V
astore 0
aload 0
aload 1
invokespecial android/support/design/widget/Snackbar/a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;
pop
aload 0
iload 2
putfield android/support/design/widget/Snackbar/l I
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getMessageView()Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getActionView()Landroid/widget/Button;
astore 3
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
aload 2
ifnonnull L1
L0:
aload 3
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
aload 3
aconst_null
invokevirtual android/widget/TextView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
areturn
L1:
aload 3
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
aload 3
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 3
new android/support/design/widget/au
dup
aload 0
aload 2
invokespecial android/support/design/widget/au/<init>(Landroid/support/design/widget/Snackbar;Landroid/view/View$OnClickListener;)V
invokevirtual android/widget/TextView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
areturn
.limit locals 4
.limit stack 5
.end method

.method static synthetic a(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bj;
aload 0
getfield android/support/design/widget/Snackbar/n Landroid/support/design/widget/bj;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/View;)Landroid/view/ViewGroup;
aconst_null
astore 3
aload 0
astore 2
L0:
aload 2
instanceof android/support/design/widget/CoordinatorLayout
ifeq L1
aload 2
checkcast android/view/ViewGroup
areturn
L1:
aload 3
astore 1
aload 2
instanceof android/widget/FrameLayout
ifeq L2
aload 2
invokevirtual android/view/View/getId()I
ldc_w 16908290
if_icmpne L3
aload 2
checkcast android/view/ViewGroup
areturn
L3:
aload 2
checkcast android/view/ViewGroup
astore 1
L2:
aload 2
astore 0
aload 2
ifnull L4
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 0
aload 0
instanceof android/view/View
ifeq L5
aload 0
checkcast android/view/View
astore 0
L4:
aload 0
astore 2
aload 1
astore 3
aload 0
ifnonnull L0
aload 1
areturn
L5:
aconst_null
astore 0
goto L4
.limit locals 4
.limit stack 2
.end method

.method static synthetic a(Landroid/support/design/widget/Snackbar;I)V
aload 0
iload 1
invokespecial android/support/design/widget/Snackbar/d(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
aload 0
getfield android/support/design/widget/Snackbar/k Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
invokespecial android/support/design/widget/Snackbar/a(Ljava/lang/CharSequence;)Landroid/support/design/widget/Snackbar;
areturn
.limit locals 2
.limit stack 3
.end method

.method static synthetic b(Landroid/support/design/widget/Snackbar;)V
aload 0
invokevirtual android/support/design/widget/Snackbar/a()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic c()Landroid/os/Handler;
getstatic android/support/design/widget/Snackbar/h Landroid/os/Handler;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic c(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/Snackbar$SnackbarLayout;
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(I)Landroid/support/design/widget/Snackbar;
.annotation invisible Landroid/support/a/y;
.end annotation
aload 0
iload 1
putfield android/support/design/widget/Snackbar/l I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private d()I
aload 0
getfield android/support/design/widget/Snackbar/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bd;
aload 0
getfield android/support/design/widget/Snackbar/m Landroid/support/design/widget/bd;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(I)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/support/design/widget/bh/a()Landroid/support/design/widget/bh;
astore 3
aload 0
getfield android/support/design/widget/Snackbar/n Landroid/support/design/widget/bj;
astore 4
aload 3
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 3
aload 4
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L4
aload 3
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
iload 1
invokestatic android/support/design/widget/bh/a(Landroid/support/design/widget/bk;I)Z
pop
L1:
aload 2
monitorexit
L3:
return
L4:
aload 3
aload 4
invokevirtual android/support/design/widget/bh/e(Landroid/support/design/widget/bj;)Z
ifeq L1
aload 3
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
iload 1
invokestatic android/support/design/widget/bh/a(Landroid/support/design/widget/bk;I)Z
pop
L5:
goto L1
L2:
astore 3
L6:
aload 2
monitorexit
L7:
aload 3
athrow
.limit locals 5
.limit stack 2
.end method

.method private e()Landroid/view/View;
.annotation invisible Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e(I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getHeight()I
i2f
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
ldc2_w 250L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
new android/support/design/widget/ba
dup
aload 0
iload 1
invokespecial android/support/design/widget/ba/<init>(Landroid/support/design/widget/Snackbar;I)V
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
return
L0:
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getContext()Landroid/content/Context;
getstatic android/support/design/c/design_snackbar_out I
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 2
aload 2
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 2
ldc2_w 250L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 2
new android/support/design/widget/bb
dup
aload 0
iload 1
invokespecial android/support/design/widget/bb/<init>(Landroid/support/design/widget/Snackbar;I)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
aload 2
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 3
.limit stack 5
.end method

.method static synthetic e(Landroid/support/design/widget/Snackbar;)V
aload 0
invokevirtual android/support/design/widget/Snackbar/b()V
return
.limit locals 1
.limit stack 1
.end method

.method private f()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
invokestatic android/support/design/widget/bh/a()Landroid/support/design/widget/bh;
astore 3
aload 0
getfield android/support/design/widget/Snackbar/l I
istore 1
aload 0
getfield android/support/design/widget/Snackbar/n Landroid/support/design/widget/bj;
astore 4
aload 3
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 3
aload 4
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L3
aload 3
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
iload 1
putfield android/support/design/widget/bk/b I
aload 3
getfield android/support/design/widget/bh/b Landroid/os/Handler;
aload 3
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
invokevirtual android/os/Handler/removeCallbacksAndMessages(Ljava/lang/Object;)V
aload 3
aload 3
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
invokevirtual android/support/design/widget/bh/a(Landroid/support/design/widget/bk;)V
aload 2
monitorexit
L1:
return
L3:
aload 3
aload 4
invokevirtual android/support/design/widget/bh/e(Landroid/support/design/widget/bj;)Z
ifeq L8
aload 3
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
iload 1
putfield android/support/design/widget/bk/b I
L4:
aload 3
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
ifnull L10
aload 3
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
iconst_4
invokestatic android/support/design/widget/bh/a(Landroid/support/design/widget/bk;I)Z
ifeq L10
aload 2
monitorexit
L5:
return
L2:
astore 3
L6:
aload 2
monitorexit
L7:
aload 3
athrow
L8:
aload 3
new android/support/design/widget/bk
dup
iload 1
aload 4
invokespecial android/support/design/widget/bk/<init>(ILandroid/support/design/widget/bj;)V
putfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
L9:
goto L4
L10:
aload 3
aconst_null
putfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
aload 3
invokevirtual android/support/design/widget/bh/b()V
aload 2
monitorexit
L11:
return
.limit locals 5
.limit stack 5
.end method

.method private f(I)V
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getVisibility()I
ifne L0
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 3
aload 3
instanceof android/support/design/widget/w
ifeq L1
aload 3
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 3
aload 3
instanceof android/support/design/widget/SwipeDismissBehavior
ifeq L1
aload 3
checkcast android/support/design/widget/SwipeDismissBehavior
astore 3
aload 3
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
ifnull L2
aload 3
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
istore 2
L3:
iload 2
ifeq L4
iconst_1
istore 2
L5:
iload 2
ifeq L6
L0:
aload 0
invokevirtual android/support/design/widget/Snackbar/b()V
return
L2:
iconst_0
istore 2
goto L3
L4:
iconst_0
istore 2
goto L5
L1:
iconst_0
istore 2
goto L5
L6:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L7
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getHeight()I
i2f
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
ldc2_w 250L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
new android/support/design/widget/ba
dup
aload 0
iload 1
invokespecial android/support/design/widget/ba/<init>(Landroid/support/design/widget/Snackbar;I)V
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
return
L7:
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getContext()Landroid/content/Context;
getstatic android/support/design/c/design_snackbar_out I
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 3
aload 3
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 3
ldc2_w 250L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 3
new android/support/design/widget/bb
dup
aload 0
iload 1
invokespecial android/support/design/widget/bb/<init>(Landroid/support/design/widget/Snackbar;I)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
aload 3
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 4
.limit stack 5
.end method

.method private g()V
aload 0
iconst_3
invokespecial android/support/design/widget/Snackbar/d(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private h()Z
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/isShown()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()V
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getParent()Landroid/view/ViewParent;
ifnonnull L0
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 1
aload 1
instanceof android/support/design/widget/w
ifeq L1
new android/support/design/widget/bc
dup
aload 0
invokespecial android/support/design/widget/bc/<init>(Landroid/support/design/widget/Snackbar;)V
astore 2
aload 2
ldc_w 0.1F
invokestatic android/support/design/widget/SwipeDismissBehavior/a(F)F
putfield android/support/design/widget/SwipeDismissBehavior/k F
aload 2
ldc_w 0.6F
invokestatic android/support/design/widget/SwipeDismissBehavior/a(F)F
putfield android/support/design/widget/SwipeDismissBehavior/l F
aload 2
iconst_0
putfield android/support/design/widget/SwipeDismissBehavior/j I
aload 2
new android/support/design/widget/aw
dup
aload 0
invokespecial android/support/design/widget/aw/<init>(Landroid/support/design/widget/Snackbar;)V
putfield android/support/design/widget/SwipeDismissBehavior/i Landroid/support/design/widget/bp;
aload 1
checkcast android/support/design/widget/w
aload 2
invokevirtual android/support/design/widget/w/a(Landroid/support/design/widget/t;)V
L1:
aload 0
getfield android/support/design/widget/Snackbar/d Landroid/view/ViewGroup;
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;)V
L0:
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L2
aload 0
invokevirtual android/support/design/widget/Snackbar/a()V
return
L2:
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
new android/support/design/widget/ax
dup
aload 0
invokespecial android/support/design/widget/ax/<init>(Landroid/support/design/widget/Snackbar;)V
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/setOnLayoutChangeListener(Landroid/support/design/widget/bg;)V
return
.limit locals 3
.limit stack 4
.end method

.method private j()Z
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 2
aload 2
instanceof android/support/design/widget/w
ifeq L0
aload 2
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 2
aload 2
instanceof android/support/design/widget/SwipeDismissBehavior
ifeq L0
aload 2
checkcast android/support/design/widget/SwipeDismissBehavior
astore 2
aload 2
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
ifnull L1
aload 2
getfield android/support/design/widget/SwipeDismissBehavior/h Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
istore 1
L2:
iload 1
ifeq L3
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L3:
iconst_0
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method final a()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getHeight()I
i2f
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
ldc2_w 250L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
new android/support/design/widget/ay
dup
aload 0
invokespecial android/support/design/widget/ay/<init>(Landroid/support/design/widget/Snackbar;)V
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
return
L0:
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getContext()Landroid/content/Context;
getstatic android/support/design/c/design_snackbar_in I
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 1
aload 1
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 1
ldc2_w 250L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 1
new android/support/design/widget/az
dup
aload 0
invokespecial android/support/design/widget/az/<init>(Landroid/support/design/widget/Snackbar;)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
aload 1
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 2
.limit stack 4
.end method

.method final b()V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield android/support/design/widget/Snackbar/d Landroid/view/ViewGroup;
aload 0
getfield android/support/design/widget/Snackbar/e Landroid/support/design/widget/Snackbar$SnackbarLayout;
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
invokestatic android/support/design/widget/bh/a()Landroid/support/design/widget/bh;
astore 2
aload 0
getfield android/support/design/widget/Snackbar/n Landroid/support/design/widget/bj;
astore 3
aload 2
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 2
aload 3
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L1
aload 2
aconst_null
putfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
aload 2
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
ifnull L1
aload 2
invokevirtual android/support/design/widget/bh/b()V
L1:
aload 1
monitorexit
L3:
return
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 4
.limit stack 2
.end method
