.bytecode 50.0
.class public synchronized android/support/design/widget/AppBarLayout$ScrollingViewBehavior
.super android/support/design/widget/df

.field private 'a' I

.method public <init>()V
aload 0
invokespecial android/support/design/widget/df/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/df/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 1
aload 2
getstatic android/support/design/n/ScrollingViewBehavior_Params [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/design/n/ScrollingViewBehavior_Params_behavior_overlapTop I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/AppBarLayout$ScrollingViewBehavior/a I
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 3
.limit stack 4
.end method

.method private a()I
aload 0
getfield android/support/design/widget/AppBarLayout$ScrollingViewBehavior/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/List;)Landroid/support/design/widget/AppBarLayout;
aload 0
invokeinterface java/util/List/size()I 0
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 3
aload 3
instanceof android/support/design/widget/AppBarLayout
ifeq L2
aload 3
checkcast android/support/design/widget/AppBarLayout
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method private c(I)V
aload 0
iload 1
putfield android/support/design/widget/AppBarLayout$ScrollingViewBehavior/a I
return
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(I)Z
aload 0
iload 1
invokespecial android/support/design/widget/df/a(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
aload 0
aload 1
aload 2
iload 3
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)Z
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
getfield android/view/ViewGroup$LayoutParams/height I
istore 7
iload 7
iconst_m1
if_icmpeq L0
iload 7
bipush -2
if_icmpne L1
L0:
aload 1
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;)Ljava/util/List;
astore 10
aload 10
invokeinterface java/util/List/isEmpty()Z 0
ifeq L2
iconst_0
ireturn
L2:
aload 10
invokeinterface java/util/List/size()I 0
istore 8
iconst_0
istore 6
L3:
iload 6
iload 8
if_icmpge L4
aload 10
iload 6
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 11
aload 11
instanceof android/support/design/widget/AppBarLayout
ifeq L5
aload 11
checkcast android/support/design/widget/AppBarLayout
astore 10
L6:
aload 10
ifnull L1
aload 10
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L1
aload 10
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L7
aload 2
iconst_1
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Z)V
L7:
iload 5
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 6
iload 6
istore 5
iload 6
ifne L8
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
istore 5
L8:
aload 10
invokevirtual android/support/design/widget/AppBarLayout/getMeasuredHeight()I
istore 8
aload 10
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
istore 9
iload 7
iconst_m1
if_icmpne L9
ldc_w 1073741824
istore 6
L10:
aload 1
aload 2
iload 3
iload 4
iload 9
iload 5
iload 8
isub
iadd
iload 6
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;III)V
iconst_1
ireturn
L5:
iload 6
iconst_1
iadd
istore 6
goto L3
L4:
aconst_null
astore 10
goto L6
L9:
ldc_w -2147483648
istore 6
goto L10
L1:
iconst_0
ireturn
.limit locals 12
.limit stack 7
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
aload 3
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 10
aload 10
instanceof android/support/design/widget/AppBarLayout$Behavior
ifeq L0
aload 10
checkcast android/support/design/widget/AppBarLayout$Behavior
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
istore 4
aload 3
invokevirtual android/view/View/getHeight()I
istore 5
aload 0
getfield android/support/design/widget/AppBarLayout$ScrollingViewBehavior/a I
istore 6
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
istore 7
aload 2
invokevirtual android/view/View/getHeight()I
istore 8
aload 0
getfield android/support/design/widget/AppBarLayout$ScrollingViewBehavior/a I
ifeq L1
aload 3
instanceof android/support/design/widget/AppBarLayout
ifeq L1
aload 3
checkcast android/support/design/widget/AppBarLayout
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
istore 9
aload 0
iload 5
iload 6
isub
iload 7
iload 8
isub
iload 4
invokestatic java/lang/Math/abs(I)I
i2f
iload 9
i2f
fdiv
invokestatic android/support/design/widget/a/a(IIF)I
invokespecial android/support/design/widget/df/b(I)Z
pop
L0:
iconst_0
ireturn
L1:
aload 0
iload 4
aload 3
invokevirtual android/view/View/getHeight()I
aload 0
getfield android/support/design/widget/AppBarLayout$ScrollingViewBehavior/a I
isub
iadd
invokespecial android/support/design/widget/df/b(I)Z
pop
goto L0
.limit locals 11
.limit stack 5
.end method

.method public final volatile synthetic b()I
aload 0
invokespecial android/support/design/widget/df/b()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic b(I)Z
aload 0
iload 1
invokespecial android/support/design/widget/df/b(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/view/View;)Z
aload 1
instanceof android/support/design/widget/AppBarLayout
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final volatile synthetic c()I
aload 0
invokespecial android/support/design/widget/df/c()I
ireturn
.limit locals 1
.limit stack 1
.end method
