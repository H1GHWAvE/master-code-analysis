.bytecode 50.0
.class final synchronized android/support/design/widget/ar
.super android/support/v7/b/a/b

.field static final 'a' D

.field static final 'b' F = 1.5F


.field static final 'c' F = 0.25F


.field static final 'd' F = 0.5F


.field static final 'e' F = 1.0F


.field final 'f' Landroid/graphics/Paint;

.field final 'g' Landroid/graphics/Paint;

.field final 'h' Landroid/graphics/RectF;

.field 'i' F

.field 'j' Landroid/graphics/Path;

.field 'k' F

.field 'l' F

.field 'm' F

.field 'n' F

.field 'o' Z

.field private 'q' Z

.field private final 'r' I

.field private final 's' I

.field private final 't' I

.field private 'u' Z

.method static <clinit>()V
ldc2_w 45.0D
invokestatic java/lang/Math/toRadians(D)D
invokestatic java/lang/Math/cos(D)D
putstatic android/support/design/widget/ar/a D
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;FFF)V
aload 0
aload 2
invokespecial android/support/v7/b/a/b/<init>(Landroid/graphics/drawable/Drawable;)V
aload 0
iconst_1
putfield android/support/design/widget/ar/q Z
aload 0
iconst_1
putfield android/support/design/widget/ar/o Z
aload 0
iconst_0
putfield android/support/design/widget/ar/u Z
aload 0
aload 1
getstatic android/support/design/f/design_fab_shadow_start_color I
invokevirtual android/content/res/Resources/getColor(I)I
putfield android/support/design/widget/ar/r I
aload 0
aload 1
getstatic android/support/design/f/design_fab_shadow_mid_color I
invokevirtual android/content/res/Resources/getColor(I)I
putfield android/support/design/widget/ar/s I
aload 0
aload 1
getstatic android/support/design/f/design_fab_shadow_end_color I
invokevirtual android/content/res/Resources/getColor(I)I
putfield android/support/design/widget/ar/t I
aload 0
new android/graphics/Paint
dup
iconst_5
invokespecial android/graphics/Paint/<init>(I)V
putfield android/support/design/widget/ar/f Landroid/graphics/Paint;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
getstatic android/graphics/Paint$Style/FILL Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 0
fload 3
invokestatic java/lang/Math/round(F)I
i2f
putfield android/support/design/widget/ar/i F
aload 0
new android/graphics/RectF
dup
invokespecial android/graphics/RectF/<init>()V
putfield android/support/design/widget/ar/h Landroid/graphics/RectF;
aload 0
new android/graphics/Paint
dup
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokespecial android/graphics/Paint/<init>(Landroid/graphics/Paint;)V
putfield android/support/design/widget/ar/g Landroid/graphics/Paint;
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
iconst_0
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
aload 0
fload 4
fload 5
invokevirtual android/support/design/widget/ar/a(FF)V
return
.limit locals 6
.limit stack 4
.end method

.method private static a(FFZ)F
iload 2
ifeq L0
ldc_w 1.5F
fload 0
fmul
f2d
dconst_1
getstatic android/support/design/widget/ar/a D
dsub
fload 1
f2d
dmul
dadd
d2f
freturn
L0:
ldc_w 1.5F
fload 0
fmul
freturn
.limit locals 3
.limit stack 6
.end method

.method private static a(F)I
fload 0
invokestatic java/lang/Math/round(F)I
istore 2
iload 2
istore 1
iload 2
iconst_2
irem
iconst_1
if_icmpne L0
iload 2
iconst_1
isub
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private a()V
aload 0
iconst_0
putfield android/support/design/widget/ar/o Z
aload 0
invokevirtual android/support/design/widget/ar/invalidateSelf()V
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/m F
fsub
fstore 2
aload 0
getfield android/support/design/widget/ar/i F
fstore 3
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/width()F
fconst_2
fload 3
fmul
fsub
fconst_0
fcmpl
ifle L0
iconst_1
istore 10
L1:
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/height()F
fconst_2
fload 3
fmul
fsub
fconst_0
fcmpl
ifle L2
iconst_1
istore 11
L3:
aload 0
getfield android/support/design/widget/ar/n F
fstore 7
aload 0
getfield android/support/design/widget/ar/n F
fstore 8
aload 0
getfield android/support/design/widget/ar/n F
fstore 4
aload 0
getfield android/support/design/widget/ar/n F
fstore 9
aload 0
getfield android/support/design/widget/ar/n F
fstore 5
aload 0
getfield android/support/design/widget/ar/n F
fstore 6
fload 3
fload 4
fload 9
ldc_w 0.5F
fmul
fsub
fload 3
fadd
fdiv
fstore 4
fload 3
fload 7
fload 8
ldc_w 0.25F
fmul
fsub
fload 3
fadd
fdiv
fstore 7
fload 3
fload 3
fload 5
fload 6
fconst_1
fmul
fsub
fadd
fdiv
fstore 5
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 12
aload 1
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/left F
fload 3
fadd
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/top F
fload 3
fadd
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
fload 4
fload 7
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
iload 10
ifeq L4
aload 1
fconst_1
fload 4
fdiv
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
fconst_0
fload 2
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/width()F
fconst_2
fload 3
fmul
fsub
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L4:
aload 1
iload 12
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 12
aload 1
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/right F
fload 3
fsub
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/bottom F
fload 3
fsub
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
fload 4
fload 5
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
ldc_w 180.0F
invokevirtual android/graphics/Canvas/rotate(F)V
aload 1
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
iload 10
ifeq L5
aload 1
fconst_1
fload 4
fdiv
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/width()F
fstore 6
aload 0
getfield android/support/design/widget/ar/i F
fneg
fstore 8
aload 1
fconst_0
fload 2
fload 6
fconst_2
fload 3
fmul
fsub
aload 0
getfield android/support/design/widget/ar/m F
fload 8
fadd
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L5:
aload 1
iload 12
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 10
aload 1
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/left F
fload 3
fadd
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/bottom F
fload 3
fsub
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
fload 4
fload 5
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
ldc_w 270.0F
invokevirtual android/graphics/Canvas/rotate(F)V
aload 1
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
iload 11
ifeq L6
aload 1
fconst_1
fload 5
fdiv
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
fconst_0
fload 2
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/height()F
fconst_2
fload 3
fmul
fsub
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L6:
aload 1
iload 10
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 10
aload 1
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/right F
fload 3
fsub
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/top F
fload 3
fadd
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
fload 4
fload 7
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
ldc_w 90.0F
invokevirtual android/graphics/Canvas/rotate(F)V
aload 1
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
iload 11
ifeq L7
aload 1
fconst_1
fload 7
fdiv
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
fconst_0
fload 2
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/height()F
fconst_2
fload 3
fmul
fsub
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L7:
aload 1
iload 10
invokevirtual android/graphics/Canvas/restoreToCount(I)V
return
L0:
iconst_0
istore 10
goto L1
L2:
iconst_0
istore 11
goto L3
.limit locals 13
.limit stack 6
.end method

.method private a(Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/widget/ar/l F
ldc_w 1.5F
fmul
fstore 2
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
aload 1
getfield android/graphics/Rect/left I
i2f
aload 0
getfield android/support/design/widget/ar/l F
fadd
aload 1
getfield android/graphics/Rect/top I
i2f
fload 2
fadd
aload 1
getfield android/graphics/Rect/right I
i2f
aload 0
getfield android/support/design/widget/ar/l F
fsub
aload 1
getfield android/graphics/Rect/bottom I
i2f
fload 2
fsub
invokevirtual android/graphics/RectF/set(FFFF)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/left F
f2i
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/top F
f2i
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/right F
f2i
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/bottom F
f2i
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
new android/graphics/RectF
dup
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/i F
aload 0
getfield android/support/design/widget/ar/i F
invokespecial android/graphics/RectF/<init>(FFFF)V
astore 1
new android/graphics/RectF
dup
aload 1
invokespecial android/graphics/RectF/<init>(Landroid/graphics/RectF;)V
astore 8
aload 8
aload 0
getfield android/support/design/widget/ar/m F
fneg
aload 0
getfield android/support/design/widget/ar/m F
fneg
invokevirtual android/graphics/RectF/inset(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
ifnonnull L0
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield android/support/design/widget/ar/j Landroid/graphics/Path;
L1:
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
getstatic android/graphics/Path$FillType/EVEN_ODD Landroid/graphics/Path$FillType;
invokevirtual android/graphics/Path/setFillType(Landroid/graphics/Path$FillType;)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/i F
fneg
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/m F
fneg
fconst_0
invokevirtual android/graphics/Path/rLineTo(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 8
ldc_w 180.0F
ldc_w 90.0F
iconst_0
invokevirtual android/graphics/Path/arcTo(Landroid/graphics/RectF;FFZ)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 1
ldc_w 270.0F
ldc_w -90.0F
iconst_0
invokevirtual android/graphics/Path/arcTo(Landroid/graphics/RectF;FFZ)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
invokevirtual android/graphics/Path/close()V
aload 8
getfield android/graphics/RectF/top F
fneg
fstore 2
fload 2
fconst_0
fcmpl
ifle L2
aload 0
getfield android/support/design/widget/ar/i F
fload 2
fdiv
fstore 3
fconst_1
fload 3
fsub
fconst_2
fdiv
fstore 4
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
astore 9
aload 0
getfield android/support/design/widget/ar/r I
istore 5
aload 0
getfield android/support/design/widget/ar/s I
istore 6
aload 0
getfield android/support/design/widget/ar/t I
istore 7
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
astore 10
aload 9
new android/graphics/RadialGradient
dup
fconst_0
fconst_0
fload 2
iconst_4
newarray int
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iload 5
iastore
dup
iconst_2
iload 6
iastore
dup
iconst_3
iload 7
iastore
iconst_4
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
fload 3
fastore
dup
iconst_2
fload 3
fload 4
fadd
fastore
dup
iconst_3
fconst_1
fastore
aload 10
invokespecial android/graphics/RadialGradient/<init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
L2:
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
astore 9
aload 1
getfield android/graphics/RectF/top F
fstore 2
aload 8
getfield android/graphics/RectF/top F
fstore 3
aload 0
getfield android/support/design/widget/ar/r I
istore 5
aload 0
getfield android/support/design/widget/ar/s I
istore 6
aload 0
getfield android/support/design/widget/ar/t I
istore 7
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
astore 1
aload 9
new android/graphics/LinearGradient
dup
fconst_0
fload 2
fconst_0
fload 3
iconst_3
newarray int
dup
iconst_0
iload 5
iastore
dup
iconst_1
iload 6
iastore
dup
iconst_2
iload 7
iastore
iconst_3
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
ldc_w 0.5F
fastore
dup
iconst_2
fconst_1
fastore
aload 1
invokespecial android/graphics/LinearGradient/<init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
iconst_0
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
return
L0:
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
invokevirtual android/graphics/Path/reset()V
goto L1
.limit locals 11
.limit stack 12
.end method

.method private static b(FFZ)F
fload 0
fstore 3
iload 2
ifeq L0
fload 0
f2d
dconst_1
getstatic android/support/design/widget/ar/a D
dsub
fload 1
f2d
dmul
dadd
d2f
fstore 3
L0:
fload 3
freturn
.limit locals 4
.limit stack 6
.end method

.method private b()V
new android/graphics/RectF
dup
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/i F
aload 0
getfield android/support/design/widget/ar/i F
invokespecial android/graphics/RectF/<init>(FFFF)V
astore 7
new android/graphics/RectF
dup
aload 7
invokespecial android/graphics/RectF/<init>(Landroid/graphics/RectF;)V
astore 8
aload 8
aload 0
getfield android/support/design/widget/ar/m F
fneg
aload 0
getfield android/support/design/widget/ar/m F
fneg
invokevirtual android/graphics/RectF/inset(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
ifnonnull L0
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield android/support/design/widget/ar/j Landroid/graphics/Path;
L1:
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
getstatic android/graphics/Path$FillType/EVEN_ODD Landroid/graphics/Path$FillType;
invokevirtual android/graphics/Path/setFillType(Landroid/graphics/Path$FillType;)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/i F
fneg
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/m F
fneg
fconst_0
invokevirtual android/graphics/Path/rLineTo(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 8
ldc_w 180.0F
ldc_w 90.0F
iconst_0
invokevirtual android/graphics/Path/arcTo(Landroid/graphics/RectF;FFZ)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 7
ldc_w 270.0F
ldc_w -90.0F
iconst_0
invokevirtual android/graphics/Path/arcTo(Landroid/graphics/RectF;FFZ)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
invokevirtual android/graphics/Path/close()V
aload 8
getfield android/graphics/RectF/top F
fneg
fstore 1
fload 1
fconst_0
fcmpl
ifle L2
aload 0
getfield android/support/design/widget/ar/i F
fload 1
fdiv
fstore 2
fconst_1
fload 2
fsub
fconst_2
fdiv
fstore 3
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
astore 9
aload 0
getfield android/support/design/widget/ar/r I
istore 4
aload 0
getfield android/support/design/widget/ar/s I
istore 5
aload 0
getfield android/support/design/widget/ar/t I
istore 6
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
astore 10
aload 9
new android/graphics/RadialGradient
dup
fconst_0
fconst_0
fload 1
iconst_4
newarray int
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iload 4
iastore
dup
iconst_2
iload 5
iastore
dup
iconst_3
iload 6
iastore
iconst_4
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
fload 2
fastore
dup
iconst_2
fload 2
fload 3
fadd
fastore
dup
iconst_3
fconst_1
fastore
aload 10
invokespecial android/graphics/RadialGradient/<init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
L2:
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
astore 9
aload 7
getfield android/graphics/RectF/top F
fstore 1
aload 8
getfield android/graphics/RectF/top F
fstore 2
aload 0
getfield android/support/design/widget/ar/r I
istore 4
aload 0
getfield android/support/design/widget/ar/s I
istore 5
aload 0
getfield android/support/design/widget/ar/t I
istore 6
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
astore 7
aload 9
new android/graphics/LinearGradient
dup
fconst_0
fload 1
fconst_0
fload 2
iconst_3
newarray int
dup
iconst_0
iload 4
iastore
dup
iconst_1
iload 5
iastore
dup
iconst_2
iload 6
iastore
iconst_3
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
ldc_w 0.5F
fastore
dup
iconst_2
fconst_1
fastore
aload 7
invokespecial android/graphics/LinearGradient/<init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
iconst_0
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
return
L0:
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
invokevirtual android/graphics/Path/reset()V
goto L1
.limit locals 11
.limit stack 12
.end method

.method private b(F)V
fload 1
invokestatic java/lang/Math/round(F)I
i2f
fstore 1
aload 0
getfield android/support/design/widget/ar/i F
fload 1
fcmpl
ifne L0
return
L0:
aload 0
fload 1
putfield android/support/design/widget/ar/i F
aload 0
iconst_1
putfield android/support/design/widget/ar/q Z
aload 0
invokevirtual android/support/design/widget/ar/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method private c()F
aload 0
getfield android/support/design/widget/ar/i F
freturn
.limit locals 1
.limit stack 1
.end method

.method private c(F)V
aload 0
fload 1
aload 0
getfield android/support/design/widget/ar/l F
invokevirtual android/support/design/widget/ar/a(FF)V
return
.limit locals 2
.limit stack 3
.end method

.method private d()F
aload 0
getfield android/support/design/widget/ar/n F
freturn
.limit locals 1
.limit stack 1
.end method

.method private d(F)V
aload 0
aload 0
getfield android/support/design/widget/ar/n F
fload 1
invokevirtual android/support/design/widget/ar/a(FF)V
return
.limit locals 2
.limit stack 3
.end method

.method private e()F
aload 0
getfield android/support/design/widget/ar/l F
freturn
.limit locals 1
.limit stack 1
.end method

.method private f()F
aload 0
getfield android/support/design/widget/ar/l F
aload 0
getfield android/support/design/widget/ar/i F
aload 0
getfield android/support/design/widget/ar/l F
fconst_2
fdiv
fadd
invokestatic java/lang/Math/max(FF)F
fconst_2
fmul
aload 0
getfield android/support/design/widget/ar/l F
fconst_2
fmul
fadd
freturn
.limit locals 1
.limit stack 4
.end method

.method private g()F
aload 0
getfield android/support/design/widget/ar/l F
aload 0
getfield android/support/design/widget/ar/i F
aload 0
getfield android/support/design/widget/ar/l F
ldc_w 1.5F
fmul
fconst_2
fdiv
fadd
invokestatic java/lang/Math/max(FF)F
fconst_2
fmul
aload 0
getfield android/support/design/widget/ar/l F
ldc_w 1.5F
fmul
fconst_2
fmul
fadd
freturn
.limit locals 1
.limit stack 4
.end method

.method final a(FF)V
fload 1
fconst_0
fcmpg
iflt L0
fload 2
fconst_0
fcmpg
ifge L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "invalid shadow size"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
fload 1
invokestatic android/support/design/widget/ar/a(F)I
i2f
fstore 3
fload 2
invokestatic android/support/design/widget/ar/a(F)I
i2f
fstore 2
fload 3
fstore 1
fload 3
fload 2
fcmpl
ifle L2
aload 0
getfield android/support/design/widget/ar/u Z
ifne L3
aload 0
iconst_1
putfield android/support/design/widget/ar/u Z
L3:
fload 2
fstore 1
L2:
aload 0
getfield android/support/design/widget/ar/n F
fload 1
fcmpl
ifne L4
aload 0
getfield android/support/design/widget/ar/l F
fload 2
fcmpl
ifne L4
return
L4:
aload 0
fload 1
putfield android/support/design/widget/ar/n F
aload 0
fload 2
putfield android/support/design/widget/ar/l F
aload 0
fload 1
ldc_w 1.5F
fmul
invokestatic java/lang/Math/round(F)I
i2f
putfield android/support/design/widget/ar/m F
aload 0
fload 2
putfield android/support/design/widget/ar/k F
aload 0
iconst_1
putfield android/support/design/widget/ar/q Z
aload 0
invokevirtual android/support/design/widget/ar/invalidateSelf()V
return
.limit locals 4
.limit stack 3
.end method

.method public final draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/widget/ar/q Z
ifeq L0
aload 0
invokevirtual android/support/design/widget/ar/getBounds()Landroid/graphics/Rect;
astore 13
aload 0
getfield android/support/design/widget/ar/l F
ldc_w 1.5F
fmul
fstore 2
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
aload 13
getfield android/graphics/Rect/left I
i2f
aload 0
getfield android/support/design/widget/ar/l F
fadd
aload 13
getfield android/graphics/Rect/top I
i2f
fload 2
fadd
aload 13
getfield android/graphics/Rect/right I
i2f
aload 0
getfield android/support/design/widget/ar/l F
fsub
aload 13
getfield android/graphics/Rect/bottom I
i2f
fload 2
fsub
invokevirtual android/graphics/RectF/set(FFFF)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/left F
f2i
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/top F
f2i
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/right F
f2i
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/bottom F
f2i
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
new android/graphics/RectF
dup
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/i F
aload 0
getfield android/support/design/widget/ar/i F
invokespecial android/graphics/RectF/<init>(FFFF)V
astore 13
new android/graphics/RectF
dup
aload 13
invokespecial android/graphics/RectF/<init>(Landroid/graphics/RectF;)V
astore 14
aload 14
aload 0
getfield android/support/design/widget/ar/m F
fneg
aload 0
getfield android/support/design/widget/ar/m F
fneg
invokevirtual android/graphics/RectF/inset(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
ifnonnull L1
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield android/support/design/widget/ar/j Landroid/graphics/Path;
L2:
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
getstatic android/graphics/Path$FillType/EVEN_ODD Landroid/graphics/Path$FillType;
invokevirtual android/graphics/Path/setFillType(Landroid/graphics/Path$FillType;)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/i F
fneg
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/m F
fneg
fconst_0
invokevirtual android/graphics/Path/rLineTo(FF)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 14
ldc_w 180.0F
ldc_w 90.0F
iconst_0
invokevirtual android/graphics/Path/arcTo(Landroid/graphics/RectF;FFZ)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 13
ldc_w 270.0F
ldc_w -90.0F
iconst_0
invokevirtual android/graphics/Path/arcTo(Landroid/graphics/RectF;FFZ)V
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
invokevirtual android/graphics/Path/close()V
aload 14
getfield android/graphics/RectF/top F
fneg
fstore 2
fload 2
fconst_0
fcmpl
ifle L3
aload 0
getfield android/support/design/widget/ar/i F
fload 2
fdiv
fstore 3
fconst_1
fload 3
fsub
fconst_2
fdiv
fstore 4
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
astore 15
aload 0
getfield android/support/design/widget/ar/r I
istore 10
aload 0
getfield android/support/design/widget/ar/s I
istore 11
aload 0
getfield android/support/design/widget/ar/t I
istore 12
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
astore 16
aload 15
new android/graphics/RadialGradient
dup
fconst_0
fconst_0
fload 2
iconst_4
newarray int
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iload 10
iastore
dup
iconst_2
iload 11
iastore
dup
iconst_3
iload 12
iastore
iconst_4
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
fload 3
fastore
dup
iconst_2
fload 3
fload 4
fadd
fastore
dup
iconst_3
fconst_1
fastore
aload 16
invokespecial android/graphics/RadialGradient/<init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
L3:
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
astore 15
aload 13
getfield android/graphics/RectF/top F
fstore 2
aload 14
getfield android/graphics/RectF/top F
fstore 3
aload 0
getfield android/support/design/widget/ar/r I
istore 10
aload 0
getfield android/support/design/widget/ar/s I
istore 11
aload 0
getfield android/support/design/widget/ar/t I
istore 12
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
astore 13
aload 15
new android/graphics/LinearGradient
dup
fconst_0
fload 2
fconst_0
fload 3
iconst_3
newarray int
dup
iconst_0
iload 10
iastore
dup
iconst_1
iload 11
iastore
dup
iconst_2
iload 12
iastore
iconst_3
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
ldc_w 0.5F
fastore
dup
iconst_2
fconst_1
fastore
aload 13
invokespecial android/graphics/LinearGradient/<init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
iconst_0
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
aload 0
iconst_0
putfield android/support/design/widget/ar/q Z
L0:
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/m F
fsub
fstore 2
aload 0
getfield android/support/design/widget/ar/i F
fstore 3
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/width()F
fconst_2
fload 3
fmul
fsub
fconst_0
fcmpl
ifle L4
iconst_1
istore 10
L5:
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/height()F
fconst_2
fload 3
fmul
fsub
fconst_0
fcmpl
ifle L6
iconst_1
istore 11
L7:
aload 0
getfield android/support/design/widget/ar/n F
fstore 7
aload 0
getfield android/support/design/widget/ar/n F
fstore 8
aload 0
getfield android/support/design/widget/ar/n F
fstore 4
aload 0
getfield android/support/design/widget/ar/n F
fstore 9
aload 0
getfield android/support/design/widget/ar/n F
fstore 5
aload 0
getfield android/support/design/widget/ar/n F
fstore 6
fload 3
fload 4
fload 9
ldc_w 0.5F
fmul
fsub
fload 3
fadd
fdiv
fstore 4
fload 3
fload 7
fload 8
ldc_w 0.25F
fmul
fsub
fload 3
fadd
fdiv
fstore 7
fload 3
fload 3
fload 5
fload 6
fconst_1
fmul
fsub
fadd
fdiv
fstore 5
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 12
aload 1
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/left F
fload 3
fadd
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/top F
fload 3
fadd
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
fload 4
fload 7
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
iload 10
ifeq L8
aload 1
fconst_1
fload 4
fdiv
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
fconst_0
fload 2
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/width()F
fconst_2
fload 3
fmul
fsub
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L8:
aload 1
iload 12
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 12
aload 1
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/right F
fload 3
fsub
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/bottom F
fload 3
fsub
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
fload 4
fload 5
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
ldc_w 180.0F
invokevirtual android/graphics/Canvas/rotate(F)V
aload 1
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
iload 10
ifeq L9
aload 1
fconst_1
fload 4
fdiv
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/width()F
fstore 6
aload 0
getfield android/support/design/widget/ar/i F
fneg
fstore 8
aload 1
fconst_0
fload 2
fload 6
fconst_2
fload 3
fmul
fsub
aload 0
getfield android/support/design/widget/ar/m F
fload 8
fadd
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L9:
aload 1
iload 12
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 10
aload 1
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/left F
fload 3
fadd
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/bottom F
fload 3
fsub
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
fload 4
fload 5
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
ldc_w 270.0F
invokevirtual android/graphics/Canvas/rotate(F)V
aload 1
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
iload 11
ifeq L10
aload 1
fconst_1
fload 5
fdiv
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
fconst_0
fload 2
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/height()F
fconst_2
fload 3
fmul
fsub
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L10:
aload 1
iload 10
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 10
aload 1
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/right F
fload 3
fsub
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
getfield android/graphics/RectF/top F
fload 3
fadd
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
fload 4
fload 7
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
ldc_w 90.0F
invokevirtual android/graphics/Canvas/rotate(F)V
aload 1
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
iload 11
ifeq L11
aload 1
fconst_1
fload 7
fdiv
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
aload 1
fconst_0
fload 2
aload 0
getfield android/support/design/widget/ar/h Landroid/graphics/RectF;
invokevirtual android/graphics/RectF/height()F
fconst_2
fload 3
fmul
fsub
aload 0
getfield android/support/design/widget/ar/i F
fneg
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L11:
aload 1
iload 10
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 0
aload 1
invokespecial android/support/v7/b/a/b/draw(Landroid/graphics/Canvas;)V
return
L1:
aload 0
getfield android/support/design/widget/ar/j Landroid/graphics/Path;
invokevirtual android/graphics/Path/reset()V
goto L2
L4:
iconst_0
istore 10
goto L5
L6:
iconst_0
istore 11
goto L7
.limit locals 17
.limit stack 12
.end method

.method public final getOpacity()I
bipush -3
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
aload 0
getfield android/support/design/widget/ar/l F
fstore 6
aload 0
getfield android/support/design/widget/ar/i F
fstore 7
aload 0
getfield android/support/design/widget/ar/o Z
ifeq L0
fload 6
ldc_w 1.5F
fmul
f2d
dstore 2
getstatic android/support/design/widget/ar/a D
dstore 4
fload 7
f2d
dconst_1
dload 4
dsub
dmul
dload 2
dadd
d2f
fstore 6
L1:
fload 6
f2d
invokestatic java/lang/Math/ceil(D)D
d2i
istore 9
aload 0
getfield android/support/design/widget/ar/l F
fstore 7
aload 0
getfield android/support/design/widget/ar/i F
fstore 8
fload 7
fstore 6
aload 0
getfield android/support/design/widget/ar/o Z
ifeq L2
fload 7
f2d
dstore 2
getstatic android/support/design/widget/ar/a D
dstore 4
fload 8
f2d
dconst_1
dload 4
dsub
dmul
dload 2
dadd
d2f
fstore 6
L2:
fload 6
f2d
invokestatic java/lang/Math/ceil(D)D
d2i
istore 10
aload 1
iload 10
iload 9
iload 10
iload 9
invokevirtual android/graphics/Rect/set(IIII)V
iconst_1
ireturn
L0:
fload 6
ldc_w 1.5F
fmul
fstore 6
goto L1
.limit locals 11
.limit stack 6
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
aload 0
iconst_1
putfield android/support/design/widget/ar/q Z
return
.limit locals 2
.limit stack 2
.end method

.method public final setAlpha(I)V
aload 0
iload 1
invokespecial android/support/v7/b/a/b/setAlpha(I)V
aload 0
getfield android/support/design/widget/ar/f Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setAlpha(I)V
aload 0
getfield android/support/design/widget/ar/g Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setAlpha(I)V
return
.limit locals 2
.limit stack 2
.end method
