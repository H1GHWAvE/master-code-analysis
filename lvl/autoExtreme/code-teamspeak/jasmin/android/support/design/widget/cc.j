.bytecode 50.0
.class final synchronized android/support/design/widget/cc
.super android/widget/LinearLayout
.implements android/view/View$OnLongClickListener

.field final 'a' Landroid/support/design/widget/bz;

.field final synthetic 'b' Landroid/support/design/widget/br;

.field private 'c' Landroid/widget/TextView;

.field private 'd' Landroid/widget/ImageView;

.field private 'e' Landroid/view/View;

.field private 'f' Landroid/widget/TextView;

.field private 'g' Landroid/widget/ImageView;

.method public <init>(Landroid/support/design/widget/br;Landroid/content/Context;Landroid/support/design/widget/bz;)V
aload 0
aload 1
putfield android/support/design/widget/cc/b Landroid/support/design/widget/br;
aload 0
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
aload 0
aload 3
putfield android/support/design/widget/cc/a Landroid/support/design/widget/bz;
aload 1
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;)I
ifeq L0
aload 0
aload 2
aload 1
invokestatic android/support/design/widget/br/a(Landroid/support/design/widget/br;)I
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/widget/cc/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 0
aload 1
invokestatic android/support/design/widget/br/b(Landroid/support/design/widget/br;)I
aload 1
invokestatic android/support/design/widget/br/c(Landroid/support/design/widget/br;)I
aload 1
invokestatic android/support/design/widget/br/d(Landroid/support/design/widget/br;)I
aload 1
invokestatic android/support/design/widget/br/e(Landroid/support/design/widget/br;)I
invokestatic android/support/v4/view/cx/b(Landroid/view/View;IIII)V
aload 0
bipush 17
invokevirtual android/support/design/widget/cc/setGravity(I)V
aload 0
invokevirtual android/support/design/widget/cc/a()V
return
.limit locals 4
.limit stack 5
.end method

.method private a(Landroid/support/design/widget/bz;Landroid/widget/TextView;Landroid/widget/ImageView;)V
aload 1
getfield android/support/design/widget/bz/b Landroid/graphics/drawable/Drawable;
astore 5
aload 1
getfield android/support/design/widget/bz/c Ljava/lang/CharSequence;
astore 6
aload 3
ifnull L0
aload 5
ifnull L1
aload 3
aload 5
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 3
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 0
iconst_0
invokevirtual android/support/design/widget/cc/setVisibility(I)V
L2:
aload 3
aload 1
getfield android/support/design/widget/bz/d Ljava/lang/CharSequence;
invokevirtual android/widget/ImageView/setContentDescription(Ljava/lang/CharSequence;)V
L0:
aload 6
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L3
iconst_1
istore 4
L4:
aload 2
ifnull L5
iload 4
ifeq L6
aload 2
aload 6
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 2
aload 1
getfield android/support/design/widget/bz/d Ljava/lang/CharSequence;
invokevirtual android/widget/TextView/setContentDescription(Ljava/lang/CharSequence;)V
aload 2
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
aload 0
iconst_0
invokevirtual android/support/design/widget/cc/setVisibility(I)V
L5:
iload 4
ifne L7
aload 1
getfield android/support/design/widget/bz/d Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L7
aload 0
aload 0
invokevirtual android/support/design/widget/cc/setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
return
L1:
aload 3
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 3
aconst_null
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
goto L2
L3:
iconst_0
istore 4
goto L4
L6:
aload 2
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
aload 2
aconst_null
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L5
L7:
aload 0
aconst_null
invokevirtual android/support/design/widget/cc/setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
aload 0
iconst_0
invokevirtual android/support/design/widget/cc/setLongClickable(Z)V
return
.limit locals 7
.limit stack 2
.end method

.method private b()Landroid/support/design/widget/bz;
aload 0
getfield android/support/design/widget/cc/a Landroid/support/design/widget/bz;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a()V
aload 0
getfield android/support/design/widget/cc/a Landroid/support/design/widget/bz;
astore 1
aload 1
getfield android/support/design/widget/bz/f Landroid/view/View;
astore 2
aload 2
ifnull L0
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 3
aload 3
aload 0
if_acmpeq L1
aload 3
ifnull L2
aload 3
checkcast android/view/ViewGroup
aload 2
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
L2:
aload 0
aload 2
invokevirtual android/support/design/widget/cc/addView(Landroid/view/View;)V
L1:
aload 0
aload 2
putfield android/support/design/widget/cc/e Landroid/view/View;
aload 0
getfield android/support/design/widget/cc/c Landroid/widget/TextView;
ifnull L3
aload 0
getfield android/support/design/widget/cc/c Landroid/widget/TextView;
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
L3:
aload 0
getfield android/support/design/widget/cc/d Landroid/widget/ImageView;
ifnull L4
aload 0
getfield android/support/design/widget/cc/d Landroid/widget/ImageView;
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 0
getfield android/support/design/widget/cc/d Landroid/widget/ImageView;
aconst_null
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
L4:
aload 0
aload 2
ldc_w 16908308
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield android/support/design/widget/cc/f Landroid/widget/TextView;
aload 0
aload 2
ldc_w 16908294
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/design/widget/cc/g Landroid/widget/ImageView;
L5:
aload 0
getfield android/support/design/widget/cc/e Landroid/view/View;
ifnonnull L6
aload 0
getfield android/support/design/widget/cc/d Landroid/widget/ImageView;
ifnonnull L7
aload 0
invokevirtual android/support/design/widget/cc/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_layout_tab_icon I
aload 0
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/ImageView
astore 2
aload 0
aload 2
iconst_0
invokevirtual android/support/design/widget/cc/addView(Landroid/view/View;I)V
aload 0
aload 2
putfield android/support/design/widget/cc/d Landroid/widget/ImageView;
L7:
aload 0
getfield android/support/design/widget/cc/c Landroid/widget/TextView;
ifnonnull L8
aload 0
invokevirtual android/support/design/widget/cc/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_layout_tab_text I
aload 0
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/TextView
astore 2
aload 0
aload 2
invokevirtual android/support/design/widget/cc/addView(Landroid/view/View;)V
aload 0
aload 2
putfield android/support/design/widget/cc/c Landroid/widget/TextView;
L8:
aload 0
getfield android/support/design/widget/cc/c Landroid/widget/TextView;
aload 0
invokevirtual android/support/design/widget/cc/getContext()Landroid/content/Context;
aload 0
getfield android/support/design/widget/cc/b Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/h(Landroid/support/design/widget/br;)I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 0
getfield android/support/design/widget/cc/b Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/i(Landroid/support/design/widget/br;)Landroid/content/res/ColorStateList;
ifnull L9
aload 0
getfield android/support/design/widget/cc/c Landroid/widget/TextView;
aload 0
getfield android/support/design/widget/cc/b Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/i(Landroid/support/design/widget/br;)Landroid/content/res/ColorStateList;
invokevirtual android/widget/TextView/setTextColor(Landroid/content/res/ColorStateList;)V
L9:
aload 0
aload 1
aload 0
getfield android/support/design/widget/cc/c Landroid/widget/TextView;
aload 0
getfield android/support/design/widget/cc/d Landroid/widget/ImageView;
invokespecial android/support/design/widget/cc/a(Landroid/support/design/widget/bz;Landroid/widget/TextView;Landroid/widget/ImageView;)V
L10:
return
L0:
aload 0
getfield android/support/design/widget/cc/e Landroid/view/View;
ifnull L11
aload 0
aload 0
getfield android/support/design/widget/cc/e Landroid/view/View;
invokevirtual android/support/design/widget/cc/removeView(Landroid/view/View;)V
aload 0
aconst_null
putfield android/support/design/widget/cc/e Landroid/view/View;
L11:
aload 0
aconst_null
putfield android/support/design/widget/cc/f Landroid/widget/TextView;
aload 0
aconst_null
putfield android/support/design/widget/cc/g Landroid/widget/ImageView;
goto L5
L6:
aload 0
getfield android/support/design/widget/cc/f Landroid/widget/TextView;
ifnonnull L12
aload 0
getfield android/support/design/widget/cc/g Landroid/widget/ImageView;
ifnull L10
L12:
aload 0
aload 1
aload 0
getfield android/support/design/widget/cc/f Landroid/widget/TextView;
aload 0
getfield android/support/design/widget/cc/g Landroid/widget/ImageView;
invokespecial android/support/design/widget/cc/a(Landroid/support/design/widget/bz;Landroid/widget/TextView;Landroid/widget/ImageView;)V
return
.limit locals 4
.limit stack 4
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 14
.end annotation
aload 0
aload 1
invokespecial android/widget/LinearLayout/onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
ldc android/support/v7/app/g
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
.annotation invisible Landroid/annotation/TargetApi;
value I = 14
.end annotation
aload 0
aload 1
invokespecial android/widget/LinearLayout/onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
aload 1
ldc android/support/v7/app/g
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityNodeInfo/setClassName(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final onLongClick(Landroid/view/View;)Z
iconst_2
newarray int
astore 1
aload 0
aload 1
invokevirtual android/support/design/widget/cc/getLocationOnScreen([I)V
aload 0
invokevirtual android/support/design/widget/cc/getContext()Landroid/content/Context;
astore 5
aload 0
invokevirtual android/support/design/widget/cc/getWidth()I
istore 2
aload 0
invokevirtual android/support/design/widget/cc/getHeight()I
istore 3
aload 5
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
istore 4
aload 5
aload 0
getfield android/support/design/widget/cc/a Landroid/support/design/widget/bz;
getfield android/support/design/widget/bz/d Ljava/lang/CharSequence;
iconst_0
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
astore 5
aload 5
bipush 49
aload 1
iconst_0
iaload
iload 2
iconst_2
idiv
iadd
iload 4
iconst_2
idiv
isub
iload 3
invokevirtual android/widget/Toast/setGravity(III)V
aload 5
invokevirtual android/widget/Toast/show()V
iconst_1
ireturn
.limit locals 6
.limit stack 5
.end method

.method public final onMeasure(II)V
aload 0
iload 1
iload 2
invokespecial android/widget/LinearLayout/onMeasure(II)V
aload 0
invokevirtual android/support/design/widget/cc/getMeasuredWidth()I
istore 1
iload 1
aload 0
getfield android/support/design/widget/cc/b Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/f(Landroid/support/design/widget/br;)I
if_icmplt L0
iload 1
aload 0
getfield android/support/design/widget/cc/b Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/g(Landroid/support/design/widget/br;)I
if_icmple L1
L0:
aload 0
iload 1
aload 0
getfield android/support/design/widget/cc/b Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/f(Landroid/support/design/widget/br;)I
aload 0
getfield android/support/design/widget/cc/b Landroid/support/design/widget/br;
invokestatic android/support/design/widget/br/g(Landroid/support/design/widget/br;)I
invokestatic android/support/design/widget/an/a(III)I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 2
invokespecial android/widget/LinearLayout/onMeasure(II)V
L1:
return
.limit locals 3
.limit stack 4
.end method

.method public final setSelected(Z)V
aload 0
invokevirtual android/support/design/widget/cc/isSelected()Z
iload 1
if_icmpeq L0
iconst_1
istore 2
L1:
aload 0
iload 1
invokespecial android/widget/LinearLayout/setSelected(Z)V
iload 2
ifeq L2
iload 1
ifeq L2
aload 0
iconst_4
invokevirtual android/support/design/widget/cc/sendAccessibilityEvent(I)V
aload 0
getfield android/support/design/widget/cc/c Landroid/widget/TextView;
ifnull L3
aload 0
getfield android/support/design/widget/cc/c Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setSelected(Z)V
L3:
aload 0
getfield android/support/design/widget/cc/d Landroid/widget/ImageView;
ifnull L2
aload 0
getfield android/support/design/widget/cc/d Landroid/widget/ImageView;
iload 1
invokevirtual android/widget/ImageView/setSelected(Z)V
L2:
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method
