.bytecode 50.0
.class final synchronized android/support/design/widget/bh
.super java/lang/Object

.field private static final 'e' I = 0


.field private static final 'f' I = 1500


.field private static final 'g' I = 2750


.field private static 'h' Landroid/support/design/widget/bh;

.field final 'a' Ljava/lang/Object;

.field final 'b' Landroid/os/Handler;

.field 'c' Landroid/support/design/widget/bk;

.field 'd' Landroid/support/design/widget/bk;

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putfield android/support/design/widget/bh/a Ljava/lang/Object;
aload 0
new android/os/Handler
dup
invokestatic android/os/Looper/getMainLooper()Landroid/os/Looper;
new android/support/design/widget/bi
dup
aload 0
invokespecial android/support/design/widget/bi/<init>(Landroid/support/design/widget/bh;)V
invokespecial android/os/Handler/<init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V
putfield android/support/design/widget/bh/b Landroid/os/Handler;
return
.limit locals 1
.limit stack 7
.end method

.method static a()Landroid/support/design/widget/bh;
getstatic android/support/design/widget/bh/h Landroid/support/design/widget/bh;
ifnonnull L0
new android/support/design/widget/bh
dup
invokespecial android/support/design/widget/bh/<init>()V
putstatic android/support/design/widget/bh/h Landroid/support/design/widget/bh;
L0:
getstatic android/support/design/widget/bh/h Landroid/support/design/widget/bh;
areturn
.limit locals 0
.limit stack 2
.end method

.method private a(ILandroid/support/design/widget/bj;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
aload 0
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
aload 2
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L3
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
iload 1
putfield android/support/design/widget/bk/b I
aload 0
getfield android/support/design/widget/bh/b Landroid/os/Handler;
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
invokevirtual android/os/Handler/removeCallbacksAndMessages(Ljava/lang/Object;)V
aload 0
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
invokevirtual android/support/design/widget/bh/a(Landroid/support/design/widget/bk;)V
aload 3
monitorexit
L1:
return
L3:
aload 0
aload 2
invokevirtual android/support/design/widget/bh/e(Landroid/support/design/widget/bj;)Z
ifeq L8
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
iload 1
putfield android/support/design/widget/bk/b I
L4:
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
ifnull L10
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
iconst_4
invokestatic android/support/design/widget/bh/a(Landroid/support/design/widget/bk;I)Z
ifeq L10
aload 3
monitorexit
L5:
return
L2:
astore 2
L6:
aload 3
monitorexit
L7:
aload 2
athrow
L8:
aload 0
new android/support/design/widget/bk
dup
iload 1
aload 2
invokespecial android/support/design/widget/bk/<init>(ILandroid/support/design/widget/bj;)V
putfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
L9:
goto L4
L10:
aload 0
aconst_null
putfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
aload 0
invokevirtual android/support/design/widget/bh/b()V
aload 3
monitorexit
L11:
return
.limit locals 4
.limit stack 5
.end method

.method private static synthetic a(Landroid/support/design/widget/bh;Landroid/support/design/widget/bk;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
aload 1
if_acmpeq L1
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
aload 1
if_acmpne L3
L1:
aload 1
iconst_2
invokestatic android/support/design/widget/bh/a(Landroid/support/design/widget/bk;I)Z
pop
L3:
aload 2
monitorexit
L4:
return
L2:
astore 0
L5:
aload 2
monitorexit
L6:
aload 0
athrow
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/support/design/widget/bj;I)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
aload 0
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
aload 1
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L4
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
iload 2
invokestatic android/support/design/widget/bh/a(Landroid/support/design/widget/bk;I)Z
pop
L1:
aload 3
monitorexit
L3:
return
L4:
aload 0
aload 1
invokevirtual android/support/design/widget/bh/e(Landroid/support/design/widget/bj;)Z
ifeq L1
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
iload 2
invokestatic android/support/design/widget/bh/a(Landroid/support/design/widget/bk;I)Z
pop
L5:
goto L1
L2:
astore 1
L6:
aload 3
monitorexit
L7:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method static a(Landroid/support/design/widget/bk;I)Z
aload 0
getfield android/support/design/widget/bk/a Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/design/widget/bj
astore 0
aload 0
ifnull L0
aload 0
iload 1
invokeinterface android/support/design/widget/bj/a(I)V 1
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/support/design/widget/bk;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
aload 1
if_acmpeq L1
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
aload 1
if_acmpne L3
L1:
aload 1
iconst_2
invokestatic android/support/design/widget/bh/a(Landroid/support/design/widget/bk;I)Z
pop
L3:
aload 2
monitorexit
L4:
return
L2:
astore 1
L5:
aload 2
monitorexit
L6:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method private f(Landroid/support/design/widget/bj;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
aload 1
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L1
aload 0
aconst_null
putfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
ifnull L1
aload 0
invokevirtual android/support/design/widget/bh/b()V
L1:
aload 2
monitorexit
L3:
return
L2:
astore 1
L4:
aload 2
monitorexit
L5:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/support/design/widget/bj;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
aload 1
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L1
aload 0
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
invokevirtual android/support/design/widget/bh/a(Landroid/support/design/widget/bk;)V
L1:
aload 2
monitorexit
L3:
return
L2:
astore 1
L4:
aload 2
monitorexit
L5:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method final a(Landroid/support/design/widget/bk;)V
aload 1
getfield android/support/design/widget/bk/b I
bipush -2
if_icmpne L0
return
L0:
sipush 2750
istore 2
aload 1
getfield android/support/design/widget/bk/b I
ifle L1
aload 1
getfield android/support/design/widget/bk/b I
istore 2
L2:
aload 0
getfield android/support/design/widget/bh/b Landroid/os/Handler;
aload 1
invokevirtual android/os/Handler/removeCallbacksAndMessages(Ljava/lang/Object;)V
aload 0
getfield android/support/design/widget/bh/b Landroid/os/Handler;
aload 0
getfield android/support/design/widget/bh/b Landroid/os/Handler;
iconst_0
aload 1
invokestatic android/os/Message/obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
iload 2
i2l
invokevirtual android/os/Handler/sendMessageDelayed(Landroid/os/Message;J)Z
pop
return
L1:
aload 1
getfield android/support/design/widget/bk/b I
iconst_m1
if_icmpne L2
sipush 1500
istore 2
goto L2
.limit locals 3
.limit stack 4
.end method

.method final b()V
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
ifnull L0
aload 0
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
putfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
aload 0
aconst_null
putfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
getfield android/support/design/widget/bk/a Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/design/widget/bj
astore 1
aload 1
ifnull L1
aload 1
invokeinterface android/support/design/widget/bj/a()V 0
L0:
return
L1:
aload 0
aconst_null
putfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/support/design/widget/bj;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
aload 1
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L1
aload 0
getfield android/support/design/widget/bh/b Landroid/os/Handler;
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
invokevirtual android/os/Handler/removeCallbacksAndMessages(Ljava/lang/Object;)V
L1:
aload 2
monitorexit
L3:
return
L2:
astore 1
L4:
aload 2
monitorexit
L5:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c(Landroid/support/design/widget/bj;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield android/support/design/widget/bh/a Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
aload 1
invokevirtual android/support/design/widget/bh/d(Landroid/support/design/widget/bj;)Z
ifeq L1
aload 0
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
invokevirtual android/support/design/widget/bh/a(Landroid/support/design/widget/bk;)V
L1:
aload 2
monitorexit
L3:
return
L2:
astore 1
L4:
aload 2
monitorexit
L5:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method final d(Landroid/support/design/widget/bj;)Z
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
ifnull L0
aload 0
getfield android/support/design/widget/bh/c Landroid/support/design/widget/bk;
aload 1
invokevirtual android/support/design/widget/bk/a(Landroid/support/design/widget/bj;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method final e(Landroid/support/design/widget/bj;)Z
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
ifnull L0
aload 0
getfield android/support/design/widget/bh/d Landroid/support/design/widget/bk;
aload 1
invokevirtual android/support/design/widget/bk/a(Landroid/support/design/widget/bj;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method
