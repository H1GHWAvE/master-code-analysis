.bytecode 50.0
.class public synchronized android/support/v4/media/session/ParcelableVolumeInfo
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field public 'a' I

.field public 'b' I

.field public 'c' I

.field public 'd' I

.field public 'e' I

.method static <clinit>()V
new android/support/v4/media/session/bi
dup
invokespecial android/support/v4/media/session/bi/<init>()V
putstatic android/support/v4/media/session/ParcelableVolumeInfo/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(IIIII)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v4/media/session/ParcelableVolumeInfo/a I
aload 0
iload 2
putfield android/support/v4/media/session/ParcelableVolumeInfo/b I
aload 0
iload 3
putfield android/support/v4/media/session/ParcelableVolumeInfo/c I
aload 0
iload 4
putfield android/support/v4/media/session/ParcelableVolumeInfo/d I
aload 0
iload 5
putfield android/support/v4/media/session/ParcelableVolumeInfo/e I
return
.limit locals 6
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcel;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/media/session/ParcelableVolumeInfo/a I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/media/session/ParcelableVolumeInfo/c I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/media/session/ParcelableVolumeInfo/d I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/media/session/ParcelableVolumeInfo/e I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/media/session/ParcelableVolumeInfo/b I
return
.limit locals 2
.limit stack 2
.end method

.method public describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/session/ParcelableVolumeInfo/a I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/media/session/ParcelableVolumeInfo/c I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/media/session/ParcelableVolumeInfo/d I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/media/session/ParcelableVolumeInfo/e I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/media/session/ParcelableVolumeInfo/b I
invokevirtual android/os/Parcel/writeInt(I)V
return
.limit locals 3
.limit stack 2
.end method
