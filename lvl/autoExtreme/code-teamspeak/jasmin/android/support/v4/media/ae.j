.bytecode 50.0
.class public synchronized abstract android/support/v4/media/ae
.super java/lang/Object

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public final 'd' I

.field public final 'e' I

.field public 'f' I

.field public 'g' Landroid/support/v4/media/ag;

.field private 'h' Ljava/lang/Object;

.method private <init>(III)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v4/media/ae/d I
aload 0
iload 2
putfield android/support/v4/media/ae/e I
aload 0
iload 3
putfield android/support/v4/media/ae/f I
return
.limit locals 4
.limit stack 2
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v4/media/ae/f I
aload 0
invokevirtual android/support/v4/media/ae/a()Ljava/lang/Object;
astore 2
aload 2
ifnull L0
aload 2
checkcast android/media/VolumeProvider
iload 1
invokevirtual android/media/VolumeProvider/setCurrentVolume(I)V
L0:
aload 0
getfield android/support/v4/media/ae/g Landroid/support/v4/media/ag;
ifnull L1
aload 0
getfield android/support/v4/media/ae/g Landroid/support/v4/media/ag;
aload 0
invokevirtual android/support/v4/media/ag/a(Landroid/support/v4/media/ae;)V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/support/v4/media/ag;)V
aload 0
aload 1
putfield android/support/v4/media/ae/g Landroid/support/v4/media/ag;
return
.limit locals 2
.limit stack 2
.end method

.method private b()I
aload 0
getfield android/support/v4/media/ae/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
getfield android/support/v4/media/ae/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield android/support/v4/media/ae/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static e()V
return
.limit locals 0
.limit stack 0
.end method

.method private static f()V
return
.limit locals 0
.limit stack 0
.end method

.method public final a()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/ae/h Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 0
getfield android/support/v4/media/ae/h Ljava/lang/Object;
areturn
L1:
aload 0
new android/support/v4/media/aj
dup
aload 0
getfield android/support/v4/media/ae/d I
aload 0
getfield android/support/v4/media/ae/e I
aload 0
getfield android/support/v4/media/ae/f I
new android/support/v4/media/af
dup
aload 0
invokespecial android/support/v4/media/af/<init>(Landroid/support/v4/media/ae;)V
invokespecial android/support/v4/media/aj/<init>(IIILandroid/support/v4/media/ak;)V
putfield android/support/v4/media/ae/h Ljava/lang/Object;
aload 0
getfield android/support/v4/media/ae/h Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 9
.end method
