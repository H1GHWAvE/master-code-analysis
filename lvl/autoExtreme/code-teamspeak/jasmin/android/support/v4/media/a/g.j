.bytecode 50.0
.class public final synchronized android/support/v4/media/a/g
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getStatus()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/MediaRouter$RouteInfo
aload 1
invokevirtual android/media/MediaRouter$RouteInfo/getName(Landroid/content/Context;)Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;I)V
aload 0
checkcast android/media/MediaRouter$RouteInfo
iload 1
invokevirtual android/media/MediaRouter$RouteInfo/requestSetVolume(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/MediaRouter$RouteInfo
aload 1
invokevirtual android/media/MediaRouter$RouteInfo/setTag(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;)I
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getSupportedTypes()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;I)V
aload 0
checkcast android/media/MediaRouter$RouteInfo
iload 1
invokevirtual android/media/MediaRouter$RouteInfo/requestUpdateVolume(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getCategory()Landroid/media/MediaRouter$RouteCategory;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getIconDrawable()Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)I
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getPlaybackType()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Ljava/lang/Object;)I
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getPlaybackStream()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static g(Ljava/lang/Object;)I
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getVolume()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static h(Ljava/lang/Object;)I
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getVolumeMax()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static i(Ljava/lang/Object;)I
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getVolumeHandling()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static j(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getTag()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static k(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/MediaRouter$RouteInfo
invokevirtual android/media/MediaRouter$RouteInfo/getGroup()Landroid/media/MediaRouter$RouteGroup;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static l(Ljava/lang/Object;)Z
aload 0
instanceof android/media/MediaRouter$RouteGroup
ireturn
.limit locals 1
.limit stack 1
.end method
