.bytecode 50.0
.class final synchronized android/support/v4/media/session/c
.super java/lang/Object
.implements android/support/v4/media/session/a

.field private 'a' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/media/session/c/a Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method private static b()Ljava/lang/String;
ldc "android.support.v4.media.session.IMediaControllerCallback"
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a()V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
L0:
aload 1
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
iconst_2
aload 1
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L1:
aload 1
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 2
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 2
athrow
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/os/Bundle;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 2
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 2
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
bipush 7
aload 2
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L5:
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 2
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/support/v4/media/MediaMetadataCompat;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 2
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 2
iconst_0
invokevirtual android/support/v4/media/MediaMetadataCompat/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
iconst_4
aload 2
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L5:
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 2
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 2
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 2
iconst_0
invokevirtual android/support/v4/media/session/ParcelableVolumeInfo/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
bipush 8
aload 2
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L5:
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 2
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 2
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 2
iconst_0
invokevirtual android/support/v4/media/session/PlaybackStateCompat/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
iconst_3
aload 2
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L5:
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 2
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method public final a(Ljava/lang/CharSequence;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 2
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 2
iconst_0
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
bipush 6
aload 2
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L5:
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 2
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 3
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
aload 2
ifnull L6
L3:
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
iconst_1
aload 3
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final a(Ljava/util/List;)V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 2
ldc "android.support.v4.media.session.IMediaControllerCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 2
aload 1
invokevirtual android/os/Parcel/writeTypedList(Ljava/util/List;)V
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
iconst_5
aload 2
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 1
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method public final asBinder()Landroid/os/IBinder;
aload 0
getfield android/support/v4/media/session/c/a Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method
