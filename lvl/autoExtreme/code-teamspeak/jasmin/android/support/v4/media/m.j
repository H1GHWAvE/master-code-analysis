.bytecode 50.0
.class final synchronized android/support/v4/media/m
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;)Landroid/graphics/Bitmap;
aload 0
checkcast android/media/MediaMetadata
aload 1
invokevirtual android/media/MediaMetadata/getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;)Ljava/util/Set;
aload 0
checkcast android/media/MediaMetadata
invokevirtual android/media/MediaMetadata/keySet()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/String;)J
aload 0
checkcast android/media/MediaMetadata
aload 1
invokevirtual android/media/MediaMetadata/getLong(Ljava/lang/String;)J
lreturn
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
aload 0
checkcast android/media/MediaMetadata
aload 1
invokevirtual android/media/MediaMetadata/getRating(Ljava/lang/String;)Landroid/media/Rating;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static d(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/MediaMetadata
aload 1
invokevirtual android/media/MediaMetadata/getText(Ljava/lang/String;)Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method
