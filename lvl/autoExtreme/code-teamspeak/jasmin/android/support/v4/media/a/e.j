.bytecode 50.0
.class public final synchronized android/support/v4/media/a/e
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)Ljava/lang/CharSequence;
aload 0
checkcast android/media/MediaRouter$RouteCategory
aload 1
invokevirtual android/media/MediaRouter$RouteCategory/getName(Landroid/content/Context;)Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;)Ljava/util/List;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
aload 0
checkcast android/media/MediaRouter$RouteCategory
aload 1
invokevirtual android/media/MediaRouter$RouteCategory/getRoutes(Ljava/util/List;)Ljava/util/List;
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;)I
aload 0
checkcast android/media/MediaRouter$RouteCategory
invokevirtual android/media/MediaRouter$RouteCategory/getSupportedTypes()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/lang/Object;)Z
aload 0
checkcast android/media/MediaRouter$RouteCategory
invokevirtual android/media/MediaRouter$RouteCategory/isGroupable()Z
ireturn
.limit locals 1
.limit stack 1
.end method
