.bytecode 50.0
.class public final synchronized android/support/v4/media/session/MediaSessionCompat$QueueItem
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field public static final 'a' I = -1


.field final 'b' Landroid/support/v4/media/MediaDescriptionCompat;

.field final 'c' J

.field 'd' Ljava/lang/Object;

.method static <clinit>()V
new android/support/v4/media/session/ap
dup
invokespecial android/support/v4/media/session/ap/<init>()V
putstatic android/support/v4/media/session/MediaSessionCompat$QueueItem/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/os/Parcel;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
getstatic android/support/v4/media/MediaDescriptionCompat/CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/MediaDescriptionCompat
putfield android/support/v4/media/session/MediaSessionCompat$QueueItem/b Landroid/support/v4/media/MediaDescriptionCompat;
aload 0
aload 1
invokevirtual android/os/Parcel/readLong()J
putfield android/support/v4/media/session/MediaSessionCompat$QueueItem/c J
return
.limit locals 2
.limit stack 3
.end method

.method synthetic <init>(Landroid/os/Parcel;B)V
aload 0
aload 1
invokespecial android/support/v4/media/session/MediaSessionCompat$QueueItem/<init>(Landroid/os/Parcel;)V
return
.limit locals 3
.limit stack 2
.end method

.method private <init>(Landroid/support/v4/media/MediaDescriptionCompat;J)V
aload 0
aconst_null
aload 1
lload 2
invokespecial android/support/v4/media/session/MediaSessionCompat$QueueItem/<init>(Ljava/lang/Object;Landroid/support/v4/media/MediaDescriptionCompat;J)V
return
.limit locals 4
.limit stack 5
.end method

.method private <init>(Ljava/lang/Object;Landroid/support/v4/media/MediaDescriptionCompat;J)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 2
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Description cannot be null."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
lload 3
ldc2_w -1L
lcmp
ifne L1
new java/lang/IllegalArgumentException
dup
ldc "Id cannot be QueueItem.UNKNOWN_ID"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 2
putfield android/support/v4/media/session/MediaSessionCompat$QueueItem/b Landroid/support/v4/media/MediaDescriptionCompat;
aload 0
lload 3
putfield android/support/v4/media/session/MediaSessionCompat$QueueItem/c J
aload 0
aload 1
putfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
return
.limit locals 5
.limit stack 4
.end method

.method private a()Landroid/support/v4/media/MediaDescriptionCompat;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/b Landroid/support/v4/media/MediaDescriptionCompat;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Object;)Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;
new android/support/v4/media/session/MediaSessionCompat$QueueItem
dup
aload 0
aload 0
checkcast android/media/session/MediaSession$QueueItem
invokevirtual android/media/session/MediaSession$QueueItem/getDescription()Landroid/media/MediaDescription;
invokestatic android/support/v4/media/MediaDescriptionCompat/a(Ljava/lang/Object;)Landroid/support/v4/media/MediaDescriptionCompat;
aload 0
checkcast android/media/session/MediaSession$QueueItem
invokevirtual android/media/session/MediaSession$QueueItem/getQueueId()J
invokespecial android/support/v4/media/session/MediaSessionCompat$QueueItem/<init>(Ljava/lang/Object;Landroid/support/v4/media/MediaDescriptionCompat;J)V
areturn
.limit locals 1
.limit stack 6
.end method

.method private b()J
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private c()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
areturn
L1:
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/b Landroid/support/v4/media/MediaDescriptionCompat;
invokevirtual android/support/v4/media/MediaDescriptionCompat/a()Ljava/lang/Object;
astore 3
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/c J
lstore 1
aload 0
new android/media/session/MediaSession$QueueItem
dup
aload 3
checkcast android/media/MediaDescription
lload 1
invokespecial android/media/session/MediaSession$QueueItem/<init>(Landroid/media/MediaDescription;J)V
putfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/d Ljava/lang/Object;
areturn
.limit locals 4
.limit stack 6
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "MediaSession.QueueItem {Description="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/b Landroid/support/v4/media/MediaDescriptionCompat;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", Id="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " }"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/b Landroid/support/v4/media/MediaDescriptionCompat;
aload 1
iload 2
invokevirtual android/support/v4/media/MediaDescriptionCompat/writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/session/MediaSessionCompat$QueueItem/c J
invokevirtual android/os/Parcel/writeLong(J)V
return
.limit locals 3
.limit stack 3
.end method
