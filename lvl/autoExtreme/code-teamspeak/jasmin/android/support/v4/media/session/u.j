.bytecode 50.0
.class final synchronized android/support/v4/media/session/u
.super android/support/v4/media/session/r

.field private 'a' Landroid/support/v4/media/session/d;

.method public <init>(Landroid/support/v4/media/session/d;)V
aload 0
invokespecial android/support/v4/media/session/r/<init>()V
aload 0
aload 1
putfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
return
.limit locals 2
.limit stack 2
.end method

.method public final a()V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/g()V 0
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in play. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final a(J)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
lload 1
invokeinterface android/support/v4/media/session/d/a(J)V 2
L1:
return
L2:
astore 3
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in skipToQueueItem. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/net/Uri;Landroid/os/Bundle;)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
aload 1
aload 2
invokeinterface android/support/v4/media/session/d/a(Landroid/net/Uri;Landroid/os/Bundle;)V 2
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in playFromUri. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/support/v4/media/RatingCompat;)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
aload 1
invokeinterface android/support/v4/media/session/d/a(Landroid/support/v4/media/RatingCompat;)V 1
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in setRating. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;Landroid/os/Bundle;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 1
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
astore 1
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
aload 1
aload 2
invokeinterface android/support/v4/media/session/d/c(Ljava/lang/String;Landroid/os/Bundle;)V 2
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in sendCustomAction. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
aload 1
aload 2
invokeinterface android/support/v4/media/session/d/a(Ljava/lang/String;Landroid/os/Bundle;)V 2
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in playFromMediaId. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method public final b()V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/h()V 0
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in pause. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final b(J)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
lload 1
invokeinterface android/support/v4/media/session/d/b(J)V 2
L1:
return
L2:
astore 3
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in seekTo. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 4
.limit stack 4
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
aload 1
aload 2
invokeinterface android/support/v4/media/session/d/b(Ljava/lang/String;Landroid/os/Bundle;)V 2
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in playFromSearch. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method public final c()V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/i()V 0
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in stop. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
aload 1
aload 2
invokeinterface android/support/v4/media/session/d/c(Ljava/lang/String;Landroid/os/Bundle;)V 2
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in sendCustomAction. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method public final d()V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/l()V 0
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in fastForward. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final e()V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/j()V 0
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in skipToNext. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final f()V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/m()V 0
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in rewind. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final g()V
.catch android/os/RemoteException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/session/u/a Landroid/support/v4/media/session/d;
invokeinterface android/support/v4/media/session/d/k()V 0
L1:
return
L2:
astore 1
ldc "MediaControllerCompat"
new java/lang/StringBuilder
dup
ldc "Dead object in skipToPrevious. "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 4
.end method
