.bytecode 50.0
.class public final synchronized android/support/v4/media/t
.super android/support/v4/media/s

.field public static final 'i' I = 126


.field public static final 'j' I = 127


.field public static final 'k' I = 130


.field public static final 'l' I = 1


.field public static final 'm' I = 2


.field public static final 'n' I = 4


.field public static final 'o' I = 8


.field public static final 'p' I = 16


.field public static final 'q' I = 32


.field public static final 'r' I = 64


.field public static final 's' I = 128


.field final 'a' Landroid/content/Context;

.field final 'b' Landroid/support/v4/media/ac;

.field final 'c' Landroid/media/AudioManager;

.field final 'd' Landroid/view/View;

.field final 'e' Ljava/lang/Object;

.field final 'f' Landroid/support/v4/media/x;

.field final 'g' Ljava/util/ArrayList;

.field final 'h' Landroid/support/v4/media/w;

.field final 't' Landroid/view/KeyEvent$Callback;

.method private <init>(Landroid/app/Activity;Landroid/support/v4/media/ac;)V
aload 0
aload 1
aconst_null
aload 2
invokespecial android/support/v4/media/t/<init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/ac;)V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/ac;)V
aload 0
invokespecial android/support/v4/media/s/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/media/t/g Ljava/util/ArrayList;
aload 0
new android/support/v4/media/u
dup
aload 0
invokespecial android/support/v4/media/u/<init>(Landroid/support/v4/media/t;)V
putfield android/support/v4/media/t/h Landroid/support/v4/media/w;
aload 0
new android/support/v4/media/v
dup
aload 0
invokespecial android/support/v4/media/v/<init>(Landroid/support/v4/media/t;)V
putfield android/support/v4/media/t/t Landroid/view/KeyEvent$Callback;
aload 1
ifnull L0
aload 1
astore 4
L1:
aload 0
aload 4
putfield android/support/v4/media/t/a Landroid/content/Context;
aload 0
aload 3
putfield android/support/v4/media/t/b Landroid/support/v4/media/ac;
aload 0
aload 0
getfield android/support/v4/media/t/a Landroid/content/Context;
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
putfield android/support/v4/media/t/c Landroid/media/AudioManager;
aload 1
ifnull L2
aload 1
invokevirtual android/app/Activity/getWindow()Landroid/view/Window;
invokevirtual android/view/Window/getDecorView()Landroid/view/View;
astore 2
L2:
aload 0
aload 2
putfield android/support/v4/media/t/d Landroid/view/View;
aload 0
aload 0
getfield android/support/v4/media/t/d Landroid/view/View;
invokestatic android/support/v4/view/ab/a(Landroid/view/View;)Ljava/lang/Object;
putfield android/support/v4/media/t/e Ljava/lang/Object;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L3
aload 0
new android/support/v4/media/x
dup
aload 0
getfield android/support/v4/media/t/a Landroid/content/Context;
aload 0
getfield android/support/v4/media/t/c Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/t/d Landroid/view/View;
aload 0
getfield android/support/v4/media/t/h Landroid/support/v4/media/w;
invokespecial android/support/v4/media/x/<init>(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;Landroid/support/v4/media/w;)V
putfield android/support/v4/media/t/f Landroid/support/v4/media/x;
return
L0:
aload 2
invokevirtual android/view/View/getContext()Landroid/content/Context;
astore 4
goto L1
L3:
aload 0
aconst_null
putfield android/support/v4/media/t/f Landroid/support/v4/media/x;
return
.limit locals 5
.limit stack 7
.end method

.method private <init>(Landroid/view/View;Landroid/support/v4/media/ac;)V
aload 0
aconst_null
aload 1
aload 2
invokespecial android/support/v4/media/t/<init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/ac;)V
return
.limit locals 3
.limit stack 4
.end method

.method static a(I)Z
iload 0
lookupswitch
79 : L0
85 : L0
86 : L0
87 : L0
88 : L0
89 : L0
90 : L0
91 : L0
126 : L0
127 : L0
130 : L0
default : L1
L1:
iconst_0
ireturn
L0:
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/view/KeyEvent;)Z
aload 1
aload 0
getfield android/support/v4/media/t/t Landroid/view/KeyEvent$Callback;
aload 0
getfield android/support/v4/media/t/e Ljava/lang/Object;
aload 0
invokestatic android/support/v4/view/ab/a(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 4
.end method

.method private j()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
ifnull L0
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method private k()[Landroid/support/v4/media/ad;
aload 0
getfield android/support/v4/media/t/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifgt L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/media/t/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/support/v4/media/ad
astore 1
aload 0
getfield android/support/v4/media/t/g Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private l()V
aload 0
invokespecial android/support/v4/media/t/k()[Landroid/support/v4/media/ad;
pop
return
.limit locals 1
.limit stack 1
.end method

.method private m()V
aload 0
invokespecial android/support/v4/media/t/k()[Landroid/support/v4/media/ad;
pop
return
.limit locals 1
.limit stack 1
.end method

.method private n()V
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
ifnull L0
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
astore 6
aload 0
getfield android/support/v4/media/t/b Landroid/support/v4/media/ac;
invokevirtual android/support/v4/media/ac/g()Z
istore 3
aload 0
getfield android/support/v4/media/t/b Landroid/support/v4/media/ac;
invokevirtual android/support/v4/media/ac/e()J
lstore 4
aload 6
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
ifnull L0
aload 6
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
astore 7
iload 3
ifeq L1
iconst_3
istore 2
L2:
iload 3
ifeq L3
fconst_1
fstore 1
L4:
aload 7
iload 2
lload 4
fload 1
invokevirtual android/media/RemoteControlClient/setPlaybackState(IJF)V
aload 6
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
bipush 60
invokevirtual android/media/RemoteControlClient/setTransportControlFlags(I)V
L0:
return
L1:
iconst_1
istore 2
goto L2
L3:
fconst_0
fstore 1
goto L4
.limit locals 8
.limit stack 5
.end method

.method private o()V
aload 0
invokespecial android/support/v4/media/t/n()V
aload 0
invokespecial android/support/v4/media/t/k()[Landroid/support/v4/media/ad;
pop
aload 0
invokespecial android/support/v4/media/t/k()[Landroid/support/v4/media/ad;
pop
return
.limit locals 1
.limit stack 1
.end method

.method private p()V
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
astore 1
aload 1
invokevirtual android/support/v4/media/x/d()V
aload 1
getfield android/support/v4/media/x/c Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 1
getfield android/support/v4/media/x/h Landroid/view/ViewTreeObserver$OnWindowAttachListener;
invokevirtual android/view/ViewTreeObserver/removeOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V
aload 1
getfield android/support/v4/media/x/c Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 1
getfield android/support/v4/media/x/i Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;
invokevirtual android/view/ViewTreeObserver/removeOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a()V
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
ifnull L0
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
astore 1
aload 1
getfield android/support/v4/media/x/o I
iconst_3
if_icmpeq L1
aload 1
iconst_3
putfield android/support/v4/media/x/o I
aload 1
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
iconst_3
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L1:
aload 1
getfield android/support/v4/media/x/n Z
ifeq L0
aload 1
invokevirtual android/support/v4/media/x/a()V
L0:
aload 0
invokespecial android/support/v4/media/t/n()V
aload 0
invokespecial android/support/v4/media/t/k()[Landroid/support/v4/media/ad;
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v4/media/ad;)V
aload 0
getfield android/support/v4/media/t/g Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final b()V
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
ifnull L0
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
astore 1
aload 1
getfield android/support/v4/media/x/o I
iconst_3
if_icmpne L1
aload 1
iconst_2
putfield android/support/v4/media/x/o I
aload 1
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
iconst_2
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L1:
aload 1
invokevirtual android/support/v4/media/x/b()V
L0:
aload 0
invokespecial android/support/v4/media/t/n()V
aload 0
invokespecial android/support/v4/media/t/k()[Landroid/support/v4/media/ad;
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/support/v4/media/ad;)V
aload 0
getfield android/support/v4/media/t/g Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final c()V
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
ifnull L0
aload 0
getfield android/support/v4/media/t/f Landroid/support/v4/media/x;
astore 1
aload 1
getfield android/support/v4/media/x/o I
iconst_1
if_icmpeq L1
aload 1
iconst_1
putfield android/support/v4/media/x/o I
aload 1
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
iconst_1
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L1:
aload 1
invokevirtual android/support/v4/media/x/b()V
L0:
aload 0
invokespecial android/support/v4/media/t/n()V
aload 0
invokespecial android/support/v4/media/t/k()[Landroid/support/v4/media/ad;
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final d()J
aload 0
getfield android/support/v4/media/t/b Landroid/support/v4/media/ac;
invokevirtual android/support/v4/media/ac/d()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final e()J
aload 0
getfield android/support/v4/media/t/b Landroid/support/v4/media/ac;
invokevirtual android/support/v4/media/ac/e()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final f()V
return
.limit locals 1
.limit stack 0
.end method

.method public final g()Z
aload 0
getfield android/support/v4/media/t/b Landroid/support/v4/media/ac;
invokevirtual android/support/v4/media/ac/g()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final h()I
bipush 100
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()I
bipush 60
ireturn
.limit locals 1
.limit stack 1
.end method
