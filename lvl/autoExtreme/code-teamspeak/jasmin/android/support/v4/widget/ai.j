.bytecode 50.0
.class final synchronized android/support/v4/widget/ai
.super java/lang/Object

.field private static final 'a' [I

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16843828
iastore
putstatic android/support/v4/widget/ai/a [I
return
.limit locals 0
.limit stack 4
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Object;)I
aload 0
ifnull L0
aload 0
checkcast android/view/WindowInsets
invokevirtual android/view/WindowInsets/getSystemWindowInsetTop()I
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
.catch all from L0 to L1 using L2
aload 0
getstatic android/support/v4/widget/ai/a [I
invokevirtual android/content/Context/obtainStyledAttributes([I)Landroid/content/res/TypedArray;
astore 0
L0:
aload 0
iconst_0
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
L1:
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
aload 1
areturn
L2:
astore 1
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public static a(Landroid/view/View;)V
aload 0
instanceof android/support/v4/widget/ak
ifeq L0
aload 0
new android/support/v4/widget/aj
dup
invokespecial android/support/v4/widget/aj/<init>()V
invokevirtual android/view/View/setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V
aload 0
sipush 1280
invokevirtual android/view/View/setSystemUiVisibility(I)V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/view/View;Ljava/lang/Object;I)V
aload 1
checkcast android/view/WindowInsets
astore 3
iload 2
iconst_3
if_icmpne L0
aload 3
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetLeft()I
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetTop()I
iconst_0
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetBottom()I
invokevirtual android/view/WindowInsets/replaceSystemWindowInsets(IIII)Landroid/view/WindowInsets;
astore 1
L1:
aload 0
aload 1
invokevirtual android/view/View/dispatchApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
pop
return
L0:
aload 3
astore 1
iload 2
iconst_5
if_icmpne L1
aload 3
iconst_0
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetTop()I
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetRight()I
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetBottom()I
invokevirtual android/view/WindowInsets/replaceSystemWindowInsets(IIII)Landroid/view/WindowInsets;
astore 1
goto L1
.limit locals 4
.limit stack 5
.end method

.method public static a(Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/Object;I)V
aload 1
checkcast android/view/WindowInsets
astore 3
iload 2
iconst_3
if_icmpne L0
aload 3
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetLeft()I
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetTop()I
iconst_0
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetBottom()I
invokevirtual android/view/WindowInsets/replaceSystemWindowInsets(IIII)Landroid/view/WindowInsets;
astore 1
L1:
aload 0
aload 1
invokevirtual android/view/WindowInsets/getSystemWindowInsetLeft()I
putfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
aload 0
aload 1
invokevirtual android/view/WindowInsets/getSystemWindowInsetTop()I
putfield android/view/ViewGroup$MarginLayoutParams/topMargin I
aload 0
aload 1
invokevirtual android/view/WindowInsets/getSystemWindowInsetRight()I
putfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
aload 0
aload 1
invokevirtual android/view/WindowInsets/getSystemWindowInsetBottom()I
putfield android/view/ViewGroup$MarginLayoutParams/bottomMargin I
return
L0:
aload 3
astore 1
iload 2
iconst_5
if_icmpne L1
aload 3
iconst_0
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetTop()I
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetRight()I
aload 3
invokevirtual android/view/WindowInsets/getSystemWindowInsetBottom()I
invokevirtual android/view/WindowInsets/replaceSystemWindowInsets(IIII)Landroid/view/WindowInsets;
astore 1
goto L1
.limit locals 4
.limit stack 5
.end method
