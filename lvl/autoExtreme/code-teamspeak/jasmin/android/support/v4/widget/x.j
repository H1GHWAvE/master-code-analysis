.bytecode 50.0
.class final synchronized android/support/v4/widget/x
.super android/support/v4/view/a

.field final synthetic 'a' Landroid/support/v4/widget/DrawerLayout;

.field private final 'c' Landroid/graphics/Rect;

.method <init>(Landroid/support/v4/widget/DrawerLayout;)V
aload 0
aload 1
putfield android/support/v4/widget/x/a Landroid/support/v4/widget/DrawerLayout;
aload 0
invokespecial android/support/v4/view/a/<init>()V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/x/c Landroid/graphics/Rect;
return
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/support/v4/view/a/q;Landroid/support/v4/view/a/q;)V
aload 0
getfield android/support/v4/widget/x/c Landroid/graphics/Rect;
astore 3
aload 2
aload 3
invokevirtual android/support/v4/view/a/q/a(Landroid/graphics/Rect;)V
aload 1
aload 3
invokevirtual android/support/v4/view/a/q/b(Landroid/graphics/Rect;)V
aload 2
aload 3
invokevirtual android/support/v4/view/a/q/c(Landroid/graphics/Rect;)V
aload 1
aload 3
invokevirtual android/support/v4/view/a/q/d(Landroid/graphics/Rect;)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/e()Z
invokevirtual android/support/v4/view/a/q/c(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/k()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/a(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/l()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/n()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/c(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/j()Z
invokevirtual android/support/v4/view/a/q/h(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/h()Z
invokevirtual android/support/v4/view/a/q/f(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/c()Z
invokevirtual android/support/v4/view/a/q/a(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/d()Z
invokevirtual android/support/v4/view/a/q/b(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/f()Z
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/g()Z
invokevirtual android/support/v4/view/a/q/e(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/i()Z
invokevirtual android/support/v4/view/a/q/g(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/b()I
invokevirtual android/support/v4/view/a/q/a(I)V
return
.limit locals 4
.limit stack 2
.end method

.method private static a(Landroid/support/v4/view/a/q;Landroid/view/ViewGroup;)V
aload 1
invokevirtual android/view/ViewGroup/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 1
iload 2
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 4
aload 4
invokestatic android/support/v4/widget/DrawerLayout/f(Landroid/view/View;)Z
ifeq L2
aload 0
aload 4
invokevirtual android/support/v4/view/a/q/c(Landroid/view/View;)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 5
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
invokestatic android/support/v4/widget/DrawerLayout/f()Z
ifeq L0
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
L1:
aload 2
ldc android/support/v4/widget/DrawerLayout
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 2
iconst_0
invokevirtual android/support/v4/view/a/q/a(Z)V
aload 2
iconst_0
invokevirtual android/support/v4/view/a/q/b(Z)V
aload 2
getstatic android/support/v4/view/a/s/a Landroid/support/v4/view/a/s;
invokevirtual android/support/v4/view/a/q/a(Landroid/support/v4/view/a/s;)Z
pop
aload 2
getstatic android/support/v4/view/a/s/b Landroid/support/v4/view/a/s;
invokevirtual android/support/v4/view/a/q/a(Landroid/support/v4/view/a/s;)Z
pop
return
L0:
aload 2
invokestatic android/support/v4/view/a/q/a(Landroid/support/v4/view/a/q;)Landroid/support/v4/view/a/q;
astore 5
aload 0
aload 1
aload 5
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 2
aload 1
invokevirtual android/support/v4/view/a/q/b(Landroid/view/View;)V
aload 1
invokestatic android/support/v4/view/cx/g(Landroid/view/View;)Landroid/view/ViewParent;
astore 6
aload 6
instanceof android/view/View
ifeq L2
aload 2
aload 6
checkcast android/view/View
invokevirtual android/support/v4/view/a/q/d(Landroid/view/View;)V
L2:
aload 0
getfield android/support/v4/widget/x/c Landroid/graphics/Rect;
astore 6
aload 5
aload 6
invokevirtual android/support/v4/view/a/q/a(Landroid/graphics/Rect;)V
aload 2
aload 6
invokevirtual android/support/v4/view/a/q/b(Landroid/graphics/Rect;)V
aload 5
aload 6
invokevirtual android/support/v4/view/a/q/c(Landroid/graphics/Rect;)V
aload 2
aload 6
invokevirtual android/support/v4/view/a/q/d(Landroid/graphics/Rect;)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/e()Z
invokevirtual android/support/v4/view/a/q/c(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/k()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/a(Ljava/lang/CharSequence;)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/l()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/n()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/c(Ljava/lang/CharSequence;)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/j()Z
invokevirtual android/support/v4/view/a/q/h(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/h()Z
invokevirtual android/support/v4/view/a/q/f(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/c()Z
invokevirtual android/support/v4/view/a/q/a(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/d()Z
invokevirtual android/support/v4/view/a/q/b(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/f()Z
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/g()Z
invokevirtual android/support/v4/view/a/q/e(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/i()Z
invokevirtual android/support/v4/view/a/q/g(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/b()I
invokevirtual android/support/v4/view/a/q/a(I)V
aload 5
invokevirtual android/support/v4/view/a/q/o()V
aload 1
checkcast android/view/ViewGroup
astore 1
aload 1
invokevirtual android/view/ViewGroup/getChildCount()I
istore 4
iconst_0
istore 3
L3:
iload 3
iload 4
if_icmpge L1
aload 1
iload 3
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 5
aload 5
invokestatic android/support/v4/widget/DrawerLayout/f(Landroid/view/View;)Z
ifeq L4
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/c(Landroid/view/View;)V
L4:
iload 3
iconst_1
iadd
istore 3
goto L3
.limit locals 7
.limit stack 3
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 2
ldc android/support/v4/widget/DrawerLayout
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
invokestatic android/support/v4/widget/DrawerLayout/f()Z
ifne L0
aload 2
invokestatic android/support/v4/widget/DrawerLayout/f(Landroid/view/View;)Z
ifeq L1
L0:
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/v4/view/a/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 2
invokevirtual android/view/accessibility/AccessibilityEvent/getEventType()I
bipush 32
if_icmpne L0
aload 2
invokevirtual android/view/accessibility/AccessibilityEvent/getText()Ljava/util/List;
astore 2
aload 0
getfield android/support/v4/widget/x/a Landroid/support/v4/widget/DrawerLayout;
invokestatic android/support/v4/widget/DrawerLayout/a(Landroid/support/v4/widget/DrawerLayout;)Landroid/view/View;
astore 1
aload 1
ifnull L1
aload 0
getfield android/support/v4/widget/x/a Landroid/support/v4/widget/DrawerLayout;
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/c(Landroid/view/View;)I
istore 3
aload 0
getfield android/support/v4/widget/x/a Landroid/support/v4/widget/DrawerLayout;
astore 1
iload 3
aload 1
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
istore 3
iload 3
iconst_3
if_icmpne L2
aload 1
getfield android/support/v4/widget/DrawerLayout/l Ljava/lang/CharSequence;
astore 1
L3:
aload 1
ifnull L1
aload 2
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L1:
iconst_1
ireturn
L2:
iload 3
iconst_5
if_icmpne L4
aload 1
getfield android/support/v4/widget/DrawerLayout/m Ljava/lang/CharSequence;
astore 1
goto L3
L4:
aconst_null
astore 1
goto L3
L0:
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/a/d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
.limit locals 4
.limit stack 3
.end method
