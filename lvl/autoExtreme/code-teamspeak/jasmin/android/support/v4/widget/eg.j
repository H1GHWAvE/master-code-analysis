.bytecode 50.0
.class public final synchronized android/support/v4/widget/eg
.super java/lang/Object

.field private static final 'L' Landroid/view/animation/Interpolator;

.field public static final 'a' I = -1


.field public static final 'b' I = 0


.field public static final 'c' I = 1


.field public static final 'd' I = 2


.field public static final 'e' I = 1


.field public static final 'f' I = 2


.field public static final 'g' I = 4


.field public static final 'h' I = 8


.field public static final 'i' I = 15


.field public static final 'j' I = 1


.field public static final 'k' I = 2


.field public static final 'l' I = 3


.field private static final 'w' Ljava/lang/String; = "ViewDragHelper"

.field private static final 'x' I = 20


.field private static final 'y' I = 256


.field private static final 'z' I = 600


.field private 'A' I

.field private 'B' [I

.field private 'C' [I

.field private 'D' [I

.field private 'E' I

.field private 'F' Landroid/view/VelocityTracker;

.field private 'G' F

.field private 'H' Landroid/support/v4/widget/ca;

.field private final 'I' Landroid/support/v4/widget/ej;

.field private 'J' Z

.field private final 'K' Landroid/view/ViewGroup;

.field private final 'M' Ljava/lang/Runnable;

.field public 'm' I

.field 'n' I

.field 'o' [F

.field 'p' [F

.field 'q' [F

.field 'r' [F

.field 's' F

.field 't' I

.field 'u' I

.field 'v' Landroid/view/View;

.method static <clinit>()V
new android/support/v4/widget/eh
dup
invokespecial android/support/v4/widget/eh/<init>()V
putstatic android/support/v4/widget/eg/L Landroid/view/animation/Interpolator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield android/support/v4/widget/eg/A I
aload 0
new android/support/v4/widget/ei
dup
aload 0
invokespecial android/support/v4/widget/ei/<init>(Landroid/support/v4/widget/eg;)V
putfield android/support/v4/widget/eg/M Ljava/lang/Runnable;
aload 2
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Parent view may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 3
ifnonnull L1
new java/lang/IllegalArgumentException
dup
ldc "Callback may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 2
putfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
aload 0
aload 3
putfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 1
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
astore 2
aload 0
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 20.0F
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/widget/eg/t I
aload 0
aload 2
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/v4/widget/eg/n I
aload 0
aload 2
invokevirtual android/view/ViewConfiguration/getScaledMaximumFlingVelocity()I
i2f
putfield android/support/v4/widget/eg/G F
aload 0
aload 2
invokevirtual android/view/ViewConfiguration/getScaledMinimumFlingVelocity()I
i2f
putfield android/support/v4/widget/eg/s F
aload 0
aload 1
getstatic android/support/v4/widget/eg/L Landroid/view/animation/Interpolator;
invokestatic android/support/v4/widget/ca/a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;
putfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
return
.limit locals 4
.limit stack 4
.end method

.method private static a(FFF)F
fload 0
invokestatic java/lang/Math/abs(F)F
fstore 3
fload 3
fload 1
fcmpg
ifge L0
fconst_0
fstore 1
L1:
fload 1
freturn
L0:
fload 3
fload 2
fcmpl
ifle L2
fload 2
fstore 1
fload 0
fconst_0
fcmpl
ifgt L1
fload 2
fneg
freturn
L2:
fload 0
freturn
.limit locals 4
.limit stack 2
.end method

.method private a(III)I
iload 1
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getWidth()I
istore 7
iload 7
iconst_2
idiv
istore 8
fconst_1
iload 1
invokestatic java/lang/Math/abs(I)I
i2f
iload 7
i2f
fdiv
invokestatic java/lang/Math/min(FF)F
fstore 6
iload 8
i2f
fstore 4
iload 8
i2f
fstore 5
fload 6
ldc_w 0.5F
fsub
f2d
ldc2_w 0.4712389167638204D
dmul
d2f
f2d
invokestatic java/lang/Math/sin(D)D
d2f
fstore 6
iload 2
invokestatic java/lang/Math/abs(I)I
istore 2
iload 2
ifle L1
fload 6
fload 5
fmul
fload 4
fadd
iload 2
i2f
fdiv
invokestatic java/lang/Math/abs(F)F
ldc_w 1000.0F
fmul
invokestatic java/lang/Math/round(F)I
iconst_4
imul
istore 1
L2:
iload 1
sipush 600
invokestatic java/lang/Math/min(II)I
ireturn
L1:
iload 1
invokestatic java/lang/Math/abs(I)I
i2f
iload 3
i2f
fdiv
fconst_1
fadd
ldc_w 256.0F
fmul
f2i
istore 1
goto L2
.limit locals 9
.limit stack 4
.end method

.method private a(Landroid/view/View;IIII)I
iload 4
aload 0
getfield android/support/v4/widget/eg/s F
f2i
aload 0
getfield android/support/v4/widget/eg/G F
f2i
invokestatic android/support/v4/widget/eg/b(III)I
istore 4
iload 5
aload 0
getfield android/support/v4/widget/eg/s F
f2i
aload 0
getfield android/support/v4/widget/eg/G F
f2i
invokestatic android/support/v4/widget/eg/b(III)I
istore 5
iload 2
invokestatic java/lang/Math/abs(I)I
istore 9
iload 3
invokestatic java/lang/Math/abs(I)I
istore 10
iload 4
invokestatic java/lang/Math/abs(I)I
istore 11
iload 5
invokestatic java/lang/Math/abs(I)I
istore 12
iload 11
iload 12
iadd
istore 13
iload 9
iload 10
iadd
istore 14
iload 4
ifeq L0
iload 11
i2f
iload 13
i2f
fdiv
fstore 6
L1:
iload 5
ifeq L2
iload 12
i2f
iload 13
i2f
fdiv
fstore 7
L3:
aload 0
iload 2
iload 4
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 1
invokevirtual android/support/v4/widget/ej/b(Landroid/view/View;)I
invokespecial android/support/v4/widget/eg/a(III)I
istore 2
aload 0
iload 3
iload 5
iconst_0
invokespecial android/support/v4/widget/eg/a(III)I
istore 3
iload 2
i2f
fstore 8
fload 7
iload 3
i2f
fmul
fload 6
fload 8
fmul
fadd
f2i
ireturn
L0:
iload 9
i2f
iload 14
i2f
fdiv
fstore 6
goto L1
L2:
iload 10
i2f
iload 14
i2f
fdiv
fstore 7
goto L3
.limit locals 15
.limit stack 5
.end method

.method public static a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
aload 0
aload 2
invokestatic android/support/v4/widget/eg/a(Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
astore 0
aload 0
aload 0
getfield android/support/v4/widget/eg/n I
i2f
fconst_1
fload 1
fdiv
fmul
f2i
putfield android/support/v4/widget/eg/n I
aload 0
areturn
.limit locals 3
.limit stack 4
.end method

.method public static a(Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
new android/support/v4/widget/eg
dup
aload 0
invokevirtual android/view/ViewGroup/getContext()Landroid/content/Context;
aload 0
aload 1
invokespecial android/support/v4/widget/eg/<init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private a(F)V
aload 0
fload 1
putfield android/support/v4/widget/eg/s F
return
.limit locals 2
.limit stack 2
.end method

.method private a(FFI)V
iconst_0
istore 5
aload 0
getfield android/support/v4/widget/eg/o [F
ifnull L0
aload 0
getfield android/support/v4/widget/eg/o [F
arraylength
iload 3
if_icmpgt L1
L0:
iload 3
iconst_1
iadd
newarray float
astore 8
iload 3
iconst_1
iadd
newarray float
astore 9
iload 3
iconst_1
iadd
newarray float
astore 10
iload 3
iconst_1
iadd
newarray float
astore 11
iload 3
iconst_1
iadd
newarray int
astore 12
iload 3
iconst_1
iadd
newarray int
astore 13
iload 3
iconst_1
iadd
newarray int
astore 14
aload 0
getfield android/support/v4/widget/eg/o [F
ifnull L2
aload 0
getfield android/support/v4/widget/eg/o [F
iconst_0
aload 8
iconst_0
aload 0
getfield android/support/v4/widget/eg/o [F
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/p [F
iconst_0
aload 9
iconst_0
aload 0
getfield android/support/v4/widget/eg/p [F
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/q [F
iconst_0
aload 10
iconst_0
aload 0
getfield android/support/v4/widget/eg/q [F
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/r [F
iconst_0
aload 11
iconst_0
aload 0
getfield android/support/v4/widget/eg/r [F
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/B [I
iconst_0
aload 12
iconst_0
aload 0
getfield android/support/v4/widget/eg/B [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/C [I
iconst_0
aload 13
iconst_0
aload 0
getfield android/support/v4/widget/eg/C [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/D [I
iconst_0
aload 14
iconst_0
aload 0
getfield android/support/v4/widget/eg/D [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L2:
aload 0
aload 8
putfield android/support/v4/widget/eg/o [F
aload 0
aload 9
putfield android/support/v4/widget/eg/p [F
aload 0
aload 10
putfield android/support/v4/widget/eg/q [F
aload 0
aload 11
putfield android/support/v4/widget/eg/r [F
aload 0
aload 12
putfield android/support/v4/widget/eg/B [I
aload 0
aload 13
putfield android/support/v4/widget/eg/C [I
aload 0
aload 14
putfield android/support/v4/widget/eg/D [I
L1:
aload 0
getfield android/support/v4/widget/eg/o [F
astore 8
aload 0
getfield android/support/v4/widget/eg/q [F
iload 3
fload 1
fastore
aload 8
iload 3
fload 1
fastore
aload 0
getfield android/support/v4/widget/eg/p [F
astore 8
aload 0
getfield android/support/v4/widget/eg/r [F
iload 3
fload 2
fastore
aload 8
iload 3
fload 2
fastore
aload 0
getfield android/support/v4/widget/eg/B [I
astore 8
fload 1
f2i
istore 7
fload 2
f2i
istore 6
iload 7
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getLeft()I
aload 0
getfield android/support/v4/widget/eg/t I
iadd
if_icmpge L3
iconst_1
istore 5
L3:
iload 5
istore 4
iload 6
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getTop()I
aload 0
getfield android/support/v4/widget/eg/t I
iadd
if_icmpge L4
iload 5
iconst_4
ior
istore 4
L4:
iload 4
istore 5
iload 7
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getRight()I
aload 0
getfield android/support/v4/widget/eg/t I
isub
if_icmple L5
iload 4
iconst_2
ior
istore 5
L5:
iload 5
istore 4
iload 6
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getBottom()I
aload 0
getfield android/support/v4/widget/eg/t I
isub
if_icmple L6
iload 5
bipush 8
ior
istore 4
L6:
aload 8
iload 3
iload 4
iastore
aload 0
aload 0
getfield android/support/v4/widget/eg/E I
iconst_1
iload 3
ishl
ior
putfield android/support/v4/widget/eg/E I
return
.limit locals 15
.limit stack 5
.end method

.method private a(FFII)Z
fload 1
invokestatic java/lang/Math/abs(F)F
fstore 1
fload 2
invokestatic java/lang/Math/abs(F)F
fstore 2
aload 0
getfield android/support/v4/widget/eg/B [I
iload 3
iaload
iload 4
iand
iload 4
if_icmpne L0
aload 0
getfield android/support/v4/widget/eg/u I
iload 4
iand
ifeq L0
aload 0
getfield android/support/v4/widget/eg/D [I
iload 3
iaload
iload 4
iand
iload 4
if_icmpeq L0
aload 0
getfield android/support/v4/widget/eg/C [I
iload 3
iaload
iload 4
iand
iload 4
if_icmpeq L0
fload 1
aload 0
getfield android/support/v4/widget/eg/n I
i2f
fcmpg
ifgt L1
fload 2
aload 0
getfield android/support/v4/widget/eg/n I
i2f
fcmpg
ifgt L1
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/widget/eg/C [I
iload 3
iaload
iload 4
iand
ifne L0
fload 1
aload 0
getfield android/support/v4/widget/eg/n I
i2f
fcmpl
ifle L0
iconst_1
ireturn
.limit locals 5
.limit stack 2
.end method

.method private a(IIII)Z
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getLeft()I
istore 8
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getTop()I
istore 9
iload 1
iload 8
isub
istore 1
iload 2
iload 9
isub
istore 2
iload 1
ifne L0
iload 2
ifne L0
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/g()V
aload 0
iconst_0
invokevirtual android/support/v4/widget/eg/b(I)V
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
astore 16
iload 3
aload 0
getfield android/support/v4/widget/eg/s F
f2i
aload 0
getfield android/support/v4/widget/eg/G F
f2i
invokestatic android/support/v4/widget/eg/b(III)I
istore 3
iload 4
aload 0
getfield android/support/v4/widget/eg/s F
f2i
aload 0
getfield android/support/v4/widget/eg/G F
f2i
invokestatic android/support/v4/widget/eg/b(III)I
istore 4
iload 1
invokestatic java/lang/Math/abs(I)I
istore 10
iload 2
invokestatic java/lang/Math/abs(I)I
istore 11
iload 3
invokestatic java/lang/Math/abs(I)I
istore 12
iload 4
invokestatic java/lang/Math/abs(I)I
istore 13
iload 12
iload 13
iadd
istore 14
iload 10
iload 11
iadd
istore 15
iload 3
ifeq L1
iload 12
i2f
iload 14
i2f
fdiv
fstore 5
L2:
iload 4
ifeq L3
iload 13
i2f
iload 14
i2f
fdiv
fstore 6
L4:
aload 0
iload 1
iload 3
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 16
invokevirtual android/support/v4/widget/ej/b(Landroid/view/View;)I
invokespecial android/support/v4/widget/eg/a(III)I
istore 3
aload 0
iload 2
iload 4
iconst_0
invokespecial android/support/v4/widget/eg/a(III)I
istore 4
iload 3
i2f
fstore 7
fload 6
iload 4
i2f
fmul
fload 5
fload 7
fmul
fadd
f2i
istore 3
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
astore 16
aload 16
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 16
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 8
iload 9
iload 1
iload 2
iload 3
invokeinterface android/support/v4/widget/cb/a(Ljava/lang/Object;IIIII)V 6
aload 0
iconst_2
invokevirtual android/support/v4/widget/eg/b(I)V
iconst_1
ireturn
L1:
iload 10
i2f
iload 15
i2f
fdiv
fstore 5
goto L2
L3:
iload 11
i2f
iload 15
i2f
fdiv
fstore 6
goto L4
.limit locals 17
.limit stack 7
.end method

.method private a(Landroid/view/View;F)Z
aload 1
ifnonnull L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 1
invokevirtual android/support/v4/widget/ej/b(Landroid/view/View;)I
ifle L2
iconst_1
istore 3
L3:
iload 3
ifeq L1
fload 2
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v4/widget/eg/n I
i2f
fcmpl
ifle L1
iconst_1
ireturn
L2:
iconst_0
istore 3
goto L3
.limit locals 4
.limit stack 2
.end method

.method private static b(F)F
fload 0
ldc_w 0.5F
fsub
f2d
ldc2_w 0.4712389167638204D
dmul
d2f
f2d
invokestatic java/lang/Math/sin(D)D
d2f
freturn
.limit locals 1
.limit stack 4
.end method

.method private static b(III)I
iload 0
invokestatic java/lang/Math/abs(I)I
istore 3
iload 3
iload 1
if_icmpge L0
iconst_0
istore 1
L1:
iload 1
ireturn
L0:
iload 3
iload 2
if_icmple L2
iload 2
istore 1
iload 0
ifgt L1
iload 2
ineg
ireturn
L2:
iload 0
ireturn
.limit locals 4
.limit stack 2
.end method

.method private b(FFI)V
iconst_1
istore 5
aload 0
fload 1
fload 2
iload 3
iconst_1
invokespecial android/support/v4/widget/eg/a(FFII)Z
ifeq L0
L1:
iload 5
istore 4
aload 0
fload 2
fload 1
iload 3
iconst_4
invokespecial android/support/v4/widget/eg/a(FFII)Z
ifeq L2
iload 5
iconst_4
ior
istore 4
L2:
iload 4
istore 5
aload 0
fload 1
fload 2
iload 3
iconst_2
invokespecial android/support/v4/widget/eg/a(FFII)Z
ifeq L3
iload 4
iconst_2
ior
istore 5
L3:
iload 5
istore 4
aload 0
fload 2
fload 1
iload 3
bipush 8
invokespecial android/support/v4/widget/eg/a(FFII)Z
ifeq L4
iload 5
bipush 8
ior
istore 4
L4:
iload 4
ifeq L5
aload 0
getfield android/support/v4/widget/eg/C [I
astore 6
aload 6
iload 3
aload 6
iload 3
iaload
iload 4
ior
iastore
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
iload 4
iload 3
invokevirtual android/support/v4/widget/ej/a(II)V
L5:
return
L0:
iconst_0
istore 5
goto L1
.limit locals 7
.limit stack 5
.end method

.method private b(IIII)V
aload 0
getfield android/support/v4/widget/eg/J Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "Cannot flingCapturedView outside of a call to Callback#onViewReleased"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getLeft()I
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getTop()I
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
aload 0
getfield android/support/v4/widget/eg/A I
invokestatic android/support/v4/view/cs/a(Landroid/view/VelocityTracker;I)F
f2i
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
aload 0
getfield android/support/v4/widget/eg/A I
invokestatic android/support/v4/view/cs/b(Landroid/view/VelocityTracker;I)F
f2i
iload 1
iload 3
iload 2
iload 4
invokevirtual android/support/v4/widget/ca/a(IIIIIIII)V
aload 0
iconst_2
invokevirtual android/support/v4/widget/eg/b(I)V
return
.limit locals 5
.limit stack 9
.end method

.method private b(Landroid/view/View;I)Z
aload 1
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
if_acmpne L0
aload 0
getfield android/support/v4/widget/eg/A I
iload 2
if_icmpne L0
iconst_1
ireturn
L0:
aload 1
ifnull L1
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 1
invokevirtual android/support/v4/widget/ej/a(Landroid/view/View;)Z
ifeq L1
aload 0
iload 2
putfield android/support/v4/widget/eg/A I
aload 0
aload 1
iload 2
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;I)V
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static b(Landroid/view/View;II)Z
aload 0
ifnonnull L0
L1:
iconst_0
ireturn
L0:
iload 1
aload 0
invokevirtual android/view/View/getLeft()I
if_icmplt L1
iload 1
aload 0
invokevirtual android/view/View/getRight()I
if_icmpge L1
iload 2
aload 0
invokevirtual android/view/View/getTop()I
if_icmplt L1
iload 2
aload 0
invokevirtual android/view/View/getBottom()I
if_icmpge L1
iconst_1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private b(Landroid/view/View;IIII)Z
aload 1
instanceof android/view/ViewGroup
ifeq L0
aload 1
checkcast android/view/ViewGroup
astore 9
aload 1
invokevirtual android/view/View/getScrollX()I
istore 7
aload 1
invokevirtual android/view/View/getScrollY()I
istore 8
aload 9
invokevirtual android/view/ViewGroup/getChildCount()I
iconst_1
isub
istore 6
L1:
iload 6
iflt L0
aload 9
iload 6
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 10
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getLeft()I
if_icmplt L2
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getRight()I
if_icmpge L2
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getTop()I
if_icmplt L2
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getBottom()I
if_icmpge L2
aload 0
aload 10
iload 2
iload 3
iload 4
iload 7
iadd
aload 10
invokevirtual android/view/View/getLeft()I
isub
iload 5
iload 8
iadd
aload 10
invokevirtual android/view/View/getTop()I
isub
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;IIII)Z
ifeq L2
iconst_1
ireturn
L2:
iload 6
iconst_1
isub
istore 6
goto L1
L0:
aload 1
iload 2
ineg
invokestatic android/support/v4/view/cx/a(Landroid/view/View;I)Z
ifne L3
aload 1
iload 3
ineg
invokestatic android/support/v4/view/cx/b(Landroid/view/View;I)Z
ifeq L4
L3:
iconst_1
ireturn
L4:
iconst_0
ireturn
.limit locals 11
.limit stack 7
.end method

.method private c(F)V
aload 0
iconst_1
putfield android/support/v4/widget/eg/J Z
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
fload 1
invokevirtual android/support/v4/widget/ej/a(Landroid/view/View;F)V
aload 0
iconst_0
putfield android/support/v4/widget/eg/J Z
aload 0
getfield android/support/v4/widget/eg/m I
iconst_1
if_icmpne L0
aload 0
iconst_0
invokevirtual android/support/v4/widget/eg/b(I)V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private c(I)V
aload 0
iload 1
putfield android/support/v4/widget/eg/u I
return
.limit locals 2
.limit stack 2
.end method

.method private c(III)V
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getLeft()I
istore 6
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getTop()I
istore 5
iload 1
istore 4
iload 2
ifeq L0
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 1
invokevirtual android/support/v4/widget/ej/a(Landroid/view/View;I)I
istore 4
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 4
iload 6
isub
invokevirtual android/view/View/offsetLeftAndRight(I)V
L0:
iload 3
ifeq L1
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/support/v4/widget/ej/c(Landroid/view/View;)I
istore 1
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 1
iload 5
isub
invokevirtual android/view/View/offsetTopAndBottom(I)V
L1:
iload 2
ifne L2
iload 3
ifeq L3
L2:
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 4
invokevirtual android/support/v4/widget/ej/b(Landroid/view/View;I)V
L3:
return
.limit locals 7
.limit stack 3
.end method

.method private c(Landroid/view/MotionEvent;)V
aload 1
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;)I
istore 5
iconst_0
istore 4
L0:
iload 4
iload 5
if_icmpge L1
aload 1
iload 4
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 6
aload 1
iload 4
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 2
aload 1
iload 4
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 3
aload 0
getfield android/support/v4/widget/eg/q [F
iload 6
fload 2
fastore
aload 0
getfield android/support/v4/widget/eg/r [F
iload 6
fload 3
fastore
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
return
.limit locals 7
.limit stack 3
.end method

.method private c(II)Z
aload 0
iload 2
invokevirtual android/support/v4/widget/eg/a(I)Z
ifne L0
L1:
iconst_0
ireturn
L0:
iload 1
iconst_1
iand
iconst_1
if_icmpne L2
iconst_1
istore 5
L3:
iload 1
iconst_2
iand
iconst_2
if_icmpne L4
iconst_1
istore 1
L5:
aload 0
getfield android/support/v4/widget/eg/q [F
iload 2
faload
aload 0
getfield android/support/v4/widget/eg/o [F
iload 2
faload
fsub
fstore 3
aload 0
getfield android/support/v4/widget/eg/r [F
iload 2
faload
aload 0
getfield android/support/v4/widget/eg/p [F
iload 2
faload
fsub
fstore 4
iload 5
ifeq L6
iload 1
ifeq L6
fload 3
fload 3
fmul
fload 4
fload 4
fmul
fadd
aload 0
getfield android/support/v4/widget/eg/n I
aload 0
getfield android/support/v4/widget/eg/n I
imul
i2f
fcmpl
ifle L1
iconst_1
ireturn
L2:
iconst_0
istore 5
goto L3
L4:
iconst_0
istore 1
goto L5
L6:
iload 5
ifeq L7
fload 3
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v4/widget/eg/n I
i2f
fcmpl
ifle L1
iconst_1
ireturn
L7:
iload 1
ifeq L1
fload 4
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v4/widget/eg/n I
i2f
fcmpl
ifle L1
iconst_1
ireturn
.limit locals 6
.limit stack 3
.end method

.method private d()F
aload 0
getfield android/support/v4/widget/eg/s F
freturn
.limit locals 1
.limit stack 1
.end method

.method private d(I)V
aload 0
getfield android/support/v4/widget/eg/o [F
ifnonnull L0
return
L0:
aload 0
getfield android/support/v4/widget/eg/o [F
iload 1
fconst_0
fastore
aload 0
getfield android/support/v4/widget/eg/p [F
iload 1
fconst_0
fastore
aload 0
getfield android/support/v4/widget/eg/q [F
iload 1
fconst_0
fastore
aload 0
getfield android/support/v4/widget/eg/r [F
iload 1
fconst_0
fastore
aload 0
getfield android/support/v4/widget/eg/B [I
iload 1
iconst_0
iastore
aload 0
getfield android/support/v4/widget/eg/C [I
iload 1
iconst_0
iastore
aload 0
getfield android/support/v4/widget/eg/D [I
iload 1
iconst_0
iastore
aload 0
aload 0
getfield android/support/v4/widget/eg/E I
iconst_1
iload 1
ishl
iconst_m1
ixor
iand
putfield android/support/v4/widget/eg/E I
return
.limit locals 2
.limit stack 4
.end method

.method private d(II)Z
aload 0
iload 2
invokevirtual android/support/v4/widget/eg/a(I)Z
ifeq L0
aload 0
getfield android/support/v4/widget/eg/B [I
iload 2
iaload
iload 1
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private e()I
aload 0
getfield android/support/v4/widget/eg/m I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e(I)V
aload 0
getfield android/support/v4/widget/eg/o [F
ifnull L0
aload 0
getfield android/support/v4/widget/eg/o [F
arraylength
iload 1
if_icmpgt L1
L0:
iload 1
iconst_1
iadd
newarray float
astore 2
iload 1
iconst_1
iadd
newarray float
astore 3
iload 1
iconst_1
iadd
newarray float
astore 4
iload 1
iconst_1
iadd
newarray float
astore 5
iload 1
iconst_1
iadd
newarray int
astore 6
iload 1
iconst_1
iadd
newarray int
astore 7
iload 1
iconst_1
iadd
newarray int
astore 8
aload 0
getfield android/support/v4/widget/eg/o [F
ifnull L2
aload 0
getfield android/support/v4/widget/eg/o [F
iconst_0
aload 2
iconst_0
aload 0
getfield android/support/v4/widget/eg/o [F
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/p [F
iconst_0
aload 3
iconst_0
aload 0
getfield android/support/v4/widget/eg/p [F
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/q [F
iconst_0
aload 4
iconst_0
aload 0
getfield android/support/v4/widget/eg/q [F
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/r [F
iconst_0
aload 5
iconst_0
aload 0
getfield android/support/v4/widget/eg/r [F
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/B [I
iconst_0
aload 6
iconst_0
aload 0
getfield android/support/v4/widget/eg/B [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/C [I
iconst_0
aload 7
iconst_0
aload 0
getfield android/support/v4/widget/eg/C [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/widget/eg/D [I
iconst_0
aload 8
iconst_0
aload 0
getfield android/support/v4/widget/eg/D [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L2:
aload 0
aload 2
putfield android/support/v4/widget/eg/o [F
aload 0
aload 3
putfield android/support/v4/widget/eg/p [F
aload 0
aload 4
putfield android/support/v4/widget/eg/q [F
aload 0
aload 5
putfield android/support/v4/widget/eg/r [F
aload 0
aload 6
putfield android/support/v4/widget/eg/B [I
aload 0
aload 7
putfield android/support/v4/widget/eg/C [I
aload 0
aload 8
putfield android/support/v4/widget/eg/D [I
L1:
return
.limit locals 9
.limit stack 5
.end method

.method private e(II)Z
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 1
iload 2
invokestatic android/support/v4/widget/eg/b(Landroid/view/View;II)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method private f()I
aload 0
getfield android/support/v4/widget/eg/t I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f(II)I
iconst_0
istore 4
iload 1
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getLeft()I
aload 0
getfield android/support/v4/widget/eg/t I
iadd
if_icmpge L0
iconst_1
istore 4
L0:
iload 4
istore 3
iload 2
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getTop()I
aload 0
getfield android/support/v4/widget/eg/t I
iadd
if_icmpge L1
iload 4
iconst_4
ior
istore 3
L1:
iload 3
istore 4
iload 1
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getRight()I
aload 0
getfield android/support/v4/widget/eg/t I
isub
if_icmple L2
iload 3
iconst_2
ior
istore 4
L2:
iload 4
istore 1
iload 2
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getBottom()I
aload 0
getfield android/support/v4/widget/eg/t I
isub
if_icmple L3
iload 4
bipush 8
ior
istore 1
L3:
iload 1
ireturn
.limit locals 5
.limit stack 3
.end method

.method private f(I)Z
aload 0
getfield android/support/v4/widget/eg/B [I
arraylength
istore 4
iconst_0
istore 2
L0:
iload 2
iload 4
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v4/widget/eg/a(I)Z
ifeq L2
aload 0
getfield android/support/v4/widget/eg/B [I
iload 2
iaload
iload 1
iand
ifeq L2
iconst_1
istore 3
L3:
iload 3
ifeq L4
iconst_1
ireturn
L2:
iconst_0
istore 3
goto L3
L4:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method private g()Landroid/view/View;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()I
aload 0
getfield android/support/v4/widget/eg/A I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()I
aload 0
getfield android/support/v4/widget/eg/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()V
aload 0
getfield android/support/v4/widget/eg/o [F
ifnonnull L0
return
L0:
aload 0
getfield android/support/v4/widget/eg/o [F
fconst_0
invokestatic java/util/Arrays/fill([FF)V
aload 0
getfield android/support/v4/widget/eg/p [F
fconst_0
invokestatic java/util/Arrays/fill([FF)V
aload 0
getfield android/support/v4/widget/eg/q [F
fconst_0
invokestatic java/util/Arrays/fill([FF)V
aload 0
getfield android/support/v4/widget/eg/r [F
fconst_0
invokestatic java/util/Arrays/fill([FF)V
aload 0
getfield android/support/v4/widget/eg/B [I
iconst_0
invokestatic java/util/Arrays/fill([II)V
aload 0
getfield android/support/v4/widget/eg/C [I
iconst_0
invokestatic java/util/Arrays/fill([II)V
aload 0
getfield android/support/v4/widget/eg/D [I
iconst_0
invokestatic java/util/Arrays/fill([II)V
aload 0
iconst_0
putfield android/support/v4/widget/eg/E I
return
.limit locals 1
.limit stack 2
.end method

.method private k()Z
aload 0
getfield android/support/v4/widget/eg/o [F
arraylength
istore 5
iconst_0
istore 4
L0:
iload 4
iload 5
if_icmpge L1
aload 0
iload 4
invokevirtual android/support/v4/widget/eg/a(I)Z
ifeq L2
aload 0
getfield android/support/v4/widget/eg/q [F
iload 4
faload
aload 0
getfield android/support/v4/widget/eg/o [F
iload 4
faload
fsub
fstore 1
aload 0
getfield android/support/v4/widget/eg/r [F
iload 4
faload
aload 0
getfield android/support/v4/widget/eg/p [F
iload 4
faload
fsub
fstore 2
fload 1
fload 1
fmul
fload 2
fload 2
fmul
fadd
aload 0
getfield android/support/v4/widget/eg/n I
aload 0
getfield android/support/v4/widget/eg/n I
imul
i2f
fcmpl
ifle L3
iconst_1
istore 3
L4:
iload 3
ifeq L5
iconst_1
ireturn
L3:
iconst_0
istore 3
goto L4
L2:
iconst_0
istore 3
goto L4
L5:
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
iconst_0
ireturn
.limit locals 6
.limit stack 3
.end method

.method private l()V
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
sipush 1000
aload 0
getfield android/support/v4/widget/eg/G F
invokevirtual android/view/VelocityTracker/computeCurrentVelocity(IF)V
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
aload 0
getfield android/support/v4/widget/eg/A I
invokestatic android/support/v4/view/cs/a(Landroid/view/VelocityTracker;I)F
aload 0
getfield android/support/v4/widget/eg/s F
aload 0
getfield android/support/v4/widget/eg/G F
invokestatic android/support/v4/widget/eg/a(FFF)F
fstore 1
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
aload 0
getfield android/support/v4/widget/eg/A I
invokestatic android/support/v4/view/cs/b(Landroid/view/VelocityTracker;I)F
aload 0
getfield android/support/v4/widget/eg/s F
aload 0
getfield android/support/v4/widget/eg/G F
invokestatic android/support/v4/widget/eg/a(FFF)F
pop
aload 0
fload 1
invokespecial android/support/v4/widget/eg/c(F)V
return
.limit locals 2
.limit stack 3
.end method

.method public final a()V
aload 0
iconst_m1
putfield android/support/v4/widget/eg/A I
aload 0
getfield android/support/v4/widget/eg/o [F
ifnull L0
aload 0
getfield android/support/v4/widget/eg/o [F
fconst_0
invokestatic java/util/Arrays/fill([FF)V
aload 0
getfield android/support/v4/widget/eg/p [F
fconst_0
invokestatic java/util/Arrays/fill([FF)V
aload 0
getfield android/support/v4/widget/eg/q [F
fconst_0
invokestatic java/util/Arrays/fill([FF)V
aload 0
getfield android/support/v4/widget/eg/r [F
fconst_0
invokestatic java/util/Arrays/fill([FF)V
aload 0
getfield android/support/v4/widget/eg/B [I
iconst_0
invokestatic java/util/Arrays/fill([II)V
aload 0
getfield android/support/v4/widget/eg/C [I
iconst_0
invokestatic java/util/Arrays/fill([II)V
aload 0
getfield android/support/v4/widget/eg/D [I
iconst_0
invokestatic java/util/Arrays/fill([II)V
aload 0
iconst_0
putfield android/support/v4/widget/eg/E I
L0:
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
ifnull L1
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/recycle()V
aload 0
aconst_null
putfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
L1:
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/view/View;I)V
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
if_acmpeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view ("
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v4/widget/eg/v Landroid/view/View;
aload 0
iload 2
putfield android/support/v4/widget/eg/A I
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 1
invokevirtual android/support/v4/widget/ej/d(Landroid/view/View;)V
aload 0
iconst_1
invokevirtual android/support/v4/widget/eg/b(I)V
return
.limit locals 3
.limit stack 5
.end method

.method public final a(I)Z
aload 0
getfield android/support/v4/widget/eg/E I
iconst_1
iload 1
ishl
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final a(II)Z
aload 0
getfield android/support/v4/widget/eg/J Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
iload 2
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
aload 0
getfield android/support/v4/widget/eg/A I
invokestatic android/support/v4/view/cs/a(Landroid/view/VelocityTracker;I)F
f2i
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
aload 0
getfield android/support/v4/widget/eg/A I
invokestatic android/support/v4/view/cs/b(Landroid/view/VelocityTracker;I)F
f2i
invokespecial android/support/v4/widget/eg/a(IIII)Z
ireturn
.limit locals 3
.limit stack 6
.end method

.method public final a(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 7
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
istore 6
iload 7
ifne L0
aload 0
invokevirtual android/support/v4/widget/eg/a()V
L0:
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
ifnonnull L1
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
L1:
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
aload 1
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
iload 7
tableswitch 0
L2
L3
L4
L3
L5
L6
L7
default : L5
L5:
aload 0
getfield android/support/v4/widget/eg/m I
iconst_1
if_icmpne L8
iconst_1
ireturn
L2:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 6
aload 0
fload 2
fload 3
iload 6
invokespecial android/support/v4/widget/eg/a(FFI)V
aload 0
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
astore 1
aload 1
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
if_acmpne L9
aload 0
getfield android/support/v4/widget/eg/m I
iconst_2
if_icmpne L9
aload 0
aload 1
iload 6
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;I)Z
pop
L9:
aload 0
getfield android/support/v4/widget/eg/B [I
iload 6
iaload
aload 0
getfield android/support/v4/widget/eg/u I
iand
ifeq L5
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
invokevirtual android/support/v4/widget/ej/c()V
goto L5
L6:
aload 1
iload 6
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 7
aload 1
iload 6
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 2
aload 1
iload 6
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 3
aload 0
fload 2
fload 3
iload 7
invokespecial android/support/v4/widget/eg/a(FFI)V
aload 0
getfield android/support/v4/widget/eg/m I
ifne L10
aload 0
getfield android/support/v4/widget/eg/B [I
iload 7
iaload
aload 0
getfield android/support/v4/widget/eg/u I
iand
ifeq L5
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
invokevirtual android/support/v4/widget/ej/c()V
goto L5
L10:
aload 0
getfield android/support/v4/widget/eg/m I
iconst_2
if_icmpne L5
aload 0
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
astore 1
aload 1
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
if_acmpne L5
aload 0
aload 1
iload 7
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;I)Z
pop
goto L5
L4:
aload 0
getfield android/support/v4/widget/eg/o [F
ifnull L5
aload 0
getfield android/support/v4/widget/eg/p [F
ifnull L5
aload 1
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;)I
istore 8
iconst_0
istore 6
L11:
iload 6
iload 8
if_icmpge L12
aload 1
iload 6
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 9
aload 1
iload 6
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 2
aload 1
iload 6
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/eg/o [F
iload 9
faload
fsub
fstore 4
aload 0
getfield android/support/v4/widget/eg/p [F
iload 9
faload
fstore 5
aload 0
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
astore 13
aload 13
ifnull L13
aload 0
aload 13
fload 4
invokespecial android/support/v4/widget/eg/a(Landroid/view/View;F)Z
ifeq L13
iconst_1
istore 7
L14:
iload 7
ifeq L15
aload 13
invokevirtual android/view/View/getLeft()I
istore 10
fload 4
f2i
istore 11
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 13
iload 11
iload 10
iadd
invokevirtual android/support/v4/widget/ej/a(Landroid/view/View;I)I
istore 11
aload 13
invokevirtual android/view/View/getTop()I
pop
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 13
invokevirtual android/support/v4/widget/ej/c(Landroid/view/View;)I
pop
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 13
invokevirtual android/support/v4/widget/ej/b(Landroid/view/View;)I
istore 12
iload 12
ifeq L12
iload 12
ifle L15
iload 11
iload 10
if_icmpne L15
L12:
aload 0
aload 1
invokespecial android/support/v4/widget/eg/c(Landroid/view/MotionEvent;)V
goto L5
L13:
iconst_0
istore 7
goto L14
L15:
aload 0
fload 4
fload 3
fload 5
fsub
iload 9
invokespecial android/support/v4/widget/eg/b(FFI)V
aload 0
getfield android/support/v4/widget/eg/m I
iconst_1
if_icmpeq L12
iload 7
ifeq L16
aload 0
aload 13
iload 9
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;I)Z
ifne L12
L16:
iload 6
iconst_1
iadd
istore 6
goto L11
L7:
aload 0
aload 1
iload 6
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
invokespecial android/support/v4/widget/eg/d(I)V
goto L5
L3:
aload 0
invokevirtual android/support/v4/widget/eg/a()V
goto L5
L8:
iconst_0
ireturn
.limit locals 14
.limit stack 4
.end method

.method public final a(Landroid/view/View;II)Z
aload 0
aload 1
putfield android/support/v4/widget/eg/v Landroid/view/View;
aload 0
iconst_m1
putfield android/support/v4/widget/eg/A I
aload 0
iload 2
iload 3
iconst_0
iconst_0
invokespecial android/support/v4/widget/eg/a(IIII)Z
istore 4
iload 4
ifne L0
aload 0
getfield android/support/v4/widget/eg/m I
ifne L0
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
ifnull L0
aload 0
aconst_null
putfield android/support/v4/widget/eg/v Landroid/view/View;
L0:
iload 4
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final b(II)Landroid/view/View;
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
invokevirtual android/view/ViewGroup/getChildCount()I
iconst_1
isub
istore 3
L0:
iload 3
iflt L1
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
iload 3
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 4
iload 1
aload 4
invokevirtual android/view/View/getLeft()I
if_icmplt L2
iload 1
aload 4
invokevirtual android/view/View/getRight()I
if_icmpge L2
iload 2
aload 4
invokevirtual android/view/View/getTop()I
if_icmplt L2
iload 2
aload 4
invokevirtual android/view/View/getBottom()I
if_icmpge L2
aload 4
areturn
L2:
iload 3
iconst_1
isub
istore 3
goto L0
L1:
aconst_null
areturn
.limit locals 5
.limit stack 2
.end method

.method public final b()V
aload 0
invokevirtual android/support/v4/widget/eg/a()V
aload 0
getfield android/support/v4/widget/eg/m I
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/b()I
pop
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/c()I
pop
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/g()V
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/b()I
istore 1
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/c()I
pop
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 1
invokevirtual android/support/v4/widget/ej/b(Landroid/view/View;I)V
L0:
aload 0
iconst_0
invokevirtual android/support/v4/widget/eg/b(I)V
return
.limit locals 2
.limit stack 3
.end method

.method final b(I)V
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
aload 0
getfield android/support/v4/widget/eg/M Ljava/lang/Runnable;
invokevirtual android/view/ViewGroup/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
getfield android/support/v4/widget/eg/m I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v4/widget/eg/m I
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
iload 1
invokevirtual android/support/v4/widget/ej/a(I)V
aload 0
getfield android/support/v4/widget/eg/m I
ifne L0
aload 0
aconst_null
putfield android/support/v4/widget/eg/v Landroid/view/View;
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/view/MotionEvent;)V
iconst_0
istore 5
iconst_0
istore 6
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 8
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
istore 7
iload 8
ifne L0
aload 0
invokevirtual android/support/v4/widget/eg/a()V
L0:
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
ifnonnull L1
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
L1:
aload 0
getfield android/support/v4/widget/eg/F Landroid/view/VelocityTracker;
aload 1
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
iload 8
tableswitch 0
L2
L3
L4
L5
L6
L7
L8
default : L6
L6:
return
L2:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 5
aload 0
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
astore 1
aload 0
fload 2
fload 3
iload 5
invokespecial android/support/v4/widget/eg/a(FFI)V
aload 0
aload 1
iload 5
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;I)Z
pop
aload 0
getfield android/support/v4/widget/eg/B [I
iload 5
iaload
aload 0
getfield android/support/v4/widget/eg/u I
iand
ifeq L6
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
invokevirtual android/support/v4/widget/ej/c()V
return
L7:
aload 1
iload 7
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 5
aload 1
iload 7
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 2
aload 1
iload 7
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 3
aload 0
fload 2
fload 3
iload 5
invokespecial android/support/v4/widget/eg/a(FFI)V
aload 0
getfield android/support/v4/widget/eg/m I
ifne L9
aload 0
aload 0
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
iload 5
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;I)Z
pop
aload 0
getfield android/support/v4/widget/eg/B [I
iload 5
iaload
aload 0
getfield android/support/v4/widget/eg/u I
iand
ifeq L6
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
invokevirtual android/support/v4/widget/ej/c()V
return
L9:
fload 2
f2i
istore 6
fload 3
f2i
istore 7
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 6
iload 7
invokestatic android/support/v4/widget/eg/b(Landroid/view/View;II)Z
ifeq L6
aload 0
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 5
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;I)Z
pop
return
L4:
aload 0
getfield android/support/v4/widget/eg/m I
iconst_1
if_icmpne L10
aload 1
aload 0
getfield android/support/v4/widget/eg/A I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 5
aload 1
iload 5
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 2
aload 1
iload 5
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/eg/q [F
aload 0
getfield android/support/v4/widget/eg/A I
faload
fsub
f2i
istore 7
fload 3
aload 0
getfield android/support/v4/widget/eg/r [F
aload 0
getfield android/support/v4/widget/eg/A I
faload
fsub
f2i
istore 8
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getLeft()I
iload 7
iadd
istore 6
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getTop()I
pop
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getLeft()I
istore 10
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getTop()I
istore 9
iload 6
istore 5
iload 7
ifeq L11
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 6
invokevirtual android/support/v4/widget/ej/a(Landroid/view/View;I)I
istore 5
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 5
iload 10
isub
invokevirtual android/view/View/offsetLeftAndRight(I)V
L11:
iload 8
ifeq L12
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/support/v4/widget/ej/c(Landroid/view/View;)I
istore 6
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 6
iload 9
isub
invokevirtual android/view/View/offsetTopAndBottom(I)V
L12:
iload 7
ifne L13
iload 8
ifeq L14
L13:
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 5
invokevirtual android/support/v4/widget/ej/b(Landroid/view/View;I)V
L14:
aload 0
aload 1
invokespecial android/support/v4/widget/eg/c(Landroid/view/MotionEvent;)V
return
L10:
aload 1
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;)I
istore 7
iload 6
istore 5
L15:
iload 5
iload 7
if_icmpge L16
aload 1
iload 5
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 6
aload 1
iload 5
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 2
aload 1
iload 5
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/eg/o [F
iload 6
faload
fsub
fstore 4
aload 0
fload 4
fload 3
aload 0
getfield android/support/v4/widget/eg/p [F
iload 6
faload
fsub
iload 6
invokespecial android/support/v4/widget/eg/b(FFI)V
aload 0
getfield android/support/v4/widget/eg/m I
iconst_1
if_icmpeq L16
aload 0
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
astore 11
aload 0
aload 11
fload 4
invokespecial android/support/v4/widget/eg/a(Landroid/view/View;F)Z
ifeq L17
aload 0
aload 11
iload 6
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;I)Z
ifne L16
L17:
iload 5
iconst_1
iadd
istore 5
goto L15
L16:
aload 0
aload 1
invokespecial android/support/v4/widget/eg/c(Landroid/view/MotionEvent;)V
return
L8:
aload 1
iload 7
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 6
aload 0
getfield android/support/v4/widget/eg/m I
iconst_1
if_icmpne L18
iload 6
aload 0
getfield android/support/v4/widget/eg/A I
if_icmpne L18
aload 1
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;)I
istore 7
L19:
iload 5
iload 7
if_icmpge L20
aload 1
iload 5
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
istore 8
iload 8
aload 0
getfield android/support/v4/widget/eg/A I
if_icmpeq L21
aload 1
iload 5
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
fstore 2
aload 1
iload 5
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
fstore 3
aload 0
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
if_acmpne L21
aload 0
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 8
invokespecial android/support/v4/widget/eg/b(Landroid/view/View;I)Z
ifeq L21
aload 0
getfield android/support/v4/widget/eg/A I
istore 5
L22:
iload 5
iconst_m1
if_icmpne L18
aload 0
invokespecial android/support/v4/widget/eg/l()V
L18:
aload 0
iload 6
invokespecial android/support/v4/widget/eg/d(I)V
return
L21:
iload 5
iconst_1
iadd
istore 5
goto L19
L3:
aload 0
getfield android/support/v4/widget/eg/m I
iconst_1
if_icmpne L23
aload 0
invokespecial android/support/v4/widget/eg/l()V
L23:
aload 0
invokevirtual android/support/v4/widget/eg/a()V
return
L5:
aload 0
getfield android/support/v4/widget/eg/m I
iconst_1
if_icmpne L24
aload 0
fconst_0
invokespecial android/support/v4/widget/eg/c(F)V
L24:
aload 0
invokevirtual android/support/v4/widget/eg/a()V
return
L20:
iconst_m1
istore 5
goto L22
.limit locals 12
.limit stack 5
.end method

.method public final c()Z
iconst_0
istore 6
aload 0
getfield android/support/v4/widget/eg/m I
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/f()Z
istore 7
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/b()I
istore 1
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/c()I
istore 2
iload 1
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getLeft()I
isub
istore 3
iload 2
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
invokevirtual android/view/View/getTop()I
isub
istore 4
iload 3
ifeq L1
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 3
invokevirtual android/view/View/offsetLeftAndRight(I)V
L1:
iload 4
ifeq L2
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 4
invokevirtual android/view/View/offsetTopAndBottom(I)V
L2:
iload 3
ifne L3
iload 4
ifeq L4
L3:
aload 0
getfield android/support/v4/widget/eg/I Landroid/support/v4/widget/ej;
aload 0
getfield android/support/v4/widget/eg/v Landroid/view/View;
iload 1
invokevirtual android/support/v4/widget/ej/b(Landroid/view/View;I)V
L4:
iload 7
istore 5
iload 7
ifeq L5
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
astore 8
iload 7
istore 5
iload 1
aload 8
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 8
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/h(Ljava/lang/Object;)I 1
if_icmpne L5
iload 7
istore 5
iload 2
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/d()I
if_icmpne L5
aload 0
getfield android/support/v4/widget/eg/H Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/g()V
iconst_0
istore 5
L5:
iload 5
ifne L0
aload 0
getfield android/support/v4/widget/eg/K Landroid/view/ViewGroup;
aload 0
getfield android/support/v4/widget/eg/M Ljava/lang/Runnable;
invokevirtual android/view/ViewGroup/post(Ljava/lang/Runnable;)Z
pop
L0:
iload 6
istore 5
aload 0
getfield android/support/v4/widget/eg/m I
iconst_2
if_icmpne L6
iconst_1
istore 5
L6:
iload 5
ireturn
.limit locals 9
.limit stack 3
.end method
