.bytecode 50.0
.class final synchronized android/support/v4/widget/bg
.super java/lang/Object

.field final 'a' Landroid/graphics/RectF;

.field final 'b' Landroid/graphics/Paint;

.field final 'c' Landroid/graphics/Paint;

.field 'd' F

.field 'e' F

.field 'f' F

.field 'g' F

.field 'h' F

.field 'i' [I

.field 'j' I

.field 'k' F

.field 'l' F

.field 'm' F

.field 'n' Z

.field 'o' Landroid/graphics/Path;

.field 'p' F

.field 'q' D

.field 'r' I

.field 's' I

.field 't' I

.field final 'u' Landroid/graphics/Paint;

.field 'v' I

.field 'w' I

.field private final 'x' Landroid/graphics/drawable/Drawable$Callback;

.method public <init>(Landroid/graphics/drawable/Drawable$Callback;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/graphics/RectF
dup
invokespecial android/graphics/RectF/<init>()V
putfield android/support/v4/widget/bg/a Landroid/graphics/RectF;
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
aload 0
fconst_0
putfield android/support/v4/widget/bg/d F
aload 0
fconst_0
putfield android/support/v4/widget/bg/e F
aload 0
fconst_0
putfield android/support/v4/widget/bg/f F
aload 0
ldc_w 5.0F
putfield android/support/v4/widget/bg/g F
aload 0
ldc_w 2.5F
putfield android/support/v4/widget/bg/h F
aload 0
new android/graphics/Paint
dup
iconst_1
invokespecial android/graphics/Paint/<init>(I)V
putfield android/support/v4/widget/bg/u Landroid/graphics/Paint;
aload 0
aload 1
putfield android/support/v4/widget/bg/x Landroid/graphics/drawable/Drawable$Callback;
aload 0
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
getstatic android/graphics/Paint$Cap/SQUARE Landroid/graphics/Paint$Cap;
invokevirtual android/graphics/Paint/setStrokeCap(Landroid/graphics/Paint$Cap;)V
aload 0
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
iconst_1
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
aload 0
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
getstatic android/graphics/Paint$Style/STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 0
getfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
getstatic android/graphics/Paint$Style/FILL Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 0
getfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
iconst_1
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(D)V
aload 0
dload 1
putfield android/support/v4/widget/bg/q D
return
.limit locals 3
.limit stack 3
.end method

.method private a(FF)V
aload 0
fload 1
f2i
putfield android/support/v4/widget/bg/r I
aload 0
fload 2
f2i
putfield android/support/v4/widget/bg/s I
return
.limit locals 3
.limit stack 2
.end method

.method private a(II)V
iload 1
iload 2
invokestatic java/lang/Math/min(II)I
i2f
fstore 3
aload 0
getfield android/support/v4/widget/bg/q D
dconst_0
dcmpg
ifle L0
fload 3
fconst_0
fcmpg
ifge L1
L0:
aload 0
getfield android/support/v4/widget/bg/g F
fconst_2
fdiv
f2d
invokestatic java/lang/Math/ceil(D)D
d2f
fstore 3
L2:
aload 0
fload 3
putfield android/support/v4/widget/bg/h F
return
L1:
fload 3
fconst_2
fdiv
f2d
aload 0
getfield android/support/v4/widget/bg/q D
dsub
d2f
fstore 3
goto L2
.limit locals 4
.limit stack 4
.end method

.method private a(Landroid/graphics/Canvas;FFLandroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/bg/n Z
ifeq L0
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
ifnonnull L1
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
getstatic android/graphics/Path$FillType/EVEN_ODD Landroid/graphics/Path$FillType;
invokevirtual android/graphics/Path/setFillType(Landroid/graphics/Path$FillType;)V
L2:
aload 0
getfield android/support/v4/widget/bg/h F
f2i
iconst_2
idiv
i2f
fstore 5
aload 0
getfield android/support/v4/widget/bg/p F
fstore 6
aload 0
getfield android/support/v4/widget/bg/q D
dconst_0
invokestatic java/lang/Math/cos(D)D
dmul
aload 4
invokevirtual android/graphics/Rect/exactCenterX()F
f2d
dadd
d2f
fstore 7
aload 0
getfield android/support/v4/widget/bg/q D
dconst_0
invokestatic java/lang/Math/sin(D)D
dmul
aload 4
invokevirtual android/graphics/Rect/exactCenterY()F
f2d
dadd
d2f
fstore 8
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
fconst_0
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 0
getfield android/support/v4/widget/bg/r I
i2f
aload 0
getfield android/support/v4/widget/bg/p F
fmul
fconst_0
invokevirtual android/graphics/Path/lineTo(FF)V
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 0
getfield android/support/v4/widget/bg/r I
i2f
aload 0
getfield android/support/v4/widget/bg/p F
fmul
fconst_2
fdiv
aload 0
getfield android/support/v4/widget/bg/s I
i2f
aload 0
getfield android/support/v4/widget/bg/p F
fmul
invokevirtual android/graphics/Path/lineTo(FF)V
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
fload 7
fload 5
fload 6
fmul
fsub
fload 8
invokevirtual android/graphics/Path/offset(FF)V
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
invokevirtual android/graphics/Path/close()V
aload 0
getfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
aload 0
getfield android/support/v4/widget/bg/w I
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
fload 2
fload 3
fadd
ldc_w 5.0F
fsub
aload 4
invokevirtual android/graphics/Rect/exactCenterX()F
aload 4
invokevirtual android/graphics/Rect/exactCenterY()F
invokevirtual android/graphics/Canvas/rotate(FFF)V
aload 1
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 0
getfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
L0:
return
L1:
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
invokevirtual android/graphics/Path/reset()V
goto L2
.limit locals 9
.limit stack 4
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/bg/a Landroid/graphics/RectF;
astore 9
aload 9
aload 2
invokevirtual android/graphics/RectF/set(Landroid/graphics/Rect;)V
aload 9
aload 0
getfield android/support/v4/widget/bg/h F
aload 0
getfield android/support/v4/widget/bg/h F
invokevirtual android/graphics/RectF/inset(FF)V
aload 0
getfield android/support/v4/widget/bg/d F
aload 0
getfield android/support/v4/widget/bg/f F
fadd
ldc_w 360.0F
fmul
fstore 3
aload 0
getfield android/support/v4/widget/bg/e F
aload 0
getfield android/support/v4/widget/bg/f F
fadd
ldc_w 360.0F
fmul
fload 3
fsub
fstore 4
aload 0
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
aload 0
getfield android/support/v4/widget/bg/w I
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
aload 9
fload 3
fload 4
iconst_0
aload 0
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V
aload 0
getfield android/support/v4/widget/bg/n Z
ifeq L0
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
ifnonnull L1
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
getstatic android/graphics/Path$FillType/EVEN_ODD Landroid/graphics/Path$FillType;
invokevirtual android/graphics/Path/setFillType(Landroid/graphics/Path$FillType;)V
L2:
aload 0
getfield android/support/v4/widget/bg/h F
f2i
iconst_2
idiv
i2f
fstore 5
aload 0
getfield android/support/v4/widget/bg/p F
fstore 6
aload 0
getfield android/support/v4/widget/bg/q D
dconst_0
invokestatic java/lang/Math/cos(D)D
dmul
aload 2
invokevirtual android/graphics/Rect/exactCenterX()F
f2d
dadd
d2f
fstore 7
aload 0
getfield android/support/v4/widget/bg/q D
dconst_0
invokestatic java/lang/Math/sin(D)D
dmul
aload 2
invokevirtual android/graphics/Rect/exactCenterY()F
f2d
dadd
d2f
fstore 8
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
fconst_0
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 0
getfield android/support/v4/widget/bg/r I
i2f
aload 0
getfield android/support/v4/widget/bg/p F
fmul
fconst_0
invokevirtual android/graphics/Path/lineTo(FF)V
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 0
getfield android/support/v4/widget/bg/r I
i2f
aload 0
getfield android/support/v4/widget/bg/p F
fmul
fconst_2
fdiv
aload 0
getfield android/support/v4/widget/bg/s I
i2f
aload 0
getfield android/support/v4/widget/bg/p F
fmul
invokevirtual android/graphics/Path/lineTo(FF)V
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
fload 7
fload 5
fload 6
fmul
fsub
fload 8
invokevirtual android/graphics/Path/offset(FF)V
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
invokevirtual android/graphics/Path/close()V
aload 0
getfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
aload 0
getfield android/support/v4/widget/bg/w I
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
fload 3
fload 4
fadd
ldc_w 5.0F
fsub
aload 2
invokevirtual android/graphics/Rect/exactCenterX()F
aload 2
invokevirtual android/graphics/Rect/exactCenterY()F
invokevirtual android/graphics/Canvas/rotate(FFF)V
aload 1
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 0
getfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
L0:
aload 0
getfield android/support/v4/widget/bg/t I
sipush 255
if_icmpge L3
aload 0
getfield android/support/v4/widget/bg/u Landroid/graphics/Paint;
aload 0
getfield android/support/v4/widget/bg/v I
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
getfield android/support/v4/widget/bg/u Landroid/graphics/Paint;
sipush 255
aload 0
getfield android/support/v4/widget/bg/t I
isub
invokevirtual android/graphics/Paint/setAlpha(I)V
aload 1
aload 2
invokevirtual android/graphics/Rect/exactCenterX()F
aload 2
invokevirtual android/graphics/Rect/exactCenterY()F
aload 2
invokevirtual android/graphics/Rect/width()I
iconst_2
idiv
i2f
aload 0
getfield android/support/v4/widget/bg/u Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawCircle(FFFLandroid/graphics/Paint;)V
L3:
return
L1:
aload 0
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
invokevirtual android/graphics/Path/reset()V
goto L2
.limit locals 10
.limit stack 6
.end method

.method private a(Landroid/graphics/ColorFilter;)V
aload 0
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
aload 1
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 0
invokevirtual android/support/v4/widget/bg/d()V
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)V
aload 0
iload 1
putfield android/support/v4/widget/bg/v I
return
.limit locals 2
.limit stack 2
.end method

.method private c(I)V
aload 0
iload 1
putfield android/support/v4/widget/bg/w I
return
.limit locals 2
.limit stack 2
.end method

.method private d(F)V
aload 0
fload 1
putfield android/support/v4/widget/bg/g F
aload 0
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
fload 1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 0
invokevirtual android/support/v4/widget/bg/d()V
return
.limit locals 2
.limit stack 2
.end method

.method private d(I)V
aload 0
iload 1
putfield android/support/v4/widget/bg/t I
return
.limit locals 2
.limit stack 2
.end method

.method private e()I
aload 0
getfield android/support/v4/widget/bg/i [I
aload 0
invokevirtual android/support/v4/widget/bg/a()I
iaload
ireturn
.limit locals 1
.limit stack 2
.end method

.method private e(F)V
fload 1
aload 0
getfield android/support/v4/widget/bg/p F
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/v4/widget/bg/p F
aload 0
invokevirtual android/support/v4/widget/bg/d()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private f()V
aload 0
aload 0
invokevirtual android/support/v4/widget/bg/a()I
invokevirtual android/support/v4/widget/bg/a(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private g()I
aload 0
getfield android/support/v4/widget/bg/t I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()F
aload 0
getfield android/support/v4/widget/bg/g F
freturn
.limit locals 1
.limit stack 1
.end method

.method private i()F
aload 0
getfield android/support/v4/widget/bg/d F
freturn
.limit locals 1
.limit stack 1
.end method

.method private j()F
aload 0
getfield android/support/v4/widget/bg/k F
freturn
.limit locals 1
.limit stack 1
.end method

.method private k()F
aload 0
getfield android/support/v4/widget/bg/l F
freturn
.limit locals 1
.limit stack 1
.end method

.method private l()I
aload 0
getfield android/support/v4/widget/bg/i [I
aload 0
getfield android/support/v4/widget/bg/j I
iaload
ireturn
.limit locals 1
.limit stack 2
.end method

.method private m()F
aload 0
getfield android/support/v4/widget/bg/e F
freturn
.limit locals 1
.limit stack 1
.end method

.method private n()F
aload 0
getfield android/support/v4/widget/bg/f F
freturn
.limit locals 1
.limit stack 1
.end method

.method private o()F
aload 0
getfield android/support/v4/widget/bg/h F
freturn
.limit locals 1
.limit stack 1
.end method

.method private p()D
aload 0
getfield android/support/v4/widget/bg/q D
dreturn
.limit locals 1
.limit stack 2
.end method

.method private q()F
aload 0
getfield android/support/v4/widget/bg/m F
freturn
.limit locals 1
.limit stack 1
.end method

.method final a()I
aload 0
getfield android/support/v4/widget/bg/j I
iconst_1
iadd
aload 0
getfield android/support/v4/widget/bg/i [I
arraylength
irem
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a(F)V
aload 0
fload 1
putfield android/support/v4/widget/bg/d F
aload 0
invokevirtual android/support/v4/widget/bg/d()V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(I)V
aload 0
iload 1
putfield android/support/v4/widget/bg/j I
aload 0
aload 0
getfield android/support/v4/widget/bg/i [I
aload 0
getfield android/support/v4/widget/bg/j I
iaload
putfield android/support/v4/widget/bg/w I
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Z)V
aload 0
getfield android/support/v4/widget/bg/n Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v4/widget/bg/n Z
aload 0
invokevirtual android/support/v4/widget/bg/d()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a([I)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
aload 1
putfield android/support/v4/widget/bg/i [I
aload 0
iconst_0
invokevirtual android/support/v4/widget/bg/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b()V
aload 0
aload 0
getfield android/support/v4/widget/bg/d F
putfield android/support/v4/widget/bg/k F
aload 0
aload 0
getfield android/support/v4/widget/bg/e F
putfield android/support/v4/widget/bg/l F
aload 0
aload 0
getfield android/support/v4/widget/bg/f F
putfield android/support/v4/widget/bg/m F
return
.limit locals 1
.limit stack 2
.end method

.method public final b(F)V
aload 0
fload 1
putfield android/support/v4/widget/bg/e F
aload 0
invokevirtual android/support/v4/widget/bg/d()V
return
.limit locals 2
.limit stack 2
.end method

.method public final c()V
aload 0
fconst_0
putfield android/support/v4/widget/bg/k F
aload 0
fconst_0
putfield android/support/v4/widget/bg/l F
aload 0
fconst_0
putfield android/support/v4/widget/bg/m F
aload 0
fconst_0
invokevirtual android/support/v4/widget/bg/a(F)V
aload 0
fconst_0
invokevirtual android/support/v4/widget/bg/b(F)V
aload 0
fconst_0
invokevirtual android/support/v4/widget/bg/c(F)V
return
.limit locals 1
.limit stack 2
.end method

.method public final c(F)V
aload 0
fload 1
putfield android/support/v4/widget/bg/f F
aload 0
invokevirtual android/support/v4/widget/bg/d()V
return
.limit locals 2
.limit stack 2
.end method

.method final d()V
aload 0
getfield android/support/v4/widget/bg/x Landroid/graphics/drawable/Drawable$Callback;
aconst_null
invokeinterface android/graphics/drawable/Drawable$Callback/invalidateDrawable(Landroid/graphics/drawable/Drawable;)V 1
return
.limit locals 1
.limit stack 2
.end method
