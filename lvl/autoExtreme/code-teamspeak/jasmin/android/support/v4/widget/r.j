.bytecode 50.0
.class public synchronized abstract android/support/v4/widget/r
.super android/widget/BaseAdapter
.implements android/support/v4/widget/w
.implements android/widget/Filterable

.field public static final 'j' I = 1

.annotation visible Ljava/lang/Deprecated;
.end annotation
.end field

.field public static final 'k' I = 2


.field protected 'a' Z

.field protected 'b' Z

.field public 'c' Landroid/database/Cursor;

.field public 'd' Landroid/content/Context;

.field protected 'e' I

.field protected 'f' Landroid/support/v4/widget/t;

.field protected 'g' Landroid/database/DataSetObserver;

.field protected 'h' Landroid/support/v4/widget/v;

.field protected 'i' Landroid/widget/FilterQueryProvider;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
aload 1
aconst_null
iconst_1
invokespecial android/support/v4/widget/r/a(Landroid/content/Context;Landroid/database/Cursor;I)V
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/database/Cursor;)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
aload 1
aload 2
iconst_1
invokespecial android/support/v4/widget/r/a(Landroid/content/Context;Landroid/database/Cursor;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/database/Cursor;I)V
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
aload 1
aload 2
iload 3
invokespecial android/support/v4/widget/r/a(Landroid/content/Context;Landroid/database/Cursor;I)V
return
.limit locals 4
.limit stack 4
.end method

.method private a(Landroid/content/Context;Landroid/database/Cursor;I)V
iconst_1
istore 5
iload 3
iconst_1
iand
iconst_1
if_icmpne L0
iload 3
iconst_2
ior
istore 3
aload 0
iconst_1
putfield android/support/v4/widget/r/b Z
L1:
aload 2
ifnull L2
L3:
aload 0
aload 2
putfield android/support/v4/widget/r/c Landroid/database/Cursor;
aload 0
iload 5
putfield android/support/v4/widget/r/a Z
aload 0
aload 1
putfield android/support/v4/widget/r/d Landroid/content/Context;
iload 5
ifeq L4
aload 2
ldc "_id"
invokeinterface android/database/Cursor/getColumnIndexOrThrow(Ljava/lang/String;)I 1
istore 4
L5:
aload 0
iload 4
putfield android/support/v4/widget/r/e I
iload 3
iconst_2
iand
iconst_2
if_icmpne L6
aload 0
new android/support/v4/widget/t
dup
aload 0
invokespecial android/support/v4/widget/t/<init>(Landroid/support/v4/widget/r;)V
putfield android/support/v4/widget/r/f Landroid/support/v4/widget/t;
aload 0
new android/support/v4/widget/u
dup
aload 0
iconst_0
invokespecial android/support/v4/widget/u/<init>(Landroid/support/v4/widget/r;B)V
putfield android/support/v4/widget/r/g Landroid/database/DataSetObserver;
L7:
iload 5
ifeq L8
aload 0
getfield android/support/v4/widget/r/f Landroid/support/v4/widget/t;
ifnull L9
aload 2
aload 0
getfield android/support/v4/widget/r/f Landroid/support/v4/widget/t;
invokeinterface android/database/Cursor/registerContentObserver(Landroid/database/ContentObserver;)V 1
L9:
aload 0
getfield android/support/v4/widget/r/g Landroid/database/DataSetObserver;
ifnull L8
aload 2
aload 0
getfield android/support/v4/widget/r/g Landroid/database/DataSetObserver;
invokeinterface android/database/Cursor/registerDataSetObserver(Landroid/database/DataSetObserver;)V 1
L8:
return
L0:
aload 0
iconst_0
putfield android/support/v4/widget/r/b Z
goto L1
L2:
iconst_0
istore 5
goto L3
L4:
iconst_m1
istore 4
goto L5
L6:
aload 0
aconst_null
putfield android/support/v4/widget/r/f Landroid/support/v4/widget/t;
aload 0
aconst_null
putfield android/support/v4/widget/r/g Landroid/database/DataSetObserver;
goto L7
.limit locals 6
.limit stack 5
.end method

.method private a(Landroid/content/Context;Landroid/database/Cursor;Z)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
iload 3
ifeq L0
iconst_1
istore 4
L1:
aload 0
aload 1
aload 2
iload 4
invokespecial android/support/v4/widget/r/a(Landroid/content/Context;Landroid/database/Cursor;I)V
return
L0:
iconst_2
istore 4
goto L1
.limit locals 5
.limit stack 4
.end method

.method private a(Landroid/widget/FilterQueryProvider;)V
aload 0
aload 1
putfield android/support/v4/widget/r/i Landroid/widget/FilterQueryProvider;
return
.limit locals 2
.limit stack 2
.end method

.method private c()Landroid/widget/FilterQueryProvider;
aload 0
getfield android/support/v4/widget/r/i Landroid/widget/FilterQueryProvider;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Landroid/database/Cursor;
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
areturn
.limit locals 1
.limit stack 1
.end method

.method public a(Ljava/lang/CharSequence;)Landroid/database/Cursor;
aload 0
getfield android/support/v4/widget/r/i Landroid/widget/FilterQueryProvider;
ifnull L0
aload 0
getfield android/support/v4/widget/r/i Landroid/widget/FilterQueryProvider;
aload 1
invokeinterface android/widget/FilterQueryProvider/runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor; 1
areturn
L0:
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
areturn
.limit locals 2
.limit stack 2
.end method

.method public abstract a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public a(Landroid/database/Cursor;)V
aload 0
aload 1
invokevirtual android/support/v4/widget/r/b(Landroid/database/Cursor;)Landroid/database/Cursor;
astore 1
aload 1
ifnull L0
aload 1
invokeinterface android/database/Cursor/close()V 0
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public abstract a(Landroid/view/View;Landroid/database/Cursor;)V
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
aload 1
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
if_acmpne L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
astore 2
aload 2
ifnull L1
aload 0
getfield android/support/v4/widget/r/f Landroid/support/v4/widget/t;
ifnull L2
aload 2
aload 0
getfield android/support/v4/widget/r/f Landroid/support/v4/widget/t;
invokeinterface android/database/Cursor/unregisterContentObserver(Landroid/database/ContentObserver;)V 1
L2:
aload 0
getfield android/support/v4/widget/r/g Landroid/database/DataSetObserver;
ifnull L1
aload 2
aload 0
getfield android/support/v4/widget/r/g Landroid/database/DataSetObserver;
invokeinterface android/database/Cursor/unregisterDataSetObserver(Landroid/database/DataSetObserver;)V 1
L1:
aload 0
aload 1
putfield android/support/v4/widget/r/c Landroid/database/Cursor;
aload 1
ifnull L3
aload 0
getfield android/support/v4/widget/r/f Landroid/support/v4/widget/t;
ifnull L4
aload 1
aload 0
getfield android/support/v4/widget/r/f Landroid/support/v4/widget/t;
invokeinterface android/database/Cursor/registerContentObserver(Landroid/database/ContentObserver;)V 1
L4:
aload 0
getfield android/support/v4/widget/r/g Landroid/database/DataSetObserver;
ifnull L5
aload 1
aload 0
getfield android/support/v4/widget/r/g Landroid/database/DataSetObserver;
invokeinterface android/database/Cursor/registerDataSetObserver(Landroid/database/DataSetObserver;)V 1
L5:
aload 0
aload 1
ldc "_id"
invokeinterface android/database/Cursor/getColumnIndexOrThrow(Ljava/lang/String;)I 1
putfield android/support/v4/widget/r/e I
aload 0
iconst_1
putfield android/support/v4/widget/r/a Z
aload 0
invokevirtual android/support/v4/widget/r/notifyDataSetChanged()V
aload 2
areturn
L3:
aload 0
iconst_m1
putfield android/support/v4/widget/r/e I
aload 0
iconst_0
putfield android/support/v4/widget/r/a Z
aload 0
invokevirtual android/support/v4/widget/r/notifyDataSetInvalidated()V
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public b(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
aload 1
aload 2
aload 3
invokevirtual android/support/v4/widget/r/a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
areturn
.limit locals 4
.limit stack 4
.end method

.method protected final b()V
aload 0
getfield android/support/v4/widget/r/b Z
ifeq L0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
ifnull L0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
invokeinterface android/database/Cursor/isClosed()Z 0
ifne L0
aload 0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
invokeinterface android/database/Cursor/requery()Z 0
putfield android/support/v4/widget/r/a Z
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
aload 1
ifnonnull L0
ldc ""
areturn
L0:
aload 1
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method

.method public getCount()I
aload 0
getfield android/support/v4/widget/r/a Z
ifeq L0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
ifnull L0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
invokeinterface android/database/Cursor/getCount()I 0
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield android/support/v4/widget/r/a Z
ifeq L0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
pop
aload 2
astore 4
aload 2
ifnonnull L1
aload 0
aload 0
getfield android/support/v4/widget/r/d Landroid/content/Context;
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
aload 3
invokevirtual android/support/v4/widget/r/b(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
astore 4
L1:
aload 0
aload 4
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
invokevirtual android/support/v4/widget/r/a(Landroid/view/View;Landroid/database/Cursor;)V
aload 4
areturn
L0:
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method public getFilter()Landroid/widget/Filter;
aload 0
getfield android/support/v4/widget/r/h Landroid/support/v4/widget/v;
ifnonnull L0
aload 0
new android/support/v4/widget/v
dup
aload 0
invokespecial android/support/v4/widget/v/<init>(Landroid/support/v4/widget/w;)V
putfield android/support/v4/widget/r/h Landroid/support/v4/widget/v;
L0:
aload 0
getfield android/support/v4/widget/r/h Landroid/support/v4/widget/v;
areturn
.limit locals 1
.limit stack 4
.end method

.method public getItem(I)Ljava/lang/Object;
aload 0
getfield android/support/v4/widget/r/a Z
ifeq L0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
ifnull L0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
pop
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public getItemId(I)J
lconst_0
lstore 4
lload 4
lstore 2
aload 0
getfield android/support/v4/widget/r/a Z
ifeq L0
lload 4
lstore 2
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
ifnull L0
lload 4
lstore 2
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
ifeq L0
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
aload 0
getfield android/support/v4/widget/r/e I
invokeinterface android/database/Cursor/getLong(I)J 1
lstore 2
L0:
lload 2
lreturn
.limit locals 6
.limit stack 2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield android/support/v4/widget/r/a Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "this should only be called when the cursor is valid"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
iload 1
invokeinterface android/database/Cursor/moveToPosition(I)Z 1
ifne L1
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "couldn't move cursor to position "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 2
astore 4
aload 2
ifnonnull L2
aload 0
aload 0
getfield android/support/v4/widget/r/d Landroid/content/Context;
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
aload 3
invokevirtual android/support/v4/widget/r/a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
astore 4
L2:
aload 0
aload 4
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
invokevirtual android/support/v4/widget/r/a(Landroid/view/View;Landroid/database/Cursor;)V
aload 4
areturn
.limit locals 5
.limit stack 5
.end method

.method public hasStableIds()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
