.bytecode 50.0
.class public final synchronized android/support/v4/widget/o
.super android/widget/ProgressBar

.field private static final 'a' I = 500


.field private static final 'b' I = 500


.field private 'c' J

.field private 'd' Z

.field private 'e' Z

.field private 'f' Z

.field private final 'g' Ljava/lang/Runnable;

.field private final 'h' Ljava/lang/Runnable;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/widget/o/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/widget/ProgressBar/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
ldc2_w -1L
putfield android/support/v4/widget/o/c J
aload 0
iconst_0
putfield android/support/v4/widget/o/d Z
aload 0
iconst_0
putfield android/support/v4/widget/o/e Z
aload 0
iconst_0
putfield android/support/v4/widget/o/f Z
aload 0
new android/support/v4/widget/p
dup
aload 0
invokespecial android/support/v4/widget/p/<init>(Landroid/support/v4/widget/o;)V
putfield android/support/v4/widget/o/g Ljava/lang/Runnable;
aload 0
new android/support/v4/widget/q
dup
aload 0
invokespecial android/support/v4/widget/q/<init>(Landroid/support/v4/widget/o;)V
putfield android/support/v4/widget/o/h Ljava/lang/Runnable;
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/widget/o;J)J
aload 0
lload 1
putfield android/support/v4/widget/o/c J
lload 1
lreturn
.limit locals 3
.limit stack 3
.end method

.method private a()V
aload 0
aload 0
getfield android/support/v4/widget/o/g Ljava/lang/Runnable;
invokevirtual android/support/v4/widget/o/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
aload 0
getfield android/support/v4/widget/o/h Ljava/lang/Runnable;
invokevirtual android/support/v4/widget/o/removeCallbacks(Ljava/lang/Runnable;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/widget/o;)Z
aload 0
iconst_0
putfield android/support/v4/widget/o/d Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private b()V
aload 0
iconst_1
putfield android/support/v4/widget/o/f Z
aload 0
aload 0
getfield android/support/v4/widget/o/h Ljava/lang/Runnable;
invokevirtual android/support/v4/widget/o/removeCallbacks(Ljava/lang/Runnable;)Z
pop
invokestatic java/lang/System/currentTimeMillis()J
aload 0
getfield android/support/v4/widget/o/c J
lsub
lstore 1
lload 1
ldc2_w 500L
lcmp
ifge L0
aload 0
getfield android/support/v4/widget/o/c J
ldc2_w -1L
lcmp
ifne L1
L0:
aload 0
bipush 8
invokevirtual android/support/v4/widget/o/setVisibility(I)V
L2:
return
L1:
aload 0
getfield android/support/v4/widget/o/d Z
ifne L2
aload 0
aload 0
getfield android/support/v4/widget/o/g Ljava/lang/Runnable;
ldc2_w 500L
lload 1
lsub
invokevirtual android/support/v4/widget/o/postDelayed(Ljava/lang/Runnable;J)Z
pop
aload 0
iconst_1
putfield android/support/v4/widget/o/d Z
return
.limit locals 3
.limit stack 6
.end method

.method static synthetic b(Landroid/support/v4/widget/o;)Z
aload 0
iconst_0
putfield android/support/v4/widget/o/e Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private c()V
aload 0
ldc2_w -1L
putfield android/support/v4/widget/o/c J
aload 0
iconst_0
putfield android/support/v4/widget/o/f Z
aload 0
aload 0
getfield android/support/v4/widget/o/g Ljava/lang/Runnable;
invokevirtual android/support/v4/widget/o/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
getfield android/support/v4/widget/o/e Z
ifne L0
aload 0
aload 0
getfield android/support/v4/widget/o/h Ljava/lang/Runnable;
ldc2_w 500L
invokevirtual android/support/v4/widget/o/postDelayed(Ljava/lang/Runnable;J)Z
pop
aload 0
iconst_1
putfield android/support/v4/widget/o/e Z
L0:
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic c(Landroid/support/v4/widget/o;)Z
aload 0
getfield android/support/v4/widget/o/f Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final onAttachedToWindow()V
aload 0
invokespecial android/widget/ProgressBar/onAttachedToWindow()V
aload 0
invokespecial android/support/v4/widget/o/a()V
return
.limit locals 1
.limit stack 1
.end method

.method public final onDetachedFromWindow()V
aload 0
invokespecial android/widget/ProgressBar/onDetachedFromWindow()V
aload 0
invokespecial android/support/v4/widget/o/a()V
return
.limit locals 1
.limit stack 1
.end method
