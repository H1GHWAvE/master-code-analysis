.bytecode 50.0
.class final synchronized android/support/v4/widget/db
.super android/support/v4/view/a

.field final synthetic 'a' Landroid/support/v4/widget/SlidingPaneLayout;

.field private final 'c' Landroid/graphics/Rect;

.method <init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
aload 0
aload 1
putfield android/support/v4/widget/db/a Landroid/support/v4/widget/SlidingPaneLayout;
aload 0
invokespecial android/support/v4/view/a/<init>()V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/db/c Landroid/graphics/Rect;
return
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/support/v4/view/a/q;Landroid/support/v4/view/a/q;)V
aload 0
getfield android/support/v4/widget/db/c Landroid/graphics/Rect;
astore 4
aload 2
aload 4
invokevirtual android/support/v4/view/a/q/a(Landroid/graphics/Rect;)V
aload 1
aload 4
invokevirtual android/support/v4/view/a/q/b(Landroid/graphics/Rect;)V
aload 2
aload 4
invokevirtual android/support/v4/view/a/q/c(Landroid/graphics/Rect;)V
aload 1
aload 4
invokevirtual android/support/v4/view/a/q/d(Landroid/graphics/Rect;)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/e()Z
invokevirtual android/support/v4/view/a/q/c(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/k()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/a(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/l()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/n()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/c(Ljava/lang/CharSequence;)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/j()Z
invokevirtual android/support/v4/view/a/q/h(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/h()Z
invokevirtual android/support/v4/view/a/q/f(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/c()Z
invokevirtual android/support/v4/view/a/q/a(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/d()Z
invokevirtual android/support/v4/view/a/q/b(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/f()Z
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/g()Z
invokevirtual android/support/v4/view/a/q/e(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/i()Z
invokevirtual android/support/v4/view/a/q/g(Z)V
aload 1
aload 2
invokevirtual android/support/v4/view/a/q/b()I
invokevirtual android/support/v4/view/a/q/a(I)V
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 2
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
invokeinterface android/support/v4/view/a/w/D(Ljava/lang/Object;)I 1
istore 3
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 1
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/w/g(Ljava/lang/Object;I)V 2
return
.limit locals 5
.limit stack 3
.end method

.method private b(Landroid/view/View;)Z
aload 0
getfield android/support/v4/widget/db/a Landroid/support/v4/widget/SlidingPaneLayout;
aload 1
invokevirtual android/support/v4/widget/SlidingPaneLayout/b(Landroid/view/View;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 2
invokestatic android/support/v4/view/a/q/a(Landroid/support/v4/view/a/q;)Landroid/support/v4/view/a/q;
astore 5
aload 0
aload 1
aload 5
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 0
getfield android/support/v4/widget/db/c Landroid/graphics/Rect;
astore 6
aload 5
aload 6
invokevirtual android/support/v4/view/a/q/a(Landroid/graphics/Rect;)V
aload 2
aload 6
invokevirtual android/support/v4/view/a/q/b(Landroid/graphics/Rect;)V
aload 5
aload 6
invokevirtual android/support/v4/view/a/q/c(Landroid/graphics/Rect;)V
aload 2
aload 6
invokevirtual android/support/v4/view/a/q/d(Landroid/graphics/Rect;)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/e()Z
invokevirtual android/support/v4/view/a/q/c(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/k()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/a(Ljava/lang/CharSequence;)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/l()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/n()Ljava/lang/CharSequence;
invokevirtual android/support/v4/view/a/q/c(Ljava/lang/CharSequence;)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/j()Z
invokevirtual android/support/v4/view/a/q/h(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/h()Z
invokevirtual android/support/v4/view/a/q/f(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/c()Z
invokevirtual android/support/v4/view/a/q/a(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/d()Z
invokevirtual android/support/v4/view/a/q/b(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/f()Z
invokevirtual android/support/v4/view/a/q/d(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/g()Z
invokevirtual android/support/v4/view/a/q/e(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/i()Z
invokevirtual android/support/v4/view/a/q/g(Z)V
aload 2
aload 5
invokevirtual android/support/v4/view/a/q/b()I
invokevirtual android/support/v4/view/a/q/a(I)V
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 5
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
invokeinterface android/support/v4/view/a/w/D(Ljava/lang/Object;)I 1
istore 3
getstatic android/support/v4/view/a/q/a Landroid/support/v4/view/a/w;
aload 2
getfield android/support/v4/view/a/q/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/w/g(Ljava/lang/Object;I)V 2
aload 5
invokevirtual android/support/v4/view/a/q/o()V
aload 2
ldc android/support/v4/widget/SlidingPaneLayout
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 2
aload 1
invokevirtual android/support/v4/view/a/q/b(Landroid/view/View;)V
aload 1
invokestatic android/support/v4/view/cx/g(Landroid/view/View;)Landroid/view/ViewParent;
astore 1
aload 1
instanceof android/view/View
ifeq L0
aload 2
aload 1
checkcast android/view/View
invokevirtual android/support/v4/view/a/q/d(Landroid/view/View;)V
L0:
aload 0
getfield android/support/v4/widget/db/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 4
iconst_0
istore 3
L1:
iload 3
iload 4
if_icmpge L2
aload 0
getfield android/support/v4/widget/db/a Landroid/support/v4/widget/SlidingPaneLayout;
iload 3
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 0
aload 1
invokespecial android/support/v4/widget/db/b(Landroid/view/View;)Z
ifne L3
aload 1
invokevirtual android/view/View/getVisibility()I
ifne L3
aload 1
iconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
aload 2
aload 1
invokevirtual android/support/v4/view/a/q/c(Landroid/view/View;)V
L3:
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
return
.limit locals 7
.limit stack 3
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 2
ldc android/support/v4/widget/SlidingPaneLayout
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 0
aload 2
invokespecial android/support/v4/widget/db/b(Landroid/view/View;)Z
ifne L0
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/v4/view/a/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method
