.bytecode 50.0
.class public final synchronized android/support/v4/widget/ba
.super android/support/v4/widget/a

.field private final 'g' Landroid/widget/ListView;

.method public <init>(Landroid/widget/ListView;)V
aload 0
aload 1
invokespecial android/support/v4/widget/a/<init>(Landroid/view/View;)V
aload 0
aload 1
putfield android/support/v4/widget/ba/g Landroid/widget/ListView;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(I)V
aload 0
getfield android/support/v4/widget/ba/g Landroid/widget/ListView;
astore 3
aload 3
invokevirtual android/widget/ListView/getFirstVisiblePosition()I
istore 2
iload 2
iconst_m1
if_icmpne L0
L1:
return
L0:
aload 3
iconst_0
invokevirtual android/widget/ListView/getChildAt(I)Landroid/view/View;
astore 4
aload 4
ifnull L1
aload 3
iload 2
aload 4
invokevirtual android/view/View/getTop()I
iload 1
isub
invokevirtual android/widget/ListView/setSelectionFromTop(II)V
return
.limit locals 5
.limit stack 4
.end method

.method public final a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)Z
aload 0
getfield android/support/v4/widget/ba/g Landroid/widget/ListView;
astore 5
aload 5
invokevirtual android/widget/ListView/getCount()I
istore 2
iload 2
ifne L0
L1:
iconst_0
ireturn
L0:
aload 5
invokevirtual android/widget/ListView/getChildCount()I
istore 3
aload 5
invokevirtual android/widget/ListView/getFirstVisiblePosition()I
istore 4
iload 1
ifle L2
iload 4
iload 3
iadd
iload 2
if_icmplt L3
aload 5
iload 3
iconst_1
isub
invokevirtual android/widget/ListView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getBottom()I
aload 5
invokevirtual android/widget/ListView/getHeight()I
if_icmple L1
L3:
iconst_1
ireturn
L2:
iload 1
ifge L1
iload 4
ifgt L3
aload 5
iconst_0
invokevirtual android/widget/ListView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getTop()I
iflt L3
iconst_0
ireturn
.limit locals 6
.limit stack 3
.end method
