.bytecode 50.0
.class public final synchronized android/support/v4/widget/ca
.super java/lang/Object

.field static final 'c' I = 16


.field private static final 'd' Ljava/lang/String; = "ScrollerCompat"

.field 'a' Ljava/lang/Object;

.field 'b' Landroid/support/v4/widget/cb;

.method private <init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V
aload 0
invokespecial java/lang/Object/<init>()V
iload 1
bipush 14
if_icmplt L0
aload 0
new android/support/v4/widget/ce
dup
invokespecial android/support/v4/widget/ce/<init>()V
putfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
L1:
aload 0
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 2
aload 3
invokeinterface android/support/v4/widget/cb/a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object; 2
putfield android/support/v4/widget/ca/a Ljava/lang/Object;
return
L0:
iload 1
bipush 9
if_icmplt L2
aload 0
new android/support/v4/widget/cd
dup
invokespecial android/support/v4/widget/cd/<init>()V
putfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
goto L1
L2:
aload 0
new android/support/v4/widget/cc
dup
invokespecial android/support/v4/widget/cc/<init>()V
putfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
goto L1
.limit locals 4
.limit stack 4
.end method

.method <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
aload 0
getstatic android/os/Build$VERSION/SDK_INT I
aload 1
aload 2
invokespecial android/support/v4/widget/ca/<init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V
return
.limit locals 3
.limit stack 4
.end method

.method private static a(Landroid/content/Context;)Landroid/support/v4/widget/ca;
aload 0
aconst_null
invokestatic android/support/v4/widget/ca/a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;
new android/support/v4/widget/ca
dup
aload 0
aload 1
invokespecial android/support/v4/widget/ca/<init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(III)V
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 1
iload 2
iload 3
invokeinterface android/support/v4/widget/cb/a(Ljava/lang/Object;III)V 4
return
.limit locals 4
.limit stack 5
.end method

.method private a(IIIII)V
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 1
iload 2
iload 3
iload 4
iload 5
invokeinterface android/support/v4/widget/cb/a(Ljava/lang/Object;IIIII)V 6
return
.limit locals 6
.limit stack 7
.end method

.method private b(III)V
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 1
iload 2
iload 3
invokeinterface android/support/v4/widget/cb/b(Ljava/lang/Object;III)V 4
return
.limit locals 4
.limit stack 5
.end method

.method private b(IIIII)V
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 1
iload 2
iload 3
iload 4
iload 5
invokeinterface android/support/v4/widget/cb/b(Ljava/lang/Object;IIIII)V 6
return
.limit locals 6
.limit stack 7
.end method

.method private c(III)V
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 1
iload 2
iload 3
invokeinterface android/support/v4/widget/cb/c(Ljava/lang/Object;III)V 4
return
.limit locals 4
.limit stack 5
.end method

.method private h()I
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/h(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private i()Z
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/g(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a(IIIIIIII)V
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 1
iload 2
iload 3
iload 4
iload 5
iload 6
iload 7
iload 8
invokeinterface android/support/v4/widget/cb/a(Ljava/lang/Object;IIIIIIII)V 9
return
.limit locals 9
.limit stack 10
.end method

.method public final a()Z
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/a(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final b()I
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/b(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final c()I
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/c(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final d()I
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/i(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final e()F
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/d(Ljava/lang/Object;)F 1
freturn
.limit locals 1
.limit stack 2
.end method

.method public final f()Z
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/e(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final g()V
aload 0
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 0
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/cb/f(Ljava/lang/Object;)V 1
return
.limit locals 1
.limit stack 2
.end method
