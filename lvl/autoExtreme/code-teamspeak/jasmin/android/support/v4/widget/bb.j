.bytecode 50.0
.class final synchronized android/support/v4/widget/bb
.super android/graphics/drawable/Drawable
.implements android/graphics/drawable/Animatable

.field private static final 'A' F = 0.8F


.field static final 'a' I = 0


.field static final 'b' I = 1


.field private static final 'e' Landroid/view/animation/Interpolator;

.field private static final 'f' Landroid/view/animation/Interpolator;

.field private static final 'g' F = 1080.0F


.field private static final 'h' I = 40


.field private static final 'i' F = 8.75F


.field private static final 'j' F = 2.5F


.field private static final 'k' I = 56


.field private static final 'l' F = 12.5F


.field private static final 'm' F = 3.0F


.field private static final 'o' F = 0.75F


.field private static final 'p' F = 0.5F


.field private static final 'q' F = 0.5F


.field private static final 'r' I = 1332


.field private static final 's' F = 5.0F


.field private static final 'v' I = 10


.field private static final 'w' I = 5


.field private static final 'x' F = 5.0F


.field private static final 'y' I = 12


.field private static final 'z' I = 6


.field private 'B' Landroid/content/res/Resources;

.field private 'C' Landroid/view/View;

.field private 'D' Landroid/view/animation/Animation;

.field private 'E' F

.field private 'F' D

.field private 'G' D

.field private final 'H' Landroid/graphics/drawable/Drawable$Callback;

.field final 'c' Landroid/support/v4/widget/bg;

.field 'd' Z

.field private final 'n' [I

.field private final 't' Ljava/util/ArrayList;

.field private 'u' F

.method static <clinit>()V
new android/view/animation/LinearInterpolator
dup
invokespecial android/view/animation/LinearInterpolator/<init>()V
putstatic android/support/v4/widget/bb/e Landroid/view/animation/Interpolator;
new android/support/v4/view/b/b
dup
invokespecial android/support/v4/view/b/b/<init>()V
putstatic android/support/v4/widget/bb/f Landroid/view/animation/Interpolator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Landroid/view/View;)V
aload 0
invokespecial android/graphics/drawable/Drawable/<init>()V
aload 0
iconst_1
newarray int
dup
iconst_0
ldc_w -16777216
iastore
putfield android/support/v4/widget/bb/n [I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/widget/bb/t Ljava/util/ArrayList;
aload 0
new android/support/v4/widget/be
dup
aload 0
invokespecial android/support/v4/widget/be/<init>(Landroid/support/v4/widget/bb;)V
putfield android/support/v4/widget/bb/H Landroid/graphics/drawable/Drawable$Callback;
aload 0
aload 2
putfield android/support/v4/widget/bb/C Landroid/view/View;
aload 0
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
putfield android/support/v4/widget/bb/B Landroid/content/res/Resources;
aload 0
new android/support/v4/widget/bg
dup
aload 0
getfield android/support/v4/widget/bb/H Landroid/graphics/drawable/Drawable$Callback;
invokespecial android/support/v4/widget/bg/<init>(Landroid/graphics/drawable/Drawable$Callback;)V
putfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
aload 0
getfield android/support/v4/widget/bb/n [I
invokevirtual android/support/v4/widget/bg/a([I)V
aload 0
iconst_1
invokevirtual android/support/v4/widget/bb/a(I)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
astore 1
new android/support/v4/widget/bc
dup
aload 0
aload 1
invokespecial android/support/v4/widget/bc/<init>(Landroid/support/v4/widget/bb;Landroid/support/v4/widget/bg;)V
astore 2
aload 2
iconst_m1
invokevirtual android/view/animation/Animation/setRepeatCount(I)V
aload 2
iconst_1
invokevirtual android/view/animation/Animation/setRepeatMode(I)V
aload 2
getstatic android/support/v4/widget/bb/e Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 2
new android/support/v4/widget/bd
dup
aload 0
aload 1
invokespecial android/support/v4/widget/bd/<init>(Landroid/support/v4/widget/bb;Landroid/support/v4/widget/bg;)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
aload 2
putfield android/support/v4/widget/bb/D Landroid/view/animation/Animation;
return
.limit locals 3
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/widget/bb;)F
aload 0
getfield android/support/v4/widget/bb/E F
freturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/v4/widget/bb;F)F
aload 0
fload 1
putfield android/support/v4/widget/bb/E F
fload 1
freturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/widget/bg;)F
aload 0
invokestatic android/support/v4/widget/bb/b(Landroid/support/v4/widget/bg;)F
freturn
.limit locals 1
.limit stack 1
.end method

.method private static a(FII)I
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
istore 5
iload 5
bipush 24
ishr
sipush 255
iand
istore 1
iload 5
bipush 16
ishr
sipush 255
iand
istore 3
iload 5
bipush 8
ishr
sipush 255
iand
istore 4
iload 5
sipush 255
iand
istore 5
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
istore 2
iload 2
bipush 24
ishr
sipush 255
iand
iload 1
isub
i2f
fload 0
fmul
f2i
istore 6
iload 2
bipush 16
ishr
sipush 255
iand
iload 3
isub
i2f
fload 0
fmul
f2i
istore 7
iload 2
bipush 8
ishr
sipush 255
iand
iload 4
isub
i2f
fload 0
fmul
f2i
istore 8
iload 5
iload 2
sipush 255
iand
iload 5
isub
i2f
fload 0
fmul
f2i
iadd
iload 1
iload 6
iadd
bipush 24
ishl
iload 3
iload 7
iadd
bipush 16
ishl
ior
iload 8
iload 4
iadd
bipush 8
ishl
ior
ior
ireturn
.limit locals 9
.limit stack 4
.end method

.method static synthetic a()Landroid/view/animation/Interpolator;
getstatic android/support/v4/widget/bb/f Landroid/view/animation/Interpolator;
areturn
.limit locals 0
.limit stack 1
.end method

.method private a(DDDDFF)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
astore 13
aload 0
getfield android/support/v4/widget/bb/B Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 11
aload 0
fload 11
f2d
dload 1
dmul
putfield android/support/v4/widget/bb/F D
aload 0
fload 11
f2d
dload 3
dmul
putfield android/support/v4/widget/bb/G D
dload 7
d2f
fload 11
fmul
fstore 12
aload 13
fload 12
putfield android/support/v4/widget/bg/g F
aload 13
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
fload 12
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 13
invokevirtual android/support/v4/widget/bg/d()V
aload 13
fload 11
f2d
dload 5
dmul
putfield android/support/v4/widget/bg/q D
aload 13
iconst_0
invokevirtual android/support/v4/widget/bg/a(I)V
aload 13
fload 9
fload 11
fmul
f2i
putfield android/support/v4/widget/bg/r I
aload 13
fload 11
fload 10
fmul
f2i
putfield android/support/v4/widget/bg/s I
aload 0
getfield android/support/v4/widget/bb/F D
d2i
aload 0
getfield android/support/v4/widget/bb/G D
d2i
invokestatic java/lang/Math/min(II)I
i2f
fstore 9
aload 13
getfield android/support/v4/widget/bg/q D
dconst_0
dcmpg
ifle L0
fload 9
fconst_0
fcmpg
ifge L1
L0:
aload 13
getfield android/support/v4/widget/bg/g F
fconst_2
fdiv
f2d
invokestatic java/lang/Math/ceil(D)D
d2f
fstore 9
L2:
aload 13
fload 9
putfield android/support/v4/widget/bg/h F
return
L1:
fload 9
fconst_2
fdiv
f2d
aload 13
getfield android/support/v4/widget/bg/q D
dsub
d2f
fstore 9
goto L2
.limit locals 14
.limit stack 5
.end method

.method static synthetic a(FLandroid/support/v4/widget/bg;)V
fload 0
aload 1
invokestatic android/support/v4/widget/bb/c(FLandroid/support/v4/widget/bg;)V
aload 1
getfield android/support/v4/widget/bg/m F
ldc_w 0.8F
fdiv
f2d
invokestatic java/lang/Math/floor(D)D
dconst_1
dadd
d2f
fstore 2
aload 1
invokestatic android/support/v4/widget/bb/b(Landroid/support/v4/widget/bg;)F
fstore 3
aload 1
getfield android/support/v4/widget/bg/k F
fstore 4
aload 1
aload 1
getfield android/support/v4/widget/bg/l F
fload 3
fsub
aload 1
getfield android/support/v4/widget/bg/k F
fsub
fload 0
fmul
fload 4
fadd
invokevirtual android/support/v4/widget/bg/a(F)V
aload 1
aload 1
getfield android/support/v4/widget/bg/l F
invokevirtual android/support/v4/widget/bg/b(F)V
aload 1
getfield android/support/v4/widget/bg/m F
fstore 3
aload 1
fload 2
aload 1
getfield android/support/v4/widget/bg/m F
fsub
fload 0
fmul
fload 3
fadd
invokevirtual android/support/v4/widget/bg/c(F)V
return
.limit locals 5
.limit stack 4
.end method

.method private transient a([I)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
aload 1
invokevirtual android/support/v4/widget/bg/a([I)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
iconst_0
invokevirtual android/support/v4/widget/bg/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private b()F
aload 0
getfield android/support/v4/widget/bb/u F
freturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Landroid/support/v4/widget/bg;)F
aload 0
getfield android/support/v4/widget/bg/g F
f2d
ldc2_w 6.283185307179586D
aload 0
getfield android/support/v4/widget/bg/q D
dmul
ddiv
invokestatic java/lang/Math/toRadians(D)D
d2f
freturn
.limit locals 1
.limit stack 6
.end method

.method static synthetic b(FLandroid/support/v4/widget/bg;)V
fload 0
aload 1
invokestatic android/support/v4/widget/bb/c(FLandroid/support/v4/widget/bg;)V
return
.limit locals 2
.limit stack 2
.end method

.method private c()V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
astore 1
new android/support/v4/widget/bc
dup
aload 0
aload 1
invokespecial android/support/v4/widget/bc/<init>(Landroid/support/v4/widget/bb;Landroid/support/v4/widget/bg;)V
astore 2
aload 2
iconst_m1
invokevirtual android/view/animation/Animation/setRepeatCount(I)V
aload 2
iconst_1
invokevirtual android/view/animation/Animation/setRepeatMode(I)V
aload 2
getstatic android/support/v4/widget/bb/e Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 2
new android/support/v4/widget/bd
dup
aload 0
aload 1
invokespecial android/support/v4/widget/bd/<init>(Landroid/support/v4/widget/bb;Landroid/support/v4/widget/bg;)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
aload 2
putfield android/support/v4/widget/bb/D Landroid/view/animation/Animation;
return
.limit locals 3
.limit stack 5
.end method

.method private static c(FLandroid/support/v4/widget/bg;)V
fload 0
ldc_w 0.75F
fcmpl
ifle L0
fload 0
ldc_w 0.75F
fsub
ldc_w 0.25F
fdiv
fstore 0
aload 1
getfield android/support/v4/widget/bg/i [I
aload 1
getfield android/support/v4/widget/bg/j I
iaload
istore 2
aload 1
getfield android/support/v4/widget/bg/i [I
aload 1
invokevirtual android/support/v4/widget/bg/a()I
iaload
istore 6
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
istore 5
iload 5
bipush 24
ishr
sipush 255
iand
istore 2
iload 5
bipush 16
ishr
sipush 255
iand
istore 3
iload 5
bipush 8
ishr
sipush 255
iand
istore 4
iload 5
sipush 255
iand
istore 5
iload 6
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
istore 6
iload 6
bipush 24
ishr
sipush 255
iand
iload 2
isub
i2f
fload 0
fmul
f2i
istore 7
iload 6
bipush 16
ishr
sipush 255
iand
iload 3
isub
i2f
fload 0
fmul
f2i
istore 8
iload 6
bipush 8
ishr
sipush 255
iand
iload 4
isub
i2f
fload 0
fmul
f2i
istore 9
aload 1
fload 0
iload 6
sipush 255
iand
iload 5
isub
i2f
fmul
f2i
iload 5
iadd
iload 2
iload 7
iadd
bipush 24
ishl
iload 3
iload 8
iadd
bipush 16
ishl
ior
iload 9
iload 4
iadd
bipush 8
ishl
ior
ior
putfield android/support/v4/widget/bg/w I
L0:
return
.limit locals 10
.limit stack 5
.end method

.method private d(F)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
fload 1
invokevirtual android/support/v4/widget/bg/c(F)V
return
.limit locals 2
.limit stack 2
.end method

.method private static d(FLandroid/support/v4/widget/bg;)V
fload 0
aload 1
invokestatic android/support/v4/widget/bb/c(FLandroid/support/v4/widget/bg;)V
aload 1
getfield android/support/v4/widget/bg/m F
ldc_w 0.8F
fdiv
f2d
invokestatic java/lang/Math/floor(D)D
dconst_1
dadd
d2f
fstore 2
aload 1
invokestatic android/support/v4/widget/bb/b(Landroid/support/v4/widget/bg;)F
fstore 3
aload 1
getfield android/support/v4/widget/bg/k F
fstore 4
aload 1
aload 1
getfield android/support/v4/widget/bg/l F
fload 3
fsub
aload 1
getfield android/support/v4/widget/bg/k F
fsub
fload 0
fmul
fload 4
fadd
invokevirtual android/support/v4/widget/bg/a(F)V
aload 1
aload 1
getfield android/support/v4/widget/bg/l F
invokevirtual android/support/v4/widget/bg/b(F)V
aload 1
getfield android/support/v4/widget/bg/m F
fstore 3
aload 1
fload 2
aload 1
getfield android/support/v4/widget/bg/m F
fsub
fload 0
fmul
fload 3
fadd
invokevirtual android/support/v4/widget/bg/c(F)V
return
.limit locals 5
.limit stack 4
.end method

.method public final a(F)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
astore 2
fload 1
aload 2
getfield android/support/v4/widget/bg/p F
fcmpl
ifeq L0
aload 2
fload 1
putfield android/support/v4/widget/bg/p F
aload 2
invokevirtual android/support/v4/widget/bg/d()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public final a(I)V
.annotation invisibleparam 1 Landroid/support/v4/widget/bf;
.end annotation
iload 1
ifne L0
aload 0
ldc2_w 56.0D
ldc2_w 56.0D
ldc2_w 12.5D
ldc2_w 3.0D
ldc_w 12.0F
ldc_w 6.0F
invokespecial android/support/v4/widget/bb/a(DDDDFF)V
return
L0:
aload 0
ldc2_w 40.0D
ldc2_w 40.0D
ldc2_w 8.75D
ldc2_w 2.5D
ldc_w 10.0F
ldc_w 5.0F
invokespecial android/support/v4/widget/bb/a(DDDDFF)V
return
.limit locals 2
.limit stack 11
.end method

.method public final a(Z)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
iload 1
invokevirtual android/support/v4/widget/bg/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b(F)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
fconst_0
invokevirtual android/support/v4/widget/bg/a(F)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
fload 1
invokevirtual android/support/v4/widget/bg/b(F)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b(I)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
iload 1
putfield android/support/v4/widget/bg/v I
return
.limit locals 2
.limit stack 2
.end method

.method final c(F)V
aload 0
fload 1
putfield android/support/v4/widget/bb/u F
aload 0
invokevirtual android/support/v4/widget/bb/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public final draw(Landroid/graphics/Canvas;)V
aload 0
invokevirtual android/support/v4/widget/bb/getBounds()Landroid/graphics/Rect;
astore 9
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 8
aload 1
aload 0
getfield android/support/v4/widget/bb/u F
aload 9
invokevirtual android/graphics/Rect/exactCenterX()F
aload 9
invokevirtual android/graphics/Rect/exactCenterY()F
invokevirtual android/graphics/Canvas/rotate(FFF)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
astore 10
aload 10
getfield android/support/v4/widget/bg/a Landroid/graphics/RectF;
astore 11
aload 11
aload 9
invokevirtual android/graphics/RectF/set(Landroid/graphics/Rect;)V
aload 11
aload 10
getfield android/support/v4/widget/bg/h F
aload 10
getfield android/support/v4/widget/bg/h F
invokevirtual android/graphics/RectF/inset(FF)V
ldc_w 360.0F
aload 10
getfield android/support/v4/widget/bg/d F
aload 10
getfield android/support/v4/widget/bg/f F
fadd
fmul
fstore 2
aload 10
getfield android/support/v4/widget/bg/e F
aload 10
getfield android/support/v4/widget/bg/f F
fadd
ldc_w 360.0F
fmul
fload 2
fsub
fstore 3
aload 10
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
aload 10
getfield android/support/v4/widget/bg/w I
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
aload 11
fload 2
fload 3
iconst_0
aload 10
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V
aload 10
getfield android/support/v4/widget/bg/n Z
ifeq L0
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
ifnonnull L1
aload 10
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
getstatic android/graphics/Path$FillType/EVEN_ODD Landroid/graphics/Path$FillType;
invokevirtual android/graphics/Path/setFillType(Landroid/graphics/Path$FillType;)V
L2:
aload 10
getfield android/support/v4/widget/bg/h F
f2i
iconst_2
idiv
i2f
fstore 4
aload 10
getfield android/support/v4/widget/bg/p F
fstore 5
aload 10
getfield android/support/v4/widget/bg/q D
dconst_0
invokestatic java/lang/Math/cos(D)D
dmul
aload 9
invokevirtual android/graphics/Rect/exactCenterX()F
f2d
dadd
d2f
fstore 6
aload 10
getfield android/support/v4/widget/bg/q D
dconst_0
invokestatic java/lang/Math/sin(D)D
dmul
aload 9
invokevirtual android/graphics/Rect/exactCenterY()F
f2d
dadd
d2f
fstore 7
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
fconst_0
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 10
getfield android/support/v4/widget/bg/r I
i2f
aload 10
getfield android/support/v4/widget/bg/p F
fmul
fconst_0
invokevirtual android/graphics/Path/lineTo(FF)V
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 10
getfield android/support/v4/widget/bg/r I
i2f
aload 10
getfield android/support/v4/widget/bg/p F
fmul
fconst_2
fdiv
aload 10
getfield android/support/v4/widget/bg/s I
i2f
aload 10
getfield android/support/v4/widget/bg/p F
fmul
invokevirtual android/graphics/Path/lineTo(FF)V
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
fload 6
fload 4
fload 5
fmul
fsub
fload 7
invokevirtual android/graphics/Path/offset(FF)V
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
invokevirtual android/graphics/Path/close()V
aload 10
getfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
aload 10
getfield android/support/v4/widget/bg/w I
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
fload 2
fload 3
fadd
ldc_w 5.0F
fsub
aload 9
invokevirtual android/graphics/Rect/exactCenterX()F
aload 9
invokevirtual android/graphics/Rect/exactCenterY()F
invokevirtual android/graphics/Canvas/rotate(FFF)V
aload 1
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
aload 10
getfield android/support/v4/widget/bg/c Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
L0:
aload 10
getfield android/support/v4/widget/bg/t I
sipush 255
if_icmpge L3
aload 10
getfield android/support/v4/widget/bg/u Landroid/graphics/Paint;
aload 10
getfield android/support/v4/widget/bg/v I
invokevirtual android/graphics/Paint/setColor(I)V
aload 10
getfield android/support/v4/widget/bg/u Landroid/graphics/Paint;
sipush 255
aload 10
getfield android/support/v4/widget/bg/t I
isub
invokevirtual android/graphics/Paint/setAlpha(I)V
aload 1
aload 9
invokevirtual android/graphics/Rect/exactCenterX()F
aload 9
invokevirtual android/graphics/Rect/exactCenterY()F
aload 9
invokevirtual android/graphics/Rect/width()I
iconst_2
idiv
i2f
aload 10
getfield android/support/v4/widget/bg/u Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawCircle(FFFLandroid/graphics/Paint;)V
L3:
aload 1
iload 8
invokevirtual android/graphics/Canvas/restoreToCount(I)V
return
L1:
aload 10
getfield android/support/v4/widget/bg/o Landroid/graphics/Path;
invokevirtual android/graphics/Path/reset()V
goto L2
.limit locals 12
.limit stack 6
.end method

.method public final getAlpha()I
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
getfield android/support/v4/widget/bg/t I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getIntrinsicHeight()I
aload 0
getfield android/support/v4/widget/bb/G D
d2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final getIntrinsicWidth()I
aload 0
getfield android/support/v4/widget/bb/F D
d2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final getOpacity()I
bipush -3
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isRunning()Z
aload 0
getfield android/support/v4/widget/bb/t Ljava/util/ArrayList;
astore 3
aload 3
invokevirtual java/util/ArrayList/size()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 3
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/animation/Animation
astore 4
aload 4
invokevirtual android/view/animation/Animation/hasStarted()Z
ifeq L2
aload 4
invokevirtual android/view/animation/Animation/hasEnded()Z
ifne L2
iconst_1
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method public final setAlpha(I)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
iload 1
putfield android/support/v4/widget/bg/t I
return
.limit locals 2
.limit stack 2
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
astore 2
aload 2
getfield android/support/v4/widget/bg/b Landroid/graphics/Paint;
aload 1
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 2
invokevirtual android/support/v4/widget/bg/d()V
return
.limit locals 3
.limit stack 2
.end method

.method public final start()V
aload 0
getfield android/support/v4/widget/bb/D Landroid/view/animation/Animation;
invokevirtual android/view/animation/Animation/reset()V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
invokevirtual android/support/v4/widget/bg/b()V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
getfield android/support/v4/widget/bg/e F
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
getfield android/support/v4/widget/bg/d F
fcmpl
ifeq L0
aload 0
iconst_1
putfield android/support/v4/widget/bb/d Z
aload 0
getfield android/support/v4/widget/bb/D Landroid/view/animation/Animation;
ldc2_w 666L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/bb/C Landroid/view/View;
aload 0
getfield android/support/v4/widget/bb/D Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
return
L0:
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
iconst_0
invokevirtual android/support/v4/widget/bg/a(I)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
invokevirtual android/support/v4/widget/bg/c()V
aload 0
getfield android/support/v4/widget/bb/D Landroid/view/animation/Animation;
ldc2_w 1332L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 0
getfield android/support/v4/widget/bb/C Landroid/view/View;
aload 0
getfield android/support/v4/widget/bb/D Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 1
.limit stack 3
.end method

.method public final stop()V
aload 0
getfield android/support/v4/widget/bb/C Landroid/view/View;
invokevirtual android/view/View/clearAnimation()V
aload 0
fconst_0
invokevirtual android/support/v4/widget/bb/c(F)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
iconst_0
invokevirtual android/support/v4/widget/bg/a(Z)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
iconst_0
invokevirtual android/support/v4/widget/bg/a(I)V
aload 0
getfield android/support/v4/widget/bb/c Landroid/support/v4/widget/bg;
invokevirtual android/support/v4/widget/bg/c()V
return
.limit locals 1
.limit stack 2
.end method
