.bytecode 50.0
.class public final synchronized android/support/v4/widget/g
.super java/lang/Object

.field private static final 'a' Landroid/support/v4/widget/j;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 23
if_icmplt L0
new android/support/v4/widget/h
dup
invokespecial android/support/v4/widget/h/<init>()V
putstatic android/support/v4/widget/g/a Landroid/support/v4/widget/j;
return
L0:
iload 0
bipush 21
if_icmplt L1
new android/support/v4/widget/k
dup
invokespecial android/support/v4/widget/k/<init>()V
putstatic android/support/v4/widget/g/a Landroid/support/v4/widget/j;
return
L1:
new android/support/v4/widget/i
dup
invokespecial android/support/v4/widget/i/<init>()V
putstatic android/support/v4/widget/g/a Landroid/support/v4/widget/j;
return
.limit locals 1
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
.annotation invisible Landroid/support/a/z;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
getstatic android/support/v4/widget/g/a Landroid/support/v4/widget/j;
aload 0
invokeinterface android/support/v4/widget/j/a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
getstatic android/support/v4/widget/g/a Landroid/support/v4/widget/j;
aload 0
aload 1
invokeinterface android/support/v4/widget/j/a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method public static a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
getstatic android/support/v4/widget/g/a Landroid/support/v4/widget/j;
aload 0
aload 1
invokeinterface android/support/v4/widget/j/a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private static b(Landroid/widget/CompoundButton;)Landroid/content/res/ColorStateList;
.annotation invisible Landroid/support/a/z;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
getstatic android/support/v4/widget/g/a Landroid/support/v4/widget/j;
aload 0
invokeinterface android/support/v4/widget/j/b(Landroid/widget/CompoundButton;)Landroid/content/res/ColorStateList; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static c(Landroid/widget/CompoundButton;)Landroid/graphics/PorterDuff$Mode;
.annotation invisible Landroid/support/a/z;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
getstatic android/support/v4/widget/g/a Landroid/support/v4/widget/j;
aload 0
invokeinterface android/support/v4/widget/j/c(Landroid/widget/CompoundButton;)Landroid/graphics/PorterDuff$Mode; 1
areturn
.limit locals 1
.limit stack 2
.end method
