.bytecode 50.0
.class public final synchronized android/support/v4/widget/al
.super java/lang/Object

.field private static final 'b' Landroid/support/v4/widget/ao;

.field private 'a' Ljava/lang/Object;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/support/v4/widget/ap
dup
invokespecial android/support/v4/widget/ap/<init>()V
putstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
return
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L1
new android/support/v4/widget/an
dup
invokespecial android/support/v4/widget/an/<init>()V
putstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
return
L1:
new android/support/v4/widget/am
dup
invokespecial android/support/v4/widget/am/<init>()V
putstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 1
invokeinterface android/support/v4/widget/ao/a(Landroid/content/Context;)Ljava/lang/Object; 1
putfield android/support/v4/widget/al/a Ljava/lang/Object;
return
.limit locals 2
.limit stack 3
.end method

.method public final a(II)V
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 0
getfield android/support/v4/widget/al/a Ljava/lang/Object;
iload 1
iload 2
invokeinterface android/support/v4/widget/ao/a(Ljava/lang/Object;II)V 3
return
.limit locals 3
.limit stack 4
.end method

.method public final a()Z
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 0
getfield android/support/v4/widget/al/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/ao/a(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a(F)Z
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 0
getfield android/support/v4/widget/al/a Ljava/lang/Object;
fload 1
invokeinterface android/support/v4/widget/ao/a(Ljava/lang/Object;F)Z 2
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final a(FF)Z
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 0
getfield android/support/v4/widget/al/a Ljava/lang/Object;
fload 1
fload 2
invokeinterface android/support/v4/widget/ao/a(Ljava/lang/Object;FF)Z 3
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final a(I)Z
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 0
getfield android/support/v4/widget/al/a Ljava/lang/Object;
iload 1
invokeinterface android/support/v4/widget/ao/a(Ljava/lang/Object;I)Z 2
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/graphics/Canvas;)Z
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 0
getfield android/support/v4/widget/al/a Ljava/lang/Object;
aload 1
invokeinterface android/support/v4/widget/ao/a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z 2
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final b()V
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 0
getfield android/support/v4/widget/al/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/ao/b(Ljava/lang/Object;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method public final c()Z
getstatic android/support/v4/widget/al/b Landroid/support/v4/widget/ao;
aload 0
getfield android/support/v4/widget/al/a Ljava/lang/Object;
invokeinterface android/support/v4/widget/ao/c(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method
