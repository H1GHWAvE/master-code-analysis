.bytecode 50.0
.class public synchronized android/support/v4/app/ax
.super android/support/v4/app/Fragment
.implements android/content/DialogInterface$OnCancelListener
.implements android/content/DialogInterface$OnDismissListener

.field public static final 'a' I = 0


.field private static final 'at' Ljava/lang/String; = "android:savedDialogState"

.field private static final 'au' Ljava/lang/String; = "android:style"

.field private static final 'av' Ljava/lang/String; = "android:theme"

.field private static final 'aw' Ljava/lang/String; = "android:cancelable"

.field private static final 'ax' Ljava/lang/String; = "android:showsDialog"

.field private static final 'ay' Ljava/lang/String; = "android:backStackId"

.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 3


.field 'e' I

.field public 'f' I

.field 'g' Z

.field 'h' Z

.field 'i' I

.field public 'j' Landroid/app/Dialog;

.field 'k' Z

.field 'l' Z

.field 'm' Z

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
aload 0
iconst_0
putfield android/support/v4/app/ax/e I
aload 0
iconst_0
putfield android/support/v4/app/ax/f I
aload 0
iconst_1
putfield android/support/v4/app/ax/g Z
aload 0
iconst_1
putfield android/support/v4/app/ax/h Z
aload 0
iconst_m1
putfield android/support/v4/app/ax/i I
return
.limit locals 1
.limit stack 2
.end method

.method private A()I
.annotation invisible Landroid/support/a/ai;
.end annotation
aload 0
getfield android/support/v4/app/ax/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private B()Z
aload 0
getfield android/support/v4/app/ax/g Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private C()Z
aload 0
getfield android/support/v4/app/ax/h Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(Z)V
aload 0
getfield android/support/v4/app/ax/l Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield android/support/v4/app/ax/l Z
aload 0
iconst_0
putfield android/support/v4/app/ax/m Z
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ifnull L1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
invokevirtual android/app/Dialog/dismiss()V
aload 0
aconst_null
putfield android/support/v4/app/ax/j Landroid/app/Dialog;
L1:
aload 0
iconst_1
putfield android/support/v4/app/ax/k Z
aload 0
getfield android/support/v4/app/ax/i I
iflt L2
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 0
getfield android/support/v4/app/ax/i I
invokevirtual android/support/v4/app/bi/b(I)V
aload 0
iconst_m1
putfield android/support/v4/app/ax/i I
return
L2:
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 2
aload 2
aload 0
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
iload 1
ifeq L3
aload 2
invokevirtual android/support/v4/app/cd/j()I
pop
return
L3:
aload 2
invokevirtual android/support/v4/app/cd/i()I
pop
return
.limit locals 3
.limit stack 2
.end method

.method private e(Z)V
aload 0
iload 1
putfield android/support/v4/app/ax/h Z
return
.limit locals 2
.limit stack 2
.end method

.method private y()V
aload 0
iconst_1
invokespecial android/support/v4/app/ax/d(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private z()Landroid/app/Dialog;
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v4/app/cd;Ljava/lang/String;)I
aload 0
iconst_0
putfield android/support/v4/app/ax/l Z
aload 0
iconst_1
putfield android/support/v4/app/ax/m Z
aload 1
aload 0
aload 2
invokevirtual android/support/v4/app/cd/a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
aload 0
iconst_0
putfield android/support/v4/app/ax/k Z
aload 0
aload 1
invokevirtual android/support/v4/app/cd/i()I
putfield android/support/v4/app/ax/i I
aload 0
getfield android/support/v4/app/ax/i I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/app/Activity;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/app/Activity;)V
aload 0
getfield android/support/v4/app/ax/m Z
ifne L0
aload 0
iconst_0
putfield android/support/v4/app/ax/l Z
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public a(Landroid/app/Dialog;I)V
iload 2
tableswitch 1
L0
L0
L1
default : L2
L2:
return
L1:
aload 1
invokevirtual android/app/Dialog/getWindow()Landroid/view/Window;
bipush 24
invokevirtual android/view/Window/addFlags(I)V
L0:
aload 1
iconst_1
invokevirtual android/app/Dialog/requestWindowFeature(I)Z
pop
return
.limit locals 3
.limit stack 2
.end method

.method public a(Landroid/os/Bundle;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/ax/R I
ifne L0
iconst_1
istore 2
L1:
aload 0
iload 2
putfield android/support/v4/app/ax/h Z
aload 1
ifnull L2
aload 0
aload 1
ldc "android:style"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/ax/e I
aload 0
aload 1
ldc "android:theme"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/ax/f I
aload 0
aload 1
ldc "android:cancelable"
iconst_1
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;Z)Z
putfield android/support/v4/app/ax/g Z
aload 0
aload 1
ldc "android:showsDialog"
aload 0
getfield android/support/v4/app/ax/h Z
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;Z)Z
putfield android/support/v4/app/ax/h Z
aload 0
aload 1
ldc "android:backStackId"
iconst_m1
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/ax/i I
L2:
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method public a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
aload 0
iconst_0
putfield android/support/v4/app/ax/l Z
aload 0
iconst_1
putfield android/support/v4/app/ax/m Z
aload 1
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 1
aload 1
aload 0
aload 2
invokevirtual android/support/v4/app/cd/a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
aload 1
invokevirtual android/support/v4/app/cd/i()I
pop
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Z)V
aload 0
iload 1
putfield android/support/v4/app/ax/g Z
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ifnull L0
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
iload 1
invokevirtual android/app/Dialog/setCancelable(Z)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
aload 0
getfield android/support/v4/app/ax/h Z
ifne L0
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
areturn
L0:
aload 0
aload 0
invokevirtual android/support/v4/app/ax/d()Landroid/app/Dialog;
putfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ifnull L1
aload 0
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield android/support/v4/app/ax/e I
invokevirtual android/support/v4/app/ax/a(Landroid/app/Dialog;I)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
invokevirtual android/app/Dialog/getContext()Landroid/content/Context;
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
areturn
L1:
aload 0
getfield android/support/v4/app/ax/N Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
ldc "layout_inflater"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/LayoutInflater
areturn
.limit locals 2
.limit stack 3
.end method

.method public b()V
aload 0
iconst_0
invokespecial android/support/v4/app/ax/d(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public final c()V
aload 0
invokespecial android/support/v4/app/Fragment/c()V
aload 0
getfield android/support/v4/app/ax/m Z
ifne L0
aload 0
getfield android/support/v4/app/ax/l Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/app/ax/l Z
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/ax/h Z
ifne L0
L1:
return
L0:
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
astore 2
aload 2
ifnull L2
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
ifnull L3
new java/lang/IllegalStateException
dup
ldc "DialogFragment can not be attached to a container view"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 2
invokevirtual android/app/Dialog/setContentView(Landroid/view/View;)V
L2:
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
invokevirtual android/support/v4/app/ax/i()Landroid/support/v4/app/bb;
invokevirtual android/app/Dialog/setOwnerActivity(Landroid/app/Activity;)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield android/support/v4/app/ax/g Z
invokevirtual android/app/Dialog/setCancelable(Z)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
invokevirtual android/app/Dialog/setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
invokevirtual android/app/Dialog/setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
aload 1
ifnull L1
aload 1
ldc "android:savedDialogState"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
astore 1
aload 1
ifnull L1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 1
invokevirtual android/app/Dialog/onRestoreInstanceState(Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public d()Landroid/app/Dialog;
.annotation invisible Landroid/support/a/y;
.end annotation
new android/app/Dialog
dup
aload 0
invokevirtual android/support/v4/app/ax/i()Landroid/support/v4/app/bb;
aload 0
getfield android/support/v4/app/ax/f I
invokespecial android/app/Dialog/<init>(Landroid/content/Context;I)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final d(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/d(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ifnull L0
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
invokevirtual android/app/Dialog/onSaveInstanceState()Landroid/os/Bundle;
astore 2
aload 2
ifnull L0
aload 1
ldc "android:savedDialogState"
aload 2
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
L0:
aload 0
getfield android/support/v4/app/ax/e I
ifeq L1
aload 1
ldc "android:style"
aload 0
getfield android/support/v4/app/ax/e I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L1:
aload 0
getfield android/support/v4/app/ax/f I
ifeq L2
aload 1
ldc "android:theme"
aload 0
getfield android/support/v4/app/ax/f I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L2:
aload 0
getfield android/support/v4/app/ax/g Z
ifne L3
aload 1
ldc "android:cancelable"
aload 0
getfield android/support/v4/app/ax/g Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
L3:
aload 0
getfield android/support/v4/app/ax/h Z
ifne L4
aload 1
ldc "android:showsDialog"
aload 0
getfield android/support/v4/app/ax/h Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
L4:
aload 0
getfield android/support/v4/app/ax/i I
iconst_m1
if_icmpeq L5
aload 1
ldc "android:backStackId"
aload 0
getfield android/support/v4/app/ax/i I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L5:
return
.limit locals 3
.limit stack 3
.end method

.method public e()V
aload 0
invokespecial android/support/v4/app/Fragment/e()V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ifnull L0
aload 0
iconst_0
putfield android/support/v4/app/ax/k Z
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
invokevirtual android/app/Dialog/show()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public f()V
aload 0
invokespecial android/support/v4/app/Fragment/f()V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ifnull L0
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
invokevirtual android/app/Dialog/hide()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public final g()V
aload 0
invokespecial android/support/v4/app/Fragment/g()V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ifnull L0
aload 0
iconst_1
putfield android/support/v4/app/ax/k Z
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
invokevirtual android/app/Dialog/dismiss()V
aload 0
aconst_null
putfield android/support/v4/app/ax/j Landroid/app/Dialog;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
return
.limit locals 2
.limit stack 0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
aload 0
getfield android/support/v4/app/ax/k Z
ifne L0
aload 0
iconst_1
invokespecial android/support/v4/app/ax/d(Z)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final q_()V
aload 0
iconst_0
putfield android/support/v4/app/ax/e I
aload 0
getfield android/support/v4/app/ax/e I
iconst_2
if_icmpeq L0
aload 0
getfield android/support/v4/app/ax/e I
iconst_3
if_icmpne L1
L0:
aload 0
ldc_w 16973913
putfield android/support/v4/app/ax/f I
L1:
aload 0
ldc_w 2131165365
putfield android/support/v4/app/ax/f I
return
.limit locals 1
.limit stack 2
.end method
