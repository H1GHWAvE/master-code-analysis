.bytecode 50.0
.class final synchronized android/support/v4/app/BackStackState
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field final 'a' [I

.field final 'b' I

.field final 'c' I

.field final 'd' Ljava/lang/String;

.field final 'e' I

.field final 'f' I

.field final 'g' Ljava/lang/CharSequence;

.field final 'h' I

.field final 'i' Ljava/lang/CharSequence;

.field final 'j' Ljava/util/ArrayList;

.field final 'k' Ljava/util/ArrayList;

.method static <clinit>()V
new android/support/v4/app/aq
dup
invokespecial android/support/v4/app/aq/<init>()V
putstatic android/support/v4/app/BackStackState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcel;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual android/os/Parcel/createIntArray()[I
putfield android/support/v4/app/BackStackState/a [I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/app/BackStackState/b I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/app/BackStackState/c I
aload 0
aload 1
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
putfield android/support/v4/app/BackStackState/d Ljava/lang/String;
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/app/BackStackState/e I
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/app/BackStackState/f I
aload 0
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
putfield android/support/v4/app/BackStackState/g Ljava/lang/CharSequence;
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/app/BackStackState/h I
aload 0
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
putfield android/support/v4/app/BackStackState/i Ljava/lang/CharSequence;
aload 0
aload 1
invokevirtual android/os/Parcel/createStringArrayList()Ljava/util/ArrayList;
putfield android/support/v4/app/BackStackState/j Ljava/util/ArrayList;
aload 0
aload 1
invokevirtual android/os/Parcel/createStringArrayList()Ljava/util/ArrayList;
putfield android/support/v4/app/BackStackState/k Ljava/util/ArrayList;
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/support/v4/app/ak;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
astore 5
iconst_0
istore 2
L0:
aload 5
ifnull L1
iload 2
istore 3
aload 5
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
ifnull L2
iload 2
aload 5
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iadd
istore 3
L2:
aload 5
getfield android/support/v4/app/ao/a Landroid/support/v4/app/ao;
astore 5
iload 3
istore 2
goto L0
L1:
aload 0
iload 2
aload 1
getfield android/support/v4/app/ak/n I
bipush 7
imul
iadd
newarray int
putfield android/support/v4/app/BackStackState/a [I
aload 1
getfield android/support/v4/app/ak/u Z
ifne L3
new java/lang/IllegalStateException
dup
ldc "Not on back stack"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 1
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
astore 5
iconst_0
istore 2
L4:
aload 5
ifnull L5
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 6
iload 2
iconst_1
iadd
istore 3
aload 6
iload 2
aload 5
getfield android/support/v4/app/ao/c I
iastore
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 6
iload 3
iconst_1
iadd
istore 4
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
ifnull L6
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/z I
istore 2
L7:
aload 6
iload 3
iload 2
iastore
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 6
iload 4
iconst_1
iadd
istore 2
aload 6
iload 4
aload 5
getfield android/support/v4/app/ao/e I
iastore
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 6
iload 2
iconst_1
iadd
istore 3
aload 6
iload 2
aload 5
getfield android/support/v4/app/ao/f I
iastore
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 6
iload 3
iconst_1
iadd
istore 2
aload 6
iload 3
aload 5
getfield android/support/v4/app/ao/g I
iastore
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 6
iload 2
iconst_1
iadd
istore 3
aload 6
iload 2
aload 5
getfield android/support/v4/app/ao/h I
iastore
aload 5
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
ifnull L8
aload 5
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 6
iload 3
iconst_1
iadd
istore 2
aload 6
iload 3
iload 4
iastore
iconst_0
istore 3
L9:
iload 3
iload 4
if_icmpge L10
aload 0
getfield android/support/v4/app/BackStackState/a [I
iload 2
aload 5
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
getfield android/support/v4/app/Fragment/z I
iastore
iload 3
iconst_1
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
goto L9
L6:
iconst_m1
istore 2
goto L7
L10:
aload 5
getfield android/support/v4/app/ao/a Landroid/support/v4/app/ao;
astore 5
goto L4
L8:
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 6
iload 3
iconst_1
iadd
istore 2
aload 6
iload 3
iconst_0
iastore
goto L10
L5:
aload 0
aload 1
getfield android/support/v4/app/ak/s I
putfield android/support/v4/app/BackStackState/b I
aload 0
aload 1
getfield android/support/v4/app/ak/t I
putfield android/support/v4/app/BackStackState/c I
aload 0
aload 1
getfield android/support/v4/app/ak/w Ljava/lang/String;
putfield android/support/v4/app/BackStackState/d Ljava/lang/String;
aload 0
aload 1
getfield android/support/v4/app/ak/y I
putfield android/support/v4/app/BackStackState/e I
aload 0
aload 1
getfield android/support/v4/app/ak/z I
putfield android/support/v4/app/BackStackState/f I
aload 0
aload 1
getfield android/support/v4/app/ak/A Ljava/lang/CharSequence;
putfield android/support/v4/app/BackStackState/g Ljava/lang/CharSequence;
aload 0
aload 1
getfield android/support/v4/app/ak/B I
putfield android/support/v4/app/BackStackState/h I
aload 0
aload 1
getfield android/support/v4/app/ak/C Ljava/lang/CharSequence;
putfield android/support/v4/app/BackStackState/i Ljava/lang/CharSequence;
aload 0
aload 1
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
putfield android/support/v4/app/BackStackState/j Ljava/util/ArrayList;
aload 0
aload 1
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
putfield android/support/v4/app/BackStackState/k Ljava/util/ArrayList;
return
.limit locals 7
.limit stack 4
.end method

.method public final a(Landroid/support/v4/app/bl;)Landroid/support/v4/app/ak;
new android/support/v4/app/ak
dup
aload 1
invokespecial android/support/v4/app/ak/<init>(Landroid/support/v4/app/bl;)V
astore 7
iconst_0
istore 4
iconst_0
istore 2
L0:
iload 2
aload 0
getfield android/support/v4/app/BackStackState/a [I
arraylength
if_icmpge L1
new android/support/v4/app/ao
dup
invokespecial android/support/v4/app/ao/<init>()V
astore 8
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 9
iload 2
iconst_1
iadd
istore 3
aload 8
aload 9
iload 2
iaload
putfield android/support/v4/app/ao/c I
getstatic android/support/v4/app/bl/b Z
ifeq L2
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Instantiate "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " op #"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " base fragment #"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/app/BackStackState/a [I
iload 3
iaload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 9
iload 3
iconst_1
iadd
istore 2
aload 9
iload 3
iaload
istore 3
iload 3
iflt L3
aload 8
aload 1
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
L4:
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 9
iload 2
iconst_1
iadd
istore 3
aload 8
aload 9
iload 2
iaload
putfield android/support/v4/app/ao/e I
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 9
iload 3
iconst_1
iadd
istore 2
aload 8
aload 9
iload 3
iaload
putfield android/support/v4/app/ao/f I
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 9
iload 2
iconst_1
iadd
istore 3
aload 8
aload 9
iload 2
iaload
putfield android/support/v4/app/ao/g I
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 9
iload 3
iconst_1
iadd
istore 2
aload 8
aload 9
iload 3
iaload
putfield android/support/v4/app/ao/h I
aload 0
getfield android/support/v4/app/BackStackState/a [I
astore 9
iload 2
iconst_1
iadd
istore 3
aload 9
iload 2
iaload
istore 6
iload 3
istore 2
iload 6
ifle L5
aload 8
new java/util/ArrayList
dup
iload 6
invokespecial java/util/ArrayList/<init>(I)V
putfield android/support/v4/app/ao/i Ljava/util/ArrayList;
iconst_0
istore 5
L6:
iload 3
istore 2
iload 5
iload 6
if_icmpge L5
getstatic android/support/v4/app/bl/b Z
ifeq L7
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Instantiate "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " set remove fragment #"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/app/BackStackState/a [I
iload 3
iaload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L7:
aload 1
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/BackStackState/a [I
iload 3
iaload
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 9
aload 8
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
aload 9
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 5
iconst_1
iadd
istore 5
iload 3
iconst_1
iadd
istore 3
goto L6
L3:
aload 8
aconst_null
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
goto L4
L5:
aload 7
aload 8
invokevirtual android/support/v4/app/ak/a(Landroid/support/v4/app/ao;)V
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
aload 7
aload 0
getfield android/support/v4/app/BackStackState/b I
putfield android/support/v4/app/ak/s I
aload 7
aload 0
getfield android/support/v4/app/BackStackState/c I
putfield android/support/v4/app/ak/t I
aload 7
aload 0
getfield android/support/v4/app/BackStackState/d Ljava/lang/String;
putfield android/support/v4/app/ak/w Ljava/lang/String;
aload 7
aload 0
getfield android/support/v4/app/BackStackState/e I
putfield android/support/v4/app/ak/y I
aload 7
iconst_1
putfield android/support/v4/app/ak/u Z
aload 7
aload 0
getfield android/support/v4/app/BackStackState/f I
putfield android/support/v4/app/ak/z I
aload 7
aload 0
getfield android/support/v4/app/BackStackState/g Ljava/lang/CharSequence;
putfield android/support/v4/app/ak/A Ljava/lang/CharSequence;
aload 7
aload 0
getfield android/support/v4/app/BackStackState/h I
putfield android/support/v4/app/ak/B I
aload 7
aload 0
getfield android/support/v4/app/BackStackState/i Ljava/lang/CharSequence;
putfield android/support/v4/app/ak/C Ljava/lang/CharSequence;
aload 7
aload 0
getfield android/support/v4/app/BackStackState/j Ljava/util/ArrayList;
putfield android/support/v4/app/ak/D Ljava/util/ArrayList;
aload 7
aload 0
getfield android/support/v4/app/BackStackState/k Ljava/util/ArrayList;
putfield android/support/v4/app/ak/E Ljava/util/ArrayList;
aload 7
iconst_1
invokevirtual android/support/v4/app/ak/e(I)V
aload 7
areturn
.limit locals 10
.limit stack 4
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/a [I
invokevirtual android/os/Parcel/writeIntArray([I)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/b I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/c I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/d Ljava/lang/String;
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/e I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/f I
invokevirtual android/os/Parcel/writeInt(I)V
aload 0
getfield android/support/v4/app/BackStackState/g Ljava/lang/CharSequence;
aload 1
iconst_0
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/h I
invokevirtual android/os/Parcel/writeInt(I)V
aload 0
getfield android/support/v4/app/BackStackState/i Ljava/lang/CharSequence;
aload 1
iconst_0
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/j Ljava/util/ArrayList;
invokevirtual android/os/Parcel/writeStringList(Ljava/util/List;)V
aload 1
aload 0
getfield android/support/v4/app/BackStackState/k Ljava/util/ArrayList;
invokevirtual android/os/Parcel/writeStringList(Ljava/util/List;)V
return
.limit locals 3
.limit stack 3
.end method
