.bytecode 50.0
.class public final synchronized android/support/v4/app/dj
.super java/lang/Object
.implements android/support/v4/app/di

.field private static final 'a' Ljava/lang/String; = "android.wearable.EXTENSIONS"

.field private static final 'b' Ljava/lang/String; = "flags"

.field private static final 'c' Ljava/lang/String; = "inProgressLabel"

.field private static final 'd' Ljava/lang/String; = "confirmLabel"

.field private static final 'e' Ljava/lang/String; = "cancelLabel"

.field private static final 'f' I = 1


.field private static final 'g' I = 1


.field private 'h' I

.field private 'i' Ljava/lang/CharSequence;

.field private 'j' Ljava/lang/CharSequence;

.field private 'k' Ljava/lang/CharSequence;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield android/support/v4/app/dj/h I
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(Landroid/support/v4/app/df;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield android/support/v4/app/dj/h I
aload 1
getfield android/support/v4/app/df/a Landroid/os/Bundle;
ldc "android.wearable.EXTENSIONS"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
astore 1
aload 1
ifnull L0
aload 0
aload 1
ldc "flags"
iconst_1
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/dj/h I
aload 0
aload 1
ldc "inProgressLabel"
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
aload 0
aload 1
ldc "confirmLabel"
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
aload 0
aload 1
ldc "cancelLabel"
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
L0:
return
.limit locals 2
.limit stack 4
.end method

.method private a()Landroid/support/v4/app/dj;
new android/support/v4/app/dj
dup
invokespecial android/support/v4/app/dj/<init>()V
astore 1
aload 1
aload 0
getfield android/support/v4/app/dj/h I
putfield android/support/v4/app/dj/h I
aload 1
aload 0
getfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
aload 1
aload 0
getfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
aload 1
aload 0
getfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dj;
aload 0
aload 1
putfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Z)Landroid/support/v4/app/dj;
iload 1
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/dj/h I
iconst_1
ior
putfield android/support/v4/app/dj/h I
aload 0
areturn
L0:
aload 0
aload 0
getfield android/support/v4/app/dj/h I
bipush -2
iand
putfield android/support/v4/app/dj/h I
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dj;
aload 0
aload 1
putfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Z)V
iload 1
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/dj/h I
iconst_1
ior
putfield android/support/v4/app/dj/h I
return
L0:
aload 0
aload 0
getfield android/support/v4/app/dj/h I
bipush -2
iand
putfield android/support/v4/app/dj/h I
return
.limit locals 2
.limit stack 3
.end method

.method private b()Z
aload 0
getfield android/support/v4/app/dj/h I
iconst_1
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dj;
aload 0
aload 1
putfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private c()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v4/app/dh;)Landroid/support/v4/app/dh;
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 2
aload 0
getfield android/support/v4/app/dj/h I
iconst_1
if_icmpeq L0
aload 2
ldc "flags"
aload 0
getfield android/support/v4/app/dj/h I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L0:
aload 0
getfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
ifnull L1
aload 2
ldc "inProgressLabel"
aload 0
getfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
L1:
aload 0
getfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
ifnull L2
aload 2
ldc "confirmLabel"
aload 0
getfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
L2:
aload 0
getfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
ifnull L3
aload 2
ldc "cancelLabel"
aload 0
getfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
L3:
aload 1
getfield android/support/v4/app/dh/a Landroid/os/Bundle;
ldc "android.wearable.EXTENSIONS"
aload 2
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic clone()Ljava/lang/Object;
new android/support/v4/app/dj
dup
invokespecial android/support/v4/app/dj/<init>()V
astore 1
aload 1
aload 0
getfield android/support/v4/app/dj/h I
putfield android/support/v4/app/dj/h I
aload 1
aload 0
getfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/i Ljava/lang/CharSequence;
aload 1
aload 0
getfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/j Ljava/lang/CharSequence;
aload 1
aload 0
getfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
putfield android/support/v4/app/dj/k Ljava/lang/CharSequence;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method
