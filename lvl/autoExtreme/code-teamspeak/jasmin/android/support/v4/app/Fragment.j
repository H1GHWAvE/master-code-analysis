.bytecode 50.0
.class public synchronized android/support/v4/app/Fragment
.super java/lang/Object
.implements android/content/ComponentCallbacks
.implements android/view/View$OnCreateContextMenuListener

.field private static final 'a' Landroid/support/v4/n/v;

.field static final 'n' Ljava/lang/Object;

.field static final 'o' I = 0


.field static final 'p' I = 1


.field static final 'q' I = 2


.field static final 'r' I = 3


.field static final 's' I = 4


.field static final 't' I = 5


.field 'A' Ljava/lang/String;

.field public 'B' Landroid/os/Bundle;

.field 'C' Landroid/support/v4/app/Fragment;

.field 'D' I

.field 'E' I

.field 'F' Z

.field 'G' Z

.field 'H' Z

.field 'I' Z

.field 'J' Z

.field 'K' Z

.field 'L' I

.field public 'M' Landroid/support/v4/app/bl;

.field public 'N' Landroid/support/v4/app/bh;

.field 'O' Landroid/support/v4/app/bl;

.field 'P' Landroid/support/v4/app/Fragment;

.field 'Q' I

.field 'R' I

.field 'S' Ljava/lang/String;

.field 'T' Z

.field 'U' Z

.field 'V' Z

.field 'W' Z

.field 'X' Z

.field 'Y' Z

.field 'Z' Z

.field 'aa' I

.field 'ab' Landroid/view/ViewGroup;

.field public 'ac' Landroid/view/View;

.field 'ad' Landroid/view/View;

.field 'ae' Z

.field 'af' Z

.field 'ag' Landroid/support/v4/app/ct;

.field 'ah' Z

.field 'ai' Z

.field 'aj' Ljava/lang/Object;

.field 'ak' Ljava/lang/Object;

.field 'al' Ljava/lang/Object;

.field 'am' Ljava/lang/Object;

.field 'an' Ljava/lang/Object;

.field 'ao' Ljava/lang/Object;

.field 'ap' Ljava/lang/Boolean;

.field 'aq' Ljava/lang/Boolean;

.field 'ar' Landroid/support/v4/app/gj;

.field 'as' Landroid/support/v4/app/gj;

.field 'u' I

.field 'v' Landroid/view/View;

.field 'w' I

.field 'x' Landroid/os/Bundle;

.field 'y' Landroid/util/SparseArray;

.field 'z' I

.method static <clinit>()V
new android/support/v4/n/v
dup
invokespecial android/support/v4/n/v/<init>()V
putstatic android/support/v4/app/Fragment/a Landroid/support/v4/n/v;
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v4/app/Fragment/u I
aload 0
iconst_m1
putfield android/support/v4/app/Fragment/z I
aload 0
iconst_m1
putfield android/support/v4/app/Fragment/D I
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Y Z
aload 0
iconst_1
putfield android/support/v4/app/Fragment/af Z
aload 0
aconst_null
putfield android/support/v4/app/Fragment/aj Ljava/lang/Object;
aload 0
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
putfield android/support/v4/app/Fragment/ak Ljava/lang/Object;
aload 0
aconst_null
putfield android/support/v4/app/Fragment/al Ljava/lang/Object;
aload 0
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
putfield android/support/v4/app/Fragment/am Ljava/lang/Object;
aload 0
aconst_null
putfield android/support/v4/app/Fragment/an Ljava/lang/Object;
aload 0
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
putfield android/support/v4/app/Fragment/ao Ljava/lang/Object;
aload 0
aconst_null
putfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
aload 0
aconst_null
putfield android/support/v4/app/Fragment/as Landroid/support/v4/app/gj;
return
.limit locals 1
.limit stack 2
.end method

.method private A()Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
areturn
.limit locals 1
.limit stack 1
.end method

.method private B()I
aload 0
getfield android/support/v4/app/Fragment/E I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private C()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/h()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private D()Landroid/support/v4/app/bi;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
areturn
.limit locals 1
.limit stack 1
.end method

.method private E()Landroid/support/v4/app/bi;
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnonnull L0
aload 0
invokevirtual android/support/v4/app/Fragment/w()V
aload 0
getfield android/support/v4/app/Fragment/u I
iconst_5
if_icmplt L1
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/q()V
L0:
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
areturn
L1:
aload 0
getfield android/support/v4/app/Fragment/u I
iconst_4
if_icmplt L2
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/p()V
goto L0
L2:
aload 0
getfield android/support/v4/app/Fragment/u I
iconst_2
if_icmplt L3
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/o()V
goto L0
L3:
aload 0
getfield android/support/v4/app/Fragment/u I
ifle L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/n()V
goto L0
.limit locals 1
.limit stack 2
.end method

.method private F()Landroid/support/v4/app/Fragment;
aload 0
getfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
areturn
.limit locals 1
.limit stack 1
.end method

.method private G()Z
aload 0
getfield android/support/v4/app/Fragment/U Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private H()Z
aload 0
getfield android/support/v4/app/Fragment/G Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private I()Z
aload 0
getfield android/support/v4/app/Fragment/J Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private J()Z
aload 0
getfield android/support/v4/app/Fragment/H Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private K()Z
aload 0
getfield android/support/v4/app/Fragment/T Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private L()Z
aload 0
getfield android/support/v4/app/Fragment/X Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private M()Z
aload 0
getfield android/support/v4/app/Fragment/Y Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private N()Z
aload 0
getfield android/support/v4/app/Fragment/V Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private O()Z
aload 0
getfield android/support/v4/app/Fragment/af Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private P()Landroid/support/v4/app/cr;
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
areturn
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L1
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " not attached to Activity"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iconst_1
putfield android/support/v4/app/Fragment/ai Z
aload 0
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 0
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/ah Z
iconst_1
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
areturn
.limit locals 1
.limit stack 5
.end method

.method private Q()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 1
.limit stack 2
.end method

.method private R()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
aconst_null
astore 1
L1:
aload 1
ifnull L2
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
aload 1
invokevirtual android/support/v4/app/Fragment/a(Landroid/app/Activity;)V
L2:
return
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/b Landroid/app/Activity;
astore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method private S()Landroid/view/View;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private T()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 1
.limit stack 2
.end method

.method private U()V
aload 0
iconst_m1
putfield android/support/v4/app/Fragment/z I
aload 0
aconst_null
putfield android/support/v4/app/Fragment/A Ljava/lang/String;
aload 0
iconst_0
putfield android/support/v4/app/Fragment/F Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/G Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/H Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/I Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/J Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/K Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/L I
aload 0
aconst_null
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 0
aconst_null
putfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 0
aconst_null
putfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Q I
aload 0
iconst_0
putfield android/support/v4/app/Fragment/R I
aload 0
aconst_null
putfield android/support/v4/app/Fragment/S Ljava/lang/String;
aload 0
iconst_0
putfield android/support/v4/app/Fragment/T Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/U Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/W Z
aload 0
aconst_null
putfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
aload 0
iconst_0
putfield android/support/v4/app/Fragment/ah Z
aload 0
iconst_0
putfield android/support/v4/app/Fragment/ai Z
return
.limit locals 1
.limit stack 2
.end method

.method private static V()V
return
.limit locals 0
.limit stack 0
.end method

.method private static W()V
return
.limit locals 0
.limit stack 0
.end method

.method private static X()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method private Y()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/Fragment/aj Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private Z()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/Fragment/ak Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpne L0
aload 0
getfield android/support/v4/app/Fragment/aj Ljava/lang/Object;
areturn
L0:
aload 0
getfield android/support/v4/app/Fragment/ak Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
aload 0
aload 1
aconst_null
invokestatic android/support/v4/app/Fragment/a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;
.annotation invisibleparam 3 Landroid/support/a/z;
.end annotation
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
.catch java/lang/InstantiationException from L0 to L1 using L3
.catch java/lang/IllegalAccessException from L0 to L1 using L4
.catch java/lang/ClassNotFoundException from L5 to L6 using L2
.catch java/lang/InstantiationException from L5 to L6 using L3
.catch java/lang/IllegalAccessException from L5 to L6 using L4
.catch java/lang/ClassNotFoundException from L6 to L7 using L2
.catch java/lang/InstantiationException from L6 to L7 using L3
.catch java/lang/IllegalAccessException from L6 to L7 using L4
.catch java/lang/ClassNotFoundException from L8 to L9 using L2
.catch java/lang/InstantiationException from L8 to L9 using L3
.catch java/lang/IllegalAccessException from L8 to L9 using L4
L0:
getstatic android/support/v4/app/Fragment/a Landroid/support/v4/n/v;
aload 1
invokevirtual android/support/v4/n/v/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Class
astore 4
L1:
aload 4
astore 3
aload 4
ifnonnull L6
L5:
aload 0
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
aload 1
invokevirtual java/lang/ClassLoader/loadClass(Ljava/lang/String;)Ljava/lang/Class;
astore 3
getstatic android/support/v4/app/Fragment/a Landroid/support/v4/n/v;
aload 1
aload 3
invokevirtual android/support/v4/n/v/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L6:
aload 3
invokevirtual java/lang/Class/newInstance()Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 0
L7:
aload 2
ifnull L9
L8:
aload 2
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Bundle/setClassLoader(Ljava/lang/ClassLoader;)V
aload 0
aload 2
putfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
L9:
aload 0
areturn
L2:
astore 0
new android/support/v4/app/az
dup
new java/lang/StringBuilder
dup
ldc "Unable to instantiate fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": make sure class name exists, is public, and has an empty constructor that is public"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokespecial android/support/v4/app/az/<init>(Ljava/lang/String;Ljava/lang/Exception;)V
athrow
L3:
astore 0
new android/support/v4/app/az
dup
new java/lang/StringBuilder
dup
ldc "Unable to instantiate fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": make sure class name exists, is public, and has an empty constructor that is public"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokespecial android/support/v4/app/az/<init>(Ljava/lang/String;Ljava/lang/Exception;)V
athrow
L4:
astore 0
new android/support/v4/app/az
dup
new java/lang/StringBuilder
dup
ldc "Unable to instantiate fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": make sure class name exists, is public, and has an empty constructor that is public"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokespecial android/support/v4/app/az/<init>(Ljava/lang/String;Ljava/lang/Exception;)V
athrow
.limit locals 5
.limit stack 5
.end method

.method private a(I)Ljava/lang/CharSequence;
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
invokevirtual android/support/v4/app/Fragment/j()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getText(I)Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method

.method private transient a(I[Ljava/lang/Object;)Ljava/lang/String;
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
invokevirtual android/support/v4/app/Fragment/j()Landroid/content/res/Resources;
iload 1
aload 2
invokevirtual android/content/res/Resources/getString(I[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method private a()V
aload 0
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/ad Landroid/view/View;
aload 0
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
invokevirtual android/view/View/restoreHierarchyState(Landroid/util/SparseArray;)V
aload 0
aconst_null
putfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onViewStateRestored()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 1
.limit stack 5
.end method

.method private a(Landroid/content/Intent;)V
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " not attached to Activity"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 0
aload 1
iconst_m1
invokevirtual android/support/v4/app/bh/a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
return
.limit locals 2
.limit stack 5
.end method

.method private a(Landroid/content/Intent;I)V
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " not attached to Activity"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 0
aload 1
iload 2
invokevirtual android/support/v4/app/bh/a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
return
.limit locals 3
.limit stack 5
.end method

.method private a(Landroid/content/res/Configuration;)V
aload 0
aload 1
invokevirtual android/support/v4/app/Fragment/onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/content/res/Configuration;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/Fragment$SavedState;)V
aload 0
getfield android/support/v4/app/Fragment/z I
iflt L0
new java/lang/IllegalStateException
dup
ldc "Fragment already active"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
ifnull L1
aload 1
getfield android/support/v4/app/Fragment$SavedState/a Landroid/os/Bundle;
ifnull L1
aload 1
getfield android/support/v4/app/Fragment$SavedState/a Landroid/os/Bundle;
astore 1
L2:
aload 0
aload 1
putfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
return
L1:
aconst_null
astore 1
goto L2
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/support/v4/app/Fragment;I)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
aload 0
iload 2
putfield android/support/v4/app/Fragment/E I
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/gj;)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;)V
aload 1
aload 0
invokevirtual android/view/View/setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/aj Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mFragmentId=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/Q I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc " mContainerId=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/R I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc " mTag="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mState="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/u I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc " mIndex="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/z I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc " mWho="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc " mBackStackNesting="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/L I
invokevirtual java/io/PrintWriter/println(I)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mAdded="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/F Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mRemoving="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/G Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mResumed="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/H Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mFromLayout="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/I Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mInLayout="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/J Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mHidden="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/T Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mDetached="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/U Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mMenuVisible="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/Y Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mHasMenu="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/X Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mRetainInstance="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/V Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mRetaining="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/W Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mUserVisibleHint="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/af Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
ifnull L0
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mFragmentManager="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnull L1
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mHost="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L1:
aload 0
getfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
ifnull L2
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mParentFragment="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L2:
aload 0
getfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
ifnull L3
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mArguments="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L3:
aload 0
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
ifnull L4
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mSavedFragmentState="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/x Landroid/os/Bundle;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L4:
aload 0
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
ifnull L5
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mSavedViewState="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/y Landroid/util/SparseArray;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L5:
aload 0
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
ifnull L6
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mTarget="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
invokevirtual java/io/PrintWriter/print(Ljava/lang/Object;)V
aload 3
ldc " mTargetRequestCode="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/E I
invokevirtual java/io/PrintWriter/println(I)V
L6:
aload 0
getfield android/support/v4/app/Fragment/aa I
ifeq L7
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mNextAnim="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/aa I
invokevirtual java/io/PrintWriter/println(I)V
L7:
aload 0
getfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
ifnull L8
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mContainer="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L8:
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L9
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mView="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L9:
aload 0
getfield android/support/v4/app/Fragment/ad Landroid/view/View;
ifnull L10
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mInnerView="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L10:
aload 0
getfield android/support/v4/app/Fragment/v Landroid/view/View;
ifnull L11
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mAnimatingAway="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/v Landroid/view/View;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mStateAfterAnimating="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/Fragment/w I
invokevirtual java/io/PrintWriter/println(I)V
L11:
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L12
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Loader Manager:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/ct/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L12:
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L13
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
new java/lang/StringBuilder
dup
ldc "Child "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/bl/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L13:
return
.limit locals 5
.limit stack 5
.end method

.method private a(Z)V
iload 1
ifeq L0
aload 0
getfield android/support/v4/app/Fragment/P Landroid/support/v4/app/Fragment;
ifnull L0
new java/lang/IllegalStateException
dup
ldc "Can't retain fragements that are nested in other fragments"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield android/support/v4/app/Fragment/V Z
return
.limit locals 2
.limit stack 3
.end method

.method private a([Ljava/lang/String;I)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " not attached to Activity"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 0
aload 1
iload 2
invokevirtual android/support/v4/app/bh/a(Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 5
.end method

.method private a(Landroid/view/Menu;)Z
iconst_0
istore 3
iconst_0
istore 4
aload 0
getfield android/support/v4/app/Fragment/T Z
ifne L0
iload 4
istore 2
aload 0
getfield android/support/v4/app/Fragment/X Z
ifeq L1
iload 4
istore 2
aload 0
getfield android/support/v4/app/Fragment/Y Z
ifeq L1
iconst_1
istore 2
L1:
iload 2
istore 3
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
iload 2
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/view/Menu;)Z
ior
istore 3
L0:
iload 3
ireturn
.limit locals 5
.limit stack 3
.end method

.method private aa()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/Fragment/al Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private ab()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/Fragment/am Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpne L0
aload 0
getfield android/support/v4/app/Fragment/al Ljava/lang/Object;
areturn
L0:
aload 0
getfield android/support/v4/app/Fragment/am Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method private ac()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/Fragment/an Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private ad()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/Fragment/ao Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpne L0
aload 0
getfield android/support/v4/app/Fragment/an Ljava/lang/Object;
areturn
L0:
aload 0
getfield android/support/v4/app/Fragment/ao Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method private ae()Z
aload 0
getfield android/support/v4/app/Fragment/aq Ljava/lang/Boolean;
ifnonnull L0
iconst_1
ireturn
L0:
aload 0
getfield android/support/v4/app/Fragment/aq Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private af()Z
aload 0
getfield android/support/v4/app/Fragment/ap Ljava/lang/Boolean;
ifnonnull L0
iconst_1
ireturn
L0:
aload 0
getfield android/support/v4/app/Fragment/ap Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private ag()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/k()Z
pop
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
invokevirtual android/support/v4/app/Fragment/e()V
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onStart()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L2
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/p()V
L2:
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L3
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/f()V
L3:
return
.limit locals 1
.limit stack 5
.end method

.method private ah()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/k()Z
pop
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
invokevirtual android/support/v4/app/Fragment/s()V
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onResume()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L2
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/q()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/k()Z
pop
L2:
return
.limit locals 1
.limit stack 5
.end method

.method private ai()V
aload 0
invokevirtual android/support/v4/app/Fragment/onLowMemory()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/t()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private aj()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_4
invokevirtual android/support/v4/app/bl/c(I)V
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
invokevirtual android/support/v4/app/Fragment/t()V
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onPause()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 1
.limit stack 5
.end method

.method private ak()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/r()V
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
invokevirtual android/support/v4/app/Fragment/f()V
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onStop()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 1
.limit stack 5
.end method

.method private al()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_1
invokevirtual android/support/v4/app/bl/c(I)V
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
invokevirtual android/support/v4/app/Fragment/g()V
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onDestroyView()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L2
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/e()V
L2:
return
.limit locals 1
.limit stack 5
.end method

.method private am()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/s()V
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
invokevirtual android/support/v4/app/Fragment/u()V
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onDestroy()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 1
.limit stack 5
.end method

.method private b(I)Ljava/lang/String;
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
invokevirtual android/support/v4/app/Fragment/j()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/support/v4/app/gj;)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/as Landroid/support/v4/app/gj;
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/view/Menu;)V
aload 0
getfield android/support/v4/app/Fragment/T Z
ifne L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/b(Landroid/view/Menu;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;)V
aload 0
aconst_null
invokevirtual android/view/View/setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V
return
.limit locals 1
.limit stack 2
.end method

.method private b(Ljava/lang/Object;)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/ak Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private b()Z
aload 0
getfield android/support/v4/app/Fragment/L I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)Z
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
.catch java/lang/ClassNotFoundException from L3 to L4 using L2
.catch java/lang/ClassNotFoundException from L4 to L5 using L2
L0:
getstatic android/support/v4/app/Fragment/a Landroid/support/v4/n/v;
aload 1
invokevirtual android/support/v4/n/v/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Class
astore 4
L1:
aload 4
astore 3
aload 4
ifnonnull L4
L3:
aload 0
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
aload 1
invokevirtual java/lang/ClassLoader/loadClass(Ljava/lang/String;)Ljava/lang/Class;
astore 3
getstatic android/support/v4/app/Fragment/a Landroid/support/v4/n/v;
aload 1
aload 3
invokevirtual android/support/v4/n/v/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
ldc android/support/v4/app/Fragment
aload 3
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
istore 2
L5:
iload 2
ireturn
L2:
astore 0
iconst_0
ireturn
.limit locals 5
.limit stack 3
.end method

.method private b(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
iconst_0
istore 4
iconst_0
istore 5
aload 0
getfield android/support/v4/app/Fragment/T Z
ifne L0
iload 5
istore 3
aload 0
getfield android/support/v4/app/Fragment/X Z
ifeq L1
iload 5
istore 3
aload 0
getfield android/support/v4/app/Fragment/Y Z
ifeq L1
iconst_1
istore 3
aload 0
aload 1
aload 2
invokevirtual android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
L1:
iload 3
istore 4
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
iload 3
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
aload 2
invokevirtual android/support/v4/app/bl/a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
ior
istore 4
L0:
iload 4
ireturn
.limit locals 6
.limit stack 4
.end method

.method private b(Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v4/app/Fragment/T Z
ifne L0
aload 0
getfield android/support/v4/app/Fragment/X Z
ifeq L1
aload 0
getfield android/support/v4/app/Fragment/Y Z
ifeq L1
aload 0
aload 1
invokevirtual android/support/v4/app/Fragment/a(Landroid/view/MenuItem;)Z
ifeq L1
L2:
iconst_1
ireturn
L1:
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/view/MenuItem;)Z
ifne L2
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;)Z
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 1
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private c(Ljava/lang/Object;)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/al Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private c(Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v4/app/Fragment/T Z
ifne L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/b(Landroid/view/MenuItem;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private d()I
aload 0
getfield android/support/v4/app/Fragment/Q I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/lang/Object;)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/am Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private d(Z)V
aload 0
iload 1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putfield android/support/v4/app/Fragment/aq Ljava/lang/Boolean;
return
.limit locals 2
.limit stack 2
.end method

.method private e(Ljava/lang/Object;)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/an Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private e(Z)V
aload 0
iload 1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putfield android/support/v4/app/Fragment/ap Ljava/lang/Boolean;
return
.limit locals 2
.limit stack 2
.end method

.method private f(Ljava/lang/Object;)V
aload 0
aload 1
putfield android/support/v4/app/Fragment/ao Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private g(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
aload 1
invokevirtual android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onCreate()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
ifnull L2
aload 1
ldc "android:support:fragments"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
astore 1
aload 1
ifnull L2
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnonnull L3
aload 0
invokevirtual android/support/v4/app/Fragment/w()V
L3:
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 1
aconst_null
invokevirtual android/support/v4/app/bl/a(Landroid/os/Parcelable;Ljava/util/List;)V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/n()V
L2:
return
.limit locals 2
.limit stack 5
.end method

.method private h(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
L0:
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
aload 1
invokevirtual android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/Fragment/Z Z
ifne L1
new android/support/v4/app/gk
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " did not call through to super.onActivityCreated()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/support/v4/app/gk/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L2
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/o()V
L2:
return
.limit locals 2
.limit stack 5
.end method

.method public static m()V
return
.limit locals 0
.limit stack 0
.end method

.method public static o()V
return
.limit locals 0
.limit stack 0
.end method

.method public static p()V
return
.limit locals 0
.limit stack 0
.end method

.method public static r()Landroid/view/animation/Animation;
aconst_null
areturn
.limit locals 0
.limit stack 1
.end method

.method public static v()V
return
.limit locals 0
.limit stack 0
.end method

.method private y()Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private z()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
aload 1
aload 0
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
astore 3
L1:
aload 3
areturn
L0:
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L2
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
astore 5
aload 5
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L3
aload 1
ifnull L3
aload 5
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L4:
iload 2
iflt L3
aload 5
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 3
aload 3
ifnull L5
aload 3
aload 1
invokevirtual android/support/v4/app/Fragment/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
astore 4
aload 4
astore 3
aload 4
ifnonnull L1
L5:
iload 2
iconst_1
isub
istore 2
goto L4
L3:
aconst_null
areturn
L2:
aconst_null
areturn
.limit locals 6
.limit stack 2
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.annotation invisible Landroid/support/a/z;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
aconst_null
areturn
.limit locals 3
.limit stack 1
.end method

.method final a(ILandroid/support/v4/app/Fragment;)V
aload 0
iload 1
putfield android/support/v4/app/Fragment/z I
aload 2
ifnull L0
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/app/Fragment/z I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield android/support/v4/app/Fragment/A Ljava/lang/String;
return
L0:
aload 0
new java/lang/StringBuilder
dup
ldc "android:fragment:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/Fragment/z I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield android/support/v4/app/Fragment/A Ljava/lang/String;
return
.limit locals 3
.limit stack 4
.end method

.method public a(Landroid/app/Activity;)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 2
.limit stack 2
.end method

.method public a(Landroid/os/Bundle;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 2
.limit stack 2
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/MenuItem;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/c()Landroid/view/LayoutInflater;
astore 1
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnonnull L0
aload 0
invokevirtual android/support/v4/app/Fragment/w()V
aload 0
getfield android/support/v4/app/Fragment/u I
iconst_5
if_icmplt L1
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/q()V
L0:
aload 1
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokestatic android/support/v4/view/ai/a(Landroid/view/LayoutInflater;Landroid/support/v4/view/as;)V
aload 1
areturn
L1:
aload 0
getfield android/support/v4/app/Fragment/u I
iconst_4
if_icmplt L2
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/p()V
goto L0
L2:
aload 0
getfield android/support/v4/app/Fragment/u I
iconst_2
if_icmplt L3
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/o()V
goto L0
L3:
aload 0
getfield android/support/v4/app/Fragment/u I
ifle L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/n()V
goto L0
.limit locals 2
.limit stack 2
.end method

.method final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
L0:
aload 0
aload 1
aload 2
invokevirtual android/support/v4/app/Fragment/a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final b(Z)V
aload 0
getfield android/support/v4/app/Fragment/Y Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v4/app/Fragment/Y Z
aload 0
getfield android/support/v4/app/Fragment/X Z
ifeq L0
aload 0
invokevirtual android/support/v4/app/Fragment/k()Z
ifeq L0
aload 0
getfield android/support/v4/app/Fragment/T Z
ifne L0
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/d()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public c()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 1
.limit stack 2
.end method

.method public c(Landroid/os/Bundle;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 2
.limit stack 2
.end method

.method public final c(Z)V
aload 0
getfield android/support/v4/app/Fragment/af Z
ifne L0
iload 1
ifeq L0
aload 0
getfield android/support/v4/app/Fragment/u I
iconst_4
if_icmpge L0
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 0
invokevirtual android/support/v4/app/bl/b(Landroid/support/v4/app/Fragment;)V
L0:
aload 0
iload 1
putfield android/support/v4/app/Fragment/af Z
iload 1
ifne L1
iconst_1
istore 1
L2:
aload 0
iload 1
putfield android/support/v4/app/Fragment/ae Z
return
L1:
iconst_0
istore 1
goto L2
.limit locals 2
.limit stack 2
.end method

.method public d(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 0
.end method

.method public e()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
aload 0
getfield android/support/v4/app/Fragment/ah Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/app/Fragment/ah Z
aload 0
getfield android/support/v4/app/Fragment/ai Z
ifne L1
aload 0
iconst_1
putfield android/support/v4/app/Fragment/ai Z
aload 0
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 0
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/ah Z
iconst_0
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
L1:
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/b()V
L0:
return
.limit locals 1
.limit stack 5
.end method

.method public final e(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/Fragment/z I
iflt L0
new java/lang/IllegalStateException
dup
ldc "Fragment already active"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
return
.limit locals 2
.limit stack 3
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public f()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 1
.limit stack 2
.end method

.method final f(Landroid/os/Bundle;)V
aload 0
aload 1
invokevirtual android/support/v4/app/Fragment/d(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/m()Landroid/os/Parcelable;
astore 2
aload 2
ifnull L0
aload 1
ldc "android:support:fragments"
aload 2
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public g()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 1
.limit stack 2
.end method

.method public final h()Landroid/content/Context;
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
invokespecial java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Landroid/support/v4/app/bb;
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/b Landroid/app/Activity;
checkcast android/support/v4/app/bb
areturn
.limit locals 1
.limit stack 1
.end method

.method public final j()Landroid/content/res/Resources;
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " not attached to Activity"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
areturn
.limit locals 1
.limit stack 5
.end method

.method public final k()Z
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/F Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final l()Z
aload 0
invokevirtual android/support/v4/app/Fragment/k()Z
ifeq L0
aload 0
getfield android/support/v4/app/Fragment/T Z
ifne L0
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual android/view/View/getWindowToken()Landroid/os/IBinder;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final n()V
aload 0
getfield android/support/v4/app/Fragment/X Z
iconst_1
if_icmpeq L0
aload 0
iconst_1
putfield android/support/v4/app/Fragment/X Z
aload 0
invokevirtual android/support/v4/app/Fragment/k()Z
ifeq L0
aload 0
getfield android/support/v4/app/Fragment/T Z
ifne L0
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/d()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 2
.limit stack 2
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
aload 0
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
aload 1
aload 2
aload 3
invokevirtual android/support/v4/app/bb/onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
return
.limit locals 4
.limit stack 4
.end method

.method public onLowMemory()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 1
.limit stack 2
.end method

.method public final q()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
ifnonnull L0
aconst_null
astore 1
L1:
aload 1
ifnull L2
aload 0
iconst_0
putfield android/support/v4/app/Fragment/Z Z
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
L2:
return
L0:
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/b Landroid/app/Activity;
astore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method public s()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 1
.limit stack 2
.end method

.method public t()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
return
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
sipush 128
invokespecial java/lang/StringBuilder/<init>(I)V
astore 1
aload 0
aload 1
invokestatic android/support/v4/n/g/a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
aload 0
getfield android/support/v4/app/Fragment/z I
iflt L0
aload 1
ldc " #"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
getfield android/support/v4/app/Fragment/z I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
L0:
aload 0
getfield android/support/v4/app/Fragment/Q I
ifeq L1
aload 1
ldc " id=0x"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
getfield android/support/v4/app/Fragment/Q I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L1:
aload 0
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
ifnull L2
aload 1
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L2:
aload 1
bipush 125
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method public u()V
aload 0
iconst_1
putfield android/support/v4/app/Fragment/Z Z
aload 0
getfield android/support/v4/app/Fragment/ai Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/app/Fragment/ai Z
aload 0
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 0
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/ah Z
iconst_0
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
L0:
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L1
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/g()V
L1:
return
.limit locals 1
.limit stack 5
.end method

.method final w()V
aload 0
new android/support/v4/app/bl
dup
invokespecial android/support/v4/app/bl/<init>()V
putfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
new android/support/v4/app/ay
dup
aload 0
invokespecial android/support/v4/app/ay/<init>(Landroid/support/v4/app/Fragment;)V
aload 0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/bh;Landroid/support/v4/app/bf;Landroid/support/v4/app/Fragment;)V
return
.limit locals 1
.limit stack 5
.end method

.method final x()V
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
ifnull L0
aload 0
getfield android/support/v4/app/Fragment/O Landroid/support/v4/app/bl;
iconst_2
invokevirtual android/support/v4/app/bl/c(I)V
L0:
aload 0
getfield android/support/v4/app/Fragment/ah Z
ifeq L1
aload 0
iconst_0
putfield android/support/v4/app/Fragment/ah Z
aload 0
getfield android/support/v4/app/Fragment/ai Z
ifne L2
aload 0
iconst_1
putfield android/support/v4/app/Fragment/ai Z
aload 0
aload 0
getfield android/support/v4/app/Fragment/N Landroid/support/v4/app/bh;
aload 0
getfield android/support/v4/app/Fragment/A Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/ah Z
iconst_0
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
L2:
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
ifnull L1
aload 0
getfield android/support/v4/app/Fragment/W Z
ifne L3
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/c()V
L1:
return
L3:
aload 0
getfield android/support/v4/app/Fragment/ag Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/d()V
return
.limit locals 1
.limit stack 5
.end method
