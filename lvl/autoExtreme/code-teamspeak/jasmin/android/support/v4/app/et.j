.bytecode 50.0
.class final synchronized android/support/v4/app/et
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "NotificationCompat"

.field static final 'b' Ljava/lang/String; = "android.support.localOnly"

.field static final 'c' Ljava/lang/String; = "android.support.actionExtras"

.field static final 'd' Ljava/lang/String; = "android.support.remoteInputs"

.field static final 'e' Ljava/lang/String; = "android.support.groupKey"

.field static final 'f' Ljava/lang/String; = "android.support.isGroupSummary"

.field static final 'g' Ljava/lang/String; = "android.support.sortKey"

.field static final 'h' Ljava/lang/String; = "android.support.useSideChannel"

.field private static final 'i' Ljava/lang/String; = "icon"

.field private static final 'j' Ljava/lang/String; = "title"

.field private static final 'k' Ljava/lang/String; = "actionIntent"

.field private static final 'l' Ljava/lang/String; = "extras"

.field private static final 'm' Ljava/lang/String; = "remoteInputs"

.field private static final 'n' Ljava/lang/Object;

.field private static 'o' Ljava/lang/reflect/Field;

.field private static 'p' Z

.field private static final 'q' Ljava/lang/Object;

.field private static 'r' Ljava/lang/Class;

.field private static 's' Ljava/lang/reflect/Field;

.field private static 't' Ljava/lang/reflect/Field;

.field private static 'u' Ljava/lang/reflect/Field;

.field private static 'v' Ljava/lang/reflect/Field;

.field private static 'w' Z

.method static <clinit>()V
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/app/et/n Ljava/lang/Object;
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/app/et/q Ljava/lang/Object;
return
.limit locals 0
.limit stack 2
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/app/Notification$Builder;Landroid/support/v4/app/ek;)Landroid/os/Bundle;
aload 1
invokevirtual android/support/v4/app/ek/a()I
pop
aload 0
iconst_0
aload 1
invokevirtual android/support/v4/app/ek/b()Ljava/lang/CharSequence;
aload 1
invokevirtual android/support/v4/app/ek/c()Landroid/app/PendingIntent;
invokevirtual android/app/Notification$Builder/addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
pop
new android/os/Bundle
dup
aload 1
invokevirtual android/support/v4/app/ek/d()Landroid/os/Bundle;
invokespecial android/os/Bundle/<init>(Landroid/os/Bundle;)V
astore 0
aload 1
invokevirtual android/support/v4/app/ek/e()[Landroid/support/v4/app/fw;
ifnull L0
aload 0
ldc "android.support.remoteInputs"
aload 1
invokevirtual android/support/v4/app/ek/e()[Landroid/support/v4/app/fw;
invokestatic android/support/v4/app/fy/a([Landroid/support/v4/app/fw;)[Landroid/os/Bundle;
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Landroid/app/Notification;)Landroid/os/Bundle;
.catch all from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L3 to L4 using L5
.catch java/lang/NoSuchFieldException from L3 to L4 using L6
.catch all from L3 to L4 using L2
.catch all from L4 to L7 using L2
.catch java/lang/IllegalAccessException from L8 to L9 using L5
.catch java/lang/NoSuchFieldException from L8 to L9 using L6
.catch all from L8 to L9 using L2
.catch java/lang/IllegalAccessException from L9 to L10 using L5
.catch java/lang/NoSuchFieldException from L9 to L10 using L6
.catch all from L9 to L10 using L2
.catch java/lang/IllegalAccessException from L11 to L12 using L5
.catch java/lang/NoSuchFieldException from L11 to L12 using L6
.catch all from L11 to L12 using L2
.catch all from L12 to L13 using L2
.catch all from L14 to L15 using L2
.catch all from L16 to L17 using L2
.catch all from L17 to L18 using L2
.catch all from L19 to L20 using L2
getstatic android/support/v4/app/et/n Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
getstatic android/support/v4/app/et/p Z
ifeq L3
aload 3
monitorexit
L1:
aconst_null
areturn
L3:
getstatic android/support/v4/app/et/o Ljava/lang/reflect/Field;
ifnonnull L9
ldc android/app/Notification
ldc "extras"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 1
ldc android/os/Bundle
aload 1
invokevirtual java/lang/reflect/Field/getType()Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L8
ldc "NotificationCompat"
ldc "Notification.extras field is not of type Bundle"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_1
putstatic android/support/v4/app/et/p Z
L4:
aload 3
monitorexit
L7:
aconst_null
areturn
L8:
aload 1
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
aload 1
putstatic android/support/v4/app/et/o Ljava/lang/reflect/Field;
L9:
getstatic android/support/v4/app/et/o Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/os/Bundle
astore 2
L10:
aload 2
astore 1
aload 2
ifnonnull L12
L11:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 1
getstatic android/support/v4/app/et/o Ljava/lang/reflect/Field;
aload 0
aload 1
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L12:
aload 3
monitorexit
L13:
aload 1
areturn
L2:
astore 0
L14:
aload 3
monitorexit
L15:
aload 0
athrow
L5:
astore 0
L16:
ldc "NotificationCompat"
ldc "Unable to access notification extras"
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L17:
iconst_1
putstatic android/support/v4/app/et/p Z
aload 3
monitorexit
L18:
aconst_null
areturn
L6:
astore 0
L19:
ldc "NotificationCompat"
ldc "Unable to access notification extras"
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L20:
goto L17
.limit locals 4
.limit stack 3
.end method

.method private static a(Landroid/support/v4/app/ek;)Landroid/os/Bundle;
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 1
aload 1
ldc "icon"
aload 0
invokevirtual android/support/v4/app/ek/a()I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 1
ldc "title"
aload 0
invokevirtual android/support/v4/app/ek/b()Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
aload 1
ldc "actionIntent"
aload 0
invokevirtual android/support/v4/app/ek/c()Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 1
ldc "extras"
aload 0
invokevirtual android/support/v4/app/ek/d()Landroid/os/Bundle;
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
aload 1
ldc "remoteInputs"
aload 0
invokevirtual android/support/v4/app/ek/e()[Landroid/support/v4/app/fw;
invokestatic android/support/v4/app/fy/a([Landroid/support/v4/app/fw;)[Landroid/os/Bundle;
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Landroid/app/Notification;ILandroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/IllegalAccessException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/lang/IllegalAccessException from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/lang/IllegalAccessException from L7 to L8 using L2
.catch all from L7 to L8 using L3
.catch all from L8 to L9 using L3
.catch all from L10 to L11 using L3
.catch all from L12 to L13 using L3
getstatic android/support/v4/app/et/q Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
invokestatic android/support/v4/app/et/g(Landroid/app/Notification;)[Ljava/lang/Object;
iload 1
aaload
astore 5
aload 0
invokestatic android/support/v4/app/et/a(Landroid/app/Notification;)Landroid/os/Bundle;
astore 0
L1:
aload 0
ifnull L14
L4:
aload 0
ldc "android.support.actionExtras"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 0
L5:
aload 0
ifnull L14
L6:
aload 0
iload 1
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/os/Bundle
astore 0
L7:
aload 2
aload 3
getstatic android/support/v4/app/et/t Ljava/lang/reflect/Field;
aload 5
invokevirtual java/lang/reflect/Field/getInt(Ljava/lang/Object;)I
getstatic android/support/v4/app/et/u Ljava/lang/reflect/Field;
aload 5
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/CharSequence
getstatic android/support/v4/app/et/v Ljava/lang/reflect/Field;
aload 5
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/app/PendingIntent
aload 0
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/el;Landroid/support/v4/app/fx;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)Landroid/support/v4/app/ek;
astore 0
L8:
aload 4
monitorexit
L9:
aload 0
areturn
L2:
astore 0
L10:
ldc "NotificationCompat"
ldc "Unable to access notification actions"
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_1
putstatic android/support/v4/app/et/w Z
aload 4
monitorexit
L11:
aconst_null
areturn
L3:
astore 0
L12:
aload 4
monitorexit
L13:
aload 0
athrow
L14:
aconst_null
astore 0
goto L7
.limit locals 6
.limit stack 6
.end method

.method private static a(Landroid/os/Bundle;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
aload 1
aload 0
ldc "icon"
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;)I
aload 0
ldc "title"
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
aload 0
ldc "actionIntent"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/PendingIntent
aload 0
ldc "extras"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
aload 0
ldc "remoteInputs"
invokestatic android/support/v4/app/aw/a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/os/Bundle;
aload 2
invokestatic android/support/v4/app/fy/a([Landroid/os/Bundle;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/fw;
invokeinterface android/support/v4/app/el/a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fw;)Landroid/support/v4/app/ek; 5
areturn
.limit locals 3
.limit stack 7
.end method

.method public static a(Landroid/support/v4/app/el;Landroid/support/v4/app/fx;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)Landroid/support/v4/app/ek;
aconst_null
astore 6
aload 5
ifnull L0
aload 5
ldc "android.support.remoteInputs"
invokestatic android/support/v4/app/aw/a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/os/Bundle;
aload 1
invokestatic android/support/v4/app/fy/a([Landroid/os/Bundle;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/fw;
astore 6
L0:
aload 0
iload 2
aload 3
aload 4
aload 5
aload 6
invokeinterface android/support/v4/app/el/a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fw;)Landroid/support/v4/app/ek; 5
areturn
.limit locals 7
.limit stack 6
.end method

.method public static a(Ljava/util/List;)Landroid/util/SparseArray;
aconst_null
astore 3
aload 0
invokeinterface java/util/List/size()I 0
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 5
aload 3
astore 4
aload 5
ifnull L2
aload 3
astore 4
aload 3
ifnonnull L3
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 4
L3:
aload 4
iload 1
aload 5
invokevirtual android/util/SparseArray/put(ILjava/lang/Object;)V
L2:
iload 1
iconst_1
iadd
istore 1
aload 4
astore 3
goto L0
L1:
aload 3
areturn
.limit locals 6
.limit stack 3
.end method

.method public static a([Landroid/support/v4/app/ek;)Ljava/util/ArrayList;
aload 0
ifnonnull L0
aconst_null
astore 3
L1:
aload 3
areturn
L0:
new java/util/ArrayList
dup
aload 0
arraylength
invokespecial java/util/ArrayList/<init>(I)V
astore 4
aload 0
arraylength
istore 2
iconst_0
istore 1
L2:
aload 4
astore 3
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
aaload
astore 3
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 5
aload 5
ldc "icon"
aload 3
invokevirtual android/support/v4/app/ek/a()I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 5
ldc "title"
aload 3
invokevirtual android/support/v4/app/ek/b()Ljava/lang/CharSequence;
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
aload 5
ldc "actionIntent"
aload 3
invokevirtual android/support/v4/app/ek/c()Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 5
ldc "extras"
aload 3
invokevirtual android/support/v4/app/ek/d()Landroid/os/Bundle;
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
aload 5
ldc "remoteInputs"
aload 3
invokevirtual android/support/v4/app/ek/e()[Landroid/support/v4/app/fw;
invokestatic android/support/v4/app/fy/a([Landroid/support/v4/app/fw;)[Landroid/os/Bundle;
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
aload 4
aload 5
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L2
.limit locals 6
.limit stack 3
.end method

.method public static a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V
new android/app/Notification$BigPictureStyle
dup
aload 0
invokeinterface android/support/v4/app/dc/a()Landroid/app/Notification$Builder; 0
invokespecial android/app/Notification$BigPictureStyle/<init>(Landroid/app/Notification$Builder;)V
aload 1
invokevirtual android/app/Notification$BigPictureStyle/setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;
aload 4
invokevirtual android/app/Notification$BigPictureStyle/bigPicture(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;
astore 0
iload 6
ifeq L0
aload 0
aload 5
invokevirtual android/app/Notification$BigPictureStyle/bigLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;
pop
L0:
iload 2
ifeq L1
aload 0
aload 3
invokevirtual android/app/Notification$BigPictureStyle/setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;
pop
L1:
return
.limit locals 7
.limit stack 3
.end method

.method public static a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
new android/app/Notification$BigTextStyle
dup
aload 0
invokeinterface android/support/v4/app/dc/a()Landroid/app/Notification$Builder; 0
invokespecial android/app/Notification$BigTextStyle/<init>(Landroid/app/Notification$Builder;)V
aload 1
invokevirtual android/app/Notification$BigTextStyle/setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
aload 4
invokevirtual android/app/Notification$BigTextStyle/bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
astore 0
iload 2
ifeq L0
aload 0
aload 3
invokevirtual android/app/Notification$BigTextStyle/setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
pop
L0:
return
.limit locals 5
.limit stack 3
.end method

.method public static a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V
new android/app/Notification$InboxStyle
dup
aload 0
invokeinterface android/support/v4/app/dc/a()Landroid/app/Notification$Builder; 0
invokespecial android/app/Notification$InboxStyle/<init>(Landroid/app/Notification$Builder;)V
aload 1
invokevirtual android/app/Notification$InboxStyle/setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
astore 0
iload 2
ifeq L0
aload 0
aload 3
invokevirtual android/app/Notification$InboxStyle/setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
pop
L0:
aload 4
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/CharSequence
invokevirtual android/app/Notification$InboxStyle/addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
pop
goto L1
L2:
return
.limit locals 5
.limit stack 3
.end method

.method private static a()Z
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
.catch java/lang/NoSuchFieldException from L0 to L1 using L3
getstatic android/support/v4/app/et/w Z
ifeq L0
L4:
iconst_0
ireturn
L0:
getstatic android/support/v4/app/et/s Ljava/lang/reflect/Field;
ifnonnull L1
ldc "android.app.Notification$Action"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
aload 0
putstatic android/support/v4/app/et/r Ljava/lang/Class;
aload 0
ldc "icon"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
putstatic android/support/v4/app/et/t Ljava/lang/reflect/Field;
getstatic android/support/v4/app/et/r Ljava/lang/Class;
ldc "title"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
putstatic android/support/v4/app/et/u Ljava/lang/reflect/Field;
getstatic android/support/v4/app/et/r Ljava/lang/Class;
ldc "actionIntent"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
putstatic android/support/v4/app/et/v Ljava/lang/reflect/Field;
ldc android/app/Notification
ldc "actions"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 0
aload 0
putstatic android/support/v4/app/et/s Ljava/lang/reflect/Field;
aload 0
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
getstatic android/support/v4/app/et/w Z
ifne L4
iconst_1
ireturn
L2:
astore 0
ldc "NotificationCompat"
ldc "Unable to access notification actions"
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_1
putstatic android/support/v4/app/et/w Z
goto L1
L3:
astore 0
ldc "NotificationCompat"
ldc "Unable to access notification actions"
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_1
putstatic android/support/v4/app/et/w Z
goto L1
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/util/ArrayList;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/ek;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 1
aload 0
invokevirtual java/util/ArrayList/size()I
invokeinterface android/support/v4/app/el/a(I)[Landroid/support/v4/app/ek; 1
astore 4
iconst_0
istore 3
L1:
iload 3
aload 4
arraylength
if_icmpge L2
aload 0
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/os/Bundle
astore 5
aload 4
iload 3
aload 1
aload 5
ldc "icon"
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;)I
aload 5
ldc "title"
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
aload 5
ldc "actionIntent"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/PendingIntent
aload 5
ldc "extras"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
aload 5
ldc "remoteInputs"
invokestatic android/support/v4/app/aw/a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/os/Bundle;
aload 2
invokestatic android/support/v4/app/fy/a([Landroid/os/Bundle;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/fw;
invokeinterface android/support/v4/app/el/a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fw;)Landroid/support/v4/app/ek; 5
aastore
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 4
areturn
.limit locals 6
.limit stack 9
.end method

.method public static b(Landroid/app/Notification;)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
getstatic android/support/v4/app/et/q Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokestatic android/support/v4/app/et/g(Landroid/app/Notification;)[Ljava/lang/Object;
astore 0
L1:
aload 0
ifnull L8
L3:
aload 0
arraylength
istore 1
L4:
aload 2
monitorexit
L5:
iload 1
ireturn
L2:
astore 0
L6:
aload 2
monitorexit
L7:
aload 0
athrow
L8:
iconst_0
istore 1
goto L4
.limit locals 3
.limit stack 1
.end method

.method public static c(Landroid/app/Notification;)Z
aload 0
invokestatic android/support/v4/app/et/a(Landroid/app/Notification;)Landroid/os/Bundle;
ldc "android.support.localOnly"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static d(Landroid/app/Notification;)Ljava/lang/String;
aload 0
invokestatic android/support/v4/app/et/a(Landroid/app/Notification;)Landroid/os/Bundle;
ldc "android.support.groupKey"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static e(Landroid/app/Notification;)Z
aload 0
invokestatic android/support/v4/app/et/a(Landroid/app/Notification;)Landroid/os/Bundle;
ldc "android.support.isGroupSummary"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static f(Landroid/app/Notification;)Ljava/lang/String;
aload 0
invokestatic android/support/v4/app/et/a(Landroid/app/Notification;)Landroid/os/Bundle;
ldc "android.support.sortKey"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static g(Landroid/app/Notification;)[Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L3 to L4 using L5
.catch all from L3 to L4 using L2
.catch all from L4 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
getstatic android/support/v4/app/et/q Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
invokestatic android/support/v4/app/et/a()Z
ifne L3
aload 1
monitorexit
L1:
aconst_null
areturn
L3:
getstatic android/support/v4/app/et/s Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [Ljava/lang/Object;
checkcast [Ljava/lang/Object;
astore 0
L4:
aload 1
monitorexit
L6:
aload 0
areturn
L2:
astore 0
L7:
aload 1
monitorexit
L8:
aload 0
athrow
L5:
astore 0
L9:
ldc "NotificationCompat"
ldc "Unable to access notification actions"
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_1
putstatic android/support/v4/app/et/w Z
aload 1
monitorexit
L10:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method
