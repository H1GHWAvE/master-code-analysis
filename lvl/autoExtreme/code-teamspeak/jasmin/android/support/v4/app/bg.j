.bytecode 50.0
.class public final synchronized android/support/v4/app/bg
.super java/lang/Object

.field final 'a' Landroid/support/v4/app/bh;

.method <init>(Landroid/support/v4/app/bh;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/support/v4/app/bh;)Landroid/support/v4/app/bg;
new android/support/v4/app/bg
dup
aload 0
invokespecial android/support/v4/app/bg/<init>(Landroid/support/v4/app/bh;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/bl/a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
areturn
.limit locals 5
.limit stack 5
.end method

.method private a(Landroid/content/res/Configuration;)V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/content/res/Configuration;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/os/Parcelable;Ljava/util/List;)V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
aload 2
invokevirtual android/support/v4/app/bl/a(Landroid/os/Parcelable;Ljava/util/List;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/support/v4/n/v;)V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
aload 1
putfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 5
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mLoadersStarted="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 5
getfield android/support/v4/app/bh/j Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 5
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Loader Manager "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 5
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc ":"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 5
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/ct/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L0:
return
.limit locals 6
.limit stack 5
.end method

.method private a(Z)V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 2
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 2
getfield android/support/v4/app/bh/j Z
ifeq L0
aload 2
iconst_0
putfield android/support/v4/app/bh/j Z
iload 1
ifeq L1
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/d()V
L0:
return
L1:
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/c()V
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/view/Menu;)Z
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/view/Menu;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
aload 2
invokevirtual android/support/v4/app/bl/a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/view/MenuItem;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/view/Menu;)V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/b(Landroid/view/Menu;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/b(Landroid/view/MenuItem;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private d()Landroid/support/v4/app/bi;
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Landroid/support/v4/app/cr;
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 1
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
areturn
L0:
aload 1
iconst_1
putfield android/support/v4/app/bh/i Z
aload 1
aload 1
ldc "(root)"
aload 1
getfield android/support/v4/app/bh/j Z
iconst_1
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
areturn
.limit locals 2
.limit stack 5
.end method

.method private f()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
aconst_null
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/bh;Landroid/support/v4/app/bf;Landroid/support/v4/app/Fragment;)V
return
.limit locals 1
.limit stack 4
.end method

.method private g()Landroid/os/Parcelable;
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/m()Landroid/os/Parcelable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()Ljava/util/List;
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
astore 6
aconst_null
astore 4
aconst_null
astore 3
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 1
L1:
aload 3
astore 4
iload 1
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 7
aload 3
astore 5
aload 7
ifnull L2
aload 3
astore 5
aload 7
getfield android/support/v4/app/Fragment/V Z
ifeq L2
aload 3
astore 4
aload 3
ifnonnull L3
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
L3:
aload 4
aload 7
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 7
iconst_1
putfield android/support/v4/app/Fragment/W Z
aload 7
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
ifnull L4
aload 7
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/z I
istore 2
L5:
aload 7
iload 2
putfield android/support/v4/app/Fragment/D I
aload 4
astore 5
getstatic android/support/v4/app/bl/b Z
ifeq L2
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "retainNonConfig: keeping retained "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 4
astore 5
L2:
iload 1
iconst_1
iadd
istore 1
aload 5
astore 3
goto L1
L4:
iconst_m1
istore 2
goto L5
L0:
aload 4
areturn
.limit locals 8
.limit stack 4
.end method

.method private i()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/n()V
return
.limit locals 1
.limit stack 1
.end method

.method private j()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/o()V
return
.limit locals 1
.limit stack 1
.end method

.method private k()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/p()V
return
.limit locals 1
.limit stack 1
.end method

.method private l()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/q()V
return
.limit locals 1
.limit stack 1
.end method

.method private m()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
iconst_4
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private n()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/r()V
return
.limit locals 1
.limit stack 1
.end method

.method private o()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
iconst_2
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private p()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
iconst_1
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private q()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/s()V
return
.limit locals 1
.limit stack 1
.end method

.method private r()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/t()V
return
.limit locals 1
.limit stack 1
.end method

.method private s()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 1
aload 1
getfield android/support/v4/app/bh/j Z
ifne L0
aload 1
iconst_1
putfield android/support/v4/app/bh/j Z
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L1
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/b()V
L2:
aload 1
iconst_1
putfield android/support/v4/app/bh/i Z
L0:
return
L1:
aload 1
getfield android/support/v4/app/bh/i Z
ifne L2
aload 1
aload 1
ldc "(root)"
aload 1
getfield android/support/v4/app/bh/j Z
iconst_0
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L2
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/f Z
ifne L2
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/b()V
goto L2
.limit locals 2
.limit stack 5
.end method

.method private t()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 1
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/d()V
L0:
return
.limit locals 2
.limit stack 1
.end method

.method private u()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 1
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/g()V
L0:
return
.limit locals 2
.limit stack 1
.end method

.method private v()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 5
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
ifnull L0
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
invokevirtual android/support/v4/n/v/size()I
istore 3
iload 3
anewarray android/support/v4/app/ct
astore 4
iload 3
iconst_1
isub
istore 1
L1:
iload 1
iflt L2
aload 4
iload 1
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
iload 1
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
checkcast android/support/v4/app/ct
aastore
iload 1
iconst_1
isub
istore 1
goto L1
L2:
iconst_0
istore 1
L3:
iload 1
iload 3
if_icmpge L0
aload 4
iload 1
aaload
astore 5
aload 5
getfield android/support/v4/app/ct/g Z
ifeq L4
getstatic android/support/v4/app/ct/b Z
ifeq L5
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Finished Retaining in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L5:
aload 5
iconst_0
putfield android/support/v4/app/ct/g Z
aload 5
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 2
L6:
iload 2
iflt L4
aload 5
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 2
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 6
aload 6
getfield android/support/v4/app/cu/i Z
ifeq L7
getstatic android/support/v4/app/ct/b Z
ifeq L8
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Finished Retaining: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L8:
aload 6
iconst_0
putfield android/support/v4/app/cu/i Z
aload 6
getfield android/support/v4/app/cu/h Z
aload 6
getfield android/support/v4/app/cu/j Z
if_icmpeq L7
aload 6
getfield android/support/v4/app/cu/h Z
ifne L7
aload 6
invokevirtual android/support/v4/app/cu/b()V
L7:
aload 6
getfield android/support/v4/app/cu/h Z
ifeq L9
aload 6
getfield android/support/v4/app/cu/e Z
ifeq L9
aload 6
getfield android/support/v4/app/cu/k Z
ifne L9
aload 6
aload 6
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 6
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L9:
iload 2
iconst_1
isub
istore 2
goto L6
L4:
aload 5
invokevirtual android/support/v4/app/ct/f()V
iload 1
iconst_1
iadd
istore 1
goto L3
L0:
return
.limit locals 7
.limit stack 4
.end method

.method private w()Landroid/support/v4/n/v;
iconst_0
istore 2
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 5
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
ifnull L0
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
invokevirtual android/support/v4/n/v/size()I
istore 4
iload 4
anewarray android/support/v4/app/ct
astore 6
iload 4
iconst_1
isub
istore 1
L1:
iload 1
iflt L2
aload 6
iload 1
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
iload 1
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
checkcast android/support/v4/app/ct
aastore
iload 1
iconst_1
isub
istore 1
goto L1
L2:
iconst_0
istore 1
L3:
iload 1
istore 3
iload 2
iload 4
if_icmpge L4
aload 6
iload 2
aaload
astore 7
aload 7
getfield android/support/v4/app/ct/g Z
ifeq L5
iconst_1
istore 1
L6:
iload 2
iconst_1
iadd
istore 2
goto L3
L5:
aload 7
invokevirtual android/support/v4/app/ct/g()V
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
aload 7
getfield android/support/v4/app/ct/e Ljava/lang/String;
invokevirtual android/support/v4/n/v/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L6
L0:
iconst_0
istore 3
L4:
iload 3
ifeq L7
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
areturn
L7:
aconst_null
areturn
.limit locals 8
.limit stack 4
.end method

.method public final a()I
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
astore 1
aload 1
ifnonnull L0
iconst_0
ireturn
L0:
aload 1
invokeinterface java/util/List/size()I 0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnonnull L0
aconst_null
areturn
L0:
aload 1
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokeinterface java/util/List/addAll(Ljava/util/Collection;)Z 1
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b()V
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
iconst_0
putfield android/support/v4/app/bl/z Z
return
.limit locals 1
.limit stack 2
.end method

.method public final c()Z
aload 0
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/k()Z
ireturn
.limit locals 1
.limit stack 1
.end method
