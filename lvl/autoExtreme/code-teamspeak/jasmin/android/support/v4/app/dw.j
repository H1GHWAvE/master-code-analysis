.bytecode 50.0
.class final synchronized android/support/v4/app/dw
.super android/support/v4/app/dv

.method <init>()V
aload 0
invokespecial android/support/v4/app/dv/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification;
new android/support/v4/app/ei
dup
aload 1
getfield android/support/v4/app/dm/a Landroid/content/Context;
aload 1
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
getfield android/support/v4/app/dm/b Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/c Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/h Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/f Landroid/widget/RemoteViews;
aload 1
getfield android/support/v4/app/dm/i I
aload 1
getfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 1
getfield android/support/v4/app/dm/e Landroid/app/PendingIntent;
aload 1
getfield android/support/v4/app/dm/g Landroid/graphics/Bitmap;
aload 1
getfield android/support/v4/app/dm/o I
aload 1
getfield android/support/v4/app/dm/p I
aload 1
getfield android/support/v4/app/dm/q Z
aload 1
getfield android/support/v4/app/dm/k Z
aload 1
getfield android/support/v4/app/dm/l Z
aload 1
getfield android/support/v4/app/dm/j I
aload 1
getfield android/support/v4/app/dm/n Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/v Z
aload 1
getfield android/support/v4/app/dm/w Ljava/lang/String;
aload 1
getfield android/support/v4/app/dm/C Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/dm/x Landroid/os/Bundle;
aload 1
getfield android/support/v4/app/dm/y I
aload 1
getfield android/support/v4/app/dm/z I
aload 1
getfield android/support/v4/app/dm/A Landroid/app/Notification;
aload 1
getfield android/support/v4/app/dm/r Ljava/lang/String;
aload 1
getfield android/support/v4/app/dm/s Z
aload 1
getfield android/support/v4/app/dm/t Ljava/lang/String;
invokespecial android/support/v4/app/ei/<init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/lang/String;Ljava/util/ArrayList;Landroid/os/Bundle;IILandroid/app/Notification;Ljava/lang/String;ZLjava/lang/String;)V
astore 3
aload 3
aload 1
getfield android/support/v4/app/dm/u Ljava/util/ArrayList;
invokestatic android/support/v4/app/dd/a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V
aload 3
aload 1
getfield android/support/v4/app/dm/m Landroid/support/v4/app/ed;
invokestatic android/support/v4/app/dd/a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
aload 2
aload 1
aload 3
invokevirtual android/support/v4/app/dn/a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dc;)Landroid/app/Notification;
areturn
.limit locals 4
.limit stack 29
.end method

.method public final a(Landroid/support/v4/app/em;)Landroid/os/Bundle;
aconst_null
astore 4
iconst_0
istore 2
aload 1
ifnonnull L0
aconst_null
areturn
L0:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 5
aload 4
astore 3
aload 1
invokevirtual android/support/v4/app/em/d()[Ljava/lang/String;
ifnull L1
aload 4
astore 3
aload 1
invokevirtual android/support/v4/app/em/d()[Ljava/lang/String;
arraylength
iconst_1
if_icmple L1
aload 1
invokevirtual android/support/v4/app/em/d()[Ljava/lang/String;
iconst_0
aaload
astore 3
L1:
aload 1
invokevirtual android/support/v4/app/em/a()[Ljava/lang/String;
arraylength
anewarray android/os/Parcelable
astore 4
L2:
iload 2
aload 4
arraylength
if_icmpge L3
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 6
aload 6
ldc "text"
aload 1
invokevirtual android/support/v4/app/em/a()[Ljava/lang/String;
iload 2
aaload
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 6
ldc "author"
aload 3
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 4
iload 2
aload 6
aastore
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 5
ldc "messages"
aload 4
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
aload 1
invokevirtual android/support/v4/app/em/g()Landroid/support/v4/app/fw;
astore 3
aload 3
ifnull L4
aload 5
ldc "remote_input"
aload 3
invokestatic android/support/v4/app/eh/a(Landroid/support/v4/app/fw;)Landroid/app/RemoteInput;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L4:
aload 5
ldc "on_reply"
aload 1
invokevirtual android/support/v4/app/em/b()Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 5
ldc "on_read"
aload 1
invokevirtual android/support/v4/app/em/c()Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 5
ldc "participants"
aload 1
invokevirtual android/support/v4/app/em/d()[Ljava/lang/String;
invokevirtual android/os/Bundle/putStringArray(Ljava/lang/String;[Ljava/lang/String;)V
aload 5
ldc "timestamp"
aload 1
invokevirtual android/support/v4/app/em/f()J
invokevirtual android/os/Bundle/putLong(Ljava/lang/String;J)V
aload 5
areturn
.limit locals 7
.limit stack 4
.end method

.method public final a(Landroid/os/Bundle;Landroid/support/v4/app/en;Landroid/support/v4/app/fx;)Landroid/support/v4/app/em;
iconst_0
istore 6
aload 1
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 1
ldc "messages"
invokevirtual android/os/Bundle/getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;
astore 8
aload 8
ifnull L2
aload 8
arraylength
anewarray java/lang/String
astore 7
iconst_0
istore 4
L3:
iload 4
aload 7
arraylength
if_icmpge L4
aload 8
iload 4
aaload
instanceof android/os/Bundle
ifne L5
iload 6
istore 5
L6:
iload 5
ifeq L1
L7:
aload 1
ldc "on_read"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/PendingIntent
astore 8
aload 1
ldc "on_reply"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/PendingIntent
astore 9
aload 1
ldc "remote_input"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/RemoteInput
astore 11
aload 1
ldc "participants"
invokevirtual android/os/Bundle/getStringArray(Ljava/lang/String;)[Ljava/lang/String;
astore 10
aload 10
ifnull L1
aload 10
arraylength
iconst_1
if_icmpne L1
aload 11
ifnull L8
aload 3
aload 11
invokevirtual android/app/RemoteInput/getResultKey()Ljava/lang/String;
aload 11
invokevirtual android/app/RemoteInput/getLabel()Ljava/lang/CharSequence;
aload 11
invokevirtual android/app/RemoteInput/getChoices()[Ljava/lang/CharSequence;
aload 11
invokevirtual android/app/RemoteInput/getAllowFreeFormInput()Z
aload 11
invokevirtual android/app/RemoteInput/getExtras()Landroid/os/Bundle;
invokeinterface android/support/v4/app/fx/a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw; 5
astore 3
L9:
aload 2
aload 7
aload 3
aload 9
aload 8
aload 10
aload 1
ldc "timestamp"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
invokeinterface android/support/v4/app/en/a([Ljava/lang/String;Landroid/support/v4/app/fw;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)Landroid/support/v4/app/em; 7
areturn
L5:
aload 7
iload 4
aload 8
iload 4
aaload
checkcast android/os/Bundle
ldc "text"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
aastore
iload 6
istore 5
aload 7
iload 4
aaload
ifnull L6
iload 4
iconst_1
iadd
istore 4
goto L3
L8:
aconst_null
astore 3
goto L9
L4:
iconst_1
istore 5
goto L6
L2:
aconst_null
astore 7
goto L7
.limit locals 12
.limit stack 8
.end method

.method public final c(Landroid/app/Notification;)Ljava/lang/String;
aload 1
getfield android/app/Notification/category Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method
