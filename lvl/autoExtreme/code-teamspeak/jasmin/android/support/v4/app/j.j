.bytecode 50.0
.class final synchronized android/support/v4/app/j
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "ActionBarDrawerToggleHoneycomb"

.field private static final 'b' [I

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16843531
iastore
putstatic android/support/v4/app/j/b [I
return
.limit locals 0
.limit stack 4
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;
aload 0
getstatic android/support/v4/app/j/b [I
invokevirtual android/app/Activity/obtainStyledAttributes([I)Landroid/content/res/TypedArray;
astore 0
aload 0
iconst_0
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/Object;Landroid/app/Activity;I)Ljava/lang/Object;
.catch java/lang/Exception from L0 to L1 using L2
aload 0
ifnonnull L3
new android/support/v4/app/k
dup
aload 1
invokespecial android/support/v4/app/k/<init>(Landroid/app/Activity;)V
astore 0
L4:
aload 0
checkcast android/support/v4/app/k
astore 3
aload 3
getfield android/support/v4/app/k/a Ljava/lang/reflect/Method;
ifnull L1
L0:
aload 1
invokevirtual android/app/Activity/getActionBar()Landroid/app/ActionBar;
astore 1
aload 3
getfield android/support/v4/app/k/b Ljava/lang/reflect/Method;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmpgt L1
aload 1
aload 1
invokevirtual android/app/ActionBar/getSubtitle()Ljava/lang/CharSequence;
invokevirtual android/app/ActionBar/setSubtitle(Ljava/lang/CharSequence;)V
L1:
aload 0
areturn
L2:
astore 1
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set content description via JB-MR2 API"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 0
areturn
L3:
goto L4
.limit locals 4
.limit stack 6
.end method

.method public static a(Ljava/lang/Object;Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Ljava/lang/Object;
.catch java/lang/Exception from L0 to L1 using L2
aload 0
ifnonnull L3
new android/support/v4/app/k
dup
aload 1
invokespecial android/support/v4/app/k/<init>(Landroid/app/Activity;)V
astore 0
L4:
aload 0
checkcast android/support/v4/app/k
astore 4
aload 4
getfield android/support/v4/app/k/a Ljava/lang/reflect/Method;
ifnull L5
L0:
aload 1
invokevirtual android/app/Activity/getActionBar()Landroid/app/ActionBar;
astore 1
aload 4
getfield android/support/v4/app/k/a Ljava/lang/reflect/Method;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
getfield android/support/v4/app/k/b Ljava/lang/reflect/Method;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 0
areturn
L2:
astore 1
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set home-as-up indicator via JB-MR2 API"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 0
areturn
L5:
aload 4
getfield android/support/v4/app/k/c Landroid/widget/ImageView;
ifnull L6
aload 4
getfield android/support/v4/app/k/c Landroid/widget/ImageView;
aload 2
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
areturn
L6:
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set home-as-up indicator"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
areturn
L3:
goto L4
.limit locals 5
.limit stack 6
.end method
