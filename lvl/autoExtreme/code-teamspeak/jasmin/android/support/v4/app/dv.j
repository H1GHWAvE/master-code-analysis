.bytecode 50.0
.class synchronized android/support/v4/app/dv
.super android/support/v4/app/ec

.method <init>()V
aload 0
invokespecial android/support/v4/app/ec/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification;
new android/support/v4/app/eg
dup
aload 1
getfield android/support/v4/app/dm/a Landroid/content/Context;
aload 1
getfield android/support/v4/app/dm/B Landroid/app/Notification;
aload 1
getfield android/support/v4/app/dm/b Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/c Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/h Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/f Landroid/widget/RemoteViews;
aload 1
getfield android/support/v4/app/dm/i I
aload 1
getfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 1
getfield android/support/v4/app/dm/e Landroid/app/PendingIntent;
aload 1
getfield android/support/v4/app/dm/g Landroid/graphics/Bitmap;
aload 1
getfield android/support/v4/app/dm/o I
aload 1
getfield android/support/v4/app/dm/p I
aload 1
getfield android/support/v4/app/dm/q Z
aload 1
getfield android/support/v4/app/dm/k Z
aload 1
getfield android/support/v4/app/dm/l Z
aload 1
getfield android/support/v4/app/dm/j I
aload 1
getfield android/support/v4/app/dm/n Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/dm/v Z
aload 1
getfield android/support/v4/app/dm/C Ljava/util/ArrayList;
aload 1
getfield android/support/v4/app/dm/x Landroid/os/Bundle;
aload 1
getfield android/support/v4/app/dm/r Ljava/lang/String;
aload 1
getfield android/support/v4/app/dm/s Z
aload 1
getfield android/support/v4/app/dm/t Ljava/lang/String;
invokespecial android/support/v4/app/eg/<init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V
astore 3
aload 3
aload 1
getfield android/support/v4/app/dm/u Ljava/util/ArrayList;
invokestatic android/support/v4/app/dd/a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V
aload 3
aload 1
getfield android/support/v4/app/dm/m Landroid/support/v4/app/ed;
invokestatic android/support/v4/app/dd/a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V
aload 2
aload 1
aload 3
invokevirtual android/support/v4/app/dn/a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dc;)Landroid/app/Notification;
areturn
.limit locals 4
.limit stack 25
.end method

.method public final a(Landroid/app/Notification;I)Landroid/support/v4/app/df;
getstatic android/support/v4/app/df/e Landroid/support/v4/app/el;
astore 3
getstatic android/support/v4/app/fn/c Landroid/support/v4/app/fx;
astore 4
aload 1
getfield android/app/Notification/actions [Landroid/app/Notification$Action;
iload 2
aaload
aload 3
aload 4
invokestatic android/support/v4/app/ef/a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
checkcast android/support/v4/app/df
areturn
.limit locals 5
.limit stack 3
.end method

.method public final a([Landroid/support/v4/app/df;)Ljava/util/ArrayList;
aload 1
ifnonnull L0
aconst_null
astore 4
L1:
aload 4
areturn
L0:
new java/util/ArrayList
dup
aload 1
arraylength
invokespecial java/util/ArrayList/<init>(I)V
astore 5
aload 1
arraylength
istore 3
iconst_0
istore 2
L2:
aload 5
astore 4
iload 2
iload 3
if_icmpge L1
aload 5
aload 1
iload 2
aaload
invokestatic android/support/v4/app/ef/a(Landroid/support/v4/app/ek;)Landroid/app/Notification$Action;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 6
.limit stack 3
.end method

.method public final a(Ljava/util/ArrayList;)[Landroid/support/v4/app/df;
getstatic android/support/v4/app/df/e Landroid/support/v4/app/el;
astore 4
getstatic android/support/v4/app/fn/c Landroid/support/v4/app/fx;
astore 5
aload 1
ifnonnull L0
aconst_null
astore 1
L1:
aload 1
checkcast [Landroid/support/v4/app/df;
checkcast [Landroid/support/v4/app/df;
areturn
L0:
aload 4
aload 1
invokevirtual java/util/ArrayList/size()I
invokeinterface android/support/v4/app/el/a(I)[Landroid/support/v4/app/ek; 1
astore 3
iconst_0
istore 2
L2:
iload 2
aload 3
arraylength
if_icmpge L3
aload 3
iload 2
aload 1
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/app/Notification$Action
aload 4
aload 5
invokestatic android/support/v4/app/ef/a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
aastore
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 3
astore 1
goto L1
.limit locals 6
.limit stack 5
.end method

.method public final d(Landroid/app/Notification;)Z
aload 1
getfield android/app/Notification/flags I
sipush 256
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final e(Landroid/app/Notification;)Ljava/lang/String;
aload 1
invokevirtual android/app/Notification/getGroup()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final f(Landroid/app/Notification;)Z
aload 1
getfield android/app/Notification/flags I
sipush 512
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final g(Landroid/app/Notification;)Ljava/lang/String;
aload 1
invokevirtual android/app/Notification/getSortKey()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 1
.end method
