.bytecode 50.0
.class public final synchronized android/support/v4/app/do
.super java/lang/Object
.implements android/support/v4/app/ds

.field private static final 'a' Ljava/lang/String; = "CarExtender"

.field private static final 'b' Ljava/lang/String; = "android.car.EXTENSIONS"

.field private static final 'c' Ljava/lang/String; = "large_icon"

.field private static final 'd' Ljava/lang/String; = "car_conversation"

.field private static final 'e' Ljava/lang/String; = "app_color"

.field private 'f' Landroid/graphics/Bitmap;

.field private 'g' Landroid/support/v4/app/dp;

.field private 'h' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v4/app/do/h I
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(Landroid/app/Notification;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v4/app/do/h I
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L0
L1:
return
L0:
aload 1
invokestatic android/support/v4/app/dd/a(Landroid/app/Notification;)Landroid/os/Bundle;
ifnonnull L2
aconst_null
astore 1
L3:
aload 1
ifnull L1
aload 0
aload 1
ldc "large_icon"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/graphics/Bitmap
putfield android/support/v4/app/do/f Landroid/graphics/Bitmap;
aload 0
aload 1
ldc "app_color"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/do/h I
aload 1
ldc "car_conversation"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
astore 1
aload 0
invokestatic android/support/v4/app/dd/a()Landroid/support/v4/app/du;
aload 1
getstatic android/support/v4/app/dp/a Landroid/support/v4/app/en;
getstatic android/support/v4/app/fn/c Landroid/support/v4/app/fx;
invokeinterface android/support/v4/app/du/a(Landroid/os/Bundle;Landroid/support/v4/app/en;Landroid/support/v4/app/fx;)Landroid/support/v4/app/em; 3
checkcast android/support/v4/app/dp
putfield android/support/v4/app/do/g Landroid/support/v4/app/dp;
return
L2:
aload 1
invokestatic android/support/v4/app/dd/a(Landroid/app/Notification;)Landroid/os/Bundle;
ldc "android.car.EXTENSIONS"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
astore 1
goto L3
.limit locals 2
.limit stack 5
.end method

.method private a()I
.annotation invisible Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/v4/app/do/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)Landroid/support/v4/app/do;
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v4/app/do/h I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/do;
aload 0
aload 1
putfield android/support/v4/app/do/f Landroid/graphics/Bitmap;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/dp;)Landroid/support/v4/app/do;
aload 0
aload 1
putfield android/support/v4/app/do/g Landroid/support/v4/app/dp;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b()Landroid/graphics/Bitmap;
aload 0
getfield android/support/v4/app/do/f Landroid/graphics/Bitmap;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Landroid/support/v4/app/dp;
aload 0
getfield android/support/v4/app/do/g Landroid/support/v4/app/dp;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v4/app/dm;)Landroid/support/v4/app/dm;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L0
aload 1
areturn
L0:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 2
aload 0
getfield android/support/v4/app/do/f Landroid/graphics/Bitmap;
ifnull L1
aload 2
ldc "large_icon"
aload 0
getfield android/support/v4/app/do/f Landroid/graphics/Bitmap;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L1:
aload 0
getfield android/support/v4/app/do/h I
ifeq L2
aload 2
ldc "app_color"
aload 0
getfield android/support/v4/app/do/h I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L2:
aload 0
getfield android/support/v4/app/do/g Landroid/support/v4/app/dp;
ifnull L3
aload 2
ldc "car_conversation"
invokestatic android/support/v4/app/dd/a()Landroid/support/v4/app/du;
aload 0
getfield android/support/v4/app/do/g Landroid/support/v4/app/dp;
invokeinterface android/support/v4/app/du/a(Landroid/support/v4/app/em;)Landroid/os/Bundle; 1
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
L3:
aload 1
invokevirtual android/support/v4/app/dm/b()Landroid/os/Bundle;
ldc "android.car.EXTENSIONS"
aload 2
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
aload 1
areturn
.limit locals 3
.limit stack 4
.end method
