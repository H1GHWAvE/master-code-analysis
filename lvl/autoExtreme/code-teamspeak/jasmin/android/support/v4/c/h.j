.bytecode 50.0
.class public synchronized android/support/v4/c/h
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "ContextCompat"

.field private static final 'b' Ljava/lang/String; = "Android"

.field private static final 'c' Ljava/lang/String; = "data"

.field private static final 'd' Ljava/lang/String; = "obb"

.field private static final 'e' Ljava/lang/String; = "files"

.field private static final 'f' Ljava/lang/String; = "cache"

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)I
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
aload 0
aload 1
invokestatic android/os/Process/myPid()I
invokestatic android/os/Process/myUid()I
invokevirtual android/content/Context/checkPermission(Ljava/lang/String;II)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public static final a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
iload 1
invokevirtual android/content/Context/getDrawable(I)Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/io/File;)Ljava/io/File;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
ldc android/support/v4/c/h
monitorenter
aload 0
astore 2
L0:
aload 0
invokevirtual java/io/File/exists()Z
ifne L7
L1:
aload 0
astore 2
L3:
aload 0
invokevirtual java/io/File/mkdirs()Z
ifne L7
aload 0
invokevirtual java/io/File/exists()Z
istore 1
L4:
iload 1
ifeq L5
aload 0
astore 2
L7:
ldc android/support/v4/c/h
monitorexit
aload 2
areturn
L5:
ldc "ContextCompat"
new java/lang/StringBuilder
dup
ldc "Unable to create files subdir "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/io/File/getPath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L6:
aconst_null
astore 2
goto L7
L2:
astore 0
ldc android/support/v4/c/h
monitorexit
aload 0
athrow
.limit locals 3
.limit stack 4
.end method

.method private static transient a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
aload 1
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 1
iload 2
aaload
astore 4
aload 0
ifnonnull L2
new java/io/File
dup
aload 4
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
aload 4
ifnull L4
new java/io/File
dup
aload 0
aload 4
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 0
goto L3
L1:
aload 0
areturn
L4:
goto L3
.limit locals 5
.limit stack 4
.end method

.method public static a(Landroid/content/Context;[Landroid/content/Intent;)Z
getstatic android/os/Build$VERSION/SDK_INT I
istore 2
iload 2
bipush 16
if_icmplt L0
aload 0
aload 1
aconst_null
invokevirtual android/content/Context/startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V
iconst_1
ireturn
L0:
iload 2
bipush 11
if_icmplt L1
aload 0
aload 1
invokevirtual android/content/Context/startActivities([Landroid/content/Intent;)V
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/content/Context;)[Ljava/io/File;
getstatic android/os/Build$VERSION/SDK_INT I
istore 1
iload 1
bipush 19
if_icmplt L0
aload 0
invokevirtual android/content/Context/getObbDirs()[Ljava/io/File;
areturn
L0:
iload 1
bipush 11
if_icmplt L1
aload 0
invokevirtual android/content/Context/getObbDir()Ljava/io/File;
astore 0
L2:
iconst_1
anewarray java/io/File
dup
iconst_0
aload 0
aastore
areturn
L1:
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "Android"
aastore
dup
iconst_1
ldc "obb"
aastore
dup
iconst_2
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
aastore
invokestatic android/support/v4/c/h/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 0
goto L2
.limit locals 2
.limit stack 5
.end method

.method private static b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L0
aload 0
iload 1
invokevirtual android/content/Context/getColorStateList(I)Landroid/content/res/ColorStateList;
areturn
L0:
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getColorStateList(I)Landroid/content/res/ColorStateList;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/content/Context;[Landroid/content/Intent;)Z
aload 0
aload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;[Landroid/content/Intent;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/content/Context;)[Ljava/io/File;
getstatic android/os/Build$VERSION/SDK_INT I
istore 1
iload 1
bipush 19
if_icmplt L0
aload 0
invokevirtual android/content/Context/getExternalCacheDirs()[Ljava/io/File;
areturn
L0:
iload 1
bipush 8
if_icmplt L1
aload 0
invokevirtual android/content/Context/getExternalCacheDir()Ljava/io/File;
astore 0
L2:
iconst_1
anewarray java/io/File
dup
iconst_0
aload 0
aastore
areturn
L1:
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "Android"
aastore
dup
iconst_1
ldc "data"
aastore
dup
iconst_2
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
aastore
dup
iconst_3
ldc "cache"
aastore
invokestatic android/support/v4/c/h/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 0
goto L2
.limit locals 2
.limit stack 5
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;
getstatic android/os/Build$VERSION/SDK_INT I
istore 2
iload 2
bipush 19
if_icmplt L0
aload 0
aload 1
invokevirtual android/content/Context/getExternalFilesDirs(Ljava/lang/String;)[Ljava/io/File;
areturn
L0:
iload 2
bipush 8
if_icmplt L1
aload 0
aload 1
invokevirtual android/content/Context/getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;
astore 0
L2:
iconst_1
anewarray java/io/File
dup
iconst_0
aload 0
aastore
areturn
L1:
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "Android"
aastore
dup
iconst_1
ldc "data"
aastore
dup
iconst_2
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
aastore
dup
iconst_3
ldc "files"
aastore
dup
iconst_4
aload 1
aastore
invokestatic android/support/v4/c/h/a(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
astore 0
goto L2
.limit locals 3
.limit stack 5
.end method

.method private static c(Landroid/content/Context;I)I
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L0
aload 0
iload 1
invokevirtual android/content/Context/getColor(I)I
ireturn
L0:
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getColor(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static c(Landroid/content/Context;)Ljava/io/File;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
invokevirtual android/content/Context/getNoBackupFilesDir()Ljava/io/File;
areturn
L0:
new java/io/File
dup
aload 0
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/dataDir Ljava/lang/String;
ldc "no_backup"
invokespecial java/io/File/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokestatic android/support/v4/c/h/a(Ljava/io/File;)Ljava/io/File;
areturn
.limit locals 1
.limit stack 4
.end method

.method private static d(Landroid/content/Context;)Ljava/io/File;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
invokevirtual android/content/Context/getCodeCacheDir()Ljava/io/File;
areturn
L0:
new java/io/File
dup
aload 0
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/dataDir Ljava/lang/String;
ldc "code_cache"
invokespecial java/io/File/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokestatic android/support/v4/c/h/a(Ljava/io/File;)Ljava/io/File;
areturn
.limit locals 1
.limit stack 4
.end method
