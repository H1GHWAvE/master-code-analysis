.bytecode 50.0
.class public final synchronized android/support/v4/c/t
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

.field public static final 'b' Ljava/lang/String; = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

.field public static final 'c' Ljava/lang/String; = "android.intent.extra.changed_package_list"

.field public static final 'd' Ljava/lang/String; = "android.intent.extra.changed_uid_list"

.field public static final 'e' Ljava/lang/String; = "android.intent.extra.HTML_TEXT"

.field public static final 'f' I = 16384


.field public static final 'g' I = 32768


.field private static final 'h' Landroid/support/v4/c/u;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 15
if_icmplt L0
new android/support/v4/c/x
dup
invokespecial android/support/v4/c/x/<init>()V
putstatic android/support/v4/c/t/h Landroid/support/v4/c/u;
return
L0:
iload 0
bipush 11
if_icmplt L1
new android/support/v4/c/w
dup
invokespecial android/support/v4/c/w/<init>()V
putstatic android/support/v4/c/t/h Landroid/support/v4/c/u;
return
L1:
new android/support/v4/c/v
dup
invokespecial android/support/v4/c/v/<init>()V
putstatic android/support/v4/c/t/h Landroid/support/v4/c/u;
return
.limit locals 1
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/ComponentName;)Landroid/content/Intent;
getstatic android/support/v4/c/t/h Landroid/support/v4/c/u;
aload 0
invokeinterface android/support/v4/c/u/a(Landroid/content/ComponentName;)Landroid/content/Intent; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
getstatic android/support/v4/c/t/h Landroid/support/v4/c/u;
aload 0
aload 1
invokeinterface android/support/v4/c/u/a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method private static b(Landroid/content/ComponentName;)Landroid/content/Intent;
getstatic android/support/v4/c/t/h Landroid/support/v4/c/u;
aload 0
invokeinterface android/support/v4/c/u/b(Landroid/content/ComponentName;)Landroid/content/Intent; 1
areturn
.limit locals 1
.limit stack 2
.end method
