.bytecode 50.0
.class final synchronized android/support/v4/l/a/a
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "android.support.v4.speech.tts"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)Landroid/speech/tts/TextToSpeech;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmpge L0
aload 2
ifnonnull L1
new android/speech/tts/TextToSpeech
dup
aload 0
aload 1
invokespecial android/speech/tts/TextToSpeech/<init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V
areturn
L1:
ldc "android.support.v4.speech.tts"
ldc "Can't specify tts engine on this device"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
new android/speech/tts/TextToSpeech
dup
aload 0
aload 1
invokespecial android/speech/tts/TextToSpeech/<init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V
areturn
L0:
new android/speech/tts/TextToSpeech
dup
aload 0
aload 1
aload 2
invokespecial android/speech/tts/TextToSpeech/<init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;Ljava/lang/String;)V
areturn
.limit locals 3
.limit stack 5
.end method
