.bytecode 50.0
.class final synchronized android/support/v4/k/b
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "DocumentFile"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
aload 0
aload 1
ldc "mime_type"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
astore 0
ldc "vnd.android.document/directory"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aconst_null
astore 0
L0:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Exception from L4 to L5 using L6
.catch all from L4 to L5 using L7
.catch java/lang/Exception from L8 to L9 using L6
.catch all from L8 to L9 using L7
.catch java/lang/Exception from L10 to L11 using L6
.catch all from L10 to L11 using L7
.catch all from L12 to L13 using L7
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
astore 0
L0:
aload 0
aload 1
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 2
aastore
aconst_null
aconst_null
aconst_null
invokevirtual android/content/ContentResolver/query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 1
L1:
aload 1
astore 0
L4:
aload 1
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L14
L5:
aload 1
astore 0
L8:
aload 1
iconst_0
invokeinterface android/database/Cursor/isNull(I)Z 1
ifne L14
L9:
aload 1
astore 0
L10:
aload 1
iconst_0
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
astore 2
L11:
aload 1
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
aload 2
areturn
L14:
aload 1
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
aconst_null
areturn
L2:
astore 2
aconst_null
astore 1
L15:
aload 1
astore 0
L12:
ldc "DocumentFile"
new java/lang/StringBuilder
dup
ldc "Failed query: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L13:
aload 1
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
aconst_null
areturn
L3:
astore 1
aconst_null
astore 0
L16:
aload 0
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
aload 1
athrow
L7:
astore 1
goto L16
L6:
astore 2
goto L15
.limit locals 3
.limit stack 6
.end method

.method private static a(Ljava/lang/AutoCloseable;)V
.catch java/lang/RuntimeException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
aload 0
ifnull L1
L0:
aload 0
invokeinterface java/lang/AutoCloseable/close()V 0
L1:
return
L2:
astore 0
aload 0
athrow
L3:
astore 0
return
.limit locals 1
.limit stack 1
.end method

.method static b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Exception from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch all from L7 to L8 using L9
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
astore 0
L0:
aload 0
aload 1
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 2
aastore
aconst_null
aconst_null
aconst_null
invokevirtual android/content/ContentResolver/query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 0
L1:
aload 0
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L10
aload 0
iconst_0
invokeinterface android/database/Cursor/isNull(I)Z 1
ifne L10
aload 0
iconst_0
invokeinterface android/database/Cursor/getLong(I)J 1
lstore 3
L4:
aload 0
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
lload 3
lreturn
L10:
aload 0
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
lconst_0
lreturn
L2:
astore 1
aconst_null
astore 0
L7:
ldc "DocumentFile"
new java/lang/StringBuilder
dup
ldc "Failed query: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L8:
aload 0
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
lconst_0
lreturn
L3:
astore 1
aconst_null
astore 0
L11:
aload 0
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
aload 1
athrow
L6:
astore 1
goto L11
L9:
astore 1
goto L11
L5:
astore 1
goto L7
.limit locals 5
.limit stack 6
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;)Z
ldc "vnd.android.document/directory"
aload 0
aload 1
ldc "mime_type"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static c(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I
aload 0
aload 1
aload 2
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
l2i
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static c(Landroid/content/Context;Landroid/net/Uri;)Z
aload 0
aload 1
ldc "mime_type"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
astore 0
ldc "vnd.android.document/directory"
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L1
L0:
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method public static d(Landroid/content/Context;Landroid/net/Uri;)Z
aload 0
aload 1
iconst_1
invokevirtual android/content/Context/checkCallingOrSelfUriPermission(Landroid/net/Uri;I)I
ifeq L0
L1:
iconst_0
ireturn
L0:
aload 0
aload 1
ldc "mime_type"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method public static e(Landroid/content/Context;Landroid/net/Uri;)Z
aload 0
aload 1
iconst_2
invokevirtual android/content/Context/checkCallingOrSelfUriPermission(Landroid/net/Uri;I)I
ifeq L0
L1:
iconst_0
ireturn
L0:
aload 0
aload 1
ldc "mime_type"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 0
aload 1
ldc "flags"
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
l2i
istore 2
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
iload 2
iconst_4
iand
ifeq L2
iconst_1
ireturn
L2:
ldc "vnd.android.document/directory"
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
iload 2
bipush 8
iand
ifeq L3
iconst_1
ireturn
L3:
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
iload 2
iconst_2
iand
ifeq L1
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method public static f(Landroid/content/Context;Landroid/net/Uri;)Z
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
invokestatic android/provider/DocumentsContract/deleteDocument(Landroid/content/ContentResolver;Landroid/net/Uri;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static g(Landroid/content/Context;Landroid/net/Uri;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Exception from L4 to L5 using L6
.catch all from L4 to L5 using L7
.catch all from L8 to L9 using L7
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
astore 0
L0:
aload 0
aload 1
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "document_id"
aastore
aconst_null
aconst_null
aconst_null
invokevirtual android/content/ContentResolver/query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 1
L1:
aload 1
astore 0
L4:
aload 1
invokeinterface android/database/Cursor/getCount()I 0
istore 2
L5:
iload 2
ifle L10
iconst_1
istore 3
L11:
aload 1
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
iload 3
ireturn
L10:
iconst_0
istore 3
goto L11
L2:
astore 4
aconst_null
astore 1
L12:
aload 1
astore 0
L8:
ldc "DocumentFile"
new java/lang/StringBuilder
dup
ldc "Failed query: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L9:
aload 1
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
iconst_0
ireturn
L3:
astore 1
aconst_null
astore 0
L13:
aload 0
invokestatic android/support/v4/k/b/a(Ljava/lang/AutoCloseable;)V
aload 1
athrow
L7:
astore 1
goto L13
L6:
astore 4
goto L12
.limit locals 5
.limit stack 6
.end method

.method private static h(Landroid/content/Context;Landroid/net/Uri;)Z
aload 0
aload 1
invokestatic android/provider/DocumentsContract/isDocumentUri(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static i(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
aload 0
aload 1
ldc "_display_name"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static j(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
aload 0
aload 1
ldc "mime_type"
invokestatic android/support/v4/k/b/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static k(Landroid/content/Context;Landroid/net/Uri;)J
aload 0
aload 1
ldc "last_modified"
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
lreturn
.limit locals 2
.limit stack 3
.end method

.method private static l(Landroid/content/Context;Landroid/net/Uri;)J
aload 0
aload 1
ldc "_size"
invokestatic android/support/v4/k/b/b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J
lreturn
.limit locals 2
.limit stack 3
.end method
