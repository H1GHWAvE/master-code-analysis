.bytecode 50.0
.class public synchronized abstract android/support/v4/k/a
.super java/lang/Object

.field static final 'a' Ljava/lang/String; = "DocumentFile"

.field private final 'b' Landroid/support/v4/k/a;

.method <init>(Landroid/support/v4/k/a;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/k/a/b Landroid/support/v4/k/a;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/support/v4/k/a;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L0
new android/support/v4/k/e
dup
aload 0
aload 1
invokespecial android/support/v4/k/e/<init>(Landroid/content/Context;Landroid/net/Uri;)V
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/io/File;)Landroid/support/v4/k/a;
new android/support/v4/k/d
dup
aconst_null
aload 0
invokespecial android/support/v4/k/d/<init>(Landroid/support/v4/k/a;Ljava/io/File;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static b(Landroid/content/Context;Landroid/net/Uri;)Landroid/support/v4/k/a;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/support/v4/k/f
dup
aconst_null
aload 0
aload 1
aload 1
invokestatic android/provider/DocumentsContract/getTreeDocumentId(Landroid/net/Uri;)Ljava/lang/String;
invokestatic android/provider/DocumentsContract/buildDocumentUriUsingTree(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
invokespecial android/support/v4/k/f/<init>(Landroid/support/v4/k/a;Landroid/content/Context;Landroid/net/Uri;)V
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 6
.end method

.method private c(Ljava/lang/String;)Landroid/support/v4/k/a;
aload 0
invokevirtual android/support/v4/k/a/l()[Landroid/support/v4/k/a;
astore 4
aload 4
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 4
iload 2
aaload
astore 5
aload 1
aload 5
invokevirtual android/support/v4/k/a/b()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 5
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aconst_null
areturn
.limit locals 6
.limit stack 2
.end method

.method private static c(Landroid/content/Context;Landroid/net/Uri;)Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L0
aload 0
aload 1
invokestatic android/provider/DocumentsContract/isDocumentUri(Landroid/content/Context;Landroid/net/Uri;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private m()Landroid/support/v4/k/a;
aload 0
getfield android/support/v4/k/a/b Landroid/support/v4/k/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method public abstract a()Landroid/net/Uri;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/support/v4/k/a;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/k/a;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract b(Ljava/lang/String;)Z
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()J
.end method

.method public abstract g()J
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public abstract j()Z
.end method

.method public abstract k()Z
.end method

.method public abstract l()[Landroid/support/v4/k/a;
.end method
