.bytecode 50.0
.class synchronized android/support/v4/m/w
.super java/lang/Object

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method synthetic <init>(B)V
aload 0
invokespecial android/support/v4/m/w/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method private static b(Ljava/util/Locale;)I
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
aload 0
invokevirtual java/util/Locale/getDisplayName(Ljava/util/Locale;)Ljava/lang/String;
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/getDirectionality(C)B
tableswitch 1
L0
L0
default : L1
L1:
iconst_0
ireturn
L0:
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public a(Ljava/util/Locale;)I
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
iconst_1
istore 3
aload 1
ifnull L0
aload 1
getstatic android/support/v4/m/u/a Ljava/util/Locale;
invokevirtual java/util/Locale/equals(Ljava/lang/Object;)Z
ifne L0
aload 1
invokestatic android/support/v4/m/e/a(Ljava/util/Locale;)Ljava/lang/String;
astore 4
aload 4
ifnonnull L1
iload 3
istore 2
aload 1
aload 1
invokevirtual java/util/Locale/getDisplayName(Ljava/util/Locale;)Ljava/lang/String;
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/getDirectionality(C)B
tableswitch 1
L2
L2
default : L3
L3:
iconst_0
istore 2
L2:
iload 2
ireturn
L1:
iload 3
istore 2
aload 4
invokestatic android/support/v4/m/u/a()Ljava/lang/String;
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifne L2
iload 3
istore 2
aload 4
invokestatic android/support/v4/m/u/b()Ljava/lang/String;
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifne L2
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
.annotation invisible Landroid/support/a/y;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
iconst_0
istore 3
L0:
iload 3
aload 1
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
istore 2
iload 2
lookupswitch
34 : L2
38 : L3
39 : L4
60 : L5
62 : L6
default : L7
L7:
aload 4
iload 2
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L8:
iload 3
iconst_1
iadd
istore 3
goto L0
L5:
aload 4
ldc "&lt;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L6:
aload 4
ldc "&gt;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L3:
aload 4
ldc "&amp;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L4:
aload 4
ldc "&#39;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L2:
aload 4
ldc "&quot;"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L8
L1:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 2
.end method
