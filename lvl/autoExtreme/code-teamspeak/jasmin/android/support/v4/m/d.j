.bytecode 50.0
.class final synchronized android/support/v4/m/d
.super java/lang/Object

.field private static final 'f' I = 1792


.field private static final 'g' [B

.field final 'a' Ljava/lang/String;

.field final 'b' Z

.field final 'c' I

.field 'd' I

.field 'e' C

.method static <clinit>()V
sipush 1792
newarray byte
putstatic android/support/v4/m/d/g [B
iconst_0
istore 0
L0:
iload 0
sipush 1792
if_icmpge L1
getstatic android/support/v4/m/d/g [B
iload 0
iload 0
invokestatic java/lang/Character/getDirectionality(I)B
bastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
return
.limit locals 1
.limit stack 3
.end method

.method <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
iconst_0
putfield android/support/v4/m/d/b Z
aload 0
aload 1
invokevirtual java/lang/String/length()I
putfield android/support/v4/m/d/c I
return
.limit locals 2
.limit stack 2
.end method

.method static a(C)B
iload 0
sipush 1792
if_icmpge L0
getstatic android/support/v4/m/d/g [B
iload 0
baload
ireturn
L0:
iload 0
invokestatic java/lang/Character/getDirectionality(C)B
ireturn
.limit locals 1
.limit stack 2
.end method

.method private b()I
aload 0
iconst_0
putfield android/support/v4/m/d/d I
iconst_0
istore 4
iconst_0
istore 5
iconst_0
istore 2
L0:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L1
iload 4
ifne L1
aload 0
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
getfield android/support/v4/m/d/d I
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
invokestatic java/lang/Character/isHighSurrogate(C)Z
ifeq L2
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
getfield android/support/v4/m/d/d I
invokestatic java/lang/Character/codePointAt(Ljava/lang/CharSequence;I)I
istore 3
aload 0
aload 0
getfield android/support/v4/m/d/d I
iload 3
invokestatic java/lang/Character/charCount(I)I
iadd
putfield android/support/v4/m/d/d I
iload 3
invokestatic java/lang/Character/getDirectionality(I)B
istore 3
L3:
iload 3
tableswitch 0
L4
L5
L5
L6
L6
L6
L6
L6
L6
L0
L6
L6
L6
L6
L7
L7
L8
L8
L9
default : L6
L6:
iload 2
istore 4
goto L0
L2:
aload 0
aload 0
getfield android/support/v4/m/d/d I
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/e C
invokestatic android/support/v4/m/d/a(C)B
istore 6
iload 6
istore 3
aload 0
getfield android/support/v4/m/d/b Z
ifeq L3
aload 0
getfield android/support/v4/m/d/e C
bipush 60
if_icmpne L10
aload 0
getfield android/support/v4/m/d/d I
istore 3
L11:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L12
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/m/d/d I
istore 6
aload 0
iload 6
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 0
aload 8
iload 6
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
bipush 62
if_icmpne L13
bipush 12
istore 3
goto L3
L13:
aload 0
getfield android/support/v4/m/d/e C
bipush 34
if_icmpeq L14
aload 0
getfield android/support/v4/m/d/e C
bipush 39
if_icmpne L11
L14:
aload 0
getfield android/support/v4/m/d/e C
istore 6
L15:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L11
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/m/d/d I
istore 7
aload 0
iload 7
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 8
iload 7
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/m/d/e C
iload 1
iload 6
if_icmpne L15
goto L11
L12:
aload 0
iload 3
putfield android/support/v4/m/d/d I
aload 0
bipush 60
putfield android/support/v4/m/d/e C
bipush 13
istore 3
goto L3
L10:
iload 6
istore 3
aload 0
getfield android/support/v4/m/d/e C
bipush 38
if_icmpne L3
L16:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L17
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/m/d/d I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 8
iload 3
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/m/d/e C
iload 1
bipush 59
if_icmpne L16
L17:
bipush 12
istore 3
goto L3
L7:
iload 2
iconst_1
iadd
istore 2
iconst_m1
istore 5
goto L0
L8:
iload 2
iconst_1
iadd
istore 2
iconst_1
istore 5
goto L0
L9:
iload 2
iconst_1
isub
istore 2
iconst_0
istore 5
goto L0
L4:
iload 2
ifne L18
L19:
iconst_m1
ireturn
L18:
iload 2
istore 4
goto L0
L5:
iload 2
ifne L20
iconst_1
ireturn
L20:
iload 2
istore 4
goto L0
L1:
iload 4
ifne L21
iconst_0
ireturn
L21:
iload 5
ifeq L22
iload 5
ireturn
L22:
aload 0
getfield android/support/v4/m/d/d I
ifle L23
aload 0
invokevirtual android/support/v4/m/d/a()B
tableswitch 14
L24
L24
L25
L25
L26
default : L27
L27:
goto L22
L24:
iload 4
iload 2
if_icmpeq L19
iload 2
iconst_1
isub
istore 2
goto L22
L25:
iload 4
iload 2
if_icmpne L28
iconst_1
ireturn
L28:
iload 2
iconst_1
isub
istore 2
goto L22
L26:
iload 2
iconst_1
iadd
istore 2
goto L22
L23:
iconst_0
ireturn
.limit locals 9
.limit stack 3
.end method

.method private c()I
iconst_0
istore 4
aload 0
aload 0
getfield android/support/v4/m/d/c I
putfield android/support/v4/m/d/d I
iconst_0
istore 2
iconst_0
istore 1
L0:
iload 4
istore 3
aload 0
getfield android/support/v4/m/d/d I
ifle L1
aload 0
invokevirtual android/support/v4/m/d/a()B
tableswitch 0
L2
L3
L3
L4
L4
L4
L4
L4
L4
L0
L4
L4
L4
L4
L5
L5
L6
L6
L7
default : L4
L4:
iload 2
ifne L0
iload 1
istore 2
goto L0
L2:
iload 1
ifne L8
iconst_m1
istore 3
L1:
iload 3
ireturn
L8:
iload 2
ifne L0
iload 1
istore 2
goto L0
L5:
iload 2
iload 1
if_icmpne L9
iconst_m1
ireturn
L9:
iload 1
iconst_1
isub
istore 1
goto L0
L3:
iload 1
ifne L10
iconst_1
ireturn
L10:
iload 2
ifne L0
iload 1
istore 2
goto L0
L6:
iload 2
iload 1
if_icmpne L11
iconst_1
ireturn
L11:
iload 1
iconst_1
isub
istore 1
goto L0
L7:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 5
.limit stack 2
.end method

.method private d()B
bipush 12
istore 2
aload 0
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
getfield android/support/v4/m/d/d I
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
invokestatic java/lang/Character/isHighSurrogate(C)Z
ifeq L0
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
getfield android/support/v4/m/d/d I
invokestatic java/lang/Character/codePointAt(Ljava/lang/CharSequence;I)I
istore 4
aload 0
aload 0
getfield android/support/v4/m/d/d I
iload 4
invokestatic java/lang/Character/charCount(I)I
iadd
putfield android/support/v4/m/d/d I
iload 4
invokestatic java/lang/Character/getDirectionality(I)B
istore 1
L1:
iload 1
ireturn
L0:
aload 0
aload 0
getfield android/support/v4/m/d/d I
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/e C
invokestatic android/support/v4/m/d/a(C)B
istore 1
aload 0
getfield android/support/v4/m/d/b Z
ifeq L2
aload 0
getfield android/support/v4/m/d/e C
bipush 60
if_icmpne L3
aload 0
getfield android/support/v4/m/d/d I
istore 4
L4:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L5
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 7
aload 0
getfield android/support/v4/m/d/d I
istore 5
aload 0
iload 5
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 0
aload 7
iload 5
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
iload 2
istore 1
aload 0
getfield android/support/v4/m/d/e C
bipush 62
if_icmpeq L1
aload 0
getfield android/support/v4/m/d/e C
bipush 34
if_icmpeq L6
aload 0
getfield android/support/v4/m/d/e C
bipush 39
if_icmpne L4
L6:
aload 0
getfield android/support/v4/m/d/e C
istore 5
L7:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L4
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 7
aload 0
getfield android/support/v4/m/d/d I
istore 6
aload 0
iload 6
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 7
iload 6
invokevirtual java/lang/String/charAt(I)C
istore 3
aload 0
iload 3
putfield android/support/v4/m/d/e C
iload 3
iload 5
if_icmpne L7
goto L4
L5:
aload 0
iload 4
putfield android/support/v4/m/d/d I
aload 0
bipush 60
putfield android/support/v4/m/d/e C
bipush 13
ireturn
L3:
aload 0
getfield android/support/v4/m/d/e C
bipush 38
if_icmpne L2
L8:
iload 2
istore 1
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L1
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 7
aload 0
getfield android/support/v4/m/d/d I
istore 4
aload 0
iload 4
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 7
iload 4
invokevirtual java/lang/String/charAt(I)C
istore 3
aload 0
iload 3
putfield android/support/v4/m/d/e C
iload 3
bipush 59
if_icmpne L8
bipush 12
ireturn
L2:
iload 1
ireturn
.limit locals 8
.limit stack 3
.end method

.method private e()B
aload 0
getfield android/support/v4/m/d/d I
istore 2
L0:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L1
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/m/d/d I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 0
aload 5
iload 3
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
bipush 62
if_icmpne L2
bipush 12
ireturn
L2:
aload 0
getfield android/support/v4/m/d/e C
bipush 34
if_icmpeq L3
aload 0
getfield android/support/v4/m/d/e C
bipush 39
if_icmpne L0
L3:
aload 0
getfield android/support/v4/m/d/e C
istore 3
L4:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L0
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/m/d/d I
istore 4
aload 0
iload 4
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 5
iload 4
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/m/d/e C
iload 1
iload 3
if_icmpne L4
goto L0
L1:
aload 0
iload 2
putfield android/support/v4/m/d/d I
aload 0
bipush 60
putfield android/support/v4/m/d/e C
bipush 13
ireturn
.limit locals 6
.limit stack 3
.end method

.method private f()B
aload 0
getfield android/support/v4/m/d/d I
istore 2
L0:
aload 0
getfield android/support/v4/m/d/d I
ifle L1
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/m/d/d I
iconst_1
isub
istore 3
aload 0
iload 3
putfield android/support/v4/m/d/d I
aload 0
aload 5
iload 3
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
bipush 60
if_icmpne L2
bipush 12
ireturn
L2:
aload 0
getfield android/support/v4/m/d/e C
bipush 62
if_icmpeq L1
aload 0
getfield android/support/v4/m/d/e C
bipush 34
if_icmpeq L3
aload 0
getfield android/support/v4/m/d/e C
bipush 39
if_icmpne L0
L3:
aload 0
getfield android/support/v4/m/d/e C
istore 3
L4:
aload 0
getfield android/support/v4/m/d/d I
ifle L0
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/m/d/d I
iconst_1
isub
istore 4
aload 0
iload 4
putfield android/support/v4/m/d/d I
aload 5
iload 4
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/m/d/e C
iload 1
iload 3
if_icmpne L4
goto L0
L1:
aload 0
iload 2
putfield android/support/v4/m/d/d I
aload 0
bipush 62
putfield android/support/v4/m/d/e C
bipush 13
ireturn
.limit locals 6
.limit stack 3
.end method

.method private g()B
L0:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L1
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 3
aload 0
getfield android/support/v4/m/d/d I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 3
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/m/d/e C
iload 1
bipush 59
if_icmpne L0
L1:
bipush 12
ireturn
.limit locals 4
.limit stack 3
.end method

.method private h()B
aload 0
getfield android/support/v4/m/d/d I
istore 1
L0:
aload 0
getfield android/support/v4/m/d/d I
ifle L1
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 3
aload 0
getfield android/support/v4/m/d/d I
iconst_1
isub
istore 2
aload 0
iload 2
putfield android/support/v4/m/d/d I
aload 0
aload 3
iload 2
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
bipush 38
if_icmpne L2
bipush 12
ireturn
L2:
aload 0
getfield android/support/v4/m/d/e C
bipush 59
if_icmpne L0
L1:
aload 0
iload 1
putfield android/support/v4/m/d/d I
aload 0
bipush 59
putfield android/support/v4/m/d/e C
bipush 13
ireturn
.limit locals 4
.limit stack 3
.end method

.method final a()B
bipush 12
istore 2
aload 0
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
getfield android/support/v4/m/d/d I
iconst_1
isub
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
invokestatic java/lang/Character/isLowSurrogate(C)Z
ifeq L0
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
getfield android/support/v4/m/d/d I
invokestatic java/lang/Character/codePointBefore(Ljava/lang/CharSequence;I)I
istore 4
aload 0
aload 0
getfield android/support/v4/m/d/d I
iload 4
invokestatic java/lang/Character/charCount(I)I
isub
putfield android/support/v4/m/d/d I
iload 4
invokestatic java/lang/Character/getDirectionality(I)B
istore 1
L1:
iload 1
ireturn
L0:
aload 0
aload 0
getfield android/support/v4/m/d/d I
iconst_1
isub
putfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/e C
invokestatic android/support/v4/m/d/a(C)B
istore 1
aload 0
getfield android/support/v4/m/d/b Z
ifeq L2
aload 0
getfield android/support/v4/m/d/e C
bipush 62
if_icmpne L3
aload 0
getfield android/support/v4/m/d/d I
istore 4
L4:
aload 0
getfield android/support/v4/m/d/d I
ifle L5
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 7
aload 0
getfield android/support/v4/m/d/d I
iconst_1
isub
istore 5
aload 0
iload 5
putfield android/support/v4/m/d/d I
aload 0
aload 7
iload 5
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
iload 2
istore 1
aload 0
getfield android/support/v4/m/d/e C
bipush 60
if_icmpeq L1
aload 0
getfield android/support/v4/m/d/e C
bipush 62
if_icmpeq L5
aload 0
getfield android/support/v4/m/d/e C
bipush 34
if_icmpeq L6
aload 0
getfield android/support/v4/m/d/e C
bipush 39
if_icmpne L4
L6:
aload 0
getfield android/support/v4/m/d/e C
istore 5
L7:
aload 0
getfield android/support/v4/m/d/d I
ifle L4
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 7
aload 0
getfield android/support/v4/m/d/d I
iconst_1
isub
istore 6
aload 0
iload 6
putfield android/support/v4/m/d/d I
aload 7
iload 6
invokevirtual java/lang/String/charAt(I)C
istore 3
aload 0
iload 3
putfield android/support/v4/m/d/e C
iload 3
iload 5
if_icmpne L7
goto L4
L5:
aload 0
iload 4
putfield android/support/v4/m/d/d I
aload 0
bipush 62
putfield android/support/v4/m/d/e C
bipush 13
ireturn
L3:
aload 0
getfield android/support/v4/m/d/e C
bipush 59
if_icmpne L2
aload 0
getfield android/support/v4/m/d/d I
istore 4
L8:
aload 0
getfield android/support/v4/m/d/d I
ifle L9
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 7
aload 0
getfield android/support/v4/m/d/d I
iconst_1
isub
istore 5
aload 0
iload 5
putfield android/support/v4/m/d/d I
aload 0
aload 7
iload 5
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
iload 2
istore 1
aload 0
getfield android/support/v4/m/d/e C
bipush 38
if_icmpeq L1
aload 0
getfield android/support/v4/m/d/e C
bipush 59
if_icmpne L8
L9:
aload 0
iload 4
putfield android/support/v4/m/d/d I
aload 0
bipush 59
putfield android/support/v4/m/d/e C
bipush 13
ireturn
L2:
iload 1
ireturn
.limit locals 8
.limit stack 4
.end method
