.bytecode 50.0
.class final synchronized android/support/v4/view/l
.super android/view/View$AccessibilityDelegate

.field final synthetic 'a' Landroid/support/v4/view/m;

.method <init>(Landroid/support/v4/view/m;)V
aload 0
aload 1
putfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 0
invokespecial android/view/View$AccessibilityDelegate/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
aload 2
invokeinterface android/support/v4/view/m/a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final getAccessibilityNodeProvider(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeProvider;
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
invokeinterface android/support/v4/view/m/a(Landroid/view/View;)Ljava/lang/Object; 1
checkcast android/view/accessibility/AccessibilityNodeProvider
areturn
.limit locals 2
.limit stack 2
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
aload 2
invokeinterface android/support/v4/view/m/b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
aload 2
invokeinterface android/support/v4/view/m/a(Landroid/view/View;Ljava/lang/Object;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
aload 2
invokeinterface android/support/v4/view/m/c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public final onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
aload 2
aload 3
invokeinterface android/support/v4/view/m/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
iload 2
aload 3
invokeinterface android/support/v4/view/m/a(Landroid/view/View;ILandroid/os/Bundle;)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final sendAccessibilityEvent(Landroid/view/View;I)V
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
iload 2
invokeinterface android/support/v4/view/m/a(Landroid/view/View;I)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public final sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
getfield android/support/v4/view/l/a Landroid/support/v4/view/m;
aload 1
aload 2
invokeinterface android/support/v4/view/m/d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V 2
return
.limit locals 3
.limit stack 3
.end method
