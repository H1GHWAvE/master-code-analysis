.bytecode 50.0
.class public final synchronized android/support/v4/view/at
.super java/lang/Object

.field static final 'a' Landroid/support/v4/view/au;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmplt L0
new android/support/v4/view/aw
dup
invokespecial android/support/v4/view/aw/<init>()V
putstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
return
L0:
new android/support/v4/view/av
dup
invokespecial android/support/v4/view/av/<init>()V
putstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/view/ViewGroup$MarginLayoutParams;)I
getstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
aload 0
invokeinterface android/support/v4/view/au/a(Landroid/view/ViewGroup$MarginLayoutParams;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Landroid/view/ViewGroup$MarginLayoutParams;I)V
getstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
aload 0
iload 1
invokeinterface android/support/v4/view/au/a(Landroid/view/ViewGroup$MarginLayoutParams;I)V 2
return
.limit locals 2
.limit stack 3
.end method

.method public static b(Landroid/view/ViewGroup$MarginLayoutParams;)I
getstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
aload 0
invokeinterface android/support/v4/view/au/b(Landroid/view/ViewGroup$MarginLayoutParams;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Landroid/view/ViewGroup$MarginLayoutParams;I)V
getstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
aload 0
iload 1
invokeinterface android/support/v4/view/au/b(Landroid/view/ViewGroup$MarginLayoutParams;I)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private static c(Landroid/view/ViewGroup$MarginLayoutParams;I)V
getstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
aload 0
iload 1
invokeinterface android/support/v4/view/au/c(Landroid/view/ViewGroup$MarginLayoutParams;I)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private static c(Landroid/view/ViewGroup$MarginLayoutParams;)Z
getstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
aload 0
invokeinterface android/support/v4/view/au/c(Landroid/view/ViewGroup$MarginLayoutParams;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/view/ViewGroup$MarginLayoutParams;)I
getstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
aload 0
invokeinterface android/support/v4/view/au/d(Landroid/view/ViewGroup$MarginLayoutParams;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/view/ViewGroup$MarginLayoutParams;I)V
getstatic android/support/v4/view/at/a Landroid/support/v4/view/au;
aload 0
iload 1
invokeinterface android/support/v4/view/au/d(Landroid/view/ViewGroup$MarginLayoutParams;I)V 2
return
.limit locals 2
.limit stack 3
.end method
