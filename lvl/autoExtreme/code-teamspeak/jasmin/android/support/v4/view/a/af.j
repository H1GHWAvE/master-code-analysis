.bytecode 50.0
.class final synchronized android/support/v4/view/a/af
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(IIIIZZ)Ljava/lang/Object;
iload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokestatic android/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo/obtain(IIIIZZ)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;
areturn
.limit locals 6
.limit stack 6
.end method

.method private static a(IIZI)Ljava/lang/Object;
iload 0
iload 1
iload 2
iload 3
invokestatic android/view/accessibility/AccessibilityNodeInfo$CollectionInfo/obtain(IIZI)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionInfo;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a(ILjava/lang/CharSequence;)Ljava/lang/Object;
new android/view/accessibility/AccessibilityNodeInfo$AccessibilityAction
dup
iload 0
aload 1
invokespecial android/view/accessibility/AccessibilityNodeInfo$AccessibilityAction/<init>(ILjava/lang/CharSequence;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/Object;)Ljava/util/List;
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
invokevirtual android/view/accessibility/AccessibilityNodeInfo/getActionList()Ljava/util/List;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;I)V
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
iload 1
invokevirtual android/view/accessibility/AccessibilityNodeInfo/setMaxTextLength(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/CharSequence;)V
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
aload 1
invokevirtual android/view/accessibility/AccessibilityNodeInfo/setError(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
aload 1
checkcast android/view/accessibility/AccessibilityNodeInfo$AccessibilityAction
invokevirtual android/view/accessibility/AccessibilityNodeInfo/addAction(Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/view/View;)Z
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
aload 1
invokevirtual android/view/accessibility/AccessibilityNodeInfo/removeChild(Landroid/view/View;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/view/View;I)Z
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
aload 1
iload 2
invokevirtual android/view/accessibility/AccessibilityNodeInfo/removeChild(Landroid/view/View;I)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method private static b(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
invokevirtual android/view/accessibility/AccessibilityNodeInfo/getError()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
aload 1
checkcast android/view/accessibility/AccessibilityNodeInfo$AccessibilityAction
invokevirtual android/view/accessibility/AccessibilityNodeInfo/removeAction(Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Object;)I
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
invokevirtual android/view/accessibility/AccessibilityNodeInfo/getMaxTextLength()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo
invokevirtual android/view/accessibility/AccessibilityNodeInfo/getWindow()Landroid/view/accessibility/AccessibilityWindowInfo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)I
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo$AccessibilityAction
invokevirtual android/view/accessibility/AccessibilityNodeInfo$AccessibilityAction/getId()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Ljava/lang/Object;)Ljava/lang/CharSequence;
aload 0
checkcast android/view/accessibility/AccessibilityNodeInfo$AccessibilityAction
invokevirtual android/view/accessibility/AccessibilityNodeInfo$AccessibilityAction/getLabel()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method
