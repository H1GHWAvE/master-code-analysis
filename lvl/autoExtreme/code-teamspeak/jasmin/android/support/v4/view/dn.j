.bytecode 50.0
.class final synchronized android/support/v4/view/dn
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
aload 1
invokevirtual android/view/View/onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;Ljava/lang/Object;)V
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
aload 0
aload 1
checkcast android/view/View$AccessibilityDelegate
invokevirtual android/view/View/setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;Z)V
aload 0
iload 1
invokevirtual android/view/View/setFitsSystemWindows(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;I)Z
aload 0
iload 1
invokevirtual android/view/View/canScrollHorizontally(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
aload 1
invokevirtual android/view/View/onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;Ljava/lang/Object;)V
aload 0
aload 1
checkcast android/view/accessibility/AccessibilityNodeInfo
invokevirtual android/view/View/onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;I)Z
aload 0
iload 1
invokevirtual android/view/View/canScrollVertically(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method
