.bytecode 50.0
.class synchronized android/support/v4/view/cy
.super java/lang/Object
.implements android/support/v4/view/di

.field 'a' Ljava/util/WeakHashMap;

.field private 'b' Ljava/lang/reflect/Method;

.field private 'c' Ljava/lang/reflect/Method;

.field private 'd' Z

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield android/support/v4/view/cy/a Ljava/util/WeakHashMap;
return
.limit locals 1
.limit stack 2
.end method

.method private static a(Landroid/support/v4/view/cq;I)Z
aload 0
invokeinterface android/support/v4/view/cq/b()I 0
istore 2
aload 0
invokeinterface android/support/v4/view/cq/a()I 0
aload 0
invokeinterface android/support/v4/view/cq/c()I 0
isub
istore 3
iload 3
ifne L0
L1:
iconst_0
ireturn
L0:
iload 1
ifge L2
iload 2
ifle L1
iconst_1
ireturn
L2:
iload 2
iload 3
iconst_1
isub
if_icmpge L1
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method private b()V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
L0:
aload 0
ldc android/view/View
ldc "dispatchStartTemporaryDetach"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v4/view/cy/b Ljava/lang/reflect/Method;
aload 0
ldc android/view/View
ldc "dispatchFinishTemporaryDetach"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v4/view/cy/c Ljava/lang/reflect/Method;
L1:
aload 0
iconst_1
putfield android/support/v4/view/cy/d Z
return
L2:
astore 1
ldc "ViewCompat"
ldc "Couldn't find method"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
.limit locals 2
.limit stack 4
.end method

.method private static b(Landroid/support/v4/view/cq;I)Z
aload 0
invokeinterface android/support/v4/view/cq/e()I 0
istore 2
aload 0
invokeinterface android/support/v4/view/cq/d()I 0
aload 0
invokeinterface android/support/v4/view/cq/f()I 0
isub
istore 3
iload 3
ifne L0
L1:
iconst_0
ireturn
L0:
iload 1
ifge L2
iload 2
ifle L1
iconst_1
ireturn
L2:
iload 2
iload 3
iconst_1
isub
if_icmpge L1
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method public A(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public B(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public C(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public D(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public E(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public F(Landroid/view/View;)I
aload 1
invokestatic android/support/v4/view/dj/a(Landroid/view/View;)I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public G(Landroid/view/View;)I
aload 1
invokestatic android/support/v4/view/dj/b(Landroid/view/View;)I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public H(Landroid/view/View;)Landroid/support/v4/view/fk;
new android/support/v4/view/fk
dup
aload 1
invokespecial android/support/v4/view/fk/<init>(Landroid/view/View;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public I(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public J(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public K(Landroid/view/View;)Ljava/lang/String;
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public L(Landroid/view/View;)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public M(Landroid/view/View;)V
return
.limit locals 2
.limit stack 0
.end method

.method public N(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public O(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public P(Landroid/view/View;)Landroid/graphics/Rect;
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public Q(Landroid/view/View;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public R(Landroid/view/View;)V
return
.limit locals 2
.limit stack 0
.end method

.method public S(Landroid/view/View;)V
return
.limit locals 2
.limit stack 0
.end method

.method public T(Landroid/view/View;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public U(Landroid/view/View;)Z
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
invokeinterface android/support/v4/view/bt/isNestedScrollingEnabled()Z 0
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public V(Landroid/view/View;)Landroid/content/res/ColorStateList;
aload 1
instanceof android/support/v4/view/cr
ifeq L0
aload 1
checkcast android/support/v4/view/cr
invokeinterface android/support/v4/view/cr/getSupportBackgroundTintList()Landroid/content/res/ColorStateList; 0
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public W(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
aload 1
instanceof android/support/v4/view/cr
ifeq L0
aload 1
checkcast android/support/v4/view/cr
invokeinterface android/support/v4/view/cr/getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode; 0
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public X(Landroid/view/View;)V
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
invokeinterface android/support/v4/view/bt/stopNestedScroll()V 0
L0:
return
.limit locals 2
.limit stack 1
.end method

.method public Y(Landroid/view/View;)Z
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
invokeinterface android/support/v4/view/bt/hasNestedScrollingParent()Z 0
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public Z(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/getWidth()I
ifle L0
aload 1
invokevirtual android/view/View/getHeight()I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public a(II)I
iload 1
iload 2
ior
ireturn
.limit locals 3
.limit stack 2
.end method

.method public a(III)I
iload 1
iload 2
invokestatic android/view/View/resolveSize(II)I
ireturn
.limit locals 4
.limit stack 2
.end method

.method public a(Landroid/view/View;)I
iconst_2
ireturn
.limit locals 2
.limit stack 1
.end method

.method a()J
ldc2_w 10L
lreturn
.limit locals 1
.limit stack 2
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
aload 2
areturn
.limit locals 3
.limit stack 1
.end method

.method public a(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;IIII)V
aload 1
iload 2
iload 3
iload 4
iload 5
invokevirtual android/view/View/invalidate(IIII)V
return
.limit locals 6
.limit stack 5
.end method

.method public a(Landroid/view/View;ILandroid/graphics/Paint;)V
return
.limit locals 4
.limit stack 0
.end method

.method public a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
aload 1
instanceof android/support/v4/view/cr
ifeq L0
aload 1
checkcast android/support/v4/view/cr
aload 2
invokeinterface android/support/v4/view/cr/setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V 1
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
aload 1
instanceof android/support/v4/view/cr
ifeq L0
aload 1
checkcast android/support/v4/view/cr
aload 2
invokeinterface android/support/v4/view/cr/setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V 1
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public a(Landroid/view/View;Landroid/graphics/Rect;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/a;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;Landroid/support/v4/view/bx;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;Ljava/lang/Runnable;)V
aload 1
aload 2
aload 0
invokevirtual android/support/v4/view/cy/a()J
invokevirtual android/view/View/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 3
.limit stack 4
.end method

.method public a(Landroid/view/View;Ljava/lang/Runnable;J)V
aload 1
aload 2
aload 0
invokevirtual android/support/v4/view/cy/a()J
lload 3
ladd
invokevirtual android/view/View/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 5
.limit stack 6
.end method

.method public a(Landroid/view/View;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/View;Z)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/view/ViewGroup;)V
return
.limit locals 2
.limit stack 0
.end method

.method public a(Landroid/view/View;FF)Z
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
fload 2
fload 3
invokeinterface android/support/v4/view/bt/dispatchNestedPreFling(FF)Z 2
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method public a(Landroid/view/View;FFZ)Z
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
fload 2
fload 3
iload 4
invokeinterface android/support/v4/view/bt/dispatchNestedFling(FFZ)Z 3
ireturn
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 4
.end method

.method public a(Landroid/view/View;I)Z
aload 1
instanceof android/support/v4/view/cq
ifeq L0
aload 1
checkcast android/support/v4/view/cq
astore 1
aload 1
invokeinterface android/support/v4/view/cq/b()I 0
istore 3
aload 1
invokeinterface android/support/v4/view/cq/a()I 0
aload 1
invokeinterface android/support/v4/view/cq/c()I 0
isub
istore 4
iload 4
ifeq L1
iload 2
ifge L2
iload 3
ifle L3
iconst_1
istore 2
L4:
iload 2
ifeq L0
iconst_1
ireturn
L3:
iconst_0
istore 2
goto L4
L2:
iload 3
iload 4
iconst_1
isub
if_icmpge L1
iconst_1
istore 2
goto L4
L1:
iconst_0
istore 2
goto L4
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 3
.end method

.method public a(Landroid/view/View;IIII[I)Z
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
iload 2
iload 3
iload 4
iload 5
aload 6
invokeinterface android/support/v4/view/bt/dispatchNestedScroll(IIII[I)Z 5
ireturn
L0:
iconst_0
ireturn
.limit locals 7
.limit stack 6
.end method

.method public a(Landroid/view/View;II[I[I)Z
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
iload 2
iload 3
aload 4
aload 5
invokeinterface android/support/v4/view/bt/dispatchNestedPreScroll(II[I[I)Z 4
ireturn
L0:
iconst_0
ireturn
.limit locals 6
.limit stack 5
.end method

.method public a(Landroid/view/View;ILandroid/os/Bundle;)Z
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method

.method public aa(Landroid/view/View;)F
aload 0
aload 1
invokevirtual android/support/v4/view/cy/O(Landroid/view/View;)F
aload 0
aload 1
invokevirtual android/support/v4/view/cy/N(Landroid/view/View;)F
fadd
freturn
.limit locals 2
.limit stack 3
.end method

.method public ab(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/getWindowToken()Landroid/os/IBinder;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
aload 2
areturn
.limit locals 3
.limit stack 1
.end method

.method public b(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public b(Landroid/view/View;IIII)V
aload 1
iload 2
iload 3
iload 4
iload 5
invokevirtual android/view/View/setPadding(IIII)V
return
.limit locals 6
.limit stack 5
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 3
.limit stack 0
.end method

.method public b(Landroid/view/View;Z)V
return
.limit locals 3
.limit stack 0
.end method

.method public b(Landroid/view/View;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public b(Landroid/view/View;I)Z
aload 1
instanceof android/support/v4/view/cq
ifeq L0
aload 1
checkcast android/support/v4/view/cq
astore 1
aload 1
invokeinterface android/support/v4/view/cq/e()I 0
istore 3
aload 1
invokeinterface android/support/v4/view/cq/d()I 0
aload 1
invokeinterface android/support/v4/view/cq/f()I 0
isub
istore 4
iload 4
ifeq L1
iload 2
ifge L2
iload 3
ifle L3
iconst_1
istore 2
L4:
iload 2
ifeq L0
iconst_1
ireturn
L3:
iconst_0
istore 2
goto L4
L2:
iload 3
iload 4
iconst_1
isub
if_icmpge L1
iconst_1
istore 2
goto L4
L1:
iconst_0
istore 2
goto L4
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 3
.end method

.method public c(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public c(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 0
.end method

.method public c(Landroid/view/View;Z)V
return
.limit locals 3
.limit stack 0
.end method

.method public c(Landroid/view/View;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public d(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/invalidate()V
return
.limit locals 2
.limit stack 1
.end method

.method public d(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public d(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 0
.end method

.method public d(Landroid/view/View;Z)V
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
iload 2
invokeinterface android/support/v4/view/bt/setNestedScrollingEnabled(Z)V 1
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public e(Landroid/view/View;)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public e(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public e(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 0
.end method

.method public f(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public f(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 0
.end method

.method public f(Landroid/view/View;)Z
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method

.method public g(Landroid/view/View;)Landroid/support/v4/view/a/aq;
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public g(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public g(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 0
.end method

.method public h(Landroid/view/View;)F
fconst_1
freturn
.limit locals 2
.limit stack 1
.end method

.method public h(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public h(Landroid/view/View;I)Z
aload 1
instanceof android/support/v4/view/bt
ifeq L0
aload 1
checkcast android/support/v4/view/bt
iload 2
invokeinterface android/support/v4/view/bt/startNestedScroll(I)Z 1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public i(Landroid/view/View;)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public i(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public j(Landroid/view/View;)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public j(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public k(Landroid/view/View;)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public k(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public l(Landroid/view/View;)Landroid/view/ViewParent;
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
areturn
.limit locals 2
.limit stack 1
.end method

.method public l(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public m(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public m(Landroid/view/View;)Z
iconst_0
istore 3
aload 1
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 1
iload 3
istore 2
aload 1
ifnull L0
iload 3
istore 2
aload 1
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
iconst_m1
if_icmpne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public n(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public n(Landroid/view/View;F)V
return
.limit locals 3
.limit stack 0
.end method

.method public o(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public p(Landroid/view/View;)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public q(Landroid/view/View;)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public r(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getPaddingLeft()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public s(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getPaddingRight()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final t(Landroid/view/View;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield android/support/v4/view/cy/d Z
ifne L3
aload 0
invokespecial android/support/v4/view/cy/b()V
L3:
aload 0
getfield android/support/v4/view/cy/b Ljava/lang/reflect/Method;
ifnull L4
L0:
aload 0
getfield android/support/v4/view/cy/b Ljava/lang/reflect/Method;
aload 1
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 1
ldc "ViewCompat"
ldc "Error calling dispatchStartTemporaryDetach"
aload 1
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
L4:
aload 1
invokevirtual android/view/View/onStartTemporaryDetach()V
return
.limit locals 2
.limit stack 3
.end method

.method public final u(Landroid/view/View;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield android/support/v4/view/cy/d Z
ifne L3
aload 0
invokespecial android/support/v4/view/cy/b()V
L3:
aload 0
getfield android/support/v4/view/cy/c Ljava/lang/reflect/Method;
ifnull L4
L0:
aload 0
getfield android/support/v4/view/cy/c Ljava/lang/reflect/Method;
aload 1
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 1
ldc "ViewCompat"
ldc "Error calling dispatchFinishTemporaryDetach"
aload 1
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
L4:
aload 1
invokevirtual android/view/View/onFinishTemporaryDetach()V
return
.limit locals 2
.limit stack 3
.end method

.method public v(Landroid/view/View;)Z
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method

.method public w(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public x(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public y(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method

.method public z(Landroid/view/View;)F
fconst_0
freturn
.limit locals 2
.limit stack 1
.end method
