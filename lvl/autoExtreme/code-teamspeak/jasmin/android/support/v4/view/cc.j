.bytecode 50.0
.class public synchronized android/support/v4/view/cc
.super android/view/ViewGroup
.implements android/support/v4/view/eq

.field private static final 'f' Ljava/lang/String; = "PagerTitleStrip"

.field private static final 'o' [I

.field private static final 'p' [I

.field private static final 'q' F = 0.6F


.field private static final 'r' I = 16


.field private static final 't' Landroid/support/v4/view/cf;

.field 'a' Landroid/support/v4/view/ViewPager;

.field 'b' Landroid/widget/TextView;

.field 'c' Landroid/widget/TextView;

.field 'd' Landroid/widget/TextView;

.field 'e' I

.field private 'g' I

.field private 'h' F

.field private 'i' I

.field private 'j' I

.field private 'k' Z

.field private 'l' Z

.field private final 'm' Landroid/support/v4/view/ce;

.field private 'n' Ljava/lang/ref/WeakReference;

.field private 's' I

.method static <clinit>()V
iconst_4
newarray int
dup
iconst_0
ldc_w 16842804
iastore
dup
iconst_1
ldc_w 16842901
iastore
dup
iconst_2
ldc_w 16842904
iastore
dup
iconst_3
ldc_w 16842927
iastore
putstatic android/support/v4/view/cc/o [I
iconst_1
newarray int
dup
iconst_0
ldc_w 16843660
iastore
putstatic android/support/v4/view/cc/p [I
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
new android/support/v4/view/ch
dup
invokespecial android/support/v4/view/ch/<init>()V
putstatic android/support/v4/view/cc/t Landroid/support/v4/view/cf;
return
L0:
new android/support/v4/view/cg
dup
invokespecial android/support/v4/view/cg/<init>()V
putstatic android/support/v4/view/cc/t Landroid/support/v4/view/cf;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/view/cc/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;B)V
iconst_0
istore 5
aload 0
aload 1
aconst_null
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_m1
putfield android/support/v4/view/cc/g I
aload 0
ldc_w -1.0F
putfield android/support/v4/view/cc/h F
aload 0
new android/support/v4/view/ce
dup
aload 0
iconst_0
invokespecial android/support/v4/view/ce/<init>(Landroid/support/v4/view/cc;B)V
putfield android/support/v4/view/cc/m Landroid/support/v4/view/ce;
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 6
aload 0
aload 6
putfield android/support/v4/view/cc/b Landroid/widget/TextView;
aload 0
aload 6
invokevirtual android/support/v4/view/cc/addView(Landroid/view/View;)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 6
aload 0
aload 6
putfield android/support/v4/view/cc/c Landroid/widget/TextView;
aload 0
aload 6
invokevirtual android/support/v4/view/cc/addView(Landroid/view/View;)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 6
aload 0
aload 6
putfield android/support/v4/view/cc/d Landroid/widget/TextView;
aload 0
aload 6
invokevirtual android/support/v4/view/cc/addView(Landroid/view/View;)V
aload 1
aconst_null
getstatic android/support/v4/view/cc/o [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 6
aload 6
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 2
iload 2
ifeq L0
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
aload 1
iload 2
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
aload 1
iload 2
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
aload 1
iload 2
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L0:
aload 6
iconst_1
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 4
iload 4
ifeq L1
iload 4
i2f
fstore 3
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
iconst_0
fload 3
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
iconst_0
fload 3
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
iconst_0
fload 3
invokevirtual android/widget/TextView/setTextSize(IF)V
L1:
aload 6
iconst_2
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L2
aload 6
iconst_2
iconst_0
invokevirtual android/content/res/TypedArray/getColor(II)I
istore 4
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
iload 4
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
iload 4
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
iload 4
invokevirtual android/widget/TextView/setTextColor(I)V
L2:
aload 0
aload 6
iconst_3
bipush 80
invokevirtual android/content/res/TypedArray/getInteger(II)I
putfield android/support/v4/view/cc/j I
aload 6
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getTextColors()Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
putfield android/support/v4/view/cc/e I
aload 0
ldc_w 0.6F
invokevirtual android/support/v4/view/cc/setNonPrimaryAlpha(F)V
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
iload 2
ifeq L3
aload 1
iload 2
getstatic android/support/v4/view/cc/p [I
invokevirtual android/content/Context/obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
astore 6
aload 6
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
istore 5
aload 6
invokevirtual android/content/res/TypedArray/recycle()V
L3:
iload 5
ifeq L4
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
invokestatic android/support/v4/view/cc/setSingleLineAllCaps(Landroid/widget/TextView;)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
invokestatic android/support/v4/view/cc/setSingleLineAllCaps(Landroid/widget/TextView;)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
invokestatic android/support/v4/view/cc/setSingleLineAllCaps(Landroid/widget/TextView;)V
L5:
aload 0
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 16.0F
fmul
f2i
putfield android/support/v4/view/cc/i I
return
L4:
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/setSingleLine()V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/setSingleLine()V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
invokevirtual android/widget/TextView/setSingleLine()V
goto L5
.limit locals 7
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/view/cc;)F
aload 0
getfield android/support/v4/view/cc/h F
freturn
.limit locals 1
.limit stack 1
.end method

.method private static setSingleLineAllCaps(Landroid/widget/TextView;)V
getstatic android/support/v4/view/cc/t Landroid/support/v4/view/cf;
aload 0
invokeinterface android/support/v4/view/cf/a(Landroid/widget/TextView;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method private setTextSize$255e752(F)V
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
iconst_0
fload 1
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
iconst_0
fload 1
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
iconst_0
fload 1
invokevirtual android/widget/TextView/setTextSize(IF)V
return
.limit locals 2
.limit stack 3
.end method

.method final a(I)V
aload 0
iconst_1
putfield android/support/v4/view/cc/k Z
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
aconst_null
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
aconst_null
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
aconst_null
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
iconst_0
aload 0
invokevirtual android/support/v4/view/cc/getWidth()I
aload 0
invokevirtual android/support/v4/view/cc/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/view/cc/getPaddingRight()I
isub
i2f
ldc_w 0.8F
fmul
f2i
invokestatic java/lang/Math/max(II)I
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
iconst_0
aload 0
invokevirtual android/support/v4/view/cc/getHeight()I
aload 0
invokevirtual android/support/v4/view/cc/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/view/cc/getPaddingBottom()I
isub
invokestatic java/lang/Math/max(II)I
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 3
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
iload 2
iload 3
invokevirtual android/widget/TextView/measure(II)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
iload 2
iload 3
invokevirtual android/widget/TextView/measure(II)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
iload 2
iload 3
invokevirtual android/widget/TextView/measure(II)V
aload 0
iload 1
putfield android/support/v4/view/cc/g I
aload 0
getfield android/support/v4/view/cc/l Z
ifne L0
aload 0
iload 1
aload 0
getfield android/support/v4/view/cc/h F
iconst_0
invokevirtual android/support/v4/view/cc/a(IFZ)V
L0:
aload 0
iconst_0
putfield android/support/v4/view/cc/k Z
return
.limit locals 4
.limit stack 4
.end method

.method a(IFZ)V
iload 1
aload 0
getfield android/support/v4/view/cc/g I
if_icmpeq L0
aload 0
getfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
invokevirtual android/support/v4/view/ViewPager/getAdapter()Landroid/support/v4/view/by;
pop
aload 0
iload 1
invokevirtual android/support/v4/view/cc/a(I)V
L1:
aload 0
iconst_1
putfield android/support/v4/view/cc/l Z
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
istore 9
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
istore 14
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
istore 8
iload 14
iconst_2
idiv
istore 13
aload 0
invokevirtual android/support/v4/view/cc/getWidth()I
istore 10
aload 0
invokevirtual android/support/v4/view/cc/getHeight()I
istore 6
aload 0
invokevirtual android/support/v4/view/cc/getPaddingLeft()I
istore 12
aload 0
invokevirtual android/support/v4/view/cc/getPaddingRight()I
istore 11
aload 0
invokevirtual android/support/v4/view/cc/getPaddingTop()I
istore 1
aload 0
invokevirtual android/support/v4/view/cc/getPaddingBottom()I
istore 7
iload 11
iload 13
iadd
istore 15
ldc_w 0.5F
fload 2
fadd
fstore 5
fload 5
fstore 4
fload 5
fconst_1
fcmpl
ifle L2
fload 5
fconst_1
fsub
fstore 4
L2:
iload 10
iload 15
isub
fload 4
iload 10
iload 12
iload 13
iadd
isub
iload 15
isub
i2f
fmul
f2i
isub
iload 13
isub
istore 13
iload 13
iload 14
iadd
istore 14
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getBaseline()I
istore 17
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getBaseline()I
istore 16
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
invokevirtual android/widget/TextView/getBaseline()I
istore 15
iload 17
iload 16
invokestatic java/lang/Math/max(II)I
iload 15
invokestatic java/lang/Math/max(II)I
istore 18
iload 18
iload 17
isub
istore 17
iload 18
iload 16
isub
istore 16
iload 18
iload 15
isub
istore 15
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
istore 18
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
istore 19
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
istore 20
iload 18
iload 17
iadd
iload 19
iload 16
iadd
invokestatic java/lang/Math/max(II)I
iload 20
iload 15
iadd
invokestatic java/lang/Math/max(II)I
istore 18
aload 0
getfield android/support/v4/view/cc/j I
bipush 112
iand
lookupswitch
16 : L3
80 : L4
default : L5
L5:
iload 1
iload 17
iadd
istore 7
iload 1
iload 16
iadd
istore 6
iload 1
iload 15
iadd
istore 1
L6:
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
iload 13
iload 6
iload 14
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
iload 6
iadd
invokevirtual android/widget/TextView/layout(IIII)V
iload 12
iload 13
aload 0
getfield android/support/v4/view/cc/i I
isub
iload 9
isub
invokestatic java/lang/Math/min(II)I
istore 6
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
iload 6
iload 7
iload 9
iload 6
iadd
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
iload 7
iadd
invokevirtual android/widget/TextView/layout(IIII)V
iload 10
iload 11
isub
iload 8
isub
aload 0
getfield android/support/v4/view/cc/i I
iload 14
iadd
invokestatic java/lang/Math/max(II)I
istore 6
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
iload 6
iload 1
iload 6
iload 8
iadd
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
iload 1
iadd
invokevirtual android/widget/TextView/layout(IIII)V
aload 0
fload 2
putfield android/support/v4/view/cc/h F
aload 0
iconst_0
putfield android/support/v4/view/cc/l Z
return
L0:
iload 3
ifne L1
fload 2
aload 0
getfield android/support/v4/view/cc/h F
fcmpl
ifne L1
return
L3:
iload 6
iload 1
isub
iload 7
isub
iload 18
isub
iconst_2
idiv
istore 1
iload 1
iload 17
iadd
istore 7
iload 1
iload 16
iadd
istore 6
iload 1
iload 15
iadd
istore 1
goto L6
L4:
iload 6
iload 7
isub
iload 18
isub
istore 1
iload 1
iload 17
iadd
istore 7
iload 1
iload 16
iadd
istore 6
iload 1
iload 15
iadd
istore 1
goto L6
.limit locals 21
.limit stack 6
.end method

.method final a(Landroid/support/v4/view/by;Landroid/support/v4/view/by;)V
aload 1
ifnull L0
aload 1
aload 0
getfield android/support/v4/view/cc/m Landroid/support/v4/view/ce;
invokevirtual android/support/v4/view/by/b(Landroid/database/DataSetObserver;)V
aload 0
aconst_null
putfield android/support/v4/view/cc/n Ljava/lang/ref/WeakReference;
L0:
aload 2
ifnull L1
aload 2
aload 0
getfield android/support/v4/view/cc/m Landroid/support/v4/view/ce;
invokevirtual android/support/v4/view/by/a(Landroid/database/DataSetObserver;)V
aload 0
new java/lang/ref/WeakReference
dup
aload 2
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
putfield android/support/v4/view/cc/n Ljava/lang/ref/WeakReference;
L1:
aload 0
getfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
ifnull L2
aload 0
iconst_m1
putfield android/support/v4/view/cc/g I
aload 0
ldc_w -1.0F
putfield android/support/v4/view/cc/h F
aload 0
aload 0
getfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
invokevirtual android/support/v4/view/ViewPager/getCurrentItem()I
invokevirtual android/support/v4/view/cc/a(I)V
aload 0
invokevirtual android/support/v4/view/cc/requestLayout()V
L2:
return
.limit locals 3
.limit stack 4
.end method

.method getMinHeight()I
iconst_0
istore 1
aload 0
invokevirtual android/support/v4/view/cc/getBackground()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L0
aload 2
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 1
.end method

.method public getTextSpacing()I
aload 0
getfield android/support/v4/view/cc/i I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected onAttachedToWindow()V
aload 0
invokespecial android/view/ViewGroup/onAttachedToWindow()V
aload 0
invokevirtual android/support/v4/view/cc/getParent()Landroid/view/ViewParent;
astore 1
aload 1
instanceof android/support/v4/view/ViewPager
ifne L0
new java/lang/IllegalStateException
dup
ldc "PagerTitleStrip must be a direct child of a ViewPager."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
checkcast android/support/v4/view/ViewPager
astore 1
aload 1
invokevirtual android/support/v4/view/ViewPager/getAdapter()Landroid/support/v4/view/by;
astore 2
aload 1
aload 0
getfield android/support/v4/view/cc/m Landroid/support/v4/view/ce;
invokevirtual android/support/v4/view/ViewPager/a(Landroid/support/v4/view/ev;)Landroid/support/v4/view/ev;
pop
aload 1
aload 0
getfield android/support/v4/view/cc/m Landroid/support/v4/view/ce;
invokevirtual android/support/v4/view/ViewPager/setOnAdapterChangeListener(Landroid/support/v4/view/eu;)V
aload 0
aload 1
putfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
aload 0
getfield android/support/v4/view/cc/n Ljava/lang/ref/WeakReference;
ifnull L1
aload 0
getfield android/support/v4/view/cc/n Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v4/view/by
astore 1
L2:
aload 0
aload 1
aload 2
invokevirtual android/support/v4/view/cc/a(Landroid/support/v4/view/by;Landroid/support/v4/view/by;)V
return
L1:
aconst_null
astore 1
goto L2
.limit locals 3
.limit stack 3
.end method

.method protected onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
getfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
ifnull L0
aload 0
aload 0
getfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
invokevirtual android/support/v4/view/ViewPager/getAdapter()Landroid/support/v4/view/by;
aconst_null
invokevirtual android/support/v4/view/cc/a(Landroid/support/v4/view/by;Landroid/support/v4/view/by;)V
aload 0
getfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
aconst_null
invokevirtual android/support/v4/view/ViewPager/a(Landroid/support/v4/view/ev;)Landroid/support/v4/view/ev;
pop
aload 0
getfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
aconst_null
invokevirtual android/support/v4/view/ViewPager/setOnAdapterChangeListener(Landroid/support/v4/view/eu;)V
aload 0
aconst_null
putfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
L0:
return
.limit locals 1
.limit stack 3
.end method

.method protected onLayout(ZIIII)V
fconst_0
fstore 6
aload 0
getfield android/support/v4/view/cc/a Landroid/support/v4/view/ViewPager;
ifnull L0
aload 0
getfield android/support/v4/view/cc/h F
fconst_0
fcmpl
iflt L1
aload 0
getfield android/support/v4/view/cc/h F
fstore 6
L1:
aload 0
aload 0
getfield android/support/v4/view/cc/g I
fload 6
iconst_1
invokevirtual android/support/v4/view/cc/a(IFZ)V
L0:
return
.limit locals 7
.limit stack 4
.end method

.method protected onMeasure(II)V
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 4
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 3
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 1
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 2
iload 4
ldc_w 1073741824
if_icmpeq L0
new java/lang/IllegalStateException
dup
ldc "Must measure with an exact width"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual android/support/v4/view/cc/getMinHeight()I
istore 4
aload 0
invokevirtual android/support/v4/view/cc/getPaddingTop()I
aload 0
invokevirtual android/support/v4/view/cc/getPaddingBottom()I
iadd
istore 5
iconst_0
iload 1
i2f
ldc_w 0.8F
fmul
f2i
invokestatic java/lang/Math/max(II)I
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 6
iconst_0
iload 2
iload 5
isub
invokestatic java/lang/Math/min(II)I
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 7
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
iload 6
iload 7
invokevirtual android/widget/TextView/measure(II)V
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
iload 6
iload 7
invokevirtual android/widget/TextView/measure(II)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
iload 6
iload 7
invokevirtual android/widget/TextView/measure(II)V
iload 3
ldc_w 1073741824
if_icmpne L1
aload 0
iload 1
iload 2
invokevirtual android/support/v4/view/cc/setMeasuredDimension(II)V
return
L1:
aload 0
iload 1
iload 4
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
iload 5
iadd
invokestatic java/lang/Math/max(II)I
invokevirtual android/support/v4/view/cc/setMeasuredDimension(II)V
return
.limit locals 8
.limit stack 5
.end method

.method public requestLayout()V
aload 0
getfield android/support/v4/view/cc/k Z
ifne L0
aload 0
invokespecial android/view/ViewGroup/requestLayout()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public setGravity(I)V
aload 0
iload 1
putfield android/support/v4/view/cc/j I
aload 0
invokevirtual android/support/v4/view/cc/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public setNonPrimaryAlpha(F)V
.annotation invisibleparam 1 Landroid/support/a/n;
a D = 0.0D
b D = 1.0D
.end annotation
aload 0
ldc_w 255.0F
fload 1
fmul
f2i
sipush 255
iand
putfield android/support/v4/view/cc/s I
aload 0
getfield android/support/v4/view/cc/s I
bipush 24
ishl
aload 0
getfield android/support/v4/view/cc/e I
ldc_w 16777215
iand
ior
istore 2
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
iload 2
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
iload 2
invokevirtual android/widget/TextView/setTextColor(I)V
return
.limit locals 3
.limit stack 3
.end method

.method public setTextColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v4/view/cc/e I
aload 0
getfield android/support/v4/view/cc/c Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield android/support/v4/view/cc/s I
bipush 24
ishl
aload 0
getfield android/support/v4/view/cc/e I
ldc_w 16777215
iand
ior
istore 1
aload 0
getfield android/support/v4/view/cc/b Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield android/support/v4/view/cc/d Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public setTextSpacing(I)V
aload 0
iload 1
putfield android/support/v4/view/cc/i I
aload 0
invokevirtual android/support/v4/view/cc/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method
