.bytecode 50.0
.class synchronized android/support/v4/view/de
.super android/support/v4/view/dd

.method <init>()V
aload 0
invokespecial android/support/v4/view/dd/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final L(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getWindowSystemUiVisibility()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final T(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/isPaddingRelative()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Paint;)V
aload 1
aload 2
invokevirtual android/view/View/setLayerPaint(Landroid/graphics/Paint;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/view/View;IIII)V
aload 1
iload 2
iload 3
iload 4
iload 5
invokevirtual android/view/View/setPaddingRelative(IIII)V
return
.limit locals 6
.limit stack 5
.end method

.method public final e(Landroid/view/View;I)V
aload 1
iload 2
invokevirtual android/view/View/setLabelFor(I)V
return
.limit locals 3
.limit stack 2
.end method

.method public final f(Landroid/view/View;I)V
aload 1
iload 2
invokevirtual android/view/View/setLayoutDirection(I)V
return
.limit locals 3
.limit stack 2
.end method

.method public final j(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getLabelFor()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final k(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getLayoutDirection()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final r(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getPaddingStart()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final s(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getPaddingEnd()I
ireturn
.limit locals 2
.limit stack 1
.end method
