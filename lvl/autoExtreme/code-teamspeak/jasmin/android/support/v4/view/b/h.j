.bytecode 50.0
.class final synchronized android/support/v4/view/b/h
.super java/lang/Object
.implements android/view/animation/Interpolator

.field private static final 'a' F = 0.002F


.field private final 'b' [F

.field private final 'c' [F

.method public <init>(FF)V
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
astore 3
aload 3
fconst_0
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 3
fload 1
fload 2
fconst_1
fconst_1
invokevirtual android/graphics/Path/quadTo(FFFF)V
aload 0
aload 3
invokespecial android/support/v4/view/b/h/<init>(Landroid/graphics/Path;)V
return
.limit locals 4
.limit stack 5
.end method

.method public <init>(FFFF)V
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
astore 5
aload 5
fconst_0
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 5
fload 1
fload 2
fload 3
fload 4
fconst_1
fconst_1
invokevirtual android/graphics/Path/cubicTo(FFFFFF)V
aload 0
aload 5
invokespecial android/support/v4/view/b/h/<init>(Landroid/graphics/Path;)V
return
.limit locals 6
.limit stack 7
.end method

.method public <init>(Landroid/graphics/Path;)V
aload 0
invokespecial java/lang/Object/<init>()V
new android/graphics/PathMeasure
dup
aload 1
iconst_0
invokespecial android/graphics/PathMeasure/<init>(Landroid/graphics/Path;Z)V
astore 1
aload 1
invokevirtual android/graphics/PathMeasure/getLength()F
fstore 2
fload 2
ldc_w 0.002F
fdiv
f2i
iconst_1
iadd
istore 4
aload 0
iload 4
newarray float
putfield android/support/v4/view/b/h/b [F
aload 0
iload 4
newarray float
putfield android/support/v4/view/b/h/c [F
iconst_2
newarray float
astore 5
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 1
iload 3
i2f
fload 2
fmul
iload 4
iconst_1
isub
i2f
fdiv
aload 5
aconst_null
invokevirtual android/graphics/PathMeasure/getPosTan(F[F[F)Z
pop
aload 0
getfield android/support/v4/view/b/h/b [F
iload 3
aload 5
iconst_0
faload
fastore
aload 0
getfield android/support/v4/view/b/h/c [F
iload 3
aload 5
iconst_1
faload
fastore
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
return
.limit locals 6
.limit stack 4
.end method

.method private static a(FF)Landroid/graphics/Path;
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
astore 2
aload 2
fconst_0
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 2
fload 0
fload 1
fconst_1
fconst_1
invokevirtual android/graphics/Path/quadTo(FFFF)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(FFFF)Landroid/graphics/Path;
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
astore 4
aload 4
fconst_0
fconst_0
invokevirtual android/graphics/Path/moveTo(FF)V
aload 4
fload 0
fload 1
fload 2
fload 3
fconst_1
fconst_1
invokevirtual android/graphics/Path/cubicTo(FFFFFF)V
aload 4
areturn
.limit locals 5
.limit stack 7
.end method

.method public final getInterpolation(F)F
fload 1
fconst_0
fcmpg
ifgt L0
fconst_0
freturn
L0:
fload 1
fconst_1
fcmpl
iflt L1
fconst_1
freturn
L1:
aload 0
getfield android/support/v4/view/b/h/b [F
arraylength
iconst_1
isub
istore 4
iconst_0
istore 3
L2:
iload 4
iload 3
isub
iconst_1
if_icmple L3
iload 3
iload 4
iadd
iconst_2
idiv
istore 5
fload 1
aload 0
getfield android/support/v4/view/b/h/b [F
iload 5
faload
fcmpg
ifge L4
iload 5
istore 4
goto L2
L4:
iload 5
istore 3
goto L2
L3:
aload 0
getfield android/support/v4/view/b/h/b [F
iload 4
faload
aload 0
getfield android/support/v4/view/b/h/b [F
iload 3
faload
fsub
fstore 2
fload 2
fconst_0
fcmpl
ifne L5
aload 0
getfield android/support/v4/view/b/h/c [F
iload 3
faload
freturn
L5:
fload 1
aload 0
getfield android/support/v4/view/b/h/b [F
iload 3
faload
fsub
fload 2
fdiv
fstore 1
aload 0
getfield android/support/v4/view/b/h/c [F
iload 3
faload
fstore 2
fload 1
aload 0
getfield android/support/v4/view/b/h/c [F
iload 4
faload
fload 2
fsub
fmul
fload 2
fadd
freturn
.limit locals 6
.limit stack 3
.end method
